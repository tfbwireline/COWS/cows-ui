﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class CustScrdDataRepository : ICustScrdDataRepository
    {
        private readonly COWSAdminDBContext _context;

        public CustScrdDataRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public CustScrdData Create(CustScrdData entity)
        {
            _context.CustScrdData.Add(entity);
            SaveAll();

            return GetById(entity.Id);
        }

        public void Delete(int id)
        {
            CustScrdData cust = GetById(id);
            _context.CustScrdData.Remove(cust);

            SaveAll();
        }

        public IQueryable<CustScrdData> Find(Expression<Func<CustScrdData, bool>> predicate)
        {
            return _context.CustScrdData
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<CustScrdData> GetAll()
        {
            throw new NotImplementedException();
        }

        public CustScrdData GetById(int id)
        {
            return _context.CustScrdData
                            .SingleOrDefault(i => i.Id == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, CustScrdData entity)
        {
            CustScrdData cust = GetById(id);
            cust.BldgNme = entity.BldgNme;
            cust.CtryCd = entity.CtryCd;
            cust.CtryRgnNme = entity.CtryRgnNme;
            cust.CtyNme = entity.CtyNme;
            cust.CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
            cust.CustCntctNme = entity.CustCntctNme;
            cust.CustCntctPgrNbr = entity.CustCntctPgrNbr;
            cust.CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
            cust.CustCntctPhnNbr = entity.CustCntctPhnNbr;
            cust.CustEmailAdr = entity.CustEmailAdr;
            cust.CustNme = entity.CustNme;
            cust.EventDes = entity.EventDes;
            cust.EventTitleTxt = entity.EventTitleTxt;
            cust.FlrId = entity.FlrId;
            cust.FrstNme = entity.FrstNme;
            cust.LstNme = entity.LstNme;
            cust.NteTxt = entity.NteTxt;
            cust.RmNbr = entity.RmNbr;
            cust.SiteAdr = entity.SiteAdr;
            cust.SrvcAssrnPocCellPhnNbr = entity.SrvcAssrnPocCellPhnNbr;
            cust.SrvcAssrnPocNme = entity.SrvcAssrnPocNme;
            cust.SrvcAssrnPocPhnNbr = entity.SrvcAssrnPocPhnNbr;
            cust.StreetAdr1 = entity.StreetAdr1;
            cust.StreetAdr2 = entity.StreetAdr2;
            cust.StreetAdr3 = entity.StreetAdr3;
            cust.SttCd = entity.SttCd;
            cust.SttPrvnNme = entity.SttPrvnNme;
            cust.ZipPstlCd = entity.ZipPstlCd;

            cust.ScrdObjId = entity.ScrdObjId;
            cust.ScrdObjTypeId = entity.ScrdObjTypeId;
            cust.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}
