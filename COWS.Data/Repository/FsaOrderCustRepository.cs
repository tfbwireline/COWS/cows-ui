﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class FsaOrderCustRepository : IFsaOrderCustRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public FsaOrderCustRepository(COWSAdminDBContext context,
            ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public FsaOrdrCust Create(FsaOrdrCust entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<FsaOrdrCust> Find(Expression<Func<FsaOrdrCust, bool>> predicate)
        {
            return _context.FsaOrdrCust
                            .Where(predicate);
        }

        public IEnumerable<FsaOrdrCust> GetAll()
        {
            return _context.FsaOrdrCust
                            .OrderBy(i => i.OrdrId)
                            .ToList();
        }

        public FsaOrdrCust GetById(int id)
        {
            throw new NotImplementedException();
            //return _context.FsaOrdrCust
            //                .SingleOrDefault(i => i.OrdrId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, FsaOrdrCust entity)
        {
            throw new NotImplementedException();
        }

        public FsaOrdrCust GetByOrderIdAndCISLevelType(int id, string cisLvlType, int csgLvlId)
        {
            Ordr order = _context.Ordr.Where(a => a.OrdrId == id).FirstOrDefault();
            FsaOrdrCust obj = _context.FsaOrdrCust
                .Where(a => a.OrdrId == id && a.CisLvlType == cisLvlType)
                .AsNoTracking()
                .FirstOrDefault();

            if (order.CsgLvlId > 0 && order.CsgLvlId <= csgLvlId)
            {
                var securedNtwkCust = _context.CustScrdData
                    .Where(a => a.ScrdObjId == obj.FsaOrdrCustId && a.ScrdObjTypeId == (byte)SecuredObjectType.FSA_ORDR_CUST)
                    .FirstOrDefault();
                if (securedNtwkCust != null)
                {
                    obj.CustNme = _common.GetDecryptValue(securedNtwkCust.CustNme);
                }
            }

            return obj;
        }
    }
}