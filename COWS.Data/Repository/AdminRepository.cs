﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Data.Repository
{
    public class AdminRepository : IAdminRepository
    {
        private readonly COWSAdminDBContext _context;

        public AdminRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<GetSCMInterfaceView> GetSCMInterfaceView()
        {
            var psftInterfaceView = _context.Query<GetSCMInterfaceView>()
                .AsNoTracking()
                .FromSql("dbo.getSCMInterfaceView").ToList<GetSCMInterfaceView>();

            return psftInterfaceView;
        }

        public IEnumerable<GetNRMBPMInterfaceView> GetNRMBPMInterfaceView()
        {
            var nrmbpmInterfaceView = _context.Query<GetNRMBPMInterfaceView>()
                .AsNoTracking()
                .FromSql("dbo.getNRMBPMInterfaceView").ToList<GetNRMBPMInterfaceView>();

            return nrmbpmInterfaceView;
        }
    }
}