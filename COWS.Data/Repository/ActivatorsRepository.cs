﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using System.Data;
using System.Linq;

namespace COWS.Data.Repository
{
    public class ActivatorsRepository : IActivatorsRepository
    {
        private readonly COWSAdminDBContext _context;

        public ActivatorsRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public DataTable GetAllFailActy(int eventTypeId)
        {
            if (eventTypeId == 0)
            {
                return GetAllFailActy();
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("FailActyId");
                dt.Columns.Add("FailActyDes");

                var data = (from fa in _context.LkFailActy
                            where (fa.RecStusId == 1)
                            && fa.EventTypeId == eventTypeId
                            orderby fa.FailActyId
                            select new { fa.FailActyId, fa.FailActyDes });

                var reader = data.GetEnumerator();

                while (reader.MoveNext())
                {
                    var rw = reader.Current;
                    dt.Rows.Add(rw.FailActyId, rw.FailActyDes);
                }
                if (dt.Rows.Count > 0)
                    return dt;
                else
                    return GetAllFailActy();
            }
        }

        public DataTable GetAllFailActy()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FailActyId");
            dt.Columns.Add("FailActyDes");

            var data = (from fa in _context.LkFailActy
                        where (fa.RecStusId == 1)
                        && fa.EventTypeId == 1
                        orderby fa.FailActyId
                        select new { fa.FailActyId, fa.FailActyDes });

            var reader = data.GetEnumerator();

            while (reader.MoveNext())
            {
                var rw = reader.Current;
                dt.Rows.Add(rw.FailActyId, rw.FailActyDes);
            }

            return dt;
        }

        public DataTable GetAllSuccActy(int eventTypeId)
        {
            if (eventTypeId == 0)
            {
                return GetAllSuccActy();
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("SucssActyId");
                dt.Columns.Add("SucssActyDes");

                var data = (from sa in _context.LkSucssActy
                            where (sa.RecStusId == 1)
                            && sa.EventTypeId == eventTypeId
                            orderby sa.SucssActyId
                            select new { sa.SucssActyId, sa.SucssActyDes });

                var reader = data.GetEnumerator();

                while (reader.MoveNext())
                {
                    var rw = reader.Current;
                    dt.Rows.Add(rw.SucssActyId, rw.SucssActyDes);
                }

                if (dt.Rows.Count > 0)
                    return dt;
                else
                    return GetAllSuccActy();
            }
        }

        public DataTable GetAllSuccActy()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SucssActyId");
            dt.Columns.Add("SucssActyDes");

            var data = (from sa in _context.LkSucssActy
                        where (sa.RecStusId == 1)
                        && sa.EventTypeId == 1
                        orderby sa.SucssActyId
                        select new { sa.SucssActyId, sa.SucssActyDes });

            var reader = data.GetEnumerator();

            while (reader.MoveNext())
            {
                var rw = reader.Current;
                dt.Rows.Add(rw.SucssActyId, rw.SucssActyDes);
            }

            return dt;
        }

        public DataTable GetAllFailCodes()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("failReasId");
            dt.Columns.Add("failReasDes");
            dt.Columns.Add("mdsFastTrkCd");
            dt.Columns.Add("ntwkOnlyCd");

            var data = (from fa in _context.LkFailReas
                        where (fa.RecStusId == 1)
                        orderby fa.FailReasId
                        select new { fa.FailReasId, fa.FailReasDes, fa.MdsFastTrkCd, fa.NtwkOnlyCd });

            var reader = data.GetEnumerator();

            while (reader.MoveNext())
            {
                var rw = reader.Current;
                dt.Rows.Add(rw.FailReasId, rw.FailReasDes, rw.MdsFastTrkCd, rw.NtwkOnlyCd);
            }

            return dt;
        }
    }
}