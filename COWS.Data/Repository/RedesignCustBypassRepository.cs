﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class RedesignCustBypassRepository : IRedesignCustBypassRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;
        private readonly ICommonRepository _common;

        public RedesignCustBypassRepository(COWSAdminDBContext context,
            IConfiguration configuration, ICommonRepository common)
        {
            _context = context;
            _configuration = configuration;
            _common = common;
        }

        public RedsgnCustBypass Create(RedsgnCustBypass entity)
        {
            //int maxId = _context.RedsgnCustBypass.Max(i => i.Id);
            //entity.Id = (short)++maxId;
            _context.RedsgnCustBypass.Add(entity);

            SaveAll();

            if (entity.CsgLvlId > 0)
            {
                var cust = GetById(entity.Id);
                CustScrdData securedData = new CustScrdData();
                securedData.ScrdObjTypeId = (byte)ScrdObjType.REDSGN_CUST_BYPASS;
                securedData.ScrdObjId = cust.Id;
                securedData.CustNme = _common.GetEncryptValue(entity.CustNme);
                securedData.CreatDt = DateTime.Now;

                _context.CustScrdData.Add(securedData);

                cust.CustNme = null;
                _context.RedsgnCustBypass.Update(cust);

                SaveAll();
            }

            return GetById(entity.Id);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<RedsgnCustBypass> Find(Expression<Func<RedsgnCustBypass, bool>> predicate)
        {
            var res = _context.RedsgnCustBypass
                                .Where(predicate)
                                .AsNoTracking();

            return res;
        }

        public IEnumerable<RedsgnCustBypass> GetAll()
        {
            return _context.RedsgnCustBypass
                            .AsNoTracking()
                            .ToList();
        }

        public RedsgnCustBypass GetById(int id)
        {
            return _context.RedsgnCustBypass
                            .SingleOrDefault(i => i.Id == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, RedsgnCustBypass entity)
        {
            RedsgnCustBypass cust = GetById(id);

            cust.H1Cd = entity.H1Cd;
            cust.CsgLvlId = entity.CsgLvlId;
            cust.RecStusId = entity.RecStusId;
            cust.ModfdByUserId = entity.ModfdByUserId;
            cust.ModfdDt = entity.ModfdDt;

            if (entity.CsgLvlId > 0)
            {
                cust.CustNme = null;
                CustScrdData securedData = _context.CustScrdData
                    .FirstOrDefault(i => i.ScrdObjId == entity.Id 
                        && i.ScrdObjTypeId == (byte)ScrdObjType.REDSGN_CUST_BYPASS);

                // Insert new Customer Secured Data
                if (securedData == null)
                {
                    securedData = new CustScrdData();
                    securedData.ScrdObjTypeId = (byte)ScrdObjType.REDSGN_CUST_BYPASS;
                    securedData.ScrdObjId = cust.Id;
                    securedData.CustNme = _common.GetEncryptValue(entity.CustNme);
                    securedData.CreatDt = DateTime.Now;

                    _context.CustScrdData.Add(securedData);
                }
                // Update existing Customer Secured Data
                else
                {
                    securedData.CustNme = _common.GetEncryptValue(entity.CustNme);
                    securedData.ModfdDt = DateTime.Now;

                    _context.CustScrdData.Update(securedData);
                }
            }
            else
            {
                cust.CustNme = entity.CustNme;
            }

            SaveAll();
        }

        public DataTable GetCustomersBypass()
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("dbo.GetCustomerBypass", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.Parameters.AddWithValue("@IsNewUI", 1);
            command.CommandType = CommandType.StoredProcedure;
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public List<string> GetCustomersBypassByID(int ID, int csgLvlId)
        {
            var y = (from x in _context.RedsgnCustBypass
                     join csd in _context.CustScrdData on new { ScrdObjId = x.Id, ScrdObjTypeId = (byte)ScrdObjType.REDSGN_CUST_BYPASS } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                         into joincsd
                     from csd in joincsd.DefaultIfEmpty()
                     where x.Id == ID
                     select new { x.H1Cd, x.RecStusId, x.CustNme, x.CsgLvlId, CUST_NME_CSD = csd.CustNme }).SingleOrDefault();

            List<string> csdList = new List<string>();
            if (y != null)
            {
                csdList.Add(y.H1Cd);
                csdList.Add(y.RecStusId.ToString());
                csdList.Add(((y.CsgLvlId > 0) && (csgLvlId != 0) && (csgLvlId <= y.CsgLvlId)) ? _common.GetDecryptValue(y.CUST_NME_CSD) : y.CustNme);
            }

            return csdList;

        }

        public int CreateCustomerByPass(RedsgnCustBypass _cust, int loggedInUserId)
        {
            List<Entities.QueryModels.CSGLevel> _csgLvl = _common.GetM5CSGLvl(_cust.H1Cd, string.Empty, string.Empty);
            byte csgLvlID = 0;
            var sl = (from c in _csgLvl select c.CsgLevelID).Distinct();
            if (sl != null && sl.Count() == 1)
                csgLvlID = sl.First();
            else
                csgLvlID = sl.Take(1).Single();
            if (_cust.Id == 0)
            {

                RedsgnCustBypass cust = new RedsgnCustBypass();
                CustScrdData newCSD = new CustScrdData();

                cust = _cust;
                cust.CreatDt = DateTime.Now;
                cust.CreatByUserId = loggedInUserId;

                newCSD.ScrdObjTypeId = (byte)ScrdObjType.REDSGN_CUST_BYPASS;
                newCSD.CustNme = _common.GetEncryptValue(_cust.CustNme);

                _context.RedsgnCustBypass.Add(cust);
                newCSD.ScrdObjId = cust.Id;
                if (newCSD.ScrdObjId > 0)
                {
                    newCSD.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(newCSD);
                }
                _context.SaveChanges();

                return cust.Id;

            }
            else
            {
                // Update

                RedsgnCustBypass cust = (from x in _context.RedsgnCustBypass
                                         where x.Id == _cust.Id
                                         select x).Single();

                CustScrdData updtCSD = (CustScrdData)(from csd in _context.CustScrdData where csd.ScrdObjId == _cust.Id && csd.ScrdObjTypeId == ((byte)ScrdObjType.REDSGN_CUST_BYPASS) select csd).SingleOrDefault();
                bool bInsertCSD = false;
                if (updtCSD == null)
                {
                    updtCSD = new CustScrdData();
                    bInsertCSD = true;
                }

                cust.H1Cd = _cust.H1Cd;
                if (csgLvlID > 0)
                {
                    updtCSD.ScrdObjId = _cust.Id;
                    updtCSD.ScrdObjTypeId = (byte)ScrdObjType.REDSGN_CUST_BYPASS;
                    updtCSD.CustNme = _common.GetEncryptValue(_cust.CustNme);
                }
                else
                    cust.CustNme = _cust.CustNme;
                cust.RecStusId = _cust.RecStusId;
                cust.ModfdDt = DateTime.Now;
                cust.ModfdByUserId = loggedInUserId;

                if ((bInsertCSD) && (csgLvlID > 0) && (updtCSD.ScrdObjId > 0))
                {
                    updtCSD.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(updtCSD);
                }
                else if ((!bInsertCSD) && (csgLvlID == 0) && (updtCSD.ScrdObjId > 0))
                    _context.CustScrdData.Remove(updtCSD);
                _context.SaveChanges();

                return cust.Id;

            }
        }

        public bool GetRedesignCustomerBypassCD(string H1_CD, string Cust_Name, int csgLvlId)
        {
            bool bRet = false;

            var c = (from a in _context.RedsgnCustBypass
                     join csd in _context.CustScrdData on new { ScrdObjId = a.Id, ScrdObjTypeId = (byte)ScrdObjType.REDSGN_CUST_BYPASS } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                            into joincsd
                     from csd in joincsd.DefaultIfEmpty()
                     where a.H1Cd == H1_CD
                            && a.RecStusId == true
                     select new { a, csd });

            if (c != null && c.Count() > 0)
            {
                string cust_nme;
                foreach (var _c in c)
                {
                    cust_nme = (_c.a.CsgLvlId == 0) ? _c.a.CustNme : (((_c.a.CsgLvlId > 0) && (csgLvlId != 0)
                                    && (csgLvlId <= _c.a.CsgLvlId)) ? _common.GetDecryptValue(_c.csd.CustNme) : string.Empty);
                    if (cust_nme == Cust_Name)
                        bRet = true;
                }
            }

            return bRet;
        }

        public bool CheckRedesignCustomerBypassCDIfExist(string H1_CD, int ID = 0)
        {
            var data = _context.RedsgnCustBypass.Where(x => x.H1Cd == H1_CD).FirstOrDefault();  

            if (ID > 0) // Update
            {
                if(data != null) // H1 Exist
                {
                    return (data.Id != ID);
                }
                // H1 do not exist
                return false;
            }
            else
            {
                return (data != null);
            }
        }
    }
}
