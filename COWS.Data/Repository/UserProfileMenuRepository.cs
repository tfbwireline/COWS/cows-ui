﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UserProfileMenuRepository : IUserProfileMenuRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public UserProfileMenuRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkUsrPrfMenu> Find(Expression<Func<LkUsrPrfMenu, bool>> predicate)
        {
            return _context.LkUsrPrfMenu
                            .Include(a => a.Menu)
                            .Include(a => a.UsrPrf)
                                .ThenInclude(b => b.MapUsrPrf)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkUsrPrfMenu> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUsrPrfMenu, out List<LkUsrPrfMenu> list))
            {
                list = _context.LkUsrPrfMenu
                            .Include(a => a.Menu)
                            .Include(a => a.UsrPrf)
                                .ThenInclude(b => b.MapUsrPrf)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkUsrPrfMenu, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkUsrPrfMenu GetById(int id)
        {
            return _context.LkUsrPrfMenu
                .Include(a => a.Menu)
                .Include(a => a.UsrPrf)
                    .ThenInclude(b => b.MapUsrPrf)
                .SingleOrDefault(a => a.UsrPrfMenuId == id);
        }

        public LkUsrPrfMenu Create(LkUsrPrfMenu entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkUsrPrfMenu entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}