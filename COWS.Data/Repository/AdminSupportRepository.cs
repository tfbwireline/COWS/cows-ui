﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class AdminSupportRepository : IAdminSupportRepository
    {
        private readonly COWSAdminDBContext _context;

        public AdminSupportRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public DataSet GetEventByEventId(int eventId, int csgLvlId)
        {
            DataSet result = new DataSet();
            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(_context.Database.GetDbConnection());
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.GetEventDataByEventID @EVENT_ID, @csglvl";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EVENT_ID", SqlDbType = SqlDbType.Int, Value = eventId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@csglvl", SqlDbType = SqlDbType.Int, Value = csgLvlId });

                _context.Database.OpenConnection();
                using (DbDataAdapter adapter = dbFactory.CreateDataAdapter())
                {
                    adapter.SelectCommand = command;
                    adapter.Fill(result);
                }
            }
            _context.Database.CloseConnection();
            return result;
        }

        public DataSet GetOrderByFtn(string ftn, int csgLvlId)
        {
            DataSet result = new DataSet();
            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(_context.Database.GetDbConnection());
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.GetOrderDataByFTN @ftn, @csglvl";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@ftn", SqlDbType = SqlDbType.VarChar, Value = ftn });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@csglvl", SqlDbType = SqlDbType.Int, Value = csgLvlId });

                _context.Database.OpenConnection();
                using (DbDataAdapter adapter = dbFactory.CreateDataAdapter())
                {
                    adapter.SelectCommand = command;
                    adapter.Fill(result);
                }
            }
            _context.Database.CloseConnection();
            return result;
        }

        public DataSet GetCPTOrRedesign(string cptNo, int csgLvlId, byte isRedesign)
        {
            DataSet result = new DataSet();
            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(_context.Database.GetDbConnection());
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.getCPTorRedsgnData @Input, @csglvl, @IsRedsgn";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Input", SqlDbType = SqlDbType.VarChar, Value = cptNo });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@csglvl", SqlDbType = SqlDbType.Int, Value = csgLvlId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@IsRedsgn", SqlDbType = SqlDbType.Bit, Value = isRedesign });

                _context.Database.OpenConnection();
                using (DbDataAdapter adapter = dbFactory.CreateDataAdapter())
                {
                    adapter.SelectCommand = command;
                    adapter.Fill(result);
                }
            }
            _context.Database.CloseConnection();
            return result;
        }

        public DataSet GetBPM(string h6)
        {
            DataSet result = new DataSet();
            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(_context.Database.GetDbConnection());
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.GetBPMByH6 @H6";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });

                _context.Database.OpenConnection();
                using (DbDataAdapter adapter = dbFactory.CreateDataAdapter())
                {
                    adapter.SelectCommand = command;
                    adapter.Fill(result);
                }
            }
            _context.Database.CloseConnection();
            return result;
        }

        public int DeleteOrder(int orderId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.deletePartialFSAOrder @ORDR_ID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@ORDR_ID", SqlDbType = SqlDbType.Int, Value = orderId });

                _context.Database.OpenConnection();
                int count = (int)command.ExecuteScalar();
                _context.Database.CloseConnection();

                return count;
            }
        }

        public string ExtractOrder(int m5OrderId, int relatedM5OrderId, string orderType, string deviceId, bool isTransaction)
        {
            //List<SqlParameter> pc = new List<SqlParameter>
            //{
            //    new SqlParameter() {ParameterName = "@M5_ORDR_ID", SqlDbType = SqlDbType.Int, Value = m5OrderId},
            //    new SqlParameter() {ParameterName = "@M5_RELT_ORDR_ID", SqlDbType = SqlDbType.Int, Value = relatedM5OrderId},
            //    new SqlParameter() {ParameterName = "@ORDR_TYPE_CD", SqlDbType = SqlDbType.VarChar, Value = orderType},
            //    new SqlParameter() {ParameterName = "@QDA_ORDR_ID", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Output},
            //    new SqlParameter() {ParameterName = "@DEVICE_ID", SqlDbType = SqlDbType.VarChar, Value = deviceId}
            //};

            //string command = (isTransaction ? "dbo.insertMach5TxnDetails" : "dbo.insertMach5OrderDetails") + " @M5_ORDR_ID, @M5_RELT_ORDR_ID, @ORDR_TYPE_CD, @QDA_ORDR_ID OUT, @DEVICE_ID";
            //_context.Database.ExecuteSqlCommand("dbo.insertMach5TxnDetails @M5_ORDR_ID, @M5_RELT_ORDR_ID, @ORDR_TYPE_CD, @QDA_ORDR_ID OUT, @DEVICE_ID", pc.ToArray());
            //string orderId = pc[3].Value.ToString();
            //return orderId;

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                string qdaOrderId = "";

                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = (isTransaction ? "dbo.insertMach5TxnDetails" : "dbo.insertMach5OrderDetails") + " @M5_ORDR_ID, @M5_RELT_ORDR_ID, @ORDR_TYPE_CD, @QDA_ORDR_ID OUT, @DEVICE_ID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@M5_ORDR_ID", SqlDbType = SqlDbType.Int, Value = m5OrderId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@M5_RELT_ORDR_ID", SqlDbType = SqlDbType.Int, Value = relatedM5OrderId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@ORDR_TYPE_CD", SqlDbType = SqlDbType.VarChar, Value = orderType });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@QDA_ORDR_ID", SqlDbType = SqlDbType.VarChar, Size = 100, Direction = ParameterDirection.Output });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@DEVICE_ID", SqlDbType = SqlDbType.VarChar, Value = deviceId });

                _context.Database.OpenConnection();
                int count = command.ExecuteNonQuery();
                _context.Database.CloseConnection();

                var asd = command.Parameters[3];

                return command.Parameters["@QDA_ORDR_ID"].Value.ToString();
            }
        }
    }
}