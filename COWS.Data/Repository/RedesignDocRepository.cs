﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class RedesignDocRepository : IRedesignDocRepository
    {
        private readonly COWSAdminDBContext _context;

        public RedesignDocRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public RedsgnDoc Create(RedsgnDoc entity)
        {
            int maxId = _context.RedsgnDoc.Max(i => i.RedsgnDocId);
            entity.RedsgnDocId = (short)++maxId;
            _context.RedsgnDoc.Add(entity);

            SaveAll();

            return GetById(entity.RedsgnDocId);
        }

        void IRepository<RedsgnDoc>.Delete(int id)
        {
            RedsgnDoc proj = GetById(id);
            _context.RedsgnDoc.Remove(proj);

            SaveAll();
        }

        public IQueryable<RedsgnDoc> Find(Expression<Func<RedsgnDoc, bool>> predicate)
        {
            return _context.RedsgnDoc
                            .Include(i => i.CreatByUser)
                            .Where(predicate);
        }

        public IEnumerable<RedsgnDoc> GetAll()
        {
            return _context.RedsgnDoc
                            .Include(i => i.CreatByUser)
                            .OrderBy(i => i.FileNme)
                            .ToList();
        }

        public RedsgnDoc GetById(int id)
        {
            return _context.RedsgnDoc
                            .Include(i => i.CreatByUser)
                            .SingleOrDefault(i => i.RedsgnDocId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, RedsgnDoc entity)
        {
            RedsgnDoc doc = GetById(id);
            doc.RecStusId = entity.RecStusId;
            doc.ModfdByUserId = entity.ModfdByUserId;
            doc.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
        
        public List<RedsgnDoc> SelectContextObject(string SortField, string SearchCriteria, params object[] parameters)
        {
            return _context.RedsgnDoc.Where(SearchCriteria, parameters).OrderBy(SortField).ToList();
        }

        public List<RedsgnDoc> SelectContextObject(string SortField, string SearchCriteria)
        {
            return _context.RedsgnDoc.Where(SearchCriteria).OrderBy(SortField).ToList();
        }

        public bool Insert(DocEntity Obj)
        {
            //var c = new RedsgnDoc();
            //c.FileNme = Obj.Name.Replace(";", string.Empty).Replace(",", string.Empty);
            //c.RedsgnId = Obj.Id;
            //c.FileCntnt = Obj.Content;
            //c.FileSizeQty = Obj.Size;
            ////c.ORDR_ID = Obj.OrderId;

            //c.CreatByUserId = Obj.CreatedByUserId;
            //c.CreatDt = DateTime.Now;

            //_context.RedsgnDoc.Add(c);
            //_context.SaveChanges();
            return true;
        }

        public bool Update(DocEntity Obj)
        {
            //var c = _context.RedsgnDoc.Where("RedsgnId == @0", Obj.Id).Single();
            //c.FileNme = Obj.Name.Replace(";", string.Empty).Replace(",", string.Empty);
            //c.RedsgnId = Obj.Id;
            //c.FileCntnt = Obj.Content;
            //c.FileSizeQty = Obj.Size;
            //// c.ORDR_ID = Obj.OrderId;

            //c.ModfdByUserId = Obj.ModifiedByUserId;
            //c.ModfdDt = DateTime.Now;
            ////c.CMNT_TXT = Obj.comments;
            //_context.SaveChanges();
            return true;
        }

        public bool Delete(int Id)
        {
            _context.RedsgnDoc.Remove(_context.RedsgnDoc.Where("RedsgnDocId == @0", Id).Single());
            _context.SaveChanges();
            return true;
        }

        public bool Delete(int[] Ids)
        {
            var objects = from s in _context.RedsgnDoc where Ids.Contains(s.RedsgnDocId) select s;

            _context.RedsgnDoc.RemoveRange(objects);
            _context.SaveChanges();
            return false;
        }

        public DocEntity Get(int id)
        {
            var userAdid = GetUserAdid(_context.RedsgnDoc.Where(a => a.RedsgnDocId == id).SingleOrDefault().CreatByUserId);
            return ReturnRelativeEntity(_context.RedsgnDoc.Where("RedsgnDocId == @0", id).Single(), userAdid);
        }

        public DocEntity GetUnique(int id, string fileName)
        {
            var l = _context.RedsgnDoc.Where("RedsgnDocId == @0 AND FileNme=@1", id, fileName);

            var ls = l.SingleOrDefault();
            if (ls != null)
            {
                var userAdid = GetUserAdid(ls.CreatByUserId);
                return ReturnRelativeEntity(ls, userAdid);
            }

            return null;
        }

        public List<DocEntity> Select(string SortField)
        {
            var l = new List<DocEntity>();
            foreach (var c in _context.RedsgnDoc.Where("RedsgnDocId != 0").OrderBy(SortField))
            {
                var userAdid = GetUserAdid(c.CreatByUserId);
                l.Add(ReturnRelativeEntity(c, userAdid));
            }
            return l;
        }

        public List<DocEntity> Select(string SortField, string SearchCriteria)
        {
            if (SearchCriteria == "")
                return Select(SortField);

            var l = new List<DocEntity>();
            foreach (var c in _context.RedsgnDoc.Where(SearchCriteria).OrderBy(SortField))
            {
                var userAdid = GetUserAdid(c.CreatByUserId);
                l.Add(ReturnRelativeEntity(c, userAdid));
            }
            return l;
        }

        private static DocEntity ReturnRelativeEntity(RedsgnDoc c, string userAdid)
        {
            var Obj = new DocEntity();
            //Obj.DocId = c.RedsgnDocId;
            //Obj.Name = c.FileNme;
            //Obj.Id = c.RedsgnId;
            //Obj.Content = c.FileCntnt.ToArray();
            //Obj.Size = c.FileSizeQty;

            //Obj.sSize = string.Format("{0:N0} KB", c.FileSizeQty / 1000);
            //Obj.CreatedByUserId = c.CreatByUserId;
            //Obj.CreatedByUserName = userAdid;
            //Obj.CreatedDateTime = c.CreatDt ?? System.DateTime.Now;
            // Obj.OrderId = c.ORDR_ID;
            //Obj.comments = c.CMNT_TXT;
            //if (c.CreatByUser != null)
            //{
            //    Obj.CreatedByUserId = c.CreatByUser.UserId;
            //    Obj.CreatedByUserName = c.CreatByUser.UserAdid;
            //    Obj.CreatedDateTime = c.CreatDt ?? System.DateTime.Now;
            //}

            //if (c.ModfdByUserId != null)
            //{
            //    Obj.ModifiedByUserId = c.ModfdByUserId ?? 0;
            //    //Obj.ModifiedByUserName = c.MODFD_BY_USER_ID ?? string.Empty;
            //    Obj.ModifiedDateTime = (DateTime)c.ModfdDt;
            //}
            return Obj;
        }

        private string GetUserAdid(int creatByUserId)
        {
            var user = _context.LkUser.Where(a => a.UserId == creatByUserId).SingleOrDefault();
            return user.UserAdid;
        }

        public int CountRedesignDocs(int Id)
        {
            int docCount = (
                from rdoc in _context.RedsgnDoc
                where (rdoc.RedsgnDocId == Id)
                select rdoc).Count();

            return docCount;
        }
    }
}