﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class FsaOrderCpeLineItemRepository : IFsaOrderCpeLineItemRepository
    {
        private readonly COWSAdminDBContext _context;

        public FsaOrderCpeLineItemRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<FsaOrdrCpeLineItem> Find(Expression<Func<FsaOrdrCpeLineItem, bool>> predicate)
        {
            return _context.FsaOrdrCpeLineItem
                            .Include(a => a.FsaOrdrGomXnci)
                            .AsNoTracking()
                            .Where(predicate);
        }

        public IEnumerable<FsaOrdrCpeLineItem> GetAll()
        {
            return _context.FsaOrdrCpeLineItem
                            .AsNoTracking()
                            .ToList();
        }

        public FsaOrdrCpeLineItem GetById(int id)
        {
            return _context.FsaOrdrCpeLineItem
                .SingleOrDefault(a => a.FsaCpeLineItemId == id);
        }

        public FsaOrdrCpeLineItem Create(FsaOrdrCpeLineItem entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, FsaOrdrCpeLineItem entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}