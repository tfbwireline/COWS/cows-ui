﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class SprintHolidayRepository : ISprintHolidayRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public SprintHolidayRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkSprintHldy Create(LkSprintHldy entity)
        {
            int maxId = _context.LkSprintHldy.Max(i => i.SprintHldyId);
            entity.SprintHldyId = (short)++maxId;
            _context.LkSprintHldy.Add(entity);

            SaveAll();

            return GetById(entity.SprintHldyId);
        }

        public void Delete(int id)
        {
            LkSprintHldy hday = GetById(id);
            _context.LkSprintHldy.Remove(hday);

            SaveAll();
        }

        public IQueryable<LkSprintHldy> Find(Expression<Func<LkSprintHldy, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkSprintHldy, out List<LkSprintHldy> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkSprintHldy> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkSprintHldy, out List<LkSprintHldy> list))
            {
                list = _context.LkSprintHldy
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderBy(i => i.SprintHldyDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkSprintHldy, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkSprintHldy GetById(int id)
        {
            return _context.LkSprintHldy
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.SprintHldyId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkSprintHldy);
            return _context.SaveChanges();
        }

        public void Update(int id, LkSprintHldy entity)
        {
            LkSprintHldy hday = GetById(id);
            hday.SprintHldyDes = entity.SprintHldyDes;
            hday.SprintHldyDt = entity.SprintHldyDt;
            hday.RecStusId = entity.RecStusId;
            hday.ModfdByUserId = entity.ModfdByUserId;
            hday.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}