﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Data.Repository
{
    public class PeopleSoftInterfaceRepository : IPeopleSoftInterfaceRepository
    {
        private readonly COWSAdminDBContext _context;

        public PeopleSoftInterfaceRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<GetSCMInterfaceView> GetSCMInterfaceView()
        {
            var psftInterfaceView = _context.Query<GetSCMInterfaceView>()
                .AsNoTracking()
                .FromSql("dbo.getSCMInterfaceView").ToList<GetSCMInterfaceView>();

            return psftInterfaceView;
        }
    }
}