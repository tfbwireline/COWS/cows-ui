﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Data.Repository
{
    public class WorkGroupRepository : IWorkGroupRepository
    {
        private readonly COWSAdminDBContext _context;

        public WorkGroupRepository(COWSAdminDBContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public DataSet GetOrderDisplayViewData(int userId, int usrPrfId)
        {
            SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.getOrderDisplayViewData_V2", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@USER_ID", userId);
            command.Parameters.AddWithValue("@USR_PRF_ID", usrPrfId);
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;
        }

        public async Task<DataTable> GetWGData(int userPrfId, int userId, int orderId, bool isCmplt, byte userCsgLevelId, string filter = null)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"web.getWGData_V2 {userPrfId}, {userId}, {orderId}, {isCmplt}, {userCsgLevelId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);

                    if (filter != "null")
                    {
                        DataView DV = dt.DefaultView;
                        DV.RowFilter = filter;

                        DataTable dtDstnct = DV.ToTable();

                        // For locking flag
                        dtDstnct.Columns.AddRange(new DataColumn[2] {
                            new DataColumn("IsLocked", typeof(bool)),
                            new DataColumn("LockedBy", typeof(string))
                        });

                        if (dtDstnct != null && dtDstnct.Rows.Count > 0)
                        {
                            var lockedOrders = _context.OrdrRecLock
                                                        .Include(i => i.LockByUser)
                                                        .ToList();

                            for (int i = 0; i < dtDstnct.Rows.Count; i++)
                            {
                                DataRow row = dtDstnct.Rows[i];
                                if (lockedOrders.Any(a => a.OrdrId == Convert.ToInt32(row["ORDR_ID"].ToString())))
                                {
                                    row["IsLocked"] = true;
                                    row["LockedBy"] = lockedOrders
                                        .FirstOrDefault(a => a.OrdrId == Convert.ToInt32(row["ORDR_ID"].ToString()))
                                        .LockByUser.FullNme;
                                }
                            }
                        }

                        return dtDstnct;
                    }
                    else
                    {
                        // For locking flag
                        dt.Columns.AddRange(new DataColumn[2] {
                            new DataColumn("IsLocked", typeof(bool)),
                            new DataColumn("LockedBy", typeof(string))
                        });

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            var lockedOrders = _context.OrdrRecLock
                                                        .Include(i => i.LockByUser)
                                                        .ToList();

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow row = dt.Rows[i];
                                if (lockedOrders.Any(a => a.OrdrId == Convert.ToInt32(row["ORDR_ID"].ToString())))
                                {
                                    row["IsLocked"] = true;
                                    row["LockedBy"] = lockedOrders
                                        .FirstOrDefault(a => a.OrdrId == Convert.ToInt32(row["ORDR_ID"].ToString()))
                                        .LockByUser.FullNme;
                                }
                            }
                        }

                        return dt;
                    }
                }
            }
        }

        public int VerifyUserIsAdminToEditCompletedOrder(string userADID, string parameter)
        {
            int iCnt = 0;

            var i = from c in _context.LkSysCfg
                    where c.PrmtrNme == parameter
                       && c.RecStusId == 1
                       && c.PrmtrValuTxt == userADID
                    select c;

            iCnt = i.Count();

            return iCnt;
        }

        public async Task<DataTable> GetFTNListDetails(int orderId, int userPrfId, int userCsgLvlId = 0)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"web.getFTNList_V2 {orderId}, {userPrfId}, {userCsgLvlId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public DataTable GetFTNListDetailsForOrderView(int orderId, int userPrfId, int userCsgLvlId = 0)
        {

            SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.getFTNList_V2", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ORDR_ID", orderId);
            command.Parameters.AddWithValue("@PRT_PRF_ID", userPrfId);
            command.Parameters.AddWithValue("@UserCsgLvlID", userCsgLvlId);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public int CompleteActiveTask(int orderID, int taskID, Int16 taskStatus, string comments, int userID)
        {
            if (comments == null)
            {
                comments = "";
            }
            if (taskID != 106 && taskID != 109)
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@p0", SqlDbType.Int),
                    new SqlParameter("@p1", SqlDbType.Int),
                    new SqlParameter("@p2", SqlDbType.SmallInt),
                    new SqlParameter("@p3", SqlDbType.VarChar),
                    new SqlParameter("@p4", SqlDbType.Int),
                };
                pc[0].Value = orderID;
                pc[1].Value = taskID;
                pc[2].Value = taskStatus;
                pc[3].Value = comments;
                pc[4].Value = userID;

                int toReturn = _context.Database.ExecuteSqlCommand("dbo.completeActiveTask @p0, @p1,@p2,@p3,@p4", pc.ToArray());
                return toReturn;
            }
            else
            {
                var tcat = (from at in _context.ActTask
                            where at.OrdrId == orderID && at.TaskId == taskID
                            select at);
                var tc = tcat.ToList();

                if (tc != null)
                {
                    foreach (var t in tc)
                    {
                        t.StusId = 2;
                        t.ModfdDt = DateTime.Now;
                    }

                    OrdrNte note = new OrdrNte();
                    note.CreatByUserId = userID;
                    note.CreatDt = DateTime.Now;
                    note.NteTxt = comments;
                    note.OrdrId = orderID;
                    note.RecStusId = 1;
                    note.NteTypeId = 9;

                    _context.OrdrNte.Add(note);
                    SaveAll();

                    return 1;
                }
                else
                    return 0;
            }
        }

        public string GetLatestNonSystemOrderNoteInfo(int orderID)
        {
            string oNteInfo = string.Empty;

            var s = (
                        from otn in _context.OrdrNte
                        join lu in _context.LkUser on otn.CreatByUserId equals lu.UserId
                        where otn.CreatByUserId != 1
                            && otn.OrdrId == orderID
                        orderby otn.NteId descending
                        select new { lu.DsplNme, lu.UserId }
                    ).ToArray();


            if (s != null && s.Count() > 0)
            {
                oNteInfo = s[0].DsplNme;

                var ugname = (from ugr in _context.MapUsrPrf
                              join lg in _context.LkUsrPrf on ugr.UsrPrfId equals lg.UsrPrfId
                              where ugr.UserId == s[0].UserId
                                && ugr.RecStusId == 1
                              select new { lg.UsrPrfNme, lg.UsrPrfId }).Distinct().ToArray();


                if (ugname != null && ugname.Count() > 0)
                {
                    if (ugname.Count() == 1)
                        oNteInfo += " - Profile: " + ugname[0].UsrPrfNme;
                    else
                    {
                        var xnci = from ug in ugname
                                   where ug.UsrPrfId == 2 || ug.UsrPrfId == 16 || ug.UsrPrfId == 100
                                   select ug;
                        if (xnci != null && xnci.Count() > 1)
                        {
                            var orgn = from o in _context.Ordr
                                       join lrg in _context.LkXnciRgn on o.RgnId equals lrg.RgnId
                                       join lg in _context.LkUsrPrf on lrg.UsrPrfId equals lg.UsrPrfId
                                       where o.OrdrId == orderID
                                       select lg.UsrPrfNme;

                            if (orgn != null && orgn.Count() > 0)
                                oNteInfo += " - Profile: " + orgn.First();
                            else
                                oNteInfo += " - Profile: " + ugname[0].UsrPrfNme;
                        }
                        else
                            oNteInfo += " - Profile: " + ugname[0].UsrPrfNme;
                    }
                }
            }
            return oNteInfo;

        }

        public int GetPreSubmitRTSStatus(int _orderID)
        {
            var ps = (from c in _context.OrdrNte
                      where c.OrdrId == _orderID
                         && c.NteTxt == "GOM user initiated RTS on this order."
                      select c).Count();
            return Convert.ToInt32(ps);

        }

        public bool OrderExistsInSalesSupportWG(int orderID)
        {
            var rspn = false;
            var c = (from _c in _context.ActTask
                     where _c.OrdrId == orderID
                        && _c.TaskId == 500
                        && _c.StusId == 0
                     select _c.OrdrId).ToArray();

            if (c != null && c.Count() > 0)
                rspn = true;
            else
                rspn = false;

            return rspn;
        }

        public bool HasBillMissingTask(int iOrderID)
        {
            var mbt = from f in _context.ActTask where f.OrdrId == iOrderID && f.TaskId == 109 && f.StusId == 0 select f;

            if (mbt != null && mbt.Count() > 0)
                return true;
            else
                return false;
        }

        public DataTable GetH5FolderInfo(int _OrderID)
        {
            var s = (from od in _context.Ordr
                     join hf in _context.H5Foldr on od.H5H6CustId equals hf.CustId
                     into hf1
                     from hf in hf1.DefaultIfEmpty()
                     where (od.OrdrId == _OrderID)
                     select new
                     {
                         OD_H5 = od.H5FoldrId == null ? 0 : od.H5FoldrId,
                         HF_H5 = hf.H5FoldrId == null ? 0 : hf.H5FoldrId
                     });


            return LinqHelper.CopyToDataTable(s, null, null);

        }

        public int CompleteGOMIBillTask(int orderID)
        {
            int cnt = 0;
            var at = (from a in _context.ActTask
                      where a.OrdrId == orderID && a.TaskId == 110 && a.StusId == 0
                      select a);

            foreach (var a in at)
            {
                a.StusId = 2;
                a.ModfdDt = DateTime.Now;
                cnt++;
            }
            _context.SaveChanges();
            return cnt;
        }

        public bool IsOrderCompleted(int orderID)
        {
            bool bRet = false;
            var _i = (from c in _context.Ordr
                      where c.OrdrId == orderID
                      select new { c.OrdrStusId }).SingleOrDefault();

            if (_i != null)
            {
                if (_i.OrdrStusId != 0 && _i.OrdrStusId != 1)
                    bRet = true;
            }

            return bRet;
        }

        public List<CPEOrdrUpdInfo> GetCPEUpdtInfo(int orderID)
        {
            List<CPEOrdrUpdInfo> lcou = new List<CPEOrdrUpdInfo>();

            var x = (from cpe in _context.FsaOrdrCpeLineItem
                     join fsa in _context.FsaOrdr on cpe.OrdrId equals fsa.OrdrId
                     join fox in _context.FsaOrdrGomXnci on cpe.FsaCpeLineItemId equals fox.FsaCpeLineItemId
                        into joincpefox
                     from fox in joincpefox.DefaultIfEmpty()
                     where fsa.OrdrId == orderID
                     select new
                     {
                         fsa.Ftn,
                         fsa.OrdrId,
                         cpe.FsaCpeLineItemId,
                         cpe.EqptTypeId,
                         cpe.EqptId,
                         cpe.MdsDes,
                         cpe.MfrNme,
                         ORDR_SHIP_DT = fox.PrchOrdrBackOrdrShipDt,
                         CUST_DLVRY_DT = fox.CustDlvryDt,
                         cpe.ManfPartCd,
                         fox.CpeVndrNme,
                         cpe.PrchOrdrNbr,
                         fox.ShpmtTrkNbr,
                         cpe.DeviceId
                     }
                     ).Distinct();

            foreach (var item in x)
            {
                CPEOrdrUpdInfo co = new CPEOrdrUpdInfo();
                co.CPEOrdrID = item.OrdrId;
                co.CPELineItemID = item.FsaCpeLineItemId;
                co.FTN = item.Ftn;
                co.EquipID = item.EqptId;
                co.EquipTypeCd = item.EqptTypeId;
                co.EquipManf = item.MfrNme;
                co.EquipDesc = item.MdsDes;
                co.CustDelvryDate = (item.CUST_DLVRY_DT == null) ? string.Empty : ((DateTime)item.CUST_DLVRY_DT).ToString("MM/dd/yyyy");
                co.OrderShipDate = (item.ORDR_SHIP_DT == null) ? string.Empty : ((DateTime)item.ORDR_SHIP_DT).ToString("MM/dd/yyyy");
                co.ManfPartCd = item.ManfPartCd;
                co.PONbr = item.PrchOrdrNbr;
                co.ShipTrkNbr = item.ShpmtTrkNbr;
                co.CarrierNme = item.CpeVndrNme;
                co.DeviceId = item.DeviceId;
                lcou.Add(co);
            }
            return lcou;

        }
        public void SaveCPEData(int _OrderID, List<CPEOrdrUpdInfo> _CPEData, int _userID, int workGrp)
        {
            var xl = (from fox in _context.FsaOrdrGomXnci
                      where fox.OrdrId == _OrderID
                        && (fox.FsaCpeLineItemId.HasValue)
                      select fox);
            if ((xl != null) && (_CPEData.Count > 0) && (xl.Count() == _CPEData.Count))
            {
                foreach (CPEOrdrUpdInfo cou in _CPEData)
                {
                    var x = xl.Where(a => a.FsaCpeLineItemId == cou.CPELineItemID).FirstOrDefault();
                    if ((workGrp != (int)WorkGroup.GOM) && (x != null))
                        x.CustDlvryDt = (cou.CustDelvryDate != null) ? DateTime.Parse(cou.CustDelvryDate) : (DateTime?)null;
                    if ((workGrp == (int)WorkGroup.GOM) && (x != null))
                    {
                        x.ShpmtTrkNbr = (cou.ShipTrkNbr != null) ? cou.ShipTrkNbr : string.Empty;
                        x.CpeVndrNme = (cou.CarrierNme != null) ? cou.CarrierNme : string.Empty;
                        x.PrchOrdrBackOrdrShipDt = (cou.OrderShipDate != "") ? DateTime.Parse(cou.OrderShipDate) : (DateTime?)null;
                    }
                }
                _context.SaveChanges();
            }
            else if ((xl != null) && (_CPEData.Count > 0) && (xl.Count() != _CPEData.Count))
            {
                List<FsaOrdrGomXnci> lfox = new List<FsaOrdrGomXnci>();
                foreach (CPEOrdrUpdInfo cou in _CPEData)
                {
                    var x = xl.Where(a => a.FsaCpeLineItemId == cou.CPELineItemID).FirstOrDefault();
                    FsaOrdrGomXnci fox = new FsaOrdrGomXnci
                    {
                        OrdrId = _OrderID,
                        FsaCpeLineItemId = cou.CPELineItemID,
                        UsrPrfId = Convert.ToInt16(workGrp),
                        GrpId = Convert.ToInt16(WorkGroup.OLDGOM),
                        CreatByUserId = _userID,
                        CreatDt = DateTime.Now
                    };

                    if ((workGrp != (int)WorkGroup.GOM) && (x != null))
                        x.CustDlvryDt = (cou.CustDelvryDate != null) ? DateTime.Parse(cou.CustDelvryDate) : (DateTime?)null;
                    else if ((workGrp != (int)WorkGroup.GOM) && (x == null))
                    {
                        fox.CustDlvryDt = (cou.CustDelvryDate != null) ? DateTime.Parse(cou.CustDelvryDate) : (DateTime?)null;
                        lfox.Add(fox);
                    }
                    else if ((workGrp == (int)WorkGroup.GOM) && (x != null))
                    {
                        x.ShpmtTrkNbr = (cou.ShipTrkNbr != null) ? cou.ShipTrkNbr : string.Empty;
                        x.CpeVndrNme = (cou.CarrierNme != null) ? cou.CarrierNme : string.Empty;
                        x.PrchOrdrBackOrdrShipDt = (cou.OrderShipDate != "") ? DateTime.Parse(cou.OrderShipDate) : (DateTime?)null;
                    }
                    else if ((workGrp == (int)WorkGroup.GOM) && (x == null))
                    {
                        if ((cou.PONbr != null) || (cou.ShipTrkNbr != null) || (cou.CarrierNme != null) || (cou.OrderShipDate != null))
                        {
                            fox.ShpmtTrkNbr = (cou.ShipTrkNbr != null) ? cou.ShipTrkNbr : string.Empty;
                            fox.CpeVndrNme = (cou.CarrierNme != null) ? cou.CarrierNme : string.Empty;
                            fox.PrchOrdrBackOrdrShipDt = (cou.OrderShipDate != "") ? DateTime.Parse(cou.OrderShipDate) : (DateTime?)null;
                            lfox.Add(fox);
                        }
                    }
                }
                if (lfox.Count > 0)
                    _context.FsaOrdrGomXnci.AddRange(lfox);
                _context.SaveChanges();
            }
            else if (xl == null)
            {
                List<FsaOrdrGomXnci> lfox = new List<FsaOrdrGomXnci>();
                foreach (CPEOrdrUpdInfo cou in _CPEData)
                {
                    FsaOrdrGomXnci fox = new FsaOrdrGomXnci();
                    fox.OrdrId = _OrderID;
                    fox.FsaCpeLineItemId = cou.CPELineItemID;
                    fox.UsrPrfId = Convert.ToInt16(workGrp);
                    fox.GrpId = (int)WorkGroup.OLDGOM;
                    fox.CreatByUserId = _userID;
                    fox.CreatDt = DateTime.Now;
                    if ((cou.CustDelvryDate != null) || (cou.PONbr != null) || (cou.ShipTrkNbr != null) || (cou.CarrierNme != null) || (cou.OrderShipDate != ""))
                    {
                        if (workGrp != (int)WorkGroup.GOM)
                            fox.CustDlvryDt = (cou.CustDelvryDate != null) ? DateTime.Parse(cou.CustDelvryDate) : (DateTime?)null;
                        if (workGrp == (int)WorkGroup.GOM)
                        {
                            fox.ShpmtTrkNbr = (cou.ShipTrkNbr != null) ? cou.ShipTrkNbr : string.Empty;
                            fox.CpeVndrNme = (cou.CarrierNme != null) ? cou.CarrierNme : string.Empty;
                            fox.PrchOrdrBackOrdrShipDt = (cou.OrderShipDate != "") ? DateTime.Parse(cou.OrderShipDate) : (DateTime?)null;
                        }
                        lfox.Add(fox);
                    }
                }
                _context.FsaOrdrGomXnci.AddRange(lfox);
                _context.SaveChanges();
            }
        }

        public void InsertUpdateGOMInfo(int orderID, int userID, int cpeStusId, string cpeEqptTypeTxt, DateTime? cmplDt, decimal? reqAmt)
        {
            bool checkForGOMCmplt = (from od in _context.Ordr
                                     join fod in _context.FsaOrdr on od.OrdrId equals fod.OrdrId
                                     where od.DmstcCd == true
                                       && fod.ProdTypeCd == "CP"
                                       && od.OrdrId == orderID
                                       //ignoring StateMachine here
                                       && (from lp in _context.LkPprt
                                           join sm in _context.Sm on lp.SmId equals sm.SmId
                                           join wpt in _context.WgPtrnSm on sm.DesrdWgPtrnId equals wpt.WgPtrnId
                                           join wpr in _context.WgProfSm on wpt.DesrdWgProfId equals wpr.WgProfId
                                           where od.PprtId == lp.PprtId
                                                             && wpr.PreReqstTaskId == 210
                                           select lp).Any()
                                       && (!(from at in _context.ActTask
                                             where od.OrdrId == at.OrdrId
                                               && at.TaskId == 210
                                             select at).Any())
                                     select od).Any();
            if ((checkForGOMCmplt) && (cpeStusId == 2))
            {
                ActTask ata = new ActTask
                {
                    OrdrId = orderID,
                    TaskId = 210,
                    StusId = 0,
                    WgProfId = 203,
                    CreatDt = DateTime.Now
                };
                _context.ActTask.Add(ata);
            }

            var xl = (from fox in _context.FsaOrdrGomXnci
                      where fox.OrdrId == orderID
                      select fox).ToList();

            if ((xl != null) && (xl.Count > 0))
            {
                foreach (var x in xl)
                {
                    x.CpeStusId = Convert.ToInt16(cpeStusId);
                    x.CpeEqptTypeTxt = cpeEqptTypeTxt;
                    x.CpeCmpltDt = cmplDt;
                    x.RqstnAmt = reqAmt;
                    x.ModfdDt = DateTime.Now;
                    x.ModfdByUserId = userID;
                }
            }
            else
            {
                FsaOrdrGomXnci fox = new FsaOrdrGomXnci
                {
                    OrdrId = orderID,
                    CpeStusId = Convert.ToInt16(cpeStusId),
                    CpeEqptTypeTxt = cpeEqptTypeTxt,
                    CpeCmpltDt = cmplDt,
                    RqstnAmt = reqAmt,
                    UsrPrfId = (short)WorkGroup.GOM,
                    GrpId = (short)WorkGroup.OLDGOM,
                    CreatByUserId = userID,
                    CreatDt = DateTime.Now
                };
                _context.FsaOrdrGomXnci.Add(fox);
            }
            _context.SaveChanges();
        }
        public void InsertUpdateAMNCIInfo(int orderID, int userID, int cpeStusId, string cpeEqptTypeTxt, DateTime? cmplDt, decimal? reqAmt)
        {
            bool checkForGOMCmplt = (from od in _context.Ordr
                                     join fod in _context.FsaOrdr on od.OrdrId equals fod.OrdrId
                                     where od.DmstcCd == true
                                       && fod.ProdTypeCd == "CP"
                                       && od.OrdrId == orderID
                                       //ignoring StateMachine here
                                       && (from lp in _context.LkPprt
                                           join sm in _context.Sm on lp.SmId equals sm.SmId
                                           join wpt in _context.WgPtrnSm on sm.DesrdWgPtrnId equals wpt.WgPtrnId
                                           join wpr in _context.WgProfSm on wpt.DesrdWgProfId equals wpr.WgProfId
                                           where od.PprtId == lp.PprtId
                                                             && wpr.PreReqstTaskId == 210
                                           select lp).Any()
                                       && (!(from at in _context.ActTask
                                             where od.OrdrId == at.OrdrId
                                               && at.TaskId == 210
                                             select at).Any())
                                     select od).Any();
            if ((checkForGOMCmplt) && (cpeStusId == 2))
            {
                ActTask ata = new ActTask
                {
                    OrdrId = orderID,
                    TaskId = 210,
                    StusId = 0,
                    WgProfId = 203,
                    CreatDt = DateTime.Now
                };
                _context.ActTask.Add(ata);
            }

            var xl = (from fox in _context.FsaOrdrGomXnci
                      where fox.OrdrId == orderID
                      select fox).ToList();

            if ((xl != null) && (xl.Count > 0))
            {
                foreach (var x in xl)
                {
                    x.CpeStusId = Convert.ToInt16(cpeStusId);
                    x.CpeEqptTypeTxt = cpeEqptTypeTxt;
                    x.CpeCmpltDt = cmplDt;
                    x.RqstnAmt = reqAmt;
                    x.ModfdDt = DateTime.Now;
                    x.ModfdByUserId = userID;
                }
            }
            else
            {
                FsaOrdrGomXnci fox = new FsaOrdrGomXnci
                {
                    OrdrId = orderID,
                    CpeStusId = Convert.ToInt16(cpeStusId),
                    CpeEqptTypeTxt = cpeEqptTypeTxt,
                    CpeCmpltDt = cmplDt,
                    RqstnAmt = reqAmt,
                    UsrPrfId = (short)WorkGroup.xNCIAmerica,
                    GrpId = (short)WorkGroup.xNCIAmerica,
                    CreatByUserId = userID,
                    CreatDt = DateTime.Now
                };
                _context.FsaOrdrGomXnci.Add(fox);
            }
            _context.SaveChanges();
        }
        public async Task<DataTable> GetGOMSpecificData(int orderId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getGOMSpecificInfo {orderId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public bool IsACRRTS(int orderId)
        {
            var at = from a in _context.ActTask
                     where a.OrdrId == orderId && a.TaskId == 500 && a.StusId == 0 && a.WgProfId == 0
                     select a.ActTaskId;

            if (at.Count() > 0)
                return true;
            return false;
        }

        public int CompleteACRRTSTask(int orderId)
        {
            int cnt = 0;
            var at = (from a in _context.ActTask
                      where a.OrdrId == orderId && a.TaskId == 500 && a.StusId == 0
                      select a);

            foreach (var a in at)
            {
                a.StusId = 2;
                a.ModfdDt = DateTime.Now;
                cnt++;
            }

            _context.SaveChanges();
            return cnt;
        }

        public int InsertOrderNotes(int orderId, int noteTypeId, int userId, string notes)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@NTE_TYPE_ID", SqlDbType.Int),
                    new SqlParameter("@NTE_TXT", SqlDbType.VarChar),
                    new SqlParameter("@CREAT_BY_USER_ID", SqlDbType.Int)
                };
            pc[0].Value = orderId;
            pc[1].Value = noteTypeId;
            pc[2].Value = notes;
            pc[3].Value = userId;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertOrderNotes @ORDR_ID, @NTE_TYPE_ID, @NTE_TXT, @CREAT_BY_USER_ID", pc.ToArray());

            return toReturn;
        }

        public void LoadNCITask(int orderId, short taskId)
        {
            var atat = (from a in _context.ActTask
                        where a.OrdrId == orderId && a.TaskId == taskId && a.StusId != 0
                        select a).SingleOrDefault();

            if (atat != null)
            {
                ActTask a = (ActTask)atat;
                a.StusId = 0;
                a.ModfdDt = DateTime.Now;
            }
            else
            {
                ActTask newTask = new ActTask();
                newTask.StusId = 0;
                newTask.OrdrId = orderId;
                newTask.TaskId = taskId;
                newTask.WgProfId = 0;
                newTask.CreatDt = DateTime.Now;

                _context.ActTask.Add(newTask);
            }

            _context.SaveChanges();
            return;

        }

        public DataTable GetCSCSpecificData(int orderId)
        {
            var s = (from a in _context.FsaOrdrCsc
                     where (a.OrdrId == orderId)
                     select new { a.OrdrId, a.SerialNbr, a.MtrlRqstnTxt, a.PrchOrdrTxt, a.IntlMntcSchrgTxt });

            return LinqHelper.CopyToDataTable(s, null, null);
        }

        public DataTable GetMDSEventData(int orderId)
        {
            var s = (from a in _context.EventCpeDev
                     join o in _context.FsaOrdr on a.CpeOrdrNbr equals o.Ftn
                     join r in _context.MdsEventOdieDev on a.EventId equals r.EventId
                     join b in _context.MdsEvent on a.EventId equals b.EventId into bjoin
                     from b in bjoin.DefaultIfEmpty()
                     join d in _context.UcaaSEvent on a.EventId equals d.EventId into djoin
                     from d in djoin.DefaultIfEmpty()
                     join c in _context.LkEventStus on b.EventStusId equals c.EventStusId into cjoin
                     from c in cjoin.DefaultIfEmpty()
                     join e in _context.LkEventStus on d.EventStusId equals e.EventStusId into ejoin
                     from e in ejoin.DefaultIfEmpty()
                     where (o.OrdrId == orderId)
                     orderby a.EventCpeDevId descending
                     select new
                     {
                         EVENT_ID = a.EventId,
                         SRVC_ASSRN_SITE_SUPP_ID = r.SrvcAssrnSiteSuppId,
                         STRT_TMST = b.StrtTmst == null ? (d.StrtTmst == null ? string.Empty : d.StrtTmst.ToString()) : b.StrtTmst.ToString(),
                         EVENT_STUS_DES = c.EventStusDes == null ? (e.EventStusDes == null ? string.Empty : e.EventStusDes) : c.EventStusDes
                     }).Union(from a in _context.EventDevSrvcMgmt
                              join o in _context.FsaOrdr on a.MnsOrdrNbr equals o.Ftn
                              join r in _context.MdsEventOdieDev on a.EventId equals r.EventId
                              join b in _context.MdsEvent on a.EventId equals b.EventId into bjoin
                              from b in bjoin.DefaultIfEmpty()
                              join d in _context.UcaaSEvent on a.EventId equals d.EventId into djoin
                              from d in djoin.DefaultIfEmpty()
                              join c in _context.LkEventStus on b.EventStusId equals c.EventStusId into cjoin
                              from c in cjoin.DefaultIfEmpty()
                              join e in _context.LkEventStus on d.EventStusId equals e.EventStusId into ejoin
                              from e in ejoin.DefaultIfEmpty()
                              where (o.OrdrId == orderId)
                              orderby a.EventDevSrvcMgmtId descending
                              select new
                              {
                                  EVENT_ID = a.EventId,
                                  SRVC_ASSRN_SITE_SUPP_ID = r.SrvcAssrnSiteSuppId,
                                  STRT_TMST = b.StrtTmst == null ? (d.StrtTmst == null ? string.Empty : d.StrtTmst.ToString()) : b.StrtTmst.ToString(),
                                  EVENT_STUS_DES = c.EventStusDes == null ? (e.EventStusDes == null ? string.Empty : e.EventStusDes) : c.EventStusDes
                              });


            return LinqHelper.CopyToDataTable(s, null, null);
        }

        public int InsertUpdateCSCInfo(int orderId, string serialNo, string mr, string po, string ims, int userId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@OrderID", SqlDbType.Int),
                    new SqlParameter("@SERIAL_NBR", SqlDbType.VarChar),
                    new SqlParameter("@MTRL_RQSTN_TXT", SqlDbType.VarChar),
                    new SqlParameter("@PRCH_ORDR_TXT", SqlDbType.VarChar),
                    new SqlParameter("@INTL_MNTC_SCHRG_TXT", SqlDbType.VarChar),
                    new SqlParameter("@USER_ID", SqlDbType.Int)
            };
            pc[0].Value = orderId;
            pc[1].Value = serialNo;
            pc[2].Value = mr;
            pc[3].Value = po;
            pc[4].Value = ims;
            pc[5].Value = userId;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.InsertUpdateCSCInfo @OrderID, @SERIAL_NBR, @MTRL_RQSTN_TXT, @PRCH_ORDR_TXT, @INTL_MNTC_SCHRG_TXT, @USER_ID", pc.ToArray());
            return toReturn;
        }

        public bool IsOrderExistInSalesSupportWG(int orderID)
        {
            var record = (from act in _context.ActTask
                     where act.OrdrId == orderID
                        && act .TaskId== 500
                        && act.StusId == 0
                     select act.OrdrId).ToArray();

            return record.Count() > 0;
        }
    }
}