﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class StdiReasonRepository : IStdiReasonRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public StdiReasonRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkStdiReas Create(LkStdiReas entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkStdiReas> Find(Expression<Func<LkStdiReas, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkStdiReas, out List<LkStdiReas> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkStdiReas> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkStdiReas, out List<LkStdiReas> list))
            {
                list = _context.LkStdiReas
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkStdiReas, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkStdiReas GetById(int id)
        {
            return _context.LkStdiReas
                .SingleOrDefault(a => a.StdiReasId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkStdiReas);
            return _context.SaveChanges();
        }

        public void Update(int id, LkStdiReas entity)
        {
            throw new NotImplementedException();
        }
    }
}
