﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class TelcoRepository : ITelcoRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public TelcoRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkTelco Create(LkTelco entity)
        {
            int maxId = _context.LkTelco.Max(i => i.TelcoId);
            entity.TelcoId = (byte)++maxId;
            _context.LkTelco.Add(entity);

            SaveAll();

            return GetById(entity.TelcoId);
        }

        public void Delete(int id)
        {
            LkTelco tel = GetById(id);
            _context.LkTelco.Remove(tel);

            SaveAll();
        }

        public IQueryable<LkTelco> Find(Expression<Func<LkTelco, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkTelco, out List<LkTelco> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkTelco> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkTelco, out List<LkTelco> list))
            {
                list = _context.LkTelco
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderBy(i => i.TelcoId)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkTelco, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkTelco GetById(int id)
        {
            return _context.LkTelco
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.TelcoId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkTelco);
            return _context.SaveChanges();
        }

        public void Update(int id, LkTelco entity)
        {
            LkTelco telco = GetById(id);
            telco.TelcoNme = entity.TelcoNme;
            telco.TelcoCntctPhnNbr = entity.TelcoCntctPhnNbr;
            telco.RecStusId = entity.RecStusId;
            telco.ModfdByUserId = entity.ModfdByUserId;
            telco.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}