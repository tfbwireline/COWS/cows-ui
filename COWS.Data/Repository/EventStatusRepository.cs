﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventStatusRepository : IEventStatusRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public EventStatusRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkEventStus> Find(Expression<Func<LkEventStus, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkEventStus, out List<LkEventStus> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkEventStus> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkEventStus, out List<LkEventStus> list))
            {
                list = _context.LkEventStus
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkEventStus, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkEventStus GetById(int id)
        {
            return _context.LkEventStus
                .SingleOrDefault(a => a.EventStusId == id);
        }

        public LkEventStus Create(LkEventStus entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkEventStus entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkEventStus);
            return _context.SaveChanges();
        }
    }
}