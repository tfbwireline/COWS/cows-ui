﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MdsEventOdieDevRepository : IMdsEventOdieDevRepository
    {
        private readonly COWSAdminDBContext _context;

        public MdsEventOdieDevRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<MdsEventOdieDev> Find(Expression<Func<MdsEventOdieDev, bool>> predicate)
        {
            return _context.MdsEventOdieDev
                .Where(predicate);
        }

        public IEnumerable<MdsEventOdieDev> GetAll()
        {
            return _context.MdsEventOdieDev.ToList();
        }

        public MdsEventOdieDev GetById(int id)
        {
            return Find(a => a.MdsEventOdieDevId == id).SingleOrDefault();
        }

        public MdsEventOdieDev Create(MdsEventOdieDev entity)
        {
            _context.MdsEventOdieDev.Add(entity);
            SaveAll();

            return GetById(entity.MdsEventOdieDevId);
        }

        public void Create(IEnumerable<MdsEventOdieDev> entity)
        {
            _context.MdsEventOdieDev
                .AddRange(entity.Select(a =>
                {
                    a.MdsEventOdieDevId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Update(int id, MdsEventOdieDev entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            var toDelete = GetById(id);
            _context.MdsEventOdieDev.Remove(toDelete);
        }

        public void Delete(Expression<Func<MdsEventOdieDev, bool>> predicate)
        {
            var toDelete = Find(predicate);
            _context.MdsEventOdieDev.RemoveRange(toDelete);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}