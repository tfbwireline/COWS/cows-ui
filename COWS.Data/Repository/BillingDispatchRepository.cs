﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace COWS.Data.Repository
{
    public class BillingDispatchRepository : IBillingDispatchRepository
    {
        private readonly COWSAdminDBContext _context;
        public BillingDispatchRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<BillingDispatchData> Get()
        {
            List<SqlParameter> p = new List<SqlParameter>
            {
                new SqlParameter("@Opt", 2) //option 2 is hardcoded for web interface
            };

            var billingDispatch = _context.Query<BillingDispatchData>()
                .AsNoTracking()
                .FromSql("[dbo].[getBillDsptchData] @Opt", p.ToArray()).ToList();

            return billingDispatch;
        }

        public IEnumerable<BillingDispatchData> GetHistory()
        {
            List<SqlParameter> p = new List<SqlParameter>
            {
                new SqlParameter("@Opt", 3) //option 3 is hardcoded for complete history
            };

            var billingDispatch = _context.Query<BillingDispatchData>()
                .AsNoTracking()
                .FromSql("[dbo].[getBillDsptchData] @Opt", p.ToArray())
                .OrderByDescending(b => b.ModifiedDate)
                .Take(500) //Limiting to 500 for performance
                .ToList();

            return billingDispatch;
        }

        public BillingDispatchData GetById(int id)
        {
            return Get()
                .Where(res => res.Id == id)
                .FirstOrDefault();
        }

        public int Create(BillingDispatchData data)
        {
            //create 

            BillDsptch billDsptch = new BillDsptch();

            billDsptch.AcptRjctCd = data.AcceptRejectCode;
            
            billDsptch.ClearLoc = data.TicketLocation;
            billDsptch.ClsDt = (DateTime)data.CloseDate;
            billDsptch.CmntTxt = data.Comment;
            //billDsptch.CreatByUser = data.CreatedByUserName;
            billDsptch.CreatByUserId = data.CreatedByUserId ?? 0;
            billDsptch.CreatDt = data.CreatedDate ?? DateTime.Now;
            billDsptch.CustId = data.CustomerId;
            billDsptch.DispCaName = data.ActionTaken;
            billDsptch.DispCatName = data.TicketCategoryName;
            billDsptch.DispCd = data.DispositionCode;
            billDsptch.DispSubcatName = data.TicketSubCategoryName;
            billDsptch.ModfdByUserId = data.ModifiedByUserId;
            billDsptch.ModfdDt = data.ModifiedDate ?? DateTime.Now;
            billDsptch.StusId = (short)data.StatusId;
            billDsptch.TickNbr = data.TicketNumber;

            _context.BillDsptch.Add(billDsptch);

            return SaveAll();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}
