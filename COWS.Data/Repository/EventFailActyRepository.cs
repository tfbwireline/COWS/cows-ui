﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventFailActyRepository : IEventFailActyRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public EventFailActyRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<EventFailActy> Find(Expression<Func<EventFailActy, bool>> predicate)
        {
            return _context.EventFailActy
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<EventFailActy> GetAll()
        {
            return _context.EventFailActy
                            .AsNoTracking()
                            .ToList();
        }

        public EventFailActy GetById(int id)
        {
            return _context.EventFailActy
                .SingleOrDefault(a => a.FailActyId == id);
        }

        public IEnumerable<EventFailActy> GetByHistId(int id)
        {
            List<EventFailActy> liESA = new List<EventFailActy>();
            var esl = (from e in _context.EventFailActy
                       where (e.EventHistId == id)
                       select e);
            return esl;
        }

        public IEnumerable<EventFailActy> GetByEventId(int id)
        {
            List<EventFailActy> liESA = new List<EventFailActy>();
            var esl = (from e in _context.EventHist
                       join efa in _context.EventFailActy on e.EventHistId equals efa.EventHistId
                       where (e.EventId == id) && (efa.RecStusId == (byte)1)
                       select efa);

            foreach (var es in esl)
            {
                EventFailActy esa = new EventFailActy();
                esa.EventHistId = es.EventHistId;
                esa.FailActyId = es.FailActyId;
                esa.RecStusId = (byte)1;
                esa.CreatDt = es.CreatDt;
                liESA.Add(esa);
            }
            return liESA;
        }

        public EventFailActy Create(EventFailActy entity)
        {
            throw new NotImplementedException();
        }

        public void CreateActy(EventFailActy entity)
        {
            _context.EventFailActy.Add(entity);
            SaveAll();
        }

        public void Update(int id, EventFailActy entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            var st = GetByHistId(id);
            if (st != null)
            {
                foreach (var ac in st)
                {
                    _context.EventFailActy.Remove(ac);
                }
            }
            SaveAll();
        }

        public void DeleteByEventId(int id)
        {
            var st = GetByEventId(id);
            foreach (var es in st)
            {
                Delete(es.EventHistId);
            }
            SaveAll();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}