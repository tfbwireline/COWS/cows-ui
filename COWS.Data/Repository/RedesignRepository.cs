﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class RedesignRepository : IRedesignRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _commonRepo;
        private readonly IConfiguration _config;
        private readonly ILogger<RedesignRepository> _logger;

        public RedesignRepository(COWSAdminDBContext context, ICommonRepository commonRepo,
            IConfiguration config, ILogger<RedesignRepository> logger)
        {
            _context = context;
            _commonRepo = commonRepo;
            _config = config;
            _logger = logger;
        }

        public Redsgn Create(Redsgn entity)
        {
            // Set Redesign Number
            string rDesgnNbr = string.Empty;
            var rds = from rd in _context.Redsgn
                      where rd.RedsgnNbr.Contains("R00")
                      select rd.RedsgnNbr;
            if (rds != null && rds.Count() > 0)
            {
                rDesgnNbr = rds.Max();
                int rID = Convert.ToInt32(rDesgnNbr.Substring(rDesgnNbr.IndexOf("R") + 1)) + 1;
                rDesgnNbr = rID.ToString();
                switch (rID.ToString().Length)
                {
                    case (5):
                        rDesgnNbr = "R000" + rDesgnNbr;
                        break;
                    case (6):
                        rDesgnNbr = "R00" + rDesgnNbr;
                        break;
                    case (7):
                        rDesgnNbr = "R0" + rDesgnNbr;
                        break;
                    case (8):
                        rDesgnNbr = "R" + rDesgnNbr;
                        break;
                    default:
                        break;
                }
            }
            else
                rDesgnNbr = "R00050000";

            entity.RedsgnNbr = rDesgnNbr;

            _context.Redsgn.Add(entity);
            SaveAll();

            if (entity.CsgLvlId > 0)
            {
                CustScrdData sdata = new CustScrdData();
                sdata.ScrdObjId = entity.RedsgnId;
                sdata.ScrdObjTypeId = (byte)ScrdObjType.REDSGN;
                sdata.CustNme = _commonRepo.GetEncryptValue(entity.CustNme);
                sdata.CustEmailAdr = _commonRepo.GetEncryptValue(entity.CustEmailAdr);
                _context.CustScrdData.Add(sdata);

                entity.CustNme = string.Empty;
                entity.CustEmailAdr = string.Empty;
                _context.Redsgn.Update(entity);

                SaveAll();
            }

            return GetById(entity.RedsgnId);
        }

        public void Delete(int id)
        {
            var redLock = _context.RedsgnRecLock.Where(i => i.RedsgnId == id);
            _context.RedsgnRecLock.RemoveRange(redLock);

            var notes = _context.RedsgnNotes.Where(i => i.RedsgnId == id);
            _context.RedsgnNotes.RemoveRange(notes);

            var emails = _context.RedsgnEmailNtfctn.Where(i => i.RedsgnId == id);
            _context.RedsgnEmailNtfctn.RemoveRange(emails);

            var docs = _context.RedsgnDoc.Where(i => i.RedsgnId == id);
            _context.RedsgnDoc.RemoveRange(docs);

            var devices = _context.RedsgnDevicesInfo.Where(i => i.RedsgnId == id);
            if (devices != null && devices.Count() > 0)
            {
                foreach (RedsgnDevicesInfo item in devices)
                {
                    var devHist = _context.RedsgnDevEventHist.Where(i => i.RedsgnDevId == item.RedsgnDevId);
                    _context.RedsgnDevEventHist.RemoveRange(devHist);
                }

                _context.RedsgnDevicesInfo.RemoveRange(devices);
            }

            var redesign = GetById(id);
            _context.Redsgn.Remove(redesign);

            _context.SaveChanges();
        }

        public IQueryable<Redsgn> Find(Expression<Func<Redsgn, bool>> predicate, string adid = null)
        {
            var redesign = _context.Redsgn
                            .Include(i => i.RedsgnDevicesInfo).ThenInclude(rdsgn => rdsgn.RedsgnDevEventHist)
                            .Include(i => i.RedsgnDoc)
                            .Include(i => i.RedsgnEmailNtfctn)
                            .Include(i => i.RedsgnNotes).ThenInclude(rdsgn => rdsgn.RedsgnNteType)
                            .Include(i => i.RedsgnNotes).ThenInclude(rdsgn => rdsgn.CretdByCdNavigation)
                            .Include(i => i.RedsgnRecLock)
                            .Include(i => i.RedsgnDevStus)
                            .Include(i => i.Stus)
                            .Where(predicate);

            if (redesign.Count() == 1)
            {
                var data = redesign.FirstOrDefault();
                if (data.CsgLvlId > 0 && adid != null)
                {
                    // LogWebActivity
                    var UserCsg = _commonRepo.GetCSGLevelAdId(adid);
                    _commonRepo.LogWebActivity(((byte)WebActyType.RedesignDetails), data.RedsgnNbr.ToString(), adid, (byte)UserCsg, (byte)data.CsgLvlId);
                }
            }

            return redesign;


        }

        public IEnumerable<Redsgn> GetAll()
        {
            return _context.Redsgn
                            .Include(i => i.RedsgnDevicesInfo)
                            .Include(i => i.RedsgnDoc)
                            .Include(i => i.RedsgnEmailNtfctn)
                            .Include(i => i.RedsgnNotes)
                            .Include(i => i.RedsgnRecLock)
                            .Include(i => i.RedsgnDevStus)
                            .Include(i => i.Stus)
                            .AsNoTracking()
                            .ToList();
        }

        public Redsgn GetById(int id)
        {
            return _context.Redsgn
                            .Include(i => i.RedsgnDevicesInfo).ThenInclude(rdsgn => rdsgn.RedsgnDevEventHist)
                            .Include(i => i.RedsgnDoc)
                            .Include(i => i.RedsgnType)
                            .Include(i => i.RedsgnEmailNtfctn)
                            .Include(i => i.RedsgnNotes).ThenInclude(rdsgn => rdsgn.RedsgnNteType)
                            .Include(i => i.RedsgnNotes).ThenInclude(rdsgn => rdsgn.CretdByCdNavigation)
                            .Include(i => i.RedsgnRecLock)
                            .Include(i => i.RedsgnDevStus)
                            .Include(i => i.Stus)
                            .SingleOrDefault(a => a.RedsgnId == id);
        }

        public Redsgn GetById(int id, string adid, bool includeSecuredData = true)
        {
            var redesign = _context.Redsgn
                            .Include(i => i.RedsgnDevicesInfo).ThenInclude(rdsgn => rdsgn.RedsgnDevEventHist)
                            .Include(i => i.RedsgnDoc)
                            .Include(i => i.RedsgnType)
                            .Include(i => i.RedsgnEmailNtfctn)
                            .Include(i => i.RedsgnNotes).ThenInclude(rdsgn => rdsgn.RedsgnNteType)
                            .Include(i => i.RedsgnNotes).ThenInclude(rdsgn => rdsgn.CretdByCdNavigation)
                            .Include(i => i.RedsgnRecLock)
                            .Include(i => i.RedsgnDevStus)
                            .Include(i => i.Stus)
                            .SingleOrDefault(a => a.RedsgnId == id);

            if (redesign.CsgLvlId > 0 && includeSecuredData)
            {
                var securedRedesign = _context.CustScrdData
                    .Where(a => a.ScrdObjId == redesign.RedsgnId && a.ScrdObjTypeId == (byte)SecuredObjectType.REDSGN)
                    .FirstOrDefault();
                if (securedRedesign != null)
                {
                    redesign.CustNme = _commonRepo.GetDecryptValue(securedRedesign.CustNme);
                    redesign.CustEmailAdr = _commonRepo.GetDecryptValue(securedRedesign.CustEmailAdr);

                }
            }

            if (redesign != null)
            {
                var data = redesign;
                if (data.CsgLvlId > 0 && adid != null)
                {
                    // LogWebActivity
                    var UserCsg = _commonRepo.GetCSGLevelAdId(adid);
                    _commonRepo.LogWebActivity(((byte)WebActyType.RedesignDetails), data.RedsgnNbr.ToString(), adid, (byte)UserCsg, (byte)data.CsgLvlId);
                }
            }

            return redesign;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, Redsgn entity)
        {
            Redsgn redesign = GetById(id);
            CustScrdData csd = _context.CustScrdData
                .SingleOrDefault(i => i.ScrdObjId == id && i.ScrdObjTypeId == (byte)SecuredObjectType.REDSGN);

            if (entity.CsgLvlId > 0)
            {
                csd.CustNme = _commonRepo.GetEncryptValue(entity.CustNme);
                csd.CustEmailAdr = _commonRepo.GetEncryptValue(entity.CustEmailAdr);

                if (csd.ScrdObjId == id)
                {
                    _context.CustScrdData.Update(csd);
                }
                else
                {
                    csd.ScrdObjId = id;
                    csd.ScrdObjTypeId = (byte)SecuredObjectType.REDSGN;
                    csd.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(csd);
                }
            }
            else
            {
                redesign.CustNme = entity.CustNme;
                redesign.CustEmailAdr = entity.CustEmailAdr;
            }

            redesign.CsgLvlId = entity.CsgLvlId;
            redesign.RedsgnCatId = entity.RedsgnCatId;
            redesign.H1Cd = entity.H1Cd;
            redesign.OdieCustId = redesign.OdieCustId == entity.OdieCustId ? redesign.OdieCustId : entity.OdieCustId;
            redesign.NteAssigned = entity.NteAssigned;
            redesign.PmAssigned = entity.PmAssigned;
            redesign.NeAsnNme = entity.NeAsnNme;
            redesign.SdeAsnNme = entity.SdeAsnNme;
            redesign.RedsgnTypeId = entity.RedsgnTypeId;
            redesign.IsSdwan = entity.IsSdwan;
            redesign.CiscSmrtLicCd = entity.CiscSmrtLicCd;
            redesign.SmrtAccntDmn = entity.SmrtAccntDmn;
            redesign.VrtlAccnt = entity.VrtlAccnt;
            redesign.EntireNwCkdId = entity.EntireNwCkdId;
            redesign.DsgnDocNbrBpm = entity.DsgnDocNbrBpm;
            redesign.DsgnDocLocTxt = entity.DsgnDocLocTxt;
            redesign.SowsFoldrPathNme = entity.SowsFoldrPathNme;
            redesign.NteLvlEffortAmt = entity.NteLvlEffortAmt;
            redesign.NteOtLvlEffortAmt = entity.NteOtLvlEffortAmt;
            redesign.PmLvlEffortAmt = entity.PmLvlEffortAmt;
            redesign.PmOtLvlEffortAmt = entity.PmOtLvlEffortAmt;
            redesign.NeLvlEffortAmt = entity.NeLvlEffortAmt;
            redesign.NeOtLvlEffortAmt = entity.NeOtLvlEffortAmt;
            redesign.SpsLvlEffortAmt = entity.SpsLvlEffortAmt;
            redesign.SpsOtLvlEffortAmt = entity.SpsOtLvlEffortAmt;
            redesign.MssImplEstAmt = entity.MssImplEstAmt;
            redesign.CostAmt = entity.CostAmt;
            redesign.BillOvrrdnCd = entity.BillOvrrdnCd;
            redesign.BillOvrrdnAmt = entity.BillOvrrdnAmt;

            redesign.SlaDt = entity.SlaDt > DateTime.MinValue ? entity.SlaDt : null;
            redesign.SubmitDt = entity.SubmitDt > DateTime.MinValue ? entity.SubmitDt : null;
            redesign.ExprtnDt = entity.ExprtnDt > DateTime.MinValue ? entity.ExprtnDt : null;
            // To prevent double extension
            if ((entity.ExtXpirnFlgCd.HasValue && entity.ExtXpirnFlgCd.Value)
                && (redesign.ExtXpirnFlgCd == null 
                    || (redesign.ExtXpirnFlgCd.HasValue && !redesign.ExtXpirnFlgCd.Value)))
            {
                if (redesign.ExprtnDt.HasValue && redesign.ExprtnDt.Value > DateTime.MinValue)
                {
                    redesign.ExtXpirnFlgCd = entity.ExtXpirnFlgCd;
                    redesign.ExprtnDt = entity.ExprtnDt.Value.AddDays(90);
                    RedsgnNotes note = new RedsgnNotes();
                    note.RedsgnId = id;
                    note.RedsgnNteTypeId = (byte)RedesignNoteType.Notes;
                    note.CretdByCd = entity.ModfdByCd.Value;
                    note.CretdDt = DateTime.Now;
                    note.Notes = string.Format("Expiration Date extended from {0} to {1}.",
                        entity.ExprtnDt.Value.ToString("MMM/dd/yyyy"),
                        redesign.ExprtnDt.Value.ToString("MMM/dd/yyyy"));
                    _context.RedsgnNotes.Add(note);
                }
            }

            var oldStatusId = redesign.StusId;
            redesign.StusId = entity.StusId;
            redesign.ModfdByCd = entity.ModfdByCd.Value;
            redesign.ModfdDt = entity.ModfdDt;

            bool isCustomerByPass = GetRedesignCustomerBypassCD(redesign.H1Cd, redesign.CustNme, redesign.CsgLvlId);
            List<int> devicesToMach5 = new List<int>();

            // Devices
            if (entity.RedsgnDevicesInfo != null && entity.RedsgnDevicesInfo.Count() > 0)
            {
                // To prevent throwing exception for null values in the list
                entity.RedsgnDevicesInfo.Remove(null);
                List<string> selectedDevices = entity.RedsgnDevicesInfo
                    .Where(i => i.RedsgnId == id).Select(i => i.DevNme).ToList();

                // Update status of existing devices to inactive which have been previously databased
                var inactivateDevices = _context.RedsgnDevicesInfo
                    .Where(i => i.RedsgnId == id && i.RecStusId == Convert.ToBoolean(ERecStatus.Active)
                        && !selectedDevices.Contains(i.DevNme));

                foreach (RedsgnDevicesInfo device in inactivateDevices)
                {
                    device.RecStusId = Convert.ToBoolean(ERecStatus.InActive);
                    device.ModfdByCd = entity.ModfdByCd;
                    device.ModfdDt = entity.ModfdDt;
                    _context.RedsgnDevicesInfo.Update(device);
                }

                // Update fields for existing devices
                var updateDevices = _context.RedsgnDevicesInfo
                    .Where(i => i.RedsgnId == id && selectedDevices.Contains(i.DevNme));
                var isBillable = false;
                var rType = _context.LkRedsgnType.SingleOrDefault(i => i.RedsgnTypeId == redesign.RedsgnTypeId);
                if (rType.IsBlblCd)
                {
                    isBillable = !isCustomerByPass;
                }

                foreach (RedsgnDevicesInfo device in updateDevices)
                {
                    RedsgnDevicesInfo updDevice = entity.RedsgnDevicesInfo
                        .SingleOrDefault(i => i.DevNme == device.DevNme);
                    device.DspchRdyCd = updDevice.DspchRdyCd;
                    device.FastTrkCd = updDevice.FastTrkCd;
                    device.ScCd = updDevice.ScCd;
                    device.H6CustId = updDevice.H6CustId;
                    device.DevCmpltnCd = updDevice.DevCmpltnCd;
                    device.RecStusId = Convert.ToBoolean(ERecStatus.Active);
                    device.ModfdByCd = entity.ModfdByCd;
                    device.ModfdDt = DateTime.Now;
                    
                    if (updDevice.DevCmpltnCd.GetValueOrDefault())
                    {
                        device.NteChrgCd = isBillable;
                        device.DevCmpltnCd = updDevice.DevCmpltnCd;

                        if (isBillable)
                        {
                            if (!device.DevBillDt.HasValue)
                            {
                                devicesToMach5.Add(device.RedsgnDevId);
                            }

                            device.DevBillDt = DateTime.Now;
                        }
                    }
                    
                    _context.RedsgnDevicesInfo.Update(device);
                }

                // Insert new active devices
                var dbDevices = _context.RedsgnDevicesInfo
                    .Where(i => i.RedsgnId == id).Select(i => i.DevNme);
                if (dbDevices != null && dbDevices.Count() > 0)
                {
                    var newDevices = entity.RedsgnDevicesInfo
                        .Where(i => !dbDevices.Contains(i.DevNme)).ToList();
                    if (newDevices != null && newDevices.Count() > 0)
                    {
                        _context.RedsgnDevicesInfo.AddRange(newDevices);
                    }
                }
                else
                {
                    _context.RedsgnDevicesInfo.AddRange(entity.RedsgnDevicesInfo);
                }
            }

            // Notes
            if (entity.RedsgnNotes != null && entity.RedsgnNotes.Count() > 0)
            {
                _context.RedsgnNotes.AddRange(entity.RedsgnNotes);
            }

            // Emails
            if (entity.RedsgnEmailNtfctn != null && entity.RedsgnEmailNtfctn.Count() > 0)
            {
                // Delete old email not in current list
                List<int> ids = entity.RedsgnEmailNtfctn.Where(i => i.RedsgnId == id)
                    .Select(i => i.RedsgnEmailNtfctnId).ToList();
                List<RedsgnEmailNtfctn> notInCurrentList = _context.RedsgnEmailNtfctn
                    .Where(i => i.RedsgnId == id && !ids.Contains(i.RedsgnEmailNtfctnId)).ToList();
                _context.RedsgnEmailNtfctn.RemoveRange(notInCurrentList);

                foreach (RedsgnEmailNtfctn email in entity.RedsgnEmailNtfctn)
                {
                    // Updated condition by Sarah Sandoval [20210202]
                    // To prevent sequence returns more than 1 exception
                    var dbEmail = _context.RedsgnEmailNtfctn
                        .FirstOrDefault(i => i.RedsgnId == id
                            && i.UserIdntfctnEmail == email.UserIdntfctnEmail);
                    if (dbEmail != null)
                    {
                        // Commented out [20201229] - Sarah Sandoval
                        // Useless self-assignment for SonarQube
                        //dbEmail.UserIdntfctnEmail = dbEmail.UserIdntfctnEmail;
                        _context.RedsgnEmailNtfctn.Update(dbEmail);
                    }
                    else
                    {
                        _context.RedsgnEmailNtfctn.Add(email);
                    }
                }
            }
            else
            {
                // Delete record if no email details is sent from model
                var emails = _context.RedsgnEmailNtfctn.Where(i => i.RedsgnId == id).ToList();
                _context.RedsgnEmailNtfctn.RemoveRange(emails);
            }

            // Docs
            if (entity.RedsgnDoc != null && entity.RedsgnDoc.Count() > 0)
            {
                // Delete old doc not in current list
                List<int> ids = entity.RedsgnDoc.Where(i => i.RedsgnId == id)
                    .Select(i => i.RedsgnDocId).ToList();
                List<RedsgnDoc> notInCurrentList = _context.RedsgnDoc
                    .Where(i => i.RedsgnId == id 
                        && !ids.Contains(i.RedsgnDocId)).ToList();
                _context.RedsgnDoc.RemoveRange(notInCurrentList);

                foreach (RedsgnDoc doc in entity.RedsgnDoc)
                {
                    RedsgnDoc redsgnDoc = _context.RedsgnDoc.FirstOrDefault(i => i.RedsgnDocId == doc.RedsgnDocId && i.FileNme == doc.FileNme);

                    if (redsgnDoc != null)
                    {
                        // Update existing Redesign Document
                        redsgnDoc.FileCntnt = doc.FileCntnt;
                        redsgnDoc.FileNme = doc.FileNme;
                        redsgnDoc.FileSizeQty = doc.FileSizeQty;
                        redsgnDoc.ModfdByUserId = doc.ModfdByUserId;
                        redsgnDoc.ModfdDt = doc.ModfdDt;
                        _context.RedsgnDoc.Update(redsgnDoc);
                    }
                    else
                    {
                        // Add new Redesign Document
                        _context.RedsgnDoc.Add(doc);
                    }
                }
            }
            else
            {
                // Delete record if no doc details is sent from model
                var docs = _context.RedsgnDoc.Where(i => i.RedsgnId == id).ToList();
                _context.RedsgnDoc.RemoveRange(docs);
            }

            SaveAll();

            // Send devices to Mach5 for Billing
            if (devicesToMach5.Count() > 0 && !isCustomerByPass)
            {
                string dev = string.Empty;
                foreach (int i in devicesToMach5)
                    dev += i.ToString() + ",";

                InsertRedesignDeviceChargesIntoMach5(dev);
            }

            // Added by Sarah Sandoval [20210226] - Call SP to insert BPM_REDSGN_TXN record
            if (oldStatusId != entity.StusId)
            {
                InsertBPMRedesignTransaction(id);
            }
        }

        public bool InsertRedesignEmail(int redesignID, Int16 STUS_ID, bool expirationFlag, bool note_updated)
        {
            bool ret = false;
            // Updated by Sarah Sandoval [20210107] - Updated link for config changes in Email Sender Service
            //string redesignUrl = _config.GetSection("AppSettings:ViewRedesignURL").Value + redesignID;
            string redesignUrl = _config.GetSection("AppSettings:ViewRedesignURL").Value;
            try
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() { ParameterName = "@redsgn_id", SqlDbType = SqlDbType.Int, Value = redesignID },
                    new SqlParameter() { ParameterName = "@stus_id", SqlDbType = SqlDbType.SmallInt, Value = STUS_ID },
                    new SqlParameter() { ParameterName = "@expirationFlag", SqlDbType = SqlDbType.Bit, Value = expirationFlag },
                    new SqlParameter() { ParameterName = "@note_updated", SqlDbType = SqlDbType.Bit, Value = note_updated },
                    new SqlParameter() { ParameterName = "@redsgn_url", SqlDbType = SqlDbType.VarChar, Value = redesignUrl }
                };
                _context.Database.ExecuteSqlCommand("dbo.insertRedesignEmailData_V2 @redsgn_id, @stus_id, @expirationFlag, @note_updated, @redsgn_url", pc.ToArray());
                ret = true;
            }
            catch (Exception e)
            {
                _logger.LogInformation($"{ e.Message } ");
            }

            return ret;
        }

        public Dictionary<string, string> GetRedesignCostValues()
        {
            Dictionary<string, string> dc = new Dictionary<string, string>();

            var a = (from b in _context.LkSysCfg.AsNoTracking()
                     where (b.PrmtrNme == "NTE_CHARGE_RATE"
                       || b.PrmtrNme == "SDE_CHARGE_RATE"
                       || b.PrmtrNme == "NTE_VOICE_CHARGE_RATE"
                       || b.PrmtrNme == "PM_VOICE_CHARGE_RATE"
                       || b.PrmtrNme == "NE_VOICE_CHARGE_RATE"
                       || b.PrmtrNme == "NTE_VOICE_OT_CHARGE_RATE"
                       || b.PrmtrNme == "PM_VOICE_OT_CHARGE_RATE"
                       || b.PrmtrNme == "NE_VOICE_OT_CHARGE_RATE"
                       || b.PrmtrNme == "SPS_VOICE_CHARGE_RATE"
                       || b.PrmtrNme == "SPS_VOICE_OT_CHARGE_RATE")
                     select new
                     {
                         b.PrmtrNme
                         ,
                         b.PrmtrValuTxt
                     });
            if (a != null && a.Count() > 0)
            {
                string key, value;
                foreach (var _a in a)
                {
                    key = _a.PrmtrNme;
                    value = _a.PrmtrValuTxt;
                    dc.Add(key, value);
                }
            }

            return dc;
        }

        public bool GetRedesignCustomerBypassCD(string H1_CD, string Cust_Name, int csgLvlId)
        {
            bool bRet = false;

            var c = (from a in _context.RedsgnCustBypass
                     join csd in _context.CustScrdData on new { ScrdObjId = a.Id, ScrdObjTypeId = (byte)ScrdObjType.REDSGN_CUST_BYPASS } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                            into joincsd
                     from csd in joincsd.DefaultIfEmpty()
                     where a.H1Cd == H1_CD
                            && a.RecStusId == true
                     select new { a, csd }).AsNoTracking();

            if (c != null && c.Count() > 0)
            {
                string cust_nme;
                foreach (var _c in c)
                {
                    cust_nme = (_c.a.CsgLvlId == 0) ? _c.a.CustNme : (((_c.a.CsgLvlId > 0) && (csgLvlId != 0)
                                    && (csgLvlId <= _c.a.CsgLvlId)) ? _commonRepo.GetDecryptValue(_c.csd.CustNme) : string.Empty);
                    if (cust_nme == Cust_Name)
                        bRet = true;
                }
            }

            return bRet;
        }

        public decimal GetH1MRCValue(string h1, byte redsgnCatID)
        {
            decimal rspn = new decimal();

            var a = (from b in _context.RedsgnH1MrcValues
                    where b.H1Id == h1
                    && b.RedsgnCatId == redsgnCatID
                    && b.RecStusId == true
                    select new { MRC_Converter_Value = b.MrcRateVsNrc }).AsNoTracking();
            if (a != null && a.Count() > 0)
            {
                rspn = a.Single().MRC_Converter_Value;
            }
            else
            {
                rspn = 0;
            }

            return rspn;
        }

        public DataTable GetRedesignSearchResults(string rdsgnNos, string custName, string nte, 
            string devices, string pm, int csgLvlId, string adid = null)
        {
            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.getAdvSearchRedesignDetails", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@RedesignNumbers", rdsgnNos);
            command.Parameters.AddWithValue("@CustomerNames", custName);
            command.Parameters.AddWithValue("@NTE", nte);
            command.Parameters.AddWithValue("@ODIEDevices", devices);
            command.Parameters.AddWithValue("@PM", pm);
            command.Parameters.AddWithValue("@CSGLvlId", csgLvlId);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            var data = dt.AsEnumerable().Where(row => row.Field<String>("Redesign Number") == rdsgnNos).Select(row => row.Field<Byte>("CSG Level"));

            if (data.Count() == 1)
            {
                var csg = data.FirstOrDefault();
                if (csg > 0 && adid != null)
                {
                    // LogWebActivity
                    var UserCsg = _commonRepo.GetCSGLevelAdId(adid);
                    _commonRepo.LogWebActivity(((byte)WebActyType.RedesignSearch), rdsgnNos, adid, (byte)UserCsg, (byte)csg);
                }
            }

            return dt;
        }

        private void InsertRedesignDeviceChargesIntoMach5(string devIds)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter() {ParameterName = "@REDSGN_DEV_IDS", SqlDbType = SqlDbType.VarChar, Value = devIds}
            };
            _context.Database.ExecuteSqlCommand("dbo.insertRedesignDeviceChargesIntoMach5 @REDSGN_DEV_IDS", pc.ToArray());
        }

        public IQueryable<RedesignNtePmData> GetNtePmRedesignData(string redsgnNbr)
        {
            var result = (from r in _context.Redsgn
                          join u1 in _context.LkUser on r.NteAssigned equals u1.UserAdid
                          join u2 in _context.LkUser on r.PmAssigned equals u2.UserAdid
                          where r.RedsgnNbr == redsgnNbr
                          select new RedesignNtePmData { RedsgnNbr = r.RedsgnNbr, NteAssigned = r.NteAssigned, NteAssignedUserId = u1.UserId, NteAssignedEmail = u1.EmailAdr, PmAssigned = r.PmAssigned, PmAssignedUserId = u2.UserId, PmAssignedEmail = u2.EmailAdr });

            return result;
        }

        private void InsertBPMRedesignTransaction(int redesignId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter() {ParameterName = "@REDSGN_ID", SqlDbType = SqlDbType.Int, Value = redesignId}
            };
            _context.Database.ExecuteSqlCommand("dbo.InsertBPMRedesignTransaction @REDSGN_ID", pc.ToArray());
        }
    }
}