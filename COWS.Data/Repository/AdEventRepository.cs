﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class AdEventRepository : IAdEventRepository
    {
        private readonly COWSAdminDBContext _context;

        //private readonly CommonRepository _common;
        private readonly ICommonRepository _common;

        public AdEventRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            //_common = new CommonRepository(context);
            _common = common;
        }

        public IQueryable<AdEvent> Find(Expression<Func<AdEvent, bool>> predicate)
        {
            IQueryable<AdEvent> ad = _context.AdEvent
                .Include(a => a.Event)
                .Include(a => a.AdEventAccsTag)
                .Include(a => a.EnhncSrvc)
                .Include(a => a.EventStus)
                .Include(a => a.WrkflwStus)
                .Include(a => a.CreatByUser)
                .Where(predicate);

            foreach (AdEvent item in ad)
            {
                if (item.Event.CsgLvlId > 0)
                {
                    GetEventDecryptValues sdata = _common.GetEventDecryptValues(item.EventId, "AD").FirstOrDefault();
                    if (sdata != null)
                    {
                        item.CustNme = sdata.CUST_NME;
                        item.CustCntctNme = sdata.CUST_CNTCT_NME;
                        item.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                        item.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                        item.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                        item.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                        item.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                        item.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                        item.EventDes = sdata.EVENT_DES;
                    }
                }
            }

            return ad;
        }

        public IEnumerable<AdEvent> GetAll()
        {
            return _context.AdEvent
                .ToList();
        }

        public AdEvent GetById(int id, string adid=null)
        {
            AdEvent ad = _context.AdEvent
                .Include(a => a.Event)
                .Include(a => a.AdEventAccsTag)
                .Include(a => a.EnhncSrvc)
                .Include(a => a.EventStus)
                .Include(a => a.WrkflwStus)
                .Include(a => a.CreatByUser)
                .SingleOrDefault(a => a.EventId == id);

            if (ad.Event.CsgLvlId > 0)
            {
                GetEventDecryptValues sdata = _common.GetEventDecryptValues(ad.EventId, "AD").FirstOrDefault();
                if (sdata != null)
                {
                    ad.CustNme = sdata.CUST_NME;
                    ad.CustCntctNme = sdata.CUST_CNTCT_NME;
                    ad.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                    ad.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                    ad.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                    ad.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                    ad.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                    ad.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                    ad.EventDes = sdata.EVENT_DES;
                }

                // LogWebActivity
                var UserCsg = _common.GetCSGLevelAdId(adid);
                _common.LogWebActivity(((byte)WebActyType.EventDetails), ad.Event.EventId.ToString(), adid, (byte)UserCsg, (byte)ad.Event.CsgLvlId);
            }

            return ad;
        }

        public AdEvent GetById(int id)
        {
            AdEvent ad = _context.AdEvent
                .Include(a => a.Event)
                .Include(a => a.AdEventAccsTag)
                .Include(a => a.EnhncSrvc)
                .Include(a => a.EventStus)
                .Include(a => a.WrkflwStus)
                .Include(a => a.CreatByUser)
                .SingleOrDefault(a => a.EventId == id);

            if (ad.Event.CsgLvlId > 0)
            {
                GetEventDecryptValues sdata = _common.GetEventDecryptValues(ad.EventId, "AD").FirstOrDefault();
                if (sdata != null)
                {
                    ad.CustNme = sdata.CUST_NME;
                    ad.CustCntctNme = sdata.CUST_CNTCT_NME;
                    ad.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                    ad.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                    ad.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                    ad.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                    ad.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                    ad.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                    ad.EventDes = sdata.EVENT_DES;
                }
            }

            return ad;
        }

        public AdEvent Create(AdEvent entity)
        {
            _context.Event.Add(entity.Event);
            SaveAll();

            entity.EventId = entity.Event.EventId;

            // Handle secure data
            if (entity.Event.CsgLvlId > 0)
            {
                SecuredData obj = new SecuredData(entity);
                List<GetEncryptValues> EncValues = _common.GetEventEncryptValues(obj).ToList();

                CustScrdData sdata = new CustScrdData();
                sdata.ScrdObjId = entity.Event.EventId;
                sdata.ScrdObjTypeId = (byte)SecuredObjectType.AD_EVENT;
                sdata.CustNme = EncValues[0].Item;
                sdata.CustCntctNme = EncValues[1].Item;
                sdata.CustCntctPhnNbr = EncValues[2].Item;
                sdata.CustEmailAdr = EncValues[3].Item;
                sdata.CustCntctCellPhnNbr = EncValues[4].Item;
                sdata.CustCntctPgrNbr = EncValues[5].Item;
                sdata.CustCntctPgrPinNbr = EncValues[6].Item;
                sdata.EventTitleTxt = EncValues[7].Item;
                sdata.EventDes = EncValues[8].Item;
                sdata.CreatDt = DateTime.Now; ;

                _context.CustScrdData.Add(sdata);

                entity.CustNme = null;
                entity.CustCntctNme = null;
                entity.CustCntctPhnNbr = null;
                entity.CustEmailAdr = null;
                entity.CustCntctCellPhnNbr = null;
                entity.CustCntctPgrNbr = null;
                entity.CustCntctPgrPinNbr = null;
                entity.EventTitleTxt = null;
                entity.EventDes = null;
            }

            _context.AdEvent.Add(entity);
            SaveAll();

            return GetById(entity.EventId);
        }

        public void Update(int id, AdEvent entity)
        {
            AdEvent ad = GetById(id);

            ad.EventTitleTxt = entity.EventTitleTxt;
            ad.EventDes = entity.EventDes;
            ad.EventStusId = entity.EventStusId;
            ad.EnhncSrvcId = entity.EnhncSrvcId;
            ad.Ftn = entity.Ftn;
            ad.H1 = entity.H1;
            ad.H6 = entity.H6;
            ad.CharsId = entity.CharsId;
            ad.CustNme = entity.CustNme;
            ad.CustEmailAdr = entity.CustEmailAdr;
            ad.CustCntctPhnNbr = entity.CustCntctPhnNbr;
            ad.CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
            ad.CustCntctNme = entity.CustCntctNme;
            ad.CustCntctPgrNbr = entity.CustCntctPgrNbr;
            ad.CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
            ad.ReqorUserId = entity.ReqorUserId;
            ad.CnfrcBrdgNbr = entity.CnfrcBrdgNbr;
            ad.CnfrcPinNbr = entity.CnfrcPinNbr;
            ad.SalsUserId = entity.SalsUserId;
            ad.EsclCd = entity.EsclCd;
            ad.EsclReasId = entity.EsclReasId;
            ad.PrimReqDt = entity.PrimReqDt;
            ad.ScndyReqDt = entity.ScndyReqDt;
            ad.PubEmailCcTxt = entity.PubEmailCcTxt;
            ad.CmpltdEmailCcTxt = entity.CmpltdEmailCcTxt;
            ad.DesCmntTxt = entity.DesCmntTxt;
            ad.DocLinkTxt = entity.DocLinkTxt;
            ad.AdEventAccsTag = entity.AdEventAccsTag;
            ad.StrtTmst = entity.StrtTmst;
            ad.EndTmst = entity.EndTmst;
            ad.EventDrtnInMinQty = entity.EventDrtnInMinQty;
            ad.ExtraDrtnTmeAmt = entity.ExtraDrtnTmeAmt;

            ad.WrkflwStusId = entity.WrkflwStusId;
            ad.ModfdByUserId = entity.ModfdByUserId;
            ad.ModfdDt = DateTime.Now;

            //ad.EventStusId = (byte)SetEventStatus(ad.WrkflwStusId);

            SaveAll();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}