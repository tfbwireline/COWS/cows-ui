﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class RedesignNoteRepository : IRedesignNoteRepository
    {
        private readonly COWSAdminDBContext _context;

        public RedesignNoteRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public RedsgnNotes Create(RedsgnNotes entity)
        {
            int maxId = _context.RedsgnNotes.Max(i => i.RedsgnNotesId);
            entity.RedsgnNotesId = (short)++maxId;
            _context.RedsgnNotes.Add(entity);

            SaveAll();

            return GetById(entity.RedsgnNotesId);
        }

        public void Delete(int id)
        {
            RedsgnNotes note = GetById(id);
            _context.RedsgnNotes.Remove(note);

            SaveAll();
        }

        public IQueryable<RedsgnNotes> Find(Expression<Func<RedsgnNotes, bool>> predicate)
        {
            return _context.RedsgnNotes
                            .Include(i => i.RedsgnNteType)
                            .Include(i => i.CretdByCdNavigation)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<RedsgnNotes> GetAll()
        {
            return _context.RedsgnNotes
                            .Include(i => i.RedsgnNteType)
                            .Include(i => i.CretdByCdNavigation)
                            .OrderBy(i => i.CretdDt)
                            .AsNoTracking()
                            .ToList();
        }

        public RedsgnNotes GetById(int id)
        {
            return _context.RedsgnNotes
                            .Include(i => i.RedsgnNteType)
                            .Include(i => i.CretdByCdNavigation)
                            .SingleOrDefault(i => i.RedsgnNotesId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, RedsgnNotes entity)
        {
            RedsgnNotes note = GetById(id);
            note.RedsgnId = entity.RedsgnId;
            note.RedsgnNteTypeId = entity.RedsgnNteTypeId;

            SaveAll();
        }
    }
}
