﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class VendorTemplateRepository : IVendorTemplateRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly int _genericVndrFldrId;

        public VendorTemplateRepository(COWSAdminDBContext context)
        {
            _context = context;
            _genericVndrFldrId = _context.VndrFoldr.SingleOrDefault(i => i.VndrCd == "GENVEN").VndrFoldrId;
        }

        public VndrTmplt Create(VndrTmplt entity)
        {
            // Added this condition to cater saving of template not just the generic template
            entity.VndrFoldrId = entity.VndrFoldrId > 0 ? entity.VndrFoldrId : _genericVndrFldrId;
            _context.VndrTmplt.Add(entity);
            SaveAll();

            return GetById(entity.VndrTmpltId);
        }

        public void Delete(int id)
        {
            VndrTmplt template = GetById(id);
            _context.VndrTmplt.Remove(template);
            SaveAll();
        }

        public IQueryable<VndrTmplt> Find(Expression<Func<VndrTmplt, bool>> predicate)
        {
            return _context.VndrTmplt
                            .Include(i => i.CreatByUser)
                            .Where(predicate);
        }

        public IEnumerable<VndrTmplt> GetAll()
        {
            return _context.VndrTmplt
                            .Include(i => i.CreatByUser)
                            .Where(i => i.VndrFoldrId == _genericVndrFldrId)
                            .ToList();
        }

        public VndrTmplt GetById(int id)
        {
            return _context.VndrTmplt
                            .Include(i => i.CreatByUser)
                            .SingleOrDefault(i => i.VndrTmpltId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, VndrTmplt entity)
        {
            throw new NotImplementedException();
        }
    }
}
