﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSActivityTypeRepository : IMDSActivityTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MDSActivityTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkMdsActyType> Find(Expression<Func<LkMdsActyType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsActyType, out List<LkMdsActyType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMdsActyType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsActyType, out List<LkMdsActyType> list))
            {
                list = _context.LkMdsActyType
                            .AsNoTracking()
                            .ToList();

                _cache.Set(CacheKeys.LkMdsActyType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMdsActyType GetById(int id)
        {
            return _context.LkMdsActyType
                .SingleOrDefault(a => a.MdsActyTypeId == id);
        }

        public LkMdsActyType Create(LkMdsActyType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkMdsActyType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMdsActyType);
            return _context.SaveChanges();
        }
    }
}