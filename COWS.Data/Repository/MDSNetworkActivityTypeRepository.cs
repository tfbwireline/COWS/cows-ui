﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSNetworkActivityTypeRepository : IMDSNetworkActivityTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MDSNetworkActivityTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMdsNtwkActyType Create(LkMdsNtwkActyType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMdsNtwkActyType> Find(Expression<Func<LkMdsNtwkActyType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsNtwkActyType, out List<LkMdsNtwkActyType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMdsNtwkActyType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsNtwkActyType, out List<LkMdsNtwkActyType> list))
            {
                list = _context.LkMdsNtwkActyType
                            .OrderBy(i => i.NtwkActyTypeDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMdsNtwkActyType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMdsNtwkActyType GetById(int id)
        {
            return _context.LkMdsNtwkActyType
                            .SingleOrDefault(i => i.NtwkActyTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMdsNtwkActyType);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMdsNtwkActyType entity)
        {
            throw new NotImplementedException();
        }
    }
}