﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class RegionRepository : IRegionRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public RegionRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkXnciRgn> Find(Expression<Func<LkXnciRgn, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkXnciRgn, out List<LkXnciRgn> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkXnciRgn> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkXnciRgn, out List<LkXnciRgn> list))
            {
                list = _context.LkXnciRgn
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkXnciRgn, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkXnciRgn GetById(int id)
        {
            return _context.LkXnciRgn
                .SingleOrDefault(a => a.RgnId == id);
        }

        public LkXnciRgn Create(LkXnciRgn entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkXnciRgn entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkXnciRgn);
            return _context.SaveChanges();
        }
    }
}