﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class DeviceModelRepository : IDeviceModelRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public DeviceModelRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkDevModel Create(LkDevModel entity)
        {
            int maxId = _context.LkDevModel.Max(i => i.DevModelId);
            entity.DevModelId = (short)++maxId;
            _context.LkDevModel.Add(entity);

            SaveAll();

            return GetById(entity.DevModelId);
        }

        public void Delete(int id)
        {
            LkDevModel proj = GetById(id);
            _context.LkDevModel.Remove(proj);

            SaveAll();
        }

        public IQueryable<LkDevModel> Find(Expression<Func<LkDevModel, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkDevModel, out List<LkDevModel> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkDevModel> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkDevModel, out List<LkDevModel> list))
            {
                list = _context.LkDevModel
                            .Include(i => i.Manf)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            //.Where(i => i.RecStusId == (byte)ERecStatus.Active)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkDevModel, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkDevModel GetById(int id)
        {
            return _context.LkDevModel
                .Include(i => i.Manf)
                .Include(i => i.CreatByUser)
                .Include(i => i.ModfdByUser)
                .Include(i => i.QlfctnVndrModel)
                .Include(i => i.MdsEventOdieDev)
                .SingleOrDefault(i => i.DevModelId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkDevModel);
            return _context.SaveChanges();
        }

        public void Update(int id, LkDevModel entity)
        {
            LkDevModel model = GetById(id);
            model.DevModelNme = entity.DevModelNme;
            model.MinDrtnTmeReqrAmt = entity.MinDrtnTmeReqrAmt;
            model.ManfId = entity.ManfId;
            model.RecStusId = entity.RecStusId;
            model.ModfdByUserId = entity.ModfdByUserId;
            model.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}