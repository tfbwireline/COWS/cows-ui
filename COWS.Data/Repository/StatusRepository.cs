﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class StatusRepository : IStatusRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public StatusRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkStus> Find(Expression<Func<LkStus, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkStus, out List<LkStus> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkStus> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkStus, out List<LkStus> list))
            {
                list = _context.LkStus
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkStus, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkStus GetById(int id)
        {
            return _context.LkStus
                .SingleOrDefault(a => a.StusId == id);
        }

        public LkStus Create(LkStus entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkStus entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkStus);
            return _context.SaveChanges();
        }
    }
}