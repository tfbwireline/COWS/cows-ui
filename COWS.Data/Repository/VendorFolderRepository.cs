﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using COWS.Entities.QueryModels;
using Microsoft.Extensions.Configuration;
using COWS.Entities.Enums;

namespace COWS.Data.Repository
{
    public class VendorFolderRepository : IVendorFolderRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _commonRepo;

        public VendorFolderRepository(COWSAdminDBContext context, ICommonRepository commonRepo, IConfiguration configuration)
        {
            _context = context;
            _commonRepo = commonRepo;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public VndrFoldr Create(VndrFoldr entity)
        {
            _context.VndrFoldr.Add(entity);
            SaveAll();

            return GetById(entity.VndrFoldrId);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<VndrFoldr> Find(Expression<Func<VndrFoldr, bool>> predicate)
        {
            return _context.VndrFoldr
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.CtryCdNavigation)
                            .Include(i => i.SttCdNavigation)
                            .Include(i => i.VndrCdNavigation)
                            .Include(i => i.VndrFoldrCntct)
                            .Include(i => i.VndrTmplt)
                            .Include(i => i.VndrOrdr)
                            .Where(predicate);

        }

        public IEnumerable<VndrFoldr> GetAll()
        {
            return _context.VndrFoldr
                            .ToList();
        }

        public VndrFoldr GetById(int id)
        {
            return _context.VndrFoldr
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.CtryCdNavigation)
                            .Include(i => i.SttCdNavigation)
                            .Include(i => i.VndrCdNavigation)
                            .Include(i => i.VndrFoldrCntct)
                            .Include(i => i.VndrTmplt)
                            .Include(i => i.VndrOrdr)
                            .SingleOrDefault(i => i.VndrFoldrId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, VndrFoldr entity)
        {
            VndrFoldr vndrFolder = GetById(id);

            if (vndrFolder != null)
            {
                vndrFolder.VndrCd = entity.VndrCd;
                vndrFolder.StreetAdr1 = entity.StreetAdr1;
                vndrFolder.StreetAdr2 = entity.StreetAdr2;
                vndrFolder.BldgNme = entity.BldgNme;
                vndrFolder.FlrId = entity.FlrId;
                vndrFolder.RmNbr = entity.RmNbr;
                vndrFolder.CtyNme = entity.CtyNme;
                vndrFolder.SttCd = entity.SttCd;
                vndrFolder.PrvnNme = entity.PrvnNme;
                vndrFolder.ZipPstlCd = entity.ZipPstlCd;
                vndrFolder.CtryCd = entity.CtryCd;
                vndrFolder.CmntTxt = entity.CmntTxt;
                vndrFolder.VndrEmailListTxt = entity.VndrEmailListTxt;
                vndrFolder.ModfdDt = entity.ModfdDt;
                vndrFolder.ModfdByUserId = entity.ModfdByUserId;
                vndrFolder.RecStusId = entity.RecStusId;

                // Vendor Folder Contacts
                if (entity.VndrFoldrCntct != null && entity.VndrFoldrCntct.Count() > 0)
                {
                    // Add contact not on list
                    List<VndrFoldrCntct> newContacts = entity.VndrFoldrCntct.Where(i => i.CntctId == 0).ToList();
                    _context.VndrFoldrCntct.AddRange(newContacts);

                    // Update
                    foreach (VndrFoldrCntct contact in entity.VndrFoldrCntct)
                    {
                        VndrFoldrCntct cntctDb = _context.VndrFoldrCntct
                            .Where(i => i.CntctId == contact.CntctId && contact.CntctId > 0).SingleOrDefault();

                        if (cntctDb != null)
                        {
                            cntctDb.CntctFrstNme = contact.CntctFrstNme;
                            cntctDb.CntctLstNme = contact.CntctLstNme;
                            cntctDb.CntctPhnNbr = contact.CntctPhnNbr;
                            cntctDb.CntctEmailAdr = contact.CntctEmailAdr;
                            cntctDb.CntctProdRoleTxt = contact.CntctProdRoleTxt;
                            cntctDb.CntctProdTypeTxt = contact.CntctProdTypeTxt;

                            _context.VndrFoldrCntct.Update(cntctDb);
                        }
                    }

                    // Delete contacts which have been previously databased
                    List<int> selectedContacts = entity.VndrFoldrCntct
                        .Where(i => i.CntctId > 0).Select(i => i.CntctId).ToList();
                    var deleteContacts = _context.VndrFoldrCntct
                        .Where(i => i.VndrFoldrId == id && !selectedContacts.Contains(i.CntctId));
                    if (deleteContacts != null && deleteContacts.Count() > 0)
                    {
                        _context.VndrFoldrCntct.RemoveRange(deleteContacts);
                    }
                }
                else
                {
                    List<VndrFoldrCntct> cntcts = _context.VndrFoldrCntct.Where(i => i.VndrFoldrId == id).ToList();
                    _context.VndrFoldrCntct.RemoveRange(cntcts);
                }

                // Vendor Folder Template
                if (entity.VndrTmplt != null && entity.VndrTmplt.Count() > 0)
                {
                    // Add template not on list
                    List<VndrTmplt> newTemplates = entity.VndrTmplt.Where(i => i.VndrTmpltId == 0).ToList();
                    _context.VndrTmplt.AddRange(newTemplates);

                    // Delete template which have been previously databased
                    List<int> selectedTemplates = entity.VndrTmplt
                        .Where(i => i.VndrTmpltId > 0).Select(i => i.VndrTmpltId).ToList();
                    var deleteTemplates = _context.VndrTmplt
                        .Where(i => i.VndrFoldrId == id && !selectedTemplates.Contains(i.VndrTmpltId));
                    if (deleteTemplates != null && deleteTemplates.Count() > 0)
                    {
                        _context.VndrTmplt.RemoveRange(deleteTemplates);
                    }
                }
                else
                {
                    List<VndrTmplt> tmplts = _context.VndrTmplt.Where(i => i.VndrFoldrId == id).ToList();
                    _context.VndrTmplt.RemoveRange(tmplts);
                }

                SaveAll();
            }
        }

        public IEnumerable<GetVendorFolder> GetVendorFolder(string vendorName, string countryCode)
        {           
            List<SqlParameter> vf = new List<SqlParameter>
            {
                new SqlParameter("@VendorName", vendorName.ToString()),
                new SqlParameter("@CountryCode", countryCode.ToString())
            };

            var vendorFolder = _context.Query<GetVendorFolder>()
                .AsNoTracking()
                .FromSql("dbo.getVendorFolders @VendorName, @CountryCode", vf.ToArray()).ToList();

            return vendorFolder;
        }

        public DataSet GetVendorOrderEmailData(int inVendorOrderID, int userCsgLvlId = 0)
        {
            SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("dbo.getVendorOrderEmailData_V2", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@VendorOrderID", inVendorOrderID);
            command.Parameters.AddWithValue("@UserCsgLvlID", userCsgLvlId);
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;
        }

        public VndrOrdrEmail GetVendorOrderEmail(int inVendorOrderEmailID)
        {
            VndrOrdrEmail outEmail = null;

                    var query = (from voe in
                                     (from em in _context.VndrOrdrEmail
                                      join user in _context.LkUser on em.CreatByUserId equals user.UserId
                                      where em.VndrOrdrEmailId == inVendorOrderEmailID
                                      select new { em, user })
                                 join u in _context.LkUser on voe.em.ModfdByUserId equals u.UserId into leftjoin
                                 from mu in leftjoin.DefaultIfEmpty()
                                 select new { voe.em, voe.user, MuUser = mu, MuUserAdid = mu.UserAdid, MuDsplNme = mu.DsplNme }); ;

                   
                    foreach (var q in query)
                    {
                        outEmail = new VndrOrdrEmail();

                        outEmail.VndrOrdrEmailId = q.em.VndrOrdrEmailId;
                        outEmail.VndrOrdrId = q.em.VndrOrdrId;
                        outEmail.VerNbr = q.em.VerNbr;
                        outEmail.VndrEmailTypeId = (DBNull.Value.Equals(q.em.VndrEmailTypeId) == true ? 0 : q.em.VndrEmailTypeId);
                        outEmail.FromEmailAdr = q.em.FromEmailAdr;
                        outEmail.ToEmailAdr = q.em.ToEmailAdr;
                        outEmail.CcEmailAdr = q.em.CcEmailAdr;
                        outEmail.EmailBody = q.em.EmailBody;
                        outEmail.EmailStusId = Convert.ToByte(q.em.EmailStusId);
                        if (q.em.SentDt != null)
                            outEmail.SentDt = Convert.ToDateTime(q.em.SentDt);
                        outEmail.CreatDt = q.em.CreatDt;
                        outEmail.CreatByUser = q.user;
                        //outEmail.CreatByUser.UserAdid = q.user.UserAdid;
                        if (q.em.ModfdByUserId != null)
                        {
                            outEmail.ModfdDt = Convert.ToDateTime(q.em.ModfdDt);
                            outEmail.ModfdByUser = q.MuUser;
                            //outEmail.ModfdByUser.UserAdid = q.MuUserAdid;
                        }
                        outEmail.EmailSubjTxt = q.em.EmailSubjTxt;

                        return outEmail;
                    }                    
           
            return outEmail;
        }

        //public VendorOrderEmailResults GetVendorOrderEmails(int vendorOrderID)
        //{
        //    SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
        //    SqlCommand command = new SqlCommand();
        //    SqlDataAdapter da = new SqlDataAdapter();
        //    DataSet ds = new DataSet();
        //    VendorOrderEmailResults toReturn = new VendorOrderEmailResults();
        //    command = new SqlCommand("dbo.getVendorOrderEmails", connection);
        //    command.CommandType = CommandType.StoredProcedure;
        //    command.Parameters.AddWithValue("@VendorOrderID", vendorOrderID);
        //    da = new SqlDataAdapter(command);
        //    da.Fill(ds);
        //    connection.Close();

        //    List<VndrOrdrEmailModel> voeList = new List<VndrOrdrEmailModel>();
        //    DataTable voeTable = ds.Tables[0];
        //    List<VndrOrdrEmailAtchmt> voeaList = new List<VndrOrdrEmailAtchmt>();
        //    DataTable voeaTable = ds.Tables[1];

        //    for (int i = 0; i < voeTable.Rows.Count; i++)
        //    {
        //        VndrOrdrEmailModel voe = new VndrOrdrEmailModel();
        //        DataRow dr = voeTable.Rows[i];
        //        voe.VndrOrdrEmailId = (dr["VNDR_ORDR_EMAIL_ID"] != DBNull.Value ? DataRowExtensions.Field<int>(dr, "VNDR_ORDR_EMAIL_ID") : 0);
        //        voe.VerNbr = (dr["VER_NBR"] != DBNull.Value ? DataRowExtensions.Field<byte>(dr, "VER_NBR") : (byte)0);
        //        voe.CreatByUserId = (dr["CREAT_BY_USER_ID"] != DBNull.Value ? DataRowExtensions.Field<int>(dr, "CREAT_BY_USER_ID") : 0);
        //        voe.CreatByUserFullName = (dr["CREATED_BY"] != DBNull.Value ? DataRowExtensions.Field<string>(dr, "CREATED_BY") : String.Empty);
        //        voe.ModfdByUserId = (dr["MODFD_BY_USER_ID"] != DBNull.Value ? DataRowExtensions.Field<int>(dr, "MODFD_BY_USER_ID") : 0);
        //        voe.ModfdByUserFullName = (dr["MODIFIED_BY"] != DBNull.Value ? DataRowExtensions.Field<string>(dr, "MODIFIED_BY") : String.Empty);
        //        voe.ModfdDt = DataRowExtensions.Field<DateTime>(dr, "MODFD_DT");
        //        voe.EmailSubjTxt = (dr["EMAIL_SUBJ_TXT"] != DBNull.Value ? DataRowExtensions.Field<string>(dr, "EMAIL_SUBJ_TXT") : String.Empty);
        //        voe.EmailStusId = (dr["EMAIL_STUS_ID"] != DBNull.Value ? DataRowExtensions.Field<short>(dr, "EMAIL_STUS_ID") : (byte)0);
        //        voe.EmailStatusDesc = (dr["STUS_DES"] != DBNull.Value ? DataRowExtensions.Field<string>(dr, "STUS_DES") : String.Empty);
        //        if (dr["SENT_TO_VNDR_DT"] != DBNull.Value)
        //            voe.SentDt = Convert.ToDateTime(dr["SENT_TO_VNDR_DT"]);
        //        if (dr["ACK_BY_VNDR_DT"] != DBNull.Value)
        //            voe.AckByVendorDate = Convert.ToDateTime(dr["ACK_BY_VNDR_DT"]);
        //        voe.EmailTypeDesc = (DBNull.Value.Equals(dr["VNDR_EMAIL_TYPE"]) == true ? string.Empty : dr["VNDR_EMAIL_TYPE"].ToString());
        //        voe.VndrEmailTypeId = Convert.ToByte(DBNull.Value.Equals(dr["VNDR_EMAIL_TYPE_ID"]) == true ? 0 : dr["VNDR_EMAIL_TYPE_ID"]);

        //        voeList.Add(voe);
        //    }

        //    for (int i = 0; i < voeaTable.Rows.Count; i++)
        //    {
        //        VndrOrdrEmailAtchmt voea = new VndrOrdrEmailAtchmt();

        //        voea.VndrOrdrEmailId = Convert.ToInt32(voeaTable.Rows[i]["VNDR_ORDR_EMAIL_ID"]);
        //        voea.FileNme = voeaTable.Rows[i]["FILE_NME"].ToString();
        //        voea.FileSizeQty = Convert.ToInt32(voeaTable.Rows[i]["FILE_SIZE_QTY"]);

        //        voeaList.Add(voea);
        //    }

        //    toReturn.VOE = voeList;
        //    toReturn.VOEA = voeaList;

        //    return toReturn;
        //}
    }
}