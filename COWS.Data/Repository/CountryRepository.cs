﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CountryRepository : ICountryRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CountryRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCtry Create(LkCtry entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCtry> Find(Expression<Func<LkCtry, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCtry, out List<LkCtry> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCtry> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCtry, out List<LkCtry> list))
            {
                list = _context.LkCtry
                            .Include(i => i.Rgn)
                            .OrderBy(i => i.CtryNme)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCtry, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCtry GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCtry);
            return _context.SaveChanges();
        }

        public void Update(int id, LkCtry entity)
        {
            throw new NotImplementedException();
        }

        public void UpdateRegion(string code, LkCtry entity)
        {
            LkCtry ctry = Find(i => i.CtryCd == code).SingleOrDefault();
            if (ctry != null)
            {
                ctry.RgnId = entity.RgnId == 0 ? null : entity.RgnId;
                ctry.ModfdByUserId = entity.ModfdByUserId;
                ctry.ModfdDt = entity.ModfdDt;

                SaveAll();
            }
        }

        public IQueryable<object> GetAllForLookup()
        {
            return _context.LkCtry
                            .AsNoTracking()
                            .Select(i => new
                            {
                                i.CtryCd,
                                i.CtryNme
                            })
                            .OrderBy(i => i.CtryNme);
        }
    }
}