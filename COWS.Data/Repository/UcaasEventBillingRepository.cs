﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UcaasEventBillingRepository : IUcaasEventBillingRepository
    {
        private readonly COWSAdminDBContext _context;

        public UcaasEventBillingRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public UcaaSEventBilling Create(UcaaSEventBilling entity)
        {
            int maxId = _context.UcaaSEventBilling.Max(i => i.UcaaSEventBillingId);
            entity.UcaaSEventBillingId = (short)++maxId;
            _context.UcaaSEventBilling.Add(entity);

            SaveAll();

            return GetById(entity.UcaaSEventBillingId);
        }

        public void Create(IEnumerable<UcaaSEventBilling> entity)
        {
            _context.UcaaSEventBilling
                .AddRange(entity.Select(a =>
                {
                    a.UcaaSEventBillingId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Delete(int id)
        {
            UcaaSEventBilling ucaas = GetById(id);
            _context.UcaaSEventBilling.Remove(ucaas);

            SaveAll();
        }

        public void Delete(Expression<Func<UcaaSEventBilling, bool>> predicate)
        {
            var toDelete = Find(predicate);
            _context.UcaaSEventBilling.RemoveRange(toDelete);
        }

        public IQueryable<UcaaSEventBilling> Find(Expression<Func<UcaaSEventBilling, bool>> predicate)
        {
            return _context.UcaaSEventBilling
                            .Include(i => i.UcaaSBillActy)
                            .Where(predicate);
        }

        public IEnumerable<UcaaSEventBilling> GetAll()
        {
            return _context.UcaaSEventBilling
                            .Include(i => i.UcaaSBillActy)
                            .OrderBy(i => i.UcaaSEventBillingId)
                            .ToList();
        }

        public UcaaSEventBilling GetById(int id)
        {
            return _context.UcaaSEventBilling
                            .Include(i => i.UcaaSBillActy)
                            .SingleOrDefault(i => i.UcaaSEventBillingId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, UcaaSEventBilling entity)
        {
            throw new NotImplementedException();
            // Delete
            // Create
        }
    }
}