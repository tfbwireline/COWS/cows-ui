﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data;

using System.Data.SqlClient;
//using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System.Threading.Tasks;
using System.Linq;
using COWS.Data.Extensions;
using System.Collections.Generic;
using System;
using COWS.Entities.Enums;

namespace COWS.Data.Repository
{
    public class CompleteOrderRepository : ICompleteOrderRepository
    {
        private readonly COWSAdminDBContext _context;
        public CompleteOrderRepository(COWSAdminDBContext context, IConfiguration configuration)
                          
        {
            _context = context;
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public DataTable GetAdminExcPerm(string name)
        {

            var q = (from s in _context.LkSysCfg
                     where (s.RecStusId == (byte)ERecStatus.Active)
                       && (s.PrmtrNme == name)
                     select new { s.PrmtrValuTxt });
            return LinqHelper.CopyToDataTable(q, null, null);
            //return (DataTable)q;

        }
        //public IEnumerable<InsertM5CompleteMsg> M5CmpltMsg(string FTN, int MsgType)
        //{
        //    List<SqlParameter> pc = new List<SqlParameter>
        //                    {
        //                        new SqlParameter() {ParameterName = "@FTN", SqlDbType = SqlDbType.VarChar, Value = FTN },
        //                        new SqlParameter() {ParameterName = "@MsgType", SqlDbType = SqlDbType.Int, Value = MsgType }
        //                    };

        //    var result = _context.Query<InsertM5CompleteMsg>()

        //        .FromSql("web.insertM5CmpltMsg @FTN, @MsgType", pc.ToArray()).ToList<InsertM5CompleteMsg>();

        //    return result;
        //}
        //public int M5CmpltMsg(string FTN, int MsgType)
        //{
        //    List<SqlParameter> pc = new List<SqlParameter>
        //                    {
        //                        new SqlParameter() {ParameterName = "@FTN", SqlDbType = SqlDbType.VarChar, Value = FTN },
        //                        new SqlParameter() {ParameterName = "@MsgType", SqlDbType = SqlDbType.Int, Value = MsgType }
        //                    };

        //    var value = _context.Database.ExecuteSqlCommand("web.insertM5CmpltMsg @FTN, @MsgType", pc.ToArray());
        //    return value;
        //}
        //public int M5CmpltMsg(string FTN, int MsgType)
        //{
        //    //int toReturn = -1;
        //    List<SqlParameter> pc = new List<SqlParameter>
        //        {
        //            new SqlParameter("@FTN", SqlDbType.VarChar),
        //            new SqlParameter("@MsgType", SqlDbType.Int)

        //        };
        //    pc[0].Value = FTN;
        //    pc[1].Value = MsgType;
        //    int toReturn = _context.Database.ExecuteSqlCommand("web.insertM5CmpltMsg @FTN,@MsgType", pc.ToArray());
        //    return toReturn;
        //}


        //var outParameter = new SqlParameter("@outParameter", DbType.Int32)
        //{
        //    Direction = ParameterDirection.Output
        //};

        //context.Database.ExecuteSqlCommand("exec GetFoo @outParameter OUT", outParameter);


        //public int M5CmpltMsg(string FTN, int MsgType)
        //{
        //    var ftn = new SqlParameter("@FTN", FTN);
        //    var msgType = new SqlParameter("@MsgType", MsgType);

        //    var returnValue = new SqlParameter
        //    {
        //        ParameterName = "@ReturnValue",
        //        SqlDbType = SqlDbType.Int,
        //        Direction = ParameterDirection.Output
        //    };
        //    var result = _context.Database.ExecuteSqlCommand("exec web.insertM5CmpltMsg_v2 @FTN, @MsgType, @ReturnValue OUT"
        //                , ftn
        //                , msgType
        //                , returnValue
        //     );
        //    return result;
        //}
        //public int M5CmpltMsg(string FTN, int MsgType)
        //{
        //        MSSqlBase.setCommand("web.insertM5CmpltMsg");
        //        addStringParam("@FTN", FTN);
        //        addIntParam("@MsgType", MsgType);

        //        return execScalar();


        //}
        public int M5CmpltMsg(string FTN, int MsgType)
        {
            SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.insertM5CmpltMsg", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@FTN", FTN);
            command.Parameters.AddWithValue("@MsgType", MsgType);
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            DataRow dr = ds.Tables[0].Rows[0];
            if(!DBNull.Value.Equals(dr.ItemArray[0]))
            {
                int result = Convert.ToInt32(dr.ItemArray[0]);
                connection.Close();
                return result;
            }
            return 0;           
        }

    }
}
