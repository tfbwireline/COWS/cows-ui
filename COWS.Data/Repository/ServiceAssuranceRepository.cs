﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class ServiceAssuranceRepository : IServiceAssuranceRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public ServiceAssuranceRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkSrvcAssrnSiteSupp Create(LkSrvcAssrnSiteSupp entity)
        {
            int maxId = _context.LkSrvcAssrnSiteSupp.Max(i => i.SrvcAssrnSiteSuppId);
            entity.SrvcAssrnSiteSuppId = (byte)++maxId;
            _context.LkSrvcAssrnSiteSupp.Add(entity);

            SaveAll();

            return GetById(entity.SrvcAssrnSiteSuppId);
        }

        public void Delete(int id)
        {
            LkSrvcAssrnSiteSupp SrvcSupp = GetById(id);
            _context.LkSrvcAssrnSiteSupp.Remove(SrvcSupp);

            SaveAll();
        }

        public IQueryable<LkSrvcAssrnSiteSupp> Find(Expression<Func<LkSrvcAssrnSiteSupp, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkSrvcAssrnSiteSupp, out List<LkSrvcAssrnSiteSupp> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkSrvcAssrnSiteSupp> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkSrvcAssrnSiteSupp, out List<LkSrvcAssrnSiteSupp> list))
            {
                list = _context.LkSrvcAssrnSiteSupp
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkSrvcAssrnSiteSupp, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkSrvcAssrnSiteSupp GetById(int id)
        {
            return _context.LkSrvcAssrnSiteSupp
                            .SingleOrDefault(i => i.SrvcAssrnSiteSuppId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkSrvcAssrnSiteSupp);
            return _context.SaveChanges();
        }

        public void Update(int id, LkSrvcAssrnSiteSupp entity)
        {
            LkSrvcAssrnSiteSupp srvcSupp = GetById(id);
            srvcSupp.SrvcAssrnSiteSuppDes = entity.SrvcAssrnSiteSuppDes;
            srvcSupp.RecStusId = entity.RecStusId;
            srvcSupp.ModfdByUserId = entity.ModfdByUserId;
            srvcSupp.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}