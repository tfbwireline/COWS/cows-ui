﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSEventPortBndwdRepository : IMDSEventPortBndwdRepository
    {
        private readonly COWSAdminDBContext _context;

        public MDSEventPortBndwdRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<MdsEventPortBndwd> Find(Expression<Func<MdsEventPortBndwd, bool>> predicate)
        {
            return _context.MdsEventPortBndwd
                           .Where(predicate);
        }

        public IEnumerable<MdsEventPortBndwd> GetAll()
        {
            return _context.MdsEventPortBndwd
                .ToList();
        }

        public MdsEventPortBndwd GetById(int id)
        {
            throw new NotImplementedException();
        }

        public MdsEventPortBndwd Create(MdsEventPortBndwd entity)
        {
            throw new NotImplementedException();
        }

        public void Create(IEnumerable<MdsEventPortBndwd> entity)
        {
            _context.MdsEventPortBndwd
                .AddRange(entity.Select(a =>
                {
                    a.PortBndwdId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Update(int id, MdsEventPortBndwd entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            throw new NotImplementedException();
        }

        public void SetInactive(Expression<Func<MdsEventPortBndwd, bool>> predicate)
        {
            _context.MdsEventPortBndwd
                .Where(predicate)
                .ToList()
                .ForEach(a => {
                    a.RecStusId = (byte)0;
                    a.ModfdDt = DateTime.Now;
                });
        }

        public IEnumerable<MdsEventPortBndwd> GetMDSEventPortBndwdByEventId(int eventId)
        {
            return _context.MdsEventPortBndwd.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1))
                .ToList();
        }

        public void AddPortBndwd(ref List<MdsEventPortBndwd> data, int eventId)
        {
            //DeleteAllPortBndwdItems(Obj.EventID);
            InactiveAllPortBndwdItems(eventId);

            foreach (MdsEventPortBndwd item in data)
            {
                MdsEventPortBndwd mpb = new MdsEventPortBndwd();

                mpb.EventId = eventId;
                mpb.M5OrdrCmpntId = item.M5OrdrCmpntId;
                mpb.M5OrdrNbr = item.M5OrdrNbr;
                mpb.Nua = item.Nua;
                mpb.PortBndwd = item.PortBndwd;
                mpb.OdieDevNme = item.OdieDevNme;
                mpb.CreatDt = DateTime.Now;
                mpb.RecStusId = (byte)1;

                _context.MdsEventPortBndwd.Add(mpb);
            }
            _context.SaveChanges();
        }

        private void InactiveAllPortBndwdItems(int eventId)
        {
            _context.MdsEventPortBndwd.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1)).ToList().ForEach(b => { b.RecStusId = ((byte)0); b.ModfdDt = DateTime.Now; });
            _context.SaveChanges();
        }
    }
}