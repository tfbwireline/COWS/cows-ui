﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptHostManagedServiceRepository : ICptHostManagedServiceRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptHostManagedServiceRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptHstMngdSrvc Create(LkCptHstMngdSrvc entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptHstMngdSrvc> Find(Expression<Func<LkCptHstMngdSrvc, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptHstMngdSrvc, out List<LkCptHstMngdSrvc> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptHstMngdSrvc> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptHstMngdSrvc, out List<LkCptHstMngdSrvc> list))
            {
                list = _context.LkCptHstMngdSrvc
                            .OrderBy(i => i.CptHstMngdSrvc)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptHstMngdSrvc, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptHstMngdSrvc GetById(int id)
        {
            return _context.LkCptHstMngdSrvc
                            .SingleOrDefault(i => i.CptHstMngdSrvcId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptHstMngdSrvc);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptHstMngdSrvc entity)
        {
            throw new NotImplementedException();
        }
    }
}