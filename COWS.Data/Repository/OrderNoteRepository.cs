﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderNoteRepository : IOrderNoteRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IEventRuleRepository _evntRuleRepo;

        public OrderNoteRepository(COWSAdminDBContext context, IEventRuleRepository evntRuleRepo)
        {
            _context = context;
            _evntRuleRepo = evntRuleRepo;
        }

        public IQueryable<OrdrNte> Find(Expression<Func<OrdrNte, bool>> predicate)
        {
            return _context.OrdrNte
                            .Include(a => a.CreatByUser)
                            .Include(a => a.NteType)
                            .AsNoTracking()
                            .Where(predicate);
        }

        public IEnumerable<OrdrNte> GetAll()
        {
            return _context.OrdrNte
                            .Include(a => a.CreatByUser)
                            .Include(a => a.NteType)
                            .AsNoTracking()
                            .ToList();
        }

        public OrdrNte GetById(int id)
        {
            return _context.OrdrNte
                .Include(a => a.CreatByUser)
                .Include(a => a.NteType)
                .SingleOrDefault(a => a.NteId == id);
        }

        public IEnumerable<OrdrNte> GetByOrdrId(int id)
        {
            return _context.OrdrNte
                .Include(a => a.CreatByUser)
                .Include(a => a.NteType)
                .Where(a => a.OrdrId == id)
                .AsNoTracking()
                .ToList();
        }

        public IEnumerable<OrdrNte> GetByNoteTypeId(int id)
        {
            return _context.OrdrNte
                .Where(a => a.NteTypeId == id).ToList();
        }

        public OrdrNte Create(OrdrNte entity)
        {
            entity.NteType = _context.LkNteType
                .Where(a => a.NteTypeId == entity.NteTypeId)
                .SingleOrDefault();

            _context.OrdrNte.Add(entity);
            SaveAll();

            return GetById(entity.NteId);
        }

        public int CreateNote(OrdrNte entity)
        {
            _context.OrdrNte.Add(entity);
            SaveAll();

            return (int)(from e in _context.OrdrNte where e.OrdrId == entity.OrdrId select (int?)e.NteId).Max();
        }
        public void Update(int id, OrdrNte entity)
        {
            OrdrNte ordr = GetById(id);
            ordr.NteTypeId = entity.NteTypeId;
            ordr.OrdrId = entity.OrdrId;
            ordr.CreatDt = entity.CreatDt;
            ordr.CreatByUserId = entity.CreatByUserId;
            ordr.RecStusId = entity.RecStusId;
            ordr.NteTxt = entity.NteTxt;
            ordr.ModfdByUserId = entity.ModfdByUserId;
            ordr.ModfdDt = entity.ModfdDt;

            SaveAll();
        }

        public List<Object> GetNotes(int inOrderID, short inNoteTypeID)
        {
            var selectQuery = from nt in _context.OrdrNte
                              join usr in _context.LkUser on nt.CreatByUserId equals usr.UserId
                              where nt.OrdrId == inOrderID
                              && nt.NteTypeId == inNoteTypeID
                              select new { NoteID = nt.NteId, NoteText = nt.NteTxt, CreateDate = nt.CreatDt, usr.FullNme };
            return selectQuery.ToList<Object>();
        }

        public void Delete(int id)
        {
            OrdrNte ordr = GetById(id);
            _context.OrdrNte.Remove(ordr);

            SaveAll();
        }

        public bool Delete(int[] Ids)
        {
            var objects = from s in _context.OrdrNte where Ids.Contains(s.NteId) select s;

            _context.OrdrNte.RemoveRange(objects);
            SaveAll();
            return true;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public List<OrderNoteView> Select(string SortField)
        {
            //var lonte = _context.OrdrNte
            //        .Join(_context.LkNteType, orn => orn.NteTypeId, lnt => lnt.NteTypeId, (orn, lnt) => new { orn, lnt })
            //        .Join(_context.LkUser, ornt => ornt.orn.CreatByUserId, lu => lu.UserId, (ornt, lus) => new { ornt.orn, ornt.lnt, lus })
            //        .Where(ornx => ornx.orn.NteId != 0)
            //        .OrderBy(SortField)
            //        .ToList();

            var lonte = (from orn in _context.OrdrNte
                         join lnt in _context.LkNteType on orn.NteTypeId equals lnt.NteTypeId
                         join lu in _context.LkUser on orn.CreatByUserId equals lu.UserId
                         where orn.NteId != 0
                         orderby (SortField)
                         select new { orn, lnt, lu }).Distinct()
                         .ToList();
            List<OrderNoteView> lont = new List<OrderNoteView>();

            foreach (var onte in lonte)
            {
                OrderNoteView ont = new OrderNoteView();
                ont.NteId = onte.orn.NteId;
                ont.NteTypeId = onte.orn.NteTypeId;
                ont.OrdrId = onte.orn.OrdrId;
                ont.CreatDt = onte.orn.CreatDt;
                ont.CreatByUserId = onte.orn.CreatByUserId;
                ont.RecStusId = onte.orn.RecStusId;
                ont.NteTxt = onte.orn.NteTxt;
                ont.ModfdByUserId = onte.orn.ModfdByUserId;
                ont.ModfdDt = onte.orn.ModfdDt;
                ont.CreatByUser = onte.lu.UserAdid;
                ont.NteType = onte.lnt.NteTypeDes;

                lont.Add(ont);
            }

            return lont;
        }

        //public List<OrdrNte> Select(string SortField, Expression<Func<OrdrNte, bool>> predicate)
        //{
        //    if (predicate. == "")
        //        return Select(SortField);

        //    //var lonte = _context.ORDR_NTE
        //    //            .Join(ONDBCntxt.LK_NTE_TYPE, orn => orn.NTE_TYPE_ID, lnt => lnt.NTE_TYPE_ID, (orn, lnt) => new { orn, lnt })
        //    //            .Join(ONDBCntxt.LK_USER, ornt => ornt.orn.CREAT_BY_USER_ID, lu => lu.USER_ID, (ornt, lus) => new { ornt.orn, ornt.lnt, lus })
        //    //            .Where(SearchCriteria)
        //    //            .OrderBy(SortField)
        //    //            .ToList();
        //    var lonte = (from orn in _context.OrdrNte
        //                 join lnt in _context.LkNteType on orn.NteTypeId equals lnt.NteTypeId
        //                 join lu in _context.LkUser on orn.CreatByUserId equals lu.UserId
        //                 where((predicate))  //orn.NteId != 0
        //                 orderby (SortField)  ///orderby(orn.OrdrId ==  0)
        //                 select orn).Distinct()
        //                 .ToList();
        //    List<OrdrNte> lont = new List<OrdrNte>();

        //    foreach (var onte in lonte)
        //    {
        //        OrdrNte ont = new OrdrNte();
        //        ont.NteId = onte.NteId;
        //        ont.NteTxt = onte.NteTxt;
        //        ont.NteType.NteTypeDes = onte.NteType.NteTypeDes;
        //        ont.RecStusId = onte.RecStusId;
        //        ont.CreatByUser.UserAdid = onte.CreatByUser.UserAdid;
        //        ont.OrdrId = onte.OrdrId;
        //        ont.CreatDt = onte.CreatDt;
        //        ont.CreatByUser.DsplNme = onte.CreatByUser.DsplNme;
        //        lont.Add(ont);
        //    }
        //    return lont;

        //}

        public List<OrderNoteView> FindForGivenOrderAndNoteType(int orderId, int noteTypeId, string sortExpression, int prntPrfID, int userID = 0)
        {
            bool b3rdCondition = false;
            orderId = orderId == null ? 0 : orderId;
            noteTypeId = noteTypeId == null ? 0 : noteTypeId;
            prntPrfID = prntPrfID == null ? 0 : prntPrfID;
            sortExpression = sortExpression == null ? "orn.NteId DESC" : sortExpression;

            #region Hide backupCode

            //var predicate = PredicateBuilder.True<OrdrNte>();
            //if (!(orderId == null || orderId == 0))
            //{
            //    predicate = predicate.And(orn => orn.OrdrId == orderId);
            //}
            //if (!(noteTypeId == null || noteTypeId == 0))
            //{
            //    predicate = predicate.And(orn => orn.NteTypeId == noteTypeId);
            //}
            //if (!(PrntPrfID == null || PrntPrfID == 0))
            //{
            //    if (PrntPrfID < 5 || PrntPrfID > 7 || GetUserGroupAccess(userID, PrntPrfID) == 0)
            //    {
            //        predicate = predicate.And(orn => orn.NteTypeId != 12);
            //    }
            //}
            //if (orderId != 0)
            //    search += string.Format("orn.ORDR_ID == {0} AND ", orderId);

            //if (noteTypeId != 0)
            //    search += string.Format("orn.NTE_TYPE_ID == {0} AND ", noteTypeId);

            //if ((_WGId < 5) || (_WGId > 7) || (tDB.GetUserGroupAccess(_userID, _WGId) == 0))
            //    search += "orn.NTE_TYPE_ID != 12 AND ";
            //_context.OrdrNte
            //    .Join(_context.LkNteType, orn => orn.NteTypeId, lnt => lnt.NteTypeId, (orn, lnt) => new { orn, lnt })
            //    .Join(_context.LkUser, ornt => ornt.orn.CreatByUserId, lu => lu.UserId, (ornt, lus) => new { ornt.orn, ornt.lnt, lus })
            //    .Where(predicate)
            //    .OrderBy(sortExpression)
            //    .ToList();

            #endregion Hide backupCode

            if (!(prntPrfID == 0))
            {
                if (prntPrfID < 5 || prntPrfID > 7 || GetUserGroupAccess(userID, prntPrfID) == 0)
                {
                    b3rdCondition = true;
                }
            }
            List<OrderNoteView> lonte = null;
            List<OrderNoteView> lont = new List<OrderNoteView>();
            if ((orderId == 0) && (noteTypeId == 0) && (prntPrfID == 0))
            {
                lonte = Select(sortExpression);
                foreach (var onte in lonte)
                {
                    OrderNoteView ont = new OrderNoteView();
                    ont.NteId = onte.NteId;
                    ont.NteTypeId = onte.NteTypeId;
                    ont.OrdrId = onte.OrdrId;
                    ont.CreatDt = onte.CreatDt;
                    ont.CreatByUserId = onte.CreatByUserId;
                    ont.RecStusId = onte.RecStusId;
                    ont.NteTxt = onte.NteTxt;
                    ont.ModfdByUserId = onte.ModfdByUserId;
                    ont.ModfdDt = onte.ModfdDt;
                    ont.CreatByUser = onte.CreatByUser;
                    ont.NteType = onte.NteType;

                    lont.Add(ont);
                }
            }
            else
            {
                var Mylonte = (from orn in _context.OrdrNte
                               join lnt in _context.LkNteType on orn.NteTypeId equals lnt.NteTypeId
                               join lu in _context.LkUser on orn.CreatByUserId equals lu.UserId
                               where (orn.OrdrId == (orderId == 0 ? orn.OrdrId : orderId) &&              //orn.ORDR_ID == {variable}
                                      orn.NteTypeId == (noteTypeId == (byte)0 ? orn.NteTypeId : noteTypeId) &&  //orn.NTE_TYPE_ID == {variable}
                                      orn.NteTypeId != (b3rdCondition ? (byte)12 : (byte)0))                         //orn.NTE_TYPE_ID != 12
                               orderby (sortExpression)
                               select new { orn, lnt, lu }).Distinct()
                             .ToList();
                foreach (var onte in Mylonte)
                {
                    OrderNoteView ont = new OrderNoteView();
                    ont.NteId = onte.orn.NteId;
                    ont.NteTypeId = onte.orn.NteTypeId;
                    ont.OrdrId = onte.orn.OrdrId;
                    ont.CreatDt = onte.orn.CreatDt;
                    ont.CreatByUserId = onte.orn.CreatByUserId;
                    ont.RecStusId = onte.orn.RecStusId;
                    ont.NteTxt = onte.orn.NteTxt;
                    ont.ModfdByUserId = onte.orn.ModfdByUserId;
                    ont.ModfdDt = onte.orn.ModfdDt;
                    ont.CreatByUser = onte.lu.UserAdid;
                    ont.NteType = onte.lnt.NteTypeDes;

                    lont.Add(ont);
                }
            }
            //List<OrdrNte> lont = new List<OrdrNte>();

            return lont;
        }

        private int GetUserGroupAccess(int userID, int PrntPrfID)
        {
            var userAccsCnt = (from c in _context.MapUsrPrf where c.UserId == userID && c.UsrPrfId == PrntPrfID && c.RecStusId == 1 select c).Count();
            return userAccsCnt;
        }

        //public void CreateEventHistory(int eventId, byte actnId, DateTime? start, DateTime? end, int userId)
        //{
        //    var query = _evntRuleRepo
        //        .Find(a => a.ActnId == actnId &&
        //            EF.Functions.Like(a.Actn.ActnDes, "Action %"))
        //        .Select(a => a.Actn)
        //        .Distinct();

        //    int[] toIncludeTime = { 1, 5, 17 }; // Include EVENT_STRT_TMST and EVENT_END_TMST

        //    if (query.Count() > 0)
        //    {
        //        string[] ids = query
        //            .Select(a => a.ActnDes.Replace("Action ", ""))
        //            .SingleOrDefault()
        //            .Split(",");

        //        foreach (string id in ids)
        //        {
        //            EventHist entity = new EventHist
        //            {
        //                EventId = eventId,
        //                ActnId = Convert.ToByte(id),
        //                EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? start : null),
        //                EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? end : null),
        //                CreatByUserId = userId,
        //                CreatDt = DateTime.Now,
        //                ModfdDt = DateTime.Now
        //            };

        //            Create(entity);
        //        }
        //    }
        //    else
        //    {
        //        EventHist entity = new EventHist
        //        {
        //            EventId = eventId,
        //            ActnId = actnId,
        //            EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(actnId)) ? start : null),
        //            EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(actnId)) ? end : null),
        //            CreatByUserId = userId,
        //            CreatDt = DateTime.Now,
        //            ModfdDt = DateTime.Now
        //        };

        //        Create(entity);
        //    }
        //}

        //public int CreateEventHistoryReturnHist(EventHist obj)
        //{
        //    int hist = 0;
        //    var query = _evntRuleRepo
        //        .Find(a => a.ActnId == obj.ActnId &&
        //            EF.Functions.Like(a.Actn.ActnDes, "Action %"))
        //        .Select(a => a.Actn)
        //        .Distinct();

        //    int[] toIncludeTime = { 1, 5, 17 }; // Include EVENT_STRT_TMST and EVENT_END_TMST
        //    int[] toIncludeComments = { 1, 3, 5, 9, 11, 16, 17, 19, 20, 23, 29, 44 }; // Include Comments

        //    if (query.Count() > 0)
        //    {
        //        string[] ids = query
        //            .Select(a => a.ActnDes.Replace("Action ", ""))
        //            .SingleOrDefault()
        //            .Split(",");

        //        foreach (string id in ids)
        //        {
        //            EventHist entity = new EventHist
        //            {
        //                EventId = obj.EventId,
        //                ActnId = Convert.ToByte(id),
        //                EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? obj.EventStrtTmst : null),
        //                EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? obj.EventEndTmst : null),
        //                CmntTxt = (toIncludeComments.Contains(Convert.ToInt32(id)) ? obj.CmntTxt : null),
        //                PreCfgCmpltCd = obj.PreCfgCmpltCd,
        //                CreatByUserId = obj.CreatByUserId,
        //                CreatDt = DateTime.Now,
        //                ModfdDt = DateTime.Now
        //            };

        //            hist = CreateHist(entity);
        //        }
        //    }
        //    else
        //    {
        //        EventHist entity = new EventHist
        //        {
        //            EventId = obj.EventId,
        //            ActnId = obj.ActnId,
        //            EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? obj.EventStrtTmst : null),
        //            EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? obj.EventEndTmst : null),
        //            CmntTxt = (toIncludeComments.Contains(Convert.ToInt32(obj.ActnId)) ? obj.CmntTxt : null),
        //            PreCfgCmpltCd = obj.PreCfgCmpltCd,
        //            CreatByUserId = obj.CreatByUserId,
        //            CreatDt = DateTime.Now,
        //            ModfdDt = DateTime.Now
        //        };

        //        //obj.EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? obj.EventStrtTmst : null);
        //        //obj.EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? obj.EventEndTmst : null);
        //        //obj.CmntTxt = (toIncludeComments.Contains(Convert.ToInt32(obj.ActnId)) ? obj.CmntTxt : null);
        //        //hist=Create(obj)
        //        hist = CreateHist(entity);
        //    }
        //    return hist;
        //}
        //public int CreateEventHistory(EventHist obj)
        //{
        //    var query = _evntRuleRepo
        //        .Find(a => a.ActnId == obj.ActnId &&
        //            EF.Functions.Like(a.Actn.ActnDes, "Action %"))
        //        .Select(a => a.Actn)
        //        .Distinct();

        //    int[] toIncludeTime = { 1, 5, 17 }; // Include EVENT_STRT_TMST and EVENT_END_TMST
        //    int[] toIncludeComments = { 1, 3, 5, 9, 11, 16, 17, 19, 20, 23, 29, 44 }; // Include Comments

        //    EventHist hstry = new EventHist();

        //    if (query.Count() > 0)
        //    {
        //        string[] ids = query
        //            .Select(a => a.ActnDes.Replace("Action ", ""))
        //            .SingleOrDefault()
        //            .Split(",");

        //        foreach (string id in ids)
        //        {
        //            EventHist entity = new EventHist
        //            {
        //                EventId = obj.EventId,
        //                ActnId = Convert.ToByte(id),
        //                EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? obj.EventStrtTmst : null),
        //                EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? obj.EventEndTmst : null),
        //                CmntTxt = (toIncludeComments.Contains(Convert.ToInt32(id)) ? obj.CmntTxt : null),
        //                PreCfgCmpltCd = string.IsNullOrWhiteSpace(obj.PreCfgCmpltCd) ? "N" : obj.PreCfgCmpltCd,
        //                CreatByUserId = obj.CreatByUserId,
        //                CreatDt = DateTime.Now,
        //                ModfdDt = DateTime.Now
        //            };

        //            hstry = Create(entity);
        //        }
        //    }
        //    else
        //    {
        //        obj.EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? obj.EventStrtTmst : null);
        //        obj.EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? obj.EventEndTmst : null);
        //        obj.CmntTxt = (toIncludeComments.Contains(Convert.ToInt32(obj.ActnId)) ? obj.CmntTxt : null);
        //        obj.PreCfgCmpltCd = string.IsNullOrWhiteSpace(obj.PreCfgCmpltCd) ? "N" : obj.PreCfgCmpltCd;
        //        obj.CreatDt = DateTime.Now;
        //        obj.ModfdDt = DateTime.Now;

        //        hstry = Create(obj);
        //    }

        //    return hstry.EventHistId;
        //}
    }
}