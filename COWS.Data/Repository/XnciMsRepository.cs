﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class XnciMsRepository : IXnciMsRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public XnciMsRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkXnciMs Create(LkXnciMs entity)
        {
            int maxId = _context.LkXnciMs.Max(i => i.MsId);
            entity.MsId = (byte)++maxId;
            _context.LkXnciMs.Add(entity);

            SaveAll();

            return GetById(entity.MsId);
        }

        public void Delete(int id)
        {
            LkXnciMs st = GetById(id);
            _context.LkXnciMs.Remove(st);

            SaveAll();
        }

        public IQueryable<LkXnciMs> Find(Expression<Func<LkXnciMs, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkXnciMs, out List<LkXnciMs> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkXnciMs> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkXnciMs, out List<LkXnciMs> list))
            {
                list = _context.LkXnciMs
                            .OrderBy(i => i.MsId)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkXnciMs, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkXnciMs GetById(int id)
        {
            return _context.LkXnciMs
                            .SingleOrDefault(i => i.MsId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkXnciMs);
            return _context.SaveChanges();
        }

        public void Update(int id, LkXnciMs entity)
        {
            LkXnciMs ms = GetById(id);
            ms.MsId = entity.MsId;
            ms.MsDes = entity.MsDes;
            ms.MsTaskId = entity.MsTaskId;
            SaveAll();
        }
    }
}