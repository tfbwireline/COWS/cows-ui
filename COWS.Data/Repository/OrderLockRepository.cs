﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace COWS.Data.Repository
{
    public class OrderLockRepository : IOrderLockRepository
    {
        private readonly COWSAdminDBContext _context;

        public OrderLockRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public OrdrRecLock CheckLock(int id)
        {
            return GetById(id);
        }

        public IQueryable<OrdrRecLock> Find(Expression<Func<OrdrRecLock, bool>> predicate)
        {
            return _context.OrdrRecLock
                            .Include(i => i.Ordr)
                            .Include(i => i.LockByUser)
                            .Where(predicate);
        }

        public OrdrRecLock GetById(int id)
        {
            return _context.OrdrRecLock
                            .Include(i => i.Ordr)
                            .Include(i => i.LockByUser)
                            .SingleOrDefault(i => i.OrdrId == id);
        }

        public int Lock(OrdrRecLock entity)
        {
            OrdrRecLock ordr = GetById(entity.OrdrId);
            if (ordr == null)
            {
                _context.OrdrRecLock.Add(entity);
                SaveAll();
            }

            return entity.OrdrId;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Unlock(int id, int userId)
        {
            OrdrRecLock ordr = _context.OrdrRecLock.SingleOrDefault(i => i.OrdrId == id && i.LockByUserId == userId);
            if (ordr != null)
            {
                _context.OrdrRecLock.Remove(ordr);
                SaveAll();
            }
        }

        public async Task<int> LockUnlockOrdersEvents(int orderId, int eventId, int userId, bool isOrder, bool unlock, int isLocked)
        {
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.lockUnlockOrdersEvents {orderId}, {eventId}, {userId}, {isOrder}, {unlock}, {isLocked}";
                    var result = await command.ExecuteScalarAsync();
                    return Convert.ToInt32(result);
                }
            }
        }
    }
}