﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Data.Repository
{
    public class EventCpeDevRepository : IEventCpeDevRepository
    {
        private readonly COWSAdminDBContext _context;

        public EventCpeDevRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<GetEventCpeDev> GetEventCpeDevByEventId(int eventId)
        {
            List<GetEventCpeDev> MDSCPEDev = new List<GetEventCpeDev>();

            var cpedevtbl = (from cd in _context.EventCpeDev
                             join fo in _context.FsaOrdr on cd.CpeOrdrNbr equals fo.Ftn into focd
                             from fc in focd.DefaultIfEmpty()
                             join od in _context.Ordr on fc.OrdrId equals od.OrdrId into odfo
                             from odf in odfo.DefaultIfEmpty()
                             where cd.EventId == eventId
                                && cd.RecStusId == ((byte)1)
                                && ((fc == null) || (fc.OrdrSubTypeCd == "IN"))
                                && ((odf == null) || (odf.OrdrStusId != ((byte)3)))
                             select new { cd, OrdrId = ((fc == null) ? -1 : fc.OrdrId) });

            foreach (var cpddev in cpedevtbl)
            {
                GetEventCpeDev item = new GetEventCpeDev();
                item.CpeDevId = cpddev.cd.EventCpeDevId;
                item.CpeOrdrId = cpddev.cd.CpeOrdrId;
                item.CpeM5OrdrNbr = cpddev.cd.CpeOrdrNbr;
                item.OrdrId = cpddev.OrdrId;
                item.EventId = eventId;
                item.DeviceId = cpddev.cd.DeviceId;
                item.AssocH6 = cpddev.cd.AssocH6;
                item.Ccd = (cpddev.cd.Ccd == null) ? string.Empty : ((DateTime)cpddev.cd.Ccd).ToString("MM/dd/yyyy");
                item.CmpntStus = cpddev.cd.OrdrStus;
                item.RcptStus = cpddev.cd.RcptStus;
                item.OdieDevNme = cpddev.cd.OdieDevNme;
                item.CreatDt = cpddev.cd.CreatDt.ToString();
                MDSCPEDev.Add(item);
            }

            return MDSCPEDev;
        }

        public void AddCPEDevTbl(ref List<GetEventCpeDev> cpedevinfo, int eventId)
        {
            //DeleteAllCPEDev(Obj.EventID);
            InactiveAllCPEDev(eventId);

            foreach (GetEventCpeDev item in cpedevinfo)
            {
                EventCpeDev mecd = new EventCpeDev();

                mecd.EventId = eventId;
                mecd.DeviceId = item.DeviceId;
                if (item.OdieDevNme.Equals("0"))
                    mecd.OdieCd = true;
                mecd.OdieDevNme = item.OdieDevNme;
                mecd.CreatDt = DateTime.Now;
                mecd.RecStusId = (byte)1;

                //if (Obj.CPEOrdrCd)
                //{
                mecd.CpeOrdrId = item.CpeOrdrId;
                mecd.CpeOrdrNbr = item.CpeM5OrdrNbr;
                mecd.AssocH6 = item.AssocH6;
                mecd.Ccd = DateTime.Parse(item.Ccd);
                mecd.OrdrStus = item.CmpntStus;
                mecd.RcptStus = item.RcptStus;
                //}
                _context.EventCpeDev.Add(mecd);
            }

            _context.SaveChanges();
        }

        private void InactiveAllCPEDev(int eventId)
        {
            _context.EventCpeDev.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1)).ToList().ForEach(b => { b.RecStusId = ((byte)0); b.ModfdDt = DateTime.Now; });
            _context.SaveChanges();
        }

        public void Create(IEnumerable<EventCpeDev> entity)
        {
            _context.EventCpeDev
                .AddRange(entity.Select(a =>
                {
                    a.EventCpeDevId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void SetInactive(int eventId)
        {
            _context.EventCpeDev
                .Where(a => a.EventId == eventId && a.RecStusId == (byte)1)
                .ToList()
                .ForEach(b =>
                {
                    b.RecStusId = (byte)0;
                    b.ModfdDt = DateTime.Now;
                });
        }
    }
}