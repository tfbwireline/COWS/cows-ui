﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UserCsgLevelRepository : IUserCsgLevelRepository
    {
        private readonly COWSAdminDBContext _context;

        public UserCsgLevelRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public UserCsgLvl Create(UserCsgLvl entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<UserCsgLvl> Find(Expression<Func<UserCsgLvl, bool>> predicate)
        {
            return _context.UserCsgLvl
                            .Include(i => i.User)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<UserCsgLvl> GetAll()
        {
            throw new NotImplementedException();
        }

        public UserCsgLvl GetById(int id)
        {
            return _context.UserCsgLvl
                .SingleOrDefault(i => i.CsgLvlId == id);
        }

        public int SaveAll()
        {
            throw new NotImplementedException();
        }

        public void Update(int id, UserCsgLvl entity)
        {
            throw new NotImplementedException();
        }
    }
}