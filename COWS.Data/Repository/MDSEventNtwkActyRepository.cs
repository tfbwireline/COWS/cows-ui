﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSEventNtwkActyRepository : IMDSEventNtwkActyRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public MDSEventNtwkActyRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public IEnumerable<MdsEventNtwkActy> GetMDSEventNtwkTrptByEventId(int eventId)
        {
            List<MdsEventNtwkActy> MDSNtwkActy = new List<MdsEventNtwkActy>();

            var ntwkActy = (from nActy in _context.MdsEventNtwkActy
                            where nActy.EventId == eventId
                            select nActy);

            foreach (var ntitem in ntwkActy)
            {
                MdsEventNtwkActy item = new MdsEventNtwkActy();
                item.Id = ntitem.Id;
                item.NtwkActyTypeId = ntitem.NtwkActyTypeId;
                item.EventId = eventId;
                item.NtwkActyType.NtwkActyTypeDes = ntitem.NtwkActyType.NtwkActyTypeDes;

                MDSNtwkActy.Add(item);
            }
            return MDSNtwkActy;
        }

        public void AddNtwkActy(ref List<MdsEventNtwkActy> data, int eventId)
        {
            DeleteAllNtwkActy(eventId);

            foreach (MdsEventNtwkActy item in data)
            {
                MdsEventNtwkActy ment = new MdsEventNtwkActy();
                
                ment.EventId = eventId;
                ment.NtwkActyTypeId = item.NtwkActyTypeId;
                
                _context.MdsEventNtwkActy.Add(ment);
                _context.SaveChanges();
            }
        }

        private void DeleteAllNtwkActy(int eventId)
        {
            var menaitems = (from mena in _context.MdsEventNtwkActy
                             where mena.EventId == eventId
                             select mena);
            foreach (MdsEventNtwkActy item in menaitems)
            {
                _context.MdsEventNtwkActy.Remove(item);
            }
            _context.SaveChanges();
        }

        public IQueryable<MdsEventNtwkActy> Find(Expression<Func<MdsEventNtwkActy, bool>> predicate)
        {
            return _context.MdsEventNtwkActy.Where(predicate);
        }

        public IEnumerable<MdsEventNtwkActy> GetAll()
        {
            throw new NotImplementedException();
        }

        public MdsEventNtwkActy GetById(int id)
        {
            throw new NotImplementedException();
        }

        public MdsEventNtwkActy Create(MdsEventNtwkActy entity)
        {
            throw new NotImplementedException();
        }

        public void Create(List<MdsEventNtwkActy> entity)
        {
            _context.MdsEventNtwkActy
                .AddRange(entity.Select(a =>
                {
                    a.Id = 0;
                    a.CreatDt = DateTime.Now;
                    return a;
                }));
        }

        public void Update(int id, MdsEventNtwkActy entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(List<MdsEventNtwkActy> entity)
        {
            _context.MdsEventNtwkActy.RemoveRange(entity);
            //SaveAll();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public IEnumerable<MdsEventNtwkActy> GetMDSEventNtwkActyByEventId(int eventId)
        {
            throw new NotImplementedException();
        }
    }
}