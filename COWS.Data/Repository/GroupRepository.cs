﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class GroupRepository : IGroupRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public GroupRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkGrp> Find(Expression<Func<LkGrp, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkGrp, out List<LkGrp> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkGrp> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkGrp, out List<LkGrp> list))
            {
                list = _context.LkGrp
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkGrp, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkGrp GetById(int id)
        {
            return _context.LkGrp
                .SingleOrDefault(a => a.GrpId == id);
        }

        public LkGrp Create(LkGrp entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkGrp entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkGrp);
            return _context.SaveChanges();
        }
    }
}