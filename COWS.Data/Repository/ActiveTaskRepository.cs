﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class ActiveTaskRepository : IActiveTaskRepository
    {
        private readonly COWSAdminDBContext _context;

        public ActiveTaskRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<ActTask> Find(Expression<Func<ActTask, bool>> predicate)
        {
            return _context.ActTask
                .Where(predicate);
        }

        public IEnumerable<ActTask> GetAll()
        {
            return _context.ActTask.ToList();
        }

        public ActTask GetById(int id)
        {
            return Find(a => a.ActTaskId == id).SingleOrDefault();
        }

        public ActTask Create(ActTask entity)
        {
            _context.ActTask.Add(entity);
            SaveAll();

            return GetById(entity.ActTaskId);
        }

        public void Update(int id, ActTask entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public int CompleteActiveTask(StateMachine sm)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.CompleteActiveTask @OrderID, @TaskID, @TaskStatus, @Comments, @UserID, @NteType";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@OrderID", SqlDbType = SqlDbType.Int, Value = sm.OrderId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@TaskID", SqlDbType = SqlDbType.Int, Value = sm.TaskId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@TaskStatus", SqlDbType = SqlDbType.SmallInt, Value = 2 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Comments", SqlDbType = SqlDbType.VarChar, Value = sm.Comments });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = sm.UserId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@NteType", SqlDbType = SqlDbType.TinyInt, Value = 0 });

                _context.Database.OpenConnection();
                int count = command.ExecuteNonQuery();
                _context.Database.CloseConnection();

                return count;
            }
        }

        public int LoadRtsTask(int orderId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.insertManualRTSTask @OrderID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@OrderID", SqlDbType = SqlDbType.Int, Value = orderId });

                _context.Database.OpenConnection();
                int count = command.ExecuteNonQuery();
                _context.Database.CloseConnection();

                return count;
            }
        }

        public int MoveToGom(int orderId, int userId)
        {
            var task = Find(a => a.OrdrId == orderId &&
                a.TaskId != 211 &&
                a.TaskId.ToString().StartsWith("2") &&
                a.StusId == 0)
                .SingleOrDefault();

            if (task != null)
            {
                task.StusId = 1;
                task.ModfdDt = DateTime.Now;
            }

            var nccoTask = Find(a => a.OrdrId == orderId &&
                a.TaskId == 111)
                .SingleOrDefault();

            if (nccoTask != null)
            {
                task.StusId = 0;
                task.ModfdDt = DateTime.Now;
            }
            else
            {
                ActTask actTask = new ActTask() {
                    OrdrId = orderId,
                    TaskId = 111,
                    StusId = 0,
                    WgProfId = 0,
                    CreatDt = DateTime.Now
                };

                _context.ActTask.Add(actTask);
            }

            OrdrNte note = new OrdrNte()
            {
                OrdrId = orderId,
                NteTypeId = 9,
                NteTxt = "Moving order from xNCI to GOM Workgroup",
                CreatByUserId = userId,
                CreatDt = DateTime.Now,
                RecStusId = 1
            };

            _context.OrdrNte.Add(note);

            return SaveAll();
        }

        public int ClearAcrTask(int orderId, int taskId, int userId)
        {
            var acrTask = Find(a => a.OrdrId == orderId && a.TaskId == taskId)
            .Include(a => a.Task)
            .SingleOrDefault();

            var sNotes = $"Completing { acrTask.Task.TaskNme } task";

            // Add notes
            byte iNteTypeID = 0;
            LkNteType nteType = _context.LkNteType.Where(x => x.NteTypeDes == "xNCI").FirstOrDefault();
            if (nteType != null)
            {
                iNteTypeID = Convert.ToByte(nteType.NteTypeId);
            }

            if (iNteTypeID != 0)
            {
                OrdrNte on = new OrdrNte();
                on.OrdrId = orderId;
                on.NteTypeId = iNteTypeID;
                on.NteTxt = sNotes;
                on.CreatByUserId = userId;
                on.RecStusId = 1;
                on.CreatDt = DateTime.Now;

                _context.OrdrNte.Add(on);
                _context.SaveChanges();
            }


            // CompleteNCIACRTasks
            if (acrTask != null)
            {
                acrTask.StusId = 2;
                acrTask.ModfdDt = DateTime.Now;

                _context.ActTask.Update(acrTask);
                _context.SaveChanges();

                return 1;
            }
            return 0;
        }

        public bool CheckIfOrderIsBAR(int orderId)
        {
            var actTask = _context.ActTask.Where(x => x.OrdrId == orderId && x.StusId == 0 && x.TaskId == 1000).OrderBy(x => x.TaskId).FirstOrDefault();
            if(actTask != null)
            {
                return actTask.TaskId == 1000;
            }
            return false;       
        }
    }
}