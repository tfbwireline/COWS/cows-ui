﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using System.Globalization;

namespace COWS.Data.Repository
{
    public class MdsEventRepository : IMdsEventRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;
        private readonly IConfiguration _configuration;
        private readonly IMDSEventNtwkCustRepository _ntwkCustRepo;
        private readonly IMDSEventNtwkTrptRepository _ntwkTrptRepo;
        private readonly IMDSEventNtwkActyRepository _ntwkActyRepo;
        private readonly IEventCpeDevRepository _eventCpeDevRepo;
        private readonly IMdsEventOdieDevRepository _mdsEventOdieDevRepo;
        private readonly IEventDeviceCompletionRepository _eventDeviceCompletionRepo;
        private readonly IEventDevSrvcMgmtRepository _eventDevSrvcMgmtRepo;
        private readonly IMDSEventSiteSrvcRepository _mdsEventSiteSrvcRepo;
        private readonly IMDSEventDslSbicCustTrptRepository _mdsEventDslSbicCustTrptRepo;
        private readonly IMDSEventSlnkWiredTrptRepository _mdsEventSlnkWiredTrptRepo;
        private readonly IMDSEventWrlsTrptRepository _mdsEventWrlsTrptRepo;
        private readonly IMDSEventPortBndwdRepository _mdsEventPortBndwdRepo;
        private readonly IEventDiscoDevRepository _eventDiscoDevRepo;
        private readonly ISearchRepository _searchRepo;
        private readonly IUserRepository _userRepo;
        private IMemoryCache _cache;

        public MdsEventRepository(COWSAdminDBContext context,
            ICommonRepository common,
            IConfiguration configuration,
            IMDSEventNtwkCustRepository ntwkCustRepo,
            IMDSEventNtwkTrptRepository ntwkTrptRepo,
            IMDSEventNtwkActyRepository ntwkActyRepo,
            IEventCpeDevRepository eventCpeDevRepo,
            IMdsEventOdieDevRepository mdsEventOdieDevRepo,
            IEventDeviceCompletionRepository eventDeviceCompletionRepo,
            IEventDevSrvcMgmtRepository eventDevSrvcMgmtRepo,
            IMDSEventSiteSrvcRepository mdsEventSiteSrvcRepo,
            IMDSEventDslSbicCustTrptRepository mdsEventDslSbicCustTrptRepo,
            IMDSEventSlnkWiredTrptRepository mdsEventSlnkWiredTrptRepo,
            IMDSEventWrlsTrptRepository mdsEventWrlsTrptRepo,
            IMDSEventPortBndwdRepository mdsEventPortBndwdRepo,
            IEventDiscoDevRepository eventDiscoDevRepo,
            ISearchRepository searchRepo,
            IUserRepository userRepo, IMemoryCache cache)
        {
            _context = context;
            _common = common;
            _configuration = configuration;
            _ntwkCustRepo = ntwkCustRepo;
            _ntwkTrptRepo = ntwkTrptRepo;
            _ntwkActyRepo = ntwkActyRepo;
            _eventCpeDevRepo = eventCpeDevRepo;
            _mdsEventOdieDevRepo = mdsEventOdieDevRepo;
            _eventDeviceCompletionRepo = eventDeviceCompletionRepo;
            _eventDevSrvcMgmtRepo = eventDevSrvcMgmtRepo;
            _mdsEventSiteSrvcRepo = mdsEventSiteSrvcRepo;
            _mdsEventDslSbicCustTrptRepo = mdsEventDslSbicCustTrptRepo;
            _mdsEventSlnkWiredTrptRepo = mdsEventSlnkWiredTrptRepo;
            _mdsEventWrlsTrptRepo = mdsEventWrlsTrptRepo;
            _mdsEventPortBndwdRepo = mdsEventPortBndwdRepo;
            _eventDiscoDevRepo = eventDiscoDevRepo;
            _searchRepo = searchRepo;
            _userRepo = userRepo;
            _cache = cache;
        }

        public IQueryable<MdsEvent> Find(Expression<Func<MdsEvent, bool>> predicate)
        {
            IQueryable<MdsEvent> mds = _context.MdsEvent
                .Include(a => a.EventStus)
                .Include(a => a.WrkflwStus)
                .Include(a => a.CreatByUser)
                .Where(predicate);

            return mds;
        }

        public IEnumerable<MdsEvent> GetAll()
        {
            return _context.MdsEvent
                .ToList();
        }

        public MdsEvent GetById(int id)
        {
            return GetById(id, null);
        }


        public MdsEvent GetById(int id, string adid, bool includeSecuredData = true)
        {
            MdsEvent mds = _context.MdsEvent
                // FOR NETWORK
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventNtwkCust)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventNtwkTrpt)
                .Include(a => a.Event)
                    .ThenInclude(a => a.NidActy)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MplsEventActyType)
                        .ThenInclude(a => a.MplsActyType)
                // FOR DISCONNECT
                .Include(a => a.Event)
                    .ThenInclude(a => a.EventDiscoDev)
                        .ThenInclude(a => a.DevModel)
                .Include(a => a.Event)
                    .ThenInclude(a => a.EventDiscoDev)
                        .ThenInclude(a => a.Manf)
                // FOR MAC
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventMacActy)
                        .ThenInclude(a => a.MdsMacActy)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventOdieDev)
                        .ThenInclude(a => a.DevModel)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventOdieDev)
                        .ThenInclude(a => a.Manf)
                .Include(a => a.Event)
                    .ThenInclude(a => a.EventDevCmplt)
                .Include(a => a.Event)
                    .ThenInclude(a => a.EventCpeDev)
                .Include(a => a.Event)
                    .ThenInclude(a => a.EventDevSrvcMgmt)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventSiteSrvc)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventDslSbicCustTrpt)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventSlnkWiredTrpt)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventWrlsTrpt)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventPortBndwd)
                .Include(a => a.Event)
                    .ThenInclude(a => a.MdsEventNtwkActy)
                        .ThenInclude(a => a.NtwkActyType)
                .Include(a => a.Event)
                .Include(a => a.MdsActyType)
                .Include(a => a.EsclReas)
                .Include(a => a.EventStus)
                .Include(a => a.WrkflwStus)
                .Include(a => a.CreatByUser)
                .Include(a => a.ReqorUser)
                .SingleOrDefault(a => a.EventId == id);

            if (mds.Event.CsgLvlId > 0 && includeSecuredData)
            {
                GetEventDecryptValues sdata = _common.GetEventDecryptValues(mds.EventId, "MDS").FirstOrDefault();
                if (sdata != null)
                {
                    mds.CustNme = sdata.CUST_NME;
                    mds.InstlSitePocNme = sdata.CUST_CNTCT_NME;
                    mds.InstlSitePocPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                    mds.InstlSitePocCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;

                    mds.SrvcAssrnPocNme = sdata.SRVC_ASSRN_POC_NME;
                    mds.SrvcAssrnPocPhnNbr = sdata.SRVC_ASSRN_POC_PHN_NBR;
                    mds.SrvcAssrnPocCellPhnNbr = sdata.SRVC_ASSRN_POC_CELL_PHN_NBR;

                    mds.StreetAdr = sdata.STREET_ADR_1;
                    mds.FlrBldgNme = sdata.FLR_ID;
                    mds.CtyNme = sdata.CTY_NME;
                    mds.SttPrvnNme = sdata.STT_PRVN_NME;
                    mds.CtryRgnNme = sdata.CTRY_RGN_NME;
                    mds.ZipCd = sdata.ZIP_PSTL_CD;

                    mds.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                    mds.CustAcctTeamPdlNme = sdata.CUST_EMAIL_ADR;
                    mds.NtwkCustNme = sdata.FRST_NME;

                    mds.AltShipStreetAdr = sdata.STREET_ADR_2;
                    mds.AltShipFlrBldgNme = sdata.BLDG_NME;
                    mds.AltShipCtyNme = sdata.RM_NBR;
                    mds.AltShipSttPrvnNme = sdata.STT_CD;
                    mds.AltShipCtryRgnNme = sdata.CTRY_CD;
                    mds.AltShipZipCd = sdata.STREET_ADR_3;
                    mds.AltShipPocNme = sdata.EVENT_DES;
                    mds.AltShipPocPhnNbr = sdata.LST_NME;
                    mds.AltShipPocEmail = sdata.NTE_TXT;
                }

                for (int i = 0; i < mds.Event.MdsEventNtwkCust.Count; i++)
                {
                    var securedNtwkCust = _context.CustScrdData
                        .Where(a => a.ScrdObjId == mds.Event.MdsEventNtwkCust.ElementAt(i).MdsEventNtwkCustId && a.ScrdObjTypeId == (byte)SecuredObjectType.MDS_EVENT_NTWK_CUST)
                        .FirstOrDefault();
                    if (securedNtwkCust != null)
                    {
                        mds.Event.MdsEventNtwkCust.ElementAt(i).InstlSitePocNme = _common.GetDecryptValue(securedNtwkCust.CustCntctNme);
                        mds.Event.MdsEventNtwkCust.ElementAt(i).InstlSitePocEmail = _common.GetDecryptValue(securedNtwkCust.CustEmailAdr);
                        mds.Event.MdsEventNtwkCust.ElementAt(i).InstlSitePocPhn = _common.GetDecryptValue(securedNtwkCust.CustCntctPhnNbr);
                    }
                }

                for (int i = 0; i < mds.Event.MdsEventNtwkTrpt.Count; i++)
                {
                    var securedNtwkTrpt = _context.CustScrdData
                        .Where(a => a.ScrdObjId == mds.Event.MdsEventNtwkTrpt.ElementAt(i).MdsEventNtwkTrptId && a.ScrdObjTypeId == (byte)SecuredObjectType.MDS_EVENT_NTWK_TRPT)
                        .FirstOrDefault();
                    if (securedNtwkTrpt != null)
                    {
                        mds.Event.MdsEventNtwkTrpt.ElementAt(i).LocCity = _common.GetDecryptValue(securedNtwkTrpt.CtyNme);
                        mds.Event.MdsEventNtwkTrpt.ElementAt(i).LocSttPrvn = _common.GetDecryptValue(securedNtwkTrpt.SttPrvnNme);
                        mds.Event.MdsEventNtwkTrpt.ElementAt(i).LocCtry = _common.GetDecryptValue(securedNtwkTrpt.CtryRgnNme);
                    }
                }
            }

            if (adid != null)
            {
                if (mds.Event.CsgLvlId > 0)
                {
                    // LogWebActivity
                    var UserCsg = _common.GetCSGLevelAdId(adid);
                    _common.LogWebActivity(((byte)WebActyType.EventDetails), mds.Event.EventId.ToString(), adid, (byte)UserCsg, (byte)mds.Event.CsgLvlId);
                }
            }

            return mds;
        }

        public MdsEvent Create(MdsEvent entity)
        {
            entity.OldEventStusId = (int)EventStatus.Visible;

            if (entity.Event.EventDiscoDev != null)
            {
                entity.Event.EventDiscoDev = entity.Event.EventDiscoDev
                    .Select(a =>
                    {
                        a.CreatDt = DateTime.Now;
                        // Added by Sarah Sandoval (20210223) - mimic the OLD COWS
                        a.OptInCktCd = string.IsNullOrWhiteSpace(a.OptInCktCd) ? null : a.OptInCktCd;
                        a.ReadyBeginCd = string.IsNullOrWhiteSpace(a.ReadyBeginCd) ? null : a.ReadyBeginCd;
                        a.OptInHCd = string.IsNullOrWhiteSpace(a.OptInHCd) ? null : a.OptInHCd;
                        return a;
                    })
                    .ToList();
            }

            // Added by Sarah Sandoval (20210223) - mimic the OLD COWS
            if (entity.Event.MdsEventSlnkWiredTrpt != null)
            {
                entity.Event.MdsEventSlnkWiredTrpt = entity.Event.MdsEventSlnkWiredTrpt
                    .Select(a =>
                    {
                        a.CreatDt = DateTime.Now;
                        a.OptInCktCd = string.IsNullOrWhiteSpace(a.OptInCktCd) ? null : a.OptInCktCd;
                        a.ReadyBeginCd = string.IsNullOrWhiteSpace(a.ReadyBeginCd) ? null : a.ReadyBeginCd;
                        a.OptInHCd = string.IsNullOrWhiteSpace(a.OptInHCd) ? null : a.OptInHCd;
                        return a;
                    })
                    .ToList();
            }

            _context.Event.Add(entity.Event);
            SaveAll();

            entity.EventId = entity.Event.EventId;

            // Handle secure data
            if (entity.Event.CsgLvlId > 0)
            {
                for (int i = 0; i < entity.Event.MdsEventNtwkCust.Count; i++)
                {
                    var ntwkCust = entity.Event.MdsEventNtwkCust.ElementAt(i);

                    CustScrdData securedNtwkCust = new CustScrdData();
                    securedNtwkCust.ScrdObjId = ntwkCust.MdsEventNtwkCustId;
                    securedNtwkCust.ScrdObjTypeId = (byte)SecuredObjectType.MDS_EVENT_NTWK_CUST;

                    securedNtwkCust.CustCntctNme = _common.GetEncryptValue(ntwkCust.InstlSitePocNme);
                    securedNtwkCust.CustEmailAdr = _common.GetEncryptValue(ntwkCust.InstlSitePocEmail);
                    securedNtwkCust.CustCntctPhnNbr = _common.GetEncryptValue(ntwkCust.InstlSitePocPhn);
                    securedNtwkCust.CreatDt = DateTime.Now;

                    ntwkCust.InstlSitePocNme = "";
                    ntwkCust.InstlSitePocEmail = "";
                    ntwkCust.InstlSitePocPhn = "";

                    _context.CustScrdData.Add(securedNtwkCust);
                    SaveAll();
                }

                for (int i = 0; i < entity.Event.MdsEventNtwkTrpt.Count; i++)
                {
                    var ntwkTrpt = entity.Event.MdsEventNtwkTrpt.ElementAt(i);

                    CustScrdData securedNtwkTrpt = new CustScrdData();
                    securedNtwkTrpt.ScrdObjId = ntwkTrpt.MdsEventNtwkTrptId;
                    securedNtwkTrpt.ScrdObjTypeId = (byte)SecuredObjectType.MDS_EVENT_NTWK_TRPT;

                    securedNtwkTrpt.CtyNme = _common.GetEncryptValue(ntwkTrpt.LocCity);
                    securedNtwkTrpt.SttPrvnNme = _common.GetEncryptValue(ntwkTrpt.LocSttPrvn);
                    securedNtwkTrpt.CtryRgnNme = _common.GetEncryptValue(ntwkTrpt.LocCtry);
                    securedNtwkTrpt.CreatDt = DateTime.Now;

                    ntwkTrpt.LocCity = "";
                    ntwkTrpt.LocSttPrvn = "";
                    ntwkTrpt.LocCtry = "";

                    _context.CustScrdData.Add(securedNtwkTrpt);
                    SaveAll();
                }


                SecuredData obj = new SecuredData(entity);
                List<GetEncryptValues> EncValues = _common.GetEventEncryptValuesMds(obj).ToList();

                CustScrdData sdata = new CustScrdData();
                sdata.ScrdObjId = entity.Event.EventId;
                sdata.ScrdObjTypeId = (byte)SecuredObjectType.MDS_EVENT;

                sdata.CustNme = EncValues[0].Item;
                sdata.CustCntctNme = EncValues[1].Item;
                sdata.CustCntctPhnNbr = EncValues[2].Item;
                sdata.CustCntctCellPhnNbr = EncValues[3].Item;
                sdata.SrvcAssrnPocNme = EncValues[4].Item;
                sdata.SrvcAssrnPocPhnNbr = EncValues[5].Item;
                sdata.SrvcAssrnPocCellPhnNbr = EncValues[6].Item;
                sdata.StreetAdr1 = EncValues[7].Item;
                sdata.FlrId = EncValues[8].Item;
                sdata.CtyNme = EncValues[9].Item;
                sdata.SttPrvnNme = EncValues[10].Item;
                sdata.CtryRgnNme = EncValues[11].Item;
                sdata.ZipPstlCd = EncValues[12].Item;
                sdata.StreetAdr2 = EncValues[13].Item;
                sdata.BldgNme = EncValues[14].Item;
                sdata.RmNbr = EncValues[15].Item;
                sdata.SttCd = EncValues[16].Item;
                sdata.CtryCd = EncValues[17].Item;
                sdata.StreetAdr3 = EncValues[18].Item;
                sdata.EventDes = EncValues[19].Item;
                sdata.LstNme = EncValues[20].Item;
                sdata.NteTxt = EncValues[21].Item;
                sdata.EventTitleTxt = EncValues[22].Item;
                sdata.CustEmailAdr = EncValues[23].Item;
                sdata.FrstNme = EncValues[24].Item;
                sdata.CreatDt = DateTime.Now;

                _context.CustScrdData.Add(sdata);

                entity.CustNme = null;
                entity.InstlSitePocNme = null;
                entity.InstlSitePocIntlPhnCd = null;
                entity.InstlSitePocCellPhnNbr = null;
                entity.SrvcAssrnPocNme = null;
                entity.SrvcAssrnPocPhnNbr = null;
                entity.SrvcAssrnPocCellPhnNbr = null;
                entity.StreetAdr = null;
                entity.FlrBldgNme = null;
                entity.CtyNme = null;
                entity.SttPrvnNme = null;
                entity.CtryRgnNme = null;
                entity.ZipCd = null;
                entity.AltShipStreetAdr = null;
                entity.AltShipFlrBldgNme = null;
                entity.AltShipCtyNme = null;
                entity.AltShipSttPrvnNme = null;
                entity.AltShipCtryRgnNme = null;
                entity.AltShipZipCd = null;
                entity.AltShipPocNme = null;
                entity.AltShipPocPhnNbr = null;
                entity.AltShipPocEmail = null;
                entity.EventTitleTxt = null;
                entity.CustAcctTeamPdlNme = null;
                entity.NtwkCustNme = null;
            }

            entity.MdrNumber = (entity.MdrNumber ?? string.Empty).Length > 25 ? entity.MdrNumber.Substring(0, 25) : entity.MdrNumber;
            _context.MdsEvent.Add(entity);
            SaveAll();

            // Added this block of code for Assign MNS PM which should be present for both Create/Update
            // Call MNS Reviewer Assignment Logic for Network-Only Events
            if ((!CheckMDSNtwkActy(ref entity, "hasMDS"))
                && entity.WrkflwStusId.Equals((byte)WorkflowStatus.Submit))
            {
                // Added condition by Sarah Sandoval [20210205]
                // Assigned PM is not needed for Network Intl Only type
                if (entity.Event.MdsEventNtwkActy.FirstOrDefault(i => i.NtwkActyTypeId.Equals(4)) == null)
                {
                    List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() {ParameterName = "@EVENT_ID", SqlDbType = SqlDbType.Int, Value = entity.EventId},
                    new SqlParameter() {ParameterName = "@WRKFLW_STUS_ID", SqlDbType = SqlDbType.TinyInt, Value = (byte)entity.WrkflwStusId}
                };

                    _context.Database.ExecuteSqlCommand("[web].[updtMNSRevwr] @EVENT_ID, @WRKFLW_STUS_ID", pc.ToArray());
                }
            }

            // km967761 - 08/05/2021 - commented out to use GetById instead
            //entity.CreatByUser = _context.LkUser.FirstOrDefault(i => i.UserId == entity.CreatByUserId);
            //entity.Event.MdsEventNtwkActy = _context.MdsEventNtwkActy
            //                                        .Include(i => i.NtwkActyType)
            //                                        .Where(i => i.EventId == entity.EventId)
            //                                        .ToList();
            
            return GetById(entity.EventId);
        }

        public void Update(int id, MdsEvent entity)
        {
            throw new NotImplementedException();
        }

        public MdsEvent Update(int id, MdsEvent entity, IEnumerable<MdsEventNtwkTrptView> networkTransport, MdsEvent mdsOld)
        {
            MdsEvent mds = mdsOld;

            // Handle secure data
            if (entity.Event.CsgLvlId > 0)
            {
                var old = _context.CustScrdData
                    .Where(a => a.ScrdObjTypeId == (byte)SecuredObjectType.MDS_EVENT && a.ScrdObjId == entity.EventId)
                    .SingleOrDefault();
                if (old != null)
                {
                    _context.CustScrdData.Remove(old);
                }

                SecuredData obj = new SecuredData(entity);
                List<GetEncryptValues> EncValues = _common.GetEventEncryptValuesMds(obj).ToList();

                CustScrdData sdata = new CustScrdData();
                sdata.ScrdObjId = mds.Event.EventId;
                sdata.ScrdObjTypeId = (byte)SecuredObjectType.MDS_EVENT;

                sdata.CustNme = EncValues[0].Item;
                sdata.CustCntctNme = EncValues[1].Item;
                sdata.CustCntctPhnNbr = EncValues[2].Item;
                sdata.CustCntctCellPhnNbr = EncValues[3].Item;
                sdata.SrvcAssrnPocNme = EncValues[4].Item;
                sdata.SrvcAssrnPocPhnNbr = EncValues[5].Item;
                sdata.SrvcAssrnPocCellPhnNbr = EncValues[6].Item;
                sdata.StreetAdr1 = EncValues[7].Item;
                sdata.FlrId = EncValues[8].Item;
                sdata.CtyNme = EncValues[9].Item;
                sdata.SttPrvnNme = EncValues[10].Item;
                sdata.CtryRgnNme = EncValues[11].Item;
                sdata.ZipPstlCd = EncValues[12].Item;
                sdata.StreetAdr2 = EncValues[13].Item;
                sdata.BldgNme = EncValues[14].Item;
                sdata.RmNbr = EncValues[15].Item;
                sdata.SttCd = EncValues[16].Item;
                sdata.CtryCd = EncValues[17].Item;
                sdata.StreetAdr3 = EncValues[18].Item;
                sdata.EventDes = EncValues[19].Item;
                sdata.LstNme = EncValues[20].Item;
                sdata.NteTxt = EncValues[21].Item;
                sdata.EventTitleTxt = EncValues[22].Item;
                sdata.CustEmailAdr = EncValues[23].Item;
                sdata.FrstNme = EncValues[24].Item;
                sdata.CreatDt = DateTime.Now;

                _context.CustScrdData.Add(sdata);

                entity.CustNme = null;
                entity.InstlSitePocNme = null;
                entity.InstlSitePocIntlPhnCd = null;
                entity.InstlSitePocCellPhnNbr = null;
                entity.SrvcAssrnPocNme = null;
                entity.SrvcAssrnPocPhnNbr = null;
                entity.SrvcAssrnPocCellPhnNbr = null;
                entity.StreetAdr = null;
                entity.FlrBldgNme = null;
                entity.CtyNme = null;
                entity.SttPrvnNme = null;
                entity.CtryRgnNme = null;
                entity.ZipCd = null;
                entity.AltShipStreetAdr = null;
                entity.AltShipFlrBldgNme = null;
                entity.AltShipCtyNme = null;
                entity.AltShipSttPrvnNme = null;
                entity.AltShipCtryRgnNme = null;
                entity.AltShipZipCd = null;
                entity.AltShipPocNme = null;
                entity.AltShipPocPhnNbr = null;
                entity.AltShipPocEmail = null;
                entity.EventTitleTxt = null;
                entity.CustAcctTeamPdlNme = null;
                entity.NtwkCustNme = null;
            }

            #region MAIN INFO
            mds.OldEventStusId = mds.EventStusId; // Save old event status
            mds.EventStusId = entity.EventStusId;
            mds.EventTitleTxt = entity.EventTitleTxt;
            mds.CustSowLocTxt = entity.CustSowLocTxt;
            mds.ShrtDes = entity.ShrtDes;
            mds.FrcdftCd = entity.FrcdftCd;
            mds.WoobIpAdr = entity.WoobIpAdr;
            mds.WrkflwStusId = entity.WrkflwStusId;
            mds.ModfdByUserId = entity.ModfdByUserId;
            mds.ModfdDt = DateTime.Now;
            mds.MdsFastTrkTypeId = entity.MdsFastTrkTypeId;

            #endregion
            #region NETWORK
            mds.NtwkH6 = entity.NtwkH6;
            mds.NtwkH1 = entity.NtwkH1;
            mds.NtwkCustNme = entity.NtwkCustNme;
            mds.Event.MplsEventActyType = entity.Event.MplsEventActyType;
            mds.VpnPltfrmTypeId = entity.VpnPltfrmTypeId;
            mds.StrtTmst = entity.StrtTmst;
            mds.EndTmst = entity.EndTmst;
            mds.ExtraDrtnTmeAmt = entity.ExtraDrtnTmeAmt;

            bool oldHasMds = mds.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 1);
            bool oldHasNetwork = mds.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 2 || a.NtwkActyTypeId == 3);
            bool oldHasNetworkIntl = mds.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 4);

            var ntwkActy = _ntwkActyRepo.Find(a => a.EventId == id).ToList();
            _ntwkActyRepo.Delete(ntwkActy);
            _ntwkActyRepo.Create(entity.Event.MdsEventNtwkActy.ToList());

            var ntwkCust = _ntwkCustRepo.Find(a => a.EventId == id).ToList();
            //_ntwkCustRepo.Delete(ntwkCust);
            //_ntwkCustRepo.Create(entity.Event.MdsEventNtwkCust.ToList());
            _ntwkCustRepo.AddNtwkCust(entity.Event.MdsEventNtwkCust.ToList(), id, entity.Event.CsgLvlId);

            //_ntwkTrptRepo.SetInactive(id);
            //_ntwkTrptRepo.Create(entity.Event.MdsEventNtwkTrpt.ToList());
            _ntwkTrptRepo.AddNtwkTrpt(entity.Event.MdsEventNtwkTrpt.ToList(), id, entity.Event.CsgLvlId);

            entity.Event.NidActy
                .ToList()
                .ForEach(a =>
                {
                    if (a.NidActyId == 0)
                    {
                        mds.Event.NidActy.Add(a);
                    }
                });
            #endregion
            #region SITE INFO
            mds.H6 = entity.H6;
            mds.Ccd = entity.Ccd;
            mds.SiteIdTxt = entity.SiteIdTxt;

            _eventCpeDevRepo.SetInactive(id);
            _eventCpeDevRepo.Create(entity.Event.EventCpeDev);
            #endregion
            #region MDS
            mds.H1 = entity.H1;
            mds.CustNme = entity.CustNme;
            mds.OdieCustId = entity.OdieCustId;
            //mds.CustAcctTeamPdlNme = entity.CustAcctTeamPdlNme;
            mds.CntctEmailList = entity.CntctEmailList;
            mds.MdsActyTypeId = entity.MdsActyTypeId;
            mds.Event.MdsEventMacActy = entity.Event.MdsEventMacActy;
            // Added condition to prevent assigned PM from being dropped
            //mds.MnsPmId = string.IsNullOrWhiteSpace(entity.MnsPmId) ? mds.MnsPmId : entity.MnsPmId;
            // Added by Sarah Sandoval [20210215] - Network Intl no PM Assigned
            // Commented above code and incorporate it below
            //var ntwkIntl = _ntwkActyRepo.Find(i => i.EventId == id && i.NtwkActyTypeId == 4).ToList();
            //mds.MnsPmId = ntwkIntl != null && ntwkIntl.Count > 0 ? string.Empty
            //    : string.IsNullOrWhiteSpace(entity.MnsPmId) ? mds.MnsPmId : entity.MnsPmId;

            bool newHasMds = mds.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 1);
            bool newHasNetwork = mds.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 2 || a.NtwkActyTypeId == 3);
            bool newHasNetworkIntl = mds.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 4);
            if (newHasMds || newHasNetworkIntl)
            {
                bool isOldNetworkOnly = (!oldHasMds && !oldHasNetworkIntl && oldHasNetwork); // Check if old have network activity type only
                bool hasChangesOnOldActivity = ((oldHasMds != newHasMds) || (oldHasNetwork != newHasNetwork) || (oldHasNetworkIntl != newHasNetworkIntl)); // Check if there is a change in Activity
                
                // Purpose of this block of code is to deduct a quantity whenever an Event changes from Network to Other (MDS, NEtworkInternational or combination)
                if (isOldNetworkOnly && hasChangesOnOldActivity && !string.IsNullOrEmpty(mds.MnsPmId) && mds.WrkflwStusId != (byte)WorkflowStatus.Visible)
                {
                    var MDSNWOnlyReviewers = _context.LkSysCfg.Where(x => x.PrmtrNme == "MDSNWOnlyReviewers").FirstOrDefault().PrmtrValuTxt;
                    if (MDSNWOnlyReviewers.ToLower().Contains(mds.MnsPmId.ToLower()))
                    {
                        var oldMnsPm = _context.LkUser.Where(a => a.UserAdid == mds.MnsPmId).FirstOrDefault();
                        if (oldMnsPm != null)
                        {
                            oldMnsPm.EventQty -= 1;
                            _context.LkUser.Update(oldMnsPm);
                            //_userRepo.Update(oldMnsPm.UserId, oldMnsPm);
                        }
                    }
                }
                mds.MnsPmId = newHasNetworkIntl ? string.Empty : entity.MnsPmId;
            }
            else if (oldHasMds && newHasNetwork)
            {
                mds.MnsPmId = string.Empty;
            }

            #region NON-DISCO
            _mdsEventOdieDevRepo.Delete(a => a.EventId == id);
            _mdsEventOdieDevRepo.Create(entity.Event.MdsEventOdieDev);

            //_eventDeviceCompletionRepo.SetInactive(a => a.EventId == id && a.RecStusId == 1);
            //_eventDeviceCompletionRepo.Create(entity.Event.EventDevCmplt);

            _eventDevSrvcMgmtRepo.SetInactive(a => a.EventId == id && a.RecStusId == 1);
            _eventDevSrvcMgmtRepo.Create(entity.Event.EventDevSrvcMgmt);
            _mdsEventSiteSrvcRepo.Delete(a => a.EventId == id);
            _mdsEventSiteSrvcRepo.Create(entity.Event.MdsEventSiteSrvc);
            _mdsEventDslSbicCustTrptRepo.SetInactive(a => a.EventId == id && a.RecStusId == 1);
            _mdsEventDslSbicCustTrptRepo.Create(entity.Event.MdsEventDslSbicCustTrpt);
            // Added by Sarah Sandoval (20210223) - mimic the OLD COWS for OptInCktCd, ReadyBeginCd and OptInHCd
            if (entity.Event.MdsEventSlnkWiredTrpt != null)
            {
                entity.Event.MdsEventSlnkWiredTrpt = entity.Event.MdsEventSlnkWiredTrpt
                    .Select(a =>
                    {
                        // Event Workflow Complete (7)
                        a.OptInCktCd = mds.WrkflwStusId.Equals(7)
                            && !(string.IsNullOrWhiteSpace(a.OptInCktCd) && string.IsNullOrWhiteSpace(a.ReadyBeginCd)
                            && string.IsNullOrWhiteSpace(a.OptInHCd)) ? "N" : string.IsNullOrWhiteSpace(a.OptInCktCd) ? null : a.OptInCktCd;
                        a.ReadyBeginCd = mds.WrkflwStusId.Equals(7)
                            && !(string.IsNullOrWhiteSpace(a.OptInCktCd) && string.IsNullOrWhiteSpace(a.ReadyBeginCd)
                            && string.IsNullOrWhiteSpace(a.OptInHCd)) ? "N" : string.IsNullOrWhiteSpace(a.ReadyBeginCd) ? null : a.ReadyBeginCd;
                        a.OptInHCd = string.IsNullOrWhiteSpace(a.OptInHCd) ? null : a.OptInHCd;
                        return a;
                    })
                    .ToList();
            }
            _mdsEventSlnkWiredTrptRepo.SetInactive(a => a.EventId == id && a.RecStusId == 1);
            _mdsEventSlnkWiredTrptRepo.Create(entity.Event.MdsEventSlnkWiredTrpt);
            _mdsEventWrlsTrptRepo.Delete(a => a.EventId == id);
            _mdsEventWrlsTrptRepo.Create(entity.Event.MdsEventWrlsTrpt);
            _mdsEventPortBndwdRepo.SetInactive(a => a.EventId == id && a.RecStusId == 1);
            _mdsEventPortBndwdRepo.Create(entity.Event.MdsEventPortBndwd);
            #endregion
            #region DISCO
            mds.DiscMgmtCd = entity.DiscMgmtCd;
            mds.FullCustDiscCd = entity.FullCustDiscCd;
            mds.FullCustDiscReasTxt = entity.FullCustDiscReasTxt;

            // Added by Sarah Sandoval (20210223) - mimic the OLD COWS for OptInCktCd, ReadyBeginCd and OptInHCd
            if (entity.Event.EventDiscoDev != null)
            {
                entity.Event.EventDiscoDev = entity.Event.EventDiscoDev
                    .Select(a =>
                    {
                        // Event Workflow Complete (7)
                        a.OptInCktCd = mds.WrkflwStusId.Equals(7) ? "Y" : string.IsNullOrWhiteSpace(a.OptInCktCd) ? null : a.OptInCktCd;
                        a.ReadyBeginCd = mds.WrkflwStusId.Equals(7) ? "Y" : string.IsNullOrWhiteSpace(a.ReadyBeginCd) ? null : a.ReadyBeginCd;
                        a.OptInHCd = string.IsNullOrWhiteSpace(a.OptInHCd) ? null : a.OptInHCd;
                        return a;
                    })
                    .ToList();
            }
            _eventDiscoDevRepo.SetInactive(id);
            _eventDiscoDevRepo.Create(entity.Event.EventDiscoDev.ToList());
            #endregion
            #endregion
            #region PHYSICAL ADDRESS
            mds.InstlSitePocNme = entity.InstlSitePocNme;
            mds.InstlSitePocIntlPhnCd = entity.InstlSitePocIntlPhnCd;
            mds.InstlSitePocPhnNbr = entity.InstlSitePocPhnNbr;
            mds.InstlSitePocIntlCellPhnCd = entity.InstlSitePocIntlCellPhnCd;
            mds.InstlSitePocCellPhnNbr = entity.InstlSitePocCellPhnNbr;
            mds.SrvcAssrnPocNme = entity.SrvcAssrnPocNme;
            mds.SrvcAssrnPocIntlPhnCd = entity.SrvcAssrnPocIntlPhnCd;
            mds.SrvcAssrnPocPhnNbr = entity.SrvcAssrnPocPhnNbr;
            mds.SrvcAssrnPocIntlCellPhnCd = entity.SrvcAssrnPocIntlCellPhnCd;
            mds.SrvcAssrnPocCellPhnNbr = entity.SrvcAssrnPocCellPhnNbr;
            mds.StreetAdr = entity.StreetAdr;
            mds.SttPrvnNme = entity.SttPrvnNme;
            mds.FlrBldgNme = entity.FlrBldgNme;
            mds.CtryRgnNme = entity.CtryRgnNme;
            mds.CtyNme = entity.CtyNme;
            mds.ZipCd = entity.ZipCd;
            mds.UsIntlCd = entity.UsIntlCd;
            mds.SrvcAvlbltyHrs = entity.SrvcAvlbltyHrs;
            mds.SrvcTmeZnCd = entity.SrvcTmeZnCd;
            #endregion
            #region CPE DELIVERY OPTION
            entity.MdrNumber = (entity.MdrNumber ?? string.Empty).Length > 25 ? entity.MdrNumber.Substring(0, 25) : entity.MdrNumber;
            mds.SprintCpeNcrId = entity.SprintCpeNcrId;
            mds.MdrNumber = entity.MdrNumber;
            mds.CpeDspchEmailAdr = entity.CpeDspchEmailAdr;
            mds.CpeDspchCmntTxt = entity.CpeDspchCmntTxt;
            mds.NidSerialNbr = entity.NidSerialNbr;
            mds.NidHostNme = entity.NidHostNme;
            #endregion
            #region PRE-STAGING
            mds.ShippedDt = entity.ShippedDt;
            mds.ShipTrkRefrNbr = entity.ShipTrkRefrNbr;
            mds.ShipCustEmailAdr = entity.ShipCustEmailAdr;
            mds.ShipDevSerialNbr = entity.ShipDevSerialNbr;
            mds.ShipCxrNme = entity.ShipCxrNme;
            mds.AltShipPocPhnNbr = entity.AltShipPocPhnNbr;
            mds.AltShipPocEmail = entity.AltShipPocEmail;
            mds.AltShipStreetAdr = entity.AltShipStreetAdr;
            mds.AltShipSttPrvnNme = entity.AltShipSttPrvnNme;
            mds.AltShipFlrBldgNme = entity.AltShipFlrBldgNme;
            mds.AltShipCtyNme = entity.AltShipCtyNme;
            mds.AltShipCtryRgnNme = entity.AltShipCtryRgnNme;
            mds.AltShipZipCd = entity.AltShipZipCd;
            #endregion PRE-STAGING
            #region REQUESTOR
            mds.ReqorUserId = entity.ReqorUserId;
            mds.ReqorUserCellPhnNbr = entity.ReqorUserCellPhnNbr;
            #endregion
            #region EVENT
            #region ESCALATION
            mds.EsclCd = entity.EsclCd;
            mds.EsclReasId = entity.EsclReasId;
            mds.PrimReqDt = entity.PrimReqDt;
            mds.ScndyReqDt = entity.ScndyReqDt;
            mds.EsclBusJustnTxt = entity.EsclBusJustnTxt;
            mds.CmpltdEmailCcTxt = entity.CmpltdEmailCcTxt;
            mds.PubEmailCcTxt = entity.PubEmailCcTxt;
            #endregion
            #region SCHEDULE
            mds.CnfrcBrdgId = entity.CnfrcBrdgId;
            mds.CnfrcBrdgNbr = entity.CnfrcBrdgNbr;
            mds.CnfrcPinNbr = entity.CnfrcPinNbr;
            mds.OnlineMeetingAdr = entity.OnlineMeetingAdr;
            mds.StrtTmst = entity.StrtTmst;
            mds.EndTmst = entity.EndTmst;
            mds.EventDrtnInMinQty = entity.EventDrtnInMinQty;
            mds.SoftAssignCd = entity.SoftAssignCd;
            mds.MdsCmpltCd = entity.MdsCmpltCd;
            mds.NtwkCmpltCd = entity.NtwkCmpltCd;
            mds.VasCmpltCd = entity.VasCmpltCd;
            mds.PreCfgCmpltCd = entity.PreCfgCmpltCd;
            #endregion
            #endregion

            mds.IpmDes = entity.IpmDes;
            mds.RltdCmpsNcrCd = entity.RltdCmpsNcrCd;
            mds.RltdCmpsNcrNme = entity.RltdCmpsNcrNme;
            mds.CmntTxt = entity.CmntTxt;
            mds.FailReasId = entity.FailReasId;
            mds.CustLtrOptOut = entity.CustLtrOptOut;

            SaveAll();

            if (mds.WrkflwStusId == (int)WorkflowStatus.Complete)
            {
                if (mds.MdsActyTypeId == 3)
                {
                    // For Disconnect, the only condition is if it is Complete
                    mds.Event.EventDiscoDev
                        //.Where(a => a.OptInCktCd != null && a.ReadyBeginCd != null && a.OptInHCd != null && a.RecStusId == 1)
                        .Where(a => a.RecStusId == 1)
                        .ToList()
                        .ForEach(a =>
                        {
                            _mdsEventSlnkWiredTrptRepo.UpdatePMFlagToNRM(mds.EventId, a.H5H6, string.Empty, true);
                        });
                }
                else if (mds.MdsActyTypeId == 2 || mds.MdsActyTypeId == 5)
                {
                    mds.Event.MdsEventSlnkWiredTrpt
                        //.Where(a => a.OptInCktCd != null && a.ReadyBeginCd != null && a.OptInHCd != null && a.RecStusId == 1)
                        .Where(a => !(string.IsNullOrWhiteSpace(a.OptInCktCd) && string.IsNullOrWhiteSpace(a.ReadyBeginCd)
                            && string.IsNullOrWhiteSpace(a.OptInHCd)) && a.RecStusId == 1)
                        .ToList()
                        .ForEach(a =>
                        {
                            _mdsEventSlnkWiredTrptRepo.UpdatePMFlagToNRM(mds.EventId, mds.H6, a.IpNuaAdr, false);
                        });
                }
            }

            if (mds.WrkflwStusId == (int)WorkflowStatus.Complete || mds.NtwkCmpltCd || mds.VasCmpltCd)
            {
                bool isNetwork = mds.Event.MdsEventNtwkActy.Any(a => a.EventId == mds.EventId && (a.NtwkActyTypeId == 2));
                bool isVas = mds.Event.MdsEventNtwkActy.Any(a => a.EventId == mds.EventId && (a.NtwkActyTypeId == 3));
                bool isCarrierEthernet = mds.Event.MplsEventActyType.Any(a => a.EventId == mds.EventId && (a.MplsActyTypeId == 20 || a.MplsActyTypeId == 21));
                bool isCarrierEthernetChange = mds.Event.MplsEventActyType.Any(a => a.EventId == mds.EventId && (a.MplsActyTypeId == 21));

                mds.Event.MdsEventNtwkTrpt
                    .Where(a => a.RecStusId == 1 && !string.IsNullOrEmpty(a.DdAprvlNbr) && !string.IsNullOrEmpty(a.AssocH6) && !string.IsNullOrEmpty(a.IpNuaAdr))
                    .ToList()
                    .ForEach(a =>
                    {
                        var SPCEorH6 = a.AssocH6; // Default use for CE

                        if(isCarrierEthernetChange && !string.IsNullOrEmpty(a.CeSrvcId))
                        {
                            isCarrierEthernetChange = true;
                            SPCEorH6 = a.CeSrvcId;
                        } else if (isCarrierEthernetChange && string.IsNullOrEmpty(a.CeSrvcId))
                        {
                            isCarrierEthernetChange = false;
                            SPCEorH6 = a.AssocH6;
                        }

                        _ntwkTrptRepo.UpdateNetworkBPMTask(a.DdAprvlNbr, SPCEorH6, (isCarrierEthernet ? 2 : (a.VasType == "1" ? 1 : 0)), isCarrierEthernetChange, mds.EventId, entity.ModfdByUserId);

                        if (a.AssocH6 != "")
                        {
                            var nidSerialNbr = "";
                            var ntwkTrpt = networkTransport
                            .Where(b => (b.AssocH6 == a.AssocH6 && b.NIDSerialNbr != "" && b.NIDSerialNbr != null))
                            .FirstOrDefault();

                            if (ntwkTrpt != null)
                            {
                                nidSerialNbr = ntwkTrpt.NIDSerialNbr;
                            }

                            _ntwkTrptRepo.UpdateNidAtBPM(-1, a.AssocH6, nidSerialNbr, mds.EventId, entity.ModfdByUserId);
                        }
                    });

                //mds.Event.NidActy
                //    .Where(a => (a.NidSerialNbr ?? "").Length > 0)
                //    .ToList()
                //    .ForEach(a =>
                //    {
                //        _ntwkTrptRepo.UpdateNidAtBPM(-1, a.H6, a.NidSerialNbr, mds.EventId, mds.CreatByUserId);
                //    });
            }

            //Complete orders for the ones in  CPE Tech status and CPE Required in (No CPE Required or Customer Install)
            if ((!entity.MdsActyTypeId.Equals(3)) && (entity.WrkflwStusId.Equals((byte)WorkflowStatus.Complete))
            && (entity.SprintCpeNcrId.Equals(3) || entity.SprintCpeNcrId.Equals(4)))
            {
                //Order could be in any of the tasks, so complete both 604 & 1000
                var ordrs = (from cp in entity.Event.EventCpeDev
                             where !cp.CpeOrdrId.Equals(-1)
                             select new { cp.CpeOrdrId, cp.DeviceId, cp.RcptStus });

                if (ordrs != null)
                {
                    foreach (var i in ordrs)
                    {
                        if (!(i.RcptStus.Equals("Cancelled")))
                        {
                            //Complete CPE Tech and Bill Activation tasks and also the order
                            List<SqlParameter> pc = new List<SqlParameter>
                            {
                                new SqlParameter() {ParameterName = "@ORDR_ID", SqlDbType = SqlDbType.Int, Value = i.CpeOrdrId},
                                new SqlParameter() {ParameterName = "@DEVICE_ID", SqlDbType = SqlDbType.VarChar, Value = i.DeviceId},
                                new SqlParameter() {ParameterName = "@NOTE", SqlDbType = SqlDbType.VarChar, Value = "Order completed by Event - " + entity.EventId.ToString()},
                                new SqlParameter() {ParameterName = "@USERID", SqlDbType = SqlDbType.Int, Value = entity.CreatByUserId}
                            };

                            _context.Database.ExecuteSqlCommand("[dbo].[CmplCPEOrdrFrmEvent] @ORDR_ID, @DEVICE_ID, @NOTE, @USERID", pc.ToArray());
                        }
                    }
                }
            }

            // Added byte on WrkflwStusId condition since it always return false on debug mode
            // Call MNS Reviewer Assignment Logic for Network-Only Events
            if ((!CheckMDSNtwkActy(ref entity, "hasMDS"))
                && (entity.WrkflwStusId.Equals((byte)WorkflowStatus.Complete) ||
                        entity.WrkflwStusId.Equals((byte)WorkflowStatus.Delete) ||
                        entity.WrkflwStusId.Equals((byte)WorkflowStatus.Submit)))
            {
                // Added condition by Sarah Sandoval [20210205]
                // Assigned PM is not needed for Network Intl Only type
                if (entity.Event.MdsEventNtwkActy.FirstOrDefault(i => i.NtwkActyTypeId.Equals(4)) == null)
                {
                    List<SqlParameter> pc = new List<SqlParameter>
                            {
                                new SqlParameter() {ParameterName = "@EVENT_ID", SqlDbType = SqlDbType.Int, Value = entity.EventId},
                                new SqlParameter() {ParameterName = "@WRKFLW_STUS_ID", SqlDbType = SqlDbType.TinyInt, Value = (byte)entity.WrkflwStusId}
                            };

                    _context.Database.ExecuteSqlCommand("[web].[updtMNSRevwr] @EVENT_ID, @WRKFLW_STUS_ID", pc.ToArray());
                }
            }
            return mds;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public DesignDocDetail GetDesignDocDetail(string h6, string dd, int VASCEFlg = 0, bool? CEChngFlg = false)
        {
            //List<SqlParameter> param = new List<SqlParameter>()
            //{
            //    new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 },
            //    new SqlParameter() { ParameterName = "@DD", SqlDbType = SqlDbType.VarChar, Value = dd },
            //    new SqlParameter() { ParameterName = "@NUA", SqlDbType = SqlDbType.VarChar, Value = nua }
            //};

            //var infos = _context.Query<DesignDocDetail>().AsNoTracking()
            //    .FromSql(
            //        "web.getDesignDocData @H6, @DD, @NUA",
            //        param.ToArray()
            //    ).ToList();

            //return infos;

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "web.getDesignDocData @H6, @DD, @VASCEFlg, @CEChngFlg";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@DD", SqlDbType = SqlDbType.VarChar, Value = dd });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@VASCEFlg", SqlDbType = SqlDbType.TinyInt, Value = VASCEFlg });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CEChngFlg", SqlDbType = SqlDbType.Bit, Value = CEChngFlg });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    var result = new DesignDocDetail
                    {
                        Table1 = reader.Translate<DesignDocDetailTable1>(),
                        Table2 = reader.Translate<DesignDocDetailTable2>(),
                        Table3 = reader.Translate<DesignDocDetailTable3>(),
                        Table4 = reader.Translate<DesignDocDetailTable4>()
                    };

                    return result;
                }
            }
        }

        //public async Task<DataTable> GetVASSubTypes()
        //{
        //    DataTable dt = new DataTable();
        //    using (var connection = _context.Database.GetDbConnection())
        //    {
        //        await connection.OpenAsync();
        //        using (var command = connection.CreateCommand())
        //        {
        //            command.CommandText = $"web.getVASSubTypesFromBPM";
        //            var result = await command.ExecuteReaderAsync();
        //            dt.Load(result);
        //            return dt;
        //        }
        //    }
        //}

        public DataTable GetVASSubTypes()
        {
            DataTable dt = new DataTable();
            if (!_cache.TryGetValue(CacheKeys.VasSubTypes, out _))
            {
                SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
                SqlCommand command = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();

                command = new SqlCommand("web.getVASSubTypesFromBPM", connection);
                command.CommandTimeout = _context.commandTimeout;
                command.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(command);
                da.Fill(dt);
                connection.Close();

                _cache.Set(CacheKeys.VasSubTypes, dt, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            _cache.TryGetValue(CacheKeys.VasSubTypes, out dt);

            return dt;
        }

        private bool CheckIfMds(MdsEvent obj)
        {
            return obj.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 1);
        }

        private bool CheckIfNetwork(MdsEvent obj)
        {
            return obj.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 2 || a.NtwkActyTypeId == 3);
        }

        private bool CheckIfDisconnect(MdsEvent obj)
        {
            return CheckIfMds(obj) && obj.MdsActyTypeId == 3;
        }

        private bool CheckIfNID(MdsEvent obj)
        {
            return CheckIfNetwork(obj) && obj.Event.MplsEventActyType.Any(a => a.MplsActyTypeId == 3);
        }

        /*  This function is called for every update on MDS event
         *  - to send messages to MACH5 through MACH5Sender service in M5EventMsg table
         *  - to update RedsgnDevicesInfo and RedsgnDevEventHist table through ODIE Service
         *  - every successful Redesign completion should send redesign email notification
         *  - will log messages on respective services (ODIESender, ODIEReceiver, MACH5Sender)
         */
        public void SaveEventDevComplete(MdsEvent obj, MdsEventView model)
        {
            //var start = DateTime.Now;
            int eventId = obj.EventId;
            bool CmplDev = false;
            List<EventDevCmplt> eventDevCmpltList = new List<EventDevCmplt>();
            List<RedsgnDevEventHist> redsgnDevEventHistList = new List<RedsgnDevEventHist>();
            List<M5EventMsg> m5EventMsgList = new List<M5EventMsg>();

            //// Get saved MDS Event Data
            //var end1 = DateTime.Now;
            //MdsEvent obj = GetById(eventId, null, false);
            //var end11 = DateTime.Now;

            // Get related data used in validation related to H6/CCD Combination
            List<MdsEventCpeDevView> mdsEventCpeDevView = new List<MdsEventCpeDevView>();
            List<MdsEventDevSrvcMgmtView> mdsEventDevSrvcMgmtView = new List<MdsEventDevSrvcMgmtView>();
            List<MdsEventDevSrvcMgmtView> mdsEventASASView = new List<MdsEventDevSrvcMgmtView>();
            //List<CPEDispatchEmailResult> cpeDispatchEmail = new List<CPEDispatchEmailResult>();
            if (!string.IsNullOrWhiteSpace(obj.H6) && obj.Ccd.HasValue)
            {
                // Recalling the SP since some column needed for validation is not provided from UI or DB
                // mdsEventCpeDevView for DLVY_CLLI
                // mdsEventDevSrvcMgmtView for M5_ORDR_ID
                //var end2 = DateTime.Now;
                //GetM5EventDatabyH6Results h6SiteLookup = _searchRepo.M5LookupByH6CCD(obj.H6, obj.Ccd.GetValueOrDefault().ToString(), "N");
                //var end22 = DateTime.Now;
                //mdsEventCpeDevView = h6SiteLookup.M5CpeTable.ToList();
                //mdsEventDevSrvcMgmtView = h6SiteLookup.M5MnsTable.ToList();
                //mdsEventASASView = h6SiteLookup.M5CpeAsasTable.ToList();
                //cpeDispatchEmail = h6SiteLookup.CPEDispatchEmail.ToList();
                mdsEventCpeDevView = model.H6Lookup.CpeDevices.ToList();
                mdsEventDevSrvcMgmtView = model.H6Lookup.MnsOrders.ToList();
                mdsEventASASView = model.H6Lookup.MnsAsasTypes.ToList();
                //cpeDispatchEmail = model.H6Lookup.CPEDispatchEmail.ToList();
            }

            // Get all EventDevCmplt data for OldCmpltCd due to DeleteAllDevCmplt() that may delete the record
            var dbEventDevCmplt = obj.Event.EventDevCmplt.ToList();

            //var end3 = DateTime.Now;
            DeleteAllDevCmplt(obj);
            //var end33 = DateTime.Now;

            // Get updated values on DB
            var devcmplodie = dbEventDevCmplt.Where(i => i.EventId == eventId && i.OdieSentDt.HasValue).ToList();
            var dbM5EvntMsgs = _context.M5EventMsg.Where(i => i.EventId == eventId).ToList();

            //// Get active EventDevCmplt data
            //List<EventDevCmplt> devcmplt = obj.Event.EventDevCmplt
            //    .Where(i => i.RecStusId == (byte)ERecStatus.Active).ToList();

            // Fetch missing data for events
            obj = FetchEventMacData(obj);

            // Do not complete redesign if there is atleast one generic MAC Activity
            bool bRedesign = !obj.Event.MdsEventMacActy.Any(i => i.MdsMacActy.MdsMacActyNme.Contains("999999999"));

            model.DevCompletion = model.DevCompletion != null ? model.DevCompletion : new List<EventDeviceCompletionView>();
            //var end4 = DateTime.Now;
            if (model.DevCompletion != null)
            {
                foreach (EventDeviceCompletionView item in model.DevCompletion)
                {
                    // Add record if it doesn't exist
                    if (!devcmplodie.Any(dc => dc.OdieDevNme == item.OdieDevNme && dc.RedsgnDevId == item.RedsgnDevId))
                    {
                        // Get the top inactive row in old db value for OdieName and RedesignDevId
                        // to mimic OldCmpltdCd value in old COWS implementation
                        EventDevCmplt dbItem = dbEventDevCmplt.OrderByDescending(i => i.EventDevCmpltId)
                            .FirstOrDefault(dc => dc.OdieDevNme == item.OdieDevNme && dc.RedsgnDevId == item.RedsgnDevId);
                        var OldCmpltdCd = dbItem != null ? dbItem.CmpltdCd : false;

                        EventDevCmplt edc = new EventDevCmplt();
                        edc.EventId = eventId;
                        edc.RedsgnDevId = item.RedsgnDevId;
                        edc.OdieDevNme = item.OdieDevNme;
                        edc.H6 = item.H6;
                        edc.CmpltdCd = item.CmpltdCd;
                        edc.RecStusId = (byte)ERecStatus.Active;
                        edc.CreatDt = DateTime.Now;
                        eventDevCmpltList.Add(edc);

                        //if (item.CmpltdCd && item.CmpltdCd != OldCmpltdCd)
                        //    CmplDev = item.CmpltdCd;

                        if (item.CmpltdCd && item.CmpltdCd != OldCmpltdCd)
                        {
                            CmplDev = item.CmpltdCd;

                            // For completed device/s, insert RedsgnDevEventHist record
                            // If RedsgnDevId does not exist in the current list
                            // If RedsgnDevId does not exist in the current DB
                            if (!redsgnDevEventHistList.Any(i => item.RedsgnDevId.HasValue
                                    && i.RedsgnDevId == item.RedsgnDevId.Value)
                                && !_context.RedsgnDevEventHist.Any(i => item.RedsgnDevId.HasValue
                                    && i.RedsgnDevId == item.RedsgnDevId.Value)
                                && !OldCmpltdCd && bRedesign)
                            {
                                RedsgnDevEventHist rde = new RedsgnDevEventHist();
                                rde.EventId = eventId;
                                rde.CmpltdDt = DateTime.Now;
                                rde.RedsgnDevId = item.RedsgnDevId.GetValueOrDefault();
                                redsgnDevEventHistList.Add(rde);
                            }

                            //// Send MNS/MSS complete messages if not sent already
                            //if (mdsEventDevSrvcMgmtView != null && mdsEventDevSrvcMgmtView.Count() > 0)
                            //{
                            //    var sentMNS = (from mem in _context.M5EventMsg
                            //                   join dm in _context.EventDevSrvcMgmt on new { mon = mem.M5OrdrNbr, di = mem.DevId, ci = mem.CmpntId.ToString() } equals new { mon = dm.MnsOrdrNbr, di = dm.DeviceId, ci = dm.M5OrdrCmpntId }
                            //                   where dm.EventId == eventId
                            //                      && dm.RecStusId == ((byte)ERecStatus.Active)
                            //                   select mem);

                            //    var dmil = obj.Event.EventDevSrvcMgmt.Where(i => i.OdieDevNme == item.OdieDevNme
                            //            && i.RecStusId == (byte)ERecStatus.Active);

                            //    // mdsEventDevSrvcMgmtView recalled from _searchRepo.M5LookupByH6CCD
                            //    // due to M5_ORDR_ID not provided from the UI
                            //    foreach (var edsm in dmil)
                            //    {
                            //        var devSrvcMgmt = mdsEventDevSrvcMgmtView.FirstOrDefault(i => i.MnsOrdrNbr == edsm.MnsOrdrNbr && i.DeviceId == edsm.DeviceId);
                            //        if (devSrvcMgmt.MNSOrdrID > 0
                            //            && !string.IsNullOrWhiteSpace(devSrvcMgmt.M5OrdrCmpntId)
                            //            && (dbM5EvntMsgs == null || !dbM5EvntMsgs.Any(i => i.CmpntId == int.Parse(devSrvcMgmt.M5OrdrCmpntId)))
                            //            && (!string.Equals(devSrvcMgmt.CmpntStus, "Cancelled", StringComparison.CurrentCultureIgnoreCase))
                            //            && (sentMNS == null || !sentMNS.Any(i => i.CmpntId == int.Parse(devSrvcMgmt.M5OrdrCmpntId))))
                            //        {
                            //            // Send M5 MNS Completion Msg
                            //            M5EventMsg mnsm5 = new M5EventMsg();
                            //            mnsm5.M5OrdrId = devSrvcMgmt.MNSOrdrID;
                            //            mnsm5.EventId = eventId;
                            //            mnsm5.M5OrdrNbr = devSrvcMgmt.MnsOrdrNbr;
                            //            if (!string.IsNullOrWhiteSpace(devSrvcMgmt.M5OrdrCmpntId))
                            //                mnsm5.CmpntId = int.Parse(devSrvcMgmt.M5OrdrCmpntId);
                            //            mnsm5.M5MsgId = devSrvcMgmt.MnsSrvcTierId.Equals((byte)64) ? (int)M5Msg.MSSComplete : (int)M5Msg.MNSComplete;
                            //            mnsm5.DevId = devSrvcMgmt.DeviceId;
                            //            mnsm5.StusId = (short)EmailStatus.Pending;
                            //            mnsm5.CreatDt = DateTime.Now;
                            //            m5EventMsgList.Add(mnsm5);
                            //        }
                            //    }
                            //}

                            // Send ASAS MNS/MSS complete messages if not sent already
                            if (mdsEventASASView != null && mdsEventASASView.Count() > 0)
                            {
                                var sentMNS = (from mem in _context.M5EventMsg
                                               join dm in _context.EventDevSrvcMgmt on new { mon = mem.M5OrdrNbr, di = mem.DevId, ci = mem.CmpntId.ToString() } equals new { mon = dm.MnsOrdrNbr, di = dm.DeviceId, ci = dm.M5OrdrCmpntId }
                                               where dm.EventId == eventId
                                                  && dm.RecStusId == ((byte)ERecStatus.Active)
                                               select mem);

                                var asasdmil = obj.Event.EventDevSrvcMgmt.Where(i => i.OdieDevNme == item.OdieDevNme
                                        && i.RecStusId == (byte)ERecStatus.Active);

                                // mdsEventDevSrvcMgmtView recalled from _searchRepo.M5LookupByH6CCD
                                // due to M5_ORDR_ID not provided from the UI
                                foreach (var asasdm in asasdmil)
                                {
                                    var asasDevSrvcMgmt = mdsEventASASView.FirstOrDefault(i => i.MnsOrdrNbr == asasdm.MnsOrdrNbr && i.DeviceId == asasdm.DeviceId);
                                    if (asasDevSrvcMgmt != null && asasDevSrvcMgmt.MNSOrdrID > 0
                                        && !string.IsNullOrWhiteSpace(asasdm.M5OrdrCmpntId)
                                        && (dbM5EvntMsgs == null || !dbM5EvntMsgs.Any(i => i.CmpntId == int.Parse(asasdm.M5OrdrCmpntId)))
                                        && (!string.Equals(asasdm.CmpntStus, "Cancelled", StringComparison.CurrentCultureIgnoreCase)
                                            || !string.Equals(asasdm.CmpntStus, "CN", StringComparison.CurrentCultureIgnoreCase))
                                        && (sentMNS == null || !sentMNS.Any(i => i.CmpntId == int.Parse(asasdm.M5OrdrCmpntId))))
                                    {
                                        // Send M5 MNS Completion Msg
                                        M5EventMsg mnsm5 = new M5EventMsg();
                                        mnsm5.M5OrdrId = asasDevSrvcMgmt.MNSOrdrID;
                                        mnsm5.EventId = eventId;
                                        mnsm5.M5OrdrNbr = asasdm.MnsOrdrNbr;
                                        if (!string.IsNullOrWhiteSpace(asasdm.M5OrdrCmpntId))
                                            mnsm5.CmpntId = int.Parse(asasdm.M5OrdrCmpntId);
                                        mnsm5.M5MsgId = asasdm.MnsSrvcTierId.Equals((byte)64) ? (int)M5Msg.MSSComplete : (int)M5Msg.MNSComplete;
                                        mnsm5.DevId = asasdm.DeviceId;
                                        mnsm5.StusId = (short)EmailStatus.Pending;
                                        mnsm5.CreatDt = DateTime.Now;
                                        m5EventMsgList.Add(mnsm5);
                                    }
                                }
                            }

                            // Call CPE Device completion SP for certain CLLI's,
                            // Check getM5EventDatabyH6 for the list
                            if (!OldCmpltdCd && obj.Event.EventCpeDev.Count > 0)
                            {
                                var cdil = obj.Event.EventCpeDev
                                    .Where(i => i.OdieDevNme == item.OdieDevNme
                                        && i.RecStusId == (byte)ERecStatus.Active);
                                foreach (var cdi in cdil)
                                {
                                    // mdsEventCpeDevView recalled from _searchRepo.M5LookupByH6CCD
                                    // due to some column not provided from UI needed for validation (DLVY_CLLI)
                                    var cpeDev = mdsEventCpeDevView
                                        .FirstOrDefault(i => i.CpeOrdrId == cdi.CpeOrdrId
                                            && i.DeviceId == cdi.DeviceId);
                                    if (!string.IsNullOrWhiteSpace(cdi.CpeOrdrId)
                                        && cpeDev != null
                                        && !string.IsNullOrWhiteSpace(cpeDev.DlvyClli)
                                        && cpeDev.DlvyClli.Length > 3
                                        && cpeDev.OrdrId.HasValue && cpeDev.OrdrId.Value > 0)
                                    // DB Values for some cpeDev.DLVY_CLLI are 'N/A'
                                    // So this is called if cpeDev.DLVY_CLLI is not equal to 'N/A'
                                    {
                                        List<SqlParameter> pc = new List<SqlParameter>
                                {
                                    new SqlParameter() { ParameterName = "@ORDR_ID", SqlDbType = SqlDbType.Int, Value = cpeDev.OrdrId },
                                    new SqlParameter() { ParameterName = "@DEVICE_ID", SqlDbType = SqlDbType.VarChar, Value = cdi.DeviceId },
                                    new SqlParameter() { ParameterName = "@NOTE", SqlDbType = SqlDbType.VarChar, Value = "V5U Note: Order Auto Completed in BPM, EventID : " + eventId }
                                };

                                        _context.Database.ExecuteSqlCommand
                                            ("[dbo].[insertOrdrCmplAll_V5U] @ORDR_ID, @DEVICE_ID, @NOTE",
                                            pc.ToArray());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //var end44 = DateTime.Now;


            //var end5 = DateTime.Now;
            if (CmplDev) //(obj.WrkflwStusId == (int)WorkflowStatus.Complete)
            {
                // Send MNS/MSS complete messages if not sent already
                if (mdsEventDevSrvcMgmtView != null && mdsEventDevSrvcMgmtView.Count() > 0)
                {
                    var sentMNS = (from mem in _context.M5EventMsg
                                   join dm in _context.EventDevSrvcMgmt on new { mon = mem.M5OrdrNbr, di = mem.DevId, ci = mem.CmpntId.ToString() } equals new { mon = dm.MnsOrdrNbr, di = dm.DeviceId, ci = dm.M5OrdrCmpntId }
                                   where dm.EventId == eventId
                                      && dm.RecStusId == ((byte)ERecStatus.Active)
                                   select mem);

                    var dmil = obj.Event.EventDevSrvcMgmt.Where(i => i.RecStusId == (byte)ERecStatus.Active);

                    // mdsEventDevSrvcMgmtView recalled from _searchRepo.M5LookupByH6CCD
                    // due to M5_ORDR_ID not provided from the UI
                    foreach (var edsm in dmil)
                    {
                        var devSrvcMgmt = mdsEventDevSrvcMgmtView.FirstOrDefault(i => i.MnsOrdrNbr == edsm.MnsOrdrNbr && i.DeviceId == edsm.DeviceId);
                        if (devSrvcMgmt != null && devSrvcMgmt.MNSOrdrID > 0
                            && !string.IsNullOrWhiteSpace(devSrvcMgmt.M5OrdrCmpntId)
                            && (dbM5EvntMsgs == null || !dbM5EvntMsgs.Any(i => i.CmpntId == int.Parse(edsm.M5OrdrCmpntId)))
                            && (!string.Equals(edsm.CmpntStus, "Cancelled", StringComparison.CurrentCultureIgnoreCase)
                            || !string.Equals(edsm.CmpntStus, "CN", StringComparison.CurrentCultureIgnoreCase))
                            && (sentMNS == null || !sentMNS.Any(i => i.CmpntId == int.Parse(edsm.M5OrdrCmpntId))))
                        {
                            // Send M5 MNS Completion Msg
                            M5EventMsg mnsm5 = new M5EventMsg();
                            mnsm5.M5OrdrId = devSrvcMgmt.MNSOrdrID;
                            mnsm5.EventId = eventId;
                            mnsm5.M5OrdrNbr = edsm.MnsOrdrNbr;
                            if (!string.IsNullOrWhiteSpace(edsm.M5OrdrCmpntId))
                                mnsm5.CmpntId = int.Parse(edsm.M5OrdrCmpntId);
                            mnsm5.M5MsgId = edsm.MnsSrvcTierId.Equals((byte)64) ? (int)M5Msg.MSSComplete : (int)M5Msg.MNSComplete;
                            mnsm5.DevId = edsm.DeviceId;
                            mnsm5.StusId = (short)EmailStatus.Pending;
                            mnsm5.CreatDt = DateTime.Now;
                            m5EventMsgList.Add(mnsm5);
                        }
                    }
                }

                // Send Device Completion for Pre-staged orders when odie device is 'Do Not send to ODIE'
                if (obj.Event.EventCpeDev != null && obj.Event.EventCpeDev.Count() > 0)
                {
                    var cdil = obj.Event.EventCpeDev
                        .Where(i => i.OdieDevNme.Equals("0") && i.RecStusId == (byte)ERecStatus.Active);
                    foreach (var cdi in cdil)
                    {
                        var cpeDev = mdsEventCpeDevView.FirstOrDefault(i => i.CpeOrdrId == cdi.CpeOrdrId && i.DeviceId == cdi.DeviceId);
                        if (!string.IsNullOrWhiteSpace(cdi.CpeOrdrId) && cpeDev != null
                            && !string.IsNullOrWhiteSpace(cpeDev.DlvyClli) && cpeDev.DlvyClli.Length > 3
                            && cpeDev.OrdrId.HasValue && cpeDev.OrdrId.Value > 0)
                        // DB Values for some cpeDev.DLVY_CLLI are 'N/A'
                        // So this is called if cpeDev.DLVY_CLLI is not equal to 'N/A'
                        {
                            // Call CPE Device completion SP for certain CLLI's,
                            // Check getM5EventDatabyH6 for the list
                            List<SqlParameter> pc = new List<SqlParameter>
                            {
                                new SqlParameter() { ParameterName = "@ORDR_ID", SqlDbType = SqlDbType.Int, Value = cpeDev.OrdrId },
                                new SqlParameter() { ParameterName = "@DEVICE_ID", SqlDbType = SqlDbType.VarChar, Value = cdi.DeviceId },
                                new SqlParameter() { ParameterName = "@NOTE", SqlDbType = SqlDbType.VarChar, Value = "V5U Note: Order Auto Completed in BPM, EventID : " + eventId }
                            };

                            _context.Database.ExecuteSqlCommand
                                ("[dbo].[insertOrdrCmplAll_V5U] @ORDR_ID, @DEVICE_ID, @NOTE",
                                pc.ToArray());
                        }
                    }
                }
            }
            //var end55 = DateTime.Now;


            //var end6 = DateTime.Now;
            _context.M5EventMsg.AddRange(m5EventMsgList);
            _context.RedsgnDevEventHist.AddRange(redsgnDevEventHistList);
            _context.EventDevCmplt.AddRange(eventDevCmpltList);
            SaveAll();
            //var end66 = DateTime.Now;

            if (CmplDev)
            {
                if ((!(mdsEventCpeDevView != null && mdsEventCpeDevView.Count() == 1
                    && (!mdsEventCpeDevView[0].CpeCmpntFmly.Contains(","))
                    && _common.IsDeviceExcluded(mdsEventCpeDevView[0].CpeCmpntFmly.Trim())))
                    && bRedesign)
                {
                    OdieReq od = new OdieReq();
                    od.OdieMsgId = (int)OdieMessageType.DesignComplete;
                    od.StusModDt = od.CreatDt = DateTime.Now;
                    od.StusId = (short)EmailStatus.Pending;
                    od.H1CustId = obj.H1;
                    od.MdsEventId = eventId;
                    _context.OdieReq.Add(od);
                }

                SaveAll();

                if (bRedesign)
                    _common.UpdateRedesignFromEvent(model.DevCompletion.ToList(), CmplDev, eventId,
                        eventDevCmpltList.Select(i => i.OdieDevNme).Distinct().ToList());
            }
        }

        private void DeleteAllDevCmplt(MdsEvent obj)
        {
            int eventId = obj.EventId;

            var devcmplnoodie = _context.EventDevCmplt.Where(i => i.EventId == eventId
                                    && !i.OdieSentDt.HasValue);
            _context.EventDevCmplt.RemoveRange(devcmplnoodie);

            var devcmplodie = _context.EventDevCmplt.Where(i => i.EventId == eventId
                                    && i.OdieSentDt.HasValue);
            foreach (var item in devcmplodie)
            {
                if (!obj.Event.EventDevCmplt
                        .Any(dc => dc.OdieDevNme == item.OdieDevNme && dc.RedsgnDevId == item.RedsgnDevId))
                    item.RecStusId = (byte)ERecStatus.InActive;
            }

            SaveAll();
        }

        private DataTable GetAppointmentDetails(int EventID, int ApptID)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.getAppointmentDetailsByEventID", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@EVENT_ID", EventID);
            command.Parameters.AddWithValue("@APPT_ID", ApptID);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public string GenerateICSFile(int eventId, int mode, string loggedInUser, int apptId, string userIds)
        {
            if (mode == 2)
            {
                try
                {
                    //string userName = loggedInUserName;

                    //if (userName.IndexOf("\\") > -1)
                    //    userName = userName.Remove(0, userName.IndexOf("\\") + 1);

                    //if (userName.Length > 0)
                    //{
                    var lu = (from u in _context.LkUser where u.UserAdid == loggedInUser select u).SingleOrDefault();

                    if (lu != null)
                    {
                        var eu = (from eau in _context.EventAsnToUser where ((eau.AsnToUserId == lu.UserId) && (eau.EventId == eventId)) select eau).SingleOrDefault();

                        if (eu != null)
                        {
                            if (eu.RecStusId == 1)
                                mode = 1;
                            else
                                mode = 0;
                        }
                        else
                            mode = 1;
                    }
                    else
                    {
                        mode = 0;
                    }
                    //}
                    //else
                    //    mode = 0;
                }
                catch
                {
                    mode = 0;
                }
            }
            DataTable DT = new DataTable();

            DT = GetAppointmentDetails(eventId, apptId);

            string emails = "";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                emails = emails + DT.Rows[i]["EMAIL_ADR"];
            }

            if ((DT != null) && (DT.Rows.Count > 0))
            {
                StringBuilder sContents = new StringBuilder();

                sContents.AppendLine("BEGIN:VCALENDAR");
                sContents.AppendLine("PRODID:-//TMobile//COWS//EN");
                sContents.AppendLine("VERSION:2.0");
                if (mode == 0)
                    sContents.AppendLine("METHOD:CANCEL");
                else
                    sContents.AppendLine("METHOD:PUBLISH");
                sContents.AppendLine("BEGIN:VEVENT");
                sContents.AppendLine("UID:" + eventId.ToString() + "_" + _configuration.GetSection("AppSettings:COWSRegion").Value);
                if (mode == 0)
                    sContents.AppendLine("SEQUENCE:7");
                else
                    sContents.AppendLine("STATUS:5");
                sContents.AppendLine("DTSTART:" + Convert.ToDateTime(DT.Rows[0]["STRT_TMST"].ToString()).ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
                sContents.AppendLine("DTEND:" + Convert.ToDateTime(DT.Rows[0]["END_TMST"].ToString()).ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));

                //for (int i = 0; i < DT.Rows.Count; i++)
                //{
                //    sContents.AppendLine("ATTENDEE:" + DT.Rows[i]["EMAIL_ADR"]);
                //}

                //if (userIds != null)
                //{
                //    foreach (string userId in userIds.Split(','))
                //    {
                //        LkUser user = _userRepo.GetById(Convert.ToInt32(userId));

                //        if (user != null)
                //        {
                //            sContents.AppendLine("ATTENDEE:" + user.EmailAdr);
                //        }
                //    }
                //}

                sContents.AppendLine("LOCATION:" + DT.Rows[0]["APPT_LOC_TXT"].ToString());

                if (!apptId.ToString().Equals("0"))
                    sContents.AppendLine("APPOINTMENT TYPE: " + DT.Rows[0]["APPT_TYPE_DES"].ToString());
                //sContents.AppendLine("DESCRIPTION;ENCODING=QUOTED-PRINTABLE:" + "Description: " + DT.Rows[0]["EVENT_DES"].ToString());

                sContents.AppendLine("DESCRIPTION; X-ALT-DESC;FMTTYPE = text / html:" + DT.Rows[0]["ONLINE_MEETING_ADR"].ToString()
                        +  "DESCRIPTION: " + DT.Rows[0]["EVENT_DES"].ToString());
                sContents.AppendLine("SUMMARY:" + DT.Rows[0]["SUBJ_TXT"].ToString());
                sContents.AppendLine("PRIORITY:3");
                if (mode == 0)
                    sContents.AppendLine("STATUS:CANCELLED");
                else
                    sContents.AppendLine("STATUS:CONFIRMED");
                sContents.AppendLine("END:VEVENT");
                sContents.AppendLine("END:VCALENDAR");

                //Response.Clear();
                //Response.ContentType = "text/calendar";
                //Response.AddHeader("Content-Disposition", "attachment;filename=" + ((eventId.ToString().Equals("0")) ? apptId.ToString() : eventId.ToString()) + ".ics");
                //Response.Write(sContents);
                //if (Response.IsClientConnected)
                //    Response.Flush();
                //Response.SuppressContent = true;
                //ApplicationInstance.CompleteRequest();

                return sContents.ToString();
            }
            return "";
        }

        private MdsEvent FetchEventMacData(MdsEvent obj)
        {
            ICollection<MdsEventMacActy> mds_mac = _context.MdsEventMacActy
                .Include(a => a.MdsMacActy)
                .Where(a => a.EventId == obj.EventId)
                .ToList();

            obj.Event.MdsEventMacActy = mds_mac;
            return obj;
        }

        private bool CheckMDSNtwkActy(ref MdsEvent Obj, string sMDSNtwkActy)
        {
            bool hasMDS = false, hasNtwk = false, hasVAS = false;
            foreach (var item in Obj.Event.MdsEventNtwkActy)
            {
                if (!hasMDS)
                    hasMDS = (item.NtwkActyTypeId == 1);
                if (!hasNtwk)
                    hasNtwk = (item.NtwkActyTypeId == 2 || item.NtwkActyTypeId == 4);
                if (!hasVAS)
                    hasVAS = (item.NtwkActyTypeId == 3);
            }
            if (sMDSNtwkActy == "hasMDS")
                return hasMDS;
            else if (sMDSNtwkActy == "hasNtwk")
                return hasNtwk;
            else if (sMDSNtwkActy == "hasVAS")
                return hasVAS;
            else if (sMDSNtwkActy == "MDSOnly")
                return hasMDS && !hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "NtwkOnly")
                return !hasMDS && hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "VASOnly")
                return !hasMDS && !hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "MDSNtwk")
                return hasMDS && hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "MDSVAS")
                return hasMDS && !hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "NtwkVAS")
                return !hasMDS && hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "MDSNtwkVAS")
                return hasMDS && hasNtwk && hasVAS;
            else
                return false;
        }
    }
}