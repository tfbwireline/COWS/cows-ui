﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class AccessMbRepository : IAccessMbRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;
        private IMemoryCache _cache;

        public AccessMbRepository(COWSAdminDBContext context, IConfiguration configuration, IMemoryCache cache)
        {
            _context = context;
            _configuration = configuration;
            _cache = cache;
        }


        public LkAccsMb GetById(int id)
        {
            return _context.LkAccsMb
                            .SingleOrDefault(x => x.AccsMbId == id);
        }


        public IQueryable<LkAccsMb> Find(Expression<Func<LkAccsMb, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkAccsMb, out List<LkAccsMb> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();            
        }

        public IEnumerable<LkAccsMb> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkAccsMb, out List<LkAccsMb> list))
            {
                list = _context.LkAccsMb
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkAccsMb, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkAccsMb Create(LkAccsMb entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkAccsMb entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkAccsMb);
            return _context.SaveChanges();
        }

    }
}
