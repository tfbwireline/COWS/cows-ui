﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class WFMRepository : IWFMRepository
    {
        private readonly COWSAdminDBContext _context;
        private Dictionary<string, int> _AUsers;

        public WFMRepository(COWSAdminDBContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public Dictionary<string, int> AUsers
        {
            get { return _AUsers; }
            set { _AUsers = value; }
        }
        public bool GetWFMRole(int userId, string profile)
        {
            int[] grpList;
            if (profile == "GOM")
            {
                grpList = new int[] { 126 };
            }
            else
            {
                grpList = new int[] { 14, 28, 112 };
            }

            if ((from a in _context.MapUsrPrf
                 join lu in _context.LkUser on a.UserId equals lu.UserId
                 where (a.UserId == userId) && (a.RecStusId == 1)
                            && grpList.Contains(a.UsrPrfId)
                 select a).Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataTable GetWFMprofile(int userId, string profile)
        {
            int[] grpList;
            if (profile == "GOM")
            {
                //grpList = new int[] { 114 };
                grpList = new int[] { 126 };
            }
            else
            {
                //grpList = new int[] { 2, 16, 100 };
                grpList = new int[] { 14, 28, 112 };
            }
            DateTime t = new DateTime(2000, 10, 6);
            //Allowing NCI users access to all three groups at once.  The primary group has a normal
            //timestamp, while the non-primary group has the modified date time stamp of 10-06-2000.
            //We only want to recognize the current user's primary roles.

            var s = (from a in _context.UserWfm
                     join lu in _context.LkUser on a.UserId equals lu.UserId
                     where (a.UserId == userId)
                            && (((a.ModfdDt != t) && (a.ModfdDt != null)) || (a.ModfdDt == null))
                            && grpList.Contains((short)a.UsrPrfId)
                     select new
                     {
                         a.UserId,
                         lu.UserAdid,
                         a.UsrPrfId,
                         a.WfmAsmtLvlId,
                         lu.DsplNme
                     }).Distinct();

            return LinqHelper.CopyToDataTable(s, null, null);
        }

        public DataTable GetNCIUser(int prfId)
        {
            var s = (from lu in _context.LkUser
                     join a in _context.UserWfm on lu.UserId equals a.UserId
                     where (a.UsrPrfId == prfId) && (a.RecStusId == 1) && (lu.RecStusId == 1 || lu.RecStusId == 2)
                     select new { lu.UserId, lu.DsplNme }).Distinct().OrderBy("DsplNme");

            return LinqHelper.CopyToDataTable(s, null, null);
        }

        public bool UserValid(string userId, string prodType, string pltfrmType, string subStatus)
        {
            int _SubStat = 0;

            if (subStatus == "PreSubmit") { _SubStat = 1; }
            else { _SubStat = 2; }

            if (pltfrmType != null)
            {
                if ((from a in _context.LkUser
                     join wf in _context.UserWfm on a.UserId equals wf.UserId
                     join lf in _context.LkProdType on wf.ProdTypeId equals lf.ProdTypeId
                     join pc in _context.LkPltfrm on wf.PltfrmCd equals pc.PltfrmCd
                     where (a.DsplNme == userId) && (pc.PltfrmNme == pltfrmType) &&
                     (lf.ProdTypeDes == prodType) && (wf.OrdrActnId == _SubStat)
                     select a).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if ((from a in _context.LkUser
                     join wf in _context.UserWfm on a.UserId equals wf.UserId
                     join lf in _context.LkProdType on wf.ProdTypeId equals lf.ProdTypeId
                     where (a.DsplNme == userId) &&
                     (lf.ProdTypeDes == prodType) && (wf.OrdrActnId == _SubStat)
                     select a).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        //public List<WFMOrdersModel> Select(string SortField)
        //{
        //    return Select(SortField, null);
        //}

        public List<WFMOrdersModel> Select(string sortExpression, string searchCriteria, byte view, string profile)
        {
            DataSet ds = new DataSet();
            Dictionary<string, int> aUsers = new Dictionary<string, int>();
            List<WFMOrdersModel> _List = new List<WFMOrdersModel>();

            if (profile == "GOM")
            {
                SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
                SqlCommand command = new SqlCommand("web.getGOMWFMOrders", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.CommandTimeout = _context.commandTimeout;
                command.Parameters.AddWithValue("@IsOrderInGOMWG", view);
                command.Parameters.AddWithValue("@whereClause", searchCriteria);
                command.Parameters.AddWithValue("@sortBy", sortExpression);
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(ds);
                connection.Close();

                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow DR in ds.Tables[0].Rows)
                    {
                        WFMOrdersModel _WFMOrder = new WFMOrdersModel
                        {
                            ORDR_ID = Convert.ToInt32(DR["ORDR_ID"].ToString()),
                            FTN = DR["FTN"].ToString(),
                            PROD_TYPE = DR["PROD_TYPE"].ToString(),
                            ORDR_TYPE = DR["ORDR_TYPE"].ToString(),
                            PLTFM_TYPE = DR["PLTFM_TYPE"].ToString(),
                            SUB_STATUS = DR["SUB_STATUS"].ToString(),
                            ASSIGNED_USER = DR["ASSIGNED_USER"].ToString(),
                            ASSIGN_DT = Convert.ToDateTime(DR["ASMT_DT"].ToString()),
                            ASSIGNED_BY = DR["ASSIGNED_BY"].ToString(),
                            MGR = DR["MGR"].ToString(),
                            VNDR_NAME = DR["VNDR_NAME"].ToString(),
                            ORDR_RCVD_DT = Convert.ToDateTime(DR["ORDR_RCVD_DT"].ToString()),
                            RTS = DR["RTSflag"].ToString(),
                        };
                        _List.Add(_WFMOrder);
                    }
                }
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        aUsers.Add(dr["ASSIGNED_USER"].ToString(), Convert.ToInt32(dr["CNT"].ToString()));
                    }
                    _AUsers = aUsers;
                }
            }
            else
            {
                SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
                SqlCommand command = new SqlCommand("web.getNCIWFMOrders", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.CommandTimeout = _context.commandTimeout;
                command.Parameters.AddWithValue("@Sort", sortExpression ?? string.Empty);
                command.Parameters.AddWithValue("@Filter", searchCriteria ?? string.Empty);
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(ds);
                connection.Close();

                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow DR in ds.Tables[0].Rows)
                    {
                        WFMOrdersModel _WFMOrder = new WFMOrdersModel
                        {
                            ORDR_ID = Convert.ToInt32(DR["ORDR_ID"].ToString()),
                            FTN = DR["FTN"].ToString(),
                            PROD_TYPE = DR["PROD_TYPE"].ToString(),
                            ORDR_TYPE = DR["ORDR_TYPE"].ToString(),
                            PLTFM_TYPE = DR["PLTFM_TYPE"].ToString(),
                            COUNTRY = DR["COUNTRY"].ToString(),
                            ASSIGNED_USER = DR["ASSIGNED_USER"].ToString(),
                            ASSIGNED_BY = DR["ASSIGNED_BY"].ToString(),
                            REGION = DR["REGION"].ToString(),
                            CUST_NME = DR["CUST_NME"].ToString(),
                            CUST_ADDR = DR["CUST_ADDR"].ToString(),
                            CITY = DR["CITY"].ToString(),
                            RTS = DR["RTS"].ToString()
                        };
                        if (DR["ASSIGN_DT"].ToString() != null && DR["ASSIGN_DT"].ToString() != "")
                            _WFMOrder.ASSIGN_DT = Convert.ToDateTime(DR["ASSIGN_DT"].ToString());

                        _List.Add(_WFMOrder);
                    }
                }
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        aUsers.Add(dr["ASSIGNED_USER"].ToString(), Convert.ToInt32(dr["CNT"].ToString()));
                    }
                    _AUsers = aUsers;
                }
            }
            return _List;
        }

        public DataSet GetWFMData(string sortExpression, string searchCriteria, byte view, string profile)
        {
            DataSet ds = new DataSet();
            Dictionary<string, int> aUsers = new Dictionary<string, int>();
            List<WFMOrdersModel> _List = new List<WFMOrdersModel>();

            if (profile == "GOM")
            {
                SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
                SqlCommand command = new SqlCommand("web.getGOMWFMOrders", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.CommandTimeout = _context.commandTimeout;
                command.Parameters.AddWithValue("@IsOrderInGOMWG", view);
                command.Parameters.AddWithValue("@whereClause", searchCriteria ?? string.Empty);
                command.Parameters.AddWithValue("@sortBy", sortExpression ?? string.Empty);
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(ds);
                connection.Close();

                //if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow DR in ds.Tables[0].Rows)
                //    {
                //        WFMOrdersModel _WFMOrder = new WFMOrdersModel
                //        {
                //            ORDR_ID = Convert.ToInt32(DR["ORDR_ID"].ToString()),
                //            FTN = DR["FTN"].ToString(),
                //            PROD_TYPE = DR["PROD_TYPE"].ToString(),
                //            ORDR_TYPE = DR["ORDR_TYPE"].ToString(),
                //            PLTFM_TYPE = DR["PLTFM_TYPE"].ToString(),
                //            SUB_STATUS = DR["SUB_STATUS"].ToString(),
                //            ASSIGNED_USER = DR["ASSIGNED_USER"].ToString(),
                //            ASSIGN_DT = Convert.ToDateTime(DR["ASMT_DT"].ToString()),
                //            ASSIGNED_BY = DR["ASSIGNED_BY"].ToString(),
                //            MGR = DR["MGR"].ToString(),
                //            VNDR_NAME = DR["VNDR_NAME"].ToString(),
                //            ORDR_RCVD_DT = Convert.ToDateTime(DR["ORDR_RCVD_DT"].ToString()),
                //            RTS = DR["RTSflag"].ToString(),
                //        };
                //        _List.Add(_WFMOrder);
                //    }
                //}
                //if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                //{
                //    foreach (DataRow dr in ds.Tables[1].Rows)
                //    {
                //        aUsers.Add(dr["ASSIGNED_USER"].ToString(), Convert.ToInt32(dr["CNT"].ToString()));
                //    }
                //    _AUsers = aUsers;
                //}
            }
            else
            {
                SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
                SqlCommand command = new SqlCommand("web.getNCIWFMOrders", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.CommandTimeout = _context.commandTimeout;
                command.Parameters.AddWithValue("@Sort", sortExpression ?? string.Empty);
                command.Parameters.AddWithValue("@Filter", searchCriteria ?? string.Empty);
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(ds);
                connection.Close();

                //if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow DR in ds.Tables[0].Rows)
                //    {
                //        WFMOrdersModel _WFMOrder = new WFMOrdersModel
                //        {
                //            ORDR_ID = Convert.ToInt32(DR["ORDR_ID"].ToString()),
                //            FTN = DR["FTN"].ToString(),
                //            PROD_TYPE = DR["PROD_TYPE"].ToString(),
                //            ORDR_TYPE = DR["ORDR_TYPE"].ToString(),
                //            PLTFM_TYPE = DR["PLTFM_TYPE"].ToString(),
                //            COUNTRY = DR["COUNTRY"].ToString(),
                //            ASSIGNED_USER = DR["ASSIGNED_USER"].ToString(),
                //            ASSIGNED_BY = DR["ASSIGNED_BY"].ToString(),
                //            REGION = DR["REGION"].ToString(),
                //            CUST_NME = DR["CUST_NME"].ToString(),
                //            CUST_ADDR = DR["CUST_ADDR"].ToString(),
                //            CITY = DR["CITY"].ToString(),
                //            RTS = DR["RTS"].ToString()
                //        };
                //        if (DR["ASSIGN_DT"].ToString() != null && DR["ASSIGN_DT"].ToString() != "")
                //            _WFMOrder.ASSIGN_DT = Convert.ToDateTime(DR["ASSIGN_DT"].ToString());

                //        _List.Add(_WFMOrder);
                //    }
                //}
                //if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                //{
                //    foreach (DataRow dr in ds.Tables[1].Rows)
                //    {
                //        aUsers.Add(dr["ASSIGNED_USER"].ToString(), Convert.ToInt32(dr["CNT"].ToString()));
                //    }
                //    _AUsers = aUsers;
                //}
            }
            return ds;
        }

        public void MakeNCIAssignments(WFMOrdersModel _WFMOrder)
        {
            int _Userid = 0;
            int _Assignerid = 0;
            string _AssignedADID = string.Empty;

            if (_WFMOrder.ASSIGNED_USER != "")
            {
                var a = (from lu in _context.LkUser
                         where lu.DsplNme == _WFMOrder.ASSIGNED_USER
                          && lu.RecStusId == 1
                         select lu).SingleOrDefault();

                if (a != null)
                    _AssignedADID = a.UserAdid;
            }

            if (_WFMOrder.ASSIGNED_USER != _WFMOrder.NEWASSIGNEE)
            {
                if (_WFMOrder.ASSIGNED_USER != string.Empty)
                {
                    var sel = (from lku in _context.LkUser
                               where lku.DsplNme == _WFMOrder.NEWASSIGNEE
                                && lku.RecStusId == 1
                               select lku).SingleOrDefault();

                    if (sel != null)
                        _Userid = sel.UserId;

                    var sl = (from u in _context.LkUser
                              where u.UserId == Convert.ToInt32(_WFMOrder.ASSIGNED_BY)
                              select u).SingleOrDefault();

                    if (sl != null)
                        _Assignerid = sl.UserId;

                    bool IPLOrdr = false;

                    if (_WFMOrder.PROD_TYPE.StartsWith("IPL ")) { IPLOrdr = true; }

                    if (IPLOrdr == false)
                    {
                        //var ordid = (from fo in _context.FsaOrdr
                        //             where fo.Ftn == _WFMOrder.FTN
                        //             select fo).SingleOrDefault();

                        //if (ordid != null)
                        //    _WFMOrder.ORDR_ID = ordid.OrdrId;
                    }
                    short PRF_ID = 0;
                    short GRP_ID = 0;

                    //if (_WFMOrder.REGION == "AMNCI") { PRF_ID = 2; GRP_ID = 5;  }
                    //if (_WFMOrder.REGION == "ENCI") { PRF_ID = 100; GRP_ID = 7;  }
                    //if (_WFMOrder.REGION == "ANCI") { PRF_ID = 16; GRP_ID = 6;  }
                    if (_WFMOrder.REGION == "AMNCI") { PRF_ID = 14; GRP_ID = 5; }
                    if (_WFMOrder.REGION == "ENCI") { PRF_ID = 112; GRP_ID = 7; }
                    if (_WFMOrder.REGION == "ANCI") { PRF_ID = 28; GRP_ID = 6; }

                    if ((from a in _context.UserWfmAsmt
                         where (a.OrdrId == _WFMOrder.ORDR_ID) &&
                         (a.UsrPrfId == PRF_ID) && (a.AsnUserId == _Userid)
                         select a).Count() > 0)
                    {
                    }
                    else
                    {
                        lock (this)
                        {
                            UserWfmAsmt _WFM = new UserWfmAsmt
                            {
                                OrdrId = _WFMOrder.ORDR_ID,
                                GrpId = GRP_ID,
                                UsrPrfId = PRF_ID,
                                AsnUserId = _Userid,
                                AsnByUserId = Convert.ToInt32(_WFMOrder.ASSIGNED_BY),
                                AsmtDt = DateTime.Now,
                                CreatDt = DateTime.Now,
                                OrdrHighlightCd = true,
                                OrdrHighlightModfdDt = DateTime.Now
                            };

                            _context.UserWfmAsmt.Add(_WFM);
                            _context.SaveChanges();
                        }
                    }

                }
                else
                {
                    var sel = (from lku in _context.LkUser
                               where lku.DsplNme == _WFMOrder.NEWASSIGNEE
                                && lku.RecStusId == 1
                               select lku).SingleOrDefault();

                    if (sel != null)
                        _Userid = sel.UserId;

                    bool IPLOrdr = false;

                    if (_WFMOrder.PROD_TYPE.StartsWith("IPL ")) { IPLOrdr = true; }

                    if (IPLOrdr == false)
                    {
                        //var ordid = (from fo in _context.FsaOrdr
                        //             where fo.Ftn == _WFMOrder.FTN
                        //             select fo).SingleOrDefault();

                        //if (ordid != null)
                        //    _WFMOrder.ORDR_ID = ordid.OrdrId;
                    }

                    short PRF_ID = 0;
                    short GRP_ID = 0;

                    //if (_WFMOrder.REGION == "AMNCI") { PRF_ID = 2; GRP_ID = 5; }
                    //if (_WFMOrder.REGION == "ENCI") { PRF_ID = 100; GRP_ID = 7; }
                    //if (_WFMOrder.REGION == "ANCI") { PRF_ID = 16; GRP_ID = 6; }
                    if (_WFMOrder.REGION == "AMNCI") { PRF_ID = 14; GRP_ID = 5; }
                    if (_WFMOrder.REGION == "ENCI") { PRF_ID = 112; GRP_ID = 7; }
                    if (_WFMOrder.REGION == "ANCI") { PRF_ID = 28; GRP_ID = 6; }

                    lock (this)
                    {
                        UserWfmAsmt _WFM = new UserWfmAsmt
                        {
                            OrdrId = _WFMOrder.ORDR_ID,
                            GrpId = GRP_ID,
                            UsrPrfId = PRF_ID,
                            AsnUserId = _Userid,
                            AsnByUserId = Convert.ToInt32(_WFMOrder.ASSIGNED_BY),
                            AsmtDt = DateTime.Now,
                            CreatDt = DateTime.Now,
                            OrdrHighlightCd = true,
                            OrdrHighlightModfdDt = DateTime.Now
                        };

                        _context.UserWfmAsmt.Add(_WFM);
                        _context.SaveChanges();
                    }

                }

                LkUser una = (from lu in _context.LkUser
                              where lu.DsplNme == _WFMOrder.NEWASSIGNEE
                              && lu.RecStusId == 1
                              select lu).SingleOrDefault();

                if (una != null)
                    una.UserOrdrCnt += GetXNCIOrderWeightageNumber(_WFMOrder); ;

                _context.SaveChanges();
            }
        }

        public void MakeNCIUnAssignments(WFMOrdersModel _WFMOrder)
        {
            bool IPLOrdr = false;
            int _Userid = 0;

            var sel = (from lku in _context.LkUser
                       where lku.DsplNme == _WFMOrder.ASSIGNED_USER
                       && lku.RecStusId == 1
                       select lku).SingleOrDefault();

            if (sel != null)
                _Userid = sel.UserId;

            if (_WFMOrder.PROD_TYPE.StartsWith("IPL ")) { IPLOrdr = true; }

            if (IPLOrdr == false)
            {
                //var ordid = (from fo in _context.FsaOrdr
                //             where fo.Ftn == _WFMOrder.FTN
                //             select fo).SingleOrDefault();

                //if (ordid != null)
                //    _WFMOrder.ORDR_ID = ordid.OrdrId;
            }

            short PRF_ID = 0;

            //if (_WFMOrder.REGION == "AMNCI") { PRF_ID = 2; }
            //if (_WFMOrder.REGION == "ENCI") { PRF_ID = 100; }
            //if (_WFMOrder.REGION == "ANCI") { PRF_ID = 16; }
            if (_WFMOrder.REGION == "AMNCI") { PRF_ID = 14; }
            if (_WFMOrder.REGION == "ENCI") { PRF_ID = 112; }
            if (_WFMOrder.REGION == "ANCI") { PRF_ID = 28; }

            var ordfetch = (from ofetch in _context.UserWfmAsmt
                            where ofetch.OrdrId == _WFMOrder.ORDR_ID && ofetch.UsrPrfId == PRF_ID
                                    && ofetch.AsnUserId == _Userid
                            select ofetch).Single();

            _context.UserWfmAsmt.Remove(ordfetch);

            _context.SaveChanges();


            LkUser una = (from lu in _context.LkUser
                          where lu.DsplNme == _WFMOrder.ASSIGNED_USER
                          && lu.RecStusId == 1
                          select lu).SingleOrDefault();

            if (una != null)
                una.UserOrdrCnt -= GetXNCIOrderWeightageNumber(_WFMOrder);

            _context.SaveChanges();

        }
        public void MakeGOMAssignments(WFMOrdersModel _WFMOrder)
        {
            int _Userid = 0;
            int _Assignerid = 0;

            if (_WFMOrder.ASSIGNED_USER != _WFMOrder.NEWASSIGNEE)
            {
                if (_WFMOrder.ASSIGNED_USER != string.Empty)
                {
                    if (_WFMOrder.SUB_STATUS == "PreSubmit")
                    {
                        LkUser ou = (from lu in _context.LkUser
                                     where lu.DsplNme == _WFMOrder.ASSIGNED_USER
                                     && lu.RecStusId == 1
                                     select lu).SingleOrDefault();

                        if (ou != null)
                        {
                            ou.UserOrdrCnt -= 1;
                            _context.SaveChanges();
                        }
                    }
                    var sel = (from lku in _context.LkUser
                               where lku.DsplNme == _WFMOrder.NEWASSIGNEE
                               && lku.RecStusId == 1
                               select lku).SingleOrDefault();

                    if (sel != null)
                        _Userid = sel.UserId;

                    var sl = (from u in _context.LkUser
                              where u.UserId == Convert.ToInt32(_WFMOrder.ASSIGNED_BY)
                              && u.RecStusId == 1
                              select u).SingleOrDefault();

                    if (sl != null)
                        _Assignerid = sl.UserId;

                    bool IPLOrdr = false;

                    if (_WFMOrder.PROD_TYPE.StartsWith("IPL ")) { IPLOrdr = true; }

                    if (IPLOrdr == false)
                    {
                        //var ordid = (from fo in _context.FsaOrdr
                        //             where fo.Ftn == Convert.ToString(_WFMOrder.FTN)
                        //             select fo).SingleOrDefault();

                        //if (ordid != null)
                        //    _WFMOrder.ORDR_ID = ordid.OrdrId;
                    }

                    short PRF_ID = 126;

                    var ordfetch = (from ofetch in _context.UserWfmAsmt
                                    where ofetch.OrdrId == _WFMOrder.ORDR_ID && ofetch.UsrPrfId == PRF_ID
                                    select ofetch).SingleOrDefault();

                    if (ordfetch != null)
                    {
                        _context.UserWfmAsmt.Remove(ordfetch);
                        _context.SaveChanges();
                    }

                    lock (this)
                    {
                        UserWfmAsmt _WFM = new UserWfmAsmt
                        {
                            OrdrId = _WFMOrder.ORDR_ID,
                            GrpId = 1,
                            UsrPrfId = PRF_ID,
                            AsnUserId = _Userid,
                            AsnByUserId = _Assignerid,
                            AsmtDt = DateTime.Now,
                            CreatDt = DateTime.Now
                        };

                        _context.UserWfmAsmt.Add(_WFM);
                        _context.SaveChanges();
                    }

                    if (_WFMOrder.SUB_STATUS == "PreSubmit")
                    {
                        LkUser una = (from lu in _context.LkUser
                                      where lu.DsplNme == _WFMOrder.NEWASSIGNEE
                                      && lu.RecStusId == 1
                                      select lu).SingleOrDefault();

                        if (una != null)
                            una.UserOrdrCnt += 1;

                        _context.SaveChanges();
                    }

                }
                else
                {
                    var sel = (from lku in _context.LkUser
                               where lku.DsplNme == _WFMOrder.NEWASSIGNEE
                               && lku.RecStusId == 1
                               select lku).SingleOrDefault();

                    if (sel != null)
                        _Userid = sel.UserId;

                    bool IPLOrdr = false;

                    if (_WFMOrder.PROD_TYPE.StartsWith("IPL ")) { IPLOrdr = true; }

                    if (IPLOrdr == false)
                    {
                        //var ordid = (from fo in _context.FsaOrdr
                        //             where fo.Ftn == _WFMOrder.FTN.ToString()
                        //             select fo).SingleOrDefault();

                        //if (ordid != null)
                        //    _WFMOrder.ORDR_ID = ordid.OrdrId;
                    }

                    short PRF_ID = 126;

                    lock (this)
                    {
                        UserWfmAsmt _WFM = new UserWfmAsmt
                        {
                            OrdrId = _WFMOrder.ORDR_ID,
                            GrpId = 1,
                            UsrPrfId = PRF_ID,
                            AsnUserId = _Userid,
                            AsnByUserId = Convert.ToInt32(_WFMOrder.ASSIGNED_BY),
                            AsmtDt = DateTime.Now,
                            CreatDt = DateTime.Now
                        };

                        _context.UserWfmAsmt.Add(_WFM);
                        _context.SaveChanges();
                    }

                    if (_WFMOrder.SUB_STATUS == "PreSubmit")
                    {
                        LkUser una = (from lu in _context.LkUser
                                      where lu.DsplNme == _WFMOrder.NEWASSIGNEE
                                      && lu.RecStusId == 1
                                      select lu).SingleOrDefault();

                        if (una != null)
                            una.UserOrdrCnt += 1;

                        _context.SaveChanges();
                    }

                }
            }
        }

        public void MakeGOMUnAssignments(WFMOrdersModel _WFMOrder)
        {
            bool IPLOrdr = false;
            int _Userid = 0;

            var sel = (from lku in _context.LkUser
                       where lku.DsplNme == _WFMOrder.ASSIGNED_USER
                       && lku.RecStusId == 1
                       select lku).SingleOrDefault();

            if (sel != null)
                _Userid = sel.UserId;

            if (_WFMOrder.PROD_TYPE.StartsWith("IPL ")) { IPLOrdr = true; }

            if (IPLOrdr == false)
            {
                //var ordid = (from fo in _context.FsaOrdr
                //             where fo.Ftn == _WFMOrder.FTN.ToString()
                //             select fo).SingleOrDefault();

                //if (ordid != null)
                //    _WFMOrder.ORDR_ID = ordid.OrdrId;
            }

            short PRF_ID = 126;

            var ordfetch = (from ofetch in _context.UserWfmAsmt
                            where ofetch.OrdrId == _WFMOrder.ORDR_ID && ofetch.UsrPrfId == PRF_ID
                            && ofetch.AsnUserId == _Userid
                            select ofetch).SingleOrDefault();

            _context.UserWfmAsmt.Remove(ordfetch);
            _context.SaveChanges();

            if (_WFMOrder.SUB_STATUS == "PreSubmit")
            {
                LkUser una = (from lu in _context.LkUser
                              where lu.DsplNme == _WFMOrder.ASSIGNED_USER
                              && lu.RecStusId == 1
                              select lu).SingleOrDefault();

                if (una != null)
                    una.UserOrdrCnt -= 1;

                _context.SaveChanges();
            }

        }

        public int InsertOrderNotes(int orderID, int noteTypeID, int userID, string notes)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@NTE_TYPE_ID", SqlDbType.Int),
                    new SqlParameter("@NTE_TXT", SqlDbType.VarChar),
                    new SqlParameter("@CREAT_BY_USER_ID", SqlDbType.Int)

                };
            pc[0].Value = orderID;
            pc[1].Value = noteTypeID;
            pc[2].Value = notes;
            pc[3].Value = userID;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertOrderNotes @ORDR_ID, @NTE_TYPE_ID, @NTE_TXT, @CREAT_BY_USER_ID", pc.ToArray());
            return toReturn;
        }

        public decimal GetXNCIOrderWeightageNumber(WFMOrdersModel _WFMOrder)
        {
            decimal nciOW = 0;

            var ordid = from od in _context.Ordr
                        join lp in _context.LkPprt on od.PprtId equals lp.PprtId into lpjoin
                        from lp in lpjoin.DefaultIfEmpty()
                        join lot in _context.LkOrdrType on lp.OrdrTypeId equals lot.OrdrTypeId into lotjoin
                        from lot in lotjoin.DefaultIfEmpty()
                        where od.OrdrId == _WFMOrder.ORDR_ID
                        select lot.XnciWfmOrdrWeightageNbr;

            if (ordid != null && ordid.Count() > 0)
            {
                nciOW = (decimal)ordid.First();
            }

            return nciOW;
        }

        public bool GetUserAssignmentColorCD(int orderID, int wgID, int userID)
        {
            var c = (from uwa in _context.UserWfmAsmt
                     where uwa.OrdrId == orderID
                         //&& uwa.GrpId == wgID
                         && uwa.UsrPrfId == wgID
                         && uwa.AsnUserId == userID
                         && uwa.OrdrHighlightCd == true
                     select uwa);

            if (c != null && c.Count() > 0)
                return true;

            return false;
        }

        public bool UpdateUserAssignmentColorCD(int orderID, int wgID, int userID)
        {
            var c = (from uwa in _context.UserWfmAsmt
                     where uwa.OrdrId == orderID
                         //&& uwa.GrpId == wgID
                         && uwa.UsrPrfId == wgID
                         && uwa.AsnUserId == userID
                     select uwa).SingleOrDefault();

            if (c != null)
            {
                UserWfmAsmt _uwa = (UserWfmAsmt)c;
                _uwa.OrdrHighlightCd = false;
                _uwa.OrdrHighlightModfdDt = DateTime.Now;

                _context.SaveChanges();
                return true;
            }
            return false;
        }

    }
}