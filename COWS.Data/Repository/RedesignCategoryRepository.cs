﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class RedesignCategoryRepository : IRedesignCategoryRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public RedesignCategoryRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkRedsgnCat> Find(Expression<Func<LkRedsgnCat, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkRedsgnCat, out List<LkRedsgnCat> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkRedsgnCat> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkRedsgnCat, out List<LkRedsgnCat> list))
            {
                list = _context.LkRedsgnCat
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkRedsgnCat, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkRedsgnCat GetById(int id)
        {
            return _context.LkRedsgnCat
                .SingleOrDefault(a => a.RedsgnCatId == id);
        }

        public LkRedsgnCat Create(LkRedsgnCat entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkRedsgnCat entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkRedsgnCat);
            return _context.SaveChanges();
        }
    }
}