﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class ServiceTierRepository : IServiceTierRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public ServiceTierRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMdsSrvcTier Create(LkMdsSrvcTier entity)
        {
            int maxId = _context.LkMdsSrvcTier.Max(i => i.MdsSrvcTierId);
            entity.MdsSrvcTierId = (byte)++maxId;
            _context.LkMdsSrvcTier.Add(entity);

            SaveAll();

            return GetById(entity.MdsSrvcTierId);
        }

        public void Delete(int id)
        {
            LkMdsSrvcTier st = GetById(id);
            _context.LkMdsSrvcTier.Remove(st);

            SaveAll();
        }

        public IQueryable<LkMdsSrvcTier> Find(Expression<Func<LkMdsSrvcTier, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsSrvcTier, out List<LkMdsSrvcTier> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMdsSrvcTier> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsSrvcTier, out List<LkMdsSrvcTier> list))
            {
                list = _context.LkMdsSrvcTier
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMdsSrvcTier, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMdsSrvcTier GetById(int id)
        {
            return _context.LkMdsSrvcTier
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.MdsSrvcTierId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMdsSrvcTier);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMdsSrvcTier entity)
        {
            LkMdsSrvcTier tier = GetById(id);
            tier.MdsSrvcTierDes = entity.MdsSrvcTierDes;
            tier.EventTypeId = entity.EventTypeId;
            tier.RecStusId = entity.RecStusId;
            tier.ModfdByUserId = entity.ModfdByUserId;
            tier.ModfdDt = entity.ModfdDt;
            
            SaveAll();
        }
    }
}