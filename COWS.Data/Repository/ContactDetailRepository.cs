﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class ContactDetailRepository : IContactDetailRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _config;
        private IMemoryCache _cache;
        private readonly IUserRepository _userRepo;

        public ContactDetailRepository(COWSAdminDBContext context, IConfiguration config, IMemoryCache cache, IUserRepository userRepo)
        {
            _context = context;
            _config = config;
            _cache = cache;
            _userRepo = userRepo;
        }

        public CntctDetl Create(CntctDetl entity)
        {
            _context.CntctDetl.Add(entity);
            SaveAll();

            return GetById(entity.Id);
        }

        public void Create(IEnumerable<CntctDetl> entity)
        {
            _context.CntctDetl.AddRange(entity);
            SaveAll();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<CntctDetl> Find(Expression<Func<CntctDetl, bool>> predicate)
        {
            return _context.CntctDetl
                            .Include(i => i.Role)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Where(predicate);
        }

        public IEnumerable<CntctDetl> GetAll()
        {
            return _context.CntctDetl
                            .Include(i => i.Role)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser);
        }

        public CntctDetl GetById(int id)
        {
            return _context.CntctDetl
                            .Include(i => i.Role)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.Id == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, CntctDetl entity)
        {
            CntctDetl cntctDetail = GetById(id);
            cntctDetail.EmailAdr = entity.EmailAdr;
            cntctDetail.PhnNbr = entity.PhnNbr;
            cntctDetail.EmailCd = entity.EmailCd;
            cntctDetail.AutoRfrshCd = entity.AutoRfrshCd;
            cntctDetail.SuprsEmail = entity.SuprsEmail;
            cntctDetail.RecStusId = entity.RecStusId;
            cntctDetail.ModfdByUserId = entity.ModfdByUserId;
            cntctDetail.ModfdDt = entity.ModfdDt;

            SaveAll();
        }

        public void Update(IEnumerable<CntctDetl> entity)
        {
            foreach (CntctDetl cd in entity)
            {
                if (cd.Id > 0)
                {
                    CntctDetl cntctDetail = GetById(cd.Id);
                    cntctDetail.EmailAdr = cd.EmailAdr;
                    cntctDetail.PhnNbr = cd.PhnNbr;
                    cntctDetail.EmailCd = cd.EmailCd;
                    cntctDetail.AutoRfrshCd = cd.AutoRfrshCd;
                    cntctDetail.SuprsEmail = cd.SuprsEmail;
                    cntctDetail.RecStusId = cd.RecStusId;
                    cntctDetail.ModfdByUserId = cd.ModfdByUserId;
                    cntctDetail.ModfdDt = cd.ModfdDt;

                    _context.CntctDetl.Update(cntctDetail);
                }
                else
                {
                    _context.CntctDetl.Add(cd);
                }
            }
                
            SaveAll();
        }

        public void SetInactive(Expression<Func<CntctDetl, bool>> predicate)
        {
            _context.CntctDetl
                .Where(predicate)
                .ToList()
                .ForEach(a => {
                    a.RecStusId = (byte)0;
                    a.ModfdDt = DateTime.Now;
                });
        }

        public DataTable GetContactDetails(int objId, string objType, int loggedInUserId, string hierId = null, string hierLvl = null, 
            string odieCustId = null, bool isNetworkOnly = false)
        {
            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.getContactDetails", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ObjID", objId);
            command.Parameters.AddWithValue("@HierID", hierId ?? "");
            command.Parameters.AddWithValue("@HierLvl", hierLvl ?? "");
            command.Parameters.AddWithValue("@ObjType", objType);
            command.Parameters.AddWithValue("@CustId", odieCustId ?? "");
            //command.Parameters.AddWithValue("@RoleId", "");
            //command.Parameters.AddWithValue("@Email", "");
            command.Parameters.AddWithValue("@LoggedInUser", loggedInUserId);
            command.Parameters.AddWithValue("@IsViaJob", 0);
            //command.Parameters.AddWithValue("@H6", h6 ?? "");
            command.Parameters.AddWithValue("@IsNetworkOnly", isNetworkOnly);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public List<string> GetEmailAddresses(int ObjId, string ObjTypeCd, string EmailCd)
        {
            var contactDetails = _context.CntctDetl
                .Where(a => a.ObjId == ObjId && a.ObjTypCd == ObjTypeCd && a.RecStusId == 1)
                .ToList();

            List<string> emailAddresses = new List<string>();
            contactDetails.ForEach(a => {
                string emailCd = a.EmailCd;

                if (emailCd.Split(",").Where(b => b == EmailCd).Any() && !a.SuprsEmail)
                {
                    emailAddresses.Add(a.EmailAdr);
                }
            });

            return emailAddresses;
        }


        public List<string> GetEmailAddresses(int ObjId, string ObjTypeCd, string EmailCd, ref List<int> contactDetailUserIds)
        {
            var contactDetails = _context.CntctDetl
                .Where(a => a.ObjId == ObjId && a.ObjTypCd == ObjTypeCd && a.RecStusId == 1)
                .ToList();

            List<int> userIds = new List<int>();
            List<string> emailAddresses = new List<string>();
            contactDetails.ForEach(a => {
                string emailCd = a.EmailCd;

                if (emailCd.Split(",").Where(b => b == EmailCd).Any() && !a.SuprsEmail)
                {
                    var user = _userRepo
                        .Find(b => b.EmailAdr.ToUpper() == a.EmailAdr.ToUpper() && b.RecStusId == (byte)1)
                        .FirstOrDefault();

                    if (user != null)
                    {
                        userIds.Add(user.UserId);
                    }

                    emailAddresses.Add(a.EmailAdr);
                }
            });

            contactDetailUserIds = userIds;

            return emailAddresses;
        }
    }
}
