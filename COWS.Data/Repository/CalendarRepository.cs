﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace COWS.Data.Repository
{
    public class CalendarRepository : ICalendarRepository
    {
        private readonly COWSAdminDBContext _context;

        public CalendarRepository(COWSAdminDBContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        //public IEnumerable<CalendarData> GetCalendarData(int isWFCalendar = 1, int userId = 0, int apptTypeId = 0, int csgLvlId = 0)
        //{
        //    List<SqlParameter> pc = new List<SqlParameter>
        //    {
        //        new SqlParameter("@IsWFCalendar", isWFCalendar),
        //        new SqlParameter("@USER_ID", userId),
        //        new SqlParameter("@APPT_TYPE_ID", apptTypeId),
        //        new SqlParameter("@CSGLvlId", csgLvlId)
        //    };

        //    var calendarData = _context.Query<CalendarData>()
        //        .AsNoTracking()
        //        .FromSql("web.getCalendarData @IsWFCalendar, @USER_ID, @APPT_TYPE_ID, @CSGLvlId", pc.ToArray()).ToList();

        //    return calendarData;
        //}

        //public bool UpdateCalendarData(int apptId, DateTime strtTmst, DateTime EndTmst, string rcurncDesTxt, string subjTxt, string des, string apptLocTxt, int apptTypeId, int creatByUserId, int modfdByUserId, string asnToUserIdListTxt, int rcurncCd, int delFlg)
        //{
        //    try
        //    {
        //        List<SqlParameter> pc = new List<SqlParameter>
        //        {
        //            new SqlParameter() {ParameterName = "@APPT_ID", SqlDbType = SqlDbType.Int, Value = apptId},
        //            new SqlParameter() {ParameterName = "@STRT_TMST", SqlDbType = SqlDbType.SmallDateTime, Value = strtTmst},
        //            new SqlParameter() {ParameterName = "@END_TMST", SqlDbType = SqlDbType.SmallDateTime, Value = EndTmst},
        //            new SqlParameter() {ParameterName = "@RCURNC_DES_TXT", SqlDbType = SqlDbType.VarChar, Value = rcurncDesTxt},
        //            new SqlParameter() {ParameterName = "@SUBJ_TXT", SqlDbType = SqlDbType.VarChar, Value = subjTxt},
        //            new SqlParameter() {ParameterName = "@DES", SqlDbType = SqlDbType.VarChar, Value = des},
        //            new SqlParameter() {ParameterName = "@APPT_LOC_TXT", SqlDbType = SqlDbType.VarChar, Value = apptLocTxt},
        //            new SqlParameter() {ParameterName = "@APPT_TYPE_ID", SqlDbType = SqlDbType.Int, Value = apptTypeId},
        //            new SqlParameter() {ParameterName = "@CREAT_BY_USER_ID", SqlDbType = SqlDbType.Int, Value = creatByUserId},
        //            new SqlParameter() {ParameterName = "@MODFD_BY_USER_ID", SqlDbType = SqlDbType.Int, Value = modfdByUserId},
        //            new SqlParameter() {ParameterName = "@ASN_TO_USER_ID_LIST_TXT", SqlDbType = SqlDbType.VarChar, Value = asnToUserIdListTxt},
        //            new SqlParameter() {ParameterName = "@RCURNC_CD", SqlDbType = SqlDbType.Bit, Value = rcurncCd},
        //            new SqlParameter() {ParameterName = "@DELFLG", SqlDbType = SqlDbType.Bit, Value = delFlg}
        //        };

        //        var updateCalendar = _context.Database.ExecuteSqlCommand("web.UpdateCalendarData @APPT_ID, @STRT_TMST, @END_TMST, @RCURNC_DES_TXT," +
        //            "@SUBJ_TXT, @DES, @APPT_LOC_TXT, @APPT_TYPE_ID, @CREAT_BY_USER_ID, @MODFD_BY_USER_ID, @ASN_TO_USER_ID_LIST_TXT, @RCURNC_CD, @DELFLG", pc.ToArray());

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        public bool CreateUpdateCalendarData(CalendarData model)
        {
            try
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() {ParameterName = "@APPT_ID", SqlDbType = SqlDbType.Int, Value = model.ApptId},
                    new SqlParameter() {ParameterName = "@STRT_TMST", SqlDbType = SqlDbType.SmallDateTime, Value = model.StrtTmst},
                    new SqlParameter() {ParameterName = "@END_TMST", SqlDbType = SqlDbType.SmallDateTime, Value = model.EndTmst},
                    new SqlParameter() {ParameterName = "@RCURNC_DES_TXT", SqlDbType = SqlDbType.VarChar, Value = model.RcurncDesTxt},
                    new SqlParameter() {ParameterName = "@SUBJ_TXT", SqlDbType = SqlDbType.VarChar, Value = model.SubjTxt.Length > 1000 ? model.SubjTxt.Substring(0,1000):model.SubjTxt},
                    new SqlParameter() {ParameterName = "@DES", SqlDbType = SqlDbType.VarChar, Value = model.Des.Length>1000?model.Des.Substring(0,1000):model.Des},
                    new SqlParameter() {ParameterName = "@APPT_LOC_TXT", SqlDbType = SqlDbType.VarChar, Value = model.ApptLocTxt},
                    new SqlParameter() {ParameterName = "@APPT_TYPE_ID", SqlDbType = SqlDbType.Int, Value = model.ApptTypeId},
                    new SqlParameter() {ParameterName = "@CREAT_BY_USER_ID", SqlDbType = SqlDbType.Int, Value = model.CreatByUserId},
                    new SqlParameter() {ParameterName = "@MODFD_BY_USER_ID", SqlDbType = SqlDbType.Int, Value = model.ModfdByUserId},
                    new SqlParameter() {ParameterName = "@ASN_TO_USER_ID_LIST_TXT", SqlDbType = SqlDbType.VarChar, Value = model.AsnToUserIdListTxt},
                    new SqlParameter() {ParameterName = "@RCURNC_CD", SqlDbType = SqlDbType.Bit, Value = model.RcurncCd},
                    new SqlParameter() {ParameterName = "@DELFLG", SqlDbType = SqlDbType.Bit, Value = model.DelFlg}
                };

                var updateCalendar = _context.Database.ExecuteSqlCommand("web.UpdateCalendarData @APPT_ID, @STRT_TMST, @END_TMST, @RCURNC_DES_TXT," +
                    "@SUBJ_TXT, @DES, @APPT_LOC_TXT, @APPT_TYPE_ID, @CREAT_BY_USER_ID, @MODFD_BY_USER_ID, @ASN_TO_USER_ID_LIST_TXT, @RCURNC_CD, @DELFLG", pc.ToArray());

               if(model.DelFlg == 0 && model.ApptId == -1) // Get latest record created.
                {
                    model.ApptId = _context.Appt.Select(x => x.ApptId)
                        .OrderByDescending(x => x)
                        .FirstOrDefault();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataSet GetApptTypes(int userId, byte isMain, byte isWF)
        {
            SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.getAPPTTypes", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userId);
            command.Parameters.AddWithValue("@IsMain", isMain);
            command.Parameters.AddWithValue("@IsNewUI", 1);
            command.Parameters.AddWithValue("@IsWF", isWF);
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;
        }

        public DataSet GetAppointmentData(int isWFCalendar = 1, int userId = 0, int apptTypeId = 0, int csgLvlId = 0)
        {
            SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.getCalendarData", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IsWFCalendar", isWFCalendar);
            command.Parameters.AddWithValue("@USER_ID", userId);
            command.Parameters.AddWithValue("@APPT_TYPE_ID", apptTypeId);
            command.Parameters.AddWithValue("@CSGLvlId", csgLvlId);
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            //if ((ds != null) && (ds.Tables.Count>0) && (ds.Tables[0].Rows.Count>0))
            //{
            //    foreach(DataRow dr in ds.Tables[0].Rows)
            //    {
            //        if (!dr["RCURNC_DES_TXT"].ToString().Equals(string.Empty))
            //        {
            //            var appointment = StaticAppointmentFactory.CreateAppointment(AppointmentType.Pattern);
            //            var pattern = dr["RCURNC_DES_TXT"].ToString();
            //            appointment.RecurrenceInfo.FromXml(pattern, DateTimeSavingMode.Utc);
            //            appointment.Start = DateTime.Parse(dr["STRT_TMST"].ToString(), CultureInfo.CurrentCulture, DateTimeStyles.RoundtripKind).ToLocalTime();
            //            appointment.End = DateTime.Parse(dr["END_TMST"].ToString(), CultureInfo.CurrentCulture, DateTimeStyles.RoundtripKind).ToLocalTime();
            //            appointment.AllDay = true;

            //            string s = DevExpress.XtraScheduler.iCalendar.iCalendarHelper.ExtractRecurrenceRule(appointment.RecurrenceInfo);
            //            dr["RCURNC_DES_TXT"] = s;
            //        }
            //    }
            //}
            return ds;
        }

        public DataSet GetAppointmentDetailsByApptID(int ApptID)
        {
            SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.getAppointmentDetailsByApptID", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@APPT_ID", ApptID);
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;
        }
    }
}