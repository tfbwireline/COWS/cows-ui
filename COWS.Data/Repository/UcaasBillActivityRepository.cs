﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UcaasBillActivityRepository : IUcaasBillActivityRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public UcaasBillActivityRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkUcaaSBillActy Create(LkUcaaSBillActy entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkUcaaSBillActy> Find(Expression<Func<LkUcaaSBillActy, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkUcaaSBillActy, out List<LkUcaaSBillActy> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkUcaaSBillActy> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUcaaSBillActy, out List<LkUcaaSBillActy> list))
            {
                list = _context.LkUcaaSBillActy
                            .OrderBy(i => i.UcaaSBillActyDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkUcaaSBillActy, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkUcaaSBillActy GetById(int id)
        {
            return _context.LkUcaaSBillActy
                            .SingleOrDefault(i => i.UcaaSBillActyId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkUcaaSBillActy);
            throw new NotImplementedException();
        }

        public void Update(int id, LkUcaaSBillActy entity)
        {
            throw new NotImplementedException();
        }
    }
}