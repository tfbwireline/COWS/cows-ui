﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class IpManagementRepository : IIpManagementRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;
        private IMemoryCache _cache;

        public IpManagementRepository(COWSAdminDBContext context, IConfiguration configuration, IMemoryCache cache)
        {
            _context = context;
            _configuration = configuration;
            _cache = cache;
        }


        public LkIpMstr GetById(int id)
        {
            return _context.LkIpMstr
                            .SingleOrDefault(x => x.Id == id);
        }

        public DataTable GetIPRecords(int type)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("dbo.GetIPRecords", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IP_TYPE", type);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public DataTable GetIpMstrRelatedAdress(int id)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.GetIpMstrRelatedAdress", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id", id);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public DataTable GetRelatedActyForSelectedIPAddress(int id, int type)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.GetRelatedActyForSelectedIPAddress", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IP_MSTR_ID", id);
            command.Parameters.AddWithValue("@IP_TYPE", type);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public IQueryable<LkIpMstr> Find(Expression<Func<LkIpMstr, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkIpMstr, out List<LkIpMstr> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkIpMstr> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkIpMstr, out List<LkIpMstr> list))
            {
                list = _context.LkIpMstr
                        .Include(x => x.RecStus)
                            .ToList();
                _cache.Set(CacheKeys.LkIpMstr, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkIpMstr Create(LkIpMstr entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkIpMstr entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkIpMstr);
            return _context.SaveChanges();
        }
    }
}
