﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSEventNtwkTrptRepository : IMDSEventNtwkTrptRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public MDSEventNtwkTrptRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public IEnumerable<MdsEventNtwkTrpt> GetMDSEventNtwkTrptByEventId(int eventId, int csgLvlId)
        {
            List<MdsEventNtwkTrpt> MDSNtwkTrpt = new List<MdsEventNtwkTrpt>();

            var ntwkTrpt = (from nTrpt in _context.MdsEventNtwkTrpt
                            join ev in _context.Event on nTrpt.EventId equals ev.EventId
                            join csd in _context.CustScrdData on nTrpt.MdsEventNtwkTrptId equals csd.ScrdObjId
                            //join csd in _context.CustScrdData on new { SCRD_OBJ_ID = nTrpt.MdsEventNtwkTrptId, SCRD_OBJ_TYPE_ID = (byte)SecuredObjectType.MDS_EVENT_NTWK_TRPT } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                                             into joincsd
                            from csd in joincsd.DefaultIfEmpty()
                            where nTrpt.EventId == eventId
                                && nTrpt.RecStusId == ((byte)1)
                            select new { nTrpt, csd.CtyNme, csd.SttPrvnNme, csd.CtryRgnNme, ev });

            foreach (var ntitem in ntwkTrpt)
            {
                MdsEventNtwkTrpt item = new MdsEventNtwkTrpt();
                item.MdsTrnsprtType = ntitem.nTrpt.MdsTrnsprtType;
                item.LecPrvdrNme = ntitem.nTrpt.LecPrvdrNme;
                item.LecCntctInfo = ntitem.nTrpt.LecCntctInfo;
                item.Mach5OrdrNbr = ntitem.nTrpt.Mach5OrdrNbr;
                item.IpNuaAdr = ntitem.nTrpt.IpNuaAdr;
                //item.MplsAccsBdwd = ntitem.nTrpt.MplsAccsBdwd;
                item.ScaNbr = ntitem.nTrpt.ScaNbr;
                //item.TaggdCd = (ntitem.nTrpt.TaggdCd == null) ? string.Empty : (((bool)ntitem.nTrpt.TaggdCd) ? "Tagged" : "Untagged");
                item.VlanNbr = ntitem.nTrpt.VlanNbr;
                //item.MultiVrfReq = ntitem.nTrpt.MultiVrfReq;
                item.IpVer = ntitem.nTrpt.IpVer;
                item.DdAprvlNbr = ntitem.nTrpt.DdAprvlNbr;
                //item.SalsEngrPhn = ntitem.nTrpt.SalsEngrPhn;
                //item.SalsEngrEmail = ntitem.nTrpt.SalsEngrEmail;
                item.AssocH6 = ntitem.nTrpt.AssocH6;
                item.BdwdNme = ntitem.nTrpt.BdwdNme;
                item.VasType = ntitem.nTrpt.VasType;
                item.CeSrvcId = ntitem.nTrpt.CeSrvcId;
                item.LocCity = (ntitem.ev.CsgLvlId > 0) ? ((ntitem == null) ? string.Empty : _common.GetDecryptValue(ntitem.CtyNme)) : ntitem.nTrpt.LocCity;
                item.LocSttPrvn = (ntitem.ev.CsgLvlId > 0) ? ((ntitem == null) ? string.Empty : _common.GetDecryptValue(ntitem.SttPrvnNme)) : ntitem.nTrpt.LocSttPrvn;
                item.LocCtry = (ntitem.ev.CsgLvlId > 0) ? ((ntitem == null) ? string.Empty : _common.GetDecryptValue(ntitem.CtryRgnNme)) : ntitem.nTrpt.LocCtry;
                //item.sAssocH6 = ntitem.nTrpt.ASSOC_H6;
                MDSNtwkTrpt.Add(item);
            }
            return MDSNtwkTrpt;
        }

        public void AddNtwkTrpt(List<MdsEventNtwkTrpt> data, int eventId, int csgLvlId)
        {
            InactiveAllNtwkTrpt(eventId);

            //List<MDS_EVENT_NTWK_TRPT> lment = new List<MDS_EVENT_NTWK_TRPT>();
            //List<MDSNtwkTrpt> ntwkTrpt = Obj.MDSNtwkTrpt;

            foreach (MdsEventNtwkTrpt item in data)
            {
                MdsEventNtwkTrpt ment = new MdsEventNtwkTrpt();
                CustScrdData csdNtwkTrpt = new CustScrdData();

                ment.EventId = eventId;
                if (csgLvlId == 0)
                {
                    ment.LocCity = item.LocCity;
                    ment.LocSttPrvn = item.LocSttPrvn;
                    ment.LocCtry = item.LocCtry;
                }
                ment.MdsTrnsprtType = item.MdsTrnsprtType;
                ment.LecPrvdrNme = item.LecPrvdrNme;
                ment.LecCntctInfo = item.LecCntctInfo;
                ment.Mach5OrdrNbr = item.Mach5OrdrNbr;
                ment.IpNuaAdr = item.IpNuaAdr;
                //ment.MplsAccsBdwd = item.MplsAccsBdwd;
                ment.ScaNbr = item.ScaNbr;
                //if (item.TaggdCd != null)
                //    ment.TaggdCd = (item.TaggdCd.Equals("Tagged"));
                //ment.VlanNbr = (ment.TaggdCd.HasValue) ? item.VlanNbr : string.Empty;
                //ment.MultiVrfReq = item.MultiVrfReq;
                ment.IpVer = item.IpVer;
                ment.VlanNbr = item.VlanNbr;
                ment.DdAprvlNbr = item.DdAprvlNbr;
                //ment.SalsEngrPhn = item.SalsEngrPhn;
                //ment.SalsEngrEmail = item.SalsEngrEmail;
                // Added condition to preventexception thrown for above character length required [20210922]
                ment.BdwdNme = item.BdwdNme.Length > 20 ? item.BdwdNme.Substring(0, 20) : item.BdwdNme;
                ment.AssocH6 = item.AssocH6;
                ment.VasType = item.VasType;
                ment.CeSrvcId = item.CeSrvcId;
                ment.CreatDt = DateTime.Now;
                ment.RecStusId = (byte)1;
                _context.MdsEventNtwkTrpt.Add(ment);
                _context.SaveChanges();

                if (csgLvlId > 0)
                {
                    csdNtwkTrpt.ScrdObjId = ment.MdsEventNtwkTrptId;
                    csdNtwkTrpt.ScrdObjTypeId = (byte)SecuredObjectType.MDS_EVENT_NTWK_TRPT;
                    csdNtwkTrpt.CtyNme = _common.GetEncryptValue(item.LocCity);
                    csdNtwkTrpt.SttPrvnNme = _common.GetEncryptValue(item.LocSttPrvn);
                    csdNtwkTrpt.CtryRgnNme = _common.GetEncryptValue(item.LocCtry);
                    csdNtwkTrpt.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(csdNtwkTrpt);
                    _context.SaveChanges();
                }

                //Call the UpdateNetworkBPMTask procedure for each record
                //if ((Obj.Schedule.WorkFlowStatus.Equals(EEventWorkFlowStatus.Complete))
                //    && !(String.IsNullOrEmpty(Obj.sDesignDocApprNo)) && !(String.IsNullOrEmpty(item.sNtwkNUA)))
                //    UpdateNetworkBPMTask(eventId, item.sNtwkNUA, Obj.sDesignDocApprNo);
            }
        }

        private void InactiveAllNtwkTrpt(int eventId)
        {
            _context.MdsEventNtwkTrpt.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1)).ToList().ForEach(b => { b.RecStusId = ((byte)0); b.ModfdDt = DateTime.Now; });
            _context.SaveChanges();
        }

        public void UpdateNetworkBPMTask(string ddNbr, string h6, int vasCeFlg, bool ceChngFlg, int eventId, int? modfdByUserId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.UpdateNetworkBPMTask @DDNbr, @H6, @VASCEFlg, @CEChngFlg, @EventID, @ModfdByUserID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@DDNbr", SqlDbType = SqlDbType.VarChar, Value = ddNbr });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@VASCEFlg", SqlDbType = SqlDbType.TinyInt, Value = vasCeFlg });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CEChngFlg", SqlDbType = SqlDbType.Bit, Value = ceChngFlg });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EventID", SqlDbType = SqlDbType.Int, Value = eventId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@ModfdByUserID", SqlDbType = SqlDbType.Int, Value = modfdByUserId != null ? modfdByUserId : 0 });

                _context.Database.OpenConnection();
                command.ExecuteNonQuery();
            }
        }

        public void UpdateNidAtBPM(int orderId, string h6, string nidSerialNumber, int eventId, int? modfdByUserId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.UpdateNIDAtBPM @OrderId, @H6, @NIDSerialNbr, @EventID, @ModfdByUserID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@OrderId", SqlDbType = SqlDbType.Int, Value = orderId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@NIDSerialNbr", SqlDbType = SqlDbType.VarChar, Value = nidSerialNumber });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EventID", SqlDbType = SqlDbType.Int, Value = eventId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@ModfdByUserID", SqlDbType = SqlDbType.Int, Value = modfdByUserId != null ? modfdByUserId : 0 });

                _context.Database.OpenConnection();
                command.ExecuteNonQuery();
            }
        }

        public IQueryable<MdsEventNtwkTrpt> Find(Expression<Func<MdsEventNtwkTrpt, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void AddPortBndwd(ref List<MdsEventNtwkTrpt> data, int eventId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MdsEventNtwkTrpt> GetAll()
        {
            throw new NotImplementedException();
        }

        public MdsEventNtwkTrpt GetById(int id)
        {
            //return _context.MdsEventNtwkTrpt
            //    .SingleOrDefault(a => a.MdsEventNtwkTrptId == id);
            throw new NotImplementedException();
        }

        public MdsEventNtwkTrpt Create(MdsEventNtwkTrpt entity)
        {
            throw new NotImplementedException();
        }

        public void Create(List<MdsEventNtwkTrpt> entity)
        {
            _context.MdsEventNtwkTrpt
                .AddRange(entity.Select(a =>
                {
                    a.MdsEventNtwkTrptId = 0;
                    // Added condition to preventexception thrown for above character length required [20210922]
                    a.BdwdNme = a.BdwdNme.Length > 20 ? a.BdwdNme.Substring(0, 20) : a.BdwdNme;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Update(int id, MdsEventNtwkTrpt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public IEnumerable<MdsEventNtwkTrpt> GetMDSEventNtwkTrptByEventId(int eventId)
        {
            throw new NotImplementedException();
        }

        public void SetInactive(int eventId)
        {
            _context.MdsEventNtwkTrpt
                .Where(a => a.EventId == eventId && a.RecStusId == (byte)1)
                .ToList()
                .ForEach(b =>
                {
                    b.RecStusId = (byte)0;
                    b.ModfdDt = DateTime.Now;
                });
        }
    }
}