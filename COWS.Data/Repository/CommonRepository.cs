﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace COWS.Data.Repository
{
    public class CommonRepository : ICommonRepository
    {
        private readonly COWSAdminDBContext _context;
        private IConfiguration _configuration;
        private IMemoryCache _cache;
        private readonly ILogger<CommonRepository> _logger;
        private readonly IServiceAssuranceRepository _srvcAssurance;
        public CommonRepository(COWSAdminDBContext context, IConfiguration configuration,
            ILogger<CommonRepository> logger, IMemoryCache cache, IServiceAssuranceRepository srvcAssurance)
        {
            _context = context;
            _configuration = configuration;
            _cache = cache;
            _logger = logger;
            _srvcAssurance = srvcAssurance;
        }

        public List<LkSysCfg> GetAllSysCfg()
        {
            //if (!_cache.TryGetValue(CacheKeys.LkSysCfg, out List<LkSysCfg> list))
            //{
               var list = _context.LkSysCfg.Where(a => a.RecStusId == 1).AsNoTracking().ToList();
            //    _cache.Set(CacheKeys.LkSysCfg, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            //}
            return list;
        }

        public IEnumerable<GetEncryptValues> GetEventEncryptValuesSipt(SecuredData ec)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ec.CustNme);  //0
            sb.Append("^");
            sb.Append(ec.CustCntctNme); //1
            sb.Append("^");
            sb.Append(ec.CustCntctPhnNbr); //2
            sb.Append("^");
            sb.Append(ec.CustEmailAdr); //3
            sb.Append("^");
            sb.Append(ec.StreetAdr); //4
            sb.Append("^");
            sb.Append(ec.FlrBldgNme); //5
            sb.Append("^");
            sb.Append(ec.CtyNme); //6
            sb.Append("^");
            sb.Append(ec.SttPrvnNme); //7
            sb.Append("^");
            sb.Append(ec.CtryRgnNme); //8
            sb.Append("^");
            sb.Append(ec.ZipCd); //9
            sb.Append("^");
            sb.Append(ec.EventTitleTxt); //10
            sb.Append("^");
            sb.Append("ContactHrs"); //11
            sb.Append("^");
            sb.Append(ec.TeamPdlNme); //12
            sb.Append("^");
            sb.Append("ntwkCustNme"); //13

            return GetEncryptValues(sb.ToString());
        }

        public IEnumerable<GetEncryptValues> GetEventEncryptValues(SecuredData entity)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(entity.CustNme);
            sb.Append("^");
            sb.Append(entity.CustCntctNme);
            sb.Append("^");
            sb.Append(entity.CustCntctPhnNbr);

            sb.Append("^");
            sb.Append(entity.CustEmailAdr);
            sb.Append("^");
            sb.Append(entity.CustCntctCellPhnNbr);
            sb.Append("^");
            sb.Append(entity.CustCntctPgrNbr);
            sb.Append("^");
            sb.Append(entity.CustCntctPgrPinNbr);
            sb.Append("^");
            sb.Append(entity.EventTitleTxt);
            sb.Append("^");
            sb.Append(entity.EventDes);

            return GetEncryptValues(sb.ToString());
        }

        public IEnumerable<GetEncryptValues> GetEventEncryptValuesMds(SecuredData entity)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(entity.CustNme);
            sb.Append("^");
            sb.Append(entity.CustCntctNme);
            sb.Append("^");
            sb.Append(entity.CustCntctPhnNbr);
            sb.Append("^");
            sb.Append(entity.CustCntctCellPhnNbr);
            sb.Append("^");
            sb.Append(entity.SaCntctNme);
            sb.Append("^");
            sb.Append(entity.SaCntctPhnNbr);
            sb.Append("^");
            sb.Append(entity.SaCntctCellPhnNbr);
            sb.Append("^");
            sb.Append(entity.StreetAdr);
            sb.Append("^");
            sb.Append(entity.FlrBldgNme);
            sb.Append("^");
            sb.Append(entity.CtyNme);
            sb.Append("^");
            sb.Append(entity.SttPrvnNme);
            sb.Append("^");
            sb.Append(entity.CtryRgnNme);
            sb.Append("^");
            sb.Append(entity.ZipCd);
            sb.Append("^");
            sb.Append(entity.AltStreetAdr1);
            sb.Append("^");
            sb.Append(entity.AltFlrBldgNme);
            sb.Append("^");
            sb.Append(entity.AltCtyNme);
            sb.Append("^");
            sb.Append(entity.AltSttPrvnNme);
            sb.Append("^");
            sb.Append(entity.AltCtryRgnNme);
            sb.Append("^");
            sb.Append(entity.AltZipCd);
            sb.Append("^");
            sb.Append(entity.AltCntctNme);
            sb.Append("^");
            sb.Append(entity.AltCntctPhnNbr);
            sb.Append("^");
            sb.Append(entity.AltCntctEmail);
            sb.Append("^");
            sb.Append(entity.EventTitleTxt);
            sb.Append("^");
            sb.Append(entity.CustEmailAdr);
            sb.Append("^");
            sb.Append(entity.NetworkCustNme);

            return GetEncryptValues(sb.ToString());
        }

        public IEnumerable<GetEncryptValues> GetEncryptValues(string sDecrypt)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@s", sDecrypt)
            };

            var encryptValues = _context.Query<GetEncryptValues>()
                .AsNoTracking()
                .FromSql("dbo.GetEncryptValues @s", pc.ToArray()).ToList<GetEncryptValues>();

            return encryptValues;
        }

        //public byte[] GetEncryptValue(string sDecrypt)
        //{
        //    List<SqlParameter> pc = new List<SqlParameter>
        //        {
        //            new SqlParameter("@p0", sDecrypt),
        //            new SqlParameter("@IsNewUI", true)
        //        };

        //    var encryptValue = _context.Query<GetEncryptValues>()
        //        .AsNoTracking()
        //        .FromSql("[dbo].[GetEncryptValue] @p0, @IsNewUI", pc.ToArray())
        //        .SingleOrDefault<GetEncryptValues>();

        //    return encryptValue.Item;
        //}

        //public string GetDecryptValue(byte[] encryptVal)
        //{
        //    if ((encryptVal != null) && (encryptVal.Length > 0))
        //    {
        //        List<SqlParameter> pc = new List<SqlParameter>
        //        {
        //            new SqlParameter("@EncryptData", encryptVal),
        //            new SqlParameter("@IsNewUI", true)
        //        };

        //        var decryptVal = _context.Query<GetDecryptValues>()
        //            .AsNoTracking()
        //            .FromSql("[dbo].[GetDeCryptValue] @EncryptData, @IsNewUI", pc.ToArray())
        //            .SingleOrDefault<GetDecryptValues>();

        //        return decryptVal != null ? decryptVal.Item : string.Empty;
        //    }

        //    return string.Empty;
        //}

        public byte[] GetEncryptValue(string sDecrypt)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@DecryptData", sDecrypt),
                };

            var encryptValue = _context.Query<GetEncryptValues>()
                .FromSql("[dbo].[GetEncryptValue] @DecryptData", pc.ToArray())
                .SingleOrDefault<GetEncryptValues>();

            return encryptValue.Item;
        }

        public string GetDecryptValue(byte[] encryptVal)
        {
            if ((encryptVal != null) && (encryptVal.Length > 0))
            {
                SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
                //SqlConnection connection = new SqlConnection(_configuration.GetSection("ConnectionStrings:SqlDbConn").Value);
                SqlCommand command = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                command = new SqlCommand("dbo.GetDeCryptValue", connection);
                command.CommandTimeout = _context.commandTimeout;
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@EncryptData", encryptVal); // if you have parameters.
                da = new SqlDataAdapter(command);
                da.Fill(ds);
                connection.Close();

                return ds.Tables[0].Rows[0][0].ToString();
            }

            return string.Empty;
        }

        public IEnumerable<GetEventDecryptValues> GetEventDecryptValues(int eventId, string eventType)
        {
            //List<SqlParameter> pc = new List<SqlParameter>
            //{
            //    new SqlParameter("@EID", eventId),
            //    new SqlParameter("@EType", eventType)
            //    //new SqlParameter("@IsNewUI", true)
            //};

            //var decryptValues = _context.Query<GetEventDecryptValues>()
            //    .AsNoTracking()
            //    .FromSql("[dbo].[GetEventDecryptValues] @EID, @EType", pc.ToArray()).ToList<GetEventDecryptValues>();

            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            command = new SqlCommand("[dbo].[GetEventDecryptValues]", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@EID", eventId);
            command.Parameters.AddWithValue("@EType", eventType);

            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            List<GetEventDecryptValues> decryptValues = new List<GetEventDecryptValues>();

            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (eventType == "MDS")
                    {
                        GetEventDecryptValues item = new GetEventDecryptValues
                        {
                            CUST_NME = dr[0].ToString(),
                            CUST_CNTCT_NME = dr[1].ToString(),
                            CUST_CNTCT_PHN_NBR = dr[2].ToString(),
                            CUST_CNTCT_CELL_PHN_NBR = dr[3].ToString(),

                            SRVC_ASSRN_POC_NME = dr[4].ToString(),
                            SRVC_ASSRN_POC_PHN_NBR = dr[5].ToString(),
                            SRVC_ASSRN_POC_CELL_PHN_NBR = dr[6].ToString(),

                            STREET_ADR_1 = dr[7].ToString(),
                            FLR_ID = dr[8].ToString(),
                            CTY_NME = dr[9].ToString(),
                            STT_PRVN_NME = dr[10].ToString(),
                            CTRY_RGN_NME = dr[11].ToString(),
                            ZIP_PSTL_CD = dr[12].ToString(),

                            EVENT_TITLE_TXT = dr[13].ToString(),
                            CUST_EMAIL_ADR = dr[14].ToString(),
                            FRST_NME = dr[15].ToString(),

                            STREET_ADR_2 = dr[16].ToString(),
                            BLDG_NME = dr[17].ToString(),
                            RM_NBR = dr[18].ToString(),
                            STT_CD = dr[19].ToString(),
                            CTRY_CD = dr[20].ToString(),
                            STREET_ADR_3 = dr[21].ToString(),
                            EVENT_DES = dr[22].ToString(),
                            LST_NME = dr[23].ToString(),
                            NTE_TXT = dr[24].ToString()
                        };
                        decryptValues.Add(item);
                    }
                    else if (eventType == "SIPT")
                    {
                        GetEventDecryptValues item = new GetEventDecryptValues
                        {
                            CUST_NME = dr[0].ToString(),
                            CUST_CNTCT_NME = dr[1].ToString(),
                            CUST_CNTCT_PHN_NBR = dr[2].ToString(),
                            //CUST_CNTCT_CELL_PHN_NBR = dr[3].ToString(), // COMMENTED OUT ON SP
                            CUST_EMAIL_ADR = dr[3].ToString(),
                            EVENT_TITLE_TXT = dr[4].ToString(),
                            STREET_ADR_1 = dr[5].ToString(),
                            FLR_ID = dr[6].ToString(),
                            CTY_NME = dr[7].ToString(),
                            STT_PRVN_NME = dr[8].ToString(),
                            CTRY_RGN_NME = dr[9].ToString(),
                            ZIP_PSTL_CD = dr[10].ToString()
                        };
                        decryptValues.Add(item);
                    }
                }
            }

            return decryptValues;
        }

        public void LogWebActivity(byte actyType, string actyVal, string userADID, byte userCSGLvl, byte objCSGLvl)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            connection.Open();
            command = new SqlCommand("web.LogWebActivity", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@actyType", actyType); // if you have parameters.
            command.Parameters.AddWithValue("@actyVal", actyVal); // if you have parameters.
            command.Parameters.AddWithValue("@userADID", userADID); // if you have parameters.
            command.Parameters.AddWithValue("@userCSGLvl", userCSGLvl); // if you have parameters.
            command.Parameters.AddWithValue("@ObjCSGLvl", objCSGLvl); // if you have parameters.
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
        }

        public void UpdateWebSession(string sUserADID, string sSessionId, string sIP)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            connection.Open();
            command = new SqlCommand("web.UpdateWebSession", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UADID", sUserADID);
            command.Parameters.AddWithValue("@IPAdr", sIP);
            command.Parameters.AddWithValue("@SID", sSessionId);
            command.Parameters.AddWithValue("@IsOldUI", false);

            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
        }

        public string GetCountryName(string sCtryCd)
        {
            string CtryNme = "";
            if ((sCtryCd != null) && (sCtryCd.Length > 0))
            {
                SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
                connection.Open();
                SqlCommand command = new SqlCommand("Select CTRY_NME from dbo.LK_CTRY WITH (NOLOCK) where CTRY_CD=@code", connection);
                command.CommandTimeout = _context.commandTimeout;
                command.Parameters.AddWithValue("@code", sCtryCd);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        CtryNme = String.Format("{0}", reader["CTRY_NME"]);
                    }
                }
                connection.Close();

                return CtryNme;
            }

            return string.Empty;
        }

        public List<UserCsgLvl> GetUserSecurityGroupByADID(string adid)
        {
            List<UserCsgLvl> usgL = new List<UserCsgLvl>();
            var _usg = (from t in _context.UserCsgLvl
                        join u in _context.LkUser on t.UserId equals u.UserId
                        where u.UserAdid == adid && t.RecStusId == 1 && u.RecStusId == 1
                        select new
                        {
                            t.CsgLvlId,
                            u.UserAdid,
                            u.UserId
                        });

            foreach (var t in _usg)
            {
                UserCsgLvl usg = new UserCsgLvl();
                usg.CsgLvlId = t.CsgLvlId;
                //usg.User = new LkUser
                //{
                //    UserId = t.UserId,
                //    UserAdid = t.UserAdid
                //};
                //usg.User.UserAdid = t.UserAdid;
                usg.UserId = t.UserId;

                usgL.Add(usg);
            }
            return usgL;
        }

        /// <span class="code-SummaryComment"><summary></span>
        /// Gets the length limit for a given field on a LINQ object ... or zero if not known
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><remarks></span>
        /// You can use the results from this method to dynamically
        /// set the allowed length of an INPUT on your web page to
        /// exactly the same length as the length of the database column.
        /// Change the database and the UI changes just by
        /// updating your DBML and recompiling.
        /// <span class="code-SummaryComment"></remarks></span>
        public int GetLengthLimit(object obj, string field)
        {
            int dblenint = 0;   // default value = we can't determine the length

            Type type = obj.GetType();
            PropertyInfo prop = type.GetProperty(field);

            // Find the Linq 'Column' attribute
            // e.g. [Column(Storage="_FileName", DbType="NChar(256) NOT NULL", CanBeNull=false)]
            object[] info = prop.GetCustomAttributes(typeof(ColumnAttribute), true);

            // Assume there is just one
            if (info.Length == 1)
            {
                ColumnAttribute ca = (ColumnAttribute)info[0];
                string dbtype = ca.TypeName;

                if (dbtype.StartsWith("NChar") || dbtype.StartsWith("NVarChar") || dbtype.StartsWith("VarChar") || dbtype.StartsWith("Char"))
                {
                    int index1 = dbtype.IndexOf("(");
                    int index2 = dbtype.IndexOf(")");
                    string dblen = dbtype.Substring(index1 + 1, index2 - index1 - 1);
                    int.TryParse(dblen, out dblenint);
                }
            }
            return dblenint;
        }

        public string GetSysCfgValue(string name)
        {
            if (!_cache.TryGetValue(CacheKeys.LkSysCfg, out List<LkSysCfg> list))
                list = GetAllSysCfg();

            var val = list.Where(i => i.PrmtrNme.Equals(name));
            return (val != null && val.Count() > 0) ? val.SingleOrDefault().PrmtrValuTxt : string.Empty;
        }

        public List<LkSysCfg> GetAllSysCfgByPrmtrValue(string value)
        {
            if (!_cache.TryGetValue(CacheKeys.LkSysCfg, out List<LkSysCfg> list))
                list = GetAllSysCfg();

            return list.Where(i => i.PrmtrValuTxt.Equals(value) && i.RecStusId.Equals(1)).ToList();
        }

        public bool GetAdminExcPerm(string name, string adid)
        {
            //if (!_cache.TryGetValue(CacheKeys.LkSysCfg, out List<LkSysCfg> list))
            var list = GetAllSysCfg();

            var val = list.Where(i => i.PrmtrNme.Equals(name) && i.PrmtrValuTxt.Equals(adid) && i.RecStusId.Equals(1));
            return (val != null && val.Count() > 0);
        }

        public int UpdateSysCfgValue(string name, string value)
        {
            LkSysCfg val = _context.LkSysCfg.Where(i => i.PrmtrNme.Equals(name)).SingleOrDefault();
            val.PrmtrValuTxt = value;

            return _context.SaveChanges();
        }

        public int GetCSGLevelAdId(string AdId)
        {
            var csgId = (from u in _context.LkUser
                         join c in _context.UserCsgLvl on u.UserId equals c.UserId
                         where u.UserAdid == AdId
                         orderby c.CsgLvlId
                         select new { c.CsgLvlId }).FirstOrDefault();
            byte myCsg = 0;
            if (csgId != null)
            {
                myCsg = csgId.CsgLvlId;
            }
            return int.Parse(myCsg.ToString());
        }

        public List<Entities.QueryModels.CSGLevel> GetM5CSGLvl(string sH1, string sH6, string sM5OrdrNbr)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            command = new SqlCommand("[web].[getM5SecrdValidation]", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@H1", sH1);
            command.Parameters.AddWithValue("@H6", sH6);
            command.Parameters.AddWithValue("@M5OrdrNbr", sM5OrdrNbr);

            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            List<Entities.QueryModels.CSGLevel> csgl = new List<Entities.QueryModels.CSGLevel>();

            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Entities.QueryModels.CSGLevel csg = new Entities.QueryModels.CSGLevel
                    {
                        CsgLevelID = byte.Parse(dr["CSG_LVL"].ToString())
                    };
                    csgl.Add(csg);
                }
            }
            return csgl;
        }

        public List<LkSysCfg> GetMenuCancelUserByADID(string adid)
        {
            List<LkSysCfg> usgL = new List<LkSysCfg>();
            var _cfg = (from a in _context.LkSysCfg
                        where a.PrmtrNme == "menuCancels"
                        && a.RecStusId == 1
                        select new
                        {
                            a.CfgId,
                            a.PrmtrValuTxt,
                            a.PrmtrNme
                        });
            foreach (var t in _cfg)
            {
                LkSysCfg usg = new LkSysCfg();
                usg.CfgId = t.CfgId;
                usg.PrmtrNme = t.PrmtrNme;
                usg.PrmtrValuTxt = t.PrmtrValuTxt;

                usgL.Add(usg);
            }
            return usgL;
        }

        public string CalculateDateWithSprintBusinessdays(DateTime dtStartTime, string sBusinessDays, bool bConcat, bool includeWeekend= false)
        {
            int[] iArr = (bConcat) ? Array.ConvertAll(sBusinessDays.Split(new char[] { '|' }), s => int.Parse(s)) : new int[] { int.Parse(sBusinessDays) };
            string sFinal = "";
            DateTime inDt = dtStartTime;
            for (int j = 0; j < iArr.Length; j++)
            {
                dtStartTime = inDt;
                int i = 0;
                if (iArr[j] > 0)
                {
                    while (i < iArr[j])
                    {
                        dtStartTime = dtStartTime.AddDays(1);
                        if(includeWeekend)
                        {
                            if (!IsSprintHoliday(dtStartTime.ToString()))
                            {
                                i++;
                            }
                        } 
                        else
                        {
                            if (!(dtStartTime.DayOfWeek == DayOfWeek.Sunday || dtStartTime.DayOfWeek == DayOfWeek.Saturday) && !IsSprintHoliday(dtStartTime.ToString()))
                            {
                                i++;
                            }
                        }      
                    }
                }
                if (!bConcat)
                    return dtStartTime.ToString("G");
                else
                    sFinal = dtStartTime.ToString("G") + "|" + sFinal;
            }
            return sFinal;
        }

        public bool IsSprintHoliday(string inputDay)
        {
            DateTime d = DateTime.Parse(inputDay).Date;
            var sh = (from it in _context.LkSprintHldy.AsNoTracking()
                      where it.SprintHldyDt == d && it.RecStusId == (byte)Entities.Enums.ERecStatus.Active
                      select it.SprintHldyId);

            if (sh.Count() > 0)
                return true;

            return false;
        }

        public string GetCowsAppCfgValue(string name)
        {
            var val = _context.LkCowsAppCfg.Where(i => i.CfgKeyNme.Equals(name));
            return (val != null && val.Count() > 0) ? val.SingleOrDefault().CfgKeyValuTxt : string.Empty;
        }

        public void UpdateNIDAtBPM(int orderID, string h6, string nidSerialNbr, int userId, int eventID)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            connection.Open();
            command = new SqlCommand("dbo.UpdateNIDAtBPM", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrdrId", orderID);
            command.Parameters.AddWithValue("@H6", h6);
            command.Parameters.AddWithValue("@NIDSerialNbr", nidSerialNbr);
            command.Parameters.AddWithValue("@EventID", eventID);
            command.Parameters.AddWithValue("@ModfdByUserID", userId);
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
        }

        public DataTable GetAllSrvcSiteSupport()
        {
            if (!_cache.TryGetValue(CacheKeys.LkSrvcAssrnSiteSupp, out List<LkSrvcAssrnSiteSupp> list))
                list = _srvcAssurance.GetAll().ToList();

                var q = (from s in list
                         where (s.RecStusId == (byte)ERecStatus.Active)
                     orderby s.SrvcAssrnSiteSuppDes
                     select new { s.SrvcAssrnSiteSuppId, s.SrvcAssrnSiteSuppDes });

            return LinqHelper.CopyToDataTable(q, null, null);
        }

        public bool IsEventEmailSent(int eventID, int emailReqType)
        {
            //List<EmailReq> x = (from er in _context.EmailReq
            //                    where ((er.EventId == eventID)
            //                            && ((DateTime.Now - er.CreatDt).TotalSeconds > 15)
            //                            && (er.EmailReqTypeId == emailReqType)
            //                            && (er.StusId == ((byte)EmailStatus.Success)))
            //                    select er).ToList();

            var x = _context.EmailReq.Where(i => i.EventId == eventID
                                                && i.EmailReqTypeId == emailReqType
                                                && i.StusId == (byte)EmailStatus.Success
                    ).Select(i => i.EmailReqId).ToList();

            return x.Count > 0;

        }

        //public List<WorkflowStatus> GetMDSActStatuses(int sEvStatus, string sActType)
        //{
        //    List<WorkflowStatus> wfl = new List<WorkflowStatus>();
        //    try {
        //        var _dtRules = _context.LkEventRule.Where(a => a.EventTypeId == 5);

        //        var q = (from r in _dtRules.AsEnumerable()
        //                 where ((r.UsrPrfId == 130) || (r.UsrPrfId == 132))
        //                 && r.StagnCd == bool.Parse((sActType.Contains("Staging")) && ((sEvStatus.Equals(((byte)EventStatus.Published).ToString()))
        //                                                    || (sEvStatus.Equals(((byte)EventStatus.Fulfilling).ToString()))
        //                                                    || (sEvStatus.Equals(((byte)EventStatus.Shipped).ToString()))
        //                                                    || (sEvStatus.Equals(((byte)EventStatus.OnHold).ToString()))) ? bool.TrueString : bool.FalseString)
        //                 && r.StrtEventStusId == sEvStatus
        //                 select r.WrkflwStusId).Distinct().ToList();

        //        if (q != null)
        //        {
        //            foreach (var item in q)
        //            { wfl.Add((WorkflowStatus)item); }
        //        }                
        //    }
        //    catch (Exception ex)
        //    { _logger.LogInformation($"GetMDSActStatuses - { ex.Message + "; " + ex.InnerException + "; " + ex.StackTrace } ");  }
        //    return wfl;
        //}

        public CustScrdData GetSecuredData(int id, int typeId)
        {
            return _context.CustScrdData
                .Where(a => a.ScrdObjId == id && a.ScrdObjTypeId == typeId)
                .SingleOrDefault();
        }

        public bool IsDeviceExcluded(string cmpntFmly)
        {
            List<string> ldev = new List<string>();
            ldev = _context.LkMdsCpeDevXclusn
                            .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                            .OrderBy(i => i.DevTypeNme)
                            .Select(i => i.DevTypeNme.Trim()).ToList();

            if (ldev.Count > 0)
            {
                foreach (string s in ldev)
                {
                    if (cmpntFmly.Contains(s.ToUpper()))
                        return true;
                }
            }

            return false;
        }

        // Used for UCaaS Event
        // Can be refactored in future - some model property is used in MDS that is not used in UCaaS
        public void UpdateRedesignFromEvent(List<EventDevCmplt> devcmplt, bool cmplDev,
            int eventID, List<string> toDevCompleteNames)
        {
            List<int> lstDeviceID = new List<int>();
            List<EventDevCmplt> devcmpltlist = devcmplt.Where(i => i.CmpltdCd
                && toDevCompleteNames.Contains(i.OdieDevNme)).ToList();
            if (cmplDev && devcmpltlist != null && devcmpltlist.Count() > 0)
            {
                var chargeCdList = _context.RedsgnDevicesInfo
                                    .Where(rd => devcmplt.Where(i => i.CmpltdCd)
                                        .Select(i => i.RedsgnDevId)
                                        .Contains(rd.RedsgnDevId)
                                            && rd.NteChrgCd && rd.RecStusId).ToList();

                bool chargeCd = chargeCdList != null && chargeCdList.Count() <= 0;

                var rdil = _context.RedsgnDevicesInfo
                                    .Where(rd => devcmplt.Where(i => i.CmpltdCd)
                                        .Select(i => i.RedsgnDevId)
                                        .Contains(rd.RedsgnDevId) && rd.RecStusId).ToList();

                int redesignID = rdil != null ? rdil.FirstOrDefault().RedsgnId : 0;

                foreach (RedsgnDevicesInfo item in rdil)
                {
                    var h6 = devcmplt.FirstOrDefault(i => i.OdieDevNme.Equals(item.DevNme)).H6;
                    if (chargeCd)
                    {
                        item.NteChrgCd = true;
                        item.H6CustId = h6;
                        item.DevCmpltnCd = true;
                        chargeCd = false;
                    }

                    if (!item.DevBillDt.HasValue)
                    {
                        item.DevBillDt = DateTime.Now;
                        item.H6CustId = h6;
                        item.DevCmpltnCd = true;

                        lstDeviceID.Add(item.RedsgnDevId);
                    }

                    if (!item.DevCmpltnCd.HasValue || !item.DevCmpltnCd.Value)
                    {
                        item.DevCmpltnCd = true;
                    }

                    _context.RedsgnDevicesInfo.Update(item);
                }

                _context.SaveChanges();

                string redesignUrl = _configuration.GetSection("AppSettings:ViewRedesignURL").Value + redesignID;
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() { ParameterName = "@EventID", SqlDbType = SqlDbType.Int, Value = eventID },
                    new SqlParameter() { ParameterName = "@redsgn_url", SqlDbType = SqlDbType.VarChar, Value = redesignUrl }
                };

                _context.Database.ExecuteSqlCommand("[web].[updateRdsnCmpltFromEvent] @EventID, @redsgn_url", pc.ToArray());

                if (lstDeviceID.Count() > 0)
                {
                    string dev = string.Join(",", lstDeviceID);

                    List<SqlParameter> pc1 = new List<SqlParameter>
                    {
                        new SqlParameter() { ParameterName = "@REDSGN_DEV_IDS", SqlDbType = SqlDbType.VarChar, Value = dev },
                        new SqlParameter() { ParameterName = "@EventID", SqlDbType = SqlDbType.Int, Value = eventID }
                    };

                    _context.Database.ExecuteSqlCommand
                        ("[dbo].[insertRedesignDeviceChargesIntoMach5] @REDSGN_DEV_IDS, @EventID",
                        pc1.ToArray());
                }
            }
        }

        // Used for MDS Event
        // Can be refactored in future - some model property is used in MDS that is not used in UCaaS
        public void UpdateRedesignFromEvent(List<EventDeviceCompletionView> devcmplt, bool cmplDev,
            int eventID, List<string> toDevCompleteNames)
        {
            List<int> lstDeviceID = new List<int>();
            List<EventDeviceCompletionView> devcmpltlist = devcmplt.Where(i => i.CmpltdCd
                && toDevCompleteNames.Contains(i.OdieDevNme)).ToList();
            if (cmplDev && devcmpltlist != null && devcmpltlist.Count() > 0)
            {
                var chargeCdList = _context.RedsgnDevicesInfo
                                    .Where(rd => devcmplt.Where(i => i.CmpltdCd)
                                        .Select(i => i.RedsgnDevId)
                                        .Contains(rd.RedsgnDevId)
                                            && rd.NteChrgCd && rd.RecStusId).ToList();

                bool chargeCd = chargeCdList != null && chargeCdList.Count() <= 0;

                var rdil = _context.RedsgnDevicesInfo
                                    .Where(rd => devcmplt.Where(i => i.CmpltdCd)
                                        .Select(i => i.RedsgnDevId)
                                        .Contains(rd.RedsgnDevId) && rd.RecStusId).ToList();

                int redesignID = rdil != null ? rdil.FirstOrDefault().RedsgnId : 0;

                foreach (RedsgnDevicesInfo item in rdil)
                {
                    var h6 = devcmplt.FirstOrDefault(i => i.OdieDevNme.Equals(item.DevNme)).H6;
                    if (chargeCd)
                    {
                        item.NteChrgCd = true;
                        item.H6CustId = h6;
                        item.DevCmpltnCd = true;
                        chargeCd = false;
                    }

                    if (!item.DevBillDt.HasValue)
                    {
                        item.DevBillDt = DateTime.Now;
                        item.H6CustId = h6;
                        item.DevCmpltnCd = true;

                        lstDeviceID.Add(item.RedsgnDevId);
                    }

                    if (!item.DevCmpltnCd.HasValue || !item.DevCmpltnCd.Value)
                    {
                        item.DevCmpltnCd = true;
                    }

                    _context.RedsgnDevicesInfo.Update(item);
                }

                _context.SaveChanges();

                string redesignUrl = _configuration.GetSection("AppSettings:ViewRedesignURL").Value + redesignID;
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() { ParameterName = "@EventID", SqlDbType = SqlDbType.Int, Value = eventID },
                    new SqlParameter() { ParameterName = "@redsgn_url", SqlDbType = SqlDbType.VarChar, Value = redesignUrl }
                };

                _context.Database.ExecuteSqlCommand("[web].[updateRdsnCmpltFromEvent] @EventID, @redsgn_url", pc.ToArray());

                if (lstDeviceID.Count() > 0)
                {
                    string dev = string.Join(",", lstDeviceID);

                    List<SqlParameter> pc1 = new List<SqlParameter>
                    {
                        new SqlParameter() { ParameterName = "@REDSGN_DEV_IDS", SqlDbType = SqlDbType.VarChar, Value = dev },
                        new SqlParameter() { ParameterName = "@EventID", SqlDbType = SqlDbType.Int, Value = eventID }
                    };

                    _context.Database.ExecuteSqlCommand
                        ("[dbo].[insertRedesignDeviceChargesIntoMach5] @REDSGN_DEV_IDS, @EventID",
                        pc1.ToArray());
                }
            }
        }

        public List<object> GetCacheObject(string cacheKey, List<object> cacheObject)
        {
            List<object> list;
            if (!_cache.TryGetValue(cacheKey, out list))
            {
                list = cacheObject;
                _cache.Set(cacheKey, cacheObject, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }

            return list;
        }

        public IEnumerable<LkRole> GetRole()
        {
            if (!_cache.TryGetValue(CacheKeys.LkRole, out List<LkRole> list))
            {
                list = _context.LkRole
                            .OrderBy(i => i.RoleNme)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkRole, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public IQueryable<LkRole> FindRole(Expression<Func<LkRole, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkRole, out List<LkRole> list))
                list = _context.LkRole.ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }
    }
}