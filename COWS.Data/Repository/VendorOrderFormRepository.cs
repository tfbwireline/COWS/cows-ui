﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VendorOrderFormRepository : IVendorOrderFormRepository
    {
        private readonly COWSAdminDBContext _context;

        public VendorOrderFormRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<VndrOrdrForm> Find(Expression<Func<VndrOrdrForm, bool>> predicate)
        {
            return _context.VndrOrdrForm
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<VndrOrdrForm> GetAll()
        {
            return _context.VndrOrdrForm.ToList();
        }

        public VndrOrdrForm GetById(int id)
        {
            return Find(a => a.VndrOrdrFormId == id).SingleOrDefault();
        }

        public VndrOrdrForm Create(VndrOrdrForm entity)
        {
            _context.VndrOrdrForm.Add(entity);
            SaveAll();

            return GetById(entity.VndrOrdrFormId);
        }

        public void Update(int id, VndrOrdrForm entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            var toDelete = GetById(id);
            _context.VndrOrdrForm.Remove(toDelete);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}