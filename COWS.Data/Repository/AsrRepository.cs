﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class AsrRepository : IAsrRepository
    {
        private readonly COWSAdminDBContext _context;

        public AsrRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<Asr> Find(Expression<Func<Asr, bool>> predicate)
        {
            return _context.Asr
                .Where(predicate);
        }

        public IEnumerable<Asr> GetAll()
        {
            return _context.Asr
                .ToList();
        }

        public Asr GetById(int id)
        {
            return Find(a => a.AsrId == id).SingleOrDefault();
        }

        public Asr Create(Asr entity)
        {
            _context.Asr.Add(entity);
            SaveAll();

            return GetById(entity.AsrId);
        }

        public void Update(int id, Asr entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}