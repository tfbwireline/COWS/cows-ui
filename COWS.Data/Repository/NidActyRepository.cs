﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class NidActyRepository : INidActyRepository
    {
        private readonly COWSAdminDBContext _context;

        public NidActyRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<NidActy> Find(Expression<Func<NidActy, bool>> predicate)
        {
            return _context.NidActy
                .Include(a => a.FsaCpeLineItem.Ordr.Ordr)
                .Where(predicate);
        }

        public IEnumerable<NidActy> GetAll()
        {
            return _context.NidActy.ToList();
        }

        public NidActy GetById(int id)
        {
            return Find(a => a.NidActyId == id).SingleOrDefault();
        }

        public NidActy Create(NidActy entity)
        {
            _context.NidActy.Add(entity);
            SaveAll();

            return GetById(entity.NidActyId);
        }

        public void Update(int id, NidActy entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public IEnumerable<NidSerialView> GetSerialNumberByH6(int id, string h6)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "web.getNIDSerialusingH6 @EVENT_ID, @H6";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EVENT_ID", SqlDbType = SqlDbType.Int, Value = id });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    var result = reader.Translate<NidSerialView>();
                    //var result = new GetNIDSerialusingH6
                    //{
                    //    Result1 = reader.Translate<NidSerialView>(),
                    //    Result2 = reader.Translate<DistinctH6NidSerialNbrView>()
                    //};

                    return result;

                    //var result1 = reader.Translate<NidSerialView>();
                    //var result2 = reader.Translate<DistinctH6NidSerialNbrView>();

                    //return result == 1 ? result1 : result2;
                }
            }
        }

        public DataTable GetSerialNumberAndIP(int ordrId, string deviceId)
        {
            var data = (from fcli in _context.FsaOrdrCpeLineItem
                        join na in _context.NidActy on fcli.FsaCpeLineItemId equals na.FsaCpeLineItemId
                        join ia in _context.IpActy on na.NidActyId equals ia.NidActyId
                        join im in _context.IpMstr on ia.IpMstrId equals im.IpMstrId
                        where fcli.OrdrId == ordrId && fcli.DeviceId == deviceId
                        select new { na.NidSerialNbr, NidIp = im.IpAdr });

            return LinqHelper.CopyToDataTable(data, null, null);
        }

    }
}