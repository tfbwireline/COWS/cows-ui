﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public OrderRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public IQueryable<Ordr> Find(Expression<Func<Ordr, bool>> predicate, string adid = null)
        {
            var order = _context.Ordr
                .Include(a => a.OrdrVlan)
                .Include(a => a.TrptOrdr)
                .Include(a => a.FsaOrdr)
                    .ThenInclude(a => a.FsaOrdrCpeLineItem)
                .Include(a => a.NccoOrdr)
                .Include(a => a.H5Foldr.H5Doc)
                .Include(a => a.OrdrStdiHist)
                    .ThenInclude(a => a.StdiReas)
                .Include(a => a.OrdrStdiHist)
                    .ThenInclude(a => a.CreatByUser)
                .Include(a => a.OrdrNte)
                    .ThenInclude(a => a.NteType)
                .Include(a => a.OrdrNte)
                    .ThenInclude(a => a.CreatByUser)
                .Include(a => a.OrdrMs)
                .Include(a => a.Ckt)
                    .ThenInclude(a => a.CktMs)
                .Include(a => a.OrdrStus)
                .Include(a => a.CreatByUser)
                .Include(a => a.ModfdByUser)
                .Where(predicate);

            if (order.Count() == 1)
            {
                var data = order.FirstOrDefault();
                if (data.CsgLvlId > 0 && adid != null)
                {
                    // LogWebActivity
                    var UserCsg = _common.GetCSGLevelAdId(adid);
                    _common.LogWebActivity(((byte)WebActyType.RedesignDetails), data.OrdrId.ToString(), adid, (byte)UserCsg, (byte)data.CsgLvlId);
                }
            }

            return order;
        }

        public IEnumerable<Ordr> GetAll()
        {
            return _context.Ordr.ToList();
        }

        public Ordr GetById(int id)
        {
            return Find(a => a.OrdrId == id).SingleOrDefault();
        }
        public bool UpdateNrm(int id, bool nrmupdate)
        {

            TrptOrdr tp = new TrptOrdr();
            tp.OrdrId = id;
            tp.NrmUpdtCd = nrmupdate;

            _context.TrptOrdr.Update(tp);
            SaveAll();
            //_context.SaveChanges();
            if (tp.NrmUpdtCd == true)
                return true;
            else
                return false;
        }
        public Ordr Create(Ordr entity)
        {
            _context.Ordr.Add(entity);
            SaveAll();

            return GetById(entity.OrdrId);
        }

        public void Update(int id, Ordr entity)
        {
            Ordr order = GetById(id);

            order.OrdrMs = entity.OrdrMs;
            //order.FsaOrdr.CustCmmtDt = entity.FsaOrdr.CustCmmtDt;
            //order.FsaOrdr.CustWantDt = entity.FsaOrdr.CustWantDt;
            order.Ckt = entity.Ckt;
        }

        public void Delete(int id)
        {
            var toDelete = GetById(id);
            _context.Ordr.Remove(toDelete);
        }


        public Ordr GetOrderPPRT(int id)
        {
            return _context.Ordr
                    .Include(a => a.Pprt)
                   .Where(x => x.OrdrId == id).FirstOrDefault();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public OrdrMs GetOrderMilestoneByOrderId(int orderId)
        {
            return _context.OrdrMs
                .Include(a => a.Ordr.FsaOrdr)
                //.Include(a => a.Ordr.NccoOrdr)
                .Where(a => a.OrdrId == orderId)
                .SingleOrDefault();
        }

        public IEnumerable<WorkGroupData> GetWgData(int orderId, int userId, int profileId = 0, int csgLvlId = 0)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "web.getWGData_V2 @USR_PRF_ID, @UserID, @OrderID, @IsCmplt, @CSGLvlId";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@USR_PRF_ID", SqlDbType = SqlDbType.SmallInt, Value = profileId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = userId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@OrderID", SqlDbType = SqlDbType.Int, Value = orderId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@IsCmplt", SqlDbType = SqlDbType.Int, Value = 0 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CSGLvlId", SqlDbType = SqlDbType.Int, Value = csgLvlId });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    return reader.Translate<WorkGroupData>();
                }
            }
        }

        public void AttachH5Folder(int orderId, int h5FolderId, int modifiedBy)
        {
            var h5 = _context.H5Foldr.SingleOrDefault(i => i.H5FoldrId == h5FolderId);
            var order = _context.Ordr.SingleOrDefault(i => i.OrdrId == orderId);

            if (h5 != null && order != null)
            {
                var country = _context.LkCtry.SingleOrDefault(i => i.CtryCd == h5.CtryCd);
                order.H5FoldrId = h5FolderId;
                order.RgnId = country != null ? country.RgnId : null;
                order.ModfdByUserId = modifiedBy;
                order.ModfdDt = DateTime.Now;
                if (order.OrdrCatId != 2)
                {
                    order.CsgLvlId = h5.CsgLvlId;
                }

                _context.Ordr.Update(order);
                SaveAll();
            }
        }

        public IEnumerable<OrderBasic> GetBasic(int orderId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.getOrderBasic @OrderID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@OrderID", SqlDbType = SqlDbType.Int, Value = orderId });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    return reader.Translate<OrderBasic>();
                }
            }
        }

        public TransportOrderDetails GetTransportOrder(int orderId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.getTransportOrderData @iOrderID, @iIsTerminating";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@iOrderID", SqlDbType = SqlDbType.Int, Value = orderId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@iIsTerminating", SqlDbType = SqlDbType.Bit, Value = 1 });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    var result = new TransportOrderDetails
                    {
                        TransportOrders = reader.Translate<TransportOrder>(),
                        OrderMilestones = reader.Translate<OrderMilestone>(),
                        CircuitMilestones = reader.Translate<CircuitMilestone>(),
                        AccountTeam = reader.Translate<ContactInfo>(),
                        AlternateCustomer = reader.Translate<ContactInfo>()
                    };

                    return result;
                }
            }
        }

        public IQueryable<Ordr> GetOrderDetails(Expression<Func<Ordr, bool>> predicate, string adid = null)
        {
            var order = _context.Ordr
                .Where(predicate).SingleOrDefault();

            if (order != null)
            {
                if (order.CsgLvlId > 0 && adid != null)
                {
                    var UserCsg = _common.GetCSGLevelAdId(adid);
                    _common.LogWebActivity(((byte)WebActyType.EventDetails), order.OrdrId.ToString(), adid, (byte)UserCsg, (byte)order.CsgLvlId);
                }

                if (order.OrdrCatId == 4)
                {
                    return _context.Ordr
                        .Include(a => a.Pprt.OrdrType)
                        .Include(a => a.Pprt.ProdType)
                        .Include(a => a.NccoOrdr)
                            .ThenInclude(a => a.OrdrType)
                        .Where(predicate);
                }
                else
                {
                    return _context.Ordr
                        .Include(a => a.Pprt.OrdrType)
                        .Include(a => a.Pprt.ProdType)
                        .Include(a => a.FsaOrdr)
                            .ThenInclude(a => a.OrdrTypeCdNavigation)
                        .Where(predicate);
                }
            }

            return null;
        }

        public bool UpdateCustomerTurnUpTask(int orderId)
        {
            var tOrdr = (from act in _context.ActTask
                         where act.OrdrId == orderId
                          && act.TaskId == 213
                          && act.StusId == 2
                         select act).SingleOrDefault();

            if (tOrdr != null)
            {
                ActTask atask = (ActTask)tOrdr;
                atask.StusId = 0;
                atask.ModfdDt = atask.CreatDt = DateTime.Now;

                _context.SaveChanges();

                var ordrMS = (from oMs in _context.OrdrMs
                              where oMs.OrdrId == orderId
                              select oMs).SingleOrDefault();

                if (ordrMS != null)
                {
                    OrdrMs oMS = (OrdrMs)ordrMS;
                    oMS.CustAcptcTurnupDt = null;
                    oMS.CreatDt = DateTime.Now;
                    _context.SaveChanges();
                }

                return true;
            }
            return false;
        }

        public IEnumerable<InstallPort> GetInstallPort(int orderId)
        {
            DataSet result = new DataSet();
            string[] sTableName = {     "Order Info",
                                            "H1 Info",
                                            "H4 Info",
                                            "H6 Info",
                                            "H5 Info",
                                            "Acct. Team Info",
                                            "MDS Inst Addr",
                                            "MDS Inst Contact",
                                            "MDS Line Items",
                                            "CPE Line Items",
                                            "VAS",
                                            "VAS Billing",
                                            "Trpt Sup Contact Info",
                                            "Trpt Billing Info",
                                            "Trpt Port Contact Info",
                                            "Trpt Port Billing Info",
                                            "Install Billing Info",
                                            "Disconnect Transport Line Items"
                                            ,"Port IP Address"
                                            ,"Port VLANs"
                                            ,"H4 Hierarchy Info"
                                            ,"H6 Hierarchy Info"
                                            ,"ES Hierarchy Info"
                                            ,"Disconnect VAS Line Items"
                                            ,"Billing Only Line Items"
                                            ,"MDS Disconnect Line Items"
                                            ,"CPE Disconnect Line Items"
                                            ,"Disconnect Contact"
                                            ,"Change Billing Line Items"
                                            ,"Disconnect Billing Line Items"
                                            ,"Accs Port Line Items"
                                            ,"PreQual"
                                      };
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.getFSAOrderDetails @OrderID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@OrderID", SqlDbType = SqlDbType.Int, Value = orderId });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    for (int i = 0; i < sTableName.Length; i++)
                    {
                        var table = new DataTable(sTableName[i]);
                        table.Load(reader);

                        result.Tables.Add(table);
                    }

                    //do
                    //{
                    //    var table = new DataTable();
                    //    table.Load(reader);

                    //    result.Tables.Add(table);
                    //} while (!reader.IsClosed);
                }
                _context.Database.CloseConnection();
            }

            List<InstallPort> installPort = new List<InstallPort>();
            DataTable dtAccsPortLineItemsInfo = new DataTable();
            dtAccsPortLineItemsInfo = result.Tables["Accs Port Line Items"];
            if (dtAccsPortLineItemsInfo != null && dtAccsPortLineItemsInfo.Rows.Count > 0)
            {
                foreach (DataRow drInfo in dtAccsPortLineItemsInfo.Rows)
                {
                    installPort.Add(new InstallPort
                    {
                        MdsDes = drInfo["MDS_DES"].ToString(),
                        M5PortStatus = drInfo["TPORT_M5_STUS_CD"].ToString(),
                        TtrptSpdOfSrvcBdwdDes = drInfo["TTRPT_SPD_OF_SRVC_BDWD_DES"].ToString(),
                        TportEthAccsTypeCd = drInfo["TPORT_ETH_ACCS_TYPE_CD"].ToString(),
                        SpaAccsIndcrDes = drInfo["TPORT_SPAE_ACCS_INDCTR"].ToString(),
                        TportVndrQuoteId = drInfo["TPORT_VNDR_QUOTE_ID"].ToString(),
                        TportEthrntNrfcIndcr = drInfo["TPORT_ETHRNT_NRFC_INDCR"].ToString(),
                        TportCnctrTypeId = drInfo["TPORT_CNCTR_TYPE_ID"].ToString(),
                        CustRoutrTagTxt = drInfo["TPORT_CUST_ROUTR_TAG_TXT"].ToString(),
                        CustRoutrAutoNegotCd = drInfo["TPORT_CUST_ROUTR_AUTO_NEGOT_CD"].ToString(),
                        IpVerTypeCd = drInfo["TPORT_IP_VER_TYPE_CD"].ToString(),
                        Ipv4AdrPrvdrCd = drInfo["TPORT_IPV4_ADR_PRVDR_CD"].ToString(),
                        Ipv4AdrQty = drInfo["TPORT_IPV4_ADR_QTY"].ToString(),
                        Ipv6AdrPrvdrCd = drInfo["TPORT_IPV6_ADR_PRVDR_CD"].ToString(),
                        Ipv6AdrQty = drInfo["TPORT_IPV6_ADR_QTY"].ToString(),
                        VlanQty = drInfo["TPORT_VLAN_QTY"].ToString(),
                        DiaNocToNocTxt = drInfo["TPORT_DIA_NOC_TO_NOC_TXT"].ToString(),
                        ProjDes = drInfo["TPORT_PROJ_DES"].ToString()
                    });
                }
            }

            return installPort;
        }

        public IEnumerable<LogicallisData> RequestToLogicallis(int orderId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.getLogicallisEmailData @ORDR_ID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@ORDR_ID", SqlDbType = SqlDbType.Int, Value = orderId });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    IEnumerable<LogicallisData> result = reader.Translate<LogicallisData>();

                    return result;
                }
            }
        }
    }
}