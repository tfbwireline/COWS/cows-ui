﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace COWS.Data.Repository
{
    public class SearchRepository : ISearchRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;
        
        public SearchRepository(COWSAdminDBContext context,
            ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public IEnumerable<AdvancedSearchOrder> GetAdvancedSearchOrderResult(dynamic model, string adid)
        {
            List<SqlParameter> param = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@FTNs",           SqlDbType = SqlDbType.VarChar,      Value = model.M5_no ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@Names",          SqlDbType = SqlDbType.VarChar,      Value = model.Name ?? DBNull.Value },
                
                new SqlParameter() { ParameterName = "@H5names",         SqlDbType = SqlDbType.VarChar,      Value = model.H5Names ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@OrderTypes",     SqlDbType = SqlDbType.VarChar,      Value = model.OrderType ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@OrderSubType",   SqlDbType = SqlDbType.VarChar,      Value = model.OrderSubType ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@CCDs",           SqlDbType = SqlDbType.DateTime,     Value = model.Ccd ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@DeviceID",       SqlDbType = SqlDbType.VarChar,      Value = model.DeviceId ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@H5_H6",          SqlDbType = SqlDbType.VarChar,      Value = model.H5_h6 ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@H1",             SqlDbType = SqlDbType.VarChar,      Value = model.H1 ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@PRSQuote",       SqlDbType = SqlDbType.VarChar,      Value = model.PrsQuote ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@SOI",            SqlDbType = SqlDbType.VarChar,      Value = model.Soi ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@WorkGroup",      SqlDbType = SqlDbType.VarChar,      Value = model.WorkGroup ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@ProductType",    SqlDbType = SqlDbType.VarChar,      Value = model.ProductType ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@PlatformType",   SqlDbType = SqlDbType.VarChar,      Value = model.PlatformType ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@PrivateLine",    SqlDbType = SqlDbType.VarChar,      Value = model.PrivateLine ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@VendorCircuit",  SqlDbType = SqlDbType.VarChar,      Value = model.VendorCircuit ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@Region",         SqlDbType = SqlDbType.VarChar,      Value = model.Region ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@VendorName",     SqlDbType = SqlDbType.VarChar,      Value = model.VendorName ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@Status",         SqlDbType = SqlDbType.VarChar,      Value = model.Status ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@NUA",            SqlDbType = SqlDbType.VarChar,      Value = model.Nua ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@ParentFTN",      SqlDbType = SqlDbType.VarChar,      Value = model.ParentM5_no ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@RelFTN",         SqlDbType = SqlDbType.VarChar,      Value = (model.IsRelatedFtn ?? false) ? "TRUE" : "FALSE" },

                new SqlParameter() { ParameterName = "@PRCH_ORDR_NBR",   SqlDbType = SqlDbType.VarChar,      Value = model.PurchaseOrderNumber ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@ATLAS_WRK_ORDR_NBR",  SqlDbType = SqlDbType.VarChar,      Value = model.AtlasWorkOrderNumber ?? DBNull.Value },
                
                new SqlParameter() { ParameterName = "@PLSFT_RQSTN_NBR",  SqlDbType = SqlDbType.VarChar,      Value = model.PeopleSoftRequisitionNumber ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@CCDOps",             SqlDbType = SqlDbType.VarChar,      Value = model.CcdOps ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@CCDOption",         SqlDbType = SqlDbType.VarChar,      Value = model.CcdOption ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@REQ_BY_USR_ID",         SqlDbType = SqlDbType.Int,          Value = model.ReqByUsrId ?? DBNull.Value},
                new SqlParameter() { ParameterName = "@RptSchedule",     SqlDbType = SqlDbType.VarChar,      Value = model.RptSchedule ?? DBNull.Value },                
                new SqlParameter() { ParameterName = "@AdhocEmailChk",         SqlDbType = SqlDbType.VarChar,      Value = model.AdhocEmailChk ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@PreQual_ID ",            SqlDbType = SqlDbType.VarChar,      Value = model.PreQualdId ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@CSGLvlId",         SqlDbType = SqlDbType.Int,          Value = model.CsgLvlId ?? DBNull.Value},
                new SqlParameter() { ParameterName = "@SiteID",       SqlDbType = SqlDbType.VarChar,      Value = model.SiteId ?? DBNull.Value }
            };
            var infos = _context.Query<AdvancedSearchOrder>().AsNoTracking()
                .FromSql(
                    "web.getAdvSearchOrderDetails @FTNs, @Names, @H5names, @OrderTypes, @OrderSubType, @CCDs, @DeviceID, @H5_H6, @H1, @PRSQuote, @SOI, @WorkGroup, " +
                    "@ProductType, @PlatformType, @PrivateLine, @VendorCircuit, @Region, @VendorName, @Status, @NUA, @ParentFTN, @RelFTN, @PRCH_ORDR_NBR, @ATLAS_WRK_ORDR_NBR, " +
                    "@PLSFT_RQSTN_NBR, @CCDOps, @CCDOption, @REQ_BY_USR_ID, @RptSchedule, @AdhocEmailChk, @PreQual_ID, @CSGLvlId, @SiteID",
                    //"web.getAdvSearchOrderDetails @FTNs, @Names, @OrderTypes, @OrderSubType, @CCDs, @DeviceID, @H5_H6, @H1, @PRSQuote, @SOI, @WorkGroup, @ProductType, @PrivateLine, " +
                    //"@VendorCircuit, @Region, @VendorName, @NUA, @ParentFTN, @RelFTN",
                    param.ToArray()
                ).ToList();

            // Added by Sarah Sandoval [20200423]
            // Removed Workgroup Name (WGID) not included in rewrite:
            // MDS (2), WBO (8), DBB
            // All Workgroup that doesn't have WGID uses default value 0
            if (infos != null && infos.Count() > 0)
            {
                int[] notIncludeIds = new int[] { 2, 8 };
                infos = infos.Where(i => !notIncludeIds.Contains(i.WGID)).ToList();

                if(infos.Count() == 1)
                {
                    
                    var info = infos[0];
                    if(info.CSG_LVL_ID > 0 && adid != null)
                    {
                        var UserCsg = _common.GetCSGLevelAdId(adid);
                        _common.LogWebActivity(((byte)WebActyType.OrderSearch), info.FTN, adid, (byte)UserCsg, (byte)info.CSG_LVL_ID);
                    }
                }

            }

            return infos;
        }
        
        public IEnumerable<AdvancedSearchRedesign> GetAdvancedSearchRedesignResult(dynamic model, string adid)
        {
            List<SqlParameter> param = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@RedesignNumbers",     SqlDbType = SqlDbType.VarChar,      Value = model.RedesignNo ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@CustomerNames",       SqlDbType = SqlDbType.VarChar,      Value = model.Customer ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@NTE",                 SqlDbType = SqlDbType.VarChar,      Value = model.NteAssigned?? DBNull.Value},
                new SqlParameter() {ParameterName = "@ODIEDevices",         SqlDbType = SqlDbType.VarChar,      Value = model.OdieDeviceName ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@PM",                  SqlDbType = SqlDbType.VarChar,      Value = model.PmAssigned ?? DBNull.Value}, // Has some kind of issue
                new SqlParameter() {ParameterName = "@H1",                  SqlDbType = SqlDbType.VarChar,      Value = model.H1 ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@RedesignCat",         SqlDbType = SqlDbType.Int,          Value = model.RedesignCategory ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@RedesignType",        SqlDbType = SqlDbType.VarChar,      Value = model.RedesignType ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@RedesignStus",        SqlDbType = SqlDbType.Int,          Value = model.RedesignStatus ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@EventID",             SqlDbType = SqlDbType.Int,          Value = model.EventId ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@MSSSE",               SqlDbType = SqlDbType.VarChar,      Value = model.MssSeAssigned ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@NE",                  SqlDbType = SqlDbType.DateTime,     Value = model.NeAssigned ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@SubmittedDate",       SqlDbType = SqlDbType.DateTime,     Value = model.SubmittedDate ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@SLADueDate",          SqlDbType = SqlDbType.DateTime,     Value = model.SlaDueDate ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@ExpirationDate",      SqlDbType = SqlDbType.DateTime,     Value = model.ExpirationDate?? DBNull.Value},
                new SqlParameter() {ParameterName = "@Status",              SqlDbType = SqlDbType.VarChar,      Value = model.Status ?? DBNull.Value}, // Has some kind of issue
                new SqlParameter() { ParameterName = "@CSGLvlId",         SqlDbType = SqlDbType.Int,          Value = model.CsgLvlId ?? DBNull.Value},
            };
            var infos = _context.Query<AdvancedSearchRedesign>().AsNoTracking()
                .FromSql(
                    "web.getAdvSearchRedesignInfo @RedesignNumbers, @CustomerNames, @NTE, @ODIEDevices, @PM, @H1, @RedesignCat, @RedesignType, @RedesignStus, @EventID, @MSSSE, @NE, " +
                    "@SubmittedDate, @SLADueDate, @ExpirationDate, @Status, @CSGLvlId",
                    param.ToArray()
                ).ToList();

            if (infos.Count() == 1)
            {
                var info = infos[0];
                if (info.CSG_LVL_ID > 0 && adid != null)
                {
                    var UserCsg = _common.GetCSGLevelAdId(adid);
                    _common.LogWebActivity(((byte)WebActyType.RedesignSearch), info.RedesignID, adid, (byte)UserCsg, (byte)info.CSG_LVL_ID);
                }

            }


            return infos;
        }

        public IEnumerable<AdvancedSearchEvent> GetAdvancedSearchEventResult(dynamic model, string adid)
        {
            List<SqlParameter> param = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@FTNs",            SqlDbType = SqlDbType.VarChar,      Value = model.M5_no ?? DBNull.Value},
                 new SqlParameter() {ParameterName ="@SOWSIds",        SqlDbType = SqlDbType.VarChar,      Value = model.SowsId  ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@AssignedUsers",   SqlDbType = SqlDbType.VarChar,      Value = model.AssignedName ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@RequestorNames",  SqlDbType = SqlDbType.VarChar,      Value = model.RequestorName ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@EventTypes",      SqlDbType = SqlDbType.VarChar,      Value = model.EventType ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@EventIDs",        SqlDbType = SqlDbType.VarChar,      Value = model.EventId ?? DBNull.Value}, 
				new SqlParameter() {ParameterName = "@ADType",          SqlDbType = SqlDbType.VarChar,      Value = model.AdType?? DBNull.Value},
                new SqlParameter() {ParameterName = "@CustName",        SqlDbType = SqlDbType.VarChar,      Value = model.Customer ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@FMSCkt",          SqlDbType = SqlDbType.VarChar,      Value = model.FmsCircuit ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@NUA",             SqlDbType = SqlDbType.VarChar,      Value = model.NUA ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@EventStatus",     SqlDbType = SqlDbType.VarChar,      Value = model.EventStatus ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@ActionType",      SqlDbType = SqlDbType.VarChar,      Value = model.FtActivityType ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@Device",          SqlDbType = SqlDbType.VarChar,      Value = model.DeviceName ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@H5_H6",           SqlDbType = SqlDbType.VarChar,      Value = model.H5_h6 ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@StrtDt",          SqlDbType = SqlDbType.DateTime,     Value = model.ApptStartDate ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@EndDt",           SqlDbType = SqlDbType.DateTime,     Value = model.ApptEndDate ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@FRBID",           SqlDbType = SqlDbType.VarChar,      Value = model.FrbRequestId ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@Serial",          SqlDbType = SqlDbType.VarChar,      Value = model.DeviceSerialNo ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@DesignType",      SqlDbType = SqlDbType.VarChar,      Value = model.FrbConfigurationType ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@OrgID",           SqlDbType = SqlDbType.VarChar,      Value = model.FrbOrgId ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@ActType",         SqlDbType = SqlDbType.VarChar,      Value = model.FrbActivityType ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@Status",          SqlDbType = SqlDbType.VarChar,      Value = model.Status ?? DBNull.Value}, 
                new SqlParameter() {ParameterName = "@ReDesign",        SqlDbType = SqlDbType.VarChar,      Value = model.RedesignNo ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@apptStartOp",      SqlDbType = SqlDbType.VarChar,     Value = model.ApptStartOp ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@apptStartOption",  SqlDbType = SqlDbType.VarChar,     Value = model.ApptStartOption ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@apptEndOp",        SqlDbType = SqlDbType.VarChar,     Value = model.ApptEndOp?? DBNull.Value},
                new SqlParameter() {ParameterName = "@apptEndOption",    SqlDbType = SqlDbType.VarChar,     Value = model.ApptEndOption ?? DBNull.Value},

                new SqlParameter() {ParameterName = "@MDSActivityType",   SqlDbType = SqlDbType.VarChar,      Value = model.MdsActivityType ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@MDSMACType",       SqlDbType = SqlDbType.VarChar,      Value = (model.MdsMacType != null ? String.Join(",", model.MdsMacType) : DBNull.Value)},
                new SqlParameter() { ParameterName = "@REQ_BY_USR_ID",         SqlDbType = SqlDbType.Int,          Value = model.ReqByUsrId ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@RptSchedule",      SqlDbType = SqlDbType.Char,         Value = model.RptSchedule ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@AdhocEmailChk",    SqlDbType = SqlDbType.VarChar,      Value = model.AdhocEmailChk ?? DBNull.Value},
                new SqlParameter() { ParameterName = "@CSGLvlId",         SqlDbType = SqlDbType.Int,          Value = model.CsgLvlId ?? DBNull.Value},
                new SqlParameter() {ParameterName = "@MDSNtwkActyType",    SqlDbType = SqlDbType.VarChar,      Value = model.MdsNtwkActyType ?? DBNull.Value},
            };
            var infos = _context.Query<AdvancedSearchEvent>().AsNoTracking()
                .FromSql(
                    "web.getAdvSearchEventDetails @FTNs, @SOWSIds, @AssignedUsers, @RequestorNames, @EventTypes, @EventIDs, @ADType, @CustName, @FMSCkt, @NUA, @EventStatus, @ActionType, " +
               "@Device, @H5_H6, @StrtDt, @EndDt, @FRBID, @Serial, @DesignType, @OrgID, @ActType, @Status, @ReDesign, @apptStartOp, @apptStartOption, @apptEndOp, @apptEndOption, " +
              "@MDSActivityType, @MDSMACType, @REQ_BY_USR_ID, @RptSchedule, @AdhocEmailChk, @CSGLvlId, @MDSNtwkActyType",
                    param.ToArray()
                ).ToList();

            // Added condition by Sarah Sandoval [20200504]
            // For CAND User search results should contain only CAND Events
            // Added condition to return CAND Events only if the user has only CAND Profiles
            //int reqUsrId = model.ReqByUsrId;
            //var profiles = _context.MapUsrPrf.Where(i => i.UserId == reqUsrId && i.RecStusId == (byte)ERecStatus.Active);
            //if (profiles != null && profiles.Count() > 0)
            //{
            //    // Added condition by Sarah Sandoval [20200529] for Archive Profile
            //    // Added the newly Archive Profile that returns CAND Events as well
            //    //var candProfiles = _context.LkUsrPrf.Where(i => i.UsrPrfNme.Contains("CAND") || i.UsrPrfNme.Contains("Archive")).Select(i => i.UsrPrfId);
            //    var archiveProfiles = _context.LkUsrPrf.Where(i => i.UsrPrfNme.Contains("Archive")).Select(i => i.UsrPrfId);
            //    var usrPrfIds = profiles.Select(i => i.UsrPrfId);

            //    if (usrPrfIds.Where(i => archiveProfiles.Contains(i)).Count() > 0
            //        && usrPrfIds.Where(i => !archiveProfiles.Contains(i)).Count() == 0)
            //    {
            //        var candEvents = new string[] { "AD", "MPLS", "NGVN", "SprintLink" };
            //        infos = infos.Where(i => candEvents.Contains(i.EventType)).ToList();
            //    }
            //}

            if (infos.Count() == 1)
            {
                var info = infos[0];
                if (info.CSG_LVL_ID > 0 && adid != null)
                {
                    var UserCsg = _common.GetCSGLevelAdId(adid);
                    _common.LogWebActivity(((byte)WebActyType.EventSearch), info.EventID, adid, (byte)UserCsg, (byte)info.CSG_LVL_ID);
                }
            }

            return infos;
        }

        public GetM5CANDEventDataResults M5OrH6Lookup(string m5, string h6, int? eventType, int vasCd = 0, bool isCEChng = false)
        {
            if (eventType == null)
            {
                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = "web.getM5CANDEventData @M5OrderNbr, @H6";
                    command.Parameters.Add(new SqlParameter() { ParameterName = "@M5OrderNbr", SqlDbType = SqlDbType.VarChar, Value = m5 });
                    command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });

                    _context.Database.OpenConnection();
                    using (var reader = command.ExecuteReader())
                    {
                        var result = new GetM5CANDEventDataResults
                        {
                            Result1 = reader.Translate<GetM5CANDEventDataResult1>(),
                            Result2 = reader.Translate<GetM5CANDEventDataResult2>(),
                            Result3 = reader.Translate<GetM5CANDEventDataResult3>()
                        };

                        return result;
                    }
                }
            }
            else
            {
                //SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
                ////SqlConnection connection = new SqlConnection(_configuration.GetSection("ConnectionStrings:SqlDbConn").Value);
                //SqlCommand command = new SqlCommand();
                //SqlDataAdapter da = new SqlDataAdapter();
                //DataSet ds = new DataSet();
                //var cmd = new SqlCommand("web.getM5CANDEventData", connection);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter() { ParameterName = "@M5OrderNbr", SqlDbType = SqlDbType.VarChar, Value = m5 });
                //cmd.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });
                //cmd.Parameters.Add(new SqlParameter() { ParameterName = "@EType", SqlDbType = SqlDbType.TinyInt, Value = eventType });
                //da = new SqlDataAdapter(cmd);
                //da.Fill(ds);
                //connection.Close();

                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = "web.getM5CANDEventData @M5OrderNbr, @H6, @EType, @VASCEFlg, @CEChngFlg";
                    command.Parameters.Add(new SqlParameter() { ParameterName = "@M5OrderNbr", SqlDbType = SqlDbType.VarChar, Value = m5 });
                    command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });
                    command.Parameters.Add(new SqlParameter() { ParameterName = "@EType", SqlDbType = SqlDbType.TinyInt, Value = eventType });
                    command.Parameters.Add(new SqlParameter() { ParameterName = "@VASCEFlg", SqlDbType = SqlDbType.TinyInt, Value = vasCd });
                    command.Parameters.Add(new SqlParameter() { ParameterName = "@CEChngFlg", SqlDbType = SqlDbType.Bit, Value = isCEChng });

                    _context.Database.OpenConnection();
                    using (var reader = command.ExecuteReader())
                    {
                        GetM5CANDEventDataResults result = null;

                        if (eventType == (byte)EventType.MDS)
                        {
                            var Result4 = reader.Translate<GetM5CANDEventDataResult4>();
                            var Result5 = reader.Translate<GetM5CANDEventDataResult5>();
                            var Result6 = reader.Translate<GetM5CANDEventDataResult6>();
                            var Result7 = reader.Translate<GetM5CANDEventDataResult7>();
                            result = new GetM5CANDEventDataResults
                            {
                                Result1 = new List<GetM5CANDEventDataResult1>(),
                                Result2 = new List<GetM5CANDEventDataResult2>(),
                                Result3 = new List<GetM5CANDEventDataResult3>(),
                                Result4 = Result4,
                                Result5 = Result5,
                                Result6 = Result6,
                                Result7 = Result7
                            };

                            //result = new GetM5CANDEventDataResults
                            //{
                            //    Result1 = new List<GetM5CANDEventDataResult1>(),
                            //    Result2 = new List<GetM5CANDEventDataResult2>(),
                            //    Result3 = new List<GetM5CANDEventDataResult3>(),
                            //    Result4 = reader.Translate<GetM5CANDEventDataResult4>(),
                            //    Result5 = reader.Translate<GetM5CANDEventDataResult5>(),
                            //    Result6 = reader.Translate<GetM5CANDEventDataResult6>()
                            //};
                        }
                        else
                        {
                            result = new GetM5CANDEventDataResults
                            {
                                Result1 = reader.Translate<GetM5CANDEventDataResult1>(),
                                Result2 = reader.Translate<GetM5CANDEventDataResult2>(),
                                Result3 = reader.Translate<GetM5CANDEventDataResult3>(),
                                Result4 = new List<GetM5CANDEventDataResult4>(),
                                Result5 = new List<GetM5CANDEventDataResult5>(),
                                Result6 = new List<GetM5CANDEventDataResult6>()
                            };
                        }

                        return result;
                    }
                }
                //return null;
            }
        }

        public GetM5EventDatabyH6Results M5LookupByH6CCD(string h6, string ccd, string ucaasCD = "Y", bool isCarrierEthernet = false)
        {
            //using (var command = _context.Database.GetDbConnection().CreateCommand())
            //{
            //    command.CommandText = "web.getM5EventDatabyH6 @H6, @CCD, @UCaaSCD, @CEFlg";
            //    command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });
            //    command.Parameters.Add(new SqlParameter() { ParameterName = "@CCD", SqlDbType = SqlDbType.VarChar, Value = ccd });
            //    command.Parameters.Add(new SqlParameter() { ParameterName = "@UCaaSCD", SqlDbType = SqlDbType.Char, Value = ucaasCD });
            //    command.Parameters.Add(new SqlParameter() { ParameterName = "@CEFlg", SqlDbType = SqlDbType.Bit, Value = isCarrierEthernet ? 1 : 0 });
            //    command.Parameters.Add(new SqlParameter() { ParameterName = "@IsNewUI", SqlDbType = SqlDbType.Bit, Value = 1 });

            //    _context.Database.OpenConnection();
            //    using (var reader = command.ExecuteReader())
            //    {
            //        string name = "a";
            //        DataSet asd = new DataSet();
            //        while (reader.NextResult())
            //        {
            //            asd.Tables.Add(name);
            //            asd.Tables[name].Load(reader);

            //            name = name + "a";
            //        }
            //    }
            //}

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "web.getM5EventDatabyH6 @H6, @CCD, @UCaaSCD, @CEFlg";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CCD", SqlDbType = SqlDbType.VarChar, Value = ccd });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@UCaaSCD", SqlDbType = SqlDbType.Char, Value = ucaasCD });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CEFlg", SqlDbType = SqlDbType.Bit, Value = isCarrierEthernet ? 1 : 0 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@IsNewUI", SqlDbType = SqlDbType.Bit, Value = 1 });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    var result = new GetM5EventDatabyH6Results();

                    if (ucaasCD == "Y")
                    {
                        result.M5CpeTable = reader.Translate<M5CpeTableResult>();
                        result.M5CustTable = reader.Translate<M5CustTableResult>();
                        result.M5AdrTable = reader.Translate<M5AdrTableResult>();
                        result.M5H1BasedOnH6 = reader.Translate<M5H1BasedOnH6Result>();
                        result.M5RelMnsTable = reader.Translate<M5RelMnsTableResult>();
                        //result.M5DslsbicTable = reader.Translate<M5DslsbicTableResult>();
                        result.CPEDispatchEmail = reader.Translate<CPEDispatchEmailResult>();
                    }
                    else
                    {
                        result.M5CpeTable = reader.Translate<M5CpeTableResult>();
                        result.M5CustTable = reader.Translate<M5CustTableResult>();
                        result.M5AdrTable = reader.Translate<M5AdrTableResult>();
                        result.M5MnsTable = reader.Translate<M5MnsTableResult>();
                        result.M5RelMnsTable = reader.Translate<M5RelMnsTableResult>();
                        //result.M5DslsbicTable = reader.Translate<M5DslsbicTableResult>();
                        result.M5DslsbicTable = new List<M5DslsbicTableResult>();
                        result.M5CpeAsasTable = reader.Translate<M5CpeAsasTableResult>();
                        result.M5PortTable = reader.Translate<M5PortTableResult>();
                        result.CPEDispatchEmail = reader.Translate<CPEDispatchEmailResult>();
                        result.NrmWiredTable = reader.Translate<NrmWiredTableResult>();
                    }

                    return result;
                }
            }
        }

        public GetM5CPEOrderDataResults M5CPEOrderData(string h6, string ccd, int eventId, string ucaasCD = "Y")
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "web.getM5CPEOrderData @H6, @CCD, @UCaaSCD, @EventID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.VarChar, Value = h6 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CCD", SqlDbType = SqlDbType.VarChar, Value = ccd });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@UCaaSCD", SqlDbType = SqlDbType.Char, Value = ucaasCD });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EventID", SqlDbType = SqlDbType.Int, Value = eventId });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    var result = new GetM5CPEOrderDataResults
                    {
                        Result1 = reader.Translate<GetM5CPEOrderDataResult1>(),
                        Result2 = reader.Translate<GetM5CPEOrderDataResult2>(),
                        Result3 = reader.Translate<GetM5CPEOrderDataResult3>()
                    };

                    return result;
                }
            }
        }

        public List<RedsgnDevicesInfo> RetrieveRedesign(string h1, string custName, int eventTypeId, byte userCsgLvl)
        {
            //var devices = _context.Redsgn
            //                .Include(r => r.RedsgnDevicesInfo)
            //                .Where(r => r.H1Cd.Trim() == h1.Trim() && r.ExprtnDt.HasValue
            //                            && (r.StusId == (short)RedesignStatus.Approved
            //                                    || r.StusId == (short)RedesignStatus.Completed)
            //                            && ((eventTypeId == (int)EventType.UCaaS && r.RedsgnCatId == 1)
            //                                    || (eventTypeId == (int)EventType.MDS && r.RedsgnCatId != 1))
            //                            && ((r.CsgLvlId == 0 && r.CustNme.Trim().Equals(custName.Trim())) ||
            //                                (r.CsgLvlId > 0 && userCsgLvl > 0 && userCsgLvl <= r.CsgLvlId
            //                                    && _common.GetDecryptValue(_context.CustScrdData
            //                                        .SingleOrDefault(i => i.ScrdObjId == r.RedsgnId
            //                                            && i.ScrdObjTypeId == (byte)SecuredObjectType.REDSGN).CustNme).Trim() == custName.Trim()))
            //                            && r.RedsgnDevicesInfo.Any(i => (i.RecStusId == true
            //                                && (i.DspchRdyCd.HasValue && i.DspchRdyCd.Value)
            //                                && (!i.DevCmpltnCd.HasValue || !i.DevCmpltnCd.Value))))
            //                .Distinct().ToList();

            var devices = _context.RedsgnDevicesInfo
                            .Include(r => r.Redsgn)
                            .Where(r => r.Redsgn.H1Cd.Trim() == h1.Trim() && r.Redsgn.ExprtnDt.HasValue
                                        && (r.Redsgn.StusId == (short)RedesignStatus.Approved
                                                || r.Redsgn.StusId == (short)RedesignStatus.Completed)
                                        && ((eventTypeId == (int)EventType.UCaaS && r.Redsgn.RedsgnCatId == (byte)RedesignCategory.Voice)
                                                || (eventTypeId == (int)EventType.MDS && r.Redsgn.RedsgnCatId != (byte)RedesignCategory.Voice))
                                        && ((r.Redsgn.CsgLvlId == 0 && r.Redsgn.CustNme.Trim().Equals(custName.Trim())) ||
                                            (r.Redsgn.CsgLvlId > 0 && userCsgLvl > 0 && userCsgLvl <= r.Redsgn.CsgLvlId
                                                && _common.GetDecryptValue(_context.CustScrdData
                                                    .SingleOrDefault(i => i.ScrdObjId == r.RedsgnId
                                                        && i.ScrdObjTypeId == (byte)SecuredObjectType.REDSGN).CustNme).Trim() == custName.Trim()))
                                        && r.RecStusId == true
                                        && (r.DspchRdyCd.HasValue && r.DspchRdyCd.Value)
                                        && (!r.DevCmpltnCd.HasValue || (r.DevCmpltnCd.HasValue && !r.DevCmpltnCd.Value)))
                            .Distinct().ToList();

            var ctr = devices.Count();

            return devices;

            //var x = (from rd in _context.Redsgn
            //         join ri in _context.RedsgnDevicesInfo on rd.RedsgnId equals ri.RedsgnId
            //         join lrt in _context.LkRedsgnType on rd.RedsgnTypeId equals lrt.RedsgnTypeId
            //         join lpu in _context.LkUser on rd.PmAssigned equals lpu.DsplNme
            //         into joinlpu
            //         from lpu in joinlpu.DefaultIfEmpty()
            //         join lnu in _context.LkUser on rd.NteAssigned equals lnu.DsplNme
            //         into joinlnu
            //         from lnu in joinlnu.DefaultIfEmpty()
            //         join csd in _context.CustScrdData on new { rd.RedsgnId, ScrdObjTypeId = (byte)SecuredObjectType.REDSGN } equals new { RedsgnId = csd.ScrdObjId, csd.ScrdObjTypeId }
            //                    into joincsd
            //         from csd in joincsd.DefaultIfEmpty()
            //         where ((rd.H1Cd == h1) && (rd.ExprtnDt != null)
            //         && (ri.RecStusId == true)
            //         && (((eventTypeId == (int)EventType.UCaaS) && (lrt.RedsgnCatId == 1))
            //             || ((eventTypeId == (int)EventType.MDS) && (lrt.RedsgnCatId != 1)))
            //         && ((rd.StusId == ((short)RedesignStatus.Approved)) || (rd.StusId == ((short)RedesignStatus.Completed)))
            //         && ((ri.DspchRdyCd.HasValue) && ((bool)ri.DspchRdyCd))
            //         && ((!ri.DevCmpltnCd.HasValue) || (!((bool)ri.DevCmpltnCd.Value)))
            //         )
            //         select new
            //         {
            //             CUST_NME = (rd.CsgLvlId == 0) ? rd.CustNme : (((rd.CsgLvlId > 0)
            //                        && (userCsgLvl != 0) && (userCsgLvl <= rd.CsgLvlId)) ? _common.GetDecryptValue(csd.CustNme) : string.Empty),
            //             NTE_EMAIL = lnu.EmailAdr,
            //             PM_EMAIL = lpu.EmailAdr,
            //             rd.H1Cd,
            //             ri.RedsgnDevId,
            //             rd.RedsgnId,
            //             rd.ExprtnDt,
            //             rd.RedsgnNbr,
            //             ri.DevNme,
            //             ri.H6CustId,
            //             ri.FastTrkCd
            //         }).Distinct().ToList();

            //List<RedsgnDevicesInfo> mdi = new List<RedsgnDevicesInfo>();
            //var y = x.Where(x1 => ((x1.CUST_NME != null) && (x1.CUST_NME.Trim() == custName.Trim())));
            //if (y != null)
            //{
            //    int j = 1;
            //    foreach (var i in y)
            //    {
            //        RedsgnDevicesInfo mo = new RedsgnDevicesInfo();
            //        mo.RedsgnDevId = i.RedsgnDevId;
            //        mo.RedsgnId = i.RedsgnId;
            //        mo.Redsgn = _context.Redsgn.SingleOrDefault(item => item.RedsgnId == i.RedsgnId);
            //        //mo.RdsnExpDt = (i.EXPRTN_DT == null) ? string.Empty : ((DateTime)i.EXPRTN_DT).ToString("MM/dd/yyyy");
            //        mo.DevNme = i.DevNme;
            //        mo.H6CustId = i.H6CustId;
            //        mo.FastTrkCd = i.FastTrkCd;
            //        //mo.PMEmail = i.PM_EMAIL;
            //        //mo.NTEEmail = i.NTE_EMAIL;
            //        //mo.SysCd = true;

            //        mdi.Add(mo);
            //    }
            //}

            //return mdi;
        }
    }
}