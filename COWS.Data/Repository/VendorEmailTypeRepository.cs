﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VendorEmailTypeRepository : IVendorEmailTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public VendorEmailTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkVndrEmailType> Find(Expression<Func<LkVndrEmailType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkVndrEmailType, out List<LkVndrEmailType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkVndrEmailType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkVndrEmailType, out List<LkVndrEmailType> list))
            {
                list = _context.LkVndrEmailType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkVndrEmailType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkVndrEmailType GetById(int id)
        {
            return Find(a => a.VndrEmailTypeId == id).SingleOrDefault();
        }

        public LkVndrEmailType Create(LkVndrEmailType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkVndrEmailType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkVndrEmailType);
            return _context.SaveChanges();
        }
    }
}