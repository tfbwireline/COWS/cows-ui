﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UcaasProdTypeRepository : IUcaasProdTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public UcaasProdTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkUcaaSProdType Create(LkUcaaSProdType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkUcaaSProdType> Find(Expression<Func<LkUcaaSProdType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkUcaaSProdType, out List<LkUcaaSProdType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkUcaaSProdType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUcaaSProdType, out List<LkUcaaSProdType> list))
            {
                list = _context.LkUcaaSProdType
                            .OrderBy(i => i.UcaaSProdType)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkUcaaSProdType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkUcaaSProdType GetById(int id)
        {
            return _context.LkUcaaSProdType
                            .SingleOrDefault(i => i.UcaaSProdTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkUcaaSProdType);
            throw new NotImplementedException();
        }

        public void Update(int id, LkUcaaSProdType entity)
        {
            throw new NotImplementedException();
        }
    }
}