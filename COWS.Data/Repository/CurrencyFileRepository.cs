﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CurrencyFileRepository : ICurrencyFileRepository
    {
        private readonly COWSAdminDBContext _context;

        public CurrencyFileRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<SrcCurFile> Find(Expression<Func<SrcCurFile, bool>> predicate)
        {
            return _context.SrcCurFile
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<SrcCurFile> GetAll()
        {
            return _context.SrcCurFile
                .Include(a => a.CreatByUser)
                .ToList();
        }

        public SrcCurFile GetById(int id)
        {
            return _context.SrcCurFile
                .Include(a => a.CreatByUser)
                .SingleOrDefault(a => a.SrcCurFileId == id);
        }

        public SrcCurFile Create(SrcCurFile entity)
        {
            _context.SrcCurFile.Add(entity);
            SaveAll();

            return GetById(entity.SrcCurFileId);
        }

        public void Update(int id, SrcCurFile entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public int UpdateCurrencyConverter(int userId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@userID", SqlDbType.Int)
                };
            pc[0].Value = userId;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.updateCurConver @userID", pc.ToArray());
            return toReturn;
        }
    }
}