﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSFastTrackTypeRepository : IMDSFastTrackTypeRepository
    {
        private readonly COWSAdminDBContext _context;

        public MDSFastTrackTypeRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<MdsFastTrkType> Find(Expression<Func<MdsFastTrkType, bool>> predicate)
        {
            return _context.MdsFastTrkType
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<MdsFastTrkType> GetAll()
        {
            return _context.MdsFastTrkType
                            .AsNoTracking()
                            .ToList();
        }

        public MdsFastTrkType GetById(string id)
        {
            return _context.MdsFastTrkType.SingleOrDefault(i => i.MdsFastTrkTypeId == id);
        }

        public MdsFastTrkType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public MdsFastTrkType Create(MdsFastTrkType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, MdsFastTrkType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}