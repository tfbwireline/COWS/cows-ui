﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CircuitChargeRepository : ICircuitChargeRepository
    {
        private readonly COWSAdminDBContext _context;

        public CircuitChargeRepository(COWSAdminDBContext context)
        {
            _context = context;
        }
        public IQueryable<OrdrCktChg> Find(Expression<Func<OrdrCktChg, bool>> predicate)
        {
            IQueryable<OrdrCktChg> obj = _context.OrdrCktChg
                .Include(a => a.CreatByUser)
                .Where(predicate);

            return obj;
        }

        public IEnumerable<OrdrCktChg> GetAll()
        {
            throw new NotImplementedException();
        }

        public OrdrCktChg GetById(int id)
        {
            throw new NotImplementedException();
        }

        public OrdrCktChg Create(OrdrCktChg entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OrdrCktChg> Create(IEnumerable<OrdrCktChg> entity)
        {
            entity = entity
                .Select(a =>
                {
                    var temp = _context.OrdrCktChg
                        .Where(b => b.OrdrId == a.OrdrId && b.CktChgTypeId == a.CktChgTypeId)
                        .Select(b => b.VerId)
                        .DefaultIfEmpty();

                    a.VerId = (short)(temp.Max() + 1);

                    return a;
                });

            //List<OrdrCktChg> list = new List<OrdrCktChg>();
            //foreach(var obj in entity)
            //{
            //    var temp = _context.OrdrCktChg
            //        .Where(b => b.OrdrId == obj.OrdrId && b.CktChgTypeId == obj.CktChgTypeId);

            //    obj.VerId = (short)(temp.Max(a => a.VerId) + 1);
            //    list.Add(obj);
            //}

            _context.OrdrCktChg.AddRange(entity.ToList());
            SaveAll();

            return entity;
        }

        public void Update(int id, OrdrCktChg entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IEnumerable<OrdrCktChg> entity)
        {
            _context.OrdrCktChg.RemoveRange(entity);

            SaveAll();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public List<OrdrCktChg> GetAddlCharges(int OrderID, bool IsTerm)
        {
            var all = Find(a => a.OrdrId == OrderID && a.TrmtgCd == IsTerm).ToList();

            var final = Find(a =>
                a.VerId == (all
                    .Where(b => b.OrdrId == a.OrdrId && b.CktChgTypeId == a.CktChgTypeId)
                    .Select(b => b.VerId)
                    .DefaultIfEmpty()
                    .Max()))
                .ToList();

            return final;

            //List<CircuitAddlCost> toReturn = new List<CircuitAddlCost>();

            //var dd = (from myMax in
            //              (from mVer in
            //                   (from ac in _context.OrdrCktChg
            //                    where ac.OrdrId == OrderID
            //                    select new { ac.VerId, ac.CktChgTypeId, ac.TrmtgCd })
            //               group mVer by new { mVer.CktChgTypeId, mVer.TrmtgCd } into g
            //               select new { pair = g.Key, maxVer = g.Max(mVer => mVer.VerId) })
            //          join finalact in _context.OrdrCktChg on myMax.pair equals new { finalact.CktChgTypeId, finalact.TrmtgCd }
            //          join addlct in _context.LkCktChgType on finalact.CktChgTypeId equals addlct.CktChgTypeId
            //          join user in _context.LkUser on finalact.CreatByUserId equals user.UserId
            //          join c in _context.LkStus on finalact.SalsStusId equals c.StusId into leftjoin
            //          from subc in leftjoin.DefaultIfEmpty()
            //          where myMax.maxVer == finalact.VerId && finalact.OrdrId == OrderID && finalact.TrmtgCd == IsTerm
            //          select new { finalact, addlct.CktChgTypeDes, user.UserAdid, user.FullNme, subc.StusDes }).OrderBy(d => d.CktChgTypeDes);


            //foreach (var d in dd)
            //{
            //    CircuitAddlCost cac = new CircuitAddlCost
            //    {
            //        OrderID = d.finalact.OrdrId,
            //        ChargeTypeID = d.finalact.CktChgTypeId,
            //        ChargeTypeDesc = d.CktChgTypeDes,
            //        Version = d.finalact.VerId,
            //        IsTerm = d.finalact.TrmtgCd,
            //        CurrencyCd = d.finalact.ChgCurId,
            //        ChargeNRC = d.finalact.ChgNrcAmt,
            //        ChargeNRCUSD = d.finalact.ChgNrcInUsdAmt ?? 0,
            //        CreatedByDisplayName = d.FullNme,
            //        CreatedByUserName = d.UserAdid,
            //        CreatedByUserId = d.finalact.CreatByUserId,
            //        CreatedDateTime = d.finalact.CreatDt,
            //        TaxRate = d.finalact.TaxRtPctQty ?? 0,
            //        Note = d.finalact.NteTxt,
            //        BillOnly = d.finalact.ReqrBillOrdrCd ?? false
            //    };
            //    if (d.finalact.XpirnDt != null)
            //        cac.ExpirationTime = Convert.ToDateTime(d.finalact.XpirnDt);
            //    if (d.finalact.SalsStusId != null)
            //    {
            //        cac.SalesSupportStatusID = Convert.ToByte(d.finalact.SalsStusId);
            //        cac.SalesSupportStatusDes = d.StusDes;
            //    }

            //    toReturn.Add(cac);
            //}

            //return toReturn;

        }

        public List<CircuitAddlCost> GetAddlChargeHistory(int OrderID, int chargeTypeID, bool IsTerm)
        {
            List<CircuitAddlCost> toReturn = new List<CircuitAddlCost>();

            var achistory = (from ac in _context.OrdrCktChg
                             join user in _context.LkUser on ac.CreatByUserId equals user.UserId
                             join act in _context.LkCktChgType on ac.CktChgTypeId equals act.CktChgTypeId
                             join l in _context.LkStus on ac.SalsStusId equals l.StusId into leftjoin
                             from subl in leftjoin.DefaultIfEmpty()
                             where ac.OrdrId == OrderID && ac.CktChgTypeId == chargeTypeID && ac.TrmtgCd == IsTerm
                             select new { ac, user.UserAdid, user.FullNme, act.CktChgTypeDes, subl.StusDes }).OrderBy(o => o.ac.VerId);

            foreach (var ac in achistory)
            {
                CircuitAddlCost cac = new CircuitAddlCost
                {
                    OrderID = ac.ac.OrdrId,
                    ChargeTypeID = ac.ac.CktChgTypeId,
                    ChargeTypeDesc = ac.CktChgTypeDes,
                    Version = ac.ac.VerId,
                    IsTerm = ac.ac.TrmtgCd,
                    CurrencyCd = ac.ac.ChgCurId,
                    ChargeNRC = ac.ac.ChgNrcAmt,
                    ChargeNRCUSD = ac.ac.ChgNrcInUsdAmt ?? 0,
                    CreatedByDisplayName = ac.FullNme,
                    CreatedByUserName = ac.UserAdid,
                    CreatedByUserId = ac.ac.CreatByUserId,
                    CreatedDateTime = ac.ac.CreatDt,
                    TaxRate = ac.ac.TaxRtPctQty ?? 0,
                    Note = ac.ac.NteTxt,
                    BillOnly = ac.ac.ReqrBillOrdrCd ?? false
                };
                if (ac.ac.XpirnDt != null)
                    cac.ExpirationTime = Convert.ToDateTime(ac.ac.XpirnDt);
                if (ac.ac.SalsStusId != null)
                {
                    cac.SalesSupportStatusID = Convert.ToByte(ac.ac.SalsStusId);
                    cac.SalesSupportStatusDes = ac.StusDes;
                }

                toReturn.Add(cac);
            }

            return toReturn;

        }

        public List<CircuitAddlCost> GetAddlCostTypes()
        {
            List<CircuitAddlCost> toReturn = new List<CircuitAddlCost>();

            var act = (from c in _context.LkCktChgType
                       where c.RecStusId == (byte)ERecStatus.Active
                       select new { c.CktChgTypeId, c.CktChgTypeDes }).OrderBy(o => o.CktChgTypeDes);

            foreach (var a in act)
            {
                CircuitAddlCost cact = new CircuitAddlCost
                {
                    ChargeTypeID = Convert.ToByte(a.CktChgTypeId),
                    ChargeTypeDesc = a.CktChgTypeDes
                };

                toReturn.Add(cact);
            }

            return toReturn;

        }

        public List<CircuitAddlCost> GetStatus()
        {
            List<CircuitAddlCost> toReturn = new List<CircuitAddlCost>();

            var statusList = (from s in _context.LkStus
                              where s.StusTypeId == "ACSS" || s.StusId == 0
                              orderby s.StusId
                              select new { s.StusId, s.StusDes });

            foreach (var s in statusList)
            {
                CircuitAddlCost cacs = new CircuitAddlCost
                {
                    StatusID = Convert.ToByte(s.StusId),
                    StatusDesc = s.StusDes
                };

                toReturn.Add(cacs);
            }
            return toReturn;
        }

        public List<CircuitAddlCost> GetCurrencyList()
        {

            List<CircuitAddlCost> retList = new List<CircuitAddlCost>();
            CircuitAddlCost dc = null;

            var cur = (from clu in
                           (from c in _context.LkCur
                            join u in _context.LkUser on c.CreatByUserId equals u.UserId
                            where c.RecStusId == (byte)ERecStatus.Active
                            select new { c, u })
                       join mu in _context.LkUser on clu.c.ModfdByUserId equals mu.UserId
                        into joinmu
                       from mu in joinmu.DefaultIfEmpty()
                       select new { clu.c, clu.u, MU_USER_ADID = mu.UserAdid });

            //if (SearchCriteria != null && SearchCriteria != String.Empty)
            //{
            //    cur = cur.Where(a => a.c.CurNme == SearchCriteria);
            //}

            //if (SortField != null && SortField != String.Empty)
            //    cur = cur.OrderBy(SortField);
            //else
            cur = cur.OrderBy(c => c.c.CurNme);


            foreach (var d in cur)
            {
                dc = new CircuitAddlCost
                {
                    CurrencyCd = d.c.CurId,
                    CurrencyDesc = d.c.CurNme,
                    Factor = Convert.ToDouble(d.c.CurCnvrsnFctrFromUsdQty),
                    RecStatusId = d.c.RecStusId,
                    CreatedByUserId = d.u.CreatByUserId,
                    CreatedByUserName = d.u.UserAdid,
                    CreatedDateTime = d.c.CreatDt
                };
                if (d.c.ModfdByUserId != null)
                {
                    dc.ModifiedByUserId = (int)d.c.ModfdByUserId;
                    dc.ModifiedByUserName = d.MU_USER_ADID;
                    dc.ModifiedDateTime = (DateTime)d.c.ModfdDt;
                }

                retList.Add(dc);
            }
            return retList;
        }

        public int AcrOrderStatus(int orderID, bool isTerm)
        {

            //Return 2 if order is completed.
            //Return 1 if the order is past the access order submitted to vendor status
            //Return 0 if the order is before the submit to vendor status
            var isComplete = (from o in _context.Ordr
                              where o.OrdrId == orderID
                              select o.OrdrStusId);


            foreach (var ic in isComplete)
                if (ic == 2)
                    return 2;

            int[] billingTasks = { 1000, 1001 };
            var isCompleteBillStarted = (from o in _context.Ordr
                                         join at in _context.ActTask on o.OrdrId equals at.OrdrId
                                         where o.OrdrId == orderID && billingTasks.Contains(at.TaskId) && at.StusId == 0
                                         select o.OrdrId);


            foreach (var ic in isCompleteBillStarted)
                return 2;

            var isSent = (from vo in _context.VndrOrdr
                          join voms in _context.VndrOrdrMs on vo.VndrOrdrId equals voms.VndrOrdrId
                          where vo.OrdrId == orderID //&& vo.TRMTG_CD == IsTerm  //Spoke with Emily, have the cancel/disco early requests check if either side has been sent.
                          select voms.SentToVndrDt);


            foreach (var sent in isSent)
                if (sent != null)
                    return 1;

            return 0;

        }

        public bool LoadRTSTask(int orderId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@OrderID", SqlDbType.Int)
                };
            pc[0].Value = orderId;


            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertManualRTSTask @OrderID", pc.ToArray());

            if (toReturn == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SetJeopardy(int orderId, int noteId, string jepCode)
        {
            //104	Additional Charge Request - Need sales support approval
            OrdrJprdy jep = new OrdrJprdy
            {
                CreatDt = DateTime.Now,
                JprdyCd = jepCode,
                NteId = noteId,
                OrdrId = orderId
            };

            _context.OrdrJprdy.Add(jep);
            _context.SaveChanges();
            GenerateRTSEmail(orderId);

        }

        public void GenerateRTSEmail(int orderId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@EMAIL_REQ_TYPE_ID", SqlDbType.Int),
                    new SqlParameter("@STUS_ID", SqlDbType.Int)
                };
            pc[0].Value = orderId;
            pc[1].Value = 1;
            pc[2].Value = 10;

            _context.Database.ExecuteSqlCommand("dbo.insertEmailRequest @ORDR_ID, @EMAIL_REQ_TYPE_ID, @STUS_ID", pc.ToArray());
        }

        public bool LoadNCITask(int orderId, short taskID)
        {
            var curTask = (from at in _context.ActTask
                           where at.OrdrId == orderId && at.TaskId == taskID
                           select at);


            foreach (ActTask ct in curTask)
            {
                if (ct.StusId != 0)
                {
                    ct.StusId = 0;
                    ct.ModfdDt = DateTime.Now;
                    _context.SaveChanges();
                }

                return true;
            }

            ActTask nat = new ActTask
            {
                OrdrId = orderId,
                TaskId = taskID,
                WgProfId = 200,
                CreatDt = DateTime.Now,
                StusId = 0
            };

            _context.ActTask.Add(nat);
            _context.SaveChanges();
            return true;
        }

        public bool InsertGOMBillingTask(int orderID)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int)
                };

            pc[0].Value = orderID;

            var i = _context.Database.ExecuteSqlCommand("dbo.loadGOMInitiateBillingTask @ORDR_ID", pc.ToArray());

            if (i == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}