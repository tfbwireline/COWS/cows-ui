﻿using COWS.Data.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using COWS.Entities.Enums;
using Microsoft.Extensions.Caching.Memory;

namespace COWS.Data.Repository
{
    public class SystemConfigRepository : ISystemConfigRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public SystemConfigRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkSysCfg> Find(Expression<Func<LkSysCfg, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkSysCfg, out List<LkSysCfg> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate);
        }

        public IEnumerable<LkSysCfg> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkSysCfg, out List<LkSysCfg> list))
            {
                list = _context.LkSysCfg
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkSysCfg, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkSysCfg GetById(int id)
        {
            return _context.LkSysCfg
                .SingleOrDefault(a => a.CfgId == id);
        }

        public LkSysCfg GetByName(string sParmNme)
        {
            return _context.LkSysCfg
                .SingleOrDefault(a => a.PrmtrNme == sParmNme);
        }

        public LkSysCfg Create(LkSysCfg entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkSysCfg entity)
        {
            LkSysCfg obj = GetById(id);
            obj.PrmtrNme = entity.PrmtrNme;
            obj.PrmtrValuTxt = entity.PrmtrValuTxt;
            obj.RecStusId = entity.RecStusId;
            _context.LkSysCfg.Update(obj);

            SaveAll();
        }


        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkSysCfg);
            return _context.SaveChanges();
        }
    }
}
