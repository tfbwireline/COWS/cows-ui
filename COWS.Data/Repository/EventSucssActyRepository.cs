﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventSucssActyRepository : IEventSucssActyRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public EventSucssActyRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<EventSucssActy> Find(Expression<Func<EventSucssActy, bool>> predicate)
        {
            return _context.EventSucssActy
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<EventSucssActy> GetAll()
        {
            return _context.EventSucssActy
                            .AsNoTracking()
                            .ToList();
        }

        public EventSucssActy GetById(int id)
        {
            return _context.EventSucssActy
                .SingleOrDefault(a => a.SucssActyId == id);
        }

        public IEnumerable<EventSucssActy> GetByHistId(int id)
        {
            List<EventSucssActy> liESA = new List<EventSucssActy>();
            var esl = (from e in _context.EventSucssActy
                       where (e.EventHistId == id)
                       select e);
            return esl;
        }

        public IEnumerable<EventSucssActy> GetByEventId(int id)
        {
            List<EventSucssActy> liESA = new List<EventSucssActy>();
            var esl = (from e in _context.EventHist
                       join efa in _context.EventSucssActy on e.EventHistId equals efa.EventHistId
                       where (e.EventId == id) && (efa.RecStusId == (byte)1)
                       select efa);

            foreach (var es in esl)
            {
                EventSucssActy esa = new EventSucssActy();
                esa.EventHistId = es.EventHistId;
                esa.SucssActyId = es.SucssActyId;
                esa.RecStusId = (byte)1;
                esa.CreatDt = es.CreatDt;
                liESA.Add(esa);
            }
            return liESA;
        }

        public EventSucssActy Create(EventSucssActy entity)
        {
            throw new NotImplementedException();
        }

        public void CreateActy(EventSucssActy entity)
        {
            _context.EventSucssActy.Add(entity);
            SaveAll();
        }

        public void Update(int id, EventSucssActy entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            var st = GetByHistId(id);
            _context.EventSucssActy.RemoveRange(st);
            if (st != null)
            {
                foreach (var ac in st)
                {
                    _context.EventSucssActy.Remove(ac);
                }
            }
            SaveAll();
        }

        public void DeleteByEventId(int id)
        {
            var st = GetByEventId(id);
            foreach (var es in st)
            {
                Delete(es.EventHistId);
            }
            SaveAll();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}