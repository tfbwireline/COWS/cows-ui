﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptMdsSupportTierRepository : ICptMdsSupportTierRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptMdsSupportTierRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptMdsSuprtTier Create(LkCptMdsSuprtTier entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptMdsSuprtTier> Find(Expression<Func<LkCptMdsSuprtTier, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptMdsSuprtTier, out List<LkCptMdsSuprtTier> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptMdsSuprtTier> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptMdsSuprtTier, out List<LkCptMdsSuprtTier> list))
            {
                list = _context.LkCptMdsSuprtTier
                            .OrderBy(i => i.CptMdsSuprtTier)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptMdsSuprtTier, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptMdsSuprtTier GetById(int id)
        {
            return _context.LkCptMdsSuprtTier
                            .SingleOrDefault(i => i.CptMdsSuprtTierId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptMdsSuprtTier);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptMdsSuprtTier entity)
        {
            throw new NotImplementedException();
        }
    }
}