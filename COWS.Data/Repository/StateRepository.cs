﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class StateRepository : IStateRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public StateRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkUsStt Create(LkUsStt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkUsStt> Find(Expression<Func<LkUsStt, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkUsStt, out List<LkUsStt> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkUsStt> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUsStt, out List<LkUsStt> list))
            {
                list = _context.LkUsStt
                            .OrderBy(i => i.UsStateName)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkUsStt, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkUsStt GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkUsStt);
            return _context.SaveChanges();
        }

        public void Update(int id, LkUsStt entity)
        {
            throw new NotImplementedException();
        }
    }
}