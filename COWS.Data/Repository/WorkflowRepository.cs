﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class WorkflowRepository : IWorkflowRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;
        private IMemoryCache _cache;

        public WorkflowRepository(COWSAdminDBContext context,
            ICommonRepository common, IMemoryCache cache)
        {
            _context = context;
            _common = common;
            _cache = cache;
        }

        public IQueryable<LkWrkflwStus> Find(Expression<Func<LkWrkflwStus, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkWrkflwStus, out List<LkWrkflwStus> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkWrkflwStus> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkWrkflwStus, out List<LkWrkflwStus> list))
            {
                list = _context.LkWrkflwStus
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkWrkflwStus, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkWrkflwStus GetById(int id)
        {
            return _context.LkWrkflwStus
                .SingleOrDefault(a => a.WrkflwStusId == id);
        }

        public LkWrkflwStus Create(LkWrkflwStus entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkWrkflwStus entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkWrkflwStus);
            return _context.SaveChanges();
        }

        public bool IsRePublish(int eventId)
        {
            //List<EventHist> x = (from eh in _context.EventHist
            //                     where (eh.EventId == eventId)
            //                          && (eh.ActnId == ((byte)Actions.Publish))
            //                          && ((DateTime.Now - eh.CreatDt).TotalSeconds > 15)
            //                     select eh).ToList();

            var x = _context.EventHist.Where(i => i.EventId == eventId
                                                && ((i.ActnId == (byte)Actions.Publish)
                                                     || (i.ActnId == (byte)Actions.EventRepublished)
                                                     || (i.ActnId == (byte)Actions.EventPublishCPECombo))
                    ).Select(i => i.EventHistId).ToList();

            return x.Count > 0;
        }

        public bool UpdateEWStatus(EventWorkflow esw)
        {
            if (esw.EventTypeId == (int)EventType.AD)
            {
                AdEvent y = (from x in _context.AdEvent
                             where x.EventId == esw.EventId
                             select x).SingleOrDefault();
                if (y != null)
                {

                    y.EventStusId = esw.EventRule.EndEventStusId;
                    y.WrkflwStusId = Byte.Parse(esw.EventRule.WrkflwStusId.ToString());
                    y.ModfdByUserId = esw.UserId;
                    y.ModfdDt = DateTime.Now;
                    if (esw.Profile == "Activator")
                        y.EndTmst = DateTime.Parse(esw.EndTime.ToString());
                }
            }
            else if (esw.EventTypeId == (int)EventType.NGVN)
            {
                NgvnEvent y = (from x in _context.NgvnEvent
                               where x.EventId == esw.EventId
                               select x).SingleOrDefault();
                if (y != null)
                {
                    y.EventStusId = esw.EventRule.EndEventStusId;
                    y.WrkflwStusId = Byte.Parse(esw.EventRule.WrkflwStusId.ToString());
                    y.ModfdByUserId = esw.UserId;
                    y.ModfdDt = DateTime.Now;
                    if (esw.Profile == "Activator")
                        y.EndTmst = DateTime.Parse(esw.EndTime.ToString());
                }
            }
            else if (esw.EventTypeId == (int)EventType.MPLS)
            {
                MplsEvent y = (from x in _context.MplsEvent
                               where x.EventId == esw.EventId
                               select x).SingleOrDefault();
                if (y != null)
                {
                    y.EventStusId = esw.EventRule.EndEventStusId;
                    y.WrkflwStusId = Byte.Parse(esw.EventRule.WrkflwStusId.ToString());
                    y.ModfdByUserId = esw.UserId;
                    y.ModfdDt = DateTime.Now;
                    if (esw.Profile == "Activator")
                        y.EndTmst = DateTime.Parse(esw.EndTime.ToString());
                }
            }
            else if (esw.EventTypeId == (int)EventType.SprintLink)
            {
                SplkEvent y = (from x in _context.SplkEvent
                               where x.EventId == esw.EventId
                               select x).SingleOrDefault();
                if (y != null)
                {
                    y.EventStusId = esw.EventRule.EndEventStusId;
                    y.WrkflwStusId = Byte.Parse(esw.EventRule.WrkflwStusId.ToString());
                    y.ModfdByUserId = esw.UserId;
                    y.ModfdDt = DateTime.Now;
                    if (esw.Profile == "Activator")
                        y.EndTmst = DateTime.Parse(esw.EndTime.ToString());
                }
            }
            else if (esw.EventTypeId == (int)EventType.MDS)
            {
                var y = (from x in _context.MdsEvent
                         where x.EventId == esw.EventId
                         select x).SingleOrDefault();
                if (y != null)
                {
                    y.EventStusId = esw.EventRule.EndEventStusId;
                    y.WrkflwStusId = Byte.Parse(esw.EventRule.WrkflwStusId.ToString());
                    y.OldEventStusId = Byte.Parse(esw.EventRule.StrtEventStusId.ToString());
                    y.ModfdByUserId = esw.UserId;
                    y.ModfdDt = DateTime.Now;

                    //_context.SaveChanges();
                    
                    //If Pre-staged, republished and already shipped then skip the fulfilling and directly go to shipped status
                    if ((y.EventStusId.Equals((byte)EventStatus.Published)) && (IsRePublish(esw.EventId))
                        && (_common.IsEventEmailSent(esw.EventId, (int)EmailREQType.MDSShippedEmail) || _common.IsEventEmailSent(esw.EventId, (int)EmailREQType.CradlePointShippedEmail))
                        )
                    {
                        //List<WorkflowStatus> wf = _common.GetMDSActStatuses(((byte)EventStatus.Published), esw.EventType).FindAll(a => a.Equals((byte)WorkflowStatus.Fulfilling));
                        //if (wf != null)
                        //{
                            y.EventStusId = (byte)EventStatus.Shipped;
                            y.WrkflwStusId = (byte)WorkflowStatus.Shipped;

                            //Insert a note stating that published & fulfilling steps are auto-completed
                            EventHist eh = new EventHist();
                            eh.EventId = esw.EventId;
                            eh.ActnId = (byte)20;
                            eh.CmntTxt = "Event has already been shipped, so it is being moved past published & fulfilling steps systematically.";
                            if (esw.StartTime != DateTime.MinValue)
                                eh.EventStrtTmst = esw.StartTime;
                            if (esw.EndTime != DateTime.MinValue)
                                eh.EventEndTmst = esw.EndTime;
                            eh.CreatByUserId = esw.UserId;
                            eh.CreatDt = DateTime.Now;
                            eh.PreCfgCmpltCd = "N";
                            _context.EventHist.Add(eh);
                        //}
                    }

                    // km967761 - 12/14/2020 - As per Jagan, commented out due to MDS_EVENT.TME_SLOT_ID not used anymore
                    //// Update the FT Rsrc avail table if event is being retracted, deleted or expired.
                    //// Removed the taking of the slot when retracking per Pramod on 04/10/2012
                    //if (esw.CalendarEventTypeId == (int)CalendarEventType.MDSFT && ((Byte.Parse(esw.EventRule.WrkflwStusId.ToString()) == 9)
                    //        || (Byte.Parse(esw.EventRule.WrkflwStusId.ToString()) == 11)

                    //        //|| (Byte.Parse(_dtRule.Rows[0]["Command"].ToString()) == 12)
                    //        ))
                    //{
                    //    List<SqlParameter> pc = new List<SqlParameter>
                    //    {
                    //        new SqlParameter("@ASN_USER_ID", SqlDbType.Int),
                    //        new SqlParameter("@EVENT_ID", SqlDbType.VarChar),
                    //        new SqlParameter("@OLD_STRT_TMST", SqlDbType.DateTime),
                    //        new SqlParameter("@OLD_TME_SLOT_ID", SqlDbType.TinyInt)
                    //    };
                    //    pc[0].Value = 0;
                    //    pc[1].Value = esw.EventId;
                    //    pc[2].Value = y.StrtTmst;
                    //    pc[3].Value = y.TmeSlotId;

                    //    int toReturn = _context.Database.ExecuteSqlCommand("dbo.deleteMDSSlotAvailibility @ASN_USER_ID, @EVENT_ID, @OLD_STRT_TMST, @OLD_TME_SLOT_ID", pc.ToArray());
                    //    //_context.DeleteMDSSlotAvailibility(0, esw.EventId, y.StrtTmst, y.TmeSlotId);
                    //}


                    if (esw.Profile == "Activator")
                        y.EndTmst = DateTime.Parse(esw.EndTime.ToString());

                    _context.SaveChanges();
                }
            }
            else if (esw.EventTypeId == (int)EventType.SIPT)
            {
                SiptEvent y = (from x in _context.SiptEvent
                               where x.EventId == esw.EventId
                               select x).SingleOrDefault();
                if (y != null)
                {
                    y.EventStusId = esw.EventRule.EndEventStusId;
                    y.WrkflwStusId = Byte.Parse(esw.EventRule.WrkflwStusId.ToString());
                    y.ModfdByUserId = esw.UserId;
                    y.ModfdDt = DateTime.Now;
                    //if (bIsActivator)
                    //    y.END_TMST = endDate;
                    _context.SaveChanges();
                }
            }
            else if (esw.EventTypeId == (int)EventType.UCaaS)
            {
                var y = (from x in _context.UcaaSEvent
                         where x.EventId == esw.EventId
                         select x).SingleOrDefault();
                if (y != null)
                {
                    y.EventStusId = esw.EventRule.EndEventStusId;
                    y.WrkflwStusId = Byte.Parse(esw.EventRule.WrkflwStusId.ToString());
                    y.OldEventStusId = Byte.Parse(esw.EventRule.StrtEventStusId.ToString());
                    y.ModfdByUserId = esw.UserId;
                    y.ModfdDt = DateTime.Now;

                    if (esw.Profile == "Activator")
                        y.EndTmst = DateTime.Parse(esw.EndTime.ToString());
                }
            }

            //try
            //{
            //    if (esw.EventRule.EndEventStusId.ToString() == "8" || esw.EventRule.WrkflwStusId.ToString() == "11")
            //    {
            //        //var bp = (from fmeo in _context.FSA_MDS_EVENT_ORDR
            //        //          where fmeo.EVENT_ID == esw.EventId
            //        //          select new { ORDR_ID = fmeo.ORDR_ID }).Distinct();
            //        //List<int> lstordrID = new List<int>();
            //        //if (bp != null && bp.Count() > 0)
            //        //{
            //        //    foreach (var _bp in bp)
            //        //    {
            //        //        if (!lstordrID.Contains(_bp.ORDR_ID))
            //        //            lstordrID.Add(_bp.ORDR_ID);
            //        //    }
            //        //}

            //        //var del = from a in _context.FSA_MDS_EVENT_ORDR
            //        //          where a.EVENT_ID == esw.EventId
            //        //          select a;

            //        //if (del != null && del.Count() > 0)
            //        //{
            //        //    _context.FSA_MDS_EVENT_ORDR.DeleteAllOnSubmit(del);
            //        //    _context.SaveChanges();
            //        //}

            //        //if (lstordrID != null && lstordrID.Count() > 0)
            //        //{
            //        //    foreach (int ordrID in lstordrID)
            //        //    {
            //        //        var o = (from fmeo in _context.FSA_MDS_EVENT_ORDR
            //        //                 join act in _context.ActTask on fmeo.OrdrId equals act.OrdrId
            //        //                 where fmeo.ORDR_ID == ordrID
            //        //                     && fmeo.EVENT_ID != esw.EventId
            //        //                     && act.TASK_ID == 1000
            //        //                     && act.STUS_ID == 3
            //        //                     && fmeo.CMPLTD_CD == true
            //        //                 select new
            //        //                 {
            //        //                     EVENT_ID = fmeo.EVENT_ID
            //        //                 }
            //        //                ).Distinct();
            //        //        foreach (var _o in o)
            //        //        {
            //        //            setCommand("[dbo].[completeMDSEvent]");
            //        //            addIntParam("@EVENT_ID", _o.EVENT_ID);
            //        //            execNoQuery();
            //        //        }
            //        //    }
            //        //}
            //    }
            //}
            //catch
            //{
            //}

            Appt ap = new Appt();

            var apx = (from a in _context.Appt where a.EventId == esw.EventId select a).SingleOrDefault();
            if (apx != null)
            {
                if (esw.EventRule.WrkflwStusId.ToString().Equals(((int)WorkflowStatus.Complete).ToString()))
                {
                    apx.RecStusId = Byte.Parse(((int)ERecStatus.InActive).ToString());
                    apx.ModfdDt = DateTime.Now;
                }
            }

            _context.SaveChanges();

            //commented out because its already taken care of in web.InsertMDSEventInfoData sp which calls completemdsevent
            //send design complete message when Install OaaS Event status is updated to Complete Pending UAT,
            //if ((_dtRule.Rows[0]["Command"].ToString() == ((int)EEventWorkFlowStatus.Complete).ToString())
            //    || (_dtRule.Rows[0]["Command"].ToString() == ((int)EEventWorkFlowStatus.CompletePendingUAT).ToString())
            //    )
            //{
            //    ESWDBCntxt.CompleteMDSEvent(_iEventID);
            //}

            if (esw.EventRule != null)
            {
                List<EventAsnToUser> assignedUser = esw.AssignUser.Where(a => a.RecStusId == 1).ToList();
                //if (esw.EventRuleCount > 0)
                //{
                //Commented as per rule provided by user to Pramod on 11/30/2011
                if (esw.EventRule.WrkflwStusId.ToString().Equals(((int)WorkflowStatus.Reject).ToString())
                    || (esw.EventRule.WrkflwStusId.ToString().Equals(((int)WorkflowStatus.Reschedule).ToString())
                        && esw.EventRule.EndEventStusId.ToString().Equals(((int)EventStatus.Rework).ToString())))
                {
                    var au = (from e in _context.EventAsnToUser
                              where (e.RecStusId == ((int)ERecStatus.Active))
                                 && (e.EventId == esw.EventId)
                              select e);
                    foreach (var a in au)
                    {
                        a.RecStusId = ((int)ERecStatus.InActive);
                        a.ModfdDt = DateTime.Now;
                    }

                    // Added by Sarah Sandoval [20220329] - Clear Conf Bridge details if no assigned activators for Yes MNS Option
                    var mds = _context.MdsEvent.Find(esw.EventId);
                    if (mds != null && mds.CnfrcBrdgId == 3)
                    {
                        mds.CnfrcBrdgNbr = string.Empty;
                        mds.CnfrcPinNbr = string.Empty;
                        mds.OnlineMeetingAdr = string.Empty;
                    }
                    
                    _context.SaveChanges();
                }
                else if (esw.CalendarEventTypeId != (int)CalendarEventType.MDSFT && ((assignedUser == null) || (assignedUser.Count == 0)))
                {
                    var au = (from e in _context.EventAsnToUser
                              where (e.RecStusId == ((int)ERecStatus.Active))
                                 && (e.EventId == esw.EventId)
                              select e);
                    foreach (var a in au)
                    {
                        a.RecStusId = ((int)ERecStatus.InActive);
                        a.ModfdDt = DateTime.Now;
                    }
                    _context.SaveChanges();
                }
                else if (esw.CalendarEventTypeId == (int)CalendarEventType.MDSFT)//disable all users except 5001
                {
                    var au = (from e in _context.EventAsnToUser
                              where (e.RecStusId == ((int)ERecStatus.Active))
                                 && (e.AsnToUserId != 5001)
                                 && (e.EventId == esw.EventId)
                              select e);
                    foreach (var a in au)
                    {
                        a.RecStusId = ((int)ERecStatus.InActive);
                        a.ModfdDt = DateTime.Now;
                    }
                    _context.SaveChanges();
                }
                else if (esw.CalendarEventTypeId != (int)CalendarEventType.MDSFT)
                {
                    List<EventAsnToUser> eaul = new List<EventAsnToUser>();

                    //var au = (from e in _context.EventAsnToUser
                    //          where (e.RecStusId == ((int)ERecStatus.Active))
                    //             && (e.EventId == esw.EventId)
                    //          select e);
                    //foreach (var a in au)
                    //{
                    //    a.RecStusId = ((int)ERecStatus.InActive);
                    //    a.ModfdDt = DateTime.Now;
                    //}

                    if ((assignedUser != null) && (assignedUser.Count > 0))
                    {
                        for (int i = 0; i < assignedUser.Count; i++)
                        {
                            var x = (from eau in _context.EventAsnToUser
                                     where ((eau.EventId == esw.EventId) && (eau.AsnToUserId == Int32.Parse(assignedUser[i].AsnToUserId.ToString()))
                                           && (eau.RoleId == ((byte)assignedUser[i].RoleId)))
                                     select eau).SingleOrDefault();

                            if (x == null)
                            {
                                EventAsnToUser eau = new EventAsnToUser();
                                eau.EventId = esw.EventId;
                                eau.AsnToUserId = assignedUser[i].AsnToUserId;
                                eau.RoleId = ((byte)assignedUser[i].RoleId);
                                eau.CreatDt = DateTime.Now;
                                eau.RecStusId = (int)(ERecStatus.Active);

                                eaul.Add(eau);
                                _context.EventAsnToUser.AddRange(eaul);
                            }
                            //else
                            //{
                            //    x.RecStusId = ((int)ERecStatus.Active);
                            //    x.ModfdDt = DateTime.Now;
                            //}
                        }
                    }
                    _context.SaveChanges();
                }
                //}
            }

            return true;
        }
    }
}