﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class DedicatedCustomerRepository : IDedicatedCustomerRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public DedicatedCustomerRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkDedctdCust Create(LkDedctdCust entity)
        {
            int maxId = _context.LkDedctdCust.Max(i => i.CustId);
            entity.CustId = ++maxId;
            _context.LkDedctdCust.Add(entity);

            SaveAll();

            return GetById(entity.CustId);
        }

        public void Delete(int id)
        {
            LkDedctdCust cust = GetById(id);
            _context.LkDedctdCust.Remove(cust);

            SaveAll();
        }

        public IQueryable<LkDedctdCust> Find(Expression<Func<LkDedctdCust, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkDedctdCust, out List<LkDedctdCust> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkDedctdCust> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkDedctdCust, out List<LkDedctdCust> list))
            {
                list = _context.LkDedctdCust
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderBy(i => i.CustNme)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkDedctdCust, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkDedctdCust GetById(int id)
        {
            return _context.LkDedctdCust
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.CustId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkDedctdCust);
            return _context.SaveChanges();
        }

        public void Update(int id, LkDedctdCust entity)
        {
            LkDedctdCust customer = GetById(id);
            customer.CustNme = entity.CustNme;
            customer.RecStusId = entity.RecStusId;
            customer.ModfdByUserId = entity.ModfdByUserId;
            customer.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}