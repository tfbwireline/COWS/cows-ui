﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSEventSlnkWiredTrptRepository : IMDSEventSlnkWiredTrptRepository
    {
        private readonly COWSAdminDBContext _context;

        public MDSEventSlnkWiredTrptRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<MdsEventSlnkWiredTrpt> Find(Expression<Func<MdsEventSlnkWiredTrpt, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MdsEventSlnkWiredTrpt> GetAll()
        {
            throw new NotImplementedException();
        }

        public MdsEventSlnkWiredTrpt GetById(int id)
        {
            throw new NotImplementedException();
        }

        public MdsEventSlnkWiredTrpt Create(MdsEventSlnkWiredTrpt entity)
        {
            throw new NotImplementedException();
        }

        public void Create(IEnumerable<MdsEventSlnkWiredTrpt> entity)
        {
            _context.MdsEventSlnkWiredTrpt
                .AddRange(entity.Select(a =>
                {
                    a.SlnkWiredTrptId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Update(int id, MdsEventSlnkWiredTrpt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            throw new NotImplementedException();
        }

        public void SetInactive(Expression<Func<MdsEventSlnkWiredTrpt, bool>> predicate)
        {
            _context.MdsEventSlnkWiredTrpt
                .Where(predicate)
                .ToList()
                .ForEach(a => {
                    a.RecStusId = (byte)0;
                    a.ModfdDt = DateTime.Now;
                });
        }

        public IEnumerable<MdsEventSlnkWiredTrpt> GetMDSEventSlnkWiredTrptByEventId(int eventId)
        {
            return _context.MdsEventSlnkWiredTrpt.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1))
                .ToList();
        }

        public void AddWiredTrptTbl(ref List<MdsEventSlnkWiredTrpt> data, int eventId)
        {
            //DeleteAllWiredTrptItems(Obj.EventID);
            InactiveAllWiredTrptItems(eventId);

            foreach (MdsEventSlnkWiredTrpt item in data)
            {
                MdsEventSlnkWiredTrpt mwt = new MdsEventSlnkWiredTrpt();

                mwt.EventId = eventId;
                mwt.H6 = item.H6;
                mwt.MdsTrnsprtType = item.MdsTrnsprtType;
                mwt.PrimBkupCd = item.PrimBkupCd;
                mwt.VndrPrvdrTrptCktId = item.VndrPrvdrTrptCktId;
                mwt.VndrPrvdrNme = item.VndrPrvdrNme;
                //mwt.TELCO_ID = byte.Parse(item.TelcoID);
                mwt.SprintMngdCd = item.SprintMngdCd;
                mwt.BdwdChnlNme = item.BdwdChnlNme;
                mwt.IpNuaAdr = item.IpNuaAdr;
                mwt.PlNbr = item.PlNbr;
                mwt.OldCktId = item.OldCktId;
                mwt.MultiLinkCktCd = item.MultiLinkCktCd;
                mwt.DedAccsCd = item.DedAccsCd;
                mwt.OdieDevNme = item.OdieDevNme;
                mwt.DlciVpiVci = item.DlciVpiVci;
                mwt.FmsNbr = item.FmsNbr;
                mwt.SpaNua = item.SpaNua;
                mwt.VlanNbr = item.VlanNbr;
                mwt.CreatDt = DateTime.Now;
                mwt.RecStusId = (byte)1;
                //mwt.OptInCktCd = (Obj.Schedule.WorkFlowStatus.Equals(EEventWorkFlowStatus.Complete) && (!(String.IsNullOrEmpty(item.OptInCktCd) && String.IsNullOrEmpty(item.ReadyBeginFlag) && String.IsNullOrEmpty(item.OptInHCd)))) ? "N" : ((!String.IsNullOrEmpty(item.OptInCktCd)) ? item.OptInCktCd : null);
                //mwt.ReadyBeginCd = (Obj.Schedule.WorkFlowStatus.Equals(EEventWorkFlowStatus.Complete) && (!(String.IsNullOrEmpty(item.OptInCktCd) && String.IsNullOrEmpty(item.ReadyBeginFlag) && String.IsNullOrEmpty(item.OptInHCd)))) ? "N" : ((!String.IsNullOrEmpty(item.ReadyBeginFlag)) ? item.ReadyBeginFlag : null);
                mwt.OptInHCd = (!String.IsNullOrEmpty(item.OptInHCd)) ? item.OptInHCd : null;

                _context.MdsEventSlnkWiredTrpt.Add(mwt);

                //Call the updateNRM procedure for each record
                //if ((Obj.Schedule.WorkFlowStatus.Equals(EEventWorkFlowStatus.Complete))
                //    && (!(String.IsNullOrEmpty(mwt.OptInCktCd) && String.IsNullOrEmpty(mwt.ReadyBeginCd) && String.IsNullOrEmpty(mwt.OptInHCd))))
                //    UpdatePMFlagToNRM(Obj.EventID, Obj.H6, item.IPNUA, false);
            }

            _context.SaveChanges();
        }

        private void InactiveAllWiredTrptItems(int eventId)
        {
            _context.MdsEventSlnkWiredTrpt.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1)).ToList().ForEach(b => { b.RecStusId = ((byte)0); b.ModfdDt = DateTime.Now; });
            _context.SaveChanges();
        }

        public void UpdatePMFlagToNRM(int eventId, string H6, string NUA, bool bDisc)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@H6", SqlDbType.VarChar),
                new SqlParameter("@NUA", SqlDbType.VarChar),
                new SqlParameter("@IsDisc", SqlDbType.Bit),
                new SqlParameter("@EventID", SqlDbType.Int)
            };

            pc[0].Value = H6;
            pc[1].Value = NUA;
            pc[2].Value = bDisc;
            pc[3].Value = eventId;

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.UpdatePMFlagAtNRM @H6, @NUA, @IsDisc, @EventID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H6", SqlDbType = SqlDbType.Int, Value = H6 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@NUA", SqlDbType = SqlDbType.VarChar, Value = NUA });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@IsDisc", SqlDbType = SqlDbType.Bit, Value = bDisc });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EventID", SqlDbType = SqlDbType.Int, Value = eventId });

                _context.Database.OpenConnection();
                command.ExecuteNonQuery();
            }

            //_context.Database.
            //var View = _context.Query<>
            //    .AsNoTracking()
            //    .FromSql("[web].[UpdatePMFlagAtNRM] @p0, @p1, @p2, @p3", pc.ToArray());
        }
    }
}