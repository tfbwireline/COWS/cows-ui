﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VendorRepository : IVendorRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public VendorRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkVndr Create(LkVndr entity)
        {
            // Created another method to cater country code
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            // Vendor Management does not have delete/remove functionality
            throw new NotImplementedException();
        }

        public IQueryable<LkVndr> Find(Expression<Func<LkVndr, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkVndr, out List<LkVndr> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkVndr> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkVndr, out List<LkVndr> list))
            {
                list = (from clu in

                            (from v in _context.LkVndr
                             join fc in _context.LkFrgnCxr on v.VndrCd equals fc.CxrCd into joinfc
                             from fc in joinfc.DefaultIfEmpty()

                             join cc in _context.LkCtryCxr on v.VndrCd equals cc.CxrCd into joincc
                             from cc in joincc.DefaultIfEmpty()

                             join lc in _context.LkCtry on cc.CtryCd equals lc.CtryCd into joinlc
                             from lc in joinlc.DefaultIfEmpty()

                             select new { v, fc, cc, lc })

                        select new LkVndr
                        {
                            VndrCd = clu.v.VndrCd,
                            VndrNme = clu.v.VndrNme,
                            ModfdByUser = clu.v.ModfdByUser,
                            CreatByUser = clu.v.CreatByUser,
                            RecStusId = clu.v.RecStusId,
                            UserWfm = new List<UserWfm>() {
                                new UserWfm() {
                                    OrgtngCtryCdNavigation = new LkCtry() {
                                        CtryCd = clu.lc.CtryCd,
                                        CtryNme = clu.lc.CtryNme
                                    }
                                }
                            }
                        }).ToList();

                _cache.Set(CacheKeys.LkVndr, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkVndr GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkVndr);
            return _context.SaveChanges();
        }

        public void Update(int id, LkVndr entity)
        {
            // Created another method to cater country code
            throw new NotImplementedException();
        }

        public LkVndr CreateVendor(LkVndr entity, string ctryCd)
        {
            DeleteVendor(entity.VndrCd);

            _context.LkVndr.Add(entity);

            if (entity.VndrCd.Length < 6)
            {
                _context.LkFrgnCxr.Add(new LkFrgnCxr()
                {
                    CxrCd = entity.VndrCd,
                    CxrNme = entity.VndrNme,
                    RecStusId = entity.RecStusId,
                    CreatByUserId = entity.CreatByUserId,
                    CreatDt = entity.CreatDt,
                    ModfdByUserId = null,
                    ModfdDt = null
                });

                _context.LkCtryCxr.Add(new LkCtryCxr()
                {
                    CxrCd = entity.VndrCd,
                    CtryCd = ctryCd,
                    CreatDt = entity.CreatDt
                });
            }

            SaveAll();

            return Find(i => i.VndrCd == entity.VndrCd).SingleOrDefault();
        }

        public void UpdateVendor(LkVndr entity, string ctryCd)
        {
            LkVndr vndr = _context.LkVndr.SingleOrDefault(i => i.VndrCd == entity.VndrCd);
            if (vndr != null)
            {
                vndr.VndrCd = entity.VndrCd;
                vndr.VndrNme = entity.VndrNme;
                vndr.RecStusId = entity.RecStusId;
                vndr.ModfdByUserId = entity.ModfdByUserId;
                vndr.ModfdDt = entity.ModfdDt;

                if (entity.VndrCd.Length < 6)
                {
                    LkFrgnCxr fcxr = _context.LkFrgnCxr.SingleOrDefault(i => i.CxrCd == entity.VndrCd);
                    fcxr.CxrCd = entity.VndrCd;
                    fcxr.CxrNme = entity.VndrNme;
                    fcxr.RecStusId = entity.RecStusId;
                    fcxr.ModfdByUserId = entity.ModfdByUserId;
                    fcxr.ModfdDt = entity.ModfdDt;

                    LkCtryCxr cxr = _context.LkCtryCxr.SingleOrDefault(i => i.CxrCd == entity.VndrCd);
                    ctryCd = string.IsNullOrWhiteSpace(ctryCd) ? cxr.CtryCd : ctryCd;
                    _context.LkCtryCxr.Remove(cxr);

                    _context.LkCtryCxr.Add(new LkCtryCxr()
                    {
                        CxrCd = entity.VndrCd,
                        CtryCd = ctryCd,
                        CreatDt = DateTime.Now
                    });
                }

                SaveAll();
            }
        }

        public void DeleteVendor(string vendorCode)
        {
            LkFrgnCxr fcxr = _context.LkFrgnCxr.SingleOrDefault(i => i.CxrCd == vendorCode);
            if(fcxr != null)
            {
                _context.LkFrgnCxr.Remove(fcxr);
            }
            LkCtryCxr cxr = _context.LkCtryCxr.SingleOrDefault(i => i.CxrCd == vendorCode);
            if (cxr != null)
            {
                _context.LkCtryCxr.Remove(cxr);
            }
            LkVndr vendor = Find(i => i.VndrCd == vendorCode).SingleOrDefault();
            if (vendor != null)
            {
                _context.LkVndr.Remove(vendor);
            }

            SaveAll();
        }
    }
}