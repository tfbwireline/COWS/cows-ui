﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CustContractLengthRepository : ICustContractLengthRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;
        private IMemoryCache _cache;

        public CustContractLengthRepository(COWSAdminDBContext context, IConfiguration configuration, IMemoryCache cache)
        {
            _context = context;
            _configuration = configuration;
            _cache = cache;
        }


        public LkCustCntrcLgth GetById(int id)
        {
            throw new NotImplementedException();
        }


        public IQueryable<LkCustCntrcLgth> Find(Expression<Func<LkCustCntrcLgth, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCustCntrcLgth, out List<LkCustCntrcLgth> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCustCntrcLgth> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCustCntrcLgth, out List<LkCustCntrcLgth> list))
            {
                list = _context.LkCustCntrcLgth
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCustCntrcLgth, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCustCntrcLgth Create(LkCustCntrcLgth entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkCustCntrcLgth entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCustCntrcLgth);
            return _context.SaveChanges();
        }

    }
}
