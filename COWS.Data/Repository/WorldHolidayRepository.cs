﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class WorldHolidayRepository : IWorldHolidayRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public WorldHolidayRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkWrldHldy Create(LkWrldHldy entity)
        {
            int maxId = _context.LkWrldHldy.Max(i => i.WrldHldyId);
            entity.WrldHldyId = (short)++maxId;
            _context.LkWrldHldy.Add(entity);

            SaveAll();

            return GetById(entity.WrldHldyId);
        }

        public void Delete(int id)
        {
            LkWrldHldy hday = GetById(id);
            _context.LkWrldHldy.Remove(hday);

            SaveAll();
        }

        public IQueryable<LkWrldHldy> Find(Expression<Func<LkWrldHldy, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkWrldHldy, out List<LkWrldHldy> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                           .AsNoTracking();

        }

        public IEnumerable<LkWrldHldy> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkWrldHldy, out List<LkWrldHldy> list))
            {
                list = _context.LkWrldHldy
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.CtryCdNavigation)
                            .OrderBy(i => i.WrldHldyDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkWrldHldy, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkWrldHldy GetById(int id)
        {
            return _context.LkWrldHldy
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.CtryCdNavigation)
                            .SingleOrDefault(i => i.WrldHldyId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkWrldHldy);
            return _context.SaveChanges();
        }

        public void Update(int id, LkWrldHldy entity)
        {
            LkWrldHldy hday = GetById(id);
            hday.WrldHldyDes = entity.WrldHldyDes;
            hday.WrldHldyDt = entity.WrldHldyDt;
            hday.CtryCd = entity.CtryCd;
            hday.CtyNme = entity.CtyNme;
            hday.RecStusId = entity.RecStusId;
            hday.ModfdByUserId = entity.ModfdByUserId;
            hday.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}