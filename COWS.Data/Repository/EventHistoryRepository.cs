﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventHistoryRepository : IEventHistoryRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IEventRuleRepository _evntRuleRepo;
        private readonly IMdsEventRepository _mdsEventRepo;
        private readonly ICommonRepository _common;
        private readonly IMDSFastTrackTypeRepository _MDSFastTrackType;
        private readonly IMDSMACActivityRepository _MDSMACActivityRepository;

        public EventHistoryRepository(COWSAdminDBContext context, IEventRuleRepository evntRuleRepo, 
            IMdsEventRepository mdsEventRepo, IMDSFastTrackTypeRepository MDSFastTrackType, IMDSMACActivityRepository MDSMACActivityRepository,
            ICommonRepository common)
        {
            _context = context;
            _evntRuleRepo = evntRuleRepo;
            _mdsEventRepo = mdsEventRepo;
            _MDSFastTrackType = MDSFastTrackType;
            _MDSMACActivityRepository = MDSMACActivityRepository;
            _common = common;
        }

        public IQueryable<EventHist> Find(Expression<Func<EventHist, bool>> predicate)
        {
            return _context.EventHist
                            .Include(a => a.Actn)
                            .Include(a => a.CreatByUser)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<EventHist> GetAll()
        {
            return _context.EventHist
                            .AsNoTracking()
                            .ToList();
        }

        public EventHist GetById(int id)
        {
            return _context.EventHist
                .SingleOrDefault(a => a.EventHistId == id);
        }

        public EventHist Create(EventHist entity)
        {
            _context.EventHist.Add(entity);
            SaveAll();

            return GetById(entity.EventHistId);
        }

        public void Update(int id, EventHist entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public int CreateEventHistory(EventHist obj, int[] toExcludeAction = null, EventType? eventType = null, string dispatchEmail = null)
        {
            var query = _evntRuleRepo
                .Find(a => a.ActnId == obj.ActnId &&
                    EF.Functions.Like(a.Actn.ActnDes, "Action %"))
                .Select(a => a.Actn)
                .Distinct();

            int[] toIncludeTime = { 2, 4, 6, 7, 8, 13, 14, 18 };//{ 1, 5, 15, 16, 17, 19 }; // Include EventStrtTmst and EventEndTmst {1, 3, 5, 9, 10, 11, 15, 16, 17, 19, 20, 21, 22, 23, 24, 25, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 62, 63, 64}            
            int[] toIncludeComments = { 2, 4, 6, 7, 8, 13, 14, 18 };//{ 1, 3, 5, 9, 10, 11, 15, 16, 17, 19, 20, 23, 29, 44 }; // Include Comments

            int[] byPassIncludeTime = { };
            int[] byPassIncludeComments = { };

            if (eventType == EventType.SIPT)
            {
                // Added by Sarah Sandoval [20210709] - To add comments and event time on history for republished (ActionID = 16)
                byPassIncludeTime = new int[] { 16, 20, 29 };
                byPassIncludeComments = new int[] { 16, 20, 29 };
            }

            EventHist hstry = new EventHist();

            if (query.Count() > 0)
            {
                IEnumerable<string> ids = query
                    .Select(a => a.ActnDes.Replace("Action ", ""))
                    .SingleOrDefault()
                    .Split(",");

                if (toExcludeAction != null)
                {
                    ids = ids.Except(toExcludeAction.Select(a => a.ToString()).ToList());
                }

                foreach (string id in ids)
                {
                    EventHist entity = new EventHist
                    {
                        EventId = obj.EventId,
                        ActnId = Convert.ToByte(id),
                        EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? null : obj.EventStrtTmst),
                        EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? null : obj.EventEndTmst),
                        CmntTxt = (toIncludeComments.Contains(Convert.ToInt32(id)) ? null : obj.CmntTxt),
                        PreCfgCmpltCd = string.IsNullOrWhiteSpace(obj.PreCfgCmpltCd) ? "N" : obj.PreCfgCmpltCd,
                        CreatByUserId = obj.CreatByUserId,
                        CreatDt = DateTime.Now,
                        ModfdDt = DateTime.Now
                    };

                    hstry = Create(entity);
                }
            }
            else
            {
                DateTime? start = DateTime.Now;
                DateTime? end = DateTime.Now;
                string comment = "";

                if (obj.FailReasId == null)
                {
                    start = (toIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? null : obj.EventStrtTmst);
                    end = (toIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? null : obj.EventEndTmst);
                    comment = (toIncludeComments.Contains(Convert.ToInt32(obj.ActnId)) ? null : obj.CmntTxt);
                }
                else
                {
                    start = obj.EventStrtTmst;
                    end = obj.EventEndTmst;
                    comment = obj.CmntTxt;
                }

                // By pass rule if there is and EventType, this block of code is added to avoid possible defect to other events.
                if (eventType != null)
                {
                    start = (byPassIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? obj.EventStrtTmst : null);
                    end = (byPassIncludeTime.Contains(Convert.ToInt32(obj.ActnId)) ? obj.EventEndTmst : null);
                    comment = (byPassIncludeComments.Contains(Convert.ToInt32(obj.ActnId)) ? obj.CmntTxt : null);
                }

                obj.EventStrtTmst = start;
                obj.EventEndTmst = end;
                obj.CmntTxt = comment;

                obj.PreCfgCmpltCd = string.IsNullOrWhiteSpace(obj.PreCfgCmpltCd) ? "N" : obj.PreCfgCmpltCd;
                obj.CreatDt = DateTime.Now;
                obj.ModfdDt = DateTime.Now;

                hstry = Create(obj);

                if (obj.ActnId == 16 && !string.IsNullOrEmpty(dispatchEmail) && toExcludeAction!= null && !toExcludeAction.Contains(8))
                {      
                    int id = 8;
                    EventHist entity = new EventHist
                    {
                        EventId = obj.EventId,
                        ActnId = Convert.ToByte(id),
                        EventStrtTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? null : obj.EventStrtTmst),
                        EventEndTmst = (toIncludeTime.Contains(Convert.ToInt32(id)) ? null : obj.EventEndTmst),
                        CmntTxt = (toIncludeComments.Contains(Convert.ToInt32(id)) ? null : obj.CmntTxt),
                        PreCfgCmpltCd = string.IsNullOrWhiteSpace(obj.PreCfgCmpltCd) ? "N" : obj.PreCfgCmpltCd,
                        CreatByUserId = obj.CreatByUserId,
                        CreatDt = DateTime.Now,
                        ModfdDt = DateTime.Now
                    };

                    hstry = Create(entity);
                }
            }

            return hstry.EventHistId;
        }

        public bool InsertEventFormChanges(MdsEvent Obj, MdsEvent old)
        {
            if (Obj.CreatByUserId == 0 && Obj.ModfdByUserId != null)
            {
                Obj.CreatByUserId = (int)Obj.ModfdByUserId;
                Obj.CreatDt = DateTime.Now;
            }

            int userId = Obj.CreatByUserId;
            //MdsEvent oldme = _mdsEventRepo.GetById(Obj.EventId);
            MdsEvent oldme = old;
            List<MdsEventFormChng> lmfc = new List<MdsEventFormChng>();
            var mdsFastTrackTypeId = Obj.MdsFastTrkTypeId != null ? Obj.MdsFastTrkTypeId.Trim() : string.Empty;
            var mdsFastTrackTypeDes = mdsFastTrackTypeId != "" ? _MDSFastTrackType.GetById(Obj.MdsFastTrkTypeId).MdsFastTrkTypeDes : string.Empty;
            var mdsoldFastTrackTypeId = oldme.MdsFastTrkTypeId != null ? oldme.MdsFastTrkTypeId.Trim() : string.Empty;
            var mdsoldFastTrackTypeDes = mdsoldFastTrackTypeId != "" ? _MDSFastTrackType.GetById(oldme.MdsFastTrkTypeId).MdsFastTrkTypeDes : string.Empty;
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            if (Obj.MdsActyTypeId == oldme.MdsActyTypeId)
            {
                if (!string.IsNullOrWhiteSpace(mdsFastTrackTypeDes)
                && string.IsNullOrWhiteSpace(mdsoldFastTrackTypeDes))
                    //  lmfc.Add(CreateMdsEventFormChange(Obj.EventId, "Event type changed to FT", userId));
                    builder.Append("Event type changed to FT ;");
                else if (mdsFastTrackTypeDes.Trim().Length <= 0 && mdsoldFastTrackTypeDes.Trim().Length > 0)
                    //  lmfc.Add(CreateMdsEventFormChange(Obj.EventId, "Event type changed to MDS", userId));
                    builder.Append("Event type changed to MDS ;");

                // Added By Sarah Sandoval [06242022] - As per request by Diane (INC70705830)
                if ((Obj.ShrtDes ?? "") != (oldme.ShrtDes ?? ""))
                    builder.Append("Short Description updated from " + oldme.ShrtDes + " to " + Obj.ShrtDes + " ; ");
                if ((Obj.IpmDes ?? "") != (oldme.IpmDes ?? ""))
                    builder.Append("IPM Description updated from " + oldme.IpmDes + " to " + Obj.IpmDes + " ; ");

                if ((Obj.H1 ?? "") != (oldme.H1 ?? ""))
                   // if (Obj.H1 != oldme.H1)
                    // lmfc.Add(CreateMdsEventFormChange(Obj.EventId, "H1 updated from " + oldme.H1 + " to " + Obj.H1, userId));
                    builder.Append("H1 updated from " + oldme.H1 + " to " + Obj.H1 + " ; ");
                if ((Obj.CustNme ?? "") != (oldme.CustNme ?? ""))
                   // lmfc.Add(CreateMdsEventFormChange(Obj.EventId, "Customer Name updated from " + oldme.CustNme + " to " + Obj.CustNme, userId));
                    builder.Append("Customer Name updated from " + oldme.CustNme + " to " + Obj.CustNme + " ; ");
                if ((Obj.CustAcctTeamPdlNme ?? "") != (oldme.CustAcctTeamPdlNme ?? ""))
                    //  lmfc.Add(CreateMdsEventFormChange(Obj.EventId, "Customer Account PDL updated from " + oldme.CustAcctTeamPdlNme + " to " + Obj.CustAcctTeamPdlNme, userId));
                    builder.Append("Customer Account PDL updated from " + oldme.CustAcctTeamPdlNme + " to " + Obj.CustAcctTeamPdlNme + " ; ");
                if ((Obj.CustSowLocTxt ?? "") != (oldme.CustSowLocTxt?? ""))
                    // lmfc.Add(CreateMdsEventFormChange(Obj.EventId, "Customer SOW updated from " + oldme.CustSowLocTxt + " to " + Obj.CustSowLocTxt, userId));
                    builder.Append("Customer SOW updated from " + oldme.CustSowLocTxt + " to " + Obj.CustSowLocTxt + " ; ");
                //if (Obj.ShrtDes != oldme.ShrtDes)
                //    // lmfc.Add(CreateMdsEventFormChange(Obj.EventId, "Short Description updated from " + oldme.ShrtDes + " to " + Obj.ShrtDes, userId));
                //    builder.Append("Short Description updated from " + oldme.ShrtDes + " to " + Obj.ShrtDes + " ; ");
                if (Obj.CrdlepntCd != oldme.CrdlepntCd)
                    //  lmfc.Add(CreateMdsEventFormChange(Obj.EventId, "Cradlepoint - Transport Only updated from " + oldme.CrdlepntCd + " to " + Obj.CrdlepntCd, userId));
                    builder.Append("Cradlepoint - Transport Only updated from " + oldme.CrdlepntCd + " to " + Obj.CrdlepntCd + " ; ");
                // Change is when table added or removed MDSMACActivity
                if (Obj.Event.MdsEventMacActy.Count > 0 && oldme.Event.MdsEventMacActy.Count > 0
                     && Obj.Event.MdsEventMacActy.Count != oldme.Event.MdsEventMacActy.Count)
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS MAC Activities updated from " + ((oldme.Event.MdsEventMacActy.Count > 0) ? String.Join(";", oldme.Event.MdsEventMacActy.Select(a => a.MdsMacActy.MdsMacActyNme).ToList()) : string.Empty) + " to " + ((Obj.Event.MdsEventMacActy.Count > 0) ? String.Join(";", Obj.Event.MdsEventMacActy.Select(b => b.MdsMacActy.MdsMacActyNme).ToList()) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("MDS MAC Activities updated from " + ((oldme.Event.MdsEventMacActy.Count > 0) ? String.Join(";", oldme.Event.MdsEventMacActy.Select(a => a.MdsMacActy.MdsMacActyNme).ToList()) : string.Empty) + " to " + ((Obj.Event.MdsEventMacActy.Count > 0) ? String.Join(";", Obj.Event.MdsEventMacActy.Select(b => b.MdsMacActy.MdsMacActyNme).ToList()) : string.Empty) + " ; ");
                else
                {
                    // Change is when same MDSMACActivity count but different activity type selected
                    var macacty = Obj.Event.MdsEventMacActy.Where(n => !oldme.Event.MdsEventMacActy.Any(o => o.MdsMacActyId == n.MdsMacActyId)).ToList();
                    if (macacty.Count > 0)
                        //   lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS MAC Activities updated from " + ((oldme.Event.MdsEventMacActy.Count > 0) ? String.Join(";", oldme.Event.MdsEventMacActy.Select(a => a.MdsMacActy.MdsMacActyNme).ToList()) : string.Empty) + " to " + ((Obj.Event.MdsEventMacActy.Count > 0) ? String.Join(";", Obj.Event.MdsEventMacActy.Select(b => b.MdsMacActy.MdsMacActyNme).ToList()) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                        builder.Append("MDS MAC Activities updated from " + ((oldme.Event.MdsEventMacActy.Count > 0) ? String.Join(";", oldme.Event.MdsEventMacActy.Select(a => a.MdsMacActy.MdsMacActyNme).ToList()) : string.Empty) + " to " + ((Obj.Event.MdsEventMacActy.Count > 0) ? String.Join(";", Obj.Event.MdsEventMacActy.Select(b => b.MdsMacActy.MdsMacActyNme).ToList()) : string.Empty) + " ; ");
                 }
            }

            // Change is when table added or removed MDSMACActivity
            if (Obj.Event.MdsEventNtwkActy.Count > 0 && oldme.Event.MdsEventNtwkActy.Count > 0
                && Obj.Event.MdsEventNtwkActy.Count != oldme.Event.MdsEventNtwkActy.Count)
                //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Network Activity types updated from " + ((oldme.Event.MdsEventNtwkActy.Count > 0) ? String.Join(";", oldme.Event.MdsEventNtwkActy.Select(a => a.NtwkActyType).ToList()) : string.Empty) + " to " + ((Obj.Event.MdsEventNtwkActy.Count > 0) ? String.Join(";", Obj.Event.MdsEventNtwkActy.Select(b => b.NtwkActyType).ToList()) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Network Activity types updated from " + ((oldme.Event.MdsEventNtwkActy.Count > 0) ? String.Join(";", oldme.Event.MdsEventNtwkActy.Select(a => a.NtwkActyType).ToList()) : string.Empty) + " to " + ((Obj.Event.MdsEventNtwkActy.Count > 0) ? String.Join(";", Obj.Event.MdsEventNtwkActy.Select(b => b.NtwkActyType).ToList()) : string.Empty) + " ; ");
            else
            {
                var ntwkacty = Obj.Event.MdsEventNtwkActy.Where(n => !oldme.Event.MdsEventNtwkActy.Any(o => o.NtwkActyTypeId == n.NtwkActyTypeId)).ToList();
                if (ntwkacty.Count > 0)
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Network Activity types updated from " + ((oldme.Event.MdsEventNtwkActy.Count > 0) ? String.Join(";", oldme.Event.MdsEventNtwkActy.Select(a => a.NtwkActyType).ToList()) : string.Empty) + " to " + ((Obj.Event.MdsEventNtwkActy.Count > 0) ? String.Join(";", Obj.Event.MdsEventNtwkActy.Select(b => b.NtwkActyType).ToList()) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Network Activity types updated from " + ((oldme.Event.MdsEventNtwkActy.Count > 0) ? String.Join(";", oldme.Event.MdsEventNtwkActy.Select(a => a.NtwkActyType).ToList()) : string.Empty) + " to " + ((Obj.Event.MdsEventNtwkActy.Count > 0) ? String.Join(";", Obj.Event.MdsEventNtwkActy.Select(b => b.NtwkActyType).ToList()) : string.Empty) + " ; ");
            }

            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.MdsActyTypeId.Equals("3")) && (oldme.MdsActyTypeId.Equals("3")) && (Obj.FullCustDiscCd != oldme.FullCustDiscCd))
                // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Total Customer Disconnect Flag updated from " + oldme.FullCustDiscCd + " to " + Obj.FullCustDiscCd, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Total Customer Disconnect Flag updated from " + oldme.FullCustDiscCd + " to " + Obj.FullCustDiscCd + " ; ");
            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.MdsActyTypeId.Equals("3")) && (oldme.MdsActyTypeId.Equals("3")) && (Obj.FullCustDiscReasTxt != oldme.FullCustDiscReasTxt))
                //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Total Customer Disconnect Reason updated from " + oldme.FullCustDiscReasTxt + " to " + Obj.FullCustDiscReasTxt, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Total Customer Disconnect Reason updated from " + oldme.FullCustDiscReasTxt + " to " + Obj.FullCustDiscReasTxt + " ; ");
            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.MdsActyTypeId.Equals("3")) && (oldme.MdsActyTypeId.Equals("3")) && (Obj.DiscMgmtCd != oldme.DiscMgmtCd))
                // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Disconnect Management Flag updated from " + oldme.DiscMgmtCd + " to " + Obj.DiscMgmtCd, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Disconnect Management Flag updated from " + oldme.DiscMgmtCd + " to " + Obj.DiscMgmtCd + " ; ");


            if (!Obj.MdsActyTypeId.Equals("3"))
            {
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.MdsEventOdieDev.Count != oldme.Event.MdsEventOdieDev.Count))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MAC Activity Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Managed Activity Table has been modified ;");

                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.EventDevCmplt.Count != oldme.Event.EventDevCmplt.Count))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Device Completion Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Device Completion Table has been modified ;");
                var oldCPEDevCount = (oldme.Event.EventCpeDev.Where(a => a.RecStusId == 1) != null) ? oldme.Event.EventCpeDev.Where(a => a.RecStusId == 1).Count() : 0;
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.EventCpeDev.Count != oldCPEDevCount))
                //    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "CPE Equipment Device Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("CPE Equipment Device Table has been modified ;");
                var oldDevSrvcCount = (oldme.Event.EventDevSrvcMgmt.Where(a => a.RecStusId == 1) != null) ? oldme.Event.EventDevSrvcMgmt.Where(a => a.RecStusId == 1).Count() : 0;
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.EventDevSrvcMgmt.Count != oldDevSrvcCount))
                //    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Device Management Service Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("MDS Device Management Service Table has been modified ;");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.MdsEventSiteSrvc.Count != oldme.Event.MdsEventSiteSrvc.Count))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Service Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Service Table has been modified ;");
                var oldDslSbicCount = (oldme.Event.MdsEventDslSbicCustTrpt.Where(a => a.RecStusId == 1) != null) ? oldme.Event.MdsEventDslSbicCustTrpt.Where(a => a.RecStusId == 1).Count() : 0;
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.MdsEventDslSbicCustTrpt.Count != oldDslSbicCount))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Customer Transport Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Customer Transport Table has been modified ;");
                var oldSlnkWiredCount = (oldme.Event.MdsEventSlnkWiredTrpt.Where(a => a.RecStusId == 1) != null) ? oldme.Event.MdsEventSlnkWiredTrpt.Where(a => a.RecStusId == 1).Count() : 0;
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.MdsEventSlnkWiredTrpt.Count != oldSlnkWiredCount))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Wired Transport Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Wired Transport Table has been modified ;");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.MdsEventWrlsTrpt.Count != oldme.Event.MdsEventWrlsTrpt.Count))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Wireless Transport Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Wireless Transport Table has been modified ;");
                var oldPortBndwdCount = (oldme.Event.MdsEventPortBndwd.Where(a => a.RecStusId == 1) != null) ? oldme.Event.MdsEventPortBndwd.Where(a => a.RecStusId == 1).Count() : 0;
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.Event.MdsEventPortBndwd.Count != oldPortBndwdCount))
                    //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Port Bandwidth Table has been modified.", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("MDS Port Bandwidth Table has been modified ;");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.InstlSitePocNme ?? "") != (oldme.InstlSitePocNme ?? "")))
                    //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Install Site POC updated from " + oldme.InstlSitePocNme + " to " + Obj.InstlSitePocNme, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Install Site POC updated from " + oldme.InstlSitePocNme + " to " + Obj.InstlSitePocNme + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.InstlSitePocPhnNbr ?? "") != (oldme.InstlSitePocPhnNbr ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Install Site POC Phone# updated from " + oldme.InstlSitePocPhnNbr + " to " + Obj.InstlSitePocPhnNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Install Site POC Phone# updated from " + oldme.InstlSitePocPhnNbr + " to " + Obj.InstlSitePocPhnNbr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.InstlSitePocCellPhnNbr ?? "") != (oldme.InstlSitePocCellPhnNbr ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Install Site POC CellPhone# updated from " + oldme.InstlSitePocPhnNbr + " to " + Obj.InstlSitePocPhnNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Install Site POC CellPhone# updated from " + oldme.InstlSitePocCellPhnNbr + " to " + Obj.InstlSitePocCellPhnNbr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.SrvcAssrnPocNme ?? "") != (oldme.SrvcAssrnPocNme ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Service Assurance POC updated from " + oldme.SrvcAssrnPocNme + " to " + Obj.SrvcAssrnPocNme, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Service Assurance POC updated from " + oldme.SrvcAssrnPocNme + " to " + Obj.SrvcAssrnPocNme + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.SrvcAssrnPocPhnNbr ?? "") != (oldme.SrvcAssrnPocPhnNbr ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Service Assurance POC Phone# updated from " + oldme.SrvcAssrnPocPhnNbr + " to " + Obj.SrvcAssrnPocPhnNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Service Assurance POC Phone# updated from " + oldme.SrvcAssrnPocPhnNbr + " to " + Obj.SrvcAssrnPocPhnNbr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.SrvcAssrnPocCellPhnNbr ?? "") != (oldme.SrvcAssrnPocCellPhnNbr ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Service Assurance POC CellPhone# updated from " + oldme.SrvcAssrnPocCellPhnNbr + " to " + Obj.SrvcAssrnPocCellPhnNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Service Assurance POC CellPhone# updated from " + oldme.SrvcAssrnPocCellPhnNbr + " to " + Obj.SrvcAssrnPocCellPhnNbr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.StreetAdr ?? "") != (oldme.StreetAdr ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Address updated from " + oldme.StreetAdr + " to " + Obj.StreetAdr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Address updated from " + oldme.StreetAdr + " to " + Obj.StreetAdr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.SttPrvnNme ?? "") != (oldme.SttPrvnNme ?? "")))
                    //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "State/Province updated from " + oldme.SttPrvnNme + " to " + Obj.SttPrvnNme, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("State/Province updated from " + oldme.SttPrvnNme + " to " + Obj.SttPrvnNme + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.ZipCd ?? "") != (oldme.ZipCd ?? "")))
                    //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Zip/Postal Code updated from " + oldme.ZipCd + " to " + Obj.ZipCd, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Zip/Postal Code updated from " + oldme.ZipCd + " to " + Obj.ZipCd + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.FlrBldgNme ?? "") != (oldme.FlrBldgNme ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Floor updated from " + oldme.FlrBldgNme + " to " + Obj.FlrBldgNme, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Floor updated from " + oldme.FlrBldgNme + " to " + Obj.FlrBldgNme + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.CtyNme ?? "") !=( oldme.CtyNme ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "City updated from " + oldme.CtyNme + " to " + Obj.CtyNme, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("City updated from " + oldme.CtyNme + " to " + Obj.CtyNme + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.CtryRgnNme ?? "") != (oldme.CtryRgnNme ?? "")))
                    //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Country updated from " + oldme.CtyNme + " to " + Obj.CtyNme, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Country updated from " + oldme.CtyNme + " to " + Obj.CtyNme + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.UsIntlCd ?? "") != (oldme.UsIntlCd ?? "")))
                    //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "US/Intl updated from " + (oldme.UsIntlCd == null ? "" : (oldme.UsIntlCd.Equals("I") ? "Intl." : "US")) + " to " + (Obj.UsIntlCd == null ? "" : (Obj.UsIntlCd.Equals("I") ? "Intl." : "US")), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("US / Intl updated from " + (oldme.UsIntlCd == null ? "" : (oldme.UsIntlCd.Equals("I") ? "Intl." : "US")) + " to " + (Obj.UsIntlCd == null ? "" : (Obj.UsIntlCd.Equals("I") ? "Intl." : "US")) + " ; " );
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.SrvcAvlbltyHrs ?? "") != (oldme.SrvcAvlbltyHrs ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Service Assurance Contact Hours updated from " + oldme.SrvcAvlbltyHrs + " to " + Obj.SrvcAvlbltyHrs, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Service Assurance Contact Hours updated from " + oldme.SrvcAvlbltyHrs + " to " + Obj.SrvcAvlbltyHrs + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.SrvcTmeZnCd ?? "") != (oldme.SrvcTmeZnCd ?? "")))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Service Assurance Time Zone updated from " + oldme.SrvcTmeZnCd + " to " + Obj.SrvcTmeZnCd, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Service Assurance Time Zone updated from " + oldme.SrvcTmeZnCd + " to " + Obj.SrvcTmeZnCd + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.SprintCpeNcr != oldme.SprintCpeNcr))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "CPE Delivery Options updated from " + oldme.SprintCpeNcr + " to " + Obj.SprintCpeNcr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("CPE Delivery Options updated from " + oldme.SprintCpeNcr + " to " + Obj.SprintCpeNcr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.CpeDspchEmailAdr ?? "") != (oldme.CpeDspchEmailAdr ?? "")))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "CPE Dispatch Email updated from " + oldme.CpeDspchEmailAdr + " to " + Obj.CpeDspchEmailAdr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("CPE Dispatch Email updated from " + oldme.CpeDspchEmailAdr + " to " + Obj.CpeDspchEmailAdr + " ; ");
                //if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.CpeDspchCmntTxt != oldme.CpeDspchCmntTxt))
                //   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "CPE Dispatch Comment updated from " + oldme.CpeDspchCmntTxt + " to " + Obj.CpeDspchCmntTxt, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                //     builder.Append("CPE Dispatch Comment updated from " + oldme.CpeDspchCmntTxt + " to " + Obj.CpeDspchCmntTxt  + " ; ");

                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && !string.IsNullOrWhiteSpace(Obj.ShippedDt.ToString()) && !string.IsNullOrWhiteSpace(oldme.ShippedDt.ToString()) && (Obj.ShippedDt.ToString().Trim() != oldme.ShippedDt.ToString().Trim()))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Shipped Date updated from " + oldme.ShippedDt + " to " + Obj.ShippedDt, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Shipped Date updated from " + oldme.ShippedDt + " to " + Obj.ShippedDt  + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.ShipCustEmailAdr ?? "") != (oldme.ShipCustEmailAdr ?? "")))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Shipment Customer Email updated from " + oldme.ShipCustEmailAdr + " to " + Obj.ShipCustEmailAdr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("Shipment Customer Email updated from " + oldme.ShipCustEmailAdr + " to " + Obj.ShipCustEmailAdr  + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.ShipCxrNme ?? "") != (oldme.ShipCxrNme ?? "")))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Shipment Carrier Name updated from " + oldme.ShipCxrNme + " to " + Obj.ShipCxrNme, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Shipment Carrier Name updated from " + oldme.ShipCxrNme + " to " + Obj.ShipCxrNme  + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.ShipTrkRefrNbr ?? "") != (oldme.ShipTrkRefrNbr ?? "")))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Shipment Tracking/Ref No. updated from " + oldme.ShipTrkRefrNbr + " to " + Obj.ShipTrkRefrNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("Shipment Tracking/Ref No. updated from " + oldme.ShipTrkRefrNbr + " to " + Obj.ShipTrkRefrNbr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((Obj.ShipDevSerialNbr ?? "") !=( oldme.ShipDevSerialNbr ?? "")))
                  //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Shipment Device Serial Number updated from " + oldme.ShipDevSerialNbr + " to " + Obj.ShipDevSerialNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("Shipment Device Serial Number updated from " + oldme.ShipDevSerialNbr + " to " + Obj.ShipDevSerialNbr + " ; ");

                // MDS Schedule
                //if(Obj.MdsActyTypeId != null && oldme.MdsActyTypeId != null){
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && (Obj.CnfrcBrdgId != oldme.CnfrcBrdgId)))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Bridge Info. updated from " + oldme.CnfrcBrdgId + " to " + Obj.CnfrcBrdgId, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("MDS Bridge Info. updated from " + oldme.CnfrcBrdgId + " to " + Obj.CnfrcBrdgId + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && ((Obj.CnfrcBrdgNbr ?? "") != (oldme.CnfrcBrdgNbr ?? ""))))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Bridge Number updated from " + oldme.CnfrcBrdgNbr + " to " + Obj.CnfrcBrdgNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("MDS Bridge Number updated from " + oldme.CnfrcBrdgNbr + " to " + Obj.CnfrcBrdgNbr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && ((Obj.CnfrcPinNbr ?? "") != (oldme.CnfrcPinNbr ?? ""))))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Bridge Pin updated from " + oldme.CnfrcPinNbr + " to " + Obj.CnfrcPinNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("MDS Bridge Pin updated from " + oldme.CnfrcPinNbr + " to " + Obj.CnfrcPinNbr + " ; ");
     
                //if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && ((Obj.OnlineMeetingAdr) != oldme.OnlineMeetingAdr)) && (!String.IsNullOrEmpty(Obj.OnlineMeetingAdr)) && (!String.IsNullOrEmpty(oldme.OnlineMeetingAdr)))
                //    //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Bridge Meeting URL updated from " + oldme.OnlineMeetingAdr + " to " + Obj.OnlineMeetingAdr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                //     builder.Append("MDS Bridge Meeting URL updated from " + oldme.OnlineMeetingAdr + " to " + Obj.OnlineMeetingAdr + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && (Obj.StrtTmst != oldme.StrtTmst)))
                  //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Start Time updated from " + oldme.StrtTmst.ToString() + " to " + Obj.StrtTmst.ToString(), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("MDS Start Time updated from " + oldme.StrtTmst.ToString() + " to " + Obj.StrtTmst.ToString() + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && (Obj.EventDrtnInMinQty != oldme.EventDrtnInMinQty)))
                  //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Event Duration updated from " + oldme.EventDrtnInMinQty.ToString() + " to " + Obj.EventDrtnInMinQty.ToString(), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("MDS Event Duration updated from " + oldme.EventDrtnInMinQty.ToString() + " to " + Obj.EventDrtnInMinQty.ToString() + " ; ");

                //if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && (Obj.ExtraDrtnTmeAmt != oldme.ExtraDrtnTmeAmt)) && (!String.IsNullOrEmpty(Obj.ExtraDrtnTmeAmt.ToString())) && (!String.IsNullOrEmpty(oldme.ExtraDrtnTmeAmt.ToString())))
                //    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Extra Duration updated from " + oldme.ExtraDrtnTmeAmt.ToString() + " to " + Obj.ExtraDrtnTmeAmt.ToString(), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                //    builder.Append("MDS Extra Duration updated from " + oldme.ExtraDrtnTmeAmt.ToString() + " to " + Obj.ExtraDrtnTmeAmt.ToString() + " ; ");

                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && (Obj.EndTmst != oldme.EndTmst)))
                   // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS End Time updated from " + oldme.EndTmst.ToString() + " to " + Obj.EndTmst.ToString(), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                     builder.Append("MDS End Time updated from " + oldme.EndTmst.ToString() + " to " + Obj.EndTmst.ToString() + " ; ");
                //}
                    var oldAssignedTo = oldme.Event.EventAsnToUser.Except(Obj.Event.EventAsnToUser).ToList();
                    //oldAssignedTo.Remove(string.Empty);
                    var objAssignedTo = Obj.Event.EventAsnToUser.Except(oldme.Event.EventAsnToUser).ToList();
                    //objAssignedTo.Remove(string.Empty);
                    if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (mdsFastTrackTypeDes.Length <= 0) && (mdsoldFastTrackTypeDes.Length <= 0) && ((oldAssignedTo.Count > 0) || (objAssignedTo.Count > 0)))
                        // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "MDS Assigned To updated from " + ((oldme.Event.EventAsnToUser.Count > 0) ? String.Join(";", oldme.Event.EventAsnToUser) : string.Empty) + " to " + ((Obj.Event.EventAsnToUser.Count > 0) ? String.Join(";", Obj.Event.EventAsnToUser) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                        builder.Append("MDS Assigned To updated from " + ((oldme.Event.EventAsnToUser.Count > 0) ? String.Join(";", oldme.Event.EventAsnToUser) : string.Empty) + " to " + ((Obj.Event.EventAsnToUser.Count > 0) ? String.Join(";", Obj.Event.EventAsnToUser) : string.Empty) + " ; ");              
            }
            var esclReasDes = _context.LkEsclReas.SingleOrDefault(i => i.EsclReasId == Obj.EsclReasId);
            var esclOldReasDes = _context.LkEsclReas.SingleOrDefault(i => i.EsclReasId == oldme.EsclReasId);
            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.EsclCd != oldme.EsclCd))
                //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Escalation Code updated from " + oldme.EsclCd.ToString() + " to " + Obj.EsclCd.ToString(), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Escalation Code updated from " + oldme.EsclCd.ToString() + " to " + Obj.EsclCd.ToString() + " ; ");
            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && Obj.EsclCd && (Obj.EsclReasId != oldme.EsclReasId))
                //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Escalation Reason updated from " + (esclOldReasDes == null ? "" : esclOldReasDes.EsclReasDes) + " to " + esclReasDes.EsclReasDes, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Escalation Reason updated from " + (esclOldReasDes == null ? "" : esclOldReasDes.EsclReasDes) + " to " + (esclReasDes == null ? "" : esclReasDes.EsclReasDes) + " ; ");
            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && Obj.EsclCd && (Obj.PrimReqDt != oldme.PrimReqDt))
                //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Escalation Primary Date updated from " + (oldme.PrimReqDt == null ? "" : oldme.PrimReqDt.ToString()) + " to " + Obj.PrimReqDt, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Escalation Primary Date updated from " + (oldme.PrimReqDt == null ? "" : oldme.PrimReqDt.ToString()) + " to " + Obj.PrimReqDt  + " ; ");
            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && Obj.EsclCd && (Obj.ScndyReqDt != oldme.ScndyReqDt))
                //lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Escalation Secondary Date updated from " + (oldme.ScndyReqDt == null ? "" : oldme.ScndyReqDt.ToString()) + " to " + Obj.ScndyReqDt, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Escalation Secondary Date updated from " + (oldme.ScndyReqDt == null ? "" : oldme.ScndyReqDt.ToString()) + " to " + Obj.ScndyReqDt + " ; ");

            if (Obj.PubEmailCcTxt != null && oldme.PubEmailCcTxt == null)
            {         
                var objpubcc = Obj.PubEmailCcTxt.ToList();
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && objpubcc.Count > 0)
                    builder.Append("Published Email CC updated from " + " to " + ((Obj.PubEmailCcTxt != "") ? String.Join(";", Obj.PubEmailCcTxt) : string.Empty) + " ; "); 
            }
            if(Obj.PubEmailCcTxt == null && oldme.PubEmailCcTxt != null)
            {
                var oldpubcc = oldme.PubEmailCcTxt.ToList();
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && oldpubcc.Count > 0)
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Published Email CC updated from " + ((oldme.PubEmailCcTxt != "") ? String.Join(";", oldme.PubEmailCcTxt) : string.Empty) + " to " + ((Obj.PubEmailCcTxt != "") ? String.Join(";", Obj.PubEmailCcTxt) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Published Email CC updated from " + ((oldme.PubEmailCcTxt != "") ? String.Join(";", oldme.PubEmailCcTxt) : string.Empty) + " to " + " ; ");
            }

            if (Obj.CmpltdEmailCcTxt != null && oldme.CmpltdEmailCcTxt == null)
            {
                var objpubcc = Obj.CmpltdEmailCcTxt.ToList();
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && objpubcc.Count > 0)
                    builder.Append("Completed Email CC updated from " + " to " + ((Obj.CmpltdEmailCcTxt != "") ? String.Join(";", Obj.CmpltdEmailCcTxt) : string.Empty) + " ; ");
            }
            if (Obj.CmpltdEmailCcTxt == null && oldme.CmpltdEmailCcTxt != null)
            {
                var oldpubcc = oldme.CmpltdEmailCcTxt.ToList();
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && oldpubcc.Count > 0)
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Published Email CC updated from " + ((oldme.PubEmailCcTxt != "") ? String.Join(";", oldme.PubEmailCcTxt) : string.Empty) + " to " + ((Obj.PubEmailCcTxt != "") ? String.Join(";", Obj.PubEmailCcTxt) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Completed Email CC updated from " + ((oldme.CmpltdEmailCcTxt != "") ? String.Join(";", oldme.CmpltdEmailCcTxt) : string.Empty) + " to " + " ; ");
            }
            if (Obj.PubEmailCcTxt != null && oldme.PubEmailCcTxt != null)
            {
                var oldpubcc = oldme.PubEmailCcTxt.Except(Obj.PubEmailCcTxt).ToList();
                //oldpubcc.Remove(string.Empty);
                
                var objpubcc = Obj.PubEmailCcTxt.Except(oldme.PubEmailCcTxt).ToList();
                //objpubcc.Remove(string.Empty);
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((oldpubcc.Count > 0) || (objpubcc.Count > 0)))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Published Email CC updated from " + ((oldme.PubEmailCcTxt != "") ? String.Join(";", oldme.PubEmailCcTxt) : string.Empty) + " to " + ((Obj.PubEmailCcTxt != "") ? String.Join(";", Obj.PubEmailCcTxt) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Published Email CC updated from " + ((oldme.PubEmailCcTxt != "") ? String.Join(";", oldme.PubEmailCcTxt) : string.Empty) + " to " + ((Obj.PubEmailCcTxt != "") ? String.Join(";", Obj.PubEmailCcTxt) : string.Empty) + " ; ");
            }
            if (Obj.CmpltdEmailCcTxt != null && oldme.CmpltdEmailCcTxt != null)
            {
                var oldcmplcc = oldme.CmpltdEmailCcTxt.Except(Obj.CmpltdEmailCcTxt).ToList();
                //oldcmplcc.Remove(string.Empty);
                var objcmplcc = Obj.CmpltdEmailCcTxt.Except(oldme.CmpltdEmailCcTxt).ToList();
                //objcmplcc.Remove(string.Empty);
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((oldcmplcc.Count > 0) || (objcmplcc.Count > 0)))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Completed Email CC updated from " + ((oldme.CmpltdEmailCcTxt != "") ? String.Join(";", oldme.CmpltdEmailCcTxt) : string.Empty) + " to " + ((Obj.CmpltdEmailCcTxt != "") ? String.Join(";", Obj.CmpltdEmailCcTxt) : string.Empty), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("Completed Email CC updated from " + ((oldme.CmpltdEmailCcTxt != "") ? String.Join(";", oldme.CmpltdEmailCcTxt) : string.Empty) + " to " + ((Obj.CmpltdEmailCcTxt != "") ? String.Join(";", Obj.CmpltdEmailCcTxt) : string.Empty) + " ; ");
            }

            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.ReqorUserId != oldme.ReqorUserId))
                // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Requestor Changed", CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Requestor Changed" + " ; ");
            if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (Obj.ReqorUserCellPhnNbr != oldme.ReqorUserCellPhnNbr))
                //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "Requestor Contact CellPhone updated from " + oldme.ReqorUserCellPhnNbr + " to " + Obj.ReqorUserCellPhnNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                builder.Append("Requestor Contact CellPhone updated from " + oldme.ReqorUserCellPhnNbr + " to " + Obj.ReqorUserCellPhnNbr + " ; ");

            if (Obj.CustLtrOptOut != oldme.CustLtrOptOut)
                builder.Append("Customer Communication Opt-Out updated from " + oldme.CustLtrOptOut + " to " + Obj.CustLtrOptOut + " ; ");

            // FT Schedule
            if (Obj.MdsActyTypeId != null && oldme.MdsActyTypeId != null)
            {
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length > 0) && (mdsoldFastTrackTypeDes.Length > 0) && (mdsFastTrackTypeDes != mdsoldFastTrackTypeDes)))
                    //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "ft type updated from " + mdsoldFastTrackTypeDes + " to " + mdsFastTrackTypeDes, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("ft type updated from " + mdsoldFastTrackTypeDes + " to " + mdsFastTrackTypeDes + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && (((mdsFastTrackTypeDes.Length > 0) && (mdsoldFastTrackTypeDes.Length > 0) && Obj.CnfrcBrdgId != oldme.CnfrcBrdgId)))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "conference bridge needed updated from " + ((oldme.CnfrcBrdgId == 4) ? "yes" : "no") + " to " + ((Obj.CnfrcBrdgId == 4) ? "yes" : "no"), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("conference bridge needed updated from " + ((oldme.CnfrcBrdgId == 4) ? "yes" : "no") + " to " + ((Obj.CnfrcBrdgId == 4) ? "yes" : "no") + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length > 0) && (Obj.TmeSlotId > 0 && oldme.TmeSlotId > 0) && (mdsoldFastTrackTypeDes.Length > 0) && (Obj.TmeSlotId != oldme.TmeSlotId)))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "timeslot updated from " + GetTimeSlotDesc(oldme.TmeSlotId.ToString()) + " to " + GetTimeSlotDesc(Obj.TmeSlotId.ToString()), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("timeslot updated from " + GetTimeSlotDesc(oldme.TmeSlotId.ToString()) + " to " + GetTimeSlotDesc(Obj.TmeSlotId.ToString())  + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length > 0) && (mdsoldFastTrackTypeDes.Length > 0) && (Obj.CnfrcBrdgNbr != oldme.CnfrcBrdgNbr)))
                    // lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "bridge number updated from " + oldme.CnfrcBrdgNbr + " to " + Obj.CnfrcBrdgNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("bridge number updated from " + oldme.CnfrcBrdgNbr + " to " + Obj.CnfrcBrdgNbr  + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length > 0) && (mdsoldFastTrackTypeDes.Length > 0) && (Obj.CnfrcPinNbr != oldme.CnfrcPinNbr)))
                    //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "bridge pin updated from " + oldme.CnfrcPinNbr + " to " + Obj.CnfrcPinNbr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("bridge pin updated from " + oldme.CnfrcPinNbr + " to " + Obj.CnfrcPinNbr  + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length > 0) && (mdsoldFastTrackTypeDes.Length > 0) && (Obj.OnlineMeetingAdr != oldme.OnlineMeetingAdr)))
                    //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "meeting url updated from " + oldme.OnlineMeetingAdr + " to " + Obj.OnlineMeetingAdr, CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                    builder.Append("meeting url updated from " + oldme.OnlineMeetingAdr + " to " + Obj.OnlineMeetingAdr  + " ; ");
                if ((Obj.MdsActyTypeId == oldme.MdsActyTypeId) && ((mdsFastTrackTypeDes.Length > 0) && (mdsoldFastTrackTypeDes.Length > 0) && (Obj.StrtTmst != oldme.StrtTmst)))
                  //  lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = "ft requested installation date updated from " + oldme.StrtTmst.ToString() + " to " + Obj.StrtTmst.ToString(), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
                  builder.Append("ft requested installation date updated from " + oldme.StrtTmst.ToString() + " to " + Obj.StrtTmst.ToString() + " ; ");
            }
            if (builder.Length > 0)
            {
                lmfc.Add(new MdsEventFormChng { MdsEventId = Obj.EventId, ChngTypeNme = builder.ToString(), CreatByUserId = Obj.CreatByUserId, CreatDt = DateTime.Now });
            }
            if (lmfc.Count > 0)
            {
                _context.MdsEventFormChng.AddRange(lmfc.Distinct(new MdsEventFormChngComparer()));
                _context.SaveChanges();
            }

            return true;

        }

        private MdsEventFormChng CreateMdsEventFormChange(int eventId, string typeName, int userId)
        {
            return new MdsEventFormChng
            {
                MdsEventId = eventId,
                ChngTypeNme = typeName,
                CreatByUserId = userId,
                CreatDt = DateTime.Now
            };
        }

        private string GetTimeSlotDesc(string timeslotID)
        {
            if (timeslotID == "1")
                return "06:00 AM CT-10:00 AM CT";
            else if (timeslotID == "2")
                return "10:00 AM CT-02:00 PM CT";
            else if (timeslotID == "3")
                return "02:00 PM CT-06:00 PM CT";
            else if (timeslotID == "4")
                return "06:00 PM CT-10:00 PM CT";
            else
                return string.Empty;
        }

        public DataTable GetEventHistByEventID(int iEventID, EventType EventTypeID)
        {
            #region linq leftouterjoin

            var y = (from eh in _context.EventHist
                     join su in _context.EventSucssActy on eh.EventHistId equals su.EventHistId
                     where eh.EventId == iEventID
                     select eh).ToList();

            var a = (from eh in _context.EventHist
                     join fu in _context.EventFailActy on eh.EventHistId equals fu.EventHistId
                     where eh.EventId == iEventID
                     select eh).ToList();

            var b = (from eh in _context.EventHist
                     join fu in _context.LkFailReas on eh.FailReasId equals fu.FailReasId
                     where ((eh.EventId == iEventID) && (eh.FailReasId != null) && (eh.FailReasId > 1))
                     select eh).ToList();

            if ((y.Count() <= 0) && (a.Count() <= 0) && (b.Count() <= 0))
            {
                //bActyFlg = true;
            }

            DataTable dtEHist = new DataTable();
            dtEHist.Columns.Add(new DataColumn("EventId"));
            dtEHist.Columns.Add(new DataColumn("EventHistId"));
            dtEHist.Columns.Add(new DataColumn("ActnDes"));
            dtEHist.Columns.Add(new DataColumn("FullNme"));
            dtEHist.Columns.Add(new DataColumn("CreatDt", typeof(DateTime)));
            dtEHist.Columns.Add(new DataColumn("FailReas"));
            dtEHist.Columns.Add(new DataColumn("FailActy"));
            dtEHist.Columns.Add(new DataColumn("SucssActy"));
            dtEHist.Columns.Add(new DataColumn("CmntTxt"));
            dtEHist.Columns.Add(new DataColumn("EventStrtTmst"));
            dtEHist.Columns.Add(new DataColumn("EventEndTmst"));

            var x = from efa in
                        (from efr in
                             (from eact in
                                  (from est in
                                       (from eh in _context.EventHist
                                        join lct in _context.LkActn on eh.ActnId equals lct.ActnId
                                        join lu in _context.LkUser on eh.CreatByUserId equals lu.UserId
                                        join esat in _context.EventSucssActy on eh.EventHistId equals esat.EventHistId
                                              into joinsat
                                        from esat in joinsat.DefaultIfEmpty()
                                        where eh.EventId == iEventID
                                        select new
                                        {
                                            eh.EventId,
                                            eh.EventHistId,
                                            lct.ActnDes,
                                            lu.FullNme,
                                            eh.CreatDt,
                                            eh.CmntTxt,
                                            eh.EventStrtTmst,
                                            eh.EventEndTmst,

                                                    //FailActyId = (eat != null ? eat.FailActyId : null),
                                                    SucssActyId = (esat != null ? esat.SucssActyId : -1),
                                            eh.FailReasId
                                        })
                                   join efat in _context.EventFailActy on est.EventHistId equals efat.EventHistId
                                     into joinefa
                                   from efat in joinefa.DefaultIfEmpty()
                                   select new
                                   {
                                       est.EventId,
                                       est.EventHistId,
                                       est.ActnDes,
                                       est.FullNme,
                                       est.CreatDt,
                                       est.CmntTxt,
                                       est.EventStrtTmst,
                                       est.EventEndTmst,
                                       FailActyId = (efat != null ? efat.FailActyId : -1),
                                       est.SucssActyId,
                                       est.FailReasId
                                   })
                              join lfr in _context.LkFailReas on eact.FailReasId equals lfr.FailReasId
                                  into joineh
                              from lfr in joineh.DefaultIfEmpty()
                              select new
                              {
                                  eact.EventId,
                                  eact.EventHistId,
                                  eact.ActnDes,
                                  eact.FullNme,
                                  eact.CreatDt,
                                  eact.CmntTxt,
                                  eact.EventStrtTmst,
                                  eact.EventEndTmst,
                                  eact.FailActyId,
                                  eact.SucssActyId,
                                  FailReasDes = (lfr != null ? lfr.FailReasDes : string.Empty)
                              })
                         join lfa in _context.LkFailActy on efr.FailActyId equals lfa.FailActyId
                             into joinefr
                         from lfa in joinefr.DefaultIfEmpty()
                         select new
                         {
                             efr.EventId,
                             efr.EventHistId,
                             efr.ActnDes,
                             efr.FullNme,
                             efr.CreatDt,
                             efr.CmntTxt,
                             efr.EventStrtTmst,
                             efr.EventEndTmst,
                             efr.SucssActyId,
                             FailActyDes = (lfa != null ? lfa.FailActyDes : string.Empty),
                             efr.FailReasDes
                         })
                    join lsa in _context.LkSucssActy on efa.SucssActyId equals lsa.SucssActyId
                        into joinefs
                    from lsa in joinefs.DefaultIfEmpty()
                    select new
                    {
                        efa.EventId,
                        efa.EventHistId,
                        efa.ActnDes,
                        efa.FullNme,
                        efa.CreatDt,
                        efa.CmntTxt,
                        efa.EventStrtTmst,
                        efa.EventEndTmst,

                        //efa.SucssActyId,
                        efa.FailActyDes,
                        SucssActyDes = (lsa != null ? lsa.SucssActyDes : string.Empty),
                        efa.FailReasDes
                    };

            x = x.OrderBy(x1 => x1.EventHistId);

            #endregion linq leftouterjoin

            int iEHID = -1;
            string sSuccActv = string.Empty;
            string sFailActv = string.Empty;
            foreach (var z in x)
            {
                DataRow dr = dtEHist.NewRow();
                if (iEHID == z.EventHistId)
                {
                    if ((!sSuccActv.Equals("None")) && (!sSuccActv.Contains(z.SucssActyDes)))
                        sSuccActv = sSuccActv + "; " + z.SucssActyDes;
                    if ((!sFailActv.Equals("None")) && (!sFailActv.Contains(z.FailActyDes)))
                        sFailActv = sFailActv + "; " + z.FailActyDes;
                }
                else
                {
                    if (sSuccActv.Contains(";"))
                        dtEHist.Rows[dtEHist.Rows.Count - 1]["SucssActy"] = sSuccActv;
                    if (sFailActv.Contains(";"))
                        dtEHist.Rows[dtEHist.Rows.Count - 1]["FailReas"] = sFailActv;
                    iEHID = z.EventHistId;
                    sSuccActv = z.SucssActyDes;
                    sFailActv = z.FailActyDes;
                    dr["EventId"] = z.EventId;
                    dr["EventHistId"] = z.EventHistId;
                    dr["ActnDes"] = z.ActnDes;
                    dr["FullNme"] = z.FullNme;
                    dr["CreatDt"] = z.CreatDt;
                    dr["FailReas"] = z.FailReasDes;
                    dr["FailActy"] = z.FailActyDes;
                    dr["SucssActy"] = z.SucssActyDes;
                    dr["CmntTxt"] = z.CmntTxt;
                    dr["EventStrtTmst"] = z.EventStrtTmst;
                    dr["EventEndTmst"] = z.EventEndTmst;
                    dtEHist.Rows.Add(dr);
                }
            }

            if (sSuccActv.Contains(";"))
                dtEHist.Rows[dtEHist.Rows.Count - 1]["SucssActy"] = sSuccActv;
            if (sFailActv.Contains(";"))
                dtEHist.Rows[dtEHist.Rows.Count - 1]["FailActy"] = sFailActv;

            if (EventTypeID == EventType.MDS)
            {
                DataTable dtEUpdtHist = new DataTable();
                var x2 = (from e in _context.MdsEventFormChng
                          join lk in _context.LkUser on e.CreatByUserId equals lk.UserId
                          where (e.MdsEventId == iEventID)
                          orderby e.MdsEventFormChngId descending
                          select new { e.MdsEventFormChngId, e.MdsEventId, e.ChngTypeNme, lk.FullNme, e.CreatDt });

                dtEUpdtHist = LinqHelper.CopyToDataTable(x2, null, null);
                foreach (DataRow dr in dtEUpdtHist.Rows)
                {
                    
                    var findEvntUpt = (from eh in dtEHist.AsEnumerable()
                                       where (eh.Field<DateTime>("CreatDt") == DateTime.Parse(dr["CreatDt"].ToString()))
                                            && (eh.Field<string>("ActnDes").Equals("Event Data Updated"))
                                       select eh
                                       ).ToList<DataRow>();

                    if (findEvntUpt.Count > 0)
                    {
                        foreach (var row in findEvntUpt)
                        {
                            if (row.Field<string>("CmntTxt").Trim().Length <= 0)
                                row.SetField("CmntTxt", dr["ChngTypeNme"].ToString());
                            else
                                row.SetField("CmntTxt", row.Field<string>("CmntTxt").Trim() + "; " + dr["ChngTypeNme"].ToString());
                        }
                    }
                    else
                    {
                        DataRow dr2 = dtEHist.NewRow();
                        dr2["EventId"] = iEventID;
                        dr2["EventHistId"] = -1;
                        dr2["ActnDes"] = "Event Data Updated";
                        dr2["FullNme"] = dr["FullNme"];
                        dr2["CreatDt"] = dr["CreatDt"];
                        dr2["FailReas"] = string.Empty;
                        dr2["FailActy"] = string.Empty;
                        dr2["SucssActy"] = string.Empty;
                        dr2["CmntTxt"] = dr["ChngTypeNme"].ToString();
                        dr2["EventStrtTmst"] = string.Empty;
                        dr2["EventEndTmst"] = string.Empty;
                        dtEHist.Rows.Add(dr2);
                    }
                }

                var query = (from order in dtEHist.AsEnumerable()
                            orderby order.Field<DateTime>("CreatDt")
                            select new { order }).ToList();

                return LinqHelper.CopyToDataTable(query, null, null); 
            }
            else
                return dtEHist;
        }

        public EventHist GetReviewerId(int eventId)
        {
            var data = _context.EventHist.Where(a => a.EventId == eventId && (a.ActnId == 5 || a.ActnId == 16)).OrderByDescending(a=>a.CreatDt).FirstOrDefault();
            return data;
        }
    }
}