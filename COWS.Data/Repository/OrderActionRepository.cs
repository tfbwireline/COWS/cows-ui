﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderActionRepository : IOrderActionRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public OrderActionRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkOrdrActn Create(LkOrdrActn entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkOrdrActn> Find(Expression<Func<LkOrdrActn, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkOrdrActn, out List<LkOrdrActn> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkOrdrActn> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkOrdrActn, out List<LkOrdrActn> list))
            {
                list = _context.LkOrdrActn
                            .Where(a => a.OrdrActnDes.Contains("Submit"))
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkOrdrActn, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkOrdrActn GetById(int id)
        {
            return _context.LkOrdrActn
                .SingleOrDefault(a => a.OrdrActnId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkOrdrActn);
            return _context.SaveChanges();
        }

        public void Update(int id, LkOrdrActn entity)
        {
            throw new NotImplementedException();
        }
    }
}