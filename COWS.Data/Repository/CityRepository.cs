﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CityRepository : ICityRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CityRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCtryCty Create(LkCtryCty entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCtryCty> Find(Expression<Func<LkCtryCty, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCtryCty, out List<LkCtryCty> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCtryCty> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCtryCty, out List<LkCtryCty> list))
            {
                list = _context.LkCtryCty
                            .OrderBy(i => i.CtyNme)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCtryCty, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCtryCty GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCtryCty);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCtryCty entity)
        {
            throw new NotImplementedException();
        }
    }
}