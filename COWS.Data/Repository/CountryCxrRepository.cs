﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CountryCxrRepository : ICountryCxrRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CountryCxrRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCtryCxr Create(LkCtryCxr entity)
        {
            // Find and delete entry if existing before creating record
            var cxr = Find(i => i.CxrCd == entity.CxrCd && i.CtryCd == entity.CtryCd).SingleOrDefault();

            if (cxr != null)
            {
                _context.LkCtryCxr.Remove(cxr);
                SaveAll();
            }

            _context.LkCtryCxr.Add(entity);

            SaveAll();

            return Find(i => i.CxrCd == entity.CxrCd && i.CtryCd == entity.CtryCd).SingleOrDefault();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCtryCxr> Find(Expression<Func<LkCtryCxr, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCtryCxr, out List<LkCtryCxr> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCtryCxr> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCtryCxr, out List<LkCtryCxr> list))
            {
                list = _context.LkCtryCxr
                            .OrderBy(i => i.CtryCd)
                            .AsNoTracking()
                            .ToList();

                _cache.Set(CacheKeys.LkCtryCxr, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCtryCxr GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCtryCxr);
            return _context.SaveChanges();
        }

        public void Update(int id, LkCtryCxr entity)
        {
            throw new NotImplementedException();
        }
    }
}