﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CurrencyListRepository : ICurrencyListRepository
    {
        private readonly COWSAdminDBContext _context;

        public CurrencyListRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public LkCur Create(LkCur entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCur> Find(Expression<Func<LkCur, bool>> predicate)
        {
            return _context.LkCur
                            .Where(predicate);
        }

        public IEnumerable<LkCur> GetAll()
        {
            return _context.LkCur
                            .OrderBy(i => i.CurId)
                            .ToList();
        }

        public LkCur GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, LkCur entity)
        {
            throw new NotImplementedException();
        }

        public List<LkCur> GetCurrencyList(string SortField, string SearchCriteria)
        {
            List<LkCur> retList = new List<LkCur>();
            LkCur dc = null;

            var cur = (from clu in
                           (from c in _context.LkCur
                            join u in _context.LkUser on c.CreatByUserId equals u.UserId
                            where c.RecStusId == (byte)ERecStatus.Active
                            select new { c, u })
                       join mu in _context.LkUser on clu.c.ModfdByUserId equals mu.UserId
                        into joinmu
                       from mu in joinmu.DefaultIfEmpty()
                       select new { clu.c, clu.u, MU_USER_ADID = mu.UserAdid });

            if (SearchCriteria != null && SearchCriteria != String.Empty)
            {
                cur = cur.Where(SearchCriteria);
            }

            if (SortField != null && SortField != String.Empty)
                cur = cur.OrderBy(SortField);
            else
                cur = cur.OrderBy(c => c.c.CurId);

            foreach (var d in cur)
            {
                dc = new LkCur
                {
                    CurId = d.c.CurId,
                    CurNme = d.c.CurNme,
                    CurCnvrsnFctrFromUsdQty = d.c.CurCnvrsnFctrFromUsdQty,
                    RecStusId = d.c.RecStusId,
                    CreatByUserId = d.u.CreatByUserId
                };
                dc.CreatByUser.UserAdid = d.u.UserAdid;
                dc.CreatDt = d.c.CreatDt;
                if (d.c.ModfdByUserId != null)
                {
                    dc.ModfdByUserId = (int)d.c.ModfdByUserId;
                    dc.ModfdByUser.UserAdid = d.MU_USER_ADID;
                    dc.ModfdDt = (DateTime)d.c.ModfdDt;
                }

                retList.Add(dc);
            }
            return retList;
        }
    }
}