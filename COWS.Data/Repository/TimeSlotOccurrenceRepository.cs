﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class TimeSlotOccurrenceRepository : ITimeSlotOccurrenceRepository
    {
        private readonly COWSAdminDBContext _context;

        public TimeSlotOccurrenceRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public TimeSlotOccurrence Create(TimeSlotOccurrence entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkEventTypeTmeSlot> Find(Expression<Func<LkEventTypeTmeSlot, bool>> predicate)
        {
            return _context.LkEventTypeTmeSlot
                            .Include(a => a.EventTypeRsrcAvlblty)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IQueryable<TimeSlotOccurrence> Find(Expression<Func<TimeSlotOccurrence, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TimeSlotOccurrence> GetAll(short EventType, DateTime startDate, DateTime endDate, bool bShowAfterHrsSlots)
        {
            if (!bShowAfterHrsSlots)
            {
                var data = from t in _context.EventTypeRsrcAvlblty
                           join lt in _context.LkEventTypeTmeSlot on t.TmeSlotId equals lt.TmeSlotId
                           where t.DayDt < endDate
                                          && t.DayDt >= startDate
                                          && t.EventTypeId == EventType
                                          && t.TmeSlotId != 21
                                          && t.TmeSlotId != 22
                                          && t.DayDt.DayOfWeek != DayOfWeek.Saturday
                                          && t.DayDt.DayOfWeek != DayOfWeek.Sunday
                           orderby t.DayDt, lt.TmeSlotStrtTme
                           select new TimeSlotOccurrence
                           {
                               Day = t.DayDt,
                               TimeSlotID = lt.TmeSlotId,
                               TimeSlotStart = lt.TmeSlotStrtTme,
                               TimeSlotEnd = lt.TmeSlotEndTme,
                               TotalTimeSlots = t.AvalTmeSlotCnt,
                               MaxTimeSlots = t.MaxAvalTmeSlotCapCnt,
                               CurAvailTimeSlots = t.CurrAvalTmeSlotCnt,
                               RsrcAvlbltyID = t.EventTypeRsrcAvlbltyId
                           };

                return data.AsNoTracking().ToList();
            }
            else
            {
                var data = from t in _context.EventTypeRsrcAvlblty
                           join lt in _context.LkEventTypeTmeSlot on t.TmeSlotId equals lt.TmeSlotId
                           where t.DayDt < endDate
                                          && t.DayDt >= startDate
                                          && t.EventTypeId == EventType
                           orderby t.DayDt, lt.TmeSlotStrtTme
                           select new TimeSlotOccurrence
                           {
                               Day = t.DayDt,
                               TimeSlotID = lt.TmeSlotId,
                               TimeSlotStart = lt.TmeSlotStrtTme,
                               TimeSlotEnd = lt.TmeSlotEndTme,
                               TotalTimeSlots = t.AvalTmeSlotCnt,
                               MaxTimeSlots = t.MaxAvalTmeSlotCapCnt,
                               CurAvailTimeSlots = t.CurrAvalTmeSlotCnt,
                               RsrcAvlbltyID = t.EventTypeRsrcAvlbltyId
                           };

                return data.AsNoTracking().ToList();
            }
        }

        public IEnumerable<TimeSlotOccurrence> GetAll()
        {
            throw new NotImplementedException();
        }

        public TimeSlotOccurrence GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            throw new NotImplementedException();
        }

        public void Update(int id, TimeSlotOccurrence entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FTAvailEvent> GetFTEventsForTS(int resourceID)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@ResourceID", resourceID)
            };

            var eventsForTS = _context.Query<FTAvailEvent>()
                .AsNoTracking()
                .FromSql("web.getFTAvailEvents @ResourceID", pc.ToArray());

            if (eventsForTS != null)
                eventsForTS.ToList<FTAvailEvent>();

            return eventsForTS;
        }

        public bool UpdateResourceAvailability(string updResultset, int userID)
        {
            try
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() {ParameterName = "@updResultset", SqlDbType = SqlDbType.VarChar, Value = updResultset},
                    new SqlParameter() {ParameterName = "@modUserID", SqlDbType = SqlDbType.Int, Value = userID},
                };

                var updateEvents = _context.Database.ExecuteSqlCommand("dbo.updateResourceAvailability @updResultset, @modUserID", pc.ToArray());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CheckUserAccessToAfterHrsSlots(string adid)
        {
            try
            {
                bool bReturn = false;

                var tss = (from e in _context.LkSysCfg
                           where e.PrmtrNme == "menuShowAfterHrsSlots"
                           && e.PrmtrValuTxt == adid
                           && e.RecStusId == 1
                           select new { e.CfgId });

                if (tss != null && tss.Count() > 0)
                    bReturn = true;

                return bReturn;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<FedlineManageUserEvent> GetInvalidTSEvents()
        {
            List<FedlineManageUserEvent> Events;
            Events = (from ie in _context.EventNvldTmeSlot
                      join td in _context.FedlineEventTadpoleData on ie.EventId equals td.EventId
                      join ud in _context.FedlineEventUserData on ie.EventId equals ud.EventId
                      join es in _context.LkEventStus on ud.EventStusId equals es.EventStusId
                      join ot in _context.LkFedlineOrdrType on td.OrdrTypeCd equals ot.OrdrTypeCd
                      where td.RecStusId == 1
                      select new FedlineManageUserEvent
                      {
                          EventID = ie.EventId,
                          FRBID = td.FrbReqId,
                          StartTime = td.StrtTme,
                          EndTime = td.EndTme,
                          Activity = ot.PrntOrdrTypeDes,
                          Status = es.EventStusDes
                      }).ToList();

            return Events;
        }

        public List<FedlineManageUserEvent> GetDisconnectEvents()
        {
            List<FedlineManageUserEvent> Events;
            List<short> ignoredStatuses = new List<short>() { 6, 13, 14 };

            Events = (from td in _context.FedlineEventTadpoleData
                      join ud in _context.FedlineEventUserData on td.EventId equals ud.EventId
                      join es in _context.LkEventStus on ud.EventStusId equals es.EventStusId
                      join ot in _context.LkFedlineOrdrType on td.OrdrTypeCd equals ot.OrdrTypeCd
                      join lua in _context.LkUser on ud.ActvUserId equals lua.UserId into llua
                      from lua in llua.DefaultIfEmpty()
                      where td.RecStusId == 1
                        && ot.PrntOrdrTypeDes == "Disconnect"
                        && es.RecStusId == 1
                        && (!ignoredStatuses.Contains(ud.EventStusId) || (td.StrtTme.HasValue && td.StrtTme.Value.Date >= DateTime.Now.Date))
                      select new FedlineManageUserEvent
                      {
                          EventID = td.EventId,
                          FRBID = td.FrbReqId,
                          StartTime = td.StrtTme,
                          EndTime = td.EndTme,
                          Activity = ot.PrntOrdrTypeDes,
                          Status = es.EventStusDes,
                          Activator = lua.DsplNme
                      }).ToList();

            return Events;
        }

        public List<FedlineManageUserEvent> GetFedEventsForTS(int resourceID)
        {
            List<FedlineManageUserEvent> Events;

            var tss = (from e in _context.EventTypeRsrcAvlblty
                       join ts in _context.LkEventTypeTmeSlot on e.TmeSlotId equals ts.TmeSlotId
                       where e.EventTypeRsrcAvlbltyId == resourceID
                       select new { day = e.DayDt, startTime = ts.TmeSlotStrtTme, endTime = ts.TmeSlotEndTme }).FirstOrDefault();

            //Add 1 hour so that anything which starts at 2:59 PM would still be
            //covered by the 2-3PM time slot.  This is how the increment works
            //when the order is received, it looks only at hour and not minutes.
            //Make the start time be Less than, so you don't get anything that
            //starts on the next hour
            DateTime start = tss.day.AddHours(tss.startTime.Hours + 1);
            DateTime end = tss.day.AddHours(tss.endTime.Hours - 1);

            Events = (from td in _context.FedlineEventTadpoleData
                      join ud in _context.FedlineEventUserData on td.EventId equals ud.EventId
                      join es in _context.LkEventStus on ud.EventStusId equals es.EventStusId
                      join ot in _context.LkFedlineOrdrType on td.OrdrTypeCd equals ot.OrdrTypeCd
                      join lue in _context.LkUser on ud.EngrUserId equals lue.UserId into llue
                      from lue in llue.DefaultIfEmpty()
                      join lua in _context.LkUser on ud.ActvUserId equals lua.UserId into llua
                      from lua in llua.DefaultIfEmpty()
                      where td.RecStusId == 1
                           && ot.PrntOrdrTypeDes != "Disconnect"
                           && es.RecStusId == 1
                           && td.StrtTme < start
                           && td.EndTme > end
                           && (ud.EventStusId != 11 && ud.EventStusId != 12 && ud.EventStusId != 13)
                      select new FedlineManageUserEvent
                      {
                          EventID = td.EventId,
                          FRBID = td.FrbReqId,
                          StartTime = td.StrtTme,
                          EndTime = td.EndTme,
                          Activator = lua.DsplNme,
                          PreConfigEng = lue.DsplNme,
                          Activity = ot.PrntOrdrTypeDes,
                          Status = es.EventStusDes
                      }).ToList();

            return Events;
        }
    }
}