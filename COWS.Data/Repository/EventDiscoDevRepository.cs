﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventDiscoDevRepository : IEventDiscoDevRepository
    {
        private readonly COWSAdminDBContext _context;

        public EventDiscoDevRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public EventDiscoDev Create(EventDiscoDev entity)
        {
            throw new NotImplementedException();
        }

        public void Create(List<EventDiscoDev> entity)
        {
            _context.EventDiscoDev
                .AddRange(entity.Select(a =>
                {
                    a.EventDiscoDevId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<EventDiscoDev> Find(Expression<Func<EventDiscoDev, bool>> predicate)
        {
            return _context.EventDiscoDev
                            .Include(i => i.Event)
                            .Include(i => i.DevModel)
                            .Include(i => i.Manf)
                            .Where(predicate);
        }

        public IEnumerable<EventDiscoDev> GetAll()
        {
            return _context.EventDiscoDev
                            .Include(i => i.Event)
                            .Include(i => i.DevModel)
                            .Include(i => i.Manf)
                            .OrderBy(i => i.OdieDevNme)
                            .ToList();
        }

        public EventDiscoDev GetById(int id)
        {
            return _context.EventDiscoDev
                            .Include(i => i.Event)
                            .Include(i => i.DevModel)
                            .Include(i => i.Manf)
                            .SingleOrDefault(i => i.EventDiscoDevId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, EventDiscoDev entity)
        {
            throw new NotImplementedException();
        }

        public void SetInactive(int eventId)
        {
            _context.EventDiscoDev
                .Where(a => a.EventId == eventId && a.RecStusId == (byte)1)
                .ToList()
                .ForEach(b =>
                {
                    b.RecStusId = (byte)0;
                    b.ModfdDt = DateTime.Now;
                });
        }
    }
}