﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class TaskRepository : ITaskRepository
    {
        private readonly COWSAdminDBContext _context;

        public TaskRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<LkTask> Find(Expression<Func<LkTask, bool>> predicate)
        {
            return _context.LkTask
                             .Where(predicate)
                             .AsNoTracking();
        }

        public IEnumerable<LkTask> GetAll()
        {
            return Find(a => 1 == 1).ToList();
        }

        public LkTask GetById(int id)
        {
            return Find(a => a.TaskId == id).SingleOrDefault();
        }

        public LkTask Create(LkTask entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkTask entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public IQueryable<LkTask> GetCurrentStatus(Expression<Func<LkTask, bool>> predicate)
        {
            return _context.LkTask
                .Include(a => a.MapPrfTask)
                .Include(a => a.ActTask)
                    .ThenInclude(a => a.Ordr.OrdrStus)
                .Where(predicate);
        }

        public string GetCurrentStatus(int taskId, int wgId, int orderId)
        {
            if (taskId == 0)
            {
                var task = _context.ActTask
                                        .Include(i => i.Task)
                                        .Where(i => i.TaskId != 211 && i.TaskId != 217 && i.TaskId != 218
                                                    && i.OrdrId == orderId && i.StusId == 0
                                                    && i.WgProfId == wgId).FirstOrDefault();

                if (task != null)
                {
                    if (string.IsNullOrWhiteSpace(task.Task.TaskNme))
                    {
                        return GetCurrentStatus(taskId, wgId, 0);
                    }
                    else
                    {
                        return $"{task.Task.TaskNme} - Pending";
                    }
                }
                else return string.Empty;
            }
            else
            {
                if (taskId != 1001 && taskId != 1000)
                {
                    var task = _context.ActTask
                                        .Include(i => i.Task)
                                        .Where(i => i.TaskId == taskId &&
                                                    i.OrdrId == orderId 
                                                    && i.StusId == 0).FirstOrDefault();

                    if (task != null)
                    {
                        return $"{task.Task.TaskNme} - Pending";
                    }
                    else
                        return string.Empty;
                }
            }

            var systemTask = _context.ActTask
                                        .Include(i => i.Ordr.OrdrStus)
                                        .Include(i => i.Task)
                                        .Where(i => (i.TaskId == 1000 || i.TaskId == 1001)
                                                && i.OrdrId == orderId && i.StusId == 0);

            if (systemTask != null)
            {
                var currentTask = systemTask.FirstOrDefault();
                if (systemTask.Any(i => i.Ordr.OrdrStusId == 0 || i.Ordr.OrdrStusId == 1))
                {
                    return (currentTask != null) ? $"{currentTask.Task.TaskNme} - Pending" : string.Empty;
                }
                else
                {
                    return (currentTask != null) ? currentTask.Ordr.OrdrStus.OrdrStusDes : string.Empty;
                }
            }
            else
            {
                return null;
            }
        }
    }
}