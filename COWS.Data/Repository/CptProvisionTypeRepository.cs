﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptProvisionTypeRepository : ICptProvisionTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptProvisionTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptPrvsnType Create(LkCptPrvsnType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptPrvsnType> Find(Expression<Func<LkCptPrvsnType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPrvsnType, out List<LkCptPrvsnType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptPrvsnType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPrvsnType, out List<LkCptPrvsnType> list))
            {
                list = _context.LkCptPrvsnType
                            .OrderBy(i => i.CptPrvsnType)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptPrvsnType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptPrvsnType GetById(int id)
        {
            return _context.LkCptPrvsnType
                            .SingleOrDefault(i => i.CptPrvsnTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptPrvsnType);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptPrvsnType entity)
        {
            throw new NotImplementedException();
        }
    }
}