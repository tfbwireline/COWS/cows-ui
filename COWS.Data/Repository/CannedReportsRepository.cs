﻿using COWS.Data.Interface;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace COWS.Data.Repository
{
    public class CannedReportsRepository : ICannedReportsRepository
    {
        private readonly COWSReportingDbContext _context;

        public CannedReportsRepository(COWSReportingDbContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Report> GetCannedReportByUserId(string reportType, int userId, int CSGLevel)
        {
            var result = _context.Query<Report>()
                                .FromSql("[dbo].[getReportListByUserID] @ReportType, @UserID, @CSGLevel ",
                                    new SqlParameter("@ReportType", reportType),
                                    new SqlParameter("@UserID", userId),
                                    new SqlParameter("@CSGLevel", CSGLevel)
                                    ).ToList();
            return result;
        }

        public CannedFile GetCannedFilesByReportID(int reportId, int CSGLevel, string reportType)
        {
            var result = _context.Query<CannedFile>()
                                .FromSql("dbo.[getCannedFilesByReportID] @REPORTID, @CSGLEVEL, @ReportType",
                                new SqlParameter("@REPORTID", reportId),
                                    new SqlParameter("@CSGLEVEL", CSGLevel),
                                    new SqlParameter("@ReportType", reportType)
                                    ).FirstOrDefault();

            return result;
        }

        public CannedFile GetCannedFilesByReportID(int reportId, int CSGLevel, string reportType, DateTime startDate, DateTime endDate)
        {
            var result = _context.Query<CannedFile>()
                                .FromSql("dbo.[getCannedFilesByReportID] @REPORTID, @CSGLEVEL, @ReportType, @StartDt, @EndDt",
                                new SqlParameter("@REPORTID", reportId),
                                    new SqlParameter("@CSGLEVEL", CSGLevel),
                                    new SqlParameter("@ReportType", reportType),
                                    new SqlParameter("@StartDt", startDate),
                                    new SqlParameter("@EndDt", endDate)
                                    ).FirstOrDefault();

            return result;
        }

        public byte[] GetCannedReportFile(string filePath)
        {

            byte[] byteArray = File.ReadAllBytes(filePath);
            // Details : Encrypt | Decrypt file
            // Source : https://www.codeproject.com/Articles/26085/File-Encryption-and-Decryption-in-C
            if (filePath.Contains("Secured"))
            {
                PasswordDeriveBytes pdb = new PasswordDeriveBytes("password", new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                byte[] decryptedData = Decrypt(byteArray, pdb.GetBytes(32), pdb.GetBytes(16));
                return decryptedData;

            }
            return byteArray;
        }

        #region Decrypt

        public static byte[] Decrypt(byte[] cipherData, string Password)
        {
            // We need to turn the password into Key and IV. 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack - 
            // trying to guess a password by enumerating all possible words. 
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the Decryption using the 
            //function that accepts byte arrays. 
            // Using PasswordDeriveBytes object we are first getting
            // 32 bytes for the Key 
            // (the default Rijndael key length is 256bit = 32bytes)
            // and then 16 bytes for the IV. 
            // IV should always be the block size, which is by default
            // 16 bytes (128 bit) for Rijndael. 
            // If you are using DES/TripleDES/RC2 the block size is
            // 8 bytes and so should be the IV size. 

            // You can also read KeySize/BlockSize properties off the
            // algorithm to find out the sizes. 
            return Decrypt(cipherData, pdb.GetBytes(32), pdb.GetBytes(16));
        }

        // Decrypt a byte array into a byte array using a key and an IV 
        public static byte[] Decrypt(byte[] cipherData,
                                    byte[] Key, byte[] IV)
        {
            // Create a MemoryStream that is going to accept the
            // decrypted bytes 
            MemoryStream ms = new MemoryStream();

            // Create a symmetric algorithm. 
            // We are going to use Rijndael because it is strong and
            // available on all platforms. 
            // You can use other algorithms, to do so substitute the next
            // line with something like 
            //     TripleDES alg = TripleDES.Create(); 
            Rijndael alg = Rijndael.Create();

            // Now set the key and the IV. 
            // We need the IV (Initialization Vector) because the algorithm
            // is operating in its default 
            // mode called CBC (Cipher Block Chaining). The IV is XORed with
            // the first block (8 byte) 
            // of the data after it is decrypted, and then each decrypted
            // block is XORed with the previous 
            // cipher block. This is done to make encryption more secure. 
            // There is also a mode called ECB which does not need an IV,
            // but it is much less secure. 
            alg.Key = Key;
            alg.IV = IV;

            // Create a CryptoStream through which we are going to be
            // pumping our data. 
            // CryptoStreamMode.Write means that we are going to be
            // writing data to the stream 
            // and the output will be written in the MemoryStream
            // we have provided. 
            CryptoStream cs = new CryptoStream(ms,
                alg.CreateDecryptor(), CryptoStreamMode.Write);

            // Write the data and make it do the decryption 
            cs.Write(cipherData, 0, cipherData.Length);

            // Close the crypto stream (or do FlushFinalBlock). 
            // This will tell it that we have done our decryption
            // and there is no more data coming in, 
            // and it is now a good time to remove the padding
            // and finalize the decryption process. 
            cs.Close();

            // Now get the decrypted data from the MemoryStream. 
            // Some people make a mistake of using GetBuffer() here,
            // which is not the right way. 
            byte[] decryptedData = ms.ToArray();

            return decryptedData;
        }

        #endregion

    }
}
