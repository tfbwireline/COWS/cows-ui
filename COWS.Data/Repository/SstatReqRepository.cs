﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class SstatReqRepository : ISstatReqRepository
    {
        private readonly COWSAdminDBContext _context;

        public SstatReqRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<SstatReq> Find(Expression<Func<SstatReq, bool>> predicate)
        {
            return _context.SstatReq
                .Where(predicate);
        }

        public IEnumerable<SstatReq> GetAll()
        {
            throw new NotImplementedException();
        }

        public SstatReq GetById(int id)
        {
            return Find(a => a.TranId == id).SingleOrDefault();
        }

        public SstatReq Create(SstatReq entity)
        {
            _context.SstatReq.Add(entity);
            SaveAll();

            return GetById(entity.TranId);
        }

        public void Update(int id, SstatReq entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}