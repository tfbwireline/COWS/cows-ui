﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace COWS.Data.Repository
{
    public class EmailReqRepository : IEmailReqRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IUserRepository _userRepository;
        private readonly IEventAsnToUserRepository _eventAsnToUserRepository;
        private readonly IEventRuleDataRepository _eventRuleDataRepository;
        private readonly IConfiguration _configuration;
        private readonly ISIPTEventRepository _SIPTRepository;
        private readonly IMDSFastTrackTypeRepository _MDSFastTrackType;
        private readonly IUcaasEventRepository _ucaasEventRepo;
        private readonly IWorkflowRepository _workflowRepo;
        private readonly IRedesignRepository _redesignRepo;
        private readonly IEventStatusRepository _eventStatusRepo;
        private readonly ISstatReqRepository _sstatReqRepo;
        private readonly IContactDetailRepository _contactDetailRepo;
        private readonly ILogger<EmailReqRepository> _logger;
        private StringBuilder sbOldActEmail = new StringBuilder();
        private StringBuilder sbTeamPDL = new StringBuilder();
        private List<int> contactDetailUserIds = new List<int>();

        public EmailReqRepository(COWSAdminDBContext context,
            IUserRepository userRepository,
            IEventAsnToUserRepository eventAsnToUserRepository,
            IEventRuleDataRepository eventRuleDataRepository,
            ISIPTEventRepository SIPTRepository,
            IMDSFastTrackTypeRepository MDSFastTrackType,
            IUcaasEventRepository ucaasEventRepo,
            IWorkflowRepository workflowRepo,
            IRedesignRepository redesignRepo,
            IEventStatusRepository eventStatusRepo,
            ISstatReqRepository sstatReqRepo,
            IContactDetailRepository contactDetailRepo,
            ILogger<EmailReqRepository> logger,
            IConfiguration configuration)
        {
            _context = context;
            _userRepository = userRepository;
            _eventAsnToUserRepository = eventAsnToUserRepository;
            _eventRuleDataRepository = eventRuleDataRepository;
            _SIPTRepository = SIPTRepository;
            _configuration = configuration;
            _MDSFastTrackType = MDSFastTrackType;
            _ucaasEventRepo = ucaasEventRepo;
            _redesignRepo = redesignRepo;
            _workflowRepo = workflowRepo;
            _eventStatusRepo = eventStatusRepo;
            _sstatReqRepo = sstatReqRepo;
            _contactDetailRepo = contactDetailRepo;
            _logger = logger;
        }

        public EmailReq Create(EmailReq entity)
        {
            _context.EmailReq.Add(entity);

            SaveAll();
            int maxId = _context.EmailReq.Max(i => i.EmailReqId);
            return GetById(maxId);
        }

        public void Delete(int id)
        {
            EmailReq st = GetById(id);
            _context.EmailReq.Remove(st);

            SaveAll();
        }

        public void DeleteRequests(List<EmailReq> emails)
        {
            _context.EmailReq.RemoveRange(emails);

            SaveAll();
        }

        public IQueryable<EmailReq> Find(Expression<Func<EmailReq, bool>> predicate)
        {
            return _context.EmailReq
                            .Where(predicate);
        }

        public IEnumerable<EmailReq> GetAll()
        {
            return _context.EmailReq
                            .OrderBy(i => i.EmailReqId)
                            .ToList();
        }

        public EmailReq GetById(int id)
        {
            return _context.EmailReq
                            .SingleOrDefault(i => i.EmailReqId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, EmailReq entity)
        {
            EmailReq App = GetById(id);
            if (App == null) App = GetByOrderId(id);
            App.OrdrId = entity.OrdrId == null ? App.OrdrId : entity.OrdrId;
            App.EmailReqTypeId = entity.EmailReqTypeId == 0 ? App.EmailReqTypeId : entity.EmailReqTypeId;
            App.StusId = entity.StusId == 0 ? App.StusId : entity.StusId;
            App.EventId = entity.EventId == null ? App.EventId : entity.EventId;
            App.EmailListTxt = entity.EmailListTxt == null ? App.EmailListTxt : entity.EmailListTxt;
            App.EmailCcTxt = entity.EmailCcTxt == null ? App.EmailCcTxt : entity.EmailCcTxt;
            App.EmailSubjTxt = entity.EmailSubjTxt == null ? App.EmailSubjTxt : entity.EmailSubjTxt;
            App.EmailBodyTxt = entity.EmailBodyTxt == null ? App.EmailBodyTxt : entity.EmailBodyTxt;
            App.SentDt = entity.SentDt == null ? App.SentDt : entity.SentDt;
            App.RedsgnId = entity.RedsgnId == null ? App.RedsgnId : entity.RedsgnId;
            App.CptId = entity.CptId == null ? App.CptId : entity.CptId;

            SaveAll();
        }

        private EmailReq GetByOrderId(int id)
        {
            return _context.EmailReq
                            .SingleOrDefault(i => i.OrdrId == id);
        }

        // Send email for CAND Events (AD, MPLS, SPLK, NGVN)
        public bool SendMail(EventWorkflow esw)
        {
            bool bActChange = false;
            bool bTimeChange = false;
            StringBuilder sbEmailAddr = new StringBuilder();
            StringBuilder sbCalendarURL = new StringBuilder();
            string CalenderEntryURL = _configuration.GetSection("AppSettings:CalenderEntryURL").Value;
            bool bEmailFlg = false;

            StringBuilder sbActUserName = new StringBuilder();
            if (esw.AssignUser != null && esw.AssignUser.Count > 0)
            {
                foreach (var au in esw.AssignUser)
                {
                    sbActUserName.AppendNoDup(string.IsNullOrWhiteSpace(_userRepository.GetById(au.AsnToUserId).FullNme)
                        ? string.Empty : (_userRepository.GetById(au.AsnToUserId).FullNme + "; "));
                }

                // Removing the trailing "; "
                if (sbActUserName.Length > 0)
                    sbActUserName.Remove(sbActUserName.Length - 2, 2);
            }

            bActChange = _eventAsnToUserRepository.AccountChange(esw.AssignUser, esw.EventId);

            if (esw.StartTime != esw.OldStartTime || esw.EndTime != esw.OldEndTime)
            {
                bTimeChange = true;
            }

            EventRuleData RuleData = _eventRuleDataRepository.GetById(esw.EventRule.EventRuleId);

            /*
             *  a.	If all the email related data is blank/0 in the xml rule,
             *      then you don’t have to enter a row in this table.
                    And can handle in code to default them as blank/0 for those elements.
                b.	[EVENT_DATA_CD] used only for MDS/UCaaS events. Values will be MDS, CPE, FT, RET
                    like found in the subsections of MDSRules/UCaasRules.xml.
                c.	[EMAILMEM_CD] is one field to house all these three elements:
                    <EmailMem><EmailMemTo><EmailMemCC>. Same case with [EMAILREV_CD] and [EMAILACT_CD].
                    For values 0 in the xml rule, leave the column values as NULL,
                        if EmailMem/EmailMemTo is 1 then column value will be 1;
                        if EmailMem/EmailMemCC is 1 the column value will be 2.
                    If you notice, I haven’t created a column for <Email> element
                    because it can derived as 1 if there is 1 or 2 in atleast one of
                    [EMAILMEM_CD]/[EMAILREV_CD]/ [EMAILACT_CD] columns.
                d.	[EMAILPUBCC_CD], [EMAILCMPLCC_CD], [EMAILOLDACT_CD], [EMAIL_SUBJ], [EMAILCC_ADR]
                    are directly mapped to <EmailPubCC>, <EmailCmplCC>, <EmailOldAct>, <EmailSubj>,
                    <CCAddr> elements.
            */
            if ((esw.EventStatusId == (byte)EventStatus.Published
                    && esw.EventRule.EndEventStusId == (byte)WorkflowStatus.Publish
                    && (bActChange || bTimeChange))
                || esw.EventStatusId != (byte)EventStatus.Published
                || esw.EventRule.EndEventStusId == (byte)WorkflowStatus.Publish)
            {
                if (RuleData != null)
                {
                    if (RuleData.EmailmemCd != null && RuleData.EmailmemCd != 0)
                    {
                        List<int> UserIDs = new List<int>();
                        sbCalendarURL.AppendNoDup(CalenderEntryURL);

                        UserIDs.Add(esw.UserId); // ModifiedByUserId

                        if (RuleData.EmailmemCd == 1)
                            UserIDs.Add(esw.RequestorId);

                        if (RuleData.EmailrevCd == 1 && esw.ReviewerId > 0)
                            UserIDs.Add(esw.UserId);

                        if (esw.AssignUser != null && esw.AssignUser.Count > 0)
                        {
                            foreach (var es in esw.AssignUser)
                            {
                                if (RuleData.EmailactCd == 1)
                                {
                                    if (sbEmailAddr.Length > 0 && sbEmailAddr[sbEmailAddr.Length - 1] != ',')
                                        sbEmailAddr.Append(",");

                                    sbEmailAddr.AppendNoDup(_userRepository.GetById(es.AsnToUserId).EmailAdr);
                                }
                            }
                        }

                        DataTable dtEmailAddr = GetEmailAddr(UserIDs);
                        if (dtEmailAddr != null && dtEmailAddr.Rows != null && dtEmailAddr.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                            {
                                if (sbEmailAddr.Length > 0 && sbEmailAddr[sbEmailAddr.Length - 1] != ',')
                                    sbEmailAddr.Append(",");

                                sbEmailAddr.AppendNoDup(dtEmailAddr.Rows[i]["EmailAddr"].ToString());
                            }
                        }

                        DataTable dtCurrentAct = GetCurrentAct(esw.AssignUser, esw.EventId);
                        if (dtCurrentAct != null && dtCurrentAct.Rows != null && dtCurrentAct.Rows.Count > 0)
                        {
                            foreach (var dr in dtCurrentAct.AsEnumerable())
                            {
                                if (RuleData.EmailoldactCd == true)
                                {
                                    if (sbEmailAddr.Length > 0 && sbEmailAddr[sbEmailAddr.Length - 1] != ',')
                                        sbEmailAddr.Append(",");

                                    sbEmailAddr.AppendNoDup(dr["EMAIL_ADR"].ToString());
                                }
                            }
                        }

                        if (RuleData.EmailpubccCd.GetValueOrDefault(false))
                            if (!string.IsNullOrWhiteSpace(esw.PubEmailCcTxt))
                            {
                                if (ValidateEmail(esw.PubEmailCcTxt))
                                {
                                    if (sbEmailAddr.Length > 0 && sbEmailAddr[sbEmailAddr.Length - 1] != ',')
                                        sbEmailAddr.Append(",");

                                    if (esw.PubEmailCcTxt.Contains(";"))
                                        sbEmailAddr.AppendNoDup(esw.PubEmailCcTxt.Trim().Replace(";", ","));
                                    else
                                        sbEmailAddr.AppendNoDup(esw.PubEmailCcTxt.Trim());
                                }
                            }

                        if (RuleData.EmailcmplccCd.GetValueOrDefault(false))
                            if (!string.IsNullOrWhiteSpace(esw.CmpltdEmailCcTxt))
                            {
                                if (ValidateEmail(esw.CmpltdEmailCcTxt))
                                {
                                    if (sbEmailAddr.Length > 0 && sbEmailAddr[sbEmailAddr.Length - 1] != ',')
                                        sbEmailAddr.Append(",");

                                    if (esw.CmpltdEmailCcTxt.ToString().Contains(";"))
                                        sbEmailAddr.AppendNoDup(esw.CmpltdEmailCcTxt.Trim().Replace(";", ","));
                                    else
                                        sbEmailAddr.AppendNoDup(esw.CmpltdEmailCcTxt.Trim());
                                }
                            }

                        string sBody = string.Empty;
                        DataTable dtCurrentEHist = GetCurrentEHist(esw.EventId);
                        DataTable dtCurrentCalData = GetCurrentCalData(esw.EventId);
                        string sTo = sbEmailAddr.Replace(";", ",").ToString();
                        string sCC = string.Empty;
                        // for MDS and UCAAS  SupressEmail has an Option in UI?
                        bool bSuppressEmails = false;

                        // General Mail
                        if (!(esw.WorkflowId == (int)WorkflowStatus.Retract
                            && esw.EventStatusId != (int)EventStatus.Visible
                            && (dtCurrentEHist.Select("ActnId = " + (int)Actions.Publish).Length > 0)
                            && dtCurrentCalData.Rows.Count > 0))
                        {
                            XElement email = new XElement("EventMail",
                                            new XElement("EventID", esw.EventId.ToString()),
                                            new XElement("SOWSEventID", esw.SOWSEventID),
                                            new XElement("EventType", esw.EventType),
                                            new XElement("EventTitle", esw.EventTitle),
                                            new XElement("StartEStatusDes", esw.EventRule.StrtEventStus.EventStusDes),
                                            new XElement("CommandDes", esw.EventRule.WrkflwStus.WrkflwStusDes),
                                            new XElement("EndEStatusDes", esw.EventRule.EndEventStus.EventStusDes),
                                            new XElement("AssignAct", sbActUserName),
                                            new XElement("CalenderEntryURL", sbCalendarURL.ToString()),
                                            new XElement("ModUserName", _userRepository.GetById(esw.UserId).FullNme),
                                            new XElement("ViewEventURL", GetViewEventURL((byte)esw.EventTypeId)),
                                            new XElement("StartDtTm", esw.StartTime),
                                            new XElement("Comments", esw.Comments));

                            // Send EmailReq
                            if (email.ToString().Length > 0 && sbEmailAddr.ToString().Length > 0)
                            {
                                EmailReq Req = new EmailReq
                                {
                                    EventId = esw.EventId,
                                    EmailReqTypeId = GetEmailRequestType(esw.EventTypeId, false),
                                    EmailListTxt = string.IsNullOrWhiteSpace(sTo) ? string.Empty : sTo.Replace(",$", ""),
                                    EmailCcTxt = string.IsNullOrWhiteSpace(sCC) ? string.Empty : sCC.Replace(",$", ""),
                                    StusId = bSuppressEmails ? (byte)EmailStatus.OnHold : (byte)EmailStatus.Pending,
                                    CreatDt = DateTime.Now,
                                    EmailBodyTxt = email.ToString(),
                                    EmailSubjTxt = GetEmailSubj(string.Empty, esw.EventRule, esw.EventTitle, esw.EventId, string.Empty, RuleData.EmailSubj)
                                };

                                Create(Req);
                                bEmailFlg = true;
                            }
                        }

                        if (esw.WorkflowId == (int)WorkflowStatus.Retract
                            && esw.EventStatusId != (int)EventStatus.Visible
                            && (dtCurrentEHist.Select("ActnId = " + (int)Actions.Publish).Length > 0)
                            && dtCurrentCalData.Rows.Count > 0)
                        {
                            XElement RetractEmail = new XElement("EventMail",
                                                    new XElement("EventID", esw.EventId),
                                                    new XElement("EventType", esw.EventType),
                                                    new XElement("EventTitle", esw.EventTitle),
                                                    new XElement("ModUserName", _userRepository.GetById(esw.UserId).FullNme),
                                                    new XElement("CalenderEntryURL", sbCalendarURL.ToString()),
                                                    new XElement("ViewEventURL", GetViewEventURL((byte)esw.EventTypeId)));
                            if (RetractEmail.ToString().Length > 0 && sbEmailAddr.ToString().Length > 0)
                            {
                                EmailReq Req = new EmailReq
                                {
                                    EventId = esw.EventId,
                                    EmailReqTypeId = GetEmailRequestType(esw.EventTypeId, true),
                                    EmailListTxt = string.IsNullOrWhiteSpace(sTo) ? string.Empty : sTo.Replace(",$", ""),
                                    EmailCcTxt = string.IsNullOrWhiteSpace(sCC) ? string.Empty : sCC.Replace(",$", ""),
                                    StusId = bSuppressEmails ? (byte)EmailStatus.OnHold : (byte)EmailStatus.Pending,
                                    CreatDt = DateTime.Now,
                                    EmailBodyTxt = RetractEmail.ToString(),
                                    EmailSubjTxt = GetEmailSubj(string.Empty, esw.EventRule, esw.EventTitle, esw.EventId, string.Empty, RuleData.EmailSubj)
                                };

                                Create(Req);
                                bEmailFlg = true;
                            }
                        }
                    }
                }
            }

            return bEmailFlg;
        }

        // Send email for SIPT Event
        public bool SIPTSendMail(EventWorkflow esw)
        {
            bool bActChange = false;
            bool bTimeChange = false;
            StringBuilder sbEmailAddr = new StringBuilder();
            StringBuilder sbCalendarURL = new StringBuilder();
            string CalenderEntryURL = _configuration.GetSection("AppSettings:CalenderEntryURL").Value;
            string EventURL = string.Format("{0}/{1}", GetViewEventURL((byte)esw.EventTypeId), esw.EventId);
            string DocURL = _configuration.GetSection("AppSettings:DocURL").Value;
            bool bEmailFlg = false;

            StringBuilder sbActUserName = new StringBuilder();
            if (esw.AssignUser != null && esw.AssignUser.Count > 0)
            {
                foreach (var au in esw.AssignUser)
                {
                    sbActUserName.AppendNoDup(string.IsNullOrWhiteSpace(_userRepository.GetById(au.AsnToUserId).FullNme)
                        ? string.Empty : (_userRepository.GetById(au.AsnToUserId).FullNme + "; "));
                }

                // Removing the trailing "; "
                if (sbActUserName.Length > 0)
                    sbActUserName.Remove(sbActUserName.Length - 2, 2);
            }

            bActChange = _eventAsnToUserRepository.AccountChange(esw.AssignUser, esw.EventId);

            if (esw.StartTime != esw.OldStartTime || esw.EndTime != esw.OldEndTime)
            {
                bTimeChange = true;
            }

            EventRuleData RuleData = _eventRuleDataRepository.GetById(esw.EventRule.EventRuleId);
            //var RuleData = esw.EventRule;

            /*
             *  a.	If all the email related data is blank/0 in the xml rule,
             *      then you don’t have to enter a row in this table.
                    And can handle in code to default them as blank/0 for those elements.
                b.	[EVENT_DATA_CD] used only for MDS/UCaaS events. Values will be MDS, CPE, FT, RET
                    like found in the subsections of MDSRules/UCaasRules.xml.
                c.	[EMAILMEM_CD] is one field to house all these three elements:
                    <EmailMem><EmailMemTo><EmailMemCC>. Same case with [EMAILREV_CD] and [EMAILACT_CD].
                    For values 0 in the xml rule, leave the column values as NULL,
                        if EmailMem/EmailMemTo is 1 then column value will be 1;
                        if EmailMem/EmailMemCC is 1 the column value will be 2.
                    If you notice, I haven’t created a column for <Email> element
                    because it can derived as 1 if there is 1 or 2 in atleast one of
                    [EMAILMEM_CD]/[EMAILREV_CD]/ [EMAILACT_CD] columns.
                d.	[EMAILPUBCC_CD], [EMAILCMPLCC_CD], [EMAILOLDACT_CD], [EMAIL_SUBJ], [EMAILCC_ADR]
                    are directly mapped to <EmailPubCC>, <EmailCmplCC>, <EmailOldAct>, <EmailSubj>,
                    <CCAddr> elements.
            */
            //List<EventRuleData> RuleData = _eventRuleDataRepository.GetByRuleId(esw.EventRule.EventRuleId);
            //foreach (var dtRule in RuleData)
            //{
            if (esw.EventRule != null)
            {
                if (_workflowRepo.UpdateEWStatus(esw))
                {
                    if ((esw.EventStatusId == (byte)EventStatus.Published
                        && esw.EventRule.EndEventStusId == (byte)WorkflowStatus.Publish
                        && (bActChange || bTimeChange))
                    || esw.EventStatusId != (byte)EventStatus.Published
                    || esw.EventRule.EndEventStusId == (byte)WorkflowStatus.Publish)
                    {
                        if (RuleData != null)
                        {
                            if (RuleData.EmailmemCd != null && RuleData.EmailmemCd != 0)
                            {
                                List<int> UserIDs = new List<int>();
                                sbCalendarURL.AppendNoDup(CalenderEntryURL);

                                UserIDs.Add(esw.UserId); // ModifiedUserId

                                if (RuleData.EmailmemCd == 1)
                                    UserIDs.Add(esw.RequestorId);

                                if (RuleData.EmailrevCd == 1 && esw.UserId > 0)
                                    UserIDs.Add(esw.UserId);

                                if (esw.AssignUser != null || esw.AssignUser.Count > 0)
                                {
                                    foreach (var es in esw.AssignUser)
                                    {
                                        if (RuleData.EmailactCd == 1)
                                        {
                                            if (sbEmailAddr.Length > 0 && sbEmailAddr[sbEmailAddr.Length - 1] != ',')
                                                sbEmailAddr.Append(",");

                                            sbEmailAddr.AppendNoDup(_userRepository.GetById(es.AsnToUserId).EmailAdr);
                                        }
                                    }
                                }

                                DataTable dtEmailAddr = GetEmailAddr(UserIDs);
                                if (dtEmailAddr != null)
                                {
                                    if (dtEmailAddr.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                                        {
                                            if ((sbEmailAddr.Length > 0) && (sbEmailAddr[sbEmailAddr.Length - 1] != ','))
                                                sbEmailAddr.Append(",");
                                            sbEmailAddr.AppendNoDup(dtEmailAddr.Rows[i]["EmailAddr"].ToString());
                                        }
                                    }
                                }

                                DataTable dtCurrentAct = GetCurrentAct(esw.AssignUser, esw.EventId);
                                if ((dtCurrentAct != null) && (dtCurrentAct.Rows.Count > 0))
                                {
                                    foreach (var dr in dtCurrentAct.AsEnumerable())
                                    {
                                        if (RuleData.EmailoldactCd == true)
                                        {
                                            if ((sbEmailAddr.Length > 0) && (sbEmailAddr[sbEmailAddr.Length - 1] != ','))
                                                sbEmailAddr.Append(",");
                                            sbEmailAddr.AppendNoDup(dr["EmailAdr"].ToString());
                                        }
                                    }
                                }

                                if (esw.TeamPdlNme.Trim().Length > 0)
                                {
                                    if ((sbEmailAddr.Length > 0) && (sbEmailAddr[sbEmailAddr.Length - 1] != ','))
                                        sbEmailAddr.Append(",");
                                    sbEmailAddr.AppendNoDup(esw.TeamPdlNme.Trim());
                                }

                                string sBody = string.Empty;
                                DataTable dtCurrentEHist = GetCurrentEHist(esw.EventId);
                                DataTable dtCurrentCalData = GetCurrentCalData(esw.EventId);
                                string sTo = sbEmailAddr.Replace(";", ",").ToString();
                                string sCC = string.Empty;
                                bool bSuppressEmails = false;  //  for MDS and UCAAS  SupressEmail has an Option in UI?
                                string sAcctType = string.Empty;
                                string sTollType = string.Empty;
                                string sRelOrdr = string.Empty;
                                if (esw.SiptReltdOrdrNos != null && esw.SiptReltdOrdrNos.Count() > 0)
                                {
                                    foreach (var item in esw.SiptReltdOrdrNos)
                                        sRelOrdr = sRelOrdr + item + ",";
                                }
                                if (esw.SiptEventActyIds != null && esw.SiptEventActyIds.Count() > 0)
                                {
                                    foreach (var item in esw.SiptEventActyIds)
                                        sAcctType = sAcctType + _SIPTRepository.GetSIPTActyTypeByID(item).SiptActyTypeDes + ",";
                                }
                                if (esw.SiptEventTollTypeIds != null && esw.SiptEventTollTypeIds.Count() > 0)
                                {
                                    foreach (var item in esw.SiptEventTollTypeIds)
                                        sTollType = sTollType + _SIPTRepository.GetSIPTTollTypeByID(item).SiptTollTypeDes + ",";
                                }

                                bool isSecured = esw.CsgLvlId > 0;
                                esw.EventTitle = isSecured ? "Private Customer" : esw.EventTitle;

                                // General Mail
                                XElement siptEmail = new XElement("EventEmail");
                                if (!((esw.WorkflowId == ((int)WorkflowStatus.Retract))
                                             && (esw.EventStatusId != ((int)EventStatus.Visible))
                                             && (dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0)
                                             && (dtCurrentCalData.Rows.Count > 0)))
                                {
                                    XElement email = new XElement("SIPTEvent",
                                                    new XElement("EventID", esw.EventId.ToString()),
                                                    new XElement("EventType", esw.EventType.ToString()),
                                                    new XElement("ProdType", esw.SiptProdTypeId.ToString()),
                                                    new XElement("EventTitle", esw.EventTitle),
                                                    new XElement("StartEStatusDes", esw.EventRule.StrtEventStus.EventStusDes),
                                                    new XElement("CommandDes", esw.EventRule.WrkflwStus.WrkflwStusDes),  //CommandDes
                                                    new XElement("EndEStatusDes", esw.EventRule.EndEventStus.EventStusDes),
                                                    new XElement("AssignAct", sbActUserName),
                                                    new XElement("CalenderEntryURL", sbCalendarURL.ToString()),
                                                    new XElement("ModUserName", _userRepository.GetById(esw.UserId).FullNme),
                                                    new XElement("ViewEventURL", EventURL),
                                                    new XElement("Comments", esw.Comments),
                                                    new XElement("ActyType", (esw.SiptEventActyIds != null && esw.SiptEventActyIds.Count > 0) ? sAcctType : string.Empty),
                                                    new XElement("TollType", (esw.SiptEventTollTypeIds != null && esw.SiptEventTollTypeIds.Count > 0) ? sTollType : string.Empty),
                                                    new XElement("Mach5OrdrNbr", esw.M5OrdrNbr),
                                                    new XElement("RelM5Ordrs", (esw.SiptReltdOrdrNos != null && esw.SiptReltdOrdrNos.Count > 0) ? sRelOrdr : string.Empty),
                                                    new XElement("H1", esw.H1),
                                                    new XElement("H6", esw.H6),
                                                    new XElement("SiteID", esw.SiteId),
                                                    new XElement("SiteAdr", isSecured ? CustomerMaskedData.address : esw.SiteAdr),
                                                    new XElement("Floor", isSecured ? CustomerMaskedData.floor : esw.FlrBldgNme),
                                                    new XElement("City", isSecured ? CustomerMaskedData.city : esw.CtyNme),
                                                    new XElement("StProv", isSecured ? CustomerMaskedData.state : esw.SttPrvnNme),
                                                    new XElement("Ctry", isSecured ? CustomerMaskedData.country : esw.CtryRgnNme),
                                                    new XElement("ZipCode", isSecured ? CustomerMaskedData.zipCode : esw.ZipCd),
                                                    new XElement("USIntlCd", esw.UsIntlCd.Equals("D") ? "US" : "International"),
                                                    new XElement("SiteCntctNme", isSecured ? CustomerMaskedData.installSitePOC : esw.SiteCntctNme),
                                                    new XElement("SiteCntctNbr", isSecured ? CustomerMaskedData.installSitePOCPhone : esw.SiteCntctPhnNbr),
                                                    new XElement("SiteCntctHrs", isSecured ? CustomerMaskedData.serviceAssurancePOCContactHrs : esw.SiteCntctHrNme),
                                                    new XElement("TeamPDL", esw.TeamPdlNme),
                                                    new XElement("CustReqStDt", esw.StartTime.ToString()),
                                                    new XElement("CustReqEndDt", esw.EndTime.ToString()),
                                                    new XElement("DesignDoc", esw.SiptDesgnDoc),
                                                    new XElement("DocURL", DocURL));

                                    // Send EmailReq
                                    if ((email.ToString().Length > 0) && (sbEmailAddr.ToString().Length > 0))
                                    {
                                        siptEmail.Add(email);

                                        // Updated by Sarah Sandoval [20200728] - Added SIPTDocs
                                        var docs = _context.SiptEventDoc.Where(i => i.EventId == esw.EventId
                                            && i.RecStusId == (byte)ERecStatus.Active).OrderBy(i => i.FileNme);
                                        if (docs != null && docs.Count() > 0)
                                        {
                                            foreach (SiptEventDoc item in docs)
                                            {
                                                XElement _xmdoc = new XElement("SIPTDoc",
                                                              new XElement("DocID", item.SiptEventDocId),
                                                              new XElement("DocName", item.FileNme),
                                                              new XElement("DocSize", string.Format("{0:N0} KB", item.FileSizeQty / 1000))
                                                              );
                                                siptEmail.Add(_xmdoc);
                                            }
                                        }

                                        EmailReq Req = new EmailReq
                                        {
                                            EventId = esw.EventId,
                                            EmailReqTypeId = (int)EmailREQType.SIPTEventEmail,
                                            EmailListTxt = (sTo.Length > 0) ? ((sTo[sTo.Length - 1] == ',') ? sTo.Substring(0, sTo.Length - 1) : sTo) : string.Empty,
                                            EmailCcTxt = (sCC.Length > 0) ? ((sCC[sCC.Length - 1] == ',') ? sCC.Substring(0, sCC.Length - 1) : sCC) : string.Empty,
                                            StusId = (bSuppressEmails) ? (byte)EmailStatus.OnHold : (byte)EmailStatus.Pending,
                                            CreatDt = DateTime.Now,
                                            EmailBodyTxt = siptEmail.ToString(),
                                            EmailSubjTxt = GetEmailSubj(string.Empty, esw.EventRule, esw.EventTitle, esw.EventId, string.Empty, RuleData.EmailSubj)
                                        };

                                        Create(Req);
                                        bEmailFlg = true;
                                    }
                                }

                                if (((esw.WorkflowId == ((int)WorkflowStatus.Retract))
                                             && (esw.EventStatusId != ((int)EventStatus.Visible))
                                             && (dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0)
                                             && (dtCurrentCalData.Rows.Count > 0)))
                                {
                                    XElement RetractEmail = new XElement("SIPTEvent",
                                                            new XElement("EventID", esw.EventId),
                                                            new XElement("EventType", esw.EventType.ToString()),
                                                            new XElement("EventTitle", esw.EventTitle),
                                                            new XElement("ModUserName", _userRepository.GetById(esw.UserId).FullNme.Trim()),
                                                            new XElement("CalenderEntryURL", sbCalendarURL.ToString()),
                                                            new XElement("ViewEventURL", EventURL));

                                    if ((RetractEmail.ToString().Length > 0) && (sbEmailAddr.ToString().Length > 0))
                                    {
                                        siptEmail.Add(RetractEmail);

                                        EmailReq Req = new EmailReq
                                        {
                                            EventId = esw.EventId,
                                            EmailReqTypeId = (int)EmailREQType.SIPTRetractEmail,
                                            EmailListTxt = (sTo.Length > 0) ? ((sTo[sTo.Length - 1] == ',') ? sTo.Substring(0, sTo.Length - 1) : sTo) : string.Empty,
                                            EmailCcTxt = (sCC.Length > 0) ? ((sCC[sCC.Length - 1] == ',') ? sCC.Substring(0, sCC.Length - 1) : sCC) : string.Empty,
                                            StusId = (bSuppressEmails) ? (byte)EmailStatus.OnHold : (byte)EmailStatus.Pending,
                                            CreatDt = DateTime.Now,
                                            EmailBodyTxt = siptEmail.ToString(),
                                            EmailSubjTxt = GetEmailSubj(string.Empty, esw.EventRule, esw.EventTitle, esw.EventId, string.Empty, RuleData.EmailSubj)
                                        };

                                        Create(Req);
                                        bEmailFlg = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            

            return bEmailFlg;
        }

        // Send email for UCaaS Event
        public bool UcaasSendMail(EventWorkflow esw)
        {
            bool bActChange = false;
            bool bTimeChange = false;
            StringBuilder sbEmailAddr = new StringBuilder();
            StringBuilder sbEmailCCAddr = new StringBuilder();
            StringBuilder sbCalendarURL = new StringBuilder();
            StringBuilder sbCommonEmailCCAddr = new StringBuilder();
            //StringBuilder sbOldActEmail = new StringBuilder();
            //StringBuilder sbTeamPDL = new StringBuilder();
            string CalenderEntryURL = _configuration.GetSection("AppSettings:CalenderEntryURL").Value;
            bool bEmailFlg = false;
            string sBody = string.Empty;
            StringBuilder sbActUserName = new StringBuilder();
            DataTable dtCurrentEHist = GetCurrentEHist(esw.EventId);
            DataTable dtCurrentCalData = GetCurrentCalData(esw.EventId);
            //byte EmailReqType = (byte)EmailREQType.UCaaSEventEmail;
            List<int> UserIDs = new List<int>();
            //esw.FailReasId = esw.FailReasId < 0 ? esw.FailReasId = 0 : esw.FailReasId;
            bool isSecured = esw.CsgLvlId > 0;
            esw.EventTitle = isSecured ? "Private Customer" : esw.EventTitle;

            if (esw.AssignUser != null && esw.AssignUser.Count > 0)
            {
                foreach (var au in esw.AssignUser)
                {
                    sbActUserName.AppendNoDup(string.IsNullOrWhiteSpace(_userRepository.GetById(au.AsnToUserId).FullNme)
                        ? string.Empty : (_userRepository.GetById(au.AsnToUserId).FullNme + "; "));
                }

                // Removing the trailing "; "
                if (sbActUserName.Length > 0)
                    sbActUserName.Remove(sbActUserName.Length - 2, 2);
            }

            bActChange = _eventAsnToUserRepository.AccountChange(esw.AssignUser, esw.EventId);
            bTimeChange = (esw.StartTime != esw.OldStartTime || esw.EndTime != esw.OldEndTime) ? true : false;

            /*
             *  a.	If all the email related data is blank/0 in the xml rule,
             *      then you don’t have to enter a row in this table.
                    And can handle in code to default them as blank/0 for those elements.
                b.	[EVENT_DATA_CD] used only for MDS/UCaaS events. Values will be MDS, CPE, FT, RET
                    like found in the subsections of MDSRules/UCaasRules.xml.
                c.	[EMAILMEM_CD] is one field to house all these three elements:
                    <EmailMem><EmailMemTo><EmailMemCC>. Same case with [EMAILREV_CD] and [EMAILACT_CD].
                    For values 0 in the xml rule, leave the column values as NULL,
                        if EmailMem/EmailMemTo is 1 then column value will be 1;
                        if EmailMem/EmailMemCC is 1 the column value will be 2.
                    If you notice, I haven’t created a column for <Email> element
                    because it can derived as 1 if there is 1 or 2 in atleast one of
                    [EMAILMEM_CD]/[EMAILREV_CD]/ [EMAILACT_CD] columns.
                d.	[EMAILPUBCC_CD], [EMAILCMPLCC_CD], [EMAILOLDACT_CD], [EMAIL_SUBJ], [EMAILCC_ADR]
                    are directly mapped to <EmailPubCC>, <EmailCmplCC>, <EmailOldAct>, <EmailSubj>,
                    <CCAddr> elements.
            */

            XElement XmlBody = null;
            XmlBody = GenerateUCaaSBody(esw, CalenderEntryURL, sbOldActEmail);

            UcaaSEvent ue = _ucaasEventRepo.GetById(esw.EventId);

            try
            {
                List<EventRuleData> RuleData = _eventRuleDataRepository.GetByRuleId((int)esw.EventRule.EventRuleId);
                if (RuleData != null && RuleData.Count > 0)
                {
                    if (_workflowRepo.UpdateEWStatus(esw))
                    {
                        string sSubj = string.Empty;
                        sbTeamPDL.Clear();
                    if (ue.CustAcctTeamPdlNme.Trim().Length > 0)
                    {
                        if (sbTeamPDL.Length > 0 && sbTeamPDL[sbTeamPDL.Length - 1] != ',')
                            sbTeamPDL.Append(",");
                        sbTeamPDL.AppendNoDup(ue.CustAcctTeamPdlNme.Trim());
                    }

                    if (esw.WorkflowId == (int)WorkflowStatus.Publish
                        || esw.WorkflowId == (int)WorkflowStatus.InProgress
                        || esw.WorkflowId == (int)WorkflowStatus.InprogressUAT
                        || esw.WorkflowId == (int)WorkflowStatus.Complete
                        || esw.WorkflowId == (int)WorkflowStatus.CompletePendingUAT)
                    {
                        if (ue.Event != null
                            && ue.Event.UcaaSEventOdieDev != null
                            && ue.Event.UcaaSEventOdieDev.Count > 0)
                        {
                            if (sbTeamPDL.Length > 0 && sbTeamPDL[sbTeamPDL.Length - 1] != ',')
                                sbTeamPDL.Append(",");

                            sbTeamPDL.AppendNoDup(String.Join(",", ue.Event.UcaaSEventOdieDev.Select(a =>
                                _context.Redsgn.SingleOrDefault(i => i.RedsgnNbr == a.RdsnNbr).NteAssigned).ToList()));

                            if ((sbTeamPDL.Length > 0) && (sbTeamPDL[sbTeamPDL.Length - 1] != ','))
                                sbTeamPDL.Append(",");

                            sbTeamPDL.AppendNoDup(String.Join(",", ue.Event.UcaaSEventOdieDev.Select(a =>
                                _context.Redsgn.SingleOrDefault(i => i.RedsgnNbr == a.RdsnNbr).PmAssigned).ToList()));
                        }
                    }

                    // Published Emails
                    if ((esw.WorkflowId == (int)WorkflowStatus.Publish
                        && esw.EventRule.EndEventStusId == (int)EventStatus.Published)
                        || esw.EventStatusId == (int)EventStatus.Published
                        || esw.EventRule.EndEventStusId != (int)EventStatus.Published)
                    {
                        if (!(esw.WorkflowId == (int)WorkflowStatus.Retract
                             && esw.EventStatusId != (int)EventStatus.Visible
                             && dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0
                             && dtCurrentCalData.Rows.Count > 0))
                        {
                            // General Mail and not necessary mean MDS Email
                            if (RuleData.Any(a => (a.EventDataCd != null) && (a.EventDataCd.Trim() == "MDS")))
                            {
                                if ((RuleData.Any(a => a.EmailmemCd.HasValue) && RuleData.Any(a => a.EmailmemCd.Value != 0))
                                        || (RuleData.Any(a => a.EmailrevCd.HasValue) && RuleData.Any(a => a.EmailrevCd.Value != 0))
                                        || (RuleData.Any(a => a.EmailactCd.HasValue) && RuleData.Any(a => a.EmailactCd.Value != 0)))
                                {
                                        sSubj = GetEmailSubj("UCaaS", esw.EventRule, esw.EventTitle, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "MDS").First().EmailSubj);
                                        //GetEmailAddr("MDS",
                                        //             esw,
                                        //             ref sbEmailAddr,
                                        //             ref sbEmailCCAddr,
                                        //             ref UserIDs,
                                        //             ref sbCommonEmailCCAddr);

                                        GetEmailAddr("MDS", (RuleData.Where(a => a.EventDataCd.Trim() == "MDS").First()), XmlBody, ref UserIDs, ref sbEmailCCAddr, ref sbEmailAddr, ref esw);

                                    if (esw.EventStatusId == (int)EventStatus.Published
                                        && dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0
                                        && (bActChange || bTimeChange)
                                        )
                                    {
                                        if (RuleData.Any(a => a.EmailoldactCd.HasValue) && RuleData.Any(a => a.EmailoldactCd.Value))
                                        {
                                            if (sbOldActEmail.Length > 0)
                                            {
                                                if (sbEmailCCAddr.Length > 0 && sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ',')
                                                    sbEmailCCAddr.Append(",");
                                                sbEmailCCAddr.AppendNoDup(sbOldActEmail);
                                            }
                                        }
                                    }

                                    if (esw.AssignUser != null && esw.AssignUser.Count > 0)
                                    {
                                        foreach (var lau in esw.AssignUser)
                                        {
                                            if (RuleData.Any(a => a.EmailactCd.HasValue) && RuleData.Any(a => a.EmailactCd.Value == 1))
                                            {
                                                if (sbEmailAddr.Length > 0 && sbEmailAddr[sbEmailAddr.Length - 1] != ',')
                                                    sbEmailAddr.Append(",");
                                                sbEmailAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                                            }
                                            else if (RuleData.Any(a => a.EmailactCd.HasValue) && RuleData.Any(a => a.EmailactCd.Value == 2))
                                                {
                                                if (sbEmailCCAddr.Length > 0 && sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ',')
                                                    sbEmailCCAddr.Append(",");
                                                sbEmailCCAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                                            }
                                        }
                                    }

                                    if (sbTeamPDL.Length > 0)
                                    {
                                        if (!sbEmailCCAddr.ToString().ToLower().Contains(sbTeamPDL.ToString().ToLower()))
                                        {
                                            if (sbEmailCCAddr.Length > 0 && sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ',')
                                                sbEmailCCAddr.Append(",");
                                            sbEmailCCAddr.AppendNoDup(sbTeamPDL);
                                        }
                                    }

                                    if (sbEmailAddr.ToString().Length <= 0 && sbEmailCCAddr.ToString().Length > 0)
                                    {
                                        sbEmailAddr.AppendNoDup(sbEmailCCAddr.ToString());
                                        sbEmailCCAddr.Clear();
                                    }

                                    // General Mail
                                    if (XmlBody.ToString().Length > 0 && sbEmailAddr.ToString().Length > 0)
                                    {
                                        EmailReq er = new EmailReq();
                                        er.EmailReqTypeId = (int)EmailREQType.UCaaSEventEmail;
                                        er.EventId = esw.EventId;
                                        er.EmailListTxt = sbEmailAddr.Replace(";", ",").ToString();
                                        er.EmailCcTxt = sbEmailCCAddr.Replace(";", ",").ToString();
                                        er.EmailSubjTxt = sSubj;
                                        er.EmailBodyTxt = XmlBody.ToString();
                                        er.StusId = esw.IsSupressEmail ? (short)EmailStatus.OnHold : (short)EmailStatus.Pending;
                                        Create(er);
                                    }
                                }
                            }

                                // Updated by Sarah Sandoval [20200724] - Commented CPE since no CPE email for UCaaS Event
                                // CPE Mail
                                //if (dtRule.EventDataCd.Trim() == "CPE")
                                //{
                                //    if (XmlBody.Elements("UCaaSEvent").Elements("EVENT_TITLE_TXT").Any()
                                //    && XmlBody.Elements("UCaaSEvent").Elements("EVENT_TITLE_TXT").First().Value.Trim().Length > 0)
                                //    {
                                //        if ((esw.WorkflowId == (int)WorkflowStatus.Publish
                                //                && (XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR").Any()
                                //                    && XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR").Single().Value.Trim().Length > 0))

                                //            || (esw.WorkflowId == (int)WorkflowStatus.Retract
                                //              && esw.EventStatusId != (int)EventStatus.Visible
                                //              && dtCurrentEHist.Select("ACTN_ID = " + ((int)Actions.Publish).ToString()).Length > 0
                                //              && dtCurrentCalData.Rows.Count > 0)

                                //            || (esw.WorkflowId == (int)WorkflowStatus.Reject
                                //              && esw.EventStatusId != (int)EventStatus.Published
                                //              && dtCurrentEHist.Select("ACTN_ID = " + ((int)Actions.Publish).ToString()).Length > 0
                                //              && dtCurrentCalData.Rows.Count > 0
                                //              && (XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR").Any()
                                //                && XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR").Single().Value.Trim().Length > 0)))
                                //        {
                                //            if ((dtRule.EmailmemCd.HasValue && dtRule.EmailmemCd.Value != 0)
                                //                || (dtRule.EmailrevCd.HasValue && dtRule.EmailrevCd.Value != 0)
                                //                || (dtRule.EmailactCd.HasValue && dtRule.EmailactCd.Value != 0))
                                //            {
                                //                GetEmailAddr("CPE", dtRule, XmlBody, ref UserIDs, ref sbEmailCCAddr, ref sbEmailAddr, ref esw);

                                //                if (esw.EventStatusId == (int)EventStatus.Published
                                //                    && dtCurrentEHist.Select("ACTN_ID = " + ((int)Actions.Publish).ToString()).Length > 0
                                //                    && (bActChange || bTimeChange))
                                //                {
                                //                    if (dtRule.EmailoldactCd.HasValue && dtRule.EmailoldactCd.Value)
                                //                    {
                                //                        if (sbOldActEmail.Length > 0)
                                //                        {
                                //                            if (sbEmailCCAddr.Length > 0 && sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ',')
                                //                                sbEmailCCAddr.Append(",");
                                //                            sbEmailCCAddr.AppendNoDup(sbOldActEmail);
                                //                        }
                                //                    }
                                //                }

                                //                if (esw.AssignUser != null && esw.AssignUser.Count > 0)
                                //                {
                                //                    foreach (var lau in esw.AssignUser)
                                //                    {
                                //                        if (dtRule.EmailactCd.HasValue && dtRule.EmailactCd.Value == 1)
                                //                        {
                                //                            if (sbEmailAddr.Length > 0 && sbEmailAddr[sbEmailAddr.Length - 1] != ',')
                                //                                sbEmailAddr.Append(",");
                                //                            sbEmailAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                                //                        }
                                //                        else if (dtRule.EmailactCd.HasValue && dtRule.EmailactCd.Value == 2)
                                //                        {
                                //                            if (sbEmailCCAddr.Length > 0 && sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ',')
                                //                                sbEmailCCAddr.Append(",");
                                //                            sbEmailCCAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                                //                        }
                                //                    }
                                //                }

                                //                if (sbTeamPDL.Length > 0)
                                //                {
                                //                    if (!(sbEmailCCAddr.ToString().ToLower().Contains(sbTeamPDL.ToString().ToLower())))
                                //                    {
                                //                        if (sbEmailCCAddr.Length > 0 && sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ',')
                                //                            sbEmailCCAddr.Append(",");
                                //                        sbEmailCCAddr.AppendNoDup(sbTeamPDL);
                                //                    }
                                //                }

                                //                if (sbEmailAddr.ToString().Length <= 0 && sbEmailCCAddr.ToString().Length > 0)
                                //                {
                                //                    sbEmailAddr.AppendNoDup(sbEmailCCAddr.ToString());
                                //                    sbEmailCCAddr.Clear();
                                //                }

                                //                if (sbEmailAddr.ToString().Length > 0)
                                //                {
                                //                    EmailReq er = new EmailReq();
                                //                    er.EmailReqTypeId = (int)EmailREQType.UCaaSCPEEmail;
                                //                    er.EventId = esw.EventId;
                                //                    er.EmailListTxt = sbEmailAddr.Replace(";", ",").ToString();
                                //                    er.EmailCcTxt = sbEmailCCAddr.Replace(";", ",").ToString();
                                //                    er.EmailSubjTxt = GetEmailSubj("CPE", esw.EventRule, esw.EventTitle, esw.EventId, esw.SOWSEventID, dtRule.EmailSubj);
                                //                    er.EmailBodyTxt = XmlBody.ToString();
                                //                    er.StusId = esw.IsSupressEmail ? (short)EmailStatus.OnHold : (short)EmailStatus.Pending;
                                //                    Create(er);
                                //                }
                                //            }
                                //        }
                                //    }
                                //}

                                // RET Mail
                               if (RuleData.Any(a => (a.EventDataCd != null) && (a.EventDataCd.Trim() == "RET")))
                                {
                                if (esw.WorkflowId == (int)WorkflowStatus.Retract
                                      && esw.EventStatusId != (int)EventStatus.Visible
                                      && dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0
                                      && dtCurrentCalData.Rows.Count > 0)
                                {
                                        if ((RuleData.Any(a => a.EmailmemCd.HasValue) && RuleData.Any(a => a.EmailmemCd.Value != 0))
                                             || (RuleData.Any(a => a.EmailrevCd.HasValue) && RuleData.Any(a => a.EmailrevCd.Value != 0))
                                             || (RuleData.Any(a => a.EmailactCd.HasValue) && RuleData.Any(a => a.EmailactCd.Value != 0)))
                                        {
                                            sSubj = GetEmailSubj("UCaaS", esw.EventRule, esw.EventTitle, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "RET").First().EmailSubj);
                                            //GetEmailAddr("RET",
                                            //         esw,
                                            //         ref sbEmailAddr,
                                            //         ref sbEmailCCAddr,
                                            //         ref UserIDs,
                                            //         ref sbCommonEmailCCAddr);

                                            GetEmailAddr("RET", (RuleData.Where(a => a.EventDataCd.Trim() == "RET").First()), XmlBody, ref UserIDs, ref sbEmailCCAddr, ref sbEmailAddr, ref esw);

                                            if (esw.EventStatusId == (int)EventStatus.Published
                                                && dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0
                                                && (bActChange || bTimeChange))
                                        {
                                                if (RuleData.Any(a => a.EmailoldactCd.HasValue) && RuleData.Any(a => a.EmailoldactCd.Value))
                                                {
                                                if (sbOldActEmail.Length > 0)
                                                {
                                                    if (sbEmailCCAddr.Length > 0 && sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ',')
                                                        sbEmailCCAddr.Append(",");
                                                    sbEmailCCAddr.AppendNoDup(sbOldActEmail);
                                                }
                                            }
                                        }

                                        if (sbTeamPDL.Length > 0)
                                        {
                                            if (!(sbEmailCCAddr.ToString().ToLower().Contains(sbTeamPDL.ToString().ToLower())))
                                            {
                                                if (sbEmailCCAddr.Length > 0 && sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ',')
                                                    sbEmailCCAddr.Append(",");
                                                sbEmailCCAddr.AppendNoDup(sbTeamPDL);
                                            }
                                        }

                                        if (sbEmailAddr.ToString().Length <= 0 && sbEmailCCAddr.ToString().Length > 0)
                                        {
                                            sbEmailAddr.AppendNoDup(sbEmailCCAddr.ToString());
                                            sbEmailCCAddr.Clear();
                                        }

                                        if (sbEmailAddr.ToString().Length > 0)
                                        {
                                            EmailReq er = new EmailReq();
                                            er.EmailReqTypeId = (int)EmailREQType.UCaaSRetractEmail;
                                            er.EventId = esw.EventId;
                                            er.EmailListTxt = sbEmailAddr.Replace(";", ",").ToString();
                                            er.EmailCcTxt = sbEmailCCAddr.Replace(";", ",").ToString();
                                            er.EmailSubjTxt = sSubj;
                                            er.EmailBodyTxt = XmlBody.ToString();
                                            er.StusId = esw.IsSupressEmail ? (short)EmailStatus.OnHold : (short)EmailStatus.Pending;
                                            Create(er);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                }

                bEmailFlg = true;
            }
            catch (Exception e)
            {
                bEmailFlg = false;
                _logger.LogInformation($"{ e.Message } ");
            }

            return bEmailFlg;
        }

        // Send email for MDS EVENT
        //MdsEventView esw, MdsEvent dMDSData
        public bool SendMDSMail(MdsEvent dMDSData, EventWorkflow esw, ICollection<MdsEventNtwkTrptView> networkTransport, string primaryContactEmail = null)
        {
            bool bActChange = false;
            bool bTimeChange = false;
            bool bFTPreConfig = false;
            StringBuilder sbEmailAddr = new StringBuilder();
            StringBuilder sbEmailCCAddr = new StringBuilder();
            StringBuilder sbCalendarURL = new StringBuilder();
            StringBuilder sbCommonEmailCCAddr = new StringBuilder();
            //StringBuilder sbOldActEmail = new StringBuilder();
            //StringBuilder sbTeamPDL = new StringBuilder();
            string CalendarEntryUrl = _configuration.GetSection("AppSettings:CalenderEntryURL").Value;
            bool bEmailFlg = false;
            string sBody = string.Empty;
            StringBuilder sbActUserName = new StringBuilder();
            DataTable dtCurrentEHist = GetCurrentEHist(esw.EventId);
            DataTable dtCurrentCalData = GetCurrentCalData(esw.EventId);
            byte EmailReqType = (byte)EmailREQType.MDSEventEmail;
            List<int> UserIDs = new List<int>();
            //esw.FailReasId = esw.FailReasId < 0 ? esw.FailReasId = 0 : esw.FailReasId;

            if (esw.AssignUser != null && esw.AssignUser.Count > 0)
            {
                foreach (var au in esw.AssignUser)
                {
                    var user = _userRepository.GetById(au.AsnToUserId);
                    sbActUserName.AppendNoDup(string.IsNullOrWhiteSpace(user.FullNme)
                        ? string.Empty : (user.FullNme + "; "));
                }

                // Removing the trailing "; "
                if (sbActUserName.Length > 0)
                    sbActUserName.Remove(sbActUserName.Length - 2, 2);
            }

            bActChange = _eventAsnToUserRepository.AccountChange(esw.AssignUser, esw.EventId);

            if (esw.StartTime != esw.OldStartTime || esw.EndTime != esw.OldEndTime)
            {
                bTimeChange = true;
            }

            if (esw.ActivatorUserId > 0)
            {
                if (esw.EventStatusId == (int)EventStatus.Published
                    && (esw.WorkflowId == (int)WorkflowStatus.Publish))
                {
                    bFTPreConfig = true;
                }
            }
            /*
                a.	If all the email related data is blank/0 in the xml rule,
                   then you don’t have to enter a row in this table.
                    And can handle in code to default them as blank/0 for those elements.
                b.	[EVENT_DATA_CD] used only for MDS/UCaaS events. Values will be MDS, CPE, FT, RET
                    like found in the subsections of MDSRules/UCaasRules.xml.
                c.	[EMAILMEM_CD] is one field to house all these three elements:
                    <EmailMem><EmailMemTo><EmailMemCC>. Same case with [EMAILREV_CD] and [EMAILACT_CD].
                    For values 0 in the xml rule, leave the column values as NULL,
                        if EmailMem/EmailMemTo is 1 then column value will be 1;
                        if EmailMem/EmailMemCC is 1 the column value will be 2.
                    If you notice, I haven’t created a column for <Email> element
                    because it can derived as 1 if there is 1 or 2 in atleast one of
                    [EMAILMEM_CD]/[EMAILREV_CD]/ [EMAILACT_CD] columns.
                d.	[EMAILPUBCC_CD], [EMAILCMPLCC_CD], [EMAILOLDACT_CD], [EMAIL_SUBJ], [EMAILCC_ADR]
                    are directly mapped to <EmailPubCC>, <EmailCmplCC>, <EmailOldAct>, <EmailSubj>,
                    <CCAddr> elements.

                •   dbo.[EVENT_RULE_DATA] is the child table to house data related to Event Emails and the
	                dbo.[LK_EVENT_RULE].[EVENT_RULE_ID] is the foreign key.
                a.	If all the email related data is blank/0 in the xml rule, then you don’t have to enter a row in
	                this table. And can handle in code to default them as blank/0 for those elements.
                b.	[EVENT_DATA_CD] used only for MDS/UCaaS events.  Values will be MDS, CPE, FT, RET like found in
	                the subsections of MDSRules/UCaasRules.xml.
                c.	[EMAILMEM_CD] is one field to house all these three <EmailMem><EmailMemTo><EmailMemCC> elements.
	                Same case with [EMAILREV_CD] and [EMAILACT_CD]. For values 0 in the xml rule, leave the column values as
	                NULL, if EmailMem/EmailMemTo is 1 then column value will be 1; if EmailMem/EmailMemCC is 1 the column value
	                will be 2. If you notice, i haven’t created a column for <Email> element because it can derived
	                as 1 if there is 1 or 2 in atleast one of [EMAILMEM_CD]/[EMAILREV_CD]/ [EMAILACT_CD] columns.
                d.	[EMAILPUBCC_CD], [EMAILCMPLCC_CD], [EMAILOLDACT_CD], [EMAIL_SUBJ], [EMAILCC_ADR] are directly
	                mapped to <EmailPubCC>, <EmailCmplCC>, <EmailOldAct>, <EmailSubj>, <CCAddr> elements
            */

            try
            {
                List<EventRuleData> RuleData = _eventRuleDataRepository.GetByRuleId((int)esw.EventRule.EventRuleId);
                //foreach (var dtRule in RuleData)
                //{
                if (RuleData != null && RuleData.Count > 0)
                {
                    if (_workflowRepo.UpdateEWStatus(esw))
                    {
                        if (!bFTPreConfig)
                        {

                            string sSubj = string.Empty;

                            // Send MDS Guest Wi-fi Emails
                            if ((esw.IsSrvcActvEmail == true || esw.IsSrvcCmntEmail == true)
                                && (esw.Profile == "Reviewer" || esw.Profile == "Activator"))
                                CreateAndSendSrvcEmails(esw.EventId, esw.IsSupressEmail);

                            // Published Emails
                            if ((esw.EventStatusId == (byte)EventStatus.Published
                                    && esw.EventRule.EndEventStusId == (byte)WorkflowStatus.Publish
                                    && (bActChange || bTimeChange))
                                || esw.EventStatusId != (byte)EventStatus.Published
                                || esw.EventRule.EndEventStusId != (byte)WorkflowStatus.Publish)
                            {
                                var eventTitleTxt = string.Empty;
                                eventTitleTxt = (dMDSData.Event.CsgLvlId > 0) ? "Private Customer" : esw.EventTitle;

                                var eventType = (dMDSData.Event.CsgLvlId > 0) ? string.Join('+', dMDSData.Event.MdsEventNtwkActy.Select(a => a.NtwkActyType.NtwkActyTypeDes).ToList()) : "";
                                var eventTypeStr = string.Join('+', dMDSData.Event.MdsEventNtwkActy.Select(a => a.NtwkActyType.NtwkActyTypeDes).ToList());
                                var activityType = (dMDSData.Event.CsgLvlId > 0 && eventType.Contains("MDS")) ? dMDSData.MdsActyType.MdsActyTypeDes : "";

                                sBody = CreateMDSEventBody(dMDSData, esw, networkTransport);
                                _logger.LogInformation($"Constructing Email Content for EventID - { dMDSData.EventId }. Email Body: { sBody } ");

                                int[] cpeRequired = new int[] { 2, 9 };
                                bool willSendThruSSTAT = false;
                                // MDS or FT Emails
                                if (!(esw.WorkflowId == (int)WorkflowStatus.Retract
                                     && esw.EventStatusId != (int)EventStatus.Visible
                                     && (dtCurrentEHist.Select("ActnId = " + (int)Actions.Publish).Length > 0)
                                     && dtCurrentCalData.Rows.Count > 0))
                                {
                                    _logger.LogInformation($"Constructing MDS or FT Emails for EventID - { dMDSData.EventId }. Suppress Email: { esw.IsSupressEmail } ");

                                    //if (RuleData.EventDataCd != null)
                                    if (RuleData.Any(a => a.EventDataCd != null))
                                    {
                                        RuleData = RuleData.FindAll(a => !string.IsNullOrWhiteSpace(a.EventDataCd));
                                        //if ((dtRule.EventDataCd.Trim() == "MDS") || (dtRule.EventDataCd.Trim() == "FT"))
                                        if (RuleData.Any(a => a.EventDataCd.Trim() == "MDS")
                                            || RuleData.Any(a => a.EventDataCd.Trim() == "FT")
                                            || RuleData.Any(a => a.EventDataCd.Trim() == "NI")
                                            || RuleData.Any(a => a.EventDataCd.Trim() == "MDSES"))
                                        {
                                            if (RuleData.Any(a => a.EventDataCd.Trim() == "MDS") && (esw.CalendarEventTypeId != (int)CalendarEventType.MDSFT))
                                            {
                                                sSubj = GetEmailSubj("MDS", esw.EventRule, eventTitleTxt, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "MDS").First().EmailSubj, eventType, activityType);
                                                // GetEmailAddr("MDS", ref esw);
                                                GetEmailAddr("MDS",
                                                             esw,
                                                             ref sbEmailAddr,
                                                             ref sbEmailCCAddr,
                                                             ref UserIDs,
                                                             ref sbCommonEmailCCAddr);
                                            }

                                            if (RuleData.Any(a => a.EventDataCd.Trim() == "FT") && esw.CalendarEventTypeId == (int)CalendarEventType.MDSFT)
                                            {
                                                EmailReqType = (byte)EmailREQType.MDSFTEmail;
                                                sSubj = GetEmailSubj("FT", esw.EventRule, eventTitleTxt, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "FT").First().EmailSubj, eventType, activityType);
                                                GetEmailAddr("FT",
                                                             esw,
                                                             ref sbEmailAddr,
                                                             ref sbEmailCCAddr,
                                                             ref UserIDs,
                                                             ref sbCommonEmailCCAddr);
                                            }

                                            if (RuleData.Any(a => a.EventDataCd.Trim() == "NI") && (esw.CalendarEventTypeId != (int)CalendarEventType.MDSFT) && esw.IsNetworkIntl)
                                            {
                                                sSubj = GetEmailSubj("Network Intl", esw.EventRule, eventTitleTxt, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "NI").First().EmailSubj, eventType, activityType);
                                                // GetEmailAddr("MDS", ref esw);
                                                GetEmailAddr("NI",
                                                             esw,
                                                             ref sbEmailAddr,
                                                             ref sbEmailCCAddr,
                                                             ref UserIDs,
                                                             ref sbCommonEmailCCAddr);
                                            }

                                            if (RuleData.Any(a => a.EventDataCd.Trim() == "MDSES") && esw.IsEscalation)
                                            {
                                                // Added by Sarah Sandoval[20220222]
                                                // Get MNSPM Assignment and include in Submitted Escalation Email
                                                if (esw.Profile == "Member" && esw.ReviewerId <= 0)
                                                {
                                                    // Get the assigned MNSPM on DB since
                                                    if (string.IsNullOrWhiteSpace(esw.ReviewerAdId))
                                                    {
                                                        // This portion is for network event since mnspm is always returned as null
                                                        var evt = _context.MdsEvent.AsNoTracking().FirstOrDefault(i => i.EventId == esw.EventId);
                                                        esw.ReviewerAdId = evt != null ? evt.MnsPmId : string.Empty;
                                                    }

                                                    var reviewer = _context.LkUser.FirstOrDefault(i => i.UserAdid == esw.ReviewerAdId);
                                                    esw.ReviewerId = reviewer != null ? reviewer.UserId : 0;
                                                }

                                                EmailReqType = (byte)EmailREQType.MDSESEventEmail;
                                                sSubj = GetEmailSubj("MDS", esw.EventRule, eventTitleTxt, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "MDSES").First().EmailSubj, eventType, activityType);
                                                // GetEmailAddr("MDS", ref esw);
                                                GetEmailAddr("MDSES",
                                                             esw,
                                                             ref sbEmailAddr,
                                                             ref sbEmailCCAddr,
                                                             ref UserIDs,
                                                             ref sbCommonEmailCCAddr);
                                                if(esw.WorkflowId != ((int)WorkflowStatus.Publish))
                                                {
                                                    sbEmailCCAddr.Append(",");
                                                    sbEmailCCAddr.Append(esw.PubEmailCcTxt);
                                                }
                                            }

                                            if ((esw.EventStatusId == ((int)EventStatus.Published))
                                                && (dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0))
                                            {
                                                //if (RuleData.Any(a => a.EmailoldactCd.Equals("1")))
                                                if (RuleData.Where(a => a.EventDataCd.Trim() == "MDS").First().EmailoldactCd == true ||
                                                    RuleData.Where(a => a.EventDataCd.Trim() == "FT").First().EmailoldactCd == true)
                                                {
                                                    if (sbOldActEmail.Length > 0)
                                                    {
                                                        if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                                            sbEmailCCAddr.Append(",");
                                                        sbEmailCCAddr.AppendNoDup(sbOldActEmail);
                                                    }
                                                }
                                            }

                                            if ((esw.WorkflowId == ((int)WorkflowStatus.Publish))
                                                                    || (esw.WorkflowId == ((int)WorkflowStatus.InProgress))
                                                                    || (esw.WorkflowId == ((int)WorkflowStatus.InprogressUAT))
                                                                    || (esw.WorkflowId == ((int)WorkflowStatus.CompletePendingUAT))
                                                                    || (esw.WorkflowId == ((int)WorkflowStatus.Complete)))
                                            {
                                                if (esw.MdsEventOdieDev != null && esw.MdsEventOdieDev.Count > 0)
                                                {
                                                    if ((sbTeamPDL.Length > 0) && (sbTeamPDL[sbTeamPDL.Length - 1] != ','))
                                                        sbTeamPDL.Append(",");
                                                    //sbTeamPDL.AppendNoDup(String.Join(",", esw.MdsEventOdieDev.Select(a => a.o.NTEEmail).ToList()));
                                                    if ((sbTeamPDL.Length > 0) && (sbTeamPDL[sbTeamPDL.Length - 1] != ','))
                                                        sbTeamPDL.Append(",");
                                                }
                                            }

                                            // Added by Sarah Sandoval [20210414] - Copy CPE PDL as per Diane's ticket (IM6144797)
                                            // On UI, Show CPE Option is true when SprintCpeNcrId in [2,9,10,12]
                                            // As per Jagan - Copy CPE PDL from Event Status Publish or Inprogress to Rework
                                            if ((esw.EventRule.StrtEventStusId == (byte)EventStatus.Published
                                                || esw.EventRule.StrtEventStusId == (byte)EventStatus.InProgress)
                                                && esw.EventRule.EndEventStusId == (byte)EventStatus.Rework
                                                && cpeRequired.Any(i => i == dMDSData.SprintCpeNcrId)
                                                && !string.IsNullOrWhiteSpace(dMDSData.CpeDspchEmailAdr))
                                            {
                                                if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                                    sbEmailCCAddr.Append(",");
                                                sbEmailCCAddr.AppendNoDup(dMDSData.CpeDspchEmailAdr);
                                            }

                                            if (sbTeamPDL.Length > 0)
                                            {
                                                if (!(sbEmailCCAddr.ToString().ToLower().Contains(sbTeamPDL.ToString().ToLower())))
                                                {
                                                    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                                        sbEmailCCAddr.Append(",");
                                                    sbEmailCCAddr.AppendNoDup(sbTeamPDL);
                                                }
                                            }

                                            if ((sbEmailAddr.ToString().Length <= 0) && (sbEmailCCAddr.ToString().Length > 0))
                                            {
                                                sbEmailAddr.AppendNoDup(sbEmailCCAddr.ToString());
                                                sbEmailCCAddr.Clear();
                                            }

                                            if ((esw.WorkflowId == ((int)WorkflowStatus.Shipped))
                                                && (esw.EventStatusId == ((int)EventStatus.Fulfilling)))
                                            {
                                                EmailReqType = (byte)EmailREQType.MDSShippedEmail;
                                                if (esw.ShipCustEmailAddr.Length > 0)
                                                {
                                                    if (!(sbEmailAddr.ToString().ToLower().Contains(esw.ShipCustEmailAddr.ToLower())))
                                                    {
                                                        if ((sbEmailAddr.Length > 0) && (sbEmailAddr[sbEmailAddr.Length - 1] != ','))
                                                            sbEmailAddr.Append(",");
                                                        sbEmailAddr.AppendNoDup(esw.ShipCustEmailAddr);
                                                    }
                                                }
                                            }

                                            if (sSubj.Length <= 0)
                                                sSubj = GetEmailSubj("MDS", esw.EventRule, eventTitleTxt, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "MDS").First().EmailSubj, eventType, activityType);

                                            if (sbEmailAddr.ToString().Length > 0
                                                && (EmailReqType != (byte)EmailREQType.MDSShippedEmail
                                                    || (EmailReqType == (byte)EmailREQType.MDSShippedEmail && !IsEventEmailSent(esw.EventId, EmailREQType.MDSShippedEmail) && isCradlePointEvent(ref dMDSData) == false))
                                                )
                                            {
                                                InsertIntoEmailReq(
                                                        EmailReqType,
                                                        esw.EventId,
                                                        sbEmailAddr.Replace(";", ",").ToString(),
                                                        sbEmailCCAddr.Replace(";", ",").ToString(),
                                                        sSubj,
                                                        sBody,
                                                        esw.IsSupressEmail);


                                                List<int> conferenceBridgeFlag = new List<int>(new int[] { 1, 2, 3, 4 }); // No, Yes MNS and Yes Other.
                                                if(( !((eventTypeStr.Contains("MDS") && (dMDSData.MdsActyTypeId == 3))) && !eventTypeStr.Contains("Network Intl") && conferenceBridgeFlag.Contains(dMDSData.CnfrcBrdgId) && dMDSData.CustLtrOptOut != true))
                                                {

                                                    byte emailRequestType = 0;
                                                    bool sendConferenceEmail = false;
                                                    string statusStr = esw.EventStatusId.ToString();


                                                    var newEmailToAddr = sbEmailAddr;
                                                    newEmailToAddr.Insert(0, $"{primaryContactEmail},");

                                                    var reviewer = _userRepository.GetById(esw.ReviewerId);
                                                    if(reviewer != null)
                                                    {
                                                        newEmailToAddr.Append($",{reviewer.EmailAdr}");
                                                    }
                                                    

                                                    StringBuilder newEmailCCAddr = new StringBuilder();
                                                    if (statusStr == "5") // Rework can be (Return, Reject and Reschedule) rework is general but the requirement is send only for Reschedule
                                                        statusStr = "4";
                                                    List<string> currentEmails = _contactDetailRepo.Find(i =>  i.RecStusId == 1 && i.ObjTypCd == "E" && i.ObjId == dMDSData.EventId && i.EmailCd.Contains(statusStr) && i.SuprsEmail == false).Select(i => i.EmailAdr).ToList();
                                                    currentEmails.Add(esw.PubEmailCcTxt);
                                                    currentEmails.Add(esw.CmpltdEmailCcTxt);
                                                    newEmailCCAddr.Append(string.Join(",", currentEmails));

                                                    // Published
                                                    if ((esw.EventStatusId == (byte)EventStatus.Published && esw.EventRule.EndEventStusId == (byte)WorkflowStatus.Publish) && (bActChange || bTimeChange) 
                                                        || (esw.EventStatusId == (byte)EventStatus.Pending && esw.EventRule.EndEventStusId == (byte)WorkflowStatus.Publish))
                                
                                                    {
                                                        emailRequestType = (byte)EmailREQType.MDSConferencePublish;
                                                        sendConferenceEmail = true;
                                                    }
                                                    // Publish or InProgress to Rework
                                                    else if ((esw.EventRule.StrtEventStusId == (byte)EventStatus.InProgress) && esw.EventRule.EndEventStusId == (byte)EventStatus.Rework)
                                                    {
                                                        emailRequestType = (byte)EmailREQType.MDSConferenceRework;
                                                        sendConferenceEmail = true;
                                                    } else if(esw.WorkflowId == ((int)WorkflowStatus.CompletePendingUAT) || esw.WorkflowId == ((int)WorkflowStatus.Complete))
                                                    {
                                                        var newEmailAddrList = GetEmailAddr(esw.AssignUser.Select(x => x.AsnToUserId).ToList());
                                                        if (newEmailAddrList != null)
                                                            if (newEmailAddrList.Rows.Count > 0)
                                                            {
                                                                for (int i = 0; i < newEmailAddrList.Rows.Count; i++)
                                                                {
                                                                    if ((newEmailToAddr.Length > 0) && (newEmailToAddr[newEmailToAddr.Length - 1] != ','))
                                                                        newEmailToAddr.Append(",");
                                                                    newEmailToAddr.AppendNoDup(newEmailAddrList.Rows[i]["EmailAddr"].ToString());
                                                                }
                                                            }


                                                        emailRequestType = (byte)EmailREQType.MDSConferenceComplete;
                                                        sendConferenceEmail = true;
                                                    }


                                                    // Send conference email
                                                    if (sendConferenceEmail)
                                                    {
                                                        InsertIntoEmailReq(
                                                           emailRequestType,
                                                           esw.EventId,
                                                           newEmailToAddr.Replace(";", ",").ToString(),
                                                           newEmailCCAddr.Replace(";", ",").ToString(),
                                                           sSubj,
                                                           null,
                                                           esw.IsSupressEmail);
                                                    }
                                                }
                                            }

                                            if (EmailReqType == (byte)EmailREQType.MDSShippedEmail
                                                && !IsEventEmailSent(esw.EventId, EmailREQType.CradlePointShippedEmail)
                                                && isCradlePointEvent(ref dMDSData) == true)
                                            {
                                                StringBuilder _sSubj = new StringBuilder();
                                                EmailReqType = (byte)EmailREQType.CradlePointShippedEmail;
                                                sbEmailAddr.Clear();
                                                sbEmailAddr.AppendNoDup(esw.ShipCustEmailAddr);

                                                _sSubj.AppendNoDup("Shipped: ");
                                                _sSubj.AppendNoDup(eventTitleTxt);
                                                _sSubj.AppendNoDup(" - Event ID: ");
                                                _sSubj.AppendNoDup(esw.EventId.ToString());

                                                InsertIntoEmailReq(EmailReqType, esw.EventId, sbEmailAddr.Replace(";", ",").ToString(), sbEmailCCAddr.Replace(";", ",").ToString(), _sSubj.ToString(), sBody, esw.IsSupressEmail);
                                            }
                                        }
                                    }
                                }

                                // CPE Emails
                                if ((new int[] { 2, 9 }).Any(i => i == dMDSData.SprintCpeNcrId) && RuleData.Any(a => (a.EventDataCd != null) && (a.EventDataCd.Trim() == "CPE")))
                                {
                                    if (
                                        // IF EVENT_TYPE = MDS; WORKFLOW = PUBLISH
                                        (esw.EventTypeId == (int)EventType.MDS
                                            && esw.WorkflowId == (int)WorkflowStatus.Publish
                                            && esw.CPEDispatchEmail != null && !esw.CPEDispatchEmail.Trim().Equals(string.Empty))
                                        // IF WORKFLOW = RETRACT; EVENT_STATUS != VISIBLE; has HISTORY of PUBLISH
                                        || (esw.WorkflowId == (int)WorkflowStatus.Retract
                                            && esw.EventStatusId != (int)EventStatus.Visible
                                            && dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0
                                            && dtCurrentCalData.Rows.Count > 0
                                            && esw.CPEDispatchEmail != null && !esw.CPEDispatchEmail.Trim().Equals(string.Empty))
                                        // IF WORKFLOW = REJECT; EVENT_STATUS == PUBLISHED; has HISTORY of PUBLISH
                                        || (esw.WorkflowId == (int)WorkflowStatus.Reject
                                            && esw.EventStatusId == (int)EventStatus.Published
                                            && dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0
                                            && dtCurrentCalData.Rows.Count > 0
                                            && esw.CPEDispatchEmail != null && !esw.CPEDispatchEmail.Trim().Equals(string.Empty)))
                                    {
                                        EmailReqType = (byte)EmailREQType.MDSCPEEmail;
                                        //if (RuleData.Where(a => a.EventDataCd.Trim() == "CPE").First().EmailactCd == 1)
                                        if (RuleData.Where(a => a.EventDataCd.Trim() == "CPE").Count() > 0)
                                        {
                                            _logger.LogInformation($"Constructing CPE Emails for EventID - { dMDSData.EventId }. Suppress Email: { esw.IsSupressEmail } ");

                                            //sSubj = GetEmailSubj("CPE", esw.EventRule, eventTitleTxt, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "CPE").First().EmailSubj);
                                            GetEmailAddr("CPE",
                                                             esw,
                                                             ref sbEmailAddr,
                                                             ref sbEmailCCAddr,
                                                             ref UserIDs,
                                                             ref sbCommonEmailCCAddr);

                                            if (esw.EventTypeId == (int)EventStatus.Published
                                                && dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0)
                                            {
                                                if (RuleData.Where(a => a.EventDataCd.Trim() == "CPE").First().EmailoldactCd == true)
                                                {
                                                    if (sbOldActEmail.Length > 0)
                                                    {
                                                        if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                                            sbEmailCCAddr.Append(",");
                                                        sbEmailCCAddr.AppendNoDup(sbOldActEmail);
                                                    }
                                                }
                                            }

                                            if (sbTeamPDL.Length > 0)
                                            {
                                                if (!(sbEmailCCAddr.ToString().ToLower().Contains(sbTeamPDL.ToString().ToLower())))
                                                {
                                                    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                                        sbEmailCCAddr.Append(",");
                                                    sbEmailCCAddr.AppendNoDup(sbTeamPDL);
                                                }
                                            }

                                            sSubj = GetEmailSubj("CPE", esw.EventRule, eventTitleTxt, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "CPE").First().EmailSubj, eventType, activityType);

                                            if (esw.CalendarEventTypeId.Equals(CalendarEventType.MDSFT))
                                            {
                                                EmailReqType = (byte)EmailREQType.MDSCPEFTEmail;
                                            }

                                            if ((sbEmailAddr.ToString().Length <= 0) && (sbEmailCCAddr.ToString().Length > 0))
                                            {
                                                sbEmailAddr.AppendNoDup(sbEmailCCAddr.ToString());
                                                sbEmailCCAddr.Clear();
                                            }

                                            if (sbEmailAddr.ToString().Length > 0)
                                            {
                                                // km967761 - 08/23/2021 - explicitly pass "true" for bSuppressEmails parameter for SSTAT - CPE Dispatch Emails
                                                // km967761 - 09/27/2021 - revert back logic for bSuppressEmails
                                                int emailReqId = InsertIntoEmailReq(EmailReqType, esw.EventId, sbEmailAddr.Replace(";", ",").ToString(), sbEmailCCAddr.Replace(";", ",").ToString(), sSubj.ToString(), sBody, esw.IsSupressEmail);

                                                // km967761 - 08/25/2021 - add condition to if "CPE_DSPCH_EMAIL_ADR" was added to EmailTo or EmailCc
                                                // km967761 - 10/28/2021 - add condition if CPE Order is not "Goodman"
                                                // jbolano15 - 12/09/2021 - Revert Goodman change
                                                if (esw.EventTypeId == (int)EventType.MDS
                                                    && esw.CPEDispatchEmail != null
                                                    && !esw.CPEDispatchEmail.Trim().Equals(string.Empty)
                                                    && !esw.IsSupressEmail)
                                                {
                                                    _sstatReqRepo.Create(new SstatReq()
                                                    {
                                                        SstatMsgId = 6,
                                                        EmailReqId = emailReqId,
                                                        StusId = (short)EmailStatus.Pending,
                                                        CreatDt = DateTime.Now
                                                    });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //bCPENote = false;    ///Not Sure where its used
                                        }
                                    }
                                }

                                // RET Emails
                                if (RuleData.Any(a => (a.EventDataCd != null) && (a.EventDataCd.Trim() == "RET")))
                                {
                                    if ((esw.WorkflowId == ((int)WorkflowStatus.Retract))
                                    && (esw.EventStatusId != ((int)EventStatus.Visible))
                                    && (dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0)
                                    && (dtCurrentCalData.Rows.Count > 0))
                                    {
                                        if (RuleData.Where(a => a.EventDataCd.Trim() == "RET").First().EmailactCd == 1)
                                        {
                                            _logger.LogInformation($"Constructing Return Emails for EventID - { dMDSData.EventId }. Suppress Email: { esw.IsSupressEmail } ");

                                            GetEmailAddr("RET",
                                                        esw,
                                                        ref sbEmailAddr,
                                                        ref sbEmailCCAddr,
                                                        ref UserIDs,
                                                        ref sbCommonEmailCCAddr);

                                            if ((esw.EventStatusId == ((int)EventStatus.Published))
                                                 && (dtCurrentEHist.Select("ActnId = " + ((int)Actions.Publish).ToString()).Length > 0))
                                            {
                                                if (RuleData.Where(a => a.EventDataCd.Trim() == "RET").First().EmailoldactCd == true)
                                                {
                                                    if (sbOldActEmail.Length > 0)
                                                    {
                                                        if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                                            sbEmailCCAddr.Append(",");
                                                        sbEmailCCAddr.AppendNoDup(sbOldActEmail);
                                                    }
                                                }
                                            }

                                            if (sbTeamPDL.Length > 0)
                                            {
                                                if (!(sbEmailCCAddr.ToString().ToLower().Contains(sbTeamPDL.ToString().ToLower())))
                                                {
                                                    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                                        sbEmailCCAddr.Append(",");
                                                    sbEmailCCAddr.AppendNoDup(sbTeamPDL);
                                                }
                                            }

                                            if ((sbEmailAddr.ToString().Length <= 0) && (sbEmailCCAddr.ToString().Length > 0))
                                            {
                                                sbEmailAddr.AppendNoDup(sbEmailCCAddr.ToString());
                                                sbEmailCCAddr.Clear();
                                            }

                                            sSubj = GetEmailSubj("RET", esw.EventRule, eventTitleTxt, esw.EventId, esw.SOWSEventID, RuleData.Where(a => a.EventDataCd.Trim() == "RET").First().EmailSubj, eventType, activityType);

                                            if (sbEmailAddr.ToString().Length > 0)
                                            {
                                                // km967761 - 09/23/2021 - explicitly pass "true" for bSuppressEmails parameter for SSTAT - CPE Dispatch Emails
                                                // km967761 - 09/27/2021 - revert back logic for bSuppressEmails
                                                int emailReqId = InsertIntoEmailReq((byte)EmailREQType.MDSRetractEmail, esw.EventId, sbEmailAddr.Replace(";", ",").ToString(), sbEmailCCAddr.Replace(";", ",").ToString(), sSubj.ToString(), sBody, esw.IsSupressEmail);

                                                // km967761 - 09/28/2021 - commented out due to LK_EVENT_RULE.EVENT_RULE_ID IN (229,275,333) already has a copy for EVENT_RULE_DATA.EVENT_RULE_CD = "CPE"
                                                //_sstatReqRepo.Create(new SstatReq()
                                                //{
                                                //    SstatMsgId = 6,
                                                //    EmailReqId = emailReqId,
                                                //    StusId = (short)EmailStatus.Pending,
                                                //    CreatDt = DateTime.Now
                                                //});
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            bEmailFlg = false;
                        }
                    }
                }

                bEmailFlg = true;
            }
            catch (Exception e)
            {
                bEmailFlg = false;
                //_logger.LogInformation($"{ e.Message } ");
                _logger.LogError($"MDS Event Email Entry failed for EventID: { dMDSData.EventId }. { e.Message } ");
            }

            return bEmailFlg;
        }

        public bool isCradlePointEvent(ref MdsEvent me)
        {
            return ((me.Event.MdsEventMacActy != null) && (me.Event.MdsEventMacActy.Any(a => a.MdsMacActyId == 20)));
        }

        private void GetEmailAddr(string sEmailTyp,
            EventWorkflow esw,
            ref StringBuilder sbEmailAddr,
            ref StringBuilder sbEmailCCAddr,
            ref List<int> UserIDs,
            ref StringBuilder sbCommonEmailCCAddr)
        {
            /*
            b.	[EVENT_DATA_CD] used only for MDS/UCaaS events.  Values will be MDS, CPE, FT, RET like found in
	            the subsections of MDSRules/UCaasRules.xml.
            c.	[EMAILMEM_CD] is one field to house all these three <EmailMem><EmailMemTo><EmailMemCC> elements.
	            Same case with [EMAILREV_CD] and [EMAILACT_CD]. For values 0 in the xml rule, leave the column values as
	            NULL, if EmailMem/EmailMemTo is 1 then column value will be 1; if EmailMem/EmailMemCC is 1 the column value
	            will be 2. If you notice, i haven’t created a column for <Email> element because it can derived
	            as 1 if there is 1 or 2 in atleast one of [EMAILMEM_CD]/[EMAILREV_CD]/ [EMAILACT_CD] columns.
            d.	[EMAILPUBCC_CD], [EMAILCMPLCC_CD], [EMAILOLDACT_CD], [EMAIL_SUBJ], [EMAILCC_ADR] are directly
	            mapped to <EmailPubCC>, <EmailCmplCC>, <EmailOldAct>, <EmailSubj>, <CCAddr> elements.
                EVENT_DATA_CD
                CPE
                FT
                MDS
                RET
            */

            UserIDs.Clear();
            sbEmailCCAddr.Clear();
            sbEmailAddr.Clear();
            sbCommonEmailCCAddr.Clear();
            DataTable dtEmailAddr = new DataTable();
            XElement XmlBody = null;

            EventRuleData dtRule = null;
            if (sEmailTyp.Equals("CPE"))
            {
                dtRule = _eventRuleDataRepository.GetByRuleIdDataCode((int)esw.EventRule.EventRuleId, sEmailTyp);
                if (dtRule != null && dtRule.EventDataCd.Trim() == "CPE")
                {
                    if ((esw.EventTypeId == (int)EventType.MDS))
                    {
                        if (!esw.CPEDispatchEmail.Trim().Equals(string.Empty))
                            sbEmailAddr.AppendNoDup(esw.CPEDispatchEmail.Trim());
                    }
                    else if ((esw.EventTypeId == (int)EventType.UCaaS))
                    {
                        if ((XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR").Any()) &&
                                                    (XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR")).Single().Value.Trim().Length > 0)
                            sbEmailAddr.AppendNoDup((XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR")).Single().Value.Trim());
                    }

                    if ((esw.AssignUser != null) && (esw.AssignUser.Count > 0))
                    {
                        foreach (var lau in esw.AssignUser)
                        {
                            if (dtRule.EmailactCd >= 1 && !lau.AsnToUserId.Equals(5001))
                            {
                                if (dtRule.EmailactCd == 2)
                                {
                                    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                        sbEmailCCAddr.Append(",");
                                    sbEmailCCAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                                }

                                if (dtRule.EmailactCd == 1)
                                {
                                    if ((sbEmailAddr.Length > 0) && (sbEmailAddr[sbEmailAddr.Length - 1] != ','))
                                        sbEmailAddr.Append(",");
                                    sbEmailAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                                }
                            }
                        }
                    }

                    if (UserIDs.Count <= 0)
                        UserIDs.Add(esw.RequestorId);

                    dtEmailAddr = GetEmailAddr(UserIDs);
                    if (dtEmailAddr != null)
                        if (dtEmailAddr.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                            {
                                if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                    sbEmailCCAddr.Append(",");
                                sbEmailCCAddr.AppendNoDup(dtEmailAddr.Rows[i]["EmailAddr"].ToString());
                            }
                        }
                }
            }
            else
            {
                dtRule = _eventRuleDataRepository.GetByRuleIdDataCode((int)esw.EventRule.EventRuleId, sEmailTyp);
                if (dtRule.EmailmemCd == 1)
                    UserIDs.Add(esw.RequestorId);

                if (dtRule.EmailrevCd == 1 && esw.ReviewerId != -1)  //  chnage esw to dMDSData  where reviewerid there
                {
                    UserIDs.Add(esw.ReviewerId);

                    DataTable _DTReviewer = GetEventReviewers(esw.EventId, esw.EventTypeId);
                    if (_DTReviewer != null && _DTReviewer.Rows.Count > 0)
                    {
                        foreach (DataRow DR in _DTReviewer.Rows)
                        {
                            int _RevID = 0;
                            try
                            {
                                _RevID = Convert.ToInt32(DR["REV_USER_ID"].ToString());
                            }
                            catch (Exception e)
                            {
                                _logger.LogInformation($"{ e.Message } ");
                            }

                            if (_RevID != esw.ReviewerId && _RevID > 0)
                                UserIDs.Add(_RevID);
                        }
                    }
                }

                if ((esw.AssignUser != null) && (esw.AssignUser.Count > 0))
                {
                    foreach (var lau in esw.AssignUser)
                    {
                        if (dtRule.EmailactCd >= 1 && !lau.AsnToUserId.Equals(5001))
                        {
                            if (dtRule.EmailactCd == 2)
                            {
                                if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                    sbEmailCCAddr.Append(",");
                                sbEmailCCAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                            }

                            if (dtRule.EmailactCd == 1)
                            {
                                if ((sbEmailAddr.Length > 0) && (sbEmailAddr[sbEmailAddr.Length - 1] != ','))
                                    sbEmailAddr.Append(",");
                                sbEmailAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                            }
                        }
                    }
                }

                if (UserIDs.Count <= 0)
                    UserIDs.Add(esw.RequestorId);

                dtEmailAddr = GetEmailAddr(UserIDs);
                if (dtEmailAddr != null)
                    if (dtEmailAddr.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                        {
                            if ((sbEmailAddr.Length > 0) && (sbEmailAddr[sbEmailAddr.Length - 1] != ','))
                                sbEmailAddr.Append(",");
                            sbEmailAddr.AppendNoDup(dtEmailAddr.Rows[i]["EmailAddr"].ToString());
                        }
                    }

                if (esw.EventTypeId == (int)EventType.MDS)
                {
                    bool bRePublish = IsRePublish(esw.EventId);

                    if (esw.IsSrvcGuestWiFiPubEmail == "1" && (esw.Profile != "Member")
                        && (esw.EventRule.EndEventStusId == ((int)EventStatus.Published))
                        && (((bRePublish) && (!IsEventEmailSent(esw.EventId, EmailREQType.MDSSrvcActivationEmail)))
                            || (!bRePublish))
                        )
                    {
                        if ((sbEmailAddr.Length > 0) && (sbEmailAddr[sbEmailAddr.Length - 1] != ','))
                            sbEmailAddr.Append(",");
                        sbEmailAddr.AppendNoDup(GetSysCfgValue("MDSSrvcGuestWifiPubEmail"));
                    }
                }

                if (esw.EventType != null)
                {
                    if (esw.EventType.Contains("Staging") || esw.EventType.Contains("WPaaS"))
                    {
                        if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                            sbEmailCCAddr.Append(",");
                        sbEmailCCAddr.AppendNoDup(GetSysCfgValue("MDSPreStagingWPaaSPDL"));
                    }
                }
            }

            // Generate CC Addr
            UserIDs.Clear();
            UserIDs.Add(esw.UserId <= 0 ? 0 : esw.UserId);
            if (dtRule.EmailmemCd == 2)
                UserIDs.Add(esw.RequestorId);

            if (dtRule.EmailrevCd == 2 && esw.ReviewerId != -1)  //  chnage esw to dMDSData  where reviewerid there
                UserIDs.Add(esw.ReviewerId);

            dtEmailAddr = GetEmailAddr(UserIDs);
            if (dtEmailAddr != null)
                if (dtEmailAddr.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                    {
                        if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                            sbEmailCCAddr.Append(",");
                        sbEmailCCAddr.AppendNoDup(dtEmailAddr.Rows[i]["EmailAddr"].ToString());
                    }
                }

            string PubCCEmailAddr = (esw.EventTypeId == (int)EventType.MDS) ? esw.PubEmailCcTxt
                : ((esw.EventTypeId == (int)EventType.UCaaS) ? (((XmlBody.Elements("UCaaSEvent").Elements("PUB_EMAIL_CC_TXT").Any()) &&
                                                            (XmlBody.Elements("UCaaSEvent").Elements("PUB_EMAIL_CC_TXT")).Single().Value.Trim().Length > 0)
                                                                ? (XmlBody.Elements("UCaaSEvent").Elements("PUB_EMAIL_CC_TXT")).Single().Value.Trim() : string.Empty) : string.Empty);
            if (dtRule.EmailpubccCd == true)
            {
                PubCCEmailAddr = PubCCEmailAddr != null ? PubCCEmailAddr : "";
                if (!PubCCEmailAddr.Equals(string.Empty))
                {
                    if (ValidateEmail(PubCCEmailAddr.Trim()))
                    {
                        string sPubCCEmail = PubCCEmailAddr.Trim().Contains(";") ? PubCCEmailAddr.Trim().Replace(";", ",") : PubCCEmailAddr.Trim();

                        if (sPubCCEmail.Contains(","))
                        {
                            foreach (string sPub in sPubCCEmail.Split(new char[] { ',' }))
                            {
                                if (!(sbEmailCCAddr.ToString().ToLower().Contains(sPub.ToLower())))
                                {
                                    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                        sbEmailCCAddr.Append(",");
                                    sbEmailCCAddr.AppendNoDup(sPub);
                                }
                            }
                        }
                        else
                        {
                            if (!(sbEmailCCAddr.ToString().ToLower().Contains(PubCCEmailAddr.Trim().ToLower())))
                            {
                                if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                    sbEmailCCAddr.Append(",");
                                sbEmailCCAddr.AppendNoDup(PubCCEmailAddr.Trim());
                            }
                        }
                    }
                }
            }

            string CmplCCEmailAddr = (esw.EventTypeId == (int)EventType.MDS) ? esw.CmpltdEmailCcTxt
                : ((esw.EventTypeId == (int)EventType.UCaaS) ? (((XmlBody.Elements("UCaaSEvent").Elements("CMPLTD_EMAIL_CC_TXT").Any()) &&
                                                            (XmlBody.Elements("UCaaSEvent").Elements("CMPLTD_EMAIL_CC_TXT")).Single().Value.Trim().Length > 0)
                                                                ? (XmlBody.Elements("UCaaSEvent").Elements("CMPLTD_EMAIL_CC_TXT")).Single().Value.Trim() : string.Empty) : string.Empty);
            if (dtRule.EmailcmplccCd == true)
            {
                CmplCCEmailAddr = CmplCCEmailAddr != null ? CmplCCEmailAddr : "";
                if (!CmplCCEmailAddr.Trim().Equals(string.Empty))
                {
                    if (ValidateEmail(CmplCCEmailAddr.Trim()))
                    {
                        string sCmplCCEmail = CmplCCEmailAddr.Trim().Contains(";") ? CmplCCEmailAddr.Trim().Replace(";", ",") : CmplCCEmailAddr.Trim();

                        if (sCmplCCEmail.Contains(","))
                        {
                            foreach (string sCmpl in sCmplCCEmail.Split(new char[] { ',' }))
                            {
                                if (!(sbEmailCCAddr.ToString().ToLower().Contains(sCmpl.ToLower())))
                                {
                                    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                        sbEmailCCAddr.Append(",");
                                    sbEmailCCAddr.AppendNoDup(sCmpl);
                                }
                            }
                        }
                        else
                        {
                            if (!(sbEmailCCAddr.ToString().ToLower().Contains(CmplCCEmailAddr.Trim().ToLower())))
                            {
                                if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                    sbEmailCCAddr.Append(",");
                                sbEmailCCAddr.AppendNoDup(CmplCCEmailAddr.Trim());
                            }
                        }
                    }
                }
            }

            if (dtRule.EmailccAdr.Trim().Length > 0)
            {
                // Added condition exclude MNSSD-Fails.list@sprint.com for Network Intl
                if (!(esw.IsNetworkIntl && (dtRule.EmailccAdr.Trim().Contains("MNSSD-Fails.list@sprint.com") || dtRule.EmailccAdr.Trim().Contains("MNSSD-Fails.list@t-mobile.com"))))
                {
                    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                        sbEmailCCAddr.Append(",");
                    sbEmailCCAddr.AppendNoDup(dtRule.EmailccAdr.Trim());
                }
            }

            if (((esw.WorkflowId == ((int)WorkflowStatus.Complete)) || (esw.WorkflowId == ((int)WorkflowStatus.CompletePendingUAT)))
                && esw.EventType != null)
            {
                if (esw.EventType.Contains("Staging") || esw.EventType.Contains("WPaaS"))
                {
                    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                        sbEmailCCAddr.Append(",");
                    //sbEmailCCAddr.AppendNoDup("cpe-provisioning@sprint.com");
                    string cpeProvisioning = GetSysCfgValue("CpeProvisioning");
                    sbEmailCCAddr.AppendNoDup(cpeProvisioning);
                }
            }

            if (esw.WorkflowId == ((int)WorkflowStatus.Publish) || esw.WorkflowId == ((int)WorkflowStatus.Complete))
            {
                //List<Redsgn> redesignData = null;
                if (esw.MdsEventOdieDev.Count() > 0)
                {
                    var data = _redesignRepo.GetNtePmRedesignData(esw.MdsEventOdieDev.Select(b => b.RdsnNbr).FirstOrDefault()).ToList();
                    if (data.Count() > 0) {
                        if (!String.IsNullOrEmpty(data[0].NteAssignedEmail))
                        {
                            if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                sbEmailCCAddr.Append(",");
                            sbEmailCCAddr.AppendNoDup(data[0].NteAssignedEmail);
                        }

                        if (!String.IsNullOrEmpty(data[0].PmAssignedEmail))
                        {
                            if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                sbEmailCCAddr.Append(",");
                            sbEmailCCAddr.AppendNoDup(data[0].PmAssignedEmail);
                        }
                    }
                    //List<Redsgn> redesignData = _redesignRepo.Find(a => a.RedsgnNbr == esw.MdsEventOdieDev.Select(b => b.RdsnNbr).FirstOrDefault()).ToList();

                    //if (redesignData.Count() > 0 && !String.IsNullOrEmpty(redesignData[0].NteAssigned)) {
                    //    var nteAssignedUserEmail = _userRepository.Find(a => a.UserAdid == redesignData[0].NteAssigned).FirstOrDefault().EmailAdr;
                    //    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                    //        sbEmailCCAddr.Append(",");
                    //    sbEmailCCAddr.AppendNoDup(nteAssignedUserEmail);
                    //}

                    //if (redesignData.Count() > 0 && !String.IsNullOrEmpty(redesignData[0].PmAssigned))
                    //{
                    //    var pmAssignedUserEmail = _userRepository.Find(a => a.UserAdid == redesignData[0].PmAssigned).FirstOrDefault().EmailAdr;
                    //    if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                    //        sbEmailCCAddr.Append(",");
                    //    sbEmailCCAddr.AppendNoDup(pmAssignedUserEmail);
                    //}
                }
            }
            if ((esw.EventTypeId == (int)EventType.MDS))
            {
                if (esw != null)
                {
                    esw.TeamPdlNme = esw.TeamPdlNme != null ? esw.TeamPdlNme : string.Empty;
                    if (ValidateEmail(esw.TeamPdlNme.Trim()))
                    {
                        if (sbCommonEmailCCAddr.Length <= 0)
                        {
                            if (esw.TeamPdlNme != null)
                            {
                                if (esw.TeamPdlNme.Trim().Contains(";"))
                                    sbCommonEmailCCAddr.AppendNoDup(esw.TeamPdlNme.Trim().Replace(";", ","));
                                else
                                    sbCommonEmailCCAddr.AppendNoDup(esw.TeamPdlNme.Trim());
                            }
                        }

                        if (sbCommonEmailCCAddr.Length > 0)
                        {
                            sbEmailCCAddr.Append(",");
                            sbEmailCCAddr.AppendNoDup(sbCommonEmailCCAddr);
                        }
                    }

                    // km967761 - 06/15/2021 - PJ025845 - GET CONTACT LIST EMAIL ADDRESESS
                    int eventId = esw.EventId;
                    string objTypeCd = "E";
                    string emailCd = esw.EventRule.EndEventStusId.ToString();

                    List<string> emailAddresses = _contactDetailRepo.GetEmailAddresses(eventId, objTypeCd, emailCd);

                    if (emailAddresses != null)
                        if (emailAddresses.Count > 0)
                        {
                            for (int i = 0; i < emailAddresses.Count; i++)
                            {
                                if ((sbEmailCCAddr.Length > 0) && (sbEmailCCAddr[sbEmailCCAddr.Length - 1] != ','))
                                    sbEmailCCAddr.Append(",");
                                sbEmailCCAddr.AppendNoDup(emailAddresses.ElementAt(i));
                            }
                        }
                }
            }
        }

        public bool IsEventEmailSent(int eventID, EmailREQType emailReqType)
        {
            List<EmailReq> x = (from er in _context.EmailReq
                                where ((er.EventId == eventID)
                                        //&& ((DateTime.Now - er.CreatDt).TotalSeconds > 15)
                                        && (er.EmailReqTypeId == ((int)(emailReqType)))
                                        && (er.StusId == ((byte)EmailStatus.Success)))
                                select er).ToList();
            return (x.Count > 0);
        }

        private bool IsRePublish(int iEventID)
        {
            List<EventHist> x = (from eh in _context.EventHist
                                 where (eh.EventId == iEventID)
                                      && ((eh.ActnId == (byte)Actions.Publish)
                                                     || (eh.ActnId == (byte)Actions.EventRepublished)
                                                     || (eh.ActnId == (byte)Actions.EventPublishCPECombo))
                                      && ((DateTime.Now - eh.CreatDt).TotalSeconds > 15)
                                 select eh).ToList();
            return (x.Count > 0);
        }

        private DataTable GetEventReviewers(int EventID, int iEventType)
        {
            DataTable dt = null;

            if (EventID > 0)
            {
                if (iEventType == (int)EventType.MDS)
                {
                    var x = (from eh in _context.EventHist
                             join ug in _context.MapUsrPrf on eh.ModfdByUserId equals ug.UserId
                             where ((eh.EventId == EventID)
                                && (ug.RecStusId == Byte.Parse(((int)ERecStatus.Active).ToString())
                                && (ug.UsrPrfId == 132)))
                             select new { REV_USER_ID = eh.ModfdByUserId }).Distinct();
                    dt = LinqHelper.CopyToDataTable(x, null, null);
                }
                else if (iEventType == (int)EventType.UCaaS)
                {
                    var x = (from eh in _context.EventHist
                             join ug in _context.MapUsrPrf on eh.ModfdByUserId equals ug.UserId
                             where ((eh.EventId == EventID)
                                && (ug.RecStusId == Byte.Parse(((int)ERecStatus.Active).ToString())
                                && (ug.UsrPrfId == 174)))
                             select new { REV_USER_ID = eh.ModfdByUserId }).Distinct();
                    dt = LinqHelper.CopyToDataTable(x, null, null);
                }

                return dt;
            }
            else
                return new DataTable();
        }

        private string GetSysCfgValue(string sParamName)
        {
            var q = (from s in _context.LkSysCfg
                     where (s.RecStusId == (byte)ERecStatus.Active && s.PrmtrNme == sParamName)
                     select new
                     {
                         PRMTR_VALU_TXT = s.PrmtrValuTxt
                     }).FirstOrDefault();

            if (q != null)
                return q.PRMTR_VALU_TXT;
            else
                return string.Empty;
        }

        private int GetEmailRequestType(int eventTypeId, bool retract)
        {
            if (eventTypeId == (int)EventType.AD || eventTypeId == (int)EventType.MPLS
                || eventTypeId == (int)EventType.NGVN || eventTypeId == (int)EventType.SprintLink)
            {
                if (retract)
                    return (int)EmailREQType.CANDRetractEmail;

                return (int)EmailREQType.CANDEventEmail;
            }
            else if (eventTypeId == (int)EventType.SIPT)
            {
                if (retract)
                    return (int)EmailREQType.SIPTRetractEmail;

                return (int)EmailREQType.SIPTEventEmail;
            }

            return 0;
        }

        protected bool ValidateEmail(string sEmailAdr)
        {
            if (sEmailAdr.Length > 0)
                if (sEmailAdr.Contains("@") && sEmailAdr.Contains("."))
                    return true;
                else
                    return false;
            else
                return false;
        }

        private string GetEmailSubj(string sEmailTyp, LkEventRule eventRule, string sEventTitle, int iEventID, string sSOWSEventID, string sEmailSubj, string eventType = "", string activityType = "")
        {
            StringBuilder sbSubj = new StringBuilder();

            if ((sEmailTyp.Length > 0) && (sEmailSubj.Trim().Length > 0))
            {
                sbSubj.AppendNoDup(sEmailSubj);
                if (eventType != "")
                {
                    sbSubj.AppendNoDup(eventType + " ");
                }
            }
            else
            {
                sbSubj.Append("(");
                sbSubj.Append(eventRule.StrtEventStus.EventStusDes.ToString());
                sbSubj.Append(" -> ");
                sbSubj.Append(eventRule.WrkflwStus.WrkflwStusDes.ToString());
                sbSubj.Append(" -> ");
                sbSubj.Append(eventRule.EndEventStus.EventStusDes.ToString());
                sbSubj.Append(") ");
            }

            sbSubj.AppendNoDup(sEventTitle == null ? "" : sEventTitle);
            if (activityType != "")
            {
                sbSubj.AppendNoDup(" " + activityType + " ");
            }
            sbSubj.AppendNoDup(" - Event ID: ");
            sbSubj.AppendNoDup(iEventID.ToString());

            return sbSubj.ToString();
        }

        private string GetViewEventURL(byte eventType)
        {
            StringBuilder sbViewEventURL = new StringBuilder();

            sbViewEventURL.AppendNoDup(_configuration.GetSection("AppSettings:ViewEventURL").Value.ToString());

            if (eventType == ((byte)EventType.AD))
                sbViewEventURL.AppendNoDup(@"AccessDelivery");
            if (eventType == ((byte)EventType.NGVN))
                sbViewEventURL.AppendNoDup(@"NGVN");
            if (eventType == ((byte)EventType.MPLS))
                sbViewEventURL.AppendNoDup(@"MPLS");
            if (eventType == ((byte)EventType.SprintLink))
                sbViewEventURL.AppendNoDup(@"SprintLink");
            if (eventType == ((byte)EventType.MDS))
                sbViewEventURL.AppendNoDup(@"MDS");
            if (eventType == ((byte)EventType.UCaaS))
                sbViewEventURL.AppendNoDup(@"UCaaS");
            if (eventType == ((byte)EventType.SIPT))
                sbViewEventURL.AppendNoDup(@"SIPT");

            return sbViewEventURL.ToString().ToLower();
        }

        public DataTable GetCurrentCalData(int iEventID)
        {
            DataTable dt = new DataTable();
            var x = (from y in _context.Appt
                     where y.EventId == iEventID
                     select new { y.EventId, y.AsnToUserIdListTxt, y.StrtTmst, y.EndTmst, y.SubjTxt, y.ApptId });
            var a = x.ToList();
            if (a != null)
                dt = LinqHelper.CopyToDataTable(a, null, null);
            return dt;
        }

        private DataTable GetCurrentEHist(int iEventID)
        {
            DataTable dt = new DataTable();
            var x = (from y in _context.EventHist
                     where y.EventId == iEventID
                     select new { y.EventId, y.ActnId });

            var a = x.ToList();
            if (a != null) dt = LinqHelper.CopyToDataTable(a, null, null);
            return dt;
        }

        private DataTable GetEmailAddr(List<int> UserIDs)
        {
            if (UserIDs != null && UserIDs.Count > 0)
            {
                var x = _userRepository.Find(u => UserIDs.Contains(u.UserId) && (u.UserId != 5001))
                    .Select(u => new { EmailAddr = u.EmailAdr }).Distinct();
                return LinqHelper.CopyToDataTable(x, null, null);
            }
            else
                return new DataTable();
        }

        public DataTable GetCurrentAct(List<EventAsnToUser> assignUsers, int iEventID)
        {
            DataTable dt = new DataTable();
            var x = (from y in _context.EventAsnToUser
                     join lu in _context.LkUser on y.AsnToUserId equals lu.UserId
                     where (y.EventId == iEventID)
                        && (y.RecStusId == ((int)1))
                     select new { y.AsnToUserId, lu.EmailAdr, lu.DsplNme }).ToList();

            if (x.Count > 0)
            {
                dt = LinqHelper.CopyToDataTable(x, null, null);
            }

            return dt;
        }

        private DataSet GetMDSSrvcEmailData(int iEventID)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.getMDSSrvcEmailData", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@EVENT_ID", iEventID); // if you have parameters.

            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;
        }

        private DataSet GetMDSEventData(int iEventID)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.GetMDSEventEmailData", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@iEventID", iEventID); // if you have parameters.

            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;
        }

        private DataSet GetMDSEventDataDetail(int H5_H6, int iEventID, string iHeader, int iFSAEventID, int iID)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.getMDSEventEmailData", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@H5_H6_CUST_ID", 0); // if you have parameters.
            command.Parameters.AddWithValue("@EVENT_ID", iEventID); // if you have parameters.
            command.Parameters.AddWithValue("@TAB_NME", iHeader); // if you have parameters.
            command.Parameters.AddWithValue("@FSA_MDS_EVENT_ID", iFSAEventID); // if you have parameters.
            command.Parameters.AddWithValue("@Tab_Seq_Nbr", iID); // if you have parameters.
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();
            return ds;
        }

        private string CreateMDSEventBody(int EventID, EventWorkflow esw)
        {
            DataSet ds = new DataSet();
            DataTable dtMDSEvent = new DataTable();
            DataTable dtTabs = new DataTable();
            XElement EventEmail = new XElement("EventEmail");
            //StringBuilder sbOldActEmail = new StringBuilder();
            //StringBuilder sbTeamPDL = new StringBuilder();
            bool bSensitiveCust = false;

            ds = GetMDSEventData(EventID);

            if (ds != null)
            {
                dtMDSEvent = ds.Tables[0];
                dtTabs = ds.Tables[1];
            }

            if ((dtMDSEvent != null) && (dtMDSEvent.Rows.Count > 0))
            {
                bSensitiveCust = Convert.ToByte(dtMDSEvent.Rows[0]["CSG_LVL_ID"]) > 0;

                XElement xa = new XElement("MDSEvent",
                            new XElement("EVENT_ID", EventID),
                            new XElement("EVENT_STUS_DES", dtMDSEvent.Rows[0]["EVENT_STUS_DES"]),
                            new XElement("WRKFLW_STUS_DES", dtMDSEvent.Rows[0]["WRKFLW_STUS_DES"]),
                            new XElement("OLD_EVENT_STUS_DES", dtMDSEvent.Rows[0]["OLD_EVENT_STUS_DES"]),
                            new XElement("EVENT_TITLE_TXT", dtMDSEvent.Rows[0]["EVENT_TITLE_TXT"]),
                            new XElement("MDS_FAST_TRK_CD", dtMDSEvent.Rows[0]["MDS_FAST_TRK_CD"]),
                            new XElement("MDS_FAST_TRK_TYPE_DES", dtMDSEvent.Rows[0]["MDS_FAST_TRK_TYPE_DES"]),
                            new XElement("H1", dtMDSEvent.Rows[0]["H1"]),
                            new XElement("CHARS_ID", dtMDSEvent.Rows[0]["CHARS_ID"]),
                            new XElement("CUST_NME", bSensitiveCust ? CustomerMaskedData.customerName : dtMDSEvent.Rows[0]["CUST_NME"]),
                            new XElement("CUST_EMAIL_ADR", dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"]),
                            new XElement("DSGN_DOC_LOC_TXT", dtMDSEvent.Rows[0]["DSGN_DOC_LOC_TXT"]),
                            new XElement("SHRT_DES", dtMDSEvent.Rows[0]["SHRT_DES"]),
                            new XElement("DES_CMNT_TXT", ((ds.Tables[2] != null) && (ds.Tables[2].Rows.Count > 0)) ? ds.Tables[2].Rows[0]["CMNT_TXT"].ToString() : string.Empty),
                            new XElement("MDS_ACTY_TYPE_DES", dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_DES"]),
                            new XElement("MDS_ACTY_TYPE_ID", dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_ID"]),
                            new XElement("MDS_INSTL_ACTY_TYPE_DES", dtMDSEvent.Rows[0]["MDS_INSTL_ACTY_TYPE_DES"]),
                            new XElement("FULL_CUST_DISC_CD", dtMDSEvent.Rows[0]["FULL_CUST_DISC_CD"]),
                            new XElement("DEV_CNT", dtMDSEvent.Rows[0]["DEV_CNT"]),
                            new XElement("DISC_NTFY_PDL_NME", dtMDSEvent.Rows[0]["DISC_NTFY_PDL_NME"]),
                            new XElement("MDS_BRDG_NEED_CD", dtMDSEvent.Rows[0]["MDS_BRDG_NEED_CD"]),
                            new XElement("STRT_TMST", (DateTime.Parse(dtMDSEvent.Rows[0]["STRT_TMST"].ToString()).ToString("MM-dd-yyyy")) + " @ " + (DateTime.Parse(dtMDSEvent.Rows[0]["STRT_TMST"].ToString()).ToString("t"))),
                            new XElement("END_TMST", dtMDSEvent.Rows[0]["END_TMST"]),
                            new XElement("EXTRA_DRTN_TME_AMT", dtMDSEvent.Rows[0]["EXTRA_DRTN_TME_AMT"]),
                            new XElement("CREAT_BY_USER_ID", dtMDSEvent.Rows[0]["CREAT_BY_USER_ID"]),
                            new XElement("MODFD_BY_USER_ID", dtMDSEvent.Rows[0]["MODFD_BY_USER_ID"]),
                            new XElement("CREAT_DT", dtMDSEvent.Rows[0]["CREAT_DT"]),
                            new XElement("CNFRC_BRDG_NBR", dtMDSEvent.Rows[0]["CNFRC_BRDG_NBR"]),
                            new XElement("CNFRC_PIN_NBR", dtMDSEvent.Rows[0]["CNFRC_PIN_NBR"]),
                            new XElement("OnlineMeetingURL", esw.OnlineMeetingAdr),  //pls double check _edb.sMeetUrl
                            new XElement("TME_SLOT_ID", dtMDSEvent.Rows[0]["TME_SLOT_ID"]),
                            new XElement("CalenderEntryURL", _configuration.GetSection("AppSettings:CalenderEntryURL").Value),
                            new XElement("ViewEventURL", _configuration.GetSection("AppSettings:ViewEventURL").Value),
                            new XElement("ChatURL", GetChatURL()),
                            new XElement("RDSGN_NBR", ((ds.Tables[7] != null) && (ds.Tables[7].Rows.Count > 0)) ? ds.Tables[7].Rows[0]["RDSN_NBR"] : string.Empty),
                            new XElement("ModUserName", dtMDSEvent.Rows[0]["MODFD_BY_USER_ID"]),
                            new XElement("AssignUsers", ((ds.Tables[3] != null) && (ds.Tables[3].Rows.Count > 0)) ? ds.Tables[3].Rows[0]["AssignUsers"] : string.Empty),
                            new XElement("AssignPhoneNumbers", ((ds.Tables[8] != null) && (ds.Tables[8].Rows.Count > 0)) ? ds.Tables[8].Rows[0]["AssignUserPhoneNumbers"] : string.Empty),
                            new XElement("RevUserName", ((ds.Tables[6] != null) && (ds.Tables[6].Rows.Count > 0)) ? ds.Tables[6].Rows[0]["REV_USR_NME"] : string.Empty),
                            new XElement("REV_PHN_NBR", ((ds.Tables[6] != null) && (ds.Tables[6].Rows.Count > 0)) ? ds.Tables[6].Rows[0]["REV_PHN_NBR"] : string.Empty),
                            new XElement("ESCL_CD", dtMDSEvent.Rows[0]["ESCL_CD"]),
                            new XElement("ESCL_REAS_DES", dtMDSEvent.Rows[0]["ESCL_REAS_DES"]),
                            new XElement("REQOR_USER", dtMDSEvent.Rows[0]["REQOR_USER"]),
                            new XElement("REQOR_USER_PHN", dtMDSEvent.Rows[0]["REQOR_USER_PHN"]),
                            new XElement("FT_TIME_BLOCK", dtMDSEvent.Rows[0]["TME_SLOT_ID"]),
                            new XElement("SOWS_EVENT_ID", dtMDSEvent.Rows[0]["SOWS_EVENT_ID"]),
                            new XElement("REQOR_USER_EMAIL", dtMDSEvent.Rows[0]["REQOR_USER_EMAIL"]),
                            new XElement("SHIP_TRK_REFR_NBR", dtMDSEvent.Rows[0]["SHIP_TRK_REFR_NBR"]),
                            new XElement("DEV_SERIAL_NBR", dtMDSEvent.Rows[0]["DEV_SERIAL_NBR"])
                            );

                EventEmail.Add(xa);

                sbOldActEmail.Clear();
                DataTable dtEmailAddr = ds.Tables[5];
                if (dtEmailAddr != null)
                    if (dtEmailAddr.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                        {
                            if ((sbOldActEmail.Length > 0) && (sbOldActEmail[sbOldActEmail.Length - 1] != ','))
                                sbOldActEmail.Append(",");
                            sbOldActEmail.AppendNoDup(dtEmailAddr.Rows[i]["EMAIL_ADR"].ToString());
                        }
                    }

                sbTeamPDL.Clear();
                if (dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim().Length > 0)
                    sbTeamPDL.AppendNoDup(dtMDSEvent.Rows[0]["CUST_EMAIL_ADR"].ToString().Trim());

                if ((dtMDSEvent.Rows[0]["MDS_ACTY_TYPE_ID"].ToString().Equals("3")) && (dtMDSEvent.Rows[0]["DISC_NTFY_PDL_NME"].ToString().Trim().Length > 0))
                {
                    if ((sbTeamPDL.Length > 0) && (sbTeamPDL[sbTeamPDL.Length - 1] != ','))
                        sbTeamPDL.Append(",");
                    sbTeamPDL.AppendNoDup(dtMDSEvent.Rows[0]["DISC_NTFY_PDL_NME"].ToString().Trim());
                }

                if ((dtTabs != null) && (dtTabs.Rows.Count > 0))
                {
                    int ID;
                    string Header;
                    int FSA_Event_ID;
                    int H5_H6 = 0;

                    DataTable dtMDSMNSOE = new DataTable();
                    DataTable dtMDSManagedDevices = new DataTable();
                    DataTable dtMDSWiredTransport = new DataTable();
                    DataTable dtMDSWirelessTransport = new DataTable();
                    DataTable dtMDSVirtualConnPVC = new DataTable();
                    DataTable dtMDSMisc = new DataTable();
                    DataTable dtMDSAddr = new DataTable();
                    DataTable dtMDSLOC = new DataTable();

                    DataSet ds1 = new DataSet();

                    for (int i = 0; i < dtTabs.Rows.Count; i++)
                    {
                        ID = Convert.ToInt32(dtTabs.Rows[i]["TAB_SEQ_NBR"].ToString());
                        Header = dtTabs.Rows[i]["TAB_NME"].ToString();
                        FSA_Event_ID = Convert.ToInt32(dtTabs.Rows[i]["FSA_MDS_EVENT_ID"].ToString());

                        ds1 = GetMDSEventDataDetail(H5_H6, EventID, Header, FSA_Event_ID, ID);

                        XElement EventTab = new XElement("Tab");
                        EventTab.SetAttributeValue("TABID", ID);

                        if ((ds1 != null) && (ds1.Tables.Count > 2))
                        {
                            dtMDSMNSOE = ds1.Tables[0];
                            dtMDSManagedDevices = ds1.Tables[1];
                            dtMDSWiredTransport = ds1.Tables[2];
                            dtMDSWirelessTransport = ds1.Tables[3];
                            dtMDSVirtualConnPVC = ds1.Tables[4];
                            dtMDSMisc = ds1.Tables[5];
                            dtMDSAddr = ds1.Tables[6];
                            dtMDSLOC = ds1.Tables[7];

                            for (int b = 0; b < dtMDSMNSOE.Rows.Count; b++)
                            {
                                XElement xb = new XElement("MDSOE",
                                  new XElement("MNS_OE_FTN", dtMDSMNSOE.Rows[b]["MDS_MNS_FTN"]),
                                  new XElement("MDS_SRVC_TIER_DES", dtMDSMNSOE.Rows[b]["MDS_SRVC_TIER_DES"]),
                                  new XElement("MDS_ENTLMNT_DES", dtMDSMNSOE.Rows[b]["MDS_ENTLMNT_DES"])
                                   );

                                EventTab.Add(xb);
                            }

                            for (int c = 0; c < dtMDSManagedDevices.Rows.Count; c++)
                            {
                                XElement xc = new XElement("MDSMagDevices",
                                  new XElement("FTN_CPE", dtMDSManagedDevices.Rows[c]["FTN_CPE"]),
                                  new XElement("DEV_NME", dtMDSManagedDevices.Rows[c]["DEV_NME"]),
                                  new XElement("MANF_NME", dtMDSManagedDevices.Rows[c]["MANF_NME"]),
                                  new XElement("DEV_MODEL_NME", dtMDSManagedDevices.Rows[c]["DEV_MODEL_NME"])
                                   );

                                EventTab.Add(xc);
                            }

                            for (int e = 0; e < dtMDSWiredTransport.Rows.Count; e++)
                            {
                                XElement xe = new XElement("MDSWirTrans",
                                  new XElement("TRPT_FTN", dtMDSWiredTransport.Rows[e]["ACCESS_FTN"]),
                                  new XElement("WdTransportType", dtMDSWiredTransport.Rows[e]["WdTransportType"]),
                                  new XElement("WdPrimaryBackup", dtMDSWiredTransport.Rows[e]["WdPrimaryBackup"]),
                                  new XElement("FMS_CKT_ID", dtMDSWiredTransport.Rows[e]["FMS_CKT_ID"]),
                                  new XElement("PL_NBR", dtMDSWiredTransport.Rows[e]["PL_NBR"]),
                                  new XElement("NUA_449_ADR", dtMDSWiredTransport.Rows[e]["NUA_449_ADR"]),
                                  new XElement("OLD_CKT_ID", dtMDSWiredTransport.Rows[e]["OLD_CKT_ID"]),
                                  new XElement("MULTI_LINK_CKT_CD", dtMDSWiredTransport.Rows[e]["MULTI_LINK_CKT"]),
                                  new XElement("FOC_DT", dtMDSWiredTransport.Rows[e]["FOC_DT"]),
                                  new XElement("NEW_NUA", dtMDSWiredTransport.Rows[e]["NEW_NUA"]),
                                  new XElement("NEW_FMS_CKT_ID", dtMDSWiredTransport.Rows[e]["NEW_FMS_CKT_ID"]),
                                  new XElement("WdSprintManaged", dtMDSWiredTransport.Rows[e]["WdSprintManaged"]),
                                  new XElement("WdTelcoCallout", dtMDSWiredTransport.Rows[e]["WdTelcoCallout"])
                                  );

                                EventTab.Add(xe);
                            }

                            for (int f = 0; f < dtMDSWirelessTransport.Rows.Count; f++)
                            {
                                XElement xf = new XElement("MDSWirlessTran",
                                  new XElement("WlPrimaryBackup", dtMDSWirelessTransport.Rows[f]["WlPrimaryBackup"]),
                                  new XElement("ESN", dtMDSWirelessTransport.Rows[f]["ESN"])
                                   );

                                EventTab.Add(xf);
                            }

                            for (int g = 0; g < dtMDSVirtualConnPVC.Rows.Count; g++)
                            {
                                XElement xg = new XElement("MDSPVC",
                                  new XElement("PVC_ID", dtMDSVirtualConnPVC.Rows[g]["DLCI_VPI_CD"])
                                   );

                                EventTab.Add(xg);
                            }

                            for (int h = 0; h < dtMDSMisc.Rows.Count; h++)
                            {
                                XElement xh = new XElement("MDSMISC",
                                  new XElement("TAB_SEQ_NBR", dtMDSMisc.Rows[h]["TAB_SEQ_NBR"]),
                                  new XElement("TAB_NME", Header),
                                  new XElement("SPRINT_CPE_NCR_DES", dtMDSMisc.Rows[h]["SPRINT_CPE_NCR_DES"]),
                                  new XElement("SRVC_ASSRN_SITE_SUPP_DES", dtMDSMisc.Rows[h]["SRVC_ASSRN_SITE_SUPP_DES"]),
                                  new XElement("CPE_DSPCH_EMAIL_ADR", dtMDSMisc.Rows[h]["CPE_DSPCH_EMAIL_ADR"]),
                                  new XElement("CPE_DSPCH_CMNT_TXT", dtMDSMisc.Rows[h]["CPE_DSPCH_CMNT_TXT"]),
                                  new XElement("TRPT_FTN", ((dtMDSWiredTransport != null) && (dtMDSWiredTransport.Rows.Count > 0)) ? dtMDSWiredTransport.Rows[0]["ACCESS_FTN"] : string.Empty),
                                  new XElement("WIRED_DEV_TRNSPRT_REQR_CD", dtMDSMisc.Rows[h]["WIRED_DEV_TRPT_REQR_CD"]),
                                  new XElement("WRLS_TRNSPRT_REQR_CD", dtMDSMisc.Rows[h]["WRLS_TRNSPRT_REQR_CD"]),
                                  new XElement("VRTL_CNCTN_CD", dtMDSMisc.Rows[h]["VRTL_CNCTN_CD"]),
                                  new XElement("INSTL_SITE_POC_NME", bSensitiveCust ? CustomerMaskedData.installSitePOC : dtMDSMisc.Rows[h]["INSTL_SITE_POC_NME"]),
                                  new XElement("INSTL_SITE_POC_PHN_NBR", bSensitiveCust ? CustomerMaskedData.installSitePOCPhone : dtMDSMisc.Rows[h]["INSTL_SITE_POC_PHN_NBR"]),
                                  new XElement("INSTL_SITE_POC_CELL_PHN_NBR", bSensitiveCust ? CustomerMaskedData.installSitePOCCell : dtMDSMisc.Rows[h]["INSTL_SITE_POC_CELL_PHN_NBR"]),
                                  new XElement("SRVC_ASSRN_POC_NME", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOC : dtMDSMisc.Rows[h]["SRVC_ASSRN_POC_NME"]),
                                  new XElement("SRVC_ASSRN_POC_PHN_NBR", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCPhone : dtMDSMisc.Rows[h]["SRVC_ASSRN_POC_PHN_NBR"]),
                                  new XElement("SRVC_ASSRN_POC_CELL_NBR", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCCell : dtMDSMisc.Rows[h]["SRVC_ASSRN_POC_CELL_NBR"]),
                                  new XElement("SA_POC_HR_NME", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCContactHrs : dtMDSMisc.Rows[h]["SA_POC_HR_NME"]),
                                  new XElement("TME_ZONE_ID", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCTimeZone : dtMDSMisc.Rows[h]["TME_ZONE_ID"])
                                  );

                                EventTab.Add(xh);
                            }

                            for (int l = 0; l < dtMDSAddr.Rows.Count; l++)
                            {
                                XElement xl = new XElement("MDSAddr",
                                  new XElement("STREET_ADR", bSensitiveCust ? CustomerMaskedData.address : dtMDSAddr.Rows[l]["STREET_ADR"]),
                                  new XElement("FLR_BLDG_NME", bSensitiveCust ? CustomerMaskedData.floor : dtMDSAddr.Rows[l]["FLR_BLDG_NME"]),
                                  new XElement("CTY_NME", bSensitiveCust ? CustomerMaskedData.city : dtMDSAddr.Rows[l]["CTY_NME"]),
                                  new XElement("STT_PRVN_NME", bSensitiveCust ? CustomerMaskedData.state : dtMDSAddr.Rows[l]["STT_PRVN_NME"]),
                                  new XElement("CTRY_RGN_NME", bSensitiveCust ? CustomerMaskedData.country : dtMDSAddr.Rows[l]["CTRY_RGN_NME"]),
                                  new XElement("ZIP_CD", bSensitiveCust ? CustomerMaskedData.zipCode : dtMDSAddr.Rows[l]["ZIP_CD"]),
                                  new XElement("US_INTL_ID", dtMDSAddr.Rows[l]["US_INTL_ID"])
                                   );

                                EventTab.Add(xl);
                            }

                            for (int d = 0; d < dtMDSLOC.Rows.Count; d++)
                            {
                                XElement xd = new XElement("MDSLOC",
                                  new XElement("MDS_LOC_TYPE_DES", dtMDSLOC.Rows[d]["MDS_LOC_TYPE_DES"]),
                                  new XElement("MDS_LOC_CAT_DES", dtMDSLOC.Rows[d]["MDS_LOC_CAT_DES"]),
                                  new XElement("MDS_LOC_NBR", dtMDSLOC.Rows[d]["MDS_LOC_NBR"]),
                                  new XElement("MDS_LOC_RAS_DT", dtMDSLOC.Rows[d]["MDS_LOC_RAS_DT"])
                                   );

                                EventTab.Add(xd);
                            }

                            EventEmail.Add(EventTab);
                        }
                    }
                }
            }

            return EventEmail.ToString();
        }

        private string CreateMDSEventBody(MdsEvent MdsEvent, EventWorkflow esw, ICollection<MdsEventNtwkTrptView> networkTransport)
        {
            DataSet ds = new DataSet();
            XElement EventEmail = new XElement("EventEmail");            
            bool bSensitiveCust = false;

            ds = GetMDSEventData(MdsEvent.EventId);

            if ((MdsEvent != null) && (MdsEvent.EventId > 0))
            {
                bSensitiveCust = (MdsEvent.Event.CsgLvlId > 0);

                var mdsFastTrackTypeId = MdsEvent.MdsFastTrkTypeId != null ? MdsEvent.MdsFastTrkTypeId.Trim() : string.Empty;
                var mdsFastTrackTypeDes = mdsFastTrackTypeId != "" ? _MDSFastTrackType.GetById(MdsEvent.MdsFastTrkTypeId).MdsFastTrkTypeDes : string.Empty;
                var mdsActyTypeId = MdsEvent.MdsActyType != null ? MdsEvent.MdsActyTypeId.ToString() : string.Empty;
                var mdsActyTypeDes = mdsActyTypeId != "" ? MdsEvent.MdsActyType.MdsActyTypeDes : string.Empty;
                var discMgmtCd = MdsEvent.DiscMgmtCd != null ? (MdsEvent.DiscMgmtCd == "Y" ? "Yes" : "No") : string.Empty;
                var fullCustDiscCd = MdsEvent.FullCustDiscCd != null ? (MdsEvent.FullCustDiscCd == true ? "Full Disconnect" : "Partial Disconnect") : string.Empty;
                var calendarEntryUrl = _configuration.GetSection("AppSettings:CalenderEntryURL").Value;
                var viewEventUrl = _configuration.GetSection("AppSettings:ViewEventURL").Value;
                var chatUrl = GetChatURL();
                var redsgnNbr = String.Join(";", MdsEvent.Event.MdsEventOdieDev.Select(a => a.RdsnNbr).ToList());
                var assignUsers = esw.AssignUser != null ? esw.AssignUser.Where(a => a.RecStusId == 1).Select(a => a.AsnToUser).ToList() : null;
                var assignUsersDsplNme = assignUsers != null ? String.Join(";", assignUsers.Select(a => a.DsplNme).ToList()) : string.Empty;
                var assignUsersPhnNbr = assignUsers != null ? String.Join(";", assignUsers.Select(a => a.PhnNbr).ToList()) : string.Empty;
                var revUserName = (ds.Tables[5] != null && ds.Tables[5].Rows.Count > 0) ? ds.Tables[5].Rows[0]["REV_USR_NME"] : string.Empty;
                var revPhnNbr = (ds.Tables[5] != null && ds.Tables[5].Rows.Count > 0) ? ds.Tables[5].Rows[0]["REV_PHN_NBR"] : string.Empty;
                var trptFtn = String.Join(";", MdsEvent.Event.EventCpeDev.Select(a => a.CpeOrdrId).ToList());
                var mdsNtwkActyTypeId = (MdsEvent.Event.MplsEventActyType != null) ? String.Join(",", MdsEvent.Event.MplsEventActyType.Select(a => a.MplsActyType.MplsActyTypeId).ToList()) : string.Empty;
                var mdsNtwkActyTypeDes = (MdsEvent.Event.MplsEventActyType != null) ? String.Join(",", MdsEvent.Event.MplsEventActyType.Select(a => a.MplsActyType.MplsActyTypeDes).ToList()) : string.Empty;  // Joshua I cant figure this out pls chck  : monir   (MdsEvent.MPLSActType == null) ? string.Empty : String.Join(",", MdsEvent.MPLSActType.Select(a => a.MPLSACTYTYPE).ToList())
                var eventStatusDes = _eventStatusRepo.GetById(MdsEvent.EventStusId).EventStusDes;
                var workflowStatusDes = _workflowRepo.GetById(MdsEvent.WrkflwStusId).WrkflwStusDes;
                var isCPEDispatchEmail = false;
                if ((ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0))
                {
                    var data = ds.Tables[0].Rows[0];
                    isCPEDispatchEmail = ((data["OLD_EVENT_STUS_DES"].ToString() == "Pending") && (data["EVENT_STUS_DES"].ToString() == "Published") && !string.IsNullOrEmpty(MdsEvent.CpeDspchCmntTxt)) ? true : false;
                }

                int eventId = MdsEvent.EventId;
                string objTypeCd = "E";
                string emailCd = esw.EventRule.EndEventStusId.ToString();

                //_contactDetailRepo.GetEmailAddresses(eventId, objTypeCd, emailCd, ref contactDetailUserIds);

                XElement xa = new XElement("MDSEvent",
                            new XElement("EVENT_ID", MdsEvent.EventId),
                            new XElement("EVENT_STUS_DES", eventStatusDes),
                            new XElement("WRKFLW_STUS_DES", workflowStatusDes),
                            new XElement("OLD_EVENT_STUS_DES", string.Empty),
                            new XElement("EVENT_TITLE_TXT", bSensitiveCust ? "Private Customer" : MdsEvent.EventTitleTxt),
                            new XElement("MDS_FAST_TRK_CD", mdsFastTrackTypeId == "A" ? "True" : "False"),
                            new XElement("MDS_FAST_TRK_TYPE_DES", mdsFastTrackTypeDes),
                            new XElement("MDS_FAST_FRCD_CD", MdsEvent.FrcdftCd.ToString()),
                            //new XElement("MDS_FAST_TRK_CD", (MdsEvent.MdsFastTrkTypeId.Length > 0).ToString()),
                            //new XElement("MDS_FAST_TRK_TYPE_DES", _MDSFastTrackType.GetById(MdsEvent.MdsFastTrkTypeId)),
                            new XElement("H1", MdsEvent.H1),
                            new XElement("CHARS_ID", MdsEvent.CharsId),
                            new XElement("CUST_NME", bSensitiveCust ? CustomerMaskedData.customerName : MdsEvent.CustNme),
                            new XElement("CUST_EMAIL_ADR", MdsEvent.CustAcctTeamPdlNme),
                            new XElement("DSGN_DOC_LOC_TXT", MdsEvent.CustSowLocTxt),
                            new XElement("SHRT_DES", bSensitiveCust && isCPEDispatchEmail ? CustomerMaskedData.blank : ReplaceBRwithNewline(MdsEvent.ShrtDes)),
                            new XElement("DES_CMNT_TXT", ReplaceBRwithNewline(MdsEvent.CmntTxt)),
                            new XElement("MDS_ACTY_TYPE_DES", mdsActyTypeDes),
                            new XElement("MDS_ACTY_TYPE_ID", mdsActyTypeId),
                            //new XElement("MDS_ACTY_TYPE_DES", MdsEvent.MdsActyType.MdsActyTypeDes),
                            //new XElement("MDS_ACTY_TYPE_ID", MdsEvent.MdsActyTypeId),
                            new XElement("DISC_MGMT_CD", discMgmtCd),
                            new XElement("FULL_CUST_DISC_CD", fullCustDiscCd),
                            new XElement("FULL_CUST_DISC_REAS_TXT", (MdsEvent.FullCustDiscReasTxt ?? "")),
                            new XElement("DEV_CNT", MdsEvent.Event.EventCpeDev.Count),     ///  Joshua I cant figure this out pls chck  : monir   MdsEvent.MDSCPEDev.Count
                            new XElement("DISC_NTFY_PDL_NME", MdsEvent.CustAcctTeamPdlNme),
                            new XElement("MDS_BRDG_NEED_CD", MdsEvent.CnfrcPinNbr),    // CnfrcBrdgId ?  or CnfrcBrdgNbr ?   pls chck  : monir
                            new XElement("STRT_TMST", String.Format("{0:MM/dd/yyyy h:mm tt}", MdsEvent.StrtTmst)),
                            new XElement("END_TMST", String.Format("{0:MM/dd/yyyy h:mm tt}", MdsEvent.EndTmst)),
                            new XElement("EXTRA_DRTN_TME_AMT", MdsEvent.ExtraDrtnTmeAmt),
                            new XElement("CREAT_BY_USER_ID", MdsEvent.CreatByUser.FullNme),
                            new XElement("MODFD_BY_USER_ID", MdsEvent.ModfdByUser != null ? MdsEvent.ModfdByUser.FullNme : ""),
                            new XElement("CREAT_DT", MdsEvent.CreatDt),
                            new XElement("CNFRC_BRDG_NBR", MdsEvent.CnfrcBrdgNbr),
                            new XElement("CNFRC_PIN_NBR", MdsEvent.CnfrcPinNbr),
                            new XElement("OnlineMeetingURL", MdsEvent.OnlineMeetingAdr),
                            new XElement("TME_SLOT_ID", MdsEvent.TmeSlotId),
                            new XElement("CalenderEntryURL", calendarEntryUrl),
                            new XElement("ViewEventURL", viewEventUrl),
                            new XElement("ChatURL", chatUrl),
                            new XElement("RDSGN_NBR", redsgnNbr),  ///  Joshua I cant figure this out pls chck  : monir   String.Join(";", MdsEvent.MDSODIEDev.Select(a => a.RedsnOdieDev.RdsnNbr).ToList())
                            new XElement("ModUserName", MdsEvent.ModfdByUser != null ? MdsEvent.ModfdByUser.FullNme : ""),
                            new XElement("AssignUsers", assignUsersDsplNme),
                            new XElement("AssignPhoneNumbers", assignUsersPhnNbr),
                            new XElement("RevUserName", revUserName),
                            new XElement("REV_PHN_NBR", revPhnNbr),
                            new XElement("ESCL_CD", MdsEvent.EsclCd),
                            new XElement("ESCL_REAS_DES", MdsEvent.EsclReas != null ? MdsEvent.EsclReas.EsclReasDes : string.Empty),
                            new XElement("ESCL_BUS_JUSTN_TXT", MdsEvent.EsclBusJustnTxt != null ? MdsEvent.EsclBusJustnTxt : string.Empty),
                            new XElement("PRIM_REQ_DT", String.Format("{0:MM/dd/yyyy h:mm tt}", MdsEvent.PrimReqDt)),
                            new XElement("SCNDY_REQ_DT", String.Format("{0:MM/dd/yyyy h:mm tt}", MdsEvent.ScndyReqDt)),
                            //new XElement("ESCL_REAS_DES", MdsEvent.EsclReas.EsclReasDes),
                            new XElement("REQOR_USER", MdsEvent.ReqorUser.FullNme),
                            new XElement("REQOR_USER_PHN", MdsEvent.ReqorUser.PhnNbr),
                            new XElement("FT_TIME_BLOCK", MdsEvent.TmeSlotId), ///  Joshua I cant figure this out pls chck  : monir MdsEvent.Schedule.TimeSlotDesc
                            new XElement("SOWS_EVENT_ID", esw.SOWSEventID),
                            new XElement("REQOR_USER_EMAIL", MdsEvent.ReqorUser.EmailAdr),
                            new XElement("SHIP_TRK_REFR_NBR", MdsEvent.ShipTrkRefrNbr),
                            new XElement("SHIP_CXR_NME", MdsEvent.ShipCxrNme),
                            new XElement("DEV_SERIAL_NBR", MdsEvent.ShipDevSerialNbr),
                            new XElement("SPRINT_CPE_NCR_DES", MdsEvent.SprintCpeNcr),
                            new XElement("CPE_DSPCH_EMAIL_ADR", MdsEvent.CpeDspchEmailAdr),
                            new XElement("CPE_DSPCH_CMNT_TXT", bSensitiveCust && isCPEDispatchEmail ? CustomerMaskedData.blank : MdsEvent.CpeDspchCmntTxt),
                            new XElement("TRPT_FTN", trptFtn), ///  Joshua I cant figure this out pls chck  : monir String.Join(";", MdsEvent.MDSCPEDev.Select(a => a.CPEOrdrID).ToList())
                            new XElement("INSTL_SITE_POC_NME", bSensitiveCust ? CustomerMaskedData.installSitePOC : MdsEvent.InstlSitePocNme),
                            new XElement("INSTL_SITE_POC_PHN_NBR", bSensitiveCust ? CustomerMaskedData.installSitePOCPhone : MdsEvent.InstlSitePocPhnNbr),
                            new XElement("INSTL_SITE_POC_CELL_PHN_NBR", bSensitiveCust ? CustomerMaskedData.installSitePOCCell : MdsEvent.InstlSitePocCellPhnNbr),
                            new XElement("SRVC_ASSRN_POC_NME", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOC : MdsEvent.SrvcAssrnPocNme),
                            new XElement("SRVC_ASSRN_POC_PHN_NBR", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCPhone : MdsEvent.SrvcAssrnPocPhnNbr),
                            new XElement("SRVC_ASSRN_POC_CELL_NBR", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCCell : MdsEvent.SrvcAssrnPocCellPhnNbr),
                            new XElement("SA_POC_HR_NME", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCContactHrs : MdsEvent.SrvcAvlbltyHrs), ///  Joshua I cant figure this out pls chck  : monir MdsEvent.SrvcAssurncePOC.ContactItems[EventContactType.ContactHrs].Description
                            new XElement("TME_ZONE_ID", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCTimeZone : MdsEvent.SrvcTmeZnCd),///  Joshua I cant figure this out pls chck  : monir MdsEvent.SrvcAssurncePOC.ContactItems[EventContactType.TimeZone].Description
                            //new XElement("SA_POC_HR_NME", ""), ///  Joshua I cant figure this out pls chck  : monir MdsEvent.SrvcAssurncePOC.ContactItems[EventContactType.ContactHrs].Description
                            //new XElement("TME_ZONE_ID", ""),///  Joshua I cant figure this out pls chck  : monir MdsEvent.SrvcAssurncePOC.ContactItems[EventContactType.TimeZone].Description
                            new XElement("STREET_ADR", bSensitiveCust ? CustomerMaskedData.address : MdsEvent.StreetAdr),
                            new XElement("FLR_BLDG_NME", bSensitiveCust ? CustomerMaskedData.floor : MdsEvent.FlrBldgNme),
                            new XElement("CTY_NME", bSensitiveCust ? CustomerMaskedData.city : MdsEvent.CtyNme),
                            new XElement("STT_PRVN_NME", bSensitiveCust ? CustomerMaskedData.state : MdsEvent.SttPrvnNme),
                            new XElement("CTRY_RGN_NME", bSensitiveCust ? CustomerMaskedData.country : MdsEvent.CtryRgnNme),
                            new XElement("ZIP_CD", bSensitiveCust ? CustomerMaskedData.zipCode : MdsEvent.ZipCd),
                            new XElement("ALT_SHIP_POC_NME", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOC : MdsEvent.AltShipPocNme),
                            new XElement("ALT_SHIP_POC_PHN_NBR", (bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCPhone : MdsEvent.AltShipPocPhnNbr)),
                            new XElement("ALT_SHIP_POC_EMAIL", (bSensitiveCust ? CustomerMaskedData.customerEmailAddress : MdsEvent.AltShipPocEmail)),
                            new XElement("ALT_SHIP_STREET_ADR", bSensitiveCust ? CustomerMaskedData.address : MdsEvent.AltShipStreetAdr),
                            new XElement("ALT_SHIP_FLR_BLDG_NME", bSensitiveCust ? CustomerMaskedData.floor : MdsEvent.AltShipFlrBldgNme),
                            new XElement("ALT_SHIP_CTY_NME", bSensitiveCust ? CustomerMaskedData.city : MdsEvent.AltShipCtyNme),
                            new XElement("ALT_SHIP_CTRY_RGN_NME", bSensitiveCust ? CustomerMaskedData.country : MdsEvent.AltShipCtryRgnNme),
                            new XElement("ALT_SHIP_ZIP_CD", bSensitiveCust ? CustomerMaskedData.zipCode : MdsEvent.AltShipCtryRgnNme),
                            new XElement("US_INTL_ID", MdsEvent.UsIntlCd != null ? (MdsEvent.UsIntlCd.Equals("D") ? "US" : "International") : string.Empty),
                            //new XElement("US_INTL_ID", MdsEvent.UsIntlCd.Equals("D") ? "US" : "International"),
                            new XElement("CPE_ORDR_CD", MdsEvent.CpeOrdrCd.ToString()),
                            new XElement("MNS_ORDR_CD", MdsEvent.MnsOrdrCd.ToString()),
                            //new XElement("NTWK_ACTY_TYPE_ID", MdsEvent.NtwkActyTypeId),
                            new XElement("NTWK_ACTY_TYPE", ((MdsEvent.Event.MdsEventNtwkActy == null) ? string.Empty : String.Join("+", MdsEvent.Event.MdsEventNtwkActy.Select(a => a.NtwkActyType.NtwkActyTypeDes).ToList()))),
                            new XElement("NTWK_H6", MdsEvent.NtwkH6),
                            new XElement("NTWK_H1", MdsEvent.NtwkH1),
                            new XElement("NTWK_CUST_NME", bSensitiveCust ? CustomerMaskedData.customerName : MdsEvent.NtwkCustNme),
                            new XElement("MDS_NTWK_ACTY_TYPE", mdsNtwkActyTypeDes),
                            new XElement("NTWK_EVENT_TYPE", MdsEvent.NtwkEventType),
                            new XElement("VPN_PLTFRM_TYPE", MdsEvent.VpnPltfrmType),
                            new XElement("MDSCmpltCd", MdsEvent.MdsCmpltCd.ToString()),
                            new XElement("NtwkCmpltCd", MdsEvent.NtwkCmpltCd.ToString()),
                            new XElement("VASCmpltCd", MdsEvent.VasCmpltCd.ToString()),
                            //new XElement("SoftAssignCd", MdsEvent.SoftAssignCd.ToString()),
                            new XElement("CEFlg", mdsNtwkActyTypeId.Contains("20").ToString()),
                            new XElement("CEChngFlg", mdsNtwkActyTypeId.Contains("21").ToString()),
                            new XElement("VASCECd", GetVASCECd(MdsEvent, mdsNtwkActyTypeId).ToString()),
                            new XElement("RltdCmpNCRCd", (MdsEvent.RltdCmpsNcrCd == null) ? string.Empty : MdsEvent.RltdCmpsNcrCd.ToString()),
                            new XElement("RltdCmpNCR", (MdsEvent.RltdCmpsNcrNme == null) ? string.Empty : MdsEvent.RltdCmpsNcrNme.ToString()),
                            new XElement("IPMDesc", MdsEvent.IpmDes),
                            new XElement("WOOB_IP_ADR", MdsEvent.WoobIpAdr),
                            new XElement("MDR_NUMBER", MdsEvent.MdrNumber)
                            //new XElement("CONTACT_DETAIL_USER_IDS", string.Join(',', contactDetailUserIds.Distinct()
                            );

                EventEmail.Add(xa);

                sbOldActEmail.Clear();
                DataTable dtEmailAddr = ds.Tables[4];
                if (dtEmailAddr != null)
                    if (dtEmailAddr.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                        {
                            if ((sbOldActEmail.Length > 0) && (sbOldActEmail[sbOldActEmail.Length - 1] != ','))
                                sbOldActEmail.Append(",");
                            sbOldActEmail.AppendNoDup(dtEmailAddr.Rows[i]["EMAIL_ADR"].ToString());
                        }
                    }

                sbTeamPDL.Clear();
                if (MdsEvent.CustAcctTeamPdlNme != null)
                {
                    if (MdsEvent.CustAcctTeamPdlNme.Trim().Length > 0)
                        if ((sbTeamPDL.Length > 0) && (sbTeamPDL[sbTeamPDL.Length - 1] != ','))
                            sbTeamPDL.Append(",");
                    sbTeamPDL.AppendNoDup(MdsEvent.CustAcctTeamPdlNme.Trim());
                }

                var eventDiscoDev = MdsEvent.Event.EventDiscoDev.Where(a => a.RecStusId == 1).ToList();
                if ((MdsEvent.MdsActyTypeId == 3) && eventDiscoDev.Count > 0)
                {
                    // Event.MDS_EVENT_DISCO_DEV
                    foreach (EventDiscoDev mdo in eventDiscoDev)
                    {
                        XElement xmdo = new XElement("MDSDiscODIEDev",
                                      new XElement("ODIEDevNme", mdo.OdieDevNme),
                                      new XElement("H5H6", mdo.H5H6),
                                      new XElement("SiteID", mdo.SiteId),
                                      new XElement("DeviceID", mdo.DeviceId),
                                      new XElement("DevManfName", mdo.Manf.ManfNme),
                                      new XElement("DevModelName", mdo.DevModel.DevModelNme),
                                      new XElement("SerialNbr", mdo.SerialNbr),
                                      new XElement("ThirdPartyCtrct", mdo.ThrdPartyCtrct)
                                      );
                        EventEmail.Add(xmdo);
                    }
                }
                else if (MdsEvent.MdsActyTypeId != 3)
                {
                    // Event.MDS_EVENT_ODIE_DEV
                    foreach (MdsEventOdieDev mod in MdsEvent.Event.MdsEventOdieDev)
                    {
                        XElement xmod = new XElement("MDSODIEDev",
                                  new XElement("RdsnNbr", mod.RdsnNbr),
                                  new XElement("RdsnExpDt", String.Format("{0:MM/dd/yyyy h:mm tt}", mod.RdsnExpDt)),
                                  new XElement("ODIEDevNme", mod.OdieDevNme),
                                  //new XElement("DevManfName", mod.Manf.ManfNme),
                                  new XElement("DevManfName", mod != null ? mod.Manf != null ? mod.Manf.ManfNme : "" : ""),
                                  //new XElement("DevModelName", mod.DevModel.DevModelNme),
                                  new XElement("DevModelName", mod != null ? mod.DevModel != null ? mod.DevModel.DevModelNme : "" : ""),
                                  new XElement("FrwlSctyCD", mod.FrwlProdCd),   ///mod.FrwlSctyCd  same as FrwlProdCd
                                  new XElement("FastTrkCD", mod.FastTrkCd),
                                  new XElement("OptOutCD", mod.OptoutCd),
                                  new XElement("IntlCD", mod.IntlCtryCd),
                                  new XElement("DediTN", mod.PhnNbr),
                                  new XElement("WOOBCD", mod.WoobCd),
                                  //new XElement("EntityMaint", mod.SrvcAssrnSiteSupp.SrvcAssrnSiteSuppDes)
                                  new XElement("EntityMaint", mod != null ? mod.SrvcAssrnSiteSupp != null ? mod.SrvcAssrnSiteSupp.SrvcAssrnSiteSuppDes : "" : "")
                                  );
                        EventEmail.Add(xmod);
                    }

                    var EventDevCmplt = MdsEvent.Event.EventDevCmplt.Where(x => x.RecStusId == 1).ToList();
                    foreach (EventDevCmplt dci in EventDevCmplt)   //Event.EVENT_DEV_CMPLT
                    {
                        XElement xmst = new XElement("MDSDevCmpltTbl",
                                  new XElement("ODIEDevNme", dci.OdieDevNme),
                                  new XElement("ODIEH6", dci.H6),
                                  new XElement("ActvCmpltCd", dci.CmpltdCd)
                                  );
                        EventEmail.Add(xmst);
                    }

                    // Event.EVENT_CPE_DEV
                    var EventCpeDev = MdsEvent.Event.EventCpeDev.Where(x => x.RecStusId == 1).ToList();
                    foreach (EventCpeDev mcd in EventCpeDev)
                    {
                        XElement xmcd = new XElement("MDSCPEDev",
                                  new XElement("CPEOrdrID", mcd.CpeOrdrId),
                                  new XElement("CPEOrdrNbr", mcd.CpeOrdrNbr),
                                  new XElement("DeviceID", mcd.DeviceId),
                                  new XElement("AssocH6", mcd.AssocH6),
                                  new XElement("CCD", String.Format("{0:MM/dd/yyyy}", mcd.Ccd)),
                                  new XElement("CmpntStus", mcd.OrdrStus),  /// CmpntStus  same as OrdrStus  ??
                                  new XElement("RcptStus", mcd.RcptStus),
                                  new XElement("OdieDevNme", mcd.OdieDevNme),
                                  new XElement("CtrctType", "")   ////CtrctType  no column such as
                                  );
                        EventEmail.Add(xmcd);
                    }

                    // Event.MdsEventDslSbicCustTrpt
                    foreach (MdsEventDslSbicCustTrpt mct in MdsEvent.Event.MdsEventDslSbicCustTrpt.Where(x=>x.RecStusId == 1).ToList())
                    {
                        XElement xmct = new XElement("MDSCustTrptInfo",
                                  new XElement("MDSTrptType", mct.MdsTrnsprtType),
                                  new XElement("PrimBkpCD", (mct.PrimBkupCd.Equals("P") ? "Primary" : (mct.PrimBkupCd.Equals("B") ? "Backup" : string.Empty))),
                                  new XElement("VndrPrvdrTrptCktID", mct.VndrPrvdrTrptCktId),
                                  new XElement("VndrPrvdrNme", mct.VndrPrvdrNme),
                                  new XElement("WrlsCD", mct.IsWirelessCd),
                                  new XElement("BwdEsn", mct.BwEsnMeid),
                                  new XElement("SCANbr", mct.ScaNbr),
                                  new XElement("IPAdr", mct.IpAdr),
                                  new XElement("SubnetMask", mct.SubnetMaskAdr),
                                  new XElement("NhopGwy", mct.NxthopGtwyAdr),
                                  new XElement("MngdCD", (mct.SprintMngdCd.Equals("S") ? "T-Mobile" : (mct.SprintMngdCd.Equals("C") ? "Customer" : string.Empty))),
                                  new XElement("ODIEDevNme", mct.OdieDevNme)
                                  );
                        EventEmail.Add(xmct);
                    }

                    // Event.MdsEventSlnkWiredTrpt
                    foreach (MdsEventSlnkWiredTrpt mwt in MdsEvent.Event.MdsEventSlnkWiredTrpt.Where(x => x.RecStusId == 1).ToList())
                    {
                        XElement xmwt = new XElement("MDSSlnkWrdTrptInfo",
                                  new XElement("H6", mwt.H6),
                                  new XElement("MDSTrptType", mwt.MdsTrnsprtType),
                                  new XElement("PrimBkpCD", (mwt.PrimBkupCd.Equals("P") ? "Primary" : (mwt.PrimBkupCd.Equals("B") ? "Backup" : string.Empty))),
                                  new XElement("VndrPrvdrTrptCktID", mwt.VndrPrvdrTrptCktId),
                                  new XElement("VndrPrvdrNme", mwt.VndrPrvdrNme),
                                  new XElement("Telco", mwt.VndrPrvdrNme),  ////mwt.Telco   no column such
                                  new XElement("BdwChnlNme", mwt.BdwdChnlNme),
                                  new XElement("PLNbr", mwt.PlNbr),
                                  new XElement("IPNUA", mwt.IpNuaAdr),
                                  new XElement("OldCktID", mwt.OldCktId),
                                  new XElement("MultiLnkCktCD", mwt.MultiLinkCktCd),
                                  new XElement("DedAccsCD", mwt.DedAccsCd),
                                  new XElement("FMSNbr", mwt.FmsNbr),
                                  new XElement("DLCIVPIVCI", mwt.DlciVpiVci),
                                  new XElement("VLANNbr", mwt.VlanNbr),
                                  new XElement("SPANUA", mwt.SpaNua),
                                  new XElement("MngdCD", (mwt.SprintMngdCd.Equals("S") ? "T-Mobile" : (mwt.SprintMngdCd.Equals("C") ? "Customer" : string.Empty))),
                                  new XElement("ODIEDevNme", mwt.OdieDevNme)
                                  );
                        EventEmail.Add(xmwt);
                    }

                    // Event.MDS_EVENT_SITE_SRVC
                    foreach (MdsEventSiteSrvc mst in MdsEvent.Event.MdsEventSiteSrvc)
                    {

                        mst.SrvcType = mst.SrvcTypeId != 0 ? _context.LkMdsSrvcType.Where(x => x.SrvcTypeId == mst.SrvcTypeId).FirstOrDefault() : null;
                        mst.ThrdPartyVndr = mst.ThrdPartyVndrId != 0 ? _context.LkMds3rdpartyVndr.Where(x => x.ThrdPartyVndrId == mst.ThrdPartyVndrId).FirstOrDefault() : null;
                        mst.ThrdPartySrvcLvl = mst.ThrdPartySrvcLvlId != 0 ? _context.LkMds3rdpartySrvcLvl.Where(x => x.ThrdPartySrvcLvlId == mst.ThrdPartySrvcLvlId).FirstOrDefault() : null;

                        XElement xmst = new XElement("MDSSrvcTbl",
                                  new XElement("Mach5H6SrvcID", mst.Mach5SrvcOrdrId),
                                  new XElement("SrvcType", mst.SrvcType != null ? mst.SrvcType.SrvcTypeDes : ""),
                                  new XElement("ThirdPartyVndr", mst.ThrdPartyVndr!= null ? mst.ThrdPartyVndr.ThrdPartyVndrDes : ""),
                                  new XElement("ThirdParty", mst.ThrdPartyId),
                                  new XElement("ThirdPartySrvcLvl", mst.ThrdPartySrvcLvl != null ? mst.ThrdPartySrvcLvl.ThrdPartySrvcLvlDes : ""),
                                  new XElement("ActvnDt", mst.ActvDt),
                                  new XElement("Comments", mst.CmntTxt),
                                  new XElement("EmailFlg", mst.EmailCd),
                                  new XElement("ODIEDevNme", mst.OdieDevNme)
                                  );
                        EventEmail.Add(xmst);
                    }

                    // Event.EVENT_DEV_SRVC_MGMT
                    if (esw.MdsEventDevSrvcMgmtView != null)
                    {
                        foreach (MdsEventDevSrvcMgmtView dms in esw.MdsEventDevSrvcMgmtView.Where(x => x.RecStusId == 1).ToList())
                        {
                            XElement xmst = new XElement("MDSDevMgmtTbl",
                                      new XElement("MNSOrdrNbr", dms.MnsOrdrNbr),
                                      new XElement("DeviceID", dms.DeviceId),
                                      new XElement("CmpntID", dms.M5OrdrCmpntId),
                                      new XElement("CmpntStus", dms.CmpntStus),
                                      new XElement("CmpntType", dms.CmpntTypeCd),
                                      new XElement("CmpntNme", dms.CmpntNme),
                                      new XElement("RltdCmpntID", dms.RltdCmpntID),
                                      new XElement("ServiceTier", dms.MnsSrvcTier),
                                      new XElement("ODIEDevNme", dms.OdieDevNme)
                                      );
                            EventEmail.Add(xmst);
                        }
                    }

                    // Event.EVENT_DEV_SRVC_MGMT
                    if (esw.MdsEventRltdDevSrvcMgmtView != null)
                    {
                        foreach (MdsEventDevSrvcMgmtView rdms in esw.MdsEventRltdDevSrvcMgmtView.Where(x => x.RecStusId == 1).ToList())
                        {
                            XElement xrmst = new XElement("MDSRelDevMgmtTbl",
                                      new XElement("MNSOrdrNbr", rdms.MnsOrdrNbr),
                                      new XElement("DeviceID", rdms.DeviceId),
                                      new XElement("CmpntID", rdms.M5OrdrCmpntId),
                                      new XElement("CmpntStus", rdms.CmpntStus),
                                      new XElement("CmpntType", rdms.CmpntTypeCd),
                                      new XElement("CmpntNme", rdms.CmpntNme),
                                      new XElement("RltdCmpntID", rdms.RltdCmpntID),
                                      new XElement("ServiceTier", rdms.MnsSrvcTier),
                                      new XElement("ODIEDevNme", rdms.OdieDevNme)
                                      );
                            EventEmail.Add(xrmst);
                        }
                    }

                    // Event.MDS_EVENT_WRLS_TRNSPRT
                    foreach (MdsEventWrlsTrpt mwl in MdsEvent.Event.MdsEventWrlsTrpt)
                    {
                        XElement xmwl = new XElement("MDSWrlsTrptTbl",
                                  new XElement("WrlsTypeCD", mwl.WrlsTypeCd),
                                  new XElement("PrimBkpCD", (mwl.PrimBkupCd.Equals("P") ? "Primary" : (mwl.PrimBkupCd.Equals("B") ? "Backup" : string.Empty))),
                                  new XElement("ESN", mwl.EsnMacId),
                                  new XElement("BillAcctNme", mwl.BillAcctNme),
                                  new XElement("BillAcctNbr", mwl.BillAcctNbr),
                                  new XElement("AcctPin", mwl.AcctPin),
                                  new XElement("SalsCD", mwl.SalsCd),
                                  new XElement("SOC", mwl.Soc),
                                  new XElement("StaticIPAdr", mwl.StaticIpAdr),
                                  new XElement("IMEI", mwl.Imei),
                                  new XElement("UICCIDNbr", mwl.UiccIdNbr),
                                  new XElement("PricePln", mwl.PricePln),
                                  new XElement("ODIEDevNme", mwl.OdieDevNme)
                                  );
                        EventEmail.Add(xmwl);
                    }

                    // Event.MDS_EVENT_PORT_BNDWD
                    var MdsEventPortBndwd = MdsEvent.Event.MdsEventPortBndwd.Where(x => x.RecStusId == 1).ToList();
                    foreach (MdsEventPortBndwd mpb in MdsEventPortBndwd)
                    {
                        XElement xmst = new XElement("MDSPortBndwd",
                                  new XElement("M5OrdrNbr", mpb.M5OrdrNbr),
                                  new XElement("M5CmpntID", mpb.M5OrdrCmpntId),
                                  new XElement("NUA", mpb.Nua),
                                  new XElement("PortBndwd", mpb.PortBndwd),
                                  new XElement("ODIEDevNme", mpb.OdieDevNme)
                                  );
                        EventEmail.Add(xmst);
                    }

                    // Event.MDS_EVENT_NTWK_CUST
                    foreach (MdsEventNtwkCust mnc in MdsEvent.Event.MdsEventNtwkCust)
                    {
                        XElement xmnc = new XElement("MDSNtwkCust",
                                  new XElement("CustCntct", bSensitiveCust ? CustomerMaskedData.customerName : mnc.InstlSitePocNme),
                                  new XElement("CntctRole", mnc.RoleNme),
                                  new XElement("CntctEmail", bSensitiveCust ? CustomerMaskedData.customerEmailAddress : mnc.InstlSitePocEmail),
                                  new XElement("CntctPhn", bSensitiveCust ? CustomerMaskedData.installSitePOCPhone : mnc.InstlSitePocPhn)
                                  );
                        EventEmail.Add(xmnc);
                    }

                    if (networkTransport != null)
                    {
                        foreach (MdsEventNtwkTrptView mnt in networkTransport.Where(a => a.RecStusId == 1).ToList())
                        {
                            // Update by Sarah Sandoval [20210318] - Removed LECCntctInfo, VLANID and CE_SRVC_ID
                            XElement xmnt = new XElement("MDSNtwkTrpt",
                                      new XElement("TrptType", mnt.MdsTrnsprtType),
                                      new XElement("LECPrvdrNme", mnt.LecPrvdrNme),
                                      //new XElement("LECCntctInfo", mnt.LecCntctInfo),
                                      new XElement("M5OrdrNbr", mnt.Mach5OrdrNbr),
                                      new XElement("NtwkNUA", mnt.IpNuaAdr),
                                      new XElement("AccsBnd", mnt.MplsAccsBdwd),
                                      new XElement("SCANbr", mnt.ScaNbr),
                                      new XElement("Tagged", mnt.TaggdCd),
                                      //new XElement("VLANID", mnt.VlanNbr),
                                      new XElement("MultiVRF", mnt.MultiVrfReq),
                                      new XElement("IPVer", mnt.IpVer),
                                      new XElement("LocCity", bSensitiveCust ? CustomerMaskedData.city : mnt.LocCity),
                                      new XElement("LocState", bSensitiveCust ? CustomerMaskedData.state : mnt.LocSttPrvn),
                                      new XElement("LocCtry", bSensitiveCust ? CustomerMaskedData.country : mnt.LocCtry),
                                      new XElement("AssocH6", mnt.AssocH6),
                                      new XElement("DD_APRVL_NBR", mnt.DdAprvlNbr),
                                      //new XElement("SALS_ENGR_EMAIL", mnt.SalsEngrEmail),
                                      //new XElement("SALS_ENGR_PHN", mnt.SalsEngrPhn),
                                      new XElement("BDWD_NME", mnt.BdwdNme),
                                      new XElement("CE_SRVC_ID", mnt.CeSrvcId),
                                      // km967761 - 07/30/2011 - Added NID info
                                      new XElement("NID_SERIAL_NBR", mnt.NIDSerialNbr),
                                      new XElement("NID_HOSTNAME", mnt.NidHostName),
                                      new XElement("NID_IP", mnt.NidIpAdr),
                                      // km967761 - 08/09/2021 - Added VAS Type
                                      new XElement("VasType", mnt.VASType)
                                      );
                            EventEmail.Add(xmnt);
                        }
                    }

                    //foreach (MdsEventNtwkTrpt mnt in MdsEvent.Event.MdsEventNtwkTrpt.Where(x => x.RecStusId == 1).ToList())   ///Event.MDS_EVENT_NTWK_TRPT
                    //{
                    //    // Update by Sarah Sandoval [20210318] - Removed LECCntctInfo, VLANID and CE_SRVC_ID
                    //    XElement xmnt = new XElement("MDSNtwkTrpt",
                    //              new XElement("TrptType", mnt.MdsTrnsprtType),
                    //              new XElement("LECPrvdrNme", mnt.LecPrvdrNme),
                    //              //new XElement("LECCntctInfo", mnt.LecCntctInfo),
                    //              new XElement("M5OrdrNbr", mnt.Mach5OrdrNbr),
                    //              new XElement("NtwkNUA", mnt.IpNuaAdr),
                    //              new XElement("AccsBnd", mnt.MplsAccsBdwd),
                    //              new XElement("SCANbr", mnt.ScaNbr),
                    //              new XElement("Tagged", mnt.TaggdCd),
                    //              //new XElement("VLANID", mnt.VlanNbr),
                    //              new XElement("MultiVRF", mnt.MultiVrfReq),
                    //              new XElement("IPVer", mnt.IpVer),
                    //              new XElement("LocCity", bSensitiveCust ? CustomerMaskedData.city : mnt.LocCity),
                    //              new XElement("LocState", bSensitiveCust ? CustomerMaskedData.state : mnt.LocSttPrvn),
                    //              new XElement("LocCtry", bSensitiveCust ? CustomerMaskedData.country : mnt.LocCtry),
                    //              new XElement("AssocH6", mnt.AssocH6),
                    //              new XElement("DD_APRVL_NBR", mnt.DdAprvlNbr),
                    //              //new XElement("SALS_ENGR_EMAIL", mnt.SalsEngrEmail),
                    //              //new XElement("SALS_ENGR_PHN", mnt.SalsEngrPhn),
                    //              new XElement("BDWD_NME", mnt.BdwdNme),
                    //              new XElement("CE_SRVC_ID", mnt.CeSrvcId)
                    //              );
                    //    EventEmail.Add(xmnt);
                    //}
                }
            }

            return EventEmail.ToString();
        }

        private string ReplaceBRwithNewline(string txtVal)
        {
            string newText = "";
            if (!string.IsNullOrWhiteSpace(txtVal))
            {
                // Create regular expressions    
                Regex regex = new Regex(@"(<br />|<br/>|</ br>|</br>|\n)");
                // Replace new line with <br/> tag    
                newText = regex.Replace(txtVal, Environment.NewLine);
            }
            
            // Result    
            return newText;
        }

        private string GetChatURL()
        {
            StringBuilder ChatURL = new StringBuilder();

            ChatURL.AppendNoDup(_configuration.GetSection("AppSettings:ViewEventURL").Value);
            ChatURL.AppendNoDup(@"ChatLinks");

            return ChatURL.ToString();
        }

        private void CreateAndSendSrvcEmails(int iEventID, bool bSuppressEmails)
        {
            DataSet ds = new DataSet();
            DataTable dtMDSEvent = new DataTable();
            DataTable dtSysCfg = new DataTable();
            XElement _EventEmail;

            ds = GetMDSSrvcEmailData(iEventID);
            if ((ds != null) && (ds.Tables.Count > 0))
            {
                dtMDSEvent = ds.Tables[0];
                dtSysCfg = ds.Tables[1];
            }
            if ((dtMDSEvent != null) && (dtMDSEvent.Rows.Count > 0)) { }
            {
                foreach (DataRow dr in dtMDSEvent.Rows)
                {
                    if ((dr["ACTV_DT"].ToString() != string.Empty) &&
                        ((dtSysCfg.Select("PRMTR_NME = 'MDSSrvcActivationEmail'")[0]["PRMTR_VALU_TXT"]).ToString() != string.Empty))
                    {
                        _EventEmail = new XElement("EventMail",
                                                                  new XElement("ActvDt", dr["ACTV_DT"].ToString()),
                                                                  new XElement("THIRD_PARTY_ID", dr["THIRD_PARTY_ID"].ToString()),
                                                                  new XElement("REQOR_NME", dr["REQOR_NME"].ToString()),
                                                                  new XElement("REQOR_PHN_NBR", dr["REQOR_PHN_NBR"].ToString()));

                        InsertIntoEmailReq((byte)EmailREQType.MDSSrvcActivationEmail, iEventID,
                            (dtSysCfg.Select("PRMTR_NME = 'MDSSrvcActivationEmail'")[0]["PRMTR_VALU_TXT"]).ToString().Replace(";", ",").ToString(),
                            dr["CUST_EMAIL_ADR"].ToString(), dr["SUBJ"].ToString(),
                            _EventEmail.ToString(), bSuppressEmails);
                        UpdateMDSSrvcTblEmailCd(Int32.Parse(dr["MDS_EVENT_SRVC_TYPE_ID"].ToString()), true);
                    }
                    else if ((dr["ACTV_DT"].ToString().Equals(string.Empty)) && (!dr["CMNT_TXT"].ToString().Equals(string.Empty))
                        && ((dtSysCfg.Select("PRMTR_NME = 'MDSSrvcCommentsEmail'")[0]["PRMTR_VALU_TXT"]).ToString() != string.Empty))
                    {
                        _EventEmail = new XElement("EventMail",
                                                                  new XElement("CMNT_TXT", dr["CMNT_TXT"].ToString().Replace("<br/>", " ")),
                                                                  new XElement("THIRD_PARTY_ID", dr["THIRD_PARTY_ID"].ToString()),
                                                                  new XElement("REQOR_NME", dr["REQOR_NME"].ToString()),
                                                                  new XElement("REQOR_PHN_NBR", dr["REQOR_PHN_NBR"].ToString()));

                        InsertIntoEmailReq((byte)EmailREQType.MDSSrvcCommentsEmail, iEventID,
                            (dtSysCfg.Select("PRMTR_NME = 'MDSSrvcCommentsEmail'")[0]["PRMTR_VALU_TXT"]).ToString().Replace(";", ",").ToString(),
                            dr["CUST_EMAIL_ADR"].ToString(), dr["SUBJ"].ToString(),
                            _EventEmail.ToString(), bSuppressEmails);
                        UpdateMDSSrvcTblEmailCd(Int32.Parse(dr["MDS_EVENT_SRVC_TYPE_ID"].ToString()), true);
                    }
                }
            }
        }

        // km967761 - 08/23/2021 - updated return value type from bool to int
        public int InsertIntoEmailReq(byte iEmailReqTypeID, int iEventID, string sTo, string sCC, string sSubj, string sBody, bool bSuppressEmails)
        {
            if ((sTo.Length > 0) || (sCC.Length > 0) || (iEmailReqTypeID == 30))
            {
                EmailReq emr = new EmailReq();
                emr.EventId = iEventID;
                emr.EmailReqTypeId = iEmailReqTypeID;
                emr.EmailListTxt = (sTo.Length > 0) ? ((sTo[sTo.Length - 1] == ',') ? sTo.Substring(0, sTo.Length - 1) : sTo) : string.Empty;
                emr.StusId = (bSuppressEmails) ? (byte)EmailStatus.OnHold : (byte)EmailStatus.Pending;
                //emr.CreatDt = DateTime.Now;
                emr.EmailBodyTxt = sBody;
                emr.EmailCcTxt = (sCC.Length > 0) ? ((sCC[sCC.Length - 1] == ',') ? sCC.Substring(0, sCC.Length - 1) : sCC) : string.Empty;
                emr.EmailSubjTxt = sSubj;

                _context.EmailReq.Add(emr);
                _context.SaveChanges();

                return emr.EmailReqId;
            }
            
            return 0;
        }

        public void UpdateMDSSrvcTblEmailCd(int iMDSSrvcID, bool bStus)
        {
            MdsEventSrvc y = (from x in _context.MdsEventSrvc
                              where x.MdsEventSrvcTypeId == iMDSSrvcID
                              select x).Single();
            y.EmailCd = bStus;
            _context.SaveChanges();
        }

        private XElement GenerateUCaaSBody(EventWorkflow esw, string CalenderEntryURL, StringBuilder sbOldActEmail)
        {
            XElement _EventEmail = new XElement("EventEmail");
            bool bSensitiveCust = false;
            DataSet ds = _ucaasEventRepo.GetUcaasEventData(esw.EventId);

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    bSensitiveCust = Convert.ToByte(ds.Tables[0].Rows[0]["CSG_LVL_ID"]) > 0;

                    XElement _xa = new XElement("UCaaSEvent",
                        new XElement("EVENT_ID", esw.EventId),
                        new XElement("EVENT_STUS_DES", ds.Tables[0].Rows[0]["EVENT_STUS_DES"]),
                        new XElement("WRKFLW_STUS_DES", ds.Tables[0].Rows[0]["WRKFLW_STUS_DES"]),
                        new XElement("OLD_EVENT_STUS_DES", ds.Tables[0].Rows[0]["OLD_EVENT_STUS_DES"]),
                        new XElement("EVENT_TITLE_TXT", bSensitiveCust ? "Private Customer" : ds.Tables[0].Rows[0]["EVENT_TITLE_TXT"]),
                        new XElement("H1", ds.Tables[0].Rows[0]["H1"]),
                        new XElement("H6", ds.Tables[0].Rows[0]["H6"]),
                        new XElement("CCD", ds.Tables[0].Rows[0]["CCD"]),
                        new XElement("CHARS_ID", ds.Tables[0].Rows[0]["CHARS_ID"]),
                        new XElement("SITE_ID", ds.Tables[0].Rows[0]["SITE_ID"]),
                        new XElement("UCaaS_ACTY_TYPE_DES", ds.Tables[0].Rows[0]["UCaaS_ACTY_TYPE_DES"]),
                        new XElement("UCaaS_PROD_TYPE_DES", ds.Tables[0].Rows[0]["UCaaS_PROD_TYPE_DES"]),
                        new XElement("UCaaS_PLAN_TYPE_DES", ds.Tables[0].Rows[0]["UCaaS_PLAN_TYPE_DES"]),
                        new XElement("CUST_NME", bSensitiveCust ? CustomerMaskedData.customerName : ds.Tables[0].Rows[0]["CUST_NME"]),
                        new XElement("CUST_SOW_LOC_TXT", ds.Tables[0].Rows[0]["CUST_SOW_LOC_TXT"]),
                        new XElement("UCaaS_DESGN_DOC", ds.Tables[0].Rows[0]["UCaaS_DESGN_DOC"]),
                        new XElement("SHRT_DES", ds.Tables[0].Rows[0]["SHRT_DES"]),
                        new XElement("CMNT_TXT", ds.Tables[0].Rows[0]["CMNT_TXT"]),
                        new XElement("MDS_BRDG_NEED_TXT", ds.Tables[0].Rows[0]["MDS_BRDG_NEED_TXT"]),
                        new XElement("STRT_TMST", (DateTime.Parse(ds.Tables[0].Rows[0]["STRT_TMST"].ToString()).ToString("MM-dd-yyyy")) + " @ " + (DateTime.Parse(ds.Tables[0].Rows[0]["STRT_TMST"].ToString()).ToString("t"))),
                        new XElement("END_TMST", ds.Tables[0].Rows[0]["END_TMST"]),
                        new XElement("EXTRA_DRTN_TME_AMT", ds.Tables[0].Rows[0]["EXTRA_DRTN_TME_AMT"]),
                        new XElement("CREAT_BY_USER_ID", ds.Tables[0].Rows[0]["CREAT_BY_USER_ID"]),
                        new XElement("MODFD_BY_USER_ID", ds.Tables[0].Rows[0]["MODFD_BY_USER_ID"]),
                        new XElement("CREAT_DT", ds.Tables[0].Rows[0]["CREAT_DT"]),
                        new XElement("CNFRC_BRDG_NBR", ds.Tables[0].Rows[0]["CNFRC_BRDG_NBR"]),
                        new XElement("CNFRC_PIN_NBR", ds.Tables[0].Rows[0]["CNFRC_PIN_NBR"]),
                        new XElement("ONLINE_MEETING_ADR", esw.OnlineMeetingAdr),
                        new XElement("CalenderEntryURL", CalenderEntryURL),
                        new XElement("ViewEventURL", GetViewEventURL((byte)esw.EventTypeId)),
                        new XElement("ChatURL", GetChatURL()),
                        new XElement("RDSGN_NBR", ((ds.Tables[11] != null) && (ds.Tables[11].Rows.Count > 0)) ? ds.Tables[11].Rows[0]["RDSN_NBR"] : string.Empty),
                        new XElement("ModUserName", ds.Tables[0].Rows[0]["MODFD_BY_USER_ID"]),
                        new XElement("AssignUsers", ((ds.Tables[7] != null) && (ds.Tables[7].Rows.Count > 0)) ? ds.Tables[7].Rows[0]["AssignUsers"] : string.Empty),
                        new XElement("AssignPhoneNumbers", ((ds.Tables[12] != null) && (ds.Tables[12].Rows.Count > 0)) ? ds.Tables[12].Rows[0]["AssignUserPhoneNumbers"] : string.Empty),
                        new XElement("RevUserName", ((ds.Tables[10] != null) && (ds.Tables[10].Rows.Count > 0)) ? ds.Tables[10].Rows[0]["REV_USR_NME"] : string.Empty),
                        new XElement("REV_PHN_NBR", ((ds.Tables[10] != null) && (ds.Tables[10].Rows.Count > 0)) ? ds.Tables[10].Rows[0]["REV_PHN_NBR"] : string.Empty),
                        new XElement("ESCL_CD", ds.Tables[0].Rows[0]["ESCL_CD"]),
                        new XElement("ESCL_REAS_DES", ds.Tables[0].Rows[0]["ESCL_REAS_DES"]),
                        new XElement("REQOR_USER", ds.Tables[0].Rows[0]["REQOR_USER"]),
                        new XElement("REQOR_USER_PHN", ds.Tables[0].Rows[0]["REQOR_USER_PHN"]),
                        new XElement("REQOR_USER_EMAIL", ds.Tables[0].Rows[0]["REQOR_USER_EMAIL"]),
                        new XElement("INSTL_SITE_POC_NME", bSensitiveCust ? CustomerMaskedData.installSitePOC : ds.Tables[0].Rows[0]["INSTL_SITE_POC_NME"]),
                        new XElement("INSTL_SITE_POC_PHN_NBR", bSensitiveCust ? CustomerMaskedData.installSitePOCPhone : ds.Tables[0].Rows[0]["INSTL_SITE_POC_PHN_NBR"]),
                        new XElement("INSTL_SITE_POC_CELL_PHN_NBR", bSensitiveCust ? CustomerMaskedData.installSitePOCCell : ds.Tables[0].Rows[0]["INSTL_SITE_POC_CELL_PHN_NBR"]),
                        new XElement("SRVC_ASSRN_POC_NME", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOC : ds.Tables[0].Rows[0]["SRVC_ASSRN_POC_NME"]),
                        new XElement("SRVC_ASSRN_POC_PHN_NBR", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCPhone : ds.Tables[0].Rows[0]["SRVC_ASSRN_POC_PHN_NBR"]),
                        new XElement("SRVC_ASSRN_POC_CELL_PHN_NBR", bSensitiveCust ? CustomerMaskedData.serviceAssurancePOCCell : ds.Tables[0].Rows[0]["SRVC_ASSRN_POC_CELL_PHN_NBR"]),
                        new XElement("STREET_ADR", bSensitiveCust ? CustomerMaskedData.address : ds.Tables[0].Rows[0]["STREET_ADR"]),
                        new XElement("FLR_BLDG_NME", bSensitiveCust ? CustomerMaskedData.floor : ds.Tables[0].Rows[0]["FLR_BLDG_NME"]),
                        new XElement("CTY_NME", bSensitiveCust ? CustomerMaskedData.city : ds.Tables[0].Rows[0]["CTY_NME"]),
                        new XElement("STT_PRVN_NME", bSensitiveCust ? CustomerMaskedData.state : ds.Tables[0].Rows[0]["STT_PRVN_NME"]),
                        new XElement("CTRY_RGN_NME", bSensitiveCust ? CustomerMaskedData.country : ds.Tables[0].Rows[0]["CTRY_RGN_NME"]),
                        new XElement("ZIP_CD", bSensitiveCust ? CustomerMaskedData.zipCode : ds.Tables[0].Rows[0]["ZIP_CD"]),
                        new XElement("US_INTL_CD", ds.Tables[0].Rows[0]["US_INTL_CD"].ToString().Equals("D") ? "US" : "International"),
                        new XElement("CPE_DSPCH_EMAIL_ADR", ds.Tables[0].Rows[0]["CPE_DSPCH_EMAIL_ADR"]),
                        new XElement("PUB_EMAIL_CC_TXT", ds.Tables[0].Rows[0]["PUB_EMAIL_CC_TXT"]),
                        new XElement("CMPLTD_EMAIL_CC_TXT", ds.Tables[0].Rows[0]["CMPLTD_EMAIL_CC_TXT"]),
                        new XElement("DISC_MGMT_CD", ((ds.Tables[0].Rows[0]["DISC_MGMT_CD"].ToString().Equals("")) ? "" : (ds.Tables[0].Rows[0]["DISC_MGMT_CD"].ToString().Equals("Y") ? "Yes" : "No"))),
                        new XElement("FULL_CUST_DISC_CD", ((ds.Tables[0].Rows[0]["FULL_CUST_DISC_CD"].ToString().Equals("")) ? "" : (bool.Parse(ds.Tables[0].Rows[0]["FULL_CUST_DISC_CD"].ToString()) ? "Full Disconnect" : "Partial Disconnect"))),
                        new XElement("FULL_CUST_DISC_REAS_TXT", ds.Tables[0].Rows[0]["FULL_CUST_DISC_REAS_TXT"])
                    );

                    _EventEmail.Add(_xa);
                }

                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        _EventEmail.Add(new XElement("UCaaSRedesign",
                            new XElement("RDSN_NBR", ds.Tables[1].Rows[i]["RDSN_NBR"]),
                            new XElement("RDSN_EXP_DT", (DateTime.Parse(ds.Tables[1].Rows[i]["RDSN_EXP_DT"].ToString()).ToString("MM-dd-yyyy"))),
                            new XElement("ODIE_DEV_NME", ds.Tables[1].Rows[i]["ODIE_DEV_NME"]),
                            new XElement("DEV_MODEL_NME", ds.Tables[1].Rows[i]["DEV_MODEL_NME"]),
                            new XElement("MANF_NME", ds.Tables[1].Rows[i]["MANF_NME"])
                        ));
                    }
                }

                if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                    {
                        _EventEmail.Add(new XElement("UCaaSDevCmpltTbl",
                            new XElement("ODIEDevNme", ds.Tables[2].Rows[i]["ODIE_DEV_NME"]),
                            new XElement("ODIEH6", ds.Tables[2].Rows[i]["H6"]),
                            new XElement("ActvCmpltCd", ds.Tables[2].Rows[i]["CMPLTD_CD"])
                        ));
                    }
                }

                if (ds.Tables[3] != null && ds.Tables[3].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[3].Rows.Count; i++)
                    {
                        _EventEmail.Add(new XElement("UCaaSCPEDev",
                            new XElement("CPE_ORDR_ID", ds.Tables[3].Rows[i]["CPE_ORDR_ID"]),
                            new XElement("CPE_ORDR_NBR", ds.Tables[3].Rows[i]["CPE_ORDR_NBR"]),
                            new XElement("DEVICE_ID", ds.Tables[3].Rows[i]["DEVICE_ID"]),
                            new XElement("ASSOC_H6", ds.Tables[3].Rows[i]["ASSOC_H6"]),
                            new XElement("REQSTN_NBR", ds.Tables[3].Rows[i]["REQSTN_NBR"]),
                            new XElement("ODIE_DEV_NME", ds.Tables[3].Rows[i]["ODIE_DEV_NME"])
                        ));
                    }
                }

                if (ds.Tables[4] != null && ds.Tables[4].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[4].Rows.Count; i++)
                    {
                        _EventEmail.Add(new XElement("UCaaSDevMgmtTbl",
                            new XElement("MNSOrdrNbr", ds.Tables[4].Rows[i]["MNS_ORDR_NBR"]),
                            new XElement("DeviceID", ds.Tables[4].Rows[i]["DEVICE_ID"]),
                            new XElement("ServiceTier", ds.Tables[4].Rows[i]["MDS_SRVC_TIER_DES"]),
                            new XElement("Entitlements", ds.Tables[4].Rows[i]["MNS_ENTLMNT_ID_TXT"]),
                            new XElement("ODIEDevNme", ds.Tables[4].Rows[i]["ODIE_DEV_NME"])
                        ));
                    }
                }

                if (ds.Tables[5] != null && ds.Tables[5].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[5].Rows.Count; i++)
                    {
                        _EventEmail.Add(new XElement("UCaaSBilling",
                            new XElement("INC_QTY", ds.Tables[5].Rows[i]["INC_QTY"]),
                            new XElement("DCR_QTY", ds.Tables[5].Rows[i]["DCR_QTY"]),
                            new XElement("UCaaS_BILL_ACTY_DES", ds.Tables[5].Rows[i]["UCaaS_BILL_ACTY_DES"]),
                            new XElement("UCaaS_BIC_TYPE", ds.Tables[5].Rows[i]["UCaaS_BIC_TYPE"]),
                            new XElement("MRC_NRC_CD", ds.Tables[5].Rows[i]["MRC_NRC_CD"])
                        ));
                    }
                }

                if (ds.Tables[0].Rows[0]["UCaaS_ACTY_TYPE_DES"].ToString().Contains("Disconnect")
                    && (ds.Tables[13] != null && ds.Tables[13].Rows.Count > 0))
                {
                    for (int i = 0; i < ds.Tables[13].Rows.Count; i++)
                    {
                        _EventEmail.Add(new XElement("UCaaSDiscODIEDev",
                            new XElement("ODIE_DEV_NME", ds.Tables[13].Rows[i]["ODIE_DEV_NME"].ToString()),
                            new XElement("H5_H6", ds.Tables[13].Rows[i]["H5_H6"].ToString()),
                            new XElement("SITE_ID", ds.Tables[13].Rows[i]["SITE_ID"].ToString()),
                            new XElement("DEVICE_ID", ds.Tables[13].Rows[i]["DEVICE_ID"].ToString()),
                            new XElement("MANF_NME", ds.Tables[13].Rows[i]["MANF_NME"].ToString()),
                            new XElement("DEV_MODEL_NME", ds.Tables[13].Rows[i]["DEV_MODEL_NME"].ToString()),
                            new XElement("SERIAL_NBR", ds.Tables[13].Rows[i]["SERIAL_NBR"].ToString()),
                            new XElement("THRD_PARTY_CTRCT", ds.Tables[13].Rows[i]["THRD_PARTY_CTRCT"].ToString())
                        ));
                    }
                }

                sbOldActEmail.Clear();
                DataTable dtEmailAddr = ds.Tables[14];
                if (dtEmailAddr != null)
                    if (dtEmailAddr.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                        {
                            if ((sbOldActEmail.Length > 0) && (sbOldActEmail[sbOldActEmail.Length - 1] != ','))
                                sbOldActEmail.Append(",");
                            sbOldActEmail.AppendNoDup(dtEmailAddr.Rows[i]["EMAIL_ADR"].ToString());
                        }
                    }
            }

            return _EventEmail;
        }

        private void GetEmailAddr(string sEmailTyp, EventRuleData ruleData, XElement XmlBody,
            ref List<int> _UserIDs, ref StringBuilder _sbEmailCCAddr, ref StringBuilder _sbEmailAddr,
            ref EventWorkflow esw)
        {
            _UserIDs.Clear();
            _sbEmailCCAddr.Clear();
            _sbEmailAddr.Clear();
            DataTable dtEmailAddr = new DataTable();

            if (sEmailTyp.Equals("CPE"))
            {
                if ((ruleData.EmailmemCd.HasValue && ruleData.EmailmemCd.Value != 0)
                    || (ruleData.EmailrevCd.HasValue && ruleData.EmailrevCd.Value != 0)
                    || (ruleData.EmailactCd.HasValue && ruleData.EmailactCd.Value != 0))
                {
                    if (XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR").Any()
                        && XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR").Single().Value.Trim().Length > 0)
                    {
                        _sbEmailAddr.AppendNoDup(XmlBody.Elements("UCaaSEvent").Elements("CPE_DSPCH_EMAIL_ADR").Single().Value.Trim());
                    }

                    if (esw.AssignUser != null && esw.AssignUser.Count > 0)
                    {
                        foreach (var lau in esw.AssignUser)
                        {
                            if (ruleData.EmailactCd.HasValue && ruleData.EmailactCd.Value != 0
                                && lau.AsnToUserId != 5001)
                            {
                                if (ruleData.EmailactCd.HasValue && ruleData.EmailactCd.Value == 2)
                                {
                                    if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                                        _sbEmailCCAddr.Append(",");
                                    _sbEmailCCAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                                }

                                if (ruleData.EmailactCd.HasValue && ruleData.EmailactCd.Value == 1)
                                {
                                    if (_sbEmailAddr.Length > 0 && _sbEmailAddr[_sbEmailAddr.Length - 1] != ',')
                                        _sbEmailAddr.Append(",");
                                    _sbEmailAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                                }
                            }
                        }
                    }

                    if (_UserIDs.Count <= 0)
                        _UserIDs.Add(esw.RequestorId);

                    dtEmailAddr = GetEmailAddr(_UserIDs);
                    if (dtEmailAddr != null && dtEmailAddr.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                        {
                            if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                                _sbEmailCCAddr.Append(",");
                            _sbEmailCCAddr.AppendNoDup(dtEmailAddr.Rows[i]["EmailAddr"].ToString());
                        }
                    }
                }
            }
            else
            {
                if (ruleData.EmailmemCd.HasValue && ruleData.EmailmemCd.Value == 1 && esw.RequestorId > 0)
                    _UserIDs.Add(esw.RequestorId);

                if (ruleData.EmailrevCd.HasValue && ruleData.EmailrevCd.Value == 1 && esw.ReviewerId > 0)
                {
                    _UserIDs.Add(esw.ReviewerId);

                    DataTable _DTReviewer = GetEventReviewers(esw.EventId, (int)EventType.UCaaS);
                    if (_DTReviewer != null && _DTReviewer.Rows.Count > 0)
                    {
                        foreach (DataRow DR in _DTReviewer.Rows)
                        {
                            int _RevID = 0;
                            try
                            {
                                _RevID = Convert.ToInt32(DR["REV_USER_ID"].ToString());
                            }
                            catch (Exception e)
                            {
                                _logger.LogInformation($"{ e.Message } ");
                            }

                            if (_RevID != esw.ReviewerId && _RevID > 0)
                                _UserIDs.Add(_RevID);
                        }
                    }
                }

                if (esw.AssignUser != null && esw.AssignUser.Count > 0)
                {
                    foreach (var lau in esw.AssignUser)
                    {
                        if (ruleData.EmailactCd.HasValue && ruleData.EmailactCd.Value > 0 && lau.AsnToUserId != 5001)
                        {
                            if (ruleData.EmailactCd.Value == 2)
                            {
                                if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                                    _sbEmailCCAddr.Append(",");
                                _sbEmailCCAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                            }

                            if (ruleData.EmailactCd.Value == 1)
                            {
                                if (_sbEmailAddr.Length > 0 && _sbEmailAddr[_sbEmailAddr.Length - 1] != ',')
                                    _sbEmailAddr.Append(",");
                                _sbEmailAddr.AppendNoDup(lau.AsnToUser.EmailAdr);
                            }
                        }
                    }
                }

                if (_UserIDs.Count <= 0)
                    _UserIDs.Add(esw.RequestorId);

                dtEmailAddr = GetEmailAddr(_UserIDs);
                if (dtEmailAddr != null && dtEmailAddr.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                    {
                        if (_sbEmailAddr.Length > 0 && _sbEmailAddr[_sbEmailAddr.Length - 1] != ',')
                            _sbEmailAddr.Append(",");
                        _sbEmailAddr.AppendNoDup(dtEmailAddr.Rows[i]["EmailAddr"].ToString());
                    }
                }
            }

            // Generate CC Addr
            _UserIDs.Clear();
            _UserIDs.Add(esw.UserId);
            if (ruleData.EmailmemCd.HasValue && ruleData.EmailmemCd.Value == 2 && esw.RequestorId > 0)
                _UserIDs.Add(esw.RequestorId);

            if (ruleData.EmailrevCd.HasValue && ruleData.EmailrevCd.Value == 2 && esw.ReviewerId > 0)
                _UserIDs.Add(esw.ReviewerId);

            dtEmailAddr = GetEmailAddr(_UserIDs);
            if (dtEmailAddr != null && dtEmailAddr.Rows.Count > 0)
            {
                for (int i = 0; i < dtEmailAddr.Rows.Count; i++)
                {
                    if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                        _sbEmailCCAddr.Append(",");
                    _sbEmailCCAddr.AppendNoDup(dtEmailAddr.Rows[i]["EmailAddr"].ToString());
                }
            }

            string PubCCEmailAddr = (XmlBody.Elements("UCaaSEvent").Elements("PUB_EMAIL_CC_TXT").Any()
                && XmlBody.Elements("UCaaSEvent").Elements("PUB_EMAIL_CC_TXT").Single().Value.Trim().Length > 0)
                    ? XmlBody.Elements("UCaaSEvent").Elements("PUB_EMAIL_CC_TXT").Single().Value.Trim()
                    : string.Empty;

            if (ruleData.EmailpubccCd.HasValue && ruleData.EmailpubccCd.Value)
                if (!string.IsNullOrWhiteSpace(PubCCEmailAddr))
                {
                    if (ValidateEmail(PubCCEmailAddr.Trim()))
                    {
                        string sPubCCEmail = PubCCEmailAddr.Trim().Contains(";")
                            ? PubCCEmailAddr.Trim().Replace(";", ",") : PubCCEmailAddr.Trim();

                        if (sPubCCEmail.Contains(","))
                        {
                            foreach (string sPub in sPubCCEmail.Split(new char[] { ',' }))
                            {
                                if (!_sbEmailCCAddr.ToString().ToLower().Contains(sPub.ToLower()))
                                {
                                    if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                                        _sbEmailCCAddr.Append(",");
                                    _sbEmailCCAddr.AppendNoDup(sPub);
                                }
                            }
                        }
                        else
                        {
                            if (!_sbEmailCCAddr.ToString().ToLower().Contains(PubCCEmailAddr.Trim().ToLower()))
                            {
                                if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                                    _sbEmailCCAddr.Append(",");
                                _sbEmailCCAddr.AppendNoDup(PubCCEmailAddr.Trim());
                            }
                        }
                    }
                }

            string CmplCCEmailAddr = (XmlBody.Elements("UCaaSEvent").Elements("CMPLTD_EMAIL_CC_TXT").Any()
                && XmlBody.Elements("UCaaSEvent").Elements("CMPLTD_EMAIL_CC_TXT").Single().Value.Trim().Length > 0)
                    ? XmlBody.Elements("UCaaSEvent").Elements("CMPLTD_EMAIL_CC_TXT").Single().Value.Trim() : string.Empty;

            if (ruleData.EmailcmplccCd.HasValue && ruleData.EmailcmplccCd.Value)
                if (!string.IsNullOrWhiteSpace(CmplCCEmailAddr))
                {
                    if (ValidateEmail(CmplCCEmailAddr.Trim()))
                    {
                        string sCmplCCEmail = CmplCCEmailAddr.Trim().Contains(";")
                            ? CmplCCEmailAddr.Trim().Replace(";", ",") : CmplCCEmailAddr.Trim();

                        if (sCmplCCEmail.Contains(","))
                        {
                            foreach (string sCmpl in sCmplCCEmail.Split(new char[] { ',' }))
                            {
                                if (!_sbEmailCCAddr.ToString().ToLower().Contains(sCmpl.ToLower()))
                                {
                                    if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                                        _sbEmailCCAddr.Append(",");
                                    _sbEmailCCAddr.AppendNoDup(sCmpl);
                                }
                            }
                        }
                        else
                        {
                            if (!_sbEmailCCAddr.ToString().ToLower().Contains(CmplCCEmailAddr.Trim().ToLower()))
                            {
                                if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                                    _sbEmailCCAddr.Append(",");
                                _sbEmailCCAddr.AppendNoDup(CmplCCEmailAddr.Trim());
                            }
                        }
                    }
                }

            if (ruleData.EmailccAdr.Trim().Length > 0)
            {
                if (_sbEmailCCAddr.Length > 0 && _sbEmailCCAddr[_sbEmailCCAddr.Length - 1] != ',')
                    _sbEmailCCAddr.Append(",");
                _sbEmailCCAddr.AppendNoDup(ruleData.EmailccAdr.Trim());
            }
        }


        public bool IsEventEmailSent(int eventId, int emailReqTypeId)
        {
            return Find(a => a.EventId == eventId && a.EmailReqTypeId == emailReqTypeId && a.StusId == (int)EmailStatus.Success && (DateTime.Now - a.CreatDt).TotalSeconds > 15).Count() > 0;
        }

        private int GetVASCECd(MdsEvent obj, string mdsNtwkActyTypeId)
        {
            var isNetwork = obj.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 2);
            var isVas = obj.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 3);

            if (isVas && isNetwork && (mdsNtwkActyTypeId.Contains("20") || mdsNtwkActyTypeId.Contains("21"))) {
                return 5;
            } else if (isVas && (mdsNtwkActyTypeId.Contains("20") || mdsNtwkActyTypeId.Contains("21"))) {
                return 4;
            } else if (isNetwork && (mdsNtwkActyTypeId.Contains("20") || mdsNtwkActyTypeId.Contains("21"))) {
                return 3;
            } else if (isVas && isNetwork && !(mdsNtwkActyTypeId.Contains("20") || mdsNtwkActyTypeId.Contains("21"))) {
                return 2;
            } else if (isVas && !(mdsNtwkActyTypeId.Contains("20") || mdsNtwkActyTypeId.Contains("21"))) {
                return 1;
            } else {
                return 0;
            }
          }

        private string GetUntilOrEmpty(string text, string stopAt = "-")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return String.Empty;
        }
    }
}