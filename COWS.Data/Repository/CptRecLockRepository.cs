﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptRecLockRepository : ICptRecLockRepository
    {
        private readonly COWSAdminDBContext _context;

        public CptRecLockRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public CptRecLock CheckLock(int id)
        {
            return GetById(id);
        }

        public IQueryable<CptRecLock> Find(Expression<Func<CptRecLock, bool>> predicate)
        {
            return _context.CptRecLock
                            .Include(i => i.Cpt)
                            .Include(i => i.LockByUser)
                            .Where(predicate);
        }

        public CptRecLock GetById(int id)
        {
            return _context.CptRecLock
                            .Include(i => i.Cpt)
                            .Include(i => i.LockByUser)
                            .SingleOrDefault(i => i.CptId == id);
        }

        public int Lock(CptRecLock entity)
        {
            CptRecLock cpt = GetById(entity.CptId);
            if (cpt == null)
            {
                _context.CptRecLock.Add(entity);
                SaveAll();

                return entity.CptId;
            }

            return 0;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Unlock(int cptId, int userId)
        {
            CptRecLock cpt = _context.CptRecLock.SingleOrDefault(i => i.CptId == cptId && i.LockByUserId == userId);
            if (cpt != null)
            {
                _context.CptRecLock.Remove(cpt);
                SaveAll();
            }
        }
    }
}
