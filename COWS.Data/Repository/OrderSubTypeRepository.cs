﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderSubTypeRepository : IOrderSubTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public OrderSubTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkOrdrSubType> Find(Expression<Func<LkOrdrSubType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkOrdrSubType, out List<LkOrdrSubType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkOrdrSubType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkOrdrSubType, out List<LkOrdrSubType> list))
            {
                list = _context.LkOrdrSubType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkOrdrSubType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkOrdrSubType GetById(int id)
        {
            return _context.LkOrdrSubType
                .SingleOrDefault(a => a.OrdrSubTypeId == id);
        }

        public LkOrdrSubType Create(LkOrdrSubType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkOrdrSubType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkOrdrSubType);
            return _context.SaveChanges();
        }
    }
}