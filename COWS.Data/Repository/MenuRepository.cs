﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MenuRepository : IMenuRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;
        private IConfiguration _configuration;
        public MenuRepository(COWSAdminDBContext context, IMemoryCache cache, IConfiguration configuration)
        {
            _context = context;
            _cache = cache;
            _configuration = configuration;
        }

        public IQueryable<LkMenu> Find(Expression<Func<LkMenu, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMenu, out List<LkMenu> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMenu> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMenu, out List<LkMenu> list))
            {
                list = _context.LkMenu
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMenu, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMenu GetById(int id)
        {
            var menu = _context.Query<LkMenu>()
                .AsNoTracking()
                .FromSql("dbo.LK_MENU")
                .Where(a => a.MenuId == id)
                .SingleOrDefault();

            return menu;
        }

        public LkMenu Create(LkMenu entity)
        {
            _context.LkMenu.Add(entity);

            SaveAll();

            return GetById(entity.MenuId);
        }

        public void Update(int id, LkMenu entity)
        {
            LkMenu menu = GetById(id);
            menu.MenuNme = entity.MenuNme;
            menu.MenuDes = entity.MenuDes;
            menu.DpthLvl = entity.DpthLvl;
            menu.PrntMenuId = entity.PrntMenuId;
            menu.DsplOrdr = entity.DsplOrdr;
            menu.SrcPath = entity.SrcPath;
            menu.RecStusId = entity.RecStusId;
            menu.ModfdDt = entity.ModfdDt;

            SaveAll();
        }

        public void Delete(int id)
        {
            LkMenu menu = GetById(id);
            _context.LkMenu.Remove(menu);

            SaveAll();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMenu);
            return _context.SaveChanges();
        }

        public List<LkSysCfg> GetExternalLinks()
        {
            var list = _context.LkSysCfg
                    .Where(a => (a.PrmtrNme == "externalLinks" || a.PrmtrNme == "externalLinkKey") && a.RecStusId == 1)
                    .OrderBy(a => a.PrmtrNme)
                    .AsNoTracking()
                    .ToList();
            return list;
        }
    }
}