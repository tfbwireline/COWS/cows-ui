﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class WholesalePartnerRepository : IWholesalePartnerRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public WholesalePartnerRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkWhlslPtnr Create(LkWhlslPtnr entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkWhlslPtnr> Find(Expression<Func<LkWhlslPtnr, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkWhlslPtnr, out List<LkWhlslPtnr> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkWhlslPtnr> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkWhlslPtnr, out List<LkWhlslPtnr> list))
            {
                list = _context.LkWhlslPtnr
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkWhlslPtnr, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkWhlslPtnr GetById(int id)
        {
            return _context.LkWhlslPtnr
                .SingleOrDefault(a => a.WhlslPtnrId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkWhlslPtnr);
            return _context.SaveChanges();
        }

        public void Update(int id, LkWhlslPtnr entity)
        {
            throw new NotImplementedException();
        }
    }
}