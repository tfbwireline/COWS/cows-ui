﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class DeviceManufacturerRepository : IDeviceManufacturerRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public DeviceManufacturerRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkDevManf Create(LkDevManf entity)
        {
            int maxId = _context.LkDevManf.Max(i => i.ManfId);
            entity.ManfId = (short)++maxId;
            _context.LkDevManf.Add(entity);

            SaveAll();

            return GetById(entity.ManfId);
        }

        public void Delete(int id)
        {
            LkDevManf manf = GetById(id);
            _context.LkDevManf.Remove(manf);

            SaveAll();
        }

        public IQueryable<LkDevManf> Find(Expression<Func<LkDevManf, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkDevManf, out List<LkDevManf> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkDevManf> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkDevManf, out List<LkDevManf> list))
            {
                list = _context.LkDevManf
                            .OrderBy(i => i.ManfNme)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkDevManf, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkDevManf GetById(int id)
        {
            return _context.LkDevManf
                            .SingleOrDefault(i => i.ManfId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkDevManf);
            return _context.SaveChanges();
        }

        public void Update(int id, LkDevManf entity)
        {
            LkDevManf manf = GetById(id);
            manf.ManfNme = entity.ManfNme;
            manf.RecStusId = entity.RecStusId;

            SaveAll();
        }
    }
}