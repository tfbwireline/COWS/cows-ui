﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EnhncSrvcRepository : IEnhncSrvcRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public EnhncSrvcRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkEnhncSrvc Create(LkEnhncSrvc entity)
        {
            int maxId = _context.LkEnhncSrvc.Max(i => i.EnhncSrvcId);
            entity.EnhncSrvcId = (byte)++maxId;
            _context.LkEnhncSrvc.Add(entity);

            SaveAll();

            return GetById(entity.EnhncSrvcId);
        }

        public void Delete(int id)
        {
           
            LkEnhncSrvc st = GetById(id);
            _context.LkEnhncSrvc.Remove(st);
            SaveAll();
        }

        public IQueryable<LkEnhncSrvc> Find(Expression<Func<LkEnhncSrvc, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkEnhncSrvc, out List<LkEnhncSrvc> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkEnhncSrvc> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkEnhncSrvc, out List<LkEnhncSrvc> list))
            {
                list = _context.LkEnhncSrvc
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderBy(i => i.EnhncSrvcId)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkEnhncSrvc, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkEnhncSrvc GetById(int id)
        {
            return _context.LkEnhncSrvc
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.EnhncSrvcId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkEnhncSrvc);
            return _context.SaveChanges();

        }

        public void Update(int id, LkEnhncSrvc entity)
        {
            LkEnhncSrvc item = GetById(id);
            item.EnhncSrvcNme = entity.EnhncSrvcNme;
            item.EnhncSrvcId = entity.EnhncSrvcId;
            item.EventTypeId = entity.EventTypeId;
            item.RecStusId = entity.RecStusId;
            item.ModfdByUserId = entity.ModfdByUserId;
            item.ModfdDt = entity.ModfdDt;
            
            SaveAll();
        }
    }
}
