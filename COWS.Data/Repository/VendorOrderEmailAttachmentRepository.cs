﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VendorOrderEmailAttachmentRepository : IVendorOrderEmailAttachmentRepository
    {
        private readonly COWSAdminDBContext _context;

        public VendorOrderEmailAttachmentRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<VndrOrdrEmailAtchmt> Find(Expression<Func<VndrOrdrEmailAtchmt, bool>> predicate)
        {
            return _context.VndrOrdrEmailAtchmt
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<VndrOrdrEmailAtchmt> GetAll()
        {
            return _context.VndrOrdrEmailAtchmt.ToList();
        }

        public VndrOrdrEmailAtchmt GetById(int id)
        {
            return Find(a => a.VndrOrdrEmailAtchmtId == id).SingleOrDefault();
        }

        public VndrOrdrEmailAtchmt Create(VndrOrdrEmailAtchmt entity)
        {
            _context.VndrOrdrEmailAtchmt.Add(entity);
            SaveAll();

            return GetById(entity.VndrOrdrEmailAtchmtId);
        }

        public void Update(int id, VndrOrdrEmailAtchmt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            var toDelete = GetById(id);
            _context.VndrOrdrEmailAtchmt.Remove(toDelete);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public bool IsAttachmentDistinct(int vendorOrderEmailId, string fileName)
        {
            return !_context.VndrOrdrEmailAtchmt
                .Include(a => a.CreatByUser)
                .Include(a => a.VndrOrdrEmail)
                .Any(a => a.VndrOrdrEmail.VndrOrdrEmailId == vendorOrderEmailId && a.FileNme == fileName);
        }
    }
}