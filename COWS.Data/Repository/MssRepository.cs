﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MSSRepository : IMSSRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;
        private readonly IUserRepository _userRepository;
        private readonly ICommonRepository _common;
        private readonly IMSSDocRepository _mssDocRepository;
        private readonly IMSSProdRepository _mssProdRepository;

        public MSSRepository(COWSAdminDBContext context,
                            IConfiguration configuration,
                             IUserRepository userRepository,
                              ICommonRepository common,
                        IMSSDocRepository mssDocRepository,
                        IMSSProdRepository mssProdRepository)
        {
            _context = context;
            _configuration = configuration;
            _userRepository = userRepository;
            _common = common;
            _mssDocRepository = mssDocRepository;
            _mssProdRepository = mssProdRepository;
        }

        public IEnumerable<GetSdeView> GetSdeView(int statusID, int userID)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@USER_ID", userID),
                new SqlParameter("@STUS_ID", statusID)
            };

            var sdeOpportunity = _context.Query<GetSdeView>()

                .FromSql("web.getSDEView_V2 @USER_ID, @STUS_ID", pc.ToArray()).ToList();

            return sdeOpportunity.OrderByDescending(x => x.DisplayDateTime);
        }

        //public IEnumerable<SdeOpportunity> SdeOpportunity(int statusID, int userID)
        //{
        //    List<SqlParameter> pc = new List<SqlParameter>
        //    {
        //        new SqlParameter("@USER_ID", userID),
        //        new SqlParameter("@STUS_ID", statusID)
        //    };

        //    var sdeOpportunity = _context.Query<SdeOpportunity>()

        //        .FromSql("web.getSDEView @USER_ID, @STUS_ID", pc.ToArray()).ToList();

        //    return sdeOpportunity;
        //}
        //public DataTable GetSdeView(int statusID, int userID)
        //{
        //    DataTable dtSde = new DataTable();
        //    dtSde.Columns.Add(new DataColumn("SDEOpportunityID"));
        //    dtSde.Columns.Add(new DataColumn("StatusID"));
        //    dtSde.Columns.Add(new DataColumn("StatusDesc"));
        //    dtSde.Columns.Add(new DataColumn("CompanyName"));
        //    dtSde.Columns.Add(new DataColumn("CreatedDateTime", typeof(DateTime)));
        //    dtSde.Columns.Add(new DataColumn("CreatedByFullName"));
        //    dtSde.Columns.Add(new DataColumn("CreatedByUserID"));
        //    dtSde.Columns.Add(new DataColumn("AssignedToID"));
        //    dtSde.Columns.Add(new DataColumn("AssignedTo"));

        //    var result = (from e in _context.SdeOpptnty
        //                  join lk in _context.LkUser on e.CreatByUserId equals lk.UserId
        //                  where (e.SdeStusId == statusID )
        //                  orderby e.SdeOpptntyId descending
        //                  select new
        //                  {
        //                      e.SdeOpptntyId,
        //                      e.SdeStusId,
        //                      //e.StatusDesc,
        //                      //e.CompanyName,
        //                      //e.CreatedDateTime,
        //                      //e.CreatedByFullName,
        //                      //e.CreatedByUserID,
        //                      //e.AssignedTo
        //                  });

        //    dtSde = LinqHelper.CopyToDataTable(result, null, null);

        //    return dtSde;

        //}

        public DataTable GetAvailableUsers()
        {
            DataTable dtSDEViewUsers = new DataTable();
            dtSDEViewUsers.Columns.Add(new DataColumn("UserId"));
            dtSDEViewUsers.Columns.Add(new DataColumn("FullNme"));

            List<int> commonIDs = new List<int> { 1, 3, 11 };
            var result = (from u in _context.LkUser
                          join m in _context.MapUsrPrf on u.UserId equals m.UserId
                          join p in _context.LkUsrPrf on m.UsrPrfId equals p.UsrPrfId
                          where p.UsrPrfNme.Contains("SystemSecurityEngineer")
                          && u.RecStusId == (byte)ERecStatus.Active
                          && m.RecStusId == (byte)ERecStatus.Active
                          && p.RecStusId == (byte)ERecStatus.Active
                          && !commonIDs.Contains(u.UserId)
                          orderby u.FullNme descending
                          select new
                          {
                              u.UserId,
                              u.FullNme
                          }).Distinct();

            dtSDEViewUsers = LinqHelper.CopyToDataTable(result, null, null);
            dtSDEViewUsers.Rows.Add(0, "All");
            return dtSDEViewUsers;
        }

        public DataTable GetViewOptions()
        {
            DataTable dtSDEViewStatus = new DataTable();
            dtSDEViewStatus.Columns.Add(new DataColumn("StusId"));
            dtSDEViewStatus.Columns.Add(new DataColumn("StudDes"));

            var result = (from ls in _context.LkStus
                          where ls.RecStusId == (byte)ERecStatus.Active
                          && ls.StusTypeId == "SDE"
                          orderby ls.StusId
                          select new
                          {
                              ls.StusId,
                              ls.StusDes
                          });

            dtSDEViewStatus = LinqHelper.CopyToDataTable(result, null, null);
            dtSDEViewStatus.Rows.Add(0, "All");
            return dtSDEViewStatus;
        }

        public IEnumerable<LkSdePrdctType> GetSDEProductTypesForAdmin()
        {
            return _context.LkSdePrdctType
                            .OrderBy(spt => spt.SdePrdctTypeDesc)
                            //.Where(spt => spt.RecStusId == (byte)Entities.Enums.ERecStatus.Active)
                            .ToList();
        }

        public IEnumerable<LkSdePrdctType> GetSDEProductTypesByIDForAdmin(int id)
        {
            return _context.LkSdePrdctType
                            .OrderBy(spt => spt.SdePrdctTypeDesc)
                            .Where(spt => spt.SdePrdctTypeId == id)
                            .Where(spt => spt.RecStusId == (byte)Entities.Enums.ERecStatus.Active)
                            .ToList();
        }

        public IEnumerable<LkSdePrdctType> InsertProductType(LkSdePrdctType entity)
        {
            // int newProductTypeID = 0;

            int lastProductTypeID = 0;

            var records = _context.LkSdePrdctType.FirstOrDefault();
            if(records != null)
            {
                lastProductTypeID = (int)(from s in _context.LkSdePrdctType
                                              select (int?)s.SdePrdctTypeId).Max();
            }


            int newProductTypeID = lastProductTypeID + 1;

            LkSdePrdctType spt = new LkSdePrdctType();

            spt.SdePrdctTypeId = (short)newProductTypeID;
            spt.SdePrdctTypeNme = entity.SdePrdctTypeNme;
            spt.SdePrdctTypeDesc = entity.SdePrdctTypeDesc;
            spt.OrdrBySeqNbr = (short)entity.OrdrBySeqNbr;
            spt.RecStusId = entity.RecStusId;
            spt.OutsrcdCd = entity.OutsrcdCd;
            spt.CreatByUserId = entity.CreatByUserId;
            spt.CreatDt = DateTime.Now;

            _context.LkSdePrdctType.Add(spt);
            _context.SaveChanges();

            return _context.LkSdePrdctType;
        }

        public bool UpdateProductType(int id, LkSdePrdctType entity)
        {
            // LkSdePrdctType updateSPT = GetById(id);
            LkSdePrdctType updateSPT =
                      (LkSdePrdctType)(from spt in _context.LkSdePrdctType
                                       where spt.SdePrdctTypeId == id
                                       select spt).SingleOrDefault();

            updateSPT.SdePrdctTypeNme = entity.SdePrdctTypeNme;
            updateSPT.SdePrdctTypeDesc = entity.SdePrdctTypeDesc;
            updateSPT.OrdrBySeqNbr = (short)entity.OrdrBySeqNbr;
            updateSPT.RecStusId = (byte)entity.RecStusId;
            updateSPT.OutsrcdCd = entity.OutsrcdCd;
            updateSPT.ModfdByUserId = entity.ModfdByUserId;
            updateSPT.ModfdDt = DateTime.Now;
            _context.SaveChanges();

            return true;
        }

        public bool DeleteProductType(int id)
        {
            LkSdePrdctType updateSPT =
                       (LkSdePrdctType)(from spt in _context.LkSdePrdctType
                                        where spt.SdePrdctTypeId == id
                                        select spt).SingleOrDefault();

            updateSPT.RecStusId = (byte)ERecStatus.InActive;
            //updateSPT.ModfdByUserId = ModfdByUserId;
            updateSPT.ModfdDt = DateTime.Now;
            _context.SaveChanges();

            return true;
        }

        public IEnumerable<LkSdePrdctType> GetAll()
        {
            return _context.LkSdePrdctType
                            .ToList();
        }

        //public void Update(int id, LkSdePrdctType entity)
        //{
        //    throw new NotImplementedException();
        //}
        public LkSdePrdctType GetById(int id)
        {
            return _context.LkSdePrdctType
                            .SingleOrDefault(i => i.SdePrdctTypeId == id);
        }

        public void Update(int id, LkSdePrdctType entity)
        {
            LkSdePrdctType telco = GetById(id);
            telco.SdePrdctTypeNme = entity.SdePrdctTypeNme;
            telco.SdePrdctTypeId = entity.SdePrdctTypeId;
            telco.RecStusId = entity.RecStusId;
            telco.ModfdByUserId = entity.ModfdByUserId;
            telco.ModfdDt = entity.ModfdDt;
            //LkEventType eventType = _context.LkEventType
            //                                .Find(entity.EventTypeId);
            //if (eventType != null)
            //{
            //    telco.EventTypeId = entity.EventTypeId;
            //    //telco.EventTypeNme = entity.EventTypeNme;
            //}
            SaveAll();
        }

        //public void Delete(int id)
        //{
        //    throw new NotImplementedException();
        //}
        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public SdeOpportunity GetByID(int sdeID)
        {
            SdeOpportunity sde = new SdeOpportunity();
            var q = (from so in _context.SdeOpptnty
                     join csd in _context.CustScrdData on so.SdeOpptntyId equals csd.ScrdObjId
                     join ls in _context.LkStus on so.SdeStusId equals ls.StusId
                     join lu in _context.LkUser on so.CreatByUserId equals lu.UserId
                     join la in _context.LkUser on so.AsnToUserId equals la.UserId

                     where so.SdeOpptntyId == sdeID
                         && so.RecStusId == (byte)ERecStatus.Active
                         && (csd.ScrdObjTypeId == (byte)SecuredObjectType.SDE_OPPTNTY)
                     orderby so.SdeOpptntyId
                     select new SdeOpportunity
                     {
                         SDEOpportunityID = so.SdeOpptntyId,
                         StatusID = so.SdeStusId,
                         Status = ls.StusDes,
                         CompanyName = _common.GetDecryptValue(csd.CustNme),
                         Address = _common.GetDecryptValue(csd.StreetAdr1),
                         Region = so.Rgn,
                         State = _common.GetDecryptValue(csd.SttPrvnNme),
                         City = _common.GetDecryptValue(csd.CtyNme),
                         ContactName = _common.GetDecryptValue(csd.CustCntctNme),
                         ContactEmail = _common.GetDecryptValue(csd.CustEmailAdr),
                         ContactPhone = _common.GetDecryptValue(csd.CustCntctPhnNbr),
                         SaleType = so.SaleType,
                         IsRFP = so.RfpCd,
                         IsWirelineCustomer = so.WrlneCustCd,
                         IsManagedServicesCustomer = so.MngdSrvcCustCd,
                         Comments = so.CmntTxt,
                         CcEmail = so.CcAdr,
                         AssignedToId = so.AsnToUserId.HasValue ? so.AsnToUserId.Value : 0,
                         AssignedTo = la.FullNme,
                         CreatedDateTime = so.CreatDt,
                         CreatedByUserId = lu.UserId,
                         CreatedByDisplayName = lu.DsplNme,
                         CreatedByEmail = lu.EmailAdr,
                         CreatedByFullName = lu.FullNme,
                         //RequestorLocation = string.Format("Phone: {0} Location: {1} {2}"
                         //           , lu.PhnNbr, lu.CtyNme, lu.SttCd)
                         RequestorLocation = string.Format("{0} {1} {2}", lu.PhnNbr, lu.CtyNme, lu.SttCd).Trim()
                     }).FirstOrDefault();

            return q;
        }

        public DataTable GetSDEProductTypes()
        {
            DataTable dtSDEProdTyp = new DataTable();
            dtSDEProdTyp.Columns.Add(new DataColumn("QTY"));
            dtSDEProdTyp.Columns.Add(new DataColumn("ProductTypeID"));
            dtSDEProdTyp.Columns.Add(new DataColumn("ProductType"));

            var q = (from sdt in _context.LkSdePrdctType
                     where sdt.RecStusId == (byte)Entities.Enums.ERecStatus.Active
                     orderby sdt.OrdrBySeqNbr
                     select new
                     {
                         QTY = 0,
                         ProductTypeID = sdt.SdePrdctTypeId,
                         ProductType = "<b>" + sdt.SdePrdctTypeNme + "</b> - "
                                     + sdt.SdePrdctTypeDesc,
                     });

            dtSDEProdTyp = LinqHelper.CopyToDataTable(q, null, null);

            return dtSDEProdTyp;
        }

        public IQueryable<LkSdePrdctType> GetAllSdeProductType(Expression<Func<LkSdePrdctType, bool>> predicate)
        {
            var sdeProductType = _context.LkSdePrdctType
                            .Where(predicate);

            return sdeProductType;
        }

        public DataTable GetWorkflowStatus(int statusID)
        {
            DataTable dtSDEWrkFlwStus = new DataTable();
            var q = (from ls in _context.LkStus
                     where ls.RecStusId == (byte)Entities.Enums.ERecStatus.Active
                     && ls.StusTypeId == "SDE"
                     orderby ls.StusId
                     select new
                     {
                         ls.StusId,
                         ls.StusDes
                     }).ToList();


            // For New, Status available should be New and Unassigned
            if (statusID == (int)SdeStatus.Draft)
                q.RemoveAll(i => i.StusId != (int)SdeStatus.Draft
                    && i.StusId != (int)SdeStatus.Unassigned);
            else if (statusID == (int)SdeStatus.Unassigned)
                q.RemoveAll(i => i.StusId != (int)SdeStatus.Draft
                    && i.StusId != (int)SdeStatus.Unassigned
                    && i.StusId != (int)SdeStatus.Assigned);

            // For Close, Status available should only be Close
            else if (statusID == (int)SdeStatus.Close)
                q.RemoveAll(i => i.StusId != (int)SdeStatus.Close);

            // Once assigned, Status available should be Assigned, Win, Lost, Ongoing and Close
            // Updated by Sarah Sandoval (09252017) - added Draft status
            else
                q.RemoveAll(i => i.StusId == (int)SdeStatus.Unassigned);


            dtSDEWrkFlwStus = LinqHelper.CopyToDataTable(q, null, null);
            return dtSDEWrkFlwStus;
        }

        public List<SDEOpportunityNote> GetSDENotes(int sdeID)
        {
            var q = (from son in _context.SdeOpptntyNte
                     join csd in _context.CustScrdData on son.SdeOpptntyNteId equals csd.ScrdObjId
                     join lu in _context.LkUser on son.CreatByUserId equals lu.UserId
                     where son.SdeOpptntyId == sdeID
                     && son.RecStusId == (byte)ERecStatus.Active
                     && (csd.ScrdObjTypeId == (byte)SecuredObjectType.SDE_OPPTNTY_NTE)
                     orderby son.CreatDt descending
                     select new SDEOpportunityNote
                     {
                         SdeNoteID = son.SdeOpptntyNteId,
                         NoteText = _common.GetDecryptValue(csd.NteTxt),
                         CreatedDateTime = son.CreatDt,
                         CreatedByUserId = son.CreatByUserId,
                         CreatedByUserName = lu.UserAdid,
                         CreatedByFullName = lu.FullNme
                     });

            return q.ToList();
        }

        public bool AddSDENote(int sdeOpportunityID, string noteText, int loggedInUser)
        {
            SdeOpptntyNte note = new SdeOpptntyNte();
            CustScrdData csd = new CustScrdData();
            note.SdeOpptntyId = sdeOpportunityID;
            note.CreatByUserId = loggedInUser;
            note.CreatDt = DateTime.Now;
            note.RecStusId = (byte)ERecStatus.Active;

            _context.SdeOpptntyNte.Add(note);
            _context.SaveChanges();

            csd.ScrdObjId = note.SdeOpptntyNteId;
            csd.ScrdObjTypeId = (byte)SecuredObjectType.SDE_OPPTNTY_NTE;
            csd.NteTxt = _common.GetEncryptValue(noteText);
            csd.CreatDt = DateTime.Now;
            _context.CustScrdData.Add(csd);
            _context.SaveChanges();

            return true;
        }

        public List<SDEOpportunityProduct> GetSDEProds(int sdeID)
        {
            var q = (from sop in _context.SdeOpptntyPrdct
                     join spt in _context.LkSdePrdctType
                        on sop.SdePrdctTypeId equals spt.SdePrdctTypeId
                     where sop.SdeOpptntyId == sdeID
                     && sop.RecStusId == (byte)ERecStatus.Active
                     orderby spt.OrdrBySeqNbr
                     select new SDEOpportunityProduct
                     {
                         Qty = sop.Qty,
                         ProductTypeID = spt.SdePrdctTypeId
                     });

            return q.ToList();
        }

        public List<SDEOpportunityDoc> GetSDEDocs(int sdeID)
        {
            var q = (from sod in _context.SdeOpptntyDoc
                     join lu in _context.LkUser on sod.CreatByUserId equals lu.UserId
                     where sod.SdeOpptntyId == sdeID
                     && sod.RecStusId == (byte)ERecStatus.Active
                     orderby sod.FileNme
                     select new SDEOpportunityDoc
                     {
                         SdeDocID = sod.SdeOpptntyDocId,
                         FileName = sod.FileNme,
                         FileContent = sod.FileCntnt.ToArray(),
                         FileSizeQuantity = sod.FileSizeQty,
                         CreatedDateTime = sod.CreatDt,
                         //CreatedByUserID = lu.UserId,
                         //CreatedByDspNme = lu.DsplNme,
                         //CreatedByEmail = lu.EmailAdr,
                         CreatedByFullName = lu.FullNme,
                         RecordStatus = sod.RecStusId == 1 ? ERecStatus.Active : ERecStatus.InActive
                     });

            return q.ToList();
        }

        public SDEOpportunityDoc GetSDEDocByID(int sdeDocID)
        {
            var q = (from sod in _context.SdeOpptntyDoc
                     where sod.SdeOpptntyDocId == sdeDocID
                     && sod.RecStusId == (byte)ERecStatus.Active
                     orderby sod.FileNme
                     select new SDEOpportunityDoc
                     {
                         SdeOpptntyDocId = sod.SdeOpptntyId,
                         SdeDocID = sod.SdeOpptntyDocId,
                         FileName = sod.FileNme,
                         FileContent = sod.FileCntnt.ToArray(),
                         FileSizeQuantity = sod.FileSizeQty,
                         CreatedDateTime = sod.CreatDt,
                         RecordStatus = sod.RecStusId == 1 ? ERecStatus.Active : ERecStatus.InActive
                     });

            return q.FirstOrDefault();
        }


        public string GetCowsAppCfgValue(string cfgKeyName)
        {
            var v = (from c in _context.LkCowsAppCfg
                     where c.CfgKeyNme == cfgKeyName
                     select new { c.CfgKeyValuTxt }).SingleOrDefault();
            return v.ToString();
        }

        public bool Insert(SdeOpportunity entity, int loggedInUserId)
        {
            SdeOpptnty sde = new SdeOpptnty();
            CustScrdData csd = new CustScrdData();
            csd.CustNme = _common.GetEncryptValue(entity.CompanyName);
            csd.StreetAdr1 = _common.GetEncryptValue(entity.Address);
            sde.Rgn = entity.Region;
            csd.SttPrvnNme = _common.GetEncryptValue(entity.State);
            csd.CtyNme = _common.GetEncryptValue(entity.City);
            csd.CustCntctNme = _common.GetEncryptValue(entity.ContactName);
            csd.CustEmailAdr = _common.GetEncryptValue(entity.ContactEmail);
            csd.CustCntctPhnNbr = _common.GetEncryptValue(entity.ContactPhone);
            sde.SaleType = entity.SaleType;
            sde.RfpCd = entity.IsRFP;
            sde.WrlneCustCd = entity.IsWirelineCustomer;
            sde.MngdSrvcCustCd = entity.IsManagedServicesCustomer;
            sde.CmntTxt = entity.Comments;
            sde.CcAdr = entity.CcEmail;
            sde.SdeStusId = (int)SdeStatus.Unassigned;
            sde.AsnToUserId = 1;
            sde.CreatByUserId = loggedInUserId;
            sde.CreatDt = DateTime.Now;
            sde.RecStusId = (byte)ERecStatus.Active;

            _context.SdeOpptnty.Add(sde);
            _context.SaveChanges();

            int newSDEOpportunityID = sde.SdeOpptntyId;
            csd.ScrdObjId = newSDEOpportunityID;
            csd.ScrdObjTypeId = (byte)SecuredObjectType.SDE_OPPTNTY;
            csd.CreatDt = DateTime.Now;
            _context.CustScrdData.Add(csd);
            _context.SaveChanges();

            entity.SDEOpportunityID = newSDEOpportunityID;

            // Saved document/s records for given OpportunityID
            for (var i = 0; i < entity.SdeDocs.Count; i++)
            {
                var doc = entity.SdeDocs[i];
            }

            _mssDocRepository.Insert(entity.SdeDocs, entity.SDEOpportunityID, loggedInUserId);

            // Saved product/s records for given OpportunityID

            _mssProdRepository.Insert(entity.SdeProducts, entity.SDEOpportunityID, loggedInUserId);

            // Saved notes for given OpportunityID and Comment Note
            if (!string.IsNullOrWhiteSpace(entity.Comments))
            {
                AddSDENote(newSDEOpportunityID, entity.Comments, loggedInUserId);
            }

            //// Saved email request record for given OpportunityID
            CreateEmailRequest(newSDEOpportunityID);

            return true;
        }

        public bool Update(SdeOpportunity entity, int loggedInUserId)
        {
            int sdeID = entity.SDEOpportunityID;
            SdeOpptnty updateSDE = (SdeOpptnty)(from sde in _context.SdeOpptnty
                                                where sde.SdeOpptntyId == sdeID
                                                select sde).Single();
            CustScrdData updtCSD = (CustScrdData)(from csd in _context.CustScrdData
                                                  where csd.ScrdObjId == sdeID && csd.ScrdObjTypeId == (byte)SecuredObjectType.SDE_OPPTNTY
                                                  select csd).SingleOrDefault();

            bool bInsertCSD = false;
            if (updtCSD == null)
            {
                updtCSD = new CustScrdData();
                bInsertCSD = true;
            }
            updtCSD.ScrdObjId = sdeID;
            updtCSD.ScrdObjTypeId = (byte)SecuredObjectType.SDE_OPPTNTY;
            int oldStatus = updateSDE.SdeStusId;
            int oldAssignedTo = updateSDE.AsnToUserId.HasValue ? updateSDE.AsnToUserId.Value : 0;
            updtCSD.CustCntctNme = _common.GetEncryptValue(entity.ContactName);
            updtCSD.CustEmailAdr = _common.GetEncryptValue(entity.ContactEmail);
            updtCSD.CustCntctPhnNbr = _common.GetEncryptValue(entity.ContactPhone);
            updateSDE.CcAdr = entity.CcEmail;
            updateSDE.SdeStusId = (short)entity.StatusID;
            updateSDE.AsnToUserId = entity.AssignedToId;
            updateSDE.ModfdByUserId = loggedInUserId;
            updateSDE.ModfdDt = DateTime.Now;

            if (oldStatus == (int)SdeStatus.Draft
                && (entity.StatusID == (int)SdeStatus.Draft || entity.StatusID == (int)SdeStatus.Unassigned))
            {
                // Updates other info on New Request
                updtCSD.CustNme = _common.GetEncryptValue(entity.CompanyName);
                updtCSD.StreetAdr1 = _common.GetEncryptValue(entity.Address);
                updateSDE.Rgn = entity.Region;
                updtCSD.SttPrvnNme = _common.GetEncryptValue(entity.State);
                updtCSD.CtyNme = _common.GetEncryptValue(entity.City);
                updtCSD.CustCntctNme = _common.GetEncryptValue(entity.ContactName);
                updtCSD.CustEmailAdr = _common.GetEncryptValue(entity.ContactEmail);
                updtCSD.CustCntctPhnNbr = _common.GetEncryptValue(entity.ContactPhone);
                updateSDE.SaleType = entity.SaleType;
                updateSDE.RfpCd = entity.IsRFP;
                updateSDE.WrlneCustCd = entity.IsWirelineCustomer;
                updateSDE.MngdSrvcCustCd = entity.IsManagedServicesCustomer;

                // Updates product/s records for given OpportunityID
                _mssProdRepository.Update(entity.SdeProducts, entity.SDEOpportunityID, loggedInUserId);
            }

            if ((bInsertCSD) && (updtCSD.ScrdObjId > 0))
            {
                updtCSD.CreatDt = DateTime.Now;
                _context.CustScrdData.Add(updtCSD);
            }
            _context.SaveChanges();

            // Updates document/s records for given OpportunityID

            _mssDocRepository.Update(entity.SdeDocs, entity.SDEOpportunityID, loggedInUserId);

            //// Saved notes for given OpportunityID and Comment Note
            if (!string.IsNullOrWhiteSpace(entity.Comments))
                AddSDENote(entity.SDEOpportunityID, entity.Comments, loggedInUserId);

            //// Saved notes when status is changed.
            if (entity.StatusID != oldStatus)
            {
                var status = _context.LkStus.Where(x => x.StusId == (short)entity.StatusID).Select(x => x.StusDes).FirstOrDefault();
                AddSDENote(entity.SDEOpportunityID, string.Format("Status changed to {0}.", status), loggedInUserId);
            }
             
            //// Saved notes when assigned person is changed.
            //// Send Email when assigned person is changed.
            if (entity.AssignedToId != 1 && oldAssignedTo != entity.AssignedToId)
            {
                var assignedTo = _context.LkUser.Where(x => x.UserId == entity.AssignedToId).Select(x => x.FullNme).FirstOrDefault();

                AddSDENote(entity.SDEOpportunityID, string.Format("{0} assigned to {1}.",
                    entity.SDEOpportunityID, assignedTo), loggedInUserId);
                CreateEmailRequest(entity.SDEOpportunityID);
            }

            return true;
        }

        //public bool Delete(int sdeID)
        //{
        //    SdeOpptnty sde = (SdeOpptnty)(from so in _context.SdeOpptnty
        //                                  where so.SdeOpptntyId == sdeID
        //                                  select so).Single();

        //    sde.RecStusId = (byte)ERecStatus.InActive;
        //    //sde.ModfdByUserId = LoggedUser.USER_ID;
        //    sde.ModfdDt = DateTime.Now;

        //    _context.SaveChanges();

        //    return true;
        //}
        //public bool Delete(int[] Ids)
        //{
        //    foreach (int id in Ids)
        //    {
        //        Delete(id);
        //    }

        //    return true;

        //}

        /// <summary>
        /// Creates new Email Request record.
        /// </summary>
        /// <param name="sdeID"></param>
        private bool CreateEmailRequest(int sdeID)
        {
            try
            {
                SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
                SqlCommand cmd = new SqlCommand("dbo.CreateSDEEmailRequest", connection);
                cmd.CommandTimeout = _context.commandTimeout;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("SDE_OPPTNTY_ID", sdeID);
                connection.Open();
                int i = cmd.ExecuteNonQuery();

                if (i == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Create EmailRequest for Account Setup
        /// </summary>
        /// <param name="requestTypeID"></param>
        /// <param name="To"></param>
        /// <param name="CC"></param>
        /// <param name="subj"></param>
        /// <param name="body"></param>
        public void CreateEmailRequest(int requestTypeID, string To, string CC, string subj, string body)
        {
            try
            {
                EmailReq er = new EmailReq();
                er.EmailReqTypeId = requestTypeID;
                er.EmailListTxt = To;
                er.EmailCcTxt = CC;
                er.EmailSubjTxt = subj;
                er.EmailBodyTxt = body;
                er.StusId = (int)EmailStatus.Pending;
                er.CreatDt = DateTime.Now;

                _context.EmailReq.Add(er);
                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public IQueryable<LkSdePrdctType> Find(Expression<Func<LkSdePrdctType, bool>> predicate)
        {
            return _context.LkSdePrdctType
                            .Where(predicate);
        }

        public byte[] byteStringToByteArray(string byteString)
        {
            return Convert.FromBase64String(byteString);
        }

        //public DataTable GetSDEViewUsers()
        //{
        //    try
        //    {
        //        if ((CacheObj.Cache["SDEViewUsers"] != null)
        //                && (((DataTable)CacheObj.Cache["SDEViewUsers"]).Rows.Count > 0))
        //            return ((DataTable)CacheObj.Cache["SDEViewUsers"]);
        //        else
        //        {
        //            List<int> commonIDs = new List<int> { 1, 3, 11 };

        //            var q = (from lu in _context.LkUser
        //                         join ugr in _context.usergrp on lu.USER_ID equals ugr.userId
        //                         where ugr.ROLE_ID == (int)ERoles.SystemSecurityEngineer
        //                         && ugr.REC_STUS_ID == (byte)ERecStatus.Active
        //                         && lu.REC_STUS_ID == (byte)ERecStatus.Active
        //                         && !commonIDs.Contains(lu.USER_ID)
        //                         select new
        //                         {
        //                             lu.USER_ID,
        //                             lu.FULL_NME
        //                         }).Distinct().OrderBy(i => i.FULL_NME);




        //            return ((DataTable)CacheObj.Cache["SDEViewUsers"]);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.ToString());
        //    }
        //}
    }
}