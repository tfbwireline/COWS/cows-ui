﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventRuleRepository : IEventRuleRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly UserRepository _userRepo;
        private IMemoryCache _cache;
        private readonly IConfiguration _configuration;

        public EventRuleRepository(COWSAdminDBContext context, IMemoryCache cache, IConfiguration configuration)
        {
            _context = context;
            _cache = cache;
            _configuration = configuration;
            _userRepo = new UserRepository(context, _cache, _configuration);
        }

        public IQueryable<LkEventRule> Find(Expression<Func<LkEventRule, bool>> predicate)
        {
            return _context.LkEventRule
                            .Include(a => a.Actn)
                            .Include(a => a.EndEventStus)
                            .Include(a => a.LkFedRuleFail)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkEventRule> GetAll()
        {
            return _context.LkEventRule
                            .AsNoTracking()
                            .ToList();
        }

        public LkEventRule GetById(int id)
        {
            return _context.LkEventRule
                .SingleOrDefault(a => a.EventRuleId == id);
        }

        public LkEventRule Create(LkEventRule entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkEventRule entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public LkEventRule GetEventRule(byte eventStatusId, byte workflowStatusId, int userId, int eventType, string orderTypeCd = null)
        {
            int profileId = _userRepo.GetFinalUserProfile(userId, eventType).UsrPrfId;

            if (eventType == 9)
            {
                LkEventRule er = _context.LkEventRule
                       .Include(a => a.StrtEventStus)
                       .Include(a => a.EndEventStus)
                       .Include(a => a.EventType)
                       .Include(a => a.WrkflwStus)
                       .Include(a => a.FailReas)
                       .Include(a => a.FedMsg)
                       .Include(a => a.UsrPrf)
                       .Include(a => a.EventRuleData)
                       .FirstOrDefault(
                           //a => a.UsrPrfId == profileId &&
                           a => a.EventTypeId == eventType &&
                           a.StrtEventStusId == eventStatusId &&
                           a.OrdrTypeCd == orderTypeCd &&
                           a.EndEventStusId == workflowStatusId);

                return er;
            }
            else
            {
                LkEventRule er = _context.LkEventRule
                    .Include(a => a.StrtEventStus)
                    .Include(a => a.EndEventStus)
                    .Include(a => a.EventType)
                    .Include(a => a.WrkflwStus)
                    .Include(a => a.FailReas)
                    .Include(a => a.FedMsg)
                    .Include(a => a.UsrPrf)
                    .Include(a => a.EventRuleData)
                    .FirstOrDefault(a => a.UsrPrfId == profileId &&
                        a.EventTypeId == eventType &&
                        a.StrtEventStusId == eventStatusId &&
                        a.WrkflwStusId == workflowStatusId);

                // Added by Sarah Sandoval [20200720]
                // To cater multiple profiles and get the next hierarchy
                if (er == null)
                {
                    var eventProfiles = _userRepo.GetActiveProfileIdByEventTypeId(userId, eventType);
                    eventProfiles.Remove(profileId);
                    if (eventProfiles != null && eventProfiles.Count() > 0)
                    {
                        er = _context.LkEventRule
                                .Include(a => a.StrtEventStus)
                                .Include(a => a.EndEventStus)
                                .Include(a => a.EventType)
                                .Include(a => a.WrkflwStus)
                                .Include(a => a.FailReas)
                                .Include(a => a.FedMsg)
                                .Include(a => a.UsrPrf)
                                .Include(a => a.EventRuleData)
                                .FirstOrDefault(a => a.UsrPrfId == eventProfiles[0] &&
                                    a.EventTypeId == eventType &&
                                    a.StrtEventStusId == eventStatusId &&
                                    a.WrkflwStusId == workflowStatusId);
                    }
                }

                return er;
            }
        }

        public LkEventRule GetEventRule2(byte eventStatusId, byte workflowStatusId, int userId, int eventType, string orderTypeCd = null, short? failReasId = null, byte? mdsActivityTypeId = null)
        {
            int profileId = _userRepo.GetFinalUserProfile(userId, eventType).UsrPrfId;

            //return Find(a => a.UsrPrfId == profileId &&
            //        a.EventTypeId == eventType &&
            //        a.StrtEventStusId == eventStatusId &&
            //        (a.EndEventStusId != eventStatusId || a.EndEventStusId == eventStatusId) &&
            //        a.WrkflwStusId == workflowStatusId)
            //    //.Include(a => a.StrtEventStus)
            //    //.Include(a => a.EndEventStus)
            //    //.Include(a => a.EventType)
            //    //.Include(a => a.WrkflwStus)
            //    //.Include(a => a.FailReas)
            //    //.Include(a => a.FedMsg)
            //    .SingleOrDefault();

            if (eventType == 9)
            {
                LkEventRule er = _context.LkEventRule
                       .Include(a => a.StrtEventStus)
                       .Include(a => a.EndEventStus)
                       .Include(a => a.EventType)
                       .Include(a => a.WrkflwStus)
                       .Include(a => a.FailReas)
                       .Include(a => a.FedMsg)
                       .Include(a => a.UsrPrf)
                       .Include(a => a.EventRuleData)
                       .FirstOrDefault(
                           //a => a.UsrPrfId == profileId &&
                           a => a.EventTypeId == eventType &&
                           a.StrtEventStusId == eventStatusId &&
                           a.OrdrTypeCd == orderTypeCd &&
                           a.EndEventStusId == workflowStatusId);

                //LkEventRule data = null;

                //if (er.Count > 0)
                //    data = er[0];

                return er;
            }
            else
            {
                LkEventRule er = null;
                if ((mdsActivityTypeId == 4) || (mdsActivityTypeId == 5))
                {
                    er = _context.LkEventRule
                        .Include(a => a.StrtEventStus)
                        .Include(a => a.EndEventStus)
                        .Include(a => a.EventType)
                        .Include(a => a.WrkflwStus)
                        .Include(a => a.FailReas)
                        .Include(a => a.FedMsg)
                        .Include(a => a.UsrPrf)
                        .Include(a => a.EventRuleData)
                        .FirstOrDefault(a => a.UsrPrfId == profileId &&
                            a.EventTypeId == eventType &&
                            a.StrtEventStusId == eventStatusId &&
                            a.WrkflwStusId == workflowStatusId &&
                            a.FailReasId == failReasId &&
                            a.StagnCd == true);

                    if (er != null)
                        return er;
                }
                er = _context.LkEventRule
                     .Include(a => a.StrtEventStus)
                     .Include(a => a.EndEventStus)
                     .Include(a => a.EventType)
                     .Include(a => a.WrkflwStus)
                     .Include(a => a.FailReas)
                     .Include(a => a.FedMsg)
                     .Include(a => a.UsrPrf)
                     .Include(a => a.EventRuleData)
                     .FirstOrDefault(a => a.UsrPrfId == profileId &&
                         a.EventTypeId == eventType &&
                         a.StrtEventStusId == eventStatusId &&
                         a.WrkflwStusId == workflowStatusId &&
                         a.FailReasId == failReasId);

                return er;
            }
        }
    }
}