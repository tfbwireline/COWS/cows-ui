﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class RedesignDevInfoRepository : IRedesignDevInfoRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _config;

        public RedesignDevInfoRepository(COWSAdminDBContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        public RedsgnDevicesInfo Create(RedsgnDevicesInfo entity)
        {
            int maxId = _context.RedsgnDevicesInfo.Max(i => i.RedsgnDevId);
            entity.RedsgnDevId = (short)++maxId;
            _context.RedsgnDevicesInfo.Add(entity);

            SaveAll();

            return GetById(entity.RedsgnDevId);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<RedsgnDevicesInfo> Find(Expression<Func<RedsgnDevicesInfo, bool>> predicate)
        {
            return _context.RedsgnDevicesInfo
                            .Where(predicate);
        }

        public IEnumerable<RedsgnDevicesInfo> GetAll()
        {
            return _context.RedsgnDevicesInfo
                            .OrderBy(i => i.DevNme)
                            .ToList();
        }

        public RedsgnDevicesInfo GetById(int id)
        {
            return _context.RedsgnDevicesInfo
                            .SingleOrDefault(i => i.RedsgnDevId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, RedsgnDevicesInfo entity)
        {
            RedsgnDevicesInfo rdi = GetById(id);
            rdi.RecStusId = entity.RecStusId;
            rdi.ModfdByCd = entity.ModfdByCd.Value;
            rdi.ModfdDt = entity.ModfdDt;

            SaveAll();
        }

        public DataTable GetRedesignDevices(int redesignId)
        {
            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.getRdsnDevices", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@redesignID", redesignId);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }
    }
}
