﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class H5DocRepository : IH5DocRepository
    {
        private readonly COWSAdminDBContext _context;

        public H5DocRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<H5Doc> Find(Expression<Func<H5Doc, bool>> predicate)
        {
            return _context.H5Doc
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<H5Doc> GetAll()
        {
            return _context.H5Doc
                .Include(a => a.CreatByUser)
                .ToList();
        }

        public H5Doc GetById(int id)
        {
            return _context.H5Doc
                .Include(a => a.CreatByUser)
                .SingleOrDefault(a => a.H5DocId == id);
        }

        public H5Doc Create(H5Doc entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, H5Doc entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}