﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VendorFolderContactRepository : IVendorFolderContactRepository
    {
        private readonly COWSAdminDBContext _context;

        public VendorFolderContactRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public VndrFoldrCntct Create(VndrFoldrCntct entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<VndrFoldrCntct> Find(Expression<Func<VndrFoldrCntct, bool>> predicate)
        {
            return _context.VndrFoldrCntct
                            .Where(predicate);
        }

        public IEnumerable<VndrFoldrCntct> GetAll()
        {
            throw new NotImplementedException();
        }

        public VndrFoldrCntct GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, VndrFoldrCntct entity)
        {
            throw new NotImplementedException();
        }

        public VndrFoldrCntct CreateVendorFolderContact(VndrFoldrCntct entity)
        {
            _context.VndrFoldrCntct.Add(entity);
            SaveAll();

            return Find(i => i.CntctId == entity.CntctId).SingleOrDefault();
        }

        public void UpdateVendorFolderContact(VndrFoldrCntct entity, string ctryCd)
        {
            VndrFoldrCntct vndrFolderContact = _context.VndrFoldrCntct.SingleOrDefault(i => i.CntctId == entity.CntctId);
            if (vndrFolderContact != null)
            {
                vndrFolderContact.CntctId = entity.CntctId;
                vndrFolderContact.VndrFoldrId = entity.VndrFoldrId;
                vndrFolderContact.CntctFrstNme = entity.CntctFrstNme;
                vndrFolderContact.CntctLstNme = entity.CntctLstNme;
                vndrFolderContact.CntctEmailAdr = entity.CntctEmailAdr;
                vndrFolderContact.CntctProdTypeTxt = entity.CntctProdTypeTxt;
                vndrFolderContact.CntctProdRoleTxt = entity.CntctProdRoleTxt;
                vndrFolderContact.CntctPhnNbr = entity.CntctPhnNbr;

                SaveAll();
            }
        }
    }
}