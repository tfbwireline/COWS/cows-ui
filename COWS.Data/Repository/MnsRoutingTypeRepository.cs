﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MnsRoutingTypeRepository : IMnsRoutingTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MnsRoutingTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMnsRoutgType Create(LkMnsRoutgType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMnsRoutgType> Find(Expression<Func<LkMnsRoutgType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMnsRoutgType, out List<LkMnsRoutgType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMnsRoutgType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMnsRoutgType, out List<LkMnsRoutgType> list))
            {
                list = _context.LkMnsRoutgType
                            .OrderBy(i => i.MnsRoutgTypeDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMnsRoutgType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMnsRoutgType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMnsRoutgType);
            throw new NotImplementedException();
        }

        public void Update(int id, LkMnsRoutgType entity)
        {
            throw new NotImplementedException();
        }
    }
}