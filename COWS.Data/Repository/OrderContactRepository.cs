﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderContactRepository : IOrderContactRepository
    {
        private readonly COWSAdminDBContext _context;
        private IConfiguration _configuration;
        private readonly ICommonRepository _common;

        public OrderContactRepository(COWSAdminDBContext context,
            IConfiguration configuration,
            ICommonRepository common)
        {
            _context = context;
            _configuration = configuration;
            _common = common;
        }

        public OrdrCntct Create(OrdrCntct entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<OrdrCntct> Find(Expression<Func<OrdrCntct, bool>> predicate)
        {
            return _context.OrdrCntct
                .Include(a => a.CntctType)
                .Include(a => a.Role)
                .Where(predicate);
        }

        public IEnumerable<OrdrCntct> GetAll()
        {
            return _context.OrdrCntct
                            .OrderBy(i => i.OrdrId)
                            .ToList();
        }

        public OrdrCntct GetById(int id)
        {
            throw new NotImplementedException();
            //return _context.OrdrCntct
            //                .SingleOrDefault(i => i.OrdrId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, OrdrCntct entity)
        {
            throw new NotImplementedException();
        }

        public List<OrdrCntct> GetSecuredOrdrCntctById(int id)
        {
            int csgLvlId = _context.Ordr.Where(a => a.OrdrId == id).FirstOrDefault().CsgLvlId;
            if (csgLvlId > 0)
            {


                var data = from oc in _context.OrdrCntct
                           join csd in _context.CustScrdData on oc.OrdrCntctId equals csd.ScrdObjId
                           where oc.OrdrId == id && csd.ScrdObjTypeId == 15

                           select new OrdrCntct
                           {
                               CisLvlType = oc.CisLvlType,
                               Role = oc.Role,
                               CntctType = oc.CntctType,
                               FrstNme = oc.FrstNme ?? GetDecryptValue(csd.FrstNme),
                               LstNme = oc.FrstNme ?? GetDecryptValue(csd.LstNme),
                               EmailAdr = oc.EmailAdr ?? GetDecryptValue(csd.CustEmailAdr),
                               CntctNme = oc.CntctNme ?? GetDecryptValue(csd.CustCntctNme),
                               IsdCd = oc.IsdCd,
                               CtyCd = oc.CtyCd,
                               Npa = oc.Npa,
                               Nxx = oc.Nxx,
                               StnNbr = oc.StnNbr,
                               PhnNbr = oc.PhnNbr,
                               FaxNbr = oc.FaxNbr,
                               PhnExtNbr = oc.PhnExtNbr,
                               CntctTypeId = oc.CntctTypeId,
                               FsaMdulId = oc.FsaMdulId
                           };

                return data.ToList();
            }
            else {
                return _context.OrdrCntct
                    .Include(a => a.CntctType)
                    .Include(a => a.Role)
                    .Where(a => a.OrdrId == id).ToList();
            }
        }

        public OrdrCntct GetH6ContactByOrderId(int id, int csgLvlId)
        {
            int[] roles = { 86, 87, 92, 6, 16, 18, 71 };
            Ordr order = _context.Ordr.Where(a => a.OrdrId == id).FirstOrDefault();
            IEnumerable<OrdrCntct> orderContacts = _context.OrdrCntct
                .Include(a => a.Role)
                .Include(a => a.CntctType)
                .Where(a => a.OrdrId == id && a.CisLvlType == "H6" && a.CntctTypeId != 17)
                .ToList();
            OrdrCntct orderContact = orderContacts.Any(a => roles.Contains((int)a.RoleId)) ? orderContacts.Where(a => roles.Contains((int)a.RoleId)).FirstOrDefault() : orderContacts.FirstOrDefault();

            if (csgLvlId > 0 && csgLvlId <= order.CsgLvlId)
            {
                var securedNtwkCust = _context.CustScrdData
                    .Where(a => a.ScrdObjId == orderContact.OrdrCntctId && a.ScrdObjTypeId == (byte)SecuredObjectType.ORDR_CNTCT)
                    .FirstOrDefault();
                if (securedNtwkCust != null)
                {
                    orderContact.FrstNme = _common.GetDecryptValue(securedNtwkCust.FrstNme);
                    orderContact.LstNme = _common.GetDecryptValue(securedNtwkCust.LstNme);
                    orderContact.CntctNme = _common.GetDecryptValue(securedNtwkCust.CustCntctNme);
                    orderContact.EmailAdr = _common.GetDecryptValue(securedNtwkCust.CustEmailAdr);
                }
            }

            return orderContact;
        }

        public string GetDecryptValue(byte[] encryptVal)
        {
            if ((encryptVal != null) && (encryptVal.Length > 0))
            {
                SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
                //SqlConnection connection = new SqlConnection(_configuration.GetSection("ConnectionStrings:SqlDbConn").Value);
                SqlCommand command = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                command = new SqlCommand("dbo.GetDeCryptValue", connection);
                command.CommandTimeout = _context.commandTimeout;
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@EncryptData", encryptVal); // if you have parameters.
                da = new SqlDataAdapter(command);
                da.Fill(ds);
                connection.Close();

                return ds.Tables[0].Rows[0][0].ToString();
            }

            return string.Empty;
        }
    }
}