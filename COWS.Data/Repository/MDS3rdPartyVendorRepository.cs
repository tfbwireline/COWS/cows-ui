﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDS3rdPartyVendorRepository : IMDS3rdPartyVendorRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MDS3rdPartyVendorRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMds3rdpartyVndr Create(LkMds3rdpartyVndr entity)
        {
            int maxId = _context.LkMds3rdpartyVndr.Max(i => i.ThrdPartyVndrId);
            entity.ThrdPartyVndrId = (short)++maxId;
            _context.LkMds3rdpartyVndr.Add(entity);

            SaveAll();

            return GetById(entity.ThrdPartyVndrId);
        }

        public void Delete(int id)
        {
            LkMds3rdpartyVndr obj = GetById(id);
            _context.LkMds3rdpartyVndr.Remove(obj);

            SaveAll();
        }

        public IQueryable<LkMds3rdpartyVndr> Find(Expression<Func<LkMds3rdpartyVndr, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMds3rdpartyVndr, out List<LkMds3rdpartyVndr> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMds3rdpartyVndr> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMds3rdpartyVndr, out List<LkMds3rdpartyVndr> list))
            {
                list = _context.LkMds3rdpartyVndr
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.SrvcType)
                            .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                            .AsNoTracking()
                            .ToList();

                _cache.Set(CacheKeys.LkMds3rdpartyVndr, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMds3rdpartyVndr GetById(int id)
        {
            return _context.LkMds3rdpartyVndr
                            .Include(i => i.CreatByUser)
                            .Include(i => i.SrvcType)
                            .SingleOrDefault(i => i.ThrdPartyVndrId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMds3rdpartyVndr);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMds3rdpartyVndr entity)
        {
            LkMds3rdpartyVndr obj = GetById(id);
            obj.ThrdPartyVndrDes = entity.ThrdPartyVndrDes;
            obj.SrvcTypeId = entity.SrvcTypeId;
            obj.RecStusId = entity.RecStusId;
            obj.ModfdByUserId = entity.ModfdByUserId;
            obj.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}