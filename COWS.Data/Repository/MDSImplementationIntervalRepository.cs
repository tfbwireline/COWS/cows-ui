﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSImplementationIntervalRepository : IMDSImplementationIntervalRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MDSImplementationIntervalRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkMdsImplmtnNtvl> Find(Expression<Func<LkMdsImplmtnNtvl, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsImplmtnNtvl, out List<LkMdsImplmtnNtvl> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMdsImplmtnNtvl> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsImplmtnNtvl, out List<LkMdsImplmtnNtvl> list))
            {
                list = _context.LkMdsImplmtnNtvl
                            .Include(a => a.EventType)
                            .AsNoTracking()
                            .ToList();

                _cache.Set(CacheKeys.LkMdsImplmtnNtvl, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMdsImplmtnNtvl GetById(int id)
        {
            return _context.LkMdsImplmtnNtvl
                .Include(a => a.EventType)
                .SingleOrDefault(a => a.EventTypeId == id);
        }

        public LkMdsImplmtnNtvl Create(LkMdsImplmtnNtvl entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkMdsImplmtnNtvl entity)
        {
            LkMdsImplmtnNtvl interval = GetById(id);
            interval.CpeNtvlInDayQty = entity.CpeNtvlInDayQty;
            interval.NonCpeNtvlInDayQty = entity.NonCpeNtvlInDayQty;
            interval.ModfdByUserId = entity.ModfdByUserId;
            interval.ModfdDt = entity.ModfdDt;

            SaveAll();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMdsImplmtnNtvl);
            return _context.SaveChanges();
        }
    }
}