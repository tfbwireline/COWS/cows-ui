﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Data.Repository
{
    public class MSSProdRepository : IMSSProdRepository
    {
        private readonly COWSAdminDBContext _context;

        public MSSProdRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public List<SDEOpportunityProduct> Get(int sdeID)
        {
            var q = (from sop in _context.SdeOpptntyPrdct
                     join spt in _context.LkSdePrdctType
                        on sop.SdePrdctTypeId equals spt.SdePrdctTypeId
                     where sop.SdeOpptntyId == sdeID
                     && sop.RecStusId == (byte)ERecStatus.Active
                     orderby spt.OrdrBySeqNbr
                     select new SDEOpportunityProduct
                     {
                         Qty = sop.Qty,
                         ProductTypeID = spt.SdePrdctTypeId
                     });

            return q.ToList();
        }

        /// <summary>
        /// Creates new SDE Opportunity Product records.
        /// </summary>
        /// <param name="sdeProds"></param>
        /// <param name="sdeID"></param>
        public void Insert(List<SDEOpportunityProduct> sdeProds, int sdeID, int loggedInUserId)
        {
            if (sdeProds != null && sdeProds.Count > 0)
            {
                SdeOpptntyPrdct newSDEProd;

                foreach (SDEOpportunityProduct item in sdeProds)
                {
                    newSDEProd = new SdeOpptntyPrdct();
                    newSDEProd.SdeOpptntyId = sdeID;
                    newSDEProd.SdePrdctTypeId = (short)item.ProductTypeID;
                    newSDEProd.Qty = item.Qty;
                    newSDEProd.CreatByUserId = loggedInUserId;
                    newSDEProd.CreatDt = DateTime.Now;
                    newSDEProd.RecStusId = (byte)ERecStatus.Active;

                    _context.SdeOpptntyPrdct.Add(newSDEProd);
                }
                _context.SaveChanges();
            }
        }

        /// <summary>
        /// Updates SDE Product records by OpportunityID
        /// </summary>
        /// <param name="sdeProds"></param>
        /// <param name="sdeID"></param>
        public void Update(List<SDEOpportunityProduct> sdeProds, int sdeID, int loggedInUserId)
        {
            if (sdeProds != null && sdeProds.Count > 0)
            {
                List<SDEOpportunityProduct> prods = new List<SDEOpportunityProduct>();
                foreach (SDEOpportunityProduct item in sdeProds)
                {
                    SdeOpptntyPrdct updateProd =
                                (SdeOpptntyPrdct)(from prod in _context.SdeOpptntyPrdct
                                                  where prod.SdeOpptntyId == sdeID
                                                  && prod.SdePrdctTypeId == item.ProductTypeID
                                                  select prod).SingleOrDefault();

                    if (updateProd != null)
                    {
                        if (item.Qty != (int)updateProd.Qty)
                        {
                            updateProd.Qty = item.Qty;
                            updateProd.RecStusId = (byte)ERecStatus.Active;
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        if (item.Qty != 0)
                            prods.Add(item);
                    }
                }

                if (prods != null && prods.Count > 0)
                    Insert(prods, sdeID, loggedInUserId);
            }
        }
    }
}