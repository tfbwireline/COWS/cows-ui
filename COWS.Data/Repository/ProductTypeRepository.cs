﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class ProductTypeRepository : IProductTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public ProductTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkProdType> Find(Expression<Func<LkProdType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkProdType, out List<LkProdType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkProdType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkProdType, out List<LkProdType> list))
            {
                list = _context.LkProdType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkProdType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkProdType GetById(int id)
        {
            return _context.LkProdType
                .SingleOrDefault(a => a.ProdTypeId == id);
        }

        public LkProdType Create(LkProdType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkProdType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkProdType);
            return _context.SaveChanges();
        }

        public IEnumerable<string> GetProductPlatformCombinations()
        {
            List<string> toReturn = new List<string>();

            var data = from pl in _context.PltfrmMapng
                       join p in _context.LkProdType on pl.FsaProdTypeCd equals p.FsaProdTypeCd
                       select new { p.ProdTypeId, pl.PltfrmCd, p.ProdNme };

            foreach (var c in data)
            {
                toReturn.Add(c.ProdTypeId.ToString() + "-" + c.PltfrmCd ?? string.Empty);
            }

            return toReturn;
        }
    }
}