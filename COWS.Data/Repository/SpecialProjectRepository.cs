﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class SpecialProjectRepository : ISpecialProjectRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public SpecialProjectRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkSpclProj Create(LkSpclProj entity)
        {
            int maxId = _context.LkSpclProj.Max(i => i.SpclProjId);
            entity.SpclProjId = (short)++maxId;
            _context.LkSpclProj.Add(entity);

            SaveAll();

            return GetById(entity.SpclProjId);
        }

        public void Delete(int id)
        {
            LkSpclProj proj = GetById(id);
            _context.LkSpclProj.Remove(proj);

            SaveAll();
        }

        public IQueryable<LkSpclProj> Find(Expression<Func<LkSpclProj, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkSpclProj, out List<LkSpclProj> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkSpclProj> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkSpclProj, out List<LkSpclProj> list))
            {
                list = _context.LkSpclProj
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderBy(i => i.SpclProjNme)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkSpclProj, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkSpclProj GetById(int id)
        {
            return _context.LkSpclProj
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.SpclProjId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkXnciRgn);
            return _context.SaveChanges();
        }

        public void Update(int id, LkSpclProj entity)
        {
            LkSpclProj proj = GetById(id);
            proj.SpclProjNme = entity.SpclProjNme;
            proj.RecStusId = entity.RecStusId;
            proj.ModfdByUserId = entity.ModfdByUserId;
            proj.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}