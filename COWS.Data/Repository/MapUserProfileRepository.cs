﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MapUserProfileRepository : IMapUserProfileRepository
    {
        private readonly COWSAdminDBContext _context;

        public MapUserProfileRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<MapUsrPrf> Find(Expression<Func<MapUsrPrf, bool>> predicate)
        {
            return _context.MapUsrPrf
                .Include(a => a.User)
                .Include(a => a.UsrPrf)
                //.ThenInclude(a => a.LkEventRule)
                .Where(predicate);
        }

        public IEnumerable<MapUsrPrf> GetAll()
        {
            return _context.MapUsrPrf
                .ToList();
        }

        public MapUsrPrf Create(MapUsrPrf entity)
        {
            //int maxId = _context.MapUsrPrf.Max(i => i.Id);
            //entity.Id = (byte)++maxId;
            _context.MapUsrPrf.Add(entity);

            SaveAll();

            return GetById(entity.Id);
        }

        public void Update(int id, MapUsrPrf entity)
        {
            MapUsrPrf profile = GetById(id);
            profile.RecStusId = entity.RecStusId;
            profile.ModfdByUserId = entity.ModfdByUserId;
            profile.ModfdDt = entity.ModfdDt;

            SaveAll();
        }

        public void Delete(int id)
        {
            MapUsrPrf profile = GetById(id);
            _context.MapUsrPrf.Remove(profile);

            //SaveAll();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public MapUsrPrf GetById(int id)
        {
            return _context.MapUsrPrf
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.Id == id);
        }
    }
}