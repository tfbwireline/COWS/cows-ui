﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class RedesignTypeRepository : IRedesignTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public RedesignTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkRedsgnType> Find(Expression<Func<LkRedsgnType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkRedsgnType, out List<LkRedsgnType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkRedsgnType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkRedsgnType, out List<LkRedsgnType> list))
            {
                list = _context.LkRedsgnType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkRedsgnType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkRedsgnType GetById(int id)
        {
            return _context.LkRedsgnType
                .SingleOrDefault(a => a.RedsgnTypeId == id);
        }

        public LkRedsgnType Create(LkRedsgnType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkRedsgnType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkRedsgnType);
            return _context.SaveChanges();
        }
    }
}