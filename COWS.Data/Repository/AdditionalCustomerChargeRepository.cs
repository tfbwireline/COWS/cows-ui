﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace COWS.Data.Repository
{
    public class AdditionalCustomerChargeRespository : IAdditionalCustomerChargeRepository
    {
        private readonly COWSAdminDBContext _context;

        public AdditionalCustomerChargeRespository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public DataTable searchFTN(string FTN)
        {
            var data = (from fsa in _context.FsaOrdr
                        join ot in _context.LkFsaOrdrType on fsa.OrdrTypeCd equals ot.FsaOrdrTypeCd
                        join pt in _context.LkFsaProdType on fsa.ProdTypeCd equals pt.FsaProdTypeCd
                        join ol in _context.OrdrRecLock on fsa.OrdrId equals ol.OrdrId into leftjoin
                        from subol in leftjoin.DefaultIfEmpty()
                        where fsa.Ftn == FTN
                        select new { ordrId = fsa.OrdrId, ftn = fsa.Ftn, ordrType = ot.FsaOrdrTypeDes, prodType = pt.FsaProdTypeDes, ol = subol.LockByUserId.ToString() })
                             .Union(
                              from ipl in _context.IplOrdr
                              join o in _context.Ordr on ipl.OrdrId equals o.OrdrId
                              join ot in _context.LkOrdrType on ipl.OrdrTypeId equals ot.OrdrTypeId
                              join pt in _context.LkProdType on ipl.ProdTypeId equals pt.ProdTypeId
                              join ol in _context.OrdrRecLock on ipl.OrdrId equals ol.OrdrId into leftjoin2
                              from subol in leftjoin2.DefaultIfEmpty()
                              where ipl.OrdrId == Convert.ToInt32(FTN)
                              select new { ordrId = o.OrdrId, ftn = ipl.OrdrId.ToString(), ordrType = ot.OrdrTypeDes, prodType = pt.ProdTypeDes, ol = subol.LockByUserId.ToString() })
                              ;

            return LinqHelper.CopyToDataTable(data, null, null);
        }

        public List<CircuitAddlCost> getAddlCharges(int ordrId, bool isTerm)
        {
            List<CircuitAddlCost> toReturn = new List<CircuitAddlCost>();



            var dd = (from myMax in
                                  (from mVer in
                                       (from ac in _context.OrdrCktChg
                                        where ac.OrdrId == ordrId
                                        select new { ac.VerId, ac.CktChgTypeId, ac.TrmtgCd })
                                   group mVer by new { mVer.CktChgTypeId, mVer.TrmtgCd } into g
                                   select new { pair = g.Key, maxVer = g.Max(mVer => mVer.VerId) })
                      join finalact in _context.OrdrCktChg on myMax.pair equals new { finalact.CktChgTypeId, finalact.TrmtgCd }
                      join addlct in _context.LkCktChgType on finalact.CktChgTypeId equals addlct.CktChgTypeId
                      join user in _context.LkUser on finalact.CreatByUserId equals user.UserId
                      join c in _context.LkStus on finalact.SalsStusId equals c.StusId into leftjoin
                      from subc in leftjoin.DefaultIfEmpty()
                      where myMax.maxVer == finalact.VerId && finalact.OrdrId == ordrId && finalact.TrmtgCd == isTerm
                      select new { finalact, addlct.CktChgTypeDes, user.UserAdid, user.FullNme, subc.StusDes }).OrderBy(d => d.CktChgTypeDes);

            foreach (var d in dd)
            {
                CircuitAddlCost cac = new CircuitAddlCost();

                cac.OrderID = d.finalact.OrdrId;
                cac.ChargeTypeID = d.finalact.CktChgTypeId;
                cac.ChargeTypeDesc = d.CktChgTypeDes;
                cac.Version = d.finalact.VerId;
                cac.IsTerm = d.finalact.TrmtgCd;
                cac.CurrencyCd = d.finalact.ChgCurId;
                cac.ChargeNRC = d.finalact.ChgNrcAmt;
                cac.ChargeNRCUSD = d.finalact.ChgNrcInUsdAmt ?? 0;
                cac.CreatedByDspNme = d.FullNme;
                cac.CreatedByUserName = d.UserAdid;
                cac.CreatedByUserID = d.finalact.CreatByUserId.ToString();
                cac.CreatedDateTime = d.finalact.CreatDt;
                cac.TaxRate = d.finalact.TaxRtPctQty ?? 0;
                cac.Note = d.finalact.NteTxt;
                cac.BillOnly = d.finalact.ReqrBillOrdrCd ?? false;
                if (d.finalact.XpirnDt != null)
                    cac.ExpirationTime = Convert.ToDateTime(d.finalact.XpirnDt);
                if (d.finalact.SalsStusId != null)
                {
                    cac.SalesSupportStatusID = Convert.ToByte(d.finalact.SalsStusId);
                    cac.SalesSupportStatusDes = d.StusDes;
                }

                toReturn.Add(cac);
            }

            return toReturn;
        }

        public List<CircuitAddlCost> getAddlChargeHistory(int OrderID, int chargeTypeID, bool IsTerm)
        {
            List<CircuitAddlCost> toReturn = new List<CircuitAddlCost>();



            var AdditionalChargeHistory = (from ac in _context.OrdrCktChg
                                           join user in _context.LkUser on ac.CreatByUserId equals user.UserId
                                           join act in _context.LkCktChgType on ac.CktChgTypeId equals act.CktChgTypeId
                                           join l in _context.LkStus on ac.SalsStusId equals l.StusId into leftjoin
                                           from subl in leftjoin.DefaultIfEmpty()
                                           where ac.OrdrId == OrderID && ac.CktChgTypeId == chargeTypeID && ac.TrmtgCd == IsTerm
                                           select new { ac, user.UserAdid, user.FullNme, act.CktChgTypeDes, subl.StusDes }).OrderBy(o => o.ac.VerId);



            foreach (var ac in AdditionalChargeHistory)
            {
                CircuitAddlCost cac = new CircuitAddlCost();

                cac.OrderID = ac.ac.OrdrId;
                cac.ChargeTypeID = ac.ac.CktChgTypeId;
                cac.ChargeTypeDesc = ac.CktChgTypeDes;
                cac.Version = ac.ac.VerId;
                cac.IsTerm = ac.ac.TrmtgCd;
                cac.CurrencyCd = ac.ac.ChgCurId;
                cac.ChargeNRC = ac.ac.ChgNrcAmt;
                cac.ChargeNRCUSD = ac.ac.ChgNrcInUsdAmt ?? 0;
                cac.CreatedByDspNme = ac.FullNme;
                cac.CreatedByUserName = ac.UserAdid;
                cac.CreatedByUserID = ac.ac.CreatByUserId.ToString();
                cac.CreatedDateTime = ac.ac.CreatDt;
                cac.TaxRate = ac.ac.TaxRtPctQty ?? 0;
                cac.Note = ac.ac.NteTxt;
                cac.BillOnly = ac.ac.ReqrBillOrdrCd ?? false;
                if (ac.ac.XpirnDt != null)
                    cac.ExpirationTime = Convert.ToDateTime(ac.ac.XpirnDt);
                if (ac.ac.SalsStusId != null)
                {
                    cac.SalesSupportStatusID = Convert.ToByte(ac.ac.SalsStusId);
                    cac.SalesSupportStatusDes = ac.StusDes;
                }

                toReturn.Add(cac);
            }

            return toReturn;
        }
    }
}