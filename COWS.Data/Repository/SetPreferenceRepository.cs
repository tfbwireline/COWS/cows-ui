﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class SetPreferenceRepository : ISetPreferenceRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public SetPreferenceRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCnfrcBrdg Create(LkCnfrcBrdg entity)
        {
            int maxId = _context.LkCnfrcBrdg.Max(i => i.CnfrcBrdgId);
            entity.CnfrcBrdgId = (short)++maxId;
            _context.LkCnfrcBrdg.Add(entity);

            SaveAll();

            return GetById(entity.CnfrcBrdgId);
        }

        public void Delete(int id)
        {
            LkCnfrcBrdg conf = GetById(id);
            _context.LkCnfrcBrdg.Remove(conf);

            SaveAll();
        }

        public IQueryable<LkCnfrcBrdg> Find(Expression<Func<LkCnfrcBrdg, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCnfrcBrdg, out List<LkCnfrcBrdg> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCnfrcBrdg> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCnfrcBrdg, out List<LkCnfrcBrdg> list))
            {
                list = _context.LkCnfrcBrdg
                            .Include(a => a.CreatByUser)
                            .Include(a => a.ModfdByUser)
                            .OrderBy(i => i.CnfrcBrdgId)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCnfrcBrdg, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCnfrcBrdg GetById(int id)
        {
            return _context.LkCnfrcBrdg
                            .SingleOrDefault(i => i.CnfrcBrdgId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCnfrcBrdg);
            return _context.SaveChanges();
        }

        public void Update(int id, LkCnfrcBrdg entity)
        {
            LkCnfrcBrdg conf = GetById(id);
            conf.CnfrcBrdgNbr = entity.CnfrcBrdgNbr;
            conf.CnfrcPinNbr = entity.CnfrcPinNbr;
            conf.Soi = entity.Soi;
            conf.OnlineMeetingAdr = entity.OnlineMeetingAdr;
            conf.RecStusId = entity.RecStusId;
            conf.ModfdByUserId = entity.ModfdByUserId;
            conf.ModfdDt = entity.ModfdDt;
            
            SaveAll();
        }
    }
}