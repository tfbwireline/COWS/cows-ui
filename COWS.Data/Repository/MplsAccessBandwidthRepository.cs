﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MplsAccessBandwidthRepository : IMplsAccessBandwidthRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MplsAccessBandwidthRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMplsAccsBdwd Create(LkMplsAccsBdwd entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMplsAccsBdwd> Find(Expression<Func<LkMplsAccsBdwd, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMplsAccsBdwd, out List<LkMplsAccsBdwd> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMplsAccsBdwd> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMplsAccsBdwd, out List<LkMplsAccsBdwd> list))
            {
                list = _context.LkMplsAccsBdwd
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMplsAccsBdwd, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMplsAccsBdwd GetById(int id)
        {
            return _context.LkMplsAccsBdwd
                            .AsNoTracking()
                            .SingleOrDefault(i => i.MplsAccsBdwdId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMplsAccsBdwd);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMplsAccsBdwd entity)
        {
            throw new NotImplementedException();
        }
    }
}