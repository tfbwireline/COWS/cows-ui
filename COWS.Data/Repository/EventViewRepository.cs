﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Data.Repository
{
    public class EventViewRepository : IEventViewRepository
    {
        private readonly COWSAdminDBContext _context;
        public IConfiguration _config { get; }

        public EventViewRepository(COWSAdminDBContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        public async Task<DataTable> GetEventViewDetailsAsync(int viewId, int siteCntnt, int userId, string newMDS, byte userCsgLevel)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"web.getViewDetails_V2 {viewId},{siteCntnt}, {userId}, {newMDS}, {userCsgLevel}, {1}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    List<string> listtoRemove = new List<string> { "CSG_LVL_ID", "EVENT STATUS ID", "SORT1", "SORT2" };
                    for (int i = dt.Columns.Count - 1; i >= 0; i--)
                    {
                        DataColumn dc = dt.Columns[i];
                        if (listtoRemove.Contains(dc.ColumnName.ToUpper()))
                        {
                            dt.Columns.Remove(dc);
                        }
                    }

                    DataTable dtDstnct = new DataTable();
                    dtDstnct = dt.Clone();
                    dtDstnct = dt.DefaultView.ToTable(true);

                    // For locking flag
                    dtDstnct.Columns.AddRange(new DataColumn[2] {
                        new DataColumn("IsLocked", typeof(bool)),
                        new DataColumn("LockedBy", typeof(string))
                    });

                    if (dtDstnct != null && dtDstnct.Rows.Count > 0)
                    {
                        var eventSiteContent = new int[] { 1, 2, 3, 4, 5, 9, 10 };
                        if (eventSiteContent.Any(i => i == siteCntnt))
                        {
                            var lockedEvents = _context.EventRecLock
                                                    .Include(i => i.LockByUser)
                                                    .AsNoTracking()
                                                    .ToList();

                            for (int i = 0; i < dtDstnct.Rows.Count; i++)
                            {
                                DataRow row = dtDstnct.Rows[i];
                                if (lockedEvents.Any(a => a.EventId == Convert.ToInt32(row["eventID"].ToString())))
                                {
                                    row["IsLocked"] = true;
                                    row["LockedBy"] = lockedEvents
                                        .FirstOrDefault(a => a.EventId == Convert.ToInt32(row["eventID"].ToString()))
                                        .LockByUser.FullNme;
                                }
                            }
                        }
                        else if (siteCntnt == 18)
                        {
                            // For Redesign
                            var lockedRedesign = _context.RedsgnRecLock.AsNoTracking()
                                                    .ToList();

                            for (int i = 0; i < dtDstnct.Rows.Count; i++)
                            {
                                DataRow row = dtDstnct.Rows[i];
                                if (lockedRedesign.Any(a => a.RedsgnId == Convert.ToInt32(row["Redesign ID"].ToString())))
                                {
                                    var lockByUserId = lockedRedesign
                                        .FirstOrDefault(a => a.RedsgnId == Convert.ToInt32(row["Redesign ID"].ToString()))
                                        .LockByUserId;
                                    row["IsLocked"] = true;
                                    row["LockedBy"] = _context.LkUser
                                        .FirstOrDefault(a => a.UserId == lockByUserId).FullNme;
                                }
                            }
                        }
                    }

                    return dtDstnct;
                }
            }
        }

        public DataTable GetCalendarViewDetails(int viewID, int eID, DateTime? date, string type, string user, string group)
        {
            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.getCalendarViewDetails", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@DSPL_VW_ID", viewID);
            command.Parameters.AddWithValue("@APPT_TYPE_ID", eID);
            if (date != null)
                command.Parameters.AddWithValue("@SelDate", date);
            if (type != null)
                command.Parameters.AddWithValue("@SelType", type);
            if (user != null)
                command.Parameters.AddWithValue("@SelUser", user);
            if (group != null)
                command.Parameters.AddWithValue("@SelGroup", group);

            command.Parameters.AddWithValue("@CSGLvlId", 1);

            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds.Tables[0];
        }

        public IEnumerable<GetEventViews> GetEventViews(int userID, int siteCntnt)
        {
            int grpId = 0;
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@USERID", userID),
                new SqlParameter("@SITE_CNTNT_ID", siteCntnt),
                new SqlParameter("@VW_TYP", 1),
                new SqlParameter("@GRP_ID", grpId),
                new SqlParameter("@IsNewUI", 1)
            };

            var eventViews = _context.Query<GetEventViews>()
                .AsNoTracking()
                .FromSql("web.getViews @USERID, @SITE_CNTNT_ID, @VW_TYP, @GRP_ID, @IsNewUI", pc.ToArray()).ToList();

            return eventViews;
        }

        public IEnumerable<GetEventViewColumns> GetEventViewColumns(int grpId, int siteCntnt)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {                
                 new SqlParameter() {ParameterName = "@SITE_CNTNT_ID", SqlDbType = SqlDbType.Int, Value = siteCntnt},
                 new SqlParameter() {ParameterName = "@GRP_ID", SqlDbType = SqlDbType.Int, Value = grpId}
            };

            var eventViewColumns = _context.Query<GetEventViewColumns>()
                .AsNoTracking()
                .FromSql("web.getViewColumns @SITE_CNTNT_ID,@GRP_ID", pc.ToArray()).ToList();

            return eventViewColumns;
        }

        public DataSet GetEventViewInfo(int viewId)
        {
            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            command = new SqlCommand("web.getView", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ViewID", viewId); // if you have parameters.
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;
        }

        public bool DeleteDsplViewColsByViewId(int viewId)
        {
            try
            {
                DeleteDsplColByViewId(viewId);
                DeleteDsplViewById(viewId);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IEnumerable<DsplVwCol> GetDsplColByViewId(int viewId)
        {
            return _context.DsplVwCol.Where(a => a.DsplVwId == viewId).AsNoTracking().ToList();
        }

        public void DeleteDsplColByViewId(int viewId)
        {
            IEnumerable<DsplVwCol> dsplCols = GetDsplColByViewId(viewId);
            foreach (var col in dsplCols)
            {
                _context.DsplVwCol.Remove(col);
            }
            SaveAll();
        }

        public void DeleteDsplViewById(int viewId)
        {
            DsplVw dsplVw = _context.DsplVw.SingleOrDefault(a => a.DsplVwId == viewId);
            _context.DsplVw.Remove(dsplVw);
            SaveAll();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public IEnumerable<LkFiltrOpr> GetFilterOperators()
        {
            return _context.LkFiltrOpr.AsNoTracking().ToList();
        }

        public bool AddEventView(int _iUserID, string _sViewName, int _idSiteCntnt, string _cDfltFlag, string _cPblFlag, string _sFltColOpr1, string _sFltColOpr2, int _iFltCol1, int _iFltCol2, string _sFltColTxt1Val,
           string _sFltColTxt2Val, string _cFltFlag, int _iSortCol1, string _sSortCol1Flag, int _iSortCol2, string _sSortCol2Flag, string _sFltLogicOpr, string _sColIDs, string _sPos)
        {
            try
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() {ParameterName = "@USERID", SqlDbType = SqlDbType.Int, Value = _iUserID},
                    new SqlParameter() {ParameterName = "@VIEWNAME", SqlDbType = SqlDbType.VarChar, Value = _sViewName},
                    new SqlParameter() {ParameterName = "@SITE_CNTNT_ID", SqlDbType = SqlDbType.Int, Value = _idSiteCntnt},
                    new SqlParameter() {ParameterName = "@DFLTFLAG", SqlDbType = SqlDbType.VarChar, Value = _cDfltFlag},
                    new SqlParameter() {ParameterName = "@PBLCFLAG", SqlDbType = SqlDbType.VarChar, Value = _cPblFlag},
                    new SqlParameter() {ParameterName = "@FLTRCOLOPR1", SqlDbType = SqlDbType.VarChar, Value = _sFltColOpr1},
                    new SqlParameter() {ParameterName = "@FLTRCOLOPR2", SqlDbType = SqlDbType.VarChar, Value = _sFltColOpr2},
                    new SqlParameter() {ParameterName = "@FLTRCOL1", SqlDbType = SqlDbType.Int, Value = _iFltCol1},
                    new SqlParameter() {ParameterName = "@FLTRCOL2", SqlDbType = SqlDbType.Int, Value = _iFltCol2},
                    new SqlParameter() {ParameterName = "@FLTRCOLTXT1VAL", SqlDbType = SqlDbType.VarChar, Value = _sFltColTxt1Val},
                    new SqlParameter() {ParameterName = "@FLTRCOLTXT2VAL", SqlDbType = SqlDbType.VarChar, Value = _sFltColTxt2Val},
                    new SqlParameter() {ParameterName = "@FLTRFLAG", SqlDbType = SqlDbType.VarChar, Value = _cFltFlag},
                    new SqlParameter() {ParameterName = "@SORTBYCOL1", SqlDbType = SqlDbType.Int, Value = _iSortCol1},
                    new SqlParameter() {ParameterName = "@SORTBYCOL1ORDER", SqlDbType = SqlDbType.VarChar, Value = _sSortCol1Flag},
                    new SqlParameter() {ParameterName = "@SORTBYCOL2", SqlDbType = SqlDbType.Int, Value = _iSortCol2},
                    new SqlParameter() {ParameterName = "@SORTBYCOL2ORDER", SqlDbType = SqlDbType.VarChar, Value = _sSortCol2Flag},
                    new SqlParameter() {ParameterName = "@LOGICOPR", SqlDbType = SqlDbType.VarChar, Value = _sFltLogicOpr},
                    new SqlParameter() {ParameterName = "@sCOLIDS", SqlDbType = SqlDbType.VarChar, Value = _sColIDs},
                    new SqlParameter() {ParameterName = "@sPOS", SqlDbType = SqlDbType.VarChar, Value = _sPos},
                };

                var addEvents = _context.Database.ExecuteSqlCommand("web.insertView @USERID,@VIEWNAME, @SITE_CNTNT_ID,@DFLTFLAG, @PBLCFLAG,@FLTRCOLOPR1, @FLTRCOLOPR2,@FLTRCOL1, @FLTRCOL2,@FLTRCOLTXT1VAL,@FLTRCOLTXT2VAL," +
                    "@FLTRFLAG, @SORTBYCOL1,@SORTBYCOL1ORDER, @SORTBYCOL2,@SORTBYCOL2ORDER, @LOGICOPR,@sCOLIDS, @sPOS", pc.ToArray());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateEventView(int _iViewID, int _iUserID, string _sViewName, int _idSiteCntnt, string _cDfltFlag, string _cPblFlag, string _sFltColOpr1, string _sFltColOpr2, int _iFltCol1, int _iFltCol2, string _sFltColTxt1Val,
            string _sFltColTxt2Val, string _cFltFlag, int _iSortCol1, string _sSortCol1Flag, int _iSortCol2, string _sSortCol2Flag, string _sFltLogicOpr, string _sColIDs, string _sPos)
        {
            try
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() {ParameterName = "@VIEWID", SqlDbType = SqlDbType.Int, Value = _iViewID},
                    new SqlParameter() {ParameterName = "@USERID", SqlDbType = SqlDbType.Int, Value = _iUserID},
                    new SqlParameter() {ParameterName = "@VIEWNAME", SqlDbType = SqlDbType.VarChar, Value = _sViewName},
                    new SqlParameter() {ParameterName = "@SITE_CNTNT_ID", SqlDbType = SqlDbType.Int, Value = _idSiteCntnt},
                    new SqlParameter() {ParameterName = "@DFLTFLAG", SqlDbType = SqlDbType.VarChar, Value = _cDfltFlag},
                    new SqlParameter() {ParameterName = "@PBLCFLAG", SqlDbType = SqlDbType.VarChar, Value = _cPblFlag},
                    new SqlParameter() {ParameterName = "@FLTRCOLOPR1", SqlDbType = SqlDbType.VarChar, Value = _sFltColOpr1},
                    new SqlParameter() {ParameterName = "@FLTRCOLOPR2", SqlDbType = SqlDbType.VarChar, Value = _sFltColOpr2},
                    new SqlParameter() {ParameterName = "@FLTRCOL1", SqlDbType = SqlDbType.Int, Value = _iFltCol1},
                    new SqlParameter() {ParameterName = "@FLTRCOL2", SqlDbType = SqlDbType.Int, Value = _iFltCol2},
                    new SqlParameter() {ParameterName = "@FLTRCOLTXT1VAL", SqlDbType = SqlDbType.VarChar, Value = _sFltColTxt1Val},
                    new SqlParameter() {ParameterName = "@FLTRCOLTXT2VAL", SqlDbType = SqlDbType.VarChar, Value = _sFltColTxt2Val},
                    new SqlParameter() {ParameterName = "@FLTRFLAG", SqlDbType = SqlDbType.VarChar, Value = _cFltFlag},
                    new SqlParameter() {ParameterName = "@SORTBYCOL1", SqlDbType = SqlDbType.Int, Value = _iSortCol1},
                    new SqlParameter() {ParameterName = "@SORTBYCOL1ORDER", SqlDbType = SqlDbType.VarChar, Value = _sSortCol1Flag},
                    new SqlParameter() {ParameterName = "@SORTBYCOL2", SqlDbType = SqlDbType.Int, Value = _iSortCol2},
                    new SqlParameter() {ParameterName = "@SORTBYCOL2ORDER", SqlDbType = SqlDbType.VarChar, Value = _sSortCol2Flag},
                    new SqlParameter() {ParameterName = "@LOGICOPR", SqlDbType = SqlDbType.VarChar, Value = _sFltLogicOpr},
                    new SqlParameter() {ParameterName = "@sCOLIDS", SqlDbType = SqlDbType.VarChar, Value = _sColIDs},
                    new SqlParameter() {ParameterName = "@sPOS", SqlDbType = SqlDbType.VarChar, Value = _sPos},
                };

                var updateEvents = _context.Database.ExecuteSqlCommand("web.updateView @VIEWID, @USERID,@VIEWNAME, @SITE_CNTNT_ID,@DFLTFLAG, @PBLCFLAG,@FLTRCOLOPR1, @FLTRCOLOPR2,@FLTRCOL1, @FLTRCOL2,@FLTRCOLTXT1VAL,@FLTRCOLTXT2VAL," +
                    "@FLTRFLAG, @SORTBYCOL1,@SORTBYCOL1ORDER, @SORTBYCOL2,@SORTBYCOL2ORDER, @LOGICOPR,@sCOLIDS, @sPOS", pc.ToArray());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}