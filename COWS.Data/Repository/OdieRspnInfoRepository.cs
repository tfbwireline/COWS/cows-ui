﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class OdieRspnInfoRepository : IOdieRspnInfoRepository
    {
        private readonly COWSAdminDBContext _context;

        public OdieRspnInfoRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public OdieRspnInfo Create(OdieRspnInfo entity)
        {
            _context.OdieRspnInfo.Add(entity);

            SaveAll();

            return GetById(entity.ReqId);
        }

        public void Delete(int id)
        {
            OdieRspnInfo entity = GetById(id);
            _context.OdieRspnInfo.Remove(entity);

            SaveAll();
        }

        public IQueryable<OdieRspnInfo> Find(Expression<Func<OdieRspnInfo, bool>> predicate)
        {
            return _context.OdieRspnInfo
                            .Include(a => a.Req)
                            .AsNoTracking()
                            .Where(predicate);
        }

        public IEnumerable<OdieRspnInfo> GetAll()
        {
            return _context.OdieRspnInfo
                            .AsNoTracking()
                            .ToList();
        }

        public OdieRspnInfo GetById(int id)
        {
            return _context.OdieRspnInfo
                            .SingleOrDefault(i => i.RspnInfoId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, OdieRspnInfo entity)
        {
            OdieRspnInfo obj = GetById(id);

            SaveAll();
        }

        public List<RedsgnDevicesInfo> GetRedesignDevicesInfo(int _iReqID)
        {
            List<RedsgnDevicesInfo> lstRDI = new List<RedsgnDevicesInfo>();

            var odrespnme = (from reqNme in _context.OdieReq
                             join respnNme in _context.OdieRspnInfo on reqNme.ReqId equals respnNme.ReqId
                                  into custInfoResp
                             from respnNme in custInfoResp.DefaultIfEmpty()
                             orderby respnNme.DevId
                             where reqNme.ReqId == _iReqID
                             select new
                             {
                                 ODIE_DEV_NME = (respnNme.DevId == null) ? " " : respnNme.DevId.ToString(),
                                 //Fast_Track_Flag = (respnNme.FAST_TRK_CD == null) ? "No" : (respnNme.FAST_TRK_CD == true) ? "Yes" : "No",
                                 //RSPN_INFO_ID = (respnNme.RSPN_INFO_ID == null) ? 0 : respnNme.RSPN_INFO_ID,
                                 STUS_ID = reqNme.StusId
                             }).Distinct();

            if (odrespnme != null && odrespnme.Count() > 0)
            {
                int Ctr = 1;
                foreach (var _or in odrespnme)
                {
                    if (_or.STUS_ID == 21)
                    {
                        RedsgnDevicesInfo rdi = new RedsgnDevicesInfo();
                        rdi.DevNme = _or.ODIE_DEV_NME;
                        rdi.RedsgnDevId = Ctr++;
                        lstRDI.Add(rdi);
                    }
                }
            }

            return lstRDI;
        }
    }
}
