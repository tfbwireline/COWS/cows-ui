﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptPlanServiceTierRepository : ICptPlanServiceTierRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptPlanServiceTierRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptPlnSrvcTier Create(LkCptPlnSrvcTier entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptPlnSrvcTier> Find(Expression<Func<LkCptPlnSrvcTier, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPlnSrvcTier, out List<LkCptPlnSrvcTier> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptPlnSrvcTier> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPlnSrvcTier, out List<LkCptPlnSrvcTier> list))
            {
                list = _context.LkCptPlnSrvcTier
                            .OrderBy(i => i.CptPlnSrvcTier)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptPlnSrvcTier, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptPlnSrvcTier GetById(int id)
        {
            return _context.LkCptPlnSrvcTier
                            .SingleOrDefault(i => i.CptPlnSrvcTierId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptPlnSrvcTier);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptPlnSrvcTier entity)
        {
            throw new NotImplementedException();
        }
    }
}