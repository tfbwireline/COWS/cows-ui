﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventTypeRepository : IEventTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public EventTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkEventType Create(LkEventType entity)
        {
            int maxId = _context.LkEventType.Max(i => i.EventTypeId);
            entity.EventTypeId = (short)++maxId;

            // Updated by Sarah Sandoval [20200611]
            // This will not be applicable due to removal of CAND Events
            // Sarah Sandoval [20190915] - Added this condition which is the same as the old COWS
            // Delete on the old COWS remove the actual record on DB
            // Create and Update maintains RECSTUSID = 0
            //LkEventType type = GetById(entity.EventTypeId);
            //type.RecStusId = (byte)ERecStatus.InActive;
            SaveAll();

            return GetById(entity.EventTypeId);
        }

        public void Delete(int id)
        {
            LkEventType type = GetById(id);
            _context.LkEventType.Remove(type);

            SaveAll();
        }

        public IQueryable<LkEventType> Find(Expression<Func<LkEventType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkEventType, out List<LkEventType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkEventType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkEventType, out List<LkEventType> list))
            {
                list = _context.LkEventType
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderBy(i => i.EventTypeNme)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkEventType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkEventType GetById(int id)
        {
            return _context.LkEventType
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.EventTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkEventType);
            return _context.SaveChanges();
        }

        public void Update(int id, LkEventType entity)
        {
            LkEventType type = GetById(id);
            type.EventTypeNme = entity.EventTypeNme;
            type.ModfdByUserId = entity.ModfdByUserId;
            type.ModfdDt = entity.ModfdDt;
            // Updated by Sarah Sandoval [20200611]
            // This will not be applicable due to removal of CAND Events
            // Sarah Sandoval [20190915] - Added this condition which is the same as the old COWS
            // Delete on the old COWS remove the actual record on DB
            // Create and Update maintains RECSTUSID = 0
            //type.RecStusId = (byte)ERecStatus.InActive;
            type.RecStusId = entity.RecStusId;

            SaveAll();
        }
    }
}
