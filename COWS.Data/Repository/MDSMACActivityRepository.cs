﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSMACActivityRepository : IMDSMACActivityRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MDSMACActivityRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMdsMacActy Create(LkMdsMacActy entity)
        {
            int maxId = _context.LkMdsMacActy.Max(i => i.MdsMacActyId);
            entity.MdsMacActyId = (short)++maxId;
            _context.LkMdsMacActy.Add(entity);

            SaveAll();

            return GetById(entity.MdsMacActyId);
        }

        public void Delete(int id)
        {
            LkMdsMacActy activity = GetById(id);
            _context.LkMdsMacActy.Remove(activity);

            SaveAll();
        }

        public IQueryable<LkMdsMacActy> Find(Expression<Func<LkMdsMacActy, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsMacActy, out List<LkMdsMacActy> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMdsMacActy> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsMacActy, out List<LkMdsMacActy> list))
            {
                list = _context.LkMdsMacActy
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                            .AsNoTracking()
                            .ToList();

                _cache.Set(CacheKeys.LkMdsMacActy, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMdsMacActy GetById(int id)
        {
            return _context.LkMdsMacActy
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .AsNoTracking()
                            .SingleOrDefault(i => i.MdsMacActyId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMdsMacActy);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMdsMacActy entity)
        {
            LkMdsMacActy activity = GetById(id);
            activity.MdsMacActyNme = entity.MdsMacActyNme;
            activity.MinDrtnTmeReqrAmt = entity.MinDrtnTmeReqrAmt;
            activity.RecStusId = entity.RecStusId;
            activity.ModfdByUserId = entity.ModfdByUserId;
            activity.ModfdDt = entity.ModfdDt;

            _context.LkMdsMacActy.Update(activity);
            SaveAll();
        }
    }
}