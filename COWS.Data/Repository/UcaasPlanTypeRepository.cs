﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UcaasPlanTypeRepository : IUcaasPlanTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public UcaasPlanTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkUcaaSPlanType Create(LkUcaaSPlanType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkUcaaSPlanType> Find(Expression<Func<LkUcaaSPlanType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkUcaaSPlanType, out List<LkUcaaSPlanType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkUcaaSPlanType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUcaaSPlanType, out List<LkUcaaSPlanType> list))
            {
                list = _context.LkUcaaSPlanType
                            .OrderBy(i => i.UcaaSPlanType)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkUcaaSPlanType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkUcaaSPlanType GetById(int id)
        {
            return _context.LkUcaaSPlanType
                            .SingleOrDefault(i => i.UcaaSPlanTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkUcaaSPlanType);
            throw new NotImplementedException();
        }

        public void Update(int id, LkUcaaSPlanType entity)
        {
            throw new NotImplementedException();
        }
    }
}