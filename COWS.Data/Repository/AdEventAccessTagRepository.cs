﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class AdEventAccessTagRepository : IAdEventAccessTagRepository
    {
        private readonly COWSAdminDBContext _context;

        public AdEventAccessTagRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<AdEventAccsTag> Find(Expression<Func<AdEventAccsTag, bool>> predicate)
        {
            return _context.AdEventAccsTag
                            .Where(predicate);
        }

        public IEnumerable<AdEventAccsTag> GetAll()
        {
            return _context.AdEventAccsTag
                .ToList();
        }

        public AdEventAccsTag GetById(int id)
        {
            //return _context.AdEventAccsTag
            //    .SingleOrDefault(a => a. == id);
            throw new NotImplementedException();
        }

        public AdEventAccsTag GetById(int eventId, string cktId)
        {
            return _context.AdEventAccsTag
                .SingleOrDefault(a => a.EventId == eventId && a.CktId == cktId);
        }

        public AdEventAccsTag Create(AdEventAccsTag entity)
        {
            // No Id key; 1:M relatopnship with AdEvent; Composite key of EventId and CktId
            throw new NotImplementedException();
        }

        public void Update(int id, AdEventAccsTag entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}