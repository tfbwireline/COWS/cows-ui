﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderTypeRepository : IOrderTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public OrderTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkOrdrType> Find(Expression<Func<LkOrdrType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkOrdrType, out List<LkOrdrType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkOrdrType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkOrdrType, out List<LkOrdrType> list))
            {
                list = _context.LkOrdrType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkOrdrType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkOrdrType GetById(int id)
        {
            return _context.LkOrdrType
                .SingleOrDefault(a => a.OrdrTypeId == id);
        }

        public LkOrdrType Create(LkOrdrType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkOrdrType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkOrdrType);
            return _context.SaveChanges();
        }
    }
}