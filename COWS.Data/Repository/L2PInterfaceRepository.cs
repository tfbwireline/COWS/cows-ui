﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace COWS.Data.Repository
{
    public class L2PInterfaceRepository : IL2PInterfaceRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _config;

        public L2PInterfaceRepository(COWSAdminDBContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        public IEnumerable<RetrieveCustomerByH1> RetrieveCustomerByH1(string h1)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@CIS_ID", h1)
            };

            var h1List = _context.Query<RetrieveCustomerByH1>()
                .AsNoTracking()
                .FromSql("web.retrieveCustomerByH1 @CIS_ID", pc.ToArray()).ToList();

            return h1List;
        }

        public IEnumerable<byte> GetM5CSGLvl(string h1, string h6, string m5OrdrNbr)
        {
            List<byte> csgLevel = new List<byte>();

            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.getM5SecrdValidation", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@H1", h1);
            command.Parameters.AddWithValue("@H6", h6);
            command.Parameters.AddWithValue("@M5OrdrNbr", m5OrdrNbr);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    csgLevel.Add(byte.Parse(row["CSG_LVL"].ToString()));
                }
            }

            return csgLevel;
        }
    }
}