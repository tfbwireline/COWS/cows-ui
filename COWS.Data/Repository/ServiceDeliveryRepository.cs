﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class ServiceDeliveryRepository : IServiceDeliveryRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public ServiceDeliveryRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkSprintCpeNcr Create(LkSprintCpeNcr entity)
        {
            int maxId = _context.LkSprintCpeNcr.Max(i => i.SprintCpeNcrId);
            entity.SprintCpeNcrId = (byte)++maxId;
            _context.LkSprintCpeNcr.Add(entity);

            SaveAll();

            return GetById(entity.SprintCpeNcrId);
        }

        public void Delete(int id)
        {
            LkSprintCpeNcr site = GetById(id);
            _context.LkSprintCpeNcr.Remove(site);

            SaveAll();
        }

        public IQueryable<LkSprintCpeNcr> Find(Expression<Func<LkSprintCpeNcr, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkSprintCpeNcr, out List<LkSprintCpeNcr> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkSprintCpeNcr> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkSprintCpeNcr, out List<LkSprintCpeNcr> list))
            {
                list = _context.LkSprintCpeNcr
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkSprintCpeNcr, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkSprintCpeNcr GetById(int id)
        {
            return _context.LkSprintCpeNcr
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.SprintCpeNcrId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkSprintCpeNcr);
            return _context.SaveChanges();
        }

        public void Update(int id, LkSprintCpeNcr entity)
        {
            LkSprintCpeNcr site = GetById(id);
            site.SprintCpeNcrDes = entity.SprintCpeNcrDes;
            site.RecStusId = entity.RecStusId;
            site.ModfdByUserId = entity.ModfdByUserId;
            site.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}