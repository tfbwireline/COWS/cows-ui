using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class UcaasEventRepository : IUcaasEventRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;
        private readonly ICommonRepository _common;
        private readonly ISearchRepository _searchRepo;
        private readonly IEventCpeDevRepository _eventCpeDevRepo;
        private readonly IEventDeviceCompletionRepository _eventDeviceCompletionRepo;
        private readonly IEventDevSrvcMgmtRepository _eventDevSrvcMgmtRepo;
        private readonly IEventDiscoDevRepository _eventDiscoDevRepo;
        private readonly IUcaasEventBillingRepository _ucaasEventBillingRepo;
        private readonly IUcaasEventOdieDeviceRepository _ucaasEventOdieDevRepo;

        public UcaasEventRepository(COWSAdminDBContext context, IConfiguration configuration,
            ICommonRepository common, ISearchRepository searchRepo,
            IEventCpeDevRepository eventCpeDevRepo, IEventDevSrvcMgmtRepository eventDevSrvcMgmtRepo,
            IEventDeviceCompletionRepository eventDeviceCompletionRepo,
            IEventDiscoDevRepository eventDiscoDevRepo,
            IUcaasEventBillingRepository ucaasEventBillingRepo,
            IUcaasEventOdieDeviceRepository ucaasEventOdieDevRepo)
        {
            _context = context;
            _configuration = configuration;
            _common = common;
            _searchRepo = searchRepo;
            _eventCpeDevRepo = eventCpeDevRepo;
            _eventDevSrvcMgmtRepo = eventDevSrvcMgmtRepo;
            _eventDeviceCompletionRepo = eventDeviceCompletionRepo;
            _eventDiscoDevRepo = eventDiscoDevRepo;
            _ucaasEventBillingRepo = ucaasEventBillingRepo;
            _ucaasEventOdieDevRepo = ucaasEventOdieDevRepo;
        }

        public UcaaSEvent Create(UcaaSEvent entity)
        {
            _context.Event.Add(entity.Event);
            SaveAll();

            // Build Event Title
            if (entity.WrkflwStusId.Equals((byte)WorkflowStatus.Visible)
                || entity.WrkflwStusId.Equals((byte)WorkflowStatus.Submit))
            {
                var ucaasProdType = _context.LkUcaaSProdType.SingleOrDefault(i => i.UcaaSProdTypeId == entity.UcaaSProdTypeId);
                if (entity.UcaaSActyTypeId.Equals((short)UcaasActivityType.Disconnect))
                {
                    //if (entity.Event.CsgLvlId > 0)
                    //    entity.EventTitleTxt = BuildTitle(entity.EventTitleTxt,
                    //        CustomerMaskedData.customerName, "", "", "", ucaasProdType.UcaaSProdType);
                    //else
                        entity.EventTitleTxt = BuildTitle(entity.EventTitleTxt,
                            entity.CustNme, "", "", "", ucaasProdType.UcaaSProdType);
                }
                else
                {
                    //if (entity.Event.CsgLvlId > 0)
                    //    entity.EventTitleTxt = BuildTitle(entity.EventTitleTxt,
                    //        CustomerMaskedData.customerName, CustomerMaskedData.city,
                    //        CustomerMaskedData.state, CustomerMaskedData.country,
                    //        ucaasProdType.UcaaSProdType);
                    //else
                        entity.EventTitleTxt = BuildTitle(entity.EventTitleTxt, entity.CustNme,
                            string.IsNullOrWhiteSpace(entity.CtyNme) ? string.Empty : entity.CtyNme,
                            string.IsNullOrWhiteSpace(entity.SttPrvnNme) ? string.Empty : entity.SttPrvnNme,
                            string.IsNullOrWhiteSpace(entity.CtryRgnNme) ? string.Empty : entity.CtryRgnNme,
                            ucaasProdType.UcaaSProdType);
                }
            }

            // UsIntlCd
            var ctry = string.IsNullOrWhiteSpace(entity.CtryRgnNme) ? string.Empty : entity.CtryRgnNme.Trim().ToUpper();
            if (ctry == "US" || ctry == "U.S" || ctry == "USA" || ctry == "U.S.A"
                || ctry == "" || ctry == "UNITED STATE OF AMERICAN"
                || ctry == "UNITED STATES" || ctry == "UNITED STATES OF AMERICA")
            {
                entity.UsIntlCd = "D";
                entity.InstlSitePocIntlPhnCd = string.Empty;
                entity.InstlSitePocIntlCellPhnCd = string.Empty;
            }
            else
                entity.UsIntlCd = "I";

            entity.EventId = entity.Event.EventId;
            _context.UcaaSEvent.Add(entity);
            SaveAll();

            // Handle Secured Data
            if (entity.Event.CsgLvlId > 0)
            {
                CustScrdData sdata = new CustScrdData();
                sdata.ScrdObjId = entity.EventId;
                sdata.ScrdObjTypeId = (byte)ScrdObjType.UCaaS_EVENT;

                if (!entity.UcaaSActyTypeId.Equals((short)UcaasActivityType.Disconnect))
                {
                    sdata.CustCntctNme = _common.GetEncryptValue(entity.InstlSitePocNme ?? "");
                    sdata.CustCntctPhnNbr = _common.GetEncryptValue(entity.InstlSitePocPhnNbr ?? "");
                    sdata.CustCntctCellPhnNbr = _common.GetEncryptValue(entity.InstlSitePocCellPhnNbr ?? "");
                    sdata.SrvcAssrnPocNme = _common.GetEncryptValue(entity.SrvcAssrnPocNme ?? "");
                    sdata.SrvcAssrnPocPhnNbr = _common.GetEncryptValue(entity.SrvcAssrnPocPhnNbr ?? "");
                    sdata.SrvcAssrnPocCellPhnNbr = _common.GetEncryptValue(entity.SrvcAssrnPocCellPhnNbr ?? "");
                    sdata.StreetAdr1 = _common.GetEncryptValue(entity.StreetAdr ?? "");
                    sdata.FlrId = _common.GetEncryptValue(entity.FlrBldgNme ?? "");
                    sdata.CtyNme = _common.GetEncryptValue(entity.CtyNme ?? "");
                    sdata.SttPrvnNme = _common.GetEncryptValue(entity.SttPrvnNme ?? "");
                    sdata.CtryRgnNme = _common.GetEncryptValue(entity.CtryRgnNme ?? "");
                    sdata.ZipPstlCd = _common.GetEncryptValue(entity.ZipCd ?? "");
                }

                sdata.CustNme = _common.GetEncryptValue(entity.CustNme ?? "");
                sdata.EventTitleTxt = _common.GetEncryptValue(entity.EventTitleTxt ?? "");
                sdata.CustEmailAdr = _common.GetEncryptValue(entity.CustAcctTeamPdlNme ?? "");
                _context.CustScrdData.Add(sdata);

                if (!entity.UcaaSActyTypeId.Equals((short)UcaasActivityType.Disconnect))
                {
                    entity.InstlSitePocNme = string.Empty;
                    entity.InstlSitePocPhnNbr = string.Empty;
                    entity.InstlSitePocCellPhnNbr = string.Empty;
                    entity.SrvcAssrnPocNme = string.Empty;
                    entity.SrvcAssrnPocPhnNbr = string.Empty;
                    entity.SrvcAssrnPocCellPhnNbr = string.Empty;
                    entity.StreetAdr = string.Empty;
                    entity.FlrBldgNme = string.Empty;
                    entity.CtyNme = string.Empty;
                    entity.SttPrvnNme = string.Empty;
                    entity.CtryRgnNme = string.Empty;
                    entity.ZipCd = string.Empty;
                }

                entity.CustNme = string.Empty;
                entity.EventTitleTxt = string.Empty;
                entity.CustAcctTeamPdlNme = string.Empty;
                _context.UcaaSEvent.Update(entity);

                SaveAll();
            }

            return GetById(entity.EventId);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<UcaaSEvent> Find(Expression<Func<UcaaSEvent, bool>> predicate, string adid = null)
        {
            var ucaas = _context.UcaaSEvent
                            .Include(i => i.EventStus)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.UcaaSEventBilling)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.UcaaSEventOdieDev)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.EventCpeDev)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.EventDevCmplt)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.EventDevSrvcMgmt)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.EventDiscoDev)
                            .Include(i => i.EsclReas)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.ReqorUser)
                            .Include(i => i.WrkflwStus)
                            .Where(predicate);

            return ucaas;
        }

        public IEnumerable<UcaaSEvent> GetAll()
        {
            return _context.UcaaSEvent
                            .ToList();
        }

        public UcaaSEvent GetById(int id)
        {
            var ucaas = _context.UcaaSEvent
                            .Include(i => i.EventStus)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.UcaaSEventBilling)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.UcaaSEventOdieDev)
                            //.Include(i => i.Event).ThenInclude(evnt => evnt.EventCpeDev)
                            //.Include(i => i.Event).ThenInclude(evnt => evnt.EventDevCmplt)
                            //.Include(i => i.Event).ThenInclude(evnt => evnt.EventDevSrvcMgmt)
                            //.Include(i => i.Event).ThenInclude(evnt => evnt.EventDiscoDev)
                            .Include(i => i.EsclReas)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.ReqorUser)
                            .Include(i => i.WrkflwStus)
                            .SingleOrDefault(i => i.EventId == id);

            ucaas.Event.EventCpeDev = _context.EventCpeDev.Where(i => i.EventId == id
                && i.RecStusId == (byte)ERecStatus.Active).ToList();
            ucaas.Event.EventDevCmplt = _context.EventDevCmplt.Where(i => i.EventId == id
                && i.RecStusId == (byte)ERecStatus.Active).ToList();
            ucaas.Event.EventDevSrvcMgmt = _context.EventDevSrvcMgmt.Where(i => i.EventId == id
                && i.RecStusId == (byte)ERecStatus.Active).ToList();
            ucaas.Event.EventDiscoDev = _context.EventDiscoDev.Where(i => i.EventId == id
                && i.RecStusId == (byte)ERecStatus.Active).ToList();

            return ucaas;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, UcaaSEvent entity)
        {
            UcaaSEvent ucaas = GetById(id);
            CustScrdData sdata = _context.CustScrdData
                .SingleOrDefault(i => i.ScrdObjId == id 
                    && i.ScrdObjTypeId == (byte)SecuredObjectType.UCaaS_EVENT);

            ucaas.OldEventStusId = ucaas.EventStusId;
            ucaas.EventStusId = entity.EventStusId;
            //ucaas.EventTitleTxt = entity.EventTitleTxt;
            ucaas.H1 = entity.H1;
            ucaas.CustNme = entity.CustNme;
            ucaas.CharsId = entity.CharsId;
            ucaas.CustAcctTeamPdlNme = entity.CustAcctTeamPdlNme;
            ucaas.CustSowLocTxt = entity.CustSowLocTxt;
            ucaas.UcaaSProdTypeId = entity.UcaaSProdTypeId;
            ucaas.UcaaSPlanTypeId = entity.UcaaSPlanTypeId;
            ucaas.UcaaSActyTypeId = entity.UcaaSActyTypeId;
            ucaas.UcaaSDesgnDoc = entity.UcaaSDesgnDoc;
            ucaas.ShrtDes = entity.ShrtDes;

            ucaas.H6 = entity.H6;
            ucaas.Ccd = entity.Ccd;
            ucaas.SiteId = entity.SiteId;
            //ucaas.SiteAdr = entity.SiteAdr;

            // Build Event Title
            if (entity.WrkflwStusId.Equals((byte)WorkflowStatus.Visible)
                || entity.WrkflwStusId.Equals((byte)WorkflowStatus.Submit))
            {
                var ucaasProdType = _context.LkUcaaSProdType.SingleOrDefault(i => i.UcaaSProdTypeId == entity.UcaaSProdTypeId);
                if (entity.UcaaSActyTypeId.Equals((short)UcaasActivityType.Disconnect))
                {
                    //if (entity.Event.CsgLvlId > 0)
                    //    entity.EventTitleTxt = BuildTitle(entity.EventTitleTxt,
                    //        CustomerMaskedData.customerName, "", "", "", ucaasProdType.UcaaSProdType);
                    //else
                        entity.EventTitleTxt = BuildTitle(entity.EventTitleTxt,
                            entity.CustNme, "", "", "", ucaasProdType.UcaaSProdType);
                }
                else
                {
                    //if (entity.Event.CsgLvlId > 0)
                    //    entity.EventTitleTxt = BuildTitle(entity.EventTitleTxt,
                    //        CustomerMaskedData.customerName, CustomerMaskedData.city,
                    //        CustomerMaskedData.state, CustomerMaskedData.country,
                    //        ucaasProdType.UcaaSProdType);
                    //else
                        entity.EventTitleTxt = BuildTitle(entity.EventTitleTxt, entity.CustNme,
                            string.IsNullOrWhiteSpace(entity.CtyNme) ? string.Empty : entity.CtyNme,
                            string.IsNullOrWhiteSpace(entity.SttPrvnNme) ? string.Empty : entity.SttPrvnNme,
                            string.IsNullOrWhiteSpace(entity.CtryRgnNme) ? string.Empty : entity.CtryRgnNme,
                            ucaasProdType.UcaaSProdType);
                }
            }

            // UsIntlCd
            var ctry = string.IsNullOrWhiteSpace(entity.CtryRgnNme) ? string.Empty : entity.CtryRgnNme.Trim().ToUpper();
            if (ctry == "US" || ctry == "U.S" || ctry == "USA" || ctry == "U.S.A"
                || ctry == "" || ctry == "UNITED STATE OF AMERICAN"
                || ctry == "UNITED STATES" || ctry == "UNITED STATES OF AMERICA")
            {
                ucaas.UsIntlCd = "D";
                ucaas.InstlSitePocIntlPhnCd = string.Empty;
                ucaas.InstlSitePocIntlCellPhnCd = string.Empty;
            }
            else
                ucaas.UsIntlCd = "I";

            // Handle Secured Data
            if (entity.Event.CsgLvlId > 0)
            {
                sdata.ScrdObjId = entity.EventId;
                sdata.ScrdObjTypeId = (byte)ScrdObjType.UCaaS_EVENT;

                if (!entity.UcaaSActyTypeId.Equals((short)UcaasActivityType.Disconnect))
                {
                    sdata.CustCntctNme = _common.GetEncryptValue(entity.InstlSitePocNme ?? "");
                    sdata.CustCntctPhnNbr = _common.GetEncryptValue(entity.InstlSitePocPhnNbr ?? "");
                    sdata.CustCntctCellPhnNbr = _common.GetEncryptValue(entity.InstlSitePocCellPhnNbr ?? "");
                    sdata.SrvcAssrnPocNme = _common.GetEncryptValue(entity.SrvcAssrnPocNme ?? "");
                    sdata.SrvcAssrnPocPhnNbr = _common.GetEncryptValue(entity.SrvcAssrnPocPhnNbr ?? "");
                    sdata.SrvcAssrnPocCellPhnNbr = _common.GetEncryptValue(entity.SrvcAssrnPocCellPhnNbr ?? "");
                    sdata.StreetAdr1 = _common.GetEncryptValue(entity.StreetAdr ?? "");
                    sdata.FlrId = _common.GetEncryptValue(entity.FlrBldgNme ?? "");
                    sdata.CtyNme = _common.GetEncryptValue(entity.CtyNme ?? "");
                    sdata.SttPrvnNme = _common.GetEncryptValue(entity.SttPrvnNme ?? "");
                    sdata.CtryRgnNme = _common.GetEncryptValue(entity.CtryRgnNme ?? "");
                    sdata.ZipPstlCd = _common.GetEncryptValue(entity.ZipCd ?? "");
                }

                sdata.CustNme = _common.GetEncryptValue(entity.CustNme ?? "");
                sdata.EventTitleTxt = _common.GetEncryptValue(entity.EventTitleTxt ?? "");
                sdata.CustEmailAdr = _common.GetEncryptValue(entity.CustAcctTeamPdlNme ?? "");

                if (sdata.ScrdObjId == id)
                {
                    _context.CustScrdData.Update(sdata);
                }
                else
                {
                    sdata.ScrdObjId = id;
                    sdata.ScrdObjTypeId = (byte)SecuredObjectType.UCaaS_EVENT;
                    sdata.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(sdata);
                }

                if (!entity.UcaaSActyTypeId.Equals((short)UcaasActivityType.Disconnect))
                {
                    ucaas.InstlSitePocNme = string.Empty;
                    ucaas.InstlSitePocPhnNbr = string.Empty;
                    ucaas.InstlSitePocCellPhnNbr = string.Empty;
                    ucaas.SrvcAssrnPocNme = string.Empty;
                    ucaas.SrvcAssrnPocPhnNbr = string.Empty;
                    ucaas.SrvcAssrnPocCellPhnNbr = string.Empty;
                    ucaas.StreetAdr = string.Empty;
                    ucaas.FlrBldgNme = string.Empty;
                    ucaas.CtyNme = string.Empty;
                    ucaas.SttPrvnNme = string.Empty;
                    ucaas.CtryRgnNme = string.Empty;
                    ucaas.ZipCd = string.Empty;
                }

                ucaas.CustNme = string.Empty;
                ucaas.EventTitleTxt = string.Empty;
                ucaas.CustAcctTeamPdlNme = string.Empty;
            }
            else
            {
                if (sdata != null)
                {
                    _context.CustScrdData.Remove(sdata);
                }

                ucaas.InstlSitePocNme = entity.InstlSitePocNme;
                ucaas.InstlSitePocCellPhnNbr = entity.InstlSitePocCellPhnNbr;
                ucaas.InstlSitePocPhnNbr = entity.InstlSitePocPhnNbr;
                ucaas.SrvcAssrnPocNme = entity.SrvcAssrnPocNme;
                ucaas.SrvcAssrnPocCellPhnNbr = entity.SrvcAssrnPocCellPhnNbr;
                ucaas.SrvcAssrnPocPhnNbr = entity.SrvcAssrnPocPhnNbr;

                ucaas.StreetAdr = entity.StreetAdr;
                ucaas.SttPrvnNme = entity.SttPrvnNme;
                ucaas.FlrBldgNme = entity.FlrBldgNme;
                ucaas.CtryRgnNme = entity.CtryRgnNme;
                ucaas.CtyNme = entity.CtyNme;
                ucaas.ZipCd = entity.ZipCd;
            }

            ucaas.SrvcAssrnPocIntlCellPhnCd = entity.SrvcAssrnPocIntlCellPhnCd;
            ucaas.SrvcAssrnPocIntlPhnCd = entity.SrvcAssrnPocIntlPhnCd;

            ucaas.SprintCpeNcrId = entity.SprintCpeNcrId;
            ucaas.CpeDspchEmailAdr = entity.CpeDspchEmailAdr;
            ucaas.CpeDspchCmntTxt = entity.CpeDspchCmntTxt;

            ucaas.DiscMgmtCd = entity.DiscMgmtCd;
            ucaas.FullCustDiscCd = entity.FullCustDiscCd;
            ucaas.FullCustDiscReasTxt = entity.FullCustDiscReasTxt;

            ucaas.EsclCd = entity.EsclCd;
            ucaas.EsclReasId = entity.EsclReasId;
            ucaas.PrimReqDt = entity.PrimReqDt;
            ucaas.ScndyReqDt = entity.ScndyReqDt;
            ucaas.BusJustnTxt = entity.BusJustnTxt;
            ucaas.CnfrcBrdgId = entity.CnfrcBrdgId;
            ucaas.CnfrcBrdgNbr = entity.CnfrcBrdgNbr;
            ucaas.CnfrcPinNbr = entity.CnfrcPinNbr;
            ucaas.OnlineMeetingAdr = entity.OnlineMeetingAdr;
            ucaas.PubEmailCcTxt = entity.PubEmailCcTxt;
            ucaas.CmpltdEmailCcTxt = entity.CmpltdEmailCcTxt;

            ucaas.ReqorUserId = entity.ReqorUserId;
            ucaas.ReqorUserCellPhnNbr = entity.ReqorUserCellPhnNbr;
            ucaas.CmntTxt = entity.CmntTxt;

            ucaas.StrtTmst = entity.StrtTmst;
            ucaas.EndTmst = entity.EndTmst;
            ucaas.ExtraDrtnTmeAmt = entity.ExtraDrtnTmeAmt;
            ucaas.EventDrtnInMinQty = entity.EventDrtnInMinQty;
            ucaas.MnsPmId = entity.MnsPmId;
            ucaas.FailReasId = entity.FailReasId;
            ucaas.WrkflwStusId = entity.WrkflwStusId;
            ucaas.ModfdByUserId = entity.ModfdByUserId;
            ucaas.ModfdDt = entity.ModfdDt;
            //ucaas.RecStusId = entity.RecStusId;

            _context.EventDevSrvcMgmt.RemoveRange(ucaas.Event.EventDevSrvcMgmt);
            if (entity.Event.EventDevSrvcMgmt.Count > 0)
            {
                foreach (var item in entity.Event.EventDevSrvcMgmt)
                {
                    item.EventDevSrvcMgmtId = 0;
                    item.EventId = id;
                    item.CreatDt = DateTime.Now;
                    item.RecStusId = 1;
                    _context.EventDevSrvcMgmt.Add(item);
                }
            }

            _context.EventCpeDev.RemoveRange(ucaas.Event.EventCpeDev);
            if (entity.Event.EventCpeDev.Count > 0)
            {
                foreach (var item in entity.Event.EventCpeDev)
                {
                    item.EventCpeDevId = 0;
                    item.EventId = id;
                    item.CreatDt = DateTime.Now;
                    item.RecStusId = 1;
                    _context.EventCpeDev.Add(item);
                }
            }

            _eventDeviceCompletionRepo.SetInactive(a => a.EventId == id && a.RecStusId == 1);
            _eventDeviceCompletionRepo.Create(entity.Event.EventDevCmplt);

            _context.EventDiscoDev.RemoveRange(ucaas.Event.EventDiscoDev);
            _context.EventDiscoDev
                .AddRange(entity.Event.EventDiscoDev.Select(a =>
                {
                    a.EventDiscoDevId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));

            _ucaasEventOdieDevRepo.Delete(a => a.EventId == id);
            _ucaasEventOdieDevRepo.Create(entity.Event.UcaaSEventOdieDev);

            if (ucaas.Event.UcaaSEventBilling.Count > 0)
            {
                foreach (var item in ucaas.Event.UcaaSEventBilling)
                {
                    var bill = entity.Event.UcaaSEventBilling.FirstOrDefault(i => i.UcaaSBillActyId == item.UcaaSBillActyId);
                    item.IncQty = bill.IncQty;
                    item.DcrQty = bill.DcrQty;
                    _context.UcaaSEventBilling.Update(item);
                }
            }

            if (entity.EventStusId == (byte)EventStatus.Completed)
            {
                var rdl = _context.Redsgn
                    .Where(rd => entity.Event.UcaaSEventOdieDev.Select(od => od.RdsnNbr).Contains(rd.RedsgnNbr));
                foreach (var rditem in rdl)
                {
                    rditem.StusId = (short)RedesignStatus.Completed;
                    rditem.ModfdDt = DateTime.Now;
                    rditem.ModfdByCd = entity.ModfdByUserId;
                    _context.Redsgn.Update(rditem);
                }
            }

            SaveAll();

            SaveEventDevComplete(id);
        }

        public void SaveEventDevComplete(int eventId)
        {
            bool CmplDev = false;
            List<EventDevCmplt> eventDevCmpltList = new List<EventDevCmplt>();
            List<RedsgnDevEventHist> redsgnDevEventHistList = new List<RedsgnDevEventHist>();
            List<M5EventMsg> m5EventMsgList = new List<M5EventMsg>();

            // Get saved UCaaS Event Data
            UcaaSEvent obj = GetById(eventId);

            // Get related data used in validation related to H6/CCD Combination
            //List<M5CpeTableResult> mdsEventCpeDevView = new List<M5CpeTableResult>();
            //List<M5MnsTableResult> mdsEventDevSrvcMgmtView = new List<M5MnsTableResult>();
            ////List<M5CpeAsasTableResult> mdsEventASASView = new List<M5CpeAsasTableResult>();
            //if (!string.IsNullOrWhiteSpace(obj.H6) && obj.Ccd.HasValue)
            //{
            //    // Recalling the SP since some column needed for validation is not provided from UI or DB
            //    // mdsEventCpeDevView for DLVY_CLLI
            //    // mdsEventDevSrvcMgmtView for M5_ORDR_ID
            //    GetM5EventDatabyH6Results h6SiteLookup = _searchRepo.M5LookupByH6CCD(obj.H6, obj.Ccd.GetValueOrDefault().ToString(), "Y");
            //    mdsEventCpeDevView = h6SiteLookup.M5CpeTable != null ? h6SiteLookup.M5CpeTable.ToList() : null;
            //    mdsEventDevSrvcMgmtView = h6SiteLookup.M5MnsTable != null ? h6SiteLookup.M5MnsTable.ToList() : null;
            //    //mdsEventASASView = h6SiteLookup.M5CpeAsasTable.ToList();
            //}

            // Get active EventDevCmplt data
            List<EventDevCmplt> devcmplt = obj.Event.EventDevCmplt
                .Where(i => i.RecStusId == (byte)ERecStatus.Active).ToList();

            // Get all EventDevCmplt data for OldCmpltCd due to DeleteAllDevCmplt() that may delete the record
            var dbEventDevCmplt = obj.Event.EventDevCmplt;
            DeleteAllDevCmplt(obj);

            var lmes = _context.M5EventMsg.Where(i => i.EventId == eventId).ToList();

            // Get updated values on DB
            var devcmplodie = _context.EventDevCmplt.Where(i => i.EventId == eventId
                && i.OdieSentDt.HasValue && i.RecStusId == (byte)ERecStatus.Active).ToList();
            var dbM5EvntMsgs = _context.M5EventMsg.Where(i => i.EventId == eventId).ToList();

            // Do not complete redesign if there is atleast one generic MAC Activity
            bool bRedesign = !obj.Event.MdsEventMacActy.Any(i => i.MdsMacActy.MdsMacActyNme.Contains("999999999"));

            foreach (EventDevCmplt item in devcmplt)
            {
                // Add record if it doesn't exist
                if (!devcmplodie.Any(dc => dc.OdieDevNme == item.OdieDevNme && dc.RedsgnDevId == item.RedsgnDevId))
                {
                    // Get the top inactive row in old db value for OdieName and RedesignDevId
                    // to mimic OldCmpltdCd value in old COWS implementation
                    EventDevCmplt dbItem = dbEventDevCmplt.OrderByDescending(i => i.EventDevCmpltId)
                        .FirstOrDefault(dc => dc.OdieDevNme == item.OdieDevNme
                            && dc.RedsgnDevId == item.RedsgnDevId && dc.RecStusId == (byte)ERecStatus.InActive);
                    var OldCmpltdCd = dbItem != null ? dbItem.CmpltdCd : false;

                    EventDevCmplt edc = new EventDevCmplt();
                    edc.EventId = eventId;
                    edc.RedsgnDevId = item.RedsgnDevId;
                    edc.OdieDevNme = item.OdieDevNme;
                    edc.H6 = item.H6;
                    edc.CmpltdCd = item.CmpltdCd;
                    edc.RecStusId = (byte)ERecStatus.Active;
                    edc.CreatDt = DateTime.Now;
                    eventDevCmpltList.Add(edc);

                    CmplDev = (item.CmpltdCd && item.CmpltdCd != OldCmpltdCd) ? item.CmpltdCd : false;

                    if (CmplDev)
                    {
                        // For completed device/s, insert RedsgnDevEventHist record
                        // If RedsgnDevId does not exist in the current list
                        // If RedsgnDevId does not exist in the current DB
                        if (!redsgnDevEventHistList.Any(i => item.RedsgnDevId.HasValue
                                && i.RedsgnDevId == item.RedsgnDevId.Value)
                            && !_context.RedsgnDevEventHist.Any(i => item.RedsgnDevId.HasValue
                                && i.RedsgnDevId == item.RedsgnDevId.Value)
                            && !OldCmpltdCd && bRedesign)
                        {
                            RedsgnDevEventHist rde = new RedsgnDevEventHist();
                            rde.EventId = eventId;
                            rde.CmpltdDt = DateTime.Now;
                            rde.RedsgnDevId = item.RedsgnDevId.GetValueOrDefault();
                            redsgnDevEventHistList.Add(rde);
                        }

                        // Send MNS/MSS complete messages if not sent already
                        if (devcmplt.FindAll(i => OldCmpltdCd && i.RedsgnDevId == item.RedsgnDevId).Count <= 0)
                        {
                            if (obj.Event.EventDevSrvcMgmt.Count > 0)
                            {
                                var dmsil = obj.Event.EventDevSrvcMgmt.Where(i => i.OdieDevNme == item.OdieDevNme
                                    && i.RecStusId == (byte)ERecStatus.Active);
                                foreach (var dmsi in dmsil)
                                {
                                    var cpeDev = obj.Event.EventCpeDev.FirstOrDefault(i => i.CpeOrdrNbr == dmsi.MnsOrdrNbr);
                                    var orderId = cpeDev != null ? int.Parse(cpeDev.CpeOrdrId) : 0;
                                    if (orderId > 0 && !string.IsNullOrWhiteSpace(dmsi.M5OrdrCmpntId)
                                        && (lmes == null || !lmes.Any(i => i.CmpntId == int.Parse(dmsi.M5OrdrCmpntId)))
                                        && (!string.Equals(dmsi.CmpntStus, "Cancelled", StringComparison.CurrentCultureIgnoreCase)))
                                    {
                                        // Send M5 MNS Completion Msg
                                        M5EventMsg mnsm5 = new M5EventMsg();
                                        mnsm5.M5OrdrId = orderId;
                                        mnsm5.EventId = eventId;
                                        mnsm5.M5OrdrNbr = dmsi.MnsOrdrNbr;
                                        if (!string.IsNullOrWhiteSpace(dmsi.M5OrdrCmpntId))
                                            mnsm5.CmpntId = int.Parse(dmsi.M5OrdrCmpntId);
                                        mnsm5.M5MsgId = dmsi.MnsSrvcTierId.Equals(64) ? (int)M5Msg.MSSComplete : (int)M5Msg.MNSComplete;
                                        mnsm5.DevId = dmsi.DeviceId;
                                        mnsm5.StusId = (short)EmailStatus.Pending;
                                        mnsm5.CreatDt = DateTime.Now;
                                        m5EventMsgList.Add(mnsm5);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Send MNS/MSS complete messages for OdieDevNme=0 i.e DoNotSendToODIE
            if (CmplDev)
            {
                if (obj.Event.EventDevSrvcMgmt.Count > 0)
                {
                    var dmsil = obj.Event.EventDevSrvcMgmt.Where(i => i.OdieDevNme == "0"
                        && i.RecStusId == (byte)ERecStatus.Active);
                    foreach (var dmsi in dmsil)
                    {
                        var cpeDev = obj.Event.EventCpeDev.FirstOrDefault(i => i.CpeOrdrNbr == dmsi.MnsOrdrNbr);
                        var orderId = cpeDev != null ? int.Parse(cpeDev.CpeOrdrId) : 0;
                        if (orderId > 0 && !string.IsNullOrWhiteSpace(dmsi.M5OrdrCmpntId)
                            && (lmes == null || !lmes.Any(i => i.CmpntId == int.Parse(dmsi.M5OrdrCmpntId)))
                            && (!string.Equals(dmsi.CmpntStus, "Cancelled", StringComparison.CurrentCultureIgnoreCase)))
                        {
                            // Send M5 MNS Completion Msg
                            M5EventMsg mnsm5 = new M5EventMsg();
                            mnsm5.M5OrdrId = orderId;
                            mnsm5.EventId = eventId;
                            mnsm5.M5OrdrNbr = dmsi.MnsOrdrNbr;
                            if (!string.IsNullOrWhiteSpace(dmsi.M5OrdrCmpntId))
                                mnsm5.CmpntId = int.Parse(dmsi.M5OrdrCmpntId);
                            mnsm5.M5MsgId = dmsi.MnsSrvcTierId.Equals(64) ? (int)M5Msg.MSSComplete : (int)M5Msg.MNSComplete;
                            mnsm5.DevId = dmsi.DeviceId;
                            mnsm5.StusId = (short)EmailStatus.Pending;
                            mnsm5.CreatDt = DateTime.Now;
                            m5EventMsgList.Add(mnsm5);
                        }
                    }
                }
            }

            _context.M5EventMsg.AddRange(m5EventMsgList);
            _context.RedsgnDevEventHist.AddRange(redsgnDevEventHistList);
            _context.EventDevCmplt.AddRange(eventDevCmpltList);
            SaveAll();

            if (CmplDev)
            {
                OdieReq od = new OdieReq();
                od.OdieMsgId = (int)OdieMessageType.DesignComplete;
                od.StusModDt = od.CreatDt = DateTime.Now;
                od.StusId = (short)EmailStatus.Pending;
                od.H1CustId = obj.H1;
                od.MdsEventId = eventId;
                _context.OdieReq.Add(od);

                SaveAll();

                if (bRedesign)
                    _common.UpdateRedesignFromEvent(devcmplt, CmplDev, eventId,
                        eventDevCmpltList.Select(i => i.OdieDevNme).Distinct().ToList());
            }
        }

        private void DeleteAllDevCmplt(UcaaSEvent obj)
        {
            int eventId = obj.EventId;

            var devcmplnoodie = _context.EventDevCmplt.Where(i => i.EventId == eventId
                                    && !i.OdieSentDt.HasValue);
            _context.EventDevCmplt.RemoveRange(devcmplnoodie);

            var devcmplodie = _context.EventDevCmplt.Where(i => i.EventId == eventId
                                    && i.OdieSentDt.HasValue);
            foreach (var item in devcmplodie)
            {
                if (!obj.Event.EventDevCmplt
                        .Any(dc => dc.OdieDevNme == item.OdieDevNme && dc.RedsgnDevId == item.RedsgnDevId))
                    item.RecStusId = (byte)ERecStatus.InActive;
            }

            SaveAll();
        }

        public DataSet GetUcaasEventData(int _iEventID)
        {
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("web.GetUCaaSEventEmailData", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@iEventID", _iEventID);
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;
        }

        private string BuildTitle(string sCurrTitle, string sParam1, string sParam2, string sParam3, string sParam4, string sParam5)
        {
            StringBuilder sbTitle = new StringBuilder();
            if (sParam5 == null)
                sParam5 = string.Empty;

            sbTitle.Append(sParam1);
            if ((sParam1.Trim().Length > 0) && (sParam2.Length > 0))
                sbTitle.Append(" - ");
            sbTitle.Append(sParam2);
            if ((sParam2.Trim().Length > 0) && (sParam3.Length > 0))
                sbTitle.Append(",");
            sbTitle.Append(sParam3);
            if ((sParam3.Trim().Length > 0) && (sParam4.Length > 0))
                sbTitle.Append(",");
            sbTitle.Append(sParam4);
            if ((sParam4.Trim().Length > 0) && (sParam5.Length > 0))
                sbTitle.Append(" -- ");
            if ((sParam4.Trim().Length <= 0) && (sParam5.Length > 0) && (sbTitle.Length > 0))
                sbTitle.Append(" ");
            sbTitle.Append(sParam5);

            return sbTitle.ToString();
        }
    }
}