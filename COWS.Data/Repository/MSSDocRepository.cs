﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Data.Repository
{
    public class MSSDocRepository : IMSSDocRepository
    {
        private readonly COWSAdminDBContext _context;

        //private readonly IUserRepository _userRepository;
        //private readonly ICommonRepository _common;
        public MSSDocRepository(COWSAdminDBContext context)
        //IUserRepository userRepository,
        // ICommonRepository common)
        {
            _context = context;
            //_userRepository = userRepository;
            //_common = common; ;
        }

        /// <summary>
        /// Get all active SDE Opportunity Documents by SDEID.
        /// </summary>
        /// <param name="sdeID"></param>
        public List<SDEOpportunityDoc> Get(int sdeID)
        {
            var q = (from sod in _context.SdeOpptntyDoc
                     join lu in _context.LkUser on sod.CreatByUserId equals lu.UserId
                     where sod.SdeOpptntyId == sdeID
                     && sod.RecStusId == (byte)Entities.Enums.ERecStatus.Active
                     orderby sod.FileNme
                     select new SDEOpportunityDoc
                     {
                         SdeDocID = sod.SdeOpptntyDocId,
                         FileName = sod.FileNme,
                         FileContent = sod.FileCntnt.ToArray(),
                         FileSizeQuantity = sod.FileSizeQty,
                         CreatedDateTime = sod.CreatDt,
                         CreatedByUserId = lu.UserId,
                         CreatedByDisplayName = lu.DsplNme,
                         CreatedByEmail = lu.EmailAdr,
                         CreatedByFullName = lu.FullNme,
                         //RecStatusId = sod.RecStusId == 1 ? Entities.Enums.ERecStatus.Active : Entities.Enums.ERecStatus.InActive
                     });

            return q.ToList();
        }

        /// <summary>
        /// Get Document Details by SDE DOC ID
        /// </summary>
        /// <param name="sdeDocID"></param>
        public SDEOpportunityDoc GetByDocID(int sdeDocID)
        {
            var q = (from sod in _context.SdeOpptntyDoc
                     join lu in _context.LkUser on sod.CreatByUserId equals lu.UserId
                     where sod.SdeOpptntyDocId == sdeDocID
                     && sod.RecStusId == (byte)Entities.Enums.ERecStatus.Active
                     select new SDEOpportunityDoc
                     {
                         SdeDocID = sod.SdeOpptntyDocId,
                         FileName = sod.FileNme,
                         FileContent = sod.FileCntnt.ToArray(),
                         FileSizeQuantity = sod.FileSizeQty,
                         CreatedDateTime = sod.CreatDt,
                         CreatedByUserId = lu.UserId,
                         CreatedByDisplayName = lu.DsplNme,
                         CreatedByEmail = lu.EmailAdr,
                         CreatedByFullName = lu.FullNme,

                         //RecStatusId = sod.RecStusId == 1 ? Entities.Enums.ERecStatus.Active : Entities.Enums.ERecStatus.InActive
                     }).SingleOrDefault();

            return q;
        }

        /// <summary>
        /// Creates new SDE Opportunity Document record.
        /// </summary>
        /// <param name="sdeDocs"></param>
        /// <param name="sdeID"></param>
        public void Insert(List<SDEOpportunityDoc> sdeDocs, int sdeID, int loggedInUserId)
        {
            if (sdeDocs != null && sdeDocs.Count > 0)
            {
                SdeOpptntyDoc docs = new SdeOpptntyDoc();
                SdeOpptntyDoc newSDEDoc;
                foreach (SDEOpportunityDoc item in sdeDocs)
                {
                    newSDEDoc = new SdeOpptntyDoc();
                    newSDEDoc.SdeOpptntyId = sdeID;
                    newSDEDoc.FileNme = item.FileName;
                    newSDEDoc.FileCntnt = item.FileContent;
                    newSDEDoc.FileSizeQty = item.FileSizeQuantity;
                    newSDEDoc.CreatByUserId = loggedInUserId;
                    newSDEDoc.CreatDt = DateTime.Now;
                    newSDEDoc.RecStusId = (byte)Entities.Enums.ERecStatus.Active;
                    _context.SdeOpptntyDoc.Add(newSDEDoc);
                }
                _context.SaveChanges();
            }
        }

        /// <summary>
        /// Updates document records by SDE OpportunityID
        /// </summary>
        /// <param name="sdeDocs"></param>
        /// <param name="sdeID"></param>
        public void Update(List<SDEOpportunityDoc> sdeDocs, int sdeID, int loggedInUserId)
        {
            // Inactivate all document record for SDEID
            Delete(sdeID);

            // Update/Insert record based on given document list
            if (sdeDocs != null && sdeDocs.Count > 0)
            {
                List<SDEOpportunityDoc> newSDEDocs = new List<SDEOpportunityDoc>();
                foreach (SDEOpportunityDoc item in sdeDocs)
                {
                    SdeOpptntyDoc updateDoc = (SdeOpptntyDoc)(from doc in _context.SdeOpptntyDoc
                                                              where doc.SdeOpptntyDocId == item.SdeDocID
                                                              select doc).SingleOrDefault();
                    if (updateDoc != null)
                    {
                        updateDoc.RecStusId = (byte)ERecStatus.Active;

                    }
                    else
                    {
                        newSDEDocs.Add(item);
                    }
                }
                _context.SaveChanges();

                if (newSDEDocs != null && newSDEDocs.Count > 0)
                    Insert(newSDEDocs, sdeID, loggedInUserId);
            }
        }


        /// <summary>
        /// Delete is not actually removing record in DB. Updating REC_STUS to 0 for Inactive.
        /// </summary>
        /// <param name="sdeID"></param>
        public void Delete(int sdeID)
        {
            var q = (from _q in _context.SdeOpptntyDoc
                     where _q.SdeOpptntyId == sdeID
                     select _q);

            foreach (SdeOpptntyDoc doc in q)
            {
                doc.RecStusId = (byte)ERecStatus.InActive;
            }
            _context.SaveChanges();

        }
    }
}