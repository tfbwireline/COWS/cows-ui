﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class ProfileHierarchyRepository : IProfileHierarchyRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public ProfileHierarchyRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }


        public LkPrfHrchy GetById(int id)
        {
            return _context.LkPrfHrchy
                            .SingleOrDefault(x => x.Id == id);
        }


        public IQueryable<LkPrfHrchy> Find(Expression<Func<LkPrfHrchy, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkPrfHrchy, out List<LkPrfHrchy> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkPrfHrchy> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkPrfHrchy, out List<LkPrfHrchy> list))
            {
                list = _context.LkPrfHrchy
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkPrfHrchy, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkPrfHrchy Create(LkPrfHrchy entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkPrfHrchy entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkPrfHrchy);
            return _context.SaveChanges();
        }

    }
}
