﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderVlanRepository : IOrderVlanRepository
    {
        private readonly COWSAdminDBContext _context;

        public OrderVlanRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public OrdrVlan Create(OrdrVlan entity)
        {
            _context.OrdrVlan.Add(entity);
            SaveAll();

            return GetById(entity.OrdrId);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<OrdrVlan> Find(Expression<Func<OrdrVlan, bool>> predicate)
        {
            //throw new NotImplementedException();
            return _context.OrdrVlan
                            .Where(predicate);
        }

        public IEnumerable<OrdrVlan> GetAll()
        {
            //throw new NotImplementedException();
            return _context.OrdrVlan
                            .OrderBy(i => i.OrdrId)
                            .ToList();
        }

        public OrdrVlan GetById(int id)
        {
            return Find(a => a.OrdrVlanId == id).SingleOrDefault();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, OrdrVlan entity)
        {
            OrdrVlan vlan = GetById(id);

            vlan.VlanId = entity.VlanId;
            vlan.VlanSrcNme = entity.VlanSrcNme;
            vlan.VlanPctQty = entity.VlanPctQty;
            vlan.TrmtgCd = entity.TrmtgCd;
            vlan.CreatByUserId = entity.CreatByUserId;
            vlan.CreatDt = entity.CreatDt;
        }
    }
}