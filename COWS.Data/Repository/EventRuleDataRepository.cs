﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventRuleDataRepository : IEventRuleDataRepository
    {
        private readonly COWSAdminDBContext _context;

        public EventRuleDataRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<EventRuleData> Find(Expression<Func<EventRuleData, bool>> predicate)
        {
            return _context.EventRuleData
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<EventRuleData> GetAll()
        {
            return _context.EventRuleData
                            .AsNoTracking()
                            .ToList();
        }

        public EventRuleData GetById(int id)
        {
            return _context.EventRuleData
                .SingleOrDefault(a => a.EventRuleId == id);
        }

        public List<EventRuleData> GetByRuleId(int id)
        {
            return _context.EventRuleData
                .Where(a => a.EventRuleId == id)
                .ToList();
        }

        public EventRuleData GetByRuleIdDataCode(int id, string sEventDataCode)
        {
            return _context.EventRuleData
                .SingleOrDefault(a => a.EventRuleId == id && a.EventDataCd == sEventDataCode);
        }

        public EventRuleData Create(EventRuleData entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, EventRuleData entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}