﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class H5FolderRepository : IH5FolderRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _commonRepo;
        private readonly IConfiguration _config;

        public H5FolderRepository(COWSAdminDBContext context, ICommonRepository commonRepo,
            IConfiguration config)
        {
            _context = context;
            _commonRepo = commonRepo;
            _config = config;
        }

        public IQueryable<H5Foldr> Find(Expression<Func<H5Foldr, bool>> predicate)
        {
            return _context.H5Foldr
                .Include(i => i.CsgLvl)
                .Include(i => i.CtryCdNavigation)
                .Include(i => i.H5Doc)
                //.Include(a => a.CustScrdData)
                .Include(i => i.CreatByUser)
                .Include(i => i.ModfdByUser)
                .Where(predicate);
        }

        public IEnumerable<H5Foldr> GetAll()
        {
            return _context.H5Foldr
                            .Include(i => i.CsgLvl)
                            .Include(i => i.CtryCdNavigation)
                            .Include(i => i.H5Doc)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .ToList();
        }

        public H5Foldr GetById(int id)
        {
            return Find(a => a.H5FoldrId == id)
                .SingleOrDefault();
        }

        public H5Foldr GetById(int id, string adid)
        {
            H5Foldr h5Folder = Find(a => a.H5FoldrId == id)
                .SingleOrDefault();

            var UserCsg = _commonRepo.GetCSGLevelAdId(adid);

            if (UserCsg > 0 && UserCsg <= h5Folder.CsgLvlId)
            {
                CustScrdData securedData = _context.CustScrdData
                    .Where(a => a.ScrdObjId == h5Folder.H5FoldrId && a.ScrdObjTypeId == (int)SecuredObjectType.H5_FOLDR)
                    .FirstOrDefault();

                if (securedData != null)
                {
                    h5Folder.CustCtyNme = _commonRepo.GetDecryptValue(securedData.CtyNme);
                    h5Folder.CtryCd = _commonRepo.GetDecryptValue(securedData.CtryCd);
                    h5Folder.CustNme = _commonRepo.GetDecryptValue(securedData.CustNme);
                }

            }

            if (adid != null)
            {
                if (h5Folder.CsgLvlId > 0)
                {
                    // LogWebActivity
                    _commonRepo.LogWebActivity((byte)WebActyType.H5FolderDetails, h5Folder.H5FoldrId.ToString(), adid, (byte)UserCsg, h5Folder.CsgLvlId);
                }
            }

            return h5Folder;
        }

        public H5Foldr Create(H5Foldr entity)
        {
            _context.H5Foldr.Add(entity);

            SaveAll();

            if (entity.CsgLvlId > 0)
            {
                CustScrdData sdata = new CustScrdData();
                sdata.ScrdObjId = entity.H5FoldrId;
                sdata.ScrdObjTypeId = (byte)ScrdObjType.H5_FOLDR;
                sdata.CustNme = _commonRepo.GetEncryptValue(entity.CustNme);
                sdata.CtyNme = _commonRepo.GetEncryptValue(entity.CustCtyNme);
                sdata.CtryCd = _commonRepo.GetEncryptValue(entity.CtryCd);
                _context.CustScrdData.Add(sdata);

                entity.CustNme = string.Empty;
                entity.CustCtyNme = string.Empty;
                entity.CtryCd = string.Empty;
                _context.H5Foldr.Update(entity);

                SaveAll();
            }

            return GetById(entity.H5FoldrId);
        }

        public void Update(int id, H5Foldr entity)
        {
            H5Foldr h5 = GetById(id);
            if (entity.CsgLvlId > 0)
            {
                CustScrdData sdata = _context.CustScrdData.SingleOrDefault(i => i.ScrdObjId == entity.H5FoldrId && i.ScrdObjTypeId == (byte)ScrdObjType.H5_FOLDR);
                sdata.CustNme = _commonRepo.GetEncryptValue(entity.CustNme);
                sdata.CtyNme = _commonRepo.GetEncryptValue(entity.CustCtyNme);
                sdata.CtryCd = _commonRepo.GetEncryptValue(entity.CtryCd);

                if (sdata.ScrdObjId == 0)
                {
                    sdata.ScrdObjId = entity.H5FoldrId;
                    sdata.ScrdObjTypeId = (byte)ScrdObjType.H5_FOLDR;
                    _context.CustScrdData.Add(sdata);
                }
                else
                {
                    _context.CustScrdData.Update(sdata);
                }

                entity.CustNme = string.Empty;
                entity.CustCtyNme = string.Empty;
                entity.CtryCd = string.Empty;
            }
            else
            {
                h5.CustNme = entity.CustNme;
                h5.CustCtyNme = entity.CustCtyNme;
                h5.CtryCd = entity.CtryCd;
            }

            h5.RecStusId = entity.RecStusId;
            h5.ModfdByUserId = entity.ModfdByUserId;
            h5.ModfdDt = entity.ModfdDt;

            // Delete CustScrdData for deactivating H5 Folder record.
            if (h5.RecStusId == (byte)ERecStatus.InActive)
            {
                CustScrdData sdata = _context.CustScrdData.SingleOrDefault(i => i.ScrdObjId == id && i.ScrdObjTypeId == (byte)ScrdObjType.H5_FOLDR);
                if (sdata != null)
                {
                    _context.CustScrdData.Remove(sdata);
                }
            }

            // For H5 Documents
            if (entity.H5Doc != null && entity.H5Doc.Count() > 0)
            {
                // Delete old doc not in current list
                List<int> ids = entity.H5Doc.Select(i => i.H5DocId).ToList();
                List<H5Doc> notInCurrentList = _context.H5Doc.Where(i => i.H5FoldrId == id && !ids.Contains(i.H5DocId)).ToList();
                _context.H5Doc.RemoveRange(notInCurrentList);

                foreach (H5Doc doc in entity.H5Doc)
                {
                    H5Doc h5Doc = _context.H5Doc.SingleOrDefault(i => i.H5DocId == doc.H5DocId && i.FileNme == doc.FileNme);

                    if (h5Doc != null)
                    {
                        // Update existing H5 Document
                        h5Doc.CmntTxt = doc.CmntTxt;
                        h5Doc.FileCntnt = doc.FileCntnt;
                        h5Doc.FileNme = doc.FileNme;
                        h5Doc.FileSizeQty = doc.FileSizeQty;
                        h5Doc.ModfdByUserId = doc.ModfdByUserId;
                        h5Doc.ModfdDt = doc.ModfdDt;
                        _context.H5Doc.Update(h5Doc);
                    }
                    else
                    {
                        // Add new H5 Document
                        _context.H5Doc.Add(doc);
                    }

                }
            }

            SaveAll();
        }

        public void Delete(int id)
        {
            H5Foldr h5Folders = GetById(id);
            var docs = _context.H5Doc.Where(i => i.H5FoldrId == id);
            _context.H5Doc.RemoveRange(docs);
            _context.H5Foldr.Remove(h5Folders);

            CustScrdData sdata = _context.CustScrdData.SingleOrDefault(i => i.ScrdObjId == id && i.ScrdObjTypeId == (byte)ScrdObjType.H5_FOLDR);
            if (sdata != null)
            {
                _context.CustScrdData.Remove(sdata);
            }

            SaveAll();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public DataTable GetH5FolderAssociations(int h5FolderId)
        {
            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            command = new SqlCommand("[dbo].[getH5FolderAssociations]", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@H5FolderID", h5FolderId);

            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public IEnumerable<H5FolderView> Search(int h5FolderId, int custId, string ftn, string custNme, string city, string country, int userId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "dbo.SearchH5Folder @H5FoldrId, @CustId, @Ftn, @CustNme, @City, @Country, @UserId";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@H5FoldrId", SqlDbType = SqlDbType.Int, Value = h5FolderId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CustId", SqlDbType = SqlDbType.Int, Value = custId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Ftn", SqlDbType = SqlDbType.VarChar, Value = ftn.Trim() });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CustNme", SqlDbType = SqlDbType.VarChar, Value = custNme.Trim() });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@City", SqlDbType = SqlDbType.VarChar, Value = city.Trim() });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Country", SqlDbType = SqlDbType.VarChar, Value = country.Trim() });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@UserId", SqlDbType = SqlDbType.Int, Value = userId });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    var result = reader.Translate<H5FolderView>();

                    return result;
                }
            }
        }

        public H5Foldr GetByIdForOrderView(int id, int usrCsglvlId)
        {
            //var dateee = DateTime.Now;
            H5Foldr h5Folder = _context.H5Foldr
                                        .Where(i => i.H5FoldrId == id)
                                        .Include(i => i.H5Doc)
                                            .ThenInclude(i => i.CreatByUser)
                                        .SingleOrDefault();
            //var dateeee = DateTime.Now;

            if (usrCsglvlId > 0 && usrCsglvlId <= h5Folder.CsgLvlId)
            {
                CustScrdData securedData = _context.CustScrdData
                    .Where(a => a.ScrdObjId == h5Folder.H5FoldrId 
                        && a.ScrdObjTypeId == (int)SecuredObjectType.H5_FOLDR)
                    .FirstOrDefault();

                if (securedData != null)
                {
                    h5Folder.CustCtyNme = _commonRepo.GetDecryptValue(securedData.CtyNme);
                    h5Folder.CtryCd = _commonRepo.GetDecryptValue(securedData.CtryCd);
                    h5Folder.CustNme = _commonRepo.GetDecryptValue(securedData.CustNme);
                }
            }

            return h5Folder;
        }
    }
}