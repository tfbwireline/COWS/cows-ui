﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MultiVrfReqRepository : IMultiVrfReqRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MultiVrfReqRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMultiVrfReq Create(LkMultiVrfReq entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMultiVrfReq> Find(Expression<Func<LkMultiVrfReq, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMultiVrfReq, out List<LkMultiVrfReq> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMultiVrfReq> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMultiVrfReq, out List<LkMultiVrfReq> list))
            {
                list = _context.LkMultiVrfReq
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMultiVrfReq, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMultiVrfReq GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMultiVrfReq);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMultiVrfReq entity)
        {
            throw new NotImplementedException();
        }
    }
}