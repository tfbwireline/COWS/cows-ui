﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VendorOrderEmailRepository : IVendorOrderEmailRepository
    {
        private readonly COWSAdminDBContext _context;

        public VendorOrderEmailRepository(COWSAdminDBContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IQueryable<VndrOrdrEmail> Find(Expression<Func<VndrOrdrEmail, bool>> predicate)
        {
            return _context.VndrOrdrEmail
                .Include(a => a.VndrEmailType)
                .Include(a => a.EmailStus)
                .Include(a => a.VndrOrdrMs)
                .Include(a => a.VndrOrdrEmailAtchmt)
                    .ThenInclude(a => a.CreatByUser)
                .Include(a => a.CreatByUser)
                .Include(a => a.ModfdByUser)
                .Where(predicate);
        }

        public IEnumerable<VndrOrdrEmail> GetAll()
        {
            return _context.VndrOrdrEmail.ToList();
        }

        public VndrOrdrEmail GetById(int id)
        {
            return Find(a => a.VndrOrdrEmailId == id).SingleOrDefault();
        }

        public VndrOrdrEmail Create(VndrOrdrEmail entity)
        {
            _context.VndrOrdrEmail.Add(entity);
            SaveAll();

            return GetById(entity.VndrOrdrEmailId);
        }

        public void Update(int id, VndrOrdrEmail entity)
        {
            VndrOrdrEmail obj = GetById(id);

            obj.VndrEmailTypeId = entity.VndrEmailTypeId;
            obj.ToEmailAdr = entity.ToEmailAdr;
            obj.CcEmailAdr = entity.CcEmailAdr;
            obj.EmailSubjTxt = entity.EmailSubjTxt;
            obj.EmailBody = entity.EmailBody;
            obj.EmailStusId = entity.EmailStusId;
            obj.ModfdByUserId = entity.ModfdByUserId;
            obj.ModfdDt = entity.ModfdDt;

            if (entity.VndrOrdrMs.Any(a => a.VndrOrdrMsId == 0))
            {
                var toAdd = entity.VndrOrdrMs.Where(a => a.VndrOrdrMsId == 0).ToList();
                _context.VndrOrdrMs.AddRange(toAdd);
                //_context.Entry(toAdd).State = EntityState.Added;
            }
        }

        public void Delete(int id)
        {
            VndrOrdrEmail entity = GetById(id);
            CascadeDelete(entity);
            _context.VndrOrdrEmail.Remove(entity);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        private void CascadeDelete(VndrOrdrEmail entity)
        {
            _context.VndrOrdrMs.RemoveRange(entity.VndrOrdrMs);
            _context.VndrOrdrEmailAtchmt.RemoveRange(entity.VndrOrdrEmailAtchmt);
        }

        public int CreateVendorOrderMs(int vendorOrderEmailId, int modifiedBy)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.createOrderMSVersion @VendorOrderEmailID, @UserID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@VendorOrderEmailID", SqlDbType = SqlDbType.Int, Value = vendorOrderEmailId });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = modifiedBy });

                _context.Database.OpenConnection();
                int count = command.ExecuteNonQuery();
                _context.Database.CloseConnection();

                return count;
            }
        }

        public int AddVendorOrderEmail(int inVendorOrderID, byte inEmailTypeID, string inEmailSubject, string inEmailBody, int inCreatedBy)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.getNewVendorOrderEmail @VendorOrderID, @EmailTypeID, @EmailSubject, @EmailBody, @UserID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@VendorOrderID", SqlDbType = SqlDbType.Int, Value = inVendorOrderID });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EmailTypeID", SqlDbType = SqlDbType.Int, Value = inEmailTypeID });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EmailSubject", SqlDbType = SqlDbType.VarChar, Value = inEmailSubject });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@EmailBody", SqlDbType = SqlDbType.VarChar, Value = inEmailBody });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = inCreatedBy });

                _context.Database.OpenConnection();
                int count = command.ExecuteNonQuery();
                _context.Database.CloseConnection();
                if (count > 0) {
                    int id = _context.VndrOrdrEmail.OrderByDescending(u => u.VndrOrdrEmailId).FirstOrDefault().VndrOrdrEmailId;
                    return id;
                }
                return 0;
            }
        }

        public int CloneVendorOrderEmail(int vendorOrderEmailID, string attachmentsExcluded, int inCreatedBy)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                int count = 0;
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.cloneVendorOrderEmail @VendorOrderEmailID, @AttachmentsExcluded, @UserID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@VendorOrderEmailID", SqlDbType = SqlDbType.Int, Value = vendorOrderEmailID });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@AttachmentsExcluded", SqlDbType = SqlDbType.VarChar, Value = attachmentsExcluded });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = inCreatedBy });

                _context.Database.OpenConnection();
                count = command.ExecuteNonQuery();
                _context.Database.CloseConnection();
                if (count != 0)
                {
                    int id = _context.VndrOrdrEmail.OrderByDescending(u => u.VndrOrdrEmailId).FirstOrDefault().VndrOrdrEmailId;
                    return id;
                }
                return 0;
            }
        }

        public bool UpdateEmailAckDate(int inVendorOrderEmailID, DateTime? inAckDate, int loggedInUserId)
        {
 
                    var emailMS = (from oms in _context.VndrOrdrMs
                                   where oms.VndrOrdrEmailId == inVendorOrderEmailID
                                   orderby oms.VerId descending
                                   select oms).First();

                    if (inAckDate != null)
                        if (emailMS.AckByVndrDt == inAckDate)
                            return true;

                    if (inAckDate == null && emailMS.AckByVndrDt == null)
                        return true;

            if (emailMS != null && inAckDate != null)
            {
                emailMS.AckByVndrDt = inAckDate;

                _context.VndrOrdrMs.Update(emailMS);
                SaveAll();
            }

                    //VndrOrdrMs voms = new VndrOrdrMs();

                    //voms.VndrOrdrId = emailMS.VndrOrdrId;
                    //voms.VerId = Convert.ToInt16(emailMS.VerId + 1);
                    //voms.VndrOrdrEmailId = emailMS.VndrOrdrEmailId;
                    //voms.SentToVndrDt = emailMS.SentToVndrDt;
                    //if (inAckDate != null)
                    //    voms.AckByVndrDt = inAckDate;
                    //voms.CreatByUserId = loggedInUserId;
                    //voms.CreatDt = DateTime.Now;

                    //_context.VndrOrdrMs.Add(voms);
                    //SaveAll();

                    return true;

        }
    }
}