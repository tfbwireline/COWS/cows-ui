﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptCustomerTypeRepository : ICptCustomerTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptCustomerTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptCustType Create(LkCptCustType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptCustType> Find(Expression<Func<LkCptCustType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptCustType, out List<LkCptCustType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptCustType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptCustType, out List<LkCptCustType> list))
            {
                list = _context.LkCptCustType
                            .OrderBy(i => i.CptCustType)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptCustType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptCustType GetById(int id)
        {
            return _context.LkCptCustType
                            .SingleOrDefault(i => i.CptCustTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptCustType);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptCustType entity)
        {
            throw new NotImplementedException();
        }
    }
}