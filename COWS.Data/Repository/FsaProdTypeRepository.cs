﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class FsaProdTypeRepository : IFsaProdTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public FsaProdTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkFsaProdType> Find(Expression<Func<LkFsaProdType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkFsaProdType, out List<LkFsaProdType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkFsaProdType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkFsaProdType, out List<LkFsaProdType> list))
            {
                list = _context.LkFsaProdType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkFsaProdType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkFsaProdType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public LkFsaProdType GetById(string id)
        {
            return _context.LkFsaProdType
                .Where(a => a.FsaProdTypeCd == id)
                .SingleOrDefault();
        }

        public LkFsaProdType Create(LkFsaProdType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkFsaProdType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(string id, LkFsaProdType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkFsaProdType);
            return _context.SaveChanges();
        }
    }
}