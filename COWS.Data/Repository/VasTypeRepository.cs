﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VasTypeRepository : IVasTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public VasTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkVasType Create(LkVasType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkVasType> Find(Expression<Func<LkVasType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkVasType, out List<LkVasType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkVasType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkVasType, out List<LkVasType> list))
            {
                list = _context.LkVasType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkVasType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkVasType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkVasType);
            return _context.SaveChanges();
        }

        public void Update(int id, LkVasType entity)
        {
            throw new NotImplementedException();
        }
    }
}