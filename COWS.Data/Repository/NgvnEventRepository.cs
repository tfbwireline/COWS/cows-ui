﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class NgvnEventRepository : INgvnEventRepository
    {
        private readonly COWSAdminDBContext _context;

        //private readonly CommonRepository _common;
        private readonly ICommonRepository _common;

        public NgvnEventRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            //_common = new CommonRepository(context);
            _common = common;
        }

        public IQueryable<NgvnEvent> Find(Expression<Func<NgvnEvent, bool>> predicate, string adid = null)
        {
            var ngvn = _context.NgvnEvent
                .Include(i => i.Event)
                .Include(a => a.NgvnEventCktIdNua)
                .Include(a => a.NgvnEventSipTrnk)
                .Include(a => a.EventStus)
                .Include(a => a.WrkflwStus)
                .Include(a => a.CreatByUser)
                .Include(a => a.SalsUser)
                .Include(a => a.ReqorUser)
                .Where(predicate);


            if(ngvn.Count() == 1)
            {
                var data = ngvn.FirstOrDefault();
                if (adid != null)
                {
                    if ((data != null) && (data.Event.CsgLvlId > 0))
                    {
                        // LogWebActivity
                        var UserCsg = _common.GetCSGLevelAdId(adid);
                        _common.LogWebActivity(((byte)WebActyType.EventDetails), data.Event.EventId.ToString(), adid, (byte)UserCsg, (byte)data.Event.CsgLvlId);
                    }
                }
            }

            return ngvn;


        }

        public IEnumerable<NgvnEvent> GetAll()
        {
            return _context.NgvnEvent
                .ToList();
        }

        public NgvnEvent GetById(int id)
        {
            return _context.NgvnEvent
                .Include(a => a.NgvnEventCktIdNua)
                .Include(a => a.NgvnEventSipTrnk)
                .Include(i => i.Event)
                .Include(a => a.EventStus)
                .Include(a => a.WrkflwStus)
                .Include(a => a.CreatByUser)
                .Include(i => i.EsclReas)
                .Include(i => i.IpVer)
                .Include(i => i.ModfdByUser)
                .Include(i => i.SalsUser)
                .Include(i => i.ReqorUser)
                .SingleOrDefault(a => a.EventId == id);
        }

        public NgvnEvent Create(NgvnEvent entity)
        {
            _context.Event.Add(entity.Event);
            SaveAll();

            entity.EventId = entity.Event.EventId;

            // Handle secure data
            if (entity.Event.CsgLvlId > 0)
            {
                SaveCustomerSecureData(null, entity);
                entity.CustNme = null;
                entity.CustCntctNme = null;
                entity.CustCntctPhnNbr = null;
                entity.CustEmailAdr = null;
                entity.CustCntctCellPhnNbr = null;
                entity.CustCntctPgrNbr = null;
                entity.CustCntctPgrPinNbr = null;
                entity.EventTitleTxt = null;
                entity.EventDes = null;
            }
            _context.NgvnEvent.Add(entity);
            SaveAll();

            //AddCircuitID(ref entity);
            //AddSIPTrunk(ref entity);

            return GetById(entity.EventId);
        }

        private void SaveCustomerSecureData(NgvnEvent oldNgvn, NgvnEvent entity)
        {
            SecuredData obj = new SecuredData(entity);
            List<GetEncryptValues> EncValues = _common.GetEventEncryptValues(obj).ToList();

            CustScrdData sdata = new CustScrdData();
            sdata.ScrdObjId = entity.Event.EventId;
            sdata.ScrdObjTypeId = (byte)SecuredObjectType.NGVN_EVENT;
            sdata.CustNme = EncValues[0].Item;
            sdata.CustCntctNme = EncValues[1].Item;
            sdata.CustCntctPhnNbr = EncValues[2].Item;
            sdata.CustEmailAdr = EncValues[3].Item;
            sdata.CustCntctCellPhnNbr = EncValues[4].Item;
            sdata.CustCntctPgrNbr = EncValues[5].Item;
            sdata.CustCntctPgrPinNbr = EncValues[6].Item;
            sdata.EventTitleTxt = EncValues[7].Item;
            sdata.EventDes = EncValues[8].Item;

            // For Update
            if (oldNgvn != null)
            {
                CustScrdData updateCSD = _context.CustScrdData
                    .SingleOrDefault(i => i.ScrdObjId == oldNgvn.EventId
                        && i.ScrdObjTypeId == (byte)SecuredObjectType.NGVN_EVENT);

                bool insertCSD = updateCSD == null ? true : false;

                if (insertCSD && entity.Event.CsgLvlId > 0)
                {
                    // Create New Customer Secured Data
                    sdata.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(sdata);
                }
                else if (!insertCSD && entity.Event.CsgLvlId == 0)
                {
                    // Remove existing Customer Secured Data
                    _context.CustScrdData.Remove(updateCSD);
                }
                else if (!insertCSD && entity.Event.CsgLvlId > 0)
                {
                    // Update existing Customer Secured Data
                    updateCSD = sdata;
                    updateCSD.ModfdDt = DateTime.Now;
                    //_context.CustScrdData.Update(sdata);
                }
            }
            else
            {
                // For Insert
                sdata.CreatDt = DateTime.Now;
                _context.CustScrdData.Add(sdata);
            }
        }

        public void Update(int id, NgvnEvent entity)
        {
            NgvnEvent updateNgvn = GetById(id);
            entity.EventId = id;
            // Customer Secured Data
            if (entity.Event.CsgLvlId > 0 && (updateNgvn.Event.CsgLvlId != entity.Event.CsgLvlId))
            {
                updateNgvn.Event.CsgLvlId = entity.Event.CsgLvlId;
                updateNgvn.Event.ModfdDt = DateTime.Now;

                SaveCustomerSecureData(updateNgvn, entity);

                updateNgvn.CustNme = null;
                updateNgvn.CustCntctNme = null;
                updateNgvn.CustCntctPhnNbr = null;
                updateNgvn.CustEmailAdr = null;
                updateNgvn.CustCntctCellPhnNbr = null;
                updateNgvn.CustCntctPgrNbr = null;
                updateNgvn.CustCntctPgrPinNbr = null;
                updateNgvn.EventTitleTxt = null;
                updateNgvn.EventDes = null;
            }
            else
            {
                updateNgvn.EventTitleTxt = entity.EventTitleTxt;
                updateNgvn.EventDes = entity.EventDes;
                updateNgvn.CustNme = entity.CustNme;
                updateNgvn.CustEmailAdr = entity.CustEmailAdr;
                updateNgvn.CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
                updateNgvn.CustCntctPhnNbr = entity.CustCntctPhnNbr;
                updateNgvn.CustCntctNme = entity.CustNme;
                updateNgvn.CustCntctPgrNbr = entity.CustCntctPgrNbr;
                updateNgvn.CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
            }

            updateNgvn.EventStusId = entity.EventStusId;
            updateNgvn.Ftn = entity.Ftn;
            updateNgvn.CharsId = entity.CharsId;
            updateNgvn.H1 = entity.H1;
            updateNgvn.H6 = entity.H6;

            updateNgvn.ReqorUserId = entity.ReqorUserId;
            updateNgvn.CnfrcBrdgNbr = entity.CnfrcBrdgNbr;
            updateNgvn.CnfrcPinNbr = entity.CnfrcPinNbr;
            updateNgvn.SalsUserId = entity.SalsUserId;

            updateNgvn.DsgnCmntTxt = entity.DsgnCmntTxt;
            updateNgvn.PubEmailCcTxt = entity.PubEmailCcTxt;
            updateNgvn.CmpltdEmailCcTxt = entity.CmpltdEmailCcTxt;

            updateNgvn.IpVerId = entity.IpVerId;
            updateNgvn.ReltdCmpsNcrCd = entity.ReltdCmpsNcrCd;
            updateNgvn.ReltdCmpsNcrNme = entity.ReltdCmpsNcrNme;

            updateNgvn.EsclCd = entity.EsclCd;
            updateNgvn.EsclReasId = entity.EsclReasId;
            updateNgvn.PrimReqDt = entity.PrimReqDt;
            updateNgvn.ScndyReqDt = entity.ScndyReqDt;
            updateNgvn.StrtTmst = entity.StrtTmst;
            updateNgvn.EndTmst = entity.EndTmst;
            updateNgvn.EventDrtnInMinQty = entity.EventDrtnInMinQty;
            updateNgvn.ExtraDrtnTmeAmt = entity.ExtraDrtnTmeAmt;

            updateNgvn.WrkflwStusId = entity.WrkflwStusId;
            updateNgvn.NgvnProdTypeId = entity.NgvnProdTypeId;
            updateNgvn.GsrCfgrnDes = entity.GsrCfgrnDes;
            updateNgvn.CablAddDueDt = entity.CablAddDueDt;
            updateNgvn.CablPrcolTypeNme = entity.CablPrcolTypeNme;
            updateNgvn.SbcPairDes = entity.SbcPairDes;

            updateNgvn.ModfdByUserId = entity.ModfdByUserId;
            updateNgvn.ModfdDt = DateTime.Now;

            AddCircuitID(ref entity);
            AddSIPTrunk(ref entity);

            SaveAll();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public IEnumerable<LkNgvnProdType> GetNgvnProdType()
        {
            return _context.LkNgvnProdType
                .Where(a => a.RecStusId == 1)
                .OrderBy(a => a.NgvnProdTypeId)
                .ToList();
        }

        private int SetEventStatus(int workflowStatusId)
        {
            int eventStatusId = 1;
            if (workflowStatusId == (int)WorkflowStatus.Visible)
            {
                eventStatusId = (int)EventStatus.Visible;
            }
            else if (workflowStatusId == (int)WorkflowStatus.Submit)
            {
                eventStatusId = (int)EventStatus.Pending;
            }
            else if (workflowStatusId == (int)WorkflowStatus.Publish)
            {
                eventStatusId = (int)EventStatus.Published;
            }
            else if (workflowStatusId == (int)WorkflowStatus.Return ||
                workflowStatusId == (int)WorkflowStatus.Reschedule ||
                workflowStatusId == (int)WorkflowStatus.Reject)
            {
                eventStatusId = (int)EventStatus.Rework;
            }
            else if (workflowStatusId == (int)WorkflowStatus.InProgress)
            {
                eventStatusId = (int)EventStatus.InProgress;
            }

            return eventStatusId;
        }

        private void AssignCommonFields(ref NgvnEvent oldNgvn, ref CustScrdData CSDTbl, ref NgvnEvent entity)
        {
            entity.EventTitleTxt = ((_common.GetLengthLimit(oldNgvn, "EventTitleTxt") > 0) && (entity.EventTitleTxt.Length > _common.GetLengthLimit(oldNgvn, "EventTitleTxt"))) ? entity.EventTitleTxt.Substring(0, _common.GetLengthLimit(oldNgvn, "EventTitleTxt")) : entity.EventTitleTxt;
            if (entity.EventStusId == (byte)EventStatus.Visible
                && ((entity.WrkflwStusId == (byte)WorkflowStatus.Visible)
                    || entity.WrkflwStusId == (byte)WorkflowStatus.Submit))
                oldNgvn.EventStusId = entity.EventStusId;
            oldNgvn.Ftn = entity.Ftn;
            oldNgvn.CharsId = entity.CharsId;
            oldNgvn.H1 = entity.H1;
            oldNgvn.H6 = entity.H6;

            if (entity.Event != null && entity.Event.CsgLvlId > 0)
            {
                SecuredData obj = new SecuredData(entity);
                List<GetEncryptValues> EncValues = _common.GetEventEncryptValues(obj).ToList();

                CSDTbl.ScrdObjId = entity.Event.EventId;
                CSDTbl.ScrdObjTypeId = (byte)SecuredObjectType.NGVN_EVENT;
                CSDTbl.CustNme = EncValues[0].Item;
                CSDTbl.CustCntctNme = EncValues[1].Item;
                CSDTbl.CustCntctPhnNbr = EncValues[2].Item;
                CSDTbl.CustEmailAdr = EncValues[3].Item;
                CSDTbl.CustCntctCellPhnNbr = EncValues[4].Item;
                CSDTbl.CustCntctPgrNbr = EncValues[5].Item;
                CSDTbl.CustCntctPgrPinNbr = EncValues[6].Item;
                CSDTbl.EventTitleTxt = EncValues[7].Item;
                CSDTbl.EventDes = EncValues[8].Item;
            }
            else
            {
                oldNgvn.CustNme = entity.CustNme;
                oldNgvn.CustCntctNme = entity.CustCntctNme;
                oldNgvn.CustCntctPhnNbr = entity.CustCntctPhnNbr;
                oldNgvn.CustEmailAdr = entity.CustEmailAdr;
                oldNgvn.CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
                oldNgvn.CustCntctPgrNbr = entity.CustCntctPgrNbr;
                oldNgvn.CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
                oldNgvn.EventTitleTxt = entity.EventTitleTxt;
                oldNgvn.EventDes = entity.EventDes;
            }

            oldNgvn.CnfrcBrdgNbr = entity.CnfrcBrdgNbr;
            oldNgvn.CnfrcPinNbr = entity.CnfrcPinNbr;
            oldNgvn.ReqorUserId = entity.ReqorUserId;
            if (entity.SalsUserId != null)
                if (!entity.SalsUserId.Equals(string.Empty))
                    oldNgvn.SalsUserId = entity.SalsUserId;
            oldNgvn.DsgnCmntTxt = entity.DsgnCmntTxt;
            oldNgvn.PubEmailCcTxt = entity.PubEmailCcTxt;
            oldNgvn.CmpltdEmailCcTxt = entity.CmpltdEmailCcTxt;

            oldNgvn.EsclCd = entity.EsclCd;
            if (entity.EsclCd)
            {
                oldNgvn.EsclReasId = (byte)entity.EsclReasId;
                oldNgvn.PrimReqDt = entity.PrimReqDt;
                oldNgvn.ScndyReqDt = entity.ScndyReqDt;
            }
            oldNgvn.StrtTmst = entity.StrtTmst;
            oldNgvn.ExtraDrtnTmeAmt = entity.ExtraDrtnTmeAmt;
            oldNgvn.EventDrtnInMinQty = entity.EventDrtnInMinQty;
            oldNgvn.EndTmst = entity.EndTmst;
            if (entity.EventStusId == (byte)EventStatus.Visible
                && (((byte)entity.WrkflwStusId == (byte)WorkflowStatus.Visible)
                    || (byte)entity.WrkflwStusId == (byte)WorkflowStatus.Submit))
                oldNgvn.WrkflwStusId = (byte)entity.WrkflwStusId;
            if (entity.NgvnProdTypeId != 0)
                oldNgvn.NgvnProdTypeId = (byte)entity.NgvnProdTypeId;
            oldNgvn.GsrCfgrnDes = entity.GsrCfgrnDes;
            oldNgvn.CablAddDueDt = entity.CablAddDueDt;
            oldNgvn.CablPrcolTypeNme = entity.CablPrcolTypeNme;
            oldNgvn.SbcPairDes = entity.SbcPairDes;
            oldNgvn.ReltdCmpsNcrCd = entity.ReltdCmpsNcrCd;
            oldNgvn.ReltdCmpsNcrNme = entity.ReltdCmpsNcrNme;
        }

        private void AddCircuitID(ref NgvnEvent entity)
        {
            int eventId = entity.EventId;
            // Delete all existing CircuitID and create new instance
            // in case of additional CircuitID has been added/removed
            var ngvnEventCktIdNua = _context.NgvnEventCktIdNua.Where(i => i.EventId == eventId).ToList();
            if (ngvnEventCktIdNua != null && ngvnEventCktIdNua.Count > 0)
            {
                _context.NgvnEventCktIdNua.RemoveRange(ngvnEventCktIdNua);
                //_context.SaveChanges();
            }

            // Add CircuitID
            if (entity.NgvnEventCktIdNua != null && entity.NgvnEventCktIdNua.Count() > 0)
            {
                foreach (NgvnEventCktIdNua item in entity.NgvnEventCktIdNua)
                {
                    //if (item.EventId == 0)
                    //{
                    //NgvnEventCktIdNua newCktID = new NgvnEventCktIdNua();

                    item.Event = null;
                    item.EventId = eventId;
                    _context.NgvnEventCktIdNua.Add(item);
                    //}
                }
                //SaveAll();
            }
        }

        private void AddSIPTrunk(ref NgvnEvent entity)
        {
            int eventId = entity.EventId;
            // Delete all existing SiptTrnk and create new instance
            // in case of additional SiptTrnk has been added/removed
            var ngvnEventSipTrnk = _context.NgvnEventSipTrnk.Where(i => i.EventId == eventId).ToList();
            if (ngvnEventSipTrnk != null && ngvnEventSipTrnk.Count > 0)
            {
                _context.NgvnEventSipTrnk.RemoveRange(ngvnEventSipTrnk);
                //_context.SaveChanges();
            }

            // Add SiptTrnk
            if (entity.NgvnEventSipTrnk != null && entity.NgvnEventSipTrnk.Count() > 0)
            {
                foreach (NgvnEventSipTrnk item in entity.NgvnEventSipTrnk)
                {
                    //NgvnEventSipTrnk newSip = new NgvnEventSipTrnk();

                    item.Event = null;
                    item.EventId = eventId;
                    _context.NgvnEventSipTrnk.Add(item);
                }
                //SaveAll();
            }
        }
    }
}