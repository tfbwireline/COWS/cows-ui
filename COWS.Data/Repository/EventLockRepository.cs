﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventLockRepository : IEventLockRepository
    {
        private readonly COWSAdminDBContext _context;

        public EventLockRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public EventRecLock CheckLock(int id)
        {
            return GetById(id);
        }

        public IQueryable<EventRecLock> Find(Expression<Func<EventRecLock, bool>> predicate)
        {
            return _context.EventRecLock
                            .Include(i => i.Event)
                            .Include(i => i.LockByUser)
                            .Where(predicate);
        }

        public EventRecLock GetById(int id)
        {
            return _context.EventRecLock
                            .Include(i => i.Event)
                            .Include(i => i.LockByUser)
                            .SingleOrDefault(i => i.EventId == id);
        }

        public int Lock(EventRecLock entity)
        {
            EventRecLock evnt = GetById(entity.EventId);
            if (evnt == null)
            {
                _context.EventRecLock.Add(entity);
                SaveAll();

                return entity.EventId;
            }

            return 0;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Unlock(int id, int userId)
        {
            EventRecLock evnt = _context.EventRecLock.SingleOrDefault(i => i.EventId == id && i.LockByUserId == userId);
            if (evnt != null)
            {
                _context.EventRecLock.Remove(evnt);
                SaveAll();
            }
        }
    }
}