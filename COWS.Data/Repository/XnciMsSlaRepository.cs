﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class XnciMsSlaRepository : IXnciMsSlaRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public XnciMsSlaRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkXnciMsSla Create(LkXnciMsSla entity)
        {
            int maxId = _context.LkXnciMsSla.Max(i => i.MsSlaId);
            entity.MsSlaId = (byte)++maxId;
            _context.LkXnciMsSla.Add(entity);

            SaveAll();

            return GetById(entity.MsSlaId);
        }

        public void Delete(int id)
        {
            LkXnciMsSla st = GetById(id);
            _context.LkXnciMsSla.Remove(st);

            SaveAll();
        }

        public IQueryable<LkXnciMsSla> Find(Expression<Func<LkXnciMsSla, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkXnciMsSla, out List<LkXnciMsSla> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkXnciMsSla> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkXnciMsSla, out List<LkXnciMsSla> list))
            {
                list = _context.LkXnciMsSla
                            .OrderBy(i => i.MsSlaId)
                            .Select(i => new LkXnciMsSla
                            {
                                MsSlaId = i.MsSlaId,
                                MsId = i.MsId,
                                PltfrmCd = i.PltfrmCd.Equals(null) ? "IP" : i.PltfrmCd,
                                //// this has been done because for table LK_XNCI_MS_SLA  if no PlatformCd it will display as IPL
                                OrdrTypeId = i.OrdrTypeId,
                                SlaInDayQty = i.SlaInDayQty,
                                ToMsId = i.ToMsId,
                                CreatByUserId = i.CreatByUserId,
                                CreatDt = i.CreatDt,
                                CreatByUser = i.CreatByUser,
                                Ms = i.Ms,
                                OrdrType = i.OrdrType,
                                PltfrmCdNavigation = i.PltfrmCdNavigation,
                                ToMs = i.ToMs
                            })
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkXnciMsSla, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkXnciMsSla GetById(int id)
        {
            return _context.LkXnciMsSla
                            .SingleOrDefault(i => i.MsSlaId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkXnciMsSla);
            return _context.SaveChanges();
        }

        public void Update(int id, LkXnciMsSla entity)
        {
            LkXnciMsSla obj = GetById(id);
            //obj.MsId = entity.MsId;
            //obj.PltfrmCd = entity.PltfrmCd;
            //obj.OrdrTypeId = entity.OrdrTypeId;
            obj.SlaInDayQty = entity.SlaInDayQty;
            //obj.CreatByUserId = entity.CreatByUserId;
            //obj.CreatDt = entity.CreatDt;
            //obj.ToMsId = entity.ToMsId;
            //LkEventType eventType = _context.LkEventType
            //                                .Find(entity.EventTypeId);
            //if (eventType != null)
            //{
            //    telco.EventTypeId = entity.EventTypeId;
            //    //telco.EventTypeNme = entity.EventTypeNme;
            //}
            SaveAll();
        }
    }
}