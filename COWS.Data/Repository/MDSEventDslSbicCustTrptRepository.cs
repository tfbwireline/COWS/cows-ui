﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSEventDslSbicCustTrptRepository : IMDSEventDslSbicCustTrptRepository
    {
        private readonly COWSAdminDBContext _context;

        public MDSEventDslSbicCustTrptRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<MdsEventDslSbicCustTrpt> Find(Expression<Func<MdsEventDslSbicCustTrpt, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MdsEventDslSbicCustTrpt> GetAll()
        {
            throw new NotImplementedException();
        }

        public MdsEventDslSbicCustTrpt GetById(int id)
        {
            throw new NotImplementedException();
        }

        public MdsEventDslSbicCustTrpt Create(MdsEventDslSbicCustTrpt entity)
        {
            throw new NotImplementedException();
        }

        public void Create(IEnumerable<MdsEventDslSbicCustTrpt> entity)
        {
            _context.MdsEventDslSbicCustTrpt
                .AddRange(entity.Select(a =>
                {
                    a.DslSbicCustTrptId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Update(int id, MdsEventDslSbicCustTrpt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            throw new NotImplementedException();
        }

        public void SetInactive(Expression<Func<MdsEventDslSbicCustTrpt, bool>> predicate)
        {
            _context.MdsEventDslSbicCustTrpt
                .Where(predicate)
                .ToList()
                .ForEach(a => {
                    a.RecStusId = (byte)0;
                    a.ModfdDt = DateTime.Now;
                });
        }

        public IEnumerable<MdsEventDslSbicCustTrpt> GetMdsEventDslSbicCustTrptByEventId(int eventId)
        {
            return _context.MdsEventDslSbicCustTrpt.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1))
                .ToList();
        }

        public void AddCustTrptTbl(ref List<MdsEventDslSbicCustTrpt> data, int eventId)
        {
            InactiveAllCustTrptItems(eventId);

            foreach (MdsEventDslSbicCustTrpt item in data)
            {
                MdsEventDslSbicCustTrpt mct = new MdsEventDslSbicCustTrpt();

                mct.EventId = eventId;
                mct.MdsTrnsprtType = item.MdsTrnsprtType;
                mct.PrimBkupCd = item.PrimBkupCd;
                mct.VndrPrvdrTrptCktId = item.VndrPrvdrTrptCktId;
                mct.VndrPrvdrNme = item.VndrPrvdrNme;
                mct.SprintMngdCd = item.SprintMngdCd;
                mct.IsWirelessCd = item.IsWirelessCd;
                mct.BwEsnMeid = item.BwEsnMeid;
                mct.ScaNbr = item.ScaNbr;
                mct.IpAdr = item.IpAdr;
                mct.SubnetMaskAdr = item.SubnetMaskAdr;
                mct.NxthopGtwyAdr = item.NxthopGtwyAdr;
                mct.OdieDevNme = item.OdieDevNme;
                mct.CreatDt = DateTime.Now;
                mct.RecStusId = (byte)1;

                _context.MdsEventDslSbicCustTrpt.Add(mct);
            }
            _context.SaveChanges();
        }

        private void InactiveAllCustTrptItems(int eventId)
        {
            _context.MdsEventDslSbicCustTrpt.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1)).ToList().ForEach(b => { b.RecStusId = ((byte)0); b.ModfdDt = DateTime.Now; });
            _context.SaveChanges();
        }
    }
}