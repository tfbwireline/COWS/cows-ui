﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDS3rdPartyServiceRepository : IMDS3rdPartyServiceRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MDS3rdPartyServiceRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMds3rdpartySrvcLvl Create(LkMds3rdpartySrvcLvl entity)
        {
            int maxId = _context.LkMds3rdpartySrvcLvl.Max(i => i.ThrdPartySrvcLvlId);
            entity.ThrdPartySrvcLvlId = (short)++maxId;
            _context.LkMds3rdpartySrvcLvl.Add(entity);

            SaveAll();

            return GetById(entity.ThrdPartySrvcLvlId);
        }

        public void Delete(int id)
        {
            LkMds3rdpartySrvcLvl obj = GetById(id);
            _context.LkMds3rdpartySrvcLvl.Remove(obj);

            SaveAll();
        }

        public IQueryable<LkMds3rdpartySrvcLvl> Find(Expression<Func<LkMds3rdpartySrvcLvl, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMds3rdpartySrvcLvl, out List<LkMds3rdpartySrvcLvl> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMds3rdpartySrvcLvl> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMds3rdpartySrvcLvl, out List<LkMds3rdpartySrvcLvl> list))
            {
                list = _context.LkMds3rdpartySrvcLvl
                            .Include(i => i.CreatByUser)
                            .Include(i => i.SrvcType)
                            .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMds3rdpartySrvcLvl, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMds3rdpartySrvcLvl GetById(int id)
        {
            return _context.LkMds3rdpartySrvcLvl
                            .Include(i => i.CreatByUser)
                            .Include(i => i.SrvcType)
                            .SingleOrDefault(i => i.ThrdPartySrvcLvlId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMds3rdpartySrvcLvl);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMds3rdpartySrvcLvl entity)
        {
            LkMds3rdpartySrvcLvl obj = GetById(id);
            obj.ThrdPartySrvcLvlDes = entity.ThrdPartySrvcLvlDes;
            obj.SrvcTypeId = entity.SrvcTypeId;
            obj.RecStusId = entity.RecStusId;
            obj.ModfdByUserId = entity.ModfdByUserId;
            obj.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}