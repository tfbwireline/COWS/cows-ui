﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace COWS.Data.Repository
{
    public class OrdrCancelRepository : IOrdrCancelRepository
    {
        private readonly COWSAdminDBContext _context;

        public OrdrCancelRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<GetOrdrCancelView> GetOrdrCancelView(string Ftn, string H5_H6)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@p0", SqlDbType.VarChar,50),
                new SqlParameter("@p1", SqlDbType.VarChar,9)
            };
            pc[0].Value = Ftn;
            pc[1].Value = H5_H6;
            var View = _context.Query<GetOrdrCancelView>()
                .AsNoTracking()
                .FromSql("[web].[getOrdrData] @p0, @p1", pc.ToArray()).ToList<GetOrdrCancelView>();
            return View;
        }

        public int cancelOrdr(string Ftn, int USER_ID, string Notes)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@p0", SqlDbType.VarChar,50),
                new SqlParameter("@p1", SqlDbType.VarChar,500),
                new SqlParameter("@p2", SqlDbType.Int),
            };
            pc[0].Value = Ftn;
            pc[1].Value = Notes;
            pc[2].Value = USER_ID;
            var view = _context.Database.ExecuteSqlCommand("web.InitiateCancelForThisFTN @p0, @p1,@p2", pc.ToArray());
            return view;
        }

        //public IEnumerable<GetOrdrUnlockView> unlockItems(int ORDR_ID, int EVENT_ID, int USER_ID, int IsOrder, int Unlock, int IsLocked, int ItemType)
        //{
        //    List<SqlParameter> pc = new List<SqlParameter>
        //    {
        //        new SqlParameter("@p0", ORDR_ID),
        //        new SqlParameter("@p1", EVENT_ID),
        //        new SqlParameter("@p2", USER_ID),
        //        new SqlParameter("@p3", IsOrder),
        //        new SqlParameter("@p4", Unlock),
        //        new SqlParameter("@p5", IsLocked),
        //        new SqlParameter("@p6", ItemType)
        //    };
        //    var View = _context.Query<GetOrdrUnlockView>()
        //        .AsNoTracking()
        //        //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", ftn,null,null,null).ToList<GetOrdrUnlockView>();
        //        .FromSql("[web].[UnlockItems] @p0, @p1,@p2, @p3, @p4,@p5, @p6", pc.ToArray()).ToList<GetOrdrUnlockView>();
        //    return View;

        //    //var View = _context.Query<GetOrdrUnlockView>()
        //    //    .AsNoTracking()
        //    //    //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", ftn,null,null,null).ToList<GetOrdrUnlockView>();
        //    //    .FromSql("[dbo].[lockUnlockItems] @p0, @p1,@p2, @p3, @p4,@p5, @p6", parameters: new[] { ORDR_ID, EVENT_ID, USER_ID, IsOrder, Unlock, IsLocked, ItemType }).ToList<GetOrdrUnlockView>();
        //    //return View;

        //    /*
        //        List<SqlParameter> pc = new List<SqlParameter>
        //        {
        //           new SqlParameter("@customerOrderID", customerProductDelivery.CustomerOrderI),
        //           new SqlParameter("@qty", customerProductDelivery.DeliveryQty)
        //        }

        //        _context.Database.ExecuteSqlCommand("sp_UpdateProductOrderAndStock @customerOrderID, @qty", pc.ToArray());
        //     */
        //}
        //public IEnumerable<GetOrdrUnlockView> GetOrdrUnlockOrdrView(string ftn)
        //{
        //    var View = _context.Query<GetOrdrUnlockView>()
        //        .AsNoTracking()
        //        //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", ftn,null,null,null).ToList<GetOrdrUnlockView>();
        //        .FromSql("[web].[getLockedItems] @p0, @p1,@p2, @p3", parameters: new[] { ftn, null, null, null }).ToList<GetOrdrUnlockView>();
        //    return View;
        //}

        //public IEnumerable<GetOrdrUnlockView> GetOrdrUnlockEventView(string EventId)
        //{
        //    var View = _context.Query<GetOrdrUnlockView>()
        //        .AsNoTracking()
        //        //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", null, OrdrId,null,null).ToList<GetOrdrUnlockView>();
        //        .FromSql("[web].[getLockedItems] @p0, @p1,@p2, @p3", parameters: new[] { null, EventId, null ,null }).ToList<GetOrdrUnlockView>();
        //    return View;
        //}
        //public IEnumerable<GetOrdrUnlockView> GetOrdrUnlockRedsgnView(string RedsgnId)
        //{
        //    var View = _context.Query<GetOrdrUnlockView>()
        //        .AsNoTracking()
        //        //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", null, null, RedsgnId, null).ToList<GetOrdrUnlockView>();
        //        .FromSql("[web].[getLockedItems] @p0, @p1,@p2, @p3", parameters: new[] { null, null, RedsgnId, null }).ToList<GetOrdrUnlockView>();
        //    return View;
        //}
        //public IEnumerable<GetOrdrUnlockView> GetOrdrUnlockCptView(string CptId)
        //{
        //    var View = _context.Query<GetOrdrUnlockView>()
        //        .AsNoTracking()
        //        //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", null, null, null, CptId).ToList<GetOrdrUnlockView>();
        //        .FromSql("[web].[getLockedItems] @p0, @p1,@p2, @p3", parameters: new[] { null, null, null, CptId }).ToList<GetOrdrUnlockView>();
        //    return View;
        //}
    }
}