﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class WFMUserAssignmentRepository : IWFMUserAssignmentRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;

        public WFMUserAssignmentRepository(COWSAdminDBContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserWfm> GetAll()
        {
            return _context.UserWfm.ToList();
        }

        public IEnumerable<WFMUserAssignment> getWFMUserAssignments(int userID, int userProfileID = 0)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@USER_ID", userID),
                new SqlParameter("@USR_PRF_ID", userProfileID)
            };

            var wfmUserAssignmentsList = _context.Query<WFMUserAssignment>()
                .AsNoTracking()
                .FromSql("web.getWFMUserAssignments @USER_ID, @USR_PRF_ID", pc.ToArray()).ToList();

            return wfmUserAssignmentsList;
        }

        public bool DeleteWFMUserAssignments(string selectedAssignmentKeys, int userID)
        {
            try
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() {ParameterName = "@WFMIDList", SqlDbType = SqlDbType.VarChar, Value = selectedAssignmentKeys},
                    new SqlParameter() {ParameterName = "@modUserID", SqlDbType = SqlDbType.Int, Value = userID},
                };

                var updateEvents = _context.Database.ExecuteSqlCommand("dbo.deleteUserWFMAssignment @WFMIDList, @modUserID", pc.ToArray());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        IEnumerable<WFMUserAssignment> IRepository<WFMUserAssignment>.GetAll()
        {
            throw new NotImplementedException();
        }

        WFMUserAssignment IRepository<WFMUserAssignment>.GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool InsertWFMAssignment(InsertWFMUserAssignments entity)
        {
            try
            {
                SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
                SqlCommand cmd = new SqlCommand("dbo.addUserWFMAssignment_V2", connection);
                cmd.CommandTimeout = _context.commandTimeout;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("userID", entity.UserId);
                cmd.Parameters.AddWithValue("groupID", entity.GrpId);
                cmd.Parameters.AddWithValue("roleIDs", entity.RoleList);
                cmd.Parameters.AddWithValue("orderActionIDs", entity.OrderActionList);
                cmd.Parameters.AddWithValue("prodPlatIDs", entity.ProdPlatList);
                cmd.Parameters.AddWithValue("wfmLevel", entity.WfmAsmtLvlId);
                cmd.Parameters.AddWithValue("countryOrigList", entity.CountryOrigList);
                cmd.Parameters.AddWithValue("modUSERID", entity.ModfdByUserId);
                cmd.Parameters.AddWithValue("usrPrfId", entity.UsrPrfId);
                cmd.Parameters.AddWithValue("IsNewUI", 1);
                connection.Open();
                int k = cmd.ExecuteNonQuery();
                connection.Close();
                if (k != 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IEnumerable<UserWfm> GetById(int id)
        {
            return _context.UserWfm.Where(a => a.UserId == id).ToList();
        }

        public IQueryable<UserWfm> Find(Expression<Func<UserWfm, bool>> predicate)
        {
            var data = _context.UserWfm
                        .Where(predicate);
            return data;
        }

        public void Update(int id, byte wfmAssignmentType)
        {
            IEnumerable<UserWfm> users = GetById(id);
            foreach (var user in users)
            {
                user.WfmAsmtLvlId = wfmAssignmentType;
                SaveAll();
            }
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public WFMUserAssignment Create(WFMUserAssignment entity)
        {
            throw new NotImplementedException();
        }

        public bool ResetOldWFMAssignments(int userID)
        {
            try
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter() {ParameterName = "@USERID", SqlDbType = SqlDbType.Int, Value = userID}
                };

                var updateEvents = _context.Database.ExecuteSqlCommand("dbo.ResetUserWFMAssignment @USERID", pc.ToArray());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Update(int id, WFMUserAssignment entity)
        {
            throw new NotImplementedException();
        }
    }
}