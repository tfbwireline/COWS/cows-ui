﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSEventNtwkCustRepository : IMDSEventNtwkCustRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public MDSEventNtwkCustRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public MdsEventNtwkCust Create(MdsEventNtwkCust entity)
        {
            throw new NotImplementedException();
        }

        public void Create(List<MdsEventNtwkCust> entity)
        {
            _context.MdsEventNtwkCust
                .AddRange(entity.Select(a =>
                {
                    a.MdsEventNtwkCustId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<MdsEventNtwkCust> Find(Expression<Func<MdsEventNtwkCust, bool>> predicate)
        {
            return _context.MdsEventNtwkCust
                           .Where(predicate);
        }

        public IEnumerable<MdsEventNtwkCust> GetAll()
        {
            return _context.MdsEventNtwkCust
                .ToList();
        }

        public MdsEventNtwkCust GetById(int id)
        {
            //return _context.MdsEventNtwkCust
            //    .SingleOrDefault(a => a.MdsEventNtwkCustId == id);
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, MdsEventNtwkCust entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MdsEventNtwkCust> GetMDEventNtwkCustByEventId(int eventId)
        {
            List<MdsEventNtwkCust> MDSNtwkCust = new List<MdsEventNtwkCust>();

            var ntwkCust = (from ncust in _context.MdsEventNtwkCust
                            join ev in _context.Event on ncust.EventId equals ev.EventId
                            join csd in _context.CustScrdData on ncust.MdsEventNtwkCustId equals csd.ScrdObjId
                            //join csd in _context.CustScrdData on new { SrcObjId = ncust.MdsEventNtwkCustId, SrcObjTypeId = (byte)SecuredObjectType.MDS_EVENT_NTWK_CUST } equals new { csd.ScrdObjId, csd.ScrdObjTypeId}
                                             into joincsd
                            from csd in joincsd.DefaultIfEmpty()
                            where ncust.EventId == eventId
                            select new { ncust, csd.CustCntctNme, csd.CustEmailAdr, csd.CustCntctPhnNbr, ev });

            foreach (var ncitem in ntwkCust)
            {
                MdsEventNtwkCust item = new MdsEventNtwkCust();
                item.AssocH6 = ncitem.ncust.AssocH6;
                item.InstlSitePocNme = (ncitem.ev.CsgLvlId > 0) ? ((ncitem == null) ? string.Empty : _common.GetDecryptValue(ncitem.CustCntctNme)) : ncitem.ncust.InstlSitePocNme;
                item.RoleNme = ncitem.ncust.RoleNme;
                item.InstlSitePocEmail = (ncitem.ev.CsgLvlId > 0) ? ((ncitem == null) ? string.Empty : _common.GetDecryptValue(ncitem.CustEmailAdr)) : ncitem.ncust.InstlSitePocEmail;
                item.InstlSitePocPhn = (ncitem.ev.CsgLvlId > 0) ? ((ncitem == null) ? string.Empty : _common.GetDecryptValue(ncitem.CustCntctPhnNbr)) : ncitem.ncust.InstlSitePocPhn;
                MDSNtwkCust.Add(item);
            }

            return MDSNtwkCust;
        }

        public void AddNtwkCust(List<MdsEventNtwkCust> data, int eventId, int csgLvlId)
        {
            DeleteAllNtwkCust(eventId);

            foreach (MdsEventNtwkCust item in data)
            {
                MdsEventNtwkCust menc = new MdsEventNtwkCust();
                CustScrdData csdNtwkCust = new CustScrdData();

                menc.EventId = eventId;
                menc.AssocH6 = item.AssocH6;
                menc.InstlSitePocNme = item.InstlSitePocNme;
                menc.InstlSitePocEmail = item.InstlSitePocEmail;
                menc.InstlSitePocPhn = item.InstlSitePocPhn;
                menc.RoleNme = item.RoleNme;
                menc.CreatDt = DateTime.Now;

                _context.MdsEventNtwkCust.Add(menc);
                _context.SaveChanges();

                if (csgLvlId > 0)
                {
                    csdNtwkCust.ScrdObjId = menc.MdsEventNtwkCustId;
                    csdNtwkCust.ScrdObjTypeId = (byte)SecuredObjectType.MDS_EVENT_NTWK_CUST;
                    csdNtwkCust.CustCntctNme = _common.GetEncryptValue(item.InstlSitePocNme);
                    csdNtwkCust.CustEmailAdr = _common.GetEncryptValue(item.InstlSitePocEmail);
                    csdNtwkCust.CustCntctPhnNbr = _common.GetEncryptValue(item.InstlSitePocPhn);
                    csdNtwkCust.CreatDt = DateTime.Now;

                    _context.CustScrdData.Add(csdNtwkCust);
                    _context.SaveChanges();
                }
            }
        }
        
        private void DeleteAllNtwkCust(int eventId)
        {
            var mencitems = (from menc in _context.MdsEventNtwkCust
                             where menc.EventId == eventId
                             select menc);
            foreach (MdsEventNtwkCust item in mencitems)
            {
                var securedNtwkCust = _context.CustScrdData
                    .Where(a => a.ScrdObjId == item.MdsEventNtwkCustId && a.ScrdObjTypeId == (byte)SecuredObjectType.MDS_EVENT_NTWK_CUST)
                    .FirstOrDefault();
                if (securedNtwkCust != null)
                {
                    _context.CustScrdData.Remove(securedNtwkCust);
                }

                _context.MdsEventNtwkCust.Remove(item);
            }
            _context.SaveChanges();
        }

        public void Delete(List<MdsEventNtwkCust> entity)
        {
            _context.MdsEventNtwkCust.RemoveRange(entity);
            //SaveAll();
        }

        public void SetInactive(int eventId)
        {
            _context.MdsEventNtwkCust
                .Where(a => a.EventId == eventId)
                .ToList()
                .ForEach(b =>
                {
                    b.ModfdDt = DateTime.Now;
                });
        }
    }
}