﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class PlatformRepository : IPlatformRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public PlatformRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkPltfrm> Find(Expression<Func<LkPltfrm, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkPltfrm, out List<LkPltfrm> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkPltfrm> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkPltfrm, out List<LkPltfrm> list))
            {
                list = _context.LkPltfrm
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkPltfrm, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkPltfrm GetById(int id)
        {
            throw new NotImplementedException();
        }

        public LkPltfrm Create(LkPltfrm entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkPltfrm entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkPltfrm);
            return _context.SaveChanges();
        }
    }
}