﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VendorEmailLanguageRepository : IVendorEmailLanguageRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public VendorEmailLanguageRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkVndrEmailLang Create(LkVndrEmailLang entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LkVndrEmailLang> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkVndrEmailLang, out List<LkVndrEmailLang> list))
            {
                list = _context.LkVndrEmailLang
                    .AsNoTracking()
                    .ToList();
                _cache.Set(CacheKeys.LkVndrEmailLang, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkVndrEmailLang Get(int id)
        {

            LkVndrEmailLang vel = new LkVndrEmailLang();
            var vnde = (from lvem in
                            (from v in _context.LkVndrEmailLang
                             join vc in _context.LkVndr on v.VndrCd equals vc.VndrCd
                             join lcu in _context.LkUser on v.CreatByUserId equals lcu.UserId
                             join c in _context.LkCtry on v.CtryCd equals c.CtryCd into leftjoin
                             from subc in leftjoin.DefaultIfEmpty()
                             where v.VndrEmailLangId == id
                             select new { v, vc, subc.CtryNme, CU_ADID = lcu.UserAdid })
                        join lu in _context.LkUser on lvem.v.ModfdByUserId equals lu.UserId into lefjoin
                        from mu in lefjoin.DefaultIfEmpty()
                        select new
                        {
                            lvem.v,
                            lvem.vc,
                            lvem.CtryNme,
                            lvem.CU_ADID,
                            MuAdid = mu.UserAdid
                        });

            var vndel = vnde.Single();
            if (vndel != null)
            {
                vel.VndrEmailLangId = (short)id;
                vel.VndrCd = vndel.v.VndrCd;
                vel.LangNme = vndel.vc.VndrNme;
                if (vndel.v.CtryCd != null)
                {
                    vel.CtryCd = vndel.v.CtryCd;
                    if (vndel.v.CtyNme != null)
                        vel.CtryCd = vndel.CtryNme + " - " + vndel.v.CtyNme;
                    else
                        vel.CtryCd = vndel.CtryNme;
                }
                vel.LangNme = vndel.v.LangNme;
                if (vndel.v.SubLangNme != null)
                {
                    vel.SubLangNme = vndel.v.SubLangNme;
                }
                vel.CreatByUserId = vndel.v.CreatByUserId;
                vel.CreatByUser.UserAdid = vndel.CU_ADID;
                if (vndel.v.ModfdByUserId != null)
                {
                    vel.ModfdByUserId = (int)vndel.v.ModfdByUserId;
                    vel.ModfdByUser.UserAdid = vndel.MuAdid;
                    vel.ModfdDt = (DateTime)vndel.v.ModfdDt;
                }
            }

            return vel;
        }

        public LkVndrEmailLang GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkVndrEmailLang entity)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            throw new NotImplementedException();
        }

        public void ApplyLanguageRules(LkVndrEmailLang _vel)
        {
            if (_vel.LangNme.Contains("%1%") && _vel.SubLangNme != null)
            {
                if (_vel.SubLangNme.Contains("|"))
                    _vel.LangNme = _vel.LangNme.Replace("%1%", _vel.SubLangNme.Substring(0, _vel.SubLangNme.IndexOf("|")));
                else
                    _vel.LangNme = _vel.LangNme.Replace("%1%", _vel.SubLangNme);
            }

            if (_vel.LangNme.Contains("%2%") && _vel.SubLangNme != null)
            {
                _vel.LangNme = _vel.LangNme.Replace("%2%", "|" + _vel.SubLangNme);
            }
        }
    }
}