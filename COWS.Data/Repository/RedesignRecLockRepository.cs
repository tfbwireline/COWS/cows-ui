﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class RedesignRecLockRepository : IRedesignRecLockRepository
    {
        private readonly COWSAdminDBContext _context;

        public RedesignRecLockRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public RedsgnRecLock CheckLock(int id)
        {
            return GetById(id);
        }

        public IQueryable<RedsgnRecLock> Find(Expression<Func<RedsgnRecLock, bool>> predicate)
        {
            return _context.RedsgnRecLock
                            .Include(i => i.Redsgn)
                            .Where(predicate);
        }

        public RedsgnRecLock GetById(int id)
        {
            return _context.RedsgnRecLock
                            .Include(i => i.Redsgn)
                            .SingleOrDefault(i => i.RedsgnId == id);
        }

        public int Lock(RedsgnRecLock entity)
        {
            RedsgnRecLock redesign = GetById(entity.RedsgnId);
            if (redesign == null)
            {
                _context.RedsgnRecLock.Add(entity);
                SaveAll();

                return entity.RedsgnId;
            }

            return 0;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Unlock(int id, int userId)
        {
            RedsgnRecLock redesign = _context.RedsgnRecLock.SingleOrDefault(i => i.RedsgnId == id && i.LockByUserId == userId);
            if (redesign != null)
            {
                _context.RedsgnRecLock.Remove(redesign);
                SaveAll();
            }
        }
    }
}
