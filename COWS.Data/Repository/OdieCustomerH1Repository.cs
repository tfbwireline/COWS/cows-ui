﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Data.Repository
{
    public class OdieCustomerH1Repository : IOdieCustomerH1Repository
    {
        private readonly COWSAdminDBContext _context;

        public OdieCustomerH1Repository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<OdieCustomerH1> GetOdieCustomerH1Data()
        {
            var odieCustH1Data = _context.Query<OdieCustomerH1>()
                .AsNoTracking()
                .FromSql("dbo.getODIECustomerH1Data").ToList<OdieCustomerH1>();

            return odieCustH1Data;
        }
    }
}
