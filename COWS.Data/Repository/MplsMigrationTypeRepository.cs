﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MplsMigrationTypeRepository : IMplsMigrationTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MplsMigrationTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMplsMgrtnType Create(LkMplsMgrtnType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMplsMgrtnType> Find(Expression<Func<LkMplsMgrtnType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMplsMgrtnType, out List<LkMplsMgrtnType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMplsMgrtnType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMplsMgrtnType, out List<LkMplsMgrtnType> list))
            {
                list = _context.LkMplsMgrtnType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMplsMgrtnType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMplsMgrtnType GetById(int id)
        {
            return _context.LkMplsMgrtnType
                            .AsNoTracking()
                            .SingleOrDefault(i => i.MplsMgrtnTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMplsMgrtnType);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMplsMgrtnType entity)
        {
            throw new NotImplementedException();
        }
    }
}