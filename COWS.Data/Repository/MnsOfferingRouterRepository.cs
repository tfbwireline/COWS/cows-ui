﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MnsOfferingRouterRepository : IMnsOfferingRouterRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MnsOfferingRouterRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMnsOffrgRoutr Create(LkMnsOffrgRoutr entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMnsOffrgRoutr> Find(Expression<Func<LkMnsOffrgRoutr, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMnsOffrgRoutr, out List<LkMnsOffrgRoutr> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMnsOffrgRoutr> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMnsOffrgRoutr, out List<LkMnsOffrgRoutr> list))
            {
                list = _context.LkMnsOffrgRoutr
                            .OrderBy(i => i.MnsOffrgRoutrDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMnsOffrgRoutr, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMnsOffrgRoutr GetById(int id)
        {
            return _context.LkMnsOffrgRoutr
                            .SingleOrDefault(i => i.MnsOffrgRoutrId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMnsOffrgRoutr);
            throw new NotImplementedException();
        }

        public void Update(int id, LkMnsOffrgRoutr entity)
        {
            throw new NotImplementedException();
        }
    }
}