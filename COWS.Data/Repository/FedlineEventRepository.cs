﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace COWS.Data.Repository
{
    public class FedlineEventRepository : IFedlineEventRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public FedlineEventRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        private Dictionary<string, FedlineActivityType> activityConverter = new Dictionary<string, FedlineActivityType>()
        {
            {"DCC", FedlineActivityType.Disconnect},
            {"DCS", FedlineActivityType.Disconnect},
            {"HED", FedlineActivityType.HeadendDisconnect},
            {"HEI", FedlineActivityType.HeadendInstall},
            {"HEM", FedlineActivityType.HeadendMAC},
            {"NCC", FedlineActivityType.Install},
            {"NCI", FedlineActivityType.Install},
            {"NCM", FedlineActivityType.Install},
            {"NDM", FedlineActivityType.Install},
            {"NIC", FedlineActivityType.Install},
            {"NIM", FedlineActivityType.Install}
        };

        public int SaveFieldUpdateHistory(int EventID, string xmlString)
        {
            int toReturn = -1;

            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@p0", SqlDbType.BigInt),
                new SqlParameter("@p1", SqlDbType.VarChar,2000),
                new SqlParameter("@p2", SqlDbType.Int),
            };
            pc[0].Value = EventID;
            pc[1].Value = xmlString;
            pc[2].Value = toReturn;
            toReturn = _context.Database.ExecuteSqlCommand("web.insertMDSEventChanges @p0, @p1,@p2", pc.ToArray());

            //toReturn = MSSqlBase.GetDateFromDate getOutputParamValue("@ID");

            return toReturn;
        }

        public bool SaveStatusHistory(int eventID, byte ActionID, string comments, int userID, bool preConfigComplete)
        {
            EventHist eh = new EventHist();
            eh.EventId = eventID;
            eh.ActnId = ActionID;
            eh.CmntTxt = comments;
            eh.CreatByUserId = userID;
            eh.CreatDt = DateTime.Now;
            if (preConfigComplete)
                eh.PreCfgCmpltCd = "Y";
            else
                eh.PreCfgCmpltCd = "N";

            _context.EventHist.Add(eh);
            _context.SaveChanges();
            return true;
        }

        public List<string> GetEmailAddresses(List<int> UserIDList)
        {
            if (UserIDList.Count > 0)
            {
                var x = (from lu in _context.LkUser
                         where UserIDList.Contains(lu.UserId)
                         select lu.EmailAdr).Distinct();
                return x.ToList();
            }
            else
                return new List<string>();
        }

        public bool SaveEventUserData(FedlineEvent fedlineEvent)
        {
            FedlineEventUserData ud = new FedlineEventUserData()
            {
                EventId = fedlineEvent.EventID,
                EventStusId = 0,
                ArrivalDt = DateTime.Now,
                EngrUserId = -1,
                ActvUserId = -1,
                CmpltEmailCcAdr = "-1",
                ShippingCxrNme = "-1",
                TrkNbr = "-1",
                PreStgCmtltCd = !fedlineEvent.UserData.PreStageComplete,
                EventTitleTxt = null,
                DocLinkTxt = null,
                DevTypeNme = null,
                RdsnNbr = null,
                TnnlCd = null
            };

            _context.FedlineEventUserData.Attach(ud);

            ud.EventStusId = fedlineEvent.WorkflowStatusID;
            ud.ArrivalDt = fedlineEvent.UserData.ArrivalDate;
            ud.EngrUserId = fedlineEvent.UserData.PreStagingEngineerUserID;
            ud.ActvUserId = fedlineEvent.UserData.MNSActivatorUserID;
            ud.PreStgCmtltCd = fedlineEvent.UserData.PreStageComplete;
            ud.EventTitleTxt = _common.GetEncryptValues(fedlineEvent.UserData.EventTitle).FirstOrDefault().Item;

            ud.CmpltEmailCcAdr = ((_common.GetLengthLimit(ud, "CmpltEmailCcAdr") > 0) && (fedlineEvent.UserData.EmailCCPDL != null) && (fedlineEvent.UserData.EmailCCPDL.Length > _common.GetLengthLimit(ud, "CmpltEmailCcAdr"))) ? fedlineEvent.UserData.EmailCCPDL.Substring(0, _common.GetLengthLimit(ud, "CmpltEmailCcAdr")) : fedlineEvent.UserData.EmailCCPDL;

            ud.ShippingCxrNme = ((_common.GetLengthLimit(ud, "ShippingCxrNme") > 0) && (fedlineEvent.UserData.ShippingCarrier != null) && (fedlineEvent.UserData.ShippingCarrier.Length > _common.GetLengthLimit(ud, "ShippingCxrNme"))) ? fedlineEvent.UserData.ShippingCarrier.Substring(0, _common.GetLengthLimit(ud, "ShippingCxrNme")) : fedlineEvent.UserData.ShippingCarrier;

            ud.TrkNbr = ((_common.GetLengthLimit(ud, "TrkNbr") > 0) && (fedlineEvent.UserData.TrackingNumber != null) && (fedlineEvent.UserData.TrackingNumber.Length > _common.GetLengthLimit(ud, "TrkNbr"))) ? fedlineEvent.UserData.TrackingNumber.Substring(0, _common.GetLengthLimit(ud, "TrkNbr")) : fedlineEvent.UserData.TrackingNumber;

            ud.DocLinkTxt = ((_common.GetLengthLimit(ud, "DocLinkTxt") > 0) && (fedlineEvent.UserData.SOWLocation != null) && (fedlineEvent.UserData.SOWLocation.Length > _common.GetLengthLimit(ud, "DocLinkTxt"))) ? fedlineEvent.UserData.SOWLocation.Substring(0, _common.GetLengthLimit(ud, "DocLinkTxt")) : fedlineEvent.UserData.SOWLocation;

            ud.DevTypeNme = ((_common.GetLengthLimit(ud, "DevTypeNme") > 0) && (fedlineEvent.UserData.DeviceType != null) && (fedlineEvent.UserData.DeviceType.Length > _common.GetLengthLimit(ud, "DevTypeNme"))) ? fedlineEvent.UserData.DeviceType.Substring(0, _common.GetLengthLimit(ud, "DevTypeNme")) : fedlineEvent.UserData.DeviceType;

            ud.RdsnNbr = ((_common.GetLengthLimit(ud, "RdsnNbr") > 0) && (fedlineEvent.UserData.Redesign != null) && (fedlineEvent.UserData.Redesign.Length > _common.GetLengthLimit(ud, "RdsnNbr"))) ? fedlineEvent.UserData.Redesign.Substring(0, _common.GetLengthLimit(ud, "RdsnNbr")) : fedlineEvent.UserData.Redesign;

            if (fedlineEvent.UserData.TunnelStatus != null && fedlineEvent.UserData.TunnelStatus.Length > 0)
                ud.TnnlCd = fedlineEvent.UserData.TunnelStatus;

            if (fedlineEvent.UserData.FailCodeID != null)
                ud.FailCdId = Convert.ToInt16(fedlineEvent.UserData.FailCodeID);

            _context.SaveChanges();
            return true;
        }

        public void SaveEventTadpoleHeadendData(FedlineEvent fedlineEvent)
        {
            #region FEDLINE_EVENT_TADPOLE_DATA

            FedlineEventTadpoleData td = (from t in _context.FedlineEventTadpoleData
                                          where t.EventId == fedlineEvent.EventID
                                          select t).First();

            td.OrdrTypeCd = fedlineEvent.TadpoleData.OrderTypeCode;
            td.StrtTme = fedlineEvent.TadpoleData.StartDate;
            td.EndTme = fedlineEvent.TadpoleData.EndDate;
            td.DevNme = fedlineEvent.TadpoleData.ManagedDeviceData.DeviceName;
            td.DevSerialNbr = fedlineEvent.TadpoleData.ManagedDeviceData.SerialNum;
            td.DevModelNbr = fedlineEvent.TadpoleData.ManagedDeviceData.Model;
            td.DevVndrNme = fedlineEvent.TadpoleData.ManagedDeviceData.Vendor;
            td.OrdrTypeCd = fedlineEvent.TadpoleData.OrderTypeCode;
            td.XstDevNme = fedlineEvent.TadpoleData.existingDevice;
            td.CustNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.CustName).FirstOrDefault().Item;

            _context.SaveChanges();

            #endregion FEDLINE_EVENT_TADPOLE_DATA

            #region FEDLINE_EVENT_ADR

            FedlineEventAdr fea = (from a in _context.FedlineEventAdr
                                   where a.FedlineEventId == td.FedlineEventId && a.AdrTypeId == 18 //Site
                                   select a).First();

            fea.FedlineEventId = td.FedlineEventId;
            fea.Street1Adr = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.SiteAddress1).FirstOrDefault().Item;
            fea.Street2Adr = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.SiteAddress2).FirstOrDefault().Item;
            fea.CityNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.City).FirstOrDefault().Item;
            fea.SttCd = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.StateProv).FirstOrDefault().Item;
            fea.ZipCd = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.Zip).FirstOrDefault().Item;
            fea.PrvnNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.StateProv).FirstOrDefault().Item;
            fea.CtryCd = fedlineEvent.TadpoleData.SiteInstallInfo.Country;

            _context.SaveChanges();

            #endregion FEDLINE_EVENT_ADR

            //Kumar -- Need to revisit to update with User ProfileID instead of RoleID

            #region FEDLINE_EVENT_CNTCTs

            FedlineEventCntct fecPrim = (from c in _context.FedlineEventCntct
                                         where c.FedlineEventId == td.FedlineEventId
                                          && c.CntctTypeId == 1
                                          && c.RoleId == 1
                                         select c).First();

            fecPrim.CntctNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.PrimaryPOCName).FirstOrDefault().Item;
            fecPrim.PhnNbr = fedlineEvent.TadpoleData.SiteInstallInfo.PrimaryPOCPhone;
            fecPrim.PhnExtNbr = fedlineEvent.TadpoleData.SiteInstallInfo.PrimaryPOCPhoneExtension;
            fecPrim.EmailAdr = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.PrimaryPOCEmail).FirstOrDefault().Item;

            FedlineEventCntct fecSec = (from c in _context.FedlineEventCntct
                                        where c.FedlineEventId == td.FedlineEventId
                                         && c.CntctTypeId == 2  //1 for Primary, 2 for secondary
                                         && c.RoleId == 1    //1 for Installation, 74 for Shipping
                                        select c).First();

            fecSec.CntctNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.SecondaryPOCName).FirstOrDefault().Item;
            fecSec.PhnNbr = fedlineEvent.TadpoleData.SiteInstallInfo.SecondaryPOCPhone;
            fecSec.PhnExtNbr = fedlineEvent.TadpoleData.SiteInstallInfo.SecondaryPOCPhoneExtension;
            fecSec.EmailAdr = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.SecondaryPOCEmail).FirstOrDefault().Item;

            _context.SaveChanges();

            #endregion FEDLINE_EVENT_CNTCTs

            #region APPT

            var apt = (from a in _context.Appt
                       where a.EventId == fedlineEvent.EventID
                       select a);

            if (apt.Count() > 0)
            {
                //Update if it already exists and it has a value
                if (fedlineEvent.TadpoleData.StartDate.HasValue)
                {
                    Appt a = apt.First();
                    a.StrtTmst = fedlineEvent.TadpoleData.StartDate.Value;
                    a.EndTmst = fedlineEvent.TadpoleData.EndDate;

                    _context.SaveChanges();
                }
                else  //If there is no Date value, delete it
                {
                    _context.Appt.Remove(apt.First());
                    _context.SaveChanges();
                }
            }
            else
            {
                //If it doesn't exist and has a value, create.  Else leave alone.
                if (fedlineEvent.TadpoleData.StartDate.HasValue)
                {
                    Appt newAPPT = GetMaskedAppointment(fedlineEvent.EventID, fedlineEvent.TadpoleData.StartDate.Value, fedlineEvent.TadpoleData.EndDate);
                    _context.Appt.Add(newAPPT);
                    _context.SaveChanges();
                }
            }

            #endregion APPT
        }

        public int SubmitHeadendEvent(FedlineEvent fedlineEvent, int UserID)
        {
            #region EVENT

            Event e = new Event();
            e.EventTypeId = (short)EventType.Fedline;
            e.CsgLvlId = (byte)Entities.Enums.CSGLevel.CSGLevel2;
            e.CreatDt = DateTime.Now;

            _context.Event.Add(e);
            _context.SaveChanges();

            #endregion EVENT

            #region FEDLINE_EVENT_TADPOLE_DATA

            FedlineEventTadpoleData td = new FedlineEventTadpoleData();
            td.EventId = e.EventId;
            td.FrbReqId = 0;
            var seq = (from fetd in _context.FedlineEventTadpoleData
                       where fetd.FrbReqId == 0 && fetd.SeqNbr <= 0
                       select fetd.SeqNbr);
            if (seq.Count() > 0)
                td.SeqNbr = seq.Min() - 1;
            else
                td.SeqNbr = -1;

            td.OrdrTypeCd = fedlineEvent.TadpoleData.OrderTypeCode;
            td.DsgnTypeCd = null;
            td.StrtTme = fedlineEvent.TadpoleData.StartDate;
            td.EndTme = fedlineEvent.TadpoleData.EndDate;
            td.DevNme = fedlineEvent.TadpoleData.ManagedDeviceData.DeviceName;
            td.DevSerialNbr = fedlineEvent.TadpoleData.ManagedDeviceData.SerialNum;
            td.DevModelNbr = fedlineEvent.TadpoleData.ManagedDeviceData.Model;
            td.DevVndrNme = fedlineEvent.TadpoleData.ManagedDeviceData.Vendor;
            td.CreatDt = DateTime.Now;
            td.RecStusId = 1;
            td.FrbOrdrTypeCd = fedlineEvent.TadpoleData.OrderTypeCode;
            td.IsMoveCd = false;
            td.XstDevNme = fedlineEvent.TadpoleData.existingDevice;
            td.IsExpediteCd = false;
            td.ReqRecvDt = null;
            td.OrgId = string.Empty;
            td.FrbInstlTypeNme = null;
            td.OrdrSubTypeNme = null;
            td.ReplmtReasNme = null;
            td.CustNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.CustName).FirstOrDefault().Item;

            _context.FedlineEventTadpoleData.Add(td);
            _context.SaveChanges();

            #endregion FEDLINE_EVENT_TADPOLE_DATA

            #region FEDLINE_EVENT_USER_DATA

            FedlineEventUserData ud = new FedlineEventUserData();
            ud.EventId = e.EventId;
            ud.EventStusId = (byte)EventStatus.Submitted;
            ud.EventTitleTxt = _common.GetEncryptValues(fedlineEvent.UserData.EventTitle).FirstOrDefault().Item;
            ud.CmpltEmailCcAdr = fedlineEvent.UserData.EmailCCPDL;
            ud.PreStgCmtltCd = false;
            ud.ActvUserId = fedlineEvent.UserData.MNSActivatorUserID;
            ud.DocLinkTxt = fedlineEvent.UserData.SOWLocation;
            ud.DevTypeNme = fedlineEvent.UserData.DeviceType;
            ud.RdsnNbr = fedlineEvent.UserData.Redesign;
            ud.TnnlCd = fedlineEvent.UserData.TunnelStatus;

            _context.FedlineEventUserData.Add(ud);
            _context.SaveChanges();

            #endregion FEDLINE_EVENT_USER_DATA

            #region Primary FEDLINE_EVENT_CNTCT

            FedlineEventCntct fecPrim = new FedlineEventCntct();
            fecPrim.FedlineEventId = td.FedlineEventId;
            fecPrim.RoleId = 1;   //1 for Installation, 74 for Shipping
            fecPrim.CntctTypeId = 1;   //1 for Primary, 2 for secondary
            fecPrim.CntctNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.PrimaryPOCName).FirstOrDefault().Item;
            fecPrim.PhnNbr = fedlineEvent.TadpoleData.SiteInstallInfo.PrimaryPOCPhone;
            fecPrim.PhnExtNbr = fedlineEvent.TadpoleData.SiteInstallInfo.PrimaryPOCPhoneExtension;
            fecPrim.EmailAdr = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.PrimaryPOCEmail).FirstOrDefault().Item;
            fecPrim.CreatDt = DateTime.Now;

            _context.FedlineEventCntct.Add(fecPrim);
            _context.SaveChanges();

            #endregion Primary FEDLINE_EVENT_CNTCT

            #region Secondary FEDLINE_EVENT_CNTCT

            FedlineEventCntct fecSec = new FedlineEventCntct();
            fecSec.FedlineEventId = td.FedlineEventId;
            fecSec.RoleId = 1;   //1 for Installation, 74 for Shipping
            fecSec.CntctTypeId = 2;   //1 for Primary, 2 for secondary
            fecSec.CntctNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.SecondaryPOCName).FirstOrDefault().Item;
            fecSec.PhnNbr = fedlineEvent.TadpoleData.SiteInstallInfo.SecondaryPOCPhone;
            fecSec.PhnExtNbr = fedlineEvent.TadpoleData.SiteInstallInfo.SecondaryPOCPhoneExtension;
            fecSec.EmailAdr = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.SecondaryPOCEmail).FirstOrDefault().Item;
            fecSec.CreatDt = DateTime.Now;

            _context.FedlineEventCntct.Add(fecSec);
            _context.SaveChanges();

            #endregion Secondary FEDLINE_EVENT_CNTCT

            #region FEDLINE_EVENT_ADR

            FedlineEventAdr fa = new FedlineEventAdr();
            fa.FedlineEventId = td.FedlineEventId;
            fa.AdrTypeId = 18;    //18 for Site, 19 for Shipping
            fa.Street1Adr = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.SiteAddress1).FirstOrDefault().Item;
            fa.Street2Adr = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.SiteAddress2).FirstOrDefault().Item;
            fa.CtryCd = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.City).FirstOrDefault().Item.ToString();
            fa.SttCd = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.StateProv).FirstOrDefault().Item;
            fa.ZipCd = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.Zip).FirstOrDefault().Item;
            fa.PrvnNme = _common.GetEncryptValues(fedlineEvent.TadpoleData.SiteInstallInfo.StateProv).FirstOrDefault().Item;
            fa.CtryCd = fedlineEvent.TadpoleData.SiteInstallInfo.Country;
            fa.CreatDt = DateTime.Now;

            _context.FedlineEventAdr.Add(fa);
            _context.SaveChanges();

            #endregion FEDLINE_EVENT_ADR

            #region APPT

            if (fedlineEvent.TadpoleData.StartDate.HasValue)
            {
                Appt apt = GetMaskedAppointment(e.EventId, fedlineEvent.TadpoleData.StartDate.Value, fedlineEvent.TadpoleData.EndDate);
                _context.Appt.Add(apt);
                _context.SaveChanges();
            }

            #endregion APPT

            #region Event History

            EventHist eh = new EventHist();
            eh.EventId = e.EventId;
            eh.ActnId = 1; //Event Submitted
            eh.CmntTxt = fedlineEvent.StatusComments;
            eh.CreatByUserId = UserID;
            eh.CreatDt = DateTime.Now;
            eh.PreCfgCmpltCd = "N";
            _context.EventHist.Add(eh);
            _context.SaveChanges();

            #endregion Event History

            fedlineEvent.EventID = e.EventId;

            return fedlineEvent.EventID;
        }

        public bool InsertMessage(string MessageType, FedlineEvent fedlineEvent, bool holdMessage)
        {
            FedlineEventMsg fem = new FedlineEventMsg();
            fem.FedlineEventId = fedlineEvent.TadpoleData.CowsFedlineID;
            fem.FedlineEventMsgStusNme = MessageType;
            if (!holdMessage)
                fem.StusId = 10;
            else
                fem.StusId = 9;
            fem.CreatDt = DateTime.Now;

            switch (MessageType)
            {
                case "StatusMessage":
                    fem.RspnDes = ((EventStatus)fedlineEvent.WorkflowStatusID).ToString();
                    break;

                case "Rework":
                    fem.RspnDes = fedlineEvent.FailCode;
                    break;

                case "DiscCompleteExc":
                    fem.RspnDes = fedlineEvent.FailCode;
                    break;

                case "ShippingConf":
                case "InstallComplete":
                    break;
            }

            _context.FedlineEventMsg.Add(fem);
            _context.SaveChanges();
            return true;
        }

        public FedlineEvent GetFedEvent(int EventID)
        {
            FedlineEvent fe = new FedlineEvent();

            #region FEDLINE_EVENT_TADPOLE_DATA

            var td = (from fetd in _context.FedlineEventTadpoleData
                      join fo in _context.LkFedlineOrdrType on fetd.OrdrTypeCd equals fo.OrdrTypeCd
                      where fetd.EventId == EventID && fetd.RecStusId == (byte)ERecStatus.Active
                      orderby fetd.FedlineEventId descending
                      select new { fetd, fo.PrntOrdrTypeDes }).FirstOrDefault();

            if (td != null)
            {
                fe.EventID = td.fetd.EventId;
                fe.TadpoleData.CowsFedlineID = td.fetd.FedlineEventId;
                fe.TadpoleData.CustName = _common.GetDecryptValue(td.fetd.CustNme);
                fe.TadpoleData.CustOrgID = td.fetd.OrgId;
                fe.TadpoleData.MACType = activityConverter[td.fetd.OrdrTypeCd];
                fe.TadpoleData.OrderTypeCode = td.fetd.OrdrTypeCd;
                fe.TadpoleData.StartDate = td.fetd.StrtTme;
                fe.TadpoleData.EndDate = td.fetd.EndTme;
                fe.TadpoleData.FRBRequestID = td.fetd.FrbReqId;
                fe.TadpoleData.Version = td.fetd.SeqNbr;
                fe.TadpoleData.DesignType = td.fetd.DsgnTypeCd;
                fe.TadpoleData.ManagedDeviceData.DeviceName = td.fetd.DevNme;
                fe.TadpoleData.ManagedDeviceData.SerialNum = td.fetd.DevSerialNbr;
                fe.TadpoleData.ManagedDeviceData.Vendor = td.fetd.DevVndrNme;
                fe.TadpoleData.ManagedDeviceData.Model = td.fetd.DevModelNbr;
                fe.TadpoleData.OrigDeviceData.DeviceName = td.fetd.OrigDevId;
                fe.TadpoleData.OrigDeviceData.SerialNum = td.fetd.OrigDevSerialNbr;
                fe.TadpoleData.OrigDeviceData.OrigReqID = td.fetd.OrigFrbReqId;
                fe.TadpoleData.OrigDeviceData.Model = td.fetd.OrigModel;
                fe.TadpoleData.isMove = td.fetd.IsMoveCd;
                fe.TadpoleData.isExpedite = td.fetd.IsExpediteCd;
                fe.TadpoleData.existingDevice = td.fetd.XstDevNme;
                fe.TadpoleData.InstallType = td.fetd.FrbInstlTypeNme;
                fe.TadpoleData.OrderSubType = td.fetd.OrdrSubTypeNme;
                fe.TadpoleData.ReorderReason = td.fetd.ReplmtReasNme;
            }
            else
                return new FedlineEvent();

            fe.TadpoleData.FRBSentDate = (from fetd in _context.FedlineEventTadpoleData
                                          where fetd.EventId == EventID
                                          && (fetd.OrdrTypeCd == "NCI" || fetd.OrdrTypeCd == "DCS")
                                          select fetd.ReqRecvDt).SingleOrDefault();

            var modDate = (from fetd in _context.FedlineEventTadpoleData
                           where fetd.EventId == EventID
                           && fetd.OrdrTypeCd == "NDM"
                           orderby fetd.FedlineEventId descending
                           select fetd.ReqRecvDt);

            if (modDate.Count() > 0)
                fe.TadpoleData.FRBModifyDate = modDate.First();

            #endregion FEDLINE_EVENT_TADPOLE_DATA

            #region Contacts

            var contacts = (from con in _context.FedlineEventCntct
                            where con.FedlineEventId == td.fetd.FedlineEventId
                            select new
                            {
                                con.RoleId,
                                con.CntctTypeId,
                                con.CntctNme,
                                con.PhnNbr,
                                con.PhnExtNbr,
                                con.EmailAdr
                            });

            foreach (var fec in contacts)
            {
                if (fec.RoleId == 1)   //Site
                {
                    if (fec.CntctTypeId == 1)     //Primary
                    {
                        fe.TadpoleData.SiteInstallInfo.PrimaryPOCName = _common.GetDecryptValue(fec.CntctNme);
                        fe.TadpoleData.SiteInstallInfo.PrimaryPOCPhone = fec.PhnNbr;
                        fe.TadpoleData.SiteInstallInfo.PrimaryPOCPhoneExtension = fec.PhnExtNbr;
                        fe.TadpoleData.SiteInstallInfo.PrimaryPOCEmail = _common.GetDecryptValue(fec.EmailAdr);
                    }
                    else if (fec.CntctTypeId == 2)     //Secondary
                    {
                        fe.TadpoleData.SiteInstallInfo.SecondaryPOCName = _common.GetDecryptValue(fec.CntctNme);
                        fe.TadpoleData.SiteInstallInfo.SecondaryPOCPhone = fec.PhnNbr;
                        fe.TadpoleData.SiteInstallInfo.SecondaryPOCPhoneExtension = fec.PhnExtNbr;
                        fe.TadpoleData.SiteInstallInfo.SecondaryPOCEmail = _common.GetDecryptValue(fec.EmailAdr);
                    }
                }
                else if (fec.RoleId == 74)     //Shipping
                {
                    if (fec.CntctTypeId == 1)     //Primary
                    {
                        fe.TadpoleData.SiteShippingInfo.PrimaryPOCName = _common.GetDecryptValue(fec.CntctNme);
                        fe.TadpoleData.SiteShippingInfo.PrimaryPOCPhone = fec.PhnNbr;
                        fe.TadpoleData.SiteShippingInfo.PrimaryPOCPhoneExtension = fec.PhnExtNbr;
                        fe.TadpoleData.SiteShippingInfo.PrimaryPOCEmail = _common.GetDecryptValue(fec.EmailAdr);
                    }
                    else if (fec.CntctTypeId == 2)     //Secondary
                    {
                        fe.TadpoleData.SiteShippingInfo.SecondaryPOCName = _common.GetDecryptValue(fec.CntctNme);
                        fe.TadpoleData.SiteShippingInfo.SecondaryPOCPhone = fec.PhnNbr;
                        fe.TadpoleData.SiteShippingInfo.SecondaryPOCPhoneExtension = fec.PhnExtNbr;
                        fe.TadpoleData.SiteShippingInfo.SecondaryPOCEmail = _common.GetDecryptValue(fec.EmailAdr);
                    }
                }
            }

            #endregion Contacts

            #region Addresses

            var addresses = (from adr in _context.FedlineEventAdr
                             where adr.FedlineEventId == td.fetd.FedlineEventId
                             select new
                             {
                                 adr.AdrTypeId,
                                 adr.Street1Adr,
                                 adr.Street2Adr,
                                 adr.CityNme,
                                 adr.SttCd,
                                 adr.ZipCd,
                                 adr.PrvnNme,
                                 adr.CtryCd
                             });

            foreach (var ad in addresses)
            {
                if (ad.AdrTypeId == 18)       //18 for Site
                {
                    fe.TadpoleData.SiteInstallInfo.SiteAddress1 = _common.GetDecryptValue(ad.Street1Adr);
                    fe.TadpoleData.SiteInstallInfo.SiteAddress2 = _common.GetDecryptValue(ad.Street2Adr);
                    fe.TadpoleData.SiteInstallInfo.City = _common.GetDecryptValue(ad.CityNme);
                    string state = _common.GetDecryptValue(ad.SttCd);
                    if (state != null && state != string.Empty)
                        fe.TadpoleData.SiteInstallInfo.StateProv = state;
                    else
                        fe.TadpoleData.SiteInstallInfo.StateProv = _common.GetDecryptValue(ad.PrvnNme);
                    fe.TadpoleData.SiteInstallInfo.Zip = _common.GetDecryptValue(ad.ZipCd);
                    fe.TadpoleData.SiteInstallInfo.Country = ad.CtryCd;
                }
                else if (ad.AdrTypeId == 19)      //19 for Shipping
                {
                    fe.TadpoleData.SiteShippingInfo.SiteAddress1 = _common.GetDecryptValue(ad.Street1Adr);
                    fe.TadpoleData.SiteShippingInfo.SiteAddress2 = _common.GetDecryptValue(ad.Street2Adr);
                    fe.TadpoleData.SiteShippingInfo.City = _common.GetDecryptValue(ad.CityNme);
                    string state = _common.GetDecryptValue(ad.SttCd);
                    if (state != null && state != string.Empty)
                        fe.TadpoleData.SiteShippingInfo.StateProv = state;
                    else
                        fe.TadpoleData.SiteShippingInfo.StateProv = _common.GetDecryptValue(ad.PrvnNme);
                    fe.TadpoleData.SiteShippingInfo.Zip = _common.GetDecryptValue(ad.ZipCd);
                    fe.TadpoleData.SiteShippingInfo.Country = ad.CtryCd;
                }
            }

            #endregion Addresses

            #region Config

            var configs = (from cd in _context.FedlineEventCfgrnData
                           where cd.FedlineEventId == td.fetd.FedlineEventId
                           select new FedlineConfigEntity
                           {
                               FedlineConfigID = cd.FedlineEventCfgrnId,
                               FedlineEventID = cd.FedlineEventId,
                               PortName = cd.PortNme,
                               IPAddress = cd.IpAdr,
                               SubnetMask = cd.SubnetMaskAdr,
                               DefaultGateway = cd.DfltGtwyAdr,
                               SPEED = cd.SpeedNme,
                               Duplex = cd.DxNme,
                               MACAddress = cd.MacAdr
                           });

            fe.TadpoleData.Configs = configs.ToList();

            #endregion Config

            #region User Data

            var ud = (from feud in _context.FedlineEventUserData
                      join ls in _context.LkEventStus on feud.EventStusId equals ls.EventStusId
                      join lue in _context.LkUser on feud.EngrUserId equals lue.UserId into llue
                      from lue in llue.DefaultIfEmpty()
                      join lua in _context.LkUser on feud.ActvUserId equals lua.UserId into llua
                      from lua in llua.DefaultIfEmpty()
                      where feud.EventId == EventID
                      select new
                      {
                          feud.EventStusId,
                          feud.EventTitleTxt,
                          feud.CmpltEmailCcAdr,
                          feud.ShippingCxrNme,
                          feud.TrkNbr,
                          feud.ArrivalDt,
                          feud.PreStgCmtltCd,
                          feud.EngrUserId,
                          feud.ActvUserId,
                          ls.EventStusDes,
                          Eng = lue.DsplNme,
                          Act = lua.DsplNme,
                          feud.DevTypeNme,
                          feud.DocLinkTxt,
                          feud.RdsnNbr,
                          feud.TnnlCd,
                          feud.SlaCd
                      }).Single();

            fe.EventStatusID = ud.EventStusId;
            fe.EventStatus = ud.EventStusDes;
            fe.UserData.EventTitle = _common.GetDecryptValue(ud.EventTitleTxt);
            fe.UserData.EmailCCPDL = ud.CmpltEmailCcAdr;
            fe.UserData.ShippingCarrier = ud.ShippingCxrNme;
            fe.UserData.TrackingNumber = ud.TrkNbr;
            fe.UserData.ArrivalDate = ud.ArrivalDt;
            fe.UserData.PreStageComplete = ud.PreStgCmtltCd;
            fe.UserData.PreStagingEngineerUserID = ud.EngrUserId;
            fe.UserData.PreStagingEngineerUserDisplay = ud.Eng;
            fe.UserData.MNSActivatorUserID = ud.ActvUserId;
            fe.UserData.MNSActivatorUserDisplay = ud.Act;
            fe.UserData.DeviceType = ud.DevTypeNme;
            fe.UserData.SOWLocation = ud.DocLinkTxt;
            fe.UserData.Redesign = ud.RdsnNbr;
            fe.UserData.TunnelStatus = ud.TnnlCd;
            fe.UserData.SlaCd = ud.SlaCd;

            #endregion User Data

            return fe;
        }

        public List<FedlineUserSearchEntity> SearchUsers(string input, byte role)
        {
            if (role != 0)
            {
                var users = (from u in _context.LkUser
                             join mup in _context.MapUsrPrf on u.UserId equals mup.UserId
                             join ph in _context.LkPrfHrchy on mup.UsrPrfId equals ph.PrntPrfId
                             where mup.RecStusId == 1
                                && ph.PrntPrfId == 128
                                //&& ugr.ROLE_ID == role
                                && (u.FullNme.Contains(input) || u.DsplNme.Contains(input))
                                && u.UserId != 1
                             orderby u.DsplNme
                             select new FedlineUserSearchEntity
                             {
                                 UserID = u.UserId,
                                 DisplayName = u.DsplNme,
                                 Email = u.EmailAdr
                             });

                return users.ToList();
            }
            else
            {
                var users = (from u in _context.LkUser
                             where (u.FullNme.Contains(input) || u.DsplNme.Contains(input))
                                && u.UserId != 1
                                && u.RecStusId == (byte)ERecStatus.Active
                             orderby u.DsplNme
                             select new FedlineUserSearchEntity
                             {
                                 UserID = u.UserId,
                                 DisplayName = u.DsplNme,
                                 Email = u.EmailAdr
                             });

                return users.ToList();
            }
        }

        public void ClearInvalidTimeSlot(int EventID)
        {
            _context.EventNvldTmeSlot.RemoveRange(_context.EventNvldTmeSlot.Where(a => a.EventId == EventID));
            _context.SaveChanges();
        }

        private Appt GetMaskedAppointment(int eventID, DateTime startDate, DateTime? endDate)
        {
            //If changes need to be made here, check in TadpoleEventDB.cs file in Tadpole helper.  This code is copied from there
            string maskedCity = "Overland Park";
            string maskedState_Province = "KS";
            string maskedCountry_Region = "US";
            string maskedNumber = "Installation Contact Phone: 9999999999";

            Appt a = new Appt();
            a.EventId = eventID;
            a.StrtTmst = startDate;
            a.EndTmst = endDate;
            a.RcurncCd = false;
            a.RcurncDesTxt = "";
            a.SubjTxt = string.Format("Private Customer - {0},{1},{2} -- Initial Install Event ID : {3}", maskedCity, maskedState_Province, maskedCountry_Region, eventID);
            a.ApptLocTxt = maskedNumber;
            a.ApptTypeId = (int)CalendarEventType.Fedline;
            a.CreatByUserId = 5000;  //COWS Fedline PDL
            a.CreatDt = DateTime.Now;
            a.AsnToUserIdListTxt = "<ResourceIds>    <ResourceId Type=\"System.Int32\" Value=\"5000\" />  </ResourceIds>";

            return a;
        }

        public bool InsertStrtProcess(string ProcessType, int _eventID)
        {
            FedlineEventPrcs fep = new FedlineEventPrcs();
            fep.EventId = _eventID;
            fep.StrtTme = DateTime.Now;
            fep.CreatDt = DateTime.Now;
            switch (ProcessType)
            {
                case "Fulfilling":
                    fep.EventStusId = 9;
                    break;

                case "In Progress":
                    fep.EventStusId = 5;
                    break;
            }

            _context.FedlineEventPrcs.Add(fep);
            _context.SaveChanges();
            return true;
        }

        public bool InsertEndProcess(string ProcessType, int _eventID, int _endStatus)
        {
            int _event_Stus_ID = 0;

            switch (ProcessType)
            {
                case "ShippingConf":
                    _event_Stus_ID = 9;
                    break;

                case "In Progress":
                    _event_Stus_ID = 5;
                    break;
            }
            FedlineEventPrcs fep = (from ep in _context.FedlineEventPrcs
                                    where ep.EventId == _eventID && ep.EventStusId == _event_Stus_ID
                                          && ep.EndTme == null
                                    orderby ep.FedlineEventPrcsId descending
                                    select ep).FirstOrDefault();

            if (fep != null)
            {
                fep.EndTme = DateTime.Now;
                fep.EventEndStusId = (byte)_endStatus;
            }

            _context.SaveChanges();
            return true;
        }

        public bool UpdateSLA_cd(int _eventID)
        {
            FedlineEventUserData ud = (from feud in _context.FedlineEventUserData
                                       where feud.EventId == _eventID
                                       select feud).SingleOrDefault();

            ud.SlaCd = "N";

            _context.SaveChanges();
            return true;
        }

        public bool CheckStatusExists(int eventID)
        {
            if ((from a in _context.EventHist
                 where (a.EventId == eventID) && ((a.ActnId == 19))
                 select a).Count() > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckCancelExists(int eventID)
        {
            if ((from a in _context.FedlineEventTadpoleData
                 where (a.EventId == eventID) && ((a.OrdrTypeCd == "NCC") || (a.OrdrTypeCd == "DCC"))
                 select a).Count() > 0)
            {
                return true;
            }
            return false;
        }

        public DataTable GetEventUpdateHistByEventID(int eventId)
        {
            DataTable dtEUpdtHist = new DataTable();
            dtEUpdtHist.Columns.Add(new DataColumn("MDS_EVENT_FORM_CHNG_ID"));
            dtEUpdtHist.Columns.Add(new DataColumn("MDS_EVENT_ID"));
            dtEUpdtHist.Columns.Add(new DataColumn("CHNG_TYPE_NME"));
            dtEUpdtHist.Columns.Add(new DataColumn("FULL_NME"));
            dtEUpdtHist.Columns.Add(new DataColumn("CREAT_DT", typeof(DateTime)));

            var x = (from e in _context.MdsEventFormChng
                     join lk in _context.LkUser on e.CreatByUserId equals lk.UserId
                     where (e.MdsEventId == eventId)
                     orderby e.MdsEventFormChngId descending
                     select new { e.MdsEventFormChngId, e.MdsEventId, e.ChngTypeNme, lk.FullNme, e.CreatDt });

            dtEUpdtHist = LinqHelper.CopyToDataTable(x, null, null);

            return dtEUpdtHist;
        }

        public DataTable GetCCDHistory(int eventId)
        {
            DataTable dtFedlineCCDHistory = new DataTable();
            dtFedlineCCDHistory.Columns.Add(new DataColumn("REQ_ACT_HIST_ID"));
            dtFedlineCCDHistory.Columns.Add(new DataColumn("EVENT_ID"));
            dtFedlineCCDHistory.Columns.Add(new DataColumn("OLD_DT", typeof(DateTime)));
            dtFedlineCCDHistory.Columns.Add(new DataColumn("NEW_DT", typeof(DateTime)));
            dtFedlineCCDHistory.Columns.Add(new DataColumn("FAIL_CD_DES"));
            dtFedlineCCDHistory.Columns.Add(new DataColumn("EVENT_HIST_ID"));
            dtFedlineCCDHistory.Columns.Add(new DataColumn("CREAT_DT", typeof(DateTime)));
            dtFedlineCCDHistory.Columns.Add(new DataColumn("RSPB_PARTY_NME"));

            var ccdh = (from ccd in _context.FedlineReqActHist
                        join efc in _context.LkFedlineEventFailCd on ccd.FailCdId equals efc.FailCdId into efcd
                        from efc in efcd.DefaultIfEmpty()
                        where ccd.EventId == eventId
                        orderby ccd.ReqActHistId descending
                        select new
                        {
                            ccd.ReqActHistId,
                            ccd.EventId,
                            ccd.OldDt,
                            ccd.NewDt,
                            efc.FailCdDes,
                            ccd.EventHistId,
                            ccd.CreatDt,
                            efc.RspbPartyNme
                        });

            dtFedlineCCDHistory = LinqHelper.CopyToDataTable(ccdh, null, null);
            return dtFedlineCCDHistory;
        }

        public DataTable GetFedlineEventFailCodes(int eventRuleId)
        {
            DataTable dtFedlineFailCodes = new DataTable();
            dtFedlineFailCodes.Columns.Add(new DataColumn("FAIL_CD_ID"));
            dtFedlineFailCodes.Columns.Add(new DataColumn("FAIL_CD_DESC"));

            var failCodes = (from frf in _context.LkFedRuleFail
                             join efc in _context.LkFedlineEventFailCd on frf.FailCdId equals efc.FailCdId into efcd
                             from efc in efcd.DefaultIfEmpty()
                             where frf.EventRuleId == eventRuleId
                             select new
                             {
                                 efc.FailCdId,
                                 efc.FailCdDes
                             });

            dtFedlineFailCodes = LinqHelper.CopyToDataTable(failCodes, null, null);
            return dtFedlineFailCodes;
        }

        public List<FedlineUserSearchEntity> SearchUsers(string input, int profileId)
        {
            var users = (from u in _context.LkUser
                         join mup in _context.MapUsrPrf on u.UserId equals mup.UserId
                         where mup.RecStusId == 1
                            && mup.UsrPrfId == profileId
                            && (u.FullNme.Contains(input) || u.DsplNme.Contains(input))
                            && u.UserId != 1
                         orderby u.DsplNme
                         select new FedlineUserSearchEntity
                         {
                             UserID = u.UserId,
                             DisplayName = u.DsplNme,
                             Email = u.EmailAdr
                         });

            return users.ToList();
        }
    }
}