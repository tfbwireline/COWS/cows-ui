﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CCDBypassRepository : ICCDBypassRepository
    {
        private readonly COWSAdminDBContext _context;

        public CCDBypassRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public CCDBypassUserView Create(CCDBypassUserView entity)
        {
            throw new NotImplementedException();
        }

        public int CreateCCD(CCDBypassUserView obj)
        {
            var exists = (from c in _context.LkSysCfg
                          where c.PrmtrNme == "menuCCD" && c.PrmtrValuTxt == obj.UserADID
                          select c.CfgId);

            if (exists.Count() > 0)
                return exists.First();
            LkSysCfg entity = new LkSysCfg();
            //int maxId = _context.LkSysCfg.Max(i => i.CfgId);
            //entity.CfgId = (int)++maxId;
            entity.PrmtrNme = "menuCCD";
            entity.PrmtrValuTxt = obj.UserADID;
            entity.RecStusId = 1;
            entity.CreatDt = DateTime.Now;
            _context.LkSysCfg.Add(entity);

            SaveAll();

            return entity.CfgId;
        }

        public void Delete(int id)
        {
            LkSysCfg cfg = GetBySYSCFGId(id);
            _context.LkSysCfg.Remove(cfg);

            SaveAll();
        }

        public IQueryable<CCDBypassUserView> Find(Expression<Func<CCDBypassUserView, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CCDBypassUserView> GetAll()
        {
            throw new NotImplementedException();
        }

        public CCDBypassUserView GetById(int id)
        {
            throw new NotImplementedException();
        }

        public LkSysCfg GetBySYSCFGId(int id)
        {
            return _context.LkSysCfg
                           .SingleOrDefault(i => i.CfgId == id && i.PrmtrNme == "menuCCD");
        }

        public int SaveAll()
        {
            //throw new NotImplementedException();
            return _context.SaveChanges();
        }

        public void Update(int id, CCDBypassUserView entity)
        {
            throw new NotImplementedException();
        }

        public List<LkUser> FindUser(string searchString, int userId)
        {
            List<LkUser> users = new List<LkUser>();

            // check user if already in list
            LkUser user = new LkUser();
            var currentUsers = GetBypassUserList(userId).Where(x => x.UserADID == searchString);
            if (currentUsers.Count() > 0)
            {        
                user.UserAdid = "exist";
                users.Add(user);

                return users;
            }

            List<short> currentUserGroups = (from mup in _context.MapUsrPrf
                                             join lph in _context.LkPrfHrchy on mup.UsrPrfId equals lph.ChldPrfId
                                             where mup.UserId == userId && mup.RecStusId == (byte)1
                                             select lph.PrntPrfId).Distinct().ToList();


            List<string> exists = (from c in _context.LkSysCfg
                                   where c.PrmtrNme == "menuCCD"
                                   select c.PrmtrValuTxt).Distinct().ToList();
            users = (from lu in _context.LkUser
                      join ugr in _context.MapUsrPrf on lu.UserId equals ugr.UserId
                      where (lu.DsplNme.Contains(searchString) || lu.FullNme.Contains(searchString) || lu.UserAdid.Contains(searchString))
                         && currentUserGroups.Contains(ugr.UsrPrfId)
                         && !exists.Contains(lu.UserAdid)
                         && lu.UserId != 1
                         && ugr.RecStusId == (byte)1
                         && lu.RecStusId == (byte)1
                         && lu.UserId != userId
                      select new LkUser
                      {
                          FullNme = lu.DsplNme,
                          UserAdid = lu.UserAdid,
                      }).Distinct().ToList();
            //List<CCDBypassUserView> list = rl.ToList();

            // Check if searchString adid exist on valid records to add.
            var searchedUser = users.Where(x => x.UserAdid == searchString).FirstOrDefault();
            if (searchedUser != null)
            {
                var ccd = new CCDBypassUserView();
                ccd.ConfigID = 0;
                ccd.UserADID = searchedUser.UserAdid;
                ccd.UserName = searchedUser.DsplNme;

                if (CreateCCD(ccd) > 0)
                {
                    users = new List<LkUser>();

                    user.UserAdid = "expressCreate";
                    user.FullNme = searchedUser.DsplNme;
                    users.Add(user);
                    return users;
                }
            }

            return users;
        }

        public List<CCDBypassUserView> GetBypassUserList(int userId)
        {
            List<CCDBypassUserView> records;
            List<short> currentUserGroups = (from mup in _context.MapUsrPrf
                                             join lph in _context.LkPrfHrchy on mup.UsrPrfId equals lph.ChldPrfId
                                             where mup.UserId == userId && mup.RecStusId == (byte)1
                                             select lph.PrntPrfId).Distinct().ToList();

            records = (from cfg in _context.LkSysCfg
                      join lu in _context.LkUser on cfg.PrmtrValuTxt equals lu.UserAdid
                      join mup in _context.MapUsrPrf on lu.UserId equals mup.UserId
                      join lph in _context.LkPrfHrchy on mup.UsrPrfId equals lph.ChldPrfId
                      where cfg.PrmtrNme == "menuCCD"
                         //&& lu.UserId != 1
                         && mup.RecStusId == (byte)ERecStatus.Active
                         && lu.RecStusId == (byte)ERecStatus.Active
                         && currentUserGroups.Contains(lph.PrntPrfId)
                      select new CCDBypassUserView
                      {
                          ConfigID = cfg.CfgId,
                          UserADID = lu.UserAdid,
                          UserName = lu.DsplNme
                      }).Distinct().ToList();
            return records;
        }

        public List<CCDHistory> GetCCDHistory(int orderID)
        {
            List<CCDHistory> toReturn = new List<CCDHistory>();

            var ccdh = (from ccd in _context.CcdHist
                        join n in _context.OrdrNte on ccd.NteId equals n.NteId
                        join u in _context.LkUser on ccd.CreatByUserId equals u.UserId
                        where ccd.OrdrId == orderID
                        orderby ccd.CcdHistId descending
                        select new
                        {
                            ccd.CcdHistId,
                            ccd.OrdrId,
                            ccd.NteId,
                            n.NteTxt,
                            ccd.OldCcdDt,
                            ccd.NewCcdDt,
                            ccd.SentToVndrDt,
                            ccd.AckByVndrDt,
                            ccd.CreatDt,
                            u.UserAdid,
                            u.DsplNme
                        });

            foreach (var c in ccdh)
            {
                CCDHistory hist = new CCDHistory();

                hist.ID = c.CcdHistId;
                hist.OrderID = c.OrdrId;
                if (c.NteId != null)
                    hist.NoteID = Convert.ToInt32(c.NteId);
                hist.Note = c.NteTxt;
                hist.CCDOld = c.OldCcdDt;
                hist.CCDNew = c.NewCcdDt;
                if (c.SentToVndrDt != null)
                    hist.VndrSent = Convert.ToDateTime(c.SentToVndrDt);
                if (c.AckByVndrDt != null)
                    hist.VndrAck = Convert.ToDateTime(c.AckByVndrDt);
                hist.Reasons = GetReason(hist.ID);
                hist.CreatedDateTime = c.CreatDt;
                hist.CreatedByDspNme = c.DsplNme;
                hist.CreatedByUserName = c.UserAdid;

                toReturn.Add(hist);
            }
            return toReturn;
        }

        private List<CCDReasons> GetReason(int historyID)
        {

            List<CCDReasons> toReturn = new List<CCDReasons>();

            var cr = from c in _context.CcdHistReas
                      join lk in _context.LkCcdMissdReas on c.CcdMissdReasId equals lk.CcdMissdReasId
                      where c.CcdHistId == historyID
                      select new { lk.CcdMissdReasId, lk.CcdMissdReasDes };

            foreach (var c in cr)
            {
                CCDReasons ccdReason = new CCDReasons();

                ccdReason.CCDMRID = c.CcdMissdReasId;
                ccdReason.CCDMRDes = c.CcdMissdReasDes;

                toReturn.Add(ccdReason);
            }
            return toReturn;
        }
    }
}