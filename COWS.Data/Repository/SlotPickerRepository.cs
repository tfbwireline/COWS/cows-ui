﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;

namespace COWS.Data.Repository
{
    public class SlotPickerRepository : ISlotPickerRepository
    {
        private readonly COWSAdminDBContext _context;
        private const int _INTERVAL = 10;
        private DataTable _dtApptRec;
        private DateTime _StartRange;
        private DateTime _EndRange;
        private readonly ICommonRepository _common;
        private readonly IConfiguration _configuration;

        public SlotPickerRepository(COWSAdminDBContext context, ICommonRepository common, IConfiguration configuration)
        {
            _context = context;
            _common = common;
            _dtApptRec = new DataTable("RecurringAppts");
            _dtApptRec.Columns.Add("APPT_ID");
            _dtApptRec.Columns.Add("StartDt", typeof(DateTime));
            _dtApptRec.Columns.Add("EndDt", typeof(DateTime));
            _dtApptRec.Columns.Add("ResourceID");
            _dtApptRec.Columns.Add("APPT_TYPE_ID");
            _configuration = configuration;
        }

        public IEnumerable<LkUser> GetEventActivator(string type, DynamicParameters param)
        {
            IEnumerable<LkUser> users = new List<LkUser>();

            // Base query
            IQueryable<LkUser> query = _context.LkUser
                .Include(a => a.MapUsrPrfUser)
                    .ThenInclude(a => a.UsrPrf)
                .Include(a => a.LkQlfctnAsnToUser)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnEventType)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnEnhncSrvc)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnIpVer)
                .Where(a =>
                    a.UserId != 1 &&
                    a.RecStusId == 1 &&
                    a.MapUsrPrfUser.Any(b =>
                        b.RecStusId == 1 &&
                        b.UsrPrf.RecStusId == 1 &&
                        EF.Functions.Like(b.UsrPrf.UsrPrfNme, "%EActivator%")) &&
                    a.LkQlfctnAsnToUser.RecStusId == 1);

            switch (type)
            {
                case "SCHEDULE":
                    var startDate = Convert.ToDateTime(param.Start.Value);
                    var endDate = Convert.ToDateTime(param.Start.Value).Date.AddDays(_INTERVAL).Add(new TimeSpan(23, 55, 00));

                    List<int> appointmentTypeIds = new List<int>();
                    appointmentTypeIds.Add((int)AppointmentType.OnShift); // Add OnShift

                    // Get all possible appointment type
                    appointmentTypeIds.AddRange(GetCurrentAppointmentTypeIds(param.EventTypeId, param.EnhanceServiceId));

                    users = query
                        .Include(a => a.ApptRcurncData)
                            .ThenInclude(a => a.Appt)
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.ApptRcurncData
                                .Any(b =>
                                    b.Appt.AsnToUserIdListTxt != null &&
                                    b.Appt.EventId != null &&
                                    appointmentTypeIds.Contains(b.Appt.ApptTypeId) &&
                                    b.Appt.RcurncDesTxt != null &&
                                    b.Appt.RcurncCd == true &&
                                    b.StrtTmst.Date >= startDate.Date.AddDays(-1) &&
                                    b.EndTmst.Date <= endDate.Date
                                )
                        );
                    break;

                case "SKILLED":
                    if (param.EventTypeId == (int)EventType.AD)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId)
                        );
                    }
                    else if (param.EventTypeId == (int)EventType.MPLS)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.LkQlfctnAsnToUser.QlfctnIpVer.Any(b => b.IpVerId == param.IpVersionId)
                        );
                    }
                    else if (param.EventTypeId == (int)EventType.SprintLink)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.LkQlfctnAsnToUser.QlfctnIpVer.Any(b => b.IpVerId == param.IpVersionId)
                        // Possibly add Event sprint Link Activity Type   ??
                        );
                    }
                    else
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId)
                        );
                    }
                    break;

                case "PRODUCT":
                    users = query
                        .Where(a => a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId));
                    break;

                default:
                    break;
            }

            return users;
        }

        public IEnumerable<LkUser> GetCheckSlots(string type, DynamicParameters param)
        {
            IEnumerable<LkUser> users = new List<LkUser>();

            // Base query
            IQueryable<LkUser> query = _context.LkUser
                .Include(a => a.MapUsrPrfUser)
                    .ThenInclude(a => a.UsrPrf)
                .Include(a => a.LkQlfctnAsnToUser)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnEventType)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnEnhncSrvc)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnIpVer)
                .Where(a =>
                    a.UserId != 1 &&
                    a.RecStusId == 1 &&
                    a.MapUsrPrfUser.Any(b =>
                        b.RecStusId == 1 &&
                        b.UsrPrf.RecStusId == 1 &&
                        EF.Functions.Like(b.UsrPrf.UsrPrfNme, "%EActivator%")) &&
                    a.LkQlfctnAsnToUser.RecStusId == 1);

            switch (type)
            {
                case "SCHEDULE":
                    var startDate = Convert.ToDateTime(param.Start.Value);
                    var endDate = Convert.ToDateTime(param.Start.Value).Date.AddDays(_INTERVAL).Add(new TimeSpan(23, 55, 00));

                    List<int> appointmentTypeIds = new List<int>();
                    appointmentTypeIds.Add((int)AppointmentType.OnShift); // Add OnShift

                    // Get all possible appointment type
                    appointmentTypeIds.AddRange(GetCurrentAppointmentTypeIds(param.EventTypeId, param.EnhanceServiceId));

                    users = query
                        .Include(a => a.ApptRcurncData)
                            .ThenInclude(a => a.Appt)
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.ApptRcurncData
                                .Any(b =>
                                    b.Appt.AsnToUserIdListTxt != null &&
                                    b.Appt.EventId != null &&
                                    appointmentTypeIds.Contains(b.Appt.ApptTypeId) &&
                                    b.Appt.RcurncDesTxt != null &&
                                    b.Appt.RcurncCd == true &&
                                    b.StrtTmst.Date >= startDate.Date.AddDays(-1) &&
                                    b.EndTmst.Date <= endDate.Date
                                )
                        );
                    break;

                case "SKILLED":
                    if (param.EventTypeId == (int)EventType.AD)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId)
                        );
                    }
                    else if (param.EventTypeId == (int)EventType.MPLS)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.LkQlfctnAsnToUser.QlfctnIpVer.Any(b => b.IpVerId == param.IpVersionId)
                        );
                    }
                    else if (param.EventTypeId == (int)EventType.SprintLink)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.LkQlfctnAsnToUser.QlfctnIpVer.Any(b => b.IpVerId == param.IpVersionId)
                        // Possibly add Event sprint Link Activity Type   ??
                        );
                    }
                    else
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId)
                        );
                    }
                    break;

                case "PRODUCT":
                    users = query
                        .Where(a => a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId));
                    break;

                default:
                    break;
            }
            return users;
        }

        public LkUsrPrf GetFinalUserProfile(int userId, int eventTypeId, bool IS_EVENT = true)
        {
            IQueryable<MapUsrPrf> mapUsrPrf = _context.MapUsrPrf
                .Where(a => a.RecStusId == 1 &&
                    a.UserId == userId);

            int profileId = 0;
            int[] profileIds = { 0, 0, 0 };
            if (IS_EVENT)
            {
                // Updated by Sarah [20190510] - Workflow status hierarchy will be Activator - Reviewer - Member
                // Get Profile ID for Event Member, Event Reviewer, Event Activator in order
                profileIds = GetProfileIdByEventTypeId(eventTypeId);

                // Can do it like this instead, but had problems with circular reference
                //if (mapUsrPrf.AsQueryable().Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EMember%") && a.UsrPrf.LkEventRule.Any(b => b.EventTypeId == eventTypeId)))
                //{
                //    profileId = profileIds[0]; // pick member elem of array
                //}
                //else if (mapUsrPrf.AsQueryable().Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EReviewer%") && a.UsrPrf.LkEventRule.Any(b => b.EventTypeId == eventTypeId)))
                //{
                //    profileId = profileIds[1]; // pick reviewer elem of array
                //}
                //else if (mapUsrPrf.AsQueryable().Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EActivator%") && a.UsrPrf.LkEventRule.Any(b => b.EventTypeId == eventTypeId)))
                //{
                //    profileId = profileIds[2]; // pick activator elem of array
                //}

                IQueryable<LkEventRule> query = _context.LkEventRule
                    .Where(a => a.EventTypeId == eventTypeId &&
                        mapUsrPrf.Select(b => b.UsrPrfId).Contains(a.UsrPrfId));

                // Updated by Sarah [20190510] - Workflow status hierarchy will be Activator - Reviewer - Member
                if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EActivator%")))
                {
                    profileId = profileIds[0];
                }
                else if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EReviewer%")))
                {
                    profileId = profileIds[1];
                }
                else if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EMember%")) || mapUsrPrf.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%SALESSUPPORT%")))
                {
                    profileId = profileIds[2];
                }

                //if (mapUsrPrf.AsQueryable().Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EMember%")))
                //{
                //    var userProfileIds = mapUsrPrf
                //        .AsQueryable()
                //        .Where(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EMember%"))
                //        .Select(a => a.UsrPrfId)
                //        .ToList();

                //    if (_context.LkEventRule.Any(a => userProfileIds.Contains(a.UsrPrfId) && a.EventTypeId == eventTypeId))
                //    {
                //        profileId = profileIds[0]; // pick member elem of array
                //    }
                //}
                //else if (mapUsrPrf.AsQueryable().Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EReviewer%")))
                //{
                //    var userProfileIds = mapUsrPrf
                //        .AsQueryable()
                //        .Where(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EReviewer%"))
                //        .Select(a => a.UsrPrfId)
                //        .ToList();

                //    if (_context.LkEventRule.Any(a => userProfileIds.Contains(a.UsrPrfId) && a.EventTypeId == eventTypeId))
                //    {
                //        profileId = profileIds[1]; // pick member elem of array
                //    }
                //}
                //else if (mapUsrPrf.AsQueryable().Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EActivator%")))
                //{
                //    var userProfileIds = mapUsrPrf
                //        .AsQueryable()
                //        .Where(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EActivator%"))
                //        .Select(a => a.UsrPrfId)
                //        .ToList();

                //    if (_context.LkEventRule.Any(a => userProfileIds.Contains(a.UsrPrfId) && a.EventTypeId == eventTypeId))
                //    {
                //        profileId = profileIds[2]; // pick member elem of array
                //    }
                //}

                //if (profileId == 0) // if user is under Sales Support
                //{
                //    // Sales Support is Event/Order Member only
                //    profileId = mapUsrPrf
                //        .AsQueryable()
                //        .Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%SALESSUPPORT%")) ? profileIds[0] : 0;
                //}
            }

            //return profileId;
            return _context.LkUsrPrf.Where(a => a.UsrPrfId == profileId).SingleOrDefault();
        }

        private List<int> GetCurrentAppointmentTypeIds(int eventTypeId, int enhanceServiceId)
        {
            List<int> result = new List<int>();

            // This is the basis for code
            //if (sAction.Length > 0)
            //{
            //    if (sAction.Equals("MDS") && sIsMDSFT.Equals("0"))
            //        lApptTypeID.Add(((int)ECalendarEventType.MDSScheduledShift));

            //    if (sAction.Equals("MDS") && sIsMDSFT.Equals("1"))
            //        lApptTypeID.Add(((int)ECalendarEventType.MDSScheduledShift));

            //    if (!sAction.Equals("MDS"))
            //    {
            //        if (iPrdID == ((int)EEventType.AD))
            //            lApptTypeID.Add(((int)ECalendarEventType.ADShift));

            //        if ((sAction.Equals("STANDARD")) || (sAction.Equals("VAS")))
            //            lApptTypeID.Add(((int)ECalendarEventType.MPLSShift));

            //        if (sAction.Equals("NGVN STANDARD"))
            //            lApptTypeID.Add(((int)ECalendarEventType.NGVNShift));

            //        if (sAction.Equals("SLNK STANDARD"))
            //            lApptTypeID.Add(((int)ECalendarEventType.SprintLinkShift));

            //        /*if (sAction.Equals("UCaaS"))
            //            lApptTypeID.Add(((int)ECalendarEventType.UCaaSShift));*/
            //    }
            //}
            //else
            //{
            //    if (iPrdID == 5)
            //        lApptTypeID.Add(((int)ECalendarEventType.MDSScheduledShift));
            //    else if (iPrdID == 4)
            //        lApptTypeID.Add(((int)ECalendarEventType.SprintLinkShift));
            //    else if (iPrdID == 3)
            //        lApptTypeID.Add(((int)ECalendarEventType.MPLSShift));
            //    else if (iPrdID == 2)
            //        lApptTypeID.Add(((int)ECalendarEventType.NGVNShift));
            //    else if (iPrdID == 1)
            //        lApptTypeID.Add(((int)ECalendarEventType.ADShift));
            //    //else if (iPrdID == 19)
            //    //lApptTypeID.Add(((int)ECalendarEventType.UCaaSShift));
            //}

            if (enhanceServiceId != 0)
            {
                if (enhanceServiceId != (int)EnhanceService.MDS) // NOT MDS
                {
                    if (eventTypeId == (int)EventType.AD) // AD Event
                    {
                        result.Add((int)AppointmentType.ADShift);
                    }
                    else if (eventTypeId == (int)EventType.MPLS) // MPLS Event
                    {
                        result.Add((int)AppointmentType.MPLSShift);
                    }
                    else if (eventTypeId == (int)EventType.SIPT) // SIPT Event
                    {
                        result.Add((int)AppointmentType.SIPT);
                    }
                    else if (eventTypeId == (int)EventType.SprintLink) // SprintLink Event
                    {
                        result.Add((int)AppointmentType.SprintLinkShift);
                    }
                    else if (eventTypeId == (int)EventType.NGVN) // NGVN Event
                    {
                        result.Add((int)AppointmentType.NGVNShift);
                    }
                }
            }

            return result;
        }

        private int[] GetProfileIdByEventTypeId(int eventTypeId)
        {
            // Updated by Sarah [20190510] - Workflow status hierarchy will be Activator - Reviewer - Member
            int[] profileIds = new int[3] { 0, 0, 0 }; // 1st = Activator, 2nd = Reviewer, 3rd = Member

            if (eventTypeId == (int)EventType.AD || eventTypeId == (int)EventType.SprintLink
                || eventTypeId == (int)EventType.MPLS || eventTypeId == (int)EventType.NGVN)
            {
                //profileIds = new int[3] { 33, 34, 32 };
                profileIds = new int[3] { 32, 34, 33 };
            }
            else if (eventTypeId == (int)EventType.SIPT || eventTypeId == (int)EventType.UCaaS)
            {
                //profileIds = new int[3] { 173, 174, 172 };
                profileIds = new int[3] { 172, 174, 173 };
            }

            return profileIds;
        }

        public DataTable GetAssignSkilledMembersforSubmitType(string sAction, string sIPVersion, string sSLNKEventType)
        {
            var x = (from ql in _context.LkQlfctn
                     join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
                     join le in _context.LkEnhncSrvc on qe.EnhncSrvcId equals le.EnhncSrvcId
                     join qi in _context.QlfctnIpVer on ql.QlfctnId equals qi.QlfctnId
                     into joinip
                     from qi in joinip.DefaultIfEmpty()
                     join lu in _context.LkUser on ql.AsnToUserId equals lu.UserId
                     select new { lu, le, qi });

            if (sAction.Equals("STANDARD") || sAction.Equals("VAS") || sAction.Equals("SLNK STANDARD"))
            {
                x = x.Where(a => a.le.EnhncSrvcNme == sAction);
                x = x.Where(a => a.qi.IpVerId == Convert.ToByte(sIPVersion));
            }
            else
                x = x.Where(a => a.le.EnhncSrvcNme == sAction);

            var y = x.Select(i => new { USER_ID = i.lu.UserId, USER_ADID = i.lu.UserAdid, DSPL_NME = i.lu.DsplNme, FULL_NME = i.lu.FullNme }).Distinct();

            return LinqHelper.CopyToDataTable(y, null, null);
        }

        public DataTable getUsersByProduct(int iProductID)
        {
            var up = (from u in _context.LkUser
                      join q in _context.LkQlfctn on u.UserId equals q.AsnToUserId
                      join qe in _context.QlfctnEventType on q.QlfctnId equals qe.QlfctnId
                      where ((q.RecStusId == (byte)ERecStatus.Active) && (u.RecStusId == (byte)ERecStatus.Active)) && (qe.EventTypeId == iProductID)
                            && (u.UserId != 1)
                      select new { USER_ID = u.UserId, USER_ADID = u.UserAdid, FULL_NME = u.FullNme, DSPL_NME = u.DsplNme }).AsNoTracking().Distinct();

            return LinqHelper.CopyToDataTable(up, null, null);
        }

        public DataTable getUsersByPrdProfile(DataTable dtPrd, DataTable dtProfile)
        {
            var q = (from up in dtPrd.AsEnumerable()
                     join ur in dtProfile.AsEnumerable() on Convert.ToInt32(up.Field<Object>("USER_ID").ToString()) equals Convert.ToInt32(ur.Field<Object>("USER_ID").ToString())
                     select new
                     {
                         USER_ID = Convert.ToString(up.Field<Object>("USER_ID").ToString()),
                         USER_ADID = Convert.ToString(up.Field<Object>("USER_ADID").ToString()),
                         FULL_NME = Convert.ToString(up.Field<Object>("FULL_NME").ToString()),
                         DSPL_NME = Convert.ToString(up.Field<Object>("DSPL_NME").ToString())
                     }).Distinct();

            return LinqHelper.CopyToDataTable(q, null, null);
        }

        public DataTable GetUsersByProfile(string profileName)
        {
            var users = (from lu in _context.LkUser
                         join mup in _context.MapUsrPrf on lu.UserId equals mup.UserId
                         join lup in _context.LkUsrPrf on mup.UsrPrfId equals lup.UsrPrfId
                         where lu.RecStusId == 1 && lu.UserId != 1
                         && lup.RecStusId == 1 && lup.UsrPrfDes.Contains(profileName)
                         && mup.RecStusId == 1
                         select new
                         {
                             USER_ID = lu.UserId,
                             USER_ADID = lu.UserAdid,
                             FULL_NME = lu.FullNme,
                             DSPL_NME = lu.DsplNme
                         }).Distinct();

            return LinqHelper.CopyToDataTable(users, null, null);
        }

        public DataSet GetWFCollection(ref DataTable dtSkilledMem, DateTime dcStart, DateTime dcEnd, string sAction, string sIsMDSFT, int iPrdID)
        {
            _dtApptRec.Clear();
            _StartRange = dcStart;
            _EndRange = dcEnd;

            List<int> lApptTypeID = new List<int>();

            lApptTypeID.Add(((int)CalendarEventType.OnShift));

            if (sAction != null && sAction.Length > 0)
            {
                if (sAction.Equals("MDS") && sIsMDSFT.Equals("0"))
                    lApptTypeID.Add(((int)CalendarEventType.MDSScheduledShift));

                if (sAction.Equals("MDS") && sIsMDSFT.Equals("1"))
                    lApptTypeID.Add(((int)CalendarEventType.MDSScheduledShift));

                if (!sAction.Equals("MDS"))
                {
                    if (iPrdID == ((int)EventType.AD))
                        lApptTypeID.Add(((int)CalendarEventType.ADShift));

                    if ((sAction.Equals("STANDARD")) || (sAction.Equals("VAS")))
                        lApptTypeID.Add(((int)CalendarEventType.MPLSShift));

                    if (sAction.Equals("NGVN STANDARD"))
                        lApptTypeID.Add(((int)CalendarEventType.NGVNShift));

                    if (sAction.Equals("SLNK STANDARD"))
                        lApptTypeID.Add(((int)CalendarEventType.SprintLinkShift));

                    /*if (sAction.Equals("UCaaS"))
                        lApptTypeID.Add(((int)ECalendarEventType.UCaaSShift));*/
                }
            }
            else
            {
                if (iPrdID == 5)
                    lApptTypeID.Add(((int)CalendarEventType.MDSScheduledShift));
                else if (iPrdID == 4)
                    lApptTypeID.Add(((int)CalendarEventType.SprintLinkShift));
                else if (iPrdID == 3)
                    lApptTypeID.Add(((int)CalendarEventType.MPLSShift));
                else if (iPrdID == 2)
                    lApptTypeID.Add(((int)CalendarEventType.NGVNShift));
                else if (iPrdID == 1)
                    lApptTypeID.Add(((int)CalendarEventType.ADShift));
                //else if (iPrdID == 19)
                //lApptTypeID.Add(((int)ECalendarEventType.UCaaSShift));
            }

            DataSet ds = new DataSet();

            DataTable dtWFCollection = new DataTable("WFCollection");
            dtWFCollection.Columns.Add("StartDt", typeof(DateTime));
            dtWFCollection.Columns.Add("EndDt", typeof(DateTime));
            dtWFCollection.Columns.Add("USER_ID");
            dtWFCollection.Columns.Add("USER_ADID");
            dtWFCollection.Columns.Add("DSPL_NME");
            dtWFCollection.Columns.Add("FULL_NME");

            DataTable dtScheduleCollection = new DataTable("ScheduleCollection");
            dtScheduleCollection.Columns.Add("USER_ID");
            dtScheduleCollection.Columns.Add("USER_ADID");
            dtScheduleCollection.Columns.Add("DSPL_NME");
            dtScheduleCollection.Columns.Add("FULL_NME");

            // Get all Recurring Appointments into temp datatable
            DataTable dt = new DataTable();

            var adev = (from apd in _context.ApptRcurncData
                        join apt in _context.Appt on apd.ApptId equals apt.ApptId
                        where (((apt.AsnToUserIdListTxt != null) && (apt.EventId == null) && (lApptTypeID.Contains(apt.ApptTypeId)) && (apt.RcurncDesTxt != null) && (apt.RcurncCd == true))
                         && ((_StartRange.Date.AddDays(-1) <= apd.StrtTmst.Date) && (apd.EndTmst.Date <= _EndRange.Date)))
                        select new
                        {
                            APPT_ID = apt.ApptId,
                            StartDt = apd.StrtTmst,
                            EndDt = apd.EndTmst,
                            ResourceID = apd.AsnUserId,
                            CREAT_DT = apt.CreatDt,
                            MODFD_DT = apt.ModfdDt,
                            APPT_TYPE_ID = apt.ApptTypeId
                        });

            DataTable dtadev = LinqHelper.CopyToDataTable(adev, null, null);

            var filar = (from ap in dtadev.AsEnumerable()
                         join sk in dtSkilledMem.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sk.Field<Object>("USER_ID").ToString())
                         where ((_StartRange.Date.AddDays(-1) <= DateTime.Parse(ap.Field<Object>("StartDt").ToString()).Date) && (DateTime.Parse(ap.Field<Object>("EndDt").ToString()).Date <= _EndRange.Date))
                         select new
                         {
                             APPT_ID = Convert.ToString(ap.Field<Object>("APPT_ID").ToString()),
                             StartDt = Convert.ToDateTime(ap.Field<Object>("StartDt").ToString()),
                             EndDt = Convert.ToDateTime(ap.Field<Object>("EndDt").ToString()),
                             ResourceID = Convert.ToString(ap.Field<Object>("ResourceID").ToString()),
                             APPT_TYPE_ID = Convert.ToString(ap.Field<Object>("APPT_TYPE_ID").ToString())
                         }
                    ).Distinct();

            _dtApptRec = LinqHelper.CopyToDataTable(filar, null, null);

            var GrpSt = (from ar1 in _dtApptRec.AsEnumerable()
                         join ar2 in _dtApptRec.AsEnumerable() on ar1.Field<Object>("ResourceID").ToString() equals ar2.Field<Object>("ResourceID").ToString()
                         where DateTime.Parse(ar1.Field<Object>("StartDt").ToString()) == DateTime.Parse(ar2.Field<Object>("EndDt").ToString())
                         select new { ar1, ar2 })
                     .ToList();

            var GrpEt = (from ar1 in _dtApptRec.AsEnumerable()
                         join ar2 in _dtApptRec.AsEnumerable() on ar1.Field<Object>("ResourceID").ToString() equals ar2.Field<Object>("ResourceID").ToString()
                         where DateTime.Parse(ar1.Field<Object>("EndDt").ToString()) == DateTime.Parse(ar2.Field<Object>("StartDt").ToString())
                         select new { ar1, ar2 })
                         .ToList();

            if (((GrpSt != null) && (GrpSt.Count > 0))
                || ((GrpEt != null) && (GrpEt.Count > 0)))
                ConsolidateAdjacentSlots(ref _dtApptRec);

            // Get all Non-Recurring Appointments into temp datatable
            var x = (from ra in _context.Appt
                     where ((ra.AsnToUserIdListTxt != null) && (ra.RcurncCd == false) && (ra.EventId == null))
                       && (lApptTypeID.Contains(ra.ApptTypeId))
                       && (_StartRange.Date.AddDays(-1) <= ra.StrtTmst.Date)
                     select new
                     {
                         APPT_ID = ra.ApptId,
                         StartDt = ra.StrtTmst,
                         EndDt = ra.EndTmst,
                         ResourceID = XmlElementExtensions.GetAttribute<List<string>>(XElement.Parse(ra.AsnToUserIdListTxt).Descendants("ResourceId"), "Value", new List<string>()),
                         CREAT_DT = ra.CreatDt,
                         MODFD_DT = ra.ModfdDt,
                         APPT_TYPE_ID = ra.ApptTypeId
                     });

            DataTable dtx = LinqHelper.CopyToDataTable(x, null, null);

            var filnonrecar = (from ap in dtx.AsEnumerable()
                               join sk in dtSkilledMem.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sk.Field<Object>("USER_ID").ToString())
                               select new
                               {
                                   APPT_ID = Convert.ToString(ap.Field<Object>("APPT_ID").ToString()),
                                   StartDt = Convert.ToString(ap.Field<Object>("StartDt").ToString()),
                                   EndDt = Convert.ToString(ap.Field<Object>("EndDt").ToString()),
                                   ResourceID = Convert.ToString(ap.Field<Object>("ResourceID").ToString()),
                                   APPT_TYPE_ID = Convert.ToString(ap.Field<Object>("APPT_TYPE_ID").ToString())
                               }
                    ).Distinct();

            dt = LinqHelper.CopyToDataTable(filnonrecar, null, null);

            _dtApptRec.AsEnumerable().CopyToDataTable(dt, LoadOption.PreserveChanges);

            var a = (from ci in dt.AsEnumerable()
                     join sk in dtSkilledMem.AsEnumerable() on Convert.ToString(ci.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sk.Field<Object>("USER_ID").ToString())
                     where ((dcStart.Date <= DateTime.Parse(ci.Field<Object>("StartDt").ToString()).Date)
                          && (DateTime.Parse(ci.Field<Object>("StartDt").ToString()).Date <= dcEnd.Date)
                          && (DateTime.Parse(ci.Field<Object>("EndDt").ToString()).Date <= dcEnd.Date)
                          && (DateTime.Parse(ci.Field<Object>("StartDt").ToString()).Date <= DateTime.Parse(ci.Field<Object>("EndDt").ToString()).Date))
                     select new
                     {
                         USER_ID = Convert.ToString(sk.Field<Object>("USER_ID").ToString()),
                         USER_ADID = Convert.ToString(sk.Field<Object>("USER_ADID").ToString()),
                         DSPL_NME = Convert.ToString(sk.Field<Object>("DSPL_NME").ToString()),
                         FULL_NME = Convert.ToString(sk.Field<Object>("FULL_NME").ToString()),
                         StartDt = Convert.ToString(ci.Field<Object>("StartDt").ToString()),
                         EndDt = Convert.ToString(ci.Field<Object>("EndDt").ToString())
                     }).Distinct();

            DataTable dtWF = LinqHelper.CopyToDataTable(a, null, null);

            if (dcStart <= dcEnd)
            {
                foreach (DataRow dr in dtWF.Rows)
                {
                    dtWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(dr["StartDt"]), Convert.ToDateTime(dr["EndDt"]), dr["USER_ID"], dr["USER_ADID"], dr["DSPL_NME"], dr["FULL_NME"] });
                    dtScheduleCollection.Rows.Add(new Object[] { dr["USER_ID"], dr["USER_ADID"], dr["DSPL_NME"], dr["FULL_NME"] });
                }
            }

            var c1 = (from fc in dtWFCollection.AsEnumerable()
                      select new
                      {
                          StartDt = DateTime.Parse(fc.Field<Object>("StartDt").ToString()),
                          EndDt = DateTime.Parse(fc.Field<Object>("EndDt").ToString()),
                          USER_ID = Int32.Parse(fc.Field<Object>("USER_ID").ToString()),
                          USER_ADID = Convert.ToString(fc.Field<Object>("USER_ADID").ToString()),
                          DSPL_NME = Convert.ToString(fc.Field<Object>("DSPL_NME").ToString()),
                          FULL_NME = Convert.ToString(fc.Field<Object>("FULL_NME").ToString())
                      }).Distinct().OrderBy(w => w.USER_ID).ThenBy(v => v.StartDt);

            dtWFCollection = LinqHelper.CopyToDataTable(c1, null, null);

            var y = (from fc in dtScheduleCollection.AsEnumerable()
                     select new
                     {
                         USER_ID = Convert.ToString(fc.Field<Object>("USER_ID").ToString()),
                         USER_ADID = Convert.ToString(fc.Field<Object>("USER_ADID").ToString()),
                         DSPL_NME = Convert.ToString(fc.Field<Object>("DSPL_NME").ToString()),
                         FULL_NME = Convert.ToString(fc.Field<Object>("FULL_NME").ToString())
                     }).Distinct();

            dtScheduleCollection = LinqHelper.CopyToDataTable(y, null, null);

            ds.Tables.Add(dtWFCollection);
            ds.Tables.Add(dtScheduleCollection);

            return ds;
        }

        protected void ConsolidateAdjacentSlots(ref DataTable dtIn)
        {
            DataTable dtTempWFCollection = new DataTable("dtTempWF");
            DataColumn[] twfkeys = new DataColumn[5];

            twfkeys[0] = new DataColumn("APPT_ID");
            twfkeys[1] = new DataColumn("StartDt", typeof(DateTime));
            twfkeys[2] = new DataColumn("EndDt", typeof(DateTime));
            twfkeys[3] = new DataColumn("ResourceID");
            twfkeys[4] = new DataColumn("APPT_TYPE_ID");
            dtTempWFCollection.Columns.Add(twfkeys[0]);
            dtTempWFCollection.Columns.Add(twfkeys[1]);
            dtTempWFCollection.Columns.Add(twfkeys[2]);
            dtTempWFCollection.Columns.Add(twfkeys[3]);
            dtTempWFCollection.Columns.Add(twfkeys[4]);
            dtTempWFCollection.PrimaryKey = twfkeys;

            DataTable dtWFCol = new DataTable("WFbusyCollection");
            DataColumn[] wfkeys = new DataColumn[5];

            wfkeys[0] = new DataColumn("APPT_ID");
            wfkeys[1] = new DataColumn("StartDt", typeof(DateTime));
            wfkeys[2] = new DataColumn("EndDt", typeof(DateTime));
            wfkeys[3] = new DataColumn("ResourceID");
            wfkeys[4] = new DataColumn("APPT_TYPE_ID");
            dtWFCol.Columns.Add(wfkeys[0]);
            dtWFCol.Columns.Add(wfkeys[1]);
            dtWFCol.Columns.Add(wfkeys[2]);
            dtWFCol.Columns.Add(wfkeys[3]);
            dtWFCol.Columns.Add(wfkeys[4]);
            dtWFCol.PrimaryKey = wfkeys;

            foreach (DataRow dr in dtIn.Rows)
            {
                DataRow[] drPrefix = dtWFCol.Select("EndDt = #" + dr[1].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'");
                DataRow[] drSuffix = dtWFCol.Select("StartDt = #" + dr[2].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'");

                if (drPrefix.Length == 1)
                {
                    if (dtTempWFCollection.Select("StartDt = #" + drPrefix[0][1].ToString() + "# AND EndDt = #" + drPrefix[0][2].ToString() + "# AND ResourceID = '" + drPrefix[0][3].ToString() + "'").Length <= 0)
                        dtTempWFCollection.Rows.Add(new Object[] { drPrefix[0][0], Convert.ToDateTime(drPrefix[0][1]), Convert.ToDateTime(drPrefix[0][2]), drPrefix[0][3], drPrefix[0][4] });
                    if (dtWFCol.Select("StartDt = #" + drPrefix[0]["StartDt"].ToString() + "# AND EndDt = #" + dr[2].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'").Length <= 0)
                        dtWFCol.Rows.Add(new Object[] { dr[0], Convert.ToDateTime(drPrefix[0]["StartDt"].ToString()), Convert.ToDateTime(dr[2]), dr[3], dr[4] });
                }
                else if (drSuffix.Length == 1)
                {
                    if (dtTempWFCollection.Select("StartDt = #" + drSuffix[0][1].ToString() + "# AND EndDt = #" + drSuffix[0][2].ToString() + "# AND ResourceID = '" + drSuffix[0][3].ToString() + "'").Length <= 0)
                        dtTempWFCollection.Rows.Add(new Object[] { drSuffix[0][0], Convert.ToDateTime(drSuffix[0][1]), Convert.ToDateTime(drSuffix[0][2]), drSuffix[0][3], drSuffix[0][4] });
                    if (dtWFCol.Select("StartDt = #" + dr[1].ToString() + "# AND EndDt = #" + drSuffix[0]["EndDt"].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'").Length <= 0)
                        dtWFCol.Rows.Add(new Object[] { dr[0], Convert.ToDateTime(dr[1]), Convert.ToDateTime(drSuffix[0]["EndDt"]), dr[3], dr[4] });
                }
                else if (dtWFCol.Select("StartDt = #" + dr[1].ToString() + "# AND EndDt = #" + dr[2].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'").Length <= 0)
                    dtWFCol.Rows.Add(new Object[] { dr[0], Convert.ToDateTime(dr[1]), Convert.ToDateTime(dr[2]), dr[3], dr[4] });
            }

            if ((dtTempWFCollection != null) && (dtTempWFCollection.Rows.Count > 0))
            {
                foreach (DataRow dr in dtTempWFCollection.Rows)
                {
                    dtWFCol.Rows.Remove(dtWFCol.Rows.Find(new Object[] { dr["APPT_ID"], dr["StartDt"], dr["EndDt"], dr["ResourceID"], dr["APPT_TYPE_ID"] }));
                }
            }

            if ((dtWFCol != null) && (dtWFCol.Rows.Count > 0))
            {
                _dtApptRec.Clear();
                foreach (DataRow dr in dtWFCol.Rows)
                {
                    _dtApptRec.Rows.Add(new Object[] { dr[0], Convert.ToDateTime(dr[1]), Convert.ToDateTime(dr[2]), dr[3], dr[4] });
                }
            }
            dtTempWFCollection.Clear();
        }

        public DataTable GetFreeBusyCollection(ref DataTable dtScheduleMember, DateTime BusyStart, DateTime DCEnd, DateTime OrigStDt, int iEventID)
        {
            DataTable dtBusyCollection = new DataTable("BusyCollection");
            dtBusyCollection.Columns.Add("StrtTmst", typeof(DateTime));
            dtBusyCollection.Columns.Add("EndTmst", typeof(DateTime));
            dtBusyCollection.Columns.Add("USER_ID");
            dtBusyCollection.Columns.Add("USER_ADID");
            dtBusyCollection.Columns.Add("DSPL_NME");
            dtBusyCollection.Columns.Add("APPT_TYPE_ID");

            _StartRange = BusyStart;
            _EndRange = DCEnd;

            _dtApptRec.Clear();

            var x = (from ra in _context.Appt
                     where ((ra.AsnToUserIdListTxt != null) && (ra.EventId != null) && (ra.RecStusId == 1))
                     && ((BusyStart <= ra.StrtTmst) && (DCEnd >= ra.EndTmst))
                     select new
                     {
                         APPT_ID = ra.ApptId,
                         ResourceID = XmlElementExtensions.GetAttribute<List<string>>(XElement.Parse(ra.AsnToUserIdListTxt).Descendants("ResourceId"), "Value", new List<string>()),
                         StrtTmst = ra.StrtTmst,
                         EndTmst = ra.EndTmst,
                         EVENT_ID = ra.EventId,
                         CREAT_DT = ra.CreatDt,
                         MODFD_DT = ra.ModfdDt,
                         APPT_TYPE_ID = ra.ApptTypeId
                     });

            DataTable dtx = LinqHelper.CopyToDataTable(x, null, null);

            var filterappt = (from aptevrec in dtx.AsEnumerable()
                              where (Convert.ToInt32(aptevrec.Field<Object>("EVENT_ID").ToString()) != iEventID)
                              select new
                              {
                                  APPT_ID = Convert.ToString(aptevrec.Field<Object>("APPT_ID").ToString()),
                                  ResourceID = Convert.ToString(aptevrec.Field<Object>("ResourceID").ToString()),
                                  StrtTmst = DateTime.Parse(aptevrec.Field<Object>("StrtTmst").ToString()),
                                  EndTmst = DateTime.Parse(aptevrec.Field<Object>("EndTmst").ToString()),
                                  EVENT_ID = Convert.ToString(aptevrec.Field<Object>("EVENT_ID").ToString()),
                                  APPT_TYPE_ID = Convert.ToString(aptevrec.Field<Object>("APPT_TYPE_ID").ToString())
                              });

            DataTable dtAppt = LinqHelper.CopyToDataTable(filterappt, null, null);            

            var newmdsev = (from md in _context.MdsEvent
                            where (((md.EventStusId == ((byte)EventStatus.OnHold)) || (md.EventStusId == ((byte)EventStatus.OnHoldUAT)) || (md.EventStusId == ((byte)EventStatus.InProgress)) || (md.EventStusId == ((byte)EventStatus.Pending)) || (md.EventStusId == ((byte)EventStatus.Published)) || (md.EventStusId == ((byte)EventStatus.Fulfilling)) || (md.EventStusId == ((byte)EventStatus.Shipped)))
                                   || ((md.EventStusId == ((byte)EventStatus.Rework))
                                       && ((md.WrkflwStusId == ((byte)WorkflowStatus.Reschedule))
                                           || (md.WrkflwStusId == ((byte)WorkflowStatus.Retract))
                                           || (md.WrkflwStusId == ((byte)WorkflowStatus.Return)))))
                               && ((BusyStart <= md.StrtTmst) && (DCEnd >= md.EndTmst))
                               && (!md.SoftAssignCd)
                            select new { md.EventId, md.CreatDt, md.ModfdDt });

            DataTable dtnewmdsev = LinqHelper.CopyToDataTable(newmdsev, null, null);

            var newmdsEvent = (from ap in dtAppt.AsEnumerable()
                               join sm in dtScheduleMember.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sm.Field<Object>("USER_ID").ToString())
                               join md in dtnewmdsev.AsEnumerable() on Convert.ToInt32(ap.Field<Object>("EVENT_ID").ToString()) equals Convert.ToInt32(md.Field<Object>("EventId").ToString())
                               where (BusyStart <= DateTime.Parse(ap.Field<Object>("StrtTmst").ToString())) && (DCEnd >= DateTime.Parse(ap.Field<Object>("EndTmst").ToString()))
                               select new
                               {
                                   StrtTmst = DateTime.Parse(ap.Field<Object>("StrtTmst").ToString()),
                                   EndTmst = DateTime.Parse(ap.Field<Object>("EndTmst").ToString()),
                                   USER_ID = Convert.ToString(sm.Field<Object>("USER_ID").ToString()),
                                   USER_ADID = Convert.ToString(sm.Field<Object>("USER_ADID").ToString()),
                                   DSPL_NME = Convert.ToString(sm.Field<Object>("DSPL_NME").ToString()),
                                   APPT_TYPE_ID = Convert.ToString(ap.Field<Object>("APPT_TYPE_ID").ToString())
                               }
                     ).Distinct();

            /*var mvsev = (from mv in SlPiDBCntxt.UCaaS_EVENT
                         where (((mv.EVENT_STUS_ID == ((byte)EEventStatus.InProgress)) || (mv.EVENT_STUS_ID == ((byte)EEventStatus.Pending)) || (mv.EVENT_STUS_ID == ((byte)EEventStatus.Published)))
                                || ((mv.EVENT_STUS_ID == ((byte)EEventStatus.Rework))
                                    && ((mv.WRKFLW_STUS_ID == ((byte)EEventWorkFlowStatus.Reschedule))
                                        || (mv.WRKFLW_STUS_ID == ((byte)EEventWorkFlowStatus.Retract))
                                        || (mv.WRKFLW_STUS_ID == ((byte)EEventWorkFlowStatus.Return)))))
                            && ((BusyStart <= mv.STRT_TMST) && (DCEnd >= mv.END_TMST))
                         select new { mv.EVENT_ID, mv.CREAT_DT, mv.MODFD_DT });

            WriteDBLog("SlotPickerDB::GetFreeBusyCollection() - mvsev_Provider : " + mvsev.ToString() + " ; Expression : " + mvsev.Expression.ToString());

            DataTable dtmvsev = LinqHelper.CopyToDataTable(mvsev, null, null);

            var mvsEvent = (from ap in dtAppt.AsEnumerable()
                            join sm in dtScheduleMember.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sm.Field<Object>("USER_ID").ToString())
                            join mv in dtmvsev.AsEnumerable() on Convert.ToInt32(ap.Field<Object>("EVENT_ID").ToString()) equals Convert.ToInt32(mv.Field<Object>("EVENT_ID").ToString())
                            where (BusyStart <= DateTime.Parse(ap.Field<Object>("StrtTmst").ToString())) && (DCEnd >= DateTime.Parse(ap.Field<Object>("EndTmst").ToString()))
                            select new
                            {
                                StrtTmst = DateTime.Parse(ap.Field<Object>("StrtTmst").ToString()),
                                EndTmst = DateTime.Parse(ap.Field<Object>("EndTmst").ToString()),
                                USER_ID = Convert.ToString(sm.Field<Object>("USER_ID").ToString()),
                                USER_ADID = Convert.ToString(sm.Field<Object>("USER_ADID").ToString()),
                                DSPL_NME = Convert.ToString(sm.Field<Object>("DSPL_NME").ToString())
                            }
                     ).Distinct();*/

            var x2 = (from apd in _context.ApptRcurncData
                      join apt in _context.Appt on apd.ApptId equals apt.ApptId
                      where ((apt.AsnToUserIdListTxt != null) && (apt.EventId == null) && ((apt.ApptTypeId > 16) || (apt.ApptTypeId < 9))
                      && (apt.ApptTypeId != 31) && (apt.ApptTypeId != 33) 
                      && (apt.RcurncDesTxt != null) && (apt.RcurncCd == true))
                      && ((_StartRange.Date.AddDays(-1) <= apd.StrtTmst.Date)
                           && (apd.EndTmst.Date <= _EndRange.Date))
                      select new
                      {
                          APPT_ID = apt.ApptId,
                          StartDt = apd.StrtTmst,
                          EndDt = apd.EndTmst,
                          ResourceID = apd.AsnUserId,
                          CREAT_DT = apt.CreatDt,
                          MODFD_DT = apt.ModfdDt,
                          APPT_TYPE_ID = apt.ApptTypeId
                      });

            DataTable dtx2 = LinqHelper.CopyToDataTable(x2, null, null);

            var FilterNonEventRec = (from ap in dtx2.AsEnumerable()
                                     join sm in dtScheduleMember.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sm.Field<Object>("USER_ID").ToString())
                                     where ((_StartRange.Date.AddDays(-1) <= DateTime.Parse(ap.Field<Object>("StartDt").ToString()).Date)
                                        && (DateTime.Parse(ap.Field<Object>("EndDt").ToString()).Date <= _EndRange.Date))
                                     select new
                                     {
                                         APPT_ID = Convert.ToString(ap.Field<Object>("APPT_ID").ToString()),
                                         StartDt = Convert.ToDateTime(ap.Field<Object>("StartDt").ToString()),
                                         EndDt = Convert.ToDateTime(ap.Field<Object>("EndDt").ToString()),
                                         ResourceID = Convert.ToString(ap.Field<Object>("ResourceID").ToString()),
                                         APPT_TYPE_ID = Convert.ToString(ap.Field<Object>("APPT_TYPE_ID").ToString())
                                     }
                    ).Distinct();

            _dtApptRec = LinqHelper.CopyToDataTable(FilterNonEventRec, null, null);

            var GrpSt = (from ar1 in _dtApptRec.AsEnumerable()
                         join ar2 in _dtApptRec.AsEnumerable() on ar1.Field<Object>("ResourceID").ToString() equals ar2.Field<Object>("ResourceID").ToString()
                         where DateTime.Parse(ar1.Field<Object>("StartDt").ToString()) == DateTime.Parse(ar2.Field<Object>("EndDt").ToString())
                         select new { ar1, ar2 })
                     .ToList();

            var GrpEt = (from ar1 in _dtApptRec.AsEnumerable()
                         join ar2 in _dtApptRec.AsEnumerable() on ar1.Field<Object>("ResourceID").ToString() equals ar2.Field<Object>("ResourceID").ToString()
                         where DateTime.Parse(ar1.Field<Object>("EndDt").ToString()) == DateTime.Parse(ar2.Field<Object>("StartDt").ToString())
                         select new { ar1, ar2 })
                         .ToList();

            if (((GrpSt != null) && (GrpSt.Count > 0))
                || ((GrpEt != null) && (GrpEt.Count > 0)))
                ConsolidateAdjacentSlots(ref _dtApptRec);

            var z = (from ra in _context.Appt
                     where ((ra.AsnToUserIdListTxt != null) && (ra.AsnToUserIdListTxt.Length > 0) && (ra.EventId == null) && ((ra.ApptTypeId > 16) || (ra.ApptTypeId < 9))
                     && (ra.ApptTypeId != 31) && (ra.ApptTypeId != 33) && (ra.RcurncCd == false))
                     select new
                     {
                         APPT_ID = ra.ApptId,
                         StartDt = ra.StrtTmst,
                         EndDt = ra.EndTmst,
                         ResourceID = XmlElementExtensions.GetAttribute<List<string>>(XElement.Parse(ra.AsnToUserIdListTxt).Descendants("ResourceId"), "Value", new List<string>()),
                         CREAT_DT = ra.CreatDt,
                         MODFD_DT = ra.ModfdDt,
                         APPT_TYPE_ID = ra.ApptTypeId
                     }
                 );

            DataTable dtz = LinqHelper.CopyToDataTable(z, null, null);

            var FilterNonEventNonRec = (from ap in dtz.AsEnumerable()
                                        join sm in dtScheduleMember.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sm.Field<Object>("USER_ID").ToString())
                                        select new
                                        {
                                            APPT_ID = Convert.ToString(ap.Field<Object>("APPT_ID").ToString()),
                                            StartDt = DateTime.Parse(ap.Field<Object>("StartDt").ToString()),
                                            EndDt = DateTime.Parse(ap.Field<Object>("EndDt").ToString()),
                                            ResourceID = Convert.ToString(ap.Field<Object>("ResourceID").ToString()),
                                            APPT_TYPE_ID = Convert.ToString(ap.Field<Object>("APPT_TYPE_ID").ToString())
                                        }
                    ).Distinct();

            DataTable dt = LinqHelper.CopyToDataTable(FilterNonEventNonRec, null, null);

            _dtApptRec.AsEnumerable().CopyToDataTable(dt, LoadOption.PreserveChanges);

            var nonEvnt = (from ap in dt.AsEnumerable()
                           join sm in dtScheduleMember.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sm.Field<Object>("USER_ID").ToString())
                           where (((BusyStart <= DateTime.Parse(ap.Field<Object>("StartDt").ToString())) && (DCEnd >= DateTime.Parse(ap.Field<Object>("EndDt").ToString())))
                                  || ((OrigStDt <= DateTime.Parse(ap.Field<Object>("EndDt").ToString())) && (OrigStDt >= DateTime.Parse(ap.Field<Object>("StartDt").ToString()))))
                           select new
                           {
                               StrtTmst = DateTime.Parse(ap.Field<Object>("StartDt").ToString()),
                               EndTmst = DateTime.Parse(ap.Field<Object>("EndDt").ToString()),
                               USER_ID = Convert.ToString(sm.Field<Object>("USER_ID").ToString()),
                               USER_ADID = Convert.ToString(sm.Field<Object>("USER_ADID").ToString()),
                               DSPL_NME = Convert.ToString(sm.Field<Object>("DSPL_NME").ToString()),
                               APPT_TYPE_ID = Convert.ToString(ap.Field<Object>("APPT_TYPE_ID").ToString())
                           }
                     ).Distinct();

            var unionEvent = newmdsEvent.Union(nonEvnt);

            dtBusyCollection = LinqHelper.CopyToDataTable(unionEvent, null, null);

            return dtBusyCollection;
        }

        public DataTable getAllMDSMACDuration()
        {
            var m = (from mac in _context.LkMdsMacActy.AsNoTracking()
                     where (mac.RecStusId == (byte)ERecStatus.Active)
                     select new { mac.MdsMacActyId, mac.MdsMacActyNme, mac.MinDrtnTmeReqrAmt });
            return LinqHelper.CopyToDataTable(m, null, null);
        }

        public Single getMDSMACDuration(DataTable dtAll, DataTable dtInput)
        {
            return (from ma in dtAll.AsEnumerable()
                    join mi in dtInput.AsEnumerable() on Convert.ToInt32(ma.Field<Object>("MDS_MAC_ACTY_ID").ToString()) equals Convert.ToInt32(mi.Field<Object>("MDS_MAC_ACTY_ID").ToString())
                    select new { MIN_DRTN_TME_REQR_AMT = Single.Parse(ma.Field<Object>("MIN_DRTN_TME_REQR_AMT").ToString()) }).Sum(tm => tm.MIN_DRTN_TME_REQR_AMT);
        }

        public Single getMDSMACDuration(DataTable dtAll, List<MdsEventMacActy> lmm)
        {
            return (from ma in dtAll.AsEnumerable()
                    join mi in lmm.AsEnumerable() on Convert.ToInt32(ma.Field<Object>("MdsMacActyId").ToString()) equals mi.MdsMacActyId
                    select new { MIN_DRTN_TME_REQR_AMT = Single.Parse(ma.Field<Object>("MinDrtnTmeReqrAmt").ToString()) }).Sum(tm => tm.MIN_DRTN_TME_REQR_AMT);
        }

        public Single getManDevDuration(bool oldMDS, DataTable dtInput)
        {
            Single TotalDur = 0;

            DataTable dtLKDEVModel;

            //if ((CacheObj.Cache["LKDEVMODEL_ALL"] != null) && (((DataTable)CacheObj.Cache["LKDEVMODEL_ALL"]).Rows.Count > 0))
            //    dtLKDEVModel = ((DataTable)CacheObj.Cache["LKDEVMODEL_ALL"]);
            //else
            //{
            var q = (from vm in _context.LkDevModel
                     where (vm.RecStusId == (byte)ERecStatus.Active)
                     select new { vm.DevModelId, vm.MinDrtnTmeReqrAmt });

            //CacheObj.AddCacheItem("LKDEVMODEL_ALL", LinqHelper.CopyToDataTable(q, null, null), iCacheTimeOut);
            dtLKDEVModel = LinqHelper.CopyToDataTable(q, null, null);
            //}

            TotalDur = (oldMDS) ? (from ma in dtLKDEVModel.AsEnumerable()
                                   join mi in dtInput.AsEnumerable() on Convert.ToString(ma.Field<Object>("DEV_MODEL_ID").ToString()) equals Convert.ToString(mi.Field<Object>("DEV_MODEL_ID").ToString())
                                   select new { MIN_DRTN_TME_REQR_AMT = Single.Parse(ma.Field<Object>("MIN_DRTN_TME_REQR_AMT").ToString()) }).Sum(tm => tm.MIN_DRTN_TME_REQR_AMT) :
                                   (from ma in dtLKDEVModel.AsEnumerable()
                                    join mi in dtInput.AsEnumerable() on Convert.ToString(ma.Field<Object>("DevModelId").ToString()) equals Convert.ToString(mi.Field<Object>("Value").ToString())
                                    select new { MIN_DRTN_TME_REQR_AMT = Single.Parse(ma.Field<Object>("MinDrtnTmeReqrAmt").ToString()) }).Sum(tm => tm.MIN_DRTN_TME_REQR_AMT);

            return TotalDur;
        }

        public DataTable GetAssignSkilledMembersforMDS(ref DataTable dtMngDev, string sH1, string sIsFirewallSecurity, 
            string sCustNme, List<LkMdsNtwkActyType> lMDSNtwkActy, List<string> lMplsActivityTypes = null)
        {
            List<int> lFinaluser = new List<int>();
            int uid = -1; int puid = -1;
            Dictionary<string, bool> lActy = new Dictionary<string, bool>();

            DataTable dtTempDev = new DataTable();
            dtTempDev.Columns.Add("USER_ID");
            dtTempDev.Columns.Add("USER_ADID");
            dtTempDev.Columns.Add("DSPL_NME");
            dtTempDev.Columns.Add("FULL_NME");

            var y = (from cu in _context.LkDedctdCust
                     where (cu.CustNme == sCustNme)
                     select cu);

            if (y.Count() > 0 && (dtMngDev != null && dtMngDev.Rows.Count > 0))
            {
                var x = (from qlevm in
                                (from qles in
                                    (from qle in
                                            (from ql in _context.LkQlfctn
                                             join qn in _context.QlfctnNtwkActyType on ql.QlfctnId equals qn.QlfctnId
                                             join lmna in _context.LkMdsNtwkActyType on qn.NtwkActyTypeId equals lmna.NtwkActyTypeId
                                             join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
                                                 into joinqlqe
                                             join qc in _context.QlfctnDedctdCust on ql.QlfctnId equals qc.QlfctnId
                                             join lc in _context.LkDedctdCust on qc.CustId equals lc.CustId
                                             from qe in joinqlqe.DefaultIfEmpty()
                                             where lc.CustNme == sCustNme
                                                 && lmna.RecStusId == ((byte)ERecStatus.Active)
                                             //&& (sMDSNtwkActy.Equals(string.Empty) || (lmna.NtwkActyTypeId.Equals(byte.Parse(sMDSNtwkActy))))
                                             select new { ql.QlfctnId, ql.AsnToUserId, qe.EnhncSrvcId, lmna.NtwkActyTypeDes })
                                     join le in _context.LkEnhncSrvc on qle.EnhncSrvcId equals le.EnhncSrvcId
                                        into joinqle2
                                     from le in joinqle2.DefaultIfEmpty()
                                     where (le.EventTypeId == ((short)EventType.MDS))
                                     select new { qle.QlfctnId, qle.AsnToUserId, le.EnhncSrvcNme, qle.NtwkActyTypeDes })
                                 join qvm in _context.QlfctnVndrModel on qles.QlfctnId equals qvm.QlfctnId
                                 into joinqvm
                                 from qvm in joinqvm.DefaultIfEmpty()
                                 select new { qles.QlfctnId, qles.AsnToUserId, qles.EnhncSrvcNme, qvm.DevModelId, qles.NtwkActyTypeDes })
                         join lu in _context.LkUser on qlevm.AsnToUserId equals lu.UserId
                          into joinqlevmlu
                         from lu in joinqlevmlu.DefaultIfEmpty()
                         select new
                         {
                             EnhncSrvcNme = (string.IsNullOrWhiteSpace(qlevm.EnhncSrvcNme)) ? string.Empty : qlevm.EnhncSrvcNme,
                             DevModelId = (qlevm.DevModelId > 0) ? qlevm.DevModelId : -1,
                             MplsActyId = -1,
                             qlevm.NtwkActyTypeDes,
                             lu.UserId,
                             lu.UserAdid,
                             lu.DsplNme,
                             lu.FullNme,
                             qlevm.QlfctnId
                         }).OrderBy(ob => ob.UserId);

                int i = 0;
                int iUserID = 0;
                List<int> lDevID = new List<int>();

                // Added by Sarah Sandoval [20210728] - Added filter for QlfctnMplsActyType
                if (lMplsActivityTypes != null && lMplsActivityTypes.Count() > 0)
                {
                    // Added for Network Enhancement [20211104]
                    // Use CE/CE Change qualification for an activator as a primary skill which encompasses all other activity type skills,
                    // slotpicker will use these criteria to compile a list of activators.
                    if (lMplsActivityTypes.Contains("20") || lMplsActivityTypes.Contains("21"))
                    {
                        lMplsActivityTypes = lMplsActivityTypes.Where(item => item.Contains("20") || item.Contains("21")).ToList();
                    }

                    var mat = (from qlma in x
                               join qma in _context.QlfctnMplsActyType on qlma.QlfctnId equals qma.QlfctnId
                               where lMplsActivityTypes.Contains(qma.MplsActyTypeId.ToString())
                               select new
                               {
                                   qlma.EnhncSrvcNme,
                                   qlma.DevModelId,
                                   MplsActyId = (qma.MplsActyTypeId > 0) ? qma.MplsActyTypeId : -1,
                                   qlma.NtwkActyTypeDes,
                                   qlma.UserId,
                                   qlma.UserAdid,
                                   qlma.DsplNme,
                                   qlma.FullNme,
                                   qlma.QlfctnId
                               }).OrderBy(ob => ob.UserId);

                    x = mat;
                }

                foreach (var x1 in x)
                {
                    if (x1.UserId != iUserID)
                    {
                        i = 0;
                        lDevID.Clear();
                    }
                    if ((x1.UserId != iUserID) || ((x1.UserId == iUserID) && (!(lDevID.Contains((int)x1.DevModelId)))))
                    {
                        foreach (DataRow dr in dtMngDev.Rows)
                        {
                            if (dr["DEV_MODEL_ID"].ToString().Equals(x1.DevModelId.ToString()))
                            {
                                i++;
                            }
                        }
                    }
                    if (i == dtMngDev.Rows.Count)
                    {
                        if (dtTempDev.Select("USER_ID = '" + x1.UserId + "'").Length <= 0)
                            dtTempDev.Rows.Add(new Object[] {x1.UserId, x1.UserAdid, x1.DsplNme, x1.FullNme });
                    }
                    iUserID = x1.UserId;
                    lDevID.Add((int)x1.DevModelId);
                }

                var z = (from td in dtTempDev.AsEnumerable()
                         join xi in x on new { a2 = Int32.Parse(td.Field<Object>("USER_ID").ToString()) } equals new { a2 = xi.UserId }
                         select new
                         {
                             ENHNC_SRVC_NME = xi.EnhncSrvcNme,
                             NTWK_ACTY_TYPE_DES = xi.NtwkActyTypeDes,
                             USER_ID = td.Field<Object>("USER_ID").ToString(),
                             USER_ADID = td.Field<Object>("USER_ADID").ToString(),
                             DSPL_NME = td.Field<Object>("DSPL_NME").ToString(),
                             FULL_NME = td.Field<Object>("FULL_NME").ToString()
                         }).AsQueryable();

                if (sIsFirewallSecurity.Equals("True"))
                    z = z.Where(b => b.ENHNC_SRVC_NME == "Firewall/Security");

                foreach (var litem in lMDSNtwkActy)
                {
                    lActy.Add(litem.NtwkActyTypeDes, false);
                }

                foreach (var c in z.Select(s3 => new { USER_ID = int.Parse(s3.USER_ID), s3.NTWK_ACTY_TYPE_DES }).Distinct().OrderBy(s1 => s1.USER_ID))
                {
                    uid = c.USER_ID;
                    if ((uid != puid) && (puid != -1))//next user
                    {
                        if (!lActy.Any(chk => chk.Value == false))
                            lFinaluser.Add(puid);
                        foreach (var key in lActy.Keys.ToList())
                            lActy[key] = false;
                    }
                    if (lActy.Any(aa => aa.Key == c.NTWK_ACTY_TYPE_DES))
                    {
                        puid = c.USER_ID;
                        lActy[c.NTWK_ACTY_TYPE_DES] = true;
                    }
                }
                if ((uid == puid) && (puid != -1))//last user
                {
                    if (!lActy.Any(chk => chk.Value == false))
                        lFinaluser.Add(puid);
                }

                var a = z.Where(fi => lFinaluser.Contains(int.Parse(fi.USER_ID))).Select(f => new { f.USER_ID, f.USER_ADID, f.DSPL_NME, f.FULL_NME }).Distinct();
                //var a = z.Select(f => new { f.USER_ID, f.USER_ADID, f.DSPL_NME, f.FULL_NME }).Distinct();

                return LinqHelper.CopyToDataTable(a, null, null);
            }
            else if (y.Count() == 0 && (dtMngDev != null && dtMngDev.Rows.Count > 0))
            {
                var x = (from qlevm in
                            (from qles in
                                (from qle in
                                    (from ql in _context.LkQlfctn
                                        join qn in _context.QlfctnNtwkActyType on ql.QlfctnId equals qn.QlfctnId
                                        join lmna in _context.LkMdsNtwkActyType on qn.NtwkActyTypeId equals lmna.NtwkActyTypeId
                                        join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
                                        into joinqlqe
                                        from qe in joinqlqe.DefaultIfEmpty()
                                            //where (sMDSNtwkActy.Equals(string.Empty) || (lmna.NtwkActyTypeId.Equals(byte.Parse(sMDSNtwkActy))))
                                            where lmna.RecStusId == ((byte)ERecStatus.Active)
                                        select new { ql.QlfctnId, ql.AsnToUserId, qe.EnhncSrvcId, lmna.NtwkActyTypeDes })
                                join le in _context.LkEnhncSrvc on qle.EnhncSrvcId equals le.EnhncSrvcId
                                    into joinqle2
                                from le in joinqle2.DefaultIfEmpty()
                                where (le.EventTypeId == ((short)EventType.MDS))
                                select new { qle.QlfctnId, qle.AsnToUserId, le.EnhncSrvcNme, qle.NtwkActyTypeDes })
                            join qvm in _context.QlfctnVndrModel on qles.QlfctnId equals qvm.QlfctnId
                                into joinqvm
                            from qvm in joinqvm.DefaultIfEmpty()
                                //where (v.Contains(qvm.DEV_MODEL_ID))
                                select new { qles.QlfctnId, qles.AsnToUserId, qles.EnhncSrvcNme, qvm.DevModelId, qles.NtwkActyTypeDes })
                         join lu in _context.LkUser on qlevm.AsnToUserId equals lu.UserId
                             into joinqlevmlu
                         from lu in joinqlevmlu.DefaultIfEmpty()
                         select new
                         {
                             EnhncSrvcNme = (string.IsNullOrWhiteSpace(qlevm.EnhncSrvcNme)) ? string.Empty : qlevm.EnhncSrvcNme,
                             DevModelId = (qlevm.DevModelId > 0) ? qlevm.DevModelId : -1,
                             MplsActyId = -1,
                             qlevm.NtwkActyTypeDes,
                             lu.UserId,
                             lu.UserAdid,
                             lu.DsplNme,
                             lu.FullNme,
                             qlevm.QlfctnId
                         }).OrderBy(ob => ob.UserId);

                int i = 0;
                int iUserID = 0;
                List<int> lDevID = new List<int>();
                foreach (var x1 in x)
                {
                    if (x1.UserId != iUserID)
                    {
                        i = 0;
                        lDevID.Clear();
                    }
                    if ((x1.UserId != iUserID) || ((x1.UserId == iUserID) && (!(lDevID.Contains((int)x1.DevModelId)))))
                    {
                        foreach (DataRow dr in dtMngDev.Rows)
                        {
                            if (dr["DEV_MODEL_ID"].ToString().Equals(x1.DevModelId.ToString()))
                            {
                                i++;
                            }
                        }
                    }
                    if (i == dtMngDev.Rows.Count)
                    {
                        if (dtTempDev.Select("USER_ID = '" + x1.UserId + "'").Length <= 0)
                            dtTempDev.Rows.Add(new Object[] { x1.UserId, x1.UserAdid, x1.DsplNme, x1.FullNme });
                        i = 0;
                    }
                    iUserID = x1.UserId;
                    lDevID.Add((int)x1.DevModelId);
                }

                // Added by Sarah Sandoval [20210728] - Added filter for QlfctnMplsActyType
                if (lMplsActivityTypes != null && lMplsActivityTypes.Count() > 0)
                {
                    // Added for Network Enhancement [20211104]
                    // Use CE/CE Change qualification for an activator as a primary skill which encompasses all other activity type skills,
                    // slotpicker will use these criteria to compile a list of activators.
                    if (lMplsActivityTypes.Contains("20") || lMplsActivityTypes.Contains("21"))
                    {
                        lMplsActivityTypes = lMplsActivityTypes.Where(item => item.Contains("20") || item.Contains("21")).ToList();
                    }

                    var mat = (from qlma in x
                               join qma in _context.QlfctnMplsActyType on qlma.QlfctnId equals qma.QlfctnId
                               where lMplsActivityTypes.Contains(qma.MplsActyTypeId.ToString())
                               select new
                               {
                                   qlma.EnhncSrvcNme,
                                   qlma.DevModelId,
                                   MplsActyId = (qma.MplsActyTypeId > 0) ? qma.MplsActyTypeId : -1,
                                   qlma.NtwkActyTypeDes,
                                   qlma.UserId,
                                   qlma.UserAdid,
                                   qlma.DsplNme,
                                   qlma.FullNme,
                                   qlma.QlfctnId
                               }).OrderBy(ob => ob.UserId);

                    x = mat;
                }

                var z = (from td in dtTempDev.AsEnumerable()
                         join xi in x on new { a2 = Int32.Parse(td.Field<Object>("USER_ID").ToString()) } equals new { a2 = xi.UserId }
                         select new
                         {
                             ENHNC_SRVC_NME = xi.EnhncSrvcNme,
                             NTWK_ACTY_TYPE_DES = xi.NtwkActyTypeDes,
                             MPLS_ACTY_TYPE = xi.MplsActyId,
                             USER_ID = td.Field<Object>("USER_ID").ToString(),
                             USER_ADID = td.Field<Object>("USER_ADID").ToString(),
                             DSPL_NME = td.Field<Object>("DSPL_NME").ToString(),
                             FULL_NME = td.Field<Object>("FULL_NME").ToString()
                         }).AsQueryable();

                if (sIsFirewallSecurity.Equals("True"))
                    z = z.Where(b => b.ENHNC_SRVC_NME == "Firewall/Security");

                if (lMplsActivityTypes != null && lMplsActivityTypes.Count() > 0)
                {
                    z = z.Where(l => lMplsActivityTypes.Contains(l.MPLS_ACTY_TYPE.ToString()));
                }

                foreach (var litem in lMDSNtwkActy)
                {
                    lActy.Add(litem.NtwkActyTypeDes, false);
                }

                foreach (var c in z.Select(s3 => new { USER_ID = int.Parse(s3.USER_ID), s3.NTWK_ACTY_TYPE_DES }).Distinct().OrderBy(s1 => s1.USER_ID))
                {
                    uid = c.USER_ID;
                    if ((uid != puid) && (puid != -1))//next user
                    {
                        if (!lActy.Any(chk => chk.Value == false))
                            lFinaluser.Add(puid);
                        foreach (var key in lActy.Keys.ToList())
                            lActy[key] = false;
                    }
                    if (lActy.Any(aa => aa.Key == c.NTWK_ACTY_TYPE_DES))
                    {
                        puid = c.USER_ID;
                        lActy[c.NTWK_ACTY_TYPE_DES] = true;
                    }
                }
                if ((uid == puid) && (puid != -1))//last user
                {
                    if (!lActy.Any(chk => chk.Value == false))
                        lFinaluser.Add(puid);
                }

                var a = z.Where(fi => lFinaluser.Contains(int.Parse(fi.USER_ID))).Select(f => new { f.USER_ID, f.USER_ADID, f.DSPL_NME, f.FULL_NME }).Distinct();

                return LinqHelper.CopyToDataTable(a, null, null);
            }
            else
            {
                var x = (from qlevm in
                             (from qles in
                                  (from qle in
                                       (from ql in _context.LkQlfctn
                                        join qn in _context.QlfctnNtwkActyType on ql.QlfctnId equals qn.QlfctnId
                                        join lmna in _context.LkMdsNtwkActyType on qn.NtwkActyTypeId equals lmna.NtwkActyTypeId
                                        join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
                                           into joinqlqe
                                        from qe in joinqlqe.DefaultIfEmpty()
                                        where lmna.RecStusId == ((byte)ERecStatus.Active)
                                            //where (sMDSNtwkActy.Equals(string.Empty) || (lmna.NtwkActyTypeId.Equals(byte.Parse(sMDSNtwkActy))))
                                            select new { ql.QlfctnId, ql.AsnToUserId, qe.EnhncSrvcId, lmna.NtwkActyTypeDes })
                                   join le in _context.LkEnhncSrvc on qle.EnhncSrvcId equals le.EnhncSrvcId
                                      into joinqle2
                                   from le in joinqle2.DefaultIfEmpty()
                                   where (le.EventTypeId == ((short)EventType.MDS))
                                   select new { qle.QlfctnId, qle.AsnToUserId, le.EnhncSrvcNme, qle.NtwkActyTypeDes })
                              select new { qles.QlfctnId, qles.AsnToUserId, qles.EnhncSrvcNme, qles.NtwkActyTypeDes })
                         join lu in _context.LkUser on qlevm.AsnToUserId equals lu.UserId
                             into joinqlevmlu
                         from lu in joinqlevmlu.DefaultIfEmpty()
                         select new
                         { 
                             qlevm.EnhncSrvcNme,
                             lu.UserId,
                             lu.UserAdid,
                             lu.DsplNme,
                             lu.FullNme,
                             qlevm.NtwkActyTypeDes,
                             qlevm.QlfctnId,
                             MplsActyId = -1
                         });

                if (sIsFirewallSecurity.Equals("True"))
                    x = x.Where(b => b.EnhncSrvcNme == "Firewall/Security");

                // Added by Sarah Sandoval [20210728] - Added filter for QlfctnMplsActyType
                if (lMplsActivityTypes != null && lMplsActivityTypes.Count() > 0)
                {
                    // Added for Network Enhancement [20211104]
                    // Use CE/CE Change qualification for an activator as a primary skill which encompasses all other activity type skills,
                    // slotpicker will use these criteria to compile a list of activators.
                    if (lMplsActivityTypes.Contains("20") || lMplsActivityTypes.Contains("21"))
                    {
                        lMplsActivityTypes = lMplsActivityTypes.Where(item => item.Contains("20") || item.Contains("21")).ToList();
                    }

                    var mat = (from qlma in x
                               join qma in _context.QlfctnMplsActyType on qlma.QlfctnId equals qma.QlfctnId
                               where lMplsActivityTypes.Contains(qma.MplsActyTypeId.ToString())
                               select new
                               {
                                   qlma.EnhncSrvcNme,
                                   qlma.UserId,
                                   qlma.UserAdid,
                                   qlma.DsplNme,
                                   qlma.FullNme,
                                   qlma.NtwkActyTypeDes,
                                   qlma.QlfctnId,
                                   MplsActyId = (qma.MplsActyTypeId > 0) ? qma.MplsActyTypeId : -1,
                               }).OrderBy(ob => ob.UserId);

                    x = mat;
                }

                foreach (var litem in lMDSNtwkActy)
                {
                    lActy.Add(litem.NtwkActyTypeDes, false);
                }

                foreach (var c in x.Select(s3 => new { s3.UserId, s3.NtwkActyTypeDes }).Distinct().OrderBy(s1 => s1.UserId))
                {
                    uid = c.UserId;
                    if ((uid != puid) && (puid != -1))//next user
                    {
                        if (!lActy.Any(chk => chk.Value == false))
                            lFinaluser.Add(puid);
                        foreach (var key in lActy.Keys.ToList())
                            lActy[key] = false;
                    }
                    if (lActy.Any(aa => aa.Key == c.NtwkActyTypeDes))
                    {
                        puid = c.UserId;
                        lActy[c.NtwkActyTypeDes] = true;
                    }
                }
                if ((uid == puid) && (puid != -1))//last user
                {
                    if (!lActy.Any(chk => chk.Value == false))
                        lFinaluser.Add(puid);
                }

                var a = x.Where(fi => lFinaluser.Contains(fi.UserId)).Select(f => new { 
                    USER_ID = f.UserId,
                    USER_ADID = f.UserAdid,
                    DSPL_NME = f.DsplNme,
                    FULL_NME = f.FullNme
                }).Distinct();

                return LinqHelper.CopyToDataTable(a, null, null);
            }

            //if (y.Count() > 0)
            //{
            //    if (dtMngDev != null)
            //    {
            //        var x = (from qlevm in
            //                     (from qles in
            //                          (from qle in
            //                               (from ql in _context.LkQlfctn
            //                                join qn in _context.QlfctnNtwkActyType on ql.QlfctnId equals qn.QlfctnId
            //                                join lmna in _context.LkMdsNtwkActyType on qn.NtwkActyTypeId equals lmna.NtwkActyTypeId
            //                                join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
            //                                   into joinqlqe
            //                                join qc in _context.QlfctnDedctdCust on ql.QlfctnId equals qc.QlfctnId
            //                                join lc in _context.LkDedctdCust on qc.CustId equals lc.CustId
            //                                from qe in joinqlqe.DefaultIfEmpty()
            //                                where lc.CustNme == sCustNme
            //                                    && lmna.RecStusId == ((byte)ERecStatus.Active)
            //                                //&& (sMDSNtwkActy.Equals(string.Empty) || (lmna.NtwkActyTypeId.Equals(byte.Parse(sMDSNtwkActy))))
            //                                select new { ql.QlfctnId, ql.AsnToUserId, qe.EnhncSrvcId, lmna.NtwkActyTypeDes })
            //                           join le in _context.LkEnhncSrvc on qle.EnhncSrvcId equals le.EnhncSrvcId
            //                              into joinqle2
            //                           from le in joinqle2.DefaultIfEmpty()
            //                           where (le.EventTypeId == ((short)EventType.MDS))
            //                           select new { qle.QlfctnId, qle.AsnToUserId, le.EnhncSrvcNme, qle.NtwkActyTypeDes })
            //                      join qvm in _context.QlfctnVndrModel on qles.QlfctnId equals qvm.QlfctnId
            //                       into joinqvm
            //                      from qvm in joinqvm.DefaultIfEmpty()
            //                      select new { qles.QlfctnId, qles.AsnToUserId, qles.EnhncSrvcNme, qvm.DevModelId, qles.NtwkActyTypeDes })
            //                 join lu in _context.LkUser on qlevm.AsnToUserId equals lu.UserId
            //                  into joinqlevmlu
            //                 from lu in joinqlevmlu.DefaultIfEmpty()
            //                 select new
            //                 {
            //                     EnhncSrvcNme = (qlevm.EnhncSrvcNme == null) ? string.Empty : qlevm.EnhncSrvcNme,
            //                     DevModelId = (qlevm.DevModelId == null) ? -1 : qlevm.DevModelId,
            //                     qlevm.NtwkActyTypeDes,
            //                     lu.UserId,
            //                     lu.UserAdid,
            //                     lu.DsplNme,
            //                     lu.FullNme
            //                 }).OrderBy(ob => ob.UserId);

            //        int i = 0;
            //        int iUserID = 0;
            //        List<int> lDevID = new List<int>();
            //        foreach (var x1 in x)
            //        {
            //            if (x1.UserId != iUserID)
            //            {
            //                i = 0;
            //                lDevID.Clear();
            //            }
            //            if ((x1.UserId != iUserID) || ((x1.UserId == iUserID) && (!(lDevID.Contains((int)x1.DevModelId)))))
            //            {
            //                foreach (DataRow dr in dtMngDev.Rows)
            //                {
            //                    if (dr["DevModelId"].ToString().Equals(x1.DevModelId.ToString()))
            //                    {
            //                        i++;
            //                    }
            //                }
            //            }
            //            if (i == dtMngDev.Rows.Count)
            //            {
            //                dtTempDev.Rows.Add(new Object[] { x1.EnhncSrvcNme, x1.DevModelId, x1.NtwkActyTypeDes, x1.UserId, x1.UserAdid, x1.DsplNme, x1.FullNme });
            //            }
            //            iUserID = x1.UserId;
            //            lDevID.Add((int)x1.DevModelId);
            //        }

            //        var z = (from td in dtTempDev.AsEnumerable()
            //                 select new
            //                 {
            //                     ENHNC_SRVC_NME = td.Field<Object>("ENHNC_SRVC_NME").ToString(),
            //                     DEV_MODEL_ID = td.Field<Object>("DEV_MODEL_ID").ToString(),
            //                     NTWK_ACTY_TYPE_DES = td.Field<Object>("NTWK_ACTY_TYPE_DES").ToString(),
            //                     USER_ID = td.Field<Object>("USER_ID").ToString(),
            //                     USER_ADID = td.Field<Object>("USER_ADID").ToString(),
            //                     DSPL_NME = td.Field<Object>("DSPL_NME").ToString(),
            //                     FULL_NME = td.Field<Object>("FULL_NME").ToString()
            //                 }).AsQueryable();

            //        if (sIsFirewallSecurity.Equals("True"))
            //            z = z.Where(b => b.ENHNC_SRVC_NME == "Firewall/Security");

            //        foreach (var litem in lMDSNtwkActy)
            //        {
            //            lActy.Add(litem.NtwkActyTypeDes, false);
            //        }

            //        foreach (var c in z.Select(s3 => new { USER_ID = int.Parse(s3.USER_ID), s3.NTWK_ACTY_TYPE_DES }).Distinct().OrderBy(s1 => s1.USER_ID))
            //        {
            //            uid = c.USER_ID;
            //            if ((uid != puid) && (puid != -1))//next user
            //            {
            //                if (!lActy.Any(chk => chk.Value == false))
            //                    lFinaluser.Add(puid);
            //                foreach (var key in lActy.Keys.ToList())
            //                    lActy[key] = false;
            //            }
            //            if (lActy.Any(aa => aa.Key == c.NTWK_ACTY_TYPE_DES))
            //            {
            //                puid = c.USER_ID;
            //                lActy[c.NTWK_ACTY_TYPE_DES] = true;
            //            }
            //        }
            //        if ((uid == puid) && (puid != -1))//last user
            //        {
            //            if (!lActy.Any(chk => chk.Value == false))
            //                lFinaluser.Add(puid);
            //        }

            //        var a = z.Where(fi => lFinaluser.Contains(int.Parse(fi.USER_ID))).Select(f => new { f.USER_ID, f.USER_ADID, f.DSPL_NME, f.FULL_NME }).Distinct();
            //        //var a = z.Select(f => new { f.USER_ID, f.USER_ADID, f.DSPL_NME, f.FULL_NME }).Distinct();

            //        return LinqHelper.CopyToDataTable(a, null, null);
            //    }
            //    else
            //    {
            //        var x = (from qlevm in
            //                     (from qles in
            //                          (from qle in
            //                               (from ql in _context.LkQlfctn
            //                                join qn in _context.QlfctnNtwkActyType on ql.QlfctnId equals qn.QlfctnId
            //                                join lmna in _context.LkMdsNtwkActyType on qn.NtwkActyTypeId equals lmna.NtwkActyTypeId
            //                                join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
            //                                   into joinqlqe
            //                                from qe in joinqlqe.DefaultIfEmpty()
            //                                where lmna.RecStusId == ((byte)ERecStatus.Active)
            //                                //where (sMDSNtwkActy.Equals(string.Empty) || (lmna.NtwkActyTypeId.Equals(byte.Parse(sMDSNtwkActy))))
            //                                select new { ql.QlfctnId, ql.AsnToUserId, qe.EnhncSrvcId, lmna.NtwkActyTypeDes })
            //                           join le in _context.LkEnhncSrvc on qle.EnhncSrvcId equals le.EnhncSrvcId
            //                              into joinqle2
            //                           from le in joinqle2.DefaultIfEmpty()
            //                           where (le.EventTypeId == ((short)EventType.MDS))
            //                           select new { qle.QlfctnId, qle.AsnToUserId, le.EnhncSrvcNme, qle.NtwkActyTypeDes })
            //                      select new { qles.QlfctnId, qles.AsnToUserId, qles.EnhncSrvcNme, qles.NtwkActyTypeDes })
            //                 join lu in _context.LkUser on qlevm.AsnToUserId equals lu.UserId
            //                     into joinqlevmlu
            //                 from lu in joinqlevmlu.DefaultIfEmpty()
            //                 select new { qlevm.EnhncSrvcNme, lu.UserId, lu.UserAdid, lu.DsplNme, lu.FullNme, qlevm.NtwkActyTypeDes });

            //        if (sIsFirewallSecurity.Equals("True"))
            //            x = x.Where(b => b.EnhncSrvcNme == "Firewall/Security");

            //        foreach (var litem in lMDSNtwkActy)
            //        {
            //            lActy.Add(litem.NtwkActyTypeDes, false);
            //        }

            //        foreach (var c in x.Select(s3 => new { s3.UserId, s3.NtwkActyTypeDes }).Distinct().OrderBy(s1 => s1.UserId))
            //        {
            //            uid = c.UserId;
            //            if ((uid != puid) && (puid != -1))//next user
            //            {
            //                if (!lActy.Any(chk => chk.Value == false))
            //                    lFinaluser.Add(puid);
            //                foreach (var key in lActy.Keys.ToList())
            //                    lActy[key] = false;
            //            }
            //            if (lActy.Any(aa => aa.Key == c.NtwkActyTypeDes))
            //            {
            //                puid = c.UserId;
            //                lActy[c.NtwkActyTypeDes] = true;
            //            }
            //        }
            //        if ((uid == puid) && (puid != -1))//last user
            //        {
            //            if (!lActy.Any(chk => chk.Value == false))
            //                lFinaluser.Add(puid);
            //        }

            //        var a = x.Where(fi => lFinaluser.Contains(fi.UserId)).Select(i => new { i.UserId, i.UserAdid, i.DsplNme, i.FullNme }).Distinct();

            //        return LinqHelper.CopyToDataTable(a, null, null);
            //    }
            //}
            //else
            //{
            //    if (dtMngDev != null)
            //    {
            //        var x = (from qlevm in
            //                     (from qles in
            //                          (from qle in
            //                                (from ql in _context.LkQlfctn
            //                                 join qn in _context.QlfctnNtwkActyType on ql.QlfctnId equals qn.QlfctnId
            //                                 join lmna in _context.LkMdsNtwkActyType on qn.NtwkActyTypeId equals lmna.NtwkActyTypeId
            //                                 join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
            //                                    into joinqlqe
            //                                 from qe in joinqlqe.DefaultIfEmpty()
            //                                     //where (sMDSNtwkActy.Equals(string.Empty) || (lmna.NtwkActyTypeId.Equals(byte.Parse(sMDSNtwkActy))))
            //                                 where lmna.RecStusId == ((byte)ERecStatus.Active)
            //                                 select new { ql.QlfctnId, ql.AsnToUserId, qe.EnhncSrvcId, lmna.NtwkActyTypeDes })
            //                           join le in _context.LkEnhncSrvc on qle.EnhncSrvcId equals le.EnhncSrvcId
            //                              into joinqle2
            //                           from le in joinqle2.DefaultIfEmpty()
            //                           where (le.EventTypeId == ((short)EventType.MDS))
            //                           select new { qle.QlfctnId, qle.AsnToUserId, le.EnhncSrvcNme, qle.NtwkActyTypeDes })
            //                      join qvm in _context.QlfctnVndrModel on qles.QlfctnId equals qvm.QlfctnId
            //                         into joinqvm
            //                      from qvm in joinqvm.DefaultIfEmpty()

            //                          //where (v.Contains(qvm.DEV_MODEL_ID))
            //                      select new { qles.QlfctnId, qles.AsnToUserId, qles.EnhncSrvcNme, qvm.DevModelId, qles.NtwkActyTypeDes })
            //                 join lu in _context.LkUser on qlevm.AsnToUserId equals lu.UserId
            //                     into joinqlevmlu
            //                 from lu in joinqlevmlu.DefaultIfEmpty()
            //                 select new
            //                 {
            //                     EnhncSrvcNme = (qlevm.EnhncSrvcNme == null) ? string.Empty : qlevm.EnhncSrvcNme,
            //                     DevModelId = (qlevm.DevModelId == null) ? -1 : qlevm.DevModelId,
            //                     qlevm.NtwkActyTypeDes,
            //                     lu.UserId,
            //                     lu.UserAdid,
            //                     lu.DsplNme,
            //                     lu.FullNme
            //                 }).OrderBy(ob => ob.UserId);

            //        int i = 0;
            //        int iUserID = 0;
            //        List<int> lDevID = new List<int>();
            //        foreach (var x1 in x)
            //        {
            //            if (x1.UserId != iUserID)
            //            {
            //                i = 0;
            //                lDevID.Clear();
            //            }
            //            if ((x1.UserId != iUserID) || ((x1.UserId == iUserID) && (!(lDevID.Contains((int)x1.DevModelId)))))
            //            {
            //                foreach (DataRow dr in dtMngDev.Rows)
            //                {
            //                    if (dr["DEV_MODEL_ID"].ToString().Equals(x1.DevModelId.ToString()))
            //                    {
            //                        i++;
            //                    }
            //                }
            //            }
            //            if (i == dtMngDev.Rows.Count)
            //            {
            //                dtTempDev.Rows.Add(new Object[] { x1.EnhncSrvcNme, x1.DevModelId, x1.NtwkActyTypeDes, x1.UserId, x1.UserAdid, x1.DsplNme, x1.FullNme });
            //                i = 0;
            //            }
            //            iUserID = x1.UserId;
            //            lDevID.Add((int)x1.DevModelId);
            //        }

            //        var z = (from td in dtTempDev.AsEnumerable()

            //                     //join x1 in x on new { a1 = Int32.Parse(td.Field<Object>("DEV_MODEL_ID").ToString()), a2 = Int32.Parse(td.Field<Object>("USER_ID").ToString()) } equals new { a1 = (int)x1.DEV_MODEL_ID, a2 = x1.USER_ID }
            //                 select new
            //                 {
            //                     ENHNC_SRVC_NME = td.Field<Object>("ENHNC_SRVC_NME").ToString(),
            //                     DEV_MODEL_ID = td.Field<Object>("DEV_MODEL_ID").ToString(),
            //                     NTWK_ACTY_TYPE_DES = td.Field<Object>("NTWK_ACTY_TYPE_DES").ToString(),
            //                     USER_ID = td.Field<Object>("USER_ID").ToString(),
            //                     USER_ADID = td.Field<Object>("USER_ADID").ToString(),
            //                     DSPL_NME = td.Field<Object>("DSPL_NME").ToString(),
            //                     FULL_NME = td.Field<Object>("FULL_NME").ToString()
            //                 }).AsQueryable();

            //        if (sIsFirewallSecurity.Equals("True"))
            //            z = z.Where(b => b.ENHNC_SRVC_NME == "Firewall/Security");

            //        foreach (var litem in lMDSNtwkActy)
            //        {
            //            lActy.Add(litem.NtwkActyTypeDes, false);
            //        }

            //        foreach (var c in z.Select(s3 => new { USER_ID = int.Parse(s3.USER_ID), s3.NTWK_ACTY_TYPE_DES }).Distinct().OrderBy(s1 => s1.USER_ID))
            //        {
            //            uid = c.USER_ID;
            //            if ((uid != puid) && (puid != -1))//next user
            //            {
            //                if (!lActy.Any(chk => chk.Value == false))
            //                    lFinaluser.Add(puid);
            //                foreach (var key in lActy.Keys.ToList())
            //                    lActy[key] = false;
            //            }
            //            if (lActy.Any(aa => aa.Key == c.NTWK_ACTY_TYPE_DES))
            //            {
            //                puid = c.USER_ID;
            //                lActy[c.NTWK_ACTY_TYPE_DES] = true;
            //            }
            //        }
            //        if ((uid == puid) && (puid != -1))//last user
            //        {
            //            if (!lActy.Any(chk => chk.Value == false))
            //                lFinaluser.Add(puid);
            //        }

            //        var a = z.Where(fi => lFinaluser.Contains(int.Parse(fi.USER_ID))).Select(f => new { f.USER_ID, f.USER_ADID, f.DSPL_NME, f.FULL_NME }).Distinct();

            //        return LinqHelper.CopyToDataTable(a, null, null);
            //    }
            //    else
            //    {
            //        var x = (from qlevm in
            //                     (from qles in
            //                          (from qle in
            //                               (from ql in _context.LkQlfctn
            //                                join qn in _context.QlfctnNtwkActyType on ql.QlfctnId equals qn.QlfctnId
            //                                join lmna in _context.LkMdsNtwkActyType on qn.NtwkActyTypeId equals lmna.NtwkActyTypeId
            //                                join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
            //                                   into joinqlqe
            //                                from qe in joinqlqe.DefaultIfEmpty()
            //                                where lmna.RecStusId == ((byte)ERecStatus.Active)
            //                                //where (sMDSNtwkActy.Equals(string.Empty) || (lmna.NtwkActyTypeId.Equals(byte.Parse(sMDSNtwkActy))))
            //                                select new { ql.QlfctnId, ql.AsnToUserId, qe.EnhncSrvcId, lmna.NtwkActyTypeDes })
            //                           join le in _context.LkEnhncSrvc on qle.EnhncSrvcId equals le.EnhncSrvcId
            //                              into joinqle2
            //                           from le in joinqle2.DefaultIfEmpty()
            //                           where (le.EventTypeId == ((short)EventType.MDS))
            //                           select new { qle.QlfctnId, qle.AsnToUserId, le.EnhncSrvcNme, qle.NtwkActyTypeDes })
            //                      select new { qles.QlfctnId, qles.AsnToUserId, qles.EnhncSrvcNme, qles.NtwkActyTypeDes })
            //                 join lu in _context.LkUser on qlevm.AsnToUserId equals lu.UserId
            //                     into joinqlevmlu
            //                 from lu in joinqlevmlu.DefaultIfEmpty()
            //                 select new { qlevm.EnhncSrvcNme, lu.UserId, lu.UserAdid, lu.DsplNme, lu.FullNme, qlevm.NtwkActyTypeDes });

            //        if (sIsFirewallSecurity.Equals("True"))
            //            x = x.Where(b => b.EnhncSrvcNme == "Firewall/Security");

            //        foreach (var litem in lMDSNtwkActy)
            //        {
            //            lActy.Add(litem.NtwkActyTypeDes, false);
            //        }

            //        foreach (var c in x.Select(s3 => new { s3.UserId, s3.NtwkActyTypeDes }).Distinct().OrderBy(s1 => s1.UserId))
            //        {
            //            uid = c.UserId;
            //            if ((uid != puid) && (puid != -1))//next user
            //            {
            //                if (!lActy.Any(chk => chk.Value == false))
            //                    lFinaluser.Add(puid);
            //                foreach (var key in lActy.Keys.ToList())
            //                    lActy[key] = false;
            //            }
            //            if (lActy.Any(aa => aa.Key == c.NtwkActyTypeDes))
            //            {
            //                puid = c.UserId;
            //                lActy[c.NtwkActyTypeDes] = true;
            //            }
            //        }
            //        if ((uid == puid) && (puid != -1))//last user
            //        {
            //            if (!lActy.Any(chk => chk.Value == false))
            //                lFinaluser.Add(puid);
            //        }

            //        var a = x.Where(fi => lFinaluser.Contains(fi.UserId)).Select(i => new { i.UserId, i.UserAdid, i.DsplNme, i.FullNme }).Distinct();

            //        return LinqHelper.CopyToDataTable(a, null, null);
            //    }
            //}
        }

        public DataTable GetAssignSkilledMembersforMPLS(string sMPLSEventType, string sIPVersion)
        {
            string s = (sMPLSEventType.Contains("VAS")) && (!(sMPLSEventType.Equals(""))) ? "VAS" : ((!(sMPLSEventType.Equals(""))) ? "STANDARD" : "");

            var x = (from ql in _context.LkQlfctn
                     join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
                     join le in _context.LkEnhncSrvc on qe.EnhncSrvcId equals le.EnhncSrvcId
                     join qi in _context.QlfctnIpVer on ql.QlfctnId equals qi.QlfctnId
                     into joinip
                     from qi in joinip.DefaultIfEmpty()
                     join lu in _context.LkUser on ql.AsnToUserId equals lu.UserId
                     select new { lu, le, qi });

            if (!(s.Equals("")))
                //x = x.Where("le.ENHNC_SRVC_NME == \"" + s + "\"");
                x = x.Where(i => i.le.EnhncSrvcNme.Equals(s));

            if (!(sIPVersion.Equals("0")))
                //x = x.Where("qi.IP_VER_ID == " + sIPVersion);
                x = x.Where(i => i.qi.IpVerId == Convert.ToByte(sIPVersion));

            var y = x.Select(i => new { i.lu.UserId, i.lu.UserAdid, i.lu.DsplNme, i.lu.FullNme }).Distinct();

            return LinqHelper.CopyToDataTable(y, null, null);
        }

        public DataTable GetAssignSkilledMembersforEventType(string sEventTypeID)
        {
            var x = (from ql in _context.LkQlfctn
                     join qe in _context.QlfctnEventType on ql.QlfctnId equals qe.QlfctnId
                     join lu in _context.LkUser on ql.AsnToUserId equals lu.UserId
                     select new { lu, qe });

            //x = x.Where("qe.EVENT_TYPE_ID == " + sEventTypeID);
            x = x.Where(i => i.qe.EventTypeId == Convert.ToInt16(sEventTypeID));

            var y = x.Select(i => new { i.lu.UserId, i.lu.UserAdid, i.lu.DsplNme, i.lu.FullNme }).Distinct();

            return LinqHelper.CopyToDataTable(y, null, null);
        }



        public string ChkForAppt(string UserID, DateTime? StrtTime, DateTime? EndTime, int EventID)
        {
            try
            {
                SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
                SqlCommand command = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataTable dt = new DataTable();
                command = new SqlCommand("web.chkForAppointment", connection);
                command.CommandTimeout = _context.commandTimeout;
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@User_ID", UserID);
                command.Parameters.AddWithValue("@StartTime", StrtTime);
                command.Parameters.AddWithValue("@EndTime", EndTime);
                command.Parameters.AddWithValue("@EventID", EventID);
                da = new SqlDataAdapter(command);
                da.Fill(dt);
                connection.Close();

                if ((dt != null) && (dt.Rows.Count > 0))
                    return dt.Rows[0][0].ToString();
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                // throw new Exception(ex.Message + " ; User : " + this.LoggedUser.DSPL_NME, ex);
            }

            return string.Empty;

        }

        public string ChkForDblBooking(DateTime? start, DateTime? end, string sUID, int iEventId)
        {

            string warning = string.Empty;
            string warningUsers = string.Empty;

            char[] delimiterChars = { '|' };
            string[] ids = sUID.Split(delimiterChars);

            if (ids != null)
            {
                foreach (string id in ids)
                {
                    string sDispNme = ChkForAppt(id.Trim(), start, end, iEventId);

                    if (sDispNme.Length > 0)
                    {
                        warningUsers = warningUsers + ", " + sDispNme;
                    }
                }
            }

            if (!string.IsNullOrEmpty(warningUsers))
            {
                warningUsers = warningUsers.Remove(0, 2);
                warning = "Warning : " + warningUsers + " already has another appt for this time.";
            }
            return warning;
        }

        public string GetActivatorWhenConflict(string[] sUserList, short dayOffset)
        {
                    var awc = (from me in _context.MdsEvent
                               join eh in _context.EventHist on new { me.EventId, ActnId = (byte)1 } equals new { eh.EventId, eh.ActnId }
                               join ea in _context.EventAsnToUser on new { me.EventId, RecStusId = (byte)1 } equals new { ea.EventId, ea.RecStusId }
                               where me.EventStusId != 8
                                && sUserList.Contains(ea.AsnToUserId.ToString())
                                && eh.CreatDt.AddDays(dayOffset) >= DateTime.Now
                               orderby eh.CreatDt
                               select new { ea.AsnToUserId, eh.CreatDt }).FirstOrDefault();

                    return (awc != null) ? awc.AsnToUserId.ToString() : string.Empty;
        }

    }
}