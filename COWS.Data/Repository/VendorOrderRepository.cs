﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VendorOrderRepository : IVendorOrderRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IH5FolderRepository _h5FolderRepo;

        public VendorOrderRepository(COWSAdminDBContext context, 
            IH5FolderRepository h5FolderRepo,
            IConfiguration configuration)
        {
            _context = context;
            _h5FolderRepo = h5FolderRepo;
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public IQueryable<VndrOrdr> Find(Expression<Func<VndrOrdr, bool>> predicate)
        {
            return _context.VndrOrdr
                .Include(a => a.Ordr.H5Foldr)
                .Include(a => a.Ordr.FsaOrdr.ProdTypeCdNavigation.LkProdType)
                .Include(a => a.Ordr.FsaOrdr.OrdrTypeCdNavigation.LkOrdrType)
                .Include(vf => vf.VndrOrdrNavigation)
                .Include(vf => vf.VndrFoldr)
                    .ThenInclude(vf => vf.VndrCdNavigation)
                .Include(vf => vf.VndrFoldr)
                    .ThenInclude(vf => vf.CtryCdNavigation)
                .Include(voe => voe.VndrOrdrEmail)
                    .ThenInclude(voet => voet.CreatByUser)
                .Include(voe => voe.VndrOrdrEmail)
                    .ThenInclude(voet => voet.ModfdByUser)
                .Include(voe => voe.VndrOrdrEmail)
                    .ThenInclude(voet => voet.VndrEmailType)
                .Include(voe => voe.VndrOrdrEmail)
                    .ThenInclude(voea => voea.VndrOrdrEmailAtchmt)
                .Include(voe => voe.VndrOrdrEmail)
                    .ThenInclude(voea => voea.EmailStus)
                .Include(voe => voe.VndrOrdrMs)
                .Where(predicate);
        }

        public IQueryable<VndrOrdrEmailAtchmt> GetEmailAttachment(int vendorOrderId)
        {
            var vendorOrderEmailId = _context.VndrOrdrEmail
                .Where(voe => voe.VndrOrdrId == vendorOrderId)
                .Select(ve => new { ve.VndrOrdrEmailId });

            var result = _context.VndrOrdrEmailAtchmt
                .Where(voea => vendorOrderEmailId
                    .Select(h => h.VndrOrdrEmailId)
                    .Contains(voea.VndrOrdrEmailId));

            return result;
        }
        public VndrOrdr GetForVendorOrderPage(int id, int userCsgLvl)
        {
            VndrOrdr vendorOrder = _context.VndrOrdr
                //.Include(a => a.Ordr)
                .Include(a => a.Ordr.FsaOrdr.ProdTypeCdNavigation.LkProdType) // LOOKUP
                .Include(a => a.Ordr.FsaOrdr.OrdrTypeCdNavigation.LkOrdrType) // LOOKUP
                //.Include(a => a.VndrFoldr)
                .Include(a => a.VndrOrdrForm)
                .Where(a => a.VndrOrdrId == id && a.RecStusId == 1)
                .SingleOrDefault();

            // Vendor Order Email
            vendorOrder.VndrOrdrEmail = _context.VndrOrdrEmail
                .Include(a => a.VndrOrdrEmailAtchmt) // LOOKUP
                .Include(a => a.VndrEmailType) // LOOKUP
                .Include(a => a.EmailStus) // LOOKUP
                .Include(a => a.CreatByUser)
                .Include(a => a.ModfdByUser)
                .Where(a => a.VndrOrdrId == id)
                .ToList();

            // Vendor Order Ms
            vendorOrder.VndrOrdrMs = _context.VndrOrdrMs
                .Where(a => vendorOrder.VndrOrdrEmail.Any(b => b.VndrOrdrEmailId == a.VndrOrdrEmailId))
                .ToList();

            // Vendor Folder
            vendorOrder.VndrFoldr = _context.VndrFoldr
                .Include(a => a.VndrFoldrCntct)
                .Include(a => a.VndrTmplt)
                .Include(a => a.VndrCdNavigation) // LOOKUP
                .Include(a => a.CtryCdNavigation) // LOOKUP
                .Where(a => a.VndrFoldrId == vendorOrder.VndrFoldrId)
                .SingleOrDefault();

            vendorOrder.VndrOrdrNavigation = _context.Ordr
                .Where(a => a.OrdrId == id)
                .SingleOrDefault();

            if ((vendorOrder.VndrOrdrNavigation != null) && (vendorOrder.VndrOrdrNavigation.H5FoldrId != null))
            {
                vendorOrder.VndrOrdrNavigation.H5Foldr = _h5FolderRepo.GetByIdForOrderView((int)vendorOrder.VndrOrdrNavigation.H5FoldrId, userCsgLvl);
            }

            return vendorOrder;
        }

        public IQueryable<LkVndrEmailType> GetVendorOrderEmailType()
        {
            return _context.LkVndrEmailType;
        }

        public VndrOrdr Create(VndrOrdr entity)
        {
            int maxId = _context.Ordr.Max(i => i.OrdrId);
            //int maxId = entity.Ordr.OrdrId;
            entity.VndrOrdrId = maxId;
            entity.VndrOrdrType = _context.LkVndrOrdrType.Where(a => a.VndrOrdrTypeId == entity.VndrOrdrTypeId).SingleOrDefault();
            _context.VndrOrdr.Add(entity);

            //_context.Ordr.Where(a => a.OrdrId == entity.VndrOrdrId).SingleOrDefault().PrntOrdrId = entity.VndrOrdrId;
            SaveAll();

            return entity;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<VndrOrdr> GetAll()
        {
            throw new NotImplementedException();
        }

        public VndrOrdr GetById(int id)
        {
            return Find(a => a.VndrOrdrId == id).SingleOrDefault();
        }
        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, VndrOrdr entity)
        {
            VndrOrdr obj = GetById(id);

            obj.TrmtgCd = entity.TrmtgCd;
            obj.BypasVndrOrdrMsCd = entity.BypasVndrOrdrMsCd;
            obj.OrdrId = entity.OrdrId;
            obj.ModfdByUserId = entity.ModfdByUserId;
            obj.ModfdDt = entity.ModfdDt;
            //obj.Ordr = entity.Ordr;

            if (entity.VndrOrdrNavigation.H5FoldrId != null)
            {
                obj.VndrOrdrNavigation = _context.Ordr.Where(a => a.OrdrId == id).SingleOrDefault();
                obj.VndrOrdrNavigation.H5FoldrId = entity.VndrOrdrNavigation.H5FoldrId;
            }
            //Ordr order = _context.Ordr.Where(a => a.OrdrId == id).SingleOrDefault();
            //order.H5FoldrId = entity.Ordr.H5FoldrId;
        }

        public IQueryable<VndrOrdr> GetVendorOrder(Expression<Func<VndrOrdr, bool>> predicate)
        {
            return _context.VndrOrdr
                //.Include(ordr => ordr.Ordr)
                .Include(vf => vf.VndrFoldr)
                    .ThenInclude(lkv => lkv.VndrCdNavigation)
                .Include(vf => vf.VndrFoldr)
                    .ThenInclude(lkv => lkv.CtryCdNavigation)
                .Include(vot => vot.VndrOrdrType)
                .Include(ordr => ordr.Ordr)
                    .ThenInclude(fsao => fsao.FsaOrdr)
                .Include(ordr => ordr.Ordr)
                    .ThenInclude(h5f => h5f.H5Foldr)
                .Include(voe => voe.VndrOrdrEmail)
                    .ThenInclude(voet => voet.VndrEmailType)
                .Include(ordr => ordr.Ordr)
                    .ThenInclude(ipl => ipl.IplOrdr)
                .Include(ordr => ordr.Ordr)
                    .ThenInclude(ncco => ncco.NccoOrdr)
                .Where(predicate);
        }

        public IEnumerable<OffnetForm> GetVendorOrderFormDynamic(int orderId)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.getVendorOrderFormDynamicData @OrderID";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@OrderID", SqlDbType = SqlDbType.Int, Value = orderId });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    return reader.Translate<OffnetForm>();
                }
            }
        }

        //public async Task<DataTable> GetVendorOrders(int vendorOrderID, int h5FolderCustID, string ftn)
        //{
        //    ftn = ftn ?? "";
        //    DataTable dt = new DataTable();
        //    using (var connection = _context.Database.GetDbConnection())
        //    {
        //        await connection.OpenAsync();
        //        using (var command = connection.CreateCommand())
        //        {
        //            command.CommandText = $"dbo.getVendorOrdersByIDH5FTN {vendorOrderID}, {h5FolderCustID}, {ftn}";
        //            var result = await command.ExecuteReaderAsync();
        //            dt.Load(result);
        //            return dt;
        //        }
        //    }
        //}

        public DataSet GetVendorOrders(int vendorOrderID, int h5FolderCustID, string ftn)
        {
            ftn = ftn ?? "";
            SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            command = new SqlCommand("dbo.getVendorOrdersByIDH5FTN", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@VendorOrderID", vendorOrderID);
            command.Parameters.AddWithValue("@H5CustID", h5FolderCustID);
            command.Parameters.AddWithValue("@FTN", ftn);
            da = new SqlDataAdapter(command);
            da.Fill(ds);
            connection.Close();

            return ds;

            //List<SqlParameter> pc = new List<SqlParameter>
            //    {
            //        new SqlParameter("@VendorOrderID", SqlDbType.Int),
            //        new SqlParameter("@H5CustID", SqlDbType.Int),
            //        new SqlParameter("@FTN", SqlDbType.VarChar)
            //    };
            //pc[0].Value = vendorOrderID;
            //pc[1].Value = h5FolderCustID;
            //pc[2].Value = ftn;

            //var results = _context.Query<VndrOrdr>()
            //    .AsNoTracking()
            //    .FromSql("dbo.getVendorOrdersByIDH5FTN @VendorOrderID, @H5CustID, @FTN", pc.ToArray()).ToList();

            //return results;
        }

        public bool IsOrderCancelled(int orderId)
        {
            bool bCancelled = false;
            var ovo = from c in _context.Ordr where c.OrdrId == orderId select c.OrdrStusId;

            var o = ovo.FirstOrDefault();

            if (o != null && Convert.ToInt32(o) == 4)
                bCancelled = true;

            return bCancelled;

        }

        public Ordr CreateOrder(Ordr entity)
        {
            _context.Ordr.Add(entity);
            SaveAll();

            return entity;
        }
    }
}
