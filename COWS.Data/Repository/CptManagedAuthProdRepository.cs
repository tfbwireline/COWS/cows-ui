﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptManagedAuthProdRepository : ICptManagedAuthProdRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptManagedAuthProdRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptMngdAuthPrd Create(LkCptMngdAuthPrd entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptMngdAuthPrd> Find(Expression<Func<LkCptMngdAuthPrd, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptMngdAuthPrd, out List<LkCptMngdAuthPrd> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptMngdAuthPrd> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptMngdAuthPrd, out List<LkCptMngdAuthPrd> list))
            {
                list = _context.LkCptMngdAuthPrd
                            .OrderBy(i => i.CptMngdAuthPrd)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptMngdAuthPrd, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptMngdAuthPrd GetById(int id)
        {
            return _context.LkCptMngdAuthPrd
                            .SingleOrDefault(i => i.CptMngdAuthPrdId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptMngdAuthPrd);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptMngdAuthPrd entity)
        {
            throw new NotImplementedException();
        }
    }
}