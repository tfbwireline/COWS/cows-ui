﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UcaasEventOdieDeviceRepository : IUcaasEventOdieDeviceRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public UcaasEventOdieDeviceRepository(COWSAdminDBContext context,
            ICommonRepository common)
        {
            _context = context;
            //_commonRepo = new CommonRepository(context);
            _common = common;
        }

        public UcaaSEventOdieDev Create(UcaaSEventOdieDev entity)
        {
            int maxId = _context.UcaaSEventOdieDev.Max(i => i.UcaaSEventOdieDevId);
            entity.UcaaSEventOdieDevId = (short)++maxId;
            _context.UcaaSEventOdieDev.Add(entity);

            SaveAll();

            return GetById(entity.UcaaSEventOdieDevId);
        }

        public void Create(IEnumerable<UcaaSEventOdieDev> entity)
        {
            _context.UcaaSEventOdieDev
                .AddRange(entity.Select(a =>
                {
                    a.UcaaSEventOdieDevId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Delete(int id)
        {
            UcaaSEventOdieDev ucaas = GetById(id);
            _context.UcaaSEventOdieDev.Remove(ucaas);

            SaveAll();
        }

        public void Delete(Expression<Func<UcaaSEventOdieDev, bool>> predicate)
        {
            var toDelete = Find(predicate);
            _context.UcaaSEventOdieDev.RemoveRange(toDelete);
        }

        public IQueryable<UcaaSEventOdieDev> Find(Expression<Func<UcaaSEventOdieDev, bool>> predicate)
        {
            return _context.UcaaSEventOdieDev
                            .Where(predicate);
        }

        public IEnumerable<UcaaSEventOdieDev> GetAll()
        {
            return _context.UcaaSEventOdieDev
                            .OrderBy(i => i.UcaaSEventOdieDevId)
                            .ToList();
        }

        public UcaaSEventOdieDev GetById(int id)
        {
            return _context.UcaaSEventOdieDev
                            .SingleOrDefault(i => i.UcaaSEventOdieDevId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, UcaaSEventOdieDev entity)
        {
            throw new NotImplementedException();
            // Delete
            // Create
        }
    }
}