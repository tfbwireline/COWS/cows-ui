﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class RedesignWorkflowRepository : IRedesignWorkflowRepository
    {
        private readonly COWSAdminDBContext _context;

        public RedesignWorkflowRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public RedsgnWrkflw Create(RedsgnWrkflw entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<RedsgnWrkflw> Find(Expression<Func<RedsgnWrkflw, bool>> predicate)
        {
            return _context.RedsgnWrkflw
                            .Include(i => i.PreWrkflwStus)
                            .Include(i => i.DesrdWrkflwStus)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<RedsgnWrkflw> GetAll()
        {
            return _context.RedsgnWrkflw
                            .Include(i => i.PreWrkflwStus)
                            .Include(i => i.DesrdWrkflwStus)
                            .AsNoTracking()
                            .ToList();
        }

        public RedsgnWrkflw GetById(int id)
        {
            return _context.RedsgnWrkflw
                            .Include(i => i.PreWrkflwStus)
                            .Include(i => i.DesrdWrkflwStus)
                            .SingleOrDefault(a => a.RedsgnWrkflwId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, RedsgnWrkflw entity)
        {
            throw new NotImplementedException();
        }
    }
}
