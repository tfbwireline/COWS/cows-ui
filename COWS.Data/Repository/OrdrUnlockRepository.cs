﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace COWS.Data.Repository
{
    public class OrdrUnlockRepository : IOrdrUnlockRepository
    {
        private readonly COWSAdminDBContext _context;

        public OrdrUnlockRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<GetOrdrUnlockView> unlockItems(int ORDR_ID, int EVENT_ID, int USER_ID, int IsOrder, int Unlock, int IsLocked, int ItemType)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@p0", ORDR_ID),
                new SqlParameter("@p1", EVENT_ID),
                new SqlParameter("@p2", USER_ID),
                new SqlParameter("@p3", IsOrder),
                new SqlParameter("@p4", Unlock),
                new SqlParameter("@p5", IsLocked),
                new SqlParameter("@p6", ItemType)
            };
            var View = _context.Query<GetOrdrUnlockView>()
                .AsNoTracking()
                //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", ftn,null,null,null).ToList<GetOrdrUnlockView>();
                .FromSql("[web].[UnlockItems] @p0, @p1,@p2, @p3, @p4,@p5, @p6", pc.ToArray()).ToList<GetOrdrUnlockView>();
            return View;

            //var View = _context.Query<GetOrdrUnlockView>()
            //    .AsNoTracking()
            //    //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", ftn,null,null,null).ToList<GetOrdrUnlockView>();
            //    .FromSql("[dbo].[lockUnlockItems] @p0, @p1,@p2, @p3, @p4,@p5, @p6", parameters: new[] { ORDR_ID, EVENT_ID, USER_ID, IsOrder, Unlock, IsLocked, ItemType }).ToList<GetOrdrUnlockView>();
            //return View;

            /*
                List<SqlParameter> pc = new List<SqlParameter>
                {
                   new SqlParameter("@customerOrderID", customerProductDelivery.CustomerOrderI),
                   new SqlParameter("@qty", customerProductDelivery.DeliveryQty)
                }

                _context.Database.ExecuteSqlCommand("sp_UpdateProductOrderAndStock @customerOrderID, @qty", pc.ToArray());
             */
        }

        public IEnumerable<GetOrdrUnlockView> GetOrdrUnlockOrdrView(string ftn)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@FTN", ftn),
                new SqlParameter("@EVENTID", ""),
                new SqlParameter("@REDID", ""),
                new SqlParameter("@CPT", "")
            };

            var View = _context.Query<GetOrdrUnlockView>()
                .AsNoTracking()
                .FromSql("[web].[getLockedItems] @FTN, @EVENTID, @REDID, @CPT", pc.ToArray()).ToList<GetOrdrUnlockView>();

            return View;

            //var View = _context.Query<GetOrdrUnlockView>()
            //    .AsNoTracking()
            //    //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", ftn,null,null,null).ToList<GetOrdrUnlockView>();
            //    .FromSql("[web].[getLockedItems] @p0, @p1,@p2, @p3", parameters: new[] { ftn, null, null, null }).ToList<GetOrdrUnlockView>();
            //return View;
        }

        public IEnumerable<GetOrdrUnlockView> GetOrdrUnlockEventView(string EventId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@FTN", ""),
                new SqlParameter("@EVENTID", EventId),
                new SqlParameter("@REDID", ""),
                new SqlParameter("@CPT", "")
            };

            var View = _context.Query<GetOrdrUnlockView>()
                .AsNoTracking()
                .FromSql("[web].[getLockedItems] @FTN, @EVENTID, @REDID, @CPT", pc.ToArray()).ToList<GetOrdrUnlockView>();

            return View;

            //var View = _context.Query<GetOrdrUnlockView>()
            //    .AsNoTracking()
            //    //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", null, OrdrId,null,null).ToList<GetOrdrUnlockView>();
            //    .FromSql("[web].[getLockedItems] @p0, @p1,@p2, @p3", parameters: new[] { null, EventId, null, null }).ToList<GetOrdrUnlockView>();
            //return View;
        }

        public IEnumerable<GetOrdrUnlockView> GetOrdrUnlockRedsgnView(string RedsgnId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@FTN", ""),
                new SqlParameter("@EVENTID", ""),
                new SqlParameter("@REDID", RedsgnId),
                new SqlParameter("@CPT", "")
            };

            var View = _context.Query<GetOrdrUnlockView>()
                .AsNoTracking()
                .FromSql("[web].[getLockedItems] @FTN, @EVENTID, @REDID, @CPT", pc.ToArray()).ToList<GetOrdrUnlockView>();

            return View;

            //var View = _context.Query<GetOrdrUnlockView>()
            //    .AsNoTracking()
            //    //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", null, null, RedsgnId, null).ToList<GetOrdrUnlockView>();
            //    .FromSql("[web].[getLockedItems] @p0, @p1,@p2, @p3", parameters: new[] { null, null, RedsgnId, null }).ToList<GetOrdrUnlockView>();
            //return View;
        }

        public IEnumerable<GetOrdrUnlockView> GetOrdrUnlockCptView(string CptId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@FTN", ""),
                new SqlParameter("@EVENTID", ""),
                new SqlParameter("@REDID", ""),
                new SqlParameter("@CPT", CptId)
            };

            var View = _context.Query<GetOrdrUnlockView>()
                .AsNoTracking()
                .FromSql("[web].[getLockedItems] @FTN, @EVENTID, @REDID, @CPT", pc.ToArray()).ToList<GetOrdrUnlockView>();

            return View;

            //var View = _context.Query<GetOrdrUnlockView>()
            //    .AsNoTracking()
            //    //.FromSql("[web].[getLockedOrders] @FTN,@EVENTID,@REDID,@CPT", null, null, null, CptId).ToList<GetOrdrUnlockView>();
            //    .FromSql("[web].[getLockedItems] @p0, @p1,@p2, @p3", parameters: new[] { null, null, null, CptId }).ToList<GetOrdrUnlockView>();
            //return View;
        }
    }
}