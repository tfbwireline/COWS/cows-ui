﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class VendorOrderMsRepository : IVendorOrderMsRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;
        private IConfiguration _configuration;
        private readonly ILogger<VendorOrderMsRepository> _logger;

        public VendorOrderMsRepository(COWSAdminDBContext context,
            ICommonRepository common,
            IConfiguration configuration,
            ILogger<VendorOrderMsRepository> logger)
        {
            _context = context;
            _common = common;
            _configuration = configuration;
            _logger = logger;
        }
        public IQueryable<VndrOrdrMs> Find(Expression<Func<VndrOrdrMs, bool>> predicate)
        {
            return _context.VndrOrdrMs
                //.Include(a => a.Actn)
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<VndrOrdrMs> GetAll()
        {
            return _context.VndrOrdrMs
                .ToList();
        }

        public VndrOrdrMs GetById(int id)
        {
            return _context.VndrOrdrMs
                .SingleOrDefault(a => a.VndrOrdrMsId == id);
        }

        public VndrOrdrMs Create(VndrOrdrMs entity)
        {
            _context.VndrOrdrMs.Add(entity);
            SaveAll();

            return GetById(entity.VndrOrdrMsId);
        }

        public void Update(int id, VndrOrdrMs entity)
        {
            VndrOrdrMs obj = GetById(id);
            obj.SentToVndrDt = entity.SentToVndrDt;
            obj.AckByVndrDt = entity.AckByVndrDt;

            SaveAll();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}
