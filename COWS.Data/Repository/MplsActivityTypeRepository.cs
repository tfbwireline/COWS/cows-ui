﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MplsActivityTypeRepository : IMplsActivityTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MplsActivityTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMplsActyType Create(LkMplsActyType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMplsActyType> Find(Expression<Func<LkMplsActyType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMplsActyType, out List<LkMplsActyType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMplsActyType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMplsActyType, out List<LkMplsActyType> list))
            {
                list = _context.LkMplsActyType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMplsActyType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMplsActyType GetById(int id)
        {
            return _context.LkMplsActyType
                            .AsNoTracking()
                            .SingleOrDefault(a => a.MplsActyTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMplsActyType);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMplsActyType entity)
        {
            throw new NotImplementedException();
        }
    }
}