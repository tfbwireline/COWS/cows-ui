﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CustomerUserProfileRepository : ICustomerUserProfileRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public CustomerUserProfileRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public CustomerUserProfileView Create(CustomerUserProfileView entity)
        {
            throw new NotImplementedException();
        }

        public int CreateH5CustomerToUserAssignment(CustomerUserProfileView entity)
        {
            //throw new NotImplementedException();
            //int maxId = _context.LkMdsSrvcTier.Max(i => i.MdsSrvcTierId);
            //entity.MdsSrvcTierId = (byte)++maxId;
            //_context.LkMdsSrvcTier.Add(entity);

            //SaveAll();

            //return GetById(entity.MdsSrvcTierId);
            LkUser _user = (from c in _context.LkUser
                            where c.UserId == entity.userId
                            select c).FirstOrDefault();

            int _userID = _user.UserId;

            UserH5Wfm _uhw = (from c in _context.UserH5Wfm
                              where entity.custID == c.CustId && entity.userId == c.UserId
                              select c).FirstOrDefault();
            if (_uhw == null)
            {
                lock (this)
                {
                    UserH5Wfm rec = new UserH5Wfm();

                    rec.CustId = entity.custID;
                    rec.UserId = entity.userId;

                    if (entity.mnsCd == 1)
                    {
                        rec.MnsCd = "Y";
                    }
                    else
                    {
                        rec.MnsCd = "N";
                    }
                    if (entity.isipCd == 1)
                    {
                        rec.IsipCd = "Y";
                    }
                    else
                    {
                        rec.IsipCd = "N";
                    }

                    if (entity.statusId == 1)
                    {
                        rec.RecStusId = (byte)1;
                    }
                    else
                    {
                        rec.RecStusId = (byte)0;
                    }
                    rec.CreatDt = DateTime.Now;
                    rec.ModfdDt = null;

                    _context.UserH5Wfm.Add(rec);
                    SaveAll();
                }
            }
            SendEmailNtfy(entity);
            return entity.custID;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
            //LkMdsSrvcTier st = GetById(id);
            //_context.LkMdsSrvcTier.Remove(st);

            //SaveAll();
        }

        public IQueryable<CustomerUserProfileView> Find(Expression<Func<CustomerUserProfileView, bool>> predicate)
        {
            throw new NotImplementedException();
            //return _context.LkMdsSrvcTier
            //                .Where(predicate);
        }

        public bool CheckH5(int _custid)
        {
            H5Foldr _h5 = (from c in _context.H5Foldr
                           where c.CustId == _custid   //where c.CustId == _custid && c.RecStusId==1
                           select c).FirstOrDefault();

            if (_h5 != null)
            { return true; }
            else
            { return false; }
        }

        public IEnumerable<CustomerUserProfileView> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CustomerUserProfileView> GetAllH5Customer()
        {
            //return _context.LkMdsSrvcTier
            //                .OrderBy(i => i.MdsSrvcTierId)
            //                .ToList();
            //throw new NotImplementedException();

            //CustIdUserProfileDBCntxt.ObjectTrackingEnabled = false;

            List<CustomerUserProfileView> retList = new List<CustomerUserProfileView>();
            CustomerUserProfileView dc = null;
            LkScrdObjType h = (from c in _context.LkScrdObjType
                               where c.ScrdObjType == "H5_FOLDR"
                               select c).FirstOrDefault();
            var profile = (from clu in
                               (from uhw in _context.UserH5Wfm
                                join h5 in _context.H5Foldr on uhw.CustId equals h5.CustId
                                  into joinh5
                                from h5 in joinh5.DefaultIfEmpty()
                                join csd in _context.CustScrdData on new { ScrdObjId = h5.H5FoldrId, ScrdObjTypeId = (byte)h.ScrdObjTypeId } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                                //join csd in _context.CustScrdData on new { SCRD_OBJ_ID = h5.H5FoldrId, SCRD_OBJ_TYPE_ID = (byte)EScrdObjType.H5_FOLDR } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                                    into joincsd
                                from csd in joincsd.DefaultIfEmpty()
                                join u in _context.LkUser on uhw.UserId equals u.UserId
                                  into joinu
                                from u in joinu.DefaultIfEmpty()
                                join lc in _context.LkCtry on h5.CtryCd equals lc.CtryCd
                                  into joinlc
                                from lc in joinlc.DefaultIfEmpty()
                                    //where h5.RecStusId==1
                                select new { uhw, h5, csd, u, lc })

                           select new
                           {
                               clu.uhw.UserH5Id,
                               clu.h5.CustId,
                               clu.h5.CustNme,
                               CUST_NME_CSD = clu.csd.CustNme,
                               clu.h5.CsgLvlId,
                               clu.u.DsplNme,
                               clu.u.UserId,
                               clu.uhw.MnsCd,
                               clu.uhw.IsipCd,
                               clu.uhw.RecStusId,
                               clu.lc.CtryNme,
                               CTRY_NME_CSD = clu.csd.CtryCd,
                               clu.uhw.CreatDt,
                               clu.uhw.ModfdDt
                           });

            foreach (var d in profile)
            {
                dc = new CustomerUserProfileView();
                dc.usrH5Id = d.UserH5Id;
                dc.custID = d.CustId;
                dc.custNme = (d.CsgLvlId == 0) ? d.CustNme : _common.GetDecryptValue(d.CUST_NME_CSD);
                dc.crtyNme = (d.CsgLvlId == 0) ? d.CtryNme : (d.CTRY_NME_CSD == null ? string.Empty : GetCountryName(_common.GetDecryptValue(d.CTRY_NME_CSD)));
                dc.usrNme = d.DsplNme;
                dc.userId = d.UserId;
                if (d.MnsCd == "Y")
                { dc.mns = "Yes"; dc.mnsCd = 1; }
                if (d.MnsCd == "N")
                { dc.mns = "No"; dc.mnsCd = 0; }
                if (d.IsipCd == "Y")
                { dc.isip = "Yes"; dc.isipCd = 1; }
                if (d.IsipCd == "N")
                { dc.isip = "No"; dc.isipCd = 0; }
                if (d.RecStusId == 1)
                { dc.status = "Enabled"; dc.statusId = 1; }
                else
                { dc.status = "Disabled"; dc.statusId = 0; }
                if (d.ModfdDt == null)
                { dc.assignDT = d.CreatDt; }
                else
                { dc.assignDT = d.ModfdDt; }

                retList.Add(dc);
            }

            return retList;
        }

        public string GetCountryName(string sCtryCd)
        {
            LkCtry q = (from lct in _context.LkCtry
                        where (lct.RecStusId == (byte)1 && lct.CtryCd == sCtryCd.Trim())
                        orderby lct.CtryNme
                        select lct).FirstOrDefault();
            return q.CtryNme;
        }

        public CustomerUserProfileView GetById(int id)
        {
            throw new NotImplementedException();
            //return _context.UserH5Wfm
            //                .SingleOrDefault(i => i.UserH5Id == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public bool SendEmailNtfy(CustomerUserProfileView entity)
        {
            LkUser _user = (from c in _context.LkUser
                            where c.UserId == entity.userId
                            select c).FirstOrDefault();
            LkScrdObjType h5 = (from c in _context.LkScrdObjType
                                where c.ScrdObjType == "H5_FOLDR"
                                select c).FirstOrDefault();
            string _userEmail = _user.EmailAdr;

            var _h5 = (from c in _context.H5Foldr
                       join csd in _context.CustScrdData on new { ScrdObjId = c.H5FoldrId, ScrdObjTypeId = (byte)h5.ScrdObjTypeId } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                               into joincsd
                       from csd in joincsd.DefaultIfEmpty()
                       where entity.custID == c.CustId
                       select new { c.CustNme, c.CsgLvlId, CUST_NME_CSD = csd.CustNme }).FirstOrDefault();

            string _custName = (_h5.CsgLvlId == 0) ? _h5.CustNme : _common.GetDecryptValue(_h5.CUST_NME_CSD);

            if (_h5 != null)
            {
                string _emailSubTxt = "H5 Customer Assignment Notification";
                string _emailListTxt = _userEmail;
                string mns = entity.mnsCd == 1 ? "Yes" : "No";
                string isip = entity.isipCd == 1 ? "Yes" : "No";
                string status = entity.statusId == 1 ? "Enabled" : "Disabled";

                string _emailBodyTxt = "<CustIDUserAssignEmail><H5_CUST_ID>" + entity.custID + "</H5_CUST_ID>"
                                    + "<H5_CUST_NME>" + _custName + "</H5_CUST_NME>"
                                    + "<DSPL_NME>" + _user.DsplNme + "</DSPL_NME>"
                                    + "<MNS>" + mns + "</MNS>"
                                    + "<ISIP>" + isip + "</ISIP>"
                                    + "<Stus>" + status + "</Stus>"
                                    + "<Date>" + DateTime.Now + "</Date>"
                                    + "</CustIDUserAssignEmail>";
                lock (this)
                {
                    EmailReq rec = new EmailReq();

                    rec.EmailBodyTxt = _emailBodyTxt;
                    rec.EmailListTxt = _emailListTxt;
                    rec.EmailSubjTxt = _emailSubTxt;
                    rec.EmailReqTypeId = 16;
                    rec.StusId = 10;
                    rec.CreatDt = DateTime.Now;

                    _context.EmailReq.Add(rec);
                    SaveAll();
                }
            }

            return true;
        }

        public void Update(int id, CustomerUserProfileView entity)
        {
            //LkMdsSrvcTier telco = GetById(id);
            //telco.MdsSrvcTierDes = entity.MdsSrvcTierDes;
            //telco.EventTypeId = entity.EventTypeId;
            //telco.RecStusId = entity.RecStusId;
            //telco.ModfdByUserId = entity.ModfdByUserId;
            //telco.ModfdDt = entity.ModfdDt;
            ////LkEventType eventType = _context.LkEventType
            ////                                .Find(entity.EventTypeId);
            ////if (eventType != null)
            ////{
            ////    telco.EventTypeId = entity.EventTypeId;
            ////    //telco.EventTypeNme = entity.EventTypeNme;
            ////}
            //SaveAll();
            LkUser _user = (from c in _context.LkUser
                            where c.DsplNme == entity.usrNme
                            select c).FirstOrDefault();

            //int _userID = _user.UserId;

            //UserH5Wfm _uhw = (from c in _context.UserH5Wfm
            //                  where entity.custID == c.CustId && _userID == c.UserId
            //                  select c).FirstOrDefault();
            //if (_uhw != null)
            //{
            //    lock (this)
            //    {
            //UserH5Wfm rec = new UserH5Wfm();
            UserH5Wfm rec = _context.UserH5Wfm
                   .SingleOrDefault(i => i.UserH5Id == id);
            rec.CustId = entity.custID;
            rec.UserId = entity.userId;

            if (entity.mnsCd == 1)
            {
                rec.MnsCd = "Y";
            }
            else
            {
                rec.MnsCd = "N";
            }
            if (entity.isipCd == 1)
            {
                rec.IsipCd = "Y";
            }
            else
            {
                rec.IsipCd = "N";
            }

            if (entity.statusId == 1)
            {
                rec.RecStusId = (byte)1;
            }
            else
            {
                rec.RecStusId = (byte)0;
            }
            //rec.CreatDt = DateTime.Now;
            rec.ModfdDt = DateTime.Now;

            //_context.UserH5Wfm.Add(rec);
            SaveAll();
            SendEmailNtfy(entity);
            //    }
            //}
        }
    }
}