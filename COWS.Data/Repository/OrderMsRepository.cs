﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderMsRepository : IOrderMsRepository
    {
        private readonly COWSAdminDBContext _context;

        public OrderMsRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<OrdrMs> Find(Expression<Func<OrdrMs, bool>> predicate)
        {
            return _context.OrdrMs
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<OrdrMs> GetAll()
        {
            return _context.OrdrMs.ToList();
        }

        public OrdrMs GetById(int id)
        {
            return Find(a => a.OrdrId == id).SingleOrDefault();
        }

        public OrdrMs Create(OrdrMs entity)
        {
            _context.OrdrMs.Add(entity);
            SaveAll();

            return GetById(entity.OrdrId);
        }

        public void Update(int id, OrdrMs entity)
        {
            OrdrMs orderMs = GetById(id);
            if (orderMs == null)
            {
                orderMs = new OrdrMs();
                orderMs.OrdrId = entity.OrdrId;
                orderMs.VerId = 1;
                orderMs.SbmtDt = entity.SbmtDt;
                orderMs.VldtdDt = entity.VldtdDt;
                orderMs.OrdrDscnctDt = entity.OrdrDscnctDt;
                orderMs.OrdrBillClearInstlDt = entity.OrdrBillClearInstlDt;
                orderMs.CustAcptcTurnupDt = entity.CustAcptcTurnupDt;
                orderMs.VndrCnclnDt = entity.VndrCnclnDt;
                orderMs.CreatByUserId = entity.CreatByUserId;
                orderMs.CreatDt = entity.CreatDt;

                _context.OrdrMs.Add(orderMs);
            }
            else
            {
                orderMs.SbmtDt = entity.SbmtDt;
                orderMs.VldtdDt = entity.VldtdDt;
                orderMs.OrdrDscnctDt = entity.OrdrDscnctDt;
                orderMs.OrdrBillClearInstlDt = entity.OrdrBillClearInstlDt;
                orderMs.CustAcptcTurnupDt = entity.CustAcptcTurnupDt;
                orderMs.VndrCnclnDt = entity.VndrCnclnDt;
            }

            //order.OrdrMs = entity.OrdrMs;
            //order.Ckt = entity.Ckt;
        }

        public void Delete(int id)
        {
            var toDelete = GetById(id);
            _context.OrdrMs.Remove(toDelete);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public int UpdateBillClearDate(int orderId, DateTime? billClearDate)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@DATE", SqlDbType.DateTime)
                };
            pc[0].Value = orderId;
            pc[1].Value = (object)billClearDate ?? DBNull.Value;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.updateBillClearDt @ORDR_ID,@DATE", pc.ToArray());
            return toReturn;
        }
    }
}