﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class SPLKEventRepository : ISPLKEventRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public SPLKEventRepository(COWSAdminDBContext context,
            ICommonRepository common)
        {
            _context = context;
            _common = common;
            //_configuration = configuration;
        }

        //public IConfiguration _configuration { get; }
        //private IConfiguration configuration;
        public SplkEvent GetSplkEventById(int id, string Adid)
        {
            SplkEvent data = new SplkEvent();
            data = _context.SplkEvent
                            .Include(i => i.CreatByUser)
                            .Include(i => i.EventStus)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.WrkflwStus)
                            .Include(i => i.IpVer)
                            .Include(i => i.EsclReas)
                            .Include(i => i.ReqorUser)
                            .Include(i => i.SalsUser)
                            .Include(i => i.WrkflwStus)
                            .Include(i => i.SplkEventAccs)
                            .Include(i => i.Event)
                            .SingleOrDefault(i => i.EventId == id);
            var dc = new SplkEvent();
            var csg = GetCSGLevelByeventId(data.EventId);
            csg = data.Event.CsgLvlId;
            if (data != null)
            {
                dc.EventId = data.EventId;
                dc.EventStusId = data.EventStusId;
                dc.Ftn = data.Ftn;
                dc.CharsId = data.CharsId;
                dc.H1 = data.H1;
                dc.H6 = data.H6;
                dc.ReqorUserId = data.ReqorUserId;
                dc.SalsUserId = data.SalsUserId;
                dc.PubEmailCcTxt = data.PubEmailCcTxt;
                dc.CmpltdEmailCcTxt = data.CmpltdEmailCcTxt;
                dc.DsgnCmntTxt = data.DsgnCmntTxt;
                dc.SplkEventTypeId = data.SplkEventTypeId;
                dc.SplkActyTypeId = data.SplkActyTypeId;
                dc.IpVerId = data.IpVerId;
                dc.MdsMngdCd = data.MdsMngdCd;
                dc.EsclCd = data.EsclCd;
                dc.PrimReqDt = data.PrimReqDt;
                dc.ScndyReqDt = data.ScndyReqDt;
                dc.EsclReasId = data.EsclReasId;
                dc.StrtTmst = data.StrtTmst;
                dc.ExtraDrtnTmeAmt = data.ExtraDrtnTmeAmt;
                dc.EndTmst = data.EndTmst;
                dc.WrkflwStusId = data.WrkflwStusId;
                dc.RecStusId = data.RecStusId;
                dc.CreatByUserId = data.CreatByUserId;
                dc.ModfdByUserId = data.ModfdByUserId;
                dc.ModfdDt = data.ModfdDt;
                dc.CreatDt = data.CreatDt;
                dc.CnfrcBrdgNbr = data.CnfrcBrdgNbr;
                dc.CnfrcPinNbr = data.CnfrcPinNbr;
                dc.EventDrtnInMinQty = data.EventDrtnInMinQty;
                dc.SowsEventId = data.SowsEventId;
                dc.ReltdCmpsNcrCd = data.ReltdCmpsNcrCd;
                dc.ReltdCmpsNcrNme = data.ReltdCmpsNcrNme;

                if (csg > 0)
                {
                    GetEventDecryptValues sdata = _common.GetEventDecryptValues(dc.EventId, "SPLK").FirstOrDefault();

                    if (sdata != null)
                    {
                        dc.CustNme = sdata.CUST_NME;
                        dc.CustCntctNme = sdata.CUST_CNTCT_NME;
                        dc.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                        dc.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                        dc.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                        dc.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                        dc.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                        dc.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                        dc.EventDes = sdata.EVENT_DES;
                    }

                    // Sarah Sandoval [20190621] - Commented for the mean time
                    // Test the method above if working if not revert to this change
                    //DataRow drDec = _common.GetEventDecryptValues2(dc.EventId, "SPLK");
                    //if (drDec != null)
                    //{
                    //    dc.CustNme = drDec[0].ToString();
                    //    dc.CustCntctNme = drDec[1].ToString();
                    //    dc.CustCntctPhnNbr = drDec[2].ToString();
                    //    dc.CustEmailAdr = drDec[3].ToString();
                    //    dc.CustCntctCellPhnNbr = drDec[4].ToString();
                    //    dc.CustCntctPgrNbr = drDec[5].ToString();
                    //    dc.CustCntctPgrPinNbr = drDec[6].ToString();
                    //    dc.EventTitleTxt = drDec[7].ToString();
                    //    dc.EventDes = drDec[8].ToString();
                    //}
                }
                else
                {
                    dc.CustNme = data.CustNme;
                    dc.CustCntctNme = data.CustCntctNme;
                    dc.CustCntctPhnNbr = data.CustCntctPhnNbr;
                    dc.CustEmailAdr = data.CustEmailAdr;
                    dc.CustCntctCellPhnNbr = data.CustCntctCellPhnNbr;
                    dc.CustCntctPgrNbr = data.CustCntctPgrNbr;
                    dc.CustCntctPgrPinNbr = data.CustCntctPgrPinNbr;
                    dc.EventTitleTxt = data.EventTitleTxt;
                    dc.EventDes = data.EventDes;
                }
                dc.CreatByUser = data.CreatByUser;
                dc.EsclReas = data.EsclReas;
                dc.Event = data.Event;
                dc.EventStus = data.EventStus;
                dc.IpVer = data.IpVer;
                dc.ModfdByUser = data.ModfdByUser;
                dc.RecStus = data.RecStus;
                dc.ReqorUser = data.ReqorUser;
                dc.SalsUser = data.SalsUser;
                dc.SplkActyType = data.SplkActyType;
                dc.SplkEventType = data.SplkEventType;
                dc.WrkflwStus = data.WrkflwStus;
                dc.SplkEventAccs = data.SplkEventAccs;
            }
            //OrderSearch = 1,
            //EventSearch = 2,
            //RedesignSearch = 3,
            //OrderDetails = 4,
            //EventDetails = 5,
            //RedesignDetails = 6
            //var UserCsg = GetCSGLevelAdId(Adid);
            if (csg > 0 && Adid != null)
            {
                var UserCsg = GetCSGLevelAdId(Adid);
                _common.LogWebActivity(((byte)WebActyType.EventDetails), dc.EventId.ToString(), Adid, (byte)UserCsg, (byte)csg);
            }
            return dc;
        }

        public string GetSysCfgValue(string sParamName)
        {
            var exists = (from c in _context.LkSysCfg
                          where (c.PrmtrNme == sParamName && c.RecStusId == 1)
                          select c.PrmtrValuTxt);

            if (exists.Count() > 0)
                return exists.First();
            else
                return string.Empty;
        }

        public IEnumerable<LkSplkActyType> GetAllSPLKActyType()
        {
            List<LkSplkActyType> splkActTpeList = new List<LkSplkActyType>();
            LkSplkActyType dc = null;
            var q = (from pt in _context.LkSplkActyType
                     where (pt.RecStusId == 1 && pt.SplkActyTypeDes != "")
                     orderby pt.SplkActyTypeId
                     select new { pt.SplkActyTypeId, pt.SplkActyTypeDes });
            foreach (var d in q)
            {
                dc = new LkSplkActyType();
                dc.SplkActyTypeId = d.SplkActyTypeId;
                dc.SplkActyTypeDes = d.SplkActyTypeDes;
                splkActTpeList.Add(dc);
            }
            return splkActTpeList;
        }

        public IEnumerable<LkSplkEventType> GetAllSPLKEventType()
        {
            List<LkSplkEventType> splkEventTpeList = new List<LkSplkEventType>();
            LkSplkEventType dc = null;
            var q = (from pt in _context.LkSplkEventType
                     where (pt.RecStusId == 1)
                     orderby pt.SplkEventTypeId
                     select new { pt.SplkEventTypeId, pt.SplkEventTypeDes });
            foreach (var d in q)
            {
                dc = new LkSplkEventType();
                dc.SplkEventTypeId = d.SplkEventTypeId;
                dc.SplkEventTypeDes = d.SplkEventTypeDes;
                splkEventTpeList.Add(dc);
            }
            return splkEventTpeList;
        }

        public bool StatusChanged(int eventID, byte eEventStatus, byte wfStatus)
        {
            var statuses = (from e in _context.SiptEvent
                            where e.EventId == eventID
                            select new { e.EventStusId, e.WrkflwStusId }).SingleOrDefault();

            if (statuses.EventStusId != eEventStatus || statuses.WrkflwStusId != wfStatus)
                return true;
            else
                return false;
        }

        public IEnumerable<SplkEvent> GetAll()
        {
            return _context.SplkEvent
                .ToList();
        }

        public IQueryable<SplkEvent> Find(Expression<Func<SplkEvent, bool>> predicate)
        {
            IQueryable<SplkEvent> splk = _context.SplkEvent
                            .Include(i => i.Event)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.EventStus)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.WrkflwStus)
                            .Include(i => i.IpVer)
                            .Include(i => i.EsclReas)
                            .Include(i => i.ReqorUser)
                            .Include(i => i.SalsUser)
                            .Include(i => i.WrkflwStus)
                            .Include(i => i.SplkEventAccs)
                            .Where(predicate);
            foreach (SplkEvent dc in splk)
            {
                if (dc.Event.CsgLvlId > 0)
                {
                    GetEventDecryptValues sdata = _common.GetEventDecryptValues(dc.EventId, "SPLK").FirstOrDefault();

                    if (sdata != null)
                    {
                        dc.CustNme = sdata.CUST_NME;
                        dc.CustCntctNme = sdata.CUST_CNTCT_NME;
                        dc.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                        dc.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                        dc.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                        dc.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                        dc.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                        dc.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                        dc.EventDes = sdata.EVENT_DES;
                    }

                    // Sarah Sandoval [20190621] - Commented for the mean time
                    // Test the method above if working if not revert to this change
                    //DataRow drDec = _common.GetEventDecryptValues2(dc.EventId, "SPLK");
                    //if (drDec != null)
                    //{
                    //    dc.CustNme = drDec[0].ToString();
                    //    dc.CustCntctNme = drDec[1].ToString();
                    //    dc.CustCntctPhnNbr = drDec[2].ToString();
                    //    dc.CustEmailAdr = drDec[3].ToString();
                    //    dc.CustCntctCellPhnNbr = drDec[4].ToString();
                    //    dc.CustCntctPgrNbr = drDec[5].ToString();
                    //    dc.CustCntctPgrPinNbr = drDec[6].ToString();
                    //    dc.EventTitleTxt = drDec[7].ToString();
                    //    dc.EventDes = drDec[8].ToString();
                    //}
                }
            }
            return splk;
        }

        public SplkEvent GetById(int id)
        {
            var dc = _context.SplkEvent
                            .Include(i => i.Event)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.EventStus)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.WrkflwStus)
                            .Include(i => i.IpVer)
                            .Include(i => i.EsclReas)
                            .Include(i => i.ReqorUser)
                            .Include(i => i.SalsUser)
                            .Include(i => i.WrkflwStus)
                            .Include(i => i.SplkEventAccs)
                            .SingleOrDefault(i => i.EventId == id);

            if (dc.Event.CsgLvlId > 0)
            {
                //GetEventDecryptValues sdata = GetEventDecryptValues(item.EventId, "SPLK").FirstOrDefault();
                //item.CustNme = sdata.CUST_NME;
                //item.CustCntctNme = sdata.CUST_CNTCT_NME;
                //item.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                //item.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                //item.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                //item.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                //item.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                //item.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                //item.EventDes = sdata.EVENT_DES;
                GetEventDecryptValues sdata = _common.GetEventDecryptValues(dc.EventId, "SPLK").FirstOrDefault();

                if (sdata != null)
                {
                    dc.CustNme = sdata.CUST_NME;
                    dc.CustCntctNme = sdata.CUST_CNTCT_NME;
                    dc.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                    dc.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                    dc.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                    dc.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                    dc.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                    dc.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                    dc.EventDes = sdata.EVENT_DES;
                }

                //DataRow drDec = _common.GetEventDecryptValues2(dc.EventId, "SPLK");
                //    if (drDec != null)
                //    {
                //        dc.CustNme = drDec[0].ToString();
                //        dc.CustCntctNme = drDec[1].ToString();
                //        dc.CustCntctPhnNbr = drDec[2].ToString();
                //        dc.CustEmailAdr = drDec[3].ToString();
                //        dc.CustCntctCellPhnNbr = drDec[4].ToString();
                //        dc.CustCntctPgrNbr = drDec[5].ToString();
                //        dc.CustCntctPgrPinNbr = drDec[6].ToString();
                //        dc.EventTitleTxt = drDec[7].ToString();
                //        dc.EventDes = drDec[8].ToString();

                //    }
            }

            return dc;
        }

        private byte GetH1CSGLvl()
        {
            var sl = (from c in _context.LkCsgLvl select c.CsgLvlId).Distinct();
            if (sl != null && sl.Count() == 1)
                return sl.First();
            else
                return sl.Take(1).Single();
        }

        public int GetCSGLevelByeventId(int id)
        {
            var csgId = (from u in _context.Event
                         where u.EventId == id
                         select new { u.CsgLvlId }).SingleOrDefault();
            return int.Parse(csgId.CsgLvlId.ToString());
        }

        public int GetCSGLevelAdId(string AdId)
        {
            var csgId = (from u in _context.LkUser
                         join c in _context.UserCsgLvl on u.UserId equals c.UserId
                         where u.UserAdid == AdId
                         orderby c.CsgLvlId
                         select new { c.CsgLvlId }).LastOrDefault();
            byte myCsg = 0;
            if (csgId != null)
            {
                myCsg = csgId.CsgLvlId;
            }
            return int.Parse(myCsg.ToString());
        }

        private void AssignCommonFields(SplkEvent data, ref SplkEvent dc, ref CustScrdData CSDTbl, byte csg, int eventId)
        {
            SplkEncryptContactModel dc2 = new SplkEncryptContactModel();
            if (dc.EventId > 0) { }
            else dc.EventId = eventId; // data.EventId;
            dc.EventStusId = data.EventStusId;
            dc.Ftn = data.Ftn;
            dc.CharsId = data.CharsId;
            dc.H1 = data.H1;
            dc.H6 = data.H6;
            dc.ReqorUserId = data.ReqorUserId == 0 ? null : data.ReqorUserId;
            dc.SalsUserId = data.SalsUserId == 0 ? null : data.SalsUserId;
            dc.PubEmailCcTxt = data.PubEmailCcTxt;
            dc.CmpltdEmailCcTxt = data.CmpltdEmailCcTxt;
            dc.DsgnCmntTxt = data.DsgnCmntTxt;
            dc.SplkEventTypeId = data.SplkEventTypeId;
            dc.SplkActyTypeId = data.SplkActyTypeId;
            dc.IpVerId = data.IpVerId;
            dc.MdsMngdCd = data.MdsMngdCd;
            dc.EsclCd = data.EsclCd;
            dc.PrimReqDt = data.PrimReqDt;
            dc.ScndyReqDt = data.ScndyReqDt;
            dc.EsclReasId = data.EsclReasId;
            dc.StrtTmst = data.StrtTmst;
            dc.ExtraDrtnTmeAmt = data.ExtraDrtnTmeAmt;
            dc.EndTmst = data.EndTmst;
            dc.WrkflwStusId = data.WrkflwStusId;
            dc.RecStusId = data.RecStusId;
            //dc.CreatByUserId = data.CreatByUserId;
            //dc.ModfdByUserId = data.ModfdByUserId;
            //dc.ModfdDt = data.ModfdDt;
            //dc.CreatDt = data.CreatDt;
            dc.CnfrcBrdgNbr = data.CnfrcBrdgNbr;
            dc.CnfrcPinNbr = data.CnfrcPinNbr;
            dc.EventDrtnInMinQty = data.EventDrtnInMinQty;
            dc.SowsEventId = data.SowsEventId;
            dc.ReltdCmpsNcrCd = data.ReltdCmpsNcrCd;
            dc.ReltdCmpsNcrNme = data.ReltdCmpsNcrNme;

            //dc.CreatByUserId = data.CreatByUserId;
            dc.EsclReasId = data.EsclReasId;
            //dc.Event = data.Event;
            dc.EventStusId = data.EventStusId;
            dc.IpVerId = data.IpVerId;
            dc.ModfdByUserId = data.ModfdByUserId;
            dc.RecStusId = data.RecStusId;
            dc.ReqorUserId = data.ReqorUserId;
            dc.SalsUserId = data.SalsUserId;
            dc.SplkActyTypeId = data.SplkActyTypeId;
            dc.SplkEventTypeId = data.SplkEventTypeId;
            //dc.WrkflwStus = data.WrkflwStus;
            //dc.SplkEventAccs = data.SplkEventAccsTag;

            dc2.CustNme = data.CustNme;
            dc2.CustCntctNme = data.CustCntctNme;
            dc2.CustCntctPhnNbr = data.CustCntctPhnNbr;
            dc2.CustEmailAdr = data.CustEmailAdr;
            dc2.CustCntctCellPhnNbr = data.CustCntctCellPhnNbr;
            dc2.CustCntctPgrNbr = data.CustCntctPgrNbr;
            dc2.CustCntctPgrPinNbr = data.CustCntctPgrPinNbr;
            dc2.EventTitleTxt = data.EventTitleTxt;
            dc2.EventDes = data.EventDes;
            if (csg > 0)
            {
                SecuredData obj = new SecuredData(dc2);
                List<GetEncryptValues> EncValues = _common.GetEventEncryptValues(obj).ToList();
                if (EncValues != null)
                {
                    CSDTbl.ScrdObjId = eventId;
                    CSDTbl.ScrdObjTypeId = (byte)SecuredObjectType.SPLK_EVENT;
                    CSDTbl.CustNme = EncValues[0].Item;
                    CSDTbl.CustCntctNme = EncValues[1].Item;
                    CSDTbl.CustCntctPhnNbr = EncValues[2].Item;
                    CSDTbl.CustEmailAdr = EncValues[3].Item;
                    CSDTbl.CustCntctCellPhnNbr = EncValues[4].Item;
                    CSDTbl.CustCntctPgrNbr = EncValues[5].Item;
                    CSDTbl.CustCntctPgrPinNbr = EncValues[6].Item;
                    CSDTbl.EventTitleTxt = EncValues[7].Item;
                    CSDTbl.EventDes = EncValues[8].Item;

                    dc.CustNme = null;
                    dc.CustCntctNme = null;
                    dc.CustCntctPhnNbr = null;
                    dc.CustEmailAdr = null;
                    dc.CustCntctCellPhnNbr = null;
                    dc.CustCntctPgrNbr = null;
                    dc.CustCntctPgrPinNbr = null;
                    dc.EventTitleTxt = null;
                    dc.EventDes = null;
                }
            }
            else
            {
                dc.CustNme = data.CustNme;
                dc.CustCntctNme = data.CustCntctNme;
                dc.CustCntctPhnNbr = data.CustCntctPhnNbr;
                dc.CustEmailAdr = data.CustEmailAdr;
                dc.CustCntctCellPhnNbr = data.CustCntctCellPhnNbr;
                dc.CustCntctPgrNbr = data.CustCntctPgrNbr;
                dc.CustCntctPgrPinNbr = data.CustCntctPgrPinNbr;
                dc.EventTitleTxt = data.EventTitleTxt;
                dc.EventDes = data.EventDes;
            }
            if ((((byte)data.EventStusId == (byte)EventStatus.Visible))
                && (((byte)data.WrkflwStusId == (byte)WorkflowStatus.Visible)
                    || (((byte)data.WrkflwStusId == (byte)WorkflowStatus.Submit))))
                dc.WrkflwStusId = (byte)data.WrkflwStusId == 0 ? (byte)WorkflowStatus.Submit : (byte)data.WrkflwStusId;
            else
                dc.WrkflwStusId = (byte)data.WrkflwStusId == 0 ? (byte)WorkflowStatus.Submit : (byte)data.WrkflwStusId;
        }

        public SplkEvent Create(SplkEvent Obj, string adId)
        {
            int newEvtId = 0;
            byte CSGLvlID = Obj.Event.CsgLvlId; // GetH1CSGLvl();  // Find out What this logic be
            lock (this)
            {
                Event evt = new Event();
                evt.EventTypeId = (int)EventType.SprintLink;
                evt.CsgLvlId = CSGLvlID;
                evt.CreatDt = DateTime.Now;
                _context.Event.Add(evt);
                SaveAll();

                newEvtId = (int)(from e in _context.Event select (int?)e.EventId).Max();
            }

            SplkEvent newSIPT = new SplkEvent();
            CustScrdData newCSD = new CustScrdData();

            //SaveAll();

            // Add SplkEventAccs
            if (Obj.SplkEventAccs != null && Obj.SplkEventAccs.Count > 0)
            {
                List<SplkEventAccs> splkAccsTag = new List<SplkEventAccs>();
                SplkEventAccs dc = null;
                foreach (var d in Obj.SplkEventAccs)
                {
                    dc = new SplkEventAccs();
                    dc.EventId = newEvtId;
                    dc.OldPlNbr = d.OldPlNbr;
                    dc.OldPortSpeedDes = d.OldPortSpeedDes;
                    dc.NewPlNbr = d.NewPlNbr;
                    dc.NewPortSpeedDes = d.NewPortSpeedDes;
                    dc.CreatDt = DateTime.Now;
                    splkAccsTag.Add(dc);
                    _context.SplkEventAccs.Add(dc);
                }
            }
            //// Add EventASNUser
            //if (Obj.Activators != null && Obj.Activators.Count > 0)
            //{
            //    List<EventAsnToUser> EventAsnUser = new List<EventAsnToUser>();
            //    EventAsnToUser dc = null;
            //    foreach (var d in Obj.Activators)
            //    {
            //        dc = new EventAsnToUser();
            //        dc.EventId = newEvtId;
            //        dc.AsnToUserId = (int)d;
            //        dc.ModfdDt = DateTime.Now;
            //        dc.RecStusId = 1;
            //        dc.RoleId = 23;   ///Change Later
            //        dc.CreatDt = DateTime.Now;
            //        EventAsnUser.Add(dc);
            //        _context.EventAsnToUser.Add(dc);
            //    }
            //}
            AssignCommonFields(Obj, ref newSIPT, ref newCSD, CSGLvlID, newEvtId);
            newSIPT.EventId = newEvtId;
            newSIPT.CreatByUserId = Obj.CreatByUserId;
            newSIPT.CreatDt = DateTime.Now;
            newSIPT.ModfdByUserId = Obj.ModfdByUserId;
            newSIPT.ModfdDt = DateTime.Now;
            if (newCSD.ScrdObjId > 0)
            {
                newCSD.CreatDt = DateTime.Now;
                _context.CustScrdData.Add(newCSD);
            }
            _context.SplkEvent.Add(newSIPT);
            SaveAll();
            //iEventID = newEvtId;

            return GetSplkEventById(newSIPT.EventId, adId);
        }

        public bool UpdateSplkEvent(int id, SplkEvent Obj)
        {
            SplkEvent newSIPT = new SplkEvent();
            byte CSGLvlID = Obj.Event.CsgLvlId; // GetH1CSGLvl();  // Find out What this logic be
            byte OldCSGLvlID = (byte)GetCSGLevelByeventId(id);
            SplkEvent updtSIPT = GetById(id); //(SplkEvent)(from se in _context.SplkEvent where se.EventId == id select se).Single();
            CustScrdData updtCSD = (CustScrdData)(from csd in _context.CustScrdData where csd.ScrdObjId == id && csd.ScrdObjTypeId == ((byte)SecuredObjectType.SPLK_EVENT) select csd).SingleOrDefault();

            updtSIPT.ModfdByUserId = Obj.ModfdByUserId;
            updtSIPT.ModfdDt = DateTime.Now;
            bool bInsertCSD = false;
            if (updtCSD == null)
            {
                updtCSD = new CustScrdData();
                bInsertCSD = true;
            }
            //AssignCommonFields(ref updtSIPT, ref updtCSD, ref Obj);
            AssignCommonFields(Obj, ref updtSIPT, ref updtCSD, CSGLvlID, id);

            // Delete all existing SplkEventAccs
            var splkAccsTg = _context.SplkEventAccs.Where(i => i.EventId == id).ToList();
            if (splkAccsTg != null && splkAccsTg.Count > 0)
            {
                _context.SplkEventAccs.RemoveRange(splkAccsTg);
                SaveAll();
            }
            // Delete all existing EventASUSER
            var eventASNUser = _context.EventAsnToUser.Where(i => i.EventId == id).ToList();
            if (eventASNUser != null && eventASNUser.Count > 0)
            {
                _context.EventAsnToUser.RemoveRange(eventASNUser);
                SaveAll();
            }
            //SaveAll();

            // Add SplkEventAccs
            if (Obj.SplkEventAccs != null && Obj.SplkEventAccs.Count > 0)
            {
                List<SplkEventAccs> splkAccsTag = new List<SplkEventAccs>();
                SplkEventAccs dc = null;
                foreach (var d in Obj.SplkEventAccs)
                {
                    dc = new SplkEventAccs();
                    dc.EventId = id;
                    dc.OldPlNbr = d.OldPlNbr;
                    dc.OldPortSpeedDes = d.OldPortSpeedDes;
                    dc.NewPlNbr = d.NewPlNbr;
                    dc.NewPortSpeedDes = d.NewPortSpeedDes;
                    dc.CreatDt = DateTime.Now;
                    splkAccsTag.Add(dc);
                    _context.SplkEventAccs.Add(dc);
                }
            }
            //// Add EventASNUser
            //if (Obj.Activators != null && Obj.Activators.Count > 0)
            //{
            //    List<EventAsnToUser> EventAsnUser = new List<EventAsnToUser>();
            //    EventAsnToUser dc = null;
            //    foreach (var d in Obj.Activators)
            //    {
            //        dc = new EventAsnToUser();
            //        dc.EventId = id;
            //        dc.AsnToUserId = (int)d;
            //        dc.ModfdDt = DateTime.Now;
            //        dc.RecStusId = 1;
            //        dc.RoleId = 23;   ///Change Later
            //        dc.CreatDt = DateTime.Now;
            //        EventAsnUser.Add(dc);
            //        _context.EventAsnToUser.Add(dc);
            //    }
            //}

            //if ((OldCSGLvlID != CSGLvlID) && (CSGLvlID > 0))
            if ((OldCSGLvlID != CSGLvlID))
            {
                Event updtEv = (Event)(from ev in _context.Event where ev.EventId == id select ev).Single();
                updtEv.CsgLvlId = CSGLvlID;
                _context.Event.Update(updtEv);
            }

            if ((bInsertCSD) && (CSGLvlID > 0) && (updtCSD.ScrdObjId > 0))
            {
                updtCSD.CreatDt = DateTime.Now;
                _context.CustScrdData.Update(updtCSD);
            }
            else if ((!bInsertCSD) && (CSGLvlID == 0) && (updtCSD.ScrdObjId > 0))
                _context.CustScrdData.Remove(updtCSD);

            _context.SplkEvent.Update(updtSIPT);
            SaveAll();

            return true;
        }

        public SplkEvent Create(SplkEvent entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, SplkEvent entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}