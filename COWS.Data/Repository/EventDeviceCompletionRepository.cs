﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventDeviceCompletionRepository : IEventDeviceCompletionRepository
    {
        private readonly COWSAdminDBContext _context;

        public EventDeviceCompletionRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<EventDevCmplt> Find(Expression<Func<EventDevCmplt, bool>> predicate)
        {
            return _context.EventDevCmplt
                            .Include(i => i.Event)
                            .Where(predicate);
        }

        public IEnumerable<EventDevCmplt> GetAll()
        {
            return _context.EventDevCmplt
                            .Include(i => i.Event)
                            .OrderBy(i => i.OdieDevNme)
                            .ToList();
        }

        public EventDevCmplt GetById(int id)
        {
            return _context.EventDevCmplt
                            .Include(i => i.Event)
                            .SingleOrDefault(i => i.EventDevCmpltId == id);
        }

        public EventDevCmplt Create(EventDevCmplt entity)
        {
            throw new NotImplementedException();
        }

        public void Create(IEnumerable<EventDevCmplt> entity)
        {
            _context.EventDevCmplt
                .AddRange(entity.Select(a =>
                {
                    a.EventDevCmpltId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Update(int id, EventDevCmplt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void SetInactive(Expression<Func<EventDevCmplt, bool>> predicate)
        {
            _context.EventDevCmplt
                .Where(predicate)
                .ToList()
                .ForEach(a => {
                    a.RecStusId = (byte)0;
                    //a.ModfdDt = DateTime.Now;
                });
        }
    }
}