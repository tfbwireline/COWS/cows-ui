﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UcaasActivityTypeRepository : IUcaasActivityTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public UcaasActivityTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkUcaaSPlanType Create(LkUcaaSPlanType entity)
        {
            throw new NotImplementedException();
        }

        public LkUcaaSActyType Create(LkUcaaSActyType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkUcaaSActyType> Find(Expression<Func<LkUcaaSActyType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkUcaaSActyType, out List<LkUcaaSActyType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkUcaaSActyType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUcaaSActyType, out List<LkUcaaSActyType> list))
            {
                list = _context.LkUcaaSActyType
                            .OrderBy(i => i.UcaaSActyTypeDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkUcaaSActyType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkUcaaSActyType GetById(int id)
        {
            return _context.LkUcaaSActyType
                            .SingleOrDefault(i => i.UcaaSActyTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkUcaaSActyType);
            throw new NotImplementedException();
        }

        public void Update(int id, LkUcaaSActyType entity)
        {
            throw new NotImplementedException();
        }
    }
}