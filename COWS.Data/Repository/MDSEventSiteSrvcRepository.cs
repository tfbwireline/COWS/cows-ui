﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSEventSiteSrvcRepository : IMDSEventSiteSrvcRepository
    {
        private readonly COWSAdminDBContext _context;

        public MDSEventSiteSrvcRepository(COWSAdminDBContext context)
        {
            _context = context;
        }
        public IQueryable<MdsEventSiteSrvc> Find(Expression<Func<MdsEventSiteSrvc, bool>> predicate)
        {
            return _context.MdsEventSiteSrvc
                .Where(predicate);
        }

        public IEnumerable<MdsEventSiteSrvc> GetAll()
        {
            throw new NotImplementedException();
        }

        public MdsEventSiteSrvc GetById(int id)
        {
            return _context.MdsEventSiteSrvc
                .SingleOrDefault(a => a.MdsEventSiteSrvcId == id);
        }

        public MdsEventSiteSrvc Create(MdsEventSiteSrvc entity)
        {
            _context.MdsEventSiteSrvc.Add(entity);
            SaveAll();

            return GetById(entity.MdsEventSiteSrvcId);
        }

        public void Create(IEnumerable<MdsEventSiteSrvc> entity)
        {
            _context.MdsEventSiteSrvc
                .AddRange(entity.Select(a =>
                {
                    a.MdsEventSiteSrvcId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Update(int id, MdsEventSiteSrvc entity)
        {
            var obj = GetById(id);

            obj.M5OrdrNbr = entity.M5OrdrNbr;
            obj.Mach5SrvcOrdrId = entity.Mach5SrvcOrdrId;
            obj.SrvcTypeId = entity.SrvcTypeId;
            obj.ThrdPartyVndrId = entity.ThrdPartyVndrId;
            obj.ThrdPartyId = entity.ThrdPartyId;
            obj.ActvDt = entity.ActvDt;
            obj.CmntTxt = entity.CmntTxt;
            obj.OdieDevNme = entity.OdieDevNme;

            SaveAll();
        }

        public void UpdateRange(int id, IEnumerable<MdsEventSiteSrvc> entity)
        {
            // Get all records to be deleted
            var toDelete = Find(a =>
                a.EventId == id &&
                !entity.Where(b => b.MdsEventSiteSrvcId != 0)
                    .Select(b => b.MdsEventSiteSrvcId)
                    .Contains(a.MdsEventSiteSrvcId)
                );
            toDelete.ToList().ForEach(a =>
            {
                Delete(a.MdsEventSiteSrvcId);
            });

            // Update or Insert new records
            entity.ToList().ForEach(a =>
            {
                if(a.MdsEventSiteSrvcId != 0)
                {
                    Update(a.MdsEventSiteSrvcId, a);
                }
                else
                {
                    a.EventId = id;
                    Create(a);
                }
            });
        }

        public void Delete(int id)
        {
            var obj = GetById(id);
            _context.MdsEventSiteSrvc.Remove(obj);
            SaveAll();
        }

        public void Delete(Expression<Func<MdsEventSiteSrvc, bool>> predicate)
        {
            var toDelete = Find(predicate);
            _context.MdsEventSiteSrvc.RemoveRange(toDelete);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public IEnumerable<GetMdsEventSiteSrvc> GetMDSEventSiteSrvcByEventId(int eventId)
        {
            List<GetMdsEventSiteSrvc> MDSSrvcTbl = new List<GetMdsEventSiteSrvc>();

            var SrvcTbl = (from esstpv in
                                           (from ess in _context.MdsEventSiteSrvc
                                            join mst in _context.LkMdsSrvcType on ess.SrvcTypeId equals mst.SrvcTypeId
                                            join tpv in _context.LkMds3rdpartyVndr on ess.ThrdPartyVndrId equals tpv.ThrdPartyVndrId
                                               into joinesstpv
                                            from tpv in joinesstpv.DefaultIfEmpty()
                                            where ess.EventId == eventId
                                            select new
                                            {
                                                ess.MdsEventSiteSrvcId,
                                                ess.M5OrdrNbr,
                                                ess.Mach5SrvcOrdrId,
                                                ess.SrvcTypeId,
                                                mst.SrvcTypeDes,
                                                ThrdPartyVndrId = (tpv != null) ? tpv.ThrdPartyVndrId : (short)0,
                                                ThrdPartyVndrDes = (tpv != null) ? tpv.ThrdPartyVndrDes : string.Empty,
                                                ess.ThrdPartyId,
                                                ess.ThrdPartySrvcLvlId,
                                                ess.ActvDt,
                                                ess.CmntTxt,
                                                ess.OdieDevNme,
                                                ess.EmailCd,
                                                ess.CreatDt
                                            })
                           join tps in _context.LkMds3rdpartySrvcLvl on esstpv.ThrdPartySrvcLvlId equals tps.ThrdPartySrvcLvlId
                           into joinesstpvtps
                           from tps in joinesstpvtps.DefaultIfEmpty()
                           select new
                           {
                               esstpv.MdsEventSiteSrvcId,
                               esstpv.M5OrdrNbr,
                               esstpv.Mach5SrvcOrdrId,
                               esstpv.SrvcTypeId,
                               esstpv.SrvcTypeDes,
                               esstpv.ThrdPartyVndrId,
                               esstpv.ThrdPartyVndrDes,
                               esstpv.ThrdPartyId,
                               ThrdPartySrvcLvlId = (tps != null) ? esstpv.ThrdPartySrvcLvlId : (short)0,
                               ThrdPartySrvcLvlDes = (tps != null) ? tps.ThrdPartySrvcLvlDes : string.Empty,
                               esstpv.ActvDt,
                               esstpv.CmntTxt,
                               esstpv.OdieDevNme,
                               esstpv.EmailCd,
                               esstpv.CreatDt
                           });

            foreach (var srvcitem in SrvcTbl)
            {
                GetMdsEventSiteSrvc item = new GetMdsEventSiteSrvc();
                item.Mach5SrvcID = srvcitem.Mach5SrvcOrdrId;
                item.M5OrdrNbr = srvcitem.M5OrdrNbr;
                item.SrvcTypeID = srvcitem.SrvcTypeId.ToString();
                item.SrvcType = srvcitem.SrvcTypeDes;
                item.ThirdPartyVndrID = srvcitem.ThrdPartyVndrId.ToString();
                item.ThirdPartyVndr = srvcitem.ThrdPartyVndrDes;
                item.ThirdParty = srvcitem.ThrdPartyId;
                item.ThirdPartySrvcLvlID = srvcitem.ThrdPartySrvcLvlId.ToString();
                item.ThirdPartySrvcLvl = srvcitem.ThrdPartySrvcLvlDes;
                item.ActvnDt = (srvcitem.ActvDt == null) ? string.Empty : ((DateTime)srvcitem.ActvDt).ToString("MM/dd/yyyy");
                item.Comments = srvcitem.CmntTxt;
                item.ODIEDevNme = srvcitem.OdieDevNme;
                item.EmailFlg = srvcitem.EmailCd;
                MDSSrvcTbl.Add(item);
            }

            return MDSSrvcTbl;
        }

        public void AddSrvcTbl(ref List<GetMdsEventSiteSrvc> data, int eventId)
        {
            DeleteAllSrvcItems(eventId);

            foreach (GetMdsEventSiteSrvc item in data)
            {
                MdsEventSiteSrvc mss = new MdsEventSiteSrvc();
                if ((item.SrvcTypeID != string.Empty) && (item.SrvcTypeID != "0") && (eventId > 0))
                {
                    mss.EventId = eventId;
                    mss.M5OrdrNbr = item.M5OrdrNbr;
                    mss.Mach5SrvcOrdrId = item.Mach5SrvcID;
                    mss.SrvcTypeId = short.Parse(item.SrvcTypeID);
                    mss.ThrdPartyId = item.ThirdParty;
                    if ((item.ThirdPartySrvcLvlID != string.Empty) && (item.ThirdPartySrvcLvlID != "0"))
                        mss.ThrdPartySrvcLvlId = short.Parse(item.ThirdPartySrvcLvlID);
                    if ((item.ThirdPartyVndrID != string.Empty) && (item.ThirdPartyVndrID != "0"))
                        mss.ThrdPartyVndrId = short.Parse(item.ThirdPartyVndrID);
                    if (item.ActvnDt != string.Empty)
                        mss.ActvDt = DateTime.Parse(item.ActvnDt);
                    mss.OdieDevNme = item.ODIEDevNme;
                    mss.CmntTxt = item.Comments;
                    mss.CreatDt = DateTime.Now;

                    _context.MdsEventSiteSrvc.Add(mss);
                }
            }

            _context.SaveChanges();
        }

        public void DeleteAllSrvcItems(int eventId)
        {
            var srvcitems = (from mss in _context.MdsEventSiteSrvc
                             where mss.EventId == eventId
                             select mss);
            foreach (MdsEventSiteSrvc item in srvcitems)
            {
                _context.MdsEventSiteSrvc.Remove(item);
            }
            _context.SaveChanges();
        }
    }
}