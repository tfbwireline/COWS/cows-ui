﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MplsEventTypeRepository : IMplsEventTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MplsEventTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMplsEventType Create(LkMplsEventType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMplsEventType> Find(Expression<Func<LkMplsEventType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMplsEventType, out List<LkMplsEventType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMplsEventType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMplsEventType, out List<LkMplsEventType> list))
            {
                list = _context.LkMplsEventType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMplsEventType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMplsEventType GetById(int id)
        {
            return _context.LkMplsEventType
                            .AsNoTracking()
                            .SingleOrDefault(i => i.MplsEventTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMplsEventType);
            return _context.SaveChanges();
        }

        public void Update(int id, LkMplsEventType entity)
        {
            throw new NotImplementedException();
        }
    }
}