﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderAddressRepository : IOrderAddressRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public OrderAddressRepository(COWSAdminDBContext context,
            ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public OrdrAdr Create(OrdrAdr entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<OrdrAdr> Find(Expression<Func<OrdrAdr, bool>> predicate)
        {
            return _context.OrdrAdr
                .Include(a => a.AdrType)
                .Include(a => a.CtryCdNavigation)
                .Where(predicate);
        }

        public IEnumerable<OrdrAdr> GetAll()
        {
            return _context.OrdrAdr
                            .OrderBy(i => i.OrdrId)
                            .ToList();
        }

        public OrdrAdr GetById(int id)
        {
            throw new NotImplementedException();
            //return _context.OrdrAdr
            //                .SingleOrDefault(i => i.OrdrId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, OrdrAdr entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<object> GetByOrderId(int orderId)
        {
            return from oa in _context.OrdrAdr
                   join at in _context.LkAdrType on oa.AdrTypeId equals at.AdrTypeId
                   join c in _context.LkCtry on oa.CtryCd equals c.CtryCd
                   where oa.OrdrId == orderId
                   select new
                   {
                       oa.AdrTypeId,
                       at.AdrTypeDes,
                       oa.BldgNme,
                       oa.CisLvlType,
                       oa.CtryCd,
                       c.CtryNme,
                       oa.CtyNme,
                       oa.FlrId,
                       oa.FsaMdulId,
                       oa.HierLvlCd,
                       oa.OrdrAdrId,
                       oa.OrdrId,
                       oa.PrvnNme,
                       oa.RmNbr,
                       oa.StreetAdr1,
                       oa.StreetAdr2,
                       oa.StreetAdr3,
                       oa.SttCd,
                       oa.ZipPstlCd
                   };
        }

        public IEnumerable<OrdrAdr> GetOrderAddressByOrderId(int orderId, int csgLvlId)
        {
            Ordr order = _context.Ordr.Where(a => a.OrdrId == orderId).FirstOrDefault();
            IEnumerable<OrdrAdr> orderAddress = _context.OrdrAdr.Where(a => a.OrdrId == orderId).ToList();

            if (order.CsgLvlId > 0 && order.CsgLvlId <= csgLvlId)
            {
                for (int i = 0; i < orderAddress.Count(); i++)
                {
                    var securedNtwkCust = _context.CustScrdData
                        .Where(a => a.ScrdObjId == orderAddress.ElementAt(i).OrdrAdrId && a.ScrdObjTypeId == (byte)SecuredObjectType.ORDR_ADR)
                        .FirstOrDefault();
                    if (securedNtwkCust != null)
                    {
                        string ctryCd = _common.GetDecryptValue(securedNtwkCust.CtryCd);
                        orderAddress.ElementAt(i).StreetAdr1 = _common.GetDecryptValue(securedNtwkCust.StreetAdr1);
                        orderAddress.ElementAt(i).StreetAdr2 = _common.GetDecryptValue(securedNtwkCust.StreetAdr2);
                        orderAddress.ElementAt(i).StreetAdr3 = _common.GetDecryptValue(securedNtwkCust.StreetAdr3);
                        orderAddress.ElementAt(i).PrvnNme = _common.GetDecryptValue(securedNtwkCust.SttPrvnNme);
                        orderAddress.ElementAt(i).SttCd = _common.GetDecryptValue(securedNtwkCust.SttCd);
                        orderAddress.ElementAt(i).ZipPstlCd = _common.GetDecryptValue(securedNtwkCust.ZipPstlCd);
                        orderAddress.ElementAt(i).FlrId = _common.GetDecryptValue(securedNtwkCust.FlrId);
                        orderAddress.ElementAt(i).RmNbr = _common.GetDecryptValue(securedNtwkCust.RmNbr);
                        orderAddress.ElementAt(i).BldgNme = _common.GetDecryptValue(securedNtwkCust.BldgNme);
                        orderAddress.ElementAt(i).CtyNme = _common.GetDecryptValue(securedNtwkCust.CtyNme);
                        orderAddress.ElementAt(i).CtryCd = string.IsNullOrEmpty(ctryCd) ? orderAddress.ElementAt(i).CtryCd : ctryCd;
                    }
                }
            }

            return orderAddress;
        }
    }
}