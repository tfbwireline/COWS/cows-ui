﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class QualificationRepository : IQualificationRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public QualificationRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkQlfctn Create(LkQlfctn entity)
        {
            int maxId = _context.LkQlfctn.Max(i => i.QlfctnId);
            entity.QlfctnId = (short)++maxId;
            entity.CreatDt = DateTime.Now;
            _context.LkQlfctn.Add(entity);

            // Saving Qualification for other entities
            foreach (QlfctnEventType item in entity.QlfctnEventType)
            {
                item.CreatDt = entity.CreatDt;
                _context.QlfctnEventType.Add(item);
            }

            foreach (QlfctnNtwkActyType item in entity.QlfctnNtwkActyType)
            {
                item.CreatDt = entity.CreatDt;
                _context.QlfctnNtwkActyType.Add(item);
            }

            foreach (QlfctnDedctdCust item in entity.QlfctnDedctdCust)
            {
                item.CreatDt = entity.CreatDt;
                _context.QlfctnDedctdCust.Add(item);
            }

            //foreach (QlfctnSpclProj item in entity.QlfctnSpclProj)
            //{
            //    item.CreatDt = entity.CreatDt;
            //    _context.QlfctnSpclProj.Add(item);
            //}

            foreach (QlfctnMplsActyType item in entity.QlfctnMplsActyType)
            {
                item.CreatDt = entity.CreatDt;
                _context.QlfctnMplsActyType.Add(item);
            }

            foreach (QlfctnEnhncSrvc item in entity.QlfctnEnhncSrvc)
            {
                item.CreatDt = entity.CreatDt;
                _context.QlfctnEnhncSrvc.Add(item);
            }

            foreach (QlfctnDev item in entity.QlfctnDev)
            {
                item.CreatDt = entity.CreatDt;
                _context.QlfctnDev.Add(item);
            }

            foreach (QlfctnVndrModel item in entity.QlfctnVndrModel)
            {
                item.CreatDt = entity.CreatDt;
                _context.QlfctnVndrModel.Add(item);
            }

            foreach (QlfctnIpVer item in entity.QlfctnIpVer)
            {
                item.CreatDt = entity.CreatDt;
                _context.QlfctnIpVer.Add(item);
            }

            SaveAll();

            return GetById(entity.QlfctnId);
        }

        public void Delete(int id)
        {
            //QlfctnEventType[] types = _context.QlfctnEventType.Where(i => i.QlfctnId == id).ToArray();
            //_context.QlfctnEventType.RemoveRange(types);

            //QlfctnNtwkActyType[] netTypes = _context.QlfctnNtwkActyType.Where(i => i.QlfctnId == id).ToArray();
            //_context.QlfctnNtwkActyType.RemoveRange(netTypes);

            //QlfctnDedctdCust[] cust = _context.QlfctnDedctdCust.Where(i => i.QlfctnId == id).ToArray();
            //_context.QlfctnDedctdCust.RemoveRange(cust);

            //QlfctnSpclProj[] proj = _context.QlfctnSpclProj.Where(i => i.QlfctnId == id).ToArray();
            //_context.QlfctnSpclProj.RemoveRange(proj);

            //QlfctnEnhncSrvc[] srvc = _context.QlfctnEnhncSrvc.Where(i => i.QlfctnId == id).ToArray();
            //_context.QlfctnEnhncSrvc.RemoveRange(srvc);

            //QlfctnDev[] model = _context.QlfctnDev.Where(i => i.QlfctnId == id).ToArray();
            //_context.QlfctnDev.RemoveRange(model);

            //QlfctnVndrModel[] vendor = _context.QlfctnVndrModel.Where(i => i.QlfctnId == id).ToArray();
            //_context.QlfctnVndrModel.RemoveRange(vendor);

            //QlfctnIpVer[] ips = _context.QlfctnIpVer.Where(i => i.QlfctnId == id).ToArray();
            //_context.QlfctnIpVer.RemoveRange(ips);

            LkQlfctn qlfctn = GetById(id);
            _context.QlfctnEventType.RemoveRange(qlfctn.QlfctnEventType);
            _context.QlfctnNtwkActyType.RemoveRange(qlfctn.QlfctnNtwkActyType);
            _context.QlfctnDedctdCust.RemoveRange(qlfctn.QlfctnDedctdCust);
            _context.QlfctnMplsActyType.RemoveRange(qlfctn.QlfctnMplsActyType);
            _context.QlfctnSpclProj.RemoveRange(qlfctn.QlfctnSpclProj);
            _context.QlfctnEnhncSrvc.RemoveRange(qlfctn.QlfctnEnhncSrvc);
            _context.QlfctnDev.RemoveRange(qlfctn.QlfctnDev);
            _context.QlfctnVndrModel.RemoveRange(qlfctn.QlfctnVndrModel);
            _context.QlfctnIpVer.RemoveRange(qlfctn.QlfctnIpVer);
            _context.LkQlfctn.Remove(qlfctn);

            SaveAll();
        }

        public IQueryable<LkQlfctn> Find(Expression<Func<LkQlfctn, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkQlfctn, out List<LkQlfctn> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkQlfctn> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkQlfctn, out List<LkQlfctn> list))
            {
                list = _context.LkQlfctn
                            .Include(i => i.QlfctnDedctdCust).ThenInclude(qlfct => qlfct.Cust)
                            .Include(i => i.QlfctnDev).ThenInclude(qlfct => qlfct.Dev)
                            .Include(i => i.QlfctnEnhncSrvc).ThenInclude(qlfct => qlfct.EnhncSrvc)
                            .Include(i => i.QlfctnEventType).ThenInclude(qlfct => qlfct.EventType)
                            .Include(i => i.QlfctnIpVer).ThenInclude(qlfct => qlfct.IpVer)
                            .Include(i => i.QlfctnNtwkActyType).ThenInclude(qlfct => qlfct.NtwkActyType)
                            .Include(i => i.QlfctnSpclProj).ThenInclude(qlfct => qlfct.SpclProj)
                            .Include(i => i.QlfctnMplsActyType).ThenInclude(qlfct => qlfct.MplsActyType)
                            .Include(i => i.QlfctnVndrModel).ThenInclude(qlfct => qlfct.DevModel).ThenInclude(a => a.Manf)
                            .Include(i => i.AsnToUser)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderByDescending(i => i.CreatDt)
                            .ToList();
                _cache.Set(CacheKeys.LkQlfctn, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkQlfctn GetById(int id)
        {
            return _context.LkQlfctn
                            .Include(i => i.QlfctnDedctdCust).ThenInclude(qlfct => qlfct.Cust)
                            .Include(i => i.QlfctnDev).ThenInclude(qlfct => qlfct.Dev)
                            .Include(i => i.QlfctnEnhncSrvc).ThenInclude(qlfct => qlfct.EnhncSrvc)
                            .Include(i => i.QlfctnEventType).ThenInclude(qlfct => qlfct.EventType)
                            .Include(i => i.QlfctnIpVer).ThenInclude(qlfct => qlfct.IpVer)
                            .Include(i => i.QlfctnNtwkActyType).ThenInclude(qlfct => qlfct.NtwkActyType)
                            .Include(i => i.QlfctnSpclProj).ThenInclude(qlfct => qlfct.SpclProj)
                            .Include(i => i.QlfctnMplsActyType).ThenInclude(qlfct => qlfct.MplsActyType)
                            .Include(i => i.QlfctnVndrModel).ThenInclude(qlfct => qlfct.DevModel)
                            .Include(i => i.AsnToUser)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.QlfctnId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkQlfctn);
            return _context.SaveChanges();
        }

        public void Update(int id, LkQlfctn entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<GetQualificationView> GetQualificationView(int userID)
        {
            var qualificationView = _context.Query<GetQualificationView>()
                .AsNoTracking()
                .FromSql("dbo.getQualificationsByUserID @p0", userID).ToList<GetQualificationView>();

            return qualificationView;
        }

        public LkQlfctn Create(GetQualificationView entity)
        {
            LkQlfctn qlftn = new LkQlfctn();
            qlftn.AsnToUserId = entity.AssignToUserID;
            qlftn.RecStusId = 1; //EnumType
            qlftn.CreatByUserId = entity.CreatedByUserID;
            qlftn.CreatDt = entity.CreatedDate;
            _context.LkQlfctn.Add(qlftn);

            SaveAll();

            int qualificationId = qlftn.QlfctnId;

            // Saving Qualification for other entities
            if (!string.IsNullOrWhiteSpace(entity.EventTypeIDs))
            {
                foreach (string id in entity.EventTypeIDs.Split(","))
                {
                    QlfctnEventType item = new QlfctnEventType();
                    item.EventTypeId = Convert.ToInt16(id);
                    item.QlfctnId = qualificationId;
                    item.CreatDt = entity.CreatedDate;
                    _context.QlfctnEventType.Add(item);
                }
            }

            if (!string.IsNullOrWhiteSpace(entity.NetworkActivityIDs))
            {
                foreach (string id in entity.NetworkActivityIDs.Split(","))
                {
                    QlfctnNtwkActyType item = new QlfctnNtwkActyType();
                    item.NtwkActyTypeId = Convert.ToByte(id);
                    item.QlfctnId = qualificationId;
                    item.CreatDt = entity.CreatedDate;
                    _context.QlfctnNtwkActyType.Add(item);
                }
            }

            if (!string.IsNullOrWhiteSpace(entity.CustomerIDs))
            {
                foreach (string id in entity.CustomerIDs.Split(","))
                {
                    QlfctnDedctdCust item = new QlfctnDedctdCust();
                    item.CustId = Convert.ToInt32(id);
                    item.QlfctnId = qualificationId;
                    item.CreatDt = entity.CreatedDate;
                    _context.QlfctnDedctdCust.Add(item);
                }
            }

            //if (!string.IsNullOrWhiteSpace(entity.SpecialProjectIDs))
            //{
            //    foreach (string id in entity.SpecialProjectIDs.Split(","))
            //    {
            //        QlfctnSpclProj item = new QlfctnSpclProj();
            //        item.SpclProjId = Convert.ToInt16(id);
            //        item.QlfctnId = qualificationId;
            //        item.CreatDt = entity.CreatedDate;
            //        _context.QlfctnSpclProj.Add(item);
            //    }
            //}

            if (!string.IsNullOrWhiteSpace(entity.MplsActyTypeIDs))
            {
                foreach (string id in entity.MplsActyTypeIDs.Split(","))
                {
                    QlfctnMplsActyType item = new QlfctnMplsActyType();
                    item.MplsActyTypeId = Convert.ToByte(id);
                    item.QlfctnId = qualificationId;
                    item.CreatDt = entity.CreatedDate;
                    _context.QlfctnMplsActyType.Add(item);
                }
            }

            if (!string.IsNullOrWhiteSpace(entity.EnhanceServiceIDs))
            {
                foreach (string id in entity.EnhanceServiceIDs.Split(","))
                {
                    QlfctnEnhncSrvc item = new QlfctnEnhncSrvc();
                    item.EnhncSrvcId = Convert.ToInt16(id);
                    item.QlfctnId = qualificationId;
                    item.CreatDt = entity.CreatedDate;
                    _context.QlfctnEnhncSrvc.Add(item);
                }
            }

            if (!string.IsNullOrWhiteSpace(entity.DeviceIDs))
            {
                foreach (string id in entity.DeviceIDs.Split(","))
                {
                    QlfctnDev item = new QlfctnDev();
                    item.DevId = Convert.ToInt16(id);
                    item.QlfctnId = qualificationId;
                    item.CreatDt = entity.CreatedDate;
                    _context.QlfctnDev.Add(item);
                }
            }

            if (!string.IsNullOrWhiteSpace(entity.VendorModelIDs))
            {
                foreach (string id in entity.VendorModelIDs.Split(","))
                {
                    QlfctnVndrModel item = new QlfctnVndrModel();
                    item.DevModelId = Convert.ToInt16(id);
                    item.QlfctnId = qualificationId;
                    item.CreatDt = entity.CreatedDate;
                    _context.QlfctnVndrModel.Add(item);
                }
            }

            if (!string.IsNullOrWhiteSpace(entity.IPVersionIDs))
            {
                foreach (string id in entity.IPVersionIDs.Split(","))
                {
                    QlfctnIpVer item = new QlfctnIpVer();
                    item.IpVerId = Convert.ToByte(id);
                    item.QlfctnId = qualificationId;
                    item.CreatDt = entity.CreatedDate;
                    _context.QlfctnIpVer.Add(item);
                }
            }

            SaveAll();

            return GetById(qualificationId);
        }

        public void Update(int id, GetQualificationView entity)
        {
            // Retain creator details.
            var oldQual = GetById(id);
            entity.CreatedByUserID = oldQual.CreatByUserId;
            entity.CreatedDate = oldQual.CreatDt;

            Delete(id);
            var qualification = Create(entity);

            //LkQlfctn qlfcn = GetById(qualification.QlfctnId);
            //qlfcn.AsnToUserId = qualification.AsnToUserId;
            //qlfcn.RecStusId = qualification.RecStusId;

            qualification.ModfdByUserId = entity.ModifiedByUserID;
            qualification.ModfdDt = entity.ModifiedDate;

            _context.LkQlfctn.Update(qualification);
            SaveAll();
        }

        public bool CheckDuplicate(int assignedUserId)
        {
            try
            {
                var data = from u in _context.LkQlfctn
                           where u.AsnToUserId == assignedUserId
                           select u;

                if (data.Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}