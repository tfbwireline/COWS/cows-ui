﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CircuitMsRepository : ICircuitMsRepository
    {
        private readonly COWSAdminDBContext _context;

        public CircuitMsRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<CktMs> Find(Expression<Func<CktMs, bool>> predicate)
        {
            return _context.CktMs
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<CktMs> GetAll()
        {
            return _context.CktMs.ToList();
        }

        public CktMs GetById(int id)
        {
            return Find(a => a.CktId == id)
                .OrderByDescending(a => a.VerId)
                .FirstOrDefault();
        }

        public CktMs Create(CktMs entity)
        {
            _context.CktMs.Add(entity);
            SaveAll();

            return GetById(entity.CktId);
        }

        public void Update(int id, CktMs entity)
        {
            CktMs cktMs = GetById(id);

            cktMs.TrgtDlvryDt = entity.TrgtDlvryDt;
            cktMs.TrgtDlvryDtRecvDt = entity.TrgtDlvryDtRecvDt;
            cktMs.AccsDlvryDt = entity.AccsDlvryDt;
            cktMs.AccsAcptcDt = entity.AccsAcptcDt;
            cktMs.CntrcTermId = entity.CntrcTermId;
            cktMs.CntrcStrtDt = entity.CntrcStrtDt;
            cktMs.VndrCnfrmDscnctDt = entity.VndrCnfrmDscnctDt;
            cktMs.CreatByUserId = entity.CreatByUserId;
            cktMs.CreatDt = entity.CreatDt;
        }

        public void Delete(int id)
        {
            var toDelete = GetById(id);
            _context.CktMs.Remove(toDelete);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }
    }
}