﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;

namespace COWS.Data.Repository
{
    public class OdieReqRepository : IOdieReqRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public OdieReqRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public OdieReq Create(OdieReq entity)
        {
            _context.OdieReq.Add(entity);

            SaveAll();

            return GetById(entity.ReqId);
        }

        public void Delete(int id)
        {
            OdieReq entity = GetById(id);
            _context.OdieReq.Remove(entity);

            SaveAll();
        }

        public IQueryable<OdieReq> Find(Expression<Func<OdieReq, bool>> predicate)
        {
            return _context.OdieReq
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<OdieReq> GetAll()
        {
            return _context.OdieReq
                            .AsNoTracking()
                            .ToList();
        }

        public OdieReq GetById(int id)
        {
            return _context.OdieReq
                .SingleOrDefault(i => i.ReqId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, OdieReq entity)
        {
            OdieReq obj = GetById(id);

            SaveAll();
        }

        public OdieReq Request(int? orderId, int odieMessageId, string h1, string customerName, bool insertCompleted, string odieCustomerId, byte csgLvlId, int? odieCustomerInfoCategoryTypeId, string deviceFilter)
        {
            OdieReq req = new OdieReq();
            CustScrdData sec = new CustScrdData();

            //req.OrdrId = orderId == 0 ? null : orderId;
            req.OdieMsgId = odieMessageId;
            req.H1CustId = h1;
            req.StusId = Convert.ToByte(insertCompleted ? 21 : 10);
            req.OdieCustId = odieCustomerId;
            req.CreatDt = DateTime.Now;
            req.StusModDt = DateTime.Now;
            req.OdieCustInfoReqCatTypeId = odieCustomerInfoCategoryTypeId;
            req.DevFltr = deviceFilter;
            req.CsgLvlId = csgLvlId;

            if (csgLvlId == 0)
            {
                req.CustNme = customerName;
            }
            _context.OdieReq.Add(req);
            SaveAll();

            if (csgLvlId > 0)
            {
                sec.ScrdObjTypeId = (byte)SecuredObjectType.ODIE_REQ;
                sec.CustNme = _common.GetEncryptValue(customerName);
                sec.ScrdObjId = req.ReqId;
                sec.CreatDt = DateTime.Now;

                _context.CustScrdData.Add(sec);
                SaveAll();
            }
            return GetById(req.ReqId);
        }

        public int CreateODIEDiscoRequest(string h1, List<EventDiscoDev> discoOElist)
        {
            int reqId = 0;
            XElement discOEXml = new XElement("DISCOE");

            foreach (var item in discoOElist)
            {
                XElement _xc = new XElement("Device",
                            new XElement("ODIEDeviceName", item.OdieDevNme));
                discOEXml.Add(_xc);
            }

            var param = new SqlParameter[] {
                new SqlParameter() {
                    ParameterName = "@DISCOE",
                    SqlDbType = System.Data.SqlDbType.NVarChar,
                    Direction = System.Data.ParameterDirection.Input,
                    Value = discOEXml.ToString()
                },
                new SqlParameter() {
                    ParameterName = "@H1_CUST_ID",
                    SqlDbType = System.Data.SqlDbType.VarChar,
                    Direction = System.Data.ParameterDirection.Input,
                    Value = h1
                },
                new SqlParameter() {
                    ParameterName = "@ID",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Output
                }
            };

            _context.Database.ExecuteSqlCommand("EXEC web.InsertODIEDiscoRequest @DISCOE, @H1_CUST_ID, @ID OUT", param);
            reqId = Convert.ToInt32(param[2].Value);

            return reqId;
        }

        public IEnumerable<OdieCustRspnModel> GetODIECustNameResp(int _iReqID, string _sH1)
        {
            //DataTable dt = new DataTable();
            //dt = null;
            IEnumerable<OdieCustRspnModel> odresp;

            if (_iReqID == 0)
            {
                odresp = (from resp in _context.OdieRspn
                          join reqNme in _context.OdieReq on resp.ReqId equals reqNme.ReqId
                          where reqNme.OdieMsgId == Convert.ToInt32(OdieMessageType.CustomerNameRequest)
                                && reqNme.StusId == 21
                                && reqNme.H1CustId == _sH1
                          orderby resp.RspnId descending
                          select new OdieCustRspnModel
                          {
                              RspnId = resp.RspnId,
                              CustTeamPdl = resp.CustTeamPdl,
                              MnspmId = resp.MnspmId,
                              SowsFoldrPathNme = resp.SowsFoldrPathNme
                          }).Distinct().OrderByDescending(resp => resp.RspnId).Take(1);

                //dt = LinqHelper.CopyToDataTable(odresp, null, null);
            }
            else
            {
                odresp = (from resp in _context.OdieRspn
                          where resp.ReqId == _iReqID
                          //select new { resp.RspnId, resp.CustTeamPdl, resp.MnspmId, resp.SowsFoldrPathNme });
                          select new OdieCustRspnModel
                          {
                              RspnId = resp.RspnId,
                              CustTeamPdl = resp.CustTeamPdl,
                              MnspmId = resp.MnspmId,
                              SowsFoldrPathNme = resp.SowsFoldrPathNme
                          });

                //dt = LinqHelper.CopyToDataTable(odresp, null, null);
            }
            return odresp;
        }

        public DataTable GetODIECustNames(IEnumerable<OdieCustRspnModel> odCustResp, int csgLvlID)
        {
            DataTable dt = new DataTable();
            //dt = null;

            var rsp = (from rspn in odCustResp.AsEnumerable()
                       select rspn.RspnId).ToList();

            var odrcn = (from respcn in _context.OdieRspn
                         join oreq in _context.OdieReq on respcn.ReqId equals oreq.ReqId
                         join csd in _context.CustScrdData on new { respcn.RspnId, ScrdObjTypeId = (byte)ScrdObjType.ODIE_RSPN } equals new { RspnId = csd.ScrdObjId, csd.ScrdObjTypeId }
                            into joincsd
                         from csd in joincsd.DefaultIfEmpty()
                         where rsp.Contains(respcn.RspnId)
                         select new
                         {
                             respcn.RspnId,
                             respcn.CustNme,
                             //CUST_NME = (oreq.CsgLvlId == 0) ? respcn.CustNme : (((oreq.CsgLvlId > 0)
                             //   && (csgLvlID != 0) && (csgLvlID <= oreq.CsgLvlId)) ? string.Empty : string.Empty),
                             respcn.CustTeamPdl,
                             respcn.MnspmId,
                             respcn.SowsFoldrPathNme,
                             respcn.OdieCustId
                         }).Distinct();

            dt = LinqHelper.CopyToDataTable(odrcn, null, null);

            return dt;
        }

        public int InsertOdieShortNameRequest(int cptID, string h1, string custName, byte csgLvlID)
        {
            OdieReq odr = new OdieReq();
            CustScrdData csd = new CustScrdData();

            odr.OdieMsgId = (int)OdieMessageType.ShortNameValidationRequest;
            odr.H1CustId = h1;
            if (csgLvlID > 0)
            {
                csd.ScrdObjTypeId = (byte)SecuredObjectType.ODIE_REQ;
                csd.CustNme = _common.GetEncryptValue(custName);
            }
            else
            {
                odr.CustNme = custName;
            }

            odr.StusId = Convert.ToByte(10);
            odr.CptId = cptID;
            odr.CreatDt = DateTime.Now;
            odr.StusModDt = DateTime.Now;
            odr.CsgLvlId = csgLvlID;

            _context.OdieReq.Add(odr);
            SaveAll();

            if (csgLvlID > 0)
            {
                csd.ScrdObjId = odr.ReqId;
                csd.CreatDt = DateTime.Now;

                _context.CustScrdData.Add(csd);
                SaveAll();
            }

            return odr.ReqId;
        }
    }
}