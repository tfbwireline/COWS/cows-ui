﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventDevSrvcMgmtRepository : IEventDevSrvcMgmtRepository
    {
        private readonly COWSAdminDBContext _context;

        public EventDevSrvcMgmtRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<GetEventDevSrvcMgmt> GetEventDevSrvcMgmtByEventId(int eventId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@eventID", eventId)
            };
            var View = _context.Query<GetEventDevSrvcMgmt>()
                .AsNoTracking()
                .FromSql("[web].[getEventDevSrvcMgmt] @eventID", pc.ToArray()).ToList<GetEventDevSrvcMgmt>();

            return View;
        }

        public void AddDevMgmtTbl(ref List<GetEventDevSrvcMgmt> data, int eventId)
        {
            InactiveAllDevMgmt(eventId);

            foreach (GetEventDevSrvcMgmt item in data)
            {
                EventDevSrvcMgmt eds = new EventDevSrvcMgmt();

                eds.EventId = eventId;
                eds.MnsOrdrNbr = item.MNSOrdrNbr;
                eds.DeviceId = item.DeviceID;
                eds.MnsSrvcTierId = item.ServiceTierID;
                eds.M5OrdrCmpntId = item.CmpntID;
                //eds.MNS_ENTLMNT_ID = item.EntitlementID;
                eds.OdieDevNme = item.OdieDevNme;
                eds.CmpntStus = item.CmpntStus;
                eds.CmpntTypeCd = item.CmpntType;
                eds.CmpntNme = item.CmpntNme;
                eds.CreatDt = DateTime.Now;
                eds.RecStusId = (byte)1;
                if ((item.OdieDevNme.Equals("0")) || (!(item.CmpntType.Equals("MNST"))))
                    eds.OdieCd = true;

                _context.EventDevSrvcMgmt.Add(eds);
            }

            _context.SaveChanges();
        }

        private void InactiveAllDevMgmt(int eventId)
        {
            _context.EventDevSrvcMgmt.Where(a => a.EventId == eventId && a.RecStusId == ((byte)1)).ToList().ForEach(b => { b.RecStusId = ((byte)0); b.ModfdDt = DateTime.Now; });
            _context.SaveChanges();
        }

        public void Create(IEnumerable<EventDevSrvcMgmt> entity)
        {
            _context.EventDevSrvcMgmt
                .AddRange(entity.Select(a =>
                {
                    a.EventDevSrvcMgmtId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void SetInactive(Expression<Func<EventDevSrvcMgmt, bool>> predicate)
        {
            _context.EventDevSrvcMgmt
                .Where(predicate)
                .ToList()
                .ForEach(a => {
                    a.RecStusId = (byte)0;
                    a.ModfdDt = DateTime.Now;
                });
        }
    }
}