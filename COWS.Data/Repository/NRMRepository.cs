﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace COWS.Data.Repository
{
    public class NRMRepository : INRMRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public NRMRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public List<NrmSrvcInstc> GetNRMServiceInstanceList(string ftn)
        {
            List<NrmSrvcInstc> listResult = new List<NrmSrvcInstc>();
            NrmSrvcInstc nrmSrvcInst = null;

            if (ftn != null && ftn != string.Empty)
            {
                var nrmSrvcObjs = from nrm in _context.NrmSrvcInstc where nrm.Ftn == ftn && nrm.RecStusId == 1 select nrm;

                foreach (var nrm in nrmSrvcObjs)
                {
                    nrmSrvcInst = new NrmSrvcInstc
                    {
                        SrvcInstcObjId = nrm.SrvcInstcObjId,
                        SrvcInstcNme = nrm.SrvcInstcNme == null ? string.Empty : nrm.SrvcInstcNme.ToString(),
                        LoopAcptDt = nrm.LoopAcptDt,
                        OrdrStusDes = nrm.OrdrStusDes == null ? string.Empty : nrm.OrdrStusDes.ToString(),
                        DscnctDt = nrm.DscnctDt,
                        ActvDt = nrm.ActvDt,
                        OrdrCmntTxt = nrm.OrdrCmntTxt == null ? string.Empty : nrm.OrdrCmntTxt.ToString(),
                        MdsCd = nrm.MdsCd,
                        H5H6Id = nrm.H5H6Id == null ? string.Empty : nrm.H5H6Id.ToString(),
                        Ftn = nrm.Ftn,
                        NuaAdr = nrm.NuaAdr == null ? string.Empty : nrm.NuaAdr.ToString(),
                        BillClearDt = nrm.BillClearDt
                    };
                    listResult.Add(nrmSrvcInst);
                }
            }

            return listResult;
        }

        public List<NrmCkt> GetNRMCircuitList(string ftn)
        {
            List<NrmCkt> listResult = new List<NrmCkt>();
            NrmCkt nrmCircuit = null;

            var nrmCircuits = (from nrm in _context.NrmCkt.Where(a => a.Ftn == ftn && a.RecStusId == 1)
                               join csd in _context.CustScrdData on new { ScrdObjId = nrm.NrmCktId, ScrdObjTypeId = (byte)ScrdObjType.NRM_CKT } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                                                     into joincsd
                               from csd in joincsd.DefaultIfEmpty()
                               select new { nrm, csd });

            foreach (var nrm in nrmCircuits)
            {
                nrmCircuit = new NrmCkt
                {
                    NrmCktId = nrm.nrm.NrmCktId
                };
                if (nrm.nrm.SrvcInstcObjId != null)
                    nrmCircuit.SrvcInstcObjId = (long)nrm.nrm.SrvcInstcObjId;
                nrmCircuit.SrvcInstcNme = nrm.nrm.SrvcInstcNme ?? string.Empty;
                nrmCircuit.Ftn = nrm.nrm.Ftn;
                nrmCircuit.CktObjId = nrm.nrm.CktObjId ?? string.Empty;
                nrmCircuit.CktNme = nrm.nrm.CktNme ?? string.Empty;
                nrmCircuit.FmsCktId = nrm.nrm.FmsCktId ?? string.Empty;
                nrmCircuit.FmsSiteId = nrm.nrm.FmsSiteId ?? string.Empty;
                nrmCircuit.NuaAdr = nrm.nrm.NuaAdr ?? string.Empty;
                nrmCircuit.PlnNme = nrm.nrm.PlnNme ?? string.Empty;
                nrmCircuit.PlnSeqNbr = nrm.nrm.PlnSeqNbr ?? string.Empty;
                nrmCircuit.TocAdr = (nrm.csd == null) ? (nrm.nrm.TocAdr ?? string.Empty) : _common.GetDecryptValue(nrm.csd.SiteAdr);
                nrmCircuit.TocNme = nrm.nrm.TocNme ?? string.Empty;
                nrmCircuit.PortAsmtDes = nrm.nrm.PortAsmtDes ?? string.Empty;
                nrmCircuit.CreatDt = nrm.nrm.CreatDt;
                nrmCircuit.TocAdr1 = (nrm.csd == null) ? (nrm.nrm.TocAdr1 ?? string.Empty) : _common.GetDecryptValue(nrm.csd.StreetAdr1);
                nrmCircuit.TocAdr2 = (nrm.csd == null) ? (nrm.nrm.TocAdr2 ?? string.Empty) : _common.GetDecryptValue(nrm.csd.StreetAdr2);
                nrmCircuit.TocCtryCd = (nrm.csd == null) ? (nrm.nrm.TocCtryCd ?? string.Empty) : _common.GetDecryptValue(nrm.csd.CtryCd);
                nrmCircuit.TocCtyNme = (nrm.csd == null) ? (nrm.nrm.TocCtyNme ?? string.Empty) : _common.GetDecryptValue(nrm.csd.CtyNme);
                nrmCircuit.TocCtryNme = (nrm.csd == null) ? (nrm.nrm.TocCtryNme ?? string.Empty) : _common.GetDecryptValue(nrm.csd.CtryRgnNme);
                nrmCircuit.TocDmstcSttNme = (nrm.csd == null) ? (nrm.nrm.TocDmstcSttNme ?? string.Empty) : _common.GetDecryptValue(nrm.csd.SttPrvnNme);
                nrmCircuit.TocZipCd = (nrm.csd == null) ? (nrm.nrm.TocZipCd ?? string.Empty) : _common.GetDecryptValue(nrm.csd.ZipPstlCd);
                listResult.Add(nrmCircuit);
            }
            return listResult;
        }

        public List<NrmVndr> GetNRMVendorList(string ftn)
        {
            List<NrmVndr> listResult = new List<NrmVndr>();
            NrmVndr nrmVendor = null;

            var nrmVendors = (from nrm in _context.NrmVndr.Where(a => a.Ftn == ftn && a.RecStusId == 1) select nrm);

            foreach (var nrm in nrmVendors)
            {
                nrmVendor = new NrmVndr
                {
                    NmsVndrId = nrm.NmsVndrId
                };
                if (nrm.SrvcInstcObjId != null)
                    nrmVendor.SrvcInstcObjId = (long)nrm.SrvcInstcObjId;
                nrmVendor.Ftn = nrm.Ftn;
                nrmVendor.VndrNme = nrm.VndrNme ?? string.Empty;
                nrmVendor.VndrTypeId = nrm.VndrTypeId ?? string.Empty;
                nrmVendor.LecId = nrm.LecId ?? string.Empty;
                nrmVendor.VndrCktId = nrm.VndrCktId ?? string.Empty;
                nrmVendor.SegTypeId = nrm.SegTypeId ?? string.Empty;
                nrmVendor.H6H5Id = nrm.H6H5Id ?? string.Empty;
                nrmVendor.CreatDt = nrm.CreatDt;

                listResult.Add(nrmVendor);
            }

            return listResult;
        }

        public List<NRMNotes> GetNRMNotes(string ftn)
        {
            List<NRMNotes> lstNRM = new List<NRMNotes>();
            //  4	    FMS-ES
            //  5	    NetworkEngineer
            //  6       CPE
            //  7	    Pricing
            //  19	    International
            //  20	    Cancel
            //  24	    NRM
            var s = from f in _context.FsaOrdr
                    join c in _context.OrdrNte on f.OrdrId equals c.OrdrId
                    join nt in _context.LkNteType on c.NteTypeId equals nt.NteTypeId
                    where f.Ftn == ftn
                        && (c.NteTypeId == 4 || c.NteTypeId == 5 || c.NteTypeId == 6 || c.NteTypeId == 7 || c.NteTypeId == 19 || c.NteTypeId == 20 || c.NteTypeId == 24)
                    orderby c.NteId descending
                    select new { c, nt };

            if (s != null && s.Count() > 0)
            {
                foreach (var _s in s)
                {
                    NRMNotes nrm = new NRMNotes
                    {
                        CreateDate = _s.c.CreatDt,
                        Note = _s.c.NteTxt,
                        NoteTypeDesc = _s.nt.NteTypeDes
                    };

                    lstNRM.Add(nrm);
                }
            }
            return lstNRM;
        }

        public void SendASRMessageToNRMBPM(int iOrderID, string notes, int focMsg = 0)
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "dbo.sendMsgToNRMBPM @ORDR_ID, @Notes, @FocMsg";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@ORDR_ID", SqlDbType = SqlDbType.Int, Value = iOrderID });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Notes", SqlDbType = SqlDbType.VarChar, Value = notes });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@FocMsg", SqlDbType = SqlDbType.Bit, Value = focMsg });

                _context.Database.OpenConnection();
                command.ExecuteNonQuery();
            }
        }
    }
}