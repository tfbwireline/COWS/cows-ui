﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml.Linq;

namespace COWS.Data.Repository
{
    public class ApptRepository : IApptRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IUserRepository _userRepository;
        private readonly IEventAsnToUserRepository _eventAsnToUserRepository;

        public ApptRepository(COWSAdminDBContext context,
            IUserRepository userRepository,
            IEventAsnToUserRepository eventAsnToUserRepository)
        {
            _context = context;
            _userRepository = userRepository;
            _eventAsnToUserRepository = eventAsnToUserRepository;
        }

        public Appt Create(Appt entity)
        {
            _context.Appt.Add(entity);

            SaveAll();
            int maxId = _context.Appt.Max(i => i.ApptId);
            return GetById(maxId);
        }

        public void Delete(int id)
        {
            Appt st = GetById(id);
            _context.Appt.Remove(st);

            SaveAll();
        }

        public IQueryable<Appt> Find(Expression<Func<Appt, bool>> predicate)
        {
            return _context.Appt
                            .Where(predicate);
        }

        public IEnumerable<Appt> GetAll()
        {
            return _context.Appt
                            .OrderBy(i => i.ApptId)
                            .ToList();
        }

        public Appt GetById(int id)
        {
            return _context.Appt
                            .SingleOrDefault(i => i.ApptId == id);
        }

        private Appt GetByEventId(int id)
        {
            return _context.Appt
                            .SingleOrDefault(i => i.EventId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, Appt entity)
        {
            Appt App = GetById(id);
            if (App == null)
                App = GetByEventId(id);
            App.EventId = entity.EventId == null ? App.EventId : entity.EventId;
            App.StrtTmst = entity.StrtTmst == null ? App.StrtTmst : entity.StrtTmst;
            App.EndTmst = entity.EndTmst == null ? App.EndTmst : entity.EndTmst;
            App.RcurncDesTxt = entity.RcurncDesTxt == null ? App.RcurncDesTxt : entity.RcurncDesTxt;
            App.SubjTxt = entity.SubjTxt == null ? App.SubjTxt : entity.SubjTxt;
            App.Des = entity.Des == null ? App.Des : entity.Des;
            App.ApptLocTxt = entity.ApptLocTxt == null ? App.ApptLocTxt : entity.ApptLocTxt;
            App.ApptTypeId = entity.ApptTypeId == null ? App.ApptTypeId : entity.ApptTypeId;
            //App.CreatByUserId = entity.CreatByUserId == null ? App.CreatByUserId : entity.CreatByUserId;
            App.ModfdByUserId = entity.ModfdByUserId == null ? App.ModfdByUserId : entity.ModfdByUserId;
            App.ModfdDt = entity.ModfdDt == null ? App.ModfdDt : entity.ModfdDt;
            //App.CreatDt = entity.CreatDt == null ? App.CreatDt : entity.CreatDt;
            App.AsnToUserIdListTxt = entity.AsnToUserIdListTxt == null ? App.AsnToUserIdListTxt : entity.AsnToUserIdListTxt;
            App.RcurncCd = entity.RcurncCd == null ? App.RcurncCd : entity.RcurncCd;
            App.ApptTypeId = entity.ApptTypeId == null ? App.ApptTypeId : entity.ApptTypeId;
            App.RecStusId = entity.RecStusId == null ? App.RecStusId : entity.RecStusId;
            SaveAll();
        }

        // Calendar Entry for CAND Events (AD, NGVN, MPLS, SPLK), MDS, SIPT and UCaas Events
        public bool CalendarEntry(EventWorkflow esw, bool bMDSFT = false, bool isNtwkIntl = false)
        {
            bool bActChange = false;
            bool bTimeChange = false;
            bool bCalendarUpdate = false;
            bool bFTPreConfig = false;
            List<EventAsnToUser> assignedUser = esw.AssignUser.Where(a => a.RecStusId == 1).ToList();

            esw.CalendarEventTypeId = GetCalendarEventType(esw.EventTypeId, esw.EnhanceServiceId, esw.MplsEventTypeId);

            StringBuilder sbActUserName = new StringBuilder();
            if (assignedUser != null && assignedUser.Count() > 0)
            {
                foreach (var au in assignedUser)
                {
                    sbActUserName.AppendNoDup(string.IsNullOrWhiteSpace(_userRepository.GetById(au.AsnToUserId).FullNme)
                        ? string.Empty : (_userRepository.GetById(au.AsnToUserId).FullNme + "; "));
                }

                // Removing the trailing "; "
                if (sbActUserName.Length > 0)
                    sbActUserName.Remove(sbActUserName.Length - 2, 2);
            }

            string sCalDesc = (assignedUser != null && assignedUser.Count() > 0)
                ? (!string.IsNullOrWhiteSpace(esw.Comments) && esw.Comments.Length > 0
                    ? esw.Comments + " ; Assigned Activators : " + sbActUserName.ToString()
                    : "Assigned Activators : " + sbActUserName.ToString())
                : esw.Comments;
            string sCalSubj = esw.EventTitle + " Event ID : " + esw.EventId;

            bActChange = _eventAsnToUserRepository.AccountChange(assignedUser, esw.EventId);

            // Moved from line 237
            // Moved due to MDS FT Complete Emails were treated as Non-FT Emails
            if (bMDSFT)
            {
                // Can't set this to the top part. Because it will not insert a record
                esw.CalendarEventTypeId = (int)CalendarEventType.MDSFT;
            }

            if (isNtwkIntl)
            {
                // Can't set this to the top part. Because it will not insert a record
                esw.CalendarEventTypeId = (int)CalendarEventType.NtwkIntl;
            }

            if (esw.CalendarEventTypeId != (int)CalendarEventType.MDSFT)
            {
                if (esw.EventStatusId != ((int)EventStatus.Visible))
                {
                    bTimeChange = (esw.OldStartTime.TimeOfDay != new TimeSpan(0, 0, 0, 0)
                        || esw.OldEndTime.TimeOfDay != new TimeSpan(0, 0, 0, 0)) ? true : false;
                }
                else
                    bTimeChange = true;
            }
            else
                bTimeChange = true;

            // Unused
            // bMDSFT = esw.CalendarEventTypeId == (int)CalendarEventType.MDSFT ? true : false;

            // Used for MDS Events
            if (esw.Profile == "Activator" && esw.EventTypeId == (int)EventType.MDS)
            {
                bFTPreConfig = esw.EventStatusId == (byte)EventStatus.Published
                                        && esw.EventRule.WrkflwStusId == (byte)WorkflowStatus.Publish;
            }

            if (esw.EventRule != null)
            {
                // The 1st condition is for MDS Events
                if (((esw.EventTypeId == (int)EventType.MDS && !bFTPreConfig)
                        && esw.CalendarEventTypeId != (int)CalendarEventType.MDSFT
                        && ((esw.EventStatusId == (byte)EventStatus.Published
                                && esw.EventRule.EndEventStusId == (byte)EventStatus.Published)
                            || (esw.EventStatusId != (byte)EventStatus.Published
                                && esw.EventRule.WrkflwStusId != (byte)WorkflowStatus.Submit)
                            || (esw.EventRule.WrkflwStusId == (byte)WorkflowStatus.Submit)
                            || (esw.EventRule.EndEventStusId != (byte)EventStatus.Published
                                && esw.EventRule.WrkflwStusId != (byte)WorkflowStatus.Submit)))
                    // The 2nd condition is for UCaaS Event
                    || (esw.EventTypeId == (int)EventType.UCaaS
                        && ((esw.EventStatusId == (byte)EventStatus.Published
                                && esw.EventRule.EndEventStusId == (byte)EventStatus.Published
                                && (bActChange || bTimeChange))
                            || (esw.EventStatusId != (byte)EventStatus.Published
                                && esw.EventRule.WrkflwStusId != (byte)WorkflowStatus.Submit)
                            || (esw.EventRule.WrkflwStusId == (byte)WorkflowStatus.Submit && !esw.IsEscalation)
                            || (esw.EventRule.EndEventStusId != (byte)EventStatus.Published)
                                && esw.EventRule.WrkflwStusId != (byte)WorkflowStatus.Submit))
                    // The 3rd condition is for Non-UCaaS and Non-MDS Events
                    || ((esw.EventTypeId != (int)EventType.UCaaS && esw.EventTypeId != (int)EventType.MDS)
                        && ((esw.EventStatusId == (byte)EventStatus.Published
                                && esw.EventRule.EndEventStusId == (byte)EventStatus.Published
                                && (bActChange || bTimeChange))
                            || esw.EventStatusId != (byte)EventStatus.Published
                            || esw.EventRule.EndEventStusId == (byte)EventStatus.Published)))
                {
                    /*
                    * [CAL_CD] column is replacement for CalendarAdd and CalendarDelOld elements.
                    * If both add/delold elements are 0 then column value will be NULL;
                    * likewise value 0 for add=0/del=1; value 1 for add=1/del=0; value 2 for add=1/del=1
                    * dbo.[LK_EVENT_RULE]
                    */
                    if ((esw.EventRule.CalCd != null)) // CalendarAdd = 1 and/or CalendarDelOld = 1
                    {
                        if ((assignedUser == null) || (assignedUser.Count <= 0) || (esw.EventRule.CalCd == 0)) // CalendarDelOld = 1
                        {
                            lock (this)
                            {
                                Appt cal = Find(a => a.EventId == esw.EventId).SingleOrDefault();

                                if (cal != null)
                                {
                                    cal.RecStusId = (int)ERecStatus.DeleteHold;
                                    cal.ModfdByUserId = esw.UserId;
                                    cal.ModfdDt = DateTime.Now;
                                    SaveAll();
                                    bCalendarUpdate = true;
                                }
                            }
                        }
                        else if (esw.EventRule.CalCd == 1 || esw.EventRule.CalCd == 2)  // CalendarAdd = 1
                        {
                            if (esw.StartTime != null && esw.EndTime != null)
                            {
                                if (assignedUser != null && assignedUser.Count() > 0)
                                {
                                    lock (this)
                                    {
                                        XElement xe = new XElement("ResourceIds",
                                                                    from c in assignedUser
                                                                    select new XElement("ResourceId",
                                                                        new XAttribute("Type", "System.Int32"),
                                                                        new XAttribute("Value", c.AsnToUserId)));

                                        Appt cal = Find(a => a.EventId == esw.EventId).SingleOrDefault();

                                        //if(bMDSFT)
                                        //{
                                        //    // Can't set this to the top part. Because it will not insert a record
                                        //    esw.CalendarEventTypeId = (int)CalendarEventType.MDSFT;
                                        //}

                                        if (cal == null)
                                        {
                                            cal = new Appt();
                                            cal.EventId = esw.EventId;
                                            cal.StrtTmst = esw.StartTime;
                                            cal.EndTmst = esw.EndTime;
                                            cal.ApptTypeId = esw.CalendarEventTypeId;
                                            cal.CreatByUserId = esw.RequestorId;
                                            cal.CreatDt = DateTime.Now;
                                            cal.ModfdByUserId = esw.UserId;
                                            cal.ModfdDt = DateTime.Now;
                                            cal.AsnToUserIdListTxt = xe.ToString();
                                            //cal.Des = sCalDesc.Trim();
                                            cal.Des = sCalDesc.Length > 1000 ? sCalDesc.Substring(0, 1000) : sCalDesc;
                                            //cal.SubjTxt = sCalSubj.Trim();
                                            cal.SubjTxt = sCalSubj.Length > 1000 ? sCalSubj.Substring(0, 1000) : sCalSubj;
                                            cal.RcurncCd = false;
                                            cal.RcurncDesTxt = string.Empty;
                                            cal.RecStusId = (byte)ERecStatus.Active;
                                            cal.ApptLocTxt = string.Format("Bridge Number;Pin : {0};{1}", esw.ConferenceBridgeNbr, esw.ConferenceBridgePin);

                                            _context.Appt.Add(cal);
                                        }
                                        else
                                        {
                                            cal.EventId = esw.EventId;
                                            cal.StrtTmst = esw.StartTime;
                                            cal.EndTmst = esw.EndTime;
                                            cal.ApptTypeId = esw.CalendarEventTypeId;
                                            cal.ModfdByUserId = esw.UserId;
                                            cal.ModfdDt = DateTime.Now;
                                            cal.AsnToUserIdListTxt = xe.ToString();
                                            //cal.Des = sCalDesc.Trim();
                                            cal.Des = sCalDesc.Length > 1000 ? sCalDesc.Substring(0, 1000) : sCalDesc;
                                            //cal.SubjTxt = sCalSubj.Trim();
                                            cal.SubjTxt = sCalSubj.Length > 1000 ? sCalSubj.Substring(0, 1000) : sCalSubj;
                                            cal.RcurncCd = false;
                                            cal.RcurncDesTxt = string.Empty;
                                            cal.RecStusId = esw.EventRule.WrkflwStusId == (byte)WorkflowStatus.Complete
                                                ? (byte)ERecStatus.InActive : (byte)ERecStatus.Active;
                                            cal.ApptLocTxt = string.Format("Bridge Number;Pin : {0};{1}", esw.ConferenceBridgeNbr, esw.ConferenceBridgePin);
                                        }

                                        SaveAll();
                                        bCalendarUpdate = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return bCalendarUpdate;
        }

        // Get ApptType for CalenderEntry
        private int GetCalendarEventType(int eventTypeId, int enhanceSrvcId, int mplsEventTypeId)
        {
            if (eventTypeId == (int)EventType.AD)
            {
                if (enhanceSrvcId == (int)EnhanceService.ADBroadband)
                    return (int)CalendarEventType.ADB;
                else if (enhanceSrvcId == (int)EnhanceService.ADGovernment)
                    return (int)CalendarEventType.ADG;
                else if (enhanceSrvcId == (int)EnhanceService.ADInternational)
                    return (int)CalendarEventType.ADI;
                else if (enhanceSrvcId == (int)EnhanceService.ADNarrowband)
                    return (int)CalendarEventType.ADN;
                else if (enhanceSrvcId == (int)EnhanceService.ADTMT)
                    return (int)CalendarEventType.ADTMT;
            }
            else if (eventTypeId == (int)EventType.MPLS)
            {
                if (mplsEventTypeId == (int)MplsEventType.ScheduledImplementation)
                    return (int)CalendarEventType.MPLS;
                else
                    return (int)CalendarEventType.MPLSVAS;
            }
            else if (eventTypeId == (int)EventType.NGVN)
                return (int)CalendarEventType.NGVN;
            else if (eventTypeId == (int)EventType.SprintLink)
                return (int)CalendarEventType.SLNK;
            else if (eventTypeId == (int)EventType.SIPT)
                return (int)CalendarEventType.SIPT;
            else if (eventTypeId == (int)EventType.UCaaS)
                return (int)CalendarEventType.UCaaS;
            else if (eventTypeId == (int)EventType.MDS)
                return (int)CalendarEventType.MDS;

            return 0;
        }
    }
}