﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class FsaOrderRepository : IFsaOrderRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public FsaOrderRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public FsaOrdr Create(FsaOrdr entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<FsaOrdr> Find(Expression<Func<FsaOrdr, bool>> predicate)
        {
            return _context.FsaOrdr
                .Include(a => a.Ordr.Pprt)
                    .ThenInclude(a => a.OrdrType)
                .Include(a => a.Ordr.OrdrStus)
                .Include(a => a.Ordr.TrptOrdr)
                //.Include(a => a.Ordr)
                //    .ThenInclude(a => a.OrdrStus)
                .Include(a => a.FsaOrdrCust)
                .Include(i => i.InstlSrvcTierCdNavigation)
                //.Include(i => i.Ordr)
                .Include(i => i.OrdrActn)
                .Include(i => i.OrdrTypeCdNavigation)
                .Include(i => i.ProdTypeCdNavigation)
                .Include(i => i.SipTrnkGrp)
                .Include(a => a.Ordr.TrptOrdr)
                .Include(a => a.FsaOrdrCpeLineItem)
                    .ThenInclude(a => a.FsaOrdrGomXnci)
                //.Include(i => i.SipTrnkGrp).ThenInclude(j => j.SipTrnkGrp)
                .Where(predicate);
        }

        public IEnumerable<FsaOrdr> GetAll()
        {
            return _context.FsaOrdr
                            .OrderBy(i => i.Ftn)
                            .ToList();
        }

        public FsaOrdr GetById(int id)
        {
            var orderTypes = _context.LkOrdrType.Select(a => a).ToList();
            var orderSubTypes = _context.LkOrdrSubType.Select(a => a).ToList();
            //var orderTypes = _context.LkOrdrType.Select(a => a).ToList();

            return _context.FsaOrdr
                .Include(a => a.Ordr.OrdrStus)
                .Include(a => a.Ordr.OrdrVlan)
                .Include(a => a.Ordr.TrptOrdr)
                    //.ThenInclude(a => a.OrdrStus)
                .Include(a => a.FsaOrdrCust)
                .Include(i => i.InstlSrvcTierCdNavigation)
                .Include(i => i.Ordr)
                .Include(i => i.OrdrActn)
                .Include(i => i.OrdrTypeCdNavigation)
                .Include(i => i.ProdTypeCdNavigation)
                .Include(i => i.SipTrnkGrp)
                .Include(i => i.FsaOrdrCpeLineItem)
                    .ThenInclude(i => i.FsaOrdrGomXnci)
                .SingleOrDefault(i => i.OrdrId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, FsaOrdr entity)
        {
            var obj = GetById(id);
            obj.Ordr.ModfdDt = DateTime.Now;

            if (obj.Ordr.TrptOrdr != null)
            {
                obj.Ordr.TrptOrdr.AccsMbDes = entity.Ordr.TrptOrdr.AccsMbDes;
                obj.Ordr.TrptOrdr.AccsCtySiteId =  entity.Ordr.TrptOrdr.AccsCtySiteId == 0 ? null : entity.Ordr.TrptOrdr.AccsCtySiteId;
                obj.Ordr.TrptOrdr.NrmUpdtCd = entity.Ordr.TrptOrdr.NrmUpdtCd;
                if (obj.ProdTypeCd == "CP")
                {
                    obj.Ordr.TrptOrdr.AccsCtySiteId = null;
                }
            }
            obj.CustCmmtDt = entity.CustCmmtDt;
            obj.CustWantDt = entity.CustWantDt;
            //obj.FsaOrdrCpeLineItem = entity.FsaOrdrCpeLineItem;

            if (obj.Ordr.OrdrVlan != null)
            {
                if (obj.Ordr.OrdrVlan.Count > 0)
                {
                    // Update
                    int orderVlanId = obj.Ordr.OrdrVlan.First().OrdrVlanId;
                    var orderVlan = obj.Ordr.OrdrVlan
                        .Where(a => a.OrdrVlanId == orderVlanId)
                        .First();

                    orderVlan.VlanId = entity.Ordr.OrdrVlan.First().VlanId;
                    orderVlan.VlanSrcNme = "COWS";
                    orderVlan.VlanPctQty = 0;

                    _context.OrdrVlan.RemoveRange(obj.Ordr.OrdrVlan.Where(a => a.OrdrVlanId != id).ToList());
                    _context.OrdrVlan.Update(orderVlan);
                }
                else
                {
                    // Insert
                    obj.Ordr.OrdrVlan = entity.Ordr.OrdrVlan;
                }
            }
            else
            {
                // Insert
                obj.Ordr.OrdrVlan = entity.Ordr.OrdrVlan;
            }

            foreach(var fsaOrdrCpeLineItem in entity.FsaOrdrCpeLineItem)
            {
                foreach(var fsaOrdrGomXnci in fsaOrdrCpeLineItem.FsaOrdrGomXnci)
                {
                    if (fsaOrdrGomXnci.Id > 0)
                    {
                        var current = obj.FsaOrdrGomXnci.Where(a => a.Id == fsaOrdrGomXnci.Id).FirstOrDefault();

                        current.PrchOrdrBackOrdrShipDt = fsaOrdrGomXnci.PrchOrdrBackOrdrShipDt;
                        current.CpeVndrNme = fsaOrdrGomXnci.CpeVndrNme;
                        current.ShpmtTrkNbr = fsaOrdrGomXnci.ShpmtTrkNbr;
                        current.CustDlvryDt = fsaOrdrGomXnci.CustDlvryDt;
                    }
                    else
                    {
                        var profileHierarchy = _context.LkPrfHrchy.Where(a => a.ChldPrfId == fsaOrdrGomXnci.UsrPrfId).SingleOrDefault();

                        if (profileHierarchy != null)
                        {
                            fsaOrdrGomXnci.UsrPrfId = profileHierarchy.PrntPrfId;
                        }

                        _context.FsaOrdrGomXnci.Add(fsaOrdrGomXnci);
                    }
                }
            }

            //entity.FsaOrdrCpeLineItem.Select(a =>
            //{
            //    a.FsaOrdrGomXnci.Select(b =>
            //    {
            //        if (b.Id > 0)
            //        {
            //            var fsaOrdrGomXnci = obj.FsaOrdrGomXnci.Where(c => c.Id == b.Id).FirstOrDefault();

            //            fsaOrdrGomXnci.PrchOrdrBackOrdrShipDt = b.PrchOrdrBackOrdrShipDt;
            //            fsaOrdrGomXnci.CpeVndrNme = b.CpeVndrNme;
            //            fsaOrdrGomXnci.ShpmtTrkNbr = b.ShpmtTrkNbr;
            //            fsaOrdrGomXnci.CustDlvryDt = b.CustDlvryDt;
            //        }
            //        //else
            //        //{
            //        //    _context.FsaOrdrGomXnci.Add(b);
            //        //}

            //        return b;
            //    });

            //    return a;
            //});

            //SaveAll();
        }

        public FsaOrdr GetTransportData(int id, bool isTerminating)
        {
            return _context.FsaOrdr
                //.Include(a => a.Ordr)
                .Include(a => a.Ordr.TrptOrdr)
                .Include(a => a.FsaOrdrCpeLineItem)
                .Where(a => a.OrdrId == id)
                .FirstOrDefault();
        }

        public FSADisconnect GetFSADisconnectDetails(int id)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@OrderID", id),

            };

            var data = _context.Query<FSADisconnect>()
                .AsNoTracking()
                .FromSql("dbo.GetFSADisconnectInformation @OrderID", pc.ToArray()).FirstOrDefault();

            return data;          
        }

        private string GetVendorName(int id, string platformCd)
        {
            var vendors = _context.LkVndr;
            var query = _context.FsaOrdr
                .Where(a => a.OrdrId == id);

            IEnumerable<LkVndr> result = null;

            if (platformCd == "SF")
            {
                result = query.AsEnumerable().Join(
                    vendors,
                    cpeLineItem => cpeLineItem.CxrAccsCd,
                    vendor => vendor.VndrCd,
                    (cpeLineItem, vendor) => {
                        return vendor;
                    });
            }
            else if (platformCd == "CP")
            {
                result = query.AsEnumerable().Join(
                    vendors,
                    cpeLineItem => cpeLineItem.VndrVpnCd,
                    vendor => vendor.VndrCd,
                    (cpeLineItem, vendor) => {
                        return vendor;
                    });
            }
            else if (platformCd == "RS")
            {
                result = query.AsEnumerable().Join(
                    vendors,
                    cpeLineItem => cpeLineItem.InstlVndrCd,
                    vendor => vendor.VndrCd,
                    (cpeLineItem, vendor) => {
                        return vendor;
                    });
            }
            else
            {
                return "";
            }

            if (result != null)
            {
                return result.First().VndrNme;
            }
            else
            {
                // Get from related order

                var relatedFtn = query.First().ReltdFtn;
                var relatedOrder = Find(a => a.Ftn == relatedFtn).FirstOrDefault();

                if (relatedOrder != null)
                {
                    return GetVendorName(relatedOrder.OrdrId, platformCd);
                }
                else
                {
                    return GetOrderVendorName(id);
                }
            }
        }

        private string GetOrderVendorName(int id)
        {
            return _context.LkVndr
                .Include(a => a.VndrFoldr)
                    .ThenInclude(a => a.VndrOrdr)
                .Where(a => 
                    a.VndrFoldr.Any(b => 
                        b.VndrOrdr.Any(c => c.OrdrId == id)
                    )
                )
                .FirstOrDefault()
                .VndrNme;
        }

        public void UpdateVendorAccessCostInfo(int id, FsaOrdr entity)
        {
            FsaOrdr fsa = GetById(id);

            fsa.TportVndrRawMrcAmt = entity.TportVndrRawMrcAmt;
            fsa.TportVndrRawNrcAmt = entity.TportVndrRawNrcAmt;
            fsa.TportVndrRvsdQotCur = entity.TportVndrRvsdQotCur;
            fsa.VndrTerm = entity.VndrTerm;

            _context.FsaOrdr.Update(fsa);
            SaveAll();
        }

        public IQueryable<FsaOrdr> GetFsaOrderCpeLineItem(Expression<Func<FsaOrdr, bool>> predicate)
        {
            return _context.FsaOrdr
                .Include(a => a.Ordr)
                //.Include(a => a.Ordr.TrptOrdr)
                .Include(a => a.FsaOrdrCpeLineItem)
                .Where(predicate);
        }
    }
}