﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSEventWrlsTrptRepository : IMDSEventWrlsTrptRepository
    {
        private readonly COWSAdminDBContext _context;

        public MDSEventWrlsTrptRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<MdsEventWrlsTrpt> Find(Expression<Func<MdsEventWrlsTrpt, bool>> predicate)
        {
            return _context.MdsEventWrlsTrpt
                           .Where(predicate);
        }

        public IEnumerable<MdsEventWrlsTrpt> GetAll()
        {
            return _context.MdsEventWrlsTrpt
                .ToList();
        }

        public MdsEventWrlsTrpt GetById(int id)
        {
            throw new NotImplementedException();
        }

        public MdsEventWrlsTrpt Create(MdsEventWrlsTrpt entity)
        {
            throw new NotImplementedException();
        }

        public void Create(IEnumerable<MdsEventWrlsTrpt> entity)
        {
            _context.MdsEventWrlsTrpt
                .AddRange(entity.Select(a =>
                {
                    a.MdsEventWrlsTrptId = 0;
                    a.CreatDt = DateTime.Now;

                    return a;
                }));
        }

        public void Update(int id, MdsEventWrlsTrpt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(Expression<Func<MdsEventWrlsTrpt, bool>> predicate)
        {
            var toDelete = Find(predicate);
            _context.MdsEventWrlsTrpt.RemoveRange(toDelete);
        }

        public int SaveAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MdsEventWrlsTrpt> GetMDSEventWrlsTrptByEventId(int eventId)
        {
            return _context.MdsEventWrlsTrpt.Where(a => a.EventId == eventId)
                .ToList();
        }

        public void AddWrlsTrptTbl(ref List<MdsEventWrlsTrpt> data, int eventId)
        {
            DeleteAllWrlsTrptItems(eventId);

            foreach (MdsEventWrlsTrpt item in data)
            {
                MdsEventWrlsTrpt mwt = new MdsEventWrlsTrpt();

                mwt.EventId = eventId;
                mwt.WrlsTypeCd = item.WrlsTypeCd;
                mwt.PrimBkupCd = item.PrimBkupCd;
                mwt.EsnMacId = item.EsnMacId;
                mwt.BillAcctNme = item.BillAcctNme;
                mwt.BillAcctNbr = item.BillAcctNbr;
                mwt.AcctPin = item.AcctPin;
                mwt.SalsCd = item.SalsCd;
                mwt.Soc = item.Soc;
                mwt.StaticIpAdr = item.StaticIpAdr;
                mwt.Imei = item.Imei;
                mwt.UiccIdNbr = item.UiccIdNbr;
                mwt.PricePln = item.PricePln;
                mwt.OdieDevNme = item.OdieDevNme;
                mwt.CreatDt = DateTime.Now;

                _context.MdsEventWrlsTrpt.Add(mwt);
            }
            _context.SaveChanges();
        }

        private void DeleteAllWrlsTrptItems(int eventId)
        {
            var wrlsitems = (from mwt in _context.MdsEventWrlsTrpt
                             where mwt.EventId == eventId
                             select mwt);

            foreach (MdsEventWrlsTrpt item in wrlsitems)
            {
                _context.MdsEventWrlsTrpt.Remove(item);
            }
            _context.SaveChanges();
        }
    }
}