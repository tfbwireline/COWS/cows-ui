﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public UserProfileRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkUsrPrf> Find(Expression<Func<LkUsrPrf, bool>> predicate)
        {
            return _context.LkUsrPrf
                            .Include(a => a.MapUsrPrf)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkUsrPrf> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUsrPrf, out List<LkUsrPrf> list))
            {
                list = _context.LkUsrPrf
                            .Include(a => a.MapUsrPrf)
                            .Include(a => a.LkPrfHrchyChldPrf)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkUsrPrf, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkUsrPrf GetById(int id)
        {
            return _context.LkUsrPrf
                .Include(a => a.MapUsrPrf).AsNoTracking()
                .SingleOrDefault(a => a.UsrPrfId == id);
        }

        public LkUsrPrf Create(LkUsrPrf entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkUsrPrf entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkUsrPrf);
            return _context.SaveChanges();
        }

        public IEnumerable<UserProfiles> GetUserProfilesByUserId(int userID, int loggedInUserId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@User_Id", userID),
                new SqlParameter("@LoggedInUserId", loggedInUserId)
            };

            var userProfiles = _context.Query<UserProfiles>()
                .AsNoTracking()
                .FromSql("web.getUserProfilesByUserId @User_Id, @LoggedInUserId", pc.ToArray()).ToList();

            if (userProfiles != null && userProfiles.Count() > 0)
            {
                string[] poolCdProfiles = new string[] {
                    "MDS Event Reviewer",
                    "MDS Event Activator",
                    "MDS CPT Manager",
                    "MDS CPT GateKeeper",
                    "MDS CPT CRM",
                    "MDS System Security Engineer"
                };
                foreach (UserProfiles usrPrf in userProfiles)
                {
                    if (poolCdProfiles.Contains(usrPrf.UsrPrfDes))
                    {
                        usrPrf.PoolCd = usrPrf.PoolCd.HasValue ? usrPrf.PoolCd.GetValueOrDefault() : 0;
                    }
                    else
                    {
                        usrPrf.PoolCd = null;
                    }
                }
            }

            return userProfiles;
        }
    }
}