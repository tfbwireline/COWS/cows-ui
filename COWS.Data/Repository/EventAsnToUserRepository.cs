﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EventAsnToUserRepository : IEventAsnToUserRepository
    {
        private readonly COWSAdminDBContext _context;

        public EventAsnToUserRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<EventAsnToUser> Find(Expression<Func<EventAsnToUser, bool>> predicate)
        {
            return _context.EventAsnToUser
                .Include(a => a.AsnToUser)
                .Where(predicate);
        }

        public IEnumerable<EventAsnToUser> GetAll()
        {
            return _context.EventAsnToUser
                .ToList();
        }

        public EventAsnToUser GetById(int id)
        {
            throw new NotImplementedException();
        }

        public EventAsnToUser Create(EventAsnToUser entity)
        {
            _context.EventAsnToUser.Add(entity);
            SaveAll();
            return Find(a => a.EventId == entity.EventId && a.AsnToUserId == entity.AsnToUserId).SingleOrDefault();
        }

        public List<EventAsnToUser> Create(List<EventAsnToUser> list)
        {
            // list can be used instead of temp; no need to create another
            List<EventAsnToUser> temp = new List<EventAsnToUser>();
            foreach (EventAsnToUser entity in list)
            {
                _context.EventAsnToUser.Add(entity);
                temp.Add(entity);
            }

            SaveAll();

            return temp;
        }

        public void Update(int id, EventAsnToUser entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, List<EventAsnToUser> list)
        {
            List<EventAsnToUser> toDelete = Find(a => a.EventId == id).ToList();
            foreach (EventAsnToUser entity in toDelete)
            {
                if (list.Select(b => b.AsnToUserId).Contains(entity.AsnToUserId))
                {
                    entity.RecStusId = 1;
                    _context.EventAsnToUser.Update(entity);
                }
                else
                {
                    entity.RecStusId = 0;
                    entity.ModfdDt = DateTime.Now;
                    _context.EventAsnToUser.Update(entity);
                }
            }

            SaveAll();

            foreach (EventAsnToUser entity in list)
            {
                int count = Find(a => a.EventId == entity.EventId && a.AsnToUserId == entity.AsnToUserId).Count();
                if (count == 0)
                {
                    entity.CreatDt = DateTime.Now;
                    Create(entity);
                }
            }
            //throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public bool AccountChange(List<EventAsnToUser> assignUsers, int iEventID)
        {
            bool changed = false;
            var evntUsers = (from eau in _context.EventAsnToUser
                             join lu in _context.LkUser on eau.AsnToUserId equals lu.UserId
                             where (eau.EventId == iEventID)
                                && (eau.RecStusId == (byte)ERecStatus.Active)
                             select new { eau.AsnToUserId, lu.EmailAdr, lu.DsplNme }).ToList();

            if (evntUsers.Count > 0)
            {
                if (assignUsers != null && (evntUsers.Count == assignUsers.Count))
                {
                    foreach (var ea in evntUsers)
                    {
                        //if (!(assignUsers.Find(au => ((au.AsnToUserId == ea.AsnToUserId) && (((byte)au.RoleID) == ea.ROLE_ID))) != null))
                        if (!(assignUsers.Find(au => au.AsnToUserId == ea.AsnToUserId) != null))
                            changed = true;
                    }
                }
                else
                    changed = true;
            }
            else
                changed = (assignUsers != null && assignUsers.Count > 0) ? true : false;

            return changed;
        }
    }
}