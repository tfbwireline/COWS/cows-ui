﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class CpeClliRepository : ICpeClliRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;
        private IMemoryCache _cache;

        public CpeClliRepository(COWSAdminDBContext context, IConfiguration configuration, IMemoryCache cache)
        {
            _context = context;
            _configuration = configuration;
            _cache = cache;
        }


        public CpeClli GetById(int id)
        {
            return _context.CpeClli
                            .SingleOrDefault(x => x.CpeClliId == id);
        }


        public IQueryable<CpeClli> Find(Expression<Func<CpeClli, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.CpeClli, out List<CpeClli> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<CpeClli> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.CpeClli, out List<CpeClli> list))
            {
                list = _context.CpeClli
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.CpeClli, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public CpeClli Create(CpeClli entity)
        {
            _cache.Remove(CacheKeys.CpeClli);
            _context.CpeClli.Add(entity);

            SaveAll();

            bool willSendPSOFT = entity.PsoftSendCd && entity.CpeClliStusId == 0;
            EmailReq req = new EmailReq();

            req.EmailReqTypeId = 38;
            req.StusId = 10;
            req.CreatDt = DateTime.Now;

            var subject = string.Empty;
            if(willSendPSOFT)
            {
                var result = _context.LkSysCfg
                                        .Where(x => x.RecStusId == (byte)ERecStatus.Active && x.PrmtrNme == "CPECLLIPSOFTPDL")
                                        .Select(x => x.PrmtrValuTxt).FirstOrDefault();

                req.EmailListTxt = (result != null) ? result : string.Empty;
                subject = "CLLI Request (For Review)";
            } else
            {
                var result = _context.LkSysCfg
                                        .Where(x => x.RecStusId == (byte)ERecStatus.Active && x.PrmtrNme == "CPECLLIEmail")
                                        .Select(x => x.PrmtrValuTxt).FirstOrDefault();

                req.EmailListTxt = (result != null) ? result : string.Empty;
                subject = entity.CpeClliStusId == 0 ? "CLLI Pending Request" : "CLLI Completed Request";

            }
           

            req.EmailCcTxt = entity.EmailCc;
            req.EmailSubjTxt = String.Format("{0}; CLLI ID: {1}", subject, entity.ClliId);
            req.EmailBodyTxt = GetXMLData(entity);

            _context.EmailReq.Add(req);
            SaveAll();

            return GetById(entity.CpeClliId);
        }

        public void Update(int id, CpeClli entity)
        {
            _cache.Remove(CacheKeys.CpeClli);

            CpeClli clli = GetById(id);
            clli.ClliId = entity.ClliId;
            clli.SiteNme = entity.SiteNme;
            clli.AddrTxt = entity.AddrTxt;
            clli.SttId = entity.SttId;
            clli.CityNme = entity.CityNme;
            clli.ZipCd = entity.ZipCd;
            clli.CountyNme = entity.CountyNme;
            clli.CmntyNme = entity.CmntyNme;
            clli.Flr = entity.Flr;
            clli.Room = entity.Room;
            clli.LttdeCoord = entity.LttdeCoord;
            clli.LngdeCoord = entity.LngdeCoord;
            clli.SprntOwndCd = entity.SprntOwndCd;
            clli.SitePurpose = entity.SitePurpose;
            clli.SiteOwnr = entity.SiteOwnr;
            clli.SitePhn = entity.SitePhn;
            clli.CmntTxt = entity.CmntTxt;
            clli.EmailCc = entity.EmailCc;
            clli.PsoftUpdDt = entity.PsoftUpdDt;
            clli.SstatUpdDt = entity.SstatUpdDt;
            clli.PsoftSendCd = entity.PsoftSendCd;
            clli.RecStusId = entity.RecStusId;
            clli.CpeClliStusId = entity.CpeClliStusId;

            SaveAll();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }


        public DataTable GetAllRequestData(string stusID, string clliId, string zipCode)
        {
            stusID = (stusID == null) ? "" : stusID;

            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("dbo.getCLLIRequests", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@StusID", stusID);
            command.Parameters.AddWithValue("@CLLI_ID", clliId);
            command.Parameters.AddWithValue("@ZIP_CD", zipCode);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            if(dt.Columns["ACTIVE/INACTIVE"] != null)
            {
                dt.Columns["ACTIVE/INACTIVE"].ColumnName = "IsActive";
            }
           
            return dt;
        }

        public string getClliID(string cityNme, string stateCode)
        {
            var cityNmeKey = cityNme.Substring(0, 4);
            int count = _context.CpeClli.Where(x => x.ClliId.StartsWith(cityNmeKey)).Count() + 1;

            string cpeKey = "";
            if(count == 0)
            {
                cpeKey = "01CPE";
            } else if(count > 0 && count < 10)
            {
                cpeKey = String.Format("0{0}CPE", count);
            }
            else
            {
                cpeKey = String.Format("{0}CPE", count);
            }

            return cityNmeKey + stateCode + cpeKey;
            
        }

        public bool checkClliIdIfExist(int cpeClliId, string clliId)
        {
            if(cpeClliId == 0)
            {
                return _context.CpeClli.Where(x => x.ClliId == clliId).Count() > 0;
            } else
            {
                return _context.CpeClli.Where(x => x.CpeClliId != cpeClliId && x.ClliId == clliId).Count() > 0;
            } 
        }


        public int SaveAll()
        {
            _cache.Remove(CacheKeys.CpeClli);
            return _context.SaveChanges();
        }


        private string GetXMLData(CpeClli clli)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<CLLI>");
            builder.Append("<LocationInfo>");
            //builder.Append(String.Format("<NetworkSiteCode>{0}</NetworkSiteCode>", CLLI.NTWK_SITE_CD));
            builder.Append(String.Format("<CLLI_ID>{0}</CLLI_ID>", clli.ClliId));
            builder.Append(String.Format("<SiteName>{0}</SiteName>", clli.SiteNme));
            //builder.Append(String.Format("<CascadeID>{0}</CascadeID>", CLLI.CASCD_ID));
            //builder.Append(String.Format("<PartnersSiteID>{0}</PartnersSiteID>", CLLI.SITE_ID));
            builder.Append(String.Format("<Address>{0}</Address>", clli.AddrTxt));
            builder.Append(String.Format("<GeoPlaceName>{0}</GeoPlaceName>", clli.CityNme));
            builder.Append(String.Format("<GeoCode>{0}</GeoCode>", clli.SttId));
            builder.Append(String.Format("<PostalCode>{0}</PostalCode>", clli.ZipCd));
            builder.Append(String.Format("<County>{0}</County>", clli.CountyNme));
            builder.Append(String.Format("<CommunityName>{0}</CommunityName>", clli.CmntyNme));
            builder.Append(String.Format("<Floor>{0}</Floor>", clli.Flr));
            builder.Append(String.Format("<Room>{0}</Room>", clli.Room));
            builder.Append(String.Format("<LatitudeCoordinate>{0}</LatitudeCoordinate>", clli.LttdeCoord));
            builder.Append(String.Format("<LongitudeCoordinate>{0}</LongitudeCoordinate>", clli.LngdeCoord));
            builder.Append("</LocationInfo>");
            builder.Append("<MiscellaneousInfo>");
            builder.Append(String.Format("<SprintOwned>{0}</SprintOwned>", clli.SprntOwndCd ? "YES" : "NO"));
            builder.Append(String.Format("<SitePurpose>{0}</SitePurpose>", clli.SitePurpose));
            builder.Append(String.Format("<SiteOwner>{0}</SiteOwner>", clli.SiteOwnr));
            //builder.Append(String.Format("<CarrierCode>{0}</CarrierCode>", CLLI.SWC_NPA));
            //builder.Append(String.Format("<SWC>{0}</SWC>", CLLI.SWC_NPA));
            builder.Append(String.Format("<SitePhone>{0}</SitePhone>", clli.SitePhn));
            builder.Append(String.Format("<Comments>{0}</Comments>", clli.CmntTxt));
            builder.Append(String.Format("<CC>{0}</CC>", clli.EmailCc));
            builder.Append(String.Format("<PSOFT>{0}</PSOFT>", (clli.PsoftUpdDt == null ? "" : Convert.ToDateTime(clli.PsoftUpdDt).ToString("MM/dd/yyyy"))));
            builder.Append(String.Format("<SSTAT>{0}</SSTAT>", (clli.SstatUpdDt == null ? "" : Convert.ToDateTime(clli.SstatUpdDt).ToString("MM/dd/yyyy"))));
            builder.Append(String.Format("<CPE_Link>{0}</CPE_Link>", GetCPE_Link(clli.CpeClliId)));
            builder.Append("</MiscellaneousInfo>");
            builder.Append("</CLLI>");

            return builder.ToString();
        }

        public string GetCPE_Link(int cpeClliId)
        {
            return String.Format("{0}{1}", _configuration.GetSection("AppSettings:ViewCPECLLI").Value, cpeClliId.ToString());
        }

    }
}
