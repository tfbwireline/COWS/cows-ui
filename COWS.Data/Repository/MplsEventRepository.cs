﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MplsEventRepository : IMplsEventRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public MplsEventRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public MplsEvent Create(MplsEvent entity)
        {
            _context.Event.Add(entity.Event);
            SaveAll();

            entity.EventId = entity.Event.EventId;

            // Handle secure data
            if (entity.Event.CsgLvlId > 0)
            {
                SaveCustomerSecureData(null, entity);
                entity.CustNme = null;
                entity.CustCntctNme = null;
                entity.CustCntctPhnNbr = null;
                entity.CustEmailAdr = null;
                entity.CustCntctCellPhnNbr = null;
                entity.CustCntctPgrNbr = null;
                entity.CustCntctPgrPinNbr = null;
                entity.EventTitleTxt = null;
                entity.EventDes = null;
            }

            _context.MplsEvent.Add(entity);
            SaveAll();

            // Add MPLS Event Access Tag
            if (entity.MplsEventAccsTag != null && entity.MplsEventAccsTag.Count > 0)
            {
                foreach (MplsEventAccsTag item in entity.MplsEventAccsTag)
                {
                    if (entity.Event.CsgLvlId > 0)
                    {
                        if (_context.CustScrdData.SingleOrDefault(i => i.ScrdObjId == item.MplsEventAccsTagId
                            && i.ScrdObjTypeId == (byte)SecuredObjectType.MPLS_EVENT_ACCS_TAG) == null)
                        {
                            CustScrdData sdata = new CustScrdData();
                            sdata.ScrdObjId = item.MplsEventAccsTagId;
                            sdata.ScrdObjTypeId = (byte)SecuredObjectType.MPLS_EVENT_ACCS_TAG;
                            sdata.CtyNme = _common.GetEncryptValues(item.LocCtyNme).ToList()[0].Item;
                            sdata.SttPrvnNme = _common.GetEncryptValues(item.LocSttNme).ToList()[0].Item;
                            sdata.CreatDt = DateTime.Now;
                            _context.CustScrdData.Add(sdata);
                        }

                        MplsEventAccsTag updateTag = _context.MplsEventAccsTag
                            .SingleOrDefault(i => i.MplsEventAccsTagId == item.MplsEventAccsTagId);
                        if (updateTag != null)
                        {
                            updateTag.LocCtyNme = null;
                            updateTag.LocSttNme = null;
                            //_context.MplsEventAccsTag.Update(updateTag);
                        }
                    }
                }
            }

            SaveAll();

            return GetById(entity.EventId);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<MplsEvent> Find(Expression<Func<MplsEvent, bool>> predicate, string adid = null)
        {
            var mpls = _context.MplsEvent
                            .Include(i => i.ActyLocale)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.EsclReas)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.MplsEventActyType)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.MplsEventVasType)
                            .Include(i => i.EventStus)
                            .Include(i => i.IpVer)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.MplsCxrPtnr)
                            .Include(i => i.MplsEventType)
                            .Include(i => i.MplsMgrtnType)
                            .Include(i => i.MultiVrfReq)
                            .Include(i => i.ReqorUser)
                            .Include(i => i.SalsUser)
                            .Include(i => i.VpnPltfrmType)
                            .Include(i => i.WhlslPtnr)
                            .Include(i => i.WrkflwStus)
                            .Include(i => i.MplsEventAccsTag)
                            .Where(predicate);

            if (mpls != null && mpls.Count() > 0)
            {
                foreach (MplsEvent item in mpls)
                {
                    if (mpls.Count() == 1)
                    {
                        if (item.Event.CsgLvlId > 0 && adid != null)
                        {
                            // LogWebActivity
                            var UserCsg = _common.GetCSGLevelAdId(adid);
                            _common.LogWebActivity(((byte)WebActyType.EventDetails), item.Event.EventId.ToString(), adid, (byte)UserCsg, (byte)item.Event.CsgLvlId);
                        }
                    }

                    if (item.Event.CsgLvlId > 0)
                    {
                        GetEventDecryptValues sdata = _common.GetEventDecryptValues(item.EventId, "MPLS").FirstOrDefault();
                        if (sdata != null)
                        {
                            item.CustNme = sdata.CUST_NME;
                            item.CustCntctNme = sdata.CUST_CNTCT_NME;
                            item.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                            item.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                            item.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                            item.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                            item.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                            item.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                            item.EventDes = sdata.EVENT_DES;
                        }

                        if (item.MplsEventAccsTag != null && item.MplsEventAccsTag.Count() > 0)
                        {
                            foreach (MplsEventAccsTag tag in item.MplsEventAccsTag)
                            {
                                CustScrdData sTagData = _context.CustScrdData
                                    .SingleOrDefault(i => i.ScrdObjId == tag.MplsEventAccsTagId
                                        && i.ScrdObjTypeId == (byte)SecuredObjectType.MPLS_EVENT_ACCS_TAG);
                                if (sTagData != null)
                                {
                                    tag.LocCtyNme = _common.GetDecryptValue(sTagData.CtyNme);
                                    tag.LocSttNme = _common.GetDecryptValue(sTagData.SttPrvnNme);
                                }
                            }
                        }
                    }
                }
            }

            return mpls;
        }

        public IEnumerable<MplsEvent> GetAll()
        {
            return _context.MplsEvent
                            .ToList();
        }

        public MplsEvent GetById(int id)
        {
            var mpls = _context.MplsEvent
                            .Include(i => i.ActyLocale)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.EsclReas)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.MplsEventActyType)
                            .Include(i => i.Event).ThenInclude(evnt => evnt.MplsEventVasType)
                            .Include(i => i.EventStus)
                            .Include(i => i.IpVer)
                            .Include(i => i.ModfdByUser)
                            .Include(i => i.MplsCxrPtnr)
                            .Include(i => i.MplsEventType)
                            .Include(i => i.MplsMgrtnType)
                            .Include(i => i.MultiVrfReq)
                            .Include(i => i.ReqorUser)
                            .Include(i => i.SalsUser)
                            .Include(i => i.VpnPltfrmType)
                            .Include(i => i.WhlslPtnr)
                            .Include(i => i.WrkflwStus)
                            .Include(i => i.MplsEventAccsTag)
                            .SingleOrDefault(i => i.EventId == id);

            if (mpls.Event.CsgLvlId > 0)
            {
                GetEventDecryptValues sdata = _common.GetEventDecryptValues(mpls.EventId, "MPLS").FirstOrDefault();
                if (sdata != null)
                {
                    mpls.CustNme = sdata.CUST_NME;
                    mpls.CustCntctNme = sdata.CUST_CNTCT_NME;
                    mpls.CustCntctPhnNbr = sdata.CUST_CNTCT_PHN_NBR;
                    mpls.CustEmailAdr = sdata.CUST_EMAIL_ADR;
                    mpls.CustCntctCellPhnNbr = sdata.CUST_CNTCT_CELL_PHN_NBR;
                    mpls.CustCntctPgrNbr = sdata.CUST_CNTCT_PGR_NBR;
                    mpls.CustCntctPgrPinNbr = sdata.CUST_CNTCT_PGR_PIN_NBR;
                    mpls.EventTitleTxt = sdata.EVENT_TITLE_TXT;
                    mpls.EventDes = sdata.EVENT_DES;
                }

                if (mpls.MplsEventAccsTag != null && mpls.MplsEventAccsTag.Count() > 0)
                {
                    foreach (MplsEventAccsTag item in mpls.MplsEventAccsTag)
                    {
                        CustScrdData sTagData = _context.CustScrdData
                            .SingleOrDefault(i => i.ScrdObjId == item.MplsEventAccsTagId
                                && i.ScrdObjTypeId == (byte)SecuredObjectType.MPLS_EVENT_ACCS_TAG);
                        if (sTagData != null)
                        {
                            item.LocCtyNme = _common.GetDecryptValue(sTagData.CtyNme);
                            item.LocSttNme = _common.GetDecryptValue(sTagData.SttPrvnNme);
                        }
                    }
                }
            }

            return mpls;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, MplsEvent entity)
        {
            MplsEvent updateMpls = GetById(id);

            // Customer Secured Data
            if (entity.Event.CsgLvlId > 0 && (updateMpls.Event.CsgLvlId != entity.Event.CsgLvlId))
            {
                updateMpls.Event.CsgLvlId = entity.Event.CsgLvlId;
                updateMpls.Event.ModfdDt = DateTime.Now;
                //_context.Event.Update(updateMpls.Event);

                SaveCustomerSecureData(updateMpls, entity);

                updateMpls.CustNme = null;
                updateMpls.CustCntctNme = null;
                updateMpls.CustCntctPhnNbr = null;
                updateMpls.CustEmailAdr = null;
                updateMpls.CustCntctCellPhnNbr = null;
                updateMpls.CustCntctPgrNbr = null;
                updateMpls.CustCntctPgrPinNbr = null;
                updateMpls.EventTitleTxt = null;
                updateMpls.EventDes = null;
            }
            else
            {
                updateMpls.EventTitleTxt = entity.EventTitleTxt;
                updateMpls.EventDes = entity.EventDes;
                updateMpls.CustNme = entity.CustNme;
                updateMpls.CustEmailAdr = entity.CustEmailAdr;
                updateMpls.CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
                updateMpls.CustCntctPhnNbr = entity.CustCntctPhnNbr;
                updateMpls.CustCntctNme = entity.CustNme;
                updateMpls.CustCntctPgrNbr = entity.CustCntctPgrNbr;
                updateMpls.CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
            }

            updateMpls.EventStusId = entity.EventStusId;
            updateMpls.Ftn = entity.Ftn;
            updateMpls.CharsId = entity.CharsId;
            updateMpls.H1 = entity.H1;
            updateMpls.H6 = entity.H6;

            updateMpls.ReqorUserId = entity.ReqorUserId;
            updateMpls.CnfrcBrdgNbr = entity.CnfrcBrdgNbr;
            updateMpls.CnfrcPinNbr = entity.CnfrcPinNbr;
            updateMpls.SalsUserId = entity.SalsUserId;

            updateMpls.PubEmailCcTxt = entity.PubEmailCcTxt;
            updateMpls.CmpltdEmailCcTxt = entity.CmpltdEmailCcTxt;
            updateMpls.DocLinkTxt = entity.DocLinkTxt;
            updateMpls.DdAprvlNbr = entity.DdAprvlNbr;
            updateMpls.NddUpdtdCd = entity.NddUpdtdCd;

            updateMpls.DesCmntTxt = entity.DesCmntTxt;
            updateMpls.MplsEventTypeId = entity.MplsEventTypeId;
            updateMpls.VpnPltfrmTypeId = entity.VpnPltfrmTypeId;
            updateMpls.IpVerId = entity.IpVerId;
            updateMpls.ActyLocaleId = (string.IsNullOrWhiteSpace(entity.ActyLocaleId) || entity.ActyLocaleId == "0")
                                                            ? null : entity.ActyLocaleId;
            updateMpls.MultiVrfReqId = entity.MultiVrfReqId;
            updateMpls.VrfNme = entity.VrfNme;
            updateMpls.MdsMngdCd = entity.MdsMngdCd;
            updateMpls.AddE2eMontrgCd = entity.AddE2eMontrgCd;
            updateMpls.MdsVrfNme = entity.MdsVrfNme;
            updateMpls.MdsIpAdr = entity.MdsIpAdr;
            updateMpls.MdsDlci = entity.MdsDlci;
            updateMpls.MdsStcRteDes = entity.MdsStcRteDes;
            updateMpls.OnNetMontrgCd = entity.OnNetMontrgCd;
            updateMpls.WhlslPtnrCd = entity.WhlslPtnrCd;
            updateMpls.WhlslPtnrId = entity.WhlslPtnrId;
            updateMpls.NniDsgnDocNme = entity.NniDsgnDocNme;
            updateMpls.CxrPtnrCd = entity.CxrPtnrCd;
            updateMpls.MplsCxrPtnrId = entity.MplsCxrPtnrId;
            updateMpls.OffNetMontrgCd = entity.OffNetMontrgCd;
            updateMpls.MgrtnCd = entity.MgrtnCd;
            updateMpls.MplsMgrtnTypeId = entity.MplsMgrtnTypeId;
            updateMpls.ReltdCmpsNcrCd = entity.ReltdCmpsNcrCd;
            updateMpls.ReltdCmpsNcrNme = entity.ReltdCmpsNcrNme;

            updateMpls.EsclCd = entity.EsclCd;
            updateMpls.EsclReasId = entity.EsclReasId;
            updateMpls.PrimReqDt = entity.PrimReqDt;
            updateMpls.ScndyReqDt = entity.ScndyReqDt;
            updateMpls.StrtTmst = entity.StrtTmst;
            updateMpls.EndTmst = entity.EndTmst;
            updateMpls.EventDrtnInMinQty = entity.EventDrtnInMinQty;
            updateMpls.ExtraDrtnTmeAmt = entity.ExtraDrtnTmeAmt;

            updateMpls.WrkflwStusId = entity.WrkflwStusId;
            updateMpls.ModfdByUserId = entity.ModfdByUserId;
            updateMpls.ModfdDt = DateTime.Now;

            // MPLS Event Activity Types and Vas Types
            SaveMplsEventActivityTypes(id, entity.Event.MplsEventActyType.ToList());
            SaveMplsEventVasTypes(id, entity.Event.MplsEventVasType.ToList());

            SaveAll();
        }

        private void SaveCustomerSecureData(MplsEvent oldMpls, MplsEvent entity)
        {
            SecuredData obj = new SecuredData(entity);
            List<GetEncryptValues> EncValues = _common.GetEventEncryptValues(obj).ToList();

            CustScrdData sdata = new CustScrdData();
            sdata.ScrdObjId = entity.Event.EventId;
            sdata.ScrdObjTypeId = (byte)SecuredObjectType.MPLS_EVENT;
            sdata.CustNme = EncValues[0].Item;
            sdata.CustCntctNme = EncValues[1].Item;
            sdata.CustCntctPhnNbr = EncValues[2].Item;
            sdata.CustEmailAdr = EncValues[3].Item;
            sdata.CustCntctCellPhnNbr = EncValues[4].Item;
            sdata.CustCntctPgrNbr = EncValues[5].Item;
            sdata.CustCntctPgrPinNbr = EncValues[6].Item;
            sdata.EventTitleTxt = EncValues[7].Item;
            sdata.EventDes = EncValues[8].Item;

            // For Update
            if (oldMpls != null)
            {
                CustScrdData updateCSD = _context.CustScrdData
                    .SingleOrDefault(i => i.ScrdObjId == oldMpls.EventId
                        && i.ScrdObjTypeId == (byte)SecuredObjectType.MPLS_EVENT);

                bool insertCSD = updateCSD == null ? true : false;

                if (insertCSD && entity.Event.CsgLvlId > 0)
                {
                    // Create New Customer Secured Data
                    sdata.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(sdata);
                }
                else if (!insertCSD && entity.Event.CsgLvlId == 0)
                {
                    // Remove existing Customer Secured Data
                    _context.CustScrdData.Remove(updateCSD);
                }
                else if (!insertCSD && entity.Event.CsgLvlId > 0)
                {
                    // Update existing Customer Secured Data
                    updateCSD = sdata;
                    updateCSD.ModfdDt = DateTime.Now;
                    //_context.CustScrdData.Update(sdata);
                }
            }
            else
            {
                // For Insert
                sdata.CreatDt = DateTime.Now;
                _context.CustScrdData.Add(sdata);
            }
        }

        private void SaveMplsEventActivityTypes(int eventId, List<MplsEventActyType> list)
        {
            // Delete all existing type and create new instance
            // in case of additional type has been added/removed
            var mplsEvntActTypes = _context.MplsEventActyType.Where(i => i.EventId == eventId).ToList();
            if (mplsEvntActTypes != null && mplsEvntActTypes.Count > 0)
            {
                _context.MplsEventActyType.RemoveRange(mplsEvntActTypes);
            }

            // Add Activity Types
            if (list != null && list.Count() > 0)
            {
                foreach (MplsEventActyType item in list)
                {
                    item.Event = null;
                    item.EventId = eventId;
                    _context.MplsEventActyType.Add(item);
                }
            }
        }

        private void SaveMplsEventVasTypes(int eventId, List<MplsEventVasType> list)
        {
            // Delete all existing type and create new instance
            // in case of additional type has been added/removed
            var vasTypes = _context.MplsEventVasType.Where(i => i.EventId == eventId).ToList();
            if (vasTypes != null && vasTypes.Count > 0)
            {
                _context.MplsEventVasType.RemoveRange(vasTypes);
            }

            // Add VAS Types
            if (list != null && list.Count > 0)
            {
                foreach (MplsEventVasType item in list)
                {
                    item.Event = null;
                    item.EventId = eventId;
                    _context.MplsEventVasType.Add(item);
                }
            }
        }
    }
}