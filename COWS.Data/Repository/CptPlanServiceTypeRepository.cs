﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptPlanServiceTypeRepository : ICptPlanServiceTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptPlanServiceTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptPlnSrvcType Create(LkCptPlnSrvcType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptPlnSrvcType> Find(Expression<Func<LkCptPlnSrvcType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPlnSrvcType, out List<LkCptPlnSrvcType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptPlnSrvcType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPlnSrvcType, out List<LkCptPlnSrvcType> list))
            {
                list = _context.LkCptPlnSrvcType
                            .OrderBy(i => i.CptPlnSrvcType)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptPlnSrvcType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptPlnSrvcType GetById(int id)
        {
            return _context.LkCptPlnSrvcType
                            .SingleOrDefault(i => i.CptPlnSrvcTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptPlnSrvcType);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptPlnSrvcType entity)
        {
            throw new NotImplementedException();
        }
    }
}