﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class EscalationReasonRepository : IEscalationReasonRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public EscalationReasonRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkEsclReas> Find(Expression<Func<LkEsclReas, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkEsclReas, out List<LkEsclReas> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkEsclReas> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkEsclReas, out List<LkEsclReas> list))
            {
                list = _context.LkEsclReas
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkEsclReas, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkEsclReas GetById(int id)
        {
            return _context.LkEsclReas
                .SingleOrDefault(a => a.EsclReasId == id);
        }

        public LkEsclReas Create(LkEsclReas entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkEsclReas entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkEsclReas);
            return _context.SaveChanges();
        }
    }
}