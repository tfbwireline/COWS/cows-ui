﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class VpnPlatformTypeRepository : IVpnPlatformTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public VpnPlatformTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkVpnPltfrmType Create(LkVpnPltfrmType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkVpnPltfrmType> Find(Expression<Func<LkVpnPltfrmType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkVpnPltfrmType, out List<LkVpnPltfrmType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkVpnPltfrmType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkVpnPltfrmType, out List<LkVpnPltfrmType> list))
            {
                list = _context.LkVpnPltfrmType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkVpnPltfrmType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkVpnPltfrmType GetById(int id)
        {
            return _context.LkVpnPltfrmType
                .SingleOrDefault(a => a.VpnPltfrmTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkVpnPltfrmType);
            return _context.SaveChanges();
        }

        public void Update(int id, LkVpnPltfrmType entity)
        {
            throw new NotImplementedException();
        }
    }
}