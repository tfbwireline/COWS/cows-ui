﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MnsPerformanceReportingRepository : IMnsPerformanceReportingRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MnsPerformanceReportingRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkMnsPrfmcRpt Create(LkMnsPrfmcRpt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkMnsPrfmcRpt> Find(Expression<Func<LkMnsPrfmcRpt, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMnsPrfmcRpt, out List<LkMnsPrfmcRpt> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkMnsPrfmcRpt> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMnsPrfmcRpt, out List<LkMnsPrfmcRpt> list))
            {
                list = _context.LkMnsPrfmcRpt
                            .OrderBy(i => i.MnsPrfmcRptDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMnsPrfmcRpt, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMnsPrfmcRpt GetById(int id)
        {
            return _context.LkMnsPrfmcRpt
                            .SingleOrDefault(i => i.MnsPrfmcRptId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMnsPrfmcRpt);
            throw new NotImplementedException();
        }

        public void Update(int id, LkMnsPrfmcRpt entity)
        {
            throw new NotImplementedException();
        }
    }
}