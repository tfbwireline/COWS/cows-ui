﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptPrimSiteRepository : ICptPrimSiteRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptPrimSiteRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptPrimSite Create(LkCptPrimSite entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptPrimSite> Find(Expression<Func<LkCptPrimSite, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPrimSite, out List<LkCptPrimSite> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptPrimSite> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPrimSite, out List<LkCptPrimSite> list))
            {
                list = _context.LkCptPrimSite
                            .OrderBy(i => i.CptPrimSite)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptPrimSite, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptPrimSite GetById(int id)
        {
            return _context.LkCptPrimSite
                            .SingleOrDefault(i => i.CptPrimSiteId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptPrimSite);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptPrimSite entity)
        {
            throw new NotImplementedException();
        }
    }
}