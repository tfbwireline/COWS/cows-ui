﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OrderStdiHistoryRepository : IOrderStdiHistoryRepository
    {
        private readonly COWSAdminDBContext _context;

        public OrderStdiHistoryRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public OrdrStdiHist Create(OrdrStdiHist entity)
        {
            entity.StdiReas = _context.LkStdiReas
                .Where(a => a.StdiReasId == entity.StdiReasId)
                .SingleOrDefault();
            _context.OrdrStdiHist.Add(entity);
            SaveAll();

            return GetById(entity.OrdrStdiId);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<OrdrStdiHist> Find(Expression<Func<OrdrStdiHist, bool>> predicate)
        {
            return _context.OrdrStdiHist
                .Include(a => a.StdiReas)
                .Include(a => a.Ordr.IplOrdr)
                .Include(a => a.Ordr.FsaOrdr)
                .Include(a => a.Ordr.NccoOrdr)
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<OrdrStdiHist> GetAll()
        {
            return _context.OrdrStdiHist
                .Include(a => a.StdiReas)
                .Include(a => a.Ordr.IplOrdr)
                .Include(a => a.Ordr.FsaOrdr)
                .Include(a => a.Ordr.NccoOrdr)
                .Include(a => a.CreatByUser)
                            .OrderBy(i => i.OrdrStdiId)
                            .ToList();
        }

        public OrdrStdiHist GetById(int id)
        {
            return _context.OrdrStdiHist
                .Include(a => a.StdiReas)
                .Include(a => a.Ordr.IplOrdr)
                .Include(a => a.Ordr.FsaOrdr)
                .Include(a => a.Ordr.NccoOrdr)
                .Include(a => a.CreatByUser)
                .Where(a => a.OrdrStdiId == id)
                .SingleOrDefault();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, OrdrStdiHist entity)
        {
            throw new NotImplementedException();
        }
    }
}