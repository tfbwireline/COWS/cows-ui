﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.SqlClient;

namespace COWS.Data.Repository
{
    public class DashboardRepository : IDashboardRepository
    {
        private readonly ReportsDbContext _context;
        
        public DashboardRepository(ReportsDbContext context)
        {
            _context = context;
        }

        public DashboardData Get()
        {
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandTimeout = _context.commandTimeout;
                command.CommandText = "[dbo].[GetDashBoardData] @Type";
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Type", SqlDbType = SqlDbType.VarChar, Value = "ALL" });

                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    var result = new DashboardData
                    {
                        MRCNRC = reader.Translate<MRCNRCData>(),
                        AverageInstallations = reader.Translate<AverageInstallationData>(),
                        InstallOrders = reader.Translate<OrderData>(),
                        NewOrders = reader.Translate<OrderData>()
                    };

                    return result;
                }
            }

        }

        //public DashboardData DashboardDataMapper(DbDataReader reader)
        //{
        //    var result = new DashboardData
        //    {
        //        MRCNRC = reader.Translate<MRCNRCData>(),

        //        AverageInstallations = reader.Translate<AverageInstallationData>(),

        //        NewOrders = reader.Translate<OrderData>(),

        //        InstallOrders = reader.Translate<OrderData>()
        //    };

        //    return result;
        //}


    }
}
