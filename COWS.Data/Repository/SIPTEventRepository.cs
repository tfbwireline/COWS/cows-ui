﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class SIPTEventRepository : ISIPTEventRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public SIPTEventRepository(COWSAdminDBContext context,
            ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public SiptEvent Create(SiptEvent entity)
        {
            _context.Event.Add(entity.Event);
            SaveAll();

            entity.EventId = entity.Event.EventId;
            _context.SiptEvent.Add(entity);
            SaveAll();

            return GetById(entity.EventId);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<SiptEvent> Find(Expression<Func<SiptEvent, bool>> predicate, string adid = null)
        {
            var sipt = _context.SiptEvent
                .Include(a => a.NtwkEngr)
                .Include(a => a.NtwkTechEngr)
                .Include(a => a.Pm)
                .Include(i => i.Event).ThenInclude(evnt => evnt.SiptEventActy)
                .Include(i => i.Event).ThenInclude(evnt => evnt.SiptEventTollType)
                .Include(i => i.Event).ThenInclude(evnt => evnt.SiptReltdOrdr)
                .Include(a => a.SiptEventDoc)
                .Include(a => a.EventStus)
                .Include(a => a.WrkflwStus)
                .Include(a => a.CreatByUser)
                .Where(predicate);

            if (sipt.Count() == 1)
            {
                var data = sipt.FirstOrDefault();
                if (adid != null)
                {
                    if (data.Event.CsgLvlId > 0)
                    {
                        // LogWebActivity
                        var UserCsg = _common.GetCSGLevelAdId(adid);
                        _common.LogWebActivity(((byte)WebActyType.EventDetails), data.Event.EventId.ToString(), adid, (byte)UserCsg, (byte)data.Event.CsgLvlId);
                    }
                }
            }

            return sipt;

        }

        public IEnumerable<SiptEvent> GetAll()
        {
            return _context.SiptEvent
                .ToList();
        }

        public SiptEvent GetById(int id)
        {
            var siptEvent =  _context.SiptEvent
                                        .Include(a => a.NtwkEngr)
                                        .Include(a => a.NtwkTechEngr)
                                        .Include(a => a.Pm)
                                        .Include(i => i.Event).ThenInclude(evnt => evnt.SiptEventActy)
                                        .Include(i => i.Event).ThenInclude(evnt => evnt.SiptEventTollType)
                                        .Include(i => i.Event).ThenInclude(evnt => evnt.SiptReltdOrdr)
                                        .Include(a => a.SiptEventDoc)
                                        .Include(a => a.EventStus)
                                        .Include(a => a.WrkflwStus)
                                        .Include(a => a.CreatByUser)
                                        .SingleOrDefault(i => i.EventId == id);

            if (siptEvent.Event.CsgLvlId > 0)
            {
                CustScrdData csd = _context.CustScrdData
                    .FirstOrDefault(i => i.ScrdObjId == id && i.ScrdObjTypeId == (byte)SecuredObjectType.SIPT_EVENT);

                if (csd != null)
                {
                    siptEvent.EventTitleTxt = _common.GetDecryptValue(csd.EventTitleTxt);
                    siptEvent.CustNme = _common.GetDecryptValue(csd.CustNme);
                    siptEvent.SiteCntctNme = _common.GetDecryptValue(csd.CustCntctNme);
                    siptEvent.SiteCntctPhnNbr = _common.GetDecryptValue(csd.CustCntctPhnNbr);
                    siptEvent.SiteCntctEmailAdr = _common.GetDecryptValue(csd.CustEmailAdr);
                    siptEvent.StreetAdr = (string.Format("{0} {1} {2}", _common.GetDecryptValue(csd.StreetAdr1),
                        csd.StreetAdr2 != null ? string.Empty : _common.GetDecryptValue(csd.StreetAdr2),
                        csd.StreetAdr3 != null ? string.Empty : _common.GetDecryptValue(csd.StreetAdr3))).Trim();
                    siptEvent.FlrBldgNme = csd.BldgNme != null ? _common.GetDecryptValue(csd.FlrId) 
                        : _common.GetDecryptValue(csd.BldgNme);
                    siptEvent.CtyNme = _common.GetDecryptValue(csd.CtyNme);
                    siptEvent.SttPrvnNme = _common.GetDecryptValue(csd.SttPrvnNme);
                    siptEvent.CtryRgnNme = _common.GetDecryptValue(csd.CtryRgnNme);
                    siptEvent.ZipCd = _common.GetDecryptValue(csd.ZipPstlCd);
                }
            }

            return siptEvent;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, SiptEvent entity)
        {
            throw new NotImplementedException();
        }

        public SiptEvent Create(SiptModel Obj, string adId)
        {
            int newEvtId = 0;
            byte CSGLvlID = Obj.EventCsgLvlId;
            //lock (this)
            //{
                Event evt = new Event();
                evt.EventTypeId = (int)EventType.SIPT;
                evt.CsgLvlId = CSGLvlID;
                evt.CreatDt = DateTime.Now;
                _context.Event.Add(evt);
                SaveAll();

                newEvtId = (int)(from e in _context.Event select (int?)e.EventId).Max();
            //}

            SiptEvent newSIPT = new SiptEvent();
            CustScrdData newCSD = new CustScrdData();

            if (Obj.SiptEventActyIds != null && Obj.SiptEventActyIds.Count() > 0)
            {
                List<SiptEventActy> siptActyType = new List<SiptEventActy>();
                SiptEventActy dc1 = null;
                foreach (var d in Obj.SiptEventActyIds)
                {
                    dc1 = new SiptEventActy();
                    dc1.SiptActyTypeId = (short)d;
                    dc1.EventId = newEvtId;
                    dc1.CreatDt = DateTime.Now;
                    //siptActyType.Add(dc1);
                    _context.SiptEventActy.Add(dc1);
                }
            }

            if (Obj.SiptEventTollTypeIds != null && Obj.SiptEventTollTypeIds.Count() > 0)
            {
                List<SiptEventTollType> siptTollType = new List<SiptEventTollType>();
                SiptEventTollType dc2 = null;
                foreach (var d in Obj.SiptEventTollTypeIds)
                {
                    dc2 = new SiptEventTollType();
                    dc2.SiptTollTypeId = (byte)d;
                    dc2.EventId = newEvtId;
                    dc2.CreatDt = DateTime.Now;
                    //siptTollType.Add(dc2);
                    _context.SiptEventTollType.Add(dc2);
                }
            }

            if (Obj.SiptReltdOrdrNos != null && Obj.SiptReltdOrdrNos.Count() > 0)
            {
                List<SiptReltdOrdr> RltdOrdrs = new List<SiptReltdOrdr>();
                SiptReltdOrdr dc3 = null;
                foreach (var d in Obj.SiptReltdOrdrNos)
                {
                    dc3 = new SiptReltdOrdr();
                    dc3.M5OrdrNbr = d.ToString();
                    dc3.EventId = newEvtId;
                    dc3.CreatDt = DateTime.Now;
                    //RltdOrdrs.Add(dc3);
                    _context.SiptReltdOrdr.Add(dc3);
                }
            }

            if (Obj.SiptEventDoc != null && Obj.SiptEventDoc.Count() > 0)
            {
                foreach (var d in Obj.SiptEventDoc)
                {
                    d.EventId = newEvtId;
                    _context.SiptEventDoc.Add(d);
                }
            }

            newSIPT.EventId = newEvtId;
            newSIPT.CreatByUserId = Obj.CreatByUserId;
            newSIPT.CreatDt = DateTime.Now;
            //newSIPT.ModfdByUserId = Obj.ModfdByUserId;
            //newSIPT.ModfdDt = DateTime.Now;

            AssignCommonFields(Obj, ref newSIPT, ref newCSD, CSGLvlID, newEvtId);

            if (newCSD.ScrdObjId > 0)
            {
                newCSD.CreatDt = DateTime.Now;
                _context.CustScrdData.Add(newCSD);
            }
            _context.SiptEvent.Add(newSIPT);
            SaveAll();

            return GetById(newSIPT.EventId);
        }

        public bool Update(int id, SiptModel Obj)
        {
            SiptEvent oldSipt = GetById(id);
            SiptEvent newSIPT = new SiptEvent();
            byte CSGLvlID = Obj.EventCsgLvlId;
            byte OldCSGLvlID = oldSipt.Event.CsgLvlId;
            SiptEvent updtSIPT = (SiptEvent)(from se in _context.SiptEvent where se.EventId == id select se).Single();
            CustScrdData updtCSD = (CustScrdData)(from csd in _context.CustScrdData where csd.ScrdObjId == id && csd.ScrdObjTypeId == ((byte)SecuredObjectType.SIPT_EVENT) select csd).SingleOrDefault();

            updtSIPT.ModfdByUserId = Obj.ModfdByUserId;
            updtSIPT.ModfdDt = DateTime.Now;
            bool bInsertCSD = false;
            if (updtCSD == null)
            {
                updtCSD = new CustScrdData();
                bInsertCSD = true;
            }
            
            AssignCommonFields(Obj, ref updtSIPT, ref updtCSD, CSGLvlID, id);

            if (Obj.SiptEventActyIds != null && Obj.SiptEventActyIds.Count() > 0)
            {
                DeleteAllActTypes(id);
                List<SiptEventActy> siptActyType = new List<SiptEventActy>();
                SiptEventActy dc1 = null;
                foreach (var d in Obj.SiptEventActyIds)
                {
                    dc1 = new SiptEventActy();
                    dc1.SiptActyTypeId = (short)d;
                    dc1.EventId = id;
                    dc1.CreatDt = DateTime.Now;
                    //siptActyType.Add(dc1);
                    _context.SiptEventActy.Add(dc1);
                }
            }

            if (Obj.SiptEventTollTypeIds != null && Obj.SiptEventTollTypeIds.Count() > 0)
            {
                DeleteAllTollTypes(id);
                List<SiptEventTollType> siptTollType = new List<SiptEventTollType>();
                SiptEventTollType dc2 = null;
                foreach (var d in Obj.SiptEventTollTypeIds)
                {
                    dc2 = new SiptEventTollType();
                    dc2.SiptTollTypeId = (byte)d;
                    dc2.EventId = id;
                    dc2.CreatDt = DateTime.Now;
                    //siptTollType.Add(dc2);
                    _context.SiptEventTollType.Add(dc2);
                }
            }

            if (Obj.SiptReltdOrdrNos != null && Obj.SiptReltdOrdrNos.Count() > 0)
            {
                DeleteAllRltdOrdrs(id);
                List<SiptReltdOrdr> RltdOrdrs = new List<SiptReltdOrdr>();
                SiptReltdOrdr dc3 = null;
                foreach (var d in Obj.SiptReltdOrdrNos)
                {
                    dc3 = new SiptReltdOrdr();
                    dc3.M5OrdrNbr = d.ToString();
                    dc3.EventId = id;
                    dc3.CreatDt = DateTime.Now;
                    //RltdOrdrs.Add(dc3);
                    _context.SiptReltdOrdr.Add(dc3);
                }
            }

            if (Obj.SiptEventDoc != null && Obj.SiptEventDoc.Count() > 0)
            {
                foreach (var d in Obj.SiptEventDoc)
                {
                    if (_context.SiptEventDoc.Where(i => i.EventId == id && i.FileNme == d.FileNme) == null)
                    {
                        d.EventId = id;
                        _context.SiptEventDoc.Add(d);
                    }
                }
            }

            if ((OldCSGLvlID != CSGLvlID))
            {
                Event updtEv = (Event)(from ev in _context.Event where ev.EventId == id select ev).Single();
                updtEv.CsgLvlId = CSGLvlID;
                _context.Event.Update(updtEv);
            }

            if ((bInsertCSD) && (CSGLvlID > 0) && (updtCSD.ScrdObjId > 0))
            {
                updtCSD.CreatDt = DateTime.Now;
                _context.CustScrdData.Update(updtCSD);
            }
            else if ((!bInsertCSD) && (CSGLvlID == 0) && (updtCSD.ScrdObjId > 0))
                _context.CustScrdData.Remove(updtCSD);

            _context.SiptEvent.Update(updtSIPT);
            SaveAll();

            return true;
        }

        public IEnumerable<LkSiptProdType> GetAllSIPTProdTypes()
        {
            return _context.LkSiptProdType
                .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                .ToList();
        }

        public IEnumerable<LkSiptTollType> GetAllSIPTTollTypes()
        {
            return _context.LkSiptTollType
                .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                .ToList();
        }

        public IEnumerable<LkSiptActyType> GetAllSIPTActyType()
        {
            return _context.LkSiptActyType
                .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                .ToList();
        }

        public void AddActTypes(int eventId, List<SiptEventActy> obj)
        {
            List<SiptEventActy> siptActyType = new List<SiptEventActy>();
            SiptEventActy dc = null;
            DeleteAllActTypes(eventId);
            foreach (var d in obj)
            {
                dc = new SiptEventActy();
                dc.SiptActyTypeId = d.SiptActyTypeId;
                dc.EventId = d.EventId;
                dc.CreatDt = DateTime.Now;
                siptActyType.Add(dc);
                _context.SiptEventActy.Add(dc);
            }

            SaveAll();
        }

        public void AddTollTypes(int eventId, List<SiptEventTollType> obj)
        {
            List<SiptEventTollType> siptTollType = new List<SiptEventTollType>();
            SiptEventTollType dc = null;
            DeleteAllTollTypes(eventId);
            foreach (var d in obj)
            {
                dc = new SiptEventTollType();
                dc.SiptTollTypeId = d.SiptTollTypeId;
                dc.EventId = d.EventId;
                dc.CreatDt = DateTime.Now;
                siptTollType.Add(dc);
                _context.SiptEventTollType.Add(dc);
            }

            SaveAll();
        }

        public void AddRltdOrdrs(int eventId, List<SiptReltdOrdr> obj)
        {
            List<SiptReltdOrdr> RltdOrdrs = new List<SiptReltdOrdr>();
            SiptReltdOrdr dc = null;
            DeleteAllRltdOrdrs(eventId);
            foreach (var d in obj)
            {
                dc = new SiptReltdOrdr();
                dc.M5OrdrNbr = d.M5OrdrNbr;
                dc.EventId = d.EventId;
                dc.CreatDt = DateTime.Now;
                RltdOrdrs.Add(dc);
                _context.SiptReltdOrdr.Add(dc);
            }

            SaveAll();
        }

        public void DeleteAllTollTypes(int eventId)
        {
            var SiptEventTollType = _context.SiptEventTollType
                           .Where(i => i.EventId == eventId);

            if (SiptEventTollType != null)
            {
                foreach (var ac in SiptEventTollType)
                {
                    _context.SiptEventTollType.Remove(ac);
                }
            }
            SaveAll();
        }

        public void DeleteAllActTypes(int eventId)
        {
            var SiptEventActy = _context.SiptEventActy
                           .Where(i => i.EventId == eventId);
            if (SiptEventActy != null)
            {
                foreach (var ac in SiptEventActy)
                {
                    _context.SiptEventActy.Remove(ac);
                }
            }
            SaveAll();
        }

        public void DeleteAllRltdOrdrs(int eventId)
        {
            var SiptReltdOrdr = _context.SiptReltdOrdr
                           .Where(i => i.EventId == eventId);

            if (SiptReltdOrdr != null)
            {
                foreach (var ac in SiptReltdOrdr)
                {
                    _context.SiptReltdOrdr.Remove(ac);
                }
            }
            SaveAll();
        }

        public LkSiptTollType GetSIPTTollTypeByID(int id)
        {
            LkSiptTollType dc = null;
            dc = _context.LkSiptTollType
                .SingleOrDefault(a => a.RecStusId == 1 &&
                                a.SiptTollTypeId == id);
            return dc;
        }

        public IEnumerable<SiptReltdOrdr> GetRltdOrdrs(int EventId)
        {
            List<SiptReltdOrdr> siptRltdOrdrList = new List<SiptReltdOrdr>();
            SiptReltdOrdr dc = null;
            var q = (from pt in _context.SiptReltdOrdr
                     where (pt.EventId == EventId)
                     orderby pt.SiptReltdOrdrId
                     select new { pt.SiptReltdOrdrId, pt.EventId, pt.M5OrdrNbr, pt.CreatDt });
            foreach (var d in q)
            {
                dc = new SiptReltdOrdr();
                dc.SiptReltdOrdrId = d.SiptReltdOrdrId;
                dc.EventId = d.EventId;
                dc.M5OrdrNbr = d.M5OrdrNbr;
                dc.CreatDt = d.CreatDt;
                siptRltdOrdrList.Add(dc);
            }
            return siptRltdOrdrList;
        }

        public LkSiptActyType GetSIPTActyTypeByID(int id)
        {
            LkSiptActyType dc = null;
            dc = _context.LkSiptActyType
                .SingleOrDefault(a => a.RecStusId == 1 &&
                                a.SiptActyTypeId == id);
            return dc;
        }

        public bool StatusChanged(int eventID, byte eEventStatus, byte wfStatus)
        {
            var statuses = (from e in _context.SiptEvent
                            where e.EventId == eventID
                            select new { e.EventStusId, e.WrkflwStusId }).SingleOrDefault();

            if (statuses.EventStusId != eEventStatus || statuses.WrkflwStusId != wfStatus)
                return true;
            else
                return false;
        }

        public SiptEventDoc GetSiptDocument(int id)
        {
            return _context.SiptEventDoc
                .SingleOrDefault(i => i.SiptEventDocId == id && i.RecStusId == (byte)ERecStatus.Active);
        }

        private void AssignCommonFields(SiptModel data, ref SiptEvent dc, ref CustScrdData CSDTbl, byte csg, int eventId)
        {
            //SiptEvent dc = new SiptEvent();
            SiptEncryptContactModel dc2 = new SiptEncryptContactModel();
            dc.EventId = eventId;
            dc.M5OrdrNbr = data.M5OrdrNbr;
            dc.H1 = data.H1;
            dc.SiteId = data.SiteId;
            dc.H6 = data.H6;
            dc.CharsId = data.CharsId;
            dc.TeamPdlNme = data.TeamPdlNme;
            dc.SiptDocLocTxt = data.SiptDocLocTxt;
            dc.CustReqStDt = data.CustReqStDt;
            dc.CustReqEndDt = data.CustReqEndDt;
            dc.WrkDes = data.WrkDes;
            dc.ReqorUserId = data.ReqorUserId == 0 ? null : data.ReqorUserId;
            dc.NtwkEngrId = data.NtwkEngrId == 0 ? null : data.NtwkEngrId;
            dc.NtwkTechEngrId = data.NtwkTechEngrId == 0 ? null : data.NtwkTechEngrId;
            dc.PmId = data.PmId == 0 ? null : data.PmId;
            dc.EventStusId = data.EventStusId == 0 ? (byte)EventStatus.Visible : data.EventStusId;
            //dc.WrkflwStusId = data.WrkflwStusId;
            //dc.CreatByUserId = data.CreatByUserId;
            //dc.ModfdByUserId = data.ModfdByUserId;
            //dc.ModfdDt = data.ModfdDt;
            //dc.CreatDt = data.CreatDt;
            dc.RecStusId = data.RecStusId;
            dc.SiptProdTypeId = data.SiptProdTypeId;
            dc.TnsTgCd = data.TnsTgCd;
            dc.PrjId = data.PrjId;
            dc.FocDt = data.FocDt;
            dc.UsIntlCd = data.UsIntlCd;
            dc.SiptDesgnDoc = data.SiptDesgnDoc;
            dc.EventTitleTxt = data.EventTitleTxt;
            dc.SiteCntctHrNme = data.SiteCntctHrNme;

            dc2.StreetAdr = data.StreetAdr;
            dc2.EventTitleTxt = data.EventTitleTxt;
            dc2.CustNme = data.CustNme;
            dc2.SiteCntctNme = data.SiteCntctNme;
            dc2.SiteCntctPhnNbr = data.SiteCntctPhnNbr;
            dc2.SiteCntctHrNme = data.SiteCntctHrNme;
            dc2.SiteCntctEmailAdr = data.SiteCntctEmailAdr;
            dc2.SiteAdr = data.SiteAdr;
            dc2.FlrBldgNme = data.FlrBldgNme;
            dc2.CtyNme = data.CtyNme;
            dc2.SttPrvnNme = data.SttPrvnNme;
            dc2.CtryRgnNme = data.CtryRgnNme;
            dc2.ZipCd = data.ZipCd;
            if (csg > 0)
            {
                SecuredData obj = new SecuredData(dc2);
                List<GetEncryptValues> EncValues = _common.GetEventEncryptValuesSipt(obj).ToList();
                //List<Byte[]> EncValues = GetEventEncryptValues(dc2);
                //List<GetEncryptValues> EncValues = _common.GetEventEncryptValuesSipt(dc2);
                if (EncValues != null)
                {
                    CSDTbl.ScrdObjId = eventId;
                    CSDTbl.ScrdObjTypeId = (byte)SecuredObjectType.SIPT_EVENT;
                    CSDTbl.CustNme = EncValues[0].Item;
                    CSDTbl.CustCntctNme = EncValues[1].Item;
                    CSDTbl.CustCntctPhnNbr = EncValues[2].Item;
                    CSDTbl.CustEmailAdr = EncValues[3].Item;
                    CSDTbl.StreetAdr1 = EncValues[4].Item;
                    CSDTbl.BldgNme = EncValues[5].Item;
                    CSDTbl.FlrId = EncValues[5].Item;
                    CSDTbl.CtyNme = EncValues[6].Item;
                    CSDTbl.SttPrvnNme = EncValues[7].Item;
                    CSDTbl.CtryRgnNme = EncValues[8].Item;
                    CSDTbl.ZipPstlCd = EncValues[9].Item;
                    CSDTbl.EventTitleTxt = EncValues[10].Item;

                    dc.StreetAdr = null;
                    dc.EventTitleTxt = null;
                    dc.CustNme = null;
                    dc.SiteCntctNme = null;
                    dc.SiteCntctPhnNbr = null;
                    //dc.SiteCntctHrNme = null;
                    dc.SiteCntctEmailAdr = null;
                    //dc.SiteAdr = null;
                    dc.FlrBldgNme = null;
                    dc.CtyNme = null;
                    dc.SttPrvnNme = null;
                    dc.CtryRgnNme = null;
                    dc.ZipCd = null;
                }
            }
            else
            {
                dc.StreetAdr = data.StreetAdr;
                dc.EventTitleTxt = data.EventTitleTxt;
                dc.CustNme = data.CustNme;
                dc.SiteCntctNme = data.SiteCntctNme;
                dc.SiteCntctPhnNbr = data.SiteCntctPhnNbr;
                dc.SiteCntctHrNme = data.SiteCntctHrNme;
                dc.SiteCntctEmailAdr = data.SiteCntctEmailAdr;
                //dc.SiteAdr = data.SiteAdr;
                dc.FlrBldgNme = data.FlrBldgNme;
                dc.CtyNme = data.CtyNme;
                dc.SttPrvnNme = data.SttPrvnNme;
                dc.CtryRgnNme = data.CtryRgnNme;
                dc.ZipCd = data.ZipCd;
            }

            if (data.EventStusId == (byte)EventStatus.Visible
                && (data.WrkflwStusId == (byte)WorkflowStatus.Visible
                    || data.WrkflwStusId == (byte)WorkflowStatus.Submit))
                dc.WrkflwStusId = data.WrkflwStusId == 0 ? (byte)WorkflowStatus.Submit : (byte)data.WrkflwStusId;
            else
                dc.WrkflwStusId = data.WrkflwStusId == 0 ? (byte)WorkflowStatus.Submit : (byte)data.WrkflwStusId;
            //dc.WrkflwStusId = data.WrkflwStusId;
            //dc.CreatByUser = data.CreatByUser;
            //dc.Event = data.Event;
            //dc.EventStus = data.EventStus;
            //dc.ModfdByUser = data.ModfdByUser;
            //dc.NtwkEngr = data.NtwkEngr;
            //dc.NtwkTechEngr = data.NtwkTechEngr;
            //dc.Pm = data.Pm;
            //dc.RecStus = data.RecStus;
            //dc.SiptProdType = data.SiptProdType;
            //dc.WrkflwStus = data.WrkflwStus;

            //return dc;
        }
    }
}