﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Data.Repository
{
    public class NRMBPMInterfaceRepository : INRMBPMInterfaceRepository
    {
        private readonly COWSAdminDBContext _context;

        public NRMBPMInterfaceRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IEnumerable<GetNRMBPMInterfaceView> GetNRMBPMInterfaceView()
        {
            var nrmbpmInterfaceView = _context.Query<GetNRMBPMInterfaceView>()
                .AsNoTracking()
                .FromSql("dbo.getNRMBPMInterfaceView").ToList<GetNRMBPMInterfaceView>();

            return nrmbpmInterfaceView;
        }
    }
}