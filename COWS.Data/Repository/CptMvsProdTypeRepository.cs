﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptMvsProdTypeRepository : ICptMvsProdTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptMvsProdTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptMvsProdType Create(LkCptMvsProdType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptMvsProdType> Find(Expression<Func<LkCptMvsProdType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptMvsProdType, out List<LkCptMvsProdType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptMvsProdType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptMvsProdType, out List<LkCptMvsProdType> list))
            {
                list = _context.LkCptMvsProdType
                            .OrderBy(i => i.CptMvsProdType)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptMvsProdType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptMvsProdType GetById(int id)
        {
            return _context.LkCptMvsProdType
                            .SingleOrDefault(i => i.CptMvsProdTypeId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptMvsProdType);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptMvsProdType entity)
        {
            throw new NotImplementedException();
        }
    }
}