﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MDSServiceTypeRepository : IMDSServiceTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public MDSServiceTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IEnumerable<LkMdsSrvcType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsSrvcType, out List<LkMdsSrvcType> list))
            {
                list = _context.LkMdsSrvcType
                            .OrderBy(i => i.SrvcTypeDes)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkMdsSrvcType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkMdsSrvcType GetById(int id)
        {
            return _context.LkMdsSrvcType
                .SingleOrDefault(i => i.SrvcTypeId == id);
        }

        public IQueryable<LkMdsSrvcType> Find(Expression<Func<LkMdsSrvcType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkMdsSrvcType, out List<LkMdsSrvcType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public LkMdsSrvcType Create(LkMdsSrvcType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkMdsSrvcType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkMdsSrvcType);
            return _context.SaveChanges();
        }
    }
}