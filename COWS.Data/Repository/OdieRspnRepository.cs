﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class OdieRspnRepository : IOdieRspnRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;

        public OdieRspnRepository(COWSAdminDBContext context, ICommonRepository common)
        {
            _context = context;
            _common = common;
        }

        public OdieRspn Create(OdieRspn entity)
        {
            _context.OdieRspn.Add(entity);

            SaveAll();

            return GetById(entity.ReqId);
        }

        public void Delete(int id)
        {
            OdieRspn entity = GetById(id);
            _context.OdieRspn.Remove(entity);

            SaveAll();
        }

        public IQueryable<OdieRspn> Find(Expression<Func<OdieRspn, bool>> predicate)
        {
            return _context.OdieRspn
                            .Include(a => a.Req)
                            .AsNoTracking()
                            .Where(predicate);
        }

        public IEnumerable<OdieRspn> GetAll()
        {
            return _context.OdieRspn
                            .AsNoTracking()
                            .ToList();
        }

        public OdieRspn GetById(int id)
        {
            return _context.OdieRspn
                .SingleOrDefault(i => i.RspnId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, OdieRspn entity)
        {
            OdieRspn obj = GetById(id);

            SaveAll();
        }

        public IEnumerable<OdieRspn> Response(int requestId, byte csgLvlId)
        {
            bool hasResult = false;
            IEnumerable<OdieRspn> res = null;

            for (int i = 0; i < 10 && !hasResult; i++)
            {
                hasResult = _context.OdieReq
                    .Any(a => a.ReqId == requestId &&
                        a.OdieMsgId == (int)OdieMessageType.CustomerNameRequest &&
                        a.StusId == 21 // All Received
                    );

                if (!hasResult)
                {
                    System.Threading.Thread.Sleep(4000);
                }
                else
                {
                    res = Find(a => a.ReqId == requestId).ToList();
                }
            }

            if (res != null && res.Count() > 0)
            {
                // If Secured Data
                if (csgLvlId > 0)
                {
                    IEnumerable<CustScrdData> customers = _context.CustScrdData
                        .Where(a => a.ScrdObjTypeId == (byte)SecuredObjectType.ODIE_RSPN && res.Any(b => b.RspnId == a.ScrdObjId))
                        .ToList();

                    res = res
                        .GroupJoin(customers,
                            rspn => rspn.RspnId,
                            cust => cust.ScrdObjId,
                            (rspn, cust) => new { rspn, cust = cust.ToList() })
                        .SelectMany(a => a.cust
                            .Select(b => new OdieRspn
                            {
                                RspnId = a.rspn.RspnId,
                                ReqId = a.rspn.ReqId,
                                CustTeamPdl = a.rspn.CustTeamPdl,
                                MnspmId = a.rspn.MnspmId,
                                SowsFoldrPathNme = a.rspn.SowsFoldrPathNme,
                                OdieCustId = a.rspn.OdieCustId,
                                CustNme = a.rspn.Req.CsgLvlId >= csgLvlId ?
                                    _common.GetDecryptValue(b.CustNme) : string.Empty
                            })
                        );
                }
            }

            return res;
        }

        public IEnumerable<GetOdieDiscoResponse> GetOdieDiscoResponse(int requestId)
        {
            bool hasResult = false;
            IEnumerable<GetOdieDiscoResponse> res = null;

            // Try calling ODIE 10 times
            for (int i = 0; i < 10 && !hasResult; i++)
            {
                List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@REQ_ID", requestId)
                };

                res = _context.Query<GetOdieDiscoResponse>().AsNoTracking()
                    .FromSql(
                        "dbo.getODIEDiscoResponse @REQ_ID", pc.ToArray()
                    ).ToList();

                hasResult = res != null && res.Count() > 0;

                if (!hasResult)
                {
                    System.Threading.Thread.Sleep(4000);
                }
            }

            return res;
        }

        public DataTable GetCustNameDetails(int RspnID, int csgLvlId)
        {
            DataTable dt = new DataTable();
            dt = null;

            var odrcnd = (from respcn in _context.OdieRspn
                          join oreq in _context.OdieReq on respcn.ReqId equals oreq.ReqId
                          join csd in _context.CustScrdData on new { respcn.RspnId, ScrdObjTypeId = (byte)ScrdObjType.ODIE_RSPN } equals new { RspnId = csd.ScrdObjId, csd.ScrdObjTypeId }
                             into joincsd
                          from csd in joincsd.DefaultIfEmpty()
                          join lu in _context.LkUser on respcn.MnspmId equals lu.UserAdid into lujoin
                          from lu in lujoin.DefaultIfEmpty()
                          join lun in _context.LkUser on respcn.NteId equals lun.UserAdid into lunjoin
                          from lun in lunjoin.DefaultIfEmpty()
                          join lsde in _context.LkUser on respcn.SdeAssigned equals lsde.UserAdid into lsdejoin
                          from lsde in lsdejoin.DefaultIfEmpty()
                          where respcn.RspnId == RspnID
                          select new
                          {
                              RSPN_ID = respcn.RspnId,
                              CUST_NME = oreq.CsgLvlId == 0 
                                            ? respcn.CustNme
                                            : (oreq.CsgLvlId > 0 && csgLvlId != 0 && csgLvlId <= oreq.CsgLvlId) 
                                                ? _common.GetDecryptValue(csd.CustNme)
                                                : string.Empty,
                              CUST_TEAM_PDL = respcn.CustTeamPdl,
                              MNSPM_ID = respcn.MnspmId,
                              NTE_ID = respcn.NteId,
                              NTEAssigned = lun.DsplNme,
                              MNSPM_USER = lu.DsplNme,
                              SOWS_FOLDR_PATH_NME = respcn.SowsFoldrPathNme,
                              ODIE_CUST_ID = respcn.OdieCustId,
                              SDEAssigned = lsde.DsplNme,
                              SDE_ID = respcn.SdeAssigned
                              //NEAssigned = lne.DSPL_NME
                              //VoiceAssigned = lv.DSPL_NME
                          }).Distinct();

            dt = LinqHelper.CopyToDataTable(odrcnd, null, null);
            return dt;
        }
    }
}