﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class ForeignCxrRepository : IForeignCxrRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public ForeignCxrRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkFrgnCxr Create(LkFrgnCxr entity)
        {
            _context.LkFrgnCxr.Add(entity);

            SaveAll();

            return Find(i => i.CxrCd == entity.CxrCd).SingleOrDefault();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkFrgnCxr> Find(Expression<Func<LkFrgnCxr, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkFrgnCxr, out List<LkFrgnCxr> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkFrgnCxr> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkFrgnCxr, out List<LkFrgnCxr> list))
            {
                list = _context.LkFrgnCxr
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderBy(i => i.CxrCd)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkFrgnCxr, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkFrgnCxr GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkFrgnCxr);
            return _context.SaveChanges();
        }

        public void Update(int id, LkFrgnCxr entity)
        {
            LkFrgnCxr cxr = Find(i => i.CxrCd == entity.CxrCd).SingleOrDefault();
            //cxr.CxrCd = entity.CxrCd;
            cxr.CxrNme = entity.CxrNme;
            cxr.RecStusId = entity.RecStusId;
            cxr.ModfdByUserId = entity.ModfdByUserId;
            cxr.ModfdDt = entity.ModfdDt;

            SaveAll();
        }
    }
}