﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace COWS.Data.Repository
{
    public class CcdRepository : ICcdRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _config;

        public CcdRepository(COWSAdminDBContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        public IEnumerable<CcdHist> GetCcdHistory(int orderId)
        {
            return _context.CcdHist
                    .Include(i => i.CreatByUser)
                    .Include(i => i.Nte)
                    .Include(i => i.CcdHistReas)
                        .ThenInclude(i => i.CcdMissdReas)
                    .Where(i => i.OrdrId == orderId)
                    .OrderBy(i => i.CreatDt)
                    .ToList();
        }

        public IEnumerable<LkCcdMissdReas> GetCcdReason()
        {
            return _context.LkCcdMissdReas
                    .OrderBy(i => i.CcdMissdReasDes)
                    .ToList();
        }

        public DataTable SearchCcd(string m5, int h5, byte bypass)
        {
            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.getCCDOrderDetails", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@FTN", m5);
            command.Parameters.AddWithValue("@H5", h5);
            command.Parameters.AddWithValue("@bypass", bypass);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public bool UpdateCcd(string orderIds, DateTime newCcd, string reasons, string notes, int userId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter() {ParameterName = "@OrdrIDs", SqlDbType = SqlDbType.VarChar, Value = orderIds},
                new SqlParameter() {ParameterName = "@newCCD", SqlDbType = SqlDbType.DateTime, Value = newCcd},
                new SqlParameter() {ParameterName = "@reasonCSV", SqlDbType = SqlDbType.VarChar, Value = reasons},
                new SqlParameter() {ParameterName = "@notes", SqlDbType = SqlDbType.VarChar, Value = notes},
                new SqlParameter() {ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = userId},
                new SqlParameter() {ParameterName = "@Out", SqlDbType = SqlDbType.Int, Direction = System.Data.ParameterDirection.Output}
            };

            _context.Database.ExecuteSqlCommand("dbo.insertCCDChange @OrdrIDs, @newCCD, @reasonCSV, @notes, @UserID, @Out OUT", pc.ToArray());
            int retId = Convert.ToInt32(pc[5].Value);
            return Convert.ToBoolean(retId);
        }
    }
}
