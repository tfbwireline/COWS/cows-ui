﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptRepository : ICptRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _config;
        private readonly ICommonRepository _commonRepo;

        public CptRepository(COWSAdminDBContext context, IConfiguration config,
            ICommonRepository commonRepo)
        {
            _context = context;
            _config = config;
            _commonRepo = commonRepo;
        }

        public Cpt Create(Cpt entity)
        {
            _context.Cpt.Add(entity);

            SaveAll();

            if (entity.CsgLvlId > 0)
            {
                CustScrdData sdata = new CustScrdData();
                sdata.ScrdObjId = entity.CptId;
                sdata.ScrdObjTypeId = (byte)ScrdObjType.CPT;
                sdata.CustNme = _commonRepo.GetEncryptValue(entity.CompnyNme ?? "");
                sdata.StreetAdr1 = _commonRepo.GetEncryptValue(entity.CustAdr ?? "");
                sdata.FlrId = _commonRepo.GetEncryptValue(entity.CustFlrBldgNme ?? "");
                sdata.CtyNme = _commonRepo.GetEncryptValue(entity.CustCtyNme ?? "");
                sdata.SttPrvnNme = _commonRepo.GetEncryptValue(entity.CustSttPrvnNme ?? "");
                sdata.CtryRgnNme = _commonRepo.GetEncryptValue(entity.CustCtryRgnNme ?? "");
                sdata.ZipPstlCd = _commonRepo.GetEncryptValue(entity.CustZipCd ?? "");
                _context.CustScrdData.Add(sdata);

                entity.CompnyNme = string.Empty;
                entity.CustAdr = string.Empty;
                entity.CustFlrBldgNme = string.Empty;
                entity.CustCtyNme = string.Empty;
                entity.CustSttPrvnNme = string.Empty;
                entity.CustCtryRgnNme = string.Empty;
                entity.CustZipCd = string.Empty;
                _context.Cpt.Update(entity);

                SaveAll();
            }

            return GetById(entity.CptId);
        }

        public void Delete(int id)
        {
            Cpt customer = GetById(id);
            _context.Cpt.Remove(customer);

            SaveAll();
        }

        public IQueryable<Cpt> Find(Expression<Func<Cpt, bool>> predicate)
        {
            return _context.Cpt
                            .Include(i => i.CptMngdAuth)
                            .Include(i => i.CptMngdSrvc)
                            .Include(i => i.CptMvsProd)
                            .Include(i => i.CptPrimSite)
                            .Include(i => i.CptPrimWan)
                            .Include(i => i.CptPrvsn).ThenInclude(cpt => cpt.CptPrvsnType)
                            .Include(i => i.CptPrvsn).ThenInclude(cpt => cpt.RecStus)
                            .Include(i => i.CptScndyTprt)
                            .Include(i => i.CptRltdInfo)
                            .Include(i => i.CptRedsgn)
                            .Include(i => i.CptSrvcType)
                            .Include(i => i.CptSuprtTier)
                            .Include(i => i.CptUserAsmt).ThenInclude(cpt => cpt.CptUser)
                            .Include(i => i.CptDoc)
                            .Include(i => i.CptHist).ThenInclude(cpt => cpt.Actn)
                            .Include(i => i.CptHist).ThenInclude(cpt => cpt.CreatByUser)
                            .Include(i => i.CptStus)
                            .Include(i => i.SubmtrUser)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .Where(predicate);


        }

        public IEnumerable<Cpt> GetAll()
        {
            return _context.Cpt
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .OrderBy(i => i.CptCustNbr)
                            .ToList();
        }

        public Cpt GetById(int id)
        {
            return _context.Cpt
                            .Include(i => i.CptMngdAuth)
                            .Include(i => i.CptMngdSrvc)
                            .Include(i => i.CptMvsProd)
                            .Include(i => i.CptPrimSite)
                            .Include(i => i.CptPrimWan)
                            .Include(i => i.CptPrvsn).ThenInclude(cpt => cpt.CptPrvsnType)
                            .Include(i => i.CptPrvsn).ThenInclude(cpt => cpt.RecStus)
                            .Include(i => i.CptScndyTprt)
                            .Include(i => i.CptRltdInfo)
                            .Include(i => i.CptRedsgn)
                            .Include(i => i.CptSrvcType)
                            .Include(i => i.CptSuprtTier)
                            .Include(i => i.CptUserAsmt).ThenInclude(cpt => cpt.CptUser)
                            .Include(i => i.CptDoc)
                            .Include(i => i.CptHist).ThenInclude(cpt => cpt.Actn)
                            .Include(i => i.CptHist).ThenInclude(cpt => cpt.CreatByUser)
                            .Include(i => i.CptStus)
                            .Include(i => i.SubmtrUser)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.CptId == id);
        }

        public Cpt GetById(int id, string adid)
        {
            var data = _context.Cpt
                            .Include(i => i.CptMngdAuth)
                            .Include(i => i.CptMngdSrvc)
                            .Include(i => i.CptMvsProd)
                            .Include(i => i.CptPrimSite)
                            .Include(i => i.CptPrimWan)
                            .Include(i => i.CptPrvsn).ThenInclude(cpt => cpt.CptPrvsnType)
                            .Include(i => i.CptPrvsn).ThenInclude(cpt => cpt.RecStus)
                            .Include(i => i.CptScndyTprt)
                            .Include(i => i.CptRltdInfo)
                            .Include(i => i.CptRedsgn)
                            .Include(i => i.CptSrvcType)
                            .Include(i => i.CptSuprtTier)
                            .Include(i => i.CptUserAsmt).ThenInclude(cpt => cpt.CptUser)
                            .Include(i => i.CptDoc)
                            .Include(i => i.CptHist).ThenInclude(cpt => cpt.Actn)
                            .Include(i => i.CptHist).ThenInclude(cpt => cpt.CreatByUser)
                            .Include(i => i.CptStus)
                            .Include(i => i.SubmtrUser)
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.CptId == id);

            if (data != null)
            {
                if(data.CsgLvlId > 0)
                {
                    var UserCsg = _commonRepo.GetCSGLevelAdId(adid);
                    // LogWebActivity
                    _commonRepo.LogWebActivity((byte)WebActyType.CPTSearch, data.CptId.ToString(), adid, (byte)UserCsg, data.CsgLvlId);
                }
            }

            return data;
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, Cpt entity)
        {
            Cpt cpt = GetById(id);

            if (cpt.SprntWhlslReslrCd != entity.SprntWhlslReslrCd)
            {
                if (entity.SprntWhlslReslrCd)
                {
                    entity.CompnyNme = string.Format("{0} - {1}", entity.SprntWhlslReslrNme, entity.CompnyNme);
                }
                else
                {
                    entity.CompnyNme = entity.CompnyNme.Replace(cpt.SprntWhlslReslrNme + " - ", string.Empty);
                }
            }

            if (entity.CsgLvlId > 0)
            {
                CustScrdData sdata = _context.CustScrdData
                    .SingleOrDefault(i => i.ScrdObjId == id && i.ScrdObjTypeId == (byte)ScrdObjType.CPT);

                sdata.CustNme = _commonRepo.GetEncryptValue(entity.CompnyNme ?? "");
                sdata.StreetAdr1 = _commonRepo.GetEncryptValue(entity.CustAdr ?? "");
                sdata.FlrId = _commonRepo.GetEncryptValue(entity.CustFlrBldgNme ?? "");
                sdata.CtyNme = _commonRepo.GetEncryptValue(entity.CustCtyNme ?? "");
                sdata.SttPrvnNme = _commonRepo.GetEncryptValue(entity.CustSttPrvnNme ?? "");
                sdata.CtryRgnNme = _commonRepo.GetEncryptValue(entity.CustCtryRgnNme ?? "");
                sdata.ZipPstlCd = _commonRepo.GetEncryptValue(entity.CustZipCd ?? "");

                if (sdata.ScrdObjId == id)
                {
                    _context.CustScrdData.Update(sdata);
                }
                else
                {
                    sdata.ScrdObjId = entity.CptId;
                    sdata.ScrdObjTypeId = (byte)ScrdObjType.CPT;
                    sdata.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(sdata);
                }

                entity.CompnyNme = string.Empty;
                entity.CustAdr = string.Empty;
                entity.CustFlrBldgNme = string.Empty;
                entity.CustCtyNme = string.Empty;
                entity.CustSttPrvnNme = string.Empty;
                entity.CustCtryRgnNme = string.Empty;
                entity.CustZipCd = string.Empty;
            }
            else
            {
                cpt.CompnyNme = entity.CompnyNme;
                cpt.CustAdr = entity.CustAdr;
                cpt.CustFlrBldgNme = entity.CustFlrBldgNme;
                cpt.CustCtyNme = entity.CustCtyNme;
                cpt.CustSttPrvnNme = entity.CustSttPrvnNme;
                cpt.CustCtryRgnNme = entity.CustCtryRgnNme;
                cpt.CustZipCd = entity.CustZipCd;
            }

            cpt.CsgLvlId = entity.CsgLvlId;
            cpt.CptStusId = entity.CptStusId;
            cpt.SeEmailAdr = entity.SeEmailAdr;
            cpt.AmEmailAdr = entity.AmEmailAdr;
            cpt.IpmEmailAdr = entity.IpmEmailAdr;
            cpt.H1 = entity.H1;
            cpt.DevCnt = entity.DevCnt;
            cpt.SprntWhlslReslrCd = entity.SprntWhlslReslrCd;
            cpt.SprntWhlslReslrNme = entity.SprntWhlslReslrNme;
            cpt.GovtCustCd = entity.GovtCustCd;
            cpt.ScrtyClrnceCd = entity.ScrtyClrnceCd;
            var isTacacsChange = (cpt.TacacsCd != entity.TacacsCd);
            cpt.TacacsCd = entity.TacacsCd;
            cpt.EncMdmCd = entity.EncMdmCd;
            cpt.HstMgndSrvcCd = entity.HstMgndSrvcCd;
            
            // CptMngdSrvc
            if (entity.CptMngdSrvc != null && entity.CptMngdSrvc.Count() > 0)
            {
                List<CptMngdSrvc> oldList = _context.CptMngdSrvc.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<short> ids = entity.CptMngdSrvc.Where(i => i.CptId == id)
                    .Select(i => i.CptHstMngdSrvcId).ToList();
                List<CptMngdSrvc> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptHstMngdSrvcId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptMngdSrvc.RemoveRange(toRemove);

                // Add new record
                List<short> oldIds = _context.CptMngdSrvc.Where(i => i.CptId == id)
                    .Select(i => i.CptHstMngdSrvcId).ToList();
                List<CptMngdSrvc> toAdd = entity.CptMngdSrvc
                    .Where(i => !oldIds.Contains(i.CptHstMngdSrvcId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptMngdSrvc.AddRange(toAdd);
            }
            else
            {
                _context.CptMngdSrvc.RemoveRange(_context.CptMngdSrvc.Where(i => i.CptId == id));
            }

            cpt.SdwanCd = entity.SdwanCd;
            cpt.CustShrtNme = entity.CustShrtNme;
            cpt.SubnetIpAdr = entity.SubnetIpAdr;
            cpt.StartIpAdr = entity.StartIpAdr;
            cpt.EndIpAdr = entity.EndIpAdr;
            cpt.QipSubnetNme = entity.QipSubnetNme;
            cpt.PreShrdKey = entity.PreShrdKey;
            cpt.RevwdPmCd = entity.RevwdPmCd;
            cpt.RetrnSdeCd = entity.RetrnSdeCd;
            cpt.RecStusId = entity.RecStusId;
            cpt.ModfdByUserId = entity.ModfdByUserId;
            cpt.ModfdDt = entity.ModfdDt;

            // PrimSites
            if (entity.CptPrimSite != null && entity.CptPrimSite.Count() > 0)
            {
                List<Entities.Models.CptPrimSite> oldList = _context.CptPrimSite.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<short> ids = entity.CptPrimSite.Where(i => i.CptId == id)
                    .Select(i => i.CptPrimSiteId).ToList();
                List<Entities.Models.CptPrimSite> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptPrimSiteId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptPrimSite.RemoveRange(toRemove);

                // Add new record
                List<short> oldIds = _context.CptPrimSite.Where(i => i.CptId == id)
                    .Select(i => i.CptPrimSiteId).ToList();
                List<Entities.Models.CptPrimSite> toAdd = entity.CptPrimSite
                    .Where(i => !oldIds.Contains(i.CptPrimSiteId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptPrimSite.AddRange(toAdd);
            }
            else
            {
                _context.CptPrimSite.RemoveRange(_context.CptPrimSite.Where(i => i.CptId == id));
            }

            // Customer Types; Related Info
            if (entity.CptRltdInfo != null && entity.CptRltdInfo.Count() > 0)
            {
                List<CptRltdInfo> oldList = _context.CptRltdInfo.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<int> ids = entity.CptRltdInfo.Where(i => i.CptId == id)
                    .Select(i => i.CptRltdInfoId).ToList();
                List<CptRltdInfo> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptRltdInfoId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptRltdInfo.RemoveRange(toRemove);

                // Add new record
                List<int> oldIds = _context.CptRltdInfo.Where(i => i.CptId == id)
                    .Select(i => i.CptRltdInfoId).ToList();
                List<CptRltdInfo> toAdd = entity.CptRltdInfo
                    .Where(i => !oldIds.Contains(i.CptRltdInfoId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptRltdInfo.AddRange(toAdd);
            }
            else
            {
                _context.CptRltdInfo.RemoveRange(_context.CptRltdInfo.Where(i => i.CptId == id));
            }

            // Primary Transport Wan
            if (entity.CptPrimWan != null && entity.CptPrimWan.Count() > 0)
            {
                List<CptPrimWan> oldList = _context.CptPrimWan.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<int> ids = entity.CptPrimWan.Where(i => i.CptId == id)
                    .Select(i => i.CptPrimWanId).ToList();
                List<CptPrimWan> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptPrimWanId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptPrimWan.RemoveRange(toRemove);

                // Add new record
                List<int> oldIds = _context.CptPrimWan.Where(i => i.CptId == id)
                    .Select(i => i.CptPrimWanId).ToList();
                List<CptPrimWan> toAdd = entity.CptPrimWan
                    .Where(i => !oldIds.Contains(i.CptPrimWanId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptPrimWan.AddRange(toAdd);
            }
            else
            {
                _context.CptPrimWan.RemoveRange(_context.CptPrimWan.Where(i => i.CptId == id));
            }

            // Secondary Transport Wan
            if (entity.CptScndyTprt != null && entity.CptScndyTprt.Count() > 0)
            {
                List<CptScndyTprt> oldList = _context.CptScndyTprt.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<int> ids = entity.CptScndyTprt.Where(i => i.CptId == id)
                    .Select(i => i.CptScndyTprtId).ToList();
                List<CptScndyTprt> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptScndyTprtId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptScndyTprt.RemoveRange(toRemove);

                // Add new record
                List<int> oldIds = _context.CptScndyTprt.Where(i => i.CptId == id)
                    .Select(i => i.CptScndyTprtId).ToList();
                List<CptScndyTprt> toAdd = entity.CptScndyTprt
                    .Where(i => !oldIds.Contains(i.CptScndyTprtId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptScndyTprt.AddRange(toAdd);
            }
            else
            {
                _context.CptScndyTprt.RemoveRange(_context.CptScndyTprt.Where(i => i.CptId == id));
            }

            // Support Tiers
            if (entity.CptSuprtTier != null && entity.CptSuprtTier.Count() > 0)
            {
                List<CptSuprtTier> oldList = _context.CptSuprtTier.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<int> ids = entity.CptSuprtTier.Where(i => i.CptId == id)
                    .Select(i => i.CptSuprtTierId).ToList();
                List<CptSuprtTier> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptSuprtTierId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptSuprtTier.RemoveRange(toRemove);

                // Add new record
                List<int> oldIds = _context.CptSuprtTier.Where(i => i.CptId == id)
                    .Select(i => i.CptSuprtTierId).ToList();
                List<CptSuprtTier> toAdd = entity.CptSuprtTier
                    .Where(i => !oldIds.Contains(i.CptSuprtTierId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptSuprtTier.AddRange(toAdd);
            }
            else
            {
                _context.CptSuprtTier.RemoveRange(_context.CptSuprtTier.Where(i => i.CptId == id));
            }

            // Service Types
            if (entity.CptSrvcType != null && entity.CptSrvcType.Count() > 0)
            {
                List<CptSrvcType> oldList = _context.CptSrvcType.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<int> ids = entity.CptSrvcType.Where(i => i.CptId == id)
                    .Select(i => i.CptSrvcTypeId).ToList();
                List<CptSrvcType> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptSrvcTypeId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptSrvcType.RemoveRange(toRemove);

                // Add new record
                List<int> oldIds = _context.CptSrvcType.Where(i => i.CptId == id)
                    .Select(i => i.CptSrvcTypeId).ToList();
                List<CptSrvcType> toAdd = entity.CptSrvcType
                    .Where(i => !oldIds.Contains(i.CptSrvcTypeId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptSrvcType.AddRange(toAdd);
            }
            else
            {
                _context.CptSrvcType.RemoveRange(_context.CptSrvcType.Where(i => i.CptId == id));
            }

            // Manage Authorization
            if (entity.CptMngdAuth != null && entity.CptMngdAuth.Count() > 0)
            {
                List<CptMngdAuth> oldList = _context.CptMngdAuth.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<int> ids = entity.CptMngdAuth.Where(i => i.CptId == id)
                    .Select(i => i.CptMngdAuthId).ToList();
                List<CptMngdAuth> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptMngdAuthId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptMngdAuth.RemoveRange(toRemove);

                // Add new record
                List<int> oldIds = _context.CptMngdAuth.Where(i => i.CptId == id)
                    .Select(i => i.CptMngdAuthId).ToList();
                List<CptMngdAuth> toAdd = entity.CptMngdAuth
                    .Where(i => !oldIds.Contains(i.CptMngdAuthId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptMngdAuth.AddRange(toAdd);
            }
            else
            {
                _context.CptMngdAuth.RemoveRange(_context.CptMngdAuth.Where(i => i.CptId == id));
            }

            // MVS Prod
            if (entity.CptMvsProd != null && entity.CptMvsProd.Count() > 0)
            {
                List<CptMvsProd> oldList = _context.CptMvsProd.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<int> ids = entity.CptMvsProd.Where(i => i.CptId == id)
                    .Select(i => i.CptMvsProdId).ToList();
                List<CptMvsProd> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptMvsProdId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptMvsProd.RemoveRange(toRemove);

                // Add new record
                List<int> oldIds = _context.CptMvsProd.Where(i => i.CptId == id)
                    .Select(i => i.CptMvsProdId).ToList();
                List<CptMvsProd> toAdd = entity.CptMvsProd
                    .Where(i => !oldIds.Contains(i.CptMvsProdId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptMvsProd.AddRange(toAdd);
            }
            else
            {
                _context.CptMvsProd.RemoveRange(_context.CptMvsProd.Where(i => i.CptId == id));
            }

            // CPT Docs
            if (entity.CptDoc != null && entity.CptDoc.Count() > 0)
            {
                List<CptDoc> oldList = _context.CptDoc.Where(i => i.CptId == id).ToList();

                // Delete old record not in current list
                List<int> ids = entity.CptDoc.Where(i => i.CptId == id)
                    .Select(i => i.CptDocId).ToList();
                List<CptDoc> toRemove = oldList
                    .Where(i => !ids.Contains(i.CptDocId)).ToList();
                if (toRemove != null && toRemove.Count() > 0)
                    _context.CptDoc.RemoveRange(toRemove);

                // Add new record
                List<int> oldIds = _context.CptDoc.Where(i => i.CptId == id)
                    .Select(i => i.CptDocId).ToList();
                List<CptDoc> toAdd = entity.CptDoc
                    .Where(i => !oldIds.Contains(i.CptDocId)).ToList();
                if (toAdd != null && toAdd.Count() > 0)
                    _context.CptDoc.AddRange(toAdd);
            }
            else
            {
                _context.CptDoc.RemoveRange(_context.CptDoc.Where(i => i.CptId == id));
            }

            // Passed null value for CptPrvsn for Cancel CPT request
            // Update Provisioning Status of those that are not yet picked up
            // No need to update Provision value unless CPT is cancel
            //if (entity.CptPrvsn == null || entity.CptPrvsn.Count <= 0)
            //{
            //    var prvsn = _context.CptPrvsn.Where(i => i.CptId == id);
            //    List<CptPrvsn> newPrvsn = new List<CptPrvsn>();
            //    foreach (CptPrvsn prov in prvsn)
            //    {
            //        if (prov.RecStusId == (short)CptProvisionStatus.PendingPickup)
            //        {
            //            prov.CptPrvsnId = 0;
            //            prov.RecStusId = (short)CptProvisionStatus.Error;
            //            prov.ModfdByUserId = entity.ModfdByUserId;
            //            prov.ModfdDt = entity.ModfdDt;
            //            prov.Cpt = null;
            //            prov.CptPrvsnType = null;
            //            prov.CreatByUser = null;
            //            prov.ModfdByUser = null;
            //            prov.RecStus = null;
            //            newPrvsn.Add(prov);
            //        }
            //    }

            //    if (newPrvsn != null && newPrvsn.Count > 0)
            //    {
            //        _context.CptPrvsn.RemoveRange(prvsn);
            //        _context.CptPrvsn.AddRange(newPrvsn);
            //    }
            //}

            // [20220304] - Updated by Sarah Sandoval
            // Update Provisioning Section only if TACACS is updated
            if (cpt.CptPrvsn != null && cpt.CptPrvsn.Count > 0 && isTacacsChange)
            {
                var tacacs = cpt.CptPrvsn.FirstOrDefault(i => i.CptPrvsnTypeId == (short)CptProvision.Tacacs);
                // Added Pending pickup status so that when the container is already picked up by NOS it will not get updated
                if (tacacs != null && tacacs.RecStusId == (short)CptProvisionStatus.PendingPickup)
                {
                    tacacs.RecStusId = (short)CptProvisionStatus.PendingPickup;
                    tacacs.ModfdDt = entity.ModfdDt;
                    tacacs.ModfdByUserId = entity.ModfdByUserId;
                    tacacs.PrvsnStusCd = entity.TacacsCd;
                    _context.CptPrvsn.Update(tacacs);
                }
            }

            // User Assignment
            if (entity.CptUserAsmt != null && entity.CptUserAsmt.Count() > 0)
            {
                List<CptUserAsmt> oldList = _context.CptUserAsmt.Where(i => i.CptId == id).ToList();

                foreach (CptUserAsmt user in entity.CptUserAsmt)
                {
                    if (user.UsrPrfId.HasValue)
                    {
                        // Get active user with same profile
                        // Should return single user only since 1 active user for specific profile assignment
                        var oldAssignedUser = oldList.SingleOrDefault(i => i.UsrPrfId == user.UsrPrfId && i.RecStusId == (byte)ERecStatus.Active);

                        // Update old assigned user status
                        if (oldAssignedUser != null)
                        {
                            oldAssignedUser.RecStusId = (byte)ERecStatus.InActive;
                            oldAssignedUser.ModfdDt = DateTime.Now;
                            oldAssignedUser.ModfdByUserId = cpt.ModfdByUserId;
                            _context.CptUserAsmt.Update(oldAssignedUser);
                        }

                        // Check if new assigned user has db record
                        var userExist = oldList.SingleOrDefault(i => i.UsrPrfId == user.UsrPrfId && i.CptUserId == user.CptUserId);
                        if (userExist != null)
                        {
                            // Update old assigned user status
                            userExist.RecStusId = (byte)ERecStatus.Active;
                            userExist.ModfdDt = DateTime.Now;
                            userExist.ModfdByUserId = cpt.ModfdByUserId;
                            _context.CptUserAsmt.Update(userExist);
                        }
                        else
                        {
                            // Add new user to this profile assignment
                            _context.CptUserAsmt.Add(user);
                        }
                    }
                }
            }
            else
            {
                // Inactivate all users
                var users = _context.CptUserAsmt.Where(i => i.CptId == id);
                foreach (CptUserAsmt user in users)
                {
                    user.RecStusId = (byte)ERecStatus.InActive;
                    user.ModfdDt = DateTime.Now;
                    user.ModfdByUserId = cpt.ModfdByUserId;
                    _context.CptUserAsmt.Update(user);
                }
            }

            // Email Requests
            if (entity.EmailReq != null && entity.EmailReq.Count() > 0)
            {
                _context.EmailReq.AddRange(entity.EmailReq);
            }

            // History
            if (entity.CptHist != null && entity.CptHist.Count() > 0)
            {
                // Add new record
                List<int> oldIds = _context.CptHist.Where(i => i.CptId == id)
                    .Select(i => i.CptHistId).ToList();
                List<CptHist> toAdd = entity.CptHist
                    .Where(i => !oldIds.Contains(i.CptHistId)).ToList();
                if (toAdd != null)
                    _context.CptHist.AddRange(toAdd);
            }

            //_context.ChangeTracker.Entries.
            if (_context.ChangeTracker.Entries().Any(i => i.Metadata.Name.Equals("COWS.Entities.Models.EmailReq")))
            {
                var emails = _context.ChangeTracker.Entries()
                    .Where(i => i.Metadata.Name.Equals("COWS.Entities.Models.EmailReq"));

                foreach (var item in emails.ToList())
                {
                    var email = (EmailReq)item.Entity;
                    if (_context.EmailReq.Any(i => i.EmailReqId == email.EmailReqId))
                    {
                        item.State = EntityState.Unchanged;
                    }
                }
            }

            SaveAll();
        }

        public IEnumerable<GetAllCptView> GetAllCpt(byte csgLevelId, byte searchView, string adid = null)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@CSGLvlId", csgLevelId),
                new SqlParameter("@IsSearchView", searchView)
            };

            var cptView = _context.Query<GetAllCptView>()
                .AsNoTracking()
                .FromSql("[web].[getCPTView_V2] @CSGLvlId, @IsSearchView", pc.ToArray()).ToList<GetAllCptView>();

            if (cptView.Count() == 1)
            {
                var id = cptView.FirstOrDefault().CptId;
                var data = GetById(id);

                if (data.CsgLvlId > 0)
                {
                    var UserCsg = _commonRepo.GetCSGLevelAdId(adid);
                    // LogWebActivity
                    _commonRepo.LogWebActivity((byte)WebActyType.CPTSearch, id.ToString(), adid, (byte)UserCsg, data.CsgLvlId);
                }
            }

            return cptView;
        }

        public DataTable GetCustomerH1Data(string h1)
        {
            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("dbo.getCPTDataByH1_v2", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@H1", h1);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            return dt;
        }

        public bool ValidateShortNameByLinkedServer(string h1, string shortName)
        {
            bool isValid = false;

            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("dbo.getODIECustomerH1Data", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string drH1 = (row["H1"]).ToString();
                    string drCustName = (row["Customer"]).ToString();

                    if (drH1.Trim().Equals(h1.Trim()) && drCustName.Trim().Equals(shortName.Trim()))
                    {
                        isValid = true;
                        break;
                    }
                }
            }

            return isValid;
        }

        public List<CptUser> GetAssignmentByProfileId(short usrPrfId)
        {
            List<CptUser> cptUsers = new List<CptUser>();

            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("dbo.GetCPTAssignmentByUserProfile", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@USR_PRF_ID", usrPrfId);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    cptUsers.Add(new CptUser
                    {
                        UserId = (int)row["UserId"],
                        UserAdId = (row["UserAdId"]).ToString(),
                        FullName = (row["FullName"]).ToString(),
                        Email = (row["Email"]).ToString(),
                        UsrPrfId = (short)row["UsrPrfId"],
                        UsrPrfName = (row["UsrPrfName"]).ToString(),
                        PoolCd = row["PoolCd"] == DBNull.Value ? false : Convert.ToBoolean(row["PoolCd"]),
                        Tasks = (int)row["Tasks"],
                        Devices = (int)row["Devices"],
                        CptQty = Convert.ToDecimal(((int)row["Tasks"] + (int)row["Devices"]) * 1.5)
                    });
                }
            }

            return cptUsers;
        }

        public List<CptAssignment> GetCptAssignmentStats()
        {
            List<CptAssignment> cptAssgnmnts = new List<CptAssignment>();

            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("dbo.GetCPTReportingDetails_V2", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    cptAssgnmnts.Add(new CptAssignment
                    {
                        CptId = (int)row["CPTID"],
                        CptCustNumber = (row["CPTNumber"]).ToString(),
                        CptStusId = (int)row["CPTStusID"],
                        CustShortName = (row["CustomerName"]).ToString(),
                        CompanyName = (row["CompanyName"]).ToString(),
                        CreatedDate = (DateTime)(row["CreatedDate"]),
                        CustTypeId = (int)row["CustomerTypeID"],
                        CustTypeName = (row["CustomerType"]).ToString(),
                        SrvcTierId = (int)row["ServiceTierID"],
                        SrvcTierName = (row["ServiceTier"]).ToString(),
                        IsGatekeeperNeeded = Convert.ToBoolean(row["IsGatekeeperNeeded"]),
                        GatekeeperId = (int)row["GatekeeperID"],
                        GatekeeperName = (row["GatekeeperName"]).ToString(),
                        IsManagerNeeded = Convert.ToBoolean(row["IsManagerNeeded"]),
                        ManagerId = (int)row["ManagerID"],
                        ManagerName = (row["ManagerName"]).ToString(),
                        IsPmNeeded = Convert.ToBoolean(row["IsPMNeeded"]),
                        PmId = (int)row["PMID"],
                        PmName = (row["PMName"]).ToString(),
                        ReturnedToSde = Convert.ToBoolean(row["ReturnedToSde"]),
                    });
                }
            }

            return cptAssgnmnts;
        }

        public List<CptAssignment> GetCptCancelStats(byte csgLevelIdLoggedInUser)
        {
            var er = (from cpt in _context.Cpt
                      join csd in _context.CustScrdData on new { cpt.CptId, ScrdObjTypeId = (byte)SecuredObjectType.CPT } equals new { CptId = csd.ScrdObjId, csd.ScrdObjTypeId }
                            into joincsd
                      from csd in joincsd.DefaultIfEmpty()
                      where cpt.CptStusId == (int)CptStatus.Cancelled
                      select new CptAssignment()
                      {
                          CptId = cpt.CptId,
                          CptCustNumber = cpt.CptCustNbr,
                          CompanyName = (cpt.CsgLvlId == 0) ? cpt.CompnyNme
                            : ((cpt.CsgLvlId > 0 && csgLevelIdLoggedInUser != 0
                                && csgLevelIdLoggedInUser <= cpt.CsgLvlId)
                                    ? _commonRepo.GetDecryptValue(csd.CustNme) : string.Empty),
                          CustShortName = cpt.CustShrtNme,
                          CreatedDate = cpt.CptHist.FirstOrDefault(i => i.ActnId == (int)Actions.CPTCancelled).CreatDt,
                      });

            return er.Where(i => i.CreatedDate.AddDays(30).Date > DateTime.Now.Date).ToList();
        }

        public void CreateCptHistory(CptHist hist)
        {
            _context.CptHist.Add(hist);
            SaveAll();
        }

        public bool IsRedesignExist(int cptId, string shrtNme)
        {
            return _context.CptRedsgn.SingleOrDefault(i => i.CptId == cptId && i.CustShrtNme == shrtNme) != null;
        }

        public string CreateRedesign(string shrtNme, string h1, string redsgnCat, int cptId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@cust_short_name", SqlDbType.VarChar),
                new SqlParameter("@H1_ID", SqlDbType.VarChar),
                new SqlParameter("@RedesignCategory", SqlDbType.VarChar),
                new SqlParameter("@CPT_ID", SqlDbType.Int)
            };
            pc[0].Value = shrtNme;
            pc[1].Value = h1;
            pc[2].Value = redsgnCat;
            pc[3].Value = cptId;

            int isSuccess = _context.Database.ExecuteSqlCommand
                ("dbo.CreateRedesignFromODIEView @cust_short_name, @H1_ID, @RedesignCategory, @CPT_ID",
                pc.ToArray());

            if (isSuccess > 0)
            {
                var redsgn = _context.CptRedsgn
                    .Include(i => i.Redsgn)
                    .SingleOrDefault(i => i.CptId == cptId && i.CustShrtNme == shrtNme);

                if (redsgn != null)
                    return redsgn.Redsgn.RedsgnNbr;
            }

            return string.Empty;
        }

        public Cpt GetSecuredData(Cpt entity)
        {
            if (entity.CsgLvlId > 0)
            {
                CustScrdData sdata = _context.CustScrdData
                    .SingleOrDefault(i => i.ScrdObjId == entity.CptId && i.ScrdObjTypeId == (byte)ScrdObjType.CPT);

                entity.CompnyNme = sdata.CustNme != null ? _commonRepo.GetDecryptValue(sdata.CustNme) : "";
                entity.CustAdr = sdata.StreetAdr1 != null ? _commonRepo.GetDecryptValue(sdata.StreetAdr1) : "";
                entity.CustFlrBldgNme = sdata.CustNme != null ? _commonRepo.GetDecryptValue(sdata.FlrId) : "";
                entity.CustCtyNme = sdata.CtyNme != null ? _commonRepo.GetDecryptValue(sdata.CtyNme) : "";
                entity.CustSttPrvnNme = sdata.SttPrvnNme != null ? _commonRepo.GetDecryptValue(sdata.SttPrvnNme) : "";
                entity.CustCtryRgnNme = sdata.CtryRgnNme != null ? _commonRepo.GetDecryptValue(sdata.CtryRgnNme) : "";
                entity.CustZipCd = sdata.ZipPstlCd != null ? _commonRepo.GetDecryptValue(sdata.ZipPstlCd) : "";
            }

            return entity;
        }
    }
}