﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class NccoOrderRepository : INccoOrderRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _common;
        private IConfiguration _configuration;
        private readonly IUserRepository _userRepo;
        private readonly ILogger<NccoOrderRepository> _logger;

        public NccoOrderRepository(COWSAdminDBContext context, 
            ICommonRepository common, 
            IConfiguration configuration,
            IUserRepository userRepo,
            ILogger<NccoOrderRepository> logger)
        {
            _context = context;
            _common = common;
            _configuration = configuration;
            _userRepo = userRepo;
            _logger = logger;
        }

        public IQueryable<NccoOrdr> Find(Expression<Func<NccoOrdr, bool>> predicate)
        {
            return _context.NccoOrdr
                //.Include(a => a.Actn)
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<NccoOrdr> GetAll()
        {
            return _context.NccoOrdr
                .ToList();
        }

        public NccoOrdr GetById(int id)
        {
            return _context.NccoOrdr
                .SingleOrDefault(a => a.OrdrId == id);
        }

        public NccoOrdr Create(NccoOrdr entity)
        {
            _context.NccoOrdr.Add(entity);
            SaveAll();

            return GetById(entity.OrdrId);
        }

        private int CreateHist(NccoOrdr entity)
        {
            _context.NccoOrdr.Add(entity);
            SaveAll();

            return (int)(from e in _context.OrdrNte where e.OrdrId == entity.OrdrId select (int?)e.NteId).Max();
        }

        public void Update(int id, NccoOrdr entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public DataTable GetNCCOWGDetails(int iOrderID)
        {
            /*
             GRP_ID	GRP_NME	        PRF_ID
                1	GOM	            114
                2	MDS	            128
                3	CSC	            58
                4	Sales Support	156
                5	AMNCI	        2
                6	ANCI	        16
                7	ENCI	        100
                11	SIPTnUCaaS	    170
                13	DCPE	        86
                15	CPE TECH	    44
                16	DBB	            72
                98	System	        184
             */
            DataTable rspn = new DataTable();

            DataTable dt = new DataTable();

            var a = (from ordr in _context.Ordr
                     join acttask in _context.ActTask on ordr.OrdrId equals acttask.OrdrId
                     join mapwt in _context.MapGrpTask on acttask.TaskId equals mapwt.TaskId
                     where ordr.OrdrId == iOrderID
                     && acttask.StusId == 0
                     select new { ordr.H5FoldrId, acttask.TaskId, mapwt.GrpId });

            if (a != null)
            {
                if (a.Count() > 0)
                {
                    //rspn.IsValid = true;
                    dt = LinqHelper.CopyToDataTable(a);
                    rspn = dt;
                }
                else
                {
                    //rspn.IsValid = true;
                    rspn = dt;
                }
            }

            return rspn;
        }

        public IEnumerable<NccoOrderView> Select(string sortExpression, string searchCriteria, int csgLvlId, int userId)
        {
            List<NccoOrderView> lstNCCO = new List<NccoOrderView>();

            NccoOrderView ncco = new NccoOrderView();

            var n = (from c in _context.NccoOrdr
                     join odr in _context.Ordr on c.OrdrId equals odr.OrdrId
                     join los in _context.LkOrdrStus on odr.OrdrStusId equals los.OrdrStusId
                     join lot in _context.LkOrdrType on c.OrdrTypeId equals lot.OrdrTypeId
                     join lop in _context.LkProdType on c.ProdTypeId equals lop.ProdTypeId
                     join lctry in _context.LkCtry on c.CtryCd equals lctry.CtryCd into lctryjoin
                     from lctry in lctryjoin.DefaultIfEmpty()
                     join csd in _context.CustScrdData on new { ScrdObjId = c.OrdrId, ScrdObjTypeId = (byte)SecuredObjectType.NCCO_ORDR } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                     into joinccsd
                     from csd2 in joinccsd.DefaultIfEmpty()
                     where ((odr.CsgLvlId == 0)
                             || ((odr.CsgLvlId > 0) && (csgLvlId != 0)
                                 && (csgLvlId <= odr.CsgLvlId)))
                     select new
                     {
                         orderID = c.OrdrId,
                         ordertype = lot.OrdrTypeDes,
                         prodtype = lop.ProdTypeDes,
                         ccd = odr.CustCmmtDt == null ? c.CcsDt : odr.CustCmmtDt,
                         customername = (odr.CsgLvlId == 0) ? c.CustNme : (((odr.CsgLvlId > 0) && (csgLvlId != 0)
                                 && (csgLvlId <= odr.CsgLvlId)) ? _common.GetDecryptValue(csd2.CustNme) : string.Empty),
                         H5 = c.H5AcctNbr,
                         ctry = (odr.CsgLvlId == 0) ? lctry.CtryNme : (((odr.CsgLvlId > 0) && (csgLvlId != 0)
                                 && (csgLvlId <= odr.CsgLvlId)) ? (csd2.CtryCd == null ? string.Empty : _common.GetCountryName(_common.GetDecryptValue(csd2.CtryCd))) : string.Empty),
                         //ctryCd = (odr.CsgLvlId == 0) ? lctry.CtryNme : (((odr.CsgLvlId > 0) && (csgLvlId != 0)
                         //        && (csgLvlId <= odr.CsgLvlId)) ? (csd2.CtryCd == null ? string.Empty : _common.GetDecryptValue(csd2.CtryCd)) : string.Empty),
                         orderStatus = los.OrdrStusDes,
                         createdBy = c.CreatByUserId,
                         CsgLvlId = odr.CsgLvlId,
                     });

            //if (searchCriteria != null && searchCriteria != string.Empty)
            //n = n.Where(SearchCriteria);
            //if (userId != null) // This will always return true; commenting out for SonarQube
            n = n.Where(c => c.createdBy == (userId == 0 ? c.createdBy : userId));

            if (sortExpression != null && sortExpression != string.Empty)
                //n = n.OrderBy(SortExpression);
                n = n.OrderByDescending(c => c.orderID);
            else
                n = n.OrderByDescending(c => c.orderID);

            foreach (var _n in n)
            {
                ncco = new NccoOrderView();
                ncco.OrdrId = _n.orderID;
                if (_n.ordertype != null)
                    ncco.OrdrType = _n.ordertype;
                if (_n.prodtype != null)
                    ncco.ProdType = _n.prodtype;
                if (_n.ccd != null)
                    //ncco.CcsDt = DateTime.ParseExact(_n.ccd.ToString(), "MMM/dd/yy", CultureInfo.InvariantCulture);
                    ncco.CcsDt = DateTime.Parse(_n.ccd.ToString());
                if (_n.customername != null)
                    ncco.CustNme = _n.customername;
                if (_n.H5 != null)
                    ncco.H5AcctNbr = Convert.ToInt32(_n.H5);
                if (_n.ctry != null)
                    ncco.Ctry = _n.ctry;
                if (_n.orderStatus != null)
                    ncco.OrdrStatus = _n.orderStatus;
                ncco.CsgLvlId = _n.CsgLvlId;
                lstNCCO.Add(ncco);
            }

            return lstNCCO;
        }

        public NccoOrderView GetNCCOOrderData(int orderID, int csgLvlId)
        {
            // NccoOrderView rspn = new NccoOrderView();

            NccoOrderView ncco = new NccoOrderView();

            var c = from no in _context.NccoOrdr
                    join luser in _context.LkUser on no.CreatByUserId equals luser.UserId
                    join ord in _context.Ordr on no.OrdrId equals ord.OrdrId
                    join csd in _context.CustScrdData on new { ScrdObjId = ord.OrdrId, ScrdObjTypeId = (byte)SecuredObjectType.NCCO_ORDR } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                        into joinccsd
                    from csd2 in joinccsd.DefaultIfEmpty()
                    where no.OrdrId == orderID
                    select new { no, luser, csd2, ord.CsgLvlId };

            if (c != null && c.Count() == 1)
            {
                foreach (var cc in c)
                {
                    if (cc.no.BillCycNbr != null)
                        ncco.BillCycNbr = cc.no.BillCycNbr;
                    if (cc.no.CcsDt != null)
                        ncco.CcsDt = DateTime.Parse(cc.no.CcsDt.ToString());
                    if (cc.no.CmntTxt != null)
                        ncco.CmntTxt = cc.no.CmntTxt;
                    ncco.CustNme = (cc.CsgLvlId == 0) ? cc.no.CustNme : (((cc.CsgLvlId > 0) && (csgLvlId != 0)
                                    && (csgLvlId <= cc.CsgLvlId)) ? _common.GetDecryptValue(cc.csd2.CustNme) : string.Empty);
                    ncco.CtryCd = (cc.CsgLvlId == 0) ? cc.no.CtryCd : (((cc.CsgLvlId > 0) && (csgLvlId != 0)
                                    && (csgLvlId <= cc.CsgLvlId)) ? _common.GetDecryptValue(cc.csd2.CtryCd) : string.Empty);
                    ncco.SiteCityNme = (cc.CsgLvlId == 0) ? cc.no.SiteCityNme : (((cc.CsgLvlId > 0) && (csgLvlId != 0)
                                    && (csgLvlId <= cc.CsgLvlId)) ? _common.GetDecryptValue(cc.csd2.CtyNme) : string.Empty);
                    ncco.EmailAdr = (cc.CsgLvlId == 0) ? cc.no.EmailAdr : (((cc.CsgLvlId > 0) && (csgLvlId != 0)
                                    && (csgLvlId <= cc.CsgLvlId)) ? _common.GetDecryptValue(cc.csd2.CustEmailAdr) : string.Empty);
                    if (cc.no.CwdDt != null)
                        ncco.CwdDt = DateTime.Parse(cc.no.CwdDt.ToString());
                    if (cc.no.H5AcctNbr != null && cc.no.H5AcctNbr != 0)
                        ncco.H5AcctNbr = Convert.ToInt32(cc.no.H5AcctNbr);
                    if (cc.no.OeNme != null)
                        ncco.OeNme = cc.no.OeNme;
                    ncco.OrdrId = cc.no.OrdrId;
                    ncco.OrdrTypeId = cc.no.OrdrTypeId;
                    ncco.ProdTypeId = cc.no.ProdTypeId;
                    if (cc.no.SolNme != null)
                        ncco.SolNme = cc.no.SolNme;
                    if (cc.no.SotsNbr != null)
                        ncco.SotsNbr = cc.no.SotsNbr;
                    if (cc.no.VndrCd != null)
                        ncco.VndrCd = cc.no.VndrCd;
                    if (cc.no.PlNbr != null)
                        ncco.PlNbr = cc.no.PlNbr;
                    if (cc.no.OrdrByLassieCd != null)
                        ncco.OrdrByLassieCd = cc.no.OrdrByLassieCd;
                    ncco.CsgLvlId = cc.CsgLvlId;
                }
                //rspn.IsValid = true;
                //rspn.ReturnValue = ncco;
            }

            return ncco;
        }

        public int insertIntoOrderTable(int csgLvlId, int userId)
        {
            int ordrID = 0;

            Ordr ordr = new Ordr();
            ordr.CreatByUserId = userId;
            ordr.OrdrCatId = 4;
            ordr.CreatDt = DateTime.Now;
            ordr.OrdrStusId = 1;
            ordr.DmstcCd = true;
            ordr.CsgLvlId = (byte)csgLvlId;
            _context.Ordr.Add(ordr);
            SaveAll();

            ordrID = ordr.OrdrId;

            return ordrID;
        }

        public bool LoadSM(int orderid, int taskid, int taskstatus)
        {
            bool rspn = false;

            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            connection.Open();
            command = new SqlCommand("dbo.InsertInitialTask", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderID", orderid); // if you have parameters.
            command.Parameters.AddWithValue("@CategoryID", 4); // if you have parameters.
            command.Parameters.AddWithValue("@TaskID", taskid); // if you have parameters.
            command.Parameters.AddWithValue("@TaskStatus", taskstatus); // if you have parameters.
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
            rspn = true;

            return rspn;
        }

        public bool InsertOrderNotes(int OrderID, int NoteTypeID, int UserID, string Notes)
        {
            bool rspn = false;
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            connection.Open();
            command = new SqlCommand("dbo.insertOrderNotes", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ORDR_ID", OrderID); // if you have parameters.
            command.Parameters.AddWithValue("@NTE_TYPE_ID", NoteTypeID); // if you have parameters.
            command.Parameters.AddWithValue("@CREAT_BY_USER_ID", UserID); // if you have parameters.
            command.Parameters.AddWithValue("@NTE_TXT", Notes); // if you have parameters.
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
            rspn = true;

            return rspn;
        }

        public bool InsertJeopardy(int orderID, string jpdyCD, string notes, byte noteTypeID, int CreatedBy)
        {
            bool rspn = false;
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            connection.Open();
            command = new SqlCommand("dbo.insertOrderJeopardy", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ORDR_ID", orderID); // if you have parameters.
            command.Parameters.AddWithValue("@JPRDY_CD", jpdyCD); // if you have parameters.
            command.Parameters.AddWithValue("@Notes", notes); // if you have parameters.
            command.Parameters.AddWithValue("@NoteTypeID", noteTypeID); // if you have parameters.
            command.Parameters.AddWithValue("@Created_By", CreatedBy); // if you have parameters.
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
            rspn = true;

            return rspn;
        }

        public bool CompleteTask(int iOrderID, int iTaskID, int iTaskStatus, string sComments, int iUserID, int noteTypeID)
        {
            bool rspn = false;
            SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            connection.Open();
            command = new SqlCommand("dbo.CompleteActiveTask", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderID", iOrderID); // if you have parameters.
            command.Parameters.AddWithValue("@TaskID", iTaskID); // if you have parameters.
            command.Parameters.AddWithValue("@TaskStatus", iTaskStatus); // if you have parameters.
            command.Parameters.AddWithValue("@Comments", sComments); // if you have parameters.
            command.Parameters.AddWithValue("@UserID", iUserID); // if you have parameters.
            command.Parameters.AddWithValue("@NteType", noteTypeID); // if you have parameters.
            command.ExecuteNonQuery();
            command.Dispose();
            connection.Close();
            rspn = true;

            return rspn;
        }

        public bool UpdateH5FolderID(NccoOrderView _ncco, int userId)
        {
            bool rspn = false;
            int ordrId = 0;
            //var a = (from b in _context.H5Foldr
            //            join c in _context.LkCtry on b.CtryCd equals c.CtryCd into cc
            //            from c in cc.DefaultIfEmpty()
            //            where b.CustId == _ncco.H5AcctNbr
            //                && b.CtryCd == _ncco.CtryCd
            var a = (from b in _context.H5Foldr
                     join c in _context.LkCtry on b.CtryCd equals c.CtryCd
                     join csd in _context.CustScrdData on new { ScrdObjId = b.H5FoldrId, ScrdObjTypeId = (byte)SecuredObjectType.H5_FOLDR } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                     into cc
                     from csd2 in cc.DefaultIfEmpty()
                     where b.CustId == _ncco.H5AcctNbr
                         && (b.CtryCd == _ncco.CtryCd || _common.GetDecryptValue(csd2.CtryCd != null ? csd2.CtryCd : new byte[7000]) == _ncco.CtryCd)  ////new byte[7000]  just Dummy
                     select new
                     {
                         H5_FOLDR_ID = b.H5FoldrId,
                         RGN_ID = c.RgnId
                     }).ToList();

            if (a != null && a.Count() > 0)
            {
                //Ordr odr = (Ordr)(from c in _context.Ordr
                //                  where c.OrdrId == _ncco.OrdrId
                //                  select c).Single();
                var MyOdr = (from b in _context.Ordr
                             where b.OrdrId == _ncco.OrdrId
                             select b).SingleOrDefault();
                if (MyOdr != null && MyOdr.OrdrId > 0)
                {
                    Ordr odr = _context.Ordr
                            .Include(i => i.CreatByUser)
                            .Include(i => i.ModfdByUser)
                            .SingleOrDefault(i => i.OrdrId == _ncco.OrdrId);
                    odr.H5FoldrId = a[0].H5_FOLDR_ID;
                    odr.RgnId = a[0].RGN_ID;
                    odr.PrntOrdrId = _ncco.OrdrId;
                    odr.H5H6CustId = _ncco.H5AcctNbr;
                }
                else
                {
                    ordrId = insertIntoOrderTable(_ncco.CsgLvlId, userId);
                    if (ordrId > 0) rspn = true;
                }
                SaveAll();
                rspn = true;
            }
            else
            {
                if (InsertH5Folder(_ncco))
                {
                    UpdateH5FolderID(_ncco, userId);
                    rspn = true;
                }
                else
                {
                    rspn = false;
                }
            }

            return rspn;
        }

        public bool InsertH5Folder(NccoOrderView _ncco)
        {
            bool rspn = false;

            H5Foldr h5 = new H5Foldr();
            CustScrdData csd = new CustScrdData();
            h5.CreatByUserId = _ncco.CreatByUserId;
            h5.CreatDt = DateTime.UtcNow;
            h5.CustId = (int)_ncco.H5AcctNbr;
            if ((_ncco.CustNme != null) && (_ncco.CsgLvlId == 0))
                h5.CustNme = _ncco.CustNme;
            else if ((_ncco.CustNme != null) && (_ncco.CsgLvlId > 0))
                csd.CustNme = _common.GetEncryptValue(_ncco.CustNme);
            if ((_ncco.CtryCd != null) && (_ncco.CsgLvlId == 0))
                h5.CtryCd = _ncco.CtryCd;
            else if ((_ncco.CtryCd != null) && (_ncco.CsgLvlId > 0))
                csd.CtryCd = _common.GetEncryptValue(_ncco.CtryCd);
            if ((_ncco.SiteCityNme != null) && (_ncco.CsgLvlId == 0))
                h5.CustCtyNme = _ncco.SiteCityNme;
            else if ((_ncco.SiteCityNme != null) && (_ncco.CsgLvlId > 0))
                csd.CtyNme = _common.GetEncryptValue(_ncco.SiteCityNme);
            h5.RecStusId = 1;
            /*----------------Revisit if Scrd Data should be saved at H5--------------------------*/
            h5.CtryCd = _ncco.CtryCd;
            h5.CustCtyNme = _ncco.SiteCityNme;
            /*----------------Revisit if Scrd Data should be saved at H5--------------------------*/
            _context.H5Foldr.Add(h5);
            SaveAll();
            if (_ncco.CsgLvlId > 0)
            {
                int newHFId = (int)(from e in _context.H5Foldr select (int?)e.H5FoldrId).Max();
                csd.ScrdObjId = newHFId;
                csd.ScrdObjTypeId = (byte)SecuredObjectType.H5_FOLDR;

                if (csd.ScrdObjId > 0)
                {
                    csd.CreatDt = DateTime.Now;
                    _context.CustScrdData.Add(csd);
                    SaveAll();
                }
            }
            rspn = true;
            return rspn;
        }

        public bool insertPLSequence(int ordID, string plNo, List<String> seqNos, int USER_ID)
        {
            bool rspn = false;
            int VOterm = 0;
            int OrdCount = (from or in _context.Ckt
                            where or.OrdrId == ordID
                            select or).Count();
            if (OrdCount == 0)
            {
                //List<Ckt> ckt = new List<Ckt>();
                try
                {
                    VOterm = (from vo in _context.VndrOrdr where vo.OrdrId == ordID && vo.TrmtgCd == true select vo.VndrOrdrId).SingleOrDefault();
                }
                catch (Exception e)
                {
                    _logger.LogInformation($"{ e.Message } ");
                }

                int cntr = 0;
                foreach (var d in seqNos)
                {
                    cntr++;
                    Ckt c = new Ckt();
                    //c.PlSeqNbr = d + " "  +"00"+cntr.ToString();
                    c.PlSeqNbr = plNo + " " + d.ToString();
                    c.OrdrId = ordID;
                    c.CreatByUserId = USER_ID;
                    c.CreatDt = DateTime.Today;
                    c.TrmtgCd = true;
                    if (VOterm != 0)
                        c.VndrOrdrId = VOterm;
                    //ckt.Add(c);
                    _context.Ckt.Add(c);
                }

                SaveAll();
            }
            else
            {
                //int i = 0;
                foreach (var d in seqNos)
                {
                    //i++;
                    try
                    {
                        VOterm = (from vo in _context.VndrOrdr where vo.OrdrId == ordID && vo.TrmtgCd == true select vo.VndrOrdrId).SingleOrDefault();
                    }
                    catch (Exception e)
                    {
                        _logger.LogInformation($"{ e.Message } ");
                    }

                    var c1 = (from c1r in _context.Ckt
                              where (c1r.OrdrId == ordID) && (c1r.PlSeqNbr == string.Empty || c1r.PlSeqNbr == null) && c1r.TrmtgCd == true
                              select c1r).FirstOrDefault();

                    if (c1 != null)
                    {
                        //c1.PlSeqNbr = d + " " + "00" + i.ToString();
                        c1.PlSeqNbr = plNo + " " + d.ToString();
                        SaveAll();
                    }
                    else
                    {
                        Ckt c = new Ckt();
                        //c.PlSeqNbr = d + " " + "00" + i.ToString();
                        c.PlSeqNbr = plNo + " " + d.ToString();
                        c.OrdrId = ordID;
                        c.CreatByUserId = USER_ID;
                        c.CreatDt = DateTime.Today;
                        c.TrmtgCd = true;
                        if (VOterm != 0)
                            c.VndrOrdrId = VOterm;
                        //ckt.Add(c);
                        _context.Ckt.Add(c);
                    }
                }
            }
            //if (OrdCount == 0)
            //{
            //    //List<Ckt> ckt = new List<Ckt>();
            //    try { VOterm = (from vo in _context.VndrOrdr where vo.OrdrId == ordID && vo.TrmtgCd == true select vo.VndrOrdrId).SingleOrDefault(); }
            //    catch { }
            //    for (int i = 0; i < seqNos.Count; i++)
            //    {
            //        Ckt c = new Ckt();
            //        c.PlSeqNbr = plNo + " " + seqNos[i];
            //        c.OrdrId = ordID;
            //        c.CreatByUserId = USER_ID;
            //        c.CreatDt = DateTime.Today;
            //        c.TrmtgCd = true;
            //        if (VOterm != 0)
            //            c.VndrOrdrId = VOterm;
            //        //ckt.Add(c);
            //        _context.Ckt.Add(c);
            //    }

            //    SaveAll();
            //}
            //else
            //{
            //    for (int i = 0; i < seqNos.Count; i++)
            //    {
            //        try { VOterm = (from vo in _context.VndrOrdr where vo.OrdrId == ordID && vo.TrmtgCd == true select vo.VndrOrdrId).SingleOrDefault(); }
            //        catch { }

            //        var c1 = (from c1r in _context.Ckt
            //                  where (c1r.OrdrId == ordID) && (c1r.PlSeqNbr == string.Empty || c1r.PlSeqNbr == null) && c1r.TrmtgCd == true
            //                    select c1r).FirstOrDefault();

            //        if (c1 != null)
            //        {
            //            c1.PlSeqNbr = plNo + " " + seqNos[i];
            //            SaveAll();
            //        }
            //        else
            //        {
            //            Ckt c = new Ckt();
            //            c.PlSeqNbr = plNo + " " + seqNos[i];
            //            c.OrdrId = ordID;
            //            c.CreatByUserId = USER_ID;
            //            c.CreatDt = DateTime.Today;
            //            c.TrmtgCd = true;
            //            if (VOterm != 0)
            //                c.VndrOrdrId = VOterm;
            //            //ckt.Add(c);
            //            _context.Ckt.Add(c);
            //        }
            //    }
            //}

            rspn = true;
            return rspn;
        }

        public bool UpdateNCCOOrderToCancel(int ordrID, NccoOrderView _ncco, int USER_ID)
        {
            bool rspn = false;
            var ncco = (from no in _context.NccoOrdr
                        where no.OrdrId == ordrID
                        select no).SingleOrDefault();
            if (ncco != null)
            {
                ncco.OrdrTypeId = 8;
                ncco.CmntTxt = _ncco.CmntTxt;
                ncco.EmailAdr = _ncco.EmailAdr;
                SaveAll();
            }
            LkUser Crtuser = _userRepo.Find(a => a.UserId == USER_ID).SingleOrDefault();
            LkUser Mdfuser = _userRepo.Find(a => a.UserId == USER_ID).SingleOrDefault();
            if (USER_ID != null && _ncco.CreatByUserId != null)
            {
                OrdrNte onte = new OrdrNte();
                onte.OrdrId = ordrID;
                onte.CreatByUserId = 1;
                onte.CreatDt = DateTime.Now;
                onte.NteTypeId = 9;
                onte.NteTxt = "Order created by: " + Crtuser.DsplNme + " ,  Cancel created by: " + Mdfuser.DsplNme;
                onte.RecStusId = 1;

                _context.OrdrNte.Add(onte);
                SaveAll();
            }

            rspn = LoadSM(ordrID, 100, 0);

            rspn = true;
            return rspn;
        }

        public int CompleteGOMIBillTask(int orderID)
        {
            int cnt = 0;
            var at = (from a in _context.ActTask
                      where a.OrdrId == orderID && a.TaskId == 110 && a.TaskId == 0
                      select a);

            foreach (var a in at)
            {
                a.StusId = 2;
                a.ModfdDt = DateTime.UtcNow;
                cnt++;
            }

            SaveAll();
            return cnt;
        }

        public DataTable GetNCCOWGDetails2(int iOrderID)
        {
            DataTable rspn = new DataTable();

            DataTable dt = new DataTable();

            var a = (from ordr in _context.Ordr
                     join acttask in _context.ActTask on ordr.OrdrId equals acttask.OrdrId
                     join mapwt in _context.MapPrfTask on acttask.TaskId equals mapwt.TaskId
                     where ordr.OrdrId == iOrderID
                        && acttask.StusId == 0
                     select new { ordr.H5FoldrId, acttask.TaskId, mapwt.PrntPrfId });

            if (a != null)
            {
                if (a.Count() > 0)
                {
                    dt = LinqHelper.CopyToDataTable(a);
                    rspn = dt;
                }
                else
                {
                    rspn = dt;
                }
            }
            return rspn;
        }

        public bool VerifyOrderInPendingStatus(int _ordrID)
        {
            bool rspn = false;

            var ncco = (from o in _context.Ordr
                        where o.OrdrId == _ordrID
                            && o.OrdrStusId == 1
                        select o).SingleOrDefault();
            if (ncco != null)
                rspn = true;

            return rspn;
        }

        public bool VerifyOrder(int _ordrID)
        {
            bool rspn = false;

            var ncco = (from o in _context.Ordr
                        where o.OrdrId == _ordrID
                        select o).SingleOrDefault();
            if (ncco != null)
                rspn = true;

            return rspn;
        }

        public bool ResendEmailOnCancel(int _ordrID)
        {
            var ncco = (from o in _context.EmailReq
                        where o.OrdrId == _ordrID
                            && o.EmailReqTypeId == 13
                        select o).SingleOrDefault();
            if (ncco != null)
            {
                EmailReq er = (EmailReq)ncco;
                _context.EmailReq.Remove(er);
                SaveAll();
            }

            return true;
        }

        public byte GetOrderSecurityDetails(int orderID)
        {
            var sec = (from o in _context.Ordr
                       where o.OrdrId == orderID
                       select o).SingleOrDefault();
            if (sec != null)
                return sec.CsgLvlId;
            else return (byte)0;
        }

        public string GetH5CustomerName(NccoOrderView ncco, int CsgLvlID)
        {
            string custNme = string.Empty;

            var cnme = (from o in _context.H5Foldr
                        join csd in _context.CustScrdData on new { ScrdObjId = o.H5FoldrId, ScrdObjTypeId = (byte)SecuredObjectType.H5_FOLDR } equals new { csd.ScrdObjId, csd.ScrdObjTypeId }
                            into ocsd
                        from csd2 in ocsd.DefaultIfEmpty()
                        where o.CustId == ncco.H5AcctNbr
                         && o.CtryCd == ncco.CtryCd
                        select new { o, csd2 }).ToList();
            if (cnme != null && cnme.Count() > 0)
                custNme = (cnme[0].o.CsgLvlId == 0) ? cnme[0].o.CustNme : (((cnme[0].o.CsgLvlId > 0) && (CsgLvlID != 0)
                                    && (CsgLvlID <= cnme[0].o.CsgLvlId)) ? _common.GetDecryptValue(cnme[0].csd2.CustNme) : string.Empty);

            return custNme;
        }

        public bool UpdatePreviousComments(int orderID, string comments)
        {
            bool bRet = false;
            OrdrNte onte = new OrdrNte();
            onte.OrdrId = orderID;
            onte.CreatByUserId = 1;
            onte.CreatDt = DateTime.UtcNow;
            onte.NteTypeId = 9;
            onte.NteTxt = "Comments from initial submission prior to cancel request: " + comments;
            onte.RecStusId = 1;

            _context.OrdrNte.Add(onte);
            SaveAll();
            bRet = true;
            return bRet;
        }

        public bool InsertOrderNotes(OrdrNte onte)
        {
            _context.OrdrNte.Add(onte);
            SaveAll();
            return true;
        }

        public bool CompleteCCDTask(int orderID)
        {
            var tc = (from at in _context.ActTask
                      where at.OrdrId == orderID && at.TaskId == 106
                      select at);

            foreach (var t in tc)
            {
                t.StusId = 2;
                t.ModfdDt = DateTime.UtcNow;
            }

            SaveAll();
            return true;
        }

        public bool MoveFromGOMFOtoXNCI(int ordrID, int userId)
        {
            bool rspn = false;
            var a = (from b in _context.ActTask
                     where b.OrdrId == ordrID
                         && b.TaskId != 1000
                         && b.TaskId.ToString().StartsWith("2")
                         && b.StusId == 1
                     select b).SingleOrDefault();

            if (a != null)
            {
                a.StusId = 0;
                a.ModfdDt = DateTime.UtcNow;
            }

            var c = (from d in _context.ActTask
                     where d.OrdrId == ordrID
                            && d.TaskId == (int)Tasks.GOMNCCOErrorNotification
                            && d.StusId == 0
                     select d).SingleOrDefault();

            if (c != null)
            {
                c.StusId = 2;
                c.ModfdDt = DateTime.UtcNow;
            }

            OrdrNte onte = new OrdrNte();
            onte.OrdrId = ordrID;
            onte.NteTypeId = 9;
            onte.NteTxt = "Completing GOM Fallout task";
            onte.CreatByUserId = userId;
            onte.RecStusId = 1;
            onte.CreatDt = DateTime.UtcNow;
            _context.OrdrNte.Add(onte);

            SaveAll();
            rspn = true;
            return rspn;
        }

        public bool UpdateNCCOOrderTable(NccoOrderView _ncco)
        {
            bool rspn = false;

            var a = (from b in _context.NccoOrdr
                     where b.OrdrId == _ncco.OrdrId
                     select b).FirstOrDefault();
            if (a != null)
            {
                CustScrdData updtCSD = (CustScrdData)(from csd in _context.CustScrdData where csd.ScrdObjId == _ncco.OrdrId && csd.ScrdObjTypeId == ((byte)SecuredObjectType.NCCO_ORDR) select csd).SingleOrDefault();
                bool bInsertCSD = false;
                if (updtCSD == null)
                {
                    updtCSD = new CustScrdData();
                    bInsertCSD = true;
                }
                AssignCommonFields(_ncco, ref updtCSD, ref a);
                if ((bInsertCSD) && (_ncco.CsgLvlId > 0) && (updtCSD.ScrdObjId > 0))
                {
                    updtCSD.CreatDt = DateTime.UtcNow;
                    _context.CustScrdData.Add(updtCSD);
                }
                else if ((!bInsertCSD) && (_ncco.CsgLvlId == 0) && (updtCSD.ScrdObjId > 0))
                    _context.CustScrdData.Remove(updtCSD);
            }
            else
            {
                NccoOrdr nccoOrdr = new NccoOrdr();
                CustScrdData newCSD = new CustScrdData();
                AssignCommonFields(_ncco, ref newCSD, ref nccoOrdr);
                _context.NccoOrdr.Add(nccoOrdr);
                if (newCSD.ScrdObjId > 0)
                {
                    newCSD.CreatDt = DateTime.UtcNow;
                    _context.CustScrdData.Add(newCSD);
                }
            }
            SaveAll();
            rspn = true;

            return rspn;
        }

        private void AssignCommonFields(NccoOrderView _ncco, ref CustScrdData CSDTbl, ref NccoOrdr nccoOrdr)
        {
            if (nccoOrdr.OrdrId == 0)
                nccoOrdr.CreatDt = DateTime.UtcNow;
            nccoOrdr.OrdrId = _ncco.OrdrId;
            nccoOrdr.OrdrTypeId = Convert.ToByte(_ncco.OrdrTypeId);
            nccoOrdr.ProdTypeId = Convert.ToByte(_ncco.ProdTypeId);
            nccoOrdr.SiteCityNme = _ncco.SiteCityNme;
            nccoOrdr.SolNme = _ncco.SolNme;
            nccoOrdr.SotsNbr = _ncco.SotsNbr;
            nccoOrdr.OeNme = _ncco.OeNme;
            nccoOrdr.BillCycNbr = _ncco.BillCycNbr;
            nccoOrdr.H5AcctNbr = _ncco.H5AcctNbr;
            if (_ncco.CcsDt != null)
                nccoOrdr.CcsDt = Convert.ToDateTime(_ncco.CcsDt);
            if (_ncco.CwdDt != null)
                nccoOrdr.CwdDt = Convert.ToDateTime(_ncco.CwdDt);
            nccoOrdr.CtryCd = _ncco.CtryCd;
            nccoOrdr.CmntTxt = _ncco.CmntTxt;
            if (_ncco.CsgLvlId == 0)
            {
                nccoOrdr.CustNme = _ncco.CustNme;
                nccoOrdr.CtryCd = _ncco.CtryCd;
                nccoOrdr.SiteCityNme = _ncco.SiteCityNme;
                nccoOrdr.EmailAdr = _ncco.EmailAdr;
            }
            else
            {
                CSDTbl.ScrdObjId = _ncco.OrdrId;
                CSDTbl.ScrdObjTypeId = (byte)SecuredObjectType.NCCO_ORDR;
                CSDTbl.CustNme = _common.GetEncryptValue(_ncco.CustNme == null ? "" : _ncco.CustNme);
                CSDTbl.CtryCd = _common.GetEncryptValue(_ncco.CtryCd == null ? "" : _ncco.CtryCd);
                CSDTbl.CtyNme = _common.GetEncryptValue(_ncco.SiteCityNme == null ? "" : _ncco.SiteCityNme);
                CSDTbl.CustEmailAdr = _common.GetEncryptValue(_ncco.EmailAdr == null ? "" : _ncco.EmailAdr);
            }
            nccoOrdr.OrdrByLassieCd = _ncco.OrdrByLassieCd;
            nccoOrdr.PlNbr = _ncco.PlNbr;
            nccoOrdr.CreatByUserId = _ncco.CreatByUserId;
            if (_ncco.VndrCd != null)
                nccoOrdr.VndrCd = _ncco.VndrCd;
        }
    }
}