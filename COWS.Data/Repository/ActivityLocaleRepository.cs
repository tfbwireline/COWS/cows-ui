﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class ActivityLocaleRepository : IActivityLocaleRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public ActivityLocaleRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkActyLocale Create(LkActyLocale entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkActyLocale> Find(Expression<Func<LkActyLocale, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkActyLocale, out List<LkActyLocale> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkActyLocale> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkActyLocale, out List<LkActyLocale> list))
            {
                list = _context.LkActyLocale
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkActyLocale, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkActyLocale GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkActyLocale);
            return _context.SaveChanges();
        }

        public void Update(int id, LkActyLocale entity)
        {
            throw new NotImplementedException();
        }
    }
}