﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Linq;
using COWS.Data.Extensions;
using System.Collections.Generic;
using System;
using COWS.Entities.Enums;

namespace COWS.Data.Repository
{
    public class DCPERepository : IDCPERepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _commonRepo;

        public DCPERepository(COWSAdminDBContext context, ICommonRepository commonRepo)
        {
            _context = context;
            _commonRepo = commonRepo;
        }

        public DataTable GetFTNData(int orderID, int usrPrfId, string adid = null)
        {
            var mdsTasks = (from mgp in _context.MapPrfTask where mgp.PrntPrfId == usrPrfId select mgp.TaskId);

            var data = (from o in _context.Ordr
                        join fs in _context.FsaOrdr on o.OrdrId equals fs.OrdrId
                        join lp in _context.LkFsaProdType on fs.ProdTypeCd equals lp.FsaProdTypeCd
                        join lo in _context.LkFsaOrdrType on fs.OrdrTypeCd equals lo.FsaOrdrTypeCd
                        join los in _context.LkOrdrStus on o.OrdrStusId equals los.OrdrStusId
                        join cat in _context.ActTask on o.OrdrId equals cat.OrdrId into leftat
                        from at in leftat.Where(cat => cat.StusId == 0).Where(cat => mdsTasks.Contains(cat.TaskId)).DefaultIfEmpty()
                        join lt in _context.LkTask on at.TaskId equals lt.TaskId into llt
                        from lt in llt.DefaultIfEmpty()
                        join wfm in _context.UserWfmAsmt on o.OrdrId equals wfm.OrdrId into lwfm
                        from wfm in lwfm.Where(wfm => ((wfm.UsrPrfId == 86) || (wfm.UsrPrfId == 98))).DefaultIfEmpty()
                        join lu in _context.LkUser on wfm.AsnUserId equals lu.UserId into llu
                        from lu in llu.DefaultIfEmpty()
                        join lst in _context.LkOrdrSubType on fs.OrdrSubTypeCd equals lst.OrdrSubTypeCd into llst
                        from lst in llst.DefaultIfEmpty()
                        join line in _context.FsaOrdrCpeLineItem on o.OrdrId equals line.OrdrId
                        where o.OrdrId == orderID
                        select new
                        {
                            o.OrdrId,
                            fs.Ftn,
                            lp.FsaProdTypeDes,
                            lo.FsaOrdrTypeDes,
                            SubTypeDes = lst == null ? string.Empty : lst.OrdrSubTypeDes,
                            los.OrdrStusDes,
                            Task = lt == null ? string.Empty : lt.TaskNme,
                            TaskID = lt == null ? string.Empty : lt.TaskId.ToString(),
                            UserADID = lu == null ? string.Empty : lu.UserAdid,
                            line.DeviceId,
                            o.CsgLvlId
                        });
            var dt = LinqHelper.CopyToDataTable(data, null, null);

            //if(data.Count() == 1)
            //{
                var csg = dt.AsEnumerable().Where(row => row.Field<Int32>("OrdrId") == orderID).Select(row => row.Field<Byte>("CsgLvlId")).FirstOrDefault();
                if (csg > 0 && adid != null)
                {
                    // LogWebActivity
                    var UserCsg = _commonRepo.GetCSGLevelAdId(adid);
                    _commonRepo.LogWebActivity(((byte)WebActyType.OrderDetails), orderID.ToString(), adid, (byte)UserCsg, (byte)csg);
                }
            //}


            return dt;
        }

        //public bool isValidUser(int OrderID)
        //{
        //    try
        //    {
        //        using (COWS DCPEDBContext = new COWS(ConnStr))
        //        {
        //            DCPEDBContext.ObjectTrackingEnabled = false;

        //            var csgLvl = (from g in DCPEDBContext.ORDR
        //                          where g.ORDR_ID == OrderID
        //                          select g.CSG_LVL_ID).Single();
        //            if (csgLvl > 0)
        //            {
        //                var csgMatch = (from c in DCPEDBContext.USER_CSG_LVL
        //                                where c.USER_ID == LoggedUser.USER_ID && c.CSG_LVL_ID <= csgLvl
        //                                select c.USER_ID);

        //                if (csgMatch.Count() > 0)
        //                    return true;
        //                else
        //                    return false;
        //            }
        //            else
        //                return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new DBSelectException(ex.Message + " ; User : " + this.LoggedUser.DSPL_NME, ex);
        //    }
        //}

        //public bool hasActiveTask(int OrderID)
        //{
        //    try
        //    {
        //        using (COWS DCPEDBContext = new COWS(ConnStr))
        //        {
        //            DCPEDBContext.ObjectTrackingEnabled = false;

        //            var act_task = (from at in DCPEDBContext.ACT_TASK
        //                            join m in DCPEDBContext.MAP_GRP_TASK on at.TASK_ID equals m.TASK_ID
        //                            where at.ORDR_ID == OrderID
        //                                && m.GRP_ID == Convert.ToInt32(EGroups.DCPE)
        //                                && at.STUS_ID == 0
        //                            select at.TASK_ID);

        //            if (act_task.Count() > 0)
        //                return true;
        //            else
        //                return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new DBSelectException(ex.Message + " ; User : " + this.LoggedUser.DSPL_NME, ex);
        //    }
        //}
        public async Task<DataTable> GetCPEOrderInfo_V5U(int orderId, string userAdid, int csgLvlId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getCpeOrdrInfo_V5U {orderId}, {userAdid}, {csgLvlId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public async Task<DataTable> GetCPELineItems_V5U(int orderId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getCpeLineItems_V5U {orderId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }


        //public DataTable GetCPELineItemsAll_V5U(int _OrderID)
        //{
        //    try
        //    {
        //        setCommand("dbo.getCpeLineItemsAll_V5U");
        //        addIntParam("@OrderID", _OrderID);
        //        return getDataTable();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new DBSelectException(ex.Message + " ; User : " + this.LoggedUser.DSPL_NME, ex);
        //    }
        //}

        public DataTable GetEquipOnlyInfo(int orderID)
        {
            var data = (from fs in _context.FsaOrdr
                        join a in _context.ActTask on fs.OrdrId equals a.OrdrId
                        where fs.CpeEqptOnlyCd == "Y" && a.TaskId == 600 && fs.OrdrId == orderID
                        select new { fs.OrdrId, fs.CpeAccsPrvdrCd, fs.CpePhnNbr, fs.CpeTstTnNbr, fs.CpeEccktId }
                    );

            return LinqHelper.CopyToDataTable(data, null, null);
        }

        public int InsertDmstcStndMatReq_V5U(int orderId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@p0", SqlDbType.Int)
                };

            pc[0].Value = orderId;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertDmstcStndMatReq_V5U @p0", pc.ToArray());
            return toReturn;
        }

        public int InsertSSTATReq(int orderId, int msgId, string ftn)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@p0", SqlDbType.Int),
                    new SqlParameter("@p1", SqlDbType.Int),
                    new SqlParameter("@p2", SqlDbType.VarChar)
                };
            pc[0].Value = orderId;
            pc[1].Value = msgId;
            pc[2].Value = ftn;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertSSTATReq @p0, @p1,@p2", pc.ToArray());
            return toReturn;
        }

        public int InsertODIEReq_V5U(int orderId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@p0", SqlDbType.Int),
                    new SqlParameter("@p1", SqlDbType.Int)
                };

            pc[0].Value = orderId;
            pc[1].Value = 5;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertODIEReq_V5U @p0,@p1", pc.ToArray());
            return toReturn;
        }

        public int InsertEqpReceipt_V5U(int fsaCpeLineItemId, int partialQty, string action, string adid, int orderId)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@FSA_CPE_LINE_ITEM_ID", SqlDbType.Int),
                    new SqlParameter("@PARTIALQTY", SqlDbType.Int),
                    new SqlParameter("@ACTION", SqlDbType.VarChar),
                    new SqlParameter("@ADID", SqlDbType.VarChar),
                    new SqlParameter("@ORDR_ID", SqlDbType.Int)
                };
            pc[0].Value = fsaCpeLineItemId;
            pc[1].Value = partialQty;
            pc[2].Value = action;
            pc[3].Value = adid ?? "";
            pc[4].Value = orderId;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertEqpReceipt_V5U @FSA_CPE_LINE_ITEM_ID, @PARTIALQTY,@ACTION,@ADID,@ORDR_ID", pc.ToArray());
            return toReturn;
        }

        public int InsertEqpRecCmpl_V5U(string adid, int orderID)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ADID", SqlDbType.VarChar),
                    new SqlParameter("@ORDR_ID", SqlDbType.Int)
                };
            pc[0].Value = adid;
            pc[1].Value = orderID;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertEqpRecCmpl_V5U @ADID, @ORDR_ID", pc.ToArray());
            return toReturn;
        }

        //public void InsertNewTech_V5U(string _ADID)
        //{
        //    try
        //    {
        //        setCommand("dbo.insertNewTech_V5U");
        //        addStringParam("@ADID", _ADID);
        //        execNoQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new DBInsertException(ex.Message + " ; User : " + this.LoggedUser.DSPL_NME, ex);
        //    }
        //}

        public DataTable GetAssignedTechList()
        {
            var data = (from u in _context.LkUser
                        join mup in _context.MapUsrPrf on u.UserId equals mup.UserId
                        where mup.UsrPrfId == 53 
                        //&& mup.PoolCd == 1
                        select new { USER_CPE_TECH_NME = u.FullNme }
                    ).Distinct();
            return LinqHelper.CopyToDataTable(data, null, null);

        }

        public int InsertTechAssignment_V5U(int orderId, string tech, DateTime dispatchTime, int eventID, string assignedADID)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@TECH", SqlDbType.VarChar),
                    new SqlParameter("@DSPTCH_TM", SqlDbType.DateTime),
                    new SqlParameter("@EVENT_ID", SqlDbType.Int),
                    new SqlParameter("@ASN_BY_USER_ADID", SqlDbType.VarChar)
                };
            pc[0].Value = orderId;
            pc[1].Value = tech;
            pc[2].Value = dispatchTime;
            pc[3].Value = eventID;
            pc[4].Value = assignedADID;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertTechAssignment_V5U_V2 @ORDR_ID,@TECH,@DSPTCH_TM,@EVENT_ID,@ASN_BY_USER_ADID", pc.ToArray());
            return toReturn;
        }

        public DataTable GetJeopardyCodeList()
        {
            var data = _context.LkCpeJprdy;
            return LinqHelper.CopyToDataTable(data, null, null);
        }

        public async Task<DataTable> GetCpeAcctCntct_V5U(int orderId, int csgLvlId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getCpeAcctCntct_V5U {csgLvlId}, {orderId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public int InsertEmailRequest_V5U(int orderID, string deviceID, int emailReqTypeID, int statusID, string fTN, string primaryEmail, string secondaryEmail, string emailNote, string emailJeopardyNote)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@DEVICE_ID", SqlDbType.VarChar),
                    new SqlParameter("@EMAIL_REQ_TYPE_ID", SqlDbType.Int),
                    new SqlParameter("@STUS_ID", SqlDbType.Int),
                    new SqlParameter("@FTN", SqlDbType.VarChar),
                    new SqlParameter("@EMAIL_LIST_TXT", SqlDbType.VarChar),
                    new SqlParameter("@EMAIL_CC_TXT", SqlDbType.VarChar),
                    new SqlParameter("@EMAIL_NTE", SqlDbType.VarChar),
                    new SqlParameter("@EMAIL_JPRDY_NTE", SqlDbType.VarChar)

                };
            pc[0].Value = orderID;
            pc[1].Value = deviceID;
            pc[2].Value = emailReqTypeID;
            pc[3].Value = statusID;
            pc[4].Value = fTN;
            pc[5].Value = primaryEmail ?? "";
            pc[6].Value = secondaryEmail ?? "";
            pc[7].Value = emailNote ?? "";
            pc[8].Value = emailJeopardyNote ?? "";

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertEmailRequest_V5U @ORDR_ID, @DEVICE_ID,@EMAIL_REQ_TYPE_ID,@STUS_ID, @FTN,@EMAIL_LIST_TXT,@EMAIL_CC_TXT, @EMAIL_NTE,@EMAIL_JPRDY_NTE", pc.ToArray());
            return toReturn;
        }

        public int InsertRTS_V5U(int orderId, string deviceID, string jeopardyCode, string note)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@p0", SqlDbType.Int),
                    new SqlParameter("@p1", SqlDbType.VarChar),
                    new SqlParameter("@p2", SqlDbType.VarChar),
                    new SqlParameter("@p3", SqlDbType.VarChar)
                };
            pc[0].Value = orderId;
            pc[1].Value = deviceID;
            pc[2].Value = jeopardyCode;
            pc[3].Value = note ?? "";

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertRTS_V5U @p0, @p1,@p2,@p3", pc.ToArray());
            return toReturn;
        }

        public async Task<DataTable> GetRequisitionNumber_V5U(int orderId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getReqNbr_V5U {orderId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public DataTable GetDomesticClliCode()
        {

            var dmcd = (from cd in _context.LkDmstcClliCd
                        where cd.RecStusId == 1
                        orderby cd.ClliCd
                        select new { cd.ClliCd, cd.LkDmstcClliCdId });

            return LinqHelper.CopyToDataTable(dmcd, null, null);
        }

        public DataTable GetInternationalClliCode()
        {

            var dmcd = (from cd in _context.LkIntlClliCd
                        where cd.RecStusId == 1
                        orderby cd.ClliCd
                        select new { cd.ClliCd, cd.LkIntlClliCdId });

            return LinqHelper.CopyToDataTable(dmcd, null, null);
        }

        public async Task<DataTable> GetCpeReqHeaderAcctCodes_V5U(int orderId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getCpeReqHdrAcctCds_V5U {orderId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public async Task<DataTable> GetCPEReqLineItems_V5U(int orderId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getCpeReqLineItems_V5U {orderId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public int InsertRequisitionHeader_V5U(PsReqHdrQueue reqHdrQueue)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@REQSTN_NBR", SqlDbType.VarChar),
                    new SqlParameter("@CUST_ELID", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_CLLI", SqlDbType.VarChar),

                    new SqlParameter("@DLVY_NME", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_ADDR1", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_ADDR2", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_ADDR3", SqlDbType.VarChar),

                    new SqlParameter("@DLVY_ADDR4", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_CTY", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_CNTY", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_ST", SqlDbType.VarChar),

                    new SqlParameter("@DLVY_ZIP", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_PHN_NBR", SqlDbType.VarChar),
                    new SqlParameter("@INST_CLLI", SqlDbType.VarChar),
                    new SqlParameter("@MRK_PKG", SqlDbType.VarChar),

                    new SqlParameter("@REF_NBR", SqlDbType.VarChar),
                    new SqlParameter("@SHIP_CMMTS", SqlDbType.VarChar),
                    new SqlParameter("@RFQ_INDCTR", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_BLDG", SqlDbType.VarChar),

                    new SqlParameter("@DLVY_FLR", SqlDbType.VarChar),
                    new SqlParameter("@DLVY_RM", SqlDbType.VarChar),
                    new SqlParameter("@RAS_DT", SqlDbType.Date)
                };
            pc[0].Value = reqHdrQueue.OrdrId;
            pc[1].Value = reqHdrQueue.ReqstnNbr;
            pc[2].Value = reqHdrQueue.CustElid ?? "";
            pc[3].Value = reqHdrQueue.DlvyClli ?? "";

            pc[4].Value = reqHdrQueue.DlvyNme ?? "";
            pc[5].Value = reqHdrQueue.DlvyAddr1 ?? "";
            pc[6].Value = reqHdrQueue.DlvyAddr2 ?? "";
            pc[7].Value = reqHdrQueue.DlvyAddr3 ?? "";

            pc[8].Value = reqHdrQueue.DlvyAddr4 ?? "";
            pc[9].Value = reqHdrQueue.DlvyCty ?? "";
            pc[10].Value = reqHdrQueue.DlvyCnty ?? "";
            pc[11].Value = reqHdrQueue.DlvySt ?? "";

            pc[12].Value = reqHdrQueue.DlvyZip ?? "";
            pc[13].Value = reqHdrQueue.DlvyPhnNbr ?? "";
            pc[14].Value = reqHdrQueue.InstClli ?? "";
            pc[15].Value = reqHdrQueue.MrkPkg ?? "";

            pc[16].Value = reqHdrQueue.RefNbr ?? "";
            pc[17].Value = reqHdrQueue.ShipCmmts ?? "";
            pc[18].Value = reqHdrQueue.RfqIndctr ?? "";
            pc[19].Value = reqHdrQueue.DlvyBldg ?? "";

            pc[20].Value = reqHdrQueue.DlvyFlr ?? "";
            pc[21].Value = reqHdrQueue.DlvyRm ?? "";
            pc[22].Value = reqHdrQueue.SentDt;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertReqHdr_V5U @ORDR_ID, @REQSTN_NBR,@CUST_ELID,@DLVY_CLLI," +
                "@DLVY_NME, @DLVY_ADDR1,@DLVY_ADDR2,@DLVY_ADDR3," +
                "@DLVY_ADDR4, @DLVY_CTY,@DLVY_CNTY,@DLVY_ST," +
                "@DLVY_ZIP, @DLVY_PHN_NBR,@INST_CLLI,@MRK_PKG," +
                "@REF_NBR, @SHIP_CMMTS,@RFQ_INDCTR,@DLVY_BLDG," +
                "@DLVY_FLR, @DLVY_RM,@RAS_DT", pc.ToArray());

            return toReturn;
        }

        public int InsertReqLineItem_V5U(PsReqLineItmQueue lineItemToAdd)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@CMPNT_ID", SqlDbType.Int),
                    new SqlParameter("@REQSTN_NBR", SqlDbType.VarChar),
                    new SqlParameter("@MAT_CD", SqlDbType.VarChar),

                    new SqlParameter("@DLVY_CLLI", SqlDbType.VarChar),
                    new SqlParameter("@ITM_DES", SqlDbType.VarChar),
                    new SqlParameter("@MANF_PART_NBR", SqlDbType.VarChar),
                    new SqlParameter("@MANF_ID", SqlDbType.VarChar),

                    new SqlParameter("@ORDR_QTY", SqlDbType.Int),
                    new SqlParameter("@UNT_MSR", SqlDbType.VarChar),
                    new SqlParameter("@UNT_PRICE", SqlDbType.Money),
                    new SqlParameter("@RAS_DT", SqlDbType.Date),

                    new SqlParameter("@VNDR_NME", SqlDbType.VarChar),
                    new SqlParameter("@BUS_UNT_GL", SqlDbType.VarChar),
                    new SqlParameter("@ACCT", SqlDbType.VarChar),
                    new SqlParameter("@COST_CNTR", SqlDbType.VarChar),

                    new SqlParameter("@PRODCT", SqlDbType.VarChar),
                    new SqlParameter("@MRKT", SqlDbType.VarChar),
                    new SqlParameter("@AFFLT", SqlDbType.VarChar),
                    new SqlParameter("@REGN", SqlDbType.VarChar),

                    new SqlParameter("@PROJ_ID", SqlDbType.VarChar),
                    new SqlParameter("@BUS_UNT_PC", SqlDbType.VarChar),
                    new SqlParameter("@ACTVY", SqlDbType.VarChar),
                    new SqlParameter("@SOURCE_TYP", SqlDbType.VarChar),

                    new SqlParameter("@RSRC_CAT", SqlDbType.VarChar),
                    new SqlParameter("@RSRC_SUB", SqlDbType.VarChar),
                    new SqlParameter("@CNTRCT_ID", SqlDbType.VarChar),
                    new SqlParameter("@CNTRCT_LN_NBR", SqlDbType.Int),

                    new SqlParameter("@AXLRY_ID", SqlDbType.VarChar),
                    new SqlParameter("@INST_CD", SqlDbType.VarChar),
                    new SqlParameter("@SENT_DT", SqlDbType.Date),
                    new SqlParameter("@FSA_CPE_LINE_ITEM_ID", SqlDbType.Int),

                    new SqlParameter("@EQPT_TYPE_ID", SqlDbType.VarChar),
                    new SqlParameter("@COMMENTS", SqlDbType.VarChar),
                    new SqlParameter("@MANF_DISCNT_CD", SqlDbType.VarChar),
                    new SqlParameter("@REQ_LINE_NBR", SqlDbType.Int)
                };
            pc[0].Value = lineItemToAdd.OrdrId;
            pc[1].Value = lineItemToAdd.CmpntId;
            pc[2].Value = lineItemToAdd.ReqstnNbr ?? "";
            pc[3].Value = lineItemToAdd.MatCd ?? "";

            pc[4].Value = lineItemToAdd.DlvyClli ?? "";
            pc[5].Value = lineItemToAdd.ItmDes ?? "";
            pc[6].Value = lineItemToAdd.ManfPartNbr ?? "";
            pc[7].Value = lineItemToAdd.ManfId ?? "";

            pc[8].Value = lineItemToAdd.OrdrQty;
            pc[9].Value = lineItemToAdd.UntMsr ?? "";
            pc[10].Value = lineItemToAdd.UntPrice ?? 0;
            pc[11].Value = lineItemToAdd.RasDt;

            pc[12].Value = lineItemToAdd.VndrNme ?? "";
            pc[13].Value = lineItemToAdd.BusUntGl ?? "";
            pc[14].Value = lineItemToAdd.Acct ?? "";
            pc[15].Value = lineItemToAdd.CostCntr ?? "";

            pc[16].Value = lineItemToAdd.Prodct ?? "";
            pc[17].Value = lineItemToAdd.Mrkt ?? "";
            pc[18].Value = lineItemToAdd.Afflt ?? "";
            pc[19].Value = lineItemToAdd.Regn ?? "";

            pc[20].Value = lineItemToAdd.ProjId ?? "";
            pc[21].Value = lineItemToAdd.BusUntPc ?? "";
            pc[22].Value = lineItemToAdd.Actvy ?? "";
            pc[23].Value = lineItemToAdd.SourceTyp ?? "";

            pc[24].Value = lineItemToAdd.RsrcCat ?? "";
            pc[25].Value = lineItemToAdd.RsrcSub ?? "";
            pc[26].Value = lineItemToAdd.CntrctId ?? "";
            pc[27].Value = lineItemToAdd.CntrctLnNbr ?? 0;

            pc[28].Value = lineItemToAdd.AxlryId ?? "";
            pc[29].Value = lineItemToAdd.InstCd ?? "";
            pc[30].Value = lineItemToAdd.SentDt;
            pc[31].Value = lineItemToAdd.FsaCpeLineItemId;

            pc[32].Value = lineItemToAdd.EqptTypeId ?? "";
            pc[33].Value = lineItemToAdd.Comments ?? "";
            pc[34].Value = lineItemToAdd.ManfDiscntCd ?? "";
            pc[35].Value = lineItemToAdd.ReqLineNbr;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertReqLineItem_V5U @ORDR_ID, @CMPNT_ID,@REQSTN_NBR,@MAT_CD," +
                "@DLVY_CLLI, @ITM_DES,@MANF_PART_NBR,@MANF_ID," +
                "@ORDR_QTY, @UNT_MSR,@UNT_PRICE,@RAS_DT," +
                "@VNDR_NME, @BUS_UNT_GL,@ACCT,@COST_CNTR," +
                "@PRODCT, @MRKT,@AFFLT,@REGN," +
                "@PROJ_ID, @BUS_UNT_PC,@ACTVY,@SOURCE_TYP," +
                "@RSRC_CAT, @RSRC_SUB,@CNTRCT_ID,@CNTRCT_LN_NBR," +
                "@AXLRY_ID, @INST_CD,@SENT_DT,@FSA_CPE_LINE_ITEM_ID," +
                "@EQPT_TYPE_ID, @COMMENTS,@MANF_DISCNT_CD,@REQ_LINE_NBR", pc.ToArray());

            return toReturn;
        }

        public DataTable GetVendorsList()
        {
            var data = (from t in _context.LkVndrScm
                        select new { t.LkVndrScmId, t.VndrNme });

            return LinqHelper.CopyToDataTable(data, null, null);
        }

        public DataTable GetReqHeaderQueueByOrderID(int orderID)
        {
            var data = (from req in _context.PsReqHdrQueue
                        where req.OrdrId == orderID
                        select new
                        {
                            req.DlvyNme,
                            req.DlvyPhnNbr,
                            req.MrkPkg
                        });

            return LinqHelper.CopyToDataTable(data, null, null);
        }

        public DataTable GetCPEPidByOrderID(int orderID)
        {
            var data = (from ordr in _context.Ordr
                        join cpid in _context.LkCpePid on ordr.DmstcCd equals cpid.DmstcCd
                        where ordr.OrdrId == orderID
                        select new
                        {
                            cpid.EqptTypeId,
                            cpid.PidCntrctType
                        });

            return LinqHelper.CopyToDataTable(data, null, null);
        }

        public async Task<DataTable> GetCpeCmplLineItems_V5U(int orderId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getCpeCmplLineItems_V5U {orderId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public DataTable GetTechAssignment_V5U(int orderID)
        {
            var data = (from wfm in _context.UserWfmAsmt
                        join u in _context.LkUser on wfm.AsnUserId equals u.UserId
                        where wfm.OrdrId == orderID && ((wfm.UsrPrfId == 44) || (wfm.UsrPrfId == 56))
                        select new
                        {
                            USER_CPE_TECH_NME = u.FullNme,
                            DSPTCH_TM = wfm.DsptchTm,
                            EVENT_ID = wfm.EventId,
                            INRTE_TM = wfm.InrteTm,
                            ONSITE_TM = wfm.OnsiteTm,
                            CMPLT_TM = wfm.CmpltTm,
                            PHN_NBR = u.PhnNbr,
                            EMAIL_ADR = u.EmailAdr,
                            wfm.CreatDt
                        }).OrderByDescending(a => a.CreatDt);

            return LinqHelper.CopyToDataTable(data, null, null);
        }

        public int InsertOrdrCmplPartial_V5U(int fsaCpeLineItemId, DateTime completeDate)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@FSA_CPE_LINE_ITEM_ID", SqlDbType.Int),
                    new SqlParameter("@CMPL_DT", SqlDbType.DateTime)

                };
            pc[0].Value = fsaCpeLineItemId;
            pc[1].Value = completeDate;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertOrdrCmplPartial_V5U @FSA_CPE_LINE_ITEM_ID,@CMPL_DT", pc.ToArray());
            return toReturn;
        }

        public int InsertOrdrCmplAll_V5U(int orderID, string deviceID, string note, string tech, DateTime? inRouteDate, DateTime? onSiteDate, DateTime? completeDate)
        {
            //var date = Convert.ToDateTime("1970-01-01T00:00:00.000Z").ToUniversalTime();
            //inRouteDate = (inRouteDate == date) ? null : inRouteDate;
            //onSiteDate = (onSiteDate == date) ? null : onSiteDate;
            //completeDate = (completeDate == date) ? null : completeDate;

            //List<SqlParameter> pc = new List<SqlParameter>
            //    {
            //        new SqlParameter() {ParameterName = "@ORDR_ID", SqlDbType = SqlDbType.Int, Value = orderID},
            //        new SqlParameter() {ParameterName = "@DEVICE_ID", SqlDbType = SqlDbType.VarChar, Value = deviceID},
            //        new SqlParameter() {ParameterName = "@NOTE", SqlDbType = SqlDbType.VarChar, Value = note==""?null:note },
            //        new SqlParameter() {ParameterName = "@TECH", SqlDbType = SqlDbType.VarChar, Value = tech},
            //        new SqlParameter() {ParameterName = "@INROUTE", SqlDbType = SqlDbType.DateTime, Value = inRouteDate},
            //        new SqlParameter() {ParameterName = "@ONSITE", SqlDbType = SqlDbType.DateTime, Value = onSiteDate},
            //        new SqlParameter() {ParameterName = "@COMPLETED", SqlDbType = SqlDbType.DateTime, Value = completeDate}
            //    };


            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@DEVICE_ID", SqlDbType.VarChar),
                    new SqlParameter("@NOTE", SqlDbType.VarChar),
                    new SqlParameter("@TECH", SqlDbType.VarChar),
                    new SqlParameter("@INROUTE", SqlDbType.DateTime),
                    new SqlParameter("@ONSITE", SqlDbType.DateTime),
                    new SqlParameter("@COMPLETED", SqlDbType.DateTime)

                };
            //var date = Convert.ToDateTime("1970-01-01T00:00:00.000Z").ToUniversalTime();
            pc[0].Value = orderID;
            pc[1].Value = string.IsNullOrEmpty(deviceID) ? "" : deviceID;
            pc[2].Value = note;
            pc[3].Value = (object)tech ?? DBNull.Value; ;
            pc[4].Value = (object)inRouteDate ?? DBNull.Value;
            pc[5].Value = (object)onSiteDate ?? DBNull.Value;
            pc[6].Value = (object)completeDate ?? DBNull.Value;
            //pc[4].Value = (inRouteDate == date) ? null : inRouteDate;
            //pc[5].Value = (onSiteDate == date) ? null : onSiteDate;
            //pc[6].Value = (completeDate == date) ? null : completeDate;

            //int toReturn;
            //if (inRouteDate == null && onSiteDate == null && completeDate == null)
            //{
            //    toReturn = _context.Database.ExecuteSqlCommand("dbo.insertOrdrCmplAll_V5U_V2 @ORDR_ID,@DEVICE_ID,@NOTE,@TECH", pc.ToArray());
            //}
            //else
            //{
            int toReturn = _context.Database.ExecuteSqlCommand("dbo.insertOrdrCmplAll_V5U_V2 @ORDR_ID,@DEVICE_ID,@NOTE,@TECH,@INROUTE,@ONSITE,@COMPLETED", pc.ToArray());
            //}
            return toReturn;
        }

        public DataTable GetTasksList()
        {
            var data = (from t in _context.LkTask
                        where t.TaskId == 600 || t.TaskId == 601 || t.TaskId == 602
                        select new { t.TaskId, t.TaskNme });

            return LinqHelper.CopyToDataTable(data, null, null);
        }

        public int UpdateCPEOrdr_V5U(int orderID, int modType, string adid, string jCode, string comments, string clli)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@MOD_TYP", SqlDbType.Int),
                    new SqlParameter("@ADID", SqlDbType.VarChar),
                    new SqlParameter("@JCODE", SqlDbType.VarChar),
                    new SqlParameter("@COMMENTS", SqlDbType.VarChar),
                    new SqlParameter("@CLLI", SqlDbType.VarChar)

                };
            pc[0].Value = orderID;
            pc[1].Value = modType;
            pc[2].Value = adid;
            pc[3].Value = jCode;
            pc[4].Value = comments;
            pc[5].Value = clli;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.updateCPEOrdr_V5U @ORDR_ID, @MOD_TYP,@ADID,@JCODE,@COMMENTS,@CLLI", pc.ToArray());
            return toReturn;
        }

        public int UpdateDmstcWFM_V5U(int orderID, string adid, string assignerADID, string comments)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@ADID", SqlDbType.VarChar),
                    new SqlParameter("@ASSIGNER_ADID", SqlDbType.VarChar),
                    new SqlParameter("@COMMENTS", SqlDbType.VarChar)

                };
            pc[0].Value = orderID;
            pc[1].Value = adid;
            pc[2].Value = assignerADID;
            pc[3].Value = comments;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.updateDmstcWFM_V5U @ORDR_ID,@ADID,@ASSIGNER_ADID,@COMMENTS", pc.ToArray());
            return toReturn;
        }

        public int ProcessCPEMve_V5U(int orderID, string adid, string taskName, string comments)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@ADID", SqlDbType.VarChar),
                    new SqlParameter("@TASK_NME", SqlDbType.VarChar),
                    new SqlParameter("@COMMENTS", SqlDbType.VarChar)

                };
            pc[0].Value = orderID;
            pc[1].Value = adid;
            pc[2].Value = taskName;
            pc[3].Value = comments;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.processCPEMve_V5U @ORDR_ID,@ADID,@TASK_NME,@COMMENTS", pc.ToArray());
            return toReturn;
        }

        public bool IsNIDDevice(int orderID, string deviceID)
        {
            var nidl = (from foc in _context.FsaOrdrCpeLineItem
                        where foc.OrdrId == orderID
                          && foc.DeviceId == deviceID
                        && EF.Functions.Like(foc.CmpntFmly, "NID%")
                        && foc.SprintMntdFlg.Equals("Y")
                        select foc).FirstOrDefault();

            return (nidl != null);
        }

        public int UpdateNIDSerialNbr(int orderID, string ftn, string deviceID, string nidSerialNbr, int userId)
        {
            int lineitemid = -1;
            var focl = (from foc in _context.FsaOrdrCpeLineItem
                        where foc.OrdrId == orderID
                          && foc.DeviceId == deviceID
                          && EF.Functions.Like(foc.CmpntFmly, "NID%")
                          && foc.SprintMntdFlg.Equals("Y")
                          & !_context.NidActy.Any(a => a.FsaCpeLineItemId == foc.FsaCpeLineItemId && a.NidSerialNbr == nidSerialNbr && a.RecStusId == (short)251)
                        select foc);
            //if (focl != null)
            //{
            //    focl.NidSerialNbr = nidSerialNbr;
            //    lineitemid = focl.FsaCpeLineItemId;
            //    _context.SaveChanges();
            //}
            if (focl != null)
            {
                List<NidActy> lnac = new List<NidActy>();
                foreach (var nidfc in focl)
                {
                    NidActy nac = new NidActy
                    {
                        NidSerialNbr = nidSerialNbr,
                        FsaCpeLineItemId = nidfc.FsaCpeLineItemId,
                        M5OrdrNbr = ftn,
                        RecStusId = (short)251,
                        CreatDt = DateTime.Now,
                        CreatByUserId = userId
                    };
                    lnac.Add(nac);
                }
                _context.NidActy.AddRange(lnac);

                _context.SaveChanges();
            }
            if (orderID > 0)
                _commonRepo.UpdateNIDAtBPM(orderID, string.Empty, nidSerialNbr, userId, -1);

            return lineitemid;
        }

        public int UpdateNuaCkt(int orderID, string nuaCkt, string userAdid)
        {
            List<SqlParameter> pc = new List<SqlParameter>
                {
                    new SqlParameter("@ORDR_ID", SqlDbType.Int),
                    new SqlParameter("@NuaCkt", SqlDbType.VarChar),
                    new SqlParameter("@User", SqlDbType.VarChar)

                };
            pc[0].Value = orderID;
            pc[1].Value = nuaCkt;
            pc[2].Value = userAdid;

            int toReturn = _context.Database.ExecuteSqlCommand("dbo.UpdateNUACkt @ORDR_ID,@NuaCkt,@User", pc.ToArray());
            return toReturn;
        }

        public async Task<DataTable> GetShippingInstr(int orderId)
        {
            DataTable dt = new DataTable();
            using (var connection = _context.Database.GetDbConnection())
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.CommandTimeout = _context.commandTimeout;
                    command.CommandText = $"dbo.getPeopleSoftHdrNte {orderId}";
                    var result = await command.ExecuteReaderAsync();
                    dt.Load(result);
                    return dt;
                }
            }
        }

        public bool IsNIDAlreadyUsed(int orderId, string nidSerialNbr)
        {
            var nidActy = _context.NidActy
                .Include(a => a.IpActy)
                .Include(a => a.FsaCpeLineItem)
                .Where(a => a.NidSerialNbr == nidSerialNbr
                    && a.RecStusId == 251
                    && a.FsaCpeLineItem.OrdrId != orderId
                    && a.IpActy
                        .Where(b => b.RecStusId == 251)
                        .Any()
                )
                .FirstOrDefault();

            if (nidActy != null)
            {
                var ipMasterId = nidActy.IpActy.FirstOrDefault().IpMstrId;

                return _context.IpMstr.Where(a => a.IpMstrId == ipMasterId && a.RecStusId == 253).Any();
            }
            return false;
        }
    }
}