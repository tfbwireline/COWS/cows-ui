﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class AccessCitySiteRepository : IAccessCitySiteRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public AccessCitySiteRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkAccsCtySite Create(LkAccsCtySite entity)
        {
            int maxId = _context.LkAccsCtySite.Max(i => i.AccsCtySiteId);
            entity.AccsCtySiteId = ++maxId;
            _context.LkAccsCtySite.Add(entity);

            SaveAll();

            return GetById(entity.AccsCtySiteId);
        }

        public void Delete(int id)
        {
            LkAccsCtySite city = GetById(id);
            _context.LkAccsCtySite.Remove(city);

            SaveAll();
        }

        public IQueryable<LkAccsCtySite> Find(Expression<Func<LkAccsCtySite, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkAccsCtySite, out List<LkAccsCtySite> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkAccsCtySite> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkAccsCtySite, out List<LkAccsCtySite> list))
            {
                list = _context.LkAccsCtySite
                            .OrderBy(i => i.AccsCtyNmeSiteCd)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkAccsCtySite, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkAccsCtySite GetById(int id)
        {
            return _context.LkAccsCtySite
                            .SingleOrDefault(i => i.AccsCtySiteId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkAccsCtySite);
            return _context.SaveChanges();
        }

        public void Update(int id, LkAccsCtySite entity)
        {
            LkAccsCtySite city = GetById(id);
            city.AccsCtyNmeSiteCd = entity.AccsCtyNmeSiteCd;

            SaveAll();
        }
    }
}