﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class AsrTypeRepository : IAsrTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IEventRuleRepository _evntRuleRepo;
        private IMemoryCache _cache;

        public AsrTypeRepository(COWSAdminDBContext context, IEventRuleRepository evntRuleRepo, IMemoryCache cache)
        {
            _context = context;
            _evntRuleRepo = evntRuleRepo;
            _cache = cache;
        }

        public IQueryable<LkAsrType> Find(Expression<Func<LkAsrType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkAsrType, out List<LkAsrType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkAsrType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkAsrType, out List<LkAsrType> list))
            {
                list = _context.LkAsrType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkAsrType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkAsrType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public LkAsrType Create(LkAsrType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkAsrType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkAsrType);
            return _context.SaveChanges();
        }
    }
}