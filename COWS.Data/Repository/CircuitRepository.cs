﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CircuitRepository : ICircuitRepository
    {
        private readonly COWSAdminDBContext _context;

        public CircuitRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public IQueryable<Ckt> Find(Expression<Func<Ckt, bool>> predicate)
        {
            return _context.Ckt
                .Include(a => a.CktMs)
                .Include(a => a.CreatByUser)
                .Where(predicate);
        }

        public IEnumerable<Ckt> GetAll()
        {
            return _context.Ckt.ToList();
        }

        public Ckt GetById(int id)
        {
            return Find(a => a.CktId == id).SingleOrDefault();
        }

        public Ckt Create(Ckt entity)
        {
            _context.Ckt.Add(entity);
            SaveAll();

            return GetById(entity.CktId);
        }

        public void Update(int id, Ckt entity)
        {
            Ckt ckt = GetById(id);

            ckt.VndrCktId = entity.VndrCktId;
            ckt.LecId = entity.LecId;
            ckt.IpAdrQty = entity.IpAdrQty;
            ckt.RtngPrcol = entity.RtngPrcol;
            ckt.CustIpAdr = entity.CustIpAdr;
            ckt.ScaDet = entity.ScaDet;
            ckt.CreatByUserId = entity.CreatByUserId;
            ckt.CreatDt = entity.CreatDt;
        }

        public void Delete(int id)
        {
            var toDelete = GetById(id);
            _context.Ckt.Remove(toDelete);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public DataTable GetPreQualLineInfo(int orderID)
        {
            List<DataTable> lstPQL = new List<DataTable>();

                    var pq = (from _pq in _context.FsaOrdr
                              join _vndr in _context.LkVndr on _pq.InstlVndrCd equals _vndr.VndrCd
                              where _pq.OrdrId == orderID
                              select new
                              {
                                  //vendorRAWMRC = _pq.TPORT_VNDR_RAW_MRC_IN_CUR_AMT,
                                  vendorRAWMRCQuote = _pq.TportVndrRawMrcQotCurAmt,
                                  vendorRAWNRCQuote = _pq.TportVndrRawNrcQotCurAmt,
                                  conversionPercentageQty = _pq.TportCnvrsnRtQty,
                                  markupPercentageQty = _pq.TportMarkupPctQty,
                                  taxPercentageQty = _pq.TportTaxPctQty,
                                  bearerChargeAmt = _pq.TportBearerChgAmt,
                                  additionalMRCAmt = _pq.TportAddlMrcAmt,
                                  calculatedRateMRC = _pq.TportCalcRtMrcUsdAmt,
                                  calculatedRateNRC = _pq.TportCalcRtNrcUsdAmt,
                                  calculatedRateMRCCurrency = _pq.TportCalcRtMrcInCurAmt,
                                  calculatedRateNRCCurrency = _pq.TportCalcRtNrcInCurAmt,
                                  currency = _pq.TportCurNme,
                                  preQualSiteID = _pq.TportPrequalSiteId,
                                  preQualLineItem = _pq.TportPrequalLineItemId,
                                  preQualExpiration = _pq.TportPreqLineItemIdXpirnDt,
                                  vendorTerm = _pq.TtrptVndrTerm,
                                  vendorNme = _pq.InstlVndrCd + " - " + _vndr.VndrNme,
                                  vendorName = _vndr.VndrNme,
                                  tportVndrRawMrcAmt = _pq.TportVndrRawMrcAmt,
                                  tportVndrRawNrcAmt = _pq.TportVndrRawNrcAmt,
                                  vndrTerm = _pq.VndrTerm,
                                  tportVndrRvsdQotCur = _pq.TportVndrRvsdQotCur
                              });

            return LinqHelper.CopyToDataTable(pq, null, null);
        }

        public IEnumerable<Ckt> GetAccessCost(int orderId)
        {
            IEnumerable<Ckt> circuits = _context.Ckt
                .Where(a => a.OrdrId == orderId)
                .AsNoTracking()
                .ToList();

            for (int i = 0; i < circuits.Count(); i++)
            {
                Ckt circuit = circuits.ElementAt(i);
                CktCost circuitCost = _context.CktCost
                    .Where(a => a.CktId == circuit.CktId)
                    .OrderByDescending(a => a.VerId)
                    .AsNoTracking()
                    .FirstOrDefault();

                if (circuitCost != null)
                {
                    circuitCost.VndrCur = _context.LkCur
                        .Where(a => a.CurId == circuitCost.VndrCurId)
                        .AsNoTracking()
                        .FirstOrDefault() ?? new LkCur();
                    circuitCost.AsrCur = _context.LkCur
                        .Where(a => a.CurId == circuitCost.AsrCurId)
                        .AsNoTracking()
                        .FirstOrDefault() ?? new LkCur();
                    circuitCost.AccsCustCur = _context.LkCur
                        .Where(a => a.CurId == circuitCost.AccsCustCurId)
                        .AsNoTracking()
                        .FirstOrDefault() ?? new LkCur();
                }

                circuit.CktCost = new List<CktCost>();
                circuit.CktCost.Add(circuitCost ?? new CktCost());
                //if (circuitCost != null)
                //{
                //    circuit.CktCost.Add(circuitCost);
                //}
                //else
                //{
                //    circuit.CktCost.Add(new CktCost());
                //}
            }

            return circuits;
        }
    }
}