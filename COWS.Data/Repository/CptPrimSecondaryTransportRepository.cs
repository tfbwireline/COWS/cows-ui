﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class CptPrimSecondaryTransportRepository : ICptPrimSecondaryTransportRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public CptPrimSecondaryTransportRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public LkCptPrimScndyTprt Create(LkCptPrimScndyTprt entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkCptPrimScndyTprt> Find(Expression<Func<LkCptPrimScndyTprt, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPrimScndyTprt, out List<LkCptPrimScndyTprt> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkCptPrimScndyTprt> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkCptPrimScndyTprt, out List<LkCptPrimScndyTprt> list))
            {
                list = _context.LkCptPrimScndyTprt
                            .OrderBy(i => i.CptPrimScndyTprt)
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkCptPrimScndyTprt, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkCptPrimScndyTprt GetById(int id)
        {
            return _context.LkCptPrimScndyTprt
                            .SingleOrDefault(i => i.CptPrimScndyTprtId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkCptPrimScndyTprt);
            throw new NotImplementedException();
        }

        public void Update(int id, LkCptPrimScndyTprt entity)
        {
            throw new NotImplementedException();
        }
    }
}