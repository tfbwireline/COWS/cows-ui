﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class MplsEventAccessTagRepository : IMplsEventAccessTagRepository
    {
        private readonly COWSAdminDBContext _context;

        public MplsEventAccessTagRepository(COWSAdminDBContext context)
        {
            _context = context;
        }

        public MplsEventAccsTag Create(MplsEventAccsTag entity)
        {
            int maxId = _context.MplsEventAccsTag.Max(i => i.MplsEventAccsTagId);
            entity.MplsEventAccsTagId = ++maxId;
            _context.MplsEventAccsTag.Add(entity);

            SaveAll();

            return GetById(entity.MplsEventAccsTagId);
        }

        public void Delete(int id)
        {
            MplsEventAccsTag tag = GetById(id);
            _context.MplsEventAccsTag.Remove(tag);

            SaveAll();
        }

        public IQueryable<MplsEventAccsTag> Find(Expression<Func<MplsEventAccsTag, bool>> predicate)
        {
            return _context.MplsEventAccsTag
                            .Include(i => i.CtryCdNavigation)
                            .Include(i => i.Event)
                            .Include(i => i.MnsOffrgRoutr)
                            .Include(i => i.MnsPrfmcRpt)
                            .Include(i => i.MnsRoutgType)
                            .Include(i => i.MplsAccsBdwd)
                            .Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<MplsEventAccsTag> GetAll()
        {
            return _context.MplsEventAccsTag
                            .Include(i => i.CtryCdNavigation)
                            .Include(i => i.Event)
                            .Include(i => i.MnsOffrgRoutr)
                            .Include(i => i.MnsPrfmcRpt)
                            .Include(i => i.MnsRoutgType)
                            .Include(i => i.MplsAccsBdwd)
                            .OrderBy(i => i.LocCtyNme)
                            .AsNoTracking()
                            .ToList();
        }

        public MplsEventAccsTag GetById(int id)
        {
            return _context.MplsEventAccsTag
                            .Include(i => i.CtryCdNavigation)
                            .Include(i => i.Event)
                            .Include(i => i.MnsOffrgRoutr)
                            .Include(i => i.MnsPrfmcRpt)
                            .Include(i => i.MnsRoutgType)
                            .Include(i => i.MplsAccsBdwd)
                            .SingleOrDefault(i => i.MplsEventAccsTagId == id);
        }

        public int SaveAll()
        {
            return _context.SaveChanges();
        }

        public void Update(int id, MplsEventAccsTag entity)
        {
            Delete(id);
            entity.CreatDt = DateTime.Now;
            Create(entity);
        }

        public List<MplsEventAccsTag> Create(List<MplsEventAccsTag> list)
        {
            // list can be used instead of temp; no need to create another
            List<MplsEventAccsTag> temp = new List<MplsEventAccsTag>();
            foreach (MplsEventAccsTag entity in list)
            {
                int maxId = _context.MplsEventAccsTag.Max(i => i.MplsEventAccsTagId);
                entity.MplsEventAccsTagId = ++maxId;
                _context.MplsEventAccsTag.Add(entity);
                temp.Add(entity);
            }

            SaveAll();

            return temp;
        }
    }
}