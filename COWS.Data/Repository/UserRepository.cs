﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;
using Microsoft.Extensions.Caching.Memory;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace COWS.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly COWSAdminDBContext _context;
        private const int _INTERVAL = 10;
        private DataTable _dtApptRec;
        private DateTime _StartRange;
        private DateTime _EndRange;
        private IMemoryCache _cache;
        private readonly IConfiguration _configuration;

        public UserRepository(COWSAdminDBContext context, IMemoryCache cache, IConfiguration configuration)
        {
            _context = context;
            _cache = cache;
            _configuration = configuration;
            SetLkUserCache();
        }

        private void SetLkUserCache()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUser, out _))
            {
                var list = _context.LkUser.Include(a => a.LkCnfrcBrdgCreatByUser).AsNoTracking().ToList();
                _cache.Set(CacheKeys.LkUser, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
        }

        public IEnumerable<LkUser> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUserGetAllActive, out List<LkUser> list))
            {
                list = _context.LkUser
                        .Where(a => a.RecStusId == 1)
                        .OrderBy(a => a.FullNme)
                        .AsNoTracking()
                        .ToList();
                _cache.Set(CacheKeys.LkUserGetAllActive, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }
        public IEnumerable<LkUser> GetAllUsers()
        {
            if (!_cache.TryGetValue(CacheKeys.LkUserGetAll, out List<LkUser> list))
            {
                list = _context.LkUser
                        .OrderBy(a => a.FullNme)
                        .AsNoTracking()
                        .ToList();
                _cache.Set(CacheKeys.LkUserGetAll, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public List<LkCnfrcBrdg> GetCnfrcBrdgCreatByUser(int userId)
        {
            var list = _context.LkCnfrcBrdg.Where(b => b.CreatByUserId == userId && b.RecStusId == 1)
                        .AsNoTracking()
                        .ToList();

            return list;
        }

        public IQueryable<LkUser> Find(Expression<Func<LkUser, bool>> predicate, bool includeNtePool = false)
        {
            List<LkUser> list = new List<LkUser>();

            //if (!_cache.TryGetValue(CacheKeys.LkUser, out list))
            //{
            //    SetLkUserCache();
            //    _cache.TryGetValue(CacheKeys.LkUser, out list);
            //}

             list = _context.LkUser.Include(a => a.LkCnfrcBrdgCreatByUser)
                        .AsNoTracking()
                        .ToList();

            // Added this condition since Redesign Assgnment can be assigned to NTE Pool
            if (includeNtePool)
            {
                return list.AsQueryable().Where(predicate).Where(a => a.FullNme != "COWS SYSTEM").AsNoTracking();
            }
            else
            {
                return list.AsQueryable().Where(predicate).Where(a => a.FullNme != "COWS SYSTEM" && a.FullNme != "NTE Pool").AsNoTracking();
            }
        }

        public LkUser GetById(int id)
        {
            return _context.LkUser
                .SingleOrDefault(a => a.UserId == id);
        }

        public LkUser Create(LkUser entity)
        {
            try
            {
                int maxId = _context.LkUser.Max(i => i.UserId);
                entity.UserId = ++maxId;
                _context.LkUser.Add(entity);

                SaveAll();
                return GetById(entity.UserId);
            }
            catch
            {
                return null;
            }
        }

        public void Update(int id, LkUser entity)
        {
            LkUser user = GetById(id);

            //user.PhnNbr = entity.PhnNbr;
            //user.EmailAdr = entity.EmailAdr;
            //user.CellPhnNbr = entity.CellPhnNbr;
            //user.PgrNbr = entity.PgrNbr;
            //user.PgrPinNbr = entity.PgrPinNbr;
            user.MenuPref = entity.MenuPref;
            user.MgrAdid = entity.MgrAdid;
            //user.ModfdByUserId = entity.ModfdByUserId;
            //user.ModfdDt = entity.ModfdDt;
            user.UserAdid = entity.UserAdid;
            user.OldUserAdid = entity.OldUserAdid;
            user.RecStusId = entity.RecStusId;


            _context.LkUser.Update(user);
            SaveAll();
        }

        public void Delete(int id)
        {
            LkUser site = GetById(id);
            site.RecStusId = 0;
            _context.LkUser.Remove(site);

            SaveAll();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkUser);
            _cache.Remove(CacheKeys.LkUserGetAllActive);
            _cache.Remove(CacheKeys.LkUserGetAll);
            return _context.SaveChanges();
        }

        public bool IsCSGLevelUser(string adid, int csgLevel)
        {
            try
            {
                var data = from u in _context.LkUser
                           join c in _context.UserCsgLvl on u.UserId equals c.UserId
                           where u.UserAdid == adid && c.CsgLvlId == csgLevel
                           select u;

                if (data.Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public byte GetCsgLevel(int userId)
        {
            byte csgLvlId = 0;
            if (_context.UserCsgLvl.AsNoTracking().Any(a => a.UserId == userId))
            {
                csgLvlId = _context.UserCsgLvl
                    .Where(a => a.UserId == userId)
                    .OrderBy(a => a.CsgLvlId)
                    .Take(1)
                    .Select(a => a.CsgLvlId)
                    .SingleOrDefault();
            }

            return csgLvlId;
        }

        public string GetCsgLevelCD(int userId)
        {
            string csgLvlCd = "";
            if (_context.UserCsgLvl.AsNoTracking().Any(a => a.UserId == userId))
            {
                var csg = _context.UserCsgLvl
                    .Where(a => a.UserId == userId)
                    .OrderBy(a => a.CsgLvlId)
                    .Take(1)
                    .Select(a => a.CsgLvl)
                    .SingleOrDefault();

                return csg.CsgLvlCd;

            }

            return csgLvlCd;
        }

        public IQueryable<LkUser> GetUsersByProfileName(string profileName)
        {
            var users = (from lu in _context.LkUser
                         join mup in _context.MapUsrPrf on lu.UserId equals mup.UserId
                         join lup in _context.LkUsrPrf on mup.UsrPrfId equals lup.UsrPrfId
                         where lu.RecStusId == 1 && lu.UserId != 1
                         && lup.RecStusId == 1 && lup.UsrPrfNme.Contains(profileName)
                         && mup.RecStusId == 1
                         select lu).Distinct().OrderBy(i => i.DsplNme).AsNoTracking();

            return users;
        }

        public IEnumerable<LkUser> GetEventActivator(string type, DynamicParameters param)
        {
            IEnumerable<LkUser> users = new List<LkUser>();

            // Base query
            IQueryable<LkUser> query = _context.LkUser
                .Include(a => a.MapUsrPrfUser)
                    .ThenInclude(a => a.UsrPrf)
                .Include(a => a.LkQlfctnAsnToUser)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnDedctdCust)
                    .ThenInclude(a => a.Cust)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnNtwkActyType)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnVndrModel)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnEventType)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnEnhncSrvc)
                    .ThenInclude(a => a.EnhncSrvc)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnIpVer)
                .Where(a =>
                    a.UserId != 1 &&
                    a.RecStusId == 1 &&
                    a.MapUsrPrfUser.Any(b =>
                        b.RecStusId == 1 &&
                        b.UsrPrf.RecStusId == 1 &&
                        EF.Functions.Like(b.UsrPrf.UsrPrfNme, "%EActivator%")) &&
                    a.LkQlfctnAsnToUser.RecStusId == 1);

            switch (type)
            {
                case "SCHEDULE":
                    var startDate = Convert.ToDateTime(param.Start.Value);
                    var endDate = Convert.ToDateTime(param.Start.Value).Date.AddDays(_INTERVAL).Add(new TimeSpan(23, 55, 00));

                    List<int> appointmentTypeIds = new List<int>();
                    appointmentTypeIds.Add((int)AppointmentType.OnShift); // Add OnShift

                    // Get all possible appointment type
                    appointmentTypeIds.AddRange(GetCurrentAppointmentTypeIds(param.EventTypeId, param.EnhanceServiceId));

                    // Get all possible skilled activators
                    var skilled = GetEventActivator("SKILLED", param).AsQueryable();

                    users = skilled
                        .Include(a => a.ApptRcurncData)
                            .ThenInclude(a => a.Appt)
                        .Where(a =>
                            a.ApptRcurncData
                                .Any(b =>
                                    b.Appt.AsnToUserIdListTxt != null &&
                                    b.Appt.EventId != null &&
                                    appointmentTypeIds.Contains(b.Appt.ApptTypeId) &&
                                    b.Appt.RcurncDesTxt != null &&
                                    b.Appt.RcurncCd == true &&
                                    b.StrtTmst.Date >= startDate.Date.AddDays(-1) &&
                                    b.EndTmst.Date <= endDate.Date
                                )
                        ).AsNoTracking();
                    break;

                case "SKILLED":
                    // Get all possible product activators
                    var product = GetEventActivator("PRODUCT", param).AsQueryable();

                    if (param.EventTypeId == (int)EventType.AD)
                    {
                        users = product
                            .Where(a =>
                                a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId)
                            ).AsNoTracking();
                    }
                    else if (param.EventTypeId == (int)EventType.MPLS)
                    {
                        users = product
                            .Where(a =>
                                a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                                a.LkQlfctnAsnToUser.QlfctnIpVer.Any(b => b.IpVerId == param.IpVersionId)
                            ).AsNoTracking();
                    }
                    else if (param.EventTypeId == (int)EventType.SprintLink)
                    {
                        users = product
                            .Where(a =>
                                a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                                a.LkQlfctnAsnToUser.QlfctnIpVer.Any(b => b.IpVerId == param.IpVersionId)
                            // Possibly add Event sprint Link Activity Type   ??
                            ).AsNoTracking();
                    }
                    else if (param.EventTypeId == (int)EventType.MDS)
                    {
                        users = product
                            .Where(a =>
                                a.LkQlfctnAsnToUser.QlfctnNtwkActyType.Any(b => param.ActivityType.Contains(b.NtwkActyTypeId))
                            ).AsNoTracking();

                        if (_context.LkDedctdCust.Where(a => a.CustNme == param.CustomerName).Count() > 0)
                        {
                            users = users
                                .Where(a =>
                                    a.LkQlfctnAsnToUser.QlfctnDedctdCust.Any(b => b.Cust.CustNme == param.CustomerName)
                                );
                        }

                        if (param.ManagedDevice != null && param.ManagedDevice.Count > 0)
                        {
                            users = users
                                .Where(a =>
                                    a.LkQlfctnAsnToUser.QlfctnVndrModel.Any(b => param.ManagedDevice.Select(c => c.DevModelId).Contains(b.DevModelId))
                                );
                        }

                        if (param.isFirewallSecured == "True")
                        {
                            users = users
                                .Where(a =>
                                    a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvc.EnhncSrvcNme == "Firewall/Security")
                                );
                        }
                    }
                    else
                    {
                        users = product
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId)
                        ).AsNoTracking();
                    }
                    break;

                case "PRODUCT":
                    users = query
                        .Where(a => a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId)).AsNoTracking();
                    break;

                default:
                    break;
            }

            return users;
        }

        public IEnumerable<LkUser> GetCheckSlots(string type, DynamicParameters param)
        {
            IEnumerable<LkUser> users = new List<LkUser>();

            // Base query
            IQueryable<LkUser> query = _context.LkUser
                .Include(a => a.MapUsrPrfUser)
                    .ThenInclude(a => a.UsrPrf)
                .Include(a => a.LkQlfctnAsnToUser)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnEventType)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnEnhncSrvc)
                .Include(a => a.LkQlfctnAsnToUser.QlfctnIpVer)
                .Where(a =>
                    a.UserId != 1 &&
                    a.RecStusId == 1 &&
                    a.MapUsrPrfUser.Any(b =>
                        b.RecStusId == 1 &&
                        b.UsrPrf.RecStusId == 1 &&
                        EF.Functions.Like(b.UsrPrf.UsrPrfNme, "%EActivator%")) &&
                    a.LkQlfctnAsnToUser.RecStusId == 1);

            switch (type)
            {
                case "SCHEDULE":
                    var startDate = Convert.ToDateTime(param.Start.Value);
                    var endDate = Convert.ToDateTime(param.Start.Value).Date.AddDays(_INTERVAL).Add(new TimeSpan(23, 55, 00));

                    List<int> appointmentTypeIds = new List<int>();
                    appointmentTypeIds.Add((int)AppointmentType.OnShift); // Add OnShift

                    // Get all possible appointment type
                    appointmentTypeIds.AddRange(GetCurrentAppointmentTypeIds(param.EventTypeId, param.EnhanceServiceId));

                    users = query
                        .Include(a => a.ApptRcurncData)
                            .ThenInclude(a => a.Appt)
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.ApptRcurncData
                                .Any(b =>
                                    b.Appt.AsnToUserIdListTxt != null &&
                                    b.Appt.EventId != null &&
                                    appointmentTypeIds.Contains(b.Appt.ApptTypeId) &&
                                    b.Appt.RcurncDesTxt != null &&
                                    b.Appt.RcurncCd == true &&
                                    b.StrtTmst.Date >= startDate.Date.AddDays(-1) &&
                                    b.EndTmst.Date <= endDate.Date
                                )
                        ).AsNoTracking();
                    break;

                case "SKILLED":
                    if (param.EventTypeId == (int)EventType.AD)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId)
                        ).AsNoTracking();
                    }
                    else if (param.EventTypeId == (int)EventType.MPLS)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.LkQlfctnAsnToUser.QlfctnIpVer.Any(b => b.IpVerId == param.IpVersionId)
                        ).AsNoTracking();
                    }
                    else if (param.EventTypeId == (int)EventType.SprintLink)
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId) &&
                            a.LkQlfctnAsnToUser.QlfctnIpVer.Any(b => b.IpVerId == param.IpVersionId)
                        // Possibly add Event sprint Link Activity Type   ??
                        ).AsNoTracking();
                    }
                    else
                    {
                        users = query
                        .Where(a =>
                            a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId) &&
                            a.LkQlfctnAsnToUser.QlfctnEnhncSrvc.Any(b => b.EnhncSrvcId == param.EnhanceServiceId)
                        ).AsNoTracking();
                    }
                    break;

                case "PRODUCT":
                    users = query
                        .Where(a => a.LkQlfctnAsnToUser.QlfctnEventType.Any(b => b.EventTypeId == param.EventTypeId)).AsNoTracking();
                    break;

                default:
                    break;
            }
            return users;
        }

        public LkUsrPrf GetFinalUserProfile(int userId, int eventTypeId, bool IS_EVENT = true)
        {
            IQueryable<MapUsrPrf> mapUsrPrf = _context.MapUsrPrf
                .Where(a => a.RecStusId == 1 &&
                    a.UserId == userId);

            int profileId = 0;
            int[] profileIds = { 0, 0, 0 };
            if (IS_EVENT)
            {
                // Updated by Sarah [20190510] - Workflow status hierarchy will be Activator - Reviewer - Member
                // Get Profile ID for Event Member, Event Reviewer, Event Activator in order
                profileIds = GetProfileIdByEventTypeId(eventTypeId);

                IQueryable<LkEventRule> query = _context.LkEventRule.Where(
                    a =>
                        a.EventTypeId == eventTypeId &&
                        mapUsrPrf.Any(b => b.UsrPrfId == a.UsrPrfId)
                    );

                // Updated by Sarah [20190510] - Workflow status hierarchy will be Activator - Reviewer - Member
                if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EActivator%")) || mapUsrPrf.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%SALESSUPPORTEActivator%") || EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%NCIEActivator%")) || query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%Fedline Preconfig Engineer%")))
                {
                    profileId = profileIds[0];
                }
                else if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EReviewer%")) || mapUsrPrf.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%SALESSUPPORTEReviewer%") || EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%NCIEReviewer%")) || query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%Fedline MNS Activator%")))
                {
                    profileId = profileIds[1];
                }
                else if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EMember%")) || mapUsrPrf.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%SALESSUPPORTEMember%") || EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%NCIEMember%")) || query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%Fedline MDS PM%")))
                {
                    profileId = profileIds[2];
                }
            }

            //return profileId;
            return _context.LkUsrPrf.Where(a => a.UsrPrfId == profileId).AsNoTracking().SingleOrDefault();
        }

        private List<int> GetCurrentAppointmentTypeIds(int eventTypeId, int enhanceServiceId)
        {
            List<int> result = new List<int>();

            // This is the basis for code
            //if (sAction.Length > 0)
            //{
            //    if (sAction.Equals("MDS") && sIsMDSFT.Equals("0"))
            //        lApptTypeID.Add(((int)ECalendarEventType.MDSScheduledShift));

            //    if (sAction.Equals("MDS") && sIsMDSFT.Equals("1"))
            //        lApptTypeID.Add(((int)ECalendarEventType.MDSScheduledShift));

            //    if (!sAction.Equals("MDS"))
            //    {
            //        if (iPrdID == ((int)EEventType.AD))
            //            lApptTypeID.Add(((int)ECalendarEventType.ADShift));

            //        if ((sAction.Equals("STANDARD")) || (sAction.Equals("VAS")))
            //            lApptTypeID.Add(((int)ECalendarEventType.MPLSShift));

            //        if (sAction.Equals("NGVN STANDARD"))
            //            lApptTypeID.Add(((int)ECalendarEventType.NGVNShift));

            //        if (sAction.Equals("SLNK STANDARD"))
            //            lApptTypeID.Add(((int)ECalendarEventType.SprintLinkShift));

            //        /*if (sAction.Equals("UCaaS"))
            //            lApptTypeID.Add(((int)ECalendarEventType.UCaaSShift));*/
            //    }
            //}
            //else
            //{
            //    if (iPrdID == 5)
            //        lApptTypeID.Add(((int)ECalendarEventType.MDSScheduledShift));
            //    else if (iPrdID == 4)
            //        lApptTypeID.Add(((int)ECalendarEventType.SprintLinkShift));
            //    else if (iPrdID == 3)
            //        lApptTypeID.Add(((int)ECalendarEventType.MPLSShift));
            //    else if (iPrdID == 2)
            //        lApptTypeID.Add(((int)ECalendarEventType.NGVNShift));
            //    else if (iPrdID == 1)
            //        lApptTypeID.Add(((int)ECalendarEventType.ADShift));
            //    //else if (iPrdID == 19)
            //    //lApptTypeID.Add(((int)ECalendarEventType.UCaaSShift));
            //}

            if (enhanceServiceId != 0)
            {
                if (enhanceServiceId != (int)EnhanceService.MDS) // NOT MDS
                {
                    if (eventTypeId == (int)EventType.AD) // AD Event
                    {
                        result.Add((int)AppointmentType.ADShift);
                    }
                    else if (eventTypeId == (int)EventType.MPLS) // MPLS Event
                    {
                        result.Add((int)AppointmentType.MPLSShift);
                    }
                    else if (eventTypeId == (int)EventType.SIPT) // SIPT Event
                    {
                        result.Add((int)AppointmentType.SIPT);
                    }
                    else if (eventTypeId == (int)EventType.SprintLink) // SprintLink Event
                    {
                        result.Add((int)AppointmentType.SprintLinkShift);
                    }
                    else if (eventTypeId == (int)EventType.NGVN) // NGVN Event
                    {
                        result.Add((int)AppointmentType.NGVNShift);
                    }
                }
                else
                {
                    result.Add((int)AppointmentType.MDSScheduledShift);
                }
            }
            else
            {
                if (eventTypeId == (int)EventType.AD) // AD Event
                {
                    result.Add((int)AppointmentType.ADShift);
                }
                else if (eventTypeId == (int)EventType.MPLS) // MPLS Event
                {
                    result.Add((int)AppointmentType.MPLSShift);
                }
                else if (eventTypeId == (int)EventType.SIPT) // SIPT Event
                {
                    result.Add((int)AppointmentType.SIPT);
                }
                else if (eventTypeId == (int)EventType.SprintLink) // SprintLink Event
                {
                    result.Add((int)AppointmentType.SprintLinkShift);
                }
                else if (eventTypeId == (int)EventType.NGVN) // NGVN Event
                {
                    result.Add((int)AppointmentType.NGVNShift);
                }
                else if (eventTypeId == (int)EventType.MDS) // NGVN Event
                {
                    result.Add((int)AppointmentType.MDSScheduledShift);
                }
            }

            return result;
        }

        private int[] GetProfileIdByEventTypeId(int eventTypeId)
        {
            // Updated by Sarah [20190510] - Workflow status hierarchy will be Activator - Reviewer - Member
            int[] profileIds = new int[3] { 0, 0, 0 }; // 1st = Activator, 2nd = Reviewer, 3rd = Member

            if (eventTypeId == (int)EventType.AD || eventTypeId == (int)EventType.SprintLink
                || eventTypeId == (int)EventType.MPLS || eventTypeId == (int)EventType.NGVN)
            {
                //profileIds = new int[3] { 33, 34, 32 };
                profileIds = new int[3] { 32, 34, 33 };
            }
            else if (eventTypeId == (int)EventType.SIPT || eventTypeId == (int)EventType.UCaaS)
            {
                //profileIds = new int[3] { 173, 174, 172 };
                profileIds = new int[3] { 172, 174, 173 };
            }
            else if (eventTypeId == (int)EventType.MDS)
            {
                profileIds = new int[3] { 130, 132, 131 };
            }
            else if (eventTypeId == (int)EventType.Fedline)
            {
                profileIds = new int[3] { 215, 216, 218 };
            }
            return profileIds;
        }

        public List<int> GetActiveProfileIdByEventTypeId(int userId, int eventTypeId)
        {
            List<int> activeProfileId = new List<int>();
            int[] profileIds = { 0, 0, 0 };

            profileIds = GetProfileIdByEventTypeId(eventTypeId);

            IQueryable<MapUsrPrf> mapUsrPrf = _context.MapUsrPrf
                .Where(a => a.RecStusId == 1 &&
                    a.UserId == userId);

            IQueryable<LkEventRule> query = _context.LkEventRule
                    .Where(a => a.EventTypeId == eventTypeId &&
                        mapUsrPrf.Any(b => b.UsrPrfId == a.UsrPrfId));

            if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EActivator%")) || mapUsrPrf.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%SALESSUPPORTEActivator%") || EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%NCIEActivator%")) || query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%Fedline Preconfig Engineer%")))
            {
                activeProfileId.Add(profileIds[0]);
            }
            if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EReviewer%")) || mapUsrPrf.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%SALESSUPPORTEReviewer%") || EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%NCIEReviewer%")) || query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%Fedline MNS Activator%")))
            {
                activeProfileId.Add(profileIds[1]);
            }
            if (query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%EMember%")) || mapUsrPrf.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%SALESSUPPORTEMember%") || EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%NCIEMember%")) || query.Any(a => EF.Functions.Like(a.UsrPrf.UsrPrfNme, "%Fedline MDS PM%")))
            {
                activeProfileId.Add(profileIds[2]);
            }

            return activeProfileId;
        }

        public DataTable GetAssignSkilledMembersforSubmitType(string sAction, string sIPVersion, string sSLNKEventType)
        {
            var x = (from ql in _context.LkQlfctn
                     join qe in _context.QlfctnEnhncSrvc on ql.QlfctnId equals qe.QlfctnId
                     join le in _context.LkEnhncSrvc on qe.EnhncSrvcId equals le.EnhncSrvcId
                     join qi in _context.QlfctnIpVer on ql.QlfctnId equals qi.QlfctnId
                     into joinip
                     from qi in joinip.DefaultIfEmpty()
                     join lu in _context.LkUser on ql.AsnToUserId equals lu.UserId
                     select new { lu, le, qi });

            if (sAction.Equals("STANDARD") || sAction.Equals("VAS") || sAction.Equals("SLNK STANDARD"))
            {
                x = x.Where(a => a.le.EnhncSrvcNme == sAction).AsNoTracking();
                x = x.Where(a => a.qi.IpVerId == Convert.ToByte(sIPVersion)).AsNoTracking();
            }
            else
                x = x.Where(a => a.le.EnhncSrvcNme == sAction).AsNoTracking();

            var y = x.Select(i => new { i.lu.UserId, i.lu.UserAdid, i.lu.DsplNme, i.lu.FullNme }).Distinct().AsNoTracking();

            return LinqHelper.CopyToDataTable(y, null, null);
        }

        public DataTable getUsersByProduct(int iProductID)
        {
            var up = (from u in _context.LkUser
                      join q in _context.LkQlfctn on u.UserId equals q.AsnToUserId
                      join qe in _context.QlfctnEventType on q.QlfctnId equals qe.QlfctnId
                      where ((q.RecStusId == (byte)ERecStatus.Active) && (u.RecStusId == (byte)ERecStatus.Active)) && (qe.EventTypeId == iProductID)
                            && (u.UserId != 1)
                      select new { u.UserId, u.UserAdid, u.FullNme, u.DsplNme }).Distinct().AsNoTracking();

            return LinqHelper.CopyToDataTable(up, null, null);
        }

        public DataTable getUsersByPrdProfile(DataTable dtPrd, DataTable dtProfile)
        {
            var q = (from up in dtPrd.AsEnumerable()
                     join ur in dtProfile.AsEnumerable() on Convert.ToInt32(up.Field<Object>("USER_ID").ToString()) equals Convert.ToInt32(ur.Field<Object>("USER_ID").ToString())
                     select new
                     {
                         USER_ID = Convert.ToString(up.Field<Object>("USER_ID").ToString()),
                         USER_ADID = Convert.ToString(up.Field<Object>("USER_ADID").ToString()),
                         FULL_NME = Convert.ToString(up.Field<Object>("FULL_NME").ToString()),
                         DSPL_NME = Convert.ToString(up.Field<Object>("DSPL_NME").ToString())
                     }).Distinct();

            return LinqHelper.CopyToDataTable(q, null, null);
        }

        public DataTable GetUsersByProfile(string profileName)
        {
            var users = (from lu in _context.LkUser
                         join mup in _context.MapUsrPrf on lu.UserId equals mup.UserId
                         join lup in _context.LkUsrPrf on mup.UsrPrfId equals lup.UsrPrfId
                         where lu.RecStusId == 1 && lu.UserId != 1
                         && lup.RecStusId == 1 && lup.UsrPrfNme.Contains(profileName)
                         && mup.RecStusId == 1
                         select lu).Distinct().OrderBy(i => i.DsplNme).AsNoTracking();

            return LinqHelper.CopyToDataTable(users, null, null);
        }

        public DataSet GetWFCollection(ref DataTable dtSkilledMem, DateTime dcStart, DateTime dcEnd, string sAction, string sIsMDSFT, int iPrdID, bool _OptOutFlag)
        {
            _dtApptRec.Clear();
            _StartRange = dcStart;
            _EndRange = dcEnd;

            List<int> lApptTypeID = new List<int>();

            lApptTypeID.Add(((int)CalendarEventType.OnShift));

            if (sAction.Length > 0)
            {
                if (sAction.Equals("MDS") && sIsMDSFT.Equals("0"))
                    lApptTypeID.Add(((int)CalendarEventType.MDSScheduledShift));

                if (sAction.Equals("MDS") && sIsMDSFT.Equals("1"))
                    lApptTypeID.Add(((int)CalendarEventType.MDSScheduledShift));

                if (!sAction.Equals("MDS"))
                {
                    if (iPrdID == ((int)EventType.AD))
                        lApptTypeID.Add(((int)CalendarEventType.ADShift));

                    if ((sAction.Equals("STANDARD")) || (sAction.Equals("VAS")))
                        lApptTypeID.Add(((int)CalendarEventType.MPLSShift));

                    if (sAction.Equals("NGVN STANDARD"))
                        lApptTypeID.Add(((int)CalendarEventType.NGVNShift));

                    if (sAction.Equals("SLNK STANDARD"))
                        lApptTypeID.Add(((int)CalendarEventType.SprintLinkShift));

                    /*if (sAction.Equals("UCaaS"))
                        lApptTypeID.Add(((int)ECalendarEventType.UCaaSShift));*/
                }
            }
            else
            {
                if (iPrdID == 5)
                    lApptTypeID.Add(((int)CalendarEventType.MDSScheduledShift));
                else if (iPrdID == 4)
                    lApptTypeID.Add(((int)CalendarEventType.SprintLinkShift));
                else if (iPrdID == 3)
                    lApptTypeID.Add(((int)CalendarEventType.MPLSShift));
                else if (iPrdID == 2)
                    lApptTypeID.Add(((int)CalendarEventType.NGVNShift));
                else if (iPrdID == 1)
                    lApptTypeID.Add(((int)CalendarEventType.ADShift));
                //else if (iPrdID == 19)
                //lApptTypeID.Add(((int)ECalendarEventType.UCaaSShift));
            }

            DataSet ds = new DataSet();

            DataTable dtWFCollection = new DataTable("WFCollection");
            dtWFCollection.Columns.Add("StartDt", typeof(DateTime));
            dtWFCollection.Columns.Add("EndDt", typeof(DateTime));
            dtWFCollection.Columns.Add("USER_ID");
            dtWFCollection.Columns.Add("USER_ADID");
            dtWFCollection.Columns.Add("DSPL_NME");
            dtWFCollection.Columns.Add("FULL_NME");

            DataTable dtScheduleCollection = new DataTable("ScheduleCollection");
            dtScheduleCollection.Columns.Add("USER_ID");
            dtScheduleCollection.Columns.Add("USER_ADID");
            dtScheduleCollection.Columns.Add("DSPL_NME");
            dtScheduleCollection.Columns.Add("FULL_NME");

            // Get all Recurring Appointments into temp datatable
            DataTable dt = new DataTable();

            var adev = (from apd in _context.ApptRcurncData
                        join apt in _context.Appt on apd.ApptId equals apt.ApptId
                        where (((apt.AsnToUserIdListTxt != null) && (apt.EventId == null) && (lApptTypeID.Contains(apt.ApptTypeId)) && (apt.RcurncDesTxt != null) && (apt.RcurncCd == true))
                         && ((_StartRange.Date.AddDays(-1) <= apd.StrtTmst.Date) && (apd.EndTmst.Date <= _EndRange.Date)))
                        select new
                        {
                            APPT_ID = apt.ApptId,
                            StartDt = apd.StrtTmst,
                            EndDt = apd.EndTmst,
                            ResourceID = apd.AsnUserId,
                            CREAT_DT = apt.CreatDt,
                            MODFD_DT = apt.ModfdDt
                        }).AsNoTracking();

            DataTable dtadev = LinqHelper.CopyToDataTable(adev, null, null);

            var filar = (from ap in dtadev.AsEnumerable()
                         join sk in dtSkilledMem.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sk.Field<Object>("USER_ID").ToString())
                         where ((_StartRange.Date.AddDays(-1) <= DateTime.Parse(ap.Field<Object>("StartDt").ToString()).Date) && (DateTime.Parse(ap.Field<Object>("EndDt").ToString()).Date <= _EndRange.Date))
                         select new
                         {
                             APPT_ID = Convert.ToString(ap.Field<Object>("APPT_ID").ToString()),
                             StartDt = Convert.ToDateTime(ap.Field<Object>("StartDt").ToString()),
                             EndDt = Convert.ToDateTime(ap.Field<Object>("EndDt").ToString()),
                             ResourceID = Convert.ToString(ap.Field<Object>("ResourceID").ToString())
                         }
                    ).Distinct();

            _dtApptRec = LinqHelper.CopyToDataTable(filar, null, null);

            var GrpSt = (from ar1 in _dtApptRec.AsEnumerable()
                         join ar2 in _dtApptRec.AsEnumerable() on ar1.Field<Object>("ResourceID").ToString() equals ar2.Field<Object>("ResourceID").ToString()
                         where DateTime.Parse(ar1.Field<Object>("StartDt").ToString()) == DateTime.Parse(ar2.Field<Object>("EndDt").ToString())
                         select new { ar1, ar2 })
                     .ToList();

            var GrpEt = (from ar1 in _dtApptRec.AsEnumerable()
                         join ar2 in _dtApptRec.AsEnumerable() on ar1.Field<Object>("ResourceID").ToString() equals ar2.Field<Object>("ResourceID").ToString()
                         where DateTime.Parse(ar1.Field<Object>("EndDt").ToString()) == DateTime.Parse(ar2.Field<Object>("StartDt").ToString())
                         select new { ar1, ar2 })
                         .ToList();

            if (((GrpSt != null) && (GrpSt.Count > 0))
                || ((GrpEt != null) && (GrpEt.Count > 0)))
                ConsolidateAdjacentSlots(ref _dtApptRec);

            // Get all Non-Recurring Appointments into temp datatable
            var x = (from ra in _context.Appt
                     where ((ra.AsnToUserIdListTxt != null) && (ra.RcurncCd == false) && (ra.EventId == null))
                       && (lApptTypeID.Contains(ra.ApptTypeId))
                       && (_StartRange.Date.AddDays(-1) <= ra.StrtTmst.Date)
                     select new
                     {
                         APPT_ID = ra.ApptId,
                         StartDt = ra.StrtTmst,
                         EndDt = ra.EndTmst,
                         ResourceID = XmlElementExtensions.GetAttribute<List<string>>(XElement.Parse(ra.AsnToUserIdListTxt).Descendants("ResourceId"), "Value", new List<string>()),
                         CREAT_DT = ra.CreatDt,
                         MODFD_DT = ra.ModfdDt
                     }).AsNoTracking();

            DataTable dtx = LinqHelper.CopyToDataTable(x, null, null);

            var filnonrecar = (from ap in dtx.AsEnumerable()
                               join sk in dtSkilledMem.AsEnumerable() on Convert.ToString(ap.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sk.Field<Object>("USER_ID").ToString())
                               select new
                               {
                                   APPT_ID = Convert.ToString(ap.Field<Object>("APPT_ID").ToString()),
                                   StartDt = Convert.ToString(ap.Field<Object>("StartDt").ToString()),
                                   EndDt = Convert.ToString(ap.Field<Object>("EndDt").ToString()),
                                   ResourceID = Convert.ToString(ap.Field<Object>("ResourceID").ToString())
                               }
                    ).Distinct();

            dt = LinqHelper.CopyToDataTable(filnonrecar, null, null);

            _dtApptRec.AsEnumerable().CopyToDataTable(dt, LoadOption.PreserveChanges);

            var a = (from ci in dt.AsEnumerable()
                     join sk in dtSkilledMem.AsEnumerable() on Convert.ToString(ci.Field<Object>("ResourceID").ToString()) equals Convert.ToString(sk.Field<Object>("USER_ID").ToString())
                     where ((dcStart.Date <= DateTime.Parse(ci.Field<Object>("StartDt").ToString()).Date)
                          && (DateTime.Parse(ci.Field<Object>("StartDt").ToString()).Date <= dcEnd.Date)
                          && (DateTime.Parse(ci.Field<Object>("EndDt").ToString()).Date <= dcEnd.Date)
                          && (DateTime.Parse(ci.Field<Object>("StartDt").ToString()).Date <= DateTime.Parse(ci.Field<Object>("EndDt").ToString()).Date))
                     select new
                     {
                         USER_ID = Convert.ToString(sk.Field<Object>("USER_ID").ToString()),
                         USER_ADID = Convert.ToString(sk.Field<Object>("USER_ADID").ToString()),
                         DSPL_NME = Convert.ToString(sk.Field<Object>("DSPL_NME").ToString()),
                         FULL_NME = Convert.ToString(sk.Field<Object>("FULL_NME").ToString()),
                         StartDt = Convert.ToString(ci.Field<Object>("StartDt").ToString()),
                         EndDt = Convert.ToString(ci.Field<Object>("EndDt").ToString())
                     }).Distinct();

            DataTable dtWF = LinqHelper.CopyToDataTable(a, null, null);

            if (dcStart <= dcEnd)
            {
                foreach (DataRow dr in dtWF.Rows)
                {
                    dtWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(dr["StartDt"]), Convert.ToDateTime(dr["EndDt"]), dr["USER_ID"], dr["USER_ADID"], dr["DSPL_NME"], dr["FULL_NME"] });
                    dtScheduleCollection.Rows.Add(new Object[] { dr["USER_ID"], dr["USER_ADID"], dr["DSPL_NME"], dr["FULL_NME"] });
                }
            }

            var c1 = (from fc in dtWFCollection.AsEnumerable()
                      select new
                      {
                          StartDt = DateTime.Parse(fc.Field<Object>("StartDt").ToString()),
                          EndDt = DateTime.Parse(fc.Field<Object>("EndDt").ToString()),
                          USER_ID = Int32.Parse(fc.Field<Object>("USER_ID").ToString()),
                          USER_ADID = Convert.ToString(fc.Field<Object>("USER_ADID").ToString()),
                          DSPL_NME = Convert.ToString(fc.Field<Object>("DSPL_NME").ToString()),
                          FULL_NME = Convert.ToString(fc.Field<Object>("FULL_NME").ToString())
                      }).Distinct().OrderBy(w => w.USER_ID).ThenBy(v => v.StartDt);

            dtWFCollection = LinqHelper.CopyToDataTable(c1, null, null);

            var y = (from fc in dtScheduleCollection.AsEnumerable()
                     select new
                     {
                         USER_ID = Convert.ToString(fc.Field<Object>("USER_ID").ToString()),
                         USER_ADID = Convert.ToString(fc.Field<Object>("USER_ADID").ToString()),
                         DSPL_NME = Convert.ToString(fc.Field<Object>("DSPL_NME").ToString()),
                         FULL_NME = Convert.ToString(fc.Field<Object>("FULL_NME").ToString())
                     }).Distinct();

            dtScheduleCollection = LinqHelper.CopyToDataTable(y, null, null);

            ds.Tables.Add(dtWFCollection);
            ds.Tables.Add(dtScheduleCollection);

            return ds;
        }

        protected void ConsolidateAdjacentSlots(ref DataTable dtIn)
        {
            DataTable dtTempWFCollection = new DataTable("dtTempWF");
            DataColumn[] twfkeys = new DataColumn[4];

            twfkeys[0] = new DataColumn("APPT_ID");
            twfkeys[1] = new DataColumn("StartDt", typeof(DateTime));
            twfkeys[2] = new DataColumn("EndDt", typeof(DateTime));
            twfkeys[3] = new DataColumn("ResourceID");
            dtTempWFCollection.Columns.Add(twfkeys[0]);
            dtTempWFCollection.Columns.Add(twfkeys[1]);
            dtTempWFCollection.Columns.Add(twfkeys[2]);
            dtTempWFCollection.Columns.Add(twfkeys[3]);
            dtTempWFCollection.PrimaryKey = twfkeys;

            DataTable dtWFCol = new DataTable("WFbusyCollection");
            DataColumn[] wfkeys = new DataColumn[4];

            wfkeys[0] = new DataColumn("APPT_ID");
            wfkeys[1] = new DataColumn("StartDt", typeof(DateTime));
            wfkeys[2] = new DataColumn("EndDt", typeof(DateTime));
            wfkeys[3] = new DataColumn("ResourceID");
            dtWFCol.Columns.Add(wfkeys[0]);
            dtWFCol.Columns.Add(wfkeys[1]);
            dtWFCol.Columns.Add(wfkeys[2]);
            dtWFCol.Columns.Add(wfkeys[3]);
            dtWFCol.PrimaryKey = wfkeys;

            foreach (DataRow dr in dtIn.Rows)
            {
                DataRow[] drPrefix = dtWFCol.Select("EndDt = #" + dr[1].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'");
                DataRow[] drSuffix = dtWFCol.Select("StartDt = #" + dr[2].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'");

                if (drPrefix.Length == 1)
                {
                    if (dtTempWFCollection.Select("StartDt = #" + drPrefix[0][1].ToString() + "# AND EndDt = #" + drPrefix[0][2].ToString() + "# AND ResourceID = '" + drPrefix[0][3].ToString() + "'").Length <= 0)
                        dtTempWFCollection.Rows.Add(new Object[] { drPrefix[0][0], Convert.ToDateTime(drPrefix[0][1]), Convert.ToDateTime(drPrefix[0][2]), drPrefix[0][3] });
                    if (dtWFCol.Select("StartDt = #" + drPrefix[0]["StartDt"].ToString() + "# AND EndDt = #" + dr[2].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'").Length <= 0)
                        dtWFCol.Rows.Add(new Object[] { dr[0], Convert.ToDateTime(drPrefix[0]["StartDt"].ToString()), Convert.ToDateTime(dr[2]), dr[3] });
                }
                else if (drSuffix.Length == 1)
                {
                    if (dtTempWFCollection.Select("StartDt = #" + drSuffix[0][1].ToString() + "# AND EndDt = #" + drSuffix[0][2].ToString() + "# AND ResourceID = '" + drSuffix[0][3].ToString() + "'").Length <= 0)
                        dtTempWFCollection.Rows.Add(new Object[] { drSuffix[0][0], Convert.ToDateTime(drSuffix[0][1]), Convert.ToDateTime(drSuffix[0][2]), drSuffix[0][3] });
                    if (dtWFCol.Select("StartDt = #" + dr[1].ToString() + "# AND EndDt = #" + drSuffix[0]["EndDt"].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'").Length <= 0)
                        dtWFCol.Rows.Add(new Object[] { dr[0], Convert.ToDateTime(dr[1]), Convert.ToDateTime(drSuffix[0]["EndDt"]), dr[3] });
                }
                else if (dtWFCol.Select("StartDt = #" + dr[1].ToString() + "# AND EndDt = #" + dr[2].ToString() + "# AND ResourceID = '" + dr[3].ToString() + "'").Length <= 0)
                    dtWFCol.Rows.Add(new Object[] { dr[0], Convert.ToDateTime(dr[1]), Convert.ToDateTime(dr[2]), dr[3] });
            }

            if ((dtTempWFCollection != null) && (dtTempWFCollection.Rows.Count > 0))
            {
                foreach (DataRow dr in dtTempWFCollection.Rows)
                {
                    dtWFCol.Rows.Remove(dtWFCol.Rows.Find(new Object[] { dr["APPT_ID"], dr["StartDt"], dr["EndDt"], dr["ResourceID"] }));
                }
            }

            if ((dtWFCol != null) && (dtWFCol.Rows.Count > 0))
            {
                _dtApptRec.Clear();
                foreach (DataRow dr in dtWFCol.Rows)
                {
                    _dtApptRec.Rows.Add(new Object[] { dr[0], Convert.ToDateTime(dr[1]), Convert.ToDateTime(dr[2]), dr[3] });
                }
            }
            dtTempWFCollection.Clear();
        }

        public bool UserActiveWithProfileName(int userId, string profileName)
        {
            var users = (from lu in _context.LkUser
                         join mup in _context.MapUsrPrf on lu.UserId equals mup.UserId
                         join lup in _context.LkUsrPrf on mup.UsrPrfId equals lup.UsrPrfId
                         where lu.RecStusId == 1 && lu.UserId == userId
                         && lup.RecStusId == 1 && lup.UsrPrfDes.Contains(profileName)
                         && mup.RecStusId == 1
                         select lu).Distinct().OrderBy(i => i.DsplNme).AsNoTracking();

            if (users != null && users.Count() > 0)
                return true;

            return false;
        }

        public DataTable GetConferenceBridgeInfo()
        {
            DataTable dt = new DataTable();
            var confBridge = (from cb in _context.LkCnfrcBrdg
                              where cb.RecStusId == (byte)ERecStatus.Active
                              select new
                              {
                                  cb.CnfrcBrdgNbr,
                                  cb.CnfrcPinNbr,
                                  cb.OnlineMeetingAdr,
                                  cb.CreatByUserId
                              }).AsNoTracking().ToList();
            if (confBridge != null)
                dt = LinqHelper.CopyToDataTable(confBridge, null, null);

            return dt;
        }

        public DataTable GetSDEViewUsers()
        {
            DataTable dt = new DataTable();
            List<int> commonIDs = new List<int> { 1, 3, 11 };

            var q = (from u in _context.LkUser
                     join mup in _context.MapUsrPrf on u.UserId equals mup.UserId
                     join up in _context.LkUsrPrf on mup.UsrPrfId equals up.UsrPrfId
                     where up.UsrPrfDes.Contains("System Security")
                     && u.RecStusId == (byte)ERecStatus.Active
                     && up.RecStusId == (byte)ERecStatus.Active
                     && !commonIDs.Contains(u.UserId)
                     select new
                     {
                         u.UserId,
                         u.DsplNme
                     }).Distinct().OrderBy(i => i.DsplNme).AsNoTracking();


            if (q != null)
            {
                dt = LinqHelper.CopyToDataTable(q, null, null);
            }
            return dt;

        }

        public int UpdateLegacyUserADID(string user_adid, string old_user_adid, string ntid_email)
        {
            int toReturn = -1;

            List<SqlParameter> pc = new List<SqlParameter>
            {
                new SqlParameter("@p0", SqlDbType.VarChar, 10),
                new SqlParameter("@p1", SqlDbType.VarChar, 10),
                new SqlParameter("@p2", SqlDbType.VarChar, 100),
            };
            pc[0].Value = user_adid;
            pc[1].Value = old_user_adid;
            pc[2].Value = ntid_email;
            toReturn = _context.Database.ExecuteSqlCommand("dbo.handleADIDChanges @p0, @p1, @p2", pc.ToArray());

            return toReturn;
        }
    }
}
