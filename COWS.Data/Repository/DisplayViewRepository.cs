﻿using COWS.Data.Interface;
using COWS.Entities.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Repository
{
    public class DisplayViewRepository : IDisplayViewRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly ICommonRepository _commonRepo;
        private readonly IConfiguration _config;

        public DisplayViewRepository(COWSAdminDBContext context, ICommonRepository commonRepo,
            IConfiguration config)
        {
            _context = context;
            _commonRepo = commonRepo;
            _config = config;
        }

        public IQueryable<DsplVw> Find(Expression<Func<DsplVw, bool>> predicate)
        {
            return _context.DsplVw
                            .Where(predicate);
        }

        public DataTable GetViewDetails(int vwID, int siteCntntID, int userID, string userAdid)
        {
            var userCsg = _commonRepo.GetCSGLevelAdId(userAdid);

            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            command = new SqlCommand("[web].[getViewDetails]", connection);
            command.CommandTimeout = _context.commandTimeout;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@VIEWID", vwID);
            command.Parameters.AddWithValue("@SITE_CNTNT_ID", siteCntntID);
            command.Parameters.AddWithValue("@USER_ID", userID);
            command.Parameters.AddWithValue("@CSGLvlId", (byte)userCsg);

            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            //if ((dt != null && dt.Rows.Count == 1) && (byte.Parse(dt.Rows[0]["CSG Level"].ToString()) > 0))
            //    _commonRepo.LogWebActivity(((byte)WebActyType.RedesignSearch), dt.Rows[0]["Redesign Number"].ToString(), userAdid, (byte)userCsg, byte.Parse(dt.Rows[0]["CSG Level"].ToString()));

            return dt;
        }
    }
}
