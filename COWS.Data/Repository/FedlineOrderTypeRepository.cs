﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Repository
{
    public class FedlineOrderTypeRepository : IFedlineOrderTypeRepository
    {
        private readonly COWSAdminDBContext _context;
        private IMemoryCache _cache;

        public FedlineOrderTypeRepository(COWSAdminDBContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public IQueryable<LkFedlineOrdrType> Find(Expression<Func<LkFedlineOrdrType, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkFedlineOrdrType, out List<LkFedlineOrdrType> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();
        }

        public IEnumerable<LkFedlineOrdrType> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkFedlineOrdrType, out List<LkFedlineOrdrType> list))
            {
                list = _context.LkFedlineOrdrType
                            .AsNoTracking()
                            .ToList();
                _cache.Set(CacheKeys.LkFedlineOrdrType, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkFedlineOrdrType GetById(int id)
        {
            throw new NotImplementedException();
        }

        public LkFedlineOrdrType Create(LkFedlineOrdrType entity)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, LkFedlineOrdrType entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkFedlineOrdrType);
            return _context.SaveChanges();
        }
    }
}