﻿using COWS.Entities.QueryModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace COWS.Data
{
    public class COWSReportingDbContext : DbContext
    {
        private IConfiguration _configuration;
        private int _commandTimeout;

        public int commandTimeout
        {
            get { return _commandTimeout; }

        }

        public COWSReportingDbContext(DbContextOptions<COWSReportingDbContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
            _commandTimeout = (_configuration.GetSection("ConnectionStrings:CommandTimeout").Exists()) ? Int32.Parse(_configuration.GetSection("ConnectionStrings:CommandTimeout").Value) : 300;
            Database.SetCommandTimeout(_commandTimeout);
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Query<Report>();

            builder.Query<CannedFile>();

            base.OnModelCreating(builder);
        }
    }
}
