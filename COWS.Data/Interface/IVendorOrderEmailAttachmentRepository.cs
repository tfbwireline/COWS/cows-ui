﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVendorOrderEmailAttachmentRepository : IRepository<VndrOrdrEmailAtchmt>
    {
        IQueryable<VndrOrdrEmailAtchmt> Find(Expression<Func<VndrOrdrEmailAtchmt, bool>> predicate);
        bool IsAttachmentDistinct(int vendorOrderEmailId, string fileName);
    }
}
