﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IWholesalePartnerRepository : IRepository<LkWhlslPtnr>
    {
        IQueryable<LkWhlslPtnr> Find(Expression<Func<LkWhlslPtnr, bool>> predicate);
    }
}