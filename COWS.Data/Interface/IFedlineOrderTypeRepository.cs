﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IFedlineOrderTypeRepository : IRepository<LkFedlineOrdrType>
    {
        IQueryable<LkFedlineOrdrType> Find(Expression<Func<LkFedlineOrdrType, bool>> predicate);
    }
}