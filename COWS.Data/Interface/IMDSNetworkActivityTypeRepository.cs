﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSNetworkActivityTypeRepository : IRepository<LkMdsNtwkActyType>
    {
        IQueryable<LkMdsNtwkActyType> Find(Expression<Func<LkMdsNtwkActyType, bool>> predicate);
    }
}