﻿using COWS.Entities.Models;
using System;
using System.Data;
using System.Threading.Tasks;

namespace COWS.Data.Interface
{
    public interface IDCPERepository
    {
        DataTable GetFTNData(int orderID, int usrPrfId, string adid = null);
        Task<DataTable> GetCPEOrderInfo_V5U(int orderId, string userAdid, int csgLvlId);
        DataTable GetTasksList();
        DataTable GetJeopardyCodeList();
        Task<DataTable> GetCpeAcctCntct_V5U(int orderId, int csgLvlId);
        DataTable GetAssignedTechList();
        DataTable GetTechAssignment_V5U(int orderID);
        Task<DataTable> GetCPELineItems_V5U(int orderId);
        DataTable GetEquipOnlyInfo(int orderID);
        Task<DataTable> GetRequisitionNumber_V5U(int orderId);
        DataTable GetReqHeaderQueueByOrderID(int orderID);
        Task<DataTable> GetCpeReqHeaderAcctCodes_V5U(int orderId);
        DataTable GetCPEPidByOrderID(int orderID);
        Task<DataTable> GetCPEReqLineItems_V5U(int orderId);
        DataTable GetVendorsList();
        DataTable GetDomesticClliCode();
        DataTable GetInternationalClliCode();
        Task<DataTable> GetCpeCmplLineItems_V5U(int orderId);
        int InsertODIEReq_V5U(int orderId);
        int InsertDmstcStndMatReq_V5U(int orderId);
        //Task<int> InsertSSTATReq(int orderId, int msgId, string ftn);
        int InsertSSTATReq(int orderId, int msgId, string ftn);
        int InsertEmailRequest_V5U(int orderID, string deviceID, int emailReqTypeID, int statusID, string fTN, string primaryEmail, string secondaryEmail, string emailNote, string emailJeopardyNote);
        int InsertRTS_V5U(int orderId, string deviceID, string jeopardyCode, string note);
        int UpdateCPEOrdr_V5U(int orderID, int modType, string adid, string jCode, string comments, string clli);
        int UpdateDmstcWFM_V5U(int orderID, string adid, string assignerADID, string comments);
        int ProcessCPEMve_V5U(int orderID, string adid, string taskName, string comments);
        int InsertReqLineItem_V5U(PsReqLineItmQueue lineItemToAdd);
        int InsertRequisitionHeader_V5U(PsReqHdrQueue reqHdrQueue);
        int InsertEqpRecCmpl_V5U(string adid, int orderID);
        int InsertEqpReceipt_V5U(int fsaCpeLineItemId, int partialQty, string action, string adid, int orderId);
        int InsertTechAssignment_V5U(int orderId, string tech, DateTime dispatchTime, int eventID, string assignedADID);
        int InsertOrdrCmplPartial_V5U(int fsaCpeLineItemId, DateTime completeDate);
        int InsertOrdrCmplAll_V5U(int orderID, string deviceID, string note, string tech, DateTime? inRouteDate, DateTime? onSiteDate, DateTime? completeDate);
        bool IsNIDDevice(int orderID, string deviceID);
        int UpdateNIDSerialNbr(int orderID, string ftn, string deviceID, string nidSerialNbr, int userId);
        int UpdateNuaCkt(int orderID, string nuaCkt, string userId);
        Task<DataTable> GetShippingInstr(int orderId);
        bool IsNIDAlreadyUsed(int orderId, string nidSerialNbr);
    }
}