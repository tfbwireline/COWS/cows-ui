﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IAsrRepository : IRepository<Asr>
    {
        IQueryable<Asr> Find(Expression<Func<Asr, bool>> predicate);
    }
}