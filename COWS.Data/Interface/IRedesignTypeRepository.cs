﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IRedesignTypeRepository : IRepository<LkRedsgnType>
    {
        IQueryable<LkRedsgnType> Find(Expression<Func<LkRedsgnType, bool>> predicate);
    }
}