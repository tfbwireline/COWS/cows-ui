﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVasTypeRepository : IRepository<LkVasType>
    {
        IQueryable<LkVasType> Find(Expression<Func<LkVasType, bool>> predicate);
    }
}