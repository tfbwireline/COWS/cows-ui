﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IDeviceManufacturerRepository : IRepository<LkDevManf>
    {
        IQueryable<LkDevManf> Find(Expression<Func<LkDevManf, bool>> predicate);
    }
}