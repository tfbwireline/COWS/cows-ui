﻿using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        T Create(T entity);

        void Update(int id, T entity);

        void Delete(int id);

        int SaveAll();
    }
}