﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSEventSiteSrvcRepository : IRepository<MdsEventSiteSrvc>
    {
        //IQueryable<MdsEventSiteSrvc> Find(Expression<Func<MdsEventSiteSrvc, bool>> predicate);
        IEnumerable<GetMdsEventSiteSrvc> GetMDSEventSiteSrvcByEventId(int eventId);
        IQueryable<MdsEventSiteSrvc> Find(Expression<Func<MdsEventSiteSrvc, bool>> predicate);
        void UpdateRange(int id, IEnumerable<MdsEventSiteSrvc> entity);
        void Create(IEnumerable<MdsEventSiteSrvc> entity);
        void Delete(Expression<Func<MdsEventSiteSrvc, bool>> predicate);
    }
}