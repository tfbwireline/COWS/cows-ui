﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventRuleRepository : IRepository<LkEventRule>
    {
        IQueryable<LkEventRule> Find(Expression<Func<LkEventRule, bool>> predicate);

        LkEventRule GetEventRule(byte eventStatusId, byte workflowStatusId, int userId, int eventType, string orderTypeCd = null);
        LkEventRule GetEventRule2(byte eventStatusId, byte workflowStatusId, int userId, int eventType, string orderTypeCd = null, short? failReasId = null, byte? mdsActivityId = null);
    }
}