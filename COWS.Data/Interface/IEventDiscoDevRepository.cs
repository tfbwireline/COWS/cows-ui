﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventDiscoDevRepository : IRepository<EventDiscoDev>
    {
        IQueryable<EventDiscoDev> Find(Expression<Func<EventDiscoDev, bool>> predicate);

        void Create(List<EventDiscoDev> entity);

        void SetInactive(int eventId);
    }
}