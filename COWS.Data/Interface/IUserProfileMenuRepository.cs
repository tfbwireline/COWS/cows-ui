﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUserProfileMenuRepository : IRepository<LkUsrPrfMenu>
    {
        IQueryable<LkUsrPrfMenu> Find(Expression<Func<LkUsrPrfMenu, bool>> predicate);
    }
}