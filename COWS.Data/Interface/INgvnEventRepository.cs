﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface INgvnEventRepository : IRepository<NgvnEvent>
    {
        IQueryable<NgvnEvent> Find(Expression<Func<NgvnEvent, bool>> predicate, string adid = null);

        IEnumerable<LkNgvnProdType> GetNgvnProdType();
    }
}