﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMplsActivityTypeRepository : IRepository<LkMplsActyType>
    {
        IQueryable<LkMplsActyType> Find(Expression<Func<LkMplsActyType, bool>> predicate);
    }
}