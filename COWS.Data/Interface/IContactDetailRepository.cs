﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IContactDetailRepository : IRepository<CntctDetl>
    {
        IQueryable<CntctDetl> Find(Expression<Func<CntctDetl, bool>> predicate);
        void Create(IEnumerable<CntctDetl> entity);
        void Update(IEnumerable<CntctDetl> entity);
        void SetInactive(Expression<Func<CntctDetl, bool>> predicate);
        DataTable GetContactDetails(int objId, string objType, int loggedInUserId, string hierId = null, string hierLvl = null, string odieCustId = null, bool isNetworkOnly = false);
        List<string> GetEmailAddresses(int ObjId, string ObjTypeCd, string EmailCd);
        List<string> GetEmailAddresses(int ObjId, string ObjTypeCd, string EmailCd, ref List<int> contactDetailUserIds);
    }
}
