﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ISPLKEventRepository : IRepository<SplkEvent>
    {
        IQueryable<SplkEvent> Find(Expression<Func<SplkEvent, bool>> predicate);

        //IEnumerable<LkSiptProdType> GetAllSIPTProdTypes();
        //IEnumerable<LkSiptTollType> GetAllSIPTTollTypes();
        IEnumerable<LkSplkActyType> GetAllSPLKActyType();

        IEnumerable<LkSplkEventType> GetAllSPLKEventType();

        bool StatusChanged(int eventID, byte eEventStatus, byte wfStatus);

        //void DeleteAllRltdOrdrs(int eventId);
        //void DeleteAllActTypes(int eventId);
        //void DeleteAllTollTypes(int eventId);
        //void AddRltdOrdrs(int eventId, List<SiptReltdOrdr> obj);
        //void AddTollTypes(int eventId, List<SiptEventTollType> obj);
        //void AddActTypes(int eventId, List<SiptEventActy> obj);
        //IEnumerable<SiptReltdOrdr> GetRltdOrdrs(int EventId);
        //IQueryable<SplkEvent> Find(Expression<Func<SplkEvent, bool>> predicate);
        string GetSysCfgValue(string sParamName);

        SplkEvent GetSplkEventById(int id, string Adid);

        int GetCSGLevelByeventId(int id);

        int GetCSGLevelAdId(string AdId);

        //SplkEvent Create(SplkEventModel Obj, string adId);
        //bool UpdateSplkEvent(int id, SplkEventModel Obj);
        SplkEvent Create(SplkEvent Obj, string adId);

        bool UpdateSplkEvent(int id, SplkEvent Obj);

        //bool chkM5Validation(string sH1, string sH6, string sM5OrdrNbr, string sADID);
    }
}