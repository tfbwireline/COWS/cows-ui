﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVendorEmailTypeRepository : IRepository<LkVndrEmailType>
    {
        IQueryable<LkVndrEmailType> Find(Expression<Func<LkVndrEmailType, bool>> predicate);
    }
}