﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSActivityTypeRepository : IRepository<LkMdsActyType>
    {
        IQueryable<LkMdsActyType> Find(Expression<Func<LkMdsActyType, bool>> predicate);
    }
}