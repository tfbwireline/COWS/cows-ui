﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IDeviceModelRepository : IRepository<LkDevModel>
    {
        IQueryable<LkDevModel> Find(Expression<Func<LkDevModel, bool>> predicate);
    }
}