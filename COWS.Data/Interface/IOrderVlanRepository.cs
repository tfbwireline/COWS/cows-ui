﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderVlanRepository : IRepository<OrdrVlan>
    {
        IQueryable<OrdrVlan> Find(Expression<Func<OrdrVlan, bool>> predicate);
    }
}