﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ISIPTEventRepository : IRepository<SiptEvent>
    {
        IQueryable<SiptEvent> Find(Expression<Func<SiptEvent, bool>> predicate, string adid= null);
        IEnumerable<LkSiptProdType> GetAllSIPTProdTypes();
        IEnumerable<LkSiptTollType> GetAllSIPTTollTypes();
        IEnumerable<LkSiptActyType> GetAllSIPTActyType();
        LkSiptActyType GetSIPTActyTypeByID(int id);
        LkSiptTollType GetSIPTTollTypeByID(int id);
        bool StatusChanged(int eventID, byte eEventStatus, byte wfStatus);
        void DeleteAllRltdOrdrs(int eventId);
        void DeleteAllActTypes(int eventId);
        void DeleteAllTollTypes(int eventId);
        void AddRltdOrdrs(int eventId, List<SiptReltdOrdr> obj);
        void AddTollTypes(int eventId, List<SiptEventTollType> obj);
        void AddActTypes(int eventId, List<SiptEventActy> obj);
        IEnumerable<SiptReltdOrdr> GetRltdOrdrs(int EventId);
        SiptEvent Create(SiptModel Obj, string adId);
        bool Update(int id, SiptModel Obj);
        SiptEventDoc GetSiptDocument(int id);
    }
}