﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSEventDslSbicCustTrptRepository : IRepository<MdsEventDslSbicCustTrpt>
    {
        IQueryable<MdsEventDslSbicCustTrpt> Find(Expression<Func<MdsEventDslSbicCustTrpt, bool>> predicate);

        IEnumerable<MdsEventDslSbicCustTrpt> GetMdsEventDslSbicCustTrptByEventId(int eventId);

        void AddCustTrptTbl(ref List<MdsEventDslSbicCustTrpt> data, int eventId);
        void Create(IEnumerable<MdsEventDslSbicCustTrpt> entity);
        void SetInactive(Expression<Func<MdsEventDslSbicCustTrpt, bool>> predicate);
    }
}