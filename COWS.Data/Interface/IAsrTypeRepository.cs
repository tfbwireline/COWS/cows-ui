﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IAsrTypeRepository : IRepository<LkAsrType>
    {
        IQueryable<LkAsrType> Find(Expression<Func<LkAsrType, bool>> predicate);
    }
}