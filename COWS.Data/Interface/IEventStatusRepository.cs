﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventStatusRepository : IRepository<LkEventStus>
    {
        IQueryable<LkEventStus> Find(Expression<Func<LkEventStus, bool>> predicate);
    }
}