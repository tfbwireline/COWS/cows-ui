﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IStatusRepository : IRepository<LkStus>
    {
        IQueryable<LkStus> Find(Expression<Func<LkStus, bool>> predicate);
    }
}