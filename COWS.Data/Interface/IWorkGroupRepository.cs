﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace COWS.Data.Interface
{
    public interface IWorkGroupRepository
    {
        DataSet GetOrderDisplayViewData(int userId, int usrPrfId);
        Task<DataTable> GetWGData(int userPrfId, int userId, int orderId, bool isCmplt, byte userCsgLevelId, string filter = null);
        int VerifyUserIsAdminToEditCompletedOrder(string userADID, string parameter);
        Task<DataTable> GetFTNListDetails(int orderId, int userPrfId, int userCsgLvlId = 0);
        DataTable GetFTNListDetailsForOrderView(int orderId, int userPrfId, int userCsgLvlId = 0);
        int CompleteActiveTask(int orderID, int taskID, Int16 taskStatus, string comments, int userID);
        string GetLatestNonSystemOrderNoteInfo(int orderID);
        int GetPreSubmitRTSStatus(int _orderID);
        bool OrderExistsInSalesSupportWG(int orderID);
        bool HasBillMissingTask(int iOrderID);
        DataTable GetH5FolderInfo(int _OrderID);
        int CompleteGOMIBillTask(int orderID);
        bool IsOrderCompleted(int orderID);
        List<CPEOrdrUpdInfo> GetCPEUpdtInfo(int orderID);
        void SaveCPEData(int _OrderID, List<CPEOrdrUpdInfo> _CPEData, int _userID, int workGrp);
        void InsertUpdateGOMInfo(int orderID, int userID, int cpeStusId, string cpeEqptTypeTxt, DateTime? cmplDt, decimal? reqAmt);
        void InsertUpdateAMNCIInfo(int orderID, int userID, int cpeStusId, string cpeEqptTypeTxt, DateTime? cmplDt, decimal? reqAmt);
        Task<DataTable> GetGOMSpecificData(int orderId);
        bool IsACRRTS(int orderId);
        int CompleteACRRTSTask(int orderId);
        int InsertOrderNotes(int orderId, int noteTypeId, int userId, string notes);
        void LoadNCITask(int orderId, short taskId);
        DataTable GetCSCSpecificData(int orderId);
        DataTable GetMDSEventData(int orderId);
        int InsertUpdateCSCInfo(int orderId, string serialNo, string mr, string po, string ims, int userId);
        bool IsOrderExistInSalesSupportWG(int orderID);
    }
}