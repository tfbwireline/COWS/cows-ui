﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderRepository : IRepository<Ordr>
    {
        IQueryable<Ordr> Find(Expression<Func<Ordr, bool>> predicate, string adid = null);
        OrdrMs GetOrderMilestoneByOrderId(int orderId);
        //IEnumerable<WorkGroupData> GetWgData(int orderId, int userId, int profileId = 0);
        IEnumerable<WorkGroupData> GetWgData(int orderId, int userId, int profileId = 0, int csgLvlId = 0);
        void AttachH5Folder(int orderId, int h5FolderId, int modifiedBy);
        IEnumerable<OrderBasic> GetBasic(int orderId);
        Ordr GetOrderPPRT(int id);
        TransportOrderDetails GetTransportOrder(int orderId);
        IQueryable<Ordr> GetOrderDetails(Expression<Func<Ordr, bool>> predicate, string adid = null);
        bool UpdateNrm(int id, bool nrmupdate);

        bool UpdateCustomerTurnUpTask(int orderId);

        IEnumerable<InstallPort> GetInstallPort(int orderId);

        IEnumerable<LogicallisData> RequestToLogicallis(int orderId);
    }
}
