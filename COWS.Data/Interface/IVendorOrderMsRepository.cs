﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Interface
{
    public interface IVendorOrderMsRepository : IRepository<VndrOrdrMs>
    {
        IQueryable<VndrOrdrMs> Find(Expression<Func<VndrOrdrMs, bool>> predicate);
    }
}
