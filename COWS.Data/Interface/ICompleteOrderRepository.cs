﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace COWS.Data.Interface
{
    public interface ICompleteOrderRepository
    {
        DataTable GetAdminExcPerm(string name);
        int M5CmpltMsg(string FTN, int MsgType);
        // IEnumerable<InsertM5CompleteMsg> M5CmpltMsg(string FTN, int MsgType);
    }
}
