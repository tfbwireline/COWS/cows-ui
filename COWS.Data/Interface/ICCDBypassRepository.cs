﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICCDBypassRepository : IRepository<CCDBypassUserView>
    {
        IQueryable<CCDBypassUserView> Find(Expression<Func<CCDBypassUserView, bool>> predicate);

        //IEnumerable<TimeSlotOccurrence> GetAll(short EventType, DateTime startDate, DateTime endDate, bool bShowAfterHrsSlots);
        //IEnumerable<FTAvailEvent> GetFTEventsForTS(int resourceID);
        //bool UpdateResourceAvailability(string updResultset, int userID);
        //bool CheckUserAccessToAfterHrsSlots(string adid);
        int CreateCCD(CCDBypassUserView obj);

        List<CCDBypassUserView> GetBypassUserList(int userId);

        List<LkUser> FindUser(string searchString, int userId);

        //List<FedlineManageUserEvent> GetEventsForTS(int resourceID);
        LkSysCfg GetBySYSCFGId(int id);
        List<CCDHistory> GetCCDHistory(int orderID);
    }
}