﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IMSSDocRepository
    {
        List<SDEOpportunityDoc> Get(int sdeID);

        SDEOpportunityDoc GetByDocID(int sdeDocID);

        void Insert(List<SDEOpportunityDoc> sdeDocs, int sdeID, int loggedInUserId);

        void Update(List<SDEOpportunityDoc> sdeDocs, int sdeID, int loggedInUserId);

        //void Delete(int sdeID);
    }
}