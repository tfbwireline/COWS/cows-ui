﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ISpecialProjectRepository : IRepository<LkSpclProj>
    {
        IQueryable<LkSpclProj> Find(Expression<Func<LkSpclProj, bool>> predicate);
    }
}