﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventRuleDataRepository : IRepository<EventRuleData>
    {
        IQueryable<EventRuleData> Find(Expression<Func<EventRuleData, bool>> predicate);

        List<EventRuleData> GetByRuleId(int id);

        EventRuleData GetByRuleIdDataCode(int id, string sEventDataCode);
    }
}