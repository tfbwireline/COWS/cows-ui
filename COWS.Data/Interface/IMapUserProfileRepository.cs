﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMapUserProfileRepository : IRepository<MapUsrPrf>
    {
        IQueryable<MapUsrPrf> Find(Expression<Func<MapUsrPrf, bool>> predicate);
    }
}