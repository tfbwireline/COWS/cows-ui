﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICityRepository : IRepository<LkCtryCty>
    {
        IQueryable<LkCtryCty> Find(Expression<Func<LkCtryCty, bool>> predicate);
    }
}