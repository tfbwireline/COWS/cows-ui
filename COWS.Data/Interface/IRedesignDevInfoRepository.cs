﻿using COWS.Entities.Models;
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IRedesignDevInfoRepository : IRepository<RedsgnDevicesInfo>
    {
        IQueryable<RedsgnDevicesInfo> Find(Expression<Func<RedsgnDevicesInfo, bool>> predicate);
        DataTable GetRedesignDevices(int redesignId);
    }
}
