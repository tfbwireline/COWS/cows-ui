﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICircuitMsRepository : IRepository<CktMs>
    {
        IQueryable<CktMs> Find(Expression<Func<CktMs, bool>> predicate);
    }
}
