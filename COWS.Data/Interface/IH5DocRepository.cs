﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IH5DocRepository : IRepository<H5Doc>
    {
        IQueryable<H5Doc> Find(Expression<Func<H5Doc, bool>> predicate);
    }
}