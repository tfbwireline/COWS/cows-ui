﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace COWS.Data.Interface
{
    public interface ISlotPickerRepository
    {
        DataTable getUsersByProduct(int iProductID);

        DataTable GetUsersByProfile(string profileName);

        DataTable getUsersByPrdProfile(DataTable dtPrd, DataTable dtProfile);

        DataTable GetAssignSkilledMembersforSubmitType(string sAction, string sIPVersion, string sSLNKEventType);

        DataSet GetWFCollection(ref DataTable dtSkilledMem, DateTime dcStart, DateTime dcEnd, string sAction, string sIsMDSFT, int iPrdID);

        DataTable GetFreeBusyCollection(ref DataTable dtScheduleMember, DateTime BusyStart, DateTime DCEnd, DateTime OrigStDt, int iEventID);

        DataTable getAllMDSMACDuration();

        Single getMDSMACDuration(DataTable dtAll, DataTable dtInput);

        Single getMDSMACDuration(DataTable dtAll, List<MdsEventMacActy> dtInput);

        Single getManDevDuration(bool oldMDS, DataTable dtInput);

        DataTable GetAssignSkilledMembersforMDS(ref DataTable dtMngDev, string sH1, string sIsFirewallSecurity, string sCustNme, List<LkMdsNtwkActyType> sMDSNtwkActy, List<string> lMplsActivityTypes = null);

        DataTable GetAssignSkilledMembersforMPLS(string sMPLSEventType, string sIPVersion);

        DataTable GetAssignSkilledMembersforEventType(string sEventTypeID);

        string GetActivatorWhenConflict(string[] sUserList, short dayOffset);

        string ChkForDblBooking(DateTime? start, DateTime? end, string sUID, int iEventId);
    }
}