﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVendorRepository : IRepository<LkVndr>
    {
        IQueryable<LkVndr> Find(Expression<Func<LkVndr, bool>> predicate);
        LkVndr CreateVendor(LkVndr entity, string ctryCd);
        void UpdateVendor(LkVndr entity, string ctryCd);
        void DeleteVendor(string vendorCode);
    }
}