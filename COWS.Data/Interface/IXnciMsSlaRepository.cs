﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IXnciMsSlaRepository : IRepository<LkXnciMsSla>
    {
        IQueryable<LkXnciMsSla> Find(Expression<Func<LkXnciMsSla, bool>> predicate);
    }
}