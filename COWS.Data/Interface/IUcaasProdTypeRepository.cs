﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUcaasProdTypeRepository : IRepository<LkUcaaSProdType>
    {
        IQueryable<LkUcaaSProdType> Find(Expression<Func<LkUcaaSProdType, bool>> predicate);
    }
}