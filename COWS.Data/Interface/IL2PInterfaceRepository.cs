﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IL2PInterfaceRepository
    {
        IEnumerable<RetrieveCustomerByH1> RetrieveCustomerByH1(string h1);
        IEnumerable<byte> GetM5CSGLvl(string h1, string h6, string m5OrdrNbr);
    }
}