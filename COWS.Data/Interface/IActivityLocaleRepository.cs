﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IActivityLocaleRepository : IRepository<LkActyLocale>
    {
        IQueryable<LkActyLocale> Find(Expression<Func<LkActyLocale, bool>> predicate);
    }
}