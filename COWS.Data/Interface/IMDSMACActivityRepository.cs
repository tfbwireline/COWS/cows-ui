﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSMACActivityRepository : IRepository<LkMdsMacActy>
    {
        IQueryable<LkMdsMacActy> Find(Expression<Func<LkMdsMacActy, bool>> predicate);
    }
}