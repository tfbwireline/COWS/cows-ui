﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMplsAccessBandwidthRepository : IRepository<LkMplsAccsBdwd>
    {
        IQueryable<LkMplsAccsBdwd> Find(Expression<Func<LkMplsAccsBdwd, bool>> predicate);
    }
}