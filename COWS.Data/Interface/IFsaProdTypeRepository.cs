﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IFsaProdTypeRepository : IRepository<LkFsaProdType>
    {
        IQueryable<LkFsaProdType> Find(Expression<Func<LkFsaProdType, bool>> predicate);
        LkFsaProdType GetById(string id);
        void Update(string id, LkFsaProdType entity);
    }
}