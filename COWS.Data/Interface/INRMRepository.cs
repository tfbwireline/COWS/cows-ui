﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface INRMRepository
    {
        List<NrmSrvcInstc> GetNRMServiceInstanceList(string ftn);
        List<NrmCkt> GetNRMCircuitList(string ftn);
        List<NrmVndr> GetNRMVendorList(string ftn);
        List<NRMNotes> GetNRMNotes(string ftn);
        void SendASRMessageToNRMBPM(int iOrderID, string notes, int focMsg = 0);
    }
}