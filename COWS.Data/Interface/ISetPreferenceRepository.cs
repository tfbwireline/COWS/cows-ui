﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ISetPreferenceRepository : IRepository<LkCnfrcBrdg>
    {
        IQueryable<LkCnfrcBrdg> Find(Expression<Func<LkCnfrcBrdg, bool>> predicate);
    }
}