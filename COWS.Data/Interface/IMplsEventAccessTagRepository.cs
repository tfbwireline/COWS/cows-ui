﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMplsEventAccessTagRepository : IRepository<MplsEventAccsTag>
    {
        IQueryable<MplsEventAccsTag> Find(Expression<Func<MplsEventAccsTag, bool>> predicate);

        List<MplsEventAccsTag> Create(List<MplsEventAccsTag> list);
    }
}