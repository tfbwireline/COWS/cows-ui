﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICircuitRepository : IRepository<Ckt>
    {
        IQueryable<Ckt> Find(Expression<Func<Ckt, bool>> predicate);
        DataTable GetPreQualLineInfo(int orderID);
        IEnumerable<Ckt> GetAccessCost(int orderId);
    }
}
