﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventSucssActyRepository : IRepository<EventSucssActy>
    {
        IQueryable<EventSucssActy> Find(Expression<Func<EventSucssActy, bool>> predicate);

        IEnumerable<EventSucssActy> GetByHistId(int id);

        IEnumerable<EventSucssActy> GetByEventId(int id);

        void DeleteByEventId(int id);

        void CreateActy(EventSucssActy entity);
    }
}