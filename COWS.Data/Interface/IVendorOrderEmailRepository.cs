﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVendorOrderEmailRepository : IRepository<VndrOrdrEmail>
    {
        IQueryable<VndrOrdrEmail> Find(Expression<Func<VndrOrdrEmail, bool>> predicate);
        int CreateVendorOrderMs(int vendorOrderEmailId, int modifiedBy);
        int AddVendorOrderEmail(int inVendorOrderID, byte inEmailTypeID, string inEmailSubject, string inEmailBody, int inCreatedBy);
        int CloneVendorOrderEmail(int vendorOrderEmailID, string attachmentsExcluded, int inCreatedBy);
        bool UpdateEmailAckDate(int inVendorOrderEmailID, DateTime? inAckDate, int loggedInUserId);
    }
}