﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ITaskRepository : IRepository<LkTask>
    {
        IQueryable<LkTask> Find(Expression<Func<LkTask, bool>> predicate);
        IQueryable<LkTask> GetCurrentStatus(Expression<Func<LkTask, bool>> predicate);
        string GetCurrentStatus(int taskId, int wgId, int orderId);
    }
}