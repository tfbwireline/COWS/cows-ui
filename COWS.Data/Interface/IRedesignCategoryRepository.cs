﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IRedesignCategoryRepository : IRepository<LkRedsgnCat>
    {
        IQueryable<LkRedsgnCat> Find(Expression<Func<LkRedsgnCat, bool>> predicate);
    }
}