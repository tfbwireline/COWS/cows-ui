﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptCustomerTypeRepository : IRepository<LkCptCustType>
    {
        IQueryable<LkCptCustType> Find(Expression<Func<LkCptCustType, bool>> predicate);
    }
}