﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IOrdrCancelRepository
    {
        IEnumerable<GetOrdrCancelView> GetOrdrCancelView(string Ftn, string H5_H6);

        //IEnumerable<GetOrdrUnlockView> GetOrdrUnlockEventView(string EventId);
        //IEnumerable<GetOrdrUnlockView> GetOrdrUnlockRedsgnView(string RedsgnId);
        //IEnumerable<GetOrdrUnlockView> GetOrdrUnlockCptView(string CptId);
        //string unlockItems(int ORDR_ID, int EVENT_ID, int USER_ID, int IsOrder, int Unlock, int IsLocked, int ItemType);
        int cancelOrdr(string Ftn, int USER_ID, string Notes);
    }
}