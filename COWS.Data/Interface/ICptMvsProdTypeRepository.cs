﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptMvsProdTypeRepository : IRepository<LkCptMvsProdType>
    {
        IQueryable<LkCptMvsProdType> Find(Expression<Func<LkCptMvsProdType, bool>> predicate);
    }
}