﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IActiveTaskRepository : IRepository<ActTask>
    {
        IQueryable<ActTask> Find(Expression<Func<ActTask, bool>> predicate);
        int CompleteActiveTask(StateMachine sm);
        int LoadRtsTask(int orderId);
        int MoveToGom(int orderId, int userId);
        int ClearAcrTask(int orderId, int taskId, int userId);
        bool CheckIfOrderIsBAR(int orderId);
    }
}
