﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface INidActyRepository : IRepository<NidActy>
    {
        IQueryable<NidActy> Find(Expression<Func<NidActy, bool>> predicate);
        IEnumerable<NidSerialView> GetSerialNumberByH6(int id, string h6);
        DataTable GetSerialNumberAndIP(int ordrId, string deviceId);
    }
}
