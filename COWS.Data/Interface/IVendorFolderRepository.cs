﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVendorFolderRepository : IRepository<VndrFoldr>
    {
        IQueryable<VndrFoldr> Find(Expression<Func<VndrFoldr, bool>> predicate);
        IEnumerable<GetVendorFolder> GetVendorFolder(string vendorName, string countryCode);
        DataSet GetVendorOrderEmailData(int inVendorOrderID, int userCsgLvlId = 0);
        VndrOrdrEmail GetVendorOrderEmail(int inVendorOrderEmailID);
        //VendorOrderEmailResults GetVendorOrderEmails(int vendorOrderID);
    }
}