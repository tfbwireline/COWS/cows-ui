﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMdsEventOdieDevRepository : IRepository<MdsEventOdieDev>
    {
        IQueryable<MdsEventOdieDev> Find(Expression<Func<MdsEventOdieDev, bool>> predicate);
        void Delete(Expression<Func<MdsEventOdieDev, bool>> predicate);
        void Create(IEnumerable<MdsEventOdieDev> entity);
    }
}