﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderStdiHistoryRepository : IRepository<OrdrStdiHist>
    {
        IQueryable<OrdrStdiHist> Find(Expression<Func<OrdrStdiHist, bool>> predicate);
    }
}