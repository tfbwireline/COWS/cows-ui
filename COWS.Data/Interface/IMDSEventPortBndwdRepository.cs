﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSEventPortBndwdRepository : IRepository<MdsEventPortBndwd>
    {
        IQueryable<MdsEventPortBndwd> Find(Expression<Func<MdsEventPortBndwd, bool>> predicate);

        IEnumerable<MdsEventPortBndwd> GetMDSEventPortBndwdByEventId(int eventId);

        void AddPortBndwd(ref List<MdsEventPortBndwd> data, int eventId);
        void SetInactive(Expression<Func<MdsEventPortBndwd, bool>> predicate);
        void Create(IEnumerable<MdsEventPortBndwd> entity);
    }
}