﻿using COWS.Entities.Models;
using System.Threading.Tasks;

namespace COWS.Data.Interface
{
    public interface IOrderLockRepository : ILockRepository<OrdrRecLock>
    {
        Task<int> LockUnlockOrdersEvents(int orderId, int eventId, int userId, bool isOrder, bool unlock, int isLocked);
    }
}