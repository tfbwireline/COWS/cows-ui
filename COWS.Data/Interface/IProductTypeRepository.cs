﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IProductTypeRepository : IRepository<LkProdType>
    {
        IQueryable<LkProdType> Find(Expression<Func<LkProdType, bool>> predicate);

        IEnumerable<string> GetProductPlatformCombinations();
    }
}