﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICountryRepository : IRepository<LkCtry>
    {
        IQueryable<LkCtry> Find(Expression<Func<LkCtry, bool>> predicate);
        void UpdateRegion(string code, LkCtry entity);
        IQueryable<object> GetAllForLookup();
    }
}