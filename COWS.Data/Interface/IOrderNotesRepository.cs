﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderNoteRepository : IRepository<OrdrNte>
    {
        IQueryable<OrdrNte> Find(Expression<Func<OrdrNte, bool>> predicate);

        List<OrderNoteView> Select(string SortField);

        List<OrderNoteView> FindForGivenOrderAndNoteType(int orderId, int noteTypeId, string sortExpression, int PrntPrfID, int userID = 0);

        bool Delete(int[] Ids);

        List<Object> GetNotes(int inOrderID, short inNoteTypeID);

        IEnumerable<OrdrNte> GetByOrdrId(int id);

        IEnumerable<OrdrNte> GetByNoteTypeId(int id);
        int CreateNote(OrdrNte entity);

        //void CreateEventHistory(int eventId, byte actnId, DateTime? start, DateTime? end, int userId);
        //int CreateEventHistory(OrdrNte obj);
        //int CreateEventHistoryReturnHist(OrdrNte obj);
    }
}