﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IOrdrUnlockRepository
    {
        IEnumerable<GetOrdrUnlockView> GetOrdrUnlockOrdrView(string Ftn);

        IEnumerable<GetOrdrUnlockView> GetOrdrUnlockEventView(string EventId);

        IEnumerable<GetOrdrUnlockView> GetOrdrUnlockRedsgnView(string RedsgnId);

        IEnumerable<GetOrdrUnlockView> GetOrdrUnlockCptView(string CptId);

        //string unlockItems(int ORDR_ID, int EVENT_ID, int USER_ID, int IsOrder, int Unlock, int IsLocked, int ItemType);
        IEnumerable<GetOrdrUnlockView> unlockItems(int ORDR_ID, int EVENT_ID, int USER_ID, int IsOrder, int Unlock, int IsLocked, int ItemType);
    }
}