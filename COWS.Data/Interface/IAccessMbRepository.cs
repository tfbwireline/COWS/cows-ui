﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Interface
{
    public interface IAccessMbRepository : IRepository<LkAccsMb>
    {
        IQueryable<LkAccsMb> Find(Expression<Func<LkAccsMb, bool>> predicate);
    }
}
