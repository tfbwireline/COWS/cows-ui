﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventDeviceCompletionRepository : IRepository<EventDevCmplt>
    {
        IQueryable<EventDevCmplt> Find(Expression<Func<EventDevCmplt, bool>> predicate);
        void Create(IEnumerable<EventDevCmplt> entity);
        void SetInactive(Expression<Func<EventDevCmplt, bool>> predicate);
    }
}