﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderSubTypeRepository : IRepository<LkOrdrSubType>
    {
        IQueryable<LkOrdrSubType> Find(Expression<Func<LkOrdrSubType, bool>> predicate);
    }
}