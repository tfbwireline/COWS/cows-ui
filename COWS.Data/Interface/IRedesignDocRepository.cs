﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IRedesignDocRepository : IRepository<RedsgnDoc>
    {
        IQueryable<RedsgnDoc> Find(Expression<Func<RedsgnDoc, bool>> predicate);

        List<RedsgnDoc> SelectContextObject(string SortField, string SearchCriteria, params object[] parameters);

        List<RedsgnDoc> SelectContextObject(string SortField, string SearchCriteria);

        bool Insert(DocEntity Obj);

        bool Update(DocEntity Obj);

        //bool Delete(int Id);

        bool Delete(int[] Ids);

        DocEntity Get(int id);

        DocEntity GetUnique(int id, string fileName);

        List<DocEntity> Select(string SortField);

        List<DocEntity> Select(string SortField, string SearchCriteria);

        int CountRedesignDocs(int Id);
    }
}