﻿using COWS.Entities.Enums;
using COWS.Entities.Models;
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventHistoryRepository : IRepository<EventHist>
    {
        IQueryable<EventHist> Find(Expression<Func<EventHist, bool>> predicate);

        //void CreateEventHistory(int eventId, byte actnId, DateTime? start, DateTime? end, int userId);
        int CreateEventHistory(EventHist obj, int[] toExcludeAction = null, EventType? eventType = null, string dispatchEmail = null);
        bool InsertEventFormChanges(MdsEvent Obj, MdsEvent old);

        //int CreateEventHistoryReturnHist(EventHist obj);
        DataTable GetEventHistByEventID(int iEventID, EventType EventTypeID);
        EventHist GetReviewerId(int eventId);
    }
}