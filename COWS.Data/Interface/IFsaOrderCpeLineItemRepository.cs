﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IFsaOrderCpeLineItemRepository : IRepository<FsaOrdrCpeLineItem>
    {
        IQueryable<FsaOrdrCpeLineItem> Find(Expression<Func<FsaOrdrCpeLineItem, bool>> predicate);
    }
}