﻿using COWS.Entities.Models;

namespace COWS.Data.Interface
{
    public interface IVendorEmailLanguageRepository : IRepository<LkVndrEmailLang>
    {
        LkVndrEmailLang Get(int id);
        void ApplyLanguageRules(LkVndrEmailLang _vel);
    }
}