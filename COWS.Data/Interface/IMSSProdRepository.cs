﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IMSSProdRepository
    {
        List<SDEOpportunityProduct> Get(int sdeID);
        void Insert(List<SDEOpportunityProduct> sdeProds, int sdeID, int loggedInUserId);
        void Update(List<SDEOpportunityProduct> sdeProds, int sdeID, int loggedInUserId);
    }
}