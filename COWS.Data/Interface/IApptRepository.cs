﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IApptRepository : IRepository<Appt>
    {
        IQueryable<Appt> Find(Expression<Func<Appt, bool>> predicate);

        bool CalendarEntry(EventWorkflow eventWorkflow, bool bMDSFT = false, bool isNtwkIntl = false);
    }
}