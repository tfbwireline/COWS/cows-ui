﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUcaasEventOdieDeviceRepository : IRepository<UcaaSEventOdieDev>
    {
        IQueryable<UcaaSEventOdieDev> Find(Expression<Func<UcaaSEventOdieDev, bool>> predicate);
        void Delete(Expression<Func<UcaaSEventOdieDev, bool>> predicate);
        void Create(IEnumerable<UcaaSEventOdieDev> entity);
    }
}