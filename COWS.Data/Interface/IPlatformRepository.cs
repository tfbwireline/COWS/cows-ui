﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IPlatformRepository : IRepository<LkPltfrm>
    {
        IQueryable<LkPltfrm> Find(Expression<Func<LkPltfrm, bool>> predicate);
    }
}