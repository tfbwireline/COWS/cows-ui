﻿using COWS.Entities.Models;

namespace COWS.Data.Interface
{
    public interface ICptRecLockRepository : ILockRepository<CptRecLock>
    {
    }
}
