﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUserCsgLevelRepository : IRepository<UserCsgLvl>
    {
        IQueryable<UserCsgLvl> Find(Expression<Func<UserCsgLvl, bool>> predicate);
    }
}