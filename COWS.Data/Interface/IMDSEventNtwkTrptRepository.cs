﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSEventNtwkTrptRepository : IRepository<MdsEventNtwkTrpt>
    {
        IQueryable<MdsEventNtwkTrpt> Find(Expression<Func<MdsEventNtwkTrpt, bool>> predicate);

        IEnumerable<MdsEventNtwkTrpt> GetMDSEventNtwkTrptByEventId(int eventId);

        void AddNtwkTrpt(List<MdsEventNtwkTrpt> data, int eventId, int csgLvlId);

        void SetInactive(int eventId);

        void Create(List<MdsEventNtwkTrpt> entity);
        void UpdateNetworkBPMTask(string ddNbr, string h6, int vasCeFlg, bool ceChngFlg, int eventId, int? modfdByUserId);
        void UpdateNidAtBPM(int orderId, string h6, string nidSerialNumber, int eventId, int? modfdByUserId);
    }
}