﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICountryCxrRepository : IRepository<LkCtryCxr>
    {
        IQueryable<LkCtryCxr> Find(Expression<Func<LkCtryCxr, bool>> predicate);
    }
}