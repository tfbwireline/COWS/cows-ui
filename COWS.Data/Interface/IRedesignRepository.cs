﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IRedesignRepository : IRepository<Redsgn>
    {
        IQueryable<Redsgn> Find(Expression<Func<Redsgn, bool>> predicate, string adid = null);
        bool InsertRedesignEmail(int redesignID, Int16 STUS_ID, bool expirationFlag, bool note_updated);
        Dictionary<string, string> GetRedesignCostValues();
        bool GetRedesignCustomerBypassCD(string H1_CD, string Cust_Name, int csgLvlId);
        decimal GetH1MRCValue(string h1, byte redsgnCatID);
        DataTable GetRedesignSearchResults(string rdsgnNos, string custName, string nte, string devices, string pm, int csgLvlId, string adid = null);
        Redsgn GetById(int id, string adid, bool includeSecuredData = true);
        IQueryable<RedesignNtePmData> GetNtePmRedesignData(string redsgnNbr);
    }
}