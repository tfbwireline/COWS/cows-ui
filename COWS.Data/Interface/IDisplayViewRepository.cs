﻿using COWS.Entities.Models;
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IDisplayViewRepository
    {
        IQueryable<DsplVw> Find(Expression<Func<DsplVw, bool>> predicate);
        DataTable GetViewDetails(int vwID, int siteCntntID, int userID, string userAdid);
    }
}
