﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMultiVrfReqRepository : IRepository<LkMultiVrfReq>
    {
        IQueryable<LkMultiVrfReq> Find(Expression<Func<LkMultiVrfReq, bool>> predicate);
    }
}