﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMenuRepository : IRepository<LkMenu>
    {
        //IEnumerable<GetSCMInterfaceView> GetSCMInterfaceView();
        //IEnumerable<GetNRMBPMInterfaceView> GetNRMBPMInterfaceView();
        IQueryable<LkMenu> Find(Expression<Func<LkMenu, bool>> predicate);

        List<LkSysCfg> GetExternalLinks();
    }
}