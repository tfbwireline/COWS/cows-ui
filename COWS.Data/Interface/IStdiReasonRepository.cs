﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IStdiReasonRepository : IRepository<LkStdiReas>
    {
        IQueryable<LkStdiReas> Find(Expression<Func<LkStdiReas, bool>> predicate);
    }
}
