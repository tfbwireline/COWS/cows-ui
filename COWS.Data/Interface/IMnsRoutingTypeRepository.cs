﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMnsRoutingTypeRepository : IRepository<LkMnsRoutgType>
    {
        IQueryable<LkMnsRoutgType> Find(Expression<Func<LkMnsRoutgType, bool>> predicate);
    }
}