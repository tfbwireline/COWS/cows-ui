﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace COWS.Data.Interface
{
    public interface IAdditionalCustomerChargeRepository
    {
        DataTable searchFTN(string FTN);

        List<CircuitAddlCost> getAddlCharges(int ordrId, bool isTerm);

        List<CircuitAddlCost> getAddlChargeHistory(int orderID, int chargeTypeId, bool IsTerm);
    }
}
