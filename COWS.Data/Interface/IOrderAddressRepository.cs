﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderAddressRepository : IRepository<OrdrAdr>
    {
        IQueryable<OrdrAdr> Find(Expression<Func<OrdrAdr, bool>> predicate);
        IQueryable<object> GetByOrderId(int orderId);
        IEnumerable<OrdrAdr> GetOrderAddressByOrderId(int orderId, int csgLvlId);
    }
}