﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptMdsSupportTierRepository : IRepository<LkCptMdsSuprtTier>
    {
        IQueryable<LkCptMdsSuprtTier> Find(Expression<Func<LkCptMdsSuprtTier, bool>> predicate);
    }
}