﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEscalationReasonRepository : IRepository<LkEsclReas>
    {
        IQueryable<LkEsclReas> Find(Expression<Func<LkEsclReas, bool>> predicate);
    }
}