﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDS3rdPartyVendorRepository : IRepository<LkMds3rdpartyVndr>
    {
        IQueryable<LkMds3rdpartyVndr> Find(Expression<Func<LkMds3rdpartyVndr, bool>> predicate);
    }
}