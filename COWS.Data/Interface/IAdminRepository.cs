﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IAdminRepository
    {
        IEnumerable<GetSCMInterfaceView> GetSCMInterfaceView();

        IEnumerable<GetNRMBPMInterfaceView> GetNRMBPMInterfaceView();
    }
}