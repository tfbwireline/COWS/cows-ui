﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ITimeSlotOccurrenceRepository : IRepository<TimeSlotOccurrence>
    {
        IQueryable<TimeSlotOccurrence> Find(Expression<Func<TimeSlotOccurrence, bool>> predicate);

        IEnumerable<TimeSlotOccurrence> GetAll(short EventType, DateTime startDate, DateTime endDate, bool bShowAfterHrsSlots);

        IEnumerable<FTAvailEvent> GetFTEventsForTS(int resourceID);

        bool UpdateResourceAvailability(string updResultset, int userID);

        bool CheckUserAccessToAfterHrsSlots(string adid);

        List<FedlineManageUserEvent> GetInvalidTSEvents();

        List<FedlineManageUserEvent> GetDisconnectEvents();

        List<FedlineManageUserEvent> GetFedEventsForTS(int resourceID);
    }
}