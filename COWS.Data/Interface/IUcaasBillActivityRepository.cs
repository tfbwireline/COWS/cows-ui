﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUcaasBillActivityRepository : IRepository<LkUcaaSBillActy>
    {
        IQueryable<LkUcaaSBillActy> Find(Expression<Func<LkUcaaSBillActy, bool>> predicate);
    }
}