﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEnhncSrvcRepository : IRepository<LkEnhncSrvc>
    {
        IQueryable<LkEnhncSrvc> Find(Expression<Func<LkEnhncSrvc, bool>> predicate);
    }
}