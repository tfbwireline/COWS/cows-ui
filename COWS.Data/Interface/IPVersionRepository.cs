﻿using COWS.Entities.Enums;
using COWS.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public class IPVersionRepository : IIPVersionRepository
    {
        private readonly COWSAdminDBContext _context;
        private readonly IConfiguration _configuration;
        private IMemoryCache _cache;

        public IPVersionRepository(COWSAdminDBContext context, IConfiguration configuration, IMemoryCache cache)
        {
            _context = context;
            _configuration = configuration;
            _cache = cache;
        }

        public LkIpVer Create(LkIpVer entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LkIpVer> Find(Expression<Func<LkIpVer, bool>> predicate)
        {
            if (!_cache.TryGetValue(CacheKeys.LkIpVer, out List<LkIpVer> list))
                list = GetAll().ToList();

            return list.AsQueryable().Where(predicate)
                            .AsNoTracking();

        }

        public IEnumerable<LkIpVer> GetAll()
        {
            if (!_cache.TryGetValue(CacheKeys.LkIpVer, out List<LkIpVer> list))
            {
                list = _context.LkIpVer
                            .OrderBy(i => i.IpVerNme)
                            .ToList();
                _cache.Set(CacheKeys.LkIpVer, list, new MemoryCacheEntryOptions().SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(3600)));
            }
            return list;
        }

        public LkIpVer GetById(int id)
        {
            return _context.LkIpVer
                            .SingleOrDefault(i => i.IpVerId == id);
        }

        public int SaveAll()
        {
            _cache.Remove(CacheKeys.LkIpVer);
            return _context.SaveChanges();
        }

        public void Update(int id, LkIpVer entity)
        {
            throw new NotImplementedException();
        }
    }
}