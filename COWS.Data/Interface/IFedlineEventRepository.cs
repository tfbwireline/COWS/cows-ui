﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;
using System.Data;

namespace COWS.Data.Interface
{
    public interface IFedlineEventRepository
    {
        FedlineEvent GetFedEvent(int EventID);

        bool CheckCancelExists(int eventID);

        void ClearInvalidTimeSlot(int EventID);

        bool SaveEventUserData(FedlineEvent fedlineEvent);

        void SaveEventTadpoleHeadendData(FedlineEvent fedlineEvent);

        bool SaveStatusHistory(int eventID, byte ActionID, string comments, int userID, bool preConfigComplete);

        bool InsertMessage(string MessageType, FedlineEvent fedlineEvent, bool holdMessage);

        bool InsertEndProcess(string ProcessType, int _eventID, int _endStatus);

        bool UpdateSLA_cd(int _eventID);

        bool InsertStrtProcess(string ProcessType, int _eventID);

        int SaveFieldUpdateHistory(int EventID, string xmlString);

        int SubmitHeadendEvent(FedlineEvent fedlineEvent, int UserID);

        List<FedlineUserSearchEntity> SearchUsers(string input, byte role);

        DataTable GetEventUpdateHistByEventID(int eventId);

        DataTable GetCCDHistory(int eventId);

        bool CheckStatusExists(int eventID);

        DataTable GetFedlineEventFailCodes(int eventRuleId);

        List<string> GetEmailAddresses(List<int> UserIDList);
        List<FedlineUserSearchEntity> SearchUsers(string input, int profileId);
    }
}