﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IForeignCxrRepository : IRepository<LkFrgnCxr>
    {
        IQueryable<LkFrgnCxr> Find(Expression<Func<LkFrgnCxr, bool>> predicate);
    }
}