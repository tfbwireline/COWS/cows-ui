﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ISstatReqRepository : IRepository<SstatReq>
    {
        IQueryable<SstatReq> Find(Expression<Func<SstatReq, bool>> predicate);
    }
}