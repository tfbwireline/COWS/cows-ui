﻿using COWS.Entities.Models;

namespace COWS.Data.Interface
{
    public interface IEventLockRepository : ILockRepository<EventRecLock>
    {
    }
}