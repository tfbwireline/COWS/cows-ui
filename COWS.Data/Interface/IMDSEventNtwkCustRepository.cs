﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSEventNtwkCustRepository : IRepository<MdsEventNtwkCust>
    {
        IQueryable<MdsEventNtwkCust> Find(Expression<Func<MdsEventNtwkCust, bool>> predicate);
        IEnumerable<MdsEventNtwkCust> GetMDEventNtwkCustByEventId(int eventId);

        void AddNtwkCust(List<MdsEventNtwkCust> data, int eventId, int csgLvlId);

        void SetInactive(int eventId);

        void Create(List<MdsEventNtwkCust> entity);
        void Delete(List<MdsEventNtwkCust> entity);
    }
}