﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptHostManagedServiceRepository : IRepository<LkCptHstMngdSrvc>
    {
        IQueryable<LkCptHstMngdSrvc> Find(Expression<Func<LkCptHstMngdSrvc, bool>> predicate);
    }
}