﻿using COWS.Entities.Models;
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUcaasEventRepository : IRepository<UcaaSEvent>
    {
        IQueryable<UcaaSEvent> Find(Expression<Func<UcaaSEvent, bool>> predicate, string adid = null);

        DataSet GetUcaasEventData(int _iEventID);
    }
}