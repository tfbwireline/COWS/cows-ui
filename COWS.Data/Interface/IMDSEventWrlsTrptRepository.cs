﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSEventWrlsTrptRepository : IRepository<MdsEventWrlsTrpt>
    {
        IQueryable<MdsEventWrlsTrpt> Find(Expression<Func<MdsEventWrlsTrpt, bool>> predicate);

        IEnumerable<MdsEventWrlsTrpt> GetMDSEventWrlsTrptByEventId(int eventId);

        void AddWrlsTrptTbl(ref List<MdsEventWrlsTrpt> data, int eventId);
        void Create(IEnumerable<MdsEventWrlsTrpt> entity);
        void Delete(Expression<Func<MdsEventWrlsTrpt, bool>> predicate);
    }
}