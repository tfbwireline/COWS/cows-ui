﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventFailActyRepository : IRepository<EventFailActy>
    {
        IQueryable<EventFailActy> Find(Expression<Func<EventFailActy, bool>> predicate);

        IEnumerable<EventFailActy> GetByHistId(int id);

        IEnumerable<EventFailActy> GetByEventId(int id);

        void DeleteByEventId(int id);

        void CreateActy(EventFailActy entity);
    }
}