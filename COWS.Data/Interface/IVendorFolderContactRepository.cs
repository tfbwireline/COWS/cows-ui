﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVendorFolderContactRepository : IRepository<VndrFoldrCntct>
    {
        IQueryable<VndrFoldrCntct> Find(Expression<Func<VndrFoldrCntct, bool>> predicate);

        VndrFoldrCntct CreateVendorFolderContact(VndrFoldrCntct entity);

        void UpdateVendorFolderContact(VndrFoldrCntct entity, string ctryCd);
    }
}