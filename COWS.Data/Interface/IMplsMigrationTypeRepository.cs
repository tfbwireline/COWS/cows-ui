﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMplsMigrationTypeRepository : IRepository<LkMplsMgrtnType>
    {
        IQueryable<LkMplsMgrtnType> Find(Expression<Func<LkMplsMgrtnType, bool>> predicate);
    }
}