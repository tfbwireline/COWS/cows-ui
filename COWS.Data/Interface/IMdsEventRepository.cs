﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace COWS.Data.Interface
{
    public interface IMdsEventRepository : IRepository<MdsEvent>
    {
        IQueryable<MdsEvent> Find(Expression<Func<MdsEvent, bool>> predicate);
        //IEnumerable<DesignDocDetail> GetDesignDocDetail(string h6, string dd, string nua);
        //Task<DataTable> GetVASSubTypes();
        DataTable GetVASSubTypes();
        DesignDocDetail GetDesignDocDetail(string h6, string dd, int VASCEFlg = 0, bool? CEChngFlg = false);
        string GenerateICSFile(int eventId, int mode, string loggedInUser, int apptId, string userIds);
        void SaveEventDevComplete(MdsEvent obj, MdsEventView mdsEvent);


        MdsEvent GetById(int id, string adid, bool includeSecuredData = true);

        MdsEvent Update(int id, MdsEvent entity, IEnumerable<MdsEventNtwkTrptView> networkTransport, MdsEvent mdsOld);

    }
}