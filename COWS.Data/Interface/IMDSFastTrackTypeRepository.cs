﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSFastTrackTypeRepository : IRepository<MdsFastTrkType>
    {
        IQueryable<MdsFastTrkType> Find(Expression<Func<MdsFastTrkType, bool>> predicate);

        MdsFastTrkType GetById(string id);
    }
}