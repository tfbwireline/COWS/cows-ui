﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICustScrdDataRepository : IRepository<CustScrdData>
    {
        IQueryable<CustScrdData> Find(Expression<Func<CustScrdData, bool>> predicate);
    }
}
