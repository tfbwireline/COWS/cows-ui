﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMnsOfferingRouterRepository : IRepository<LkMnsOffrgRoutr>
    {
        IQueryable<LkMnsOffrgRoutr> Find(Expression<Func<LkMnsOffrgRoutr, bool>> predicate);
    }
}