﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IAccessCitySiteRepository : IRepository<LkAccsCtySite>
    {
        IQueryable<LkAccsCtySite> Find(Expression<Func<LkAccsCtySite, bool>> predicate);
    }
}