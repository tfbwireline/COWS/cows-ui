﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface INRMBPMInterfaceRepository
    {
        IEnumerable<GetNRMBPMInterfaceView> GetNRMBPMInterfaceView();
    }
}