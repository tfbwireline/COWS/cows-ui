﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Interface
{
    public interface IIpManagementRepository : IRepository<LkIpMstr>
    {
        IQueryable<LkIpMstr> Find(Expression<Func<LkIpMstr, bool>> predicate);
        
        DataTable GetIPRecords(int id);
        DataTable GetIpMstrRelatedAdress(int id);

        DataTable GetRelatedActyForSelectedIPAddress(int id, int type);
    }
}
