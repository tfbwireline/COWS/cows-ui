﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Data.Interface
{
    public interface IDashboardRepository
    {
        DashboardData Get();
    }
}
