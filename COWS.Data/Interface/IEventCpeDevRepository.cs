﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IEventCpeDevRepository
    {
        //IQueryable<EventCpeDev> Find(Expression<Func<EventCpeDev, bool>> predicate);
        IEnumerable<GetEventCpeDev> GetEventCpeDevByEventId(int eventId);

        void AddCPEDevTbl(ref List<GetEventCpeDev> Obj, int eventId);
        void Create(IEnumerable<EventCpeDev> entity);
        void SetInactive(int eventId);
    }
}