﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMSSRepository
    {
        IEnumerable<GetSdeView> GetSdeView(int statusID, int userID);

        DataTable GetAvailableUsers();

        DataTable GetViewOptions();

        //DataTable GetSdeView(int statusID, int userID);
        IEnumerable<LkSdePrdctType> GetSDEProductTypesForAdmin();

        IEnumerable<LkSdePrdctType> GetSDEProductTypesByIDForAdmin(int id);

        IEnumerable<LkSdePrdctType> InsertProductType(LkSdePrdctType lkSdeProductType);

        bool UpdateProductType(int id, LkSdePrdctType lkSdeProductType);

        bool DeleteProductType(int id);

        SdeOpportunity GetByID(int sdeID);

        DataTable GetSDEProductTypes();

        DataTable GetWorkflowStatus(int statusID);

        List<SDEOpportunityNote> GetSDENotes(int sdeID);

        List<SDEOpportunityDoc> GetSDEDocs(int sdeID);

        List<SDEOpportunityProduct> GetSDEProds(int sdeID);

        SDEOpportunityDoc GetSDEDocByID(int sdeDocID);

        bool AddSDENote(int sdeOpportunityID, string noteText, int loggedInUser);

        string GetCowsAppCfgValue(string cfgKeyName);

        bool Insert(SdeOpportunity entity, int loggedInUserId);

        bool Update(SdeOpportunity entity, int loggedInUserId);

        void Update(int id, LkSdePrdctType obj);

        //bool Delete(int sdeID);
        //bool Delete(int[] Ids);

        IQueryable<LkSdePrdctType> Find(Expression<Func<LkSdePrdctType, bool>> predicate);

        IQueryable<LkSdePrdctType> GetAllSdeProductType(Expression<Func<LkSdePrdctType, bool>> predicate);

        byte[] byteStringToByteArray(string byteString);
    }
}