﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IWorkflowRepository : IRepository<LkWrkflwStus>
    {
        IQueryable<LkWrkflwStus> Find(Expression<Func<LkWrkflwStus, bool>> predicate);
        bool UpdateEWStatus(EventWorkflow esw);
    }
}