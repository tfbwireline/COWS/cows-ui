﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IH5FolderRepository : IRepository<H5Foldr>
    {
        IQueryable<H5Foldr> Find(Expression<Func<H5Foldr, bool>> predicate);
        //H5Foldr GetByOrderId(int id);
        DataTable GetH5FolderAssociations(int h5FolderId);
        IEnumerable<H5FolderView> Search(int h5FolderId, int custId, string ftn, string custNme, string city, string country, int userId);
        H5Foldr GetById(int id, string adid);
        H5Foldr GetByIdForOrderView(int id, int usrCsglvlId);
    }
}