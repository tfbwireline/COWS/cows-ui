﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IWFMUserAssignmentRepository : IRepository<WFMUserAssignment>
    {
        IEnumerable<WFMUserAssignment> getWFMUserAssignments(int userID, int userProfileID = 0);

        bool DeleteWFMUserAssignments(string selectedAssignmentKeys, int userID);

        bool ResetOldWFMAssignments(int userID);

        bool InsertWFMAssignment(InsertWFMUserAssignments entity);

        IQueryable<UserWfm> Find(Expression<Func<UserWfm, bool>> predicate);

        void Update(int id, byte wfmAssignmentType);
    }
}