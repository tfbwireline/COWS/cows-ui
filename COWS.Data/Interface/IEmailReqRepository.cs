﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEmailReqRepository : IRepository<EmailReq>
    {
        IQueryable<EmailReq> Find(Expression<Func<EmailReq, bool>> predicate);
        bool SIPTSendMail(EventWorkflow esw);
        bool SendMail(EventWorkflow esw);
        bool SendMDSMail(MdsEvent dMDSData, EventWorkflow esw, ICollection<MdsEventNtwkTrptView> networkTransport, string primaryContact = null);
        bool UcaasSendMail(EventWorkflow esw);
        int InsertIntoEmailReq(byte iEmailReqTypeID, int iEventID, string sTo, string sCC, string sSubj, string sBody, bool bSuppressEmails);
        void DeleteRequests(List<EmailReq> emails);
        bool IsEventEmailSent(int eventId, int emailReqTypeId);
    }
}