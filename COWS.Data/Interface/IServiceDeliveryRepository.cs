﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IServiceDeliveryRepository : IRepository<LkSprintCpeNcr>
    {
        IQueryable<LkSprintCpeNcr> Find(Expression<Func<LkSprintCpeNcr, bool>> predicate);
    }
}