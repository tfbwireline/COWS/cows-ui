﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSImplementationIntervalRepository : IRepository<LkMdsImplmtnNtvl>
    {
        IQueryable<LkMdsImplmtnNtvl> Find(Expression<Func<LkMdsImplmtnNtvl, bool>> predicate);
    }
}