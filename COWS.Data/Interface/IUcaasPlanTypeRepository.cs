﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUcaasPlanTypeRepository : IRepository<LkUcaaSPlanType>
    {
        IQueryable<LkUcaaSPlanType> Find(Expression<Func<LkUcaaSPlanType, bool>> predicate);
    }
}