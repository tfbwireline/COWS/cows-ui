﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface ISearchRepository
    {
        IEnumerable<AdvancedSearchOrder> GetAdvancedSearchOrderResult(dynamic model, string adid);

        IEnumerable<AdvancedSearchEvent> GetAdvancedSearchEventResult(dynamic model, string adid);

        IEnumerable<AdvancedSearchRedesign> GetAdvancedSearchRedesignResult(dynamic model, string adid);

        GetM5CANDEventDataResults M5OrH6Lookup(string m5, string h6, int? eventType = null, int vasCd = 0, bool isCEChng = false);

        //GetM5EventDatabyH6Results M5LookupByH6CCD(string h6, string ccd, string ucaasCD);
        GetM5EventDatabyH6Results M5LookupByH6CCD(string h6, string ccd, string ucaasCD, bool isCarrierEthernet = false);

        GetM5CPEOrderDataResults M5CPEOrderData(string h6, string ccd, int eventId, string ucaasCD);


        List<RedsgnDevicesInfo> RetrieveRedesign(string h1, string custName, int eventTypeId, byte userCsgLvl);
    }
}