﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IRedesignNoteRepository : IRepository<RedsgnNotes>
    {
        IQueryable<RedsgnNotes> Find(Expression<Func<RedsgnNotes, bool>> predicate);
    }
}
