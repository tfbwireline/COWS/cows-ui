﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMnsPerformanceReportingRepository : IRepository<LkMnsPrfmcRpt>
    {
        IQueryable<LkMnsPrfmcRpt> Find(Expression<Func<LkMnsPrfmcRpt, bool>> predicate);
    }
}