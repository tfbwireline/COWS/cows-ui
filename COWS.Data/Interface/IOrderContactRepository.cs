﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderContactRepository : IRepository<OrdrCntct>
    {
        IQueryable<OrdrCntct> Find(Expression<Func<OrdrCntct, bool>> predicate);
        List<OrdrCntct> GetSecuredOrdrCntctById(int id);
        OrdrCntct GetH6ContactByOrderId(int id, int csgLvlId);
    }
}