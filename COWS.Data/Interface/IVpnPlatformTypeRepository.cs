﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVpnPlatformTypeRepository : IRepository<LkVpnPltfrmType>
    {
        IQueryable<LkVpnPltfrmType> Find(Expression<Func<LkVpnPltfrmType, bool>> predicate);
    }
}