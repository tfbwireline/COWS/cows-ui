﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IServiceTierRepository : IRepository<LkMdsSrvcTier>
    {
        IQueryable<LkMdsSrvcTier> Find(Expression<Func<LkMdsSrvcTier, bool>> predicate);
    }
}