﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IAdEventAccessTagRepository : IRepository<AdEventAccsTag>
    {
        IQueryable<AdEventAccsTag> Find(Expression<Func<AdEventAccsTag, bool>> predicate);

        AdEventAccsTag GetById(int eventId, string cktId);
    }
}