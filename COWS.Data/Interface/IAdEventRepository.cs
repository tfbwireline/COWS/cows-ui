﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IAdEventRepository : IRepository<AdEvent>
    {
        AdEvent GetById(int id, string adid);

        IQueryable<AdEvent> Find(Expression<Func<AdEvent, bool>> predicate);
    }
}