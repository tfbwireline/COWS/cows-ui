﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ISprintHolidayRepository : IRepository<LkSprintHldy>
    {
        IQueryable<LkSprintHldy> Find(Expression<Func<LkSprintHldy, bool>> predicate);
    }
}