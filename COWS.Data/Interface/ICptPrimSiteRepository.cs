﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptPrimSiteRepository : IRepository<LkCptPrimSite>
    {
        IQueryable<LkCptPrimSite> Find(Expression<Func<LkCptPrimSite, bool>> predicate);
    }
}