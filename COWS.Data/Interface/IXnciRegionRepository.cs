﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IXnciRegionRepository : IRepository<LkXnciRgn>
    {
        IQueryable<LkXnciRgn> Find(Expression<Func<LkXnciRgn, bool>> predicate);
    }
}