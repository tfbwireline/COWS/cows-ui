﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOdieRspnInfoRepository : IRepository<OdieRspnInfo>
    {
        IQueryable<OdieRspnInfo> Find(Expression<Func<OdieRspnInfo, bool>> predicate);
        List<RedsgnDevicesInfo> GetRedesignDevicesInfo(int _iReqID);
    }
}
