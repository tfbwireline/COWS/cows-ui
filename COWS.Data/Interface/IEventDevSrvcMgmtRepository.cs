﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventDevSrvcMgmtRepository
    {
        IEnumerable<GetEventDevSrvcMgmt> GetEventDevSrvcMgmtByEventId(int eventId);

        void AddDevMgmtTbl(ref List<GetEventDevSrvcMgmt> data, int eventId);
        void Create(IEnumerable<EventDevSrvcMgmt> entity);
        void SetInactive(Expression<Func<EventDevSrvcMgmt, bool>> predicate);
    }
}