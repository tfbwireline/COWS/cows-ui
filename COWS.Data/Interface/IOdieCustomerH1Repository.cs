﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IOdieCustomerH1Repository
    {
        IEnumerable<OdieCustomerH1> GetOdieCustomerH1Data();
    }
}
