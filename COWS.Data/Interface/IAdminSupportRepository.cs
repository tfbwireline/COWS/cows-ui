﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IAdminSupportRepository
    {
        DataSet GetEventByEventId(int eventId, int csgLvlId);
        DataSet GetOrderByFtn(string ftn, int csgLvlId);
        DataSet GetCPTOrRedesign(string cptNo, int csgLvlId, byte isRedesign);
        int DeleteOrder(int orderId);
        string ExtractOrder(int m5OrderId, int relatedM5OrderId, string orderType, string deviceId, bool isTransaction);
        DataSet GetBPM(string h6);
    }
}