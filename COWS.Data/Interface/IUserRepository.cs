﻿using COWS.Data.Extensions;
using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUserRepository : IRepository<LkUser>
    {
        IEnumerable<LkUser> GetAllUsers();

        List<LkCnfrcBrdg> GetCnfrcBrdgCreatByUser(int userId);

        IQueryable<LkUser> Find(Expression<Func<LkUser, bool>> predicate, bool includeNtePool = false);

        bool IsCSGLevelUser(string adid, int csgLevel);

        IQueryable<LkUser> GetUsersByProfileName(string profileName);

        IEnumerable<LkUser> GetEventActivator(string type, DynamicParameters param);

        LkUsrPrf GetFinalUserProfile(int userId, int eventTypeId, bool IS_EVENT = true);

        byte GetCsgLevel(int userId);

        string GetCsgLevelCD(int userId);

        List<int> GetActiveProfileIdByEventTypeId(int userId, int eventTypeId);

        bool UserActiveWithProfileName(int userId, string profileName);

        DataTable GetConferenceBridgeInfo();

        DataTable GetSDEViewUsers();

        int UpdateLegacyUserADID(string user_adid, string old_user_adid, string ntid_email);
    }
}