﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ILockRepository<T> where T : class
    {
        T GetById(int id);

        IQueryable<T> Find(Expression<Func<T, bool>> predicate);

        T CheckLock(int id);

        int Lock(T entity);

        void Unlock(int id, int userId);

        int SaveAll();
    }
}