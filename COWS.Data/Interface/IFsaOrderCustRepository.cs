﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IFsaOrderCustRepository : IRepository<FsaOrdrCust>
    {
        IQueryable<FsaOrdrCust> Find(Expression<Func<FsaOrdrCust, bool>> predicate);
        FsaOrdrCust GetByOrderIdAndCISLevelType(int id, string cisLvlType, int csgLvlId);
    }
}