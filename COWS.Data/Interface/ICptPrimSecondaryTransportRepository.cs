﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptPrimSecondaryTransportRepository : IRepository<LkCptPrimScndyTprt>
    {
        IQueryable<LkCptPrimScndyTprt> Find(Expression<Func<LkCptPrimScndyTprt, bool>> predicate);
    }
}