﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IWorldHolidayRepository : IRepository<LkWrldHldy>
    {
        IQueryable<LkWrldHldy> Find(Expression<Func<LkWrldHldy, bool>> predicate);
    }
}