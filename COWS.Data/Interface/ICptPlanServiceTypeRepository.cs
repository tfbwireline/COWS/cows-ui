﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptPlanServiceTypeRepository : IRepository<LkCptPlnSrvcType>
    {
        IQueryable<LkCptPlnSrvcType> Find(Expression<Func<LkCptPlnSrvcType, bool>> predicate);
    }
}