﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IStateRepository : IRepository<LkUsStt>
    {
        IQueryable<LkUsStt> Find(Expression<Func<LkUsStt, bool>> predicate);
    }
}