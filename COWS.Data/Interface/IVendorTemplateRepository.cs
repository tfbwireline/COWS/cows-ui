﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVendorTemplateRepository : IRepository<VndrTmplt>
    {
        IQueryable<VndrTmplt> Find(Expression<Func<VndrTmplt, bool>> predicate);
    }
}
