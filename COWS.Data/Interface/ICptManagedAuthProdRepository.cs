﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptManagedAuthProdRepository : IRepository<LkCptMngdAuthPrd>
    {
        IQueryable<LkCptMngdAuthPrd> Find(Expression<Func<LkCptMngdAuthPrd, bool>> predicate);
    }
}