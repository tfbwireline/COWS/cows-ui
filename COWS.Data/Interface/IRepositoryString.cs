﻿using System.Collections.Generic;

namespace COWS.Data.Interface
{
    public interface IRepositoryString<T> where T : class
    {
        IEnumerable<T> GetAll();

        T GetById(string id);

        T Create(T entity);

        void Update(string id, T entity);

        void Delete(string id);

        int SaveAll();
    }
}