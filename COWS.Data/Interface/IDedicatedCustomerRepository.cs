﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IDedicatedCustomerRepository : IRepository<LkDedctdCust>
    {
        IQueryable<LkDedctdCust> Find(Expression<Func<LkDedctdCust, bool>> predicate);
    }
}