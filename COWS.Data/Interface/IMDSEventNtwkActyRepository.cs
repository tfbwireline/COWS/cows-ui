﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSEventNtwkActyRepository : IRepository<MdsEventNtwkActy>
    {
        IQueryable<MdsEventNtwkActy> Find(Expression<Func<MdsEventNtwkActy, bool>> predicate);

        IEnumerable<MdsEventNtwkActy> GetMDSEventNtwkActyByEventId(int eventId);

        void AddNtwkActy(ref List<MdsEventNtwkActy> data, int eventId);

        void Create(List<MdsEventNtwkActy> entity);
        void Delete(List<MdsEventNtwkActy> entity);
    }
}