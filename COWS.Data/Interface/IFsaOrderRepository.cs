﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IFsaOrderRepository : IRepository<FsaOrdr>
    {
        IQueryable<FsaOrdr> Find(Expression<Func<FsaOrdr, bool>> predicate);
        FsaOrdr GetTransportData(int id, bool isTerminating);
        FSADisconnect GetFSADisconnectDetails(int id);
        void UpdateVendorAccessCostInfo(int id, FsaOrdr entity);
        IQueryable<FsaOrdr> GetFsaOrderCpeLineItem(Expression<Func<FsaOrdr, bool>> predicate);
    }
}