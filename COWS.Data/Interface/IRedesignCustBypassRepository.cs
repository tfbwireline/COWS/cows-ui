﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IRedesignCustBypassRepository : IRepository<RedsgnCustBypass>
    {
        IQueryable<RedsgnCustBypass> Find(Expression<Func<RedsgnCustBypass, bool>> predicate);
        DataTable GetCustomersBypass();
        List<string> GetCustomersBypassByID(int ID, int csgLvlId);
        int CreateCustomerByPass(RedsgnCustBypass _cust, int loggedInUserId);
        bool GetRedesignCustomerBypassCD(string H1_CD, string Cust_Name, int csgLvlId);

        bool CheckRedesignCustomerBypassCDIfExist(string H1_CD, int ID);
    }
}
