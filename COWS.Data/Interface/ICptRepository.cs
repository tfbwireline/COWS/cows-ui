﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptRepository : IRepository<Cpt>
    {
        Cpt GetById(int id, string adid);
        IQueryable<Cpt> Find(Expression<Func<Cpt, bool>> predicate);
        IEnumerable<GetAllCptView> GetAllCpt(byte csgLevelId, byte searchView, string adid = null);
        DataTable GetCustomerH1Data(string h1);
        bool ValidateShortNameByLinkedServer(string h1, string shortName);
        List<CptUser> GetAssignmentByProfileId(short usrPrfId);
        List<CptAssignment> GetCptAssignmentStats();
        List<CptAssignment> GetCptCancelStats(byte csgLevelIdLoggedInUser);
        void CreateCptHistory(CptHist hist);
        bool IsRedesignExist(int cptId, string shrtNme);
        string CreateRedesign(string shrtNme, string h1, string redsgnCat, int cptId);
        Cpt GetSecuredData(Cpt entity);
    }
}