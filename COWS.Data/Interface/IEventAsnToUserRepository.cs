﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventAsnToUserRepository : IRepository<EventAsnToUser>
    {
        IQueryable<EventAsnToUser> Find(Expression<Func<EventAsnToUser, bool>> predicate);

        List<EventAsnToUser> Create(List<EventAsnToUser> list);

        void Update(int id, List<EventAsnToUser> list);

        bool AccountChange(List<EventAsnToUser> assignUsers, int iEventID);
    }
}