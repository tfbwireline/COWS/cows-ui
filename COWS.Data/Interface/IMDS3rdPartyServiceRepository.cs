﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDS3rdPartyServiceRepository : IRepository<LkMds3rdpartySrvcLvl>
    {
        IQueryable<LkMds3rdpartySrvcLvl> Find(Expression<Func<LkMds3rdpartySrvcLvl, bool>> predicate);
    }
}