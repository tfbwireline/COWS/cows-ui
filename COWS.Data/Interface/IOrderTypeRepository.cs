﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderTypeRepository : IRepository<LkOrdrType>
    {
        IQueryable<LkOrdrType> Find(Expression<Func<LkOrdrType, bool>> predicate);
    }
}