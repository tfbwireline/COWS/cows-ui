﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace COWS.Data.Interface
{
    public interface IVendorOrderRepository : IRepository<VndrOrdr>
    {
        IQueryable<VndrOrdr> Find(Expression<Func<VndrOrdr, bool>> predicate);
        IQueryable<LkVndrEmailType> GetVendorOrderEmailType();
        IQueryable<VndrOrdrEmailAtchmt> GetEmailAttachment(int vendorOrderId);
        IQueryable<VndrOrdr> GetVendorOrder(Expression<Func<VndrOrdr, bool>> predicate);
        IEnumerable<OffnetForm> GetVendorOrderFormDynamic(int orderId);
        DataSet GetVendorOrders(int vendorOrderID, int h5FolderCustID, string ftn);
        //Task<DataTable> GetVendorOrders(int vendorOrderID, int h5FolderCustID, string ftn);
        bool IsOrderCancelled(int orderId);
        VndrOrdr GetForVendorOrderPage(int id, int userCsgLvl);
        Ordr CreateOrder(Ordr entity);
    }
}
