﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IQualificationRepository : IRepository<LkQlfctn>
    {
        IQueryable<LkQlfctn> Find(Expression<Func<LkQlfctn, bool>> predicate);
        IEnumerable<GetQualificationView> GetQualificationView(int userID);
        LkQlfctn Create(GetQualificationView entity);
        void Update(int id, GetQualificationView entity);
        bool CheckDuplicate(int assignedUserId);
    }
}