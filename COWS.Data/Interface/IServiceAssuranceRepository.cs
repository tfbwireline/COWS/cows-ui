﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IServiceAssuranceRepository : IRepository<LkSrvcAssrnSiteSupp>
    {
        IQueryable<LkSrvcAssrnSiteSupp> Find(Expression<Func<LkSrvcAssrnSiteSupp, bool>> predicate);
    }
}