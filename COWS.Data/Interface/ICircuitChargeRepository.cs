﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICircuitChargeRepository : IRepository<OrdrCktChg>
    {
        List<OrdrCktChg> GetAddlCharges(int OrderID, bool IsTerm);
        List<CircuitAddlCost> GetAddlChargeHistory(int OrderID, int chargeTypeID, bool IsTerm);
        List<CircuitAddlCost> GetAddlCostTypes();
        List<CircuitAddlCost> GetStatus();
        List<CircuitAddlCost> GetCurrencyList();
        int AcrOrderStatus(int orderID, bool isTerm);
        IQueryable<OrdrCktChg> Find(Expression<Func<OrdrCktChg, bool>> predicate);
        IEnumerable<OrdrCktChg> Create(IEnumerable<OrdrCktChg> entity);
        void Delete(IEnumerable<OrdrCktChg> entity);
        bool LoadRTSTask(int orderId);
        void SetJeopardy(int orderId, int noteId, string jepCode);
        bool LoadNCITask(int orderId, short taskID);
        bool InsertGOMBillingTask(int orderID);
    }
}