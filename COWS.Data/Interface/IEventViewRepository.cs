﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace COWS.Data.Interface
{
    public interface IEventViewRepository
    {
        IEnumerable<GetEventViews> GetEventViews(int userID, int siteCntnt);

        Task<DataTable> GetEventViewDetailsAsync(int viewId, int siteCntnt, int userId, string newMDS, byte userCsgLevel);

        DataTable GetCalendarViewDetails(int viewID, int eID, DateTime? date, string type, string user, string group);

        IEnumerable<GetEventViewColumns> GetEventViewColumns(int grpId, int siteCntnt);

        DataSet GetEventViewInfo(int viewId);

        bool DeleteDsplViewColsByViewId(int viewId);

        IEnumerable<LkFiltrOpr> GetFilterOperators();

        bool AddEventView(int _iUserID, string _sViewName, int _idSiteCntnt, string _cDfltFlag, string _cPblFlag, string _sFltColOpr1, string _sFltColOpr2, int _iFltCol1, int _iFltCol2, string _sFltColTxt1Val,
          string _sFltColTxt2Val, string _cFltFlag, int _iSortCol1, string _sSortCol1Flag, int _iSortCol2, string _sSortCol2Flag, string _sFltLogicOpr, string _sColIDs, string _sPos);

        bool UpdateEventView(int _iViewID, int _iUserID, string _sViewName, int _idSiteCntnt, string _cDfltFlag, string _cPblFlag, string _sFltColOpr1, string _sFltColOpr2, int _iFltCol1, int _iFltCol2, string _sFltColTxt1Val,
            string _sFltColTxt2Val, string _cFltFlag, int _iSortCol1, string _sSortCol1Flag, int _iSortCol2, string _sSortCol2Flag, string _sFltLogicOpr, string _sColIDs, string _sPos);
    }
}