﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Interface
{
    public interface IProfileHierarchyRepository : IRepository<LkPrfHrchy>
    {
        IQueryable<LkPrfHrchy> Find(Expression<Func<LkPrfHrchy, bool>> predicate);
    }
}
