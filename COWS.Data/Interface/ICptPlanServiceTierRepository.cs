﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptPlanServiceTierRepository : IRepository<LkCptPlnSrvcTier>
    {
        IQueryable<LkCptPlnSrvcTier> Find(Expression<Func<LkCptPlnSrvcTier, bool>> predicate);
    }
}