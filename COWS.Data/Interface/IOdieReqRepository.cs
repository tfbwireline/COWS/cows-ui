﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOdieReqRepository : IRepository<OdieReq>
    {
        IQueryable<OdieReq> Find(Expression<Func<OdieReq, bool>> predicate);
        OdieReq Request(int? orderId, int odieMessageId, string h1, string customerName, bool insertCompleted, string odieCustomerId, byte csgLvlId, int? odieCustomerInfoCategoryTypeId = null, string deviceFilter = "");
        int CreateODIEDiscoRequest(string h1, List<EventDiscoDev> discoOElist);
        DataTable GetODIECustNames(IEnumerable<OdieCustRspnModel> odCustResp, int csgLvlID);
        IEnumerable<OdieCustRspnModel> GetODIECustNameResp(int _iReqID, string _sH1);
        int InsertOdieShortNameRequest(int cptID, string h1, string custName, byte csgLvlID);
    }
}