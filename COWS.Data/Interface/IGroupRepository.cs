﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IGroupRepository : IRepository<LkGrp>
    {
        IQueryable<LkGrp> Find(Expression<Func<LkGrp, bool>> predicate);
    }
}