﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace COWS.Data.Interface
{
    public interface ICcdRepository
    {
        IEnumerable<LkCcdMissdReas> GetCcdReason();
        IEnumerable<CcdHist> GetCcdHistory(int orderId);
        DataTable SearchCcd(string m5, int h5, byte bypass);
        bool UpdateCcd(string orderId, DateTime newCcd, string reason, string notes, int userId);
    }
}
