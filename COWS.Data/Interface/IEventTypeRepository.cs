﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IEventTypeRepository : IRepository<LkEventType>
    {
        IQueryable<LkEventType> Find(Expression<Func<LkEventType, bool>> predicate);
    }
}