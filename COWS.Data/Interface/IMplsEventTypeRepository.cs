﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMplsEventTypeRepository : IRepository<LkMplsEventType>
    {
        IQueryable<LkMplsEventType> Find(Expression<Func<LkMplsEventType, bool>> predicate);
    }
}