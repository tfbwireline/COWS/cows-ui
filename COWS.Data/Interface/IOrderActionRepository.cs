﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderActionRepository : IRepository<LkOrdrActn>
    {
        IQueryable<LkOrdrActn> Find(Expression<Func<LkOrdrActn, bool>> predicate);
    }
}