﻿using COWS.Entities.QueryModels;
using System.Data;

namespace COWS.Data.Interface
{
    public interface ICalendarRepository
    {
        //IEnumerable<CalendarData> GetCalendarData(int isWFCalendar = 1, int userId = 0, int apptTypeId = 0, int csgLvlId = 0);
        //bool UpdateCalendarData(int apptId, DateTime strtTmst, DateTime EndTmst, string rcurncDesTxt, string subjTxt, string des, string apptLocTxt, int apptTypeId, int creatByUserId, int modfdByUserId, string asnToUserIdListTxt, int rcurncCd, int delFlg);
        bool CreateUpdateCalendarData(CalendarData model);

        DataSet GetApptTypes(int userId, byte isMain, byte isWF);

        DataSet GetAppointmentData(int isWFCalendar = 1, int userId = 0, int apptTypeId = 0, int csgLvlId = 0);
        DataSet GetAppointmentDetailsByApptID(int ApptID);
    }
}