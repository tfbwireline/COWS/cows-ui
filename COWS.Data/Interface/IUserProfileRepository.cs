﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUserProfileRepository : IRepository<LkUsrPrf>
    {
        IQueryable<LkUsrPrf> Find(Expression<Func<LkUsrPrf, bool>> predicate);

        IEnumerable<UserProfiles> GetUserProfilesByUserId(int userID, int loggedInUserId);
    }
}