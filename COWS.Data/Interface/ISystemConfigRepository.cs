﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ISystemConfigRepository : IRepository<LkSysCfg>
    {
        IQueryable<LkSysCfg> Find(Expression<Func<LkSysCfg, bool>> predicate);

        LkSysCfg GetByName(string sParmNme);
    }
}
