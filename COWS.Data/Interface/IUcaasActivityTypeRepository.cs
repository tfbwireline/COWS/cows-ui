﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUcaasActivityTypeRepository : IRepository<LkUcaaSActyType>
    {
        IQueryable<LkUcaaSActyType> Find(Expression<Func<LkUcaaSActyType, bool>> predicate);
    }
}