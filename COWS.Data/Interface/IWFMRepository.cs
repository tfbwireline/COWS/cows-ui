﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;
using System.Data;

namespace COWS.Data.Interface
{
    public interface IWFMRepository
    {
        bool GetWFMRole(int userId, string profile);
        DataTable GetWFMprofile(int userId, string profile);
        DataTable GetNCIUser(int prfId);
        bool UserValid(string userId, string prodType, string pltfrmType, string subStatus);
        //List<WFMOrdersModel> Select(string SortField);
        List<WFMOrdersModel> Select(string sortExpression, string searchCriteria, byte view, string profile);
        DataSet GetWFMData(string sortExpression, string searchCriteria, byte view, string profile);
        void MakeNCIAssignments(WFMOrdersModel _WFMOrder);
        void MakeNCIUnAssignments(WFMOrdersModel _WFMOrder);
        void MakeGOMAssignments(WFMOrdersModel _WFMOrder);
        void MakeGOMUnAssignments(WFMOrdersModel _WFMOrder);
        int InsertOrderNotes(int orderID, int noteTypeID, int userID, string notes);
        decimal GetXNCIOrderWeightageNumber(WFMOrdersModel _WFMOrder);
        bool GetUserAssignmentColorCD(int orderID, int wgID, int userID);
        bool UpdateUserAssignmentColorCD(int orderID, int wgID, int userID);
    }
}