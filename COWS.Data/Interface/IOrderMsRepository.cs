﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOrderMsRepository : IRepository<OrdrMs>
    {
        IQueryable<OrdrMs> Find(Expression<Func<OrdrMs, bool>> predicate);
        int UpdateBillClearDate(int orderId, DateTime? billClearDate);
    }
}
