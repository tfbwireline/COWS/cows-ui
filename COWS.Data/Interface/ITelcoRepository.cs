﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ITelcoRepository : IRepository<LkTelco>
    {
        IQueryable<LkTelco> Find(Expression<Func<LkTelco, bool>> predicate);
    }
}