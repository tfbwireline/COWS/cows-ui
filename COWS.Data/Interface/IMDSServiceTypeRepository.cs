﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSServiceTypeRepository : IRepository<LkMdsSrvcType>
    {
        IQueryable<LkMdsSrvcType> Find(Expression<Func<LkMdsSrvcType, bool>> predicate);
    }
}