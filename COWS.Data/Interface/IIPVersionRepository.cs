﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IIPVersionRepository : IRepository<LkIpVer>
    {
        IQueryable<LkIpVer> Find(Expression<Func<LkIpVer, bool>> predicate);
    }
}