﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IRedesignWorkflowRepository : IRepository<RedsgnWrkflw>
    {
        IQueryable<RedsgnWrkflw> Find(Expression<Func<RedsgnWrkflw, bool>> predicate);
    }
}
