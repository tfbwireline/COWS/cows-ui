﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICurrencyListRepository : IRepository<LkCur>
    {
        IQueryable<LkCur> Find(Expression<Func<LkCur, bool>> predicate);
        List<LkCur> GetCurrencyList(string SortField, string SearchCriteria);
    }
}