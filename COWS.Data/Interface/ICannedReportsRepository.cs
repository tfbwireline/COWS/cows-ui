﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Data.Interface
{
    public interface ICannedReportsRepository
    {
        IEnumerable<Report> GetCannedReportByUserId(string reportType, int userId, int CSGLevel);
        CannedFile GetCannedFilesByReportID(int reportId, int CSGLevel, string reportType);
        CannedFile GetCannedFilesByReportID(int reportId, int CSGLevel, string reportType, DateTime startDate, DateTime endDate);

        byte[] GetCannedReportFile(string filePath);
    }
}
