﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface INccoOrderRepository : IRepository<NccoOrdr>
    {
        IQueryable<NccoOrdr> Find(Expression<Func<NccoOrdr, bool>> predicate);

        DataTable GetNCCOWGDetails(int iOrderID);

        IEnumerable<NccoOrderView> Select(string SortExpression, string SearchCriteria, int CsgLvlId, int UserId);

        NccoOrderView GetNCCOOrderData(int orderID, int csgLvlId);

        int insertIntoOrderTable(int csgLvlId, int userId);

        bool LoadSM(int orderid, int taskid, int taskstatus);

        bool InsertOrderNotes(int OrderID, int NoteTypeID, int UserID, string Notes);

        bool InsertJeopardy(int orderID, string jpdyCD, string notes, byte noteTypeID, int CreatedBy);

        bool CompleteTask(int iOrderID, int iTaskID, int iTaskStatus, string sComments, int iUserID, int noteTypeID);

        bool UpdateH5FolderID(NccoOrderView _ncco, int userId);

        bool InsertH5Folder(NccoOrderView _ncco);

        bool insertPLSequence(int ordID, string plNo, List<String> seqNos, int USER_ID);

        bool UpdateNCCOOrderToCancel(int ordrID, NccoOrderView _ncco, int USER_ID);

        DataTable GetNCCOWGDetails2(int iOrderID);

        bool VerifyOrderInPendingStatus(int _ordrID);

        bool VerifyOrder(int _ordrID);

        byte GetOrderSecurityDetails(int orderID);

        string GetH5CustomerName(NccoOrderView ncco, int CsgLvlID);

        bool UpdatePreviousComments(int orderID, string comments);

        bool InsertOrderNotes(OrdrNte onte);

        bool CompleteCCDTask(int orderID);

        bool UpdateNCCOOrderTable(NccoOrderView _ncco);

        bool MoveFromGOMFOtoXNCI(int ordrID, int userId);

        bool ResendEmailOnCancel(int _ordrID);

        int CompleteGOMIBillTask(int orderID);
    }
}