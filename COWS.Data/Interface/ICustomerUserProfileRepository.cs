﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICustomerUserProfileRepository : IRepository<CustomerUserProfileView>
    {
        IQueryable<CustomerUserProfileView> Find(Expression<Func<CustomerUserProfileView, bool>> predicate);

        int CreateH5CustomerToUserAssignment(CustomerUserProfileView entity);

        string GetCountryName(string sCtryCd);

        bool SendEmailNtfy(CustomerUserProfileView entity);

        IEnumerable<CustomerUserProfileView> GetAllH5Customer();

        bool CheckH5(int _custid);
    }
}