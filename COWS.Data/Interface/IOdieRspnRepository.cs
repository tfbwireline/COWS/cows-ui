﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IOdieRspnRepository : IRepository<OdieRspn>
    {
        IQueryable<OdieRspn> Find(Expression<Func<OdieRspn, bool>> predicate);

        IEnumerable<OdieRspn> Response(int requestId, byte csgLvlId);

        IEnumerable<GetOdieDiscoResponse> GetOdieDiscoResponse(int requestId);

        DataTable GetCustNameDetails(int RspnID, int csgLvlId);
    }
}