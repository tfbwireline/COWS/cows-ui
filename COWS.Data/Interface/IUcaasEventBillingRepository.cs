﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IUcaasEventBillingRepository : IRepository<UcaaSEventBilling>
    {
        IQueryable<UcaaSEventBilling> Find(Expression<Func<UcaaSEventBilling, bool>> predicate);
        void Delete(Expression<Func<UcaaSEventBilling, bool>> predicate);
        void Create(IEnumerable<UcaaSEventBilling> entity);
    }
}