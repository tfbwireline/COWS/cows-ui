﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICptProvisionTypeRepository : IRepository<LkCptPrvsnType>
    {
        IQueryable<LkCptPrvsnType> Find(Expression<Func<LkCptPrvsnType, bool>> predicate);
    }
}