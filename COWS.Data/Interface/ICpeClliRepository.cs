﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace COWS.Data.Interface
{
    public interface ICpeClliRepository : IRepository<CpeClli>
    {
        IQueryable<CpeClli> Find(Expression<Func<CpeClli, bool>> predicate);
        DataTable GetAllRequestData(string stusID, string clliId, string zipCd);

        string getClliID(string cityNme, string stateCode);

        bool checkClliIdIfExist(int cpeClliId, string clliId);
    }
}
