﻿using System.Data;

namespace COWS.Data.Interface
{
    public interface IActivatorsRepository
    {
        DataTable GetAllSuccActy(int eventTypeId);

        DataTable GetAllFailActy(int eventTypeId);

        DataTable GetAllFailCodes();
    }
}