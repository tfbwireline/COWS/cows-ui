﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMDSEventSlnkWiredTrptRepository : IRepository<MdsEventSlnkWiredTrpt>
    {
        IQueryable<MdsEventSlnkWiredTrpt> Find(Expression<Func<MdsEventSlnkWiredTrpt, bool>> predicate);

        IEnumerable<MdsEventSlnkWiredTrpt> GetMDSEventSlnkWiredTrptByEventId(int eventId);

        void AddWiredTrptTbl(ref List<MdsEventSlnkWiredTrpt> data, int eventId);
        void Create(IEnumerable<MdsEventSlnkWiredTrpt> entity);
        void SetInactive(Expression<Func<MdsEventSlnkWiredTrpt, bool>> predicate);
        void UpdatePMFlagToNRM(int eventId, string H6, string NUA, bool bDisc);
    }
}