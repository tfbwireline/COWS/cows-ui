﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IVendorOrderFormRepository : IRepository<VndrOrdrForm>
    {
        IQueryable<VndrOrdrForm> Find(Expression<Func<VndrOrdrForm, bool>> predicate);
    }
}
