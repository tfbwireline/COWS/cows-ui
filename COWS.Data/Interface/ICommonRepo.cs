﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using CSGLevel = COWS.Entities.QueryModels.CSGLevel;

namespace COWS.Data.Interface
{
    public interface ICommonRepository
    {
        IEnumerable<GetEncryptValues> GetEventEncryptValues(SecuredData entity);
        IEnumerable<GetEncryptValues> GetEncryptValues(string sDecrypt);
        byte[] GetEncryptValue(string sDecrypt);
        string GetDecryptValue(byte[] encryptVal);
        //string GetDecryptValue2(byte[] encryptVal);
        IEnumerable<GetEventDecryptValues> GetEventDecryptValues(int eventId, string eventType);
        void LogWebActivity(byte actyType, string actyVal, string userADID, byte userCSGLvl, byte objCSGLvl);
        void UpdateWebSession(string sUserADID, string sSessionId, string sIP);
        IEnumerable<GetEncryptValues> GetEventEncryptValuesSipt(SecuredData ec);
        int GetLengthLimit(object obj, string field);
        List<UserCsgLvl> GetUserSecurityGroupByADID(string adid);
        string GetCountryName(string sCtryCd);
        string GetSysCfgValue(string name);
        List<LkSysCfg> GetAllSysCfgByPrmtrValue(string value);
        bool GetAdminExcPerm(string name, string adid);
        int UpdateSysCfgValue(string name, string value);
        int GetCSGLevelAdId(string AdId);
        List<CSGLevel> GetM5CSGLvl(string sH1, string sH6, string sM5OrdrNbr);
        List<LkSysCfg> GetMenuCancelUserByADID(string adid);
        List<LkSysCfg> GetAllSysCfg();
        string CalculateDateWithSprintBusinessdays(DateTime dtStartTime, string sBusinessDays, bool bConcat = false, bool includeWeekend = false);
        bool IsSprintHoliday(string dtInput);
        string GetCowsAppCfgValue(string name);
        void UpdateNIDAtBPM(int orderID, string h6, string nidSerialNbr, int userId, int eventID);
        DataTable GetAllSrvcSiteSupport();
        bool IsEventEmailSent(int eventID, int emailReqType);
        //List<WorkflowStatus> GetMDSActStatuses(int sEvStatus, string sActType, bool isBizWifi);
        CustScrdData GetSecuredData(int id, int typeId);
        bool IsDeviceExcluded(string cmpntFmly);
        void UpdateRedesignFromEvent(List<EventDevCmplt> devcmplt, bool cmplDev,
            int eventID, List<string> toDevCompleteNames);
        IEnumerable<GetEncryptValues> GetEventEncryptValuesMds(SecuredData entity);
        List<object> GetCacheObject(string cacheKey, List<object> cacheObject);
        //List<WorkflowStatus> GetMDSActStatuses(int sEvStatus, string sActType);
        void UpdateRedesignFromEvent(List<EventDeviceCompletionView> devcmplt, bool cmplDev, int eventID, List<string> toDevCompleteNames);
        IQueryable<LkRole> FindRole(Expression<Func<LkRole, bool>> predicate);
    }
}
