﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface ICurrencyFileRepository : IRepository<SrcCurFile>
    {
        IQueryable<SrcCurFile> Find(Expression<Func<SrcCurFile, bool>> predicate);
        int UpdateCurrencyConverter(int userId);
    }
}