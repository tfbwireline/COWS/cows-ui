﻿using System.Collections.Generic;
using COWS.Entities.QueryModels;

namespace COWS.Data.Repository
{
    public interface IBillingDispatchRepository
    {
        IEnumerable<BillingDispatchData> Get();
        IEnumerable<BillingDispatchData> GetHistory();
        BillingDispatchData GetById(int id);
        int SaveAll();
        int Create(BillingDispatchData data);
    }
}