﻿using COWS.Entities.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace COWS.Data.Interface
{
    public interface IMplsEventRepository : IRepository<MplsEvent>
    {
        IQueryable<MplsEvent> Find(Expression<Func<MplsEvent, bool>> predicate, string adid = null);
    }
}