﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace COWS.Data.Extensions
{
    public static class LinqHelper
    {
        public static DataTable CopyToDataTable<T>(this IEnumerable<T> source)
        {
            return new ObjectShredder<T>().Shred(source, null, null);
        }

        public static DataTable CopyToDataTable<T>(this IEnumerable<T> source,
                                                    DataTable table, LoadOption? options)
        {
            return new ObjectShredder<T>().Shred(source, table, options);
        }
    }

    public class ObjectShredder<T>
    {
        private System.Reflection.FieldInfo[] _fi;
        private System.Reflection.PropertyInfo[] _pi;
        private System.Collections.Generic.Dictionary<string, int> _ordinalMap;
        private System.Type _type;
        private int ListCtr = 0;
        private bool bList = false;
        private Random objRand = new Random();

        // ObjectShredder constructor.
        public ObjectShredder()
        {
            _type = typeof(T);
            _fi = _type.GetFields();
            _pi = _type.GetProperties();
            _ordinalMap = new Dictionary<string, int>();
        }

        /// <summary>
        /// Loads a DataTable from a sequence of objects.
        /// </summary>
        /// <param name="source">The sequence of objects to load into the DataTable.</param>
        /// <param name="table">The input table. The schema of the table must match that
        /// the type T.  If the table is null, a new table is created with a schema
        /// created from the public properties and fields of the type T.</param>
        /// <param name="options">Specifies how values from the source sequence will be applied to
        /// existing rows in the table.</param>
        /// <returns>A DataTable created from the source sequence.</returns>
        public DataTable Shred(IEnumerable<T> source, DataTable table, LoadOption? options)
        {
            //complete db call, if needed
            //source = source.ToList();

            // Load the table from the scalar sequence if T is a primitive type.
            if (typeof(T).IsPrimitive)
            {
                return ShredPrimitive(source, table, options);
            }

            // Create a new table if the input table is null.
            if (table == null)
            {
                table = new DataTable(typeof(T).Name + objRand.Next(99, 999999).ToString());
            }

            // Initialize the ordinal map and extend the table schema based on type T.
            table = ExtendTable(table, typeof(T));

            // Enumerate the source sequence and load the object values into rows.
            table.BeginLoadData();
            using (IEnumerator<T> e = source.GetEnumerator())
            {
                while (e.MoveNext())
                {
                    if (options != null)
                    {
                        table.LoadDataRow(ShredObject(table, e.Current, 0), (LoadOption)options);
                    }
                    else
                    {
                        table.LoadDataRow(ShredObject(table, e.Current, 0), true);
                        if (bList == true)
                        {
                            for (int i = 1; i < ListCtr; i++)
                            {
                                table.LoadDataRow(ShredObject(table, e.Current, i), true);
                            }
                            bList = false;
                            ListCtr = 0;
                        }
                    }
                }
            }
            table.EndLoadData();

            // Return the table.
            return table;
        }

        public DataTable ShredPrimitive(IEnumerable<T> source, DataTable table, LoadOption? options)
        {
            // Create a new table if the input table is null.
            if (table == null)
            {
                table = new DataTable(typeof(T).Name + objRand.Next(99, 999999).ToString());
            }

            if (!table.Columns.Contains("Value"))
            {
                table.Columns.Add("Value", typeof(T));
            }

            // Enumerate the source sequence and load the scalar values into rows.
            table.BeginLoadData();
            using (IEnumerator<T> e = source.GetEnumerator())
            {
                Object[] values = new object[table.Columns.Count];
                while (e.MoveNext())
                {
                    values[table.Columns["Value"].Ordinal] = e.Current;

                    if (options != null)
                    {
                        table.LoadDataRow(values, (LoadOption)options);
                    }
                    else
                    {
                        table.LoadDataRow(values, true);
                    }
                }
            }
            table.EndLoadData();

            // Return the table.
            return table;
        }

        public object[] ShredObject(DataTable table, T instance, int iListIndx)
        {
            FieldInfo[] fi = _fi;
            PropertyInfo[] pi = _pi;

            if (instance.GetType() != typeof(T))
            {
                // If the instance is derived from T, extend the table schema
                // and get the properties and fields.
                ExtendTable(table, instance.GetType());
                fi = instance.GetType().GetFields();
                pi = instance.GetType().GetProperties();
            }

            // Add the property and field values of the instance to an array.
            Object[] values = new object[table.Columns.Count];
            foreach (FieldInfo f in fi)
            {
                values[_ordinalMap[f.Name]] = f.GetValue(instance);
            }

            foreach (PropertyInfo p in pi)
            {
                if (p.PropertyType.Name.Contains("List"))
                {
                    if (bList == false)
                    {
                        ListCtr = ((List<string>)p.GetValue(instance, null)).Count;
                        bList = true;
                    }
                    if (((List<string>)p.GetValue(instance, null)).Count > 0)
                        values[_ordinalMap[p.Name]] = ((List<string>)p.GetValue(instance, null))[iListIndx];
                    else
                        values[_ordinalMap[p.Name]] = string.Empty;
                }
                else
                {
                    values[_ordinalMap[p.Name]] = p.GetValue(instance, null);
                }
            }

            // Return the property and field values of the instance.
            return values;
        }

        public DataTable ExtendTable(DataTable table, Type type)
        {
            // Extend the table schema if the input table was null or if the value
            // in the sequence is derived from type T.
            foreach (FieldInfo f in type.GetFields())
            {
                if (!_ordinalMap.ContainsKey(f.Name))
                {
                    // Add the field as a column in the table if it doesn't exist
                    // already.
                    DataColumn dc = table.Columns.Contains(f.Name) ? table.Columns[f.Name]
                        : table.Columns.Add(f.Name, f.FieldType);

                    // Add the field to the ordinal map.
                    _ordinalMap.Add(f.Name, dc.Ordinal);
                }
            }
            foreach (PropertyInfo p in type.GetProperties())
            {
                if (!_ordinalMap.ContainsKey(p.Name))
                {
                    // Add the property as a column in the table if it doesn't exist
                    // already.
                    //DataColumn dc = table.Columns.Contains(p.Name) ? table.Columns[p.Name]
                    //    : table.Columns.Add(p.Name, p.PropertyType);

                    if (p.PropertyType.Name.Contains("List"))
                    {
                        DataColumn dc = table.Columns.Contains(p.Name) ? table.Columns[p.Name]
                        : table.Columns.Add(p.Name, typeof(string));
                        _ordinalMap.Add(p.Name, dc.Ordinal);
                    }
                    else
                    {
                        DataColumn dc = table.Columns.Contains(p.Name) ? table.Columns[p.Name]
                            : table.Columns.Add(p.Name, Nullable.GetUnderlyingType(
                                                        p.PropertyType) ?? p.PropertyType);
                        _ordinalMap.Add(p.Name, dc.Ordinal);
                    }

                    // Add the property to the ordinal map.
                }
            }

            // Return the table.
            return table;
        }
    }

    public static class LinqExtenions
    {
        public static Dictionary<TFirstKey, Dictionary<TSecondKey, TValue>> Pivot<TSource, TFirstKey, TSecondKey, TValue>(this IEnumerable<TSource> source, Func<TSource, TFirstKey> firstKeySelector, Func<TSource, TSecondKey> secondKeySelector, Func<IEnumerable<TSource>, TValue> aggregate)
        {
            var retVal = new Dictionary<TFirstKey, Dictionary<TSecondKey, TValue>>();

            var l = source.ToLookup(firstKeySelector);
            foreach (var item in l)
            {
                var dict = new Dictionary<TSecondKey, TValue>();
                retVal.Add(item.Key, dict);
                var subdict = item.ToLookup(secondKeySelector);
                foreach (var subitem in subdict)
                {
                    dict.Add(subitem.Key, aggregate(subitem));
                }
            }

            return retVal;
        }

        public static bool IsListEqual<T>(IList<T> sourceList, IList<T> targetList, IComparer<T> comparer) where T : IComparable<T>
        {
            /*if (sourceList.Count != targetList.Count)
            {
                return false;
            }*/
            int index = 0;
            while (index < sourceList.Count &&
                   comparer.Compare(sourceList[index], targetList[index]) == 0)
            {
                index++;
            }
            /*if (index != sourceList.Count)
            {
                return false;
            }*/
            if (index == 0)
                return false;
            else
                return true;
        }
    }

    public static class PredicateBuilder
    {
        public static Expression<Func<T, bool>> True<T>()
        {
            return f => true;
        }

        public static Expression<Func<T, bool>> False<T>()
        {
            return f => false;
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
                                                            Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
                                                             Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
        }
    }

    public static class XmlElementExtensions
    {
        /// <summary>
        /// Attempts to get and convert an attribute from a <see cref="XmlElement"/>. If the item does not exist or can't be converted a <paramref name="defaultValue"/> is returned.
        /// </summary>
        /// <typeparam name="T">The <see cref="Type"/> to try to convert to.</typeparam>
        /// <param name="element">The <see cref="XmlElement"/> to extract the attribute value from.</param>
        /// <param name="key">The key to use or the extraction</param>
        /// <param name="defaultValue">The default value if <paramref name="key"/> is not found.</param>
        /// <returns>The value from <paramref name="attributes"/> if <paramref name="key"/> exists; otherwise <paramref name="defaultValue"/>.</returns>
        public static T GetAttribute<T>(this XElement element, string key, T defaultValue)
        {
            if (element == null)
                return defaultValue;

            if (element.Attribute(key) == null)
                return defaultValue;

            var stringValue = element.Attribute(key).Value;

            var converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(T));

            if (!converter.CanConvertFrom(typeof(string)))
                return defaultValue;

            return (T)converter.ConvertFromString(stringValue);
        }

        public static List<string> GetAttribute<List>(this IEnumerable<XElement> elementlist, string key, List<string> defaultValue)
        {
            if (elementlist == null)
                return defaultValue;

            List<string> stringValue = new List<string>();// string.Empty;
            foreach (var element in elementlist)
            {
                if (element.Attribute(key).Value != string.Empty)
                    stringValue.Add(element.Attribute(key).Value);
            }
            //var converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(T));

            //if (!converter.CanConvertFrom(typeof(List<string>)))
            //    return defaultValue;
            return stringValue;
        }
    }

    internal static class ListExtensions
    {
        public static List<T> Filter<T>
            (this List<T> source, string columnName,
             string compValue, string condType)
        {
            ParameterExpression parameter = Expression.Parameter(typeof(T), "x");
            Expression property = Expression.Property(parameter, columnName);
            Expression constant = Expression.Constant(compValue);
            Expression equality = (condType == "==") ? Expression.Equal(property, constant) : Expression.NotEqual(property, constant);
            Expression<Func<T, bool>> predicate =
                Expression.Lambda<Func<T, bool>>(equality, parameter);

            Func<T, bool> compiled = predicate.Compile();
            return source.Where(compiled).ToList();
        }
    }

    public static class DTExtensions
    {
        public static DataTable Delete(this DataTable table, string filter)
        {
            table.Select(filter).Delete();
            table.AcceptChanges();
            return table;
        }

        public static void Delete(this IEnumerable<DataRow> rows)
        {
            foreach (var row in rows)
                row.Delete();
        }
    }

    public static class DSExtensions
    {
        public static string ToXml(this DataSet ds)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
        }
    }

    public static class DataTableExt
    {
        public static void ConvertColumnType(this DataTable dt, string columnName, Type newType)
        {
            using (DataColumn dc = new DataColumn(columnName + "_new", newType))
            {
                // Add the new column which has the new type, and move it to the ordinal of the old column
                int ordinal = dt.Columns[columnName].Ordinal;
                dt.Columns.Add(dc);
                dc.SetOrdinal(ordinal);

                // Get and convert the values of the old column, and insert them into the new
                foreach (DataRow dr in dt.Rows)
                    dr[dc.ColumnName] = Convert.ChangeType(dr[columnName], newType);

                // Remove the old column
                dt.Columns.Remove(columnName);

                // Give the new column the old column's name
                dc.ColumnName = columnName;
            }
        }
    }
}