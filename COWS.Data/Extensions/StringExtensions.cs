﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace COWS.Data.Extensions
{
    public static class StringBuilderExtns
    {
        public static StringBuilder AppendNoDup(this StringBuilder strb, string str)
        {
            return new StringBuilder(strb.ToString().Contains(str) ? strb.ToString() : strb.Append(str).ToString());
        }

        public static StringBuilder AppendNoDup(this StringBuilder strb, StringBuilder str)
        {
            return new StringBuilder(strb.ToString().Contains(str.ToString()) ? strb.ToString() : strb.Append(str).ToString());
        }
    }

    public static class StringExtns
    {
        public static string TidyUpEmailAddr(this string sEmail)
        {
            StringBuilder sbout = new StringBuilder();
            sEmail = sEmail.Replace(';', ',');
            string[] Emails = sEmail.Split(new char[] { ',' });
            foreach (string s in Emails)
            {
                if (s.Trim().Length > 0)
                {
                    if (((!(sbout.ToString().IndexOf(s.Trim(), 0, StringComparison.CurrentCultureIgnoreCase) != -1)) || (sbout.Length <= 0))
                        && (s.Trim().Contains("@")) && (s.Trim().Contains(".")))
                    {
                        if (sbout.Length <= 0)
                            sbout.Append(s.Trim());
                        else
                            sbout.Append(',').Append(s.Trim());
                    }
                }
            }
            return sbout.ToString();
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            if (source == null) return false;
            return source.IndexOf(toCheck, comp) >= 0;
        }

        private static string SerializeObject<T>(Encoding encoding, T obj)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();

                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, encoding))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(xmlTextWriter, obj);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                }

                return ByteArrayToString(encoding, memoryStream.ToArray());
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        public static string Serialize(object dataToSerialize)
        {
            if (dataToSerialize == null) return null;

            using (StringWriter stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer(dataToSerialize.GetType());
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
        }

        public static T Deserialize<T>(string xmlText)
        {
            if (String.IsNullOrWhiteSpace(xmlText)) return default(T);

            using (StringReader stringReader = new System.IO.StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
        }
    }
}