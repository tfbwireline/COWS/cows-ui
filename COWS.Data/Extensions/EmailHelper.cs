﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

namespace COWS.Data.Extensions
{
    public class EmailHelper
    {
        private readonly IConfiguration _configuration;

        public EmailHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //private static string sServerName = _configuration.GetSection("AppSettings:ServerName").Value;
        public bool SendSingleEncryptEmail(X509Certificate2 EncryptCert, string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody, List<string> attachments)
        {
            bool bResult = false;
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();

            if (sEmailTo.Length > 0)
            {
                string sServerName = _configuration.GetSection("AppSettings:ServerName").Value;
                SmtpClient sc = new SmtpClient(sServerName);
                MailMessage message = new MailMessage();
                try
                {
                    string[] sFrom = sEmailFrom.Split(new Char[] { ',' });
                    string[] sTo = sEmailTo.Split(new Char[] { ',' });
                    string[] sCC = sEmailCC.Split(new Char[] { ',' });

                    //string[] sBCC = sEmailBCC.Split(new Char[] { ',' });
                    /*Stream s;
                    StringBuilder Message = new StringBuilder();
                    MailMessage myMail = new MailMessage(sEmailFrom, sEmailTo);
                    SmtpClient sc = new SmtpClient(sServerName);
                    myMail.Subject = sSubject;
                    myMail.Body = sBody;
                    Message.AppendLine("content-type: multipart/mixed;");
                    Message.AppendLine(" boundary=\"----=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX\"");
                    Message.AppendLine();
                    Message.AppendLine("This is a multi-part message in MIME format.");
                    Message.AppendLine();
                    Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX");
                    Message.AppendLine("content-type: text/html; charset=\"us-ascii\"");
                    Message.AppendLine("content-transfer-encoding: 7bit");
                    Message.AppendLine();
                    Message.AppendLine(sBody);
                    Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX");
                    Message.AppendLine("content-disposition: attachment; filename=\"" + "x.txt" + "\"");
                    Message.AppendLine("content-transfer-encoding: base64");
                    Message.AppendLine("content-type: application/edine;\n name=\"" + "E_x.htm" + "\"");
                    Message.AppendLine();

                    if (attachments != null)
                    {
                        if (attachments.Count > 0)
                        {
                            foreach (KeyValuePair<string, byte[]> attch in attachments)
                            {
                                //myMail.Attachments.Add(new Attachment((Stream)attch.Value, attch.Key));
                                //Message.AppendLine(Convert.ToBase64String(attch.Value));
                                s = new MemoryStream(attch.Value);
                                myMail.Attachments.Add(new Attachment(s, attch.Key));
                            }
                        }
                    }

                    //Message.AppendLine(Convert.ToBase64String(Encoding.ASCII.GetBytes(data)));
                    string data = string.Empty;
                    try
                    {
                        data = File.ReadAllText(sFilePath);
                    }
                    catch
                    {
                        data = " Failed to add attachment to Mail. [CYP]";
                    }

                    Message.AppendLine(Convert.ToBase64String(Encoding.ASCII.GetBytes(data)));
                    Message.AppendLine();

                    Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX--");

                    byte[] BodyBytes = Encoding.ASCII.GetBytes(Message.ToString());

                    EnvelopedCms ECms = new EnvelopedCms(new ContentInfo(BodyBytes));

                    CmsRecipient Recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, EncryptCert);

                    ECms.Encrypt(Recipient);

                    byte[] EncryptedBytes = ECms.Encode();

                    MemoryStream ms1 = new MemoryStream(EncryptedBytes);
                    AlternateView av1 = new AlternateView(ms1, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m; content-transfer-encoding=Base64; content-disposition=attachment; fileName=smime.p7m;");

                    myMail.AlternateViews.Add(av1);

                    myMail.IsBodyHtml = true;
                    myMail.Priority = MailPriority.Normal;

                    sc.Send(myMail);*/

                    message.From = new MailAddress(sEmailFrom);
                    message.Subject = sSubject;

                    if (attachments != null && attachments.Count > 0)
                    {
                        StringBuilder buffer = new StringBuilder();
                        buffer.Append("MIME-Version: 1.0\r\n");
                        buffer.Append("Content-Type: multipart/mixed; boundary=unique-boundary-1\r\n");
                        buffer.Append("\r\n");
                        buffer.Append("This is a multi-part message in MIME format.\r\n");
                        buffer.Append("--unique-boundary-1\r\n");
                        buffer.Append("Content-Type: text/plain\r\n");  //could use text/html as well here if you want a html message
                        buffer.Append("Content-Transfer-Encoding: 7Bit\r\n\r\n");
                        buffer.Append(sBody);
                        if (!sBody.EndsWith("\r\n"))
                            buffer.Append("\r\n");
                        buffer.Append("\r\n\r\n");

                        foreach (string filename in attachments)
                        {
                            FileInfo fileInfo = new FileInfo(filename);
                            buffer.Append("--unique-boundary-1\r\n");
                            buffer.Append("Content-Type: application/octet-stream; file=" + fileInfo.Name + "\r\n");
                            buffer.Append("Content-Transfer-Encoding: base64\r\n");
                            buffer.Append("Content-Disposition: attachment; filename=" + fileInfo.Name + "\r\n");
                            buffer.Append("\r\n");
                            byte[] binaryData = File.ReadAllBytes(filename);

                            string base64Value = Convert.ToBase64String(binaryData, 0, binaryData.Length);
                            int position = 0;
                            while (position < base64Value.Length)
                            {
                                int chunkSize = 100;
                                if (base64Value.Length - (position + chunkSize) < 0)
                                    chunkSize = base64Value.Length - position;
                                buffer.Append(base64Value.Substring(position, chunkSize));
                                buffer.Append("\r\n");
                                position += chunkSize;
                            }
                            buffer.Append("\r\n");
                        }

                        sBody = buffer.ToString();
                    }
                    else
                    {
                        sBody = "Content-Type: text/plain\r\nContent-Transfer-Encoding: 7Bit\r\n\r\n" + sBody;
                    }

                    byte[] messageData = Encoding.ASCII.GetBytes(sBody);
                    ContentInfo content = new ContentInfo(messageData);
                    EnvelopedCms envelopedCms = new EnvelopedCms(content);
                    CmsRecipientCollection toCollection = new CmsRecipientCollection();
                    foreach (string address in sTo)
                    {
                        if (address != string.Empty)
                        {
                            message.To.Add(new MailAddress(address));
                            X509Certificate2 certificate = EncryptCert;
                            CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                            toCollection.Add(recipient);
                        }
                    }

                    foreach (string address in sCC)
                    {
                        if (address != string.Empty)
                        {
                            message.CC.Add(new MailAddress(address));
                            X509Certificate2 certificate = EncryptCert;
                            CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                            toCollection.Add(recipient);
                        }
                    }

                    //foreach (string address in sBCC)
                    //{
                    //    message.Bcc.Add(new MailAddress(address));
                    //    X509Certificate2 certificate = EncryptCert;
                    //    CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                    //    toCollection.Add(recipient);
                    //}

                    envelopedCms.Encrypt(toCollection);
                    byte[] encryptedBytes = envelopedCms.Encode();

                    //add digital signature:
                    /*SignedCms signedCms = new SignedCms(new ContentInfo(encryptedBytes));
                    X509Certificate2 signerCertificate = EncryptCert;
                    CmsSigner signer = new CmsSigner(SubjectIdentifierType.SubjectKeyIdentifier, signerCertificate);
                    signedCms.ComputeSignature(signer);
                    encryptedBytes = signedCms.Encode();*/

                    //end digital signature section

                    MemoryStream stream = new MemoryStream(encryptedBytes);
                    AlternateView view = new AlternateView(stream, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m; content-transfer-encoding=Base64; content-disposition=attachment; fileName=smime.p7m;");

                    //AlternateView view = new AlternateView(stream, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");
                    message.AlternateViews.Add(view);

                    sc.Send(message);
                    bResult = true;
                }
                catch (Exception ex) { bResult = false; }
                finally
                {
                    if ((message.Attachments != null) && (message.Attachments.Count > 0))
                    {
                        foreach (Attachment att in message.Attachments)
                        {
                            att.Dispose();
                        }
                    }
                    message.Dispose();
                    sc.Dispose();
                    if ((attachments != null) && (attachments.Count > 0))
                    {
                        foreach (string path in attachments)
                        {
                            int i = 0;
                            while ((File.Exists(path)) && (i < 10))
                            {
                                try
                                {
                                    File.Delete(path);
                                }
                                catch (Exception ex)
                                {
                                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace, string.Empty);
                                }
                                i++;
                            }
                        }
                    }
                }
            }
            return bResult;
        }

        public bool SendSingleEmail(string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody, List<string> attachments)
        {
            bool bResult = false;

            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();

            if (sEmailTo.Length > 0)
            {
                MailMessage myMail = new MailMessage(sEmailFrom, sEmailTo, sSubject, sBody);
                string sServerName = _configuration.GetSection("AppSettings:ServerName").Value;
                SmtpClient sc = new SmtpClient(sServerName);

                try
                {
                    if (sEmailCC.Length > 0)
                        myMail.CC.Add(sEmailCC);
                    if (sEmailBCC.Length > 0)
                        myMail.Bcc.Add(sEmailBCC);

                    myMail.IsBodyHtml = false;
                    myMail.Priority = MailPriority.Normal;
                    myMail.Subject = sSubject;
                    myMail.Body = sBody;
                    myMail.Body = Regex.Replace(sBody, @"(?<!\t)((?<!\r)(?=\n)|(?=\r\n))", "\t", RegexOptions.Multiline);
                    if (attachments != null)
                    {
                        if (attachments.Count > 0)
                        {
                            foreach (string attch in attachments)
                            {
                                //s = new MemoryStream(attch.Value);
                                //myMail.Attachments.Add(new Attachment(s, attch.Key));
                                //FileInfo fi = new FileInfo(attch);
                                myMail.Attachments.Add(new Attachment(attch));
                            }
                        }
                    }
                    sc.Send(myMail);
                    bResult = true;
                }
                catch (Exception ex)
                {
                    bResult = false;
                    System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                    info.Add("BodyFormat", ((myMail.IsBodyHtml) ? "Html" : "Text"));
                    info.Add("Priority", myMail.Priority.ToString());
                    info.Add("From", "[" + Convert.ToString(myMail.From) + "]");
                    info.Add("To", "[" + Convert.ToString(myMail.To) + "]");
                    info.Add("Cc", "[" + Convert.ToString(myMail.CC) + "]");
                    info.Add("Bcc", "[" + Convert.ToString(myMail.Bcc) + "]");
                    info.Add("Subject", myMail.Subject);
                    info.Add("Body", myMail.Body);
                    info.Add("Attach", myMail.Attachments[0].ToString());
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace, string.Empty);
                }
                finally
                {
                    if ((myMail.Attachments != null) && (myMail.Attachments.Count > 0))
                    {
                        foreach (Attachment att in myMail.Attachments)
                        {
                            att.Dispose();
                        }
                    }
                    myMail.Dispose();
                    sc.Dispose();
                    if ((attachments != null) && (attachments.Count > 0))
                    {
                        foreach (string path in attachments)
                        {
                            int i = 0;
                            while ((File.Exists(path)) && (i < 10))
                            {
                                try
                                {
                                    File.Delete(path);
                                }
                                catch (Exception ex)
                                {
                                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace, string.Empty);
                                }
                                i++;
                            }
                        }
                    }
                }
            }
            return bResult;
        }

        public bool SendSingleEmail(bool bIsHtml, MailPriority priority, string sEmailFrom, string sEmailTo, string sEmailCC, string sEmailBCC, string sSubject, string sBody)
        {
            bool bResult = false;

            sEmailFrom = sEmailFrom.TidyUpEmailAddr();
            sEmailTo = sEmailTo.TidyUpEmailAddr();
            sEmailCC = sEmailCC.TidyUpEmailAddr();
            sEmailBCC = sEmailBCC.TidyUpEmailAddr();
            MailMessage myMail = new MailMessage();
            try
            {
                if (sEmailTo.Length > 0)
                {
                    myMail = new MailMessage(sEmailFrom, sEmailTo, sSubject, sBody);
                    string sServerName = _configuration.GetSection("AppSettings:ServerName").Value;
                    SmtpClient sc = new SmtpClient(sServerName);
                    myMail.IsBodyHtml = bIsHtml;
                    myMail.Priority = priority;
                    myMail.Subject = sSubject;
                    myMail.Body = sBody;

                    if (sEmailCC.Length > 0)
                        myMail.CC.Add(sEmailCC);
                    if (sEmailBCC.Length > 0)
                        myMail.Bcc.Add(sEmailBCC);

                    sc.Send(myMail);
                    bResult = true;
                }
            }
            catch (Exception ex)
            {
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("BodyFormat", ((myMail.IsBodyHtml) ? "Html" : "Text"));
                info.Add("Priority", myMail.Priority.ToString());
                info.Add("From", "[" + Convert.ToString(myMail.From) + "]");
                info.Add("To", "[" + Convert.ToString(myMail.To) + "]");
                info.Add("Cc", "[" + Convert.ToString(myMail.CC) + "]");
                info.Add("Bcc", "[" + Convert.ToString(myMail.Bcc) + "]");
                info.Add("Subject", myMail.Subject);
                info.Add("Body", myMail.Body);
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace + " ;Info-" + info.ToQueryString(), string.Empty);
            }
            return bResult;
        }
    }
}