﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COWS.Data.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property) { return items.GroupBy(property).Select(x => x.First()); }
    }
}
