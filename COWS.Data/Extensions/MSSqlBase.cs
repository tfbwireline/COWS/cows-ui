﻿using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;

//using fl = LogManager.FileLogger;

namespace COWS.Data.Extensions
{
    //////////////////////////////////////////////////////////////////////////////
    public class MSSqlBaseData
    {
        public SqlConnection _conn = null;
        public DataSet _ds = null;
        public SqlCommand _cmd = null;
        public SqlTransaction _trans = null;
        public DataSet _dsMS = null;

        //////////////////////////////////////////////////////////////////////////////
        public MSSqlBaseData(bool blOpenConnection, string strConn)
        {
            if (blOpenConnection)
                //EncryptConfig();
                openAConnection(strConn);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void openAConnection(string strConn)
        {
            if (_conn == null)
            {
                _conn = new SqlConnection(strConn);
                _conn.Open();
            }
            else
            {
                if (_conn.State == ConnectionState.Closed)
                    try
                    {
                        _conn.Open();
                    }
                    catch
                    {
                        _conn = new SqlConnection(strConn);
                        _conn.Open();
                    }
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        public void closeAConnection()
        {
            if (_conn != null)
            {
                if (_conn.State == ConnectionState.Open)
                    //_conn.Close();
                    _conn.Dispose();
                _conn = null;
                //	GC.Collect();
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////
    public abstract class MSSqlBase
    {
        protected MSSqlBaseData _dbData;
        protected bool _blSuccess = true;
        protected bool _blPassThrough = false;
        public string err = string.Empty; // SQL Error Message

        //go430862,03/01/08,fetch the catch time out value
        public string _connStr = String.Empty;//ConfigurationManager.AppSettings["ConnectionString"];

        public string _connRptStr = String.Empty;
        private readonly IConfiguration _configuration;

        public MSSqlBase()
        {
            if ((_connStr == string.Empty) || (_connStr == null))
            {
                try
                {
                    _connStr = _configuration.GetConnectionString("SqlDbConn").ToString();
                }
                catch
                {
                    _connStr = _configuration.GetConnectionString("SqlDbConn").ToString();
                }
            }
            if (((_connRptStr == string.Empty) || (_connRptStr == null)) && (_configuration.GetConnectionString("COWSOnReporting") != null))
            {
                try
                {
                    _connRptStr = _configuration.GetConnectionString("COWSOnReporting").ToString();
                }
                catch
                {
                    _connRptStr = _configuration.GetConnectionString("COWSOnReporting").ToString();
                }
            }

            _dbData = new MSSqlBaseData(false, _connStr);
        }

        //////////////////////////////////////////////////////////////////////////////
        public MSSqlBase(MSSqlBaseData dataToUse)
        {
            if (dataToUse != null)
            {
                _blPassThrough = true;
                _dbData = dataToUse;
            }
            else
                _dbData = new MSSqlBaseData(true, _connStr);
        }

        //////////////////////////////////////////////////////////////////////////////
        public MSSqlBase(string spName, MSSqlBaseData dataToUse, CommandType cmdType)
        {
            if (dataToUse != null)
            {
                _blPassThrough = true;
                _dbData = dataToUse;
            }
            else
                _dbData = new MSSqlBaseData(true, _connStr);

            setCommand(spName);
            setAction(cmdType);
        }

        //////////////////////////////////////////////////////////////////////////////
        public MSSqlBaseData data
        {
            get { return _dbData; }
        }

        //////////////////////////////////////////////////////////////////////////////
        // set the name of the command, whether it is stored procedure or inline tsql
        public void setCommand(string spName)
        {
            _dbData._cmd = null;
            _dbData._cmd = new SqlCommand(spName);
            _dbData._cmd.CommandType = CommandType.StoredProcedure;			 // assume this unless otherwise specified;
            _dbData._cmd.CommandTimeout = 1200;                              //jrg7298, 20080929, increased timeout to handle multiline/911db

            if (_dbData._trans != null)
                _dbData._cmd.Transaction = _dbData._trans;
        }

        // set the name of the command, whether it is stored procedure or inline tsql
        public void setCommandText(string str)
        {
            _dbData._cmd = null;
            _dbData._cmd = new SqlCommand(str);
            _dbData._cmd.CommandType = CommandType.Text;			 // assume this unless otherwise specified;
            _dbData._cmd.CommandTimeout = 1200;                      //jrg7298, 20080929, increased timeout to handle multiline/911db

            if (_dbData._trans != null)
                _dbData._cmd.Transaction = _dbData._trans;
        }

        //////////////////////////////////////////////////////////////////////////////
        public bool updateDataTable(DataTable dt)
        {
            bool returnFlag = false;

            if (!_blPassThrough)
                _dbData.openAConnection(_connStr);
            try
            {
                _dbData._cmd.Connection = _dbData._conn;
                SqlDataAdapter da = new SqlDataAdapter(_dbData._cmd);
                SqlCommandBuilder builder = new SqlCommandBuilder(da);
                builder.GetInsertCommand();
                da.Update(dt);
                returnFlag = true;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
                GC.Collect();
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            return returnFlag;
        }

        //////////////////////////////////////////////////////////////////////////////
        // set the action to be either a stored procedure or inline tsql
        public void setAction(CommandType iCommandType)
        {
            _dbData._cmd.CommandType = iCommandType;
        }

        //////////////////////////////////////////////////////////////////////////////
        //// transactions
        public void startTransaction()
        {
            if (!_blPassThrough)
                _dbData.openAConnection(_connStr);

            _dbData._trans = _dbData._conn.BeginTransaction();
        }

        //////////////////////////////////////////////////////////////////////////////
        public void commitTransaction()
        {
            try
            {
                _dbData._trans.Commit();
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        public void rollbackTransaction()
        {
            try
            {
                _dbData._trans.Rollback();
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        //// datasets, datatable, dataview retrieval; execnoquery updates
        public DataSet getDataSet()
        {
            try
            {
                if (!_blPassThrough)
                    _dbData.openAConnection(_connStr);

                _dbData._cmd.Connection = _dbData._conn;
                SqlDataAdapter da = new SqlDataAdapter(_dbData._cmd);
                _dbData._ds = new DataSet();
                da.Fill(_dbData._ds);
            }
            catch (Exception ex)
            {
                this.err = ex.Message;
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            return (_dbData._ds);
        }

        /// <summary>
        /// Provide the possibility of set the table names for each result set.
        /// </summary>
        /// <param name="sTableNames">String array of DataTable names.</param>
        /// <returns>Returns a DataSet with all the DataTables names already set.</returns>
        public DataSet getDataSet(string[] sTableNames)
        {
            try
            {
                if (!_blPassThrough)
                    _dbData.openAConnection(_connStr);

                _dbData._cmd.Connection = _dbData._conn;
                SqlDataAdapter da = new SqlDataAdapter(_dbData._cmd);
                _dbData._ds = new DataSet();

                for (int i = 0; i < sTableNames.Length; i++)
                {
                    if (i == 0)
                        da.TableMappings.Add("Table", sTableNames[i]);
                    else
                        da.TableMappings.Add(string.Format("Table{0}", i), sTableNames[i]);
                }
                da.Fill(_dbData._ds);
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            return (_dbData._ds);
        }

        public DataTable getRptDataTable()
        {
            _dbData.openAConnection(_connRptStr);
            try
            {
                _dbData._cmd.Connection = _dbData._conn;
                SqlDataAdapter da = new SqlDataAdapter(_dbData._cmd);
                _dbData._ds = new DataSet();
                da.Fill(_dbData._ds);
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            if ((_dbData._ds != null) && (_dbData._ds.Tables.Count > 0))
                return _dbData._ds.Tables[0];
            else
                return null;
        }

        //////////////////////////////////////////////////////////////////////////////
        public DataTable getDataTable()
        {
            if (!_blPassThrough)
                _dbData.openAConnection(_connStr);
            try
            {
                _dbData._cmd.Connection = _dbData._conn;
                SqlDataAdapter da = new SqlDataAdapter(_dbData._cmd);
                _dbData._ds = new DataSet();
                da.Fill(_dbData._ds);
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            if ((_dbData._ds != null) && (_dbData._ds.Tables.Count > 0))
                return _dbData._ds.Tables[0];
            else
                return null;
        }

        //////////////////////////////////////////////////////////////////////////////
        public DataView getDataView()
        {
            if (!_blPassThrough)
                _dbData.openAConnection(_connStr);

            try
            {
                _dbData._cmd.Connection = _dbData._conn;
                SqlDataAdapter da = new SqlDataAdapter(_dbData._cmd);
                _dbData._ds = new DataSet();
                da.Fill(_dbData._ds);

                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }

            if ((_dbData._ds != null) && (_dbData._ds.Tables.Count > 0))
                return _dbData._ds.Tables[0].DefaultView;
            else
                return null;
        }

        //////////////////////////////////////////////////////////////////////////////
        public int execNoQuery()
        {
            if (!_blPassThrough)
                _dbData.openAConnection(_connStr);
            int iResult = 0;
            try
            {
                _dbData._cmd.Connection = _dbData._conn;
                iResult = _dbData._cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
                iResult = 0;
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            return iResult;
        }

        //////////////////////////////////////////////////////////////////////////////
        public int execNoQuery(bool blnThrowEx)
        {
            if (!_blPassThrough)
                _dbData.openAConnection(_connStr);
            int iResult = 0;
            try
            {
                _dbData._cmd.Connection = _dbData._conn;
                iResult = _dbData._cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
                iResult = 0;
                throw ex;
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            return iResult;
        }

        public int execNoQueryOut(string ParmName, bool Out)
        {
            int iResult = 0;
            if (!Out)
            {
                iResult = execNoQuery();
            }
            else
            {
                if (!_blPassThrough)
                    _dbData.openAConnection(_connStr);

                try
                {
                    _dbData._cmd.Connection = _dbData._conn;
                    iResult = _dbData._cmd.ExecuteNonQuery();
                    iResult = Convert.ToInt32(_dbData._cmd.Parameters[ParmName].SqlValue.ToString());
                }
                catch (Exception ex)
                {
                    //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
                    iResult = 0;
                }
                finally
                {
                    if ((!_blPassThrough) && (_dbData._trans == null))
                        _dbData.closeAConnection();
                }
            }
            return iResult;
        }

        //Added with D9662
        //////////////////////////////////////////////////////////////////////////////
        public int execScalar()
        {
            if (!_blPassThrough)
                _dbData.openAConnection(_connStr);
            int iResult = -1;
            try
            {
                _dbData._cmd.Connection = _dbData._conn;
                iResult = (int)_dbData._cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
                iResult = -1;
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            return iResult;
        }

        public string execScalarstr()
        {
            if (!_blPassThrough)
                _dbData.openAConnection(_connStr);
            string sResult = string.Empty;
            try
            {
                _dbData._cmd.Connection = _dbData._conn;
                sResult = (string)_dbData._cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd != null) ? _dbData._cmd.CommandText + ";" + getParametersAsString() : ""));
                sResult = string.Empty;
            }
            finally
            {
                if ((!_blPassThrough) && (_dbData._trans == null))
                    _dbData.closeAConnection();
            }
            return sResult;
        }

        private string getParametersAsString()
        {
            string s = string.Empty;
            if ((_dbData._cmd != null) && (_dbData._cmd.CommandText != null)
                && (_dbData._cmd.Parameters != null) && (_dbData._cmd.Parameters.Count > 0))
            {
                foreach (SqlParameter sp in _dbData._cmd.Parameters)
                {
                    s = "|" + sp.ParameterName + "^" + ((sp.Value == null) ? "" : sp.Value.ToString());
                }
            }
            return s;
        }

        //////////////////////////////////////////////////////////////////////////////
        //// add parameters for stored procedures

        //////////////////////////////////////////////////////////////////////////////
        // add a long parameter to the list - with an optional output parameter if specified
        public void addIntParam(string strName, int value, bool blOutput)
        {
            SqlParameter myParm;
            long lHold = -1;

            if (blOutput)
            {
                myParm = new SqlParameter(strName, lHold);
                myParm.SqlDbType = SqlDbType.Int;
                myParm.Size = 4;
                myParm.Direction = ParameterDirection.Output;
            }
            else
                myParm = new SqlParameter(strName, value);

            _dbData._cmd.Parameters.Add(myParm);
        }

        public int getOutputParamValue(string paramName)
        {
            try
            {
                return (int)_dbData._cmd.Parameters[paramName].Value;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace + ";" + paramName, ((_dbData._cmd.Parameters[paramName].Value != null) ? getParametersAsString() : _dbData._cmd.CommandText));
                return 0;
            }
        }

        public string getOutputParamValueString(string paramName)
        {
            try
            {
                return (string)_dbData._cmd.Parameters[paramName].Value;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, ((_dbData._cmd.Parameters[paramName].Value != null) ? getParametersAsString() : _dbData._cmd.CommandText));
                return string.Empty;
            }
        }

        public void addIntParam(string strName, int value)
        {
            addIntParam(strName, value, false);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void addInt64Param(string strName, Int64 value, bool blOutput)
        {
            SqlParameter myParm;
            long lHold = -1;

            if (blOutput)
            {
                myParm = new SqlParameter(strName, lHold);
                myParm.SqlDbType = SqlDbType.BigInt;
                myParm.Size = 8;
                myParm.Direction = ParameterDirection.Output;
            }
            else
                myParm = new SqlParameter(strName, value);

            _dbData._cmd.Parameters.Add(myParm);
        }

        public void addInt64Param(string strName, Int64 value)
        {
            addInt64Param(strName, value, false);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void addTinyIntParam(string strName, byte value, bool blOutput)
        {
            SqlParameter myParm;
            long lHold = -1;

            if (blOutput)
            {
                myParm = new SqlParameter(strName, lHold);
                myParm.SqlDbType = SqlDbType.TinyInt;
                myParm.Size = 1;
                myParm.Direction = ParameterDirection.Output;
            }
            else
                myParm = new SqlParameter(strName, value);

            _dbData._cmd.Parameters.Add(myParm);
        }

        public void addShortIntParam(string strName, short value)
        {
            addShortIntParam(strName, value, false);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void addShortIntParam(string strName, short value, bool blOutput)
        {
            SqlParameter myParm;
            long lHold = -1;

            if (blOutput)
            {
                myParm = new SqlParameter(strName, lHold);
                myParm.SqlDbType = SqlDbType.SmallInt;
                myParm.Size = 1;
                myParm.Direction = ParameterDirection.Output;
            }
            else
                myParm = new SqlParameter(strName, value);

            _dbData._cmd.Parameters.Add(myParm);
        }

        public void addTinyIntParam(string strName, byte value)
        {
            addTinyIntParam(strName, value, false);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void addStringParam(string strName, string value, bool blOutput, int iSize)
        {
            SqlParameter myParm;
            string strHold = null;

            if (blOutput)
            {
                myParm = new SqlParameter(strName, strHold);
                myParm.SqlDbType = SqlDbType.VarChar;
                myParm.Size = iSize;
                myParm.Direction = ParameterDirection.Output;
            }
            else
            {
                if (value != null)
                    myParm = new SqlParameter(strName, value);
                else
                {
                    myParm = new SqlParameter(strName, DBNull.Value);
                }
            }
            _dbData._cmd.Parameters.Add(myParm);
        }

        public void addStringParam(string strName, string value)
        {
            addStringParam(strName, value, false, 0);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void addBooleanParam(string strName, bool value)
        {
            _dbData._cmd.Parameters.AddWithValue(strName, value);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void addDateParam(string strName, DateTime? value)
        {
            SqlParameter myParm;
            if (value == DateTime.MinValue || value == null)
            {
                myParm = new SqlParameter(strName, DBNull.Value);
                myParm.IsNullable = true;
            }
            else
                myParm = new SqlParameter(strName, value);

            _dbData._cmd.Parameters.Add(myParm);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void addIdentityParam()
        {
            addIntParam("@Identity", 0, true);
        }

        #region " PJ004672 "

        public void addSmallIntParam(string strName, short value)
        {
            addSmallIntParam(strName, value, false);
        }

        public void addMoneyParam(string strName, decimal value)
        {
            addMoneyParam(strName, value, false);
        }

        public void addFloatParam(string strName, float value)
        {
            addFloatParam(strName, value, false);
        }

        public void addSmallIntParam(string strName, short value, bool blOutput)
        {
            SqlParameter myParm;
            long lHold = -1;

            if (blOutput)
            {
                myParm = new SqlParameter(strName, lHold);
                myParm.SqlDbType = SqlDbType.SmallInt;
                myParm.Size = 2;
                myParm.Direction = ParameterDirection.Output;
            }
            else
                myParm = new SqlParameter(strName, value);

            _dbData._cmd.Parameters.Add(myParm);
        }

        public void addMoneyParam(string strName, decimal value, bool blOutput)
        {
            SqlParameter myParm;
            long lHold = -1;

            if (blOutput)
            {
                myParm = new SqlParameter(strName, lHold);
                myParm.SqlDbType = SqlDbType.Money;
                myParm.Size = 8;
                myParm.Direction = ParameterDirection.Output;
            }
            else
                myParm = new SqlParameter(strName, value);

            _dbData._cmd.Parameters.Add(myParm);
        }

        public void addFloatParam(string strName, float value, bool blOutput)
        {
            SqlParameter myParm;
            long lHold = -1;

            if (blOutput)
            {
                myParm = new SqlParameter(strName, lHold);
                myParm.SqlDbType = SqlDbType.Float;
                myParm.Size = 4;
                myParm.Direction = ParameterDirection.Output;
            }
            else
                myParm = new SqlParameter(strName, value);

            _dbData._cmd.Parameters.Add(myParm);
        }

        #endregion " PJ004672 "

        //////////////////////////////////////////////////////////////////////////////
        // retrieve an long output parameter that was named above
        public long getLongParam(string strName)
        {
            return Convert.ToInt32(_dbData._cmd.Parameters[strName].Value);
        }

        //////////////////////////////////////////////////////////////////////////////
        public string getStringParam(string strName)
        {
            return _dbData._cmd.Parameters[strName].Value.ToString();
        }

        //////////////////////////////////////////////////////////////////////////////
        public long getIdentity()
        {
            return getLongParam("@Identity");
        }

        #region "TWC D9662"

        ////////////////////////////////////////////////
        public void addBitParam(string strName, Byte value)
        {
            SqlParameter myParam;
            //Convert.to

            myParam = new SqlParameter(strName, value);
            myParam.SqlDbType = SqlDbType.Bit;
            myParam.Direction = ParameterDirection.Input;
            _dbData._cmd.Parameters.Add(myParam);
        }

        public void addCharParam(string strName, Char value)
        {
            SqlParameter myParam;

            myParam = new SqlParameter(strName, value);
            myParam.SqlDbType = SqlDbType.Char;
            myParam.Direction = ParameterDirection.Input;
            _dbData._cmd.Parameters.Add(myParam);
        }

        public void addGuidParam(string strName, Guid value)
        {
            SqlParameter myParm;
            myParm = new SqlParameter(strName, SqlDbType.UniqueIdentifier);
            _dbData._cmd.Parameters.Add(myParm);
        }

        //////////////////////////////////////////////////////////////////////////////
        public void addBinaryParam(string strName, byte[] value)
        {
            SqlParameter myParm;
            myParm = new SqlParameter(strName, SqlDbType.Binary);
            myParm.Value = (null == value) ? (new byte[0]) : value;
            _dbData._cmd.Parameters.Add(myParm);
        }

        public void addVarBinaryParam(string strName, byte[] value)
        {
            SqlParameter myParm;
            myParm = new SqlParameter(strName, SqlDbType.VarBinary);
            myParm.Value = (null == value) ? (new byte[0]) : value;
            _dbData._cmd.Parameters.Add(myParm);
        }

        #endregion "TWC D9662"

        /////////////////////////////////////////////////////////////////////////
        public DataTable GetLookup(string sQueryName)
        {
            setCommand(sQueryName);
            return getDataTable();
        }

        /////////////////////////////////////////////////////////////////////////
        public void ClearParams()
        {
            _dbData._cmd.Parameters.Clear();
        }

        ///////////////////////////////////////////////////////////////////////////
        static public bool GetBool(object objIn)
        {
            bool bResult = false;

            try
            {
                bResult = (bool)objIn;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, string.Empty);
            }

            return bResult;
        }

        ///////////////////////////////////////////////////////////////////////////
        static public DateTime GetDate(object objIn)
        {
            DateTime datResult = DateTime.MinValue;

            try
            {
                string sInput = (string)objIn;
                datResult = DateTime.Parse(sInput);
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, string.Empty);
            }

            return datResult;
        }

        ///////////////////////////////////////////////////////////////////////////
        static public DateTime GetDateFromDate(object objIn)
        {
            DateTime datResult = DateTime.MinValue;

            try
            {
                DateTime sInput = (DateTime)objIn;
                datResult = sInput;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, string.Empty);
            }

            return datResult;
        }

        ///////////////////////////////////////////////////////////////////////////
        static public Int64 GetInt64(object objIn)
        {
            Int64 iResult = 0;

            try
            {
                if (objIn != null)
                    iResult = (Int64)objIn;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, string.Empty);
            }

            return iResult;
        }

        ///////////////////////////////////////////////////////////////////////////
        static public int GetInteger(object objIn)
        {
            int iResult = 0;

            try
            {
                if (objIn != null)
                    iResult = (int)objIn;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, string.Empty);
            }

            return iResult;
        }

        ///////////////////////////////////////////////////////////////////////////
        static public int GetTinyIntToInteger(object objIn)
        {
            int iResult = 0;

            try
            {
                if (objIn != null)
                    iResult = (byte)objIn;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, string.Empty);
            }

            return iResult;
        }

        ///////////////////////////////////////////////////////////////////////////
        static public short GetShort(object objIn)
        {
            short iResult = 0;

            try
            {
                if (objIn != null)
                    iResult = (short)objIn;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.Error, fl.MsgType.error, 0, string.Empty, ex.Message + ";" + ex.StackTrace, string.Empty);
            }

            return iResult;
        }
    }
}