﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;

namespace COWS.Data.Extensions
{
    public class DynamicParameters
    {
        // Custom ViewModel for dynamic parameters
        public int EventTypeId { get; set; }

        public int EnhanceServiceId { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public int IpVersionId { get; set; }
        #region MDS
        public int[] MdsMacActivityTypeIds { get; set; }
        public List<MdsEventOdieDevView> ManagedDevice { get; set; }
        public string CustomerName { get; set; }
        public string isFirewallSecured { get; set; } // True or False
        public int[] ActivityType { get; set; } // MDS Network Activity Type
        #endregion
    }
}