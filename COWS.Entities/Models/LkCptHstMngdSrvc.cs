﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCptHstMngdSrvc
    {
        public LkCptHstMngdSrvc()
        {
            CptMngdSrvc = new HashSet<CptMngdSrvc>();
        }

        public short CptHstMngdSrvcId { get; set; }
        public string CptHstMngdSrvc { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CptMngdSrvc> CptMngdSrvc { get; set; }
    }
}
