﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class M5BillDsptchMsg
    {
        public int M5BillDsptchMsgId { get; set; }
        public int BillDsptchId { get; set; }
        public short StusId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? SentDt { get; set; }
        public string ErrMsg { get; set; }

        public virtual LkStus Stus { get; set; }
    }
}
