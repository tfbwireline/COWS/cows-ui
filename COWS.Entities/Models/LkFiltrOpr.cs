﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFiltrOpr
    {
        public LkFiltrOpr()
        {
            DsplVwFiltrCol1Opr = new HashSet<DsplVw>();
            DsplVwFiltrCol2Opr = new HashSet<DsplVw>();
            DsplVwFiltrLogicOpr = new HashSet<DsplVw>();
        }

        public string FiltrOprId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public string FiltrOprDes { get; set; }
        public string LogicOprCd { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual ICollection<DsplVw> DsplVwFiltrCol1Opr { get; set; }
        public virtual ICollection<DsplVw> DsplVwFiltrCol2Opr { get; set; }
        public virtual ICollection<DsplVw> DsplVwFiltrLogicOpr { get; set; }
    }
}
