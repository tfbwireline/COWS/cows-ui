﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SdeOpptnty
    {
        public SdeOpptnty()
        {
            SdeOpptntyDoc = new HashSet<SdeOpptntyDoc>();
            SdeOpptntyNte = new HashSet<SdeOpptntyNte>();
            SdeOpptntyPrdct = new HashSet<SdeOpptntyPrdct>();
        }

        public int SdeOpptntyId { get; set; }
        public short SdeStusId { get; set; }
        public string Rgn { get; set; }
        public string SaleType { get; set; }
        public bool RfpCd { get; set; }
        public bool WrlneCustCd { get; set; }
        public bool MngdSrvcCustCd { get; set; }
        public string CmntTxt { get; set; }
        public string CcAdr { get; set; }
        public int? AsnToUserId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser AsnToUser { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkStus SdeStus { get; set; }
        public virtual ICollection<SdeOpptntyDoc> SdeOpptntyDoc { get; set; }
        public virtual ICollection<SdeOpptntyNte> SdeOpptntyNte { get; set; }
        public virtual ICollection<SdeOpptntyPrdct> SdeOpptntyPrdct { get; set; }
    }
}
