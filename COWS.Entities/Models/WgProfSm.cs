﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class WgProfSm
    {
        public int WgProfId { get; set; }
        public short PreReqstTaskId { get; set; }
        public short DesrdTaskId { get; set; }
        public int OptId { get; set; }
        public int? CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public bool PreReqstTaskGrpReqrCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkTask DesrdTask { get; set; }
        public virtual LkTask PreReqstTask { get; set; }
    }
}
