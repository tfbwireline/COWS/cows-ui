﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventNtwkCust
    {
        public int MdsEventNtwkCustId { get; set; }
        public int EventId { get; set; }
        public string InstlSitePocNme { get; set; }
        public string InstlSitePocPhn { get; set; }
        public string InstlSitePocEmail { get; set; }
        public string RoleNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string AssocH6 { get; set; }

        public virtual Event Event { get; set; }
    }
}
