﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCtryCxr
    {
        public string CtryCd { get; set; }
        public string CxrCd { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual LkFrgnCxr CxrCdNavigation { get; set; }
    }
}
