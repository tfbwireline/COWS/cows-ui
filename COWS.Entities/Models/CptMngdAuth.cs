﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptMngdAuth
    {
        public int CptMngdAuthId { get; set; }
        public int CptId { get; set; }
        public short CptMngdAuthPrdId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptMngdAuthPrd CptMngdAuthPrd { get; set; }
    }
}
