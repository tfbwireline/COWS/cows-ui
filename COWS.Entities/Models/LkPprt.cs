﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkPprt
    {
        public LkPprt()
        {
            Ordr = new HashSet<Ordr>();
        }

        public int PprtId { get; set; }
        public short SmId { get; set; }
        public byte? OrdrTypeId { get; set; }
        public string OrdrSubTypeCd { get; set; }
        public byte? ProdTypeId { get; set; }
        public string PprtNme { get; set; }
        public int ModfdByUserId { get; set; }
        public DateTime ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public bool TrptCd { get; set; }
        public bool IntlCd { get; set; }
        public bool MdsCd { get; set; }
        public bool ReltdFtnCd { get; set; }
        public short? OrdrCatId { get; set; }

        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkOrdrType OrdrType { get; set; }
        public virtual LkProdType ProdType { get; set; }
        public virtual ICollection<Ordr> Ordr { get; set; }
    }
}
