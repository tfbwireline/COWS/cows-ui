﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class PltfrmMapng
    {
        public int PltfrmMapngId { get; set; }
        public string PltfrmCd { get; set; }
        public string ProdNme { get; set; }
        public string FsaProdTypeCd { get; set; }
        public string CxrPtnrAvalCd { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkFsaProdType FsaProdTypeCdNavigation { get; set; }
        public virtual LkPltfrm PltfrmCdNavigation { get; set; }
        public virtual LkProd ProdNmeNavigation { get; set; }
    }
}
