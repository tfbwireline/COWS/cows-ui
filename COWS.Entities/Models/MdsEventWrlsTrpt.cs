﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventWrlsTrpt
    {
        public int MdsEventWrlsTrptId { get; set; }
        public int? FsaMdsEventId { get; set; }
        public string PrimBkupCd { get; set; }
        public string EsnMacId { get; set; }
        public byte TabSeqNbr { get; set; }
        public int? EventId { get; set; }
        public string WrlsTypeCd { get; set; }
        public string BillAcctNme { get; set; }
        public string BillAcctNbr { get; set; }
        public string AcctPin { get; set; }
        public string SalsCd { get; set; }
        public string Soc { get; set; }
        public string StaticIpAdr { get; set; }
        public string Imei { get; set; }
        public string UiccIdNbr { get; set; }
        public string PricePln { get; set; }
        public string OdieDevNme { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
    }
}
