﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkActn
    {
        public LkActn()
        {
            CptHist = new HashSet<CptHist>();
            EventHist = new HashSet<EventHist>();
            LkEventRule = new HashSet<LkEventRule>();
        }

        public byte ActnId { get; set; }
        public string ActnDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short? ActnTypeId { get; set; }

        public virtual LkActnType ActnType { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CptHist> CptHist { get; set; }
        public virtual ICollection<EventHist> EventHist { get; set; }
        public virtual ICollection<LkEventRule> LkEventRule { get; set; }
    }
}
