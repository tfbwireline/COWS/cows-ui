﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMdsBrdgNeedReas
    {
        public LkMdsBrdgNeedReas()
        {
            MdsEvent = new HashSet<MdsEvent>();
            MdsEventNew = new HashSet<MdsEventNew>();
            UcaaSEvent = new HashSet<UcaaSEvent>();
        }

        public byte MdsBrdgNeedId { get; set; }
        public string MdsBrdgNeedTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public int? CreatByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual ICollection<MdsEvent> MdsEvent { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNew { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEvent { get; set; }
    }
}
