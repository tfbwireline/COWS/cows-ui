﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkEventType
    {
        public LkEventType()
        {
            Event = new HashSet<Event>();
            EventTypeRsrcAvlblty = new HashSet<EventTypeRsrcAvlblty>();
            LkEnhncSrvc = new HashSet<LkEnhncSrvc>();
            LkEventRule = new HashSet<LkEventRule>();
            LkEventTypeTmeSlot = new HashSet<LkEventTypeTmeSlot>();
            LkFailActy = new HashSet<LkFailActy>();
            LkMdsSrvcTier = new HashSet<LkMdsSrvcTier>();
            LkMplsActyType = new HashSet<LkMplsActyType>();
            LkMplsEventType = new HashSet<LkMplsEventType>();
            LkSucssActy = new HashSet<LkSucssActy>();
            LkVasType = new HashSet<LkVasType>();
            LkVpnPltfrmType = new HashSet<LkVpnPltfrmType>();
            QlfctnEventType = new HashSet<QlfctnEventType>();
            ReltdEvent = new HashSet<ReltdEvent>();
        }

        public short EventTypeId { get; set; }
        public string EventTypeNme { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkMdsImplmtnNtvl LkMdsImplmtnNtvl { get; set; }
        public virtual ICollection<Event> Event { get; set; }
        public virtual ICollection<EventTypeRsrcAvlblty> EventTypeRsrcAvlblty { get; set; }
        public virtual ICollection<LkEnhncSrvc> LkEnhncSrvc { get; set; }
        public virtual ICollection<LkEventRule> LkEventRule { get; set; }
        public virtual ICollection<LkEventTypeTmeSlot> LkEventTypeTmeSlot { get; set; }
        public virtual ICollection<LkFailActy> LkFailActy { get; set; }
        public virtual ICollection<LkMdsSrvcTier> LkMdsSrvcTier { get; set; }
        public virtual ICollection<LkMplsActyType> LkMplsActyType { get; set; }
        public virtual ICollection<LkMplsEventType> LkMplsEventType { get; set; }
        public virtual ICollection<LkSucssActy> LkSucssActy { get; set; }
        public virtual ICollection<LkVasType> LkVasType { get; set; }
        public virtual ICollection<LkVpnPltfrmType> LkVpnPltfrmType { get; set; }
        public virtual ICollection<QlfctnEventType> QlfctnEventType { get; set; }
        public virtual ICollection<ReltdEvent> ReltdEvent { get; set; }
    }
}
