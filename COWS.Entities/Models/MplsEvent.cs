﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MplsEvent
    {
        public MplsEvent()
        {
            MplsEventAccsTag = new HashSet<MplsEventAccsTag>();
        }

        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public string Ftn { get; set; }
        public string CharsId { get; set; }
        public string H1 { get; set; }
        public string H6 { get; set; }
        public int ReqorUserId { get; set; }
        public int SalsUserId { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public string DocLinkTxt { get; set; }
        public string DdAprvlNbr { get; set; }
        public bool NddUpdtdCd { get; set; }
        public string DesCmntTxt { get; set; }
        public byte MplsEventTypeId { get; set; }
        public byte VpnPltfrmTypeId { get; set; }
        public byte? IpVerId { get; set; }
        public string ActyLocaleId { get; set; }
        public string MultiVrfReqId { get; set; }
        public string VrfNme { get; set; }
        public bool MdsMngdCd { get; set; }
        public bool AddE2eMontrgCd { get; set; }
        public string MdsVrfNme { get; set; }
        public string MdsIpAdr { get; set; }
        public string MdsDlci { get; set; }
        public string MdsStcRteDes { get; set; }
        public bool OnNetMontrgCd { get; set; }
        public bool WhlslPtnrCd { get; set; }
        public byte? WhlslPtnrId { get; set; }
        public string NniDsgnDocNme { get; set; }
        public bool CxrPtnrCd { get; set; }
        public string MplsCxrPtnrId { get; set; }
        public bool OffNetMontrgCd { get; set; }
        public bool MgrtnCd { get; set; }
        public byte? MplsMgrtnTypeId { get; set; }
        public bool EsclCd { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime StrtTmst { get; set; }
        public short ExtraDrtnTmeAmt { get; set; }
        public DateTime EndTmst { get; set; }
        public byte? WrkflwStusId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public short EventDrtnInMinQty { get; set; }
        public int? SowsEventId { get; set; }
        public bool ReltdCmpsNcrCd { get; set; }
        public string ReltdCmpsNcrNme { get; set; }
        public string CustNme { get; set; }
        public string CustCntctNme { get; set; }
        public string CustCntctPhnNbr { get; set; }
        public string CustEmailAdr { get; set; }
        public string CustCntctCellPhnNbr { get; set; }
        public string CustCntctPgrNbr { get; set; }
        public string CustCntctPgrPinNbr { get; set; }
        public string EventTitleTxt { get; set; }
        public string EventDes { get; set; }

        public virtual LkActyLocale ActyLocale { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkEsclReas EsclReas { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkEventStus EventStus { get; set; }
        public virtual LkIpVer IpVer { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkVndr MplsCxrPtnr { get; set; }
        public virtual LkMplsEventType MplsEventType { get; set; }
        public virtual LkMplsMgrtnType MplsMgrtnType { get; set; }
        public virtual LkMultiVrfReq MultiVrfReq { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUser ReqorUser { get; set; }
        public virtual LkUser SalsUser { get; set; }
        public virtual LkVpnPltfrmType VpnPltfrmType { get; set; }
        public virtual LkWhlslPtnr WhlslPtnr { get; set; }
        public virtual LkWrkflwStus WrkflwStus { get; set; }
        public virtual ICollection<MplsEventAccsTag> MplsEventAccsTag { get; set; }
    }
}
