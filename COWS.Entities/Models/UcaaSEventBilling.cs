﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class UcaaSEventBilling
    {
        public int UcaaSEventBillingId { get; set; }
        public int EventId { get; set; }
        public short UcaaSBillActyId { get; set; }
        public DateTime CreatDt { get; set; }
        public short IncQty { get; set; }
        public short? DcrQty { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkUcaaSBillActy UcaaSBillActy { get; set; }
    }
}
