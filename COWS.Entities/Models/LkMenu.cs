﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMenu
    {
        public LkMenu()
        {
            LkUsrPrfMenu = new HashSet<LkUsrPrfMenu>();
        }

        public short MenuId { get; set; }
        public string MenuNme { get; set; }
        public string MenuDes { get; set; }
        public byte DpthLvl { get; set; }
        public short PrntMenuId { get; set; }
        public byte? DsplOrdr { get; set; }
        public string SrcPath { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<LkUsrPrfMenu> LkUsrPrfMenu { get; set; }
    }
}
