﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CustScrdData
    {
        public int Id { get; set; }
        public int ScrdObjId { get; set; }
        public byte ScrdObjTypeId { get; set; }
        public byte[] EventTitleTxt { get; set; }
        public byte[] EventDes { get; set; }
        public byte[] CustNme { get; set; }
        public byte[] CustEmailAdr { get; set; }
        public byte[] CustCntctNme { get; set; }
        public byte[] CustCntctPhnNbr { get; set; }
        public byte[] CustCntctCellPhnNbr { get; set; }
        public byte[] CustCntctPgrNbr { get; set; }
        public byte[] CustCntctPgrPinNbr { get; set; }
        public byte[] SrvcAssrnPocNme { get; set; }
        public byte[] SrvcAssrnPocPhnNbr { get; set; }
        public byte[] SrvcAssrnPocCellPhnNbr { get; set; }
        public byte[] BldgNme { get; set; }
        public byte[] CtyNme { get; set; }
        public byte[] FlrId { get; set; }
        public byte[] SttPrvnNme { get; set; }
        public byte[] RmNbr { get; set; }
        public byte[] SiteAdr { get; set; }
        public byte[] StreetAdr1 { get; set; }
        public byte[] StreetAdr2 { get; set; }
        public byte[] StreetAdr3 { get; set; }
        public byte[] SttCd { get; set; }
        public byte[] ZipPstlCd { get; set; }
        public byte[] CtryRgnNme { get; set; }
        public byte[] FrstNme { get; set; }
        public byte[] LstNme { get; set; }
        public byte[] NteTxt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte[] CtryCd { get; set; }

        public virtual LkScrdObjType ScrdObjType { get; set; }
    }
}
