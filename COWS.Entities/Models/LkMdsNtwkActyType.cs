﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMdsNtwkActyType
    {
        public LkMdsNtwkActyType()
        {
            MdsEventNtwkActy = new HashSet<MdsEventNtwkActy>();
            QlfctnNtwkActyType = new HashSet<QlfctnNtwkActyType>();
        }

        public byte NtwkActyTypeId { get; set; }
        public string NtwkActyTypeCd { get; set; }
        public string NtwkActyTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<MdsEventNtwkActy> MdsEventNtwkActy { get; set; }
        public virtual ICollection<QlfctnNtwkActyType> QlfctnNtwkActyType { get; set; }
    }
}
