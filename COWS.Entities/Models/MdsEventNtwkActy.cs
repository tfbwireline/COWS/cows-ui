﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventNtwkActy
    {
        public long Id { get; set; }
        public int EventId { get; set; }
        public byte NtwkActyTypeId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkMdsNtwkActyType NtwkActyType { get; set; }
    }
}
