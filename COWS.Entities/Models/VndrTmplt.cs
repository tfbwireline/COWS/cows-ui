﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class VndrTmplt
    {
        public int VndrTmpltId { get; set; }
        public int VndrFoldrId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual VndrFoldr VndrFoldr { get; set; }
    }
}
