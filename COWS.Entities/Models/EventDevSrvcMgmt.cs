﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventDevSrvcMgmt
    {
        public int EventDevSrvcMgmtId { get; set; }
        public int EventId { get; set; }
        public string DeviceId { get; set; }
        public byte? MnsSrvcTierId { get; set; }
        public string OdieDevNme { get; set; }
        public DateTime CreatDt { get; set; }
        public string MnsOrdrNbr { get; set; }
        public bool? OdieCd { get; set; }
        public string CmpntStus { get; set; }
        public string CmpntNme { get; set; }
        public string CmpntTypeCd { get; set; }
        public string M5OrdrCmpntId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkMdsSrvcTier MnsSrvcTier { get; set; }
    }
}
