﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMdsSrvcType
    {
        public LkMdsSrvcType()
        {
            LkMds3rdpartySrvcLvl = new HashSet<LkMds3rdpartySrvcLvl>();
            LkMds3rdpartyVndr = new HashSet<LkMds3rdpartyVndr>();
            MdsEventSiteSrvc = new HashSet<MdsEventSiteSrvc>();
            MdsEventSrvc = new HashSet<MdsEventSrvc>();
        }

        public short SrvcTypeId { get; set; }
        public string SrvcTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<LkMds3rdpartySrvcLvl> LkMds3rdpartySrvcLvl { get; set; }
        public virtual ICollection<LkMds3rdpartyVndr> LkMds3rdpartyVndr { get; set; }
        public virtual ICollection<MdsEventSiteSrvc> MdsEventSiteSrvc { get; set; }
        public virtual ICollection<MdsEventSrvc> MdsEventSrvc { get; set; }
    }
}
