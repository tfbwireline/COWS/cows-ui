﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class M5EventMsg
    {
        public int M5EventMsgId { get; set; }
        public int M5OrdrId { get; set; }
        public string M5OrdrNbr { get; set; }
        public int M5MsgId { get; set; }
        public string DevId { get; set; }
        public int? CmpntId { get; set; }
        public short StusId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? SentDt { get; set; }
        public int? EventId { get; set; }

        public virtual LkStus Stus { get; set; }
    }
}
