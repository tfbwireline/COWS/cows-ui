﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptRecLock
    {
        public int CptId { get; set; }
        public DateTime StrtRecLockTmst { get; set; }
        public int LockByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkUser LockByUser { get; set; }
    }
}
