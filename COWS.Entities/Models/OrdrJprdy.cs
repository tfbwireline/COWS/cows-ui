﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OrdrJprdy
    {
        public int OrdrId { get; set; }
        public string JprdyCd { get; set; }
        public int NteId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual OrdrNte Nte { get; set; }
        public virtual Ordr Ordr { get; set; }
    }
}
