﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkAccsMb
    {
        public int AccsMbId { get; set; }
        public string AccsMbDes { get; set; }
        public DateTime CreatDt { get; set; }
        public int? SortId { get; set; }
        public byte? AsrTypeId { get; set; }
    }
}
