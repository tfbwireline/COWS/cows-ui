﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class WgPtrnSm
    {
        public int WgPtrnId { get; set; }
        public int PreReqstWgProfId { get; set; }
        public int DesrdWgProfId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
