﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkSprintCpeNcr
    {
        public LkSprintCpeNcr()
        {
            MdsEvent = new HashSet<MdsEvent>();
            UcaaSEvent = new HashSet<UcaaSEvent>();
        }

        public byte SprintCpeNcrId { get; set; }
        public string SprintCpeNcrDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<MdsEvent> MdsEvent { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEvent { get; set; }
    }
}
