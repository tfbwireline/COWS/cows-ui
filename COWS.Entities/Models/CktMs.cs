﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CktMs
    {
        public int CktId { get; set; }
        public short VerId { get; set; }
        public DateTime? TrgtDlvryDtRecvDt { get; set; }
        public DateTime? TrgtDlvryDt { get; set; }
        public DateTime? AccsDlvryDt { get; set; }
        public DateTime? AccsAcptcDt { get; set; }
        public DateTime? CntrcStrtDt { get; set; }
        public DateTime? CntrcEndDt { get; set; }
        public DateTime? RnlDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public string CntrcTermId { get; set; }
        public DateTime? VndrCnfrmDscnctDt { get; set; }

        public virtual Ckt Ckt { get; set; }
        public virtual LkCustCntrcLgth CntrcTerm { get; set; }
        public virtual LkUser CreatByUser { get; set; }
    }
}
