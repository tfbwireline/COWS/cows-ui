﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnNotes
    {
        public int RedsgnNotesId { get; set; }
        public int RedsgnId { get; set; }
        public byte RedsgnNteTypeId { get; set; }
        public string Notes { get; set; }
        public int CretdByCd { get; set; }
        public DateTime CretdDt { get; set; }

        public virtual LkUser CretdByCdNavigation { get; set; }
        public virtual Redsgn Redsgn { get; set; }
        public virtual LkRedsgnNotesType RedsgnNteType { get; set; }
    }
}
