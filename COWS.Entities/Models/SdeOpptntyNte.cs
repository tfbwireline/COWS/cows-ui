﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SdeOpptntyNte
    {
        public int SdeOpptntyNteId { get; set; }
        public int SdeOpptntyId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual SdeOpptnty SdeOpptnty { get; set; }
    }
}
