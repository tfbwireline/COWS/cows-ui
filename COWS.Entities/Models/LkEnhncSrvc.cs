﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkEnhncSrvc
    {
        public LkEnhncSrvc()
        {
            AdEvent = new HashSet<AdEvent>();
            QlfctnEnhncSrvc = new HashSet<QlfctnEnhncSrvc>();
        }

        public short EnhncSrvcId { get; set; }
        public string EnhncSrvcNme { get; set; }
        public short EventTypeId { get; set; }
        public short MinDrtnTmeReqrAmt { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkEventType EventType { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<AdEvent> AdEvent { get; set; }
        public virtual ICollection<QlfctnEnhncSrvc> QlfctnEnhncSrvc { get; set; }
    }
}
