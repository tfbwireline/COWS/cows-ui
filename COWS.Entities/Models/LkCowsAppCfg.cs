﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCowsAppCfg
    {
        public string CfgKeyNme { get; set; }
        public string CfgKeyValuTxt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
