﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class UserCsgLvl
    {
        public int UserId { get; set; }
        public byte CsgLvlId { get; set; }
        public string EmplActCd { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCsgLvl CsgLvl { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUser User { get; set; }
    }
}
