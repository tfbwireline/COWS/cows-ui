﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class H5Doc
    {
        public int H5DocId { get; set; }
        public int H5FoldrId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string CmntTxt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual H5Foldr H5Foldr { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
