﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCur
    {
        public LkCur()
        {
            CktCostAccsCustCur = new HashSet<CktCost>();
            CktCostAsrCur = new HashSet<CktCost>();
            CktCostVndrCur = new HashSet<CktCost>();
            OrdrCktChg = new HashSet<OrdrCktChg>();
            TrptOrdr = new HashSet<TrptOrdr>();
        }

        public string CurId { get; set; }
        public string CurNme { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public decimal? CurCnvrsnFctrFromUsdQty { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CktCost> CktCostAccsCustCur { get; set; }
        public virtual ICollection<CktCost> CktCostAsrCur { get; set; }
        public virtual ICollection<CktCost> CktCostVndrCur { get; set; }
        public virtual ICollection<OrdrCktChg> OrdrCktChg { get; set; }
        public virtual ICollection<TrptOrdr> TrptOrdr { get; set; }
    }
}
