﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkXnciRgn
    {
        public LkXnciRgn()
        {
            LkCtry = new HashSet<LkCtry>();
            Ordr = new HashSet<Ordr>();
            TrptOrdr = new HashSet<TrptOrdr>();
        }

        public short RgnId { get; set; }
        public string RgnDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreateByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public short GrpId { get; set; }
        public short? UsrPrfId { get; set; }

        public virtual LkUser CreateByUser { get; set; }
        public virtual LkGrp Grp { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUsrPrf UsrPrf { get; set; }
        public virtual ICollection<LkCtry> LkCtry { get; set; }
        public virtual ICollection<Ordr> Ordr { get; set; }
        public virtual ICollection<TrptOrdr> TrptOrdr { get; set; }
    }
}
