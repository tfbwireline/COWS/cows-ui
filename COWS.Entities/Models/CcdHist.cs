﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CcdHist
    {
        public CcdHist()
        {
            CcdHistReas = new HashSet<CcdHistReas>();
        }

        public int CcdHistId { get; set; }
        public int OrdrId { get; set; }
        public DateTime OldCcdDt { get; set; }
        public DateTime NewCcdDt { get; set; }
        public int NteId { get; set; }
        public DateTime? SentToVndrDt { get; set; }
        public DateTime? AckByVndrDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual OrdrNte Nte { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual ICollection<CcdHistReas> CcdHistReas { get; set; }
    }
}
