﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class H5Foldr
    {
        public H5Foldr()
        {
            H5Doc = new HashSet<H5Doc>();
            Ordr = new HashSet<Ordr>();
        }

        public int H5FoldrId { get; set; }
        public int CustId { get; set; }
        public string CustCtyNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public string CtryCd { get; set; }
        public byte CsgLvlId { get; set; }
        public string CustNme { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCsgLvl CsgLvl { get; set; }
        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<H5Doc> H5Doc { get; set; }
        public virtual ICollection<Ordr> Ordr { get; set; }
    }
}
