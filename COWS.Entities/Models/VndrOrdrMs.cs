﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class VndrOrdrMs
    {
        public int VndrOrdrMsId { get; set; }
        public int VndrOrdrId { get; set; }
        public short VerId { get; set; }
        public int? VndrOrdrEmailId { get; set; }
        public DateTime? SentToVndrDt { get; set; }
        public DateTime? AckByVndrDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual VndrOrdr VndrOrdr { get; set; }
        public virtual VndrOrdrEmail VndrOrdrEmail { get; set; }
    }
}
