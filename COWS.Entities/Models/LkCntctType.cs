﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCntctType
    {
        public LkCntctType()
        {
            FedlineEventCntct = new HashSet<FedlineEventCntct>();
            OrdrCntct = new HashSet<OrdrCntct>();
        }

        public byte CntctTypeId { get; set; }
        public string CntctTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<FedlineEventCntct> FedlineEventCntct { get; set; }
        public virtual ICollection<OrdrCntct> OrdrCntct { get; set; }
    }
}
