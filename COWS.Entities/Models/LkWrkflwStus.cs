﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkWrkflwStus
    {
        public LkWrkflwStus()
        {
            AdEvent = new HashSet<AdEvent>();
            LkEventRule = new HashSet<LkEventRule>();
            MdsEvent = new HashSet<MdsEvent>();
            MdsEventNew = new HashSet<MdsEventNew>();
            MplsEvent = new HashSet<MplsEvent>();
            NgvnEvent = new HashSet<NgvnEvent>();
            SiptEvent = new HashSet<SiptEvent>();
            SplkEvent = new HashSet<SplkEvent>();
            UcaaSEvent = new HashSet<UcaaSEvent>();
        }

        public byte WrkflwStusId { get; set; }
        public string WrkflwStusDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<AdEvent> AdEvent { get; set; }
        public virtual ICollection<LkEventRule> LkEventRule { get; set; }
        public virtual ICollection<MdsEvent> MdsEvent { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNew { get; set; }
        public virtual ICollection<MplsEvent> MplsEvent { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEvent { get; set; }
        public virtual ICollection<SiptEvent> SiptEvent { get; set; }
        public virtual ICollection<SplkEvent> SplkEvent { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEvent { get; set; }
    }
}
