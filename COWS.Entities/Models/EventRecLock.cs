﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventRecLock
    {
        public int EventId { get; set; }
        public DateTime StrtRecLockTmst { get; set; }
        public int LockByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkUser LockByUser { get; set; }
    }
}
