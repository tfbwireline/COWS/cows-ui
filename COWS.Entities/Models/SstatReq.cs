﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SstatReq
    {
        public int TranId { get; set; }
        public int? OrdrId { get; set; }
        public int SstatMsgId { get; set; }
        public DateTime? StusModDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short StusId { get; set; }
        public string Ftn { get; set; }
        public int? EmailReqId { get; set; }

        public virtual FsaOrdr Ordr { get; set; }
        public virtual LkStus Stus { get; set; }
    }
}
