﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class DsplVw
    {
        public DsplVw()
        {
            DsplVwCol = new HashSet<DsplVwCol>();
        }

        public int UserId { get; set; }
        public string DsplVwNme { get; set; }
        public int SiteCntntId { get; set; }
        public string SiteCntntColVwCd { get; set; }
        public string DsplVwDes { get; set; }
        public string DsplVwWebAdr { get; set; }
        public string DfltVwCd { get; set; }
        public string PblcVwCd { get; set; }
        public string FiltrCol1OprId { get; set; }
        public string FiltrCol2OprId { get; set; }
        public int? FiltrCol1Id { get; set; }
        public int? FiltrCol2Id { get; set; }
        public string FiltrCol1ValuTxt { get; set; }
        public string FiltrCol2ValuTxt { get; set; }
        public string FiltrOnCd { get; set; }
        public int? SortByCol1Id { get; set; }
        public int? SortByCol2Id { get; set; }
        public int? GrpByCol1Id { get; set; }
        public int? GrpByCol2Id { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string FiltrLogicOprId { get; set; }
        public string SortByCol1AscOrdrCd { get; set; }
        public string SortByCol2AscOrdrCd { get; set; }
        public byte SeqNbr { get; set; }
        public int DsplVwId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkFiltrOpr FiltrCol1Opr { get; set; }
        public virtual LkFiltrOpr FiltrCol2Opr { get; set; }
        public virtual LkFiltrOpr FiltrLogicOpr { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkUser User { get; set; }
        public virtual ICollection<DsplVwCol> DsplVwCol { get; set; }
    }
}
