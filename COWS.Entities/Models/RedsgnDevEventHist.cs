﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnDevEventHist
    {
        public int RedsgnDevEventHistId { get; set; }
        public int RedsgnDevId { get; set; }
        public int EventId { get; set; }
        public DateTime CmpltdDt { get; set; }

        public virtual Event Event { get; set; }
        public virtual RedsgnDevicesInfo RedsgnDev { get; set; }
    }
}
