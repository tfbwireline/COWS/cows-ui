﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class UserWfmAsmt
    {
        public int OrdrId { get; set; }
        public short GrpId { get; set; }
        public int AsnUserId { get; set; }
        public int AsnByUserId { get; set; }
        public DateTime AsmtDt { get; set; }
        public DateTime CreatDt { get; set; }
        public bool? OrdrHighlightCd { get; set; }
        public DateTime? OrdrHighlightModfdDt { get; set; }
        public DateTime? DsptchTm { get; set; }
        public DateTime? InrteTm { get; set; }
        public DateTime? OnsiteTm { get; set; }
        public DateTime? CmpltTm { get; set; }
        public int? EventId { get; set; }
        public short? UsrPrfId { get; set; }

        public virtual LkUser AsnByUser { get; set; }
        public virtual LkUser AsnUser { get; set; }
        public virtual LkGrp Grp { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkUsrPrf UsrPrf { get; set; }
    }
}
