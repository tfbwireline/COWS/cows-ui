﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventAsnToUser
    {
        public int EventId { get; set; }
        public int AsnToUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public byte RoleId { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser AsnToUser { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkRole Role { get; set; }
    }
}
