﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventCpeDev
    {
        public int EventCpeDevId { get; set; }
        public int EventId { get; set; }
        public string CpeOrdrId { get; set; }
        public string DeviceId { get; set; }
        public string AssocH6 { get; set; }
        public string MaintVndrPo { get; set; }
        public string ReqstnNbr { get; set; }
        public DateTime? Ccd { get; set; }
        public string OdieDevNme { get; set; }
        public string OrdrStus { get; set; }
        public string RcptStus { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public bool? OdieCd { get; set; }
        public string CpeOrdrNbr { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual Event Event { get; set; }
    }
}
