﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMdsCpeDevXclusn
    {
        public short MdsCpeDevXclusnId { get; set; }
        public string DevTypeNme { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
    }
}
