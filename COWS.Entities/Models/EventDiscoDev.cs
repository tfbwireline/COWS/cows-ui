﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventDiscoDev
    {
        public int EventDiscoDevId { get; set; }
        public int EventId { get; set; }
        public string OdieDevNme { get; set; }
        public string H5H6 { get; set; }
        public string SiteId { get; set; }
        public string DeviceId { get; set; }
        public short? DevModelId { get; set; }
        public short? ManfId { get; set; }
        public string SerialNbr { get; set; }
        public string ThrdPartyCtrct { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string ReadyBeginCd { get; set; }
        public string OptInCktCd { get; set; }
        public string OptInHCd { get; set; }

        public virtual LkDevModel DevModel { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkDevManf Manf { get; set; }
    }
}
