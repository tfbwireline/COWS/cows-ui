﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnWrkflw
    {
        public int RedsgnWrkflwId { get; set; }
        public short? PreWrkflwStusId { get; set; }
        public short? DesrdWrkflwStusId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CretdDt { get; set; }

        public virtual LkStus DesrdWrkflwStus { get; set; }
        public virtual LkStus PreWrkflwStus { get; set; }
    }
}
