﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SiptEvent
    {
        public SiptEvent()
        {
            SiptEventDoc = new HashSet<SiptEventDoc>();
        }

        public int EventId { get; set; }
        public string M5OrdrNbr { get; set; }
        public string H1 { get; set; }
        public string SiteId { get; set; }
        public string H6 { get; set; }
        public string CharsId { get; set; }
        public string TeamPdlNme { get; set; }
        public string SiptDocLocTxt { get; set; }
        public DateTime? CustReqStDt { get; set; }
        public DateTime? CustReqEndDt { get; set; }
        public string WrkDes { get; set; }
        public int? ReqorUserId { get; set; }
        public int? NtwkEngrId { get; set; }
        public int? NtwkTechEngrId { get; set; }
        public int? PmId { get; set; }
        public byte EventStusId { get; set; }
        public byte WrkflwStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public short SiptProdTypeId { get; set; }
        public string PrjId { get; set; }
        public DateTime? FocDt { get; set; }
        public string UsIntlCd { get; set; }
        public string SiptDesgnDoc { get; set; }
        public string StreetAdr { get; set; }
        public string EventTitleTxt { get; set; }
        public string CustNme { get; set; }
        public string SiteCntctNme { get; set; }
        public string SiteCntctPhnNbr { get; set; }
        public string SiteCntctHrNme { get; set; }
        public string SiteCntctEmailAdr { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtyNme { get; set; }
        public string SttPrvnNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string ZipCd { get; set; }
        public bool? TnsTgCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkEventStus EventStus { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkUser NtwkEngr { get; set; }
        public virtual LkUser NtwkTechEngr { get; set; }
        public virtual LkUser Pm { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkSiptProdType SiptProdType { get; set; }
        public virtual LkWrkflwStus WrkflwStus { get; set; }
        public virtual ICollection<SiptEventDoc> SiptEventDoc { get; set; }
    }
}
