﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkProdType
    {
        public LkProdType()
        {
            IplOrdr = new HashSet<IplOrdr>();
            LkPprt = new HashSet<LkPprt>();
            NccoOrdr = new HashSet<NccoOrdr>();
            UserWfm = new HashSet<UserWfm>();
        }

        public byte ProdTypeId { get; set; }
        public string ProdTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public short OrdrCatId { get; set; }
        public bool DmstcCd { get; set; }
        public string FsaProdTypeCd { get; set; }
        public string ProdNme { get; set; }
        public bool TrptCd { get; set; }

        public virtual LkFsaProdType FsaProdTypeCdNavigation { get; set; }
        public virtual LkOrdrCat OrdrCat { get; set; }
        public virtual LkProd ProdNmeNavigation { get; set; }
        public virtual ICollection<IplOrdr> IplOrdr { get; set; }
        public virtual ICollection<LkPprt> LkPprt { get; set; }
        public virtual ICollection<NccoOrdr> NccoOrdr { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
    }
}
