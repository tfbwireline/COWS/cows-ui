﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SdeOpptntyPrdct
    {
        public int SdeOpptntyPrdctId { get; set; }
        public int SdeOpptntyId { get; set; }
        public short SdePrdctTypeId { get; set; }
        public int Qty { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual SdeOpptnty SdeOpptnty { get; set; }
        public virtual LkSdePrdctType SdePrdctType { get; set; }
    }
}
