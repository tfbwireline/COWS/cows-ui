﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnDev
    {
        public int QlfctnId { get; set; }
        public short DevId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkDev Dev { get; set; }
        public virtual LkQlfctn Qlfctn { get; set; }
    }
}
