﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsFastTrkType
    {
        public MdsFastTrkType()
        {
            MdsEventNew = new HashSet<MdsEventNew>();
        }

        public string MdsFastTrkTypeId { get; set; }
        public string MdsFastTrkTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNew { get; set; }
    }
}
