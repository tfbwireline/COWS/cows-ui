﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SiptEventTollType
    {
        public int EventId { get; set; }
        public byte SiptTollTypeId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkSiptTollType SiptTollType { get; set; }
    }
}
