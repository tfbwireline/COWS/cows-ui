﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptPrvsn
    {
        public int CptPrvsnId { get; set; }
        public int CptId { get; set; }
        public short CptPrvsnTypeId { get; set; }
        public bool? PrvsnStusCd { get; set; }
        public string PrvsnNteTxt { get; set; }
        public string SrvrNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public short RecStusId { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptPrvsnType CptPrvsnType { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkStus RecStus { get; set; }
    }
}
