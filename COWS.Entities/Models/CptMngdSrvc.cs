﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptMngdSrvc
    {
        public int CptMngdSrvcId { get; set; }
        public int CptId { get; set; }
        public short CptHstMngdSrvcId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptHstMngdSrvc CptHstMngdSrvc { get; set; }
    }
}
