﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkVndr
    {
        public LkVndr()
        {
            LkVndrEmailLang = new HashSet<LkVndrEmailLang>();
            MplsEvent = new HashSet<MplsEvent>();
            NccoOrdr = new HashSet<NccoOrdr>();
            UserWfm = new HashSet<UserWfm>();
            VndrFoldr = new HashSet<VndrFoldr>();
        }

        public string VndrCd { get; set; }
        public string VndrNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<LkVndrEmailLang> LkVndrEmailLang { get; set; }
        public virtual ICollection<MplsEvent> MplsEvent { get; set; }
        public virtual ICollection<NccoOrdr> NccoOrdr { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
        public virtual ICollection<VndrFoldr> VndrFoldr { get; set; }
    }
}
