﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventDslSbicCustTrpt
    {
        public int DslSbicCustTrptId { get; set; }
        public int EventId { get; set; }
        public string PrimBkupCd { get; set; }
        public string VndrPrvdrTrptCktId { get; set; }
        public string VndrPrvdrNme { get; set; }
        public string IsWirelessCd { get; set; }
        public string BwEsnMeid { get; set; }
        public string ScaNbr { get; set; }
        public string IpAdr { get; set; }
        public string SubnetMaskAdr { get; set; }
        public string NxthopGtwyAdr { get; set; }
        public string SprintMngdCd { get; set; }
        public string OdieDevNme { get; set; }
        public DateTime CreatDt { get; set; }
        public string MdsTrnsprtType { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual Event Event { get; set; }
    }
}
