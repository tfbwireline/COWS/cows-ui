﻿namespace COWS.Entities.Models
{
    public class CCDBypassUserView
    {
        public int ConfigID { get; set; }
        public string UserADID { get; set; }   /*Varchar 10*/
        public string UserName { get; set; }   /*Varchar 100*/
    }
}