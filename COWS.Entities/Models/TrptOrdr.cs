﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class TrptOrdr
    {
        public int OrdrId { get; set; }
        public byte RecStusId { get; set; }
        public string CmntTxt { get; set; }
        public short? SalsChnlId { get; set; }
        public int? XnciAsnMgrId { get; set; }
        public short? CcdMissdReasId { get; set; }
        public DateTime? OrdrAsnVwDt { get; set; }
        public DateTime? CustAcptdDt { get; set; }
        public DateTime? BillClearDt { get; set; }
        public DateTime? CustCntrcXpirDt { get; set; }
        public int? CktLgthInKmsQty { get; set; }
        public int? AccsInMbValuQty { get; set; }
        public string AEndSpclCustAccsDetlTxt { get; set; }
        public bool AEndNewCktCd { get; set; }
        public short? AEndRgnId { get; set; }
        public string AEndRegion { get; set; }
        public string BEndSpclCustAccsDetlTxt { get; set; }
        public bool BEndNewCktCd { get; set; }
        public string VndrLocalAccsPrvdrGrpNme { get; set; }
        public short? VndrTypeId { get; set; }
        public short? IptAvlbltyId { get; set; }
        public string CustBillCurId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string VndrOrdrId { get; set; }
        public string TotHoldDrtnTmeTxt { get; set; }
        public int? AccsCtySiteId { get; set; }
        public bool? NrmUpdtCd { get; set; }
        public DateTime? CnfrmRnlDt { get; set; }
        public string AccsMbDes { get; set; }
        public DateTime? StdiDt { get; set; }
        public byte? StdiReasId { get; set; }

        public virtual LkXnciRgn AEndRgn { get; set; }
        public virtual LkAccsCtySite AccsCtySite { get; set; }
        public virtual LkCcdMissdReas CcdMissdReas { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCur CustBillCur { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkStdiReas StdiReas { get; set; }
        public virtual LkUser XnciAsnMgr { get; set; }
    }
}
