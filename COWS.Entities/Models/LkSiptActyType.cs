﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkSiptActyType
    {
        public LkSiptActyType()
        {
            SiptEventActy = new HashSet<SiptEventActy>();
        }

        public short SiptActyTypeId { get; set; }
        public string SiptActyTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<SiptEventActy> SiptEventActy { get; set; }
    }
}
