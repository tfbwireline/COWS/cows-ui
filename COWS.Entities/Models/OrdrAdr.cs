﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OrdrAdr
    {
        public int OrdrAdrId { get; set; }
        public int OrdrId { get; set; }
        public byte AdrTypeId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public string CtryCd { get; set; }
        public string CisLvlType { get; set; }
        public string FsaMdulId { get; set; }
        public bool HierLvlCd { get; set; }
        public string StreetAdr1 { get; set; }
        public string StreetAdr2 { get; set; }
        public string CtyNme { get; set; }
        public string PrvnNme { get; set; }
        public string SttCd { get; set; }
        public string ZipPstlCd { get; set; }
        public string BldgNme { get; set; }
        public string FlrId { get; set; }
        public string RmNbr { get; set; }
        public string StreetAdr3 { get; set; }

        public virtual LkAdrType AdrType { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
