﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkXnciMsSla
    {
        public int MsSlaId { get; set; }
        public short MsId { get; set; }
        public string PltfrmCd { get; set; }
        public byte? OrdrTypeId { get; set; }
        public short SlaInDayQty { get; set; }
        public short? ToMsId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkXnciMs Ms { get; set; }
        public virtual LkOrdrType OrdrType { get; set; }
        public virtual LkPltfrm PltfrmCdNavigation { get; set; }
        public virtual LkXnciMs ToMs { get; set; }
    }
}
