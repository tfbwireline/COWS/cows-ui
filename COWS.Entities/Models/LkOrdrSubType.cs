﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkOrdrSubType
    {
        public byte OrdrSubTypeId { get; set; }
        public string OrdrSubTypeCd { get; set; }
        public string OrdrSubTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
