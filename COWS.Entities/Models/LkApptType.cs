﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkApptType
    {
        public LkApptType()
        {
            Appt = new HashSet<Appt>();
            ApptRcurncData = new HashSet<ApptRcurncData>();
        }

        public int ApptTypeId { get; set; }
        public string ApptTypeDes { get; set; }
        public byte? RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<Appt> Appt { get; set; }
        public virtual ICollection<ApptRcurncData> ApptRcurncData { get; set; }
    }
}
