﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkEsclReas
    {
        public LkEsclReas()
        {
            AdEvent = new HashSet<AdEvent>();
            MdsEvent = new HashSet<MdsEvent>();
            MdsEventNew = new HashSet<MdsEventNew>();
            MplsEvent = new HashSet<MplsEvent>();
            NgvnEvent = new HashSet<NgvnEvent>();
            SplkEvent = new HashSet<SplkEvent>();
            UcaaSEvent = new HashSet<UcaaSEvent>();
        }

        public byte EsclReasId { get; set; }
        public string EsclReasDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<AdEvent> AdEvent { get; set; }
        public virtual ICollection<MdsEvent> MdsEvent { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNew { get; set; }
        public virtual ICollection<MplsEvent> MplsEvent { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEvent { get; set; }
        public virtual ICollection<SplkEvent> SplkEvent { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEvent { get; set; }
    }
}
