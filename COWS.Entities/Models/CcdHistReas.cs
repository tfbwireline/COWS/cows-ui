﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CcdHistReas
    {
        public int CcdHistId { get; set; }
        public short CcdMissdReasId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual CcdHist CcdHist { get; set; }
        public virtual LkCcdMissdReas CcdMissdReas { get; set; }
    }
}
