﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventNvldTmeSlot
    {
        public int EventId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
    }
}
