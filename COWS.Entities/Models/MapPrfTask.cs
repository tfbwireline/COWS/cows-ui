﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MapPrfTask
    {
        public int MapPrfTaskId { get; set; }
        public short TaskId { get; set; }
        public short PrntPrfId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkUsrPrf PrntPrf { get; set; }
        public virtual LkTask Task { get; set; }
    }
}
