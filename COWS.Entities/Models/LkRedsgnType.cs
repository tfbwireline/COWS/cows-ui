﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkRedsgnType
    {
        public LkRedsgnType()
        {
            Redsgn = new HashSet<Redsgn>();
        }

        public byte RedsgnTypeId { get; set; }
        public string RedsgnTypeNme { get; set; }
        public string RedsgnTypeDes { get; set; }
        public bool RecstusId { get; set; }
        public DateTime CretdDt { get; set; }
        public byte? RedsgnCatId { get; set; }
        public bool IsBlblCd { get; set; }
        public bool? OdieReqCd { get; set; }

        public virtual LkRedsgnCat RedsgnCat { get; set; }
        public virtual ICollection<Redsgn> Redsgn { get; set; }
    }
}
