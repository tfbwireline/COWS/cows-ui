﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventNtwkTrpt
    {
        public int MdsEventNtwkTrptId { get; set; }
        public int EventId { get; set; }
        public string MdsTrnsprtType { get; set; }
        public string LecPrvdrNme { get; set; }
        public string LecCntctInfo { get; set; }
        public string Mach5OrdrNbr { get; set; }
        public string IpNuaAdr { get; set; }
        public string MplsAccsBdwd { get; set; }
        public string ScaNbr { get; set; }
        public bool? TaggdCd { get; set; }
        public string VlanNbr { get; set; }
        public string MultiVrfReq { get; set; }
        public string IpVer { get; set; }
        public string LocCity { get; set; }
        public string LocSttPrvn { get; set; }
        public string LocCtry { get; set; }
        public string AssocH6 { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string DdAprvlNbr { get; set; }
        public string VasType { get; set; }
        public string CeSrvcId { get; set; }
        public string BdwdNme { get; set; }

        public virtual Event Event { get; set; }
    }
}
