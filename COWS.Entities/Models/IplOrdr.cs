﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class IplOrdr
    {
        public int OrdrId { get; set; }
        public bool ReSbmsnCd { get; set; }
        public DateTime? CustCntrcSignDt { get; set; }
        public DateTime? CustOrdrSbmtDt { get; set; }
        public DateTime? CustWantDt { get; set; }
        public DateTime? CustCmmtDt { get; set; }
        public bool CustAcptErlySrvcCd { get; set; }
        public bool NewCnstrctnCd { get; set; }
        public bool UnmanSiteCd { get; set; }
        public short? DscnctReasCd { get; set; }
        public string TspCd { get; set; }
        public string ScaNbr { get; set; }
        public bool GvrmntCustCd { get; set; }
        public string PreXstPlNbr { get; set; }
        public bool H5EsAcctToBeDscnctCd { get; set; }
        public string PrsPriceQotNbr { get; set; }
        public string GcsPriceQotNbr { get; set; }
        public string CustCntrcLgthId { get; set; }
        public string SalsPersnNme { get; set; }
        public string ClcmNme { get; set; }
        public string Soi { get; set; }
        public string SalsPersnCid { get; set; }
        public string SalsBrNme { get; set; }
        public short? OrdrStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte? ProdTypeId { get; set; }
        public string SrvcTypeCd { get; set; }
        public string CktTypeCd { get; set; }
        public byte OrdrTypeId { get; set; }
        public string H5CustAcctNbr { get; set; }
        public string OrgtngCharsId { get; set; }
        public string TrmtgCharsId { get; set; }
        public string OrgtngCoId { get; set; }
        public string TrmtgCoCd { get; set; }
        public string OeqptRadBoxNeedCd { get; set; }
        public string OeqptCsuDsuCd { get; set; }
        public string OeqptPreXstClliCd { get; set; }
        public decimal? OeqptCpeDiscPct { get; set; }
        public string OeqptCpeCmntTxt { get; set; }
        public string CalBefDspchHrTxt { get; set; }
        public bool XtnlMoveCd { get; set; }
        public int? CktSpdQty { get; set; }
        public string OrdrCmntTxt { get; set; }
        public string SalsPersnCoCd { get; set; }
        public byte? PrevOrdrTypeId { get; set; }
        public int? ReltdOrdrId { get; set; }
        public string BasePlanTypeNme { get; set; }
        public string PrmtCd { get; set; }
        public string OrgtngCmntTxt { get; set; }
        public string TrmtgCmntTxt { get; set; }
        public string PlCatTxt { get; set; }
        public string ClcmPhnNbr { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkStus OrdrStus { get; set; }
        public virtual LkOrdrType OrdrType { get; set; }
        public virtual LkOrdrType PrevOrdrType { get; set; }
        public virtual LkProdType ProdType { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
