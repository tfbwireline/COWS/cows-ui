﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MplsEventAccsTag
    {
        public int MplsEventAccsTagId { get; set; }
        public int EventId { get; set; }
        public string LocCtyNme { get; set; }
        public string LocSttNme { get; set; }
        public string VasSolNbr { get; set; }
        public string TrsNet449Adr { get; set; }
        public byte? MplsAccsBdwdId { get; set; }
        public string MnsSipAdr { get; set; }
        public byte? MnsOffrgRoutrId { get; set; }
        public string MnsRoutgTypeId { get; set; }
        public byte? MnsPrfmcRptId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public string CtryCd { get; set; }
        public string PlDalCktNbr { get; set; }
        public string TrnsprtOeFtnNbr { get; set; }

        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual MplsEvent Event { get; set; }
        public virtual LkMnsOffrgRoutr MnsOffrgRoutr { get; set; }
        public virtual LkMnsPrfmcRpt MnsPrfmcRpt { get; set; }
        public virtual LkMnsRoutgType MnsRoutgType { get; set; }
        public virtual LkMplsAccsBdwd MplsAccsBdwd { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
