﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkAdrType
    {
        public LkAdrType()
        {
            FedlineEventAdr = new HashSet<FedlineEventAdr>();
            OrdrAdr = new HashSet<OrdrAdr>();
        }

        public byte AdrTypeId { get; set; }
        public string AdrTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<FedlineEventAdr> FedlineEventAdr { get; set; }
        public virtual ICollection<OrdrAdr> OrdrAdr { get; set; }
    }
}
