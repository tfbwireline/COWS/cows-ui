﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MplsEventActyType
    {
        public int EventId { get; set; }
        public byte MplsActyTypeId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkMplsActyType MplsActyType { get; set; }
    }
}
