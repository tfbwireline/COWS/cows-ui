﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class NrmCkt
    {
        public int NrmCktId { get; set; }
        public long? SrvcInstcObjId { get; set; }
        public string SrvcInstcNme { get; set; }
        public string Ftn { get; set; }
        public string CktObjId { get; set; }
        public string CktNme { get; set; }
        public string FmsCktId { get; set; }
        public string NuaAdr { get; set; }
        public string PlnNme { get; set; }
        public string PlnSeqNbr { get; set; }
        public string TocAdr { get; set; }
        public string PortAsmtDes { get; set; }
        public string TocNme { get; set; }
        public string FmsSiteId { get; set; }
        public DateTime CreatDt { get; set; }
        public string TocAdr1 { get; set; }
        public string TocAdr2 { get; set; }
        public string TocCtyNme { get; set; }
        public string TocDmstcSttNme { get; set; }
        public string TocZipCd { get; set; }
        public string TocCtryCd { get; set; }
        public string TocCtryNme { get; set; }
        public byte RecStusId { get; set; }
        public DateTime ModfdDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
    }
}
