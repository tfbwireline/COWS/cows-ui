﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnH1MrcValues
    {
        public string H1Id { get; set; }
        public byte RedsgnCatId { get; set; }
        public decimal MrcRateVsNrc { get; set; }
        public bool RecStusId { get; set; }

        public virtual LkRedsgnCat RedsgnCat { get; set; }
    }
}
