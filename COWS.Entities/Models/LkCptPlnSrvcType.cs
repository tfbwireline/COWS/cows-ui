﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCptPlnSrvcType
    {
        public LkCptPlnSrvcType()
        {
            CptSrvcType = new HashSet<CptSrvcType>();
        }

        public short CptPlnSrvcTypeId { get; set; }
        public string CptPlnSrvcType { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CptSrvcType> CptSrvcType { get; set; }
    }
}
