﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkActyLocale
    {
        public LkActyLocale()
        {
            MplsEvent = new HashSet<MplsEvent>();
        }

        public string ActyLocaleId { get; set; }
        public string ActyLocaleDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<MplsEvent> MplsEvent { get; set; }
    }
}
