﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineEventCfgrnData
    {
        public int FedlineEventCfgrnId { get; set; }
        public int FedlineEventId { get; set; }
        public string PortNme { get; set; }
        public string IpAdr { get; set; }
        public string SubnetMaskAdr { get; set; }
        public string DfltGtwyAdr { get; set; }
        public string SpeedNme { get; set; }
        public string DxNme { get; set; }
        public string MacAdr { get; set; }

        public virtual FedlineEventTadpoleData FedlineEvent { get; set; }
    }
}
