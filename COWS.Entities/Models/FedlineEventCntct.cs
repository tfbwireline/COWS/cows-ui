﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineEventCntct
    {
        public int FedlineEventCntctId { get; set; }
        public int FedlineEventId { get; set; }
        public byte RoleId { get; set; }
        public byte CntctTypeId { get; set; }
        public string PhnNbr { get; set; }
        public DateTime CreatDt { get; set; }
        public string PhnExtNbr { get; set; }
        public byte[] CntctNme { get; set; }
        public byte[] EmailAdr { get; set; }

        public virtual LkCntctType CntctType { get; set; }
        public virtual FedlineEventTadpoleData FedlineEvent { get; set; }
        public virtual LkRole Role { get; set; }
    }
}
