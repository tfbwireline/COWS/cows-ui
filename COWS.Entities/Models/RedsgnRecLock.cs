﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnRecLock
    {
        public int RedsgnId { get; set; }
        public DateTime StrtRecLockTmst { get; set; }
        public int LockByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Redsgn Redsgn { get; set; }
    }
}
