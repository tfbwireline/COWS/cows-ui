﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkEventStus
    {
        public LkEventStus()
        {
            AdEvent = new HashSet<AdEvent>();
            FedlineEventPrcsEventEndStus = new HashSet<FedlineEventPrcs>();
            FedlineEventPrcsEventStus = new HashSet<FedlineEventPrcs>();
            FedlineEventUserData = new HashSet<FedlineEventUserData>();
            LkEventRuleEndEventStus = new HashSet<LkEventRule>();
            LkEventRuleStrtEventStus = new HashSet<LkEventRule>();
            MdsEventEventStus = new HashSet<MdsEvent>();
            MdsEventNewEventStus = new HashSet<MdsEventNew>();
            MdsEventNewOldEventStus = new HashSet<MdsEventNew>();
            MdsEventOldEventStus = new HashSet<MdsEvent>();
            MplsEvent = new HashSet<MplsEvent>();
            NgvnEvent = new HashSet<NgvnEvent>();
            SiptEvent = new HashSet<SiptEvent>();
            SplkEvent = new HashSet<SplkEvent>();
            UcaaSEventEventStus = new HashSet<UcaaSEvent>();
            UcaaSEventOldEventStus = new HashSet<UcaaSEvent>();
        }

        public byte EventStusId { get; set; }
        public string EventStusDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<AdEvent> AdEvent { get; set; }
        public virtual ICollection<FedlineEventPrcs> FedlineEventPrcsEventEndStus { get; set; }
        public virtual ICollection<FedlineEventPrcs> FedlineEventPrcsEventStus { get; set; }
        public virtual ICollection<FedlineEventUserData> FedlineEventUserData { get; set; }
        public virtual ICollection<LkEventRule> LkEventRuleEndEventStus { get; set; }
        public virtual ICollection<LkEventRule> LkEventRuleStrtEventStus { get; set; }
        public virtual ICollection<MdsEvent> MdsEventEventStus { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNewEventStus { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNewOldEventStus { get; set; }
        public virtual ICollection<MdsEvent> MdsEventOldEventStus { get; set; }
        public virtual ICollection<MplsEvent> MplsEvent { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEvent { get; set; }
        public virtual ICollection<SiptEvent> SiptEvent { get; set; }
        public virtual ICollection<SplkEvent> SplkEvent { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEventEventStus { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEventOldEventStus { get; set; }
    }
}
