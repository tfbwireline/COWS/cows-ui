﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MplsEventVasType
    {
        public int EventId { get; set; }
        public byte VasTypeId { get; set; }
        public DateTime CreatDt { get; set; }
        public int Id { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkVasType VasType { get; set; }
    }
}
