﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCustCntrcLgth
    {
        public LkCustCntrcLgth()
        {
            CktMs = new HashSet<CktMs>();
        }

        public string CustCntrcLgthId { get; set; }
        public string CustCntrcLgthDes { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CktMs> CktMs { get; set; }
    }
}
