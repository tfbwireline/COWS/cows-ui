﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class ReltdEvent
    {
        public int EventId { get; set; }
        public int ReltdEventId { get; set; }
        public short EventTypeId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int Id { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkEventType EventType { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual Event ReltdEventNavigation { get; set; }
    }
}
