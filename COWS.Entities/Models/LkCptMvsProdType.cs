﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCptMvsProdType
    {
        public LkCptMvsProdType()
        {
            CptMvsProd = new HashSet<CptMvsProd>();
        }

        public short CptMvsProdTypeId { get; set; }
        public string CptMvsProdType { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CptMvsProd> CptMvsProd { get; set; }
    }
}
