﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCcdMissdReas
    {
        public LkCcdMissdReas()
        {
            CcdHistReas = new HashSet<CcdHistReas>();
            TrptOrdr = new HashSet<TrptOrdr>();
        }

        public short CcdMissdReasId { get; set; }
        public string CcdMissdReasDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CcdHistReas> CcdHistReas { get; set; }
        public virtual ICollection<TrptOrdr> TrptOrdr { get; set; }
    }
}
