﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkUsrPrf
    {
        public LkUsrPrf()
        {
            CptUserAsmt = new HashSet<CptUserAsmt>();
            FsaOrdrGomXnci = new HashSet<FsaOrdrGomXnci>();
            LkEventRule = new HashSet<LkEventRule>();
            LkPrfHrchyChldPrf = new HashSet<LkPrfHrchy>();
            LkPrfHrchyPrntPrf = new HashSet<LkPrfHrchy>();
            LkUsrPrfMenu = new HashSet<LkUsrPrfMenu>();
            LkXnciRgn = new HashSet<LkXnciRgn>();
            MapPrfTask = new HashSet<MapPrfTask>();
            MapUsrPrf = new HashSet<MapUsrPrf>();
            UserWfm = new HashSet<UserWfm>();
            UserWfmAsmt = new HashSet<UserWfmAsmt>();
        }

        public short UsrPrfId { get; set; }
        public string UsrPrfNme { get; set; }
        public string UsrPrfDes { get; set; }
        public byte? SrchCd { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CptUserAsmt> CptUserAsmt { get; set; }
        public virtual ICollection<FsaOrdrGomXnci> FsaOrdrGomXnci { get; set; }
        public virtual ICollection<LkEventRule> LkEventRule { get; set; }
        public virtual ICollection<LkPrfHrchy> LkPrfHrchyChldPrf { get; set; }
        public virtual ICollection<LkPrfHrchy> LkPrfHrchyPrntPrf { get; set; }
        public virtual ICollection<LkUsrPrfMenu> LkUsrPrfMenu { get; set; }
        public virtual ICollection<LkXnciRgn> LkXnciRgn { get; set; }
        public virtual ICollection<MapPrfTask> MapPrfTask { get; set; }
        public virtual ICollection<MapUsrPrf> MapUsrPrf { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
        public virtual ICollection<UserWfmAsmt> UserWfmAsmt { get; set; }
    }
}
