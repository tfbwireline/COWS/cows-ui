﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkPrfHrchy
    {
        public int Id { get; set; }
        public short ChldPrfId { get; set; }
        public short PrntPrfId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUsrPrf ChldPrf { get; set; }
        public virtual LkUsrPrf PrntPrf { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
