﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkStus
    {
        public LkStus()
        {
            ActTask = new HashSet<ActTask>();
            Asr = new HashSet<Asr>();
            BillDsptch = new HashSet<BillDsptch>();
            CntctDetl = new HashSet<CntctDetl>();
            CpeClli = new HashSet<CpeClli>();
            Cpt = new HashSet<Cpt>();
            CptPrvsn = new HashSet<CptPrvsn>();
            CptRedsgn = new HashSet<CptRedsgn>();
            EmailReq = new HashSet<EmailReq>();
            FedlineEventMsg = new HashSet<FedlineEventMsg>();
            FsaOrdrGomXnci = new HashSet<FsaOrdrGomXnci>();
            IpActy = new HashSet<IpActy>();
            IpMstr = new HashSet<IpMstr>();
            IplOrdr = new HashSet<IplOrdr>();
            LkIpMstr = new HashSet<LkIpMstr>();
            LkTaskOrdrStus = new HashSet<LkTask>();
            LkTaskWgStus = new HashSet<LkTask>();
            M5BillDsptchMsg = new HashSet<M5BillDsptchMsg>();
            M5EventMsg = new HashSet<M5EventMsg>();
            NidActy = new HashSet<NidActy>();
            NrmSrvcInstc = new HashSet<NrmSrvcInstc>();
            OdieReq = new HashSet<OdieReq>();
            OrdrCktChg = new HashSet<OrdrCktChg>();
            RedsgnRedsgnDevStus = new HashSet<Redsgn>();
            RedsgnStus = new HashSet<Redsgn>();
            RedsgnWrkflwDesrdWrkflwStus = new HashSet<RedsgnWrkflw>();
            RedsgnWrkflwPreWrkflwStus = new HashSet<RedsgnWrkflw>();
            SdeOpptnty = new HashSet<SdeOpptnty>();
            SstatReq = new HashSet<SstatReq>();
            VndrOrdrEmail = new HashSet<VndrOrdrEmail>();
        }

        public string StusDes { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public string StusTypeId { get; set; }
        public short StusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkStusType StusType { get; set; }
        public virtual ICollection<ActTask> ActTask { get; set; }
        public virtual ICollection<Asr> Asr { get; set; }
        public virtual ICollection<BillDsptch> BillDsptch { get; set; }
        public virtual ICollection<CntctDetl> CntctDetl { get; set; }
        public virtual ICollection<CpeClli> CpeClli { get; set; }
        public virtual ICollection<Cpt> Cpt { get; set; }
        public virtual ICollection<CptPrvsn> CptPrvsn { get; set; }
        public virtual ICollection<CptRedsgn> CptRedsgn { get; set; }
        public virtual ICollection<EmailReq> EmailReq { get; set; }
        public virtual ICollection<FedlineEventMsg> FedlineEventMsg { get; set; }
        public virtual ICollection<FsaOrdrGomXnci> FsaOrdrGomXnci { get; set; }
        public virtual ICollection<IpActy> IpActy { get; set; }
        public virtual ICollection<IpMstr> IpMstr { get; set; }
        public virtual ICollection<IplOrdr> IplOrdr { get; set; }
        public virtual ICollection<LkIpMstr> LkIpMstr { get; set; }
        public virtual ICollection<LkTask> LkTaskOrdrStus { get; set; }
        public virtual ICollection<LkTask> LkTaskWgStus { get; set; }
        public virtual ICollection<M5BillDsptchMsg> M5BillDsptchMsg { get; set; }
        public virtual ICollection<M5EventMsg> M5EventMsg { get; set; }
        public virtual ICollection<NidActy> NidActy { get; set; }
        public virtual ICollection<NrmSrvcInstc> NrmSrvcInstc { get; set; }
        public virtual ICollection<OdieReq> OdieReq { get; set; }
        public virtual ICollection<OrdrCktChg> OrdrCktChg { get; set; }
        public virtual ICollection<Redsgn> RedsgnRedsgnDevStus { get; set; }
        public virtual ICollection<Redsgn> RedsgnStus { get; set; }
        public virtual ICollection<RedsgnWrkflw> RedsgnWrkflwDesrdWrkflwStus { get; set; }
        public virtual ICollection<RedsgnWrkflw> RedsgnWrkflwPreWrkflwStus { get; set; }
        public virtual ICollection<SdeOpptnty> SdeOpptnty { get; set; }
        public virtual ICollection<SstatReq> SstatReq { get; set; }
        public virtual ICollection<VndrOrdrEmail> VndrOrdrEmail { get; set; }
    }
}
