﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFedMsg
    {
        public LkFedMsg()
        {
            LkEventRule = new HashSet<LkEventRule>();
        }

        public byte FedMsgId { get; set; }
        public string FedMsgDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<LkEventRule> LkEventRule { get; set; }
    }
}
