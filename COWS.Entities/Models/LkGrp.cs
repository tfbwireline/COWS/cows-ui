﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkGrp
    {
        public LkGrp()
        {
            FsaOrdrGomXnci = new HashSet<FsaOrdrGomXnci>();
            LkXnciRgn = new HashSet<LkXnciRgn>();
            MapGrpTask = new HashSet<MapGrpTask>();
            UserWfm = new HashSet<UserWfm>();
            UserWfmAsmt = new HashSet<UserWfmAsmt>();
        }

        public short GrpId { get; set; }
        public string GrpNme { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<FsaOrdrGomXnci> FsaOrdrGomXnci { get; set; }
        public virtual ICollection<LkXnciRgn> LkXnciRgn { get; set; }
        public virtual ICollection<MapGrpTask> MapGrpTask { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
        public virtual ICollection<UserWfmAsmt> UserWfmAsmt { get; set; }
    }
}
