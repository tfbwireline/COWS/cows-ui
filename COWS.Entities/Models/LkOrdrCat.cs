﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkOrdrCat
    {
        public LkOrdrCat()
        {
            LkProdType = new HashSet<LkProdType>();
            Ordr = new HashSet<Ordr>();
        }

        public short OrdrCatId { get; set; }
        public string OrdrCatDes { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<LkProdType> LkProdType { get; set; }
        public virtual ICollection<Ordr> Ordr { get; set; }
    }
}
