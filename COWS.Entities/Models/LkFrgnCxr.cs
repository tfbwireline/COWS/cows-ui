﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFrgnCxr
    {
        public LkFrgnCxr()
        {
            LkCtryCxr = new HashSet<LkCtryCxr>();
        }

        public string CxrCd { get; set; }
        public string CxrNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<LkCtryCxr> LkCtryCxr { get; set; }
    }
}
