﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class VndrFoldr
    {
        public VndrFoldr()
        {
            VndrFoldrCntct = new HashSet<VndrFoldrCntct>();
            VndrOrdr = new HashSet<VndrOrdr>();
            VndrTmplt = new HashSet<VndrTmplt>();
        }

        public int VndrFoldrId { get; set; }
        public string VndrCd { get; set; }
        public string StreetAdr1 { get; set; }
        public string StreetAdr2 { get; set; }
        public string CtyNme { get; set; }
        public string PrvnNme { get; set; }
        public string ZipPstlCd { get; set; }
        public string SttCd { get; set; }
        public string CmntTxt { get; set; }
        public string VndrEmailListTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public string CtryCd { get; set; }
        public string BldgNme { get; set; }
        public string FlrId { get; set; }
        public string RmNbr { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUsStt SttCdNavigation { get; set; }
        public virtual LkVndr VndrCdNavigation { get; set; }
        public virtual ICollection<VndrFoldrCntct> VndrFoldrCntct { get; set; }
        public virtual ICollection<VndrOrdr> VndrOrdr { get; set; }
        public virtual ICollection<VndrTmplt> VndrTmplt { get; set; }
    }
}
