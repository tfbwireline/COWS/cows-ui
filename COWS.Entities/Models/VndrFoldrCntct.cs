﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class VndrFoldrCntct
    {
        public int CntctId { get; set; }
        public int VndrFoldrId { get; set; }
        public string CntctFrstNme { get; set; }
        public string CntctLstNme { get; set; }
        public string CntctEmailAdr { get; set; }
        public string CntctProdTypeTxt { get; set; }
        public string CntctProdRoleTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CntctPhnNbr { get; set; }

        public virtual VndrFoldr VndrFoldr { get; set; }
    }
}
