﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkAccsCtySite
    {
        public LkAccsCtySite()
        {
            TrptOrdr = new HashSet<TrptOrdr>();
        }

        public int AccsCtySiteId { get; set; }
        public string AccsCtyNmeSiteCd { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<TrptOrdr> TrptOrdr { get; set; }
    }
}
