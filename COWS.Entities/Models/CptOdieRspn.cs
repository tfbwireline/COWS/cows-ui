﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptOdieRspn
    {
        public int CptOdieRspnId { get; set; }
        public int CptId { get; set; }
        public int ReqId { get; set; }
        public int AsnUsrId { get; set; }
        public string AsnUsrNme { get; set; }
        public DateTime CreatDt { get; set; }
        public short RecStusId { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual OdieReq Req { get; set; }
    }
}
