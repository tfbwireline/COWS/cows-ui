﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class UserWfm
    {
        public int WfmId { get; set; }
        public int UserId { get; set; }
        public short GrpId { get; set; }
        public byte RoleId { get; set; }
        public byte? OrdrTypeId { get; set; }
        public byte? ProdTypeId { get; set; }
        public string PltfrmCd { get; set; }
        public string VndrCd { get; set; }
        public string OrgtngCtryCd { get; set; }
        public string IplTrmtgCtryCd { get; set; }
        public byte? OrdrActnId { get; set; }
        public byte? WfmAsmtLvlId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short? UsrPrfId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkGrp Grp { get; set; }
        public virtual LkCtry IplTrmtgCtryCdNavigation { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkOrdrActn OrdrActn { get; set; }
        public virtual LkOrdrType OrdrType { get; set; }
        public virtual LkCtry OrgtngCtryCdNavigation { get; set; }
        public virtual LkPltfrm PltfrmCdNavigation { get; set; }
        public virtual LkProdType ProdType { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkRole Role { get; set; }
        public virtual LkUser User { get; set; }
        public virtual LkUsrPrf UsrPrf { get; set; }
        public virtual LkVndr VndrCdNavigation { get; set; }
    }
}
