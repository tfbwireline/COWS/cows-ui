﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCktChgType
    {
        public LkCktChgType()
        {
            OrdrCktChg = new HashSet<OrdrCktChg>();
        }

        public short CktChgTypeId { get; set; }
        public string CktChgTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<OrdrCktChg> OrdrCktChg { get; set; }
    }
}
