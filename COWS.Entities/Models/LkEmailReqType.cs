﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkEmailReqType
    {
        public LkEmailReqType()
        {
            EmailReq = new HashSet<EmailReq>();
        }

        public int EmailReqTypeId { get; set; }
        public string EmailReqTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public string EmailReqType { get; set; }

        public virtual ICollection<EmailReq> EmailReq { get; set; }
    }
}
