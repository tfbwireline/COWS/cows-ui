﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFedlineEventFailCd
    {
        public LkFedlineEventFailCd()
        {
            FedlineEventUserData = new HashSet<FedlineEventUserData>();
            LkFedRuleFail = new HashSet<LkFedRuleFail>();
        }

        public short FailCdId { get; set; }
        public string FailCdDes { get; set; }
        public byte RecStusId { get; set; }
        public int? CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string RspbPartyNme { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<FedlineEventUserData> FedlineEventUserData { get; set; }
        public virtual ICollection<LkFedRuleFail> LkFedRuleFail { get; set; }
    }
}
