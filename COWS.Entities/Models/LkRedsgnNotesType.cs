﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkRedsgnNotesType
    {
        public LkRedsgnNotesType()
        {
            RedsgnNotes = new HashSet<RedsgnNotes>();
        }

        public byte RedsgnNteTypeId { get; set; }
        public string RedsgnNteTypeCd { get; set; }
        public string RedsgnNteTypeDes { get; set; }
        public bool RecStusId { get; set; }
        public DateTime CretdDt { get; set; }

        public virtual ICollection<RedsgnNotes> RedsgnNotes { get; set; }
    }
}
