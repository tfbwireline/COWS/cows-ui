﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFedRuleFail
    {
        public byte FedRuleFailId { get; set; }
        public int EventRuleId { get; set; }
        public short FailCdId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkEventRule EventRule { get; set; }
        public virtual LkFedlineEventFailCd FailCd { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
