﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkRedsgnCat
    {
        public LkRedsgnCat()
        {
            LkRedsgnType = new HashSet<LkRedsgnType>();
            Redsgn = new HashSet<Redsgn>();
            RedsgnH1MrcValues = new HashSet<RedsgnH1MrcValues>();
        }

        public byte RedsgnCatId { get; set; }
        public string RedsgnCatDes { get; set; }
        public bool RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatUserId { get; set; }

        public virtual LkUser CreatUser { get; set; }
        public virtual ICollection<LkRedsgnType> LkRedsgnType { get; set; }
        public virtual ICollection<Redsgn> Redsgn { get; set; }
        public virtual ICollection<RedsgnH1MrcValues> RedsgnH1MrcValues { get; set; }
    }
}
