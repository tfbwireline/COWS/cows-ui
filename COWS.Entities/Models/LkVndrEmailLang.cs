﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkVndrEmailLang
    {
        public short VndrEmailLangId { get; set; }
        public string VndrCd { get; set; }
        public string LangNme { get; set; }
        public string SubLangNme { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CtryCd { get; set; }
        public string CtyNme { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCtryCty Ct { get; set; }
        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkVndr VndrCdNavigation { get; set; }
    }
}
