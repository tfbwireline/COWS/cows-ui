﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineEventUserData
    {
        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public string CmpltEmailCcAdr { get; set; }
        public int? EngrUserId { get; set; }
        public string ShippingCxrNme { get; set; }
        public string TrkNbr { get; set; }
        public DateTime? ArrivalDt { get; set; }
        public bool PreStgCmtltCd { get; set; }
        public int? ActvUserId { get; set; }
        public short? FailCdId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public string DevTypeNme { get; set; }
        public string DocLinkTxt { get; set; }
        public string RdsnNbr { get; set; }
        public string TnnlCd { get; set; }
        public string SlaCd { get; set; }
        public byte[] EventTitleTxt { get; set; }

        public virtual LkUser ActvUser { get; set; }
        public virtual LkUser EngrUser { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkEventStus EventStus { get; set; }
        public virtual LkFedlineEventFailCd FailCd { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
    }
}
