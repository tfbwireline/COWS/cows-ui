﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class Cpt
    {
        public Cpt()
        {
            CptDoc = new HashSet<CptDoc>();
            CptHist = new HashSet<CptHist>();
            CptMngdAuth = new HashSet<CptMngdAuth>();
            CptMngdSrvc = new HashSet<CptMngdSrvc>();
            CptMvsProd = new HashSet<CptMvsProd>();
            CptOdieRspn = new HashSet<CptOdieRspn>();
            CptPrimSite = new HashSet<CptPrimSite>();
            CptPrimWan = new HashSet<CptPrimWan>();
            CptPrvsn = new HashSet<CptPrvsn>();
            CptRedsgn = new HashSet<CptRedsgn>();
            CptRltdInfo = new HashSet<CptRltdInfo>();
            CptScndyTprt = new HashSet<CptScndyTprt>();
            CptSrvcType = new HashSet<CptSrvcType>();
            CptSuprtTier = new HashSet<CptSuprtTier>();
            CptUserAsmt = new HashSet<CptUserAsmt>();
            EmailReq = new HashSet<EmailReq>();
            OdieReq = new HashSet<OdieReq>();
        }

        public int CptId { get; set; }
        public string CptCustNbr { get; set; }
        public string CustShrtNme { get; set; }
        public string SubnetIpAdr { get; set; }
        public string StartIpAdr { get; set; }
        public string EndIpAdr { get; set; }
        public string QipSubnetNme { get; set; }
        public string MplsIpAdr { get; set; }
        public string WpaaSIpAdr { get; set; }
        public int SubmtrUserId { get; set; }
        public string SeEmailAdr { get; set; }
        public string AmEmailAdr { get; set; }
        public string IpmEmailAdr { get; set; }
        public string H1 { get; set; }
        public short DevCnt { get; set; }
        public bool? GovtCustCd { get; set; }
        public bool? ScrtyClrnceCd { get; set; }
        public bool SprntWhlslReslrCd { get; set; }
        public string SprntWhlslReslrNme { get; set; }
        public bool TacacsCd { get; set; }
        public bool EncMdmCd { get; set; }
        public bool HstMgndSrvcCd { get; set; }
        public short CptStusId { get; set; }
        public int? CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public string PreShrdKey { get; set; }
        public bool RevwdPmCd { get; set; }
        public bool RetrnSdeCd { get; set; }
        public bool SdwanCd { get; set; }
        public byte CsgLvlId { get; set; }
        public string CompnyNme { get; set; }
        public string CustAdr { get; set; }
        public string CustFlrBldgNme { get; set; }
        public string CustCtyNme { get; set; }
        public string CustSttPrvnNme { get; set; }
        public string CustCtryRgnNme { get; set; }
        public string CustZipCd { get; set; }
        public string PreStgdKey { get; set; }

        public virtual LkStus CptStus { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUser SubmtrUser { get; set; }
        public virtual CptRecLock CptRecLock { get; set; }
        public virtual ICollection<CptDoc> CptDoc { get; set; }
        public virtual ICollection<CptHist> CptHist { get; set; }
        public virtual ICollection<CptMngdAuth> CptMngdAuth { get; set; }
        public virtual ICollection<CptMngdSrvc> CptMngdSrvc { get; set; }
        public virtual ICollection<CptMvsProd> CptMvsProd { get; set; }
        public virtual ICollection<CptOdieRspn> CptOdieRspn { get; set; }
        public virtual ICollection<CptPrimSite> CptPrimSite { get; set; }
        public virtual ICollection<CptPrimWan> CptPrimWan { get; set; }
        public virtual ICollection<CptPrvsn> CptPrvsn { get; set; }
        public virtual ICollection<CptRedsgn> CptRedsgn { get; set; }
        public virtual ICollection<CptRltdInfo> CptRltdInfo { get; set; }
        public virtual ICollection<CptScndyTprt> CptScndyTprt { get; set; }
        public virtual ICollection<CptSrvcType> CptSrvcType { get; set; }
        public virtual ICollection<CptSuprtTier> CptSuprtTier { get; set; }
        public virtual ICollection<CptUserAsmt> CptUserAsmt { get; set; }
        public virtual ICollection<EmailReq> EmailReq { get; set; }
        public virtual ICollection<OdieReq> OdieReq { get; set; }
    }
}
