﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventSrvc
    {
        public int MdsEventSrvcTypeId { get; set; }
        public int FsaMdsEventId { get; set; }
        public int Mach5H6SrvcId { get; set; }
        public short SrvcTypeId { get; set; }
        public short? ThrdPartyVndrId { get; set; }
        public string ThrdPartyId { get; set; }
        public short? ThrdPartySrvcLvlId { get; set; }
        public DateTime? ActvDt { get; set; }
        public string CmntTxt { get; set; }
        public bool EmailCd { get; set; }
        public byte TabSeqNbr { get; set; }

        public virtual LkMdsSrvcType SrvcType { get; set; }
        public virtual LkMds3rdpartySrvcLvl ThrdPartySrvcLvl { get; set; }
        public virtual LkMds3rdpartyVndr ThrdPartyVndr { get; set; }
    }
}
