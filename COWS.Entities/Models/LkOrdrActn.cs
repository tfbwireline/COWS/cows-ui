﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkOrdrActn
    {
        public LkOrdrActn()
        {
            FsaOrdr = new HashSet<FsaOrdr>();
            UserWfm = new HashSet<UserWfm>();
        }

        public byte OrdrActnId { get; set; }
        public string OrdrActnDes { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<FsaOrdr> FsaOrdr { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
    }
}
