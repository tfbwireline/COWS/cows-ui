﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class DsplVwCol
    {
        public int DsplVwId { get; set; }
        public int DsplColId { get; set; }
        public byte DsplPosFromLeftNbr { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual DsplVw DsplVw { get; set; }
    }
}
