﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SrcCurFile
    {
        public int SrcCurFileId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntntBtsm { get; set; }
        public string PrsdCurListTxt { get; set; }
        public DateTime? PrcsdDt { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
