﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptPrimSite
    {
        public int PrimSiteId { get; set; }
        public int CptId { get; set; }
        public short CptPrimSiteId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptPrimSite CptPrimSiteNavigation { get; set; }
    }
}
