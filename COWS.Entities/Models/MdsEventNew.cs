﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventNew
    {
        public int EventId { get; set; }
        public bool MdsFastTrkCd { get; set; }
        public string MdsFastTrkTypeId { get; set; }
        public byte EventStusId { get; set; }
        public byte? OldEventStusId { get; set; }
        public byte WrkflwStusId { get; set; }
        public string CharsId { get; set; }
        public string H1Id { get; set; }
        public byte? MdsActyTypeId { get; set; }
        public string CustAcctTeamPdlNme { get; set; }
        public string ShrtDes { get; set; }
        public int? SowsEventId { get; set; }
        public short? FailReasId { get; set; }
        public bool PreCfgCmpltCd { get; set; }
        public int? CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public bool FullCustDiscCd { get; set; }
        public int? DiscoDevCnt { get; set; }
        public string DsgnDocLocTxt { get; set; }
        public string DiscNtfyPdlNme { get; set; }
        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public string AltCnfrcBrgNme { get; set; }
        public DateTime StrtTmst { get; set; }
        public short ExtraDrtnTmeAmt { get; set; }
        public DateTime EndTmst { get; set; }
        public byte RecStusId { get; set; }
        public short EventDrtnInMinQty { get; set; }
        public byte? TmeSlotId { get; set; }
        public short? EventTypeId { get; set; }
        public byte? SprintCpeNcrId { get; set; }
        public byte? SrvcAssrnSiteSuppId { get; set; }
        public string CpeDspchEmailAdr { get; set; }
        public string CpeDspchCmntTxt { get; set; }
        public bool EsclCd { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public string BusJustnTxt { get; set; }
        public int? ReqorUserId { get; set; }
        public string ReqorCellPhnNbr { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public string MnsPmId { get; set; }
        public string OdieCustId { get; set; }
        public bool? SprsEmailOnPubCd { get; set; }
        public string OptOutReasTxt { get; set; }
        public int? DevCnt { get; set; }
        public string CsgLvlCd { get; set; }
        public string BusJustnCmntTxt { get; set; }
        public byte? MdsBrdgNeedId { get; set; }
        public string EsclBusJustTxt { get; set; }
        public string OnlineMeetingAdr { get; set; }
        public DateTime? FulfilledDt { get; set; }
        public DateTime? ShippedDt { get; set; }
        public string ShipTrkRefrNbr { get; set; }
        public string ShipCustNme { get; set; }
        public string ShipCustEmailAdr { get; set; }
        public string ShipCustPhnNbr { get; set; }
        public string WoobIpAdr { get; set; }
        public string DevSerialNbr { get; set; }
        public bool? MdsBrdgNeedCd { get; set; }
        public string EventTitleTxt { get; set; }
        public string CustNme { get; set; }
        public string CustEmailAdr { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkEsclReas EsclReas { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkEventStus EventStus { get; set; }
        public virtual LkFailReas FailReas { get; set; }
        public virtual LkEventTypeTmeSlot LkEventTypeTmeSlot { get; set; }
        public virtual LkMdsBrdgNeedReas MdsBrdgNeed { get; set; }
        public virtual MdsFastTrkType MdsFastTrkType { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkEventStus OldEventStus { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUser ReqorUser { get; set; }
        public virtual LkWrkflwStus WrkflwStus { get; set; }
    }
}
