﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventHist
    {
        public EventHist()
        {
            EventFailActy = new HashSet<EventFailActy>();
            EventSucssActy = new HashSet<EventSucssActy>();
            FedlineReqActHist = new HashSet<FedlineReqActHist>();
        }

        public int EventHistId { get; set; }
        public int EventId { get; set; }
        public byte ActnId { get; set; }
        public string CmntTxt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string PreCfgCmpltCd { get; set; }
        public short? FailReasId { get; set; }
        public int? FsaMdsEventId { get; set; }
        public DateTime? EventStrtTmst { get; set; }
        public DateTime? EventEndTmst { get; set; }

        public virtual LkActn Actn { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkFailReas FailReas { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual ICollection<EventFailActy> EventFailActy { get; set; }
        public virtual ICollection<EventSucssActy> EventSucssActy { get; set; }
        public virtual ICollection<FedlineReqActHist> FedlineReqActHist { get; set; }
    }
}
