﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class UserCpeTech
    {
        public int UserCpeTechId { get; set; }
        public string UserCpeTechAdid { get; set; }
        public string UserCpeTechNme { get; set; }
        public byte? RecStusId { get; set; }
        public DateTime? CreatDt { get; set; }
    }
}
