﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkVasType
    {
        public LkVasType()
        {
            MplsEventVasType = new HashSet<MplsEventVasType>();
        }

        public byte VasTypeId { get; set; }
        public string VasTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short EventTypeId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkEventType EventType { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<MplsEventVasType> MplsEventVasType { get; set; }
    }
}
