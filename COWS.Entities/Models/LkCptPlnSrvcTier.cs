﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCptPlnSrvcTier
    {
        public LkCptPlnSrvcTier()
        {
            CptPrimWan = new HashSet<CptPrimWan>();
            CptRltdInfo = new HashSet<CptRltdInfo>();
            CptScndyTprt = new HashSet<CptScndyTprt>();
        }

        public short CptPlnSrvcTierId { get; set; }
        public string CptPlnSrvcTier { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CptPrimWan> CptPrimWan { get; set; }
        public virtual ICollection<CptRltdInfo> CptRltdInfo { get; set; }
        public virtual ICollection<CptScndyTprt> CptScndyTprt { get; set; }
    }
}
