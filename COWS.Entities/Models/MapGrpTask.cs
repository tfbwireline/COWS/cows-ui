﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MapGrpTask
    {
        public int MapGrpTaskId { get; set; }
        public short TaskId { get; set; }
        public short GrpId { get; set; }
        public int ModfdByUserId { get; set; }
        public DateTime ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkGrp Grp { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkTask Task { get; set; }
    }
}
