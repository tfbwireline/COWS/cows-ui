﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineEventTadpoleXml
    {
        public int MsgXmlId { get; set; }
        public string ErrorMsgTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public bool IsErrorCd { get; set; }
        public bool IsRejectCd { get; set; }
        public int? FrbReqId { get; set; }
        public byte[] MsgXmlTxt { get; set; }
    }
}
