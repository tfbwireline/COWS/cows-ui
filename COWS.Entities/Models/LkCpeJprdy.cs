﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCpeJprdy
    {
        public int CpeJprdyId { get; set; }
        public string CpeJprdyCd { get; set; }
        public string CpeJprdyDes { get; set; }
        public string CpeJprdyDsply { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
