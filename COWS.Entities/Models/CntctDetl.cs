﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CntctDetl
    {
        public int Id { get; set; }
        public int ObjId { get; set; }
        public string ObjTypCd { get; set; }
        public string HierLvlCd { get; set; }
        public string HierId { get; set; }
        public byte RoleId { get; set; }
        public string EmailAdr { get; set; }
        public string PhnNbr { get; set; }
        public bool? AutoRfrshCd { get; set; }
        public string EmailCd { get; set; }
        public short RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public bool SuprsEmail { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkStus RecStus { get; set; }
        public virtual LkRole Role { get; set; }
    }
}
