﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnCustBypass
    {
        public int Id { get; set; }
        public string H1Cd { get; set; }
        public bool RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte CsgLvlId { get; set; }
        public string CustNme { get; set; }

        public virtual LkCsgLvl CsgLvl { get; set; }
    }
}
