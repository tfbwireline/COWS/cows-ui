﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkUsrPrfMenu
    {
        public int UsrPrfMenuId { get; set; }
        public short UsrPrfId { get; set; }
        public short MenuId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkMenu Menu { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUsrPrf UsrPrf { get; set; }
    }
}
