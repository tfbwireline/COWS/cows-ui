﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCtryCty
    {
        public LkCtryCty()
        {
            LkVndrEmailLang = new HashSet<LkVndrEmailLang>();
            LkWrldHldy = new HashSet<LkWrldHldy>();
        }

        public string CtyNme { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CtryCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<LkVndrEmailLang> LkVndrEmailLang { get; set; }
        public virtual ICollection<LkWrldHldy> LkWrldHldy { get; set; }
    }
}
