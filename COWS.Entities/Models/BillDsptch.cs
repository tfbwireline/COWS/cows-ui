﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class BillDsptch
    {
        public int BillDsptchId { get; set; }
        public string CustId { get; set; }
        public string ClearLoc { get; set; }
        public string DispCd { get; set; }
        public string DispCatName { get; set; }
        public string DispSubcatName { get; set; }
        public string DispCaName { get; set; }
        public DateTime ClsDt { get; set; }
        public string TickNbr { get; set; }
        public string AcptRjctCd { get; set; }
        public string CmntTxt { get; set; }
        public short StusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkStus Stus { get; set; }
    }
}
