﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkUcaaSBillActy
    {
        public LkUcaaSBillActy()
        {
            UcaaSEventBilling = new HashSet<UcaaSEventBilling>();
        }

        public short UcaaSBillActyId { get; set; }
        public string UcaaSBillActyDes { get; set; }
        public string UcaaSBicType { get; set; }
        public string MrcNrcCd { get; set; }
        public short UcaaSPlanTypeId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUcaaSPlanType UcaaSPlanType { get; set; }
        public virtual ICollection<UcaaSEventBilling> UcaaSEventBilling { get; set; }
    }
}
