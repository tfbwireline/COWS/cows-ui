﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkEventTypeTmeSlot
    {
        public LkEventTypeTmeSlot()
        {
            EventTypeRsrcAvlblty = new HashSet<EventTypeRsrcAvlblty>();
            MdsEventNew = new HashSet<MdsEventNew>();
        }

        public short EventTypeId { get; set; }
        public byte TmeSlotId { get; set; }
        public TimeSpan TmeSlotStrtTme { get; set; }
        public TimeSpan TmeSlotEndTme { get; set; }
        public byte AvlbltyCapPctQty { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkEventType EventType { get; set; }
        public virtual ICollection<EventTypeRsrcAvlblty> EventTypeRsrcAvlblty { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNew { get; set; }
    }
}
