﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnEventType
    {
        public short EventTypeId { get; set; }
        public int QlfctnId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkEventType EventType { get; set; }
        public virtual LkQlfctn Qlfctn { get; set; }
    }
}
