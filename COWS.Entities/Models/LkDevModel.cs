﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkDevModel
    {
        public LkDevModel()
        {
            EventDiscoDev = new HashSet<EventDiscoDev>();
            MdsEventOdieDev = new HashSet<MdsEventOdieDev>();
            QlfctnVndrModel = new HashSet<QlfctnVndrModel>();
            UcaaSEventOdieDev = new HashSet<UcaaSEventOdieDev>();
        }

        public short DevModelId { get; set; }
        public string DevModelNme { get; set; }
        public short MinDrtnTmeReqrAmt { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short ManfId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkDevManf Manf { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<EventDiscoDev> EventDiscoDev { get; set; }
        public virtual ICollection<MdsEventOdieDev> MdsEventOdieDev { get; set; }
        public virtual ICollection<QlfctnVndrModel> QlfctnVndrModel { get; set; }
        public virtual ICollection<UcaaSEventOdieDev> UcaaSEventOdieDev { get; set; }
    }
}
