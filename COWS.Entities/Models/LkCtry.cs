﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCtry
    {
        public LkCtry()
        {
            H5Foldr = new HashSet<H5Foldr>();
            LkCtryCty = new HashSet<LkCtryCty>();
            LkCtryCxr = new HashSet<LkCtryCxr>();
            LkVndrEmailLang = new HashSet<LkVndrEmailLang>();
            LkWrldHldy = new HashSet<LkWrldHldy>();
            MplsEventAccsTag = new HashSet<MplsEventAccsTag>();
            NccoOrdr = new HashSet<NccoOrdr>();
            OrdrAdr = new HashSet<OrdrAdr>();
            UserWfmIplTrmtgCtryCdNavigation = new HashSet<UserWfm>();
            UserWfmOrgtngCtryCdNavigation = new HashSet<UserWfm>();
            VndrFoldr = new HashSet<VndrFoldr>();
        }

        public string CtryNme { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public string CtryCd { get; set; }
        public short? RgnId { get; set; }
        public string L2pCtryCd { get; set; }
        public int? IsdCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkXnciRgn Rgn { get; set; }
        public virtual ICollection<H5Foldr> H5Foldr { get; set; }
        public virtual ICollection<LkCtryCty> LkCtryCty { get; set; }
        public virtual ICollection<LkCtryCxr> LkCtryCxr { get; set; }
        public virtual ICollection<LkVndrEmailLang> LkVndrEmailLang { get; set; }
        public virtual ICollection<LkWrldHldy> LkWrldHldy { get; set; }
        public virtual ICollection<MplsEventAccsTag> MplsEventAccsTag { get; set; }
        public virtual ICollection<NccoOrdr> NccoOrdr { get; set; }
        public virtual ICollection<OrdrAdr> OrdrAdr { get; set; }
        public virtual ICollection<UserWfm> UserWfmIplTrmtgCtryCdNavigation { get; set; }
        public virtual ICollection<UserWfm> UserWfmOrgtngCtryCdNavigation { get; set; }
        public virtual ICollection<VndrFoldr> VndrFoldr { get; set; }
    }
}
