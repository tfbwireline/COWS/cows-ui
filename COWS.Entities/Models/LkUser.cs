﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkUser
    {
        public LkUser()
        {
            AdEventCreatByUser = new HashSet<AdEvent>();
            AdEventModfdByUser = new HashSet<AdEvent>();
            AdEventReqorUser = new HashSet<AdEvent>();
            AdEventSalsUser = new HashSet<AdEvent>();
            ApptCreatByUser = new HashSet<Appt>();
            ApptModfdByUser = new HashSet<Appt>();
            ApptRcurncData = new HashSet<ApptRcurncData>();
            Asr = new HashSet<Asr>();
            BillDsptchCreatByUser = new HashSet<BillDsptch>();
            BillDsptchModfdByUser = new HashSet<BillDsptch>();
            CcdHist = new HashSet<CcdHist>();
            Ckt = new HashSet<Ckt>();
            CktCost = new HashSet<CktCost>();
            CktMs = new HashSet<CktMs>();
            CntctDetlCreatByUser = new HashSet<CntctDetl>();
            CntctDetlModfdByUser = new HashSet<CntctDetl>();
            CpeClliCreatByUser = new HashSet<CpeClli>();
            CpeClliModfdByUser = new HashSet<CpeClli>();
            CptCreatByUser = new HashSet<Cpt>();
            CptDocCreatByUser = new HashSet<CptDoc>();
            CptDocModfdByUser = new HashSet<CptDoc>();
            CptHist = new HashSet<CptHist>();
            CptModfdByUser = new HashSet<Cpt>();
            CptPrvsnCreatByUser = new HashSet<CptPrvsn>();
            CptPrvsnModfdByUser = new HashSet<CptPrvsn>();
            CptRecLock = new HashSet<CptRecLock>();
            CptRedsgn = new HashSet<CptRedsgn>();
            CptSubmtrUser = new HashSet<Cpt>();
            CptUserAsmtCptUser = new HashSet<CptUserAsmt>();
            CptUserAsmtCreatByUser = new HashSet<CptUserAsmt>();
            CptUserAsmtModfdByUser = new HashSet<CptUserAsmt>();
            DsplVwCreatByUser = new HashSet<DsplVw>();
            DsplVwModfdByUser = new HashSet<DsplVw>();
            DsplVwUser = new HashSet<DsplVw>();
            EventAsnToUser = new HashSet<EventAsnToUser>();
            EventHistCreatByUser = new HashSet<EventHist>();
            EventHistModfdByUser = new HashSet<EventHist>();
            EventRecLock = new HashSet<EventRecLock>();
            EventTypeRsrcAvlblty = new HashSet<EventTypeRsrcAvlblty>();
            FedlineEventUserDataActvUser = new HashSet<FedlineEventUserData>();
            FedlineEventUserDataEngrUser = new HashSet<FedlineEventUserData>();
            FedlineEventUserDataModfdByUser = new HashSet<FedlineEventUserData>();
            FsaOrdrCscCreatByUser = new HashSet<FsaOrdrCsc>();
            FsaOrdrCscModfdByUser = new HashSet<FsaOrdrCsc>();
            FsaOrdrGomXnciCreatByUser = new HashSet<FsaOrdrGomXnci>();
            FsaOrdrGomXnciModfdByUser = new HashSet<FsaOrdrGomXnci>();
            H5DocCreatByUser = new HashSet<H5Doc>();
            H5DocModfdByUser = new HashSet<H5Doc>();
            H5FoldrCreatByUser = new HashSet<H5Foldr>();
            H5FoldrModfdByUser = new HashSet<H5Foldr>();
            IpActyCreatByUser = new HashSet<IpActy>();
            IpActyModfdByUser = new HashSet<IpActy>();
            IpMstrCreatByUser = new HashSet<IpMstr>();
            IpMstrModfdByUser = new HashSet<IpMstr>();
            IplOrdrCreatByUser = new HashSet<IplOrdr>();
            IplOrdrModfdByUser = new HashSet<IplOrdr>();
            LkActnCreatByUser = new HashSet<LkActn>();
            LkActnModfdByUser = new HashSet<LkActn>();
            LkActyLocaleCreatByUser = new HashSet<LkActyLocale>();
            LkActyLocaleModfdByUser = new HashSet<LkActyLocale>();
            LkApptTypeCreatByUser = new HashSet<LkApptType>();
            LkApptTypeModfdByUser = new HashSet<LkApptType>();
            LkAsrType = new HashSet<LkAsrType>();
            LkCcdMissdReas = new HashSet<LkCcdMissdReas>();
            LkCktChgType = new HashSet<LkCktChgType>();
            LkCnfrcBrdgCreatByUser = new HashSet<LkCnfrcBrdg>();
            LkCnfrcBrdgModfdByUser = new HashSet<LkCnfrcBrdg>();
            LkCpePidCreatByUser = new HashSet<LkCpePid>();
            LkCpePidModfdByUser = new HashSet<LkCpePid>();
            LkCptCustTypeCreatByUser = new HashSet<LkCptCustType>();
            LkCptCustTypeModfdByUser = new HashSet<LkCptCustType>();
            LkCptHstMngdSrvcCreatByUser = new HashSet<LkCptHstMngdSrvc>();
            LkCptHstMngdSrvcModfdByUser = new HashSet<LkCptHstMngdSrvc>();
            LkCptMdsSuprtTierCreatByUser = new HashSet<LkCptMdsSuprtTier>();
            LkCptMdsSuprtTierModfdByUser = new HashSet<LkCptMdsSuprtTier>();
            LkCptMngdAuthPrdCreatByUser = new HashSet<LkCptMngdAuthPrd>();
            LkCptMngdAuthPrdModfdByUser = new HashSet<LkCptMngdAuthPrd>();
            LkCptMvsProdTypeCreatByUser = new HashSet<LkCptMvsProdType>();
            LkCptMvsProdTypeModfdByUser = new HashSet<LkCptMvsProdType>();
            LkCptPlnSrvcTierCreatByUser = new HashSet<LkCptPlnSrvcTier>();
            LkCptPlnSrvcTierModfdByUser = new HashSet<LkCptPlnSrvcTier>();
            LkCptPlnSrvcTypeCreatByUser = new HashSet<LkCptPlnSrvcType>();
            LkCptPlnSrvcTypeModfdByUser = new HashSet<LkCptPlnSrvcType>();
            LkCptPrimScndyTprtCreatByUser = new HashSet<LkCptPrimScndyTprt>();
            LkCptPrimScndyTprtModfdByUser = new HashSet<LkCptPrimScndyTprt>();
            LkCptPrimSiteCreatByUser = new HashSet<LkCptPrimSite>();
            LkCptPrimSiteModfdByUser = new HashSet<LkCptPrimSite>();
            LkCptPrvsnTypeCreatByUser = new HashSet<LkCptPrvsnType>();
            LkCptPrvsnTypeModfdByUser = new HashSet<LkCptPrvsnType>();
            LkCsgLvlCreatByUser = new HashSet<LkCsgLvl>();
            LkCsgLvlModfdByUser = new HashSet<LkCsgLvl>();
            LkCtryCreatByUser = new HashSet<LkCtry>();
            LkCtryCtyCreatByUser = new HashSet<LkCtryCty>();
            LkCtryCtyModfdByUser = new HashSet<LkCtryCty>();
            LkCtryModfdByUser = new HashSet<LkCtry>();
            LkCurCreatByUser = new HashSet<LkCur>();
            LkCurModfdByUser = new HashSet<LkCur>();
            LkCustCntrcLgthCreatByUser = new HashSet<LkCustCntrcLgth>();
            LkCustCntrcLgthModfdByUser = new HashSet<LkCustCntrcLgth>();
            LkDedctdCustCreatByUser = new HashSet<LkDedctdCust>();
            LkDedctdCustModfdByUser = new HashSet<LkDedctdCust>();
            LkDevCreatByUser = new HashSet<LkDev>();
            LkDevManf = new HashSet<LkDevManf>();
            LkDevModelCreatByUser = new HashSet<LkDevModel>();
            LkDevModelModfdByUser = new HashSet<LkDevModel>();
            LkDevModfdByUser = new HashSet<LkDev>();
            LkDmstcClliCdCreatByUser = new HashSet<LkDmstcClliCd>();
            LkDmstcClliCdModfdByUser = new HashSet<LkDmstcClliCd>();
            LkEnhncSrvcCreatByUser = new HashSet<LkEnhncSrvc>();
            LkEnhncSrvcModfdByUser = new HashSet<LkEnhncSrvc>();
            LkEsclReasCreatByUser = new HashSet<LkEsclReas>();
            LkEsclReasModfdByUser = new HashSet<LkEsclReas>();
            LkEventStusCreatByUser = new HashSet<LkEventStus>();
            LkEventStusModfdByUser = new HashSet<LkEventStus>();
            LkEventTypeCreatByUser = new HashSet<LkEventType>();
            LkEventTypeModfdByUser = new HashSet<LkEventType>();
            LkFailActyCreatByUser = new HashSet<LkFailActy>();
            LkFailActyModfdByUser = new HashSet<LkFailActy>();
            LkFailReasCreatByUser = new HashSet<LkFailReas>();
            LkFailReasModfdByUser = new HashSet<LkFailReas>();
            LkFedlineEventFailCdCreatByUser = new HashSet<LkFedlineEventFailCd>();
            LkFedlineEventFailCdModfdByUser = new HashSet<LkFedlineEventFailCd>();
            LkFedlineOrdrType = new HashSet<LkFedlineOrdrType>();
            LkFiltrOprCreatByUser = new HashSet<LkFiltrOpr>();
            LkFiltrOprModfdByUser = new HashSet<LkFiltrOpr>();
            LkFrgnCxrCreatByUser = new HashSet<LkFrgnCxr>();
            LkFrgnCxrModfdByUser = new HashSet<LkFrgnCxr>();
            LkGrpCreatByUser = new HashSet<LkGrp>();
            LkGrpModfdByUser = new HashSet<LkGrp>();
            LkIntlClliCdCreatByUser = new HashSet<LkIntlClliCd>();
            LkIntlClliCdModfdByUser = new HashSet<LkIntlClliCd>();
            LkIpMstrCreatByUser = new HashSet<LkIpMstr>();
            LkIpMstrModfdByUser = new HashSet<LkIpMstr>();
            LkIpVerCreatByUser = new HashSet<LkIpVer>();
            LkIpVerModfdByUser = new HashSet<LkIpVer>();
            LkMds3rdpartySrvcLvl = new HashSet<LkMds3rdpartySrvcLvl>();
            LkMds3rdpartyVndrCreatByUser = new HashSet<LkMds3rdpartyVndr>();
            LkMds3rdpartyVndrModfdByUser = new HashSet<LkMds3rdpartyVndr>();
            LkMdsActyTypeCreatByUser = new HashSet<LkMdsActyType>();
            LkMdsActyTypeModfdByUser = new HashSet<LkMdsActyType>();
            LkMdsBrdgNeedReasCreatByUser = new HashSet<LkMdsBrdgNeedReas>();
            LkMdsBrdgNeedReasModfdByUser = new HashSet<LkMdsBrdgNeedReas>();
            LkMdsImplmtnNtvlCreatByUser = new HashSet<LkMdsImplmtnNtvl>();
            LkMdsImplmtnNtvlModfdByUser = new HashSet<LkMdsImplmtnNtvl>();
            LkMdsMacActyCreatByUser = new HashSet<LkMdsMacActy>();
            LkMdsMacActyModfdByUser = new HashSet<LkMdsMacActy>();
            LkMdsSrvcTierCreatByUser = new HashSet<LkMdsSrvcTier>();
            LkMdsSrvcTierModfdByUser = new HashSet<LkMdsSrvcTier>();
            LkMnsOffrgRoutrCreatByUser = new HashSet<LkMnsOffrgRoutr>();
            LkMnsOffrgRoutrModfdByUser = new HashSet<LkMnsOffrgRoutr>();
            LkMnsPrfmcRptCreatByUser = new HashSet<LkMnsPrfmcRpt>();
            LkMnsPrfmcRptModfdByUser = new HashSet<LkMnsPrfmcRpt>();
            LkMnsRoutgTypeCreatByUser = new HashSet<LkMnsRoutgType>();
            LkMnsRoutgTypeModfdByUser = new HashSet<LkMnsRoutgType>();
            LkMplsAccsBdwdCreatByUser = new HashSet<LkMplsAccsBdwd>();
            LkMplsAccsBdwdModfdByUser = new HashSet<LkMplsAccsBdwd>();
            LkMplsActyTypeCreatByUser = new HashSet<LkMplsActyType>();
            LkMplsActyTypeModfdByUser = new HashSet<LkMplsActyType>();
            LkMplsEventTypeCreatByUser = new HashSet<LkMplsEventType>();
            LkMplsEventTypeModfdByUser = new HashSet<LkMplsEventType>();
            LkMplsMgrtnTypeCreatByUser = new HashSet<LkMplsMgrtnType>();
            LkMplsMgrtnTypeModfdByUser = new HashSet<LkMplsMgrtnType>();
            LkMultiVrfReqCreatByUser = new HashSet<LkMultiVrfReq>();
            LkMultiVrfReqModfdByUser = new HashSet<LkMultiVrfReq>();
            LkNgvnProdTypeCreatByUser = new HashSet<LkNgvnProdType>();
            LkNgvnProdTypeModfdByUser = new HashSet<LkNgvnProdType>();
            LkNteTypeCreatByUser = new HashSet<LkNteType>();
            LkNteTypeModfdByUser = new HashSet<LkNteType>();
            LkOrdrCatCreatByUser = new HashSet<LkOrdrCat>();
            LkOrdrCatModfdByUser = new HashSet<LkOrdrCat>();
            LkPprt = new HashSet<LkPprt>();
            LkQlfctnCreatByUser = new HashSet<LkQlfctn>();
            LkQlfctnModfdByUser = new HashSet<LkQlfctn>();
            LkRedsgnCat = new HashSet<LkRedsgnCat>();
            LkRole = new HashSet<LkRole>();
            LkScrdObjTypeCreatByUser = new HashSet<LkScrdObjType>();
            LkScrdObjTypeModfdByUser = new HashSet<LkScrdObjType>();
            LkSdePrdctTypeCreatByUser = new HashSet<LkSdePrdctType>();
            LkSdePrdctTypeModfdByUser = new HashSet<LkSdePrdctType>();
            LkSiptActyTypeCreatByUser = new HashSet<LkSiptActyType>();
            LkSiptActyTypeModfdByUser = new HashSet<LkSiptActyType>();
            LkSpclProjCreatByUser = new HashSet<LkSpclProj>();
            LkSpclProjModfdByUser = new HashSet<LkSpclProj>();
            LkSplkActyTypeCreatByUser = new HashSet<LkSplkActyType>();
            LkSplkActyTypeModfdByUser = new HashSet<LkSplkActyType>();
            LkSplkEventTypeCreatByUser = new HashSet<LkSplkEventType>();
            LkSplkEventTypeModfdByUser = new HashSet<LkSplkEventType>();
            LkSprintCpeNcrCreatByUser = new HashSet<LkSprintCpeNcr>();
            LkSprintCpeNcrModfdByUser = new HashSet<LkSprintCpeNcr>();
            LkSprintHldyCreatByUser = new HashSet<LkSprintHldy>();
            LkSprintHldyModfdByUser = new HashSet<LkSprintHldy>();
            LkSrvcAssrnSiteSuppCreatByUser = new HashSet<LkSrvcAssrnSiteSupp>();
            LkSrvcAssrnSiteSuppModfdByUser = new HashSet<LkSrvcAssrnSiteSupp>();
            LkStdiReasCreatByUser = new HashSet<LkStdiReas>();
            LkStdiReasModfdByUser = new HashSet<LkStdiReas>();
            LkStusCreatByUser = new HashSet<LkStus>();
            LkStusModfdByUser = new HashSet<LkStus>();
            LkStusType = new HashSet<LkStusType>();
            LkSucssActyCreatByUser = new HashSet<LkSucssActy>();
            LkSucssActyModfdByUser = new HashSet<LkSucssActy>();
            LkTask = new HashSet<LkTask>();
            LkTelcoCreatByUser = new HashSet<LkTelco>();
            LkTelcoModfdByUser = new HashSet<LkTelco>();
            LkUcaaSActyTypeCreatByUser = new HashSet<LkUcaaSActyType>();
            LkUcaaSActyTypeModfdByUser = new HashSet<LkUcaaSActyType>();
            LkUcaaSBillActyCreatByUser = new HashSet<LkUcaaSBillActy>();
            LkUcaaSBillActyModfdByUser = new HashSet<LkUcaaSBillActy>();
            LkUcaaSPlanTypeCreatByUser = new HashSet<LkUcaaSPlanType>();
            LkUcaaSPlanTypeModfdByUser = new HashSet<LkUcaaSPlanType>();
            LkUcaaSProdTypeCreatByUser = new HashSet<LkUcaaSProdType>();
            LkUcaaSProdTypeModfdByUser = new HashSet<LkUcaaSProdType>();
            LkUsSttCreatByUser = new HashSet<LkUsStt>();
            LkUsSttModfdByUser = new HashSet<LkUsStt>();
            LkVasTypeCreatByUser = new HashSet<LkVasType>();
            LkVasTypeModfdByUser = new HashSet<LkVasType>();
            LkVndrCreatByUser = new HashSet<LkVndr>();
            LkVndrEmailLangCreatByUser = new HashSet<LkVndrEmailLang>();
            LkVndrEmailLangModfdByUser = new HashSet<LkVndrEmailLang>();
            LkVndrModfdByUser = new HashSet<LkVndr>();
            LkVndrScmCreatByUser = new HashSet<LkVndrScm>();
            LkVndrScmModfdByUser = new HashSet<LkVndrScm>();
            LkVpnPltfrmTypeCreatByUser = new HashSet<LkVpnPltfrmType>();
            LkVpnPltfrmTypeModfdByUser = new HashSet<LkVpnPltfrmType>();
            LkWhlslPtnrCreatByUser = new HashSet<LkWhlslPtnr>();
            LkWhlslPtnrModfdByUser = new HashSet<LkWhlslPtnr>();
            LkWrkflwStusCreatByUser = new HashSet<LkWrkflwStus>();
            LkWrkflwStusModfdByUser = new HashSet<LkWrkflwStus>();
            LkWrldHldyCreatByUser = new HashSet<LkWrldHldy>();
            LkWrldHldyModfdByUser = new HashSet<LkWrldHldy>();
            LkXnciMsSla = new HashSet<LkXnciMsSla>();
            LkXnciRgn = new HashSet<LkXnciRgn>();
            MapGrpTask = new HashSet<MapGrpTask>();
            MapPrfTask = new HashSet<MapPrfTask>();
            MapUsrPrfCreatByUser = new HashSet<MapUsrPrf>();
            MapUsrPrfModfdByUser = new HashSet<MapUsrPrf>();
            MapUsrPrfUser = new HashSet<MapUsrPrf>();
            MdsEventCreatByUser = new HashSet<MdsEvent>();
            MdsEventFormChng = new HashSet<MdsEventFormChng>();
            MdsEventModfdByUser = new HashSet<MdsEvent>();
            MdsEventNewCreatByUser = new HashSet<MdsEventNew>();
            MdsEventNewModfdByUser = new HashSet<MdsEventNew>();
            MdsEventNewReqorUser = new HashSet<MdsEventNew>();
            MdsEventReqorUser = new HashSet<MdsEvent>();
            MdsFastTrkTypeCreatByUser = new HashSet<MdsFastTrkType>();
            MdsFastTrkTypeModfdByUser = new HashSet<MdsFastTrkType>();
            MplsEventCreatByUser = new HashSet<MplsEvent>();
            MplsEventModfdByUser = new HashSet<MplsEvent>();
            MplsEventReqorUser = new HashSet<MplsEvent>();
            MplsEventSalsUser = new HashSet<MplsEvent>();
            NccoOrdr = new HashSet<NccoOrdr>();
            NgvnEventCreatByUser = new HashSet<NgvnEvent>();
            NgvnEventModfdByUser = new HashSet<NgvnEvent>();
            NgvnEventReqorUser = new HashSet<NgvnEvent>();
            NgvnEventSalsUser = new HashSet<NgvnEvent>();
            NidActyCreatByUser = new HashSet<NidActy>();
            NidActyModfdByUser = new HashSet<NidActy>();
            OrdrAdr = new HashSet<OrdrAdr>();
            OrdrCktChg = new HashSet<OrdrCktChg>();
            OrdrCntct = new HashSet<OrdrCntct>();
            OrdrCreatByUser = new HashSet<Ordr>();
            OrdrModfdByUser = new HashSet<Ordr>();
            OrdrMs = new HashSet<OrdrMs>();
            OrdrNteCreatByUser = new HashSet<OrdrNte>();
            OrdrNteModfdByUser = new HashSet<OrdrNte>();
            OrdrRecLock = new HashSet<OrdrRecLock>();
            OrdrStdiHistCreatByUser = new HashSet<OrdrStdiHist>();
            OrdrStdiHistModfdByUser = new HashSet<OrdrStdiHist>();
            OrdrVlan = new HashSet<OrdrVlan>();
            RedsgnCretdByCdNavigation = new HashSet<Redsgn>();
            RedsgnDevicesInfoCretdByCdNavigation = new HashSet<RedsgnDevicesInfo>();
            RedsgnDevicesInfoModfdByCdNavigation = new HashSet<RedsgnDevicesInfo>();
            RedsgnDoc = new HashSet<RedsgnDoc>();
            RedsgnEmailNtfctn = new HashSet<RedsgnEmailNtfctn>();
            RedsgnModfdByCdNavigation = new HashSet<Redsgn>();
            RedsgnNotes = new HashSet<RedsgnNotes>();
            ReltdEventCreatByUser = new HashSet<ReltdEvent>();
            ReltdEventModfdByUser = new HashSet<ReltdEvent>();
            SdeOpptntyAsnToUser = new HashSet<SdeOpptnty>();
            SdeOpptntyCreatByUser = new HashSet<SdeOpptnty>();
            SdeOpptntyDoc = new HashSet<SdeOpptntyDoc>();
            SdeOpptntyModfdByUser = new HashSet<SdeOpptnty>();
            SdeOpptntyNte = new HashSet<SdeOpptntyNte>();
            SdeOpptntyPrdct = new HashSet<SdeOpptntyPrdct>();
            SiptEventCreatByUser = new HashSet<SiptEvent>();
            SiptEventDoc = new HashSet<SiptEventDoc>();
            SiptEventModfdByUser = new HashSet<SiptEvent>();
            SiptEventNtwkEngr = new HashSet<SiptEvent>();
            SiptEventNtwkTechEngr = new HashSet<SiptEvent>();
            SiptEventPm = new HashSet<SiptEvent>();
            Sm = new HashSet<Sm>();
            SplkEventCreatByUser = new HashSet<SplkEvent>();
            SplkEventModfdByUser = new HashSet<SplkEvent>();
            SplkEventReqorUser = new HashSet<SplkEvent>();
            SplkEventSalsUser = new HashSet<SplkEvent>();
            SrcCurFile = new HashSet<SrcCurFile>();
            TrptOrdrCreatByUser = new HashSet<TrptOrdr>();
            TrptOrdrModfdByUser = new HashSet<TrptOrdr>();
            TrptOrdrXnciAsnMgr = new HashSet<TrptOrdr>();
            UcaaSEventCreatByUser = new HashSet<UcaaSEvent>();
            UcaaSEventModfdByUser = new HashSet<UcaaSEvent>();
            UcaaSEventReqorUser = new HashSet<UcaaSEvent>();
            UserCsgLvlCreatByUser = new HashSet<UserCsgLvl>();
            UserCsgLvlModfdByUser = new HashSet<UserCsgLvl>();
            UserCsgLvlUser = new HashSet<UserCsgLvl>();
            UserH5Wfm = new HashSet<UserH5Wfm>();
            UserWfmAsmtAsnByUser = new HashSet<UserWfmAsmt>();
            UserWfmAsmtAsnUser = new HashSet<UserWfmAsmt>();
            UserWfmCreatByUser = new HashSet<UserWfm>();
            UserWfmModfdByUser = new HashSet<UserWfm>();
            UserWfmUser = new HashSet<UserWfm>();
            VndrFoldrCreatByUser = new HashSet<VndrFoldr>();
            VndrFoldrModfdByUser = new HashSet<VndrFoldr>();
            VndrOrdrCreatByUser = new HashSet<VndrOrdr>();
            VndrOrdrEmailAtchmt = new HashSet<VndrOrdrEmailAtchmt>();
            VndrOrdrEmailCreatByUser = new HashSet<VndrOrdrEmail>();
            VndrOrdrEmailModfdByUser = new HashSet<VndrOrdrEmail>();
            VndrOrdrForm = new HashSet<VndrOrdrForm>();
            VndrOrdrModfdByUser = new HashSet<VndrOrdr>();
            VndrOrdrMs = new HashSet<VndrOrdrMs>();
            VndrTmplt = new HashSet<VndrTmplt>();
            WgProfSm = new HashSet<WgProfSm>();
        }

        public int UserId { get; set; }
        public string UserAdid { get; set; }
        public string UserAcf2Id { get; set; }
        public string FullNme { get; set; }
        public string EmailAdr { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CellPhnNbr { get; set; }
        public string PgrNbr { get; set; }
        public string PgrPinNbr { get; set; }
        public string DsplNme { get; set; }
        public decimal? UserOrdrCnt { get; set; }
        public string MgrAdid { get; set; }
        public decimal? CptQty { get; set; }
        public string CtyNme { get; set; }
        public string SttCd { get; set; }
        public DateTime? LstLoggdinDt { get; set; }
        public string PhnNbr { get; set; }
        public short? EventQty { get; set; }
        public string MenuPref { get; set; }
        public string OldUserAdid { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual LkQlfctn LkQlfctnAsnToUser { get; set; }
        public virtual ICollection<AdEvent> AdEventCreatByUser { get; set; }
        public virtual ICollection<AdEvent> AdEventModfdByUser { get; set; }
        public virtual ICollection<AdEvent> AdEventReqorUser { get; set; }
        public virtual ICollection<AdEvent> AdEventSalsUser { get; set; }
        public virtual ICollection<Appt> ApptCreatByUser { get; set; }
        public virtual ICollection<Appt> ApptModfdByUser { get; set; }
        public virtual ICollection<ApptRcurncData> ApptRcurncData { get; set; }
        public virtual ICollection<Asr> Asr { get; set; }
        public virtual ICollection<BillDsptch> BillDsptchCreatByUser { get; set; }
        public virtual ICollection<BillDsptch> BillDsptchModfdByUser { get; set; }
        public virtual ICollection<CcdHist> CcdHist { get; set; }
        public virtual ICollection<Ckt> Ckt { get; set; }
        public virtual ICollection<CktCost> CktCost { get; set; }
        public virtual ICollection<CktMs> CktMs { get; set; }
        public virtual ICollection<CntctDetl> CntctDetlCreatByUser { get; set; }
        public virtual ICollection<CntctDetl> CntctDetlModfdByUser { get; set; }
        public virtual ICollection<CpeClli> CpeClliCreatByUser { get; set; }
        public virtual ICollection<CpeClli> CpeClliModfdByUser { get; set; }
        public virtual ICollection<Cpt> CptCreatByUser { get; set; }
        public virtual ICollection<CptDoc> CptDocCreatByUser { get; set; }
        public virtual ICollection<CptDoc> CptDocModfdByUser { get; set; }
        public virtual ICollection<CptHist> CptHist { get; set; }
        public virtual ICollection<Cpt> CptModfdByUser { get; set; }
        public virtual ICollection<CptPrvsn> CptPrvsnCreatByUser { get; set; }
        public virtual ICollection<CptPrvsn> CptPrvsnModfdByUser { get; set; }
        public virtual ICollection<CptRecLock> CptRecLock { get; set; }
        public virtual ICollection<CptRedsgn> CptRedsgn { get; set; }
        public virtual ICollection<Cpt> CptSubmtrUser { get; set; }
        public virtual ICollection<CptUserAsmt> CptUserAsmtCptUser { get; set; }
        public virtual ICollection<CptUserAsmt> CptUserAsmtCreatByUser { get; set; }
        public virtual ICollection<CptUserAsmt> CptUserAsmtModfdByUser { get; set; }
        public virtual ICollection<DsplVw> DsplVwCreatByUser { get; set; }
        public virtual ICollection<DsplVw> DsplVwModfdByUser { get; set; }
        public virtual ICollection<DsplVw> DsplVwUser { get; set; }
        public virtual ICollection<EventAsnToUser> EventAsnToUser { get; set; }
        public virtual ICollection<EventHist> EventHistCreatByUser { get; set; }
        public virtual ICollection<EventHist> EventHistModfdByUser { get; set; }
        public virtual ICollection<EventRecLock> EventRecLock { get; set; }
        public virtual ICollection<EventTypeRsrcAvlblty> EventTypeRsrcAvlblty { get; set; }
        public virtual ICollection<FedlineEventUserData> FedlineEventUserDataActvUser { get; set; }
        public virtual ICollection<FedlineEventUserData> FedlineEventUserDataEngrUser { get; set; }
        public virtual ICollection<FedlineEventUserData> FedlineEventUserDataModfdByUser { get; set; }
        public virtual ICollection<FsaOrdrCsc> FsaOrdrCscCreatByUser { get; set; }
        public virtual ICollection<FsaOrdrCsc> FsaOrdrCscModfdByUser { get; set; }
        public virtual ICollection<FsaOrdrGomXnci> FsaOrdrGomXnciCreatByUser { get; set; }
        public virtual ICollection<FsaOrdrGomXnci> FsaOrdrGomXnciModfdByUser { get; set; }
        public virtual ICollection<H5Doc> H5DocCreatByUser { get; set; }
        public virtual ICollection<H5Doc> H5DocModfdByUser { get; set; }
        public virtual ICollection<H5Foldr> H5FoldrCreatByUser { get; set; }
        public virtual ICollection<H5Foldr> H5FoldrModfdByUser { get; set; }
        public virtual ICollection<IpActy> IpActyCreatByUser { get; set; }
        public virtual ICollection<IpActy> IpActyModfdByUser { get; set; }
        public virtual ICollection<IpMstr> IpMstrCreatByUser { get; set; }
        public virtual ICollection<IpMstr> IpMstrModfdByUser { get; set; }
        public virtual ICollection<IplOrdr> IplOrdrCreatByUser { get; set; }
        public virtual ICollection<IplOrdr> IplOrdrModfdByUser { get; set; }
        public virtual ICollection<LkActn> LkActnCreatByUser { get; set; }
        public virtual ICollection<LkActn> LkActnModfdByUser { get; set; }
        public virtual ICollection<LkActyLocale> LkActyLocaleCreatByUser { get; set; }
        public virtual ICollection<LkActyLocale> LkActyLocaleModfdByUser { get; set; }
        public virtual ICollection<LkApptType> LkApptTypeCreatByUser { get; set; }
        public virtual ICollection<LkApptType> LkApptTypeModfdByUser { get; set; }
        public virtual ICollection<LkAsrType> LkAsrType { get; set; }
        public virtual ICollection<LkCcdMissdReas> LkCcdMissdReas { get; set; }
        public virtual ICollection<LkCktChgType> LkCktChgType { get; set; }
        public virtual ICollection<LkCnfrcBrdg> LkCnfrcBrdgCreatByUser { get; set; }
        public virtual ICollection<LkCnfrcBrdg> LkCnfrcBrdgModfdByUser { get; set; }
        public virtual ICollection<LkCpePid> LkCpePidCreatByUser { get; set; }
        public virtual ICollection<LkCpePid> LkCpePidModfdByUser { get; set; }
        public virtual ICollection<LkCptCustType> LkCptCustTypeCreatByUser { get; set; }
        public virtual ICollection<LkCptCustType> LkCptCustTypeModfdByUser { get; set; }
        public virtual ICollection<LkCptHstMngdSrvc> LkCptHstMngdSrvcCreatByUser { get; set; }
        public virtual ICollection<LkCptHstMngdSrvc> LkCptHstMngdSrvcModfdByUser { get; set; }
        public virtual ICollection<LkCptMdsSuprtTier> LkCptMdsSuprtTierCreatByUser { get; set; }
        public virtual ICollection<LkCptMdsSuprtTier> LkCptMdsSuprtTierModfdByUser { get; set; }
        public virtual ICollection<LkCptMngdAuthPrd> LkCptMngdAuthPrdCreatByUser { get; set; }
        public virtual ICollection<LkCptMngdAuthPrd> LkCptMngdAuthPrdModfdByUser { get; set; }
        public virtual ICollection<LkCptMvsProdType> LkCptMvsProdTypeCreatByUser { get; set; }
        public virtual ICollection<LkCptMvsProdType> LkCptMvsProdTypeModfdByUser { get; set; }
        public virtual ICollection<LkCptPlnSrvcTier> LkCptPlnSrvcTierCreatByUser { get; set; }
        public virtual ICollection<LkCptPlnSrvcTier> LkCptPlnSrvcTierModfdByUser { get; set; }
        public virtual ICollection<LkCptPlnSrvcType> LkCptPlnSrvcTypeCreatByUser { get; set; }
        public virtual ICollection<LkCptPlnSrvcType> LkCptPlnSrvcTypeModfdByUser { get; set; }
        public virtual ICollection<LkCptPrimScndyTprt> LkCptPrimScndyTprtCreatByUser { get; set; }
        public virtual ICollection<LkCptPrimScndyTprt> LkCptPrimScndyTprtModfdByUser { get; set; }
        public virtual ICollection<LkCptPrimSite> LkCptPrimSiteCreatByUser { get; set; }
        public virtual ICollection<LkCptPrimSite> LkCptPrimSiteModfdByUser { get; set; }
        public virtual ICollection<LkCptPrvsnType> LkCptPrvsnTypeCreatByUser { get; set; }
        public virtual ICollection<LkCptPrvsnType> LkCptPrvsnTypeModfdByUser { get; set; }
        public virtual ICollection<LkCsgLvl> LkCsgLvlCreatByUser { get; set; }
        public virtual ICollection<LkCsgLvl> LkCsgLvlModfdByUser { get; set; }
        public virtual ICollection<LkCtry> LkCtryCreatByUser { get; set; }
        public virtual ICollection<LkCtryCty> LkCtryCtyCreatByUser { get; set; }
        public virtual ICollection<LkCtryCty> LkCtryCtyModfdByUser { get; set; }
        public virtual ICollection<LkCtry> LkCtryModfdByUser { get; set; }
        public virtual ICollection<LkCur> LkCurCreatByUser { get; set; }
        public virtual ICollection<LkCur> LkCurModfdByUser { get; set; }
        public virtual ICollection<LkCustCntrcLgth> LkCustCntrcLgthCreatByUser { get; set; }
        public virtual ICollection<LkCustCntrcLgth> LkCustCntrcLgthModfdByUser { get; set; }
        public virtual ICollection<LkDedctdCust> LkDedctdCustCreatByUser { get; set; }
        public virtual ICollection<LkDedctdCust> LkDedctdCustModfdByUser { get; set; }
        public virtual ICollection<LkDev> LkDevCreatByUser { get; set; }
        public virtual ICollection<LkDevManf> LkDevManf { get; set; }
        public virtual ICollection<LkDevModel> LkDevModelCreatByUser { get; set; }
        public virtual ICollection<LkDevModel> LkDevModelModfdByUser { get; set; }
        public virtual ICollection<LkDev> LkDevModfdByUser { get; set; }
        public virtual ICollection<LkDmstcClliCd> LkDmstcClliCdCreatByUser { get; set; }
        public virtual ICollection<LkDmstcClliCd> LkDmstcClliCdModfdByUser { get; set; }
        public virtual ICollection<LkEnhncSrvc> LkEnhncSrvcCreatByUser { get; set; }
        public virtual ICollection<LkEnhncSrvc> LkEnhncSrvcModfdByUser { get; set; }
        public virtual ICollection<LkEsclReas> LkEsclReasCreatByUser { get; set; }
        public virtual ICollection<LkEsclReas> LkEsclReasModfdByUser { get; set; }
        public virtual ICollection<LkEventStus> LkEventStusCreatByUser { get; set; }
        public virtual ICollection<LkEventStus> LkEventStusModfdByUser { get; set; }
        public virtual ICollection<LkEventType> LkEventTypeCreatByUser { get; set; }
        public virtual ICollection<LkEventType> LkEventTypeModfdByUser { get; set; }
        public virtual ICollection<LkFailActy> LkFailActyCreatByUser { get; set; }
        public virtual ICollection<LkFailActy> LkFailActyModfdByUser { get; set; }
        public virtual ICollection<LkFailReas> LkFailReasCreatByUser { get; set; }
        public virtual ICollection<LkFailReas> LkFailReasModfdByUser { get; set; }
        public virtual ICollection<LkFedlineEventFailCd> LkFedlineEventFailCdCreatByUser { get; set; }
        public virtual ICollection<LkFedlineEventFailCd> LkFedlineEventFailCdModfdByUser { get; set; }
        public virtual ICollection<LkFedlineOrdrType> LkFedlineOrdrType { get; set; }
        public virtual ICollection<LkFiltrOpr> LkFiltrOprCreatByUser { get; set; }
        public virtual ICollection<LkFiltrOpr> LkFiltrOprModfdByUser { get; set; }
        public virtual ICollection<LkFrgnCxr> LkFrgnCxrCreatByUser { get; set; }
        public virtual ICollection<LkFrgnCxr> LkFrgnCxrModfdByUser { get; set; }
        public virtual ICollection<LkGrp> LkGrpCreatByUser { get; set; }
        public virtual ICollection<LkGrp> LkGrpModfdByUser { get; set; }
        public virtual ICollection<LkIntlClliCd> LkIntlClliCdCreatByUser { get; set; }
        public virtual ICollection<LkIntlClliCd> LkIntlClliCdModfdByUser { get; set; }
        public virtual ICollection<LkIpMstr> LkIpMstrCreatByUser { get; set; }
        public virtual ICollection<LkIpMstr> LkIpMstrModfdByUser { get; set; }
        public virtual ICollection<LkIpVer> LkIpVerCreatByUser { get; set; }
        public virtual ICollection<LkIpVer> LkIpVerModfdByUser { get; set; }
        public virtual ICollection<LkMds3rdpartySrvcLvl> LkMds3rdpartySrvcLvl { get; set; }
        public virtual ICollection<LkMds3rdpartyVndr> LkMds3rdpartyVndrCreatByUser { get; set; }
        public virtual ICollection<LkMds3rdpartyVndr> LkMds3rdpartyVndrModfdByUser { get; set; }
        public virtual ICollection<LkMdsActyType> LkMdsActyTypeCreatByUser { get; set; }
        public virtual ICollection<LkMdsActyType> LkMdsActyTypeModfdByUser { get; set; }
        public virtual ICollection<LkMdsBrdgNeedReas> LkMdsBrdgNeedReasCreatByUser { get; set; }
        public virtual ICollection<LkMdsBrdgNeedReas> LkMdsBrdgNeedReasModfdByUser { get; set; }
        public virtual ICollection<LkMdsImplmtnNtvl> LkMdsImplmtnNtvlCreatByUser { get; set; }
        public virtual ICollection<LkMdsImplmtnNtvl> LkMdsImplmtnNtvlModfdByUser { get; set; }
        public virtual ICollection<LkMdsMacActy> LkMdsMacActyCreatByUser { get; set; }
        public virtual ICollection<LkMdsMacActy> LkMdsMacActyModfdByUser { get; set; }
        public virtual ICollection<LkMdsSrvcTier> LkMdsSrvcTierCreatByUser { get; set; }
        public virtual ICollection<LkMdsSrvcTier> LkMdsSrvcTierModfdByUser { get; set; }
        public virtual ICollection<LkMnsOffrgRoutr> LkMnsOffrgRoutrCreatByUser { get; set; }
        public virtual ICollection<LkMnsOffrgRoutr> LkMnsOffrgRoutrModfdByUser { get; set; }
        public virtual ICollection<LkMnsPrfmcRpt> LkMnsPrfmcRptCreatByUser { get; set; }
        public virtual ICollection<LkMnsPrfmcRpt> LkMnsPrfmcRptModfdByUser { get; set; }
        public virtual ICollection<LkMnsRoutgType> LkMnsRoutgTypeCreatByUser { get; set; }
        public virtual ICollection<LkMnsRoutgType> LkMnsRoutgTypeModfdByUser { get; set; }
        public virtual ICollection<LkMplsAccsBdwd> LkMplsAccsBdwdCreatByUser { get; set; }
        public virtual ICollection<LkMplsAccsBdwd> LkMplsAccsBdwdModfdByUser { get; set; }
        public virtual ICollection<LkMplsActyType> LkMplsActyTypeCreatByUser { get; set; }
        public virtual ICollection<LkMplsActyType> LkMplsActyTypeModfdByUser { get; set; }
        public virtual ICollection<LkMplsEventType> LkMplsEventTypeCreatByUser { get; set; }
        public virtual ICollection<LkMplsEventType> LkMplsEventTypeModfdByUser { get; set; }
        public virtual ICollection<LkMplsMgrtnType> LkMplsMgrtnTypeCreatByUser { get; set; }
        public virtual ICollection<LkMplsMgrtnType> LkMplsMgrtnTypeModfdByUser { get; set; }
        public virtual ICollection<LkMultiVrfReq> LkMultiVrfReqCreatByUser { get; set; }
        public virtual ICollection<LkMultiVrfReq> LkMultiVrfReqModfdByUser { get; set; }
        public virtual ICollection<LkNgvnProdType> LkNgvnProdTypeCreatByUser { get; set; }
        public virtual ICollection<LkNgvnProdType> LkNgvnProdTypeModfdByUser { get; set; }
        public virtual ICollection<LkNteType> LkNteTypeCreatByUser { get; set; }
        public virtual ICollection<LkNteType> LkNteTypeModfdByUser { get; set; }
        public virtual ICollection<LkOrdrCat> LkOrdrCatCreatByUser { get; set; }
        public virtual ICollection<LkOrdrCat> LkOrdrCatModfdByUser { get; set; }
        public virtual ICollection<LkPprt> LkPprt { get; set; }
        public virtual ICollection<LkQlfctn> LkQlfctnCreatByUser { get; set; }
        public virtual ICollection<LkQlfctn> LkQlfctnModfdByUser { get; set; }
        public virtual ICollection<LkRedsgnCat> LkRedsgnCat { get; set; }
        public virtual ICollection<LkRole> LkRole { get; set; }
        public virtual ICollection<LkScrdObjType> LkScrdObjTypeCreatByUser { get; set; }
        public virtual ICollection<LkScrdObjType> LkScrdObjTypeModfdByUser { get; set; }
        public virtual ICollection<LkSdePrdctType> LkSdePrdctTypeCreatByUser { get; set; }
        public virtual ICollection<LkSdePrdctType> LkSdePrdctTypeModfdByUser { get; set; }
        public virtual ICollection<LkSiptActyType> LkSiptActyTypeCreatByUser { get; set; }
        public virtual ICollection<LkSiptActyType> LkSiptActyTypeModfdByUser { get; set; }
        public virtual ICollection<LkSpclProj> LkSpclProjCreatByUser { get; set; }
        public virtual ICollection<LkSpclProj> LkSpclProjModfdByUser { get; set; }
        public virtual ICollection<LkSplkActyType> LkSplkActyTypeCreatByUser { get; set; }
        public virtual ICollection<LkSplkActyType> LkSplkActyTypeModfdByUser { get; set; }
        public virtual ICollection<LkSplkEventType> LkSplkEventTypeCreatByUser { get; set; }
        public virtual ICollection<LkSplkEventType> LkSplkEventTypeModfdByUser { get; set; }
        public virtual ICollection<LkSprintCpeNcr> LkSprintCpeNcrCreatByUser { get; set; }
        public virtual ICollection<LkSprintCpeNcr> LkSprintCpeNcrModfdByUser { get; set; }
        public virtual ICollection<LkSprintHldy> LkSprintHldyCreatByUser { get; set; }
        public virtual ICollection<LkSprintHldy> LkSprintHldyModfdByUser { get; set; }
        public virtual ICollection<LkSrvcAssrnSiteSupp> LkSrvcAssrnSiteSuppCreatByUser { get; set; }
        public virtual ICollection<LkSrvcAssrnSiteSupp> LkSrvcAssrnSiteSuppModfdByUser { get; set; }
        public virtual ICollection<LkStdiReas> LkStdiReasCreatByUser { get; set; }
        public virtual ICollection<LkStdiReas> LkStdiReasModfdByUser { get; set; }
        public virtual ICollection<LkStus> LkStusCreatByUser { get; set; }
        public virtual ICollection<LkStus> LkStusModfdByUser { get; set; }
        public virtual ICollection<LkStusType> LkStusType { get; set; }
        public virtual ICollection<LkSucssActy> LkSucssActyCreatByUser { get; set; }
        public virtual ICollection<LkSucssActy> LkSucssActyModfdByUser { get; set; }
        public virtual ICollection<LkTask> LkTask { get; set; }
        public virtual ICollection<LkTelco> LkTelcoCreatByUser { get; set; }
        public virtual ICollection<LkTelco> LkTelcoModfdByUser { get; set; }
        public virtual ICollection<LkUcaaSActyType> LkUcaaSActyTypeCreatByUser { get; set; }
        public virtual ICollection<LkUcaaSActyType> LkUcaaSActyTypeModfdByUser { get; set; }
        public virtual ICollection<LkUcaaSBillActy> LkUcaaSBillActyCreatByUser { get; set; }
        public virtual ICollection<LkUcaaSBillActy> LkUcaaSBillActyModfdByUser { get; set; }
        public virtual ICollection<LkUcaaSPlanType> LkUcaaSPlanTypeCreatByUser { get; set; }
        public virtual ICollection<LkUcaaSPlanType> LkUcaaSPlanTypeModfdByUser { get; set; }
        public virtual ICollection<LkUcaaSProdType> LkUcaaSProdTypeCreatByUser { get; set; }
        public virtual ICollection<LkUcaaSProdType> LkUcaaSProdTypeModfdByUser { get; set; }
        public virtual ICollection<LkUsStt> LkUsSttCreatByUser { get; set; }
        public virtual ICollection<LkUsStt> LkUsSttModfdByUser { get; set; }
        public virtual ICollection<LkVasType> LkVasTypeCreatByUser { get; set; }
        public virtual ICollection<LkVasType> LkVasTypeModfdByUser { get; set; }
        public virtual ICollection<LkVndr> LkVndrCreatByUser { get; set; }
        public virtual ICollection<LkVndrEmailLang> LkVndrEmailLangCreatByUser { get; set; }
        public virtual ICollection<LkVndrEmailLang> LkVndrEmailLangModfdByUser { get; set; }
        public virtual ICollection<LkVndr> LkVndrModfdByUser { get; set; }
        public virtual ICollection<LkVndrScm> LkVndrScmCreatByUser { get; set; }
        public virtual ICollection<LkVndrScm> LkVndrScmModfdByUser { get; set; }
        public virtual ICollection<LkVpnPltfrmType> LkVpnPltfrmTypeCreatByUser { get; set; }
        public virtual ICollection<LkVpnPltfrmType> LkVpnPltfrmTypeModfdByUser { get; set; }
        public virtual ICollection<LkWhlslPtnr> LkWhlslPtnrCreatByUser { get; set; }
        public virtual ICollection<LkWhlslPtnr> LkWhlslPtnrModfdByUser { get; set; }
        public virtual ICollection<LkWrkflwStus> LkWrkflwStusCreatByUser { get; set; }
        public virtual ICollection<LkWrkflwStus> LkWrkflwStusModfdByUser { get; set; }
        public virtual ICollection<LkWrldHldy> LkWrldHldyCreatByUser { get; set; }
        public virtual ICollection<LkWrldHldy> LkWrldHldyModfdByUser { get; set; }
        public virtual ICollection<LkXnciMsSla> LkXnciMsSla { get; set; }
        public virtual ICollection<LkXnciRgn> LkXnciRgn { get; set; }
        public virtual ICollection<MapGrpTask> MapGrpTask { get; set; }
        public virtual ICollection<MapPrfTask> MapPrfTask { get; set; }
        public virtual ICollection<MapUsrPrf> MapUsrPrfCreatByUser { get; set; }
        public virtual ICollection<MapUsrPrf> MapUsrPrfModfdByUser { get; set; }
        public virtual ICollection<MapUsrPrf> MapUsrPrfUser { get; set; }
        public virtual ICollection<MdsEvent> MdsEventCreatByUser { get; set; }
        public virtual ICollection<MdsEventFormChng> MdsEventFormChng { get; set; }
        public virtual ICollection<MdsEvent> MdsEventModfdByUser { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNewCreatByUser { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNewModfdByUser { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNewReqorUser { get; set; }
        public virtual ICollection<MdsEvent> MdsEventReqorUser { get; set; }
        public virtual ICollection<MdsFastTrkType> MdsFastTrkTypeCreatByUser { get; set; }
        public virtual ICollection<MdsFastTrkType> MdsFastTrkTypeModfdByUser { get; set; }
        public virtual ICollection<MplsEvent> MplsEventCreatByUser { get; set; }
        public virtual ICollection<MplsEvent> MplsEventModfdByUser { get; set; }
        public virtual ICollection<MplsEvent> MplsEventReqorUser { get; set; }
        public virtual ICollection<MplsEvent> MplsEventSalsUser { get; set; }
        public virtual ICollection<NccoOrdr> NccoOrdr { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEventCreatByUser { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEventModfdByUser { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEventReqorUser { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEventSalsUser { get; set; }
        public virtual ICollection<NidActy> NidActyCreatByUser { get; set; }
        public virtual ICollection<NidActy> NidActyModfdByUser { get; set; }
        public virtual ICollection<OrdrAdr> OrdrAdr { get; set; }
        public virtual ICollection<OrdrCktChg> OrdrCktChg { get; set; }
        public virtual ICollection<OrdrCntct> OrdrCntct { get; set; }
        public virtual ICollection<Ordr> OrdrCreatByUser { get; set; }
        public virtual ICollection<Ordr> OrdrModfdByUser { get; set; }
        public virtual ICollection<OrdrMs> OrdrMs { get; set; }
        public virtual ICollection<OrdrNte> OrdrNteCreatByUser { get; set; }
        public virtual ICollection<OrdrNte> OrdrNteModfdByUser { get; set; }
        public virtual ICollection<OrdrRecLock> OrdrRecLock { get; set; }
        public virtual ICollection<OrdrStdiHist> OrdrStdiHistCreatByUser { get; set; }
        public virtual ICollection<OrdrStdiHist> OrdrStdiHistModfdByUser { get; set; }
        public virtual ICollection<OrdrVlan> OrdrVlan { get; set; }
        public virtual ICollection<Redsgn> RedsgnCretdByCdNavigation { get; set; }
        public virtual ICollection<RedsgnDevicesInfo> RedsgnDevicesInfoCretdByCdNavigation { get; set; }
        public virtual ICollection<RedsgnDevicesInfo> RedsgnDevicesInfoModfdByCdNavigation { get; set; }
        public virtual ICollection<RedsgnDoc> RedsgnDoc { get; set; }
        public virtual ICollection<RedsgnEmailNtfctn> RedsgnEmailNtfctn { get; set; }
        public virtual ICollection<Redsgn> RedsgnModfdByCdNavigation { get; set; }
        public virtual ICollection<RedsgnNotes> RedsgnNotes { get; set; }
        public virtual ICollection<ReltdEvent> ReltdEventCreatByUser { get; set; }
        public virtual ICollection<ReltdEvent> ReltdEventModfdByUser { get; set; }
        public virtual ICollection<SdeOpptnty> SdeOpptntyAsnToUser { get; set; }
        public virtual ICollection<SdeOpptnty> SdeOpptntyCreatByUser { get; set; }
        public virtual ICollection<SdeOpptntyDoc> SdeOpptntyDoc { get; set; }
        public virtual ICollection<SdeOpptnty> SdeOpptntyModfdByUser { get; set; }
        public virtual ICollection<SdeOpptntyNte> SdeOpptntyNte { get; set; }
        public virtual ICollection<SdeOpptntyPrdct> SdeOpptntyPrdct { get; set; }
        public virtual ICollection<SiptEvent> SiptEventCreatByUser { get; set; }
        public virtual ICollection<SiptEventDoc> SiptEventDoc { get; set; }
        public virtual ICollection<SiptEvent> SiptEventModfdByUser { get; set; }
        public virtual ICollection<SiptEvent> SiptEventNtwkEngr { get; set; }
        public virtual ICollection<SiptEvent> SiptEventNtwkTechEngr { get; set; }
        public virtual ICollection<SiptEvent> SiptEventPm { get; set; }
        public virtual ICollection<Sm> Sm { get; set; }
        public virtual ICollection<SplkEvent> SplkEventCreatByUser { get; set; }
        public virtual ICollection<SplkEvent> SplkEventModfdByUser { get; set; }
        public virtual ICollection<SplkEvent> SplkEventReqorUser { get; set; }
        public virtual ICollection<SplkEvent> SplkEventSalsUser { get; set; }
        public virtual ICollection<SrcCurFile> SrcCurFile { get; set; }
        public virtual ICollection<TrptOrdr> TrptOrdrCreatByUser { get; set; }
        public virtual ICollection<TrptOrdr> TrptOrdrModfdByUser { get; set; }
        public virtual ICollection<TrptOrdr> TrptOrdrXnciAsnMgr { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEventCreatByUser { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEventModfdByUser { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEventReqorUser { get; set; }
        public virtual ICollection<UserCsgLvl> UserCsgLvlCreatByUser { get; set; }
        public virtual ICollection<UserCsgLvl> UserCsgLvlModfdByUser { get; set; }
        public virtual ICollection<UserCsgLvl> UserCsgLvlUser { get; set; }
        public virtual ICollection<UserH5Wfm> UserH5Wfm { get; set; }
        public virtual ICollection<UserWfmAsmt> UserWfmAsmtAsnByUser { get; set; }
        public virtual ICollection<UserWfmAsmt> UserWfmAsmtAsnUser { get; set; }
        public virtual ICollection<UserWfm> UserWfmCreatByUser { get; set; }
        public virtual ICollection<UserWfm> UserWfmModfdByUser { get; set; }
        public virtual ICollection<UserWfm> UserWfmUser { get; set; }
        public virtual ICollection<VndrFoldr> VndrFoldrCreatByUser { get; set; }
        public virtual ICollection<VndrFoldr> VndrFoldrModfdByUser { get; set; }
        public virtual ICollection<VndrOrdr> VndrOrdrCreatByUser { get; set; }
        public virtual ICollection<VndrOrdrEmailAtchmt> VndrOrdrEmailAtchmt { get; set; }
        public virtual ICollection<VndrOrdrEmail> VndrOrdrEmailCreatByUser { get; set; }
        public virtual ICollection<VndrOrdrEmail> VndrOrdrEmailModfdByUser { get; set; }
        public virtual ICollection<VndrOrdrForm> VndrOrdrForm { get; set; }
        public virtual ICollection<VndrOrdr> VndrOrdrModfdByUser { get; set; }
        public virtual ICollection<VndrOrdrMs> VndrOrdrMs { get; set; }
        public virtual ICollection<VndrTmplt> VndrTmplt { get; set; }
        public virtual ICollection<WgProfSm> WgProfSm { get; set; }
    }
}
