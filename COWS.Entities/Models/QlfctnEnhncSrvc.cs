﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnEnhncSrvc
    {
        public int QlfctnId { get; set; }
        public short EnhncSrvcId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkEnhncSrvc EnhncSrvc { get; set; }
        public virtual LkQlfctn Qlfctn { get; set; }
    }
}
