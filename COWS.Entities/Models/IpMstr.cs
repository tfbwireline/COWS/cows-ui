﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class IpMstr
    {
        public int IpMstrId { get; set; }
        public string IpAdr { get; set; }
        public short RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkStus RecStus { get; set; }
    }
}
