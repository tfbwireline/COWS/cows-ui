﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkProd
    {
        public LkProd()
        {
            LkProdType = new HashSet<LkProdType>();
            PltfrmMapng = new HashSet<PltfrmMapng>();
        }

        public string ProdNme { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<LkProdType> LkProdType { get; set; }
        public virtual ICollection<PltfrmMapng> PltfrmMapng { get; set; }
    }
}
