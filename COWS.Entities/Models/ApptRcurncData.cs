﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class ApptRcurncData
    {
        public long ApptRcurncDataId { get; set; }
        public int ApptId { get; set; }
        public DateTime StrtTmst { get; set; }
        public DateTime EndTmst { get; set; }
        public int ApptTypeId { get; set; }
        public int AsnUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Appt Appt { get; set; }
        public virtual LkApptType ApptType { get; set; }
        public virtual LkUser AsnUser { get; set; }
    }
}
