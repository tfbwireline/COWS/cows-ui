﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class PsReqLineItmQueue
    {
        public int ReqLineItmId { get; set; }
        public int OrdrId { get; set; }
        public int? CmpntId { get; set; }
        public string ReqstnNbr { get; set; }
        public string MatCd { get; set; }
        public string DlvyClli { get; set; }
        public string ItmDes { get; set; }
        public string ManfPartNbr { get; set; }
        public string ManfId { get; set; }
        public int? OrdrQty { get; set; }
        public string UntMsr { get; set; }
        public decimal? UntPrice { get; set; }
        public DateTime? RasDt { get; set; }
        public string VndrNme { get; set; }
        public string BusUntGl { get; set; }
        public string Acct { get; set; }
        public string CostCntr { get; set; }
        public string Prodct { get; set; }
        public string Mrkt { get; set; }
        public string Afflt { get; set; }
        public string Regn { get; set; }
        public string ProjId { get; set; }
        public string BusUntPc { get; set; }
        public string Actvy { get; set; }
        public string SourceTyp { get; set; }
        public string RsrcCat { get; set; }
        public string RsrcSub { get; set; }
        public string CntrctId { get; set; }
        public int? CntrctLnNbr { get; set; }
        public string AxlryId { get; set; }
        public string InstCd { get; set; }
        public byte? RecStusId { get; set; }
        public DateTime? CreatDt { get; set; }
        public DateTime? SentDt { get; set; }
        public int? FsaCpeLineItemId { get; set; }
        public string EqptTypeId { get; set; }
        public string Comments { get; set; }
        public string ManfDiscntCd { get; set; }
        public int? ReqLineNbr { get; set; }
    }
}
