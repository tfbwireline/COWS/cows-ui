﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMds3rdpartyVndr
    {
        public LkMds3rdpartyVndr()
        {
            MdsEventSiteSrvc = new HashSet<MdsEventSiteSrvc>();
            MdsEventSrvc = new HashSet<MdsEventSrvc>();
        }

        public short ThrdPartyVndrId { get; set; }
        public string ThrdPartyVndrDes { get; set; }
        public short SrvcTypeId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkMdsSrvcType SrvcType { get; set; }
        public virtual ICollection<MdsEventSiteSrvc> MdsEventSiteSrvc { get; set; }
        public virtual ICollection<MdsEventSrvc> MdsEventSrvc { get; set; }
    }
}
