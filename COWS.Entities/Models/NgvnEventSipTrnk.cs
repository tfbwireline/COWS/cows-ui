﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class NgvnEventSipTrnk
    {
        public int EventId { get; set; }
        public string SipTrnkNme { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual NgvnEvent Event { get; set; }
    }
}
