﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkSdePrdctType
    {
        public LkSdePrdctType()
        {
            SdeOpptntyPrdct = new HashSet<SdeOpptntyPrdct>();
        }

        public short SdePrdctTypeId { get; set; }
        public string SdePrdctTypeNme { get; set; }
        public string SdePrdctTypeDesc { get; set; }
        public short OrdrBySeqNbr { get; set; }
        public bool OutsrcdCd { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<SdeOpptntyPrdct> SdeOpptntyPrdct { get; set; }
    }
}
