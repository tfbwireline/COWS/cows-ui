﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SipTrnkGrp
    {
        public SipTrnkGrp()
        {
            MplsAccs = new HashSet<MplsAccs>();
        }

        public int SipTrnkGrpId { get; set; }
        public int OrdrId { get; set; }
        public int CmpntId { get; set; }
        public bool? IsInstlCd { get; set; }
        public int SipTrnkQty { get; set; }
        public string NwUserAdr { get; set; }

        public virtual FsaOrdr Ordr { get; set; }
        public virtual ICollection<MplsAccs> MplsAccs { get; set; }
    }
}
