﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class VndrOrdrEmail
    {
        public VndrOrdrEmail()
        {
            VndrOrdrEmailAtchmt = new HashSet<VndrOrdrEmailAtchmt>();
            VndrOrdrMs = new HashSet<VndrOrdrMs>();
        }

        public int VndrOrdrEmailId { get; set; }
        public int VndrOrdrId { get; set; }
        public byte VerNbr { get; set; }
        public string FromEmailAdr { get; set; }
        public string ToEmailAdr { get; set; }
        public string CcEmailAdr { get; set; }
        public string EmailBody { get; set; }
        public short EmailStusId { get; set; }
        public DateTime? SentDt { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string EmailSubjTxt { get; set; }
        public byte? VndrEmailTypeId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkStus EmailStus { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkVndrEmailType VndrEmailType { get; set; }
        public virtual VndrOrdr VndrOrdr { get; set; }
        public virtual ICollection<VndrOrdrEmailAtchmt> VndrOrdrEmailAtchmt { get; set; }
        public virtual ICollection<VndrOrdrMs> VndrOrdrMs { get; set; }
    }
}
