﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OrdrNte
    {
        public OrdrNte()
        {
            CcdHist = new HashSet<CcdHist>();
            OrdrJprdy = new HashSet<OrdrJprdy>();
        }

        public int NteId { get; set; }
        public byte NteTypeId { get; set; }
        public int OrdrId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public string NteTxt { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkNteType NteType { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<CcdHist> CcdHist { get; set; }
        public virtual ICollection<OrdrJprdy> OrdrJprdy { get; set; }
    }
}
