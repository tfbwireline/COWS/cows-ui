﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MplsAccs
    {
        public int MplsAccsId { get; set; }
        public int SipTrnkGrpId { get; set; }
        public string CktId { get; set; }
        public string NwUserAdr { get; set; }

        public virtual SipTrnkGrp SipTrnkGrp { get; set; }
    }
}
