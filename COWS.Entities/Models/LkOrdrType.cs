﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkOrdrType
    {
        public LkOrdrType()
        {
            IplOrdrOrdrType = new HashSet<IplOrdr>();
            IplOrdrPrevOrdrType = new HashSet<IplOrdr>();
            LkPprt = new HashSet<LkPprt>();
            LkXnciMsSla = new HashSet<LkXnciMsSla>();
            NccoOrdr = new HashSet<NccoOrdr>();
            UserWfm = new HashSet<UserWfm>();
        }

        public byte OrdrTypeId { get; set; }
        public string OrdrTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public string FsaOrdrTypeCd { get; set; }
        public decimal? XnciWfmOrdrWeightageNbr { get; set; }

        public virtual LkFsaOrdrType FsaOrdrTypeCdNavigation { get; set; }
        public virtual ICollection<IplOrdr> IplOrdrOrdrType { get; set; }
        public virtual ICollection<IplOrdr> IplOrdrPrevOrdrType { get; set; }
        public virtual ICollection<LkPprt> LkPprt { get; set; }
        public virtual ICollection<LkXnciMsSla> LkXnciMsSla { get; set; }
        public virtual ICollection<NccoOrdr> NccoOrdr { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
    }
}
