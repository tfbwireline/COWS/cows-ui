﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class IpActy
    {
        public int IpActyId { get; set; }
        public int IpMstrId { get; set; }
        public int? NidActyId { get; set; }
        public int? NatSipActyId { get; set; }
        public short RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual NidActy NidActy { get; set; }
        public virtual LkStus RecStus { get; set; }
    }
}
