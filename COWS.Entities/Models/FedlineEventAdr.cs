﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineEventAdr
    {
        public int FedlineEventAdrId { get; set; }
        public int FedlineEventId { get; set; }
        public byte AdrTypeId { get; set; }
        public string CtryCd { get; set; }
        public DateTime CreatDt { get; set; }
        public byte[] Street1Adr { get; set; }
        public byte[] Street2Adr { get; set; }
        public byte[] CityNme { get; set; }
        public byte[] SttCd { get; set; }
        public byte[] ZipCd { get; set; }
        public byte[] PrvnNme { get; set; }

        public virtual LkAdrType AdrType { get; set; }
        public virtual FedlineEventTadpoleData FedlineEvent { get; set; }
    }
}
