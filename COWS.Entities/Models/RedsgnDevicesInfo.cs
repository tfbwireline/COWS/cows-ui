﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnDevicesInfo
    {
        public RedsgnDevicesInfo()
        {
            RedsgnDevEventHist = new HashSet<RedsgnDevEventHist>();
        }

        public int RedsgnDevId { get; set; }
        public int RedsgnId { get; set; }
        public short SeqNbr { get; set; }
        public string DevNme { get; set; }
        public bool? FastTrkCd { get; set; }
        public bool? DspchRdyCd { get; set; }
        public bool RecStusId { get; set; }
        public int CretdByCd { get; set; }
        public DateTime CretdDt { get; set; }
        public int? ModfdByCd { get; set; }
        public DateTime? ModfdDt { get; set; }
        public bool NteChrgCd { get; set; }
        public DateTime? DevBillDt { get; set; }
        public bool? DevCmpltnCd { get; set; }
        public string H6CustId { get; set; }
        public DateTime? EventCmpltnDt { get; set; }
        public string ScCd { get; set; }

        public virtual LkUser CretdByCdNavigation { get; set; }
        public virtual LkUser ModfdByCdNavigation { get; set; }
        public virtual Redsgn Redsgn { get; set; }
        public virtual ICollection<RedsgnDevEventHist> RedsgnDevEventHist { get; set; }
    }
}
