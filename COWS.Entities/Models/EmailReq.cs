﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EmailReq
    {
        public EmailReq()
        {
            Asr = new HashSet<Asr>();
        }

        public int EmailReqId { get; set; }
        public int? OrdrId { get; set; }
        public int EmailReqTypeId { get; set; }
        public short StusId { get; set; }
        public DateTime CreatDt { get; set; }
        public int? EventId { get; set; }
        public string EmailListTxt { get; set; }
        public string EmailCcTxt { get; set; }
        public string EmailSubjTxt { get; set; }
        public string EmailBodyTxt { get; set; }
        public DateTime? SentDt { get; set; }
        public int? RedsgnId { get; set; }
        public int? CptId { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkEmailReqType EmailReqType { get; set; }
        public virtual Event Event { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual Redsgn Redsgn { get; set; }
        public virtual LkStus Stus { get; set; }
        public virtual ICollection<Asr> Asr { get; set; }
    }
}
