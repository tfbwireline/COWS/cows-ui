﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MapUsrPrf
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public short UsrPrfId { get; set; }
        public byte? PoolCd { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUser User { get; set; }
        public virtual LkUsrPrf UsrPrf { get; set; }
    }
}
