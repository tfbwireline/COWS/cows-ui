﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OrdrCktChg
    {
        public int OrdrId { get; set; }
        public short CktChgTypeId { get; set; }
        public short VerId { get; set; }
        public string ChgCurId { get; set; }
        public decimal ChgNrcAmt { get; set; }
        public decimal? ChgNrcInUsdAmt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public decimal? TaxRtPctQty { get; set; }
        public string NteTxt { get; set; }
        public short? SalsStusId { get; set; }
        public bool? ReqrBillOrdrCd { get; set; }
        public DateTime? XpirnDt { get; set; }
        public bool TrmtgCd { get; set; }

        public virtual LkCur ChgCur { get; set; }
        public virtual LkCktChgType CktChgType { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkStus SalsStus { get; set; }
    }
}
