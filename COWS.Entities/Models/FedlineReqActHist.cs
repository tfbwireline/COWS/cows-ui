﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineReqActHist
    {
        public int ReqActHistId { get; set; }
        public int EventId { get; set; }
        public DateTime? OldDt { get; set; }
        public DateTime? NewDt { get; set; }
        public short? FailCdId { get; set; }
        public DateTime? CreatDt { get; set; }
        public int? EventHistId { get; set; }

        public virtual Event Event { get; set; }
        public virtual EventHist EventHist { get; set; }
    }
}
