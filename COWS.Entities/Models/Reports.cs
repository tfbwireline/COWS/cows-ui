﻿using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.Models
{
    public class CannedReport
    {
        public Report Report { get; set; }
        public CannedFile File { get; set; }

    }
}
