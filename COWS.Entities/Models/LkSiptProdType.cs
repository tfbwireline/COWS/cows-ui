﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkSiptProdType
    {
        public LkSiptProdType()
        {
            SiptEvent = new HashSet<SiptEvent>();
        }

        public short SiptProdTypeId { get; set; }
        public string SiptProdTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<SiptEvent> SiptEvent { get; set; }
    }
}
