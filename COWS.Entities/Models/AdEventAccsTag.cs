﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class AdEventAccsTag
    {
        public int EventId { get; set; }
        public string CktId { get; set; }
        public DateTime CreatDt { get; set; }
        public string OldCktId { get; set; }

        public virtual AdEvent Event { get; set; }
    }
}
