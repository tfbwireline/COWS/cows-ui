﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class Sm
    {
        public short SmId { get; set; }
        public int PreReqstWgPtrnId { get; set; }
        public int DesrdWgPtrnId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
    }
}
