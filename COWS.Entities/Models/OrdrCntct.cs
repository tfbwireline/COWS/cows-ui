﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OrdrCntct
    {
        public int OrdrCntctId { get; set; }
        public int OrdrId { get; set; }
        public byte CntctTypeId { get; set; }
        public string TmeZoneId { get; set; }
        public bool IntprtrCd { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public byte? RoleId { get; set; }
        public string CisLvlType { get; set; }
        public string FsaMdulId { get; set; }
        public string CntctHrTxt { get; set; }
        public string Npa { get; set; }
        public string Nxx { get; set; }
        public string StnNbr { get; set; }
        public string CtyCd { get; set; }
        public string IsdCd { get; set; }
        public string PhnExtNbr { get; set; }
        public string SuppdLangNme { get; set; }
        public string FsaTmeZoneCd { get; set; }
        public string FrstNme { get; set; }
        public string LstNme { get; set; }
        public string EmailAdr { get; set; }
        public string CntctNme { get; set; }
        public string PhnNbr { get; set; }
        public string FaxNbr { get; set; }

        public virtual LkCntctType CntctType { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkRole Role { get; set; }
    }
}
