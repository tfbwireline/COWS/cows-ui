﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkSysCfg
    {
        public int CfgId { get; set; }
        public string PrmtrNme { get; set; }
        public string PrmtrValuTxt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
    }
}
