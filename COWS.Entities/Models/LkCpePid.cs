﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCpePid
    {
        public int CpePidId { get; set; }
        public bool? DmstcCd { get; set; }
        public string PidProdId { get; set; }
        public string PidCntrctType { get; set; }
        public string EqptTypeId { get; set; }
        public string DropShp { get; set; }
        public string ProjId { get; set; }
        public string Acct { get; set; }
        public string Prodct { get; set; }
        public string Actvy { get; set; }
        public string SourceTyp { get; set; }
        public string RsrcCat { get; set; }
        public string BusUntGl { get; set; }
        public string CostCntr { get; set; }
        public string Mrkt { get; set; }
        public string Regn { get; set; }
        public string BusUntPc { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
