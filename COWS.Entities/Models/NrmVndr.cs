﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class NrmVndr
    {
        public int NmsVndrId { get; set; }
        public long? SrvcInstcObjId { get; set; }
        public string Ftn { get; set; }
        public string VndrNme { get; set; }
        public string VndrTypeId { get; set; }
        public string LecId { get; set; }
        public string VndrCktId { get; set; }
        public string SegTypeId { get; set; }
        public string H6H5Id { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime ModfdDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
    }
}
