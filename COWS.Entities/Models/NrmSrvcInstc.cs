﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class NrmSrvcInstc
    {
        public int NrmSrvcInstcId { get; set; }
        public long? SrvcInstcObjId { get; set; }
        public string SrvcInstcNme { get; set; }
        public DateTime? LoopAcptDt { get; set; }
        public string OrdrStusDes { get; set; }
        public DateTime? DscnctDt { get; set; }
        public DateTime? ActvDt { get; set; }
        public string OrdrCmntTxt { get; set; }
        public bool MdsCd { get; set; }
        public string H5H6Id { get; set; }
        public string Ftn { get; set; }
        public string NuaAdr { get; set; }
        public DateTime ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? BillClearDt { get; set; }
        public short RecStusId { get; set; }

        public virtual LkStus RecStus { get; set; }
    }
}
