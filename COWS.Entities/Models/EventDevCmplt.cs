﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventDevCmplt
    {
        public int EventDevCmpltId { get; set; }
        public int EventId { get; set; }
        public string OdieDevNme { get; set; }
        public string H6 { get; set; }
        public bool CmpltdCd { get; set; }
        public DateTime CreatDt { get; set; }
        public int? RedsgnDevId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? OdieSentDt { get; set; }

        public virtual Event Event { get; set; }
    }
}
