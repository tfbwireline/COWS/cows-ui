﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMdsSrvcTier
    {
        public LkMdsSrvcTier()
        {
            EventDevSrvcMgmt = new HashSet<EventDevSrvcMgmt>();
            FsaOrdr = new HashSet<FsaOrdr>();
        }

        public byte MdsSrvcTierId { get; set; }
        public string MdsSrvcTierDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short? EventTypeId { get; set; }
        public string MdsSrvcTierCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkEventType EventType { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<EventDevSrvcMgmt> EventDevSrvcMgmt { get; set; }
        public virtual ICollection<FsaOrdr> FsaOrdr { get; set; }
    }
}
