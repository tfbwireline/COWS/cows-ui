﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkQlfctn
    {
        public LkQlfctn()
        {
            QlfctnDedctdCust = new HashSet<QlfctnDedctdCust>();
            QlfctnDev = new HashSet<QlfctnDev>();
            QlfctnEnhncSrvc = new HashSet<QlfctnEnhncSrvc>();
            QlfctnEventType = new HashSet<QlfctnEventType>();
            QlfctnIpVer = new HashSet<QlfctnIpVer>();
            QlfctnMplsActyType = new HashSet<QlfctnMplsActyType>();
            QlfctnNtwkActyType = new HashSet<QlfctnNtwkActyType>();
            QlfctnSpclProj = new HashSet<QlfctnSpclProj>();
            QlfctnVndrModel = new HashSet<QlfctnVndrModel>();
        }

        public int QlfctnId { get; set; }
        public int AsnToUserId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser AsnToUser { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<QlfctnDedctdCust> QlfctnDedctdCust { get; set; }
        public virtual ICollection<QlfctnDev> QlfctnDev { get; set; }
        public virtual ICollection<QlfctnEnhncSrvc> QlfctnEnhncSrvc { get; set; }
        public virtual ICollection<QlfctnEventType> QlfctnEventType { get; set; }
        public virtual ICollection<QlfctnIpVer> QlfctnIpVer { get; set; }
        public virtual ICollection<QlfctnMplsActyType> QlfctnMplsActyType { get; set; }
        public virtual ICollection<QlfctnNtwkActyType> QlfctnNtwkActyType { get; set; }
        public virtual ICollection<QlfctnSpclProj> QlfctnSpclProj { get; set; }
        public virtual ICollection<QlfctnVndrModel> QlfctnVndrModel { get; set; }
    }
}
