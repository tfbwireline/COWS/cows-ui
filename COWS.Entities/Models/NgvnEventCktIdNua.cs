﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class NgvnEventCktIdNua
    {
        public int EventId { get; set; }
        public string CktIdNua { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual NgvnEvent Event { get; set; }
    }
}
