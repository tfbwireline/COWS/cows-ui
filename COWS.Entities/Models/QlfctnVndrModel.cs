﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnVndrModel
    {
        public int QlfctnId { get; set; }
        public short DevModelId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkDevModel DevModel { get; set; }
        public virtual LkQlfctn Qlfctn { get; set; }
    }
}
