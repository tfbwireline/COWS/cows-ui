﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OrdrStdiHist
    {
        public int OrdrStdiId { get; set; }
        public int OrdrId { get; set; }
        public byte StdiReasId { get; set; }
        public string CmntTxt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime StdiDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkStdiReas StdiReas { get; set; }
    }
}
