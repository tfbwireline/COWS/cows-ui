﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEvent
    {
        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public string H1 { get; set; }
        public string CharsId { get; set; }
        public string CustAcctTeamPdlNme { get; set; }
        public string CustSowLocTxt { get; set; }
        public byte? MdsActyTypeId { get; set; }
        public string ShrtDes { get; set; }
        public string H6 { get; set; }
        public DateTime? Ccd { get; set; }
        public byte? OptOutReasId { get; set; }
        public string SiteIdTxt { get; set; }
        public string InstlSitePocIntlPhnCd { get; set; }
        public string InstlSitePocIntlCellPhnCd { get; set; }
        public string SrvcAssrnPocIntlPhnCd { get; set; }
        public string SrvcAssrnPocIntlCellPhnCd { get; set; }
        public string UsIntlCd { get; set; }
        public byte? SprintCpeNcrId { get; set; }
        public string CpeDspchEmailAdr { get; set; }
        public string CpeDspchCmntTxt { get; set; }
        public bool EsclCd { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public byte CnfrcBrdgId { get; set; }
        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public string OnlineMeetingAdr { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public int ReqorUserId { get; set; }
        public string ReqorUserCellPhnNbr { get; set; }
        public string CmntTxt { get; set; }
        public string MdsFastTrkTypeId { get; set; }
        public byte? TmeSlotId { get; set; }
        public string DiscMgmtCd { get; set; }
        public bool? FullCustDiscCd { get; set; }
        public string FullCustDiscReasTxt { get; set; }
        public DateTime StrtTmst { get; set; }
        public DateTime? EndTmst { get; set; }
        public short? ExtraDrtnTmeAmt { get; set; }
        public short? EventDrtnInMinQty { get; set; }
        public byte WrkflwStusId { get; set; }
        public byte? OldEventStusId { get; set; }
        public short? FailReasId { get; set; }
        public bool? CustTrptReqrCd { get; set; }
        public bool? WrlesTrptReqrCd { get; set; }
        public string WoobIpAdr { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public string EsclPolicyLocTxt { get; set; }
        public DateTime? ShippedDt { get; set; }
        public string ShipTrkRefrNbr { get; set; }
        public string ShipCustEmailAdr { get; set; }
        public string ShipDevSerialNbr { get; set; }
        public string EsclBusJustnTxt { get; set; }
        public string OptOutBusJustnTxt { get; set; }
        public string BusJustnTxt { get; set; }
        public bool PreCfgCmpltCd { get; set; }
        public string ShipCxrNme { get; set; }
        public string SrvcTmeZnCd { get; set; }
        public string SrvcAvlbltyHrs { get; set; }
        public bool? CpeOrdrCd { get; set; }
        public bool? MnsOrdrCd { get; set; }
        public string MnsPmId { get; set; }
        public bool CrdlepntCd { get; set; }
        public string StreetAdr { get; set; }
        public string EventTitleTxt { get; set; }
        public string CustNme { get; set; }
        public string InstlSitePocNme { get; set; }
        public string InstlSitePocPhnNbr { get; set; }
        public string InstlSitePocCellPhnNbr { get; set; }
        public string SrvcAssrnPocNme { get; set; }
        public string SrvcAssrnPocPhnNbr { get; set; }
        public string SrvcAssrnPocCellPhnNbr { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtyNme { get; set; }
        public string SttPrvnNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string ZipCd { get; set; }
        public byte? NtwkEventTypeId { get; set; }
        public byte? VpnPltfrmTypeId { get; set; }
        public string NtwkH6 { get; set; }
        public string NtwkH1 { get; set; }
        public string NtwkCustNme { get; set; }
        public bool? FrcdftCd { get; set; }
        public bool MdsCmpltCd { get; set; }
        public bool NtwkCmpltCd { get; set; }
        public bool VasCmpltCd { get; set; }
        public bool SoftAssignCd { get; set; }
        public string AltShipPocNme { get; set; }
        public string AltShipPocPhnNbr { get; set; }
        public string AltShipPocEmail { get; set; }
        public string AltShipStreetAdr { get; set; }
        public string AltShipFlrBldgNme { get; set; }
        public string AltShipCtyNme { get; set; }
        public string AltShipSttPrvnNme { get; set; }
        public string AltShipCtryRgnNme { get; set; }
        public string AltShipZipCd { get; set; }
        public string NidHostNme { get; set; }
        public string NidSerialNbr { get; set; }
        public string IpmDes { get; set; }
        public bool? RltdCmpsNcrCd { get; set; }
        public string RltdCmpsNcrNme { get; set; }
        public string CntctEmailList { get; set; }
        public string OdieCustId { get; set; }
        public string MdrNumber { get; set; }
        public bool? CustLtrOptOut { get; set; }

        public virtual LkMdsBrdgNeedReas CnfrcBrdg { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkEsclReas EsclReas { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkEventStus EventStus { get; set; }
        public virtual LkFailReas FailReas { get; set; }
        public virtual LkMdsActyType MdsActyType { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkMplsEventType NtwkEventType { get; set; }
        public virtual LkEventStus OldEventStus { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUser ReqorUser { get; set; }
        public virtual LkSprintCpeNcr SprintCpeNcr { get; set; }
        public virtual LkVpnPltfrmType VpnPltfrmType { get; set; }
        public virtual LkWrkflwStus WrkflwStus { get; set; }
    }
}
