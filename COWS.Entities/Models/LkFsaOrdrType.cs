﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFsaOrdrType
    {
        public LkFsaOrdrType()
        {
            FsaOrdr = new HashSet<FsaOrdr>();
            LkOrdrType = new HashSet<LkOrdrType>();
        }

        public string FsaOrdrTypeCd { get; set; }
        public string FsaOrdrTypeDes { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<FsaOrdr> FsaOrdr { get; set; }
        public virtual ICollection<LkOrdrType> LkOrdrType { get; set; }
    }
}
