﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkVndrOrdrType
    {
        public LkVndrOrdrType()
        {
            VndrOrdr = new HashSet<VndrOrdr>();
        }

        public byte VndrOrdrTypeId { get; set; }
        public string VndrOrdrTypeDes { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<VndrOrdr> VndrOrdr { get; set; }
    }
}
