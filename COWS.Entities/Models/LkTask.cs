﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkTask
    {
        public LkTask()
        {
            ActTask = new HashSet<ActTask>();
            LkXnciMs = new HashSet<LkXnciMs>();
            MapGrpTask = new HashSet<MapGrpTask>();
            MapPrfTask = new HashSet<MapPrfTask>();
            WgProfSmDesrdTask = new HashSet<WgProfSm>();
            WgProfSmPreReqstTask = new HashSet<WgProfSm>();
        }

        public short TaskId { get; set; }
        public string TaskNme { get; set; }
        public short OrdrStusId { get; set; }
        public short WgStusId { get; set; }
        public int ModfdByUserId { get; set; }
        public DateTime ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string TaskDes { get; set; }

        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkStus OrdrStus { get; set; }
        public virtual LkStus WgStus { get; set; }
        public virtual ICollection<ActTask> ActTask { get; set; }
        public virtual ICollection<LkXnciMs> LkXnciMs { get; set; }
        public virtual ICollection<MapGrpTask> MapGrpTask { get; set; }
        public virtual ICollection<MapPrfTask> MapPrfTask { get; set; }
        public virtual ICollection<WgProfSm> WgProfSmDesrdTask { get; set; }
        public virtual ICollection<WgProfSm> WgProfSmPreReqstTask { get; set; }
    }
}
