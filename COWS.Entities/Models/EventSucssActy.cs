﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventSucssActy
    {
        public int EventHistId { get; set; }
        public short SucssActyId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual EventHist EventHist { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkSucssActy SucssActy { get; set; }
    }
}
