﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptRedsgn
    {
        public int Id { get; set; }
        public int CptId { get; set; }
        public int? RedsgnId { get; set; }
        public string CustShrtNme { get; set; }
        public string H1 { get; set; }
        public short StusId { get; set; }
        public int CreatUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdUserId { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkUser CreatUser { get; set; }
        public virtual Redsgn Redsgn { get; set; }
        public virtual LkStus Stus { get; set; }
    }
}
