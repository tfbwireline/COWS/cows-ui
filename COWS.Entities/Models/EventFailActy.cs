﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventFailActy
    {
        public int EventHistId { get; set; }
        public short FailActyId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual EventHist EventHist { get; set; }
        public virtual LkFailActy FailActy { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
