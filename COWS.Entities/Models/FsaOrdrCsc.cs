﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FsaOrdrCsc
    {
        public int OrdrId { get; set; }
        public string SerialNbr { get; set; }
        public string MtrlRqstnTxt { get; set; }
        public string PrchOrdrTxt { get; set; }
        public string IntlMntcSchrgTxt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual FsaOrdr Ordr { get; set; }
    }
}
