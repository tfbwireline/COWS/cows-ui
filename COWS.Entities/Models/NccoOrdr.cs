﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class NccoOrdr
    {
        public int OrdrId { get; set; }
        public string SiteCityNme { get; set; }
        public string CtryCd { get; set; }
        public int? H5AcctNbr { get; set; }
        public string SolNme { get; set; }
        public string OeNme { get; set; }
        public string SotsNbr { get; set; }
        public string BillCycNbr { get; set; }
        public byte ProdTypeId { get; set; }
        public byte OrdrTypeId { get; set; }
        public DateTime? CwdDt { get; set; }
        public DateTime? CcsDt { get; set; }
        public string EmailAdr { get; set; }
        public string CmntTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public string PlNbr { get; set; }
        public string OrdrByLassieCd { get; set; }
        public string VndrCd { get; set; }
        public string CustNme { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkOrdrType OrdrType { get; set; }
        public virtual LkProdType ProdType { get; set; }
        public virtual LkVndr VndrCdNavigation { get; set; }
    }
}
