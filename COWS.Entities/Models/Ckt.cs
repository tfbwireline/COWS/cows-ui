﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class Ckt
    {
        public Ckt()
        {
            CktCost = new HashSet<CktCost>();
            CktMs = new HashSet<CktMs>();
        }

        public int CktId { get; set; }
        public int? OrdrId { get; set; }
        public string VndrCktId { get; set; }
        public string LecId { get; set; }
        public DateTime? VndrCntrcTermEndDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public int? VndrOrdrId { get; set; }
        public string PlSeqNbr { get; set; }
        public bool TrmtgCd { get; set; }
        public string RtngPrcol { get; set; }
        public string IpAdrQty { get; set; }
        public string CustIpAdr { get; set; }
        public string ScaDet { get; set; }
        public string RltdVndrOrdrId { get; set; }
        public string XCnnctPrvdr { get; set; }
        public string TDwnAssn { get; set; }
        public string TDwnReq { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual VndrOrdr VndrOrdr { get; set; }
        public virtual ICollection<CktCost> CktCost { get; set; }
        public virtual ICollection<CktMs> CktMs { get; set; }
    }
}
