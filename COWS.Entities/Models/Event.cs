﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class Event
    {
        public Event()
        {
            Appt = new HashSet<Appt>();
            EmailReq = new HashSet<EmailReq>();
            EventAsnToUser = new HashSet<EventAsnToUser>();
            EventCpeDev = new HashSet<EventCpeDev>();
            EventDevCmplt = new HashSet<EventDevCmplt>();
            EventDevSrvcMgmt = new HashSet<EventDevSrvcMgmt>();
            EventDiscoDev = new HashSet<EventDiscoDev>();
            EventHist = new HashSet<EventHist>();
            FedlineEventPrcs = new HashSet<FedlineEventPrcs>();
            FedlineEventTadpoleData = new HashSet<FedlineEventTadpoleData>();
            FedlineReqActHist = new HashSet<FedlineReqActHist>();
            MdsEventDslSbicCustTrpt = new HashSet<MdsEventDslSbicCustTrpt>();
            MdsEventFormChng = new HashSet<MdsEventFormChng>();
            MdsEventMacActy = new HashSet<MdsEventMacActy>();
            MdsEventNtwkActy = new HashSet<MdsEventNtwkActy>();
            MdsEventNtwkCust = new HashSet<MdsEventNtwkCust>();
            MdsEventNtwkTrpt = new HashSet<MdsEventNtwkTrpt>();
            MdsEventOdieDev = new HashSet<MdsEventOdieDev>();
            MdsEventPortBndwd = new HashSet<MdsEventPortBndwd>();
            MdsEventSiteSrvc = new HashSet<MdsEventSiteSrvc>();
            MdsEventSlnkWiredTrpt = new HashSet<MdsEventSlnkWiredTrpt>();
            MdsEventWrlsTrpt = new HashSet<MdsEventWrlsTrpt>();
            MplsEventActyType = new HashSet<MplsEventActyType>();
            MplsEventVasType = new HashSet<MplsEventVasType>();
            NidActy = new HashSet<NidActy>();
            OdieReq = new HashSet<OdieReq>();
            OdieRspnInfo = new HashSet<OdieRspnInfo>();
            RedsgnDevEventHist = new HashSet<RedsgnDevEventHist>();
            ReltdEventEvent = new HashSet<ReltdEvent>();
            ReltdEventReltdEventNavigation = new HashSet<ReltdEvent>();
            SiptEventActy = new HashSet<SiptEventActy>();
            SiptEventTollType = new HashSet<SiptEventTollType>();
            SiptReltdOrdr = new HashSet<SiptReltdOrdr>();
            UcaaSEventBilling = new HashSet<UcaaSEventBilling>();
            UcaaSEventOdieDev = new HashSet<UcaaSEventOdieDev>();
        }

        public int EventId { get; set; }
        public short EventTypeId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public byte CsgLvlId { get; set; }

        public virtual LkEventType EventType { get; set; }
        public virtual AdEvent AdEvent { get; set; }
        public virtual EventNvldTmeSlot EventNvldTmeSlot { get; set; }
        public virtual EventRecLock EventRecLock { get; set; }
        public virtual FedlineEventUserData FedlineEventUserData { get; set; }
        public virtual MdsEvent MdsEvent { get; set; }
        public virtual MdsEventNew MdsEventNew { get; set; }
        public virtual MplsEvent MplsEvent { get; set; }
        public virtual NgvnEvent NgvnEvent { get; set; }
        public virtual SiptEvent SiptEvent { get; set; }
        public virtual SplkEvent SplkEvent { get; set; }
        public virtual UcaaSEvent UcaaSEvent { get; set; }
        public virtual ICollection<Appt> Appt { get; set; }
        public virtual ICollection<EmailReq> EmailReq { get; set; }
        public virtual ICollection<EventAsnToUser> EventAsnToUser { get; set; }
        public virtual ICollection<EventCpeDev> EventCpeDev { get; set; }
        public virtual ICollection<EventDevCmplt> EventDevCmplt { get; set; }
        public virtual ICollection<EventDevSrvcMgmt> EventDevSrvcMgmt { get; set; }
        public virtual ICollection<EventDiscoDev> EventDiscoDev { get; set; }
        public virtual ICollection<EventHist> EventHist { get; set; }
        public virtual ICollection<FedlineEventPrcs> FedlineEventPrcs { get; set; }
        public virtual ICollection<FedlineEventTadpoleData> FedlineEventTadpoleData { get; set; }
        public virtual ICollection<FedlineReqActHist> FedlineReqActHist { get; set; }
        public virtual ICollection<MdsEventDslSbicCustTrpt> MdsEventDslSbicCustTrpt { get; set; }
        public virtual ICollection<MdsEventFormChng> MdsEventFormChng { get; set; }
        public virtual ICollection<MdsEventMacActy> MdsEventMacActy { get; set; }
        public virtual ICollection<MdsEventNtwkActy> MdsEventNtwkActy { get; set; }
        public virtual ICollection<MdsEventNtwkCust> MdsEventNtwkCust { get; set; }
        public virtual ICollection<MdsEventNtwkTrpt> MdsEventNtwkTrpt { get; set; }
        public virtual ICollection<MdsEventOdieDev> MdsEventOdieDev { get; set; }
        public virtual ICollection<MdsEventPortBndwd> MdsEventPortBndwd { get; set; }
        public virtual ICollection<MdsEventSiteSrvc> MdsEventSiteSrvc { get; set; }
        public virtual ICollection<MdsEventSlnkWiredTrpt> MdsEventSlnkWiredTrpt { get; set; }
        public virtual ICollection<MdsEventWrlsTrpt> MdsEventWrlsTrpt { get; set; }
        public virtual ICollection<MplsEventActyType> MplsEventActyType { get; set; }
        public virtual ICollection<MplsEventVasType> MplsEventVasType { get; set; }
        public virtual ICollection<NidActy> NidActy { get; set; }
        public virtual ICollection<OdieReq> OdieReq { get; set; }
        public virtual ICollection<OdieRspnInfo> OdieRspnInfo { get; set; }
        public virtual ICollection<RedsgnDevEventHist> RedsgnDevEventHist { get; set; }
        public virtual ICollection<ReltdEvent> ReltdEventEvent { get; set; }
        public virtual ICollection<ReltdEvent> ReltdEventReltdEventNavigation { get; set; }
        public virtual ICollection<SiptEventActy> SiptEventActy { get; set; }
        public virtual ICollection<SiptEventTollType> SiptEventTollType { get; set; }
        public virtual ICollection<SiptReltdOrdr> SiptReltdOrdr { get; set; }
        public virtual ICollection<UcaaSEventBilling> UcaaSEventBilling { get; set; }
        public virtual ICollection<UcaaSEventOdieDev> UcaaSEventOdieDev { get; set; }
    }
}
