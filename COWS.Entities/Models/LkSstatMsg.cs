﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkSstatMsg
    {
        public LkSstatMsg()
        {
            SstatReq = new HashSet<SstatReq>();
        }

        public int SstatMsgId { get; set; }
        public string SstatMsgNme { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<SstatReq> SstatReq { get; set; }
    }
}
