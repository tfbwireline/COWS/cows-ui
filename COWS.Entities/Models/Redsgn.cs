﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class Redsgn
    {
        public Redsgn()
        {
            CptRedsgn = new HashSet<CptRedsgn>();
            EmailReq = new HashSet<EmailReq>();
            RedsgnDevicesInfo = new HashSet<RedsgnDevicesInfo>();
            RedsgnDoc = new HashSet<RedsgnDoc>();
            RedsgnEmailNtfctn = new HashSet<RedsgnEmailNtfctn>();
            RedsgnNotes = new HashSet<RedsgnNotes>();
        }

        public int RedsgnId { get; set; }
        public string RedsgnNbr { get; set; }
        public string H1Cd { get; set; }
        public string NteAssigned { get; set; }
        public string PmAssigned { get; set; }
        public byte RedsgnTypeId { get; set; }
        public short StusId { get; set; }
        public bool EntireNwCkdId { get; set; }
        public short? RedsgnDevStusId { get; set; }
        public int CretdByCd { get; set; }
        public DateTime CretdDt { get; set; }
        public int? ModfdByCd { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime? SubmitDt { get; set; }
        public DateTime? ModSubmitDt { get; set; }
        public DateTime? SlaDt { get; set; }
        public string OdieCustId { get; set; }
        public string ApprovNbr { get; set; }
        public decimal? CostAmt { get; set; }
        public DateTime? ExprtnDt { get; set; }
        public byte CsgLvlId { get; set; }
        public bool? ExtXpirnFlgCd { get; set; }
        public byte? RedsgnCatId { get; set; }
        public string DsgnDocLocTxt { get; set; }
        public string SdeAsnNme { get; set; }
        public decimal? NteLvlEffortAmt { get; set; }
        public decimal? PmLvlEffortAmt { get; set; }
        public decimal? NeLvlEffortAmt { get; set; }
        public decimal? NteOtLvlEffortAmt { get; set; }
        public decimal? PmOtLvlEffortAmt { get; set; }
        public decimal? NeOtLvlEffortAmt { get; set; }
        public decimal? SpsLvlEffortAmt { get; set; }
        public string NeAsnNme { get; set; }
        public string SowsFoldrPathNme { get; set; }
        public decimal? SpsOtLvlEffortAmt { get; set; }
        public bool BillOvrrdnCd { get; set; }
        public decimal? BillOvrrdnAmt { get; set; }
        public bool IsSdwan { get; set; }
        public string CustNme { get; set; }
        public string CustEmailAdr { get; set; }
        public string DsgnDocNbrBpm { get; set; }
        public bool? CiscSmrtLicCd { get; set; }
        public string SmrtAccntDmn { get; set; }
        public decimal? MssImplEstAmt { get; set; }
        public string VrtlAccnt { get; set; }
        public string BpmRedsgnNbr { get; set; }
        public string AddlDocTxt { get; set; }
        public string CntctEmailList { get; set; }

        public virtual LkUser CretdByCdNavigation { get; set; }
        public virtual LkCsgLvl CsgLvl { get; set; }
        public virtual LkUser ModfdByCdNavigation { get; set; }
        public virtual LkRedsgnCat RedsgnCat { get; set; }
        public virtual LkStus RedsgnDevStus { get; set; }
        public virtual LkRedsgnType RedsgnType { get; set; }
        public virtual LkStus Stus { get; set; }
        public virtual RedsgnRecLock RedsgnRecLock { get; set; }
        public virtual ICollection<CptRedsgn> CptRedsgn { get; set; }
        public virtual ICollection<EmailReq> EmailReq { get; set; }
        public virtual ICollection<RedsgnDevicesInfo> RedsgnDevicesInfo { get; set; }
        public virtual ICollection<RedsgnDoc> RedsgnDoc { get; set; }
        public virtual ICollection<RedsgnEmailNtfctn> RedsgnEmailNtfctn { get; set; }
        public virtual ICollection<RedsgnNotes> RedsgnNotes { get; set; }
    }
}
