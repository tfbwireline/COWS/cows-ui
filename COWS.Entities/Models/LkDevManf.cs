﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkDevManf
    {
        public LkDevManf()
        {
            EventDiscoDev = new HashSet<EventDiscoDev>();
            LkDevModel = new HashSet<LkDevModel>();
            MdsEventOdieDev = new HashSet<MdsEventOdieDev>();
            UcaaSEventOdieDev = new HashSet<UcaaSEventOdieDev>();
        }

        public short ManfId { get; set; }
        public string ManfNme { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<EventDiscoDev> EventDiscoDev { get; set; }
        public virtual ICollection<LkDevModel> LkDevModel { get; set; }
        public virtual ICollection<MdsEventOdieDev> MdsEventOdieDev { get; set; }
        public virtual ICollection<UcaaSEventOdieDev> UcaaSEventOdieDev { get; set; }
    }
}
