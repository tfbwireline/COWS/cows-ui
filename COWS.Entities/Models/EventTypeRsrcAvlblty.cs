﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventTypeRsrcAvlblty
    {
        public int EventTypeRsrcAvlbltyId { get; set; }
        public short EventTypeId { get; set; }
        public DateTime DayDt { get; set; }
        public byte TmeSlotId { get; set; }
        public short AvalTmeSlotCnt { get; set; }
        public short MaxAvalTmeSlotCapCnt { get; set; }
        public short CurrAvalTmeSlotCnt { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string EventIdListTxt { get; set; }

        public virtual LkEventType EventType { get; set; }
        public virtual LkEventTypeTmeSlot LkEventTypeTmeSlot { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
    }
}
