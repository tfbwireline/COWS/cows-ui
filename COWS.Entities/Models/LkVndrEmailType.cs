﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkVndrEmailType
    {
        public LkVndrEmailType()
        {
            VndrOrdrEmail = new HashSet<VndrOrdrEmail>();
        }

        public byte VndrEmailTypeId { get; set; }
        public string VndrEmailTypeDes { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<VndrOrdrEmail> VndrOrdrEmail { get; set; }
    }
}
