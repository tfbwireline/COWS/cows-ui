﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FsaOrdrCpeLineItem
    {
        public FsaOrdrCpeLineItem()
        {
            FsaOrdrGomXnci = new HashSet<FsaOrdrGomXnci>();
            NidActy = new HashSet<NidActy>();
        }

        public int FsaCpeLineItemId { get; set; }
        public int OrdrId { get; set; }
        public string SrvcLineItemCd { get; set; }
        public string EqptTypeId { get; set; }
        public string EqptId { get; set; }
        public string MfrNme { get; set; }
        public string CntrcTypeId { get; set; }
        public string CntrcTermId { get; set; }
        public string InstlnCd { get; set; }
        public string MntcCd { get; set; }
        public DateTime CreatDt { get; set; }
        public string DiscPlNbr { get; set; }
        public int? DiscFmsCktNbr { get; set; }
        public string DiscNwAdr { get; set; }
        public string MdsDes { get; set; }
        public int? CmpntId { get; set; }
        public string LineItemCd { get; set; }
        public string FsaOrgnlInstlOrdrId { get; set; }
        public string CxrSrvcId { get; set; }
        public string CxrCktId { get; set; }
        public string TtrptAccsTypeCd { get; set; }
        public string TtrptSpdOfSrvcBdwdDes { get; set; }
        public string TtrptAccsTypeDes { get; set; }
        public int? FsaOrdrVasId { get; set; }
        public string PlsftRqstnNbr { get; set; }
        public DateTime? RqstnDt { get; set; }
        public string PrchOrdrNbr { get; set; }
        public DateTime? EqptItmRcvdDt { get; set; }
        public string MatlCd { get; set; }
        public string UnitMsr { get; set; }
        public string ManfPartCd { get; set; }
        public string VndrCd { get; set; }
        public int? OrdrQty { get; set; }
        public string UnitPrice { get; set; }
        public string ManfDiscntCd { get; set; }
        public int? FmsCktNbr { get; set; }
        public string EqptRcvdByAdid { get; set; }
        public DateTime? CmplDt { get; set; }
        public int? PoLnNbr { get; set; }
        public int? RcvdQty { get; set; }
        public byte? PsRcvdStus { get; set; }
        public string Pid { get; set; }
        public string DropShp { get; set; }
        public string DeviceId { get; set; }
        public string Supplier { get; set; }
        public int? OrdrCmpntId { get; set; }
        public short? ItmStus { get; set; }
        public string CmpntFmly { get; set; }
        public int? RltdCmpntId { get; set; }
        public string StusCd { get; set; }
        public string MfrPsId { get; set; }
        public bool? CpeReuseCd { get; set; }
        public string InstlDsgnDocNbr { get; set; }
        public string TtrptAccsArngtCd { get; set; }
        public string TtrptEncapCd { get; set; }
        public string TtrptFbrHandOffCd { get; set; }
        public string CxrAccsCd { get; set; }
        public string CktId { get; set; }
        public DateTime? TtrptQotXpirnDt { get; set; }
        public string TtrptAccsTermDes { get; set; }
        public string TtrptAccsBdwdType { get; set; }
        public string PlNbr { get; set; }
        public string PortRtTypeCd { get; set; }
        public string TportIpVerTypeCd { get; set; }
        public string TportIpv4AdrPrvdrCd { get; set; }
        public string TportIpv4AdrQty { get; set; }
        public string TportIpv6AdrPrvdrCd { get; set; }
        public string TportIpv6AdrQty { get; set; }
        public short? TportVlanQty { get; set; }
        public string TportEthrntNrfcIndcr { get; set; }
        public string TportCnctrTypeId { get; set; }
        public string TportCustRoutrTagTxt { get; set; }
        public string TportCustRoutrAutoNegotCd { get; set; }
        public string SpaAccsIndcrDes { get; set; }
        public string TportDiaNocToNocTxt { get; set; }
        public string TportProjDes { get; set; }
        public string TportVndrQuoteId { get; set; }
        public string TportEthAccsTypeCd { get; set; }
        public string ZsclrQuoteId { get; set; }
        public string TtrptCosCd { get; set; }
        public string SmrtAcctDomnTxt { get; set; }
        public string VrtlAcctDomnTxt { get; set; }
        public string NidSerialNbr { get; set; }
        public string SprintMntdFlg { get; set; }
        public string CpeCustPrvdSerialNbr { get; set; }

        public virtual FsaOrdr Ordr { get; set; }
        public virtual ICollection<FsaOrdrGomXnci> FsaOrdrGomXnci { get; set; }
        public virtual ICollection<NidActy> NidActy { get; set; }
    }
}
