﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptRltdInfo
    {
        public int CptRltdInfoId { get; set; }
        public int CptId { get; set; }
        public short? CptPlnSrvcTierId { get; set; }
        public short CptCustTypeId { get; set; }
        public bool? HstdUcCd { get; set; }
        public bool CubeSiptSmiCd { get; set; }
        public bool? E2eNddCd { get; set; }
        public string NtwkDsgnDocTxt { get; set; }
        public string CmntTxt { get; set; }
        public bool? CustUcdCd { get; set; }
        public bool? SpsSowCd { get; set; }
        public DateTime CreatDt { get; set; }
        public bool? SdwanCustCd { get; set; }
        public bool? SuplmntlEthrntCd { get; set; }
        public string CntctEmailList { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptCustType CptCustType { get; set; }
        public virtual LkCptPlnSrvcTier CptPlnSrvcTier { get; set; }
    }
}
