﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkUsStt
    {
        public LkUsStt()
        {
            VndrFoldr = new HashSet<VndrFoldr>();
        }

        public string UsStateName { get; set; }
        public string UsStateId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public string TmeZoneId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<VndrFoldr> VndrFoldr { get; set; }
    }
}
