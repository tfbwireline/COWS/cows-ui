﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkRole
    {
        public LkRole()
        {
            CntctDetl = new HashSet<CntctDetl>();
            EventAsnToUser = new HashSet<EventAsnToUser>();
            FedlineEventCntct = new HashSet<FedlineEventCntct>();
            OrdrCntct = new HashSet<OrdrCntct>();
            UserWfm = new HashSet<UserWfm>();
        }

        public byte RoleId { get; set; }
        public string RoleNme { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public string RoleTypeId { get; set; }
        public string RoleCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual ICollection<CntctDetl> CntctDetl { get; set; }
        public virtual ICollection<EventAsnToUser> EventAsnToUser { get; set; }
        public virtual ICollection<FedlineEventCntct> FedlineEventCntct { get; set; }
        public virtual ICollection<OrdrCntct> OrdrCntct { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
    }
}
