﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OrdrMs
    {
        public int OrdrId { get; set; }
        public short VerId { get; set; }
        public DateTime? PreSbmtDt { get; set; }
        public DateTime? SbmtDt { get; set; }
        public DateTime? VldtdDt { get; set; }
        public DateTime? OrdrBillClearInstlDt { get; set; }
        public DateTime? OrdrDscnctDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? VndrCnclnDt { get; set; }
        public DateTime? CustAcptcTurnupDt { get; set; }
        public DateTime? XnciCnfrmRnlDt { get; set; }
        public string RnlDtReqrCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
    }
}
