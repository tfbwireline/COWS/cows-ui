﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class VndrOrdr
    {
        public VndrOrdr()
        {
            Ckt = new HashSet<Ckt>();
            VndrOrdrEmail = new HashSet<VndrOrdrEmail>();
            VndrOrdrForm = new HashSet<VndrOrdrForm>();
            VndrOrdrMs = new HashSet<VndrOrdrMs>();
        }

        public int VndrOrdrId { get; set; }
        public int VndrFoldrId { get; set; }
        public int? OrdrId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public byte RecStusId { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte VndrOrdrTypeId { get; set; }
        public bool TrmtgCd { get; set; }
        public int? PrevOrdrId { get; set; }
        public bool? BypasVndrOrdrMsCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual Ordr PrevOrdr { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual VndrFoldr VndrFoldr { get; set; }
        public virtual Ordr VndrOrdrNavigation { get; set; }
        public virtual LkVndrOrdrType VndrOrdrType { get; set; }
        public virtual ICollection<Ckt> Ckt { get; set; }
        public virtual ICollection<VndrOrdrEmail> VndrOrdrEmail { get; set; }
        public virtual ICollection<VndrOrdrForm> VndrOrdrForm { get; set; }
        public virtual ICollection<VndrOrdrMs> VndrOrdrMs { get; set; }
    }
}
