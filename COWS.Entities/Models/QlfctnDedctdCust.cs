﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnDedctdCust
    {
        public int QlfctnId { get; set; }
        public int CustId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkDedctdCust Cust { get; set; }
        public virtual LkQlfctn Qlfctn { get; set; }
    }
}
