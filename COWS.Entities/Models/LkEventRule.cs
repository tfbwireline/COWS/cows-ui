﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkEventRule
    {
        public LkEventRule()
        {
            EventRuleData = new HashSet<EventRuleData>();
            LkFedRuleFail = new HashSet<LkFedRuleFail>();
        }

        public int EventRuleId { get; set; }
        public short UsrPrfId { get; set; }
        public short EventTypeId { get; set; }
        public string OrdrTypeCd { get; set; }
        public byte StrtEventStusId { get; set; }
        public byte EndEventStusId { get; set; }
        public byte? WrkflwStusId { get; set; }
        public byte? CalCd { get; set; }
        public byte ActnId { get; set; }
        public short? FailReasId { get; set; }
        public bool? StagnCd { get; set; }
        public byte? FedMsgId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte? DsplCd { get; set; }

        public virtual LkActn Actn { get; set; }
        public virtual LkEventStus EndEventStus { get; set; }
        public virtual LkEventType EventType { get; set; }
        public virtual LkFailReas FailReas { get; set; }
        public virtual LkFedMsg FedMsg { get; set; }
        public virtual LkFedlineOrdrType OrdrTypeCdNavigation { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkEventStus StrtEventStus { get; set; }
        public virtual LkUsrPrf UsrPrf { get; set; }
        public virtual LkWrkflwStus WrkflwStus { get; set; }
        public virtual ICollection<EventRuleData> EventRuleData { get; set; }
        public virtual ICollection<LkFedRuleFail> LkFedRuleFail { get; set; }
    }
}
