﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventFormChng
    {
        public int MdsEventFormChngId { get; set; }
        public int MdsEventId { get; set; }
        public string ChngTypeNme { get; set; }
        public DateTime CreatDt { get; set; }
        public int? CreatByUserId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual Event MdsEvent { get; set; }
    }
}
