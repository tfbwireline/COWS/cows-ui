﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnIpVer
    {
        public int QlfctnId { get; set; }
        public byte IpVerId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkIpVer IpVer { get; set; }
        public virtual LkQlfctn Qlfctn { get; set; }
    }
}
