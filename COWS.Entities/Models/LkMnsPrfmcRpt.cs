﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMnsPrfmcRpt
    {
        public LkMnsPrfmcRpt()
        {
            MplsEventAccsTag = new HashSet<MplsEventAccsTag>();
        }

        public byte MnsPrfmcRptId { get; set; }
        public string MnsPrfmcRptDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<MplsEventAccsTag> MplsEventAccsTag { get; set; }
    }
}
