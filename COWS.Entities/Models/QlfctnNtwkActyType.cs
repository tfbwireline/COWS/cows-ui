﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnNtwkActyType
    {
        public byte NtwkActyTypeId { get; set; }
        public int QlfctnId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkMdsNtwkActyType NtwkActyType { get; set; }
        public virtual LkQlfctn Qlfctn { get; set; }
    }
}
