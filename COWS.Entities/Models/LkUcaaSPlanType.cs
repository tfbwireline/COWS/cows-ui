﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkUcaaSPlanType
    {
        public LkUcaaSPlanType()
        {
            LkUcaaSBillActy = new HashSet<LkUcaaSBillActy>();
            UcaaSEvent = new HashSet<UcaaSEvent>();
        }

        public short UcaaSPlanTypeId { get; set; }
        public string UcaaSPlanType { get; set; }
        public string UcaaSPlanTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<LkUcaaSBillActy> LkUcaaSBillActy { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEvent { get; set; }
    }
}
