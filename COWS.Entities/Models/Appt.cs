﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class Appt
    {
        public Appt()
        {
            ApptRcurncData = new HashSet<ApptRcurncData>();
        }

        public int ApptId { get; set; }
        public int? EventId { get; set; }
        public DateTime StrtTmst { get; set; }
        public DateTime? EndTmst { get; set; }
        public string RcurncDesTxt { get; set; }
        public string SubjTxt { get; set; }
        public string Des { get; set; }
        public string ApptLocTxt { get; set; }
        public int ApptTypeId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string AsnToUserIdListTxt { get; set; }
        public bool RcurncCd { get; set; }
        public int? ActyTypeId { get; set; }
        public byte? RecStusId { get; set; }

        public virtual LkApptType ApptType { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<ApptRcurncData> ApptRcurncData { get; set; }
    }
}
