﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkUcaaSActyType
    {
        public LkUcaaSActyType()
        {
            UcaaSEvent = new HashSet<UcaaSEvent>();
        }

        public short UcaaSActyTypeId { get; set; }
        public string UcaaSActyTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEvent { get; set; }
    }
}
