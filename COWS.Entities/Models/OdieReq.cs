﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OdieReq
    {
        public OdieReq()
        {
            CptOdieRspn = new HashSet<CptOdieRspn>();
            OdieRspn = new HashSet<OdieRspn>();
            OdieRspnInfo = new HashSet<OdieRspnInfo>();
        }

        public int ReqId { get; set; }
        public int? OrdrId { get; set; }
        public int OdieMsgId { get; set; }
        public DateTime StusModDt { get; set; }
        public string H1CustId { get; set; }
        public DateTime CreatDt { get; set; }
        public short StusId { get; set; }
        public int? MdsEventId { get; set; }
        public byte? TabSeqNbr { get; set; }
        public string OdieCustId { get; set; }
        public int? CptId { get; set; }
        public int? OdieCustInfoReqCatTypeId { get; set; }
        public string DevFltr { get; set; }
        public byte CsgLvlId { get; set; }
        public string CustNme { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCsgLvl CsgLvl { get; set; }
        public virtual Event MdsEvent { get; set; }
        public virtual FsaOrdr Ordr { get; set; }
        public virtual LkStus Stus { get; set; }
        public virtual ICollection<CptOdieRspn> CptOdieRspn { get; set; }
        public virtual ICollection<OdieRspn> OdieRspn { get; set; }
        public virtual ICollection<OdieRspnInfo> OdieRspnInfo { get; set; }
    }
}
