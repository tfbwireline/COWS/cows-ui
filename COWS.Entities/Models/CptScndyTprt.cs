﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptScndyTprt
    {
        public int CptScndyTprtId { get; set; }
        public int CptId { get; set; }
        public short CptPlnSrvcTierId { get; set; }
        public short CptPrimScndyTprtId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptPlnSrvcTier CptPlnSrvcTier { get; set; }
        public virtual LkCptPrimScndyTprt CptPrimScndyTprt { get; set; }
    }
}
