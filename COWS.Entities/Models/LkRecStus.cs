﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkRecStus
    {
        public LkRecStus()
        {
            AdEvent = new HashSet<AdEvent>();
            Appt = new HashSet<Appt>();
            CpeClli = new HashSet<CpeClli>();
            Cpt = new HashSet<Cpt>();
            CptDoc = new HashSet<CptDoc>();
            EventAsnToUser = new HashSet<EventAsnToUser>();
            EventFailActy = new HashSet<EventFailActy>();
            EventRuleData = new HashSet<EventRuleData>();
            EventSucssActy = new HashSet<EventSucssActy>();
            FedlineEventTadpoleData = new HashSet<FedlineEventTadpoleData>();
            H5Doc = new HashSet<H5Doc>();
            H5Foldr = new HashSet<H5Foldr>();
            IplOrdr = new HashSet<IplOrdr>();
            LkActn = new HashSet<LkActn>();
            LkActnType = new HashSet<LkActnType>();
            LkActyLocale = new HashSet<LkActyLocale>();
            LkAdrType = new HashSet<LkAdrType>();
            LkApptType = new HashSet<LkApptType>();
            LkCcdMissdReas = new HashSet<LkCcdMissdReas>();
            LkCktChgType = new HashSet<LkCktChgType>();
            LkCnfrcBrdg = new HashSet<LkCnfrcBrdg>();
            LkCntctType = new HashSet<LkCntctType>();
            LkCpePid = new HashSet<LkCpePid>();
            LkCptCustType = new HashSet<LkCptCustType>();
            LkCptHstMngdSrvc = new HashSet<LkCptHstMngdSrvc>();
            LkCptMdsSuprtTier = new HashSet<LkCptMdsSuprtTier>();
            LkCptMngdAuthPrd = new HashSet<LkCptMngdAuthPrd>();
            LkCptMvsProdType = new HashSet<LkCptMvsProdType>();
            LkCptPlnSrvcTier = new HashSet<LkCptPlnSrvcTier>();
            LkCptPlnSrvcType = new HashSet<LkCptPlnSrvcType>();
            LkCptPrimScndyTprt = new HashSet<LkCptPrimScndyTprt>();
            LkCptPrimSite = new HashSet<LkCptPrimSite>();
            LkCptPrvsnType = new HashSet<LkCptPrvsnType>();
            LkCsgLvl = new HashSet<LkCsgLvl>();
            LkCtry = new HashSet<LkCtry>();
            LkCtryCty = new HashSet<LkCtryCty>();
            LkCur = new HashSet<LkCur>();
            LkCustCntrcLgth = new HashSet<LkCustCntrcLgth>();
            LkDedctdCust = new HashSet<LkDedctdCust>();
            LkDev = new HashSet<LkDev>();
            LkDevManf = new HashSet<LkDevManf>();
            LkDevModel = new HashSet<LkDevModel>();
            LkDmstcClliCd = new HashSet<LkDmstcClliCd>();
            LkEnhncSrvc = new HashSet<LkEnhncSrvc>();
            LkEsclReas = new HashSet<LkEsclReas>();
            LkEventRule = new HashSet<LkEventRule>();
            LkEventStus = new HashSet<LkEventStus>();
            LkEventType = new HashSet<LkEventType>();
            LkFailActy = new HashSet<LkFailActy>();
            LkFailReas = new HashSet<LkFailReas>();
            LkFedMsg = new HashSet<LkFedMsg>();
            LkFedRuleFail = new HashSet<LkFedRuleFail>();
            LkFedlineEventFailCd = new HashSet<LkFedlineEventFailCd>();
            LkFedlineOrdrType = new HashSet<LkFedlineOrdrType>();
            LkFrgnCxr = new HashSet<LkFrgnCxr>();
            LkGrp = new HashSet<LkGrp>();
            LkIntlClliCd = new HashSet<LkIntlClliCd>();
            LkIpVer = new HashSet<LkIpVer>();
            LkMdsActyType = new HashSet<LkMdsActyType>();
            LkMdsCpeDevXclusn = new HashSet<LkMdsCpeDevXclusn>();
            LkMdsImplmtnNtvl = new HashSet<LkMdsImplmtnNtvl>();
            LkMdsMacActy = new HashSet<LkMdsMacActy>();
            LkMdsNtwkActyType = new HashSet<LkMdsNtwkActyType>();
            LkMdsSrvcTier = new HashSet<LkMdsSrvcTier>();
            LkMenu = new HashSet<LkMenu>();
            LkMnsOffrgRoutr = new HashSet<LkMnsOffrgRoutr>();
            LkMnsPrfmcRpt = new HashSet<LkMnsPrfmcRpt>();
            LkMnsRoutgType = new HashSet<LkMnsRoutgType>();
            LkMplsAccsBdwd = new HashSet<LkMplsAccsBdwd>();
            LkMplsActyType = new HashSet<LkMplsActyType>();
            LkMplsEventType = new HashSet<LkMplsEventType>();
            LkMplsMgrtnType = new HashSet<LkMplsMgrtnType>();
            LkMultiVrfReq = new HashSet<LkMultiVrfReq>();
            LkNgvnProdType = new HashSet<LkNgvnProdType>();
            LkNteType = new HashSet<LkNteType>();
            LkOrdrCat = new HashSet<LkOrdrCat>();
            LkPrfHrchy = new HashSet<LkPrfHrchy>();
            LkQlfctn = new HashSet<LkQlfctn>();
            LkScrdObjType = new HashSet<LkScrdObjType>();
            LkSdePrdctType = new HashSet<LkSdePrdctType>();
            LkSiptActyType = new HashSet<LkSiptActyType>();
            LkSiptProdType = new HashSet<LkSiptProdType>();
            LkSiptTollType = new HashSet<LkSiptTollType>();
            LkSpclProj = new HashSet<LkSpclProj>();
            LkSplkActyType = new HashSet<LkSplkActyType>();
            LkSplkEventType = new HashSet<LkSplkEventType>();
            LkSprintCpeNcr = new HashSet<LkSprintCpeNcr>();
            LkSprintHldy = new HashSet<LkSprintHldy>();
            LkSrvcAssrnSiteSupp = new HashSet<LkSrvcAssrnSiteSupp>();
            LkStdiReas = new HashSet<LkStdiReas>();
            LkStus = new HashSet<LkStus>();
            LkSucssActy = new HashSet<LkSucssActy>();
            LkSysCfg = new HashSet<LkSysCfg>();
            LkTelco = new HashSet<LkTelco>();
            LkUcaaSActyType = new HashSet<LkUcaaSActyType>();
            LkUcaaSBillActy = new HashSet<LkUcaaSBillActy>();
            LkUcaaSPlanType = new HashSet<LkUcaaSPlanType>();
            LkUcaaSProdType = new HashSet<LkUcaaSProdType>();
            LkUsStt = new HashSet<LkUsStt>();
            LkUser = new HashSet<LkUser>();
            LkUsrPrf = new HashSet<LkUsrPrf>();
            LkUsrPrfMenu = new HashSet<LkUsrPrfMenu>();
            LkVasType = new HashSet<LkVasType>();
            LkVndr = new HashSet<LkVndr>();
            LkVndrEmailLang = new HashSet<LkVndrEmailLang>();
            LkVndrScm = new HashSet<LkVndrScm>();
            LkVpnPltfrmType = new HashSet<LkVpnPltfrmType>();
            LkWhlslPtnr = new HashSet<LkWhlslPtnr>();
            LkWrkflwStus = new HashSet<LkWrkflwStus>();
            LkWrldHldy = new HashSet<LkWrldHldy>();
            LkXnciRgn = new HashSet<LkXnciRgn>();
            MapUsrPrf = new HashSet<MapUsrPrf>();
            MdsEvent = new HashSet<MdsEvent>();
            MdsEventNew = new HashSet<MdsEventNew>();
            MdsFastTrkType = new HashSet<MdsFastTrkType>();
            MplsEvent = new HashSet<MplsEvent>();
            MplsEventAccsTag = new HashSet<MplsEventAccsTag>();
            NgvnEvent = new HashSet<NgvnEvent>();
            NrmCkt = new HashSet<NrmCkt>();
            NrmVndr = new HashSet<NrmVndr>();
            Ordr = new HashSet<Ordr>();
            OrdrAdr = new HashSet<OrdrAdr>();
            OrdrCntct = new HashSet<OrdrCntct>();
            OrdrNte = new HashSet<OrdrNte>();
            RedsgnDoc = new HashSet<RedsgnDoc>();
            SdeOpptnty = new HashSet<SdeOpptnty>();
            SdeOpptntyDoc = new HashSet<SdeOpptntyDoc>();
            SdeOpptntyNte = new HashSet<SdeOpptntyNte>();
            SdeOpptntyPrdct = new HashSet<SdeOpptntyPrdct>();
            SiptEvent = new HashSet<SiptEvent>();
            SiptEventDoc = new HashSet<SiptEventDoc>();
            SplkEvent = new HashSet<SplkEvent>();
            SrcCurFile = new HashSet<SrcCurFile>();
            TrptOrdr = new HashSet<TrptOrdr>();
            UcaaSEvent = new HashSet<UcaaSEvent>();
            UserCsgLvl = new HashSet<UserCsgLvl>();
            UserH5Wfm = new HashSet<UserH5Wfm>();
            UserWfm = new HashSet<UserWfm>();
            VndrFoldr = new HashSet<VndrFoldr>();
            VndrOrdr = new HashSet<VndrOrdr>();
            VndrOrdrEmail = new HashSet<VndrOrdrEmail>();
            VndrOrdrEmailAtchmt = new HashSet<VndrOrdrEmailAtchmt>();
            VndrOrdrForm = new HashSet<VndrOrdrForm>();
            VndrTmplt = new HashSet<VndrTmplt>();
        }

        public byte RecStusId { get; set; }
        public string RecStusDes { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<AdEvent> AdEvent { get; set; }
        public virtual ICollection<Appt> Appt { get; set; }
        public virtual ICollection<CpeClli> CpeClli { get; set; }
        public virtual ICollection<Cpt> Cpt { get; set; }
        public virtual ICollection<CptDoc> CptDoc { get; set; }
        public virtual ICollection<EventAsnToUser> EventAsnToUser { get; set; }
        public virtual ICollection<EventFailActy> EventFailActy { get; set; }
        public virtual ICollection<EventRuleData> EventRuleData { get; set; }
        public virtual ICollection<EventSucssActy> EventSucssActy { get; set; }
        public virtual ICollection<FedlineEventTadpoleData> FedlineEventTadpoleData { get; set; }
        public virtual ICollection<H5Doc> H5Doc { get; set; }
        public virtual ICollection<H5Foldr> H5Foldr { get; set; }
        public virtual ICollection<IplOrdr> IplOrdr { get; set; }
        public virtual ICollection<LkActn> LkActn { get; set; }
        public virtual ICollection<LkActnType> LkActnType { get; set; }
        public virtual ICollection<LkActyLocale> LkActyLocale { get; set; }
        public virtual ICollection<LkAdrType> LkAdrType { get; set; }
        public virtual ICollection<LkApptType> LkApptType { get; set; }
        public virtual ICollection<LkCcdMissdReas> LkCcdMissdReas { get; set; }
        public virtual ICollection<LkCktChgType> LkCktChgType { get; set; }
        public virtual ICollection<LkCnfrcBrdg> LkCnfrcBrdg { get; set; }
        public virtual ICollection<LkCntctType> LkCntctType { get; set; }
        public virtual ICollection<LkCpePid> LkCpePid { get; set; }
        public virtual ICollection<LkCptCustType> LkCptCustType { get; set; }
        public virtual ICollection<LkCptHstMngdSrvc> LkCptHstMngdSrvc { get; set; }
        public virtual ICollection<LkCptMdsSuprtTier> LkCptMdsSuprtTier { get; set; }
        public virtual ICollection<LkCptMngdAuthPrd> LkCptMngdAuthPrd { get; set; }
        public virtual ICollection<LkCptMvsProdType> LkCptMvsProdType { get; set; }
        public virtual ICollection<LkCptPlnSrvcTier> LkCptPlnSrvcTier { get; set; }
        public virtual ICollection<LkCptPlnSrvcType> LkCptPlnSrvcType { get; set; }
        public virtual ICollection<LkCptPrimScndyTprt> LkCptPrimScndyTprt { get; set; }
        public virtual ICollection<LkCptPrimSite> LkCptPrimSite { get; set; }
        public virtual ICollection<LkCptPrvsnType> LkCptPrvsnType { get; set; }
        public virtual ICollection<LkCsgLvl> LkCsgLvl { get; set; }
        public virtual ICollection<LkCtry> LkCtry { get; set; }
        public virtual ICollection<LkCtryCty> LkCtryCty { get; set; }
        public virtual ICollection<LkCur> LkCur { get; set; }
        public virtual ICollection<LkCustCntrcLgth> LkCustCntrcLgth { get; set; }
        public virtual ICollection<LkDedctdCust> LkDedctdCust { get; set; }
        public virtual ICollection<LkDev> LkDev { get; set; }
        public virtual ICollection<LkDevManf> LkDevManf { get; set; }
        public virtual ICollection<LkDevModel> LkDevModel { get; set; }
        public virtual ICollection<LkDmstcClliCd> LkDmstcClliCd { get; set; }
        public virtual ICollection<LkEnhncSrvc> LkEnhncSrvc { get; set; }
        public virtual ICollection<LkEsclReas> LkEsclReas { get; set; }
        public virtual ICollection<LkEventRule> LkEventRule { get; set; }
        public virtual ICollection<LkEventStus> LkEventStus { get; set; }
        public virtual ICollection<LkEventType> LkEventType { get; set; }
        public virtual ICollection<LkFailActy> LkFailActy { get; set; }
        public virtual ICollection<LkFailReas> LkFailReas { get; set; }
        public virtual ICollection<LkFedMsg> LkFedMsg { get; set; }
        public virtual ICollection<LkFedRuleFail> LkFedRuleFail { get; set; }
        public virtual ICollection<LkFedlineEventFailCd> LkFedlineEventFailCd { get; set; }
        public virtual ICollection<LkFedlineOrdrType> LkFedlineOrdrType { get; set; }
        public virtual ICollection<LkFrgnCxr> LkFrgnCxr { get; set; }
        public virtual ICollection<LkGrp> LkGrp { get; set; }
        public virtual ICollection<LkIntlClliCd> LkIntlClliCd { get; set; }
        public virtual ICollection<LkIpVer> LkIpVer { get; set; }
        public virtual ICollection<LkMdsActyType> LkMdsActyType { get; set; }
        public virtual ICollection<LkMdsCpeDevXclusn> LkMdsCpeDevXclusn { get; set; }
        public virtual ICollection<LkMdsImplmtnNtvl> LkMdsImplmtnNtvl { get; set; }
        public virtual ICollection<LkMdsMacActy> LkMdsMacActy { get; set; }
        public virtual ICollection<LkMdsNtwkActyType> LkMdsNtwkActyType { get; set; }
        public virtual ICollection<LkMdsSrvcTier> LkMdsSrvcTier { get; set; }
        public virtual ICollection<LkMenu> LkMenu { get; set; }
        public virtual ICollection<LkMnsOffrgRoutr> LkMnsOffrgRoutr { get; set; }
        public virtual ICollection<LkMnsPrfmcRpt> LkMnsPrfmcRpt { get; set; }
        public virtual ICollection<LkMnsRoutgType> LkMnsRoutgType { get; set; }
        public virtual ICollection<LkMplsAccsBdwd> LkMplsAccsBdwd { get; set; }
        public virtual ICollection<LkMplsActyType> LkMplsActyType { get; set; }
        public virtual ICollection<LkMplsEventType> LkMplsEventType { get; set; }
        public virtual ICollection<LkMplsMgrtnType> LkMplsMgrtnType { get; set; }
        public virtual ICollection<LkMultiVrfReq> LkMultiVrfReq { get; set; }
        public virtual ICollection<LkNgvnProdType> LkNgvnProdType { get; set; }
        public virtual ICollection<LkNteType> LkNteType { get; set; }
        public virtual ICollection<LkOrdrCat> LkOrdrCat { get; set; }
        public virtual ICollection<LkPrfHrchy> LkPrfHrchy { get; set; }
        public virtual ICollection<LkQlfctn> LkQlfctn { get; set; }
        public virtual ICollection<LkScrdObjType> LkScrdObjType { get; set; }
        public virtual ICollection<LkSdePrdctType> LkSdePrdctType { get; set; }
        public virtual ICollection<LkSiptActyType> LkSiptActyType { get; set; }
        public virtual ICollection<LkSiptProdType> LkSiptProdType { get; set; }
        public virtual ICollection<LkSiptTollType> LkSiptTollType { get; set; }
        public virtual ICollection<LkSpclProj> LkSpclProj { get; set; }
        public virtual ICollection<LkSplkActyType> LkSplkActyType { get; set; }
        public virtual ICollection<LkSplkEventType> LkSplkEventType { get; set; }
        public virtual ICollection<LkSprintCpeNcr> LkSprintCpeNcr { get; set; }
        public virtual ICollection<LkSprintHldy> LkSprintHldy { get; set; }
        public virtual ICollection<LkSrvcAssrnSiteSupp> LkSrvcAssrnSiteSupp { get; set; }
        public virtual ICollection<LkStdiReas> LkStdiReas { get; set; }
        public virtual ICollection<LkStus> LkStus { get; set; }
        public virtual ICollection<LkSucssActy> LkSucssActy { get; set; }
        public virtual ICollection<LkSysCfg> LkSysCfg { get; set; }
        public virtual ICollection<LkTelco> LkTelco { get; set; }
        public virtual ICollection<LkUcaaSActyType> LkUcaaSActyType { get; set; }
        public virtual ICollection<LkUcaaSBillActy> LkUcaaSBillActy { get; set; }
        public virtual ICollection<LkUcaaSPlanType> LkUcaaSPlanType { get; set; }
        public virtual ICollection<LkUcaaSProdType> LkUcaaSProdType { get; set; }
        public virtual ICollection<LkUsStt> LkUsStt { get; set; }
        public virtual ICollection<LkUser> LkUser { get; set; }
        public virtual ICollection<LkUsrPrf> LkUsrPrf { get; set; }
        public virtual ICollection<LkUsrPrfMenu> LkUsrPrfMenu { get; set; }
        public virtual ICollection<LkVasType> LkVasType { get; set; }
        public virtual ICollection<LkVndr> LkVndr { get; set; }
        public virtual ICollection<LkVndrEmailLang> LkVndrEmailLang { get; set; }
        public virtual ICollection<LkVndrScm> LkVndrScm { get; set; }
        public virtual ICollection<LkVpnPltfrmType> LkVpnPltfrmType { get; set; }
        public virtual ICollection<LkWhlslPtnr> LkWhlslPtnr { get; set; }
        public virtual ICollection<LkWrkflwStus> LkWrkflwStus { get; set; }
        public virtual ICollection<LkWrldHldy> LkWrldHldy { get; set; }
        public virtual ICollection<LkXnciRgn> LkXnciRgn { get; set; }
        public virtual ICollection<MapUsrPrf> MapUsrPrf { get; set; }
        public virtual ICollection<MdsEvent> MdsEvent { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNew { get; set; }
        public virtual ICollection<MdsFastTrkType> MdsFastTrkType { get; set; }
        public virtual ICollection<MplsEvent> MplsEvent { get; set; }
        public virtual ICollection<MplsEventAccsTag> MplsEventAccsTag { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEvent { get; set; }
        public virtual ICollection<NrmCkt> NrmCkt { get; set; }
        public virtual ICollection<NrmVndr> NrmVndr { get; set; }
        public virtual ICollection<Ordr> Ordr { get; set; }
        public virtual ICollection<OrdrAdr> OrdrAdr { get; set; }
        public virtual ICollection<OrdrCntct> OrdrCntct { get; set; }
        public virtual ICollection<OrdrNte> OrdrNte { get; set; }
        public virtual ICollection<RedsgnDoc> RedsgnDoc { get; set; }
        public virtual ICollection<SdeOpptnty> SdeOpptnty { get; set; }
        public virtual ICollection<SdeOpptntyDoc> SdeOpptntyDoc { get; set; }
        public virtual ICollection<SdeOpptntyNte> SdeOpptntyNte { get; set; }
        public virtual ICollection<SdeOpptntyPrdct> SdeOpptntyPrdct { get; set; }
        public virtual ICollection<SiptEvent> SiptEvent { get; set; }
        public virtual ICollection<SiptEventDoc> SiptEventDoc { get; set; }
        public virtual ICollection<SplkEvent> SplkEvent { get; set; }
        public virtual ICollection<SrcCurFile> SrcCurFile { get; set; }
        public virtual ICollection<TrptOrdr> TrptOrdr { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEvent { get; set; }
        public virtual ICollection<UserCsgLvl> UserCsgLvl { get; set; }
        public virtual ICollection<UserH5Wfm> UserH5Wfm { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
        public virtual ICollection<VndrFoldr> VndrFoldr { get; set; }
        public virtual ICollection<VndrOrdr> VndrOrdr { get; set; }
        public virtual ICollection<VndrOrdrEmail> VndrOrdrEmail { get; set; }
        public virtual ICollection<VndrOrdrEmailAtchmt> VndrOrdrEmailAtchmt { get; set; }
        public virtual ICollection<VndrOrdrForm> VndrOrdrForm { get; set; }
        public virtual ICollection<VndrTmplt> VndrTmplt { get; set; }
    }
}
