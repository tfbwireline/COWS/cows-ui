﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkAsrType
    {
        public LkAsrType()
        {
            Asr = new HashSet<Asr>();
        }

        public int AsrTypeId { get; set; }
        public string AsrTypeNme { get; set; }
        public string AsrTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public int? CreatByUserId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual ICollection<Asr> Asr { get; set; }
    }
}
