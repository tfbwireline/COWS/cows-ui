﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SiptEventDoc
    {
        public int SiptEventDocId { get; set; }
        public int EventId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual SiptEvent Event { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
