﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFedlineOrdrType
    {
        public LkFedlineOrdrType()
        {
            FedlineEventTadpoleDataFrbOrdrTypeCdNavigation = new HashSet<FedlineEventTadpoleData>();
            FedlineEventTadpoleDataOrdrTypeCdNavigation = new HashSet<FedlineEventTadpoleData>();
            LkEventRule = new HashSet<LkEventRule>();
        }

        public string OrdrTypeCd { get; set; }
        public string OrdrTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int? CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public string PrntOrdrTypeDes { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<FedlineEventTadpoleData> FedlineEventTadpoleDataFrbOrdrTypeCdNavigation { get; set; }
        public virtual ICollection<FedlineEventTadpoleData> FedlineEventTadpoleDataOrdrTypeCdNavigation { get; set; }
        public virtual ICollection<LkEventRule> LkEventRule { get; set; }
    }
}
