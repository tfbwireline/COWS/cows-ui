﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptDoc
    {
        public int CptDocId { get; set; }
        public int CptId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public string CmntTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
