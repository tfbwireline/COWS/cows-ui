﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkStdiReas
    {
        public LkStdiReas()
        {
            OrdrStdiHist = new HashSet<OrdrStdiHist>();
            TrptOrdr = new HashSet<TrptOrdr>();
        }

        public byte StdiReasId { get; set; }
        public string StdiReasDes { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int? CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<OrdrStdiHist> OrdrStdiHist { get; set; }
        public virtual ICollection<TrptOrdr> TrptOrdr { get; set; }
    }
}
