﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineEventMsg
    {
        public int FedlineEventMsgId { get; set; }
        public int FedlineEventId { get; set; }
        public string FedlineEventMsgStusNme { get; set; }
        public string RspnDes { get; set; }
        public short StusId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? SentDt { get; set; }

        public virtual FedlineEventTadpoleData FedlineEvent { get; set; }
        public virtual LkStus Stus { get; set; }
    }
}
