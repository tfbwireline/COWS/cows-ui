﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkCsgLvl
    {
        public LkCsgLvl()
        {
            H5Foldr = new HashSet<H5Foldr>();
            OdieReq = new HashSet<OdieReq>();
            Ordr = new HashSet<Ordr>();
            Redsgn = new HashSet<Redsgn>();
            RedsgnCustBypass = new HashSet<RedsgnCustBypass>();
            UserCsgLvl = new HashSet<UserCsgLvl>();
        }

        public byte CsgLvlId { get; set; }
        public string CsgLvlCd { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<H5Foldr> H5Foldr { get; set; }
        public virtual ICollection<OdieReq> OdieReq { get; set; }
        public virtual ICollection<Ordr> Ordr { get; set; }
        public virtual ICollection<Redsgn> Redsgn { get; set; }
        public virtual ICollection<RedsgnCustBypass> RedsgnCustBypass { get; set; }
        public virtual ICollection<UserCsgLvl> UserCsgLvl { get; set; }
    }
}
