﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnMplsActyType
    {
        public byte MplsActyTypeId { get; set; }
        public int QlfctnId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkMplsActyType MplsActyType { get; set; }
        public virtual LkQlfctn Qlfctn { get; set; }
    }
}
