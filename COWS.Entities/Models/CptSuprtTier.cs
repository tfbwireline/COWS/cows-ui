﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptSuprtTier
    {
        public int CptSuprtTierId { get; set; }
        public int CptId { get; set; }
        public short CptMdsSuprtTierId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptMdsSuprtTier CptMdsSuprtTier { get; set; }
    }
}
