﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class NidActy
    {
        public NidActy()
        {
            IpActy = new HashSet<IpActy>();
        }

        public int NidActyId { get; set; }
        public string NidSerialNbr { get; set; }
        public string NidHostNme { get; set; }
        public int? FsaCpeLineItemId { get; set; }
        public int? EventId { get; set; }
        public string M5OrdrNbr { get; set; }
        public string DdAprvlNbr { get; set; }
        public string H6 { get; set; }
        public short RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual Event Event { get; set; }
        public virtual FsaOrdrCpeLineItem FsaCpeLineItem { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkStus RecStus { get; set; }
        public virtual ICollection<IpActy> IpActy { get; set; }
    }
}
