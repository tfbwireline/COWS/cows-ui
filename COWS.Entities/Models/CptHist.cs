﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptHist
    {
        public int CptHistId { get; set; }
        public int CptId { get; set; }
        public byte ActnId { get; set; }
        public string CmntTxt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkActn Actn { get; set; }
        public virtual Cpt Cpt { get; set; }
        public virtual LkUser CreatByUser { get; set; }
    }
}
