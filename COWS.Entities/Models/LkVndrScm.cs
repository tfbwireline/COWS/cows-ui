﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkVndrScm
    {
        public int LkVndrScmId { get; set; }
        public string VndrCd { get; set; }
        public string VndrNme { get; set; }
        public string VndrFullNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
