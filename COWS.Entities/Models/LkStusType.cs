﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkStusType
    {
        public LkStusType()
        {
            LkStus = new HashSet<LkStus>();
        }

        public string StusTypeId { get; set; }
        public string StusTypeDes { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual ICollection<LkStus> LkStus { get; set; }
    }
}
