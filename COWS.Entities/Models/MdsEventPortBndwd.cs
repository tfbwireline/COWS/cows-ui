﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventPortBndwd
    {
        public int PortBndwdId { get; set; }
        public int EventId { get; set; }
        public string OdieDevNme { get; set; }
        public string M5OrdrNbr { get; set; }
        public string M5OrdrCmpntId { get; set; }
        public string Nua { get; set; }
        public string PortBndwd { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual Event Event { get; set; }
    }
}
