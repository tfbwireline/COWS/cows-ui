﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkSiptTollType
    {
        public LkSiptTollType()
        {
            SiptEventTollType = new HashSet<SiptEventTollType>();
        }

        public byte SiptTollTypeId { get; set; }
        public string SiptTollTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<SiptEventTollType> SiptEventTollType { get; set; }
    }
}
