﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventOdieDev
    {
        public int MdsEventOdieDevId { get; set; }
        public int EventId { get; set; }
        public string RdsnNbr { get; set; }
        public DateTime RdsnExpDt { get; set; }
        public string OdieDevNme { get; set; }
        public short DevModelId { get; set; }
        public short ManfId { get; set; }
        public string FrwlProdCd { get; set; }
        public string IntlCtryCd { get; set; }
        public string PhnNbr { get; set; }
        public string FastTrkCd { get; set; }
        public string WoobCd { get; set; }
        public bool? OptoutCd { get; set; }
        public byte? SrvcAssrnSiteSuppId { get; set; }
        public DateTime CreatDt { get; set; }
        public bool SysCd { get; set; }
        public string ScCd { get; set; }

        public virtual LkDevModel DevModel { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkDevManf Manf { get; set; }
        public virtual LkSrvcAssrnSiteSupp SrvcAssrnSiteSupp { get; set; }
    }
}
