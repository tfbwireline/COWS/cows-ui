﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkMplsEventType
    {
        public LkMplsEventType()
        {
            MdsEvent = new HashSet<MdsEvent>();
            MplsEvent = new HashSet<MplsEvent>();
        }

        public byte MplsEventTypeId { get; set; }
        public string MplsEventTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short EventTypeId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkEventType EventType { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<MdsEvent> MdsEvent { get; set; }
        public virtual ICollection<MplsEvent> MplsEvent { get; set; }
    }
}
