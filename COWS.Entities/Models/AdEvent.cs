﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class AdEvent
    {
        public AdEvent()
        {
            AdEventAccsTag = new HashSet<AdEventAccsTag>();
        }

        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public string Ftn { get; set; }
        public string CharsId { get; set; }
        public string H1 { get; set; }
        public string H6 { get; set; }
        public short EnhncSrvcId { get; set; }
        public int ReqorUserId { get; set; }
        public int SalsUserId { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public string DesCmntTxt { get; set; }
        public bool CpeAtndCd { get; set; }
        public string DocLinkTxt { get; set; }
        public bool EsclCd { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime StrtTmst { get; set; }
        public short ExtraDrtnTmeAmt { get; set; }
        public DateTime EndTmst { get; set; }
        public byte WrkflwStusId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte? IpVerId { get; set; }
        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public short EventDrtnInMinQty { get; set; }
        public int? SowsEventId { get; set; }
        public string CustNme { get; set; }
        public string CustCntctNme { get; set; }
        public string CustCntctPhnNbr { get; set; }
        public string CustEmailAdr { get; set; }
        public string CustCntctCellPhnNbr { get; set; }
        public string CustCntctPgrNbr { get; set; }
        public string CustCntctPgrPinNbr { get; set; }
        public string EventTitleTxt { get; set; }
        public string EventDes { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkEnhncSrvc EnhncSrvc { get; set; }
        public virtual LkEsclReas EsclReas { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkEventStus EventStus { get; set; }
        public virtual LkIpVer IpVer { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUser ReqorUser { get; set; }
        public virtual LkUser SalsUser { get; set; }
        public virtual LkWrkflwStus WrkflwStus { get; set; }
        public virtual ICollection<AdEventAccsTag> AdEventAccsTag { get; set; }
    }
}
