﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFailReas
    {
        public LkFailReas()
        {
            EventHist = new HashSet<EventHist>();
            LkEventRule = new HashSet<LkEventRule>();
            MdsEvent = new HashSet<MdsEvent>();
            MdsEventNew = new HashSet<MdsEventNew>();
            UcaaSEvent = new HashSet<UcaaSEvent>();
        }

        public short FailReasId { get; set; }
        public string FailReasDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public bool MdsFastTrkCd { get; set; }
        public byte? NtwkOnlyCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<EventHist> EventHist { get; set; }
        public virtual ICollection<LkEventRule> LkEventRule { get; set; }
        public virtual ICollection<MdsEvent> MdsEvent { get; set; }
        public virtual ICollection<MdsEventNew> MdsEventNew { get; set; }
        public virtual ICollection<UcaaSEvent> UcaaSEvent { get; set; }
    }
}
