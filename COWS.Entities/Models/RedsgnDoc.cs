﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnDoc
    {
        public int RedsgnDocId { get; set; }
        public int RedsgnId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime? CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte RecStusId { get; set; }
        public short? RedsgnDevStusId { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual Redsgn Redsgn { get; set; }
    }
}
