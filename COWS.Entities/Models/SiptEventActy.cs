﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SiptEventActy
    {
        public int EventId { get; set; }
        public short SiptActyTypeId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkSiptActyType SiptActyType { get; set; }
    }
}
