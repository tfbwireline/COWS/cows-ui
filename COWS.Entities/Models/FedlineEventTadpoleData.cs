﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineEventTadpoleData
    {
        public FedlineEventTadpoleData()
        {
            FedlineEventAdr = new HashSet<FedlineEventAdr>();
            FedlineEventCfgrnData = new HashSet<FedlineEventCfgrnData>();
            FedlineEventCntct = new HashSet<FedlineEventCntct>();
            FedlineEventMsg = new HashSet<FedlineEventMsg>();
        }

        public int FedlineEventId { get; set; }
        public int EventId { get; set; }
        public int FrbReqId { get; set; }
        public int SeqNbr { get; set; }
        public string OrdrTypeCd { get; set; }
        public string DsgnTypeCd { get; set; }
        public DateTime? StrtTme { get; set; }
        public DateTime? EndTme { get; set; }
        public string DevNme { get; set; }
        public string DevSerialNbr { get; set; }
        public string DevModelNbr { get; set; }
        public string DevVndrNme { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public string FrbOrdrTypeCd { get; set; }
        public bool IsMoveCd { get; set; }
        public string XstDevNme { get; set; }
        public bool IsExpediteCd { get; set; }
        public DateTime? ReqRecvDt { get; set; }
        public string OrgId { get; set; }
        public string FrbInstlTypeNme { get; set; }
        public string OrdrSubTypeNme { get; set; }
        public string ReplmtReasNme { get; set; }
        public int? OrigFrbReqId { get; set; }
        public string OrigDevId { get; set; }
        public string OrigDevSerialNbr { get; set; }
        public string OrigModel { get; set; }
        public byte[] CustNme { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkFedlineOrdrType FrbOrdrTypeCdNavigation { get; set; }
        public virtual LkFedlineOrdrType OrdrTypeCdNavigation { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<FedlineEventAdr> FedlineEventAdr { get; set; }
        public virtual ICollection<FedlineEventCfgrnData> FedlineEventCfgrnData { get; set; }
        public virtual ICollection<FedlineEventCntct> FedlineEventCntct { get; set; }
        public virtual ICollection<FedlineEventMsg> FedlineEventMsg { get; set; }
    }
}
