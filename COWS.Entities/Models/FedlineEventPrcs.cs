﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class FedlineEventPrcs
    {
        public int FedlineEventPrcsId { get; set; }
        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public DateTime? StrtTme { get; set; }
        public DateTime? EndTme { get; set; }
        public DateTime? CreatDt { get; set; }
        public byte? EventEndStusId { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkEventStus EventEndStus { get; set; }
        public virtual LkEventStus EventStus { get; set; }
    }
}
