﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class RedsgnEmailNtfctn
    {
        public int RedsgnEmailNtfctnId { get; set; }
        public int RedsgnId { get; set; }
        public string UserIdntfctnEmail { get; set; }
        public bool RecStusId { get; set; }
        public int CretdUserId { get; set; }
        public DateTime CretdDt { get; set; }

        public virtual LkUser CretdUser { get; set; }
        public virtual Redsgn Redsgn { get; set; }
    }
}
