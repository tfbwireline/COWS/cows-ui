﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkWrldHldy
    {
        public int WrldHldyId { get; set; }
        public DateTime WrldHldyDt { get; set; }
        public string WrldHldyDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CtyNme { get; set; }
        public string CtryCd { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkCtryCty Ct { get; set; }
        public virtual LkCtry CtryCdNavigation { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
