﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkNteType
    {
        public LkNteType()
        {
            OrdrNte = new HashSet<OrdrNte>();
        }

        public byte NteTypeId { get; set; }
        public string NteTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<OrdrNte> OrdrNte { get; set; }
    }
}
