﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class ActTask
    {
        public int ActTaskId { get; set; }
        public int OrdrId { get; set; }
        public short TaskId { get; set; }
        public short StusId { get; set; }
        public int WgProfId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual Ordr Ordr { get; set; }
        public virtual LkStus Stus { get; set; }
        public virtual LkTask Task { get; set; }
    }
}
