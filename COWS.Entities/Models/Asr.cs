﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class Asr
    {
        public int AsrId { get; set; }
        public int OrdrId { get; set; }
        public int AsrTypeId { get; set; }
        public string IpNodeTxt { get; set; }
        public string AccsBdwdDes { get; set; }
        public string AccsCtyNmeSiteCd { get; set; }
        public bool? TrnspntCd { get; set; }
        public string EntrncAsmtTxt { get; set; }
        public string MstrFtnCd { get; set; }
        public string DlciDes { get; set; }
        public bool? H1MatchMstrVasCd { get; set; }
        public string AsrNotesTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public short StusId { get; set; }
        public int CreatByUserId { get; set; }
        public string PtnrCxrCd { get; set; }
        public int? EmailReqId { get; set; }
        public string LecNniNbr { get; set; }
        public string IntlDomEmailCd { get; set; }

        public virtual LkAsrType AsrType { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual EmailReq EmailReq { get; set; }
        public virtual Ordr Ordr { get; set; }
        public virtual LkStus Stus { get; set; }
    }
}
