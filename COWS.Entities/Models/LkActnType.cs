﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkActnType
    {
        public LkActnType()
        {
            LkActn = new HashSet<LkActn>();
        }

        public short ActnTypeId { get; set; }
        public string ActnType { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<LkActn> LkActn { get; set; }
    }
}
