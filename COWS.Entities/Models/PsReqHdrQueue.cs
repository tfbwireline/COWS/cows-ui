﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class PsReqHdrQueue
    {
        public int ReqHdrId { get; set; }
        public int OrdrId { get; set; }
        public string ReqstnNbr { get; set; }
        public string CustElid { get; set; }
        public string DlvyClli { get; set; }
        public string DlvyNme { get; set; }
        public string DlvyAddr1 { get; set; }
        public string DlvyAddr2 { get; set; }
        public string DlvyAddr3 { get; set; }
        public string DlvyAddr4 { get; set; }
        public string DlvyCty { get; set; }
        public string DlvyCnty { get; set; }
        public string DlvySt { get; set; }
        public string DlvyZip { get; set; }
        public string DlvyPhnNbr { get; set; }
        public string InstClli { get; set; }
        public string MrkPkg { get; set; }
        public DateTime? ReqstnDt { get; set; }
        public string RefNbr { get; set; }
        public string ShipCmmts { get; set; }
        public string RfqIndctr { get; set; }
        public byte? RecStusId { get; set; }
        public DateTime? CreatDt { get; set; }
        public DateTime? SentDt { get; set; }
        public string DlvyBldg { get; set; }
        public string DlvyFlr { get; set; }
        public string DlvyRm { get; set; }
        public string CsgLvl { get; set; }
        public int? BatchSeq { get; set; }
    }
}
