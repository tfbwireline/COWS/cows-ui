﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkOrdrStus
    {
        public LkOrdrStus()
        {
            Ordr = new HashSet<Ordr>();
        }

        public byte OrdrStusId { get; set; }
        public string OrdrStusDes { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<Ordr> Ordr { get; set; }
    }
}
