﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptSrvcType
    {
        public int CptSrvcTypeId { get; set; }
        public int CptId { get; set; }
        public short CptPlnSrvcTypeId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptPlnSrvcType CptPlnSrvcType { get; set; }
    }
}
