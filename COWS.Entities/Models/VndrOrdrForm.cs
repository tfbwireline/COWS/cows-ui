﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class VndrOrdrForm
    {
        public VndrOrdrForm()
        {
            InverseTmplt = new HashSet<VndrOrdrForm>();
        }

        public int VndrOrdrFormId { get; set; }
        public int VndrOrdrId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public bool TmpltCd { get; set; }
        public int? TmpltId { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual VndrOrdrForm Tmplt { get; set; }
        public virtual VndrOrdr VndrOrdr { get; set; }
        public virtual ICollection<VndrOrdrForm> InverseTmplt { get; set; }
    }
}
