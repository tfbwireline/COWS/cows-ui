﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkSplkActyType
    {
        public LkSplkActyType()
        {
            SplkEvent = new HashSet<SplkEvent>();
        }

        public byte SplkActyTypeId { get; set; }
        public string SplkActyTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<SplkEvent> SplkEvent { get; set; }
    }
}
