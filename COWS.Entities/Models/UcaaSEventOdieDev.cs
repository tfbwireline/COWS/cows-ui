﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class UcaaSEventOdieDev
    {
        public int UcaaSEventOdieDevId { get; set; }
        public int EventId { get; set; }
        public string RdsnNbr { get; set; }
        public DateTime RdsnExpDt { get; set; }
        public string OdieDevNme { get; set; }
        public short DevModelId { get; set; }
        public short ManfId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkDevModel DevModel { get; set; }
        public virtual Event Event { get; set; }
        public virtual LkDevManf Manf { get; set; }
    }
}
