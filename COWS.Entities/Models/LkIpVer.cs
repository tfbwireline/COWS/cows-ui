﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkIpVer
    {
        public LkIpVer()
        {
            AdEvent = new HashSet<AdEvent>();
            MplsEvent = new HashSet<MplsEvent>();
            NgvnEvent = new HashSet<NgvnEvent>();
            QlfctnIpVer = new HashSet<QlfctnIpVer>();
            SplkEvent = new HashSet<SplkEvent>();
        }

        public byte IpVerId { get; set; }
        public string IpVerNme { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
        public virtual ICollection<AdEvent> AdEvent { get; set; }
        public virtual ICollection<MplsEvent> MplsEvent { get; set; }
        public virtual ICollection<NgvnEvent> NgvnEvent { get; set; }
        public virtual ICollection<QlfctnIpVer> QlfctnIpVer { get; set; }
        public virtual ICollection<SplkEvent> SplkEvent { get; set; }
    }
}
