﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class MdsEventMacActy
    {
        public int EventId { get; set; }
        public int MdsMacActyId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
        public virtual LkMdsMacActy MdsMacActy { get; set; }
    }
}
