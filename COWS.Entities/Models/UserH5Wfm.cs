﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class UserH5Wfm
    {
        public int UserH5Id { get; set; }
        public int UserId { get; set; }
        public int CustId { get; set; }
        public string SegNme { get; set; }
        public string MnsCd { get; set; }
        public string IsipCd { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }

        public virtual LkRecStus RecStus { get; set; }
        public virtual LkUser User { get; set; }
    }
}
