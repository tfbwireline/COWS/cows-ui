﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptMvsProd
    {
        public int CptMvsProdId { get; set; }
        public int CptId { get; set; }
        public short CptMvsProdTypeId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkCptMvsProdType CptMvsProdType { get; set; }
    }
}
