﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkFsaProdType
    {
        public LkFsaProdType()
        {
            FsaOrdr = new HashSet<FsaOrdr>();
            LkProdType = new HashSet<LkProdType>();
            PltfrmMapng = new HashSet<PltfrmMapng>();
        }

        public string FsaProdTypeCd { get; set; }
        public string FsaProdTypeDes { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<FsaOrdr> FsaOrdr { get; set; }
        public virtual ICollection<LkProdType> LkProdType { get; set; }
        public virtual ICollection<PltfrmMapng> PltfrmMapng { get; set; }
    }
}
