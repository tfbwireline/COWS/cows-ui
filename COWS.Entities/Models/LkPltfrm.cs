﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkPltfrm
    {
        public LkPltfrm()
        {
            LkXnciMsSla = new HashSet<LkXnciMsSla>();
            Ordr = new HashSet<Ordr>();
            PltfrmMapng = new HashSet<PltfrmMapng>();
            UserWfm = new HashSet<UserWfm>();
        }

        public string PltfrmCd { get; set; }
        public string PltfrmNme { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual ICollection<LkXnciMsSla> LkXnciMsSla { get; set; }
        public virtual ICollection<Ordr> Ordr { get; set; }
        public virtual ICollection<PltfrmMapng> PltfrmMapng { get; set; }
        public virtual ICollection<UserWfm> UserWfm { get; set; }
    }
}
