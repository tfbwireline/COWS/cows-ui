﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CpeClli
    {
        public int CpeClliId { get; set; }
        public string ClliId { get; set; }
        public string SiteNme { get; set; }
        public string AddrTxt { get; set; }
        public string SttId { get; set; }
        public string CityNme { get; set; }
        public string ZipCd { get; set; }
        public string CountyNme { get; set; }
        public string CmntyNme { get; set; }
        public string Flr { get; set; }
        public string Room { get; set; }
        public string LttdeCoord { get; set; }
        public string LngdeCoord { get; set; }
        public string SiteOwnr { get; set; }
        public bool SprntOwndCd { get; set; }
        public string SitePurpose { get; set; }
        public string SitePhn { get; set; }
        public string CmntTxt { get; set; }
        public string EmailCc { get; set; }
        public DateTime? PsoftUpdDt { get; set; }
        public DateTime? SstatUpdDt { get; set; }
        public bool PsoftSendCd { get; set; }
        public byte RecStusId { get; set; }
        public short CpeClliStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkStus CpeClliStus { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
