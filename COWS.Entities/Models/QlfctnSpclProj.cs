﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class QlfctnSpclProj
    {
        public int QlfctnId { get; set; }
        public short SpclProjId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkQlfctn Qlfctn { get; set; }
        public virtual LkSpclProj SpclProj { get; set; }
    }
}
