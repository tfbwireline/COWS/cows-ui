﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class LkXnciMs
    {
        public LkXnciMs()
        {
            LkXnciMsSlaMs = new HashSet<LkXnciMsSla>();
            LkXnciMsSlaToMs = new HashSet<LkXnciMsSla>();
        }

        public short MsId { get; set; }
        public string MsDes { get; set; }
        public short? MsTaskId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkTask MsTask { get; set; }
        public virtual ICollection<LkXnciMsSla> LkXnciMsSlaMs { get; set; }
        public virtual ICollection<LkXnciMsSla> LkXnciMsSlaToMs { get; set; }
    }
}
