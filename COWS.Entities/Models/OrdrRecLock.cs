﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class OrdrRecLock
    {
        public int OrdrId { get; set; }
        public DateTime StrtRecLockTmst { get; set; }
        public int LockByUserId { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkUser LockByUser { get; set; }
        public virtual Ordr Ordr { get; set; }
    }
}
