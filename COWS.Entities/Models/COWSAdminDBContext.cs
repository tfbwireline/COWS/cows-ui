﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using COWS.Entities.QueryModels;
using Microsoft.Extensions.Configuration;

namespace COWS.Entities.Models
{
    public partial class COWSAdminDBContext : DbContext
    {
        private IConfiguration _configuration;
        private int _commandTimeout;

        public int commandTimeout
        {
            get { return _commandTimeout; }
        }

        public COWSAdminDBContext(IConfiguration configuration)
        {
            _configuration = configuration;
            _commandTimeout = (_configuration.GetSection("ConnectionStrings:CommandTimeout").Exists()) ? Int32.Parse(_configuration.GetSection("ConnectionStrings:CommandTimeout").Value) : 300;
            Database.SetCommandTimeout(_commandTimeout);
        }

        public COWSAdminDBContext(DbContextOptions<COWSAdminDBContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
            _commandTimeout = (_configuration.GetSection("ConnectionStrings:CommandTimeout").Exists()) ? Int32.Parse(_configuration.GetSection("ConnectionStrings:CommandTimeout").Value) : 300;
            Database.SetCommandTimeout(_commandTimeout);
        }

        public virtual DbSet<ActTask> ActTask { get; set; }
        public virtual DbSet<AdEvent> AdEvent { get; set; }
        public virtual DbSet<AdEventAccsTag> AdEventAccsTag { get; set; }
        public virtual DbSet<Appt> Appt { get; set; }
        public virtual DbSet<ApptRcurncData> ApptRcurncData { get; set; }
        public virtual DbSet<Asr> Asr { get; set; }
        public virtual DbSet<BillDsptch> BillDsptch { get; set; }
        public virtual DbSet<CcdHist> CcdHist { get; set; }
        public virtual DbSet<CcdHistReas> CcdHistReas { get; set; }
        public virtual DbSet<Ckt> Ckt { get; set; }
        public virtual DbSet<CktCost> CktCost { get; set; }
        public virtual DbSet<CktMs> CktMs { get; set; }
        public virtual DbSet<CntctDetl> CntctDetl { get; set; }
        public virtual DbSet<CpeClli> CpeClli { get; set; }
        public virtual DbSet<Cpt> Cpt { get; set; }
        public virtual DbSet<CptDoc> CptDoc { get; set; }
        public virtual DbSet<CptHist> CptHist { get; set; }
        public virtual DbSet<CptMngdAuth> CptMngdAuth { get; set; }
        public virtual DbSet<CptMngdSrvc> CptMngdSrvc { get; set; }
        public virtual DbSet<CptMvsProd> CptMvsProd { get; set; }
        public virtual DbSet<CptOdieRspn> CptOdieRspn { get; set; }
        public virtual DbSet<CptPrimSite> CptPrimSite { get; set; }
        public virtual DbSet<CptPrimWan> CptPrimWan { get; set; }
        public virtual DbSet<CptPrvsn> CptPrvsn { get; set; }
        public virtual DbSet<CptRecLock> CptRecLock { get; set; }
        public virtual DbSet<CptRedsgn> CptRedsgn { get; set; }
        public virtual DbSet<CptRltdInfo> CptRltdInfo { get; set; }
        public virtual DbSet<CptScndyTprt> CptScndyTprt { get; set; }
        public virtual DbSet<CptSrvcType> CptSrvcType { get; set; }
        public virtual DbSet<CptSuprtTier> CptSuprtTier { get; set; }
        public virtual DbSet<CptUserAsmt> CptUserAsmt { get; set; }
        public virtual DbSet<CustScrdData> CustScrdData { get; set; }
        public virtual DbSet<DsplVw> DsplVw { get; set; }
        public virtual DbSet<DsplVwCol> DsplVwCol { get; set; }
        public virtual DbSet<EmailReq> EmailReq { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<EventAsnToUser> EventAsnToUser { get; set; }
        public virtual DbSet<EventCpeDev> EventCpeDev { get; set; }
        public virtual DbSet<EventDevCmplt> EventDevCmplt { get; set; }
        public virtual DbSet<EventDevSrvcMgmt> EventDevSrvcMgmt { get; set; }
        public virtual DbSet<EventDiscoDev> EventDiscoDev { get; set; }
        public virtual DbSet<EventFailActy> EventFailActy { get; set; }
        public virtual DbSet<EventHist> EventHist { get; set; }
        public virtual DbSet<EventNvldTmeSlot> EventNvldTmeSlot { get; set; }
        public virtual DbSet<EventRecLock> EventRecLock { get; set; }
        public virtual DbSet<EventRuleData> EventRuleData { get; set; }
        public virtual DbSet<EventSucssActy> EventSucssActy { get; set; }
        public virtual DbSet<EventTypeRsrcAvlblty> EventTypeRsrcAvlblty { get; set; }
        public virtual DbSet<FedlineEventAdr> FedlineEventAdr { get; set; }
        public virtual DbSet<FedlineEventCfgrnData> FedlineEventCfgrnData { get; set; }
        public virtual DbSet<FedlineEventCntct> FedlineEventCntct { get; set; }
        public virtual DbSet<FedlineEventMsg> FedlineEventMsg { get; set; }
        public virtual DbSet<FedlineEventPrcs> FedlineEventPrcs { get; set; }
        public virtual DbSet<FedlineEventTadpoleData> FedlineEventTadpoleData { get; set; }
        public virtual DbSet<FedlineEventTadpoleXml> FedlineEventTadpoleXml { get; set; }
        public virtual DbSet<FedlineEventUserData> FedlineEventUserData { get; set; }
        public virtual DbSet<FedlineReqActHist> FedlineReqActHist { get; set; }
        public virtual DbSet<FsaOrdr> FsaOrdr { get; set; }
        public virtual DbSet<FsaOrdrCpeLineItem> FsaOrdrCpeLineItem { get; set; }
        public virtual DbSet<FsaOrdrCsc> FsaOrdrCsc { get; set; }
        public virtual DbSet<FsaOrdrCust> FsaOrdrCust { get; set; }
        public virtual DbSet<FsaOrdrGomXnci> FsaOrdrGomXnci { get; set; }
        public virtual DbSet<H5Doc> H5Doc { get; set; }
        public virtual DbSet<H5Foldr> H5Foldr { get; set; }
        public virtual DbSet<IpActy> IpActy { get; set; }
        public virtual DbSet<IpMstr> IpMstr { get; set; }
        public virtual DbSet<IplOrdr> IplOrdr { get; set; }
        public virtual DbSet<LkAccsCtySite> LkAccsCtySite { get; set; }
        public virtual DbSet<LkAccsMb> LkAccsMb { get; set; }
        public virtual DbSet<LkActn> LkActn { get; set; }
        public virtual DbSet<LkActnType> LkActnType { get; set; }
        public virtual DbSet<LkActyLocale> LkActyLocale { get; set; }
        public virtual DbSet<LkAdrType> LkAdrType { get; set; }
        public virtual DbSet<LkApptType> LkApptType { get; set; }
        public virtual DbSet<LkAsrType> LkAsrType { get; set; }
        public virtual DbSet<LkCcdMissdReas> LkCcdMissdReas { get; set; }
        public virtual DbSet<LkCktChgType> LkCktChgType { get; set; }
        public virtual DbSet<LkCnfrcBrdg> LkCnfrcBrdg { get; set; }
        public virtual DbSet<LkCntctType> LkCntctType { get; set; }
        public virtual DbSet<LkCowsAppCfg> LkCowsAppCfg { get; set; }
        public virtual DbSet<LkCpeJprdy> LkCpeJprdy { get; set; }
        public virtual DbSet<LkCpePid> LkCpePid { get; set; }
        public virtual DbSet<LkCptCustType> LkCptCustType { get; set; }
        public virtual DbSet<LkCptHstMngdSrvc> LkCptHstMngdSrvc { get; set; }
        public virtual DbSet<LkCptMdsSuprtTier> LkCptMdsSuprtTier { get; set; }
        public virtual DbSet<LkCptMngdAuthPrd> LkCptMngdAuthPrd { get; set; }
        public virtual DbSet<LkCptMvsProdType> LkCptMvsProdType { get; set; }
        public virtual DbSet<LkCptPlnSrvcTier> LkCptPlnSrvcTier { get; set; }
        public virtual DbSet<LkCptPlnSrvcType> LkCptPlnSrvcType { get; set; }
        public virtual DbSet<LkCptPrimScndyTprt> LkCptPrimScndyTprt { get; set; }
        public virtual DbSet<LkCptPrimSite> LkCptPrimSite { get; set; }
        public virtual DbSet<LkCptPrvsnType> LkCptPrvsnType { get; set; }
        public virtual DbSet<LkCsgLvl> LkCsgLvl { get; set; }
        public virtual DbSet<LkCtry> LkCtry { get; set; }
        public virtual DbSet<LkCtryCty> LkCtryCty { get; set; }
        public virtual DbSet<LkCtryCxr> LkCtryCxr { get; set; }
        public virtual DbSet<LkCur> LkCur { get; set; }
        public virtual DbSet<LkCustCntrcLgth> LkCustCntrcLgth { get; set; }
        public virtual DbSet<LkDedctdCust> LkDedctdCust { get; set; }
        public virtual DbSet<LkDev> LkDev { get; set; }
        public virtual DbSet<LkDevManf> LkDevManf { get; set; }
        public virtual DbSet<LkDevModel> LkDevModel { get; set; }
        public virtual DbSet<LkDmstcClliCd> LkDmstcClliCd { get; set; }
        public virtual DbSet<LkEmailReqType> LkEmailReqType { get; set; }
        public virtual DbSet<LkEnhncSrvc> LkEnhncSrvc { get; set; }
        public virtual DbSet<LkEsclReas> LkEsclReas { get; set; }
        public virtual DbSet<LkEventRule> LkEventRule { get; set; }
        public virtual DbSet<LkEventStus> LkEventStus { get; set; }
        public virtual DbSet<LkEventType> LkEventType { get; set; }
        public virtual DbSet<LkEventTypeTmeSlot> LkEventTypeTmeSlot { get; set; }
        public virtual DbSet<LkFailActy> LkFailActy { get; set; }
        public virtual DbSet<LkFailReas> LkFailReas { get; set; }
        public virtual DbSet<LkFedMsg> LkFedMsg { get; set; }
        public virtual DbSet<LkFedRuleFail> LkFedRuleFail { get; set; }
        public virtual DbSet<LkFedlineEventFailCd> LkFedlineEventFailCd { get; set; }
        public virtual DbSet<LkFedlineOrdrType> LkFedlineOrdrType { get; set; }
        public virtual DbSet<LkFiltrOpr> LkFiltrOpr { get; set; }
        public virtual DbSet<LkFrgnCxr> LkFrgnCxr { get; set; }
        public virtual DbSet<LkFsaOrdrType> LkFsaOrdrType { get; set; }
        public virtual DbSet<LkFsaProdType> LkFsaProdType { get; set; }
        public virtual DbSet<LkGrp> LkGrp { get; set; }
        public virtual DbSet<LkIntlClliCd> LkIntlClliCd { get; set; }
        public virtual DbSet<LkIpMstr> LkIpMstr { get; set; }
        public virtual DbSet<LkIpVer> LkIpVer { get; set; }
        public virtual DbSet<LkMds3rdpartySrvcLvl> LkMds3rdpartySrvcLvl { get; set; }
        public virtual DbSet<LkMds3rdpartyVndr> LkMds3rdpartyVndr { get; set; }
        public virtual DbSet<LkMdsActyType> LkMdsActyType { get; set; }
        public virtual DbSet<LkMdsBrdgNeedReas> LkMdsBrdgNeedReas { get; set; }
        public virtual DbSet<LkMdsCpeDevXclusn> LkMdsCpeDevXclusn { get; set; }
        public virtual DbSet<LkMdsImplmtnNtvl> LkMdsImplmtnNtvl { get; set; }
        public virtual DbSet<LkMdsMacActy> LkMdsMacActy { get; set; }
        public virtual DbSet<LkMdsNtwkActyType> LkMdsNtwkActyType { get; set; }
        public virtual DbSet<LkMdsSrvcTier> LkMdsSrvcTier { get; set; }
        public virtual DbSet<LkMdsSrvcType> LkMdsSrvcType { get; set; }
        public virtual DbSet<LkMenu> LkMenu { get; set; }
        public virtual DbSet<LkMnsOffrgRoutr> LkMnsOffrgRoutr { get; set; }
        public virtual DbSet<LkMnsPrfmcRpt> LkMnsPrfmcRpt { get; set; }
        public virtual DbSet<LkMnsRoutgType> LkMnsRoutgType { get; set; }
        public virtual DbSet<LkMplsAccsBdwd> LkMplsAccsBdwd { get; set; }
        public virtual DbSet<LkMplsActyType> LkMplsActyType { get; set; }
        public virtual DbSet<LkMplsEventType> LkMplsEventType { get; set; }
        public virtual DbSet<LkMplsMgrtnType> LkMplsMgrtnType { get; set; }
        public virtual DbSet<LkMultiVrfReq> LkMultiVrfReq { get; set; }
        public virtual DbSet<LkNgvnProdType> LkNgvnProdType { get; set; }
        public virtual DbSet<LkNteType> LkNteType { get; set; }
        public virtual DbSet<LkOrdrActn> LkOrdrActn { get; set; }
        public virtual DbSet<LkOrdrCat> LkOrdrCat { get; set; }
        public virtual DbSet<LkOrdrStus> LkOrdrStus { get; set; }
        public virtual DbSet<LkOrdrSubType> LkOrdrSubType { get; set; }
        public virtual DbSet<LkOrdrType> LkOrdrType { get; set; }
        public virtual DbSet<LkPltfrm> LkPltfrm { get; set; }
        public virtual DbSet<LkPprt> LkPprt { get; set; }
        public virtual DbSet<LkPrfHrchy> LkPrfHrchy { get; set; }
        public virtual DbSet<LkProd> LkProd { get; set; }
        public virtual DbSet<LkProdType> LkProdType { get; set; }
        public virtual DbSet<LkQlfctn> LkQlfctn { get; set; }
        public virtual DbSet<LkRecStus> LkRecStus { get; set; }
        public virtual DbSet<LkRedsgnCat> LkRedsgnCat { get; set; }
        public virtual DbSet<LkRedsgnNotesType> LkRedsgnNotesType { get; set; }
        public virtual DbSet<LkRedsgnType> LkRedsgnType { get; set; }
        public virtual DbSet<LkRole> LkRole { get; set; }
        public virtual DbSet<LkScrdObjType> LkScrdObjType { get; set; }
        public virtual DbSet<LkSdePrdctType> LkSdePrdctType { get; set; }
        public virtual DbSet<LkSiptActyType> LkSiptActyType { get; set; }
        public virtual DbSet<LkSiptProdType> LkSiptProdType { get; set; }
        public virtual DbSet<LkSiptTollType> LkSiptTollType { get; set; }
        public virtual DbSet<LkSpclProj> LkSpclProj { get; set; }
        public virtual DbSet<LkSplkActyType> LkSplkActyType { get; set; }
        public virtual DbSet<LkSplkEventType> LkSplkEventType { get; set; }
        public virtual DbSet<LkSprintCpeNcr> LkSprintCpeNcr { get; set; }
        public virtual DbSet<LkSprintHldy> LkSprintHldy { get; set; }
        public virtual DbSet<LkSrvcAssrnSiteSupp> LkSrvcAssrnSiteSupp { get; set; }
        public virtual DbSet<LkStdiReas> LkStdiReas { get; set; }
        public virtual DbSet<LkStus> LkStus { get; set; }
        public virtual DbSet<LkStusType> LkStusType { get; set; }
        public virtual DbSet<LkSucssActy> LkSucssActy { get; set; }
        public virtual DbSet<LkSysCfg> LkSysCfg { get; set; }
        public virtual DbSet<LkTask> LkTask { get; set; }
        public virtual DbSet<LkTelco> LkTelco { get; set; }
        public virtual DbSet<LkUcaaSActyType> LkUcaaSActyType { get; set; }
        public virtual DbSet<LkUcaaSBillActy> LkUcaaSBillActy { get; set; }
        public virtual DbSet<LkUcaaSPlanType> LkUcaaSPlanType { get; set; }
        public virtual DbSet<LkUcaaSProdType> LkUcaaSProdType { get; set; }
        public virtual DbSet<LkUsStt> LkUsStt { get; set; }
        public virtual DbSet<LkUser> LkUser { get; set; }
        public virtual DbSet<LkUsrPrf> LkUsrPrf { get; set; }
        public virtual DbSet<LkUsrPrfMenu> LkUsrPrfMenu { get; set; }
        public virtual DbSet<LkVasType> LkVasType { get; set; }
        public virtual DbSet<LkVndr> LkVndr { get; set; }
        public virtual DbSet<LkVndrEmailLang> LkVndrEmailLang { get; set; }
        public virtual DbSet<LkVndrEmailType> LkVndrEmailType { get; set; }
        public virtual DbSet<LkVndrOrdrType> LkVndrOrdrType { get; set; }
        public virtual DbSet<LkVndrScm> LkVndrScm { get; set; }
        public virtual DbSet<LkVpnPltfrmType> LkVpnPltfrmType { get; set; }
        public virtual DbSet<LkWhlslPtnr> LkWhlslPtnr { get; set; }
        public virtual DbSet<LkWrkflwStus> LkWrkflwStus { get; set; }
        public virtual DbSet<LkWrldHldy> LkWrldHldy { get; set; }
        public virtual DbSet<LkXnciMs> LkXnciMs { get; set; }
        public virtual DbSet<LkXnciMsSla> LkXnciMsSla { get; set; }
        public virtual DbSet<LkXnciRgn> LkXnciRgn { get; set; }
        public virtual DbSet<M5BillDsptchMsg> M5BillDsptchMsg { get; set; }
        public virtual DbSet<M5EventMsg> M5EventMsg { get; set; }
        public virtual DbSet<MapGrpTask> MapGrpTask { get; set; }
        public virtual DbSet<MapPrfTask> MapPrfTask { get; set; }
        public virtual DbSet<MapUsrPrf> MapUsrPrf { get; set; }
        public virtual DbSet<MdsEvent> MdsEvent { get; set; }
        public virtual DbSet<MdsEventDslSbicCustTrpt> MdsEventDslSbicCustTrpt { get; set; }
        public virtual DbSet<MdsEventFormChng> MdsEventFormChng { get; set; }
        public virtual DbSet<MdsEventMacActy> MdsEventMacActy { get; set; }
        public virtual DbSet<MdsEventNew> MdsEventNew { get; set; }
        public virtual DbSet<MdsEventNtwkActy> MdsEventNtwkActy { get; set; }
        public virtual DbSet<MdsEventNtwkCust> MdsEventNtwkCust { get; set; }
        public virtual DbSet<MdsEventNtwkTrpt> MdsEventNtwkTrpt { get; set; }
        public virtual DbSet<MdsEventOdieDev> MdsEventOdieDev { get; set; }
        public virtual DbSet<MdsEventPortBndwd> MdsEventPortBndwd { get; set; }
        public virtual DbSet<MdsEventSiteSrvc> MdsEventSiteSrvc { get; set; }
        public virtual DbSet<MdsEventSlnkWiredTrpt> MdsEventSlnkWiredTrpt { get; set; }
        public virtual DbSet<MdsEventSrvc> MdsEventSrvc { get; set; }
        public virtual DbSet<MdsEventWrlsTrpt> MdsEventWrlsTrpt { get; set; }
        public virtual DbSet<MdsFastTrkType> MdsFastTrkType { get; set; }
        public virtual DbSet<MplsAccs> MplsAccs { get; set; }
        public virtual DbSet<MplsEvent> MplsEvent { get; set; }
        public virtual DbSet<MplsEventAccsTag> MplsEventAccsTag { get; set; }
        public virtual DbSet<MplsEventActyType> MplsEventActyType { get; set; }
        public virtual DbSet<MplsEventVasType> MplsEventVasType { get; set; }
        public virtual DbSet<NccoOrdr> NccoOrdr { get; set; }
        public virtual DbSet<NgvnEvent> NgvnEvent { get; set; }
        public virtual DbSet<NgvnEventCktIdNua> NgvnEventCktIdNua { get; set; }
        public virtual DbSet<NgvnEventSipTrnk> NgvnEventSipTrnk { get; set; }
        public virtual DbSet<NidActy> NidActy { get; set; }
        public virtual DbSet<NrmCkt> NrmCkt { get; set; }
        public virtual DbSet<NrmSrvcInstc> NrmSrvcInstc { get; set; }
        public virtual DbSet<NrmVndr> NrmVndr { get; set; }
        public virtual DbSet<OdieReq> OdieReq { get; set; }
        public virtual DbSet<OdieRspn> OdieRspn { get; set; }
        public virtual DbSet<OdieRspnInfo> OdieRspnInfo { get; set; }
        public virtual DbSet<Ordr> Ordr { get; set; }
        public virtual DbSet<OrdrAdr> OrdrAdr { get; set; }
        public virtual DbSet<OrdrCktChg> OrdrCktChg { get; set; }
        public virtual DbSet<OrdrCntct> OrdrCntct { get; set; }
        public virtual DbSet<OrdrJprdy> OrdrJprdy { get; set; }
        public virtual DbSet<OrdrMs> OrdrMs { get; set; }
        public virtual DbSet<OrdrNte> OrdrNte { get; set; }
        public virtual DbSet<OrdrRecLock> OrdrRecLock { get; set; }
        public virtual DbSet<OrdrStdiHist> OrdrStdiHist { get; set; }
        public virtual DbSet<OrdrVlan> OrdrVlan { get; set; }
        public virtual DbSet<PltfrmMapng> PltfrmMapng { get; set; }
        public virtual DbSet<PsReqHdrQueue> PsReqHdrQueue { get; set; }
        public virtual DbSet<PsReqLineItmQueue> PsReqLineItmQueue { get; set; }
        public virtual DbSet<QlfctnDedctdCust> QlfctnDedctdCust { get; set; }
        public virtual DbSet<QlfctnDev> QlfctnDev { get; set; }
        public virtual DbSet<QlfctnEnhncSrvc> QlfctnEnhncSrvc { get; set; }
        public virtual DbSet<QlfctnEventType> QlfctnEventType { get; set; }
        public virtual DbSet<QlfctnIpVer> QlfctnIpVer { get; set; }
        public virtual DbSet<QlfctnMplsActyType> QlfctnMplsActyType { get; set; }
        public virtual DbSet<QlfctnNtwkActyType> QlfctnNtwkActyType { get; set; }
        public virtual DbSet<QlfctnSpclProj> QlfctnSpclProj { get; set; }
        public virtual DbSet<QlfctnVndrModel> QlfctnVndrModel { get; set; }
        public virtual DbSet<Redsgn> Redsgn { get; set; }
        public virtual DbSet<RedsgnCustBypass> RedsgnCustBypass { get; set; }
        public virtual DbSet<RedsgnDevEventHist> RedsgnDevEventHist { get; set; }
        public virtual DbSet<RedsgnDevicesInfo> RedsgnDevicesInfo { get; set; }
        public virtual DbSet<RedsgnDoc> RedsgnDoc { get; set; }
        public virtual DbSet<RedsgnEmailNtfctn> RedsgnEmailNtfctn { get; set; }
        public virtual DbSet<RedsgnH1MrcValues> RedsgnH1MrcValues { get; set; }
        public virtual DbSet<RedsgnNotes> RedsgnNotes { get; set; }
        public virtual DbSet<RedsgnRecLock> RedsgnRecLock { get; set; }
        public virtual DbSet<RedsgnWrkflw> RedsgnWrkflw { get; set; }
        public virtual DbSet<ReltdEvent> ReltdEvent { get; set; }
        public virtual DbSet<SdeOpptnty> SdeOpptnty { get; set; }
        public virtual DbSet<SdeOpptntyDoc> SdeOpptntyDoc { get; set; }
        public virtual DbSet<SdeOpptntyNte> SdeOpptntyNte { get; set; }
        public virtual DbSet<SdeOpptntyPrdct> SdeOpptntyPrdct { get; set; }
        public virtual DbSet<SipTrnkGrp> SipTrnkGrp { get; set; }
        public virtual DbSet<SiptEvent> SiptEvent { get; set; }
        public virtual DbSet<SiptEventActy> SiptEventActy { get; set; }
        public virtual DbSet<SiptEventDoc> SiptEventDoc { get; set; }
        public virtual DbSet<SiptEventTollType> SiptEventTollType { get; set; }
        public virtual DbSet<SiptReltdOrdr> SiptReltdOrdr { get; set; }
        public virtual DbSet<Sm> Sm { get; set; }
        public virtual DbSet<SplkEvent> SplkEvent { get; set; }
        public virtual DbSet<SplkEventAccs> SplkEventAccs { get; set; }
        public virtual DbSet<SrcCurFile> SrcCurFile { get; set; }
        public virtual DbSet<SstatReq> SstatReq { get; set; }
        public virtual DbSet<TrptOrdr> TrptOrdr { get; set; }
        public virtual DbSet<UcaaSEvent> UcaaSEvent { get; set; }
        public virtual DbSet<UcaaSEventBilling> UcaaSEventBilling { get; set; }
        public virtual DbSet<UcaaSEventOdieDev> UcaaSEventOdieDev { get; set; }
        public virtual DbSet<UserCpeTech> UserCpeTech { get; set; }
        public virtual DbSet<UserCsgLvl> UserCsgLvl { get; set; }
        public virtual DbSet<UserH5Wfm> UserH5Wfm { get; set; }
        public virtual DbSet<UserWfm> UserWfm { get; set; }
        public virtual DbSet<UserWfmAsmt> UserWfmAsmt { get; set; }
        public virtual DbSet<VndrFoldr> VndrFoldr { get; set; }
        public virtual DbSet<VndrFoldrCntct> VndrFoldrCntct { get; set; }
        public virtual DbSet<VndrOrdr> VndrOrdr { get; set; }
        public virtual DbSet<VndrOrdrEmail> VndrOrdrEmail { get; set; }
        public virtual DbSet<VndrOrdrEmailAtchmt> VndrOrdrEmailAtchmt { get; set; }
        public virtual DbSet<VndrOrdrForm> VndrOrdrForm { get; set; }
        public virtual DbSet<VndrOrdrMs> VndrOrdrMs { get; set; }
        public virtual DbSet<VndrTmplt> VndrTmplt { get; set; }
        public virtual DbSet<WgProfSm> WgProfSm { get; set; }
        public virtual DbSet<WgPtrnSm> WgPtrnSm { get; set; }

        // Unable to generate entity type for table 'dbo.MPLS_SAVE_UNMASKED_CUST_DATA'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=TVMXD969\\\\MS2017_TEST,2788;Database=COWS;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region QUERY MODEL
            modelBuilder.Query<AdvancedSearchEvent>();
            modelBuilder.Query<AdvancedSearchOrder>();
            modelBuilder.Query<AdvancedSearchRedesign>();
            modelBuilder.Query<GetOrdrUnlockView>();
            modelBuilder.Query<GetNRMBPMInterfaceView>();
            modelBuilder.Query<GetOrdrCancelView>();
            modelBuilder.Query<TimeSlotOccurrence>();
            modelBuilder.Query<FTAvailEvent>();
            modelBuilder.Query<FedlineManageUserEvent>();
            modelBuilder.Query<WFMUserAssignment>();
            modelBuilder.Query<InsertWFMUserAssignments>();
            modelBuilder.Query<CustomerUserProfileView>();
            modelBuilder.Query<UserProfiles>();
            modelBuilder.Query<GetQualificationView>();
            modelBuilder.Query<GetSCMInterfaceView>();
            modelBuilder.Query<GetEventViews>();
            modelBuilder.Query<GetEventViewDetails>();
            modelBuilder.Query<GetEventViewColumns>();
            modelBuilder.Query<RetrieveCustomerByH1>();
            modelBuilder.Query<GetEncryptValues>();
            modelBuilder.Query<GetDecryptValues>();
            modelBuilder.Query<GetEventDecryptValues>();
            modelBuilder.Query<GetMdsEventSiteSrvc>();
            modelBuilder.Query<GetEventDevSrvcMgmt>();
            modelBuilder.Query<GetEventCpeDev>();
            modelBuilder.Query<GetOdieDiscoResponse>();
            modelBuilder.Query<SdeOpportunity>();
            modelBuilder.Query<CalendarData>();
            modelBuilder.Query<SDEOpportunityNote>();
            modelBuilder.Query<SDEOpportunityProduct>();
            modelBuilder.Query<SDEOpportunityDoc>();
            modelBuilder.Query<GetSdeView>();
            modelBuilder.Query<GetAllCptView>();
            modelBuilder.Query<DocEntity>();
            modelBuilder.Query<DesignDocDetail>();
            modelBuilder.Query<OdieCustomerH1>();
            modelBuilder.Query<GetVendorFolder>();
            modelBuilder.Query<BillingDispatchData>();
            modelBuilder.Query<FSADisconnect>();
            modelBuilder.Query<InsertM5CompleteMsg>();
            #endregion

            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<ActTask>(entity =>
            {
                entity.ToTable("ACT_TASK", "dbo");

                entity.HasIndex(e => new { e.OrdrId, e.TaskId })
                    .HasName("UX01_ACT_TASK")
                    .IsUnique();

                entity.Property(e => e.ActTaskId).HasColumnName("ACT_TASK_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.StusId).HasColumnName("STUS_ID");

                entity.Property(e => e.TaskId).HasColumnName("TASK_ID");

                entity.Property(e => e.WgProfId).HasColumnName("WG_PROF_ID");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.ActTask)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ACT_TASK");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.ActTask)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_ACT_TASK");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.ActTask)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_ACT_TASK");
            });

            modelBuilder.Entity<AdEvent>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("AD_EVENT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX07_AD_EVENT");

                entity.HasIndex(e => e.EnhncSrvcId)
                    .HasName("IX02_AD_EVENT");

                entity.HasIndex(e => e.EventStusId)
                    .HasName("IX01_AD_EVENT");

                entity.HasIndex(e => e.Ftn)
                    .HasName("IX11_AD_EVENT_FTN");

                entity.HasIndex(e => e.IpVerId)
                    .HasName("IX10_AD_EVENT");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX08_AD_EVENT");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX06_AD_EVENT");

                entity.HasIndex(e => e.ReqorUserId)
                    .HasName("IX03_AD_EVENT");

                entity.HasIndex(e => e.SalsUserId)
                    .HasName("IX04_AD_EVENT");

                entity.HasIndex(e => e.WrkflwStusId)
                    .HasName("IX05_AD_EVENT");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltdEmailCcTxt)
                    .HasColumnName("CMPLTD_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcBrdgNbr)
                    .HasColumnName("CNFRC_BRDG_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcPinNbr)
                    .HasColumnName("CNFRC_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CpeAtndCd).HasColumnName("CPE_ATND_CD");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustCntctCellPhnNbr)
                    .HasColumnName("CUST_CNTCT_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctNme)
                    .HasColumnName("CUST_CNTCT_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPgrNbr)
                    .HasColumnName("CUST_CNTCT_PGR_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPgrPinNbr)
                    .HasColumnName("CUST_CNTCT_PGR_PIN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPhnNbr)
                    .HasColumnName("CUST_CNTCT_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustEmailAdr)
                    .HasColumnName("CUST_EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.DesCmntTxt)
                    .IsRequired()
                    .HasColumnName("DES_CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.DocLinkTxt)
                    .HasColumnName("DOC_LINK_TXT")
                    .HasMaxLength(1000);

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EnhncSrvcId).HasColumnName("ENHNC_SRVC_ID");

                entity.Property(e => e.EsclCd).HasColumnName("ESCL_CD");

                entity.Property(e => e.EsclReasId).HasColumnName("ESCL_REAS_ID");

                entity.Property(e => e.EventDes)
                    .HasColumnName("EVENT_DES")
                    .IsUnicode(false);

                entity.Property(e => e.EventDrtnInMinQty).HasColumnName("EVENT_DRTN_IN_MIN_QTY");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt)
                    .HasColumnName("EVENT_TITLE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraDrtnTmeAmt).HasColumnName("EXTRA_DRTN_TME_AMT");

                entity.Property(e => e.Ftn)
                    .HasColumnName("FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.H1)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.H6).HasMaxLength(9);

                entity.Property(e => e.IpVerId).HasColumnName("IP_VER_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PrimReqDt)
                    .HasColumnName("PRIM_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PubEmailCcTxt)
                    .HasColumnName("PUB_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReqorUserId).HasColumnName("REQOR_USER_ID");

                entity.Property(e => e.SalsUserId).HasColumnName("SALS_USER_ID");

                entity.Property(e => e.ScndyReqDt)
                    .HasColumnName("SCNDY_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SowsEventId).HasColumnName("SOWS_EVENT_ID");

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.AdEventCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_AD_EVENT");

                entity.HasOne(d => d.EnhncSrvc)
                    .WithMany(p => p.AdEvent)
                    .HasForeignKey(d => d.EnhncSrvcId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK09_AD_EVENT");

                entity.HasOne(d => d.EsclReas)
                    .WithMany(p => p.AdEvent)
                    .HasForeignKey(d => d.EsclReasId)
                    .HasConstraintName("FK08_AD_EVENT");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.AdEvent)
                    .HasForeignKey<AdEvent>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK10_AD_EVENT");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.AdEvent)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_AD_EVENT");

                entity.HasOne(d => d.IpVer)
                    .WithMany(p => p.AdEvent)
                    .HasForeignKey(d => d.IpVerId)
                    .HasConstraintName("FK12_AD_EVENT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.AdEventModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK07_AD_EVENT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.AdEvent)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_AD_EVENT");

                entity.HasOne(d => d.ReqorUser)
                    .WithMany(p => p.AdEventReqorUser)
                    .HasForeignKey(d => d.ReqorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_AD_EVENT");

                entity.HasOne(d => d.SalsUser)
                    .WithMany(p => p.AdEventSalsUser)
                    .HasForeignKey(d => d.SalsUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_AD_EVENT");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.AdEvent)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_AD_EVENT");
            });

            modelBuilder.Entity<AdEventAccsTag>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.CktId });

                entity.ToTable("AD_EVENT_ACCS_TAG", "dbo");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.CktId)
                    .HasColumnName("CKT_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OldCktId)
                    .HasColumnName("OLD_CKT_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.AdEventAccsTag)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_AD_EVENT_ACCS_TAG");
            });

            modelBuilder.Entity<Appt>(entity =>
            {
                entity.ToTable("APPT", "dbo");

                entity.HasIndex(e => e.ActyTypeId)
                    .HasName("IX05_APPT");

                entity.HasIndex(e => e.ApptTypeId)
                    .HasName("IX01_APPT");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_APPT");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_APPT");

                entity.HasIndex(e => e.StrtTmst)
                    .HasName("IX07_APPT_STRT");

                entity.Property(e => e.ApptId).HasColumnName("APPT_ID");

                entity.Property(e => e.ActyTypeId).HasColumnName("ACTY_TYPE_ID");

                entity.Property(e => e.ApptLocTxt)
                    .HasColumnName("APPT_LOC_TXT")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ApptTypeId).HasColumnName("APPT_TYPE_ID");

                entity.Property(e => e.AsnToUserIdListTxt)
                    .HasColumnName("ASN_TO_USER_ID_LIST_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Des)
                    .HasColumnName("DES")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RcurncCd).HasColumnName("RCURNC_CD");

                entity.Property(e => e.RcurncDesTxt)
                    .HasColumnName("RCURNC_DES_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("datetime");

                entity.Property(e => e.SubjTxt)
                    .HasColumnName("SUBJ_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.ApptType)
                    .WithMany(p => p.Appt)
                    .HasForeignKey(d => d.ApptTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_APPT");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.ApptCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_APPT");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.Appt)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK03_APPT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.ApptModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_APPT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.Appt)
                    .HasForeignKey(d => d.RecStusId)
                    .HasConstraintName("FK06_APPT");
            });

            modelBuilder.Entity<ApptRcurncData>(entity =>
            {
                entity.ToTable("APPT_RCURNC_DATA", "dbo");

                entity.HasIndex(e => e.ApptId)
                    .HasName("IX01_APPT_RCURNC_DATA");

                entity.HasIndex(e => e.ApptTypeId)
                    .HasName("IX02_APPT_RCURNC_DATA");

                entity.HasIndex(e => e.AsnUserId)
                    .HasName("IX03_APPT_RCURNC_DATA");

                entity.Property(e => e.ApptRcurncDataId).HasColumnName("APPT_RCURNC_DATA_ID");

                entity.Property(e => e.ApptId).HasColumnName("APPT_ID");

                entity.Property(e => e.ApptTypeId).HasColumnName("APPT_TYPE_ID");

                entity.Property(e => e.AsnUserId).HasColumnName("ASN_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("datetime");

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Appt)
                    .WithMany(p => p.ApptRcurncData)
                    .HasForeignKey(d => d.ApptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_APPT_RCURNC_DATA");

                entity.HasOne(d => d.ApptType)
                    .WithMany(p => p.ApptRcurncData)
                    .HasForeignKey(d => d.ApptTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_APPT_RCURNC_DATA");

                entity.HasOne(d => d.AsnUser)
                    .WithMany(p => p.ApptRcurncData)
                    .HasForeignKey(d => d.AsnUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_APPT_RCURNC_DATA");
            });

            modelBuilder.Entity<Asr>(entity =>
            {
                entity.ToTable("ASR", "dbo");

                entity.Property(e => e.AsrId).HasColumnName("ASR_ID");

                entity.Property(e => e.AccsBdwdDes)
                    .HasColumnName("ACCS_BDWD_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AccsCtyNmeSiteCd)
                    .HasColumnName("ACCS_CTY_NME_SITE_CD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AsrNotesTxt)
                    .HasColumnName("ASR_NOTES_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.AsrTypeId).HasColumnName("ASR_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.DlciDes)
                    .HasColumnName("DLCI_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailReqId).HasColumnName("EMAIL_REQ_ID");

                entity.Property(e => e.EntrncAsmtTxt)
                    .HasColumnName("ENTRNC_ASMT_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.H1MatchMstrVasCd).HasColumnName("H1_MATCH_MSTR_VAS_CD");

                entity.Property(e => e.IntlDomEmailCd)
                    .HasColumnName("INTL_DOM_EMAIL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IpNodeTxt)
                    .HasColumnName("IP_NODE_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LecNniNbr)
                    .HasColumnName("LEC_NNI_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MstrFtnCd)
                    .HasColumnName("MSTR_FTN_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.PtnrCxrCd)
                    .HasColumnName("PTNR_CXR_CD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StusId).HasColumnName("STUS_ID");

                entity.Property(e => e.TrnspntCd).HasColumnName("TRNSPNT_CD");

                entity.HasOne(d => d.AsrType)
                    .WithMany(p => p.Asr)
                    .HasForeignKey(d => d.AsrTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MACH5_ASR");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.Asr)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_MACH5_ASR");

                entity.HasOne(d => d.EmailReq)
                    .WithMany(p => p.Asr)
                    .HasForeignKey(d => d.EmailReqId)
                    .HasConstraintName("FK01_ASR");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.Asr)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MACH5_ASR");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.Asr)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_MACH5_ASR");
            });

            modelBuilder.Entity<BillDsptch>(entity =>
            {
                entity.ToTable("BILL_DSPTCH", "dbo");

                entity.HasIndex(e => e.ClsDt)
                    .HasName("IX02_BILL_DSPTCH");

                entity.HasIndex(e => e.CustId)
                    .HasName("IX01_BILL_DSPTCH");

                entity.Property(e => e.BillDsptchId).HasColumnName("BILL_DSPTCH_ID");

                entity.Property(e => e.AcptRjctCd)
                    .HasColumnName("ACPT_RJCT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ClearLoc)
                    .IsRequired()
                    .HasColumnName("CLEAR_LOC")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClsDt)
                    .HasColumnName("CLS_DT")
                    .HasColumnType("date");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustId)
                    .IsRequired()
                    .HasColumnName("CUST_ID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.DispCaName)
                    .IsRequired()
                    .HasColumnName("DISP_CA_NAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DispCatName)
                    .IsRequired()
                    .HasColumnName("DISP_CAT_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DispCd)
                    .IsRequired()
                    .HasColumnName("DISP_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DispSubcatName)
                    .IsRequired()
                    .HasColumnName("DISP_SUBCAT_NAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.StusId)
                    .HasColumnName("STUS_ID")
                    .HasDefaultValueSql("((241))");

                entity.Property(e => e.TickNbr)
                    .IsRequired()
                    .HasColumnName("TICK_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.BillDsptchCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_BILL_DSPTCH");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.BillDsptchModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_BILL_DSPTCH");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.BillDsptch)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_BILL_DSPTCH");
            });

            modelBuilder.Entity<CcdHist>(entity =>
            {
                entity.ToTable("CCD_HIST", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX03_CCD_HIST");

                entity.HasIndex(e => e.NteId)
                    .HasName("IX02_CCD_HIST");

                entity.HasIndex(e => e.OrdrId)
                    .HasName("IX01_CCD_HIST");

                entity.Property(e => e.CcdHistId).HasColumnName("CCD_HIST_ID");

                entity.Property(e => e.AckByVndrDt)
                    .HasColumnName("ACK_BY_VNDR_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NewCcdDt)
                    .HasColumnName("NEW_CCD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.NteId).HasColumnName("NTE_ID");

                entity.Property(e => e.OldCcdDt)
                    .HasColumnName("OLD_CCD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.SentToVndrDt)
                    .HasColumnName("SENT_TO_VNDR_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CcdHist)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_CCD_HIST");

                entity.HasOne(d => d.Nte)
                    .WithMany(p => p.CcdHist)
                    .HasForeignKey(d => d.NteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CCD_HIST");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.CcdHist)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CCD_HIST");
            });

            modelBuilder.Entity<CcdHistReas>(entity =>
            {
                entity.HasKey(e => new { e.CcdHistId, e.CcdMissdReasId });

                entity.ToTable("CCD_HIST_REAS", "dbo");

                entity.Property(e => e.CcdHistId).HasColumnName("CCD_HIST_ID");

                entity.Property(e => e.CcdMissdReasId).HasColumnName("CCD_MISSD_REAS_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.CcdHist)
                    .WithMany(p => p.CcdHistReas)
                    .HasForeignKey(d => d.CcdHistId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CCD_HIST_REAS");

                entity.HasOne(d => d.CcdMissdReas)
                    .WithMany(p => p.CcdHistReas)
                    .HasForeignKey(d => d.CcdMissdReasId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CCD_HIST_REAS");
            });

            modelBuilder.Entity<Ckt>(entity =>
            {
                entity.ToTable("CKT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_CKT");

                entity.HasIndex(e => e.OrdrId)
                    .HasName("IX04_CKT");

                entity.HasIndex(e => e.VndrOrdrId)
                    .HasName("IX03_CKT");

                entity.Property(e => e.CktId).HasColumnName("CKT_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustIpAdr)
                    .HasColumnName("CUST_IP_ADR")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.IpAdrQty)
                    .HasColumnName("IP_ADR_QTY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LecId)
                    .HasColumnName("LEC_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.PlSeqNbr)
                    .HasColumnName("PL_SEQ_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RltdVndrOrdrId)
                    .HasColumnName("RLTD_VNDR_ORDR_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RtngPrcol)
                    .HasColumnName("RTNG_PRCOL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ScaDet)
                    .HasColumnName("SCA_DET")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.TrmtgCd).HasColumnName("TRMTG_CD");

                entity.Property(e => e.VndrCktId)
                    .HasColumnName("VNDR_CKT_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.VndrCntrcTermEndDt)
                    .HasColumnName("VNDR_CNTRC_TERM_END_DT")
                    .HasColumnType("smalldatetime");
                
                entity.Property(e => e.XCnnctPrvdr)
                    .HasColumnName("X_CNNCT_PRVDR")
                    .HasMaxLength(500)
                    .IsUnicode(false);
                
                entity.Property(e => e.TDwnAssn)
                    .HasColumnName("T_DWN_ASSN")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TDwnReq)
                    .HasColumnName("T_DWN_REQ")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.VndrOrdrId).HasColumnName("VNDR_ORDR_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.Ckt)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CKT");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.Ckt)
                    .HasForeignKey(d => d.OrdrId)
                    .HasConstraintName("FK02_CKT");

                entity.HasOne(d => d.VndrOrdr)
                    .WithMany(p => p.Ckt)
                    .HasForeignKey(d => d.VndrOrdrId)
                    .HasConstraintName("FK03_CKT");
            });

            modelBuilder.Entity<CktCost>(entity =>
            {
                entity.HasKey(e => new { e.CktId, e.VerId });

                entity.ToTable("CKT_COST", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_CKT_COST");

                entity.Property(e => e.CktId).HasColumnName("CKT_ID");

                entity.Property(e => e.VerId).HasColumnName("VER_ID");

                entity.Property(e => e.AccsCustCurId)
                    .HasColumnName("ACCS_CUST_CUR_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.AccsCustMrcAmt)
                    .HasColumnName("ACCS_CUST_MRC_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.AccsCustMrcInUsdAmt)
                    .HasColumnName("ACCS_CUST_MRC_IN_USD_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.AccsCustNrcAmt)
                    .HasColumnName("ACCS_CUST_NRC_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.AccsCustNrcInUsdAmt)
                    .HasColumnName("ACCS_CUST_NRC_IN_USD_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.AsrCurId)
                    .HasColumnName("ASR_CUR_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.AsrMrcAty)
                    .HasColumnName("ASR_MRC_ATY")
                    .HasColumnType("money");

                entity.Property(e => e.AsrMrcInUsdAmt)
                    .HasColumnName("ASR_MRC_IN_USD_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.AsrNrcAmt)
                    .HasColumnName("ASR_NRC_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.AsrNrcInUsdAmt)
                    .HasColumnName("ASR_NRC_IN_USD_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.BndlCd).HasColumnName("BNDL_CD");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.QotNbr)
                    .HasColumnName("QOT_NBR")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TaxRtPctQty)
                    .HasColumnName("TAX_RT_PCT_QTY")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.VndrCurId)
                    .HasColumnName("VNDR_CUR_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.VndrMrcAmt)
                    .HasColumnName("VNDR_MRC_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.VndrMrcInUsdAmt)
                    .HasColumnName("VNDR_MRC_IN_USD_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.VndrNrcAmt)
                    .HasColumnName("VNDR_NRC_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.VndrNrcInUsdAmt)
                    .HasColumnName("VNDR_NRC_IN_USD_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.VndrOrdrNbr)
                    .HasColumnName("VNDR_ORDR_NBR")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.AccsCustCur)
                    .WithMany(p => p.CktCostAccsCustCur)
                    .HasForeignKey(d => d.AccsCustCurId)
                    .HasConstraintName("FK02_CKT_COST");

                entity.HasOne(d => d.AsrCur)
                    .WithMany(p => p.CktCostAsrCur)
                    .HasForeignKey(d => d.AsrCurId)
                    .HasConstraintName("FK03_CKT_COST");

                entity.HasOne(d => d.Ckt)
                    .WithMany(p => p.CktCost)
                    .HasForeignKey(d => d.CktId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CKT_COST");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CktCost)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_CKT_COST");

                entity.HasOne(d => d.VndrCur)
                    .WithMany(p => p.CktCostVndrCur)
                    .HasForeignKey(d => d.VndrCurId)
                    .HasConstraintName("FK04_CKT_COST");
            });

            modelBuilder.Entity<CktMs>(entity =>
            {
                entity.HasKey(e => new { e.CktId, e.VerId });

                entity.ToTable("CKT_MS", "dbo");

                entity.HasIndex(e => e.CntrcTermId)
                    .HasName("IX02_CKT_MS");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_CKT_MS");

                entity.Property(e => e.CktId).HasColumnName("CKT_ID");

                entity.Property(e => e.VerId).HasColumnName("VER_ID");

                entity.Property(e => e.AccsAcptcDt)
                    .HasColumnName("ACCS_ACPTC_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.AccsDlvryDt)
                    .HasColumnName("ACCS_DLVRY_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CntrcEndDt)
                    .HasColumnName("CNTRC_END_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CntrcStrtDt)
                    .HasColumnName("CNTRC_STRT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CntrcTermId)
                    .HasColumnName("CNTRC_TERM_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RnlDt)
                    .HasColumnName("RNL_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.TrgtDlvryDt)
                    .HasColumnName("TRGT_DLVRY_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.TrgtDlvryDtRecvDt)
                    .HasColumnName("TRGT_DLVRY_DT_RECV_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.VndrCnfrmDscnctDt)
                    .HasColumnName("VNDR_CNFRM_DSCNCT_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.Ckt)
                    .WithMany(p => p.CktMs)
                    .HasForeignKey(d => d.CktId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CKT_MS");

                entity.HasOne(d => d.CntrcTerm)
                    .WithMany(p => p.CktMs)
                    .HasForeignKey(d => d.CntrcTermId)
                    .HasConstraintName("FK03_CKT_MS");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CktMs)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CKT_MS");
            });

            modelBuilder.Entity<CntctDetl>(entity =>
            {
                entity.ToTable("CNTCT_DETL", "dbo");

                entity.HasIndex(e => new { e.ObjId, e.ObjTypCd })
                    .HasName("IX01_CNTCT_DETL");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AutoRfrshCd)
                    .IsRequired()
                    .HasColumnName("AUTO_RFRSH_CD")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailAdr)
                    .IsRequired()
                    .HasColumnName("EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EmailCd)
                    .HasColumnName("EMAIL_CD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HierId)
                    .IsRequired()
                    .HasColumnName("HIER_ID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.HierLvlCd)
                    .IsRequired()
                    .HasColumnName("HIER_LVL_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.ObjId).HasColumnName("OBJ_ID");

                entity.Property(e => e.ObjTypCd)
                    .IsRequired()
                    .HasColumnName("OBJ_TYP_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PhnNbr)
                    .HasColumnName("PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RoleId).HasColumnName("ROLE_ID");

                entity.Property(e => e.SuprsEmail).HasColumnName("SUPRS_EMAIL");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CntctDetlCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_CNTCT_DETL");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.CntctDetlModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_CNTCT_DETL");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.CntctDetl)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CNTCT_DETL");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.CntctDetl)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CNTCT_DETL");
            });

            modelBuilder.Entity<CpeClli>(entity =>
            {
                entity.ToTable("CPE_CLLI", "dbo");

                entity.Property(e => e.CpeClliId).HasColumnName("CPE_CLLI_ID");

                entity.Property(e => e.AddrTxt)
                    .IsRequired()
                    .HasColumnName("ADDR_TXT")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CityNme)
                    .IsRequired()
                    .HasColumnName("CITY_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ClliId)
                    .IsRequired()
                    .HasColumnName("CLLI_ID")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CmntyNme)
                    .HasColumnName("CMNTY_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CountyNme)
                    .HasColumnName("COUNTY_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CpeClliStusId).HasColumnName("CPE_CLLI_STUS_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailCc)
                    .HasColumnName("EMAIL_CC")
                    .IsUnicode(false);

                entity.Property(e => e.Flr)
                    .HasColumnName("FLR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LngdeCoord)
                    .HasColumnName("LNGDE_COORD")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LttdeCoord)
                    .HasColumnName("LTTDE_COORD")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.PsoftSendCd).HasColumnName("PSOFT_SEND_CD");

                entity.Property(e => e.PsoftUpdDt)
                    .HasColumnName("PSOFT_UPD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Room)
                    .HasColumnName("ROOM")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SiteNme)
                    .HasColumnName("SITE_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiteOwnr)
                    .HasColumnName("SITE_OWNR")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SitePhn)
                    .HasColumnName("SITE_PHN")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SitePurpose)
                    .IsRequired()
                    .HasColumnName("SITE_PURPOSE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SprntOwndCd).HasColumnName("SPRNT_OWND_CD");

                entity.Property(e => e.SstatUpdDt)
                    .HasColumnName("SSTAT_UPD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.SttId)
                    .IsRequired()
                    .HasColumnName("STT_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCd)
                    .IsRequired()
                    .HasColumnName("ZIP_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.CpeClliStus)
                    .WithMany(p => p.CpeClli)
                    .HasForeignKey(d => d.CpeClliStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_CPE_CLLI");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CpeClliCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPE_CLLI");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.CpeClliModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_CPE_CLLI");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.CpeClli)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_CPE_CLLI");
            });

            modelBuilder.Entity<Cpt>(entity =>
            {
                entity.ToTable("CPT", "dbo");

                entity.HasIndex(e => e.CptCustNbr)
                    .HasName("UX01_CPT")
                    .IsUnique();

                entity.HasIndex(e => e.CptStusId)
                    .HasName("IX02_CPT");

                entity.HasIndex(e => e.CustShrtNme)
                    .HasName("IX03_CPT");

                entity.HasIndex(e => e.H1)
                    .HasName("IX01_CPT");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.AmEmailAdr)
                    .HasColumnName("AM_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CompnyNme)
                    .HasColumnName("COMPNY_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CptCustNbr)
                    .IsRequired()
                    .HasColumnName("CPT_CUST_NBR")
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.CptStusId).HasColumnName("CPT_STUS_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.CustAdr)
                    .HasColumnName("CUST_ADR")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CustCtryRgnNme)
                    .HasColumnName("CUST_CTRY_RGN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustCtyNme)
                    .HasColumnName("CUST_CTY_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustFlrBldgNme)
                    .HasColumnName("CUST_FLR_BLDG_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustShrtNme)
                    .HasColumnName("CUST_SHRT_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustSttPrvnNme)
                    .HasColumnName("CUST_STT_PRVN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustZipCd)
                    .HasColumnName("CUST_ZIP_CD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DevCnt).HasColumnName("DEV_CNT");

                entity.Property(e => e.EncMdmCd).HasColumnName("ENC_MDM_CD");

                entity.Property(e => e.EndIpAdr)
                    .HasColumnName("END_IP_ADR")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.GovtCustCd).HasColumnName("GOVT_CUST_CD");

                entity.Property(e => e.H1)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.HstMgndSrvcCd).HasColumnName("HST_MGND_SRVC_CD");

                entity.Property(e => e.IpmEmailAdr)
                    .HasColumnName("IPM_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.MplsIpAdr)
                    .HasColumnName("MPLS_IP_ADR")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PreShrdKey)
                    .HasColumnName("PRE_SHRD_KEY")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PreStgdKey)
                    .HasColumnName("PRE_STGD_KEY")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.QipSubnetNme)
                    .HasColumnName("QIP_SUBNET_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RetrnSdeCd).HasColumnName("RETRN_SDE_CD");

                entity.Property(e => e.RevwdPmCd).HasColumnName("REVWD_PM_CD");

                entity.Property(e => e.ScrtyClrnceCd).HasColumnName("SCRTY_CLRNCE_CD");

                entity.Property(e => e.SdwanCd).HasColumnName("SDWAN_CD");

                entity.Property(e => e.SeEmailAdr)
                    .HasColumnName("SE_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SprntWhlslReslrCd).HasColumnName("SPRNT_WHLSL_RESLR_CD");

                entity.Property(e => e.SprntWhlslReslrNme)
                    .HasColumnName("SPRNT_WHLSL_RESLR_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StartIpAdr)
                    .HasColumnName("START_IP_ADR")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SubmtrUserId).HasColumnName("SUBMTR_USER_ID");

                entity.Property(e => e.SubnetIpAdr)
                    .HasColumnName("SUBNET_IP_ADR")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.TacacsCd).HasColumnName("TACACS_CD");

                entity.Property(e => e.WpaaSIpAdr)
                    .HasColumnName("WPaaS_IP_ADR")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.CptStus)
                    .WithMany(p => p.Cpt)
                    .HasForeignKey(d => d.CptStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_CPT");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CptCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK01_CPT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.CptModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_CPT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.Cpt)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_CPT");

                entity.HasOne(d => d.SubmtrUser)
                    .WithMany(p => p.CptSubmtrUser)
                    .HasForeignKey(d => d.SubmtrUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_CPT");
            });

            modelBuilder.Entity<CptDoc>(entity =>
            {
                entity.ToTable("CPT_DOC", "dbo");

                entity.Property(e => e.CptDocId).HasColumnName("CPT_DOC_ID");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileCntnt)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FileSizeQty).HasColumnName("FILE_SIZE_QTY");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptDoc)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_DOC");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CptDocCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_DOC");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.CptDocModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_CPT_DOC");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.CptDoc)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_CPT_DOC");
            });

            modelBuilder.Entity<CptHist>(entity =>
            {
                entity.ToTable("CPT_HIST", "dbo");

                entity.HasIndex(e => new { e.ActnId, e.CptId })
                    .HasName("IX01_CPT_HIST");

                entity.Property(e => e.CptHistId).HasColumnName("CPT_HIST_ID");

                entity.Property(e => e.ActnId).HasColumnName("ACTN_ID");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Actn)
                    .WithMany(p => p.CptHist)
                    .HasForeignKey(d => d.ActnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_CPT_HIST");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptHist)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_HIST");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CptHist)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_HIST");
            });

            modelBuilder.Entity<CptMngdAuth>(entity =>
            {
                entity.ToTable("CPT_MNGD_AUTH", "dbo");

                entity.Property(e => e.CptMngdAuthId).HasColumnName("CPT_MNGD_AUTH_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptMngdAuthPrdId).HasColumnName("CPT_MNGD_AUTH_PRD_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptMngdAuth)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_MNGD_AUTH");

                entity.HasOne(d => d.CptMngdAuthPrd)
                    .WithMany(p => p.CptMngdAuth)
                    .HasForeignKey(d => d.CptMngdAuthPrdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_MNGD_AUTH");
            });

            modelBuilder.Entity<CptMngdSrvc>(entity =>
            {
                entity.ToTable("CPT_MNGD_SRVC", "dbo");

                entity.Property(e => e.CptMngdSrvcId).HasColumnName("CPT_MNGD_SRVC_ID");

                entity.Property(e => e.CptHstMngdSrvcId).HasColumnName("CPT_HST_MNGD_SRVC_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.CptHstMngdSrvc)
                    .WithMany(p => p.CptMngdSrvc)
                    .HasForeignKey(d => d.CptHstMngdSrvcId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_MNGD_SRVC");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptMngdSrvc)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_MNGD_SRVC");
            });

            modelBuilder.Entity<CptMvsProd>(entity =>
            {
                entity.ToTable("CPT_MVS_PROD", "dbo");

                entity.Property(e => e.CptMvsProdId).HasColumnName("CPT_MVS_PROD_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptMvsProdTypeId).HasColumnName("CPT_MVS_PROD_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptMvsProd)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_MVS_PROD");

                entity.HasOne(d => d.CptMvsProdType)
                    .WithMany(p => p.CptMvsProd)
                    .HasForeignKey(d => d.CptMvsProdTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_MVS_PROD");
            });

            modelBuilder.Entity<CptOdieRspn>(entity =>
            {
                entity.ToTable("CPT_ODIE_RSPN", "dbo");

                entity.Property(e => e.CptOdieRspnId).HasColumnName("CPT_ODIE_RSPN_ID");

                entity.Property(e => e.AsnUsrId).HasColumnName("ASN_USR_ID");

                entity.Property(e => e.AsnUsrNme)
                    .IsRequired()
                    .HasColumnName("ASN_USR_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReqId).HasColumnName("REQ_ID");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptOdieRspn)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_ODIE_RSPN");

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.CptOdieRspn)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_ODIE_RSPN");
            });

            modelBuilder.Entity<CptPrimSite>(entity =>
            {
                entity.HasKey(e => e.PrimSiteId);

                entity.ToTable("CPT_PRIM_SITE", "dbo");

                entity.Property(e => e.PrimSiteId).HasColumnName("PRIM_SITE_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptPrimSiteId).HasColumnName("CPT_PRIM_SITE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptPrimSite)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_PRIM_SITE");

                entity.HasOne(d => d.CptPrimSiteNavigation)
                    .WithMany(p => p.CptPrimSiteNavigation)
                    .HasForeignKey(d => d.CptPrimSiteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_PRIM_SITE");
            });

            modelBuilder.Entity<CptPrimWan>(entity =>
            {
                entity.ToTable("CPT_PRIM_WAN", "dbo");

                entity.Property(e => e.CptPrimWanId).HasColumnName("CPT_PRIM_WAN_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptPlnSrvcTierId).HasColumnName("CPT_PLN_SRVC_TIER_ID");

                entity.Property(e => e.CptPrimScndyTprtId).HasColumnName("CPT_PRIM_SCNDY_TPRT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Qty).HasColumnName("QTY");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptPrimWan)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_PRIM_WAN");

                entity.HasOne(d => d.CptPlnSrvcTier)
                    .WithMany(p => p.CptPrimWan)
                    .HasForeignKey(d => d.CptPlnSrvcTierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_CPT_PRIM_WAN");

                entity.HasOne(d => d.CptPrimScndyTprt)
                    .WithMany(p => p.CptPrimWan)
                    .HasForeignKey(d => d.CptPrimScndyTprtId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_PRIM_WAN");
            });

            modelBuilder.Entity<CptPrvsn>(entity =>
            {
                entity.ToTable("CPT_PRVSN", "dbo");

                entity.HasIndex(e => e.CptId)
                    .HasName("IX02_CPT_PRVSN");

                entity.HasIndex(e => e.PrvsnStusCd)
                    .HasName("IX01_CPT_PRVSN");

                entity.Property(e => e.CptPrvsnId).HasColumnName("CPT_PRVSN_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptPrvsnTypeId).HasColumnName("CPT_PRVSN_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrvsnNteTxt)
                    .HasColumnName("PRVSN_NTE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.PrvsnStusCd).HasColumnName("PRVSN_STUS_CD");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((301))");

                entity.Property(e => e.SrvrNme)
                    .HasColumnName("SRVR_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptPrvsn)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_PRVSN");

                entity.HasOne(d => d.CptPrvsnType)
                    .WithMany(p => p.CptPrvsn)
                    .HasForeignKey(d => d.CptPrvsnTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_CPT_PRVSN");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CptPrvsnCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_PRVSN");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.CptPrvsnModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_CPT_PRVSN");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.CptPrvsn)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_CPT_PRVSN");
            });

            modelBuilder.Entity<CptRecLock>(entity =>
            {
                entity.HasKey(e => e.CptId);

                entity.ToTable("CPT_REC_LOCK", "dbo");

                entity.Property(e => e.CptId)
                    .HasColumnName("CPT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LockByUserId).HasColumnName("LOCK_BY_USER_ID");

                entity.Property(e => e.StrtRecLockTmst)
                    .HasColumnName("STRT_REC_LOCK_TMST")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.Cpt)
                    .WithOne(p => p.CptRecLock)
                    .HasForeignKey<CptRecLock>(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_REC_LOCK");

                entity.HasOne(d => d.LockByUser)
                    .WithMany(p => p.CptRecLock)
                    .HasForeignKey(d => d.LockByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_REC_LOCK");
            });

            modelBuilder.Entity<CptRedsgn>(entity =>
            {
                entity.ToTable("CPT_REDSGN", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreatUserId).HasColumnName("CREAT_USER_ID");

                entity.Property(e => e.CustShrtNme)
                    .IsRequired()
                    .HasColumnName("CUST_SHRT_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.H1)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ModfdUserId).HasColumnName("MODFD_USER_ID");

                entity.Property(e => e.RedsgnId).HasColumnName("REDSGN_ID");

                entity.Property(e => e.StusId).HasColumnName("STUS_ID");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptRedsgn)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_REDSGN");

                entity.HasOne(d => d.CreatUser)
                    .WithMany(p => p.CptRedsgn)
                    .HasForeignKey(d => d.CreatUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_REDSGN");

                entity.HasOne(d => d.Redsgn)
                    .WithMany(p => p.CptRedsgn)
                    .HasForeignKey(d => d.RedsgnId)
                    .HasConstraintName("FK03_CPT_REDSGN");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.CptRedsgn)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_CPT_REDSGN");
            });

            modelBuilder.Entity<CptRltdInfo>(entity =>
            {
                entity.ToTable("CPT_RLTD_INFO", "dbo");

                entity.Property(e => e.CptRltdInfoId).HasColumnName("CPT_RLTD_INFO_ID");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CntctEmailList)
                    .HasColumnName("CNTCT_EMAIL_LIST")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CptCustTypeId).HasColumnName("CPT_CUST_TYPE_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptPlnSrvcTierId).HasColumnName("CPT_PLN_SRVC_TIER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CubeSiptSmiCd).HasColumnName("CUBE_SIPT_SMI_CD");

                entity.Property(e => e.CustUcdCd).HasColumnName("CUST_UCD_CD");

                entity.Property(e => e.E2eNddCd).HasColumnName("E2E_NDD_CD");

                entity.Property(e => e.HstdUcCd).HasColumnName("HSTD_UC_CD");

                entity.Property(e => e.NtwkDsgnDocTxt)
                    .HasColumnName("NTWK_DSGN_DOC_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.SdwanCustCd)
                    .HasColumnName("SDWAN_CUST_CD")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SpsSowCd).HasColumnName("SPS_SOW_CD");

                entity.Property(e => e.SuplmntlEthrntCd)
                    .HasColumnName("SUPLMNTL_ETHRNT_CD")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CptCustType)
                    .WithMany(p => p.CptRltdInfo)
                    .HasForeignKey(d => d.CptCustTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_CPT_RLTD_INFO");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptRltdInfo)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_RLTD_INFO");

                entity.HasOne(d => d.CptPlnSrvcTier)
                    .WithMany(p => p.CptRltdInfo)
                    .HasForeignKey(d => d.CptPlnSrvcTierId)
                    .HasConstraintName("FK01_CPT_RLTD_INFO");
            });

            modelBuilder.Entity<CptScndyTprt>(entity =>
            {
                entity.ToTable("CPT_SCNDY_TPRT", "dbo");

                entity.Property(e => e.CptScndyTprtId).HasColumnName("CPT_SCNDY_TPRT_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptPlnSrvcTierId).HasColumnName("CPT_PLN_SRVC_TIER_ID");

                entity.Property(e => e.CptPrimScndyTprtId).HasColumnName("CPT_PRIM_SCNDY_TPRT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptScndyTprt)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_SCNDY_TPRT");

                entity.HasOne(d => d.CptPlnSrvcTier)
                    .WithMany(p => p.CptScndyTprt)
                    .HasForeignKey(d => d.CptPlnSrvcTierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_CPT_SCNDY_TPRT");

                entity.HasOne(d => d.CptPrimScndyTprt)
                    .WithMany(p => p.CptScndyTprt)
                    .HasForeignKey(d => d.CptPrimScndyTprtId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_SCNDY_TPRT");
            });

            modelBuilder.Entity<CptSrvcType>(entity =>
            {
                entity.ToTable("CPT_SRVC_TYPE", "dbo");

                entity.Property(e => e.CptSrvcTypeId).HasColumnName("CPT_SRVC_TYPE_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptPlnSrvcTypeId).HasColumnName("CPT_PLN_SRVC_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptSrvcType)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_SRVC_TYPE");

                entity.HasOne(d => d.CptPlnSrvcType)
                    .WithMany(p => p.CptSrvcType)
                    .HasForeignKey(d => d.CptPlnSrvcTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_SRVC_TYPE");
            });

            modelBuilder.Entity<CptSuprtTier>(entity =>
            {
                entity.ToTable("CPT_SUPRT_TIER", "dbo");

                entity.Property(e => e.CptSuprtTierId).HasColumnName("CPT_SUPRT_TIER_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptMdsSuprtTierId).HasColumnName("CPT_MDS_SUPRT_TIER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptSuprtTier)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_SUPRT_TIER");

                entity.HasOne(d => d.CptMdsSuprtTier)
                    .WithMany(p => p.CptSuprtTier)
                    .HasForeignKey(d => d.CptMdsSuprtTierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_SUPRT_TIER");
            });

            modelBuilder.Entity<CptUserAsmt>(entity =>
            {
                entity.ToTable("CPT_USER_ASMT", "dbo");

                entity.HasIndex(e => e.CptId)
                    .HasName("IX01_CPT_USER_ASMT");

                entity.Property(e => e.CptUserAsmtId).HasColumnName("CPT_USER_ASMT_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CptUserId).HasColumnName("CPT_USER_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.RoleId).HasColumnName("ROLE_ID");

                entity.Property(e => e.UsrPrfId).HasColumnName("USR_PRF_ID");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.CptUserAsmt)
                    .HasForeignKey(d => d.CptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CPT_USER_ASMT");

                entity.HasOne(d => d.CptUser)
                    .WithMany(p => p.CptUserAsmtCptUser)
                    .HasForeignKey(d => d.CptUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_CPT_USER_ASMT");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.CptUserAsmtCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CPT_USER_ASMT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.CptUserAsmtModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_CPT_USER_ASMT");

                entity.HasOne(d => d.UsrPrf)
                    .WithMany(p => p.CptUserAsmt)
                    .HasForeignKey(d => d.UsrPrfId)
                    .HasConstraintName("FK05_CPT_USER_ASMT");
            });

            modelBuilder.Entity<CustScrdData>(entity =>
            {
                entity.ToTable("CUST_SCRD_DATA", "dbo");

                entity.HasIndex(e => new { e.ScrdObjId, e.ScrdObjTypeId })
                    .HasName("UX01_CUST_SCRD_DATA")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BldgNme).HasColumnName("BLDG_NME");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryCd).HasColumnName("CTRY_CD");

                entity.Property(e => e.CtryRgnNme).HasColumnName("CTRY_RGN_NME");

                entity.Property(e => e.CtyNme).HasColumnName("CTY_NME");

                entity.Property(e => e.CustCntctCellPhnNbr).HasColumnName("CUST_CNTCT_CELL_PHN_NBR");

                entity.Property(e => e.CustCntctNme).HasColumnName("CUST_CNTCT_NME");

                entity.Property(e => e.CustCntctPgrNbr).HasColumnName("CUST_CNTCT_PGR_NBR");

                entity.Property(e => e.CustCntctPgrPinNbr).HasColumnName("CUST_CNTCT_PGR_PIN_NBR");

                entity.Property(e => e.CustCntctPhnNbr).HasColumnName("CUST_CNTCT_PHN_NBR");

                entity.Property(e => e.CustEmailAdr).HasColumnName("CUST_EMAIL_ADR");

                entity.Property(e => e.CustNme).HasColumnName("CUST_NME");

                entity.Property(e => e.EventDes).HasColumnName("EVENT_DES");

                entity.Property(e => e.EventTitleTxt).HasColumnName("EVENT_TITLE_TXT");

                entity.Property(e => e.FlrId).HasColumnName("FLR_ID");

                entity.Property(e => e.FrstNme).HasColumnName("FRST_NME");

                entity.Property(e => e.LstNme).HasColumnName("LST_NME");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.NteTxt).HasColumnName("NTE_TXT");

                entity.Property(e => e.RmNbr).HasColumnName("RM_NBR");

                entity.Property(e => e.ScrdObjId).HasColumnName("SCRD_OBJ_ID");

                entity.Property(e => e.ScrdObjTypeId).HasColumnName("SCRD_OBJ_TYPE_ID");

                entity.Property(e => e.SiteAdr).HasColumnName("SITE_ADR");

                entity.Property(e => e.SrvcAssrnPocCellPhnNbr).HasColumnName("SRVC_ASSRN_POC_CELL_PHN_NBR");

                entity.Property(e => e.SrvcAssrnPocNme).HasColumnName("SRVC_ASSRN_POC_NME");

                entity.Property(e => e.SrvcAssrnPocPhnNbr).HasColumnName("SRVC_ASSRN_POC_PHN_NBR");

                entity.Property(e => e.StreetAdr1).HasColumnName("STREET_ADR_1");

                entity.Property(e => e.StreetAdr2).HasColumnName("STREET_ADR_2");

                entity.Property(e => e.StreetAdr3).HasColumnName("STREET_ADR_3");

                entity.Property(e => e.SttCd).HasColumnName("STT_CD");

                entity.Property(e => e.SttPrvnNme).HasColumnName("STT_PRVN_NME");

                entity.Property(e => e.ZipPstlCd).HasColumnName("ZIP_PSTL_CD");

                entity.HasOne(d => d.ScrdObjType)
                    .WithMany(p => p.CustScrdData)
                    .HasForeignKey(d => d.ScrdObjTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_CUST_SCRD_DATA");
            });

            modelBuilder.Entity<DsplVw>(entity =>
            {
                entity.ToTable("DSPL_VW", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX10_DSPL_VW");

                entity.HasIndex(e => e.FiltrCol1Id)
                    .HasName("IX04_DSPL_VW");

                entity.HasIndex(e => e.FiltrCol1OprId)
                    .HasName("IX02_DSPL_VW");

                entity.HasIndex(e => e.FiltrCol2Id)
                    .HasName("IX05_DSPL_VW");

                entity.HasIndex(e => e.FiltrCol2OprId)
                    .HasName("IX03_DSPL_VW");

                entity.HasIndex(e => e.FiltrLogicOprId)
                    .HasName("IX14_DSPL_VW");

                entity.HasIndex(e => e.GrpByCol1Id)
                    .HasName("IX08_DSPL_VW");

                entity.HasIndex(e => e.GrpByCol2Id)
                    .HasName("IX09_DSPL_VW");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX11_DSPL_VW");

                entity.HasIndex(e => e.SiteCntntColVwCd)
                    .HasName("IX12_DSPL_VW");

                entity.HasIndex(e => e.SortByCol1Id)
                    .HasName("IX06_DSPL_VW");

                entity.HasIndex(e => e.SortByCol2Id)
                    .HasName("IX07_DSPL_VW");

                entity.HasIndex(e => new { e.UserId, e.DsplVwNme, e.SiteCntntId })
                    .HasName("UX01_DSPL_VW")
                    .IsUnique();

                entity.Property(e => e.DsplVwId)
                    .HasColumnName("DSPL_VW_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DfltVwCd)
                    .IsRequired()
                    .HasColumnName("DFLT_VW_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.DsplVwDes)
                    .HasColumnName("DSPL_VW_DES")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DsplVwNme)
                    .IsRequired()
                    .HasColumnName("DSPL_VW_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DsplVwWebAdr)
                    .HasColumnName("DSPL_VW_WEB_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FiltrCol1Id).HasColumnName("FILTR_COL_1_ID");

                entity.Property(e => e.FiltrCol1OprId)
                    .HasColumnName("FILTR_COL_1_OPR_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FiltrCol1ValuTxt)
                    .HasColumnName("FILTR_COL_1_VALU_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FiltrCol2Id).HasColumnName("FILTR_COL_2_ID");

                entity.Property(e => e.FiltrCol2OprId)
                    .HasColumnName("FILTR_COL_2_OPR_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FiltrCol2ValuTxt)
                    .HasColumnName("FILTR_COL_2_VALU_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FiltrLogicOprId)
                    .HasColumnName("FILTR_LOGIC_OPR_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FiltrOnCd)
                    .IsRequired()
                    .HasColumnName("FILTR_ON_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.GrpByCol1Id).HasColumnName("GRP_BY_COL_1_ID");

                entity.Property(e => e.GrpByCol2Id).HasColumnName("GRP_BY_COL_2_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PblcVwCd)
                    .IsRequired()
                    .HasColumnName("PBLC_VW_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.SeqNbr)
                    .HasColumnName("SEQ_NBR")
                    .HasDefaultValueSql("((99))");

                entity.Property(e => e.SiteCntntColVwCd)
                    .IsRequired()
                    .HasColumnName("SITE_CNTNT_COL_VW_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.SiteCntntId).HasColumnName("SITE_CNTNT_ID");

                entity.Property(e => e.SortByCol1AscOrdrCd)
                    .IsRequired()
                    .HasColumnName("SORT_BY_COL_1_ASC_ORDR_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.SortByCol1Id).HasColumnName("SORT_BY_COL_1_ID");

                entity.Property(e => e.SortByCol2AscOrdrCd)
                    .IsRequired()
                    .HasColumnName("SORT_BY_COL_2_ASC_ORDR_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.SortByCol2Id).HasColumnName("SORT_BY_COL_2_ID");

                entity.Property(e => e.UserId).HasColumnName("USER_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.DsplVwCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_DSPL_VW");

                entity.HasOne(d => d.FiltrCol1Opr)
                    .WithMany(p => p.DsplVwFiltrCol1Opr)
                    .HasForeignKey(d => d.FiltrCol1OprId)
                    .HasConstraintName("FK05_DSPL_VW");

                entity.HasOne(d => d.FiltrCol2Opr)
                    .WithMany(p => p.DsplVwFiltrCol2Opr)
                    .HasForeignKey(d => d.FiltrCol2OprId)
                    .HasConstraintName("FK06_DSPL_VW");

                entity.HasOne(d => d.FiltrLogicOpr)
                    .WithMany(p => p.DsplVwFiltrLogicOpr)
                    .HasForeignKey(d => d.FiltrLogicOprId)
                    .HasConstraintName("FK14_DSPL_VW");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.DsplVwModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK01_DSPL_VW");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DsplVwUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_DSPL_VW");
            });

            modelBuilder.Entity<DsplVwCol>(entity =>
            {
                entity.HasKey(e => new { e.DsplVwId, e.DsplColId });

                entity.ToTable("DSPL_VW_COL", "dbo");

                entity.Property(e => e.DsplVwId).HasColumnName("DSPL_VW_ID");

                entity.Property(e => e.DsplColId).HasColumnName("DSPL_COL_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DsplPosFromLeftNbr).HasColumnName("DSPL_POS_FROM_LEFT_NBR");

                entity.HasOne(d => d.DsplVw)
                    .WithMany(p => p.DsplVwCol)
                    .HasForeignKey(d => d.DsplVwId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_DSPL_VW_COL");
            });

            modelBuilder.Entity<EmailReq>(entity =>
            {
                entity.ToTable("EMAIL_REQ", "dbo");

                entity.HasIndex(e => e.EmailReqTypeId)
                    .HasName("IX03_EMAIL_REQ");

                entity.HasIndex(e => e.EventId)
                    .HasName("IX04_EMAIL_REQ");

                entity.HasIndex(e => e.OrdrId)
                    .HasName("IX02_EMAIL_REQ");

                entity.HasIndex(e => e.StusId)
                    .HasName("IX01_EMAIL_REQ");

                entity.Property(e => e.EmailReqId).HasColumnName("EMAIL_REQ_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailBodyTxt)
                    .HasColumnName("EMAIL_BODY_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.EmailCcTxt)
                    .HasColumnName("EMAIL_CC_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.EmailListTxt)
                    .HasColumnName("EMAIL_LIST_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.EmailReqTypeId).HasColumnName("EMAIL_REQ_TYPE_ID");

                entity.Property(e => e.EmailSubjTxt)
                    .HasColumnName("EMAIL_SUBJ_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.RedsgnId).HasColumnName("REDSGN_ID");

                entity.Property(e => e.SentDt)
                    .HasColumnName("SENT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.StusId).HasColumnName("STUS_ID");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.EmailReq)
                    .HasForeignKey(d => d.CptId)
                    .HasConstraintName("FK07_EMAIL_REQ");

                entity.HasOne(d => d.EmailReqType)
                    .WithMany(p => p.EmailReq)
                    .HasForeignKey(d => d.EmailReqTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_EMAIL_REQ");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EmailReq)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK05_EMAIL_REQ");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.EmailReq)
                    .HasForeignKey(d => d.OrdrId)
                    .HasConstraintName("FK01_EMAIL_REQ");

                entity.HasOne(d => d.Redsgn)
                    .WithMany(p => p.EmailReq)
                    .HasForeignKey(d => d.RedsgnId)
                    .HasConstraintName("FK06_EMAIL_REQ");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.EmailReq)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_EMAIL_REQ");
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity.ToTable("EVENT", "dbo");

                entity.HasIndex(e => e.EventTypeId)
                    .HasName("IX01_EVENT");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.Event)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT");
            });

            modelBuilder.Entity<EventAsnToUser>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.AsnToUserId, e.RoleId });

                entity.ToTable("EVENT_ASN_TO_USER", "dbo");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_EVENT_ASN_TO_USER");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.AsnToUserId).HasColumnName("ASN_TO_USER_ID");

                entity.Property(e => e.RoleId)
                    .HasColumnName("ROLE_ID")
                    .HasDefaultValueSql("((23))");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.AsnToUser)
                    .WithMany(p => p.EventAsnToUser)
                    .HasForeignKey(d => d.AsnToUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_ASN_TO_USER");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventAsnToUser)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_EVENT_ASN_TO_USER");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.EventAsnToUser)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_EVENT_ASN_TO_USER");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.EventAsnToUser)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_EVENT_ASN_TO_USER");
            });

            modelBuilder.Entity<EventCpeDev>(entity =>
            {
                entity.ToTable("EVENT_CPE_DEV", "dbo");

                entity.HasIndex(e => e.CpeOrdrNbr)
                    .HasName("IX02_EVENT_CPE_DEV");

                entity.HasIndex(e => new { e.CpeOrdrNbr, e.RecStusId, e.EventId })
                    .HasName("IX01_EVENT_CPE_DEV");

                entity.Property(e => e.EventCpeDevId).HasColumnName("EVENT_CPE_DEV_ID");

                entity.Property(e => e.AssocH6)
                    .HasColumnName("ASSOC_H6")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Ccd)
                    .HasColumnName("CCD")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CpeOrdrId)
                    .HasColumnName("CPE_ORDR_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CpeOrdrNbr)
                    .HasColumnName("CPE_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeviceId)
                    .IsRequired()
                    .HasColumnName("DEVICE_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.MaintVndrPo)
                    .HasColumnName("MAINT_VNDR_PO")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OdieCd).HasColumnName("ODIE_CD");

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrStus)
                    .HasColumnName("ORDR_STUS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RcptStus)
                    .HasColumnName("RCPT_STUS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReqstnNbr)
                    .HasColumnName("REQSTN_NBR")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventCpeDev)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_CPE_DEV");
            });

            modelBuilder.Entity<EventDevCmplt>(entity =>
            {
                entity.ToTable("EVENT_DEV_CMPLT", "dbo");

                entity.HasIndex(e => e.EventId)
                    .HasName("IX01_EVENT_DEV_CMPLT");

                entity.HasIndex(e => e.RedsgnDevId)
                    .HasName("IX02_EVENT_DEV_CMPLT");

                entity.Property(e => e.EventDevCmpltId).HasColumnName("EVENT_DEV_CMPLT_ID");

                entity.Property(e => e.CmpltdCd).HasColumnName("CMPLTD_CD");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.H6)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OdieSentDt)
                    .HasColumnName("ODIE_SENT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RedsgnDevId).HasColumnName("REDSGN_DEV_ID");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventDevCmplt)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_DEV_CMPLT");
            });

            modelBuilder.Entity<EventDevSrvcMgmt>(entity =>
            {
                entity.ToTable("EVENT_DEV_SRVC_MGMT", "dbo");

                entity.HasIndex(e => e.EventId)
                    .HasName("IX01_EVENT_DEV_SRVC_MGMT");

                entity.HasIndex(e => e.MnsOrdrNbr)
                    .HasName("IX02_EVENT_DEV_SRVC_MGMT");

                entity.Property(e => e.EventDevSrvcMgmtId).HasColumnName("EVENT_DEV_SRVC_MGMT_ID");

                entity.Property(e => e.CmpntNme)
                    .HasColumnName("CMPNT_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CmpntStus)
                    .HasColumnName("CMPNT_STUS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CmpntTypeCd)
                    .HasColumnName("CMPNT_TYPE_CD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("DEVICE_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.M5OrdrCmpntId)
                    .HasColumnName("M5_ORDR_CMPNT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MnsOrdrNbr)
                    .HasColumnName("MNS_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MnsSrvcTierId).HasColumnName("MNS_SRVC_TIER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OdieCd).HasColumnName("ODIE_CD");

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventDevSrvcMgmt)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_DEV_SRVC_MGMT");

                entity.HasOne(d => d.MnsSrvcTier)
                    .WithMany(p => p.EventDevSrvcMgmt)
                    .HasForeignKey(d => d.MnsSrvcTierId)
                    .HasConstraintName("FK02_EVENT_DEV_SRVC_MGMT");
            });

            modelBuilder.Entity<EventDiscoDev>(entity =>
            {
                entity.ToTable("EVENT_DISCO_DEV", "dbo");

                entity.Property(e => e.EventDiscoDevId).HasColumnName("EVENT_DISCO_DEV_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DevModelId).HasColumnName("DEV_MODEL_ID");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("DEVICE_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.H5H6)
                    .HasColumnName("H5_H6")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.ManfId).HasColumnName("MANF_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OptInCktCd)
                    .HasColumnName("OPT_IN_CKT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OptInHCd)
                    .HasColumnName("OPT_IN_H_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReadyBeginCd)
                    .HasColumnName("READY_BEGIN_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SerialNbr)
                    .HasColumnName("SERIAL_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasColumnName("SITE_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ThrdPartyCtrct)
                    .HasColumnName("THRD_PARTY_CTRCT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.DevModel)
                    .WithMany(p => p.EventDiscoDev)
                    .HasForeignKey(d => d.DevModelId)
                    .HasConstraintName("FK02_EVENT_DISCO_DEV");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventDiscoDev)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_DISCO_DEV");

                entity.HasOne(d => d.Manf)
                    .WithMany(p => p.EventDiscoDev)
                    .HasForeignKey(d => d.ManfId)
                    .HasConstraintName("FK03_EVENT_DISCO_DEV");
            });

            modelBuilder.Entity<EventFailActy>(entity =>
            {
                entity.HasKey(e => new { e.EventHistId, e.FailActyId, e.RecStusId });

                entity.ToTable("EVENT_FAIL_ACTY", "dbo");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_EVENT_FAIL_ACTY");

                entity.Property(e => e.EventHistId).HasColumnName("EVENT_HIST_ID");

                entity.Property(e => e.FailActyId).HasColumnName("FAIL_ACTY_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.EventHist)
                    .WithMany(p => p.EventFailActy)
                    .HasForeignKey(d => d.EventHistId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_EVENT_FAIL_ACTY");

                entity.HasOne(d => d.FailActy)
                    .WithMany(p => p.EventFailActy)
                    .HasForeignKey(d => d.FailActyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_EVENT_FAIL_ACTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.EventFailActy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_FAIL_ACTY");
            });

            modelBuilder.Entity<EventHist>(entity =>
            {
                entity.ToTable("EVENT_HIST", "dbo");

                entity.HasIndex(e => e.ActnId)
                    .HasName("IX05_EVENT_HIST");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_EVENT_HIST");

                entity.HasIndex(e => e.EventId)
                    .HasName("IX04_EVENT_HIST");

                entity.HasIndex(e => e.FailReasId)
                    .HasName("IX03_EVENT_HIST");

                entity.HasIndex(e => e.FsaMdsEventId)
                    .HasName("IX06_EVENT_HIST");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_EVENT_HIST");

                entity.Property(e => e.EventHistId).HasColumnName("EVENT_HIST_ID");

                entity.Property(e => e.ActnId).HasColumnName("ACTN_ID");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventEndTmst)
                    .HasColumnName("EVENT_END_TMST")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.EventStrtTmst)
                    .HasColumnName("EVENT_STRT_TMST")
                    .HasColumnType("datetime");

                entity.Property(e => e.FailReasId).HasColumnName("FAIL_REAS_ID");

                entity.Property(e => e.FsaMdsEventId).HasColumnName("FSA_MDS_EVENT_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.PreCfgCmpltCd)
                    .HasColumnName("PRE_CFG_CMPLT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.HasOne(d => d.Actn)
                    .WithMany(p => p.EventHist)
                    .HasForeignKey(d => d.ActnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_EVENT_HIST");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.EventHistCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_EVENT_HIST");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventHist)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_EVENT_HIST");

                entity.HasOne(d => d.FailReas)
                    .WithMany(p => p.EventHist)
                    .HasForeignKey(d => d.FailReasId)
                    .HasConstraintName("FK05_EVENT_HIST");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.EventHistModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK01_EVENT_HIST");
            });

            modelBuilder.Entity<EventNvldTmeSlot>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("EVENT_NVLD_TME_SLOT", "dbo");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.EventNvldTmeSlot)
                    .HasForeignKey<EventNvldTmeSlot>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_NVLD_TME_SLOT");
            });

            modelBuilder.Entity<EventRecLock>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("EVENT_REC_LOCK", "dbo");

                entity.HasIndex(e => e.LockByUserId)
                    .HasName("IX01_EVENT_REC_LOCK");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LockByUserId).HasColumnName("LOCK_BY_USER_ID");

                entity.Property(e => e.StrtRecLockTmst)
                    .HasColumnName("STRT_REC_LOCK_TMST")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.EventRecLock)
                    .HasForeignKey<EventRecLock>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_EVENT_REC_LOCK");

                entity.HasOne(d => d.LockByUser)
                    .WithMany(p => p.EventRecLock)
                    .HasForeignKey(d => d.LockByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_REC_LOCK");
            });

            modelBuilder.Entity<EventRuleData>(entity =>
            {
                entity.ToTable("EVENT_RULE_DATA", "dbo");

                entity.HasIndex(e => e.EventRuleId)
                    .HasName("IX01_EVENT_RULE_DATA");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailSubj)
                    .HasColumnName("EMAIL_SUBJ")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.EmailactCd).HasColumnName("EMAILACT_CD");

                entity.Property(e => e.EmailccAdr)
                    .HasColumnName("EMAILCC_ADR")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.EmailcmplccCd).HasColumnName("EMAILCMPLCC_CD");

                entity.Property(e => e.EmailmemCd).HasColumnName("EMAILMEM_CD");

                entity.Property(e => e.EmailoldactCd).HasColumnName("EMAILOLDACT_CD");

                entity.Property(e => e.EmailpubccCd).HasColumnName("EMAILPUBCC_CD");

                entity.Property(e => e.EmailrevCd).HasColumnName("EMAILREV_CD");

                entity.Property(e => e.EventDataCd)
                    .HasColumnName("EVENT_DATA_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.EventRuleId).HasColumnName("EVENT_RULE_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.EventRule)
                    .WithMany(p => p.EventRuleData)
                    .HasForeignKey(d => d.EventRuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_EVENT_RULE_DATA");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.EventRuleData)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_RULE_DATA");
            });

            modelBuilder.Entity<EventSucssActy>(entity =>
            {
                entity.HasKey(e => new { e.EventHistId, e.SucssActyId, e.RecStusId });

                entity.ToTable("EVENT_SUCSS_ACTY", "dbo");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_EVENT_SUCSS_ACTY");

                entity.Property(e => e.EventHistId).HasColumnName("EVENT_HIST_ID");

                entity.Property(e => e.SucssActyId).HasColumnName("SUCSS_ACTY_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.EventHist)
                    .WithMany(p => p.EventSucssActy)
                    .HasForeignKey(d => d.EventHistId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_EVENT_SUCSS_ACTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.EventSucssActy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_EVENT_SUCSS_ACTY");

                entity.HasOne(d => d.SucssActy)
                    .WithMany(p => p.EventSucssActy)
                    .HasForeignKey(d => d.SucssActyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_EVENT_SUCSS_ACTY");
            });

            modelBuilder.Entity<EventTypeRsrcAvlblty>(entity =>
            {
                entity.ToTable("EVENT_TYPE_RSRC_AVLBLTY", "dbo");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX01_EVENT_TYPE_RSRC_AVLBLTY");

                entity.HasIndex(e => new { e.EventTypeId, e.DayDt, e.TmeSlotId })
                    .HasName("UX01_EVENT_TYPE_RSRC_AVLBLTY")
                    .IsUnique();

                entity.Property(e => e.EventTypeRsrcAvlbltyId)
                    .HasColumnName("EVENT_TYPE_RSRC_AVLBLTY_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AvalTmeSlotCnt).HasColumnName("AVAL_TME_SLOT_CNT");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CurrAvalTmeSlotCnt).HasColumnName("CURR_AVAL_TME_SLOT_CNT");

                entity.Property(e => e.DayDt)
                    .HasColumnName("DAY_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EventIdListTxt)
                    .HasColumnName("EVENT_ID_LIST_TXT")
                    .HasColumnType("xml");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.MaxAvalTmeSlotCapCnt).HasColumnName("MAX_AVAL_TME_SLOT_CAP_CNT");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.TmeSlotId).HasColumnName("TME_SLOT_ID");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.EventTypeRsrcAvlblty)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_EVENT_TYPE_RSRC_AVLBLTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.EventTypeRsrcAvlblty)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK01_EVENT_TYPE_RSRC_AVLBLTY");

                entity.HasOne(d => d.LkEventTypeTmeSlot)
                    .WithMany(p => p.EventTypeRsrcAvlblty)
                    .HasForeignKey(d => new { d.TmeSlotId, d.EventTypeId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_EVENT_TYPE_RSRC_AVLBLTY");
            });

            modelBuilder.Entity<FedlineEventAdr>(entity =>
            {
                entity.ToTable("FEDLINE_EVENT_ADR", "dbo");

                entity.HasIndex(e => new { e.FedlineEventId, e.AdrTypeId })
                    .HasName("UX01_FEDLINE_EVENT_ADR")
                    .IsUnique();

                entity.Property(e => e.FedlineEventAdrId).HasColumnName("FEDLINE_EVENT_ADR_ID");

                entity.Property(e => e.AdrTypeId).HasColumnName("ADR_TYPE_ID");

                entity.Property(e => e.CityNme).HasColumnName("CITY_NME");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.FedlineEventId).HasColumnName("FEDLINE_EVENT_ID");

                entity.Property(e => e.PrvnNme).HasColumnName("PRVN_NME");

                entity.Property(e => e.Street1Adr).HasColumnName("STREET_1_ADR");

                entity.Property(e => e.Street2Adr).HasColumnName("STREET_2_ADR");

                entity.Property(e => e.SttCd).HasColumnName("STT_CD");

                entity.Property(e => e.ZipCd).HasColumnName("ZIP_CD");

                entity.HasOne(d => d.AdrType)
                    .WithMany(p => p.FedlineEventAdr)
                    .HasForeignKey(d => d.AdrTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_FEDLINE_EVENT_ADR");

                entity.HasOne(d => d.FedlineEvent)
                    .WithMany(p => p.FedlineEventAdr)
                    .HasForeignKey(d => d.FedlineEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_EVENT_ADR");
            });

            modelBuilder.Entity<FedlineEventCfgrnData>(entity =>
            {
                entity.HasKey(e => e.FedlineEventCfgrnId);

                entity.ToTable("FEDLINE_EVENT_CFGRN_DATA", "dbo");

                entity.HasIndex(e => new { e.FedlineEventId, e.PortNme })
                    .HasName("UX01_FEDLINE_EVENT_CFGRN_DATA")
                    .IsUnique();

                entity.Property(e => e.FedlineEventCfgrnId).HasColumnName("FEDLINE_EVENT_CFGRN_ID");

                entity.Property(e => e.DfltGtwyAdr)
                    .HasColumnName("DFLT_GTWY_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.DxNme)
                    .HasColumnName("DX_NME")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.FedlineEventId).HasColumnName("FEDLINE_EVENT_ID");

                entity.Property(e => e.IpAdr)
                    .IsRequired()
                    .HasColumnName("IP_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.MacAdr)
                    .HasColumnName("MAC_ADR")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.PortNme)
                    .IsRequired()
                    .HasColumnName("PORT_NME")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SpeedNme)
                    .HasColumnName("SPEED_NME")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SubnetMaskAdr)
                    .HasColumnName("SUBNET_MASK_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.FedlineEvent)
                    .WithMany(p => p.FedlineEventCfgrnData)
                    .HasForeignKey(d => d.FedlineEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_EVENT_CFGRN_DATA");
            });

            modelBuilder.Entity<FedlineEventCntct>(entity =>
            {
                entity.ToTable("FEDLINE_EVENT_CNTCT", "dbo");

                entity.HasIndex(e => new { e.FedlineEventId, e.RoleId, e.CntctTypeId })
                    .HasName("UX01_FEDLINE_EVENT_CNTCT")
                    .IsUnique();

                entity.Property(e => e.FedlineEventCntctId).HasColumnName("FEDLINE_EVENT_CNTCT_ID");

                entity.Property(e => e.CntctNme).HasColumnName("CNTCT_NME");

                entity.Property(e => e.CntctTypeId).HasColumnName("CNTCT_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailAdr).HasColumnName("EMAIL_ADR");

                entity.Property(e => e.FedlineEventId).HasColumnName("FEDLINE_EVENT_ID");

                entity.Property(e => e.PhnExtNbr)
                    .HasColumnName("PHN_EXT_NBR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PhnNbr)
                    .HasColumnName("PHN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("ROLE_ID");

                entity.HasOne(d => d.CntctType)
                    .WithMany(p => p.FedlineEventCntct)
                    .HasForeignKey(d => d.CntctTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_FEDLINE_EVENT_CNTCT");

                entity.HasOne(d => d.FedlineEvent)
                    .WithMany(p => p.FedlineEventCntct)
                    .HasForeignKey(d => d.FedlineEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_EVENT_CNTCT");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.FedlineEventCntct)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_FEDLINE_EVENT_CNTCT");
            });

            modelBuilder.Entity<FedlineEventMsg>(entity =>
            {
                entity.ToTable("FEDLINE_EVENT_MSG", "dbo");

                entity.Property(e => e.FedlineEventMsgId).HasColumnName("FEDLINE_EVENT_MSG_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FedlineEventId).HasColumnName("FEDLINE_EVENT_ID");

                entity.Property(e => e.FedlineEventMsgStusNme)
                    .IsRequired()
                    .HasColumnName("FEDLINE_EVENT_MSG_STUS_NME")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.RspnDes)
                    .HasColumnName("RSPN_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SentDt)
                    .HasColumnName("SENT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.StusId).HasColumnName("STUS_ID");

                entity.HasOne(d => d.FedlineEvent)
                    .WithMany(p => p.FedlineEventMsg)
                    .HasForeignKey(d => d.FedlineEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_EVENT_MSG");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.FedlineEventMsg)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_FEDLINE_EVENT_MSG");
            });

            modelBuilder.Entity<FedlineEventPrcs>(entity =>
            {
                entity.ToTable("FEDLINE_EVENT_PRCS", "dbo");

                entity.Property(e => e.FedlineEventPrcsId).HasColumnName("FEDLINE_EVENT_PRCS_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndTme)
                    .HasColumnName("END_TME")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EventEndStusId).HasColumnName("EVENT_END_STUS_ID");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.StrtTme)
                    .HasColumnName("STRT_TME")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.EventEndStus)
                    .WithMany(p => p.FedlineEventPrcsEventEndStus)
                    .HasForeignKey(d => d.EventEndStusId)
                    .HasConstraintName("FK03_FEDLINE_EVENT_PRCS");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.FedlineEventPrcs)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_EVENT_PRCS");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.FedlineEventPrcsEventStus)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_FEDLINE_EVENT_PRCS");
            });

            modelBuilder.Entity<FedlineEventTadpoleData>(entity =>
            {
                entity.HasKey(e => e.FedlineEventId);

                entity.ToTable("FEDLINE_EVENT_TADPOLE_DATA", "dbo");

                entity.HasIndex(e => e.CreatDt)
                    .HasName("IX01_FEDLINE_EVENT_TADPOLE_DATA");

                entity.HasIndex(e => new { e.FrbReqId, e.SeqNbr })
                    .HasName("UX01_FEDLINE_EVENT_TADPOLE_DATA")
                    .IsUnique();

                entity.Property(e => e.FedlineEventId).HasColumnName("FEDLINE_EVENT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustNme).HasColumnName("CUST_NME");

                entity.Property(e => e.DevModelNbr)
                    .HasColumnName("DEV_MODEL_NBR")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DevNme)
                    .IsRequired()
                    .HasColumnName("DEV_NME")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.DevSerialNbr)
                    .IsRequired()
                    .HasColumnName("DEV_SERIAL_NBR")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.DevVndrNme)
                    .HasColumnName("DEV_VNDR_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DsgnTypeCd)
                    .HasColumnName("DSGN_TYPE_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EndTme)
                    .HasColumnName("END_TME")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.FrbInstlTypeNme)
                    .HasColumnName("FRB_INSTL_TYPE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FrbOrdrTypeCd)
                    .IsRequired()
                    .HasColumnName("FRB_ORDR_TYPE_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.FrbReqId).HasColumnName("FRB_REQ_ID");

                entity.Property(e => e.IsExpediteCd).HasColumnName("IS_EXPEDITE_CD");

                entity.Property(e => e.IsMoveCd).HasColumnName("IS_MOVE_CD");

                entity.Property(e => e.OrdrSubTypeNme)
                    .HasColumnName("ORDR_SUB_TYPE_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrTypeCd)
                    .IsRequired()
                    .HasColumnName("ORDR_TYPE_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.OrgId)
                    .IsRequired()
                    .HasColumnName("ORG_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.OrigDevId)
                    .HasColumnName("ORIG_DEV_ID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.OrigDevSerialNbr)
                    .HasColumnName("ORIG_DEV_SERIAL_NBR")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OrigFrbReqId).HasColumnName("ORIG_FRB_REQ_ID");

                entity.Property(e => e.OrigModel)
                    .HasColumnName("ORIG_MODEL")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.ReplmtReasNme)
                    .HasColumnName("REPLMT_REAS_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ReqRecvDt)
                    .HasColumnName("REQ_RECV_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SeqNbr).HasColumnName("SEQ_NBR");

                entity.Property(e => e.StrtTme)
                    .HasColumnName("STRT_TME")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.XstDevNme)
                    .HasColumnName("XST_DEV_NME")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.FedlineEventTadpoleData)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_EVENT_TADPOLE_DATA");

                entity.HasOne(d => d.FrbOrdrTypeCdNavigation)
                    .WithMany(p => p.FedlineEventTadpoleDataFrbOrdrTypeCdNavigation)
                    .HasForeignKey(d => d.FrbOrdrTypeCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_FEDLINE_EVENT_TADPOLE_DATA");

                entity.HasOne(d => d.OrdrTypeCdNavigation)
                    .WithMany(p => p.FedlineEventTadpoleDataOrdrTypeCdNavigation)
                    .HasForeignKey(d => d.OrdrTypeCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_FEDLINE_EVENT_TADPOLE_DATA");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.FedlineEventTadpoleData)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_FEDLINE_EVENT_TADPOLE_DATA");
            });

            modelBuilder.Entity<FedlineEventTadpoleXml>(entity =>
            {
                entity.HasKey(e => e.MsgXmlId);

                entity.ToTable("FEDLINE_EVENT_TADPOLE_XML", "dbo");

                entity.Property(e => e.MsgXmlId).HasColumnName("MSG_XML_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ErrorMsgTxt)
                    .HasColumnName("ERROR_MSG_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.FrbReqId).HasColumnName("FRB_REQ_ID");

                entity.Property(e => e.IsErrorCd).HasColumnName("IS_ERROR_CD");

                entity.Property(e => e.IsRejectCd).HasColumnName("IS_REJECT_CD");

                entity.Property(e => e.MsgXmlTxt).HasColumnName("MSG_XML_TXT");
            });

            modelBuilder.Entity<FedlineEventUserData>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("FEDLINE_EVENT_USER_DATA", "dbo");

                entity.HasIndex(e => e.EventStusId)
                    .HasName("IX01_FEDLINE_EVENT_USER_DATA");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActvUserId).HasColumnName("ACTV_USER_ID");

                entity.Property(e => e.ArrivalDt)
                    .HasColumnName("ARRIVAL_DT")
                    .HasColumnType("date");

                entity.Property(e => e.CmpltEmailCcAdr)
                    .HasColumnName("CMPLT_EMAIL_CC_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DevTypeNme)
                    .HasColumnName("DEV_TYPE_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DocLinkTxt)
                    .HasColumnName("DOC_LINK_TXT")
                    .HasMaxLength(1000);

                entity.Property(e => e.EngrUserId).HasColumnName("ENGR_USER_ID");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt).HasColumnName("EVENT_TITLE_TXT");

                entity.Property(e => e.FailCdId).HasColumnName("FAIL_CD_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PreStgCmtltCd).HasColumnName("PRE_STG_CMTLT_CD");

                entity.Property(e => e.RdsnNbr)
                    .HasColumnName("RDSN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingCxrNme)
                    .HasColumnName("SHIPPING_CXR_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SlaCd)
                    .HasColumnName("SLA_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TnnlCd)
                    .HasColumnName("TNNL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TrkNbr)
                    .HasColumnName("TRK_NBR")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.ActvUser)
                    .WithMany(p => p.FedlineEventUserDataActvUser)
                    .HasForeignKey(d => d.ActvUserId)
                    .HasConstraintName("FK04_FEDLINE_EVENT_USER_DATA");

                entity.HasOne(d => d.EngrUser)
                    .WithMany(p => p.FedlineEventUserDataEngrUser)
                    .HasForeignKey(d => d.EngrUserId)
                    .HasConstraintName("FK03_FEDLINE_EVENT_USER_DATA");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.FedlineEventUserData)
                    .HasForeignKey<FedlineEventUserData>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_EVENT_USER_DATA");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.FedlineEventUserData)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_FEDLINE_EVENT_USER_DATA");

                entity.HasOne(d => d.FailCd)
                    .WithMany(p => p.FedlineEventUserData)
                    .HasForeignKey(d => d.FailCdId)
                    .HasConstraintName("FK06_FEDLINE_EVENT_USER_DATA");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.FedlineEventUserDataModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_FEDLINE_EVENT_USER_DATA");
            });

            modelBuilder.Entity<FedlineReqActHist>(entity =>
            {
                entity.HasKey(e => e.ReqActHistId);

                entity.ToTable("FEDLINE_REQ_ACT_HIST", "dbo");

                entity.Property(e => e.ReqActHistId).HasColumnName("REQ_ACT_HIST_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventHistId).HasColumnName("EVENT_HIST_ID");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.FailCdId).HasColumnName("FAIL_CD_ID");

                entity.Property(e => e.NewDt)
                    .HasColumnName("NEW_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.OldDt)
                    .HasColumnName("OLD_DT")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.EventHist)
                    .WithMany(p => p.FedlineReqActHist)
                    .HasForeignKey(d => d.EventHistId)
                    .HasConstraintName("FK02_FEDLINE_REQ_ACT_HIST");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.FedlineReqActHist)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_REQ_ACT_HIST");
            });

            modelBuilder.Entity<FsaOrdr>(entity =>
            {
                entity.HasKey(e => e.OrdrId);

                entity.ToTable("FSA_ORDR", "dbo");

                entity.HasIndex(e => e.InstlSrvcTierCd)
                    .HasName("IX08_FSA_ORDR");

                entity.HasIndex(e => e.OrdrActnId)
                    .HasName("IX01_FSA_ORDR");

                entity.HasIndex(e => e.OrdrTypeCd)
                    .HasName("IX02_FSA_ORDR");

                entity.HasIndex(e => e.ProdTypeCd)
                    .HasName("IX04_FSA_ORDR");

                entity.Property(e => e.OrdrId)
                    .HasColumnName("ORDR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AddlIpAdrCd)
                    .HasColumnName("ADDL_IP_ADR_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Ckt)
                    .HasColumnName("CKT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CktId)
                    .HasColumnName("CKT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CpeAccsPrvdrCd)
                    .HasColumnName("CPE_ACCS_PRVDR_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CpeCpeOrdrTypeCd)
                    .HasColumnName("CPE_CPE_ORDR_TYPE_CD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CpeDlvryDutyAmt)
                    .HasColumnName("CPE_DLVRY_DUTY_AMT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CpeDlvryDutyId)
                    .HasColumnName("CPE_DLVRY_DUTY_ID")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.CpeEccktId)
                    .HasColumnName("CPE_ECCKT_ID")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.CpeEqptOnlyCd)
                    .HasColumnName("CPE_EQPT_ONLY_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CpeMscpCd)
                    .HasColumnName("CPE_MSCP_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CpePhnNbr)
                    .HasColumnName("CPE_PHN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CpePhnNbrTypeCd)
                    .HasColumnName("CPE_PHN_NBR_TYPE_CD")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.CpeRecOnlyCd)
                    .HasColumnName("CPE_REC_ONLY_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CpeShipChgAmt)
                    .HasColumnName("CPE_SHIP_CHG_AMT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CpeTstTnNbr)
                    .HasColumnName("CPE_TST_TN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CpeVndr)
                    .HasColumnName("CPE_VNDR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CpwType)
                    .HasColumnName("CPW_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustAcptErlySrvcCd)
                    .HasColumnName("CUST_ACPT_ERLY_SRVC_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CustCmmtDt)
                    .HasColumnName("CUST_CMMT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustFrwlCd)
                    .HasColumnName("CUST_FRWL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CustOrdrSbmtDt)
                    .HasColumnName("CUST_ORDR_SBMT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustPrmsOcpyCd)
                    .HasColumnName("CUST_PRMS_OCPY_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CustSignedDt)
                    .HasColumnName("CUST_SIGNED_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustWantDt)
                    .HasColumnName("CUST_WANT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CxrAccsCd)
                    .HasColumnName("CXR_ACCS_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CxrCktId)
                    .HasColumnName("CXR_CKT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CxrSrvcId)
                    .HasColumnName("CXR_SRVC_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ddr)
                    .HasColumnName("DDR")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Ddu)
                    .HasColumnName("DDU")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DiscCnclBefStrtReasCd)
                    .HasColumnName("DISC_CNCL_BEF_STRT_REAS_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DiscCnclBefStrtReasDetlTxt)
                    .HasColumnName("DISC_CNCL_BEF_STRT_REAS_DETL_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DiscReasCd)
                    .HasColumnName("DISC_REAS_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DomnNme)
                    .HasColumnName("DOMN_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FsaExpTypeCd)
                    .HasColumnName("FSA_EXP_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.FsaOrgnlInstlOrdrId).HasColumnName("FSA_ORGNL_INSTL_ORDR_ID");

                entity.Property(e => e.Ftn)
                    .IsRequired()
                    .HasColumnName("FTN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GvrmntTypeId)
                    .HasColumnName("GVRMNT_TYPE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.InstlDsgnDocNbr)
                    .HasColumnName("INSTL_DSGN_DOC_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InstlEsclCd)
                    .HasColumnName("INSTL_ESCL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InstlNwTypeCd)
                    .HasColumnName("INSTL_NW_TYPE_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSoluSrvcDes)
                    .HasColumnName("INSTL_SOLU_SRVC_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSrvcTierCd).HasColumnName("INSTL_SRVC_TIER_CD");

                entity.Property(e => e.InstlTrnsprtTypeCd)
                    .HasColumnName("INSTL_TRNSPRT_TYPE_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.InstlVndrCd)
                    .HasColumnName("INSTL_VNDR_CD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IntlPlCatId)
                    .HasColumnName("INTL_PL_CAT_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.IntlPlCktTypeCd)
                    .HasColumnName("INTL_PL_CKT_TYPE_CD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.IntlPlPlusIpCd)
                    .HasColumnName("INTL_PL_PLUS_IP_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IntlPlSrvcTypeCd)
                    .HasColumnName("INTL_PL_SRVC_TYPE_CD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Ipv4SubnetMaskAdr)
                    .HasColumnName("IPV4_SUBNET_MASK_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.MultCustOrdrCd)
                    .HasColumnName("MULT_CUST_ORDR_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MultOrdrIndexNbr).HasColumnName("MULT_ORDR_INDEX_NBR");

                entity.Property(e => e.MultOrdrTotCnt).HasColumnName("MULT_ORDR_TOT_CNT");

                entity.Property(e => e.NrfcPrcolCd)
                    .HasColumnName("NRFC_PRCOL_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Nua)
                    .HasColumnName("NUA")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NwUserAdr)
                    .HasColumnName("NW_USER_ADR")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.OrangeOffrCd)
                    .HasColumnName("ORANGE_OFFR_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrActnId).HasColumnName("ORDR_ACTN_ID");

                entity.Property(e => e.OrdrChngDes)
                    .HasColumnName("ORDR_CHNG_DES")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrSbmtDt)
                    .HasColumnName("ORDR_SBMT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrdrSubTypeCd)
                    .HasColumnName("ORDR_SUB_TYPE_CD")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrTypeCd)
                    .HasColumnName("ORDR_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.OssOptCd)
                    .HasColumnName("OSS_OPT_CD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PlNbr)
                    .HasColumnName("PL_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PortRtTypeCd)
                    .HasColumnName("PORT_RT_TYPE_CD")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.PreQualNbr)
                    .HasColumnName("PRE_QUAL_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PreSbmtCreatDt)
                    .HasColumnName("PRE_SBMT_CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PrntFtn)
                    .HasColumnName("PRNT_FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProdTypeCd)
                    .HasColumnName("PROD_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ReltdFtn)
                    .HasColumnName("RELTD_FTN")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ScaNbr)
                    .HasColumnName("SCA_NBR")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasColumnName("SITE_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SotsId)
                    .HasColumnName("SOTS_ID")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.SpaAccsIndcrDes)
                    .HasColumnName("SPA_ACCS_INDCR_DES")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TportAddlMrcAmt)
                    .HasColumnName("TPORT_ADDL_MRC_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportBearerChgAmt)
                    .HasColumnName("TPORT_BEARER_CHG_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportBrstblPriceDiscCd)
                    .HasColumnName("TPORT_BRSTBL_PRICE_DISC_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportBrstblUsageTypeCd)
                    .HasColumnName("TPORT_BRSTBL_USAGE_TYPE_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportCalcRtMrcInCurAmt)
                    .HasColumnName("TPORT_CALC_RT_MRC_IN_CUR_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportCalcRtMrcUsdAmt)
                    .HasColumnName("TPORT_CALC_RT_MRC_USD_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportCalcRtNrcInCurAmt)
                    .HasColumnName("TPORT_CALC_RT_NRC_IN_CUR_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportCalcRtNrcUsdAmt)
                    .HasColumnName("TPORT_CALC_RT_NRC_USD_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportCnctrTypeId)
                    .HasColumnName("TPORT_CNCTR_TYPE_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TportCnvrsnRtQty)
                    .HasColumnName("TPORT_CNVRSN_RT_QTY")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportCurNme)
                    .HasColumnName("TPORT_CUR_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportCurrInetPrvdrCd)
                    .HasColumnName("TPORT_CURR_INET_PRVDR_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportCustIpAdr)
                    .HasColumnName("TPORT_CUST_IP_ADR")
                    .IsUnicode(false);

                entity.Property(e => e.TportCustPrvddIpAdrIndcr)
                    .HasColumnName("TPORT_CUST_PRVDD_IP_ADR_INDCR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportCustRoutrAutoNegotCd)
                    .HasColumnName("TPORT_CUST_ROUTR_AUTO_NEGOT_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.TportCustRoutrTagTxt)
                    .HasColumnName("TPORT_CUST_ROUTR_TAG_TXT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TportDiaClassOfSrvcTxt)
                    .HasColumnName("TPORT_DIA_CLASS_OF_SRVC_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportDiaNocToNocTxt)
                    .HasColumnName("TPORT_DIA_NOC_TO_NOC_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportDualStkAdrPrvdrTxt)
                    .HasColumnName("TPORT_DUAL_STK_ADR_PRVDR_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportDvstyTypeNme)
                    .HasColumnName("TPORT_DVSTY_TYPE_NME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TportEthAccsTypeCd)
                    .HasColumnName("TPORT_ETH_ACCS_TYPE_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TportEthrntNrfcIndcr)
                    .HasColumnName("TPORT_ETHRNT_NRFC_INDCR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpVerTypeCd)
                    .HasColumnName("TPORT_IP_VER_TYPE_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpv4AdrPrvdrCd)
                    .HasColumnName("TPORT_IPV4_ADR_PRVDR_CD")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpv4AdrQty)
                    .HasColumnName("TPORT_IPV4_ADR_QTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpv6AdrPrvdrCd)
                    .HasColumnName("TPORT_IPV6_ADR_PRVDR_CD")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpv6AdrQty)
                    .HasColumnName("TPORT_IPV6_ADR_QTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TportLinkMgtPrcolTxt)
                    .HasColumnName("TPORT_LINK_MGT_PRCOL_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportMarkupPctQty)
                    .HasColumnName("TPORT_MARKUP_PCT_QTY")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportMultiMegCd)
                    .HasColumnName("TPORT_MULTI_MEG_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TportPortTypeId)
                    .HasColumnName("TPORT_PORT_TYPE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TportPplvlsCd)
                    .HasColumnName("TPORT_PPLVLS_CD")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.TportPreqLineItemIdXpirnDt)
                    .HasColumnName("TPORT_PREQ_LINE_ITEM_ID_XPIRN_DT")
                    .HasColumnType("date");

                entity.Property(e => e.TportPrequalLineItemId)
                    .HasColumnName("TPORT_PREQUAL_LINE_ITEM_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TportPrequalSiteId)
                    .HasColumnName("TPORT_PREQUAL_SITE_ID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.TportProjDes)
                    .HasColumnName("TPORT_PROJ_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportScrmbTxt)
                    .HasColumnName("TPORT_SCRMB_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportSpclInstrtnTxt)
                    .HasColumnName("TPORT_SPCL_INSTRTN_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TportTaxPctQty)
                    .HasColumnName("TPORT_TAX_PCT_QTY")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportUserTypeId)
                    .HasColumnName("TPORT_USER_TYPE_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportVlanQty).HasColumnName("TPORT_VLAN_QTY");

                entity.Property(e => e.TportVndrQuoteId)
                    .HasColumnName("TPORT_VNDR_QUOTE_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TportVndrRawMrcAmt)
                    .HasColumnName("TPORT_VNDR_RAW_MRC_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportVndrRawMrcInCurAmt)
                    .HasColumnName("TPORT_VNDR_RAW_MRC_IN_CUR_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportVndrRawMrcQotCurAmt)
                    .HasColumnName("TPORT_VNDR_RAW_MRC_QOT_CUR_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportVndrRawNrcAmt)
                    .HasColumnName("TPORT_VNDR_RAW_NRC_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportVndrRawNrcQotCurAmt)
                    .HasColumnName("TPORT_VNDR_RAW_NRC_QOT_CUR_AMT")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportVndrRvsdQotCur)
                    .HasColumnName("TPORT_VNDR_RVSD_QOT_CUR")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TportXstIpCnctnCd)
                    .HasColumnName("TPORT_XST_IP_CNCTN_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TspCd)
                    .HasColumnName("TSP_CD")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.TsupGcsNbr)
                    .HasColumnName("TSUP_GCS_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TsupNearestCrossStreetNme)
                    .HasColumnName("TSUP_NEAREST_CROSS_STREET_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TsupPrsQotNbr)
                    .HasColumnName("TSUP_PRS_QOT_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TsupRfqNbr)
                    .HasColumnName("TSUP_RFQ_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TsupTelcoDemarcBldgNme)
                    .HasColumnName("TSUP_TELCO_DEMARC_BLDG_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TsupTelcoDemarcFlrId)
                    .HasColumnName("TSUP_TELCO_DEMARC_FLR_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TsupTelcoDemarcRmNbr)
                    .HasColumnName("TSUP_TELCO_DEMARC_RM_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsArngtCd)
                    .HasColumnName("TTRPT_ACCS_ARNGT_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsBdwdType)
                    .HasColumnName("TTRPT_ACCS_BDWD_TYPE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsCntrcTermCd)
                    .HasColumnName("TTRPT_ACCS_CNTRC_TERM_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsPrvdrCd)
                    .HasColumnName("TTRPT_ACCS_PRVDR_CD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsTermDes)
                    .HasColumnName("TTRPT_ACCS_TERM_DES")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsTypeCd)
                    .HasColumnName("TTRPT_ACCS_TYPE_CD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsTypeDes)
                    .HasColumnName("TTRPT_ACCS_TYPE_DES")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAltAccsCd)
                    .HasColumnName("TTRPT_ALT_ACCS_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAltAccsPrvdrCd)
                    .HasColumnName("TTRPT_ALT_ACCS_PRVDR_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptClockSrcCd)
                    .HasColumnName("TTRPT_CLOCK_SRC_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptCsuDsuPrvddByVndrCd)
                    .HasColumnName("TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptEncapCd)
                    .HasColumnName("TTRPT_ENCAP_CD")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptFbrHandOffCd)
                    .HasColumnName("TTRPT_FBR_HAND_OFF_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptFrmngDes)
                    .HasColumnName("TTRPT_FRMNG_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptIsOchronousCd)
                    .HasColumnName("TTRPT_IS_OCHRONOUS_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptJackNrfcTypeCd)
                    .HasColumnName("TTRPT_JACK_NRFC_TYPE_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptLinkPrcolCd)
                    .HasColumnName("TTRPT_LINK_PRCOL_CD")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptMngdDataSrvcCd)
                    .HasColumnName("TTRPT_MNGD_DATA_SRVC_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptMplsBackhaulCd)
                    .HasColumnName("TTRPT_MPLS_BACKHAUL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptMplsVpnOvrSplkCd)
                    .HasColumnName("TTRPT_MPLS_VPN_OVR_SPLK_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptNcdeDes)
                    .HasColumnName("TTRPT_NCDE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptNwAdr)
                    .HasColumnName("TTRPT_NW_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptOssCd)
                    .HasColumnName("TTRPT_OSS_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptPrlelPrcsCktId)
                    .HasColumnName("TTRPT_PRLEL_PRCS_CKT_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptPrlelPrcsTypeCd)
                    .HasColumnName("TTRPT_PRLEL_PRCS_TYPE_CD")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptQotXpirnDt)
                    .HasColumnName("TTRPT_QOT_XPIRN_DT")
                    .HasColumnType("date");

                entity.Property(e => e.TtrptReltdFctyCktId)
                    .HasColumnName("TTRPT_RELTD_FCTY_CKT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptReltdFctyReuseCd)
                    .HasColumnName("TTRPT_RELTD_FCTY_REUSE_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptRoutgTypeCd)
                    .HasColumnName("TTRPT_ROUTG_TYPE_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptShrdTnantCd)
                    .HasColumnName("TTRPT_SHRD_TNANT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptSpdOfSrvcBdwdDes)
                    .HasColumnName("TTRPT_SPD_OF_SRVC_BDWD_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptSrvcTypeId)
                    .HasColumnName("TTRPT_SRVC_TYPE_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptStratumLvlCd)
                    .HasColumnName("TTRPT_STRATUM_LVL_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptStrtTmeSlotNbr).HasColumnName("TTRPT_STRT_TME_SLOT_NBR");

                entity.Property(e => e.TtrptTmeSoltCnt).HasColumnName("TTRPT_TME_SOLT_CNT");

                entity.Property(e => e.TtrptVndrTerm)
                    .HasColumnName("TTRPT_VNDR_TERM")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.VndoCntrcTermId)
                    .HasColumnName("VNDO_CNTRC_TERM_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VndrTerm)
                    .HasColumnName("VNDR_TERM")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.VndrVpnCd)
                    .HasColumnName("VNDR_VPN_CD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.InstlSrvcTierCdNavigation)
                    .WithMany(p => p.FsaOrdr)
                    .HasForeignKey(d => d.InstlSrvcTierCd)
                    .HasConstraintName("FK14_FSA_ORDR");

                entity.HasOne(d => d.OrdrActn)
                    .WithMany(p => p.FsaOrdr)
                    .HasForeignKey(d => d.OrdrActnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK16_FSA_ORDR");

                entity.HasOne(d => d.Ordr)
                    .WithOne(p => p.FsaOrdr)
                    .HasForeignKey<FsaOrdr>(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK25_FSA_ORDR");

                entity.HasOne(d => d.OrdrTypeCdNavigation)
                    .WithMany(p => p.FsaOrdr)
                    .HasForeignKey(d => d.OrdrTypeCd)
                    .HasConstraintName("FK18_FSA_ORDR");

                entity.HasOne(d => d.ProdTypeCdNavigation)
                    .WithMany(p => p.FsaOrdr)
                    .HasForeignKey(d => d.ProdTypeCd)
                    .HasConstraintName("FK20_FSA_ORDR");
            });

            modelBuilder.Entity<FsaOrdrCpeLineItem>(entity =>
            {
                entity.HasKey(e => e.FsaCpeLineItemId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("FSA_ORDR_CPE_LINE_ITEM", "dbo");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("IX03_FSA_ORDR_CPE_LINE_ITEM");

                entity.HasIndex(e => e.ItmStus)
                    .HasName("IX04_FSA_ORDR_CPE_LINE_ITEM");

                entity.HasIndex(e => e.PrchOrdrNbr)
                    .HasName("IX05_FSA_ORDR_CPE_LINE_ITEM");

                entity.HasIndex(e => new { e.TtrptSpdOfSrvcBdwdDes, e.LineItemCd })
                    .HasName("IX02_FSA_ORDR_CPE_LINE_ITEM");

                entity.HasIndex(e => new { e.TtrptSpdOfSrvcBdwdDes, e.OrdrId })
                    .HasName("IX01_FSA_ORDR_CPE_LINE_ITEM");

                entity.Property(e => e.FsaCpeLineItemId).HasColumnName("FSA_CPE_LINE_ITEM_ID");

                entity.Property(e => e.CktId)
                    .HasColumnName("CKT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CmplDt)
                    .HasColumnName("CMPL_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CmpntFmly)
                    .HasColumnName("CMPNT_FMLY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CmpntId).HasColumnName("CMPNT_ID");

                entity.Property(e => e.CntrcTermId)
                    .HasColumnName("CNTRC_TERM_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CntrcTypeId)
                    .HasColumnName("CNTRC_TYPE_ID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CpeCustPrvdSerialNbr)
                    .HasColumnName("CPE_CUST_PRVD_SERIAL_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CpeReuseCd).HasColumnName("CPE_REUSE_CD");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CxrAccsCd)
                    .HasColumnName("CXR_ACCS_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CxrCktId)
                    .HasColumnName("CXR_CKT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CxrSrvcId)
                    .HasColumnName("CXR_SRVC_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeviceId)
                    .HasColumnName("DEVICE_ID")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.DiscFmsCktNbr).HasColumnName("DISC_FMS_CKT_NBR");

                entity.Property(e => e.DiscNwAdr)
                    .HasColumnName("DISC_NW_ADR")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DiscPlNbr)
                    .HasColumnName("DISC_PL_NBR")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DropShp)
                    .HasColumnName("DROP_SHP")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EqptId)
                    .HasColumnName("EQPT_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EqptItmRcvdDt)
                    .HasColumnName("EQPT_ITM_RCVD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EqptRcvdByAdid)
                    .HasColumnName("EQPT_RCVD_BY_ADID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.EqptTypeId)
                    .HasColumnName("EQPT_TYPE_ID")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.FmsCktNbr).HasColumnName("FMS_CKT_NBR");

                entity.Property(e => e.FsaOrdrVasId).HasColumnName("FSA_ORDR_VAS_ID");

                entity.Property(e => e.FsaOrgnlInstlOrdrId)
                    .HasColumnName("FSA_ORGNL_INSTL_ORDR_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InstlDsgnDocNbr)
                    .HasColumnName("INSTL_DSGN_DOC_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InstlnCd)
                    .HasColumnName("INSTLN_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ItmStus).HasColumnName("ITM_STUS");

                entity.Property(e => e.LineItemCd)
                    .IsRequired()
                    .HasColumnName("LINE_ITEM_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ManfDiscntCd)
                    .HasColumnName("MANF_DISCNT_CD")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ManfPartCd)
                    .HasColumnName("MANF_PART_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MatlCd)
                    .HasColumnName("MATL_CD")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.MdsDes)
                    .HasColumnName("MDS_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MfrNme)
                    .HasColumnName("MFR_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MfrPsId)
                    .HasColumnName("MFR_PS_ID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MntcCd)
                    .HasColumnName("MNTC_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.NidSerialNbr)
                    .HasColumnName("NID_SERIAL_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrCmpntId).HasColumnName("ORDR_CMPNT_ID");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.OrdrQty).HasColumnName("ORDR_QTY");

                entity.Property(e => e.Pid)
                    .HasColumnName("PID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.PlNbr)
                    .HasColumnName("PL_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PlsftRqstnNbr)
                    .HasColumnName("PLSFT_RQSTN_NBR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PoLnNbr).HasColumnName("PO_LN_NBR");

                entity.Property(e => e.PortRtTypeCd)
                    .HasColumnName("PORT_RT_TYPE_CD")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.PrchOrdrNbr)
                    .HasColumnName("PRCH_ORDR_NBR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.PsRcvdStus).HasColumnName("PS_RCVD_STUS");

                entity.Property(e => e.RcvdQty).HasColumnName("RCVD_QTY");

                entity.Property(e => e.RltdCmpntId).HasColumnName("RLTD_CMPNT_ID");

                entity.Property(e => e.RqstnDt)
                    .HasColumnName("RQSTN_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SmrtAcctDomnTxt)
                    .HasColumnName("SMRT_ACCT_DOMN_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SpaAccsIndcrDes)
                    .HasColumnName("SPA_ACCS_INDCR_DES")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SprintMntdFlg)
                    .HasColumnName("SPRINT_MNTD_FLG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcLineItemCd)
                    .HasColumnName("SRVC_LINE_ITEM_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StusCd)
                    .HasColumnName("STUS_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Supplier)
                    .HasColumnName("SUPPLIER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportCnctrTypeId)
                    .HasColumnName("TPORT_CNCTR_TYPE_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TportCustRoutrAutoNegotCd)
                    .HasColumnName("TPORT_CUST_ROUTR_AUTO_NEGOT_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.TportCustRoutrTagTxt)
                    .HasColumnName("TPORT_CUST_ROUTR_TAG_TXT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TportDiaNocToNocTxt)
                    .HasColumnName("TPORT_DIA_NOC_TO_NOC_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportEthAccsTypeCd)
                    .HasColumnName("TPORT_ETH_ACCS_TYPE_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TportEthrntNrfcIndcr)
                    .HasColumnName("TPORT_ETHRNT_NRFC_INDCR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpVerTypeCd)
                    .HasColumnName("TPORT_IP_VER_TYPE_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpv4AdrPrvdrCd)
                    .HasColumnName("TPORT_IPV4_ADR_PRVDR_CD")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpv4AdrQty)
                    .HasColumnName("TPORT_IPV4_ADR_QTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpv6AdrPrvdrCd)
                    .HasColumnName("TPORT_IPV6_ADR_PRVDR_CD")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.TportIpv6AdrQty)
                    .HasColumnName("TPORT_IPV6_ADR_QTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TportProjDes)
                    .HasColumnName("TPORT_PROJ_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TportVlanQty).HasColumnName("TPORT_VLAN_QTY");

                entity.Property(e => e.TportVndrQuoteId)
                    .HasColumnName("TPORT_VNDR_QUOTE_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsArngtCd)
                    .HasColumnName("TTRPT_ACCS_ARNGT_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsBdwdType)
                    .HasColumnName("TTRPT_ACCS_BDWD_TYPE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsTermDes)
                    .HasColumnName("TTRPT_ACCS_TERM_DES")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsTypeCd)
                    .HasColumnName("TTRPT_ACCS_TYPE_CD")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptAccsTypeDes)
                    .HasColumnName("TTRPT_ACCS_TYPE_DES")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptCosCd)
                    .HasColumnName("TTRPT_COS_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptEncapCd)
                    .HasColumnName("TTRPT_ENCAP_CD")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptFbrHandOffCd)
                    .HasColumnName("TTRPT_FBR_HAND_OFF_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtrptQotXpirnDt)
                    .HasColumnName("TTRPT_QOT_XPIRN_DT")
                    .HasColumnType("date");

                entity.Property(e => e.TtrptSpdOfSrvcBdwdDes)
                    .HasColumnName("TTRPT_SPD_OF_SRVC_BDWD_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnitMsr)
                    .HasColumnName("UNIT_MSR")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("UNIT_PRICE")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.VndrCd)
                    .HasColumnName("VNDR_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.VrtlAcctDomnTxt)
                    .HasColumnName("VRTL_ACCT_DOMN_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ZsclrQuoteId)
                    .HasColumnName("ZSCLR_QUOTE_ID")
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.FsaOrdrCpeLineItem)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FSA_ORDR_CPE_LINE_ITEM");
            });

            modelBuilder.Entity<FsaOrdrCsc>(entity =>
            {
                entity.HasKey(e => e.OrdrId);

                entity.ToTable("FSA_ORDR_CSC", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_FSA_ORDR_CSC");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_FSA_ORDR_CSC");

                entity.Property(e => e.OrdrId)
                    .HasColumnName("ORDR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IntlMntcSchrgTxt)
                    .HasColumnName("INTL_MNTC_SCHRG_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MtrlRqstnTxt)
                    .HasColumnName("MTRL_RQSTN_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrchOrdrTxt)
                    .HasColumnName("PRCH_ORDR_TXT")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNbr)
                    .HasColumnName("SERIAL_NBR")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.FsaOrdrCscCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_FSA_ORDR_CSC");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.FsaOrdrCscModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_FSA_ORDR_CSC");

                entity.HasOne(d => d.Ordr)
                    .WithOne(p => p.FsaOrdrCsc)
                    .HasForeignKey<FsaOrdrCsc>(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FSA_ORDER_CSC");
            });

            modelBuilder.Entity<FsaOrdrCust>(entity =>
            {
                entity.ToTable("FSA_ORDR_CUST", "dbo");

                entity.HasIndex(e => new { e.OrdrId, e.CisLvlType })
                    .HasName("UX01_FSA_ORDR_CUST")
                    .IsUnique();

                entity.Property(e => e.FsaOrdrCustId).HasColumnName("FSA_ORDR_CUST_ID");

                entity.Property(e => e.BrCd)
                    .HasColumnName("BR_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CharsCustId)
                    .HasColumnName("CHARS_CUST_ID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.CisLvlType)
                    .IsRequired()
                    .HasColumnName("CIS_LVL_TYPE")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ClliCd)
                    .HasColumnName("CLLI_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CurrBillCycCd)
                    .IsRequired()
                    .HasColumnName("CURR_BILL_CYC_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CustId).HasColumnName("CUST_ID");

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.FutBillCycCd)
                    .HasColumnName("FUT_BILL_CYC_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.SalsPersnPrimCid)
                    .HasColumnName("SALS_PERSN_PRIM_CID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.SalsPersnScndyCid)
                    .HasColumnName("SALS_PERSN_SCNDY_CID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasColumnName("SITE_ID")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.SoiCd)
                    .HasColumnName("SOI_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcSubTypeId)
                    .HasColumnName("SRVC_SUB_TYPE_ID")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.TaxXmptCd)
                    .HasColumnName("TAX_XMPT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.FsaOrdrCust)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FSA_ORDR_CUST");
            });

            modelBuilder.Entity<FsaOrdrGomXnci>(entity =>
            {
                entity.ToTable("FSA_ORDR_GOM_XNCI", "dbo");

                entity.HasIndex(e => e.CpeStusId)
                    .HasName("IX03_FSA_ORDR_GOM_XNCI");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_FSA_ORDR_GOM_XNCI");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX01_FSA_ORDR_GOM_XNCI");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AtlasWrkOrdrNbr)
                    .HasColumnName("ATLAS_WRK_ORDR_NBR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.BillFtn)
                    .HasColumnName("BILL_FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CourierTrkNbr)
                    .HasColumnName("COURIER_TRK_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CpeCmpltDt)
                    .HasColumnName("CPE_CMPLT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CpeEqptTypeTxt)
                    .HasColumnName("CPE_EQPT_TYPE_TXT")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CpeStusId).HasColumnName("CPE_STUS_ID");

                entity.Property(e => e.CpeVndrNme)
                    .HasColumnName("CPE_VNDR_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CstmDt)
                    .HasColumnName("CSTM_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CustDlvryDt)
                    .HasColumnName("CUST_DLVRY_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CustDscnctCd).HasColumnName("CUST_DSCNCT_CD");

                entity.Property(e => e.CustDscnctDt)
                    .HasColumnName("CUST_DSCNCT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CustPrchCd).HasColumnName("CUST_PRCH_CD");

                entity.Property(e => e.CustRnlCd).HasColumnName("CUST_RNL_CD");

                entity.Property(e => e.CustRnlTermNbr).HasColumnName("CUST_RNL_TERM_NBR");

                entity.Property(e => e.CustRntlCd).HasColumnName("CUST_RNTL_CD");

                entity.Property(e => e.CustRntlTermNbr).HasColumnName("CUST_RNTL_TERM_NBR");

                entity.Property(e => e.DrtbnShipDt)
                    .HasColumnName("DRTBN_SHIP_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EqptInstlDt)
                    .HasColumnName("EQPT_INSTL_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FsaCpeLineItemId).HasColumnName("FSA_CPE_LINE_ITEM_ID");

                entity.Property(e => e.GrpId).HasColumnName("GRP_ID");

                entity.Property(e => e.HdwrDlvrdDt)
                    .HasColumnName("HDWR_DLVRD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.LogicalisMnthLeaseRtAmt)
                    .HasColumnName("LOGICALIS_MNTH_LEASE_RT_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.LogicalisRnlMnthLeaseRtAmt)
                    .HasColumnName("LOGICALIS_RNL_MNTH_LEASE_RT_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.PlnNme)
                    .HasColumnName("PLN_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PlsftRcptId)
                    .HasColumnName("PLSFT_RCPT_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PlsftRelsDt)
                    .HasColumnName("PLSFT_RELS_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PlsftRelsId)
                    .HasColumnName("PLSFT_RELS_ID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.PlsftRqstnNbr)
                    .HasColumnName("PLSFT_RQSTN_NBR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.PrchOrdrBackOrdrShipDt)
                    .HasColumnName("PRCH_ORDR_BACK_ORDR_SHIP_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PrchOrdrNbr)
                    .HasColumnName("PRCH_ORDR_NBR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.RqstnAmt)
                    .HasColumnName("RQSTN_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.RqstnDt)
                    .HasColumnName("RQSTN_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ShpmtSignByNme)
                    .HasColumnName("SHPMT_SIGN_BY_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ShpmtTrkNbr)
                    .HasColumnName("SHPMT_TRK_NBR")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Term60DayNtfctnDt)
                    .HasColumnName("TERM_60_DAY_NTFCTN_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.TermEndDt)
                    .HasColumnName("TERM_END_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.TermStrtDt)
                    .HasColumnName("TERM_STRT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.UsrPrfId).HasColumnName("USR_PRF_ID");

                entity.Property(e => e.VndrCourierTrkNbr)
                    .HasColumnName("VNDR_COURIER_TRK_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndrRecvFromDt)
                    .HasColumnName("VNDR_RECV_FROM_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.VndrShipDt)
                    .HasColumnName("VNDR_SHIP_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.CpeStus)
                    .WithMany(p => p.FsaOrdrGomXnci)
                    .HasForeignKey(d => d.CpeStusId)
                    .HasConstraintName("FK05_FSA_ORDR_GOM_XNCI");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.FsaOrdrGomXnciCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FSA_ORDR_GOM_XNCI");

                entity.HasOne(d => d.FsaCpeLineItem)
                    .WithMany(p => p.FsaOrdrGomXnci)
                    .HasForeignKey(d => d.FsaCpeLineItemId)
                    .HasConstraintName("FK06_FSA_ORDR_GOM_XNCI");

                entity.HasOne(d => d.Grp)
                    .WithMany(p => p.FsaOrdrGomXnci)
                    .HasForeignKey(d => d.GrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_FSA_ORDR_GOM_XNCI");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.FsaOrdrGomXnciModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_FSA_ORDR_GOM_XNCI");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.FsaOrdrGomXnci)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_FSA_ORDR_GOM_XNCI");

                entity.HasOne(d => d.UsrPrf)
                    .WithMany(p => p.FsaOrdrGomXnci)
                    .HasForeignKey(d => d.UsrPrfId)
                    .HasConstraintName("FK07_FSA_ORDR_GOM_XNCI");
            });

            modelBuilder.Entity<H5Doc>(entity =>
            {
                entity.ToTable("H5_DOC", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX03_H5_DOC");

                entity.HasIndex(e => e.H5FoldrId)
                    .HasName("IX01_H5_DOC");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX04_H5_DOC");

                entity.Property(e => e.H5DocId).HasColumnName("H5_DOC_ID");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileCntnt)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FileSizeQty).HasColumnName("FILE_SIZE_QTY");

                entity.Property(e => e.H5FoldrId).HasColumnName("H5_FOLDR_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.H5DocCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_H5_DOC");

                entity.HasOne(d => d.H5Foldr)
                    .WithMany(p => p.H5Doc)
                    .HasForeignKey(d => d.H5FoldrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_H5_DOC");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.H5DocModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK04_H5_DOC");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.H5Doc)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_H5_DOC");
            });

            modelBuilder.Entity<H5Foldr>(entity =>
            {
                entity.ToTable("H5_FOLDR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX04_H5_FOLDR");

                entity.HasIndex(e => e.CtryCd)
                    .HasName("IX02_H5_FOLDR");

                entity.HasIndex(e => e.CustId)
                    .HasName("IX01_H5_FOLDR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX05_H5_FOLDR");

                entity.Property(e => e.H5FoldrId).HasColumnName("H5_FOLDR_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CustCtyNme)
                    .HasColumnName("CUST_CTY_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustId).HasColumnName("CUST_ID");

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.H5FoldrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_H5_FOLDR");

                entity.HasOne(d => d.CsgLvl)
                    .WithMany(p => p.H5Foldr)
                    .HasForeignKey(d => d.CsgLvlId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_H5_FOLDR");

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.H5Foldr)
                    .HasForeignKey(d => d.CtryCd)
                    .HasConstraintName("FK01_H5_FOLDR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.H5FoldrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK04_H5_FOLDR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.H5Foldr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_H5_FOLDR");
            });

            modelBuilder.Entity<IpActy>(entity =>
            {
                entity.ToTable("IP_ACTY", "dbo");

                entity.HasIndex(e => e.IpMstrId)
                    .HasName("IX02_IP_ACTY");

                entity.HasIndex(e => e.NatSipActyId)
                    .HasName("IX03_IP_ACTY");

                entity.HasIndex(e => e.NidActyId)
                    .HasName("IX01_IP_ACTY");

                entity.Property(e => e.IpActyId).HasColumnName("IP_ACTY_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IpMstrId).HasColumnName("IP_MSTR_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.NatSipActyId).HasColumnName("NAT_SIP_ACTY_ID");

                entity.Property(e => e.NidActyId).HasColumnName("NID_ACTY_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((251))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.IpActyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_IP_ACTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.IpActyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_IP_ACTY");

                entity.HasOne(d => d.NidActy)
                    .WithMany(p => p.IpActy)
                    .HasForeignKey(d => d.NidActyId)
                    .HasConstraintName("FK04_IP_ACTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.IpActy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_IP_ACTY");
            });

            modelBuilder.Entity<IpMstr>(entity =>
            {
                entity.ToTable("IP_MSTR", "dbo");

                entity.HasIndex(e => e.IpAdr)
                    .HasName("UX01_IP_MSTR")
                    .IsUnique();

                entity.Property(e => e.IpMstrId).HasColumnName("IP_MSTR_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IpAdr)
                    .IsRequired()
                    .HasColumnName("IP_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((252))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.IpMstrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_IP_MSTR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.IpMstrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_IP_MSTR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.IpMstr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_IP_MSTR");
            });

            modelBuilder.Entity<IplOrdr>(entity =>
            {
                entity.HasKey(e => e.OrdrId);

                entity.ToTable("IPL_ORDR", "dbo");

                entity.HasIndex(e => e.CktTypeCd)
                    .HasName("IX05_IPL_ORDR");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX08_IPL_ORDR");

                entity.HasIndex(e => e.CustCntrcLgthId)
                    .HasName("IX03_IPL_ORDR");

                entity.HasIndex(e => e.DscnctReasCd)
                    .HasName("IX02_IPL_ORDR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX09_IPL_ORDR");

                entity.HasIndex(e => e.OrdrStusId)
                    .HasName("IX04_IPL_ORDR");

                entity.HasIndex(e => e.OrdrTypeId)
                    .HasName("IX01_IPL_ORDR");

                entity.HasIndex(e => e.ProdTypeId)
                    .HasName("IX07_IPL_ORDR");

                entity.HasIndex(e => e.SrvcTypeCd)
                    .HasName("IX06_IPL_ORDR");

                entity.Property(e => e.OrdrId)
                    .HasColumnName("ORDR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BasePlanTypeNme)
                    .HasColumnName("BASE_PLAN_TYPE_NME")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CalBefDspchHrTxt)
                    .HasColumnName("CAL_BEF_DSPCH_HR_TXT")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CktSpdQty).HasColumnName("CKT_SPD_QTY");

                entity.Property(e => e.CktTypeCd)
                    .HasColumnName("CKT_TYPE_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClcmNme)
                    .IsRequired()
                    .HasColumnName("CLCM_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClcmPhnNbr)
                    .HasColumnName("CLCM_PHN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustAcptErlySrvcCd).HasColumnName("CUST_ACPT_ERLY_SRVC_CD");

                entity.Property(e => e.CustCmmtDt)
                    .HasColumnName("CUST_CMMT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustCntrcLgthId)
                    .HasColumnName("CUST_CNTRC_LGTH_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntrcSignDt)
                    .HasColumnName("CUST_CNTRC_SIGN_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustOrdrSbmtDt)
                    .HasColumnName("CUST_ORDR_SBMT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustWantDt)
                    .HasColumnName("CUST_WANT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.DscnctReasCd).HasColumnName("DSCNCT_REAS_CD");

                entity.Property(e => e.GcsPriceQotNbr)
                    .HasColumnName("GCS_PRICE_QOT_NBR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GvrmntCustCd).HasColumnName("GVRMNT_CUST_CD");

                entity.Property(e => e.H5CustAcctNbr)
                    .HasColumnName("H5_CUST_ACCT_NBR")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.H5EsAcctToBeDscnctCd).HasColumnName("H5_ES_ACCT_TO_BE_DSCNCT_CD");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NewCnstrctnCd).HasColumnName("NEW_CNSTRCTN_CD");

                entity.Property(e => e.OeqptCpeCmntTxt)
                    .HasColumnName("OEQPT_CPE_CMNT_TXT")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.OeqptCpeDiscPct)
                    .HasColumnName("OEQPT_CPE_DISC_PCT")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.OeqptCsuDsuCd)
                    .HasColumnName("OEQPT_CSU_DSU_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OeqptPreXstClliCd)
                    .HasColumnName("OEQPT_PRE_XST_CLLI_CD")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.OeqptRadBoxNeedCd)
                    .HasColumnName("OEQPT_RAD_BOX_NEED_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrCmntTxt)
                    .HasColumnName("ORDR_CMNT_TXT")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrStusId).HasColumnName("ORDR_STUS_ID");

                entity.Property(e => e.OrdrTypeId).HasColumnName("ORDR_TYPE_ID");

                entity.Property(e => e.OrgtngCharsId)
                    .HasColumnName("ORGTNG_CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrgtngCmntTxt)
                    .HasColumnName("ORGTNG_CMNT_TXT")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.OrgtngCoId)
                    .HasColumnName("ORGTNG_CO_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlCatTxt)
                    .HasColumnName("PL_CAT_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PreXstPlNbr)
                    .HasColumnName("PRE_XST_PL_NBR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrevOrdrTypeId).HasColumnName("PREV_ORDR_TYPE_ID");

                entity.Property(e => e.PrmtCd)
                    .HasColumnName("PRMT_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ProdTypeId).HasColumnName("PROD_TYPE_ID");

                entity.Property(e => e.PrsPriceQotNbr)
                    .HasColumnName("PRS_PRICE_QOT_NBR")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.ReSbmsnCd).HasColumnName("RE_SBMSN_CD");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReltdOrdrId).HasColumnName("RELTD_ORDR_ID");

                entity.Property(e => e.SalsBrNme)
                    .HasColumnName("SALS_BR_NME")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SalsPersnCid)
                    .HasColumnName("SALS_PERSN_CID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SalsPersnCoCd)
                    .HasColumnName("SALS_PERSN_CO_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SalsPersnNme)
                    .IsRequired()
                    .HasColumnName("SALS_PERSN_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ScaNbr)
                    .HasColumnName("SCA_NBR")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Soi)
                    .HasColumnName("SOI")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcTypeCd)
                    .HasColumnName("SRVC_TYPE_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TrmtgCharsId)
                    .HasColumnName("TRMTG_CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TrmtgCmntTxt)
                    .HasColumnName("TRMTG_CMNT_TXT")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.TrmtgCoCd)
                    .HasColumnName("TRMTG_CO_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TspCd)
                    .HasColumnName("TSP_CD")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.UnmanSiteCd).HasColumnName("UNMAN_SITE_CD");

                entity.Property(e => e.XtnlMoveCd).HasColumnName("XTNL_MOVE_CD");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.IplOrdrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK10_IPL_ORDR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.IplOrdrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK12_IPL_ORDR");

                entity.HasOne(d => d.Ordr)
                    .WithOne(p => p.IplOrdr)
                    .HasForeignKey<IplOrdr>(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_IPL_ORDR");

                entity.HasOne(d => d.OrdrStus)
                    .WithMany(p => p.IplOrdr)
                    .HasForeignKey(d => d.OrdrStusId)
                    .HasConstraintName("FK05_IPL_ORDR");

                entity.HasOne(d => d.OrdrType)
                    .WithMany(p => p.IplOrdrOrdrType)
                    .HasForeignKey(d => d.OrdrTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_IPL_ORDR");

                entity.HasOne(d => d.PrevOrdrType)
                    .WithMany(p => p.IplOrdrPrevOrdrType)
                    .HasForeignKey(d => d.PrevOrdrTypeId)
                    .HasConstraintName("FK13_IPL_ORDR");

                entity.HasOne(d => d.ProdType)
                    .WithMany(p => p.IplOrdr)
                    .HasForeignKey(d => d.ProdTypeId)
                    .HasConstraintName("FK03_IPL_ORDR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.IplOrdr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK11_IPL_ORDR");
            });

            modelBuilder.Entity<LkAccsCtySite>(entity =>
            {
                entity.HasKey(e => e.AccsCtySiteId);

                entity.ToTable("LK_ACCS_CTY_SITE", "dbo");

                entity.HasIndex(e => e.AccsCtyNmeSiteCd)
                    .HasName("UX01_LK_ACCS_CTY_SITE")
                    .IsUnique();

                entity.Property(e => e.AccsCtySiteId)
                    .HasColumnName("ACCS_CTY_SITE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccsCtyNmeSiteCd)
                    .IsRequired()
                    .HasColumnName("ACCS_CTY_NME_SITE_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<LkAccsMb>(entity =>
            {
                entity.HasKey(e => e.AccsMbId);

                entity.ToTable("LK_ACCS_MB", "dbo");

                entity.Property(e => e.AccsMbId).HasColumnName("ACCS_MB_ID");

                entity.Property(e => e.AccsMbDes)
                    .IsRequired()
                    .HasColumnName("ACCS_MB_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AsrTypeId).HasColumnName("ASR_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SortId).HasColumnName("SORT_ID");
            });

            modelBuilder.Entity<LkActn>(entity =>
            {
                entity.HasKey(e => e.ActnId);

                entity.ToTable("LK_ACTN", "dbo");

                entity.HasIndex(e => e.ActnDes)
                    .HasName("UX01_LK_ACTN")
                    .IsUnique();

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_ACTN");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_ACTN");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_ACTN");

                entity.Property(e => e.ActnId).HasColumnName("ACTN_ID");

                entity.Property(e => e.ActnDes)
                    .IsRequired()
                    .HasColumnName("ACTN_DES")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ActnTypeId).HasColumnName("ACTN_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.ActnType)
                    .WithMany(p => p.LkActn)
                    .HasForeignKey(d => d.ActnTypeId)
                    .HasConstraintName("FK04_LK_ACTN");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkActnCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_ACTN");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkActnModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_ACTN");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkActn)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_ACTN");
            });

            modelBuilder.Entity<LkActnType>(entity =>
            {
                entity.HasKey(e => e.ActnTypeId);

                entity.ToTable("LK_ACTN_TYPE", "dbo");

                entity.HasIndex(e => e.ActnType)
                    .HasName("UX01_LK_ACTN_TYPE")
                    .IsUnique();

                entity.Property(e => e.ActnTypeId)
                    .HasColumnName("ACTN_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActnType)
                    .IsRequired()
                    .HasColumnName("ACTN_TYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkActnType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_ACTN_TYPE");
            });

            modelBuilder.Entity<LkActyLocale>(entity =>
            {
                entity.HasKey(e => e.ActyLocaleId);

                entity.ToTable("LK_ACTY_LOCALE", "dbo");

                entity.HasIndex(e => e.ActyLocaleDes)
                    .HasName("UX01_LK_ACTY_LOCALE")
                    .IsUnique();

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_ACTY_LOCALE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_ACTY_LOCALE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_ACTY_LOCALE");

                entity.Property(e => e.ActyLocaleId)
                    .HasColumnName("ACTY_LOCALE_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ActyLocaleDes)
                    .IsRequired()
                    .HasColumnName("ACTY_LOCALE_DES")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkActyLocaleCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_ACTY_LOCALE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkActyLocaleModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_ACTY_LOCALE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkActyLocale)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_ACTY_LOCALE");
            });

            modelBuilder.Entity<LkAdrType>(entity =>
            {
                entity.HasKey(e => e.AdrTypeId);

                entity.ToTable("LK_ADR_TYPE", "dbo");

                entity.HasIndex(e => e.AdrTypeDes)
                    .HasName("UX01_LK_ADR_TYPE")
                    .IsUnique();

                entity.Property(e => e.AdrTypeId).HasColumnName("ADR_TYPE_ID");

                entity.Property(e => e.AdrTypeDes)
                    .IsRequired()
                    .HasColumnName("ADR_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkAdrType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_ADR_TYPE");
            });

            modelBuilder.Entity<LkApptType>(entity =>
            {
                entity.HasKey(e => e.ApptTypeId);

                entity.ToTable("LK_APPT_TYPE", "dbo");

                entity.HasIndex(e => e.ApptTypeDes)
                    .HasName("UX01_LK_APPT_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_APPT_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_APPT_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_APPT_TYPE");

                entity.Property(e => e.ApptTypeId)
                    .HasColumnName("APPT_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ApptTypeDes)
                    .IsRequired()
                    .HasColumnName("APPT_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkApptTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_APPT_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkApptTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_APPT_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkApptType)
                    .HasForeignKey(d => d.RecStusId)
                    .HasConstraintName("FK03_LK_APPT_TYPE");
            });

            modelBuilder.Entity<LkAsrType>(entity =>
            {
                entity.HasKey(e => e.AsrTypeId);

                entity.ToTable("LK_ASR_TYPE", "dbo");

                entity.Property(e => e.AsrTypeId)
                    .HasColumnName("ASR_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AsrTypeDes)
                    .HasColumnName("ASR_TYPE_DES")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AsrTypeNme)
                    .IsRequired()
                    .HasColumnName("ASR_TYPE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkAsrType)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK01_LK_ASR_TYPE");
            });

            modelBuilder.Entity<LkCcdMissdReas>(entity =>
            {
                entity.HasKey(e => e.CcdMissdReasId);

                entity.ToTable("LK_CCD_MISSD_REAS", "dbo");

                entity.HasIndex(e => e.CcdMissdReasDes)
                    .HasName("UX01_LK_CCD_MISSD_REAS")
                    .IsUnique();

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_CCD_MISSD_REAS");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX02_LK_CCD_MISSD_REAS");

                entity.Property(e => e.CcdMissdReasId)
                    .HasColumnName("CCD_MISSD_REAS_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CcdMissdReasDes)
                    .IsRequired()
                    .HasColumnName("CCD_MISSD_REAS_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCcdMissdReas)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_CCD_MISSD_REAS");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCcdMissdReas)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CCD_MISSD_REAS");
            });

            modelBuilder.Entity<LkCktChgType>(entity =>
            {
                entity.HasKey(e => e.CktChgTypeId);

                entity.ToTable("LK_CKT_CHG_TYPE", "dbo");

                entity.HasIndex(e => e.CktChgTypeDes)
                    .HasName("UX01_LK_CKT_CHG_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_CKT_CHG_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_CKT_CHG_TYPE");

                entity.Property(e => e.CktChgTypeId)
                    .HasColumnName("CKT_CHG_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CktChgTypeDes)
                    .IsRequired()
                    .HasColumnName("CKT_CHG_TYPE_DES")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCktChgType)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CKT_CHG_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCktChgType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_CKT_CHG_TYPE");
            });

            modelBuilder.Entity<LkCnfrcBrdg>(entity =>
            {
                entity.HasKey(e => e.CnfrcBrdgId);

                entity.ToTable("LK_CNFRC_BRDG", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_CNFRC_BRDG");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_CNFRC_BRDG");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_CNFRC_BRDG");

                entity.HasIndex(e => new { e.CreatByUserId, e.CnfrcBrdgNbr, e.CnfrcPinNbr, e.Soi })
                    .HasName("UX01_LK_CNFRC_BRDG")
                    .IsUnique();

                entity.Property(e => e.CnfrcBrdgId)
                    .HasColumnName("CNFRC_BRDG_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CnfrcBrdgNbr)
                    .IsRequired()
                    .HasColumnName("CNFRC_BRDG_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcPinNbr)
                    .HasColumnName("CNFRC_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OnlineMeetingAdr)
                    .HasColumnName("ONLINE_MEETING_ADR")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Soi)
                    .HasColumnName("SOI")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCnfrcBrdgCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CNFRC_BRDG");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCnfrcBrdgModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CNFRC_BRDG");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCnfrcBrdg)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CNFRC_BRDG");
            });

            modelBuilder.Entity<LkCntctType>(entity =>
            {
                entity.HasKey(e => e.CntctTypeId);

                entity.ToTable("LK_CNTCT_TYPE", "dbo");

                entity.HasIndex(e => e.CntctTypeDes)
                    .HasName("UX01_LK_CNTCT_TYPE")
                    .IsUnique();

                entity.Property(e => e.CntctTypeId).HasColumnName("CNTCT_TYPE_ID");

                entity.Property(e => e.CntctTypeDes)
                    .IsRequired()
                    .HasColumnName("CNTCT_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCntctType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CNTCT_TYPE");
            });

            modelBuilder.Entity<LkCowsAppCfg>(entity =>
            {
                entity.HasKey(e => e.CfgKeyNme);

                entity.ToTable("LK_COWS_APP_CFG", "dbo");

                entity.Property(e => e.CfgKeyNme)
                    .HasColumnName("CFG_KEY_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CfgKeyValuTxt)
                    .IsRequired()
                    .HasColumnName("CFG_KEY_VALU_TXT")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<LkCpeJprdy>(entity =>
            {
                entity.HasKey(e => e.CpeJprdyId);

                entity.ToTable("LK_CPE_JPRDY", "dbo");

                entity.HasIndex(e => e.CpeJprdyCd)
                    .HasName("UX01_LK_CPE_JPRDY")
                    .IsUnique();

                entity.Property(e => e.CpeJprdyId).HasColumnName("CPE_JPRDY_ID");

                entity.Property(e => e.CpeJprdyCd)
                    .IsRequired()
                    .HasColumnName("CPE_JPRDY_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CpeJprdyDes)
                    .IsRequired()
                    .HasColumnName("CPE_JPRDY_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CpeJprdyDsply)
                    .IsRequired()
                    .HasColumnName("CPE_JPRDY_DSPLY")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<LkCpePid>(entity =>
            {
                entity.HasKey(e => e.CpePidId)
                    .HasName("CPE_PID_ID");

                entity.ToTable("LK_CPE_PID", "dbo");

                entity.HasIndex(e => e.CpePidId)
                    .HasName("UX01_LK_CPE_PID")
                    .IsUnique();

                entity.Property(e => e.CpePidId).HasColumnName("CPE_PID_ID");

                entity.Property(e => e.Acct)
                    .HasColumnName("ACCT")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Actvy)
                    .HasColumnName("ACTVY")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.BusUntGl)
                    .HasColumnName("BUS_UNT_GL")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.BusUntPc)
                    .HasColumnName("BUS_UNT_PC")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CostCntr)
                    .HasColumnName("COST_CNTR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DmstcCd).HasColumnName("DMSTC_CD");

                entity.Property(e => e.DropShp)
                    .HasColumnName("DROP_SHP")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EqptTypeId)
                    .HasColumnName("EQPT_TYPE_ID")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Mrkt)
                    .HasColumnName("MRKT")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.PidCntrctType)
                    .HasColumnName("PID_CNTRCT_TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PidProdId)
                    .HasColumnName("PID_PROD_ID")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Prodct)
                    .HasColumnName("PRODCT")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ProjId)
                    .HasColumnName("PROJ_ID")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Regn)
                    .HasColumnName("REGN")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.RsrcCat)
                    .HasColumnName("RSRC_CAT")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SourceTyp)
                    .HasColumnName("SOURCE_TYP")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCpePidCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPE_PID");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCpePidModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPE_PID");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCpePid)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPE_PID");
            });

            modelBuilder.Entity<LkCptCustType>(entity =>
            {
                entity.HasKey(e => e.CptCustTypeId);

                entity.ToTable("LK_CPT_CUST_TYPE", "dbo");

                entity.HasIndex(e => e.CptCustType)
                    .HasName("UX01_LK_CPT_CUST_TYPE")
                    .IsUnique();

                entity.Property(e => e.CptCustTypeId)
                    .HasColumnName("CPT_CUST_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptCustType)
                    .IsRequired()
                    .HasColumnName("CPT_CUST_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptCustTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_CUST_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptCustTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_CUST_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptCustType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_CUST_TYPE");
            });

            modelBuilder.Entity<LkCptHstMngdSrvc>(entity =>
            {
                entity.HasKey(e => e.CptHstMngdSrvcId);

                entity.ToTable("LK_CPT_HST_MNGD_SRVC", "dbo");

                entity.HasIndex(e => e.CptHstMngdSrvc)
                    .HasName("UX01_LK_CPT_HST_MNGD_SRVC")
                    .IsUnique();

                entity.Property(e => e.CptHstMngdSrvcId)
                    .HasColumnName("CPT_HST_MNGD_SRVC_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptHstMngdSrvc)
                    .IsRequired()
                    .HasColumnName("CPT_HST_MNGD_SRVC")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptHstMngdSrvcCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_HST_MNGD_SRVC");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptHstMngdSrvcModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_HST_MNGD_SRVC");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptHstMngdSrvc)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_HST_MNGD_SRVC");
            });

            modelBuilder.Entity<LkCptMdsSuprtTier>(entity =>
            {
                entity.HasKey(e => e.CptMdsSuprtTierId);

                entity.ToTable("LK_CPT_MDS_SUPRT_TIER", "dbo");

                entity.HasIndex(e => e.CptMdsSuprtTier)
                    .HasName("UX01_LK_CPT_MDS_SUPRT_TIER")
                    .IsUnique();

                entity.Property(e => e.CptMdsSuprtTierId)
                    .HasColumnName("CPT_MDS_SUPRT_TIER_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptMdsSuprtTier)
                    .IsRequired()
                    .HasColumnName("CPT_MDS_SUPRT_TIER")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptMdsSuprtTierCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_MDS_SUPRT_TIER");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptMdsSuprtTierModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_MDS_SUPRT_TIER");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptMdsSuprtTier)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_MDS_SUPRT_TIER");
            });

            modelBuilder.Entity<LkCptMngdAuthPrd>(entity =>
            {
                entity.HasKey(e => e.CptMngdAuthPrdId)
                    .HasName("PK_CPT_MNGD_AUTH_PRD");

                entity.ToTable("LK_CPT_MNGD_AUTH_PRD", "dbo");

                entity.HasIndex(e => e.CptMngdAuthPrd)
                    .HasName("UX01_LK_CPT_MNGD_AUTH_PRD")
                    .IsUnique();

                entity.Property(e => e.CptMngdAuthPrdId)
                    .HasColumnName("CPT_MNGD_AUTH_PRD_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptMngdAuthPrd)
                    .IsRequired()
                    .HasColumnName("CPT_MNGD_AUTH_PRD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptMngdAuthPrdCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_MNGD_AUTH_PRD");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptMngdAuthPrdModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_MNGD_AUTH_PRD");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptMngdAuthPrd)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_MNGD_AUTH_PRD");
            });

            modelBuilder.Entity<LkCptMvsProdType>(entity =>
            {
                entity.HasKey(e => e.CptMvsProdTypeId);

                entity.ToTable("LK_CPT_MVS_PROD_TYPE", "dbo");

                entity.HasIndex(e => e.CptMvsProdType)
                    .HasName("UX01_LK_CPT_MVS_PROD_TYPE")
                    .IsUnique();

                entity.Property(e => e.CptMvsProdTypeId)
                    .HasColumnName("CPT_MVS_PROD_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptMvsProdType)
                    .IsRequired()
                    .HasColumnName("CPT_MVS_PROD_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptMvsProdTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_MVS_PROD_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptMvsProdTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_MVS_PROD_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptMvsProdType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_MVS_PROD_TYPE");
            });

            modelBuilder.Entity<LkCptPlnSrvcTier>(entity =>
            {
                entity.HasKey(e => e.CptPlnSrvcTierId);

                entity.ToTable("LK_CPT_PLN_SRVC_TIER", "dbo");

                entity.HasIndex(e => e.CptPlnSrvcTier)
                    .HasName("UX01_LK_CPT_PLN_SRVC_TIER")
                    .IsUnique();

                entity.Property(e => e.CptPlnSrvcTierId)
                    .HasColumnName("CPT_PLN_SRVC_TIER_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptPlnSrvcTier)
                    .IsRequired()
                    .HasColumnName("CPT_PLN_SRVC_TIER")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptPlnSrvcTierCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_PLN_SRVC_TIER");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptPlnSrvcTierModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_PLN_SRVC_TIER");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptPlnSrvcTier)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_PLN_SRVC_TIER");
            });

            modelBuilder.Entity<LkCptPlnSrvcType>(entity =>
            {
                entity.HasKey(e => e.CptPlnSrvcTypeId);

                entity.ToTable("LK_CPT_PLN_SRVC_TYPE", "dbo");

                entity.HasIndex(e => e.CptPlnSrvcType)
                    .HasName("UX01_LK_CPT_PLN_SRVC_TYPE")
                    .IsUnique();

                entity.Property(e => e.CptPlnSrvcTypeId)
                    .HasColumnName("CPT_PLN_SRVC_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptPlnSrvcType)
                    .IsRequired()
                    .HasColumnName("CPT_PLN_SRVC_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptPlnSrvcTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_PLN_SRVC_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptPlnSrvcTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_PLN_SRVC_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptPlnSrvcType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_PLN_SRVC_TYPE");
            });

            modelBuilder.Entity<LkCptPrimScndyTprt>(entity =>
            {
                entity.HasKey(e => e.CptPrimScndyTprtId);

                entity.ToTable("LK_CPT_PRIM_SCNDY_TPRT", "dbo");

                entity.HasIndex(e => e.CptPrimScndyTprt)
                    .HasName("UX01_LK_CPT_PRIM_SCNDY_TPRT")
                    .IsUnique();

                entity.Property(e => e.CptPrimScndyTprtId)
                    .HasColumnName("CPT_PRIM_SCNDY_TPRT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptPrimScndyTprt)
                    .IsRequired()
                    .HasColumnName("CPT_PRIM_SCNDY_TPRT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptPrimScndyTprtCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_PRIM_SCNDY_TPRT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptPrimScndyTprtModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_PRIM_SCNDY_TPRT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptPrimScndyTprt)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_PRIM_SCNDY_TPRT");
            });

            modelBuilder.Entity<LkCptPrimSite>(entity =>
            {
                entity.HasKey(e => e.CptPrimSiteId);

                entity.ToTable("LK_CPT_PRIM_SITE", "dbo");

                entity.HasIndex(e => e.CptPrimSite)
                    .HasName("UX01_LK_CPT_PRIM_SITE")
                    .IsUnique();

                entity.Property(e => e.CptPrimSiteId)
                    .HasColumnName("CPT_PRIM_SITE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptPrimSite)
                    .IsRequired()
                    .HasColumnName("CPT_PRIM_SITE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptPrimSiteCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_PRIM_SITE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptPrimSiteModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_PRIM_SITE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptPrimSite)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_PRIM_SITE");
            });

            modelBuilder.Entity<LkCptPrvsnType>(entity =>
            {
                entity.HasKey(e => e.CptPrvsnTypeId);

                entity.ToTable("LK_CPT_PRVSN_TYPE", "dbo");

                entity.HasIndex(e => e.CptPrvsnType)
                    .HasName("UX01_LK_CPT_PRVSN_TYPE")
                    .IsUnique();

                entity.Property(e => e.CptPrvsnTypeId)
                    .HasColumnName("CPT_PRVSN_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CptPrvsnType)
                    .IsRequired()
                    .HasColumnName("CPT_PRVSN_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCptPrvsnTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CPT_PRVSN_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCptPrvsnTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CPT_PRVSN_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCptPrvsnType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CPT_PRVSN_TYPE");
            });

            modelBuilder.Entity<LkCsgLvl>(entity =>
            {
                entity.HasKey(e => e.CsgLvlId);

                entity.ToTable("LK_CSG_LVL", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_CSG_LVL");

                entity.HasIndex(e => e.CsgLvlCd)
                    .HasName("UX01_LK_CSG_LVL")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_CSG_LVL");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_CSG_LVL");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CsgLvlCd)
                    .IsRequired()
                    .HasColumnName("CSG_LVL_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCsgLvlCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CSG_LVL");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCsgLvlModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CSG_LVL");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCsgLvl)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CSG_LVL");
            });

            modelBuilder.Entity<LkCtry>(entity =>
            {
                entity.HasKey(e => e.CtryCd);

                entity.ToTable("LK_CTRY", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_CTRY");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_CTRY");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_LK_CTRY");

                entity.HasIndex(e => e.RgnId)
                    .HasName("IX04_LK_CTRY");

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryNme)
                    .IsRequired()
                    .HasColumnName("CTRY_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsdCd).HasColumnName("ISD_CD");

                entity.Property(e => e.L2pCtryCd)
                    .HasColumnName("L2P_CTRY_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RgnId).HasColumnName("RGN_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCtryCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CTRY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCtryModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CTRY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCtry)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CTRY");

                entity.HasOne(d => d.Rgn)
                    .WithMany(p => p.LkCtry)
                    .HasForeignKey(d => d.RgnId)
                    .HasConstraintName("FK04_LK_CTRY");
            });

            modelBuilder.Entity<LkCtryCty>(entity =>
            {
                entity.HasKey(e => new { e.CtyNme, e.CtryCd });

                entity.ToTable("LK_CTRY_CTY", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_CTRY_CTY");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_CTRY_CTY");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_LK_CTRY_CTY");

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCtryCtyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CTRY_CTY");

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.LkCtryCty)
                    .HasForeignKey(d => d.CtryCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CTRY_CTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCtryCtyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK04_LK_CTRY_CTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCtryCty)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_CTRY_CTY");
            });

            modelBuilder.Entity<LkCtryCxr>(entity =>
            {
                entity.HasKey(e => new { e.CxrCd, e.CtryCd });

                entity.ToTable("LK_CTRY_CXR", "dbo");

                entity.Property(e => e.CxrCd)
                    .HasColumnName("CXR_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.LkCtryCxr)
                    .HasForeignKey(d => d.CtryCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CTRY_CXR");

                entity.HasOne(d => d.CxrCdNavigation)
                    .WithMany(p => p.LkCtryCxr)
                    .HasForeignKey(d => d.CxrCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_CTRY_CXR");
            });

            modelBuilder.Entity<LkCur>(entity =>
            {
                entity.HasKey(e => e.CurId);

                entity.ToTable("LK_CUR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX04_LK_CUR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_CUR");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_LK_CUR");

                entity.Property(e => e.CurId)
                    .HasColumnName("CUR_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CurCnvrsnFctrFromUsdQty)
                    .HasColumnName("CUR_CNVRSN_FCTR_FROM_USD_QTY")
                    .HasColumnType("decimal(26, 16)");

                entity.Property(e => e.CurNme)
                    .IsRequired()
                    .HasColumnName("CUR_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCurCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_CUR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCurModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_CUR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCur)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CUR");
            });

            modelBuilder.Entity<LkCustCntrcLgth>(entity =>
            {
                entity.HasKey(e => e.CustCntrcLgthId);

                entity.ToTable("LK_CUST_CNTRC_LGTH", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_CUST_CNTRC_LGTH");

                entity.HasIndex(e => e.CustCntrcLgthDes)
                    .HasName("UX01_LK_CUST_CNTRC_LGTH")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_CUST_CNTRC_LGTH");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_LK_CUST_CNTRC_LGTH");

                entity.Property(e => e.CustCntrcLgthId)
                    .HasColumnName("CUST_CNTRC_LGTH_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustCntrcLgthDes)
                    .IsRequired()
                    .HasColumnName("CUST_CNTRC_LGTH_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkCustCntrcLgthCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_CUST_CNTRC_LGTH");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkCustCntrcLgthModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_CUST_CNTRC_LGTH");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkCustCntrcLgth)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_CUST_CNTRC_LGTH");
            });

            modelBuilder.Entity<LkDedctdCust>(entity =>
            {
                entity.HasKey(e => e.CustId);

                entity.ToTable("LK_DEDCTD_CUST", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_DEDCTD_CUST");

                entity.HasIndex(e => e.CustNme)
                    .HasName("UX01_LK_DEDCTD_CUST")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_DEDCTD_CUST");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_DEDCTD_CUST");

                entity.Property(e => e.CustId)
                    .HasColumnName("CUST_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustNme)
                    .IsRequired()
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkDedctdCustCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_DEDCTD_CUST");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkDedctdCustModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_DEDCTD_CUST");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkDedctdCust)
                    .HasForeignKey(d => d.RecStusId)
                    .HasConstraintName("FK02_LK_DEDCTD_CUST");
            });

            modelBuilder.Entity<LkDev>(entity =>
            {
                entity.HasKey(e => e.DevId);

                entity.ToTable("LK_DEV", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_DEV");

                entity.HasIndex(e => e.DevNme)
                    .HasName("UX01_LK_DEV")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_DEV");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_DEV");

                entity.Property(e => e.DevId)
                    .HasColumnName("DEV_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DevNme)
                    .IsRequired()
                    .HasColumnName("DEV_NME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkDevCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_DEV");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkDevModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_DEV");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkDev)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_DEV");
            });

            modelBuilder.Entity<LkDevManf>(entity =>
            {
                entity.HasKey(e => e.ManfId);

                entity.ToTable("LK_DEV_MANF", "dbo");

                entity.HasIndex(e => e.ManfNme)
                    .HasName("UX01_LK_DEV_MANF")
                    .IsUnique();

                entity.Property(e => e.ManfId)
                    .HasColumnName("MANF_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ManfNme)
                    .IsRequired()
                    .HasColumnName("MANF_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkDevManf)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_DEV_MANF");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkDevManf)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_DEV_MANF");
            });

            modelBuilder.Entity<LkDevModel>(entity =>
            {
                entity.HasKey(e => e.DevModelId);

                entity.ToTable("LK_DEV_MODEL", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_DEV_MODEL");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_DEV_MODEL");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_DEV_MODEL");

                entity.HasIndex(e => new { e.ManfId, e.DevModelNme })
                    .HasName("UX01_LK_DEV_MODEL")
                    .IsUnique();

                entity.Property(e => e.DevModelId)
                    .HasColumnName("DEV_MODEL_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DevModelNme)
                    .IsRequired()
                    .HasColumnName("DEV_MODEL_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ManfId).HasColumnName("MANF_ID");

                entity.Property(e => e.MinDrtnTmeReqrAmt).HasColumnName("MIN_DRTN_TME_REQR_AMT");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkDevModelCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_DEV_MODEL");

                entity.HasOne(d => d.Manf)
                    .WithMany(p => p.LkDevModel)
                    .HasForeignKey(d => d.ManfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_DEV_MODEL");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkDevModelModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_DEV_MODEL");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkDevModel)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_DEV_MODEL");
            });

            modelBuilder.Entity<LkDmstcClliCd>(entity =>
            {
                entity.ToTable("LK_DMSTC_CLLI_CD", "dbo");

                entity.HasIndex(e => e.ClliCd)
                    .HasName("UX01_LK_DMSTC_CLLI_CD")
                    .IsUnique();

                entity.Property(e => e.LkDmstcClliCdId).HasColumnName("LK_DMSTC_CLLI_CD_ID");

                entity.Property(e => e.ClliCd)
                    .IsRequired()
                    .HasColumnName("CLLI_CD")
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkDmstcClliCdCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_DMSTC_CLLI_CD");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkDmstcClliCdModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_DMSTC_CLLI_CD");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkDmstcClliCd)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_DMSTC_CLLI_CD");
            });

            modelBuilder.Entity<LkEmailReqType>(entity =>
            {
                entity.HasKey(e => e.EmailReqTypeId);

                entity.ToTable("LK_EMAIL_REQ_TYPE", "dbo");

                entity.HasIndex(e => e.EmailReqTypeDes)
                    .HasName("UX01_LK_EMAIL_REQ_TYPE")
                    .IsUnique();

                entity.Property(e => e.EmailReqTypeId)
                    .HasColumnName("EMAIL_REQ_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailReqType)
                    .IsRequired()
                    .HasColumnName("EMAIL_REQ_TYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmailReqTypeDes)
                    .IsRequired()
                    .HasColumnName("EMAIL_REQ_TYPE_DES")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkEnhncSrvc>(entity =>
            {
                entity.HasKey(e => e.EnhncSrvcId);

                entity.ToTable("LK_ENHNC_SRVC", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_ENHNC_SRVC");

                entity.HasIndex(e => e.EnhncSrvcNme)
                    .HasName("UX01_LK_ENHNC_SRVC")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_ENHNC_SRVC");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_ENHNC_SRVC");

                entity.Property(e => e.EnhncSrvcId)
                    .HasColumnName("ENHNC_SRVC_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EnhncSrvcNme)
                    .IsRequired()
                    .HasColumnName("ENHNC_SRVC_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.MinDrtnTmeReqrAmt).HasColumnName("MIN_DRTN_TME_REQR_AMT");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkEnhncSrvcCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_ENHNC_SRVC");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkEnhncSrvc)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_ENHNC_SRVC");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkEnhncSrvcModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_ENHNC_SRVC");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkEnhncSrvc)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_ENHNC_SRVC");
            });

            modelBuilder.Entity<LkEsclReas>(entity =>
            {
                entity.HasKey(e => e.EsclReasId);

                entity.ToTable("LK_ESCL_REAS", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_ESCL_REAS");

                entity.HasIndex(e => e.EsclReasDes)
                    .HasName("UX01_LK_ESCL_REAS")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_ESCL_REAS");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_ESCL_REAS");

                entity.Property(e => e.EsclReasId).HasColumnName("ESCL_REAS_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EsclReasDes)
                    .IsRequired()
                    .HasColumnName("ESCL_REAS_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkEsclReasCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_ESCL_REAS");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkEsclReasModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_ESCL_REAS");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkEsclReas)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_ESCL_REAS");
            });

            modelBuilder.Entity<LkEventRule>(entity =>
            {
                entity.HasKey(e => e.EventRuleId);

                entity.ToTable("LK_EVENT_RULE", "dbo");

                entity.HasIndex(e => e.EventTypeId)
                    .HasName("IX01_LK_EVENT_RULE");

                entity.Property(e => e.EventRuleId)
                    .HasColumnName("EVENT_RULE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActnId).HasColumnName("ACTN_ID");

                entity.Property(e => e.CalCd).HasColumnName("CAL_CD");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DsplCd).HasColumnName("DSPL_CD");

                entity.Property(e => e.EndEventStusId).HasColumnName("END_EVENT_STUS_ID");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.FailReasId).HasColumnName("FAIL_REAS_ID");

                entity.Property(e => e.FedMsgId).HasColumnName("FED_MSG_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrTypeCd)
                    .HasColumnName("ORDR_TYPE_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.StagnCd).HasColumnName("STAGN_CD");

                entity.Property(e => e.StrtEventStusId).HasColumnName("STRT_EVENT_STUS_ID");

                entity.Property(e => e.UsrPrfId).HasColumnName("USR_PRF_ID");

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.HasOne(d => d.Actn)
                    .WithMany(p => p.LkEventRule)
                    .HasForeignKey(d => d.ActnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK08_LK_EVENT_RULE");

                entity.HasOne(d => d.EndEventStus)
                    .WithMany(p => p.LkEventRuleEndEventStus)
                    .HasForeignKey(d => d.EndEventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_LK_EVENT_RULE");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkEventRule)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_EVENT_RULE");

                entity.HasOne(d => d.FailReas)
                    .WithMany(p => p.LkEventRule)
                    .HasForeignKey(d => d.FailReasId)
                    .HasConstraintName("FK09_LK_EVENT_RULE");

                entity.HasOne(d => d.FedMsg)
                    .WithMany(p => p.LkEventRule)
                    .HasForeignKey(d => d.FedMsgId)
                    .HasConstraintName("FK10_LK_EVENT_RULE");

                entity.HasOne(d => d.OrdrTypeCdNavigation)
                    .WithMany(p => p.LkEventRule)
                    .HasForeignKey(d => d.OrdrTypeCd)
                    .HasConstraintName("FK07_LK_EVENT_RULE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkEventRule)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_EVENT_RULE");

                entity.HasOne(d => d.StrtEventStus)
                    .WithMany(p => p.LkEventRuleStrtEventStus)
                    .HasForeignKey(d => d.StrtEventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_EVENT_RULE");

                entity.HasOne(d => d.UsrPrf)
                    .WithMany(p => p.LkEventRule)
                    .HasForeignKey(d => d.UsrPrfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_EVENT_RULE");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.LkEventRule)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .HasConstraintName("FK06_LK_EVENT_RULE");
            });

            modelBuilder.Entity<LkEventStus>(entity =>
            {
                entity.HasKey(e => e.EventStusId);

                entity.ToTable("LK_EVENT_STUS", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_EVENT_STUS");

                entity.HasIndex(e => e.EventStusDes)
                    .HasName("UX01_LK_EVENT_STUS")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_EVENT_STUS");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_EVENT_STUS");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventStusDes)
                    .IsRequired()
                    .HasColumnName("EVENT_STUS_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkEventStusCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_EVENT_STUS");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkEventStusModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_EVENT_STUS");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkEventStus)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_EVENT_STUS");
            });

            modelBuilder.Entity<LkEventType>(entity =>
            {
                entity.HasKey(e => e.EventTypeId);

                entity.ToTable("LK_EVENT_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_EVENT_TYPE");

                entity.HasIndex(e => e.EventTypeNme)
                    .HasName("UX01_LK_EVENT_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_EVENT_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_EVENT_TYPE");

                entity.Property(e => e.EventTypeId)
                    .HasColumnName("EVENT_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventTypeNme)
                    .IsRequired()
                    .HasColumnName("EVENT_TYPE_NME")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkEventTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_EVENT_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkEventTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_EVENT_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkEventType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_EVENT_TYPE");
            });

            modelBuilder.Entity<LkEventTypeTmeSlot>(entity =>
            {
                entity.HasKey(e => new { e.TmeSlotId, e.EventTypeId });

                entity.ToTable("LK_EVENT_TYPE_TME_SLOT", "dbo");

                entity.HasIndex(e => new { e.EventTypeId, e.TmeSlotStrtTme, e.TmeSlotEndTme })
                    .HasName("UX01_LK_EVENT_TYPE_TME_SLOT")
                    .IsUnique();

                entity.Property(e => e.TmeSlotId).HasColumnName("TME_SLOT_ID");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.AvlbltyCapPctQty).HasColumnName("AVLBLTY_CAP_PCT_QTY");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TmeSlotEndTme).HasColumnName("TME_SLOT_END_TME");

                entity.Property(e => e.TmeSlotStrtTme).HasColumnName("TME_SLOT_STRT_TME");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkEventTypeTmeSlot)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_EVENT_TYPE_TME_SLOT");
            });

            modelBuilder.Entity<LkFailActy>(entity =>
            {
                entity.HasKey(e => e.FailActyId);

                entity.ToTable("LK_FAIL_ACTY", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_FAIL_ACTY");

                entity.HasIndex(e => e.FailActyDes)
                    .HasName("UX01_LK_FAIL_ACTY")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_FAIL_ACTY");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_FAIL_ACTY");

                entity.Property(e => e.FailActyId)
                    .HasColumnName("FAIL_ACTY_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventTypeId)
                    .HasColumnName("EVENT_TYPE_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FailActyDes)
                    .IsRequired()
                    .HasColumnName("FAIL_ACTY_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkFailActyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_FAIL_ACTY");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkFailActy)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_FAIL_ACTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkFailActyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_FAIL_ACTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkFailActy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_FAIL_ACTY");
            });

            modelBuilder.Entity<LkFailReas>(entity =>
            {
                entity.HasKey(e => e.FailReasId);

                entity.ToTable("LK_FAIL_REAS", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_FAIL_REAS");

                entity.HasIndex(e => e.FailReasDes)
                    .HasName("UX01_LK_FAIL_REAS")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_FAIL_REAS");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_FAIL_REAS");

                entity.Property(e => e.FailReasId)
                    .HasColumnName("FAIL_REAS_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FailReasDes)
                    .IsRequired()
                    .HasColumnName("FAIL_REAS_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MdsFastTrkCd).HasColumnName("MDS_FAST_TRK_CD");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NtwkOnlyCd).HasColumnName("NTWK_ONLY_CD");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkFailReasCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_FAIL_REAS");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkFailReasModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_FAIL_REAS");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkFailReas)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_FAIL_REAS");
            });

            modelBuilder.Entity<LkFedMsg>(entity =>
            {
                entity.HasKey(e => e.FedMsgId);

                entity.ToTable("LK_FED_MSG", "dbo");

                entity.Property(e => e.FedMsgId).HasColumnName("FED_MSG_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FedMsgDes)
                    .IsRequired()
                    .HasColumnName("FED_MSG_DES")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkFedMsg)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_FED_MSG");
            });

            modelBuilder.Entity<LkFedRuleFail>(entity =>
            {
                entity.HasKey(e => e.FedRuleFailId);

                entity.ToTable("LK_FED_RULE_FAIL", "dbo");

                entity.Property(e => e.FedRuleFailId).HasColumnName("FED_RULE_FAIL_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventRuleId).HasColumnName("EVENT_RULE_ID");

                entity.Property(e => e.FailCdId).HasColumnName("FAIL_CD_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.EventRule)
                    .WithMany(p => p.LkFedRuleFail)
                    .HasForeignKey(d => d.EventRuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_FED_RULE_FAIL");

                entity.HasOne(d => d.FailCd)
                    .WithMany(p => p.LkFedRuleFail)
                    .HasForeignKey(d => d.FailCdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_FED_RULE_FAIL");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkFedRuleFail)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_FED_RULE_FAIL");
            });

            modelBuilder.Entity<LkFedlineEventFailCd>(entity =>
            {
                entity.HasKey(e => e.FailCdId);

                entity.ToTable("LK_FEDLINE_EVENT_FAIL_CD", "dbo");

                entity.HasIndex(e => e.FailCdDes)
                    .HasName("UX01_LK_FEDLINE_EVENT_FAIL_CD")
                    .IsUnique();

                entity.Property(e => e.FailCdId)
                    .HasColumnName("FAIL_CD_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FailCdDes)
                    .IsRequired()
                    .HasColumnName("FAIL_CD_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.RspbPartyNme)
                    .HasColumnName("RSPB_PARTY_NME")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkFedlineEventFailCdCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK02_FEDLINE_EVENT_FAIL_CD");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkFedlineEventFailCdModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_FEDLINE_EVENT_FAIL_CD");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkFedlineEventFailCd)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_FEDLINE_EVENT_FAIL_CD");
            });

            modelBuilder.Entity<LkFedlineOrdrType>(entity =>
            {
                entity.HasKey(e => e.OrdrTypeCd);

                entity.ToTable("LK_FEDLINE_ORDR_TYPE", "dbo");

                entity.Property(e => e.OrdrTypeCd)
                    .HasColumnName("ORDR_TYPE_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrTypeDes)
                    .IsRequired()
                    .HasColumnName("ORDR_TYPE_DES")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PrntOrdrTypeDes)
                    .HasColumnName("PRNT_ORDR_TYPE_DES")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkFedlineOrdrType)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK02_LK_FEDLINE_ORDR_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkFedlineOrdrType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_FEDLINE_ORDR_TYPE");
            });

            modelBuilder.Entity<LkFiltrOpr>(entity =>
            {
                entity.HasKey(e => e.FiltrOprId);

                entity.ToTable("LK_FILTR_OPR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_FILTR_OPR");

                entity.HasIndex(e => e.FiltrOprDes)
                    .HasName("UX01_LK_FILTR_OPR")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_FILTR_OPR");

                entity.Property(e => e.FiltrOprId)
                    .HasColumnName("FILTR_OPR_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FiltrOprDes)
                    .IsRequired()
                    .HasColumnName("FILTR_OPR_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('equal')");

                entity.Property(e => e.LogicOprCd)
                    .IsRequired()
                    .HasColumnName("LOGIC_OPR_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkFiltrOprCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_FILTR_OPR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkFiltrOprModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_FILTR_OPR");
            });

            modelBuilder.Entity<LkFrgnCxr>(entity =>
            {
                entity.HasKey(e => e.CxrCd);

                entity.ToTable("LK_FRGN_CXR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_FRGN_CXR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_FRGN_CXR");

                entity.HasIndex(e => new { e.CxrCd, e.CxrNme })
                    .HasName("UX01_LK_FRGN_CXR")
                    .IsUnique();

                entity.Property(e => e.CxrCd)
                    .HasColumnName("CXR_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CxrNme)
                    .IsRequired()
                    .HasColumnName("CXR_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkFrgnCxrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_FRGN_CXR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkFrgnCxrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_FRGN_CXR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkFrgnCxr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_FRGN_CXR");
            });

            modelBuilder.Entity<LkFsaOrdrType>(entity =>
            {
                entity.HasKey(e => e.FsaOrdrTypeCd);

                entity.ToTable("LK_FSA_ORDR_TYPE", "dbo");

                entity.HasIndex(e => e.FsaOrdrTypeDes)
                    .HasName("UX01_LK_FSA_ORDR_TYPE")
                    .IsUnique();

                entity.Property(e => e.FsaOrdrTypeCd)
                    .HasColumnName("FSA_ORDR_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FsaOrdrTypeDes)
                    .IsRequired()
                    .HasColumnName("FSA_ORDR_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkFsaProdType>(entity =>
            {
                entity.HasKey(e => e.FsaProdTypeCd);

                entity.ToTable("LK_FSA_PROD_TYPE", "dbo");

                entity.HasIndex(e => e.FsaProdTypeDes)
                    .HasName("UX01_LK_FSA_PROD_TYPE")
                    .IsUnique();

                entity.Property(e => e.FsaProdTypeCd)
                    .HasColumnName("FSA_PROD_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FsaProdTypeDes)
                    .IsRequired()
                    .HasColumnName("FSA_PROD_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkGrp>(entity =>
            {
                entity.HasKey(e => e.GrpId);

                entity.ToTable("LK_GRP", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_GRP");

                entity.HasIndex(e => e.GrpNme)
                    .HasName("UX01_LK_GRP")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_GRP");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_GRP");

                entity.Property(e => e.GrpId)
                    .HasColumnName("GRP_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GrpNme)
                    .IsRequired()
                    .HasColumnName("GRP_NME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkGrpCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_GRP");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkGrpModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_GRP");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkGrp)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_GRP");
            });

            modelBuilder.Entity<LkIntlClliCd>(entity =>
            {
                entity.ToTable("LK_INTL_CLLI_CD", "dbo");

                entity.HasIndex(e => e.ClliCd)
                    .HasName("UX01_LK_INTL_CLLI_CD")
                    .IsUnique();

                entity.Property(e => e.LkIntlClliCdId).HasColumnName("LK_INTL_CLLI_CD_ID");

                entity.Property(e => e.ClliCd)
                    .IsRequired()
                    .HasColumnName("CLLI_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkIntlClliCdCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_INTL_CLLI_CD");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkIntlClliCdModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_INTL_CLLI_CD");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkIntlClliCd)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_INTL_CLLI_CD");
            });

            modelBuilder.Entity<LkIpMstr>(entity =>
            {
                entity.ToTable("LK_IP_MSTR", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndIpAdr)
                    .IsRequired()
                    .HasColumnName("END_IP_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.StrtIpAdr)
                    .IsRequired()
                    .HasColumnName("STRT_IP_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SystmCd)
                    .IsRequired()
                    .HasColumnName("SYSTM_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkIpMstrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_IP_MSTR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkIpMstrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_IP_MSTR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkIpMstr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_IP_MSTR");
            });

            modelBuilder.Entity<LkIpVer>(entity =>
            {
                entity.HasKey(e => e.IpVerId);

                entity.ToTable("LK_IP_VER", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_IP_VER");

                entity.HasIndex(e => e.IpVerNme)
                    .HasName("UX01_LK_IP_VER")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_IP_VER");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_IP_VER");

                entity.Property(e => e.IpVerId).HasColumnName("IP_VER_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IpVerNme)
                    .IsRequired()
                    .HasColumnName("IP_VER_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkIpVerCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_IP_VER");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkIpVerModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_IP_VER");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkIpVer)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_IP_VER");
            });

            modelBuilder.Entity<LkMds3rdpartySrvcLvl>(entity =>
            {
                entity.HasKey(e => e.ThrdPartySrvcLvlId);

                entity.ToTable("LK_MDS_3RDPARTY_SRVC_LVL", "dbo");

                entity.Property(e => e.ThrdPartySrvcLvlId)
                    .HasColumnName("THRD_PARTY_SRVC_LVL_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SrvcTypeId).HasColumnName("SRVC_TYPE_ID");

                entity.Property(e => e.ThrdPartySrvcLvlDes)
                    .IsRequired()
                    .HasColumnName("THRD_PARTY_SRVC_LVL_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMds3rdpartySrvcLvl)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MDS_3RDPARTY_SRVC_LVL");

                entity.HasOne(d => d.SrvcType)
                    .WithMany(p => p.LkMds3rdpartySrvcLvl)
                    .HasForeignKey(d => d.SrvcTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MDS_3RDPARTY_SRVC_LVL");
            });

            modelBuilder.Entity<LkMds3rdpartyVndr>(entity =>
            {
                entity.HasKey(e => e.ThrdPartyVndrId);

                entity.ToTable("LK_MDS_3RDPARTY_VNDR", "dbo");

                entity.Property(e => e.ThrdPartyVndrId)
                    .HasColumnName("THRD_PARTY_VNDR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SrvcTypeId).HasColumnName("SRVC_TYPE_ID");

                entity.Property(e => e.ThrdPartyVndrDes)
                    .IsRequired()
                    .HasColumnName("THRD_PARTY_VNDR_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMds3rdpartyVndrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MDS_3RDPARTY_VNDR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMds3rdpartyVndrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_MDS_3RDPARTY_VNDR");

                entity.HasOne(d => d.SrvcType)
                    .WithMany(p => p.LkMds3rdpartyVndr)
                    .HasForeignKey(d => d.SrvcTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MDS_3RDPARTY_VNDR");
            });

            modelBuilder.Entity<LkMdsActyType>(entity =>
            {
                entity.HasKey(e => e.MdsActyTypeId);

                entity.ToTable("LK_MDS_ACTY_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MDS_ACTY_TYPE");

                entity.HasIndex(e => e.MdsActyTypeDes)
                    .HasName("UX01_LK_MDS_ACTY_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MDS_ACTY_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MDS_ACTY_TYPE");

                entity.Property(e => e.MdsActyTypeId).HasColumnName("MDS_ACTY_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MdsActyTypeDes)
                    .IsRequired()
                    .HasColumnName("MDS_ACTY_TYPE_DES")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMdsActyTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MDS_ACTY_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMdsActyTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_MDS_ACTY_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMdsActyType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MDS_ACTY_TYPE");
            });

            modelBuilder.Entity<LkMdsBrdgNeedReas>(entity =>
            {
                entity.HasKey(e => e.MdsBrdgNeedId);

                entity.ToTable("LK_MDS_BRDG_NEED_REAS", "dbo");

                entity.Property(e => e.MdsBrdgNeedId).HasColumnName("MDS_BRDG_NEED_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MdsBrdgNeedTxt)
                    .IsRequired()
                    .HasColumnName("MDS_BRDG_NEED_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMdsBrdgNeedReasCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK01_LK_MDS_BRDG_NEEDED_REAS");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMdsBrdgNeedReasModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_MDS_BRDG_NEEDED_REAS");
            });

            modelBuilder.Entity<LkMdsCpeDevXclusn>(entity =>
            {
                entity.HasKey(e => e.MdsCpeDevXclusnId);

                entity.ToTable("LK_MDS_CPE_DEV_XCLUSN", "dbo");

                entity.HasIndex(e => e.DevTypeNme)
                    .HasName("UX01_LK_MDS_CPE_DEV_XCLUSN")
                    .IsUnique();

                entity.Property(e => e.MdsCpeDevXclusnId)
                    .HasColumnName("MDS_CPE_DEV_XCLUSN_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DevTypeNme)
                    .IsRequired()
                    .HasColumnName("DEV_TYPE_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMdsCpeDevXclusn)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MDS_CPE_DEV_XCLUSN");
            });

            modelBuilder.Entity<LkMdsImplmtnNtvl>(entity =>
            {
                entity.HasKey(e => e.EventTypeId);

                entity.ToTable("LK_MDS_IMPLMTN_NTVL", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MDS_IMPLMTN_NTVL");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX01_LK_MDS_IMPLMTN_NTVL");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_LK_MDS_IMPLMTN_NTVL");

                entity.Property(e => e.EventTypeId)
                    .HasColumnName("EVENT_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CpeNtvlInDayQty).HasColumnName("CPE_NTVL_IN_DAY_QTY");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NonCpeNtvlInDayQty).HasColumnName("NON_CPE_NTVL_IN_DAY_QTY");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMdsImplmtnNtvlCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MDS_IMPLMTN_NTVL");

                entity.HasOne(d => d.EventType)
                    .WithOne(p => p.LkMdsImplmtnNtvl)
                    .HasForeignKey<LkMdsImplmtnNtvl>(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_MDS_IMPLMTN_NTVL");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMdsImplmtnNtvlModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK01_LK_MDS_IMPLMTN_NTVL");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMdsImplmtnNtvl)
                    .HasForeignKey(d => d.RecStusId)
                    .HasConstraintName("FK03_LK_MDS_IMPLMTN_NTVL");
            });

            modelBuilder.Entity<LkMdsMacActy>(entity =>
            {
                entity.HasKey(e => e.MdsMacActyId);

                entity.ToTable("LK_MDS_MAC_ACTY", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MDS_MAC_ACTY");

                entity.HasIndex(e => e.MdsMacActyNme)
                    .HasName("UX01_LK_MDS_MAC_ACTY")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MDS_MAC_ACTY");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MDS_MAC_ACTY");

                entity.Property(e => e.MdsMacActyId)
                    .HasColumnName("MDS_MAC_ACTY_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MdsMacActyNme)
                    .IsRequired()
                    .HasColumnName("MDS_MAC_ACTY_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MinDrtnTmeReqrAmt).HasColumnName("MIN_DRTN_TME_REQR_AMT");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMdsMacActyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MDS_MAC_ACTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMdsMacActyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_MDS_MAC_ACTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMdsMacActy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MDS_MAC_ACTY");
            });

            modelBuilder.Entity<LkMdsNtwkActyType>(entity =>
            {
                entity.HasKey(e => e.NtwkActyTypeId);

                entity.ToTable("LK_MDS_NTWK_ACTY_TYPE", "dbo");

                entity.HasIndex(e => e.NtwkActyTypeCd)
                    .HasName("UX01_LK_MDS_NTWK_ACTY_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.NtwkActyTypeDes)
                    .HasName("UX02_LK_MDS_NTWK_ACTY_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MDS_NTWK_ACTY_TYPE");

                entity.Property(e => e.NtwkActyTypeId).HasColumnName("NTWK_ACTY_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NtwkActyTypeCd)
                    .IsRequired()
                    .HasColumnName("NTWK_ACTY_TYPE_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.NtwkActyTypeDes)
                    .IsRequired()
                    .HasColumnName("NTWK_ACTY_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMdsNtwkActyType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MDS_NTWK_ACTY_TYPE");
            });

            modelBuilder.Entity<LkMdsSrvcTier>(entity =>
            {
                entity.HasKey(e => e.MdsSrvcTierId);

                entity.ToTable("LK_MDS_SRVC_TIER", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MDS_SRVC_TIER");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MDS_SRVC_TIER");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MDS_SRVC_TIER");

                entity.Property(e => e.MdsSrvcTierId).HasColumnName("MDS_SRVC_TIER_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.MdsSrvcTierCd)
                    .HasColumnName("MDS_SRVC_TIER_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MdsSrvcTierDes)
                    .IsRequired()
                    .HasColumnName("MDS_SRVC_TIER_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMdsSrvcTierCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_MDS_SRVC_TIER");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkMdsSrvcTier)
                    .HasForeignKey(d => d.EventTypeId)
                    .HasConstraintName("FK04_LK_MDS_SRVC_TIER");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMdsSrvcTierModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_MDS_SRVC_TIER");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMdsSrvcTier)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MDS_SRVC_TIER");
            });

            modelBuilder.Entity<LkMdsSrvcType>(entity =>
            {
                entity.HasKey(e => e.SrvcTypeId);

                entity.ToTable("LK_MDS_SRVC_TYPE", "dbo");

                entity.Property(e => e.SrvcTypeId)
                    .HasColumnName("SRVC_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SrvcTypeDes)
                    .IsRequired()
                    .HasColumnName("SRVC_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkMenu>(entity =>
            {
                entity.HasKey(e => e.MenuId);

                entity.ToTable("LK_MENU", "dbo");

                entity.HasIndex(e => e.MenuNme)
                    .HasName("IX01_LK_MENU");

                entity.Property(e => e.MenuId)
                    .HasColumnName("MENU_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DpthLvl).HasColumnName("DPTH_LVL");

                entity.Property(e => e.DsplOrdr).HasColumnName("DSPL_ORDR");

                entity.Property(e => e.MenuDes)
                    .IsRequired()
                    .HasColumnName("MENU_DES")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.MenuNme)
                    .IsRequired()
                    .HasColumnName("MENU_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PrntMenuId).HasColumnName("PRNT_MENU_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SrcPath)
                    .HasColumnName("SRC_PATH")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMenu)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MENU");
            });

            modelBuilder.Entity<LkMnsOffrgRoutr>(entity =>
            {
                entity.HasKey(e => e.MnsOffrgRoutrId);

                entity.ToTable("LK_MNS_OFFRG_ROUTR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MNS_OFFRG_ROUTR");

                entity.HasIndex(e => e.MnsOffrgRoutrDes)
                    .HasName("UX01_LK_MNS_OFFRG_ROUTR")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MNS_OFFRG_ROUTR");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MNS_OFFRG_ROUTR");

                entity.Property(e => e.MnsOffrgRoutrId).HasColumnName("MNS_OFFRG_ROUTR_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MnsOffrgRoutrDes)
                    .IsRequired()
                    .HasColumnName("MNS_OFFRG_ROUTR_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMnsOffrgRoutrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MNS_OFFRG_ROUTR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMnsOffrgRoutrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_MNS_OFFRG_ROUTR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMnsOffrgRoutr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MNS_OFFRG_ROUTR");
            });

            modelBuilder.Entity<LkMnsPrfmcRpt>(entity =>
            {
                entity.HasKey(e => e.MnsPrfmcRptId);

                entity.ToTable("LK_MNS_PRFMC_RPT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MNS_PRFMC_RPT");

                entity.HasIndex(e => e.MnsPrfmcRptDes)
                    .HasName("UX01_LK_MNS_PRFMC_RPT")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MNS_PRFMC_RPT");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MNS_PRFMC_RPT");

                entity.Property(e => e.MnsPrfmcRptId).HasColumnName("MNS_PRFMC_RPT_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MnsPrfmcRptDes)
                    .IsRequired()
                    .HasColumnName("MNS_PRFMC_RPT_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMnsPrfmcRptCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MNS_PRFMC_RPT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMnsPrfmcRptModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_MNS_PRFMC_RPT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMnsPrfmcRpt)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MNS_PRFMC_RPT");
            });

            modelBuilder.Entity<LkMnsRoutgType>(entity =>
            {
                entity.HasKey(e => e.MnsRoutgTypeId);

                entity.ToTable("LK_MNS_ROUTG_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MNS_ROUTG_TYPE");

                entity.HasIndex(e => e.MnsRoutgTypeDes)
                    .HasName("UX01_LK_MNS_ROUTG_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MNS_ROUTG_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MNS_ROUTG_TYPE");

                entity.Property(e => e.MnsRoutgTypeId)
                    .HasColumnName("MNS_ROUTG_TYPE_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MnsRoutgTypeDes)
                    .IsRequired()
                    .HasColumnName("MNS_ROUTG_TYPE_DES")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMnsRoutgTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_MNS_ROUTG_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMnsRoutgTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_MNS_ROUTG_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMnsRoutgType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MNS_ROUTG_TYPE");
            });

            modelBuilder.Entity<LkMplsAccsBdwd>(entity =>
            {
                entity.HasKey(e => e.MplsAccsBdwdId);

                entity.ToTable("LK_MPLS_ACCS_BDWD", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MPLS_ACCS_BDWD");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MPLS_ACCS_BDWD");

                entity.HasIndex(e => e.MplsAccsBdwdDes)
                    .HasName("UX01_LK_MPLS_ACCS_BDWD")
                    .IsUnique();

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MPLS_ACCS_BDWD");

                entity.Property(e => e.MplsAccsBdwdId).HasColumnName("MPLS_ACCS_BDWD_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MplsAccsBdwdDes)
                    .IsRequired()
                    .HasColumnName("MPLS_ACCS_BDWD_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMplsAccsBdwdCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_MPLS_ACCS_BDWD");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMplsAccsBdwdModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_MPLS_ACCS_BDWD");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMplsAccsBdwd)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MPLS_ACCS_BDWD");
            });

            modelBuilder.Entity<LkMplsActyType>(entity =>
            {
                entity.HasKey(e => e.MplsActyTypeId);

                entity.ToTable("LK_MPLS_ACTY_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MPLS_ACTY_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MPLS_ACTY_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MPLS_ACTY_TYPE");

                entity.HasIndex(e => new { e.MplsActyTypeDes, e.EventTypeId })
                    .HasName("UX01_LK_MPLS_ACTY_TYPE")
                    .IsUnique();

                entity.Property(e => e.MplsActyTypeId).HasColumnName("MPLS_ACTY_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MplsActyTypeDes)
                    .IsRequired()
                    .HasColumnName("MPLS_ACTY_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMplsActyTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_MPLS_ACTY_TYPE");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkMplsActyType)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_MPLS_ACTY_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMplsActyTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_MPLS_ACTY_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMplsActyType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MPLS_ACTY_TYPE");
            });

            modelBuilder.Entity<LkMplsEventType>(entity =>
            {
                entity.HasKey(e => e.MplsEventTypeId);

                entity.ToTable("LK_MPLS_EVENT_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MPLS_EVENT_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MPLS_EVENT_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MPLS_EVENT_TYPE");

                entity.HasIndex(e => new { e.MplsEventTypeDes, e.EventTypeId })
                    .HasName("UX01_LK_MPLS_EVENT_TYPE")
                    .IsUnique();

                entity.Property(e => e.MplsEventTypeId).HasColumnName("MPLS_EVENT_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MplsEventTypeDes)
                    .IsRequired()
                    .HasColumnName("MPLS_EVENT_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMplsEventTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_MPLS_EVENT_TYPE");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkMplsEventType)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_MPLS_EVENT_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMplsEventTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_MPLS_EVENT_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMplsEventType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MPLS_EVENT_TYPE");
            });

            modelBuilder.Entity<LkMplsMgrtnType>(entity =>
            {
                entity.HasKey(e => e.MplsMgrtnTypeId);

                entity.ToTable("LK_MPLS_MGRTN_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MPLS_MGRTN_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MPLS_MGRTN_TYPE");

                entity.HasIndex(e => e.MplsMgrtnTypeDes)
                    .HasName("UX01_LK_MPLS_MGRTN_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MPLS_MGRTN_TYPE");

                entity.Property(e => e.MplsMgrtnTypeId).HasColumnName("MPLS_MGRTN_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MplsMgrtnTypeDes)
                    .IsRequired()
                    .HasColumnName("MPLS_MGRTN_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMplsMgrtnTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MPLS_MGRTN_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMplsMgrtnTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_MPLS_MGRTN_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMplsMgrtnType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MPLS_MGRTN_TYPE");
            });

            modelBuilder.Entity<LkMultiVrfReq>(entity =>
            {
                entity.HasKey(e => e.MultiVrfReqId);

                entity.ToTable("LK_MULTI_VRF_REQ", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_MULTI_VRF_REQ");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_MULTI_VRF_REQ");

                entity.HasIndex(e => e.MultiVrfReqDes)
                    .HasName("UX01_LK_MULTI_VRF_REQ")
                    .IsUnique();

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_MULTI_VRF_REQ");

                entity.Property(e => e.MultiVrfReqId)
                    .HasColumnName("MULTI_VRF_REQ_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MultiVrfReqDes)
                    .IsRequired()
                    .HasColumnName("MULTI_VRF_REQ_DES")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkMultiVrfReqCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_MULTI_VRF_REQ");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkMultiVrfReqModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_MULTI_VRF_REQ");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkMultiVrfReq)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_MULTI_VRF_REQ");
            });

            modelBuilder.Entity<LkNgvnProdType>(entity =>
            {
                entity.HasKey(e => e.NgvnProdTypeId);

                entity.ToTable("LK_NGVN_PROD_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_NGVN_PROD_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_NGVN_PROD_TYPE");

                entity.HasIndex(e => e.NgvnProdTypeDes)
                    .HasName("UX01_LK_NGVN_PROD_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_NGVN_PROD_TYPE");

                entity.Property(e => e.NgvnProdTypeId).HasColumnName("NGVN_PROD_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NgvnProdTypeDes)
                    .IsRequired()
                    .HasColumnName("NGVN_PROD_TYPE_DES")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkNgvnProdTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_NGVN_PROD_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkNgvnProdTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_NGVN_PROD_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkNgvnProdType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_NGVN_PROD_TYPE");
            });

            modelBuilder.Entity<LkNteType>(entity =>
            {
                entity.HasKey(e => e.NteTypeId);

                entity.ToTable("LK_NTE_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_NTE_TYPE");

                entity.HasIndex(e => e.NteTypeDes)
                    .HasName("UX01_LK_NTE_TYPE")
                    .IsUnique();

                entity.Property(e => e.NteTypeId).HasColumnName("NTE_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NteTypeDes)
                    .IsRequired()
                    .HasColumnName("NTE_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkNteTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_NTE_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkNteTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_NTE_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkNteType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_NTE_TYPE");
            });

            modelBuilder.Entity<LkOrdrActn>(entity =>
            {
                entity.HasKey(e => e.OrdrActnId);

                entity.ToTable("LK_ORDR_ACTN", "dbo");

                entity.HasIndex(e => e.OrdrActnDes)
                    .HasName("UX01_LK_ORDR_ACTN")
                    .IsUnique();

                entity.Property(e => e.OrdrActnId).HasColumnName("ORDR_ACTN_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrdrActnDes)
                    .IsRequired()
                    .HasColumnName("ORDR_ACTN_DES")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkOrdrCat>(entity =>
            {
                entity.HasKey(e => e.OrdrCatId);

                entity.ToTable("LK_ORDR_CAT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_ORDR_CAT");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_ORDR_CAT");

                entity.HasIndex(e => e.OrdrCatDes)
                    .HasName("UX01_LK_ORDR_CAT")
                    .IsUnique();

                entity.Property(e => e.OrdrCatId)
                    .HasColumnName("ORDR_CAT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrCatDes)
                    .IsRequired()
                    .HasColumnName("ORDR_CAT_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkOrdrCatCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_ORDR_CAT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkOrdrCatModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_ORDR_CAT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkOrdrCat)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_ORDR_CAT");
            });

            modelBuilder.Entity<LkOrdrStus>(entity =>
            {
                entity.HasKey(e => e.OrdrStusId);

                entity.ToTable("LK_ORDR_STUS", "dbo");

                entity.HasIndex(e => e.OrdrStusDes)
                    .HasName("UX01_LK_ORDR_STUS")
                    .IsUnique();

                entity.Property(e => e.OrdrStusId).HasColumnName("ORDR_STUS_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrdrStusDes)
                    .IsRequired()
                    .HasColumnName("ORDR_STUS_DES")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkOrdrSubType>(entity =>
            {
                entity.HasKey(e => e.OrdrSubTypeId);

                entity.ToTable("LK_ORDR_SUB_TYPE", "dbo");

                entity.HasIndex(e => e.OrdrSubTypeCd)
                    .HasName("UX01_LK_ORDR_SUB_TYPE")
                    .IsUnique();

                entity.Property(e => e.OrdrSubTypeId).HasColumnName("ORDR_SUB_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrdrSubTypeCd)
                    .IsRequired()
                    .HasColumnName("ORDR_SUB_TYPE_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrSubTypeDes)
                    .HasColumnName("ORDR_SUB_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkOrdrType>(entity =>
            {
                entity.HasKey(e => e.OrdrTypeId);

                entity.ToTable("LK_ORDR_TYPE", "dbo");

                entity.HasIndex(e => e.FsaOrdrTypeCd)
                    .HasName("IX03_LK_ORDR_TYPE");

                entity.HasIndex(e => e.OrdrTypeDes)
                    .HasName("UX01_LK_ORDR_TYPE")
                    .IsUnique();

                entity.Property(e => e.OrdrTypeId).HasColumnName("ORDR_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FsaOrdrTypeCd)
                    .HasColumnName("FSA_ORDR_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrTypeDes)
                    .IsRequired()
                    .HasColumnName("ORDR_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.XnciWfmOrdrWeightageNbr)
                    .HasColumnName("XNCI_WFM_ORDR_WEIGHTAGE_NBR")
                    .HasColumnType("decimal(3, 2)");

                entity.HasOne(d => d.FsaOrdrTypeCdNavigation)
                    .WithMany(p => p.LkOrdrType)
                    .HasForeignKey(d => d.FsaOrdrTypeCd)
                    .HasConstraintName("FK04_LK_ORDR_TYPE");
            });

            modelBuilder.Entity<LkPltfrm>(entity =>
            {
                entity.HasKey(e => e.PltfrmCd);

                entity.ToTable("LK_PLTFRM", "dbo");

                entity.HasIndex(e => e.PltfrmNme)
                    .HasName("UX01_LK_PLTFRM")
                    .IsUnique();

                entity.Property(e => e.PltfrmCd)
                    .HasColumnName("PLTFRM_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PltfrmNme)
                    .IsRequired()
                    .HasColumnName("PLTFRM_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkPprt>(entity =>
            {
                entity.HasKey(e => e.PprtId);

                entity.ToTable("LK_PPRT", "dbo");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX01_LK_PPRT");

                entity.HasIndex(e => e.OrdrTypeId)
                    .HasName("IX02_LK_PPRT");

                entity.HasIndex(e => e.ProdTypeId)
                    .HasName("IX03_LK_PPRT");

                entity.HasIndex(e => e.SmId)
                    .HasName("IX04_LK_PPRT");

                entity.Property(e => e.PprtId)
                    .HasColumnName("PPRT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IntlCd).HasColumnName("INTL_CD");

                entity.Property(e => e.MdsCd).HasColumnName("MDS_CD");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrdrCatId).HasColumnName("ORDR_CAT_ID");

                entity.Property(e => e.OrdrSubTypeCd)
                    .HasColumnName("ORDR_SUB_TYPE_CD")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrTypeId).HasColumnName("ORDR_TYPE_ID");

                entity.Property(e => e.PprtNme)
                    .HasColumnName("PPRT_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ProdTypeId).HasColumnName("PROD_TYPE_ID");

                entity.Property(e => e.ReltdFtnCd).HasColumnName("RELTD_FTN_CD");

                entity.Property(e => e.SmId).HasColumnName("SM_ID");

                entity.Property(e => e.TrptCd).HasColumnName("TRPT_CD");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkPprt)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_PPRT");

                entity.HasOne(d => d.OrdrType)
                    .WithMany(p => p.LkPprt)
                    .HasForeignKey(d => d.OrdrTypeId)
                    .HasConstraintName("FK03_LK_PPRT");

                entity.HasOne(d => d.ProdType)
                    .WithMany(p => p.LkPprt)
                    .HasForeignKey(d => d.ProdTypeId)
                    .HasConstraintName("FK02_LK_PPRT");
            });

            modelBuilder.Entity<LkPrfHrchy>(entity =>
            {
                entity.ToTable("LK_PRF_HRCHY", "dbo");

                entity.HasIndex(e => new { e.ChldPrfId, e.PrntPrfId })
                    .HasName("UX01_LK_PRF_HRCHY")
                    .IsUnique();

                entity.HasIndex(e => new { e.PrntPrfId, e.ChldPrfId })
                    .HasName("IX01_LK_PRF_HRCHY");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChldPrfId).HasColumnName("CHLD_PRF_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PrntPrfId).HasColumnName("PRNT_PRF_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.ChldPrf)
                    .WithMany(p => p.LkPrfHrchyChldPrf)
                    .HasForeignKey(d => d.ChldPrfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_PRF_HRCHY");

                entity.HasOne(d => d.PrntPrf)
                    .WithMany(p => p.LkPrfHrchyPrntPrf)
                    .HasForeignKey(d => d.PrntPrfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_PRF_HRCHY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkPrfHrchy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_PRF_HRCHY");
            });

            modelBuilder.Entity<LkProd>(entity =>
            {
                entity.HasKey(e => e.ProdNme);

                entity.ToTable("LK_PROD", "dbo");

                entity.Property(e => e.ProdNme)
                    .HasColumnName("PROD_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<LkProdType>(entity =>
            {
                entity.HasKey(e => e.ProdTypeId);

                entity.ToTable("LK_PROD_TYPE", "dbo");

                entity.HasIndex(e => e.FsaProdTypeCd)
                    .HasName("IX03_LK_PROD_TYPE");

                entity.HasIndex(e => e.ProdNme)
                    .HasName("IX04_LK_PROD_TYPE");

                entity.HasIndex(e => new { e.ProdTypeDes, e.OrdrCatId })
                    .HasName("UX01_LK_PROD_TYPE")
                    .IsUnique();

                entity.Property(e => e.ProdTypeId).HasColumnName("PROD_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DmstcCd).HasColumnName("DMSTC_CD");

                entity.Property(e => e.FsaProdTypeCd)
                    .HasColumnName("FSA_PROD_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrCatId).HasColumnName("ORDR_CAT_ID");

                entity.Property(e => e.ProdNme)
                    .IsRequired()
                    .HasColumnName("PROD_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProdTypeDes)
                    .IsRequired()
                    .HasColumnName("PROD_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TrptCd).HasColumnName("TRPT_CD");

                entity.HasOne(d => d.FsaProdTypeCdNavigation)
                    .WithMany(p => p.LkProdType)
                    .HasForeignKey(d => d.FsaProdTypeCd)
                    .HasConstraintName("FK05_LK_PROD_TYPE");

                entity.HasOne(d => d.OrdrCat)
                    .WithMany(p => p.LkProdType)
                    .HasForeignKey(d => d.OrdrCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_PROD_TYPE");

                entity.HasOne(d => d.ProdNmeNavigation)
                    .WithMany(p => p.LkProdType)
                    .HasForeignKey(d => d.ProdNme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_LK_PROD_TYPE");
            });

            modelBuilder.Entity<LkQlfctn>(entity =>
            {
                entity.HasKey(e => e.QlfctnId);

                entity.ToTable("LK_QLFCTN", "dbo");

                entity.HasIndex(e => e.AsnToUserId)
                    .HasName("UX01_LK_QLFCTN")
                    .IsUnique();

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_QLFCTN");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_QLFCTN");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_QLFCTN");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.AsnToUserId).HasColumnName("ASN_TO_USER_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.AsnToUser)
                    .WithOne(p => p.LkQlfctnAsnToUser)
                    .HasForeignKey<LkQlfctn>(d => d.AsnToUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_QLFCTN");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkQlfctnCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_QLFCTN");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkQlfctnModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_QLFCTN");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkQlfctn)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_QLFCTN");
            });

            modelBuilder.Entity<LkRecStus>(entity =>
            {
                entity.HasKey(e => e.RecStusId);

                entity.ToTable("LK_REC_STUS", "dbo");

                entity.HasIndex(e => e.RecStusDes)
                    .HasName("UX01_LK_REC_STUS")
                    .IsUnique();

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusDes)
                    .IsRequired()
                    .HasColumnName("REC_STUS_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkRedsgnCat>(entity =>
            {
                entity.HasKey(e => e.RedsgnCatId);

                entity.ToTable("LK_REDSGN_CAT", "dbo");

                entity.HasIndex(e => e.RedsgnCatDes)
                    .HasName("UX01_LK_REDSGN_CAT")
                    .IsUnique();

                entity.Property(e => e.RedsgnCatId).HasColumnName("REDSGN_CAT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("date");

                entity.Property(e => e.CreatUserId).HasColumnName("CREAT_USER_ID");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.RedsgnCatDes)
                    .IsRequired()
                    .HasColumnName("REDSGN_CAT_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatUser)
                    .WithMany(p => p.LkRedsgnCat)
                    .HasForeignKey(d => d.CreatUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_REDSGN_CAT");
            });

            modelBuilder.Entity<LkRedsgnNotesType>(entity =>
            {
                entity.HasKey(e => e.RedsgnNteTypeId);

                entity.ToTable("LK_REDSGN_NOTES_TYPE", "dbo");

                entity.Property(e => e.RedsgnNteTypeId).HasColumnName("REDSGN_NTE_TYPE_ID");

                entity.Property(e => e.CretdDt)
                    .HasColumnName("CRETD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.RedsgnNteTypeCd)
                    .IsRequired()
                    .HasColumnName("REDSGN_NTE_TYPE_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RedsgnNteTypeDes)
                    .IsRequired()
                    .HasColumnName("REDSGN_NTE_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkRedsgnType>(entity =>
            {
                entity.HasKey(e => e.RedsgnTypeId);

                entity.ToTable("LK_REDSGN_TYPE", "dbo");

                entity.Property(e => e.RedsgnTypeId).HasColumnName("REDSGN_TYPE_ID");

                entity.Property(e => e.CretdDt)
                    .HasColumnName("CRETD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.IsBlblCd).HasColumnName("IS_BLBL_CD");

                entity.Property(e => e.OdieReqCd)
                    .IsRequired()
                    .HasColumnName("ODIE_REQ_CD")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.RecstusId).HasColumnName("RECSTUS_ID");

                entity.Property(e => e.RedsgnCatId).HasColumnName("REDSGN_CAT_ID");

                entity.Property(e => e.RedsgnTypeDes)
                    .IsRequired()
                    .HasColumnName("REDSGN_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RedsgnTypeNme)
                    .IsRequired()
                    .HasColumnName("REDSGN_TYPE_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.RedsgnCat)
                    .WithMany(p => p.LkRedsgnType)
                    .HasForeignKey(d => d.RedsgnCatId)
                    .HasConstraintName("FK01_LK_REDSGN_TYPE");
            });

            modelBuilder.Entity<LkRole>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("LK_ROLE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_ROLE");

                entity.HasIndex(e => new { e.RoleNme, e.RoleTypeId })
                    .HasName("UX01_LK_ROLE")
                    .IsUnique();

                entity.Property(e => e.RoleId).HasColumnName("ROLE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RoleCd)
                    .HasColumnName("ROLE_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RoleNme)
                    .HasColumnName("ROLE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RoleTypeId)
                    .IsRequired()
                    .HasColumnName("ROLE_TYPE_ID")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkRole)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_ROLE");
            });

            modelBuilder.Entity<LkScrdObjType>(entity =>
            {
                entity.HasKey(e => e.ScrdObjTypeId);

                entity.ToTable("LK_SCRD_OBJ_TYPE", "dbo");

                entity.HasIndex(e => e.ScrdObjType)
                    .HasName("UX01_LK_SCRD_OBJ_TYPE")
                    .IsUnique();

                entity.Property(e => e.ScrdObjTypeId).HasColumnName("SCRD_OBJ_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ScrdObjType)
                    .IsRequired()
                    .HasColumnName("SCRD_OBJ_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ScrdObjTypeDes)
                    .IsRequired()
                    .HasColumnName("SCRD_OBJ_TYPE_DES")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkScrdObjTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_SCRD_OBJ_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkScrdObjTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_SCRD_OBJ_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkScrdObjType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SCRD_OBJ_TYPE");
            });

            modelBuilder.Entity<LkSdePrdctType>(entity =>
            {
                entity.HasKey(e => e.SdePrdctTypeId);

                entity.ToTable("LK_SDE_PRDCT_TYPE", "dbo");

                entity.Property(e => e.SdePrdctTypeId)
                    .HasColumnName("SDE_PRDCT_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrdrBySeqNbr).HasColumnName("ORDR_BY_SEQ_NBR");

                entity.Property(e => e.OutsrcdCd).HasColumnName("OUTSRCD_CD");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SdePrdctTypeDesc)
                    .IsRequired()
                    .HasColumnName("SDE_PRDCT_TYPE_DESC")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SdePrdctTypeNme)
                    .IsRequired()
                    .HasColumnName("SDE_PRDCT_TYPE_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSdePrdctTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SDE_PRDCT_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSdePrdctTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_SDE_PRDCT_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSdePrdctType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_SDE_PRDCT_TYPE");
            });

            modelBuilder.Entity<LkSiptActyType>(entity =>
            {
                entity.HasKey(e => e.SiptActyTypeId);

                entity.ToTable("LK_SIPT_ACTY_TYPE", "dbo");

                entity.Property(e => e.SiptActyTypeId)
                    .HasColumnName("SIPT_ACTY_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SiptActyTypeDes)
                    .IsRequired()
                    .HasColumnName("SIPT_ACTY_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSiptActyTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_SIPT_ACTY_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSiptActyTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_SIPT_ACTY_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSiptActyType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SIPT_ACTY_TYPE");
            });

            modelBuilder.Entity<LkSiptProdType>(entity =>
            {
                entity.HasKey(e => e.SiptProdTypeId);

                entity.ToTable("LK_SIPT_PROD_TYPE", "dbo");

                entity.Property(e => e.SiptProdTypeId)
                    .HasColumnName("SIPT_PROD_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SiptProdTypeDes)
                    .IsRequired()
                    .HasColumnName("SIPT_PROD_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSiptProdType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SIPT_PROD_TYPE");
            });

            modelBuilder.Entity<LkSiptTollType>(entity =>
            {
                entity.HasKey(e => e.SiptTollTypeId);

                entity.ToTable("LK_SIPT_TOLL_TYPE", "dbo");

                entity.Property(e => e.SiptTollTypeId).HasColumnName("SIPT_TOLL_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SiptTollTypeDes)
                    .IsRequired()
                    .HasColumnName("SIPT_TOLL_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSiptTollType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SIPT_TOLL_TYPE");
            });

            modelBuilder.Entity<LkSpclProj>(entity =>
            {
                entity.HasKey(e => e.SpclProjId);

                entity.ToTable("LK_SPCL_PROJ", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_SPCL_PROJ");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_SPCL_PROJ");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_SPCL_PROJ");

                entity.HasIndex(e => e.SpclProjNme)
                    .HasName("UX01_LK_SPCL_PROJ")
                    .IsUnique();

                entity.Property(e => e.SpclProjId)
                    .HasColumnName("SPCL_PROJ_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SpclProjNme)
                    .IsRequired()
                    .HasColumnName("SPCL_PROJ_NME")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSpclProjCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_SPCL_PROJ");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSpclProjModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_SPCL_PROJ");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSpclProj)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SPCL_PROJ");
            });

            modelBuilder.Entity<LkSplkActyType>(entity =>
            {
                entity.HasKey(e => e.SplkActyTypeId);

                entity.ToTable("LK_SPLK_ACTY_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_SPLK_ACTY_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_SPLK_ACTY_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_SPLK_ACTY_TYPE");

                entity.HasIndex(e => e.SplkActyTypeDes)
                    .HasName("UX01_LK_SPLK_ACTY_TYPE")
                    .IsUnique();

                entity.Property(e => e.SplkActyTypeId).HasColumnName("SPLK_ACTY_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SplkActyTypeDes)
                    .IsRequired()
                    .HasColumnName("SPLK_ACTY_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSplkActyTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_SPLK_ACTY_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSplkActyTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_SPLK_ACTY_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSplkActyType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SPLK_ACTY_TYPE");
            });

            modelBuilder.Entity<LkSplkEventType>(entity =>
            {
                entity.HasKey(e => e.SplkEventTypeId);

                entity.ToTable("LK_SPLK_EVENT_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_SPLK_EVENT_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_SPLK_EVENT_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_SPLK_EVENT_TYPE");

                entity.HasIndex(e => e.SplkEventTypeDes)
                    .HasName("UX01_LK_SPLK_EVENT_TYPE")
                    .IsUnique();

                entity.Property(e => e.SplkEventTypeId).HasColumnName("SPLK_EVENT_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SplkEventTypeDes)
                    .IsRequired()
                    .HasColumnName("SPLK_EVENT_TYPE_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSplkEventTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_SPLK_EVENT_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSplkEventTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_SPLK_EVENT_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSplkEventType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SPLK_EVENT_TYPE");
            });

            modelBuilder.Entity<LkSprintCpeNcr>(entity =>
            {
                entity.HasKey(e => e.SprintCpeNcrId);

                entity.ToTable("LK_SPRINT_CPE_NCR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_SPRINT_CPE_NCR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_SPRINT_CPE_NCR");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_SPRINT_CPE_NCR");

                entity.HasIndex(e => e.SprintCpeNcrDes)
                    .HasName("UX01_LK_SPRINT_CPE_NCR")
                    .IsUnique();

                entity.Property(e => e.SprintCpeNcrId).HasColumnName("SPRINT_CPE_NCR_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SprintCpeNcrDes)
                    .IsRequired()
                    .HasColumnName("SPRINT_CPE_NCR_DES")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSprintCpeNcrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_SPRINT_CPE_NCR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSprintCpeNcrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_SPRINT_CPE_NCR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSprintCpeNcr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SPRINT_CPE_NCR");
            });

            modelBuilder.Entity<LkSprintHldy>(entity =>
            {
                entity.HasKey(e => e.SprintHldyId);

                entity.ToTable("LK_SPRINT_HLDY", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_SPRINT_HLDY");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_SPRINT_HLDY");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_LK_SPRINT_HLDY");

                entity.HasIndex(e => e.SprintHldyDt)
                    .HasName("UX01_LK_SPRINT_HLDY")
                    .IsUnique();

                entity.Property(e => e.SprintHldyId)
                    .HasColumnName("SPRINT_HLDY_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SprintHldyDes)
                    .IsRequired()
                    .HasColumnName("SPRINT_HLDY_DES")
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.SprintHldyDt)
                    .HasColumnName("SPRINT_HLDY_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSprintHldyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_SPRINT_HLDY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSprintHldyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK01_LK_SPRINT_HLDY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSprintHldy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_SPRINT_HLDY");
            });

            modelBuilder.Entity<LkSrvcAssrnSiteSupp>(entity =>
            {
                entity.HasKey(e => e.SrvcAssrnSiteSuppId);

                entity.ToTable("LK_SRVC_ASSRN_SITE_SUPP", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_SRVC_ASSRN_SITE_SUPP");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_SRVC_ASSRN_SITE_SUPP");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_SRVC_ASSRN_SITE_SUPP");

                entity.HasIndex(e => e.SrvcAssrnSiteSuppDes)
                    .HasName("UX01_LK_SRVC_ASSRN_SITE_SUPP")
                    .IsUnique();

                entity.Property(e => e.SrvcAssrnSiteSuppId).HasColumnName("SRVC_ASSRN_SITE_SUPP_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SrvcAssrnSiteSuppDes)
                    .IsRequired()
                    .HasColumnName("SRVC_ASSRN_SITE_SUPP_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSrvcAssrnSiteSuppCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_SRVC_ASSRN_SITE_SUPP");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSrvcAssrnSiteSuppModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_SRVC_ASSRN_SITE_SUPP");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSrvcAssrnSiteSupp)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SRVC_ASSRN_SITE_SUPP");
            });

            modelBuilder.Entity<LkStdiReas>(entity =>
            {
                entity.HasKey(e => e.StdiReasId);

                entity.ToTable("LK_STDI_REAS", "dbo");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX01_LK_STDI_REAS");

                entity.HasIndex(e => e.StdiReasDes)
                    .HasName("UX01_LK_STDI_REAS")
                    .IsUnique();

                entity.Property(e => e.StdiReasId).HasColumnName("STDI_REAS_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.StdiReasDes)
                    .IsRequired()
                    .HasColumnName("STDI_REAS_DES")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkStdiReasCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK02_LK_STDI_REAS");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkStdiReasModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK01_LK_STDI_REAS");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkStdiReas)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_STDI_REAS");
            });

            modelBuilder.Entity<LkStus>(entity =>
            {
                entity.HasKey(e => e.StusId);

                entity.ToTable("LK_STUS", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_STUS");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_STUS");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_LK_STUS");

                entity.HasIndex(e => e.StusTypeId)
                    .HasName("IX04_LK_STUS");

                entity.HasIndex(e => new { e.StusDes, e.StusTypeId })
                    .HasName("UX01_LK_STUS")
                    .IsUnique();

                entity.Property(e => e.StusId)
                    .HasColumnName("STUS_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.StusDes)
                    .IsRequired()
                    .HasColumnName("STUS_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StusTypeId)
                    .IsRequired()
                    .HasColumnName("STUS_TYPE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkStusCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_STUS");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkStusModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_STUS");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkStus)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_STUS");

                entity.HasOne(d => d.StusType)
                    .WithMany(p => p.LkStus)
                    .HasForeignKey(d => d.StusTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_STUS");
            });

            modelBuilder.Entity<LkStusType>(entity =>
            {
                entity.HasKey(e => e.StusTypeId);

                entity.ToTable("LK_STUS_TYPE", "dbo");

                entity.HasIndex(e => e.StusTypeDes)
                    .HasName("UX01_LK_STUS_TYPE")
                    .IsUnique();

                entity.Property(e => e.StusTypeId)
                    .HasColumnName("STUS_TYPE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.StusTypeDes)
                    .IsRequired()
                    .HasColumnName("STUS_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkStusType)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_STUS_TYPE");
            });

            modelBuilder.Entity<LkSucssActy>(entity =>
            {
                entity.HasKey(e => e.SucssActyId);

                entity.ToTable("LK_SUCSS_ACTY", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_SUCSS_ACTY");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_SUCSS_ACTY");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_SUCSS_ACTY");

                entity.HasIndex(e => e.SucssActyDes)
                    .HasName("UX01_LK_SUCSS_ACTY")
                    .IsUnique();

                entity.Property(e => e.SucssActyId)
                    .HasColumnName("SUCSS_ACTY_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventTypeId)
                    .HasColumnName("EVENT_TYPE_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SucssActyDes)
                    .IsRequired()
                    .HasColumnName("SUCSS_ACTY_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkSucssActyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_SUCSS_ACTY");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkSucssActy)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_SUCSS_ACTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkSucssActyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_SUCSS_ACTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSucssActy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SUCSS_ACTY");
            });

            modelBuilder.Entity<LkSysCfg>(entity =>
            {
                entity.HasKey(e => e.CfgId);

                entity.ToTable("LK_SYS_CFG", "dbo");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_SYS_CFG");

                entity.Property(e => e.CfgId).HasColumnName("CFG_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PrmtrNme)
                    .IsRequired()
                    .HasColumnName("PRMTR_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrmtrValuTxt)
                    .HasColumnName("PRMTR_VALU_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkSysCfg)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_SYS_CFG");
            });

            modelBuilder.Entity<LkTask>(entity =>
            {
                entity.HasKey(e => e.TaskId);

                entity.ToTable("LK_TASK", "dbo");

                entity.HasIndex(e => e.TaskNme)
                    .HasName("UX01_LK_TASK")
                    .IsUnique();

                entity.Property(e => e.TaskId)
                    .HasColumnName("TASK_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrdrStusId).HasColumnName("ORDR_STUS_ID");

                entity.Property(e => e.TaskDes)
                    .HasColumnName("TASK_DES")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.TaskNme)
                    .IsRequired()
                    .HasColumnName("TASK_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WgStusId).HasColumnName("WG_STUS_ID");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkTask)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_TASK");

                entity.HasOne(d => d.OrdrStus)
                    .WithMany(p => p.LkTaskOrdrStus)
                    .HasForeignKey(d => d.OrdrStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_TASK");

                entity.HasOne(d => d.WgStus)
                    .WithMany(p => p.LkTaskWgStus)
                    .HasForeignKey(d => d.WgStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_TASK");
            });

            modelBuilder.Entity<LkTelco>(entity =>
            {
                entity.HasKey(e => e.TelcoId);

                entity.ToTable("LK_TELCO", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_TELCO");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_TELCO");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_TELCO");

                entity.HasIndex(e => e.TelcoNme)
                    .HasName("UX01_LK_TELCO")
                    .IsUnique();

                entity.Property(e => e.TelcoId).HasColumnName("TELCO_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TelcoCntctPhnNbr)
                    .IsRequired()
                    .HasColumnName("TELCO_CNTCT_PHN_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TelcoNme)
                    .IsRequired()
                    .HasColumnName("TELCO_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkTelcoCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_TELCO");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkTelcoModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_TELCO");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkTelco)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_TELCO");
            });

            modelBuilder.Entity<LkUcaaSActyType>(entity =>
            {
                entity.HasKey(e => e.UcaaSActyTypeId);

                entity.ToTable("LK_UCaaS_ACTY_TYPE", "dbo");

                entity.HasIndex(e => e.UcaaSActyTypeDes)
                    .HasName("UX01_LK_UCaaS_ACTY_TYPE")
                    .IsUnique();

                entity.Property(e => e.UcaaSActyTypeId)
                    .HasColumnName("UCaaS_ACTY_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UcaaSActyTypeDes)
                    .IsRequired()
                    .HasColumnName("UCaaS_ACTY_TYPE_DES")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkUcaaSActyTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_UCaaS_ACTY_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkUcaaSActyTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_UCaaS_ACTY_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkUcaaSActyType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_UCaaS_ACTY_TYPE");
            });

            modelBuilder.Entity<LkUcaaSBillActy>(entity =>
            {
                entity.HasKey(e => e.UcaaSBillActyId);

                entity.ToTable("LK_UCaaS_BILL_ACTY", "dbo");

                entity.HasIndex(e => new { e.UcaaSBillActyDes, e.UcaaSBicType, e.MrcNrcCd, e.UcaaSPlanTypeId })
                    .HasName("UX01_LK_UCaaS_BILL_ACTY")
                    .IsUnique();

                entity.Property(e => e.UcaaSBillActyId)
                    .HasColumnName("UCaaS_BILL_ACTY_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MrcNrcCd)
                    .IsRequired()
                    .HasColumnName("MRC_NRC_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UcaaSBicType)
                    .IsRequired()
                    .HasColumnName("UCaaS_BIC_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UcaaSBillActyDes)
                    .IsRequired()
                    .HasColumnName("UCaaS_BILL_ACTY_DES")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UcaaSPlanTypeId).HasColumnName("UCaaS_PLAN_TYPE_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkUcaaSBillActyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_UCaaS_BILL_ACTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkUcaaSBillActyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_UCaaS_BILL_ACTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkUcaaSBillActy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_UCaaS_BILL_ACTY");

                entity.HasOne(d => d.UcaaSPlanType)
                    .WithMany(p => p.LkUcaaSBillActy)
                    .HasForeignKey(d => d.UcaaSPlanTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_UCaaS_BILL_ACTY");
            });

            modelBuilder.Entity<LkUcaaSPlanType>(entity =>
            {
                entity.HasKey(e => e.UcaaSPlanTypeId);

                entity.ToTable("LK_UCaaS_PLAN_TYPE", "dbo");

                entity.HasIndex(e => e.UcaaSPlanType)
                    .HasName("UX01_LK_UCaaS_PLAN_TYPE")
                    .IsUnique();

                entity.Property(e => e.UcaaSPlanTypeId)
                    .HasColumnName("UCaaS_PLAN_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UcaaSPlanType)
                    .IsRequired()
                    .HasColumnName("UCaaS_PLAN_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UcaaSPlanTypeDes)
                    .IsRequired()
                    .HasColumnName("UCaaS_PLAN_TYPE_DES")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkUcaaSPlanTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_UCaaS_PLAN_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkUcaaSPlanTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_UCaaS_PLAN_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkUcaaSPlanType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_UCaaS_PLAN_TYPE");
            });

            modelBuilder.Entity<LkUcaaSProdType>(entity =>
            {
                entity.HasKey(e => e.UcaaSProdTypeId);

                entity.ToTable("LK_UCaaS_PROD_TYPE", "dbo");

                entity.HasIndex(e => e.UcaaSProdType)
                    .HasName("UX01_LK_UCaaS_PROD_TYPE")
                    .IsUnique();

                entity.Property(e => e.UcaaSProdTypeId)
                    .HasColumnName("UCaaS_PROD_TYPE_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UcaaSProdType)
                    .IsRequired()
                    .HasColumnName("UCaaS_PROD_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UcaaSProdTypeDes)
                    .IsRequired()
                    .HasColumnName("UCaaS_PROD_TYPE_DES")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkUcaaSProdTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_UCaaS_PROD_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkUcaaSProdTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_UCaaS_PROD_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkUcaaSProdType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_UCaaS_PROD_TYPE");
            });

            modelBuilder.Entity<LkUsStt>(entity =>
            {
                entity.HasKey(e => e.UsStateName);

                entity.ToTable("LK_US_STT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_US_STT");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_US_STT");

                entity.HasIndex(e => e.TmeZoneId)
                    .HasName("IX03_LK_US_STT");

                entity.HasIndex(e => e.UsStateId)
                    .HasName("UX01_LK_US_STT")
                    .IsUnique();

                entity.Property(e => e.UsStateName)
                    .HasColumnName("US_STATE_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TmeZoneId)
                    .HasColumnName("TME_ZONE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UsStateId)
                    .IsRequired()
                    .HasColumnName("US_STATE_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkUsSttCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_US_STT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkUsSttModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_US_STT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkUsStt)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_US_STT");
            });

            modelBuilder.Entity<LkUser>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("LK_USER", "dbo");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_USER");

                entity.HasIndex(e => e.UserAdid)
                    .HasName("UX01_LK_USER")
                    .IsUnique();

                entity.Property(e => e.UserId)
                    .HasColumnName("USER_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CellPhnNbr)
                    .HasColumnName("CELL_PHN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CptQty)
                    .HasColumnName("CPT_QTY")
                    .HasColumnType("decimal(19, 1)");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DsplNme)
                    .HasColumnName("DSPL_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAdr)
                    .IsRequired()
                    .HasColumnName("EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EventQty).HasColumnName("EVENT_QTY");

                entity.Property(e => e.FullNme)
                    .IsRequired()
                    .HasColumnName("FULL_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LstLoggdinDt)
                    .HasColumnName("LST_LOGGDIN_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.MenuPref)
                    .HasColumnName("MENU_PREF")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MgrAdid)
                    .HasColumnName("MGR_ADID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OldUserAdid)
                    .HasColumnName("OLD_USER_ADID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PgrNbr)
                    .HasColumnName("PGR_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PgrPinNbr)
                    .HasColumnName("PGR_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhnNbr)
                    .HasColumnName("PHN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SttCd)
                    .HasColumnName("STT_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserAcf2Id)
                    .HasColumnName("USER_ACF2_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserAdid)
                    .IsRequired()
                    .HasColumnName("USER_ADID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserOrdrCnt)
                    .HasColumnName("USER_ORDR_CNT")
                    .HasColumnType("decimal(7, 2)");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkUser)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_USER");
            });

            modelBuilder.Entity<LkUsrPrf>(entity =>
            {
                entity.HasKey(e => e.UsrPrfId);

                entity.ToTable("LK_USR_PRF", "dbo");

                entity.HasIndex(e => e.UsrPrfNme)
                    .HasName("IX01_LK_USR_PRF");

                entity.Property(e => e.UsrPrfId)
                    .HasColumnName("USR_PRF_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SrchCd).HasColumnName("SRCH_CD");

                entity.Property(e => e.UsrPrfDes)
                    .IsRequired()
                    .HasColumnName("USR_PRF_DES")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UsrPrfNme)
                    .IsRequired()
                    .HasColumnName("USR_PRF_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkUsrPrf)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_USR_PRF");
            });

            modelBuilder.Entity<LkUsrPrfMenu>(entity =>
            {
                entity.HasKey(e => e.UsrPrfMenuId);

                entity.ToTable("LK_USR_PRF_MENU", "dbo");

                entity.HasIndex(e => e.UsrPrfId)
                    .HasName("IX01_LK_USR_PRF_MENU");

                entity.HasIndex(e => new { e.UsrPrfId, e.MenuId })
                    .HasName("UX01_LK_USR_PRF_MENU")
                    .IsUnique();

                entity.Property(e => e.UsrPrfMenuId).HasColumnName("USR_PRF_MENU_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MenuId).HasColumnName("MENU_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UsrPrfId).HasColumnName("USR_PRF_ID");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.LkUsrPrfMenu)
                    .HasForeignKey(d => d.MenuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_USR_PRF_MENU");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkUsrPrfMenu)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_USR_PRF_MENU");

                entity.HasOne(d => d.UsrPrf)
                    .WithMany(p => p.LkUsrPrfMenu)
                    .HasForeignKey(d => d.UsrPrfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_USR_PRF_MENU");
            });

            modelBuilder.Entity<LkVasType>(entity =>
            {
                entity.HasKey(e => e.VasTypeId);

                entity.ToTable("LK_VAS_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_VAS_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_VAS_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_VAS_TYPE");

                entity.HasIndex(e => new { e.VasTypeDes, e.EventTypeId })
                    .HasName("UX01_LK_VAS_TYPE")
                    .IsUnique();

                entity.Property(e => e.VasTypeId).HasColumnName("VAS_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.VasTypeDes)
                    .IsRequired()
                    .HasColumnName("VAS_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkVasTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_VAS_TYPE");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkVasType)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_VAS_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkVasTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_VAS_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkVasType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_VAS_TYPE");
            });

            modelBuilder.Entity<LkVndr>(entity =>
            {
                entity.HasKey(e => e.VndrCd);

                entity.ToTable("LK_VNDR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_VNDR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_LK_VNDR");

                entity.HasIndex(e => new { e.VndrCd, e.VndrNme })
                    .HasName("UX01_LK_VNDR")
                    .IsUnique();

                entity.Property(e => e.VndrCd)
                    .HasColumnName("VNDR_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.VndrNme)
                    .IsRequired()
                    .HasColumnName("VNDR_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkVndrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_VNDR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkVndrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_VNDR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkVndr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_VNDR");
            });

            modelBuilder.Entity<LkVndrEmailLang>(entity =>
            {
                entity.HasKey(e => e.VndrEmailLangId);

                entity.ToTable("LK_VNDR_EMAIL_LANG", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_VNDR_EMAIL_LANG");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_VNDR_EMAIL_LANG");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_VNDR_EMAIL_LANG");

                entity.HasIndex(e => new { e.VndrCd, e.CtryCd, e.CtyNme })
                    .HasName("UX01_LK_VNDR_EMAIL_LANG")
                    .IsUnique();

                entity.Property(e => e.VndrEmailLangId)
                    .HasColumnName("VNDR_EMAIL_LANG_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LangNme)
                    .IsRequired()
                    .HasColumnName("LANG_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SubLangNme)
                    .HasColumnName("SUB_LANG_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.VndrCd)
                    .IsRequired()
                    .HasColumnName("VNDR_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkVndrEmailLangCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_VNDR_EMAIL_LANG");

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.LkVndrEmailLang)
                    .HasForeignKey(d => d.CtryCd)
                    .HasConstraintName("FK05_LK_VNDR_EMAIL_LANG");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkVndrEmailLangModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_VNDR_EMAIL_LANG");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkVndrEmailLang)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_VNDR_EMAIL_LANG");

                entity.HasOne(d => d.VndrCdNavigation)
                    .WithMany(p => p.LkVndrEmailLang)
                    .HasForeignKey(d => d.VndrCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_VNDR_EMAIL_LANG");

                entity.HasOne(d => d.Ct)
                    .WithMany(p => p.LkVndrEmailLang)
                    .HasForeignKey(d => new { d.CtyNme, d.CtryCd })
                    .HasConstraintName("FK06_LK_VNDR_EMAIL_LANG");
            });

            modelBuilder.Entity<LkVndrEmailType>(entity =>
            {
                entity.HasKey(e => e.VndrEmailTypeId);

                entity.ToTable("LK_VNDR_EMAIL_TYPE", "dbo");

                entity.HasIndex(e => e.VndrEmailTypeDes)
                    .HasName("UX01_LK_VNDR_EMAIL_TYPE")
                    .IsUnique();

                entity.Property(e => e.VndrEmailTypeId).HasColumnName("VNDR_EMAIL_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.VndrEmailTypeDes)
                    .IsRequired()
                    .HasColumnName("VNDR_EMAIL_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkVndrOrdrType>(entity =>
            {
                entity.HasKey(e => e.VndrOrdrTypeId);

                entity.ToTable("LK_VNDR_ORDR_TYPE", "dbo");

                entity.HasIndex(e => e.VndrOrdrTypeDes)
                    .HasName("UX01_LK_VNDR_ORDR_TYPE")
                    .IsUnique();

                entity.Property(e => e.VndrOrdrTypeId).HasColumnName("VNDR_ORDR_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.VndrOrdrTypeDes)
                    .IsRequired()
                    .HasColumnName("VNDR_ORDR_TYPE_DES")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LkVndrScm>(entity =>
            {
                entity.ToTable("LK_VNDR_SCM", "dbo");

                entity.HasIndex(e => new { e.VndrNme, e.VndrCd })
                    .HasName("UX01_LK_VNDR_SCM")
                    .IsUnique();

                entity.Property(e => e.LkVndrScmId).HasColumnName("LK_VNDR_SCM_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.VndrCd)
                    .IsRequired()
                    .HasColumnName("VNDR_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.VndrFullNme)
                    .IsRequired()
                    .HasColumnName("VNDR_FULL_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndrNme)
                    .IsRequired()
                    .HasColumnName("VNDR_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkVndrScmCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_VNDR_SCM");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkVndrScmModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_VNDR_SCM");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkVndrScm)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_VNDR_SCM");
            });

            modelBuilder.Entity<LkVpnPltfrmType>(entity =>
            {
                entity.HasKey(e => e.VpnPltfrmTypeId);

                entity.ToTable("LK_VPN_PLTFRM_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_VPN_PLTFRM_TYPE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_VPN_PLTFRM_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_VPN_PLTFRM_TYPE");

                entity.HasIndex(e => new { e.VpnPltfrmTypeDes, e.EventTypeId })
                    .HasName("UX01_LK_VPN_PLTFRM_TYPE")
                    .IsUnique();

                entity.Property(e => e.VpnPltfrmTypeId).HasColumnName("VPN_PLTFRM_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.VpnPltfrmTypeDes)
                    .IsRequired()
                    .HasColumnName("VPN_PLTFRM_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkVpnPltfrmTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_VPN_PLTFRM_TYPE");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.LkVpnPltfrmType)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_VPN_PLTFRM_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkVpnPltfrmTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_LK_VPN_PLTFRM_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkVpnPltfrmType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_VPN_PLTFRM_TYPE");
            });

            modelBuilder.Entity<LkWhlslPtnr>(entity =>
            {
                entity.HasKey(e => e.WhlslPtnrId);

                entity.ToTable("LK_WHLSL_PTNR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_WHLSL_PTNR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_WHLSL_PTNR");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_WHLSL_PTNR");

                entity.HasIndex(e => e.WhlslPtnrNme)
                    .HasName("UX01_LK_WHLSL_PTNR")
                    .IsUnique();

                entity.Property(e => e.WhlslPtnrId).HasColumnName("WHLSL_PTNR_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.WhlslPtnrNme)
                    .IsRequired()
                    .HasColumnName("WHLSL_PTNR_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkWhlslPtnrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK02_LK_WHLSL_PTNR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkWhlslPtnrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_WHLSL_PTNR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkWhlslPtnr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_WHLSL_PTNR");
            });

            modelBuilder.Entity<LkWrkflwStus>(entity =>
            {
                entity.HasKey(e => e.WrkflwStusId);

                entity.ToTable("LK_WRKFLW_STUS", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_WRKFLW_STUS");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_WRKFLW_STUS");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_WRKFLW_STUS");

                entity.HasIndex(e => e.WrkflwStusDes)
                    .HasName("UX01_LK_WRKFLW_STUS")
                    .IsUnique();

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.WrkflwStusDes)
                    .IsRequired()
                    .HasColumnName("WRKFLW_STUS_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkWrkflwStusCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_WRKFLW_STUS");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkWrkflwStusModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_LK_WRKFLW_STUS");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkWrkflwStus)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_WRKFLW_STUS");
            });

            modelBuilder.Entity<LkWrldHldy>(entity =>
            {
                entity.HasKey(e => e.WrldHldyId);

                entity.ToTable("LK_WRLD_HLDY", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_LK_WRLD_HLDY");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_LK_WRLD_HLDY");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_WRLD_HLDY");

                entity.HasIndex(e => new { e.CtryCd, e.WrldHldyDt, e.CtyNme })
                    .HasName("UX01_LK_WRLD_HLDY")
                    .IsUnique();

                entity.Property(e => e.WrldHldyId)
                    .HasColumnName("WRLD_HLDY_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryCd)
                    .IsRequired()
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.WrldHldyDes)
                    .IsRequired()
                    .HasColumnName("WRLD_HLDY_DES")
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.WrldHldyDt)
                    .HasColumnName("WRLD_HLDY_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkWrldHldyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_WRLD_HLDY");

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.LkWrldHldy)
                    .HasForeignKey(d => d.CtryCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_WRLD_HLDY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.LkWrldHldyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK04_LK_WRLD_HLDY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkWrldHldy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_WRLD_HLDY");

                entity.HasOne(d => d.Ct)
                    .WithMany(p => p.LkWrldHldy)
                    .HasForeignKey(d => new { d.CtyNme, d.CtryCd })
                    .HasConstraintName("FK05_LK_WRLD_HLDY");
            });

            modelBuilder.Entity<LkXnciMs>(entity =>
            {
                entity.HasKey(e => e.MsId);

                entity.ToTable("LK_XNCI_MS", "dbo");

                entity.HasIndex(e => e.MsDes)
                    .HasName("UX01_LK_XNCI_MS")
                    .IsUnique();

                entity.HasIndex(e => e.MsTaskId)
                    .HasName("IX01_LK_XNCI_MS");

                entity.Property(e => e.MsId)
                    .HasColumnName("MS_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MsDes)
                    .HasColumnName("MS_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MsTaskId).HasColumnName("MS_TASK_ID");

                entity.HasOne(d => d.MsTask)
                    .WithMany(p => p.LkXnciMs)
                    .HasForeignKey(d => d.MsTaskId)
                    .HasConstraintName("FK01_LK_XNCI_MS");
            });

            modelBuilder.Entity<LkXnciMsSla>(entity =>
            {
                entity.HasKey(e => e.MsSlaId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("LK_XNCI_MS_SLA", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_LK_XNCI_MS_SLA");

                entity.HasIndex(e => e.ToMsId)
                    .HasName("IX02_LK_XNCI_MS_SLA");

                entity.HasIndex(e => new { e.MsId, e.PltfrmCd, e.OrdrTypeId })
                    .HasName("UX01_LK_XNCI_MS_SLA")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.MsSlaId).HasColumnName("MS_SLA_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MsId).HasColumnName("MS_ID");

                entity.Property(e => e.OrdrTypeId).HasColumnName("ORDR_TYPE_ID");

                entity.Property(e => e.PltfrmCd)
                    .HasColumnName("PLTFRM_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SlaInDayQty).HasColumnName("SLA_IN_DAY_QTY");

                entity.Property(e => e.ToMsId).HasColumnName("TO_MS_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.LkXnciMsSla)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_LK_XNCI_MS_SLA");

                entity.HasOne(d => d.Ms)
                    .WithMany(p => p.LkXnciMsSlaMs)
                    .HasForeignKey(d => d.MsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_XNCI_MS_SLA");

                entity.HasOne(d => d.OrdrType)
                    .WithMany(p => p.LkXnciMsSla)
                    .HasForeignKey(d => d.OrdrTypeId)
                    .HasConstraintName("FK05_LK_XNCI_MS_SLA");

                entity.HasOne(d => d.PltfrmCdNavigation)
                    .WithMany(p => p.LkXnciMsSla)
                    .HasForeignKey(d => d.PltfrmCd)
                    .HasConstraintName("FK02_LK_XNCI_MS_SLA");

                entity.HasOne(d => d.ToMs)
                    .WithMany(p => p.LkXnciMsSlaToMs)
                    .HasForeignKey(d => d.ToMsId)
                    .HasConstraintName("FK01_LK_XNCI_MS_SLA");
            });

            modelBuilder.Entity<LkXnciRgn>(entity =>
            {
                entity.HasKey(e => e.RgnId);

                entity.ToTable("LK_XNCI_RGN", "dbo");

                entity.HasIndex(e => e.GrpId)
                    .HasName("IX02_LK_XNCI_RGN");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_LK_XNCI_RGN");

                entity.HasIndex(e => e.RgnDes)
                    .HasName("UX01_LK_XNCI_RGN")
                    .IsUnique();

                entity.Property(e => e.RgnId)
                    .HasColumnName("RGN_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreateByUserId).HasColumnName("CREATE_BY_USER_ID");

                entity.Property(e => e.GrpId).HasColumnName("GRP_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RgnDes)
                    .IsRequired()
                    .HasColumnName("RGN_DES")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.UsrPrfId).HasColumnName("USR_PRF_ID");

                entity.HasOne(d => d.CreateByUser)
                    .WithMany(p => p.LkXnciRgn)
                    .HasForeignKey(d => d.CreateByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_LK_XNCI_RGN");

                entity.HasOne(d => d.Grp)
                    .WithMany(p => p.LkXnciRgn)
                    .HasForeignKey(d => d.GrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_LK_XNCI_RGN");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.LkXnciRgn)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_LK_XNCI_RGN");

                entity.HasOne(d => d.UsrPrf)
                    .WithMany(p => p.LkXnciRgn)
                    .HasForeignKey(d => d.UsrPrfId)
                    .HasConstraintName("FK04_LK_XNCI_RGN");
            });

            modelBuilder.Entity<M5BillDsptchMsg>(entity =>
            {
                entity.ToTable("M5_BILL_DSPTCH_MSG", "dbo");

                entity.HasIndex(e => e.BillDsptchId)
                    .HasName("IX01_M5_BILL_DSPTCH_MSG");

                entity.Property(e => e.M5BillDsptchMsgId).HasColumnName("M5_BILL_DSPTCH_MSG_ID");

                entity.Property(e => e.BillDsptchId).HasColumnName("BILL_DSPTCH_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ErrMsg)
                    .HasColumnName("ERR_MSG")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.SentDt)
                    .HasColumnName("SENT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.StusId).HasColumnName("STUS_ID");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.M5BillDsptchMsg)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_M5_BILL_DSPTCH_MSG");
            });

            modelBuilder.Entity<M5EventMsg>(entity =>
            {
                entity.ToTable("M5_EVENT_MSG", "dbo");

                entity.HasIndex(e => new { e.DevId, e.CmpntId, e.M5OrdrNbr })
                    .HasName("IX01_M5_EVENT_MSG");

                entity.Property(e => e.M5EventMsgId).HasColumnName("M5_EVENT_MSG_ID");

                entity.Property(e => e.CmpntId).HasColumnName("CMPNT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.DevId)
                    .HasColumnName("DEV_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.M5MsgId).HasColumnName("M5_MSG_ID");

                entity.Property(e => e.M5OrdrId).HasColumnName("M5_ORDR_ID");

                entity.Property(e => e.M5OrdrNbr)
                    .IsRequired()
                    .HasColumnName("M5_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SentDt)
                    .HasColumnName("SENT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.StusId).HasColumnName("STUS_ID");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.M5EventMsg)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_M5_EVENT_MSG");
            });

            modelBuilder.Entity<MapGrpTask>(entity =>
            {
                entity.ToTable("MAP_GRP_TASK", "dbo");

                entity.HasIndex(e => new { e.TaskId, e.GrpId })
                    .HasName("UX02_MAP_GRP_TASK")
                    .IsUnique();

                entity.Property(e => e.MapGrpTaskId).HasColumnName("MAP_GRP_TASK_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GrpId).HasColumnName("GRP_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TaskId).HasColumnName("TASK_ID");

                entity.HasOne(d => d.Grp)
                    .WithMany(p => p.MapGrpTask)
                    .HasForeignKey(d => d.GrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_MAP_GRP_TASK");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.MapGrpTask)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MAP_GRP_TASK");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.MapGrpTask)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MAP_GRP_TASK");
            });

            modelBuilder.Entity<MapPrfTask>(entity =>
            {
                entity.ToTable("MAP_PRF_TASK", "dbo");

                entity.HasIndex(e => new { e.TaskId, e.PrntPrfId })
                    .HasName("UX02_MAP_PRF_TASK")
                    .IsUnique();

                entity.Property(e => e.MapPrfTaskId).HasColumnName("MAP_PRF_TASK_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PrntPrfId).HasColumnName("PRNT_PRF_ID");

                entity.Property(e => e.TaskId).HasColumnName("TASK_ID");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.MapPrfTask)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK01_MAP_PRF_TASK");

                entity.HasOne(d => d.PrntPrf)
                    .WithMany(p => p.MapPrfTask)
                    .HasForeignKey(d => d.PrntPrfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_MAP_PRF_TASK");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.MapPrfTask)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MAP_PRF_TASK");
            });

            modelBuilder.Entity<MapUsrPrf>(entity =>
            {
                entity.ToTable("MAP_USR_PRF", "dbo");

                entity.HasIndex(e => new { e.UserId, e.UsrPrfId })
                    .HasName("UX01_MAP_USR_PRF")
                    .IsUnique();

                entity.HasIndex(e => new { e.UsrPrfId, e.UserId })
                    .HasName("IX01_MAP_USR_PRF");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PoolCd).HasColumnName("POOL_CD");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UserId).HasColumnName("USER_ID");

                entity.Property(e => e.UsrPrfId).HasColumnName("USR_PRF_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.MapUsrPrfCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_MAP_USR_PRF");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.MapUsrPrfModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_MAP_USR_PRF");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.MapUsrPrf)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_MAP_USR_PRF");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.MapUsrPrfUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MAP_USR_PRF");

                entity.HasOne(d => d.UsrPrf)
                    .WithMany(p => p.MapUsrPrf)
                    .HasForeignKey(d => d.UsrPrfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MAP_USR_PRF");
            });

            modelBuilder.Entity<MdsEvent>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("MDS_EVENT", "dbo");

                entity.HasIndex(e => e.CreatDt)
                    .HasName("IX02_MDS_EVENT");

                entity.HasIndex(e => e.EventStusId)
                    .HasName("IX01_MDS_EVENT");

                entity.HasIndex(e => e.MdsActyTypeId)
                    .HasName("IX03_MDS_EVENT");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AltShipCtryRgnNme)
                    .HasColumnName("ALT_SHIP_CTRY_RGN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AltShipCtyNme)
                    .HasColumnName("ALT_SHIP_CTY_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AltShipFlrBldgNme)
                    .HasColumnName("ALT_SHIP_FLR_BLDG_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AltShipPocEmail)
                    .HasColumnName("ALT_SHIP_POC_EMAIL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AltShipPocNme)
                    .HasColumnName("ALT_SHIP_POC_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AltShipPocPhnNbr)
                    .HasColumnName("ALT_SHIP_POC_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AltShipStreetAdr)
                    .HasColumnName("ALT_SHIP_STREET_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AltShipSttPrvnNme)
                    .HasColumnName("ALT_SHIP_STT_PRVN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AltShipZipCd)
                    .HasColumnName("ALT_SHIP_ZIP_CD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BusJustnTxt)
                    .HasColumnName("BUS_JUSTN_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Ccd)
                    .HasColumnName("CCD")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CmpltdEmailCcTxt)
                    .HasColumnName("CMPLTD_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcBrdgId).HasColumnName("CNFRC_BRDG_ID");

                entity.Property(e => e.CnfrcBrdgNbr)
                    .HasColumnName("CNFRC_BRDG_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcPinNbr)
                    .HasColumnName("CNFRC_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CustLtrOptOut)
                    .HasColumnName("CUST_LTR_OPT_OUT");

                entity.Property(e => e.CntctEmailList)
                    .HasColumnName("CNTCT_EMAIL_LIST")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CpeDspchCmntTxt)
                    .HasColumnName("CPE_DSPCH_CMNT_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CpeDspchEmailAdr)
                    .HasColumnName("CPE_DSPCH_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CpeOrdrCd)
                    .IsRequired()
                    .HasColumnName("CPE_ORDR_CD")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CrdlepntCd).HasColumnName("CRDLEPNT_CD");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryRgnNme)
                    .HasColumnName("CTRY_RGN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustAcctTeamPdlNme)
                    .HasColumnName("CUST_ACCT_TEAM_PDL_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CustSowLocTxt)
                    .HasColumnName("CUST_SOW_LOC_TXT")
                    .HasMaxLength(2000);

                entity.Property(e => e.CustTrptReqrCd).HasColumnName("CUST_TRPT_REQR_CD");

                entity.Property(e => e.DiscMgmtCd)
                    .HasColumnName("DISC_MGMT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EsclBusJustnTxt)
                    .HasColumnName("ESCL_BUS_JUSTN_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.EsclCd).HasColumnName("ESCL_CD");

                entity.Property(e => e.EsclPolicyLocTxt)
                    .HasColumnName("ESCL_POLICY_LOC_TXT")
                    .HasMaxLength(1000);

                entity.Property(e => e.EsclReasId).HasColumnName("ESCL_REAS_ID");

                entity.Property(e => e.EventDrtnInMinQty).HasColumnName("EVENT_DRTN_IN_MIN_QTY");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt)
                    .HasColumnName("EVENT_TITLE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraDrtnTmeAmt).HasColumnName("EXTRA_DRTN_TME_AMT");

                entity.Property(e => e.FailReasId).HasColumnName("FAIL_REAS_ID");

                entity.Property(e => e.FlrBldgNme)
                    .HasColumnName("FLR_BLDG_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FrcdftCd).HasColumnName("FRCDFT_CD");

                entity.Property(e => e.FullCustDiscCd).HasColumnName("FULL_CUST_DISC_CD");

                entity.Property(e => e.FullCustDiscReasTxt)
                    .HasColumnName("FULL_CUST_DISC_REAS_TXT")
                    .HasMaxLength(2500)
                    .IsUnicode(false);

                entity.Property(e => e.H1)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.H6).HasMaxLength(9);

                entity.Property(e => e.InstlSitePocCellPhnNbr)
                    .HasColumnName("INSTL_SITE_POC_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocIntlCellPhnCd)
                    .HasColumnName("INSTL_SITE_POC_INTL_CELL_PHN_CD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocIntlPhnCd)
                    .HasColumnName("INSTL_SITE_POC_INTL_PHN_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocNme)
                    .HasColumnName("INSTL_SITE_POC_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocPhnNbr)
                    .HasColumnName("INSTL_SITE_POC_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IpmDes)
                    .HasColumnName("IPM_DES")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.MdrNumber)
                    .HasColumnName("MDR_NUMBER")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.MdsActyTypeId).HasColumnName("MDS_ACTY_TYPE_ID");

                entity.Property(e => e.MdsCmpltCd).HasColumnName("MDS_CMPLT_CD");

                entity.Property(e => e.MdsFastTrkTypeId)
                    .HasColumnName("MDS_FAST_TRK_TYPE_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MnsOrdrCd)
                    .IsRequired()
                    .HasColumnName("MNS_ORDR_CD")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MnsPmId)
                    .HasColumnName("MNS_PM_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.NidHostNme)
                    .HasColumnName("NID_HOST_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NidSerialNbr)
                    .HasColumnName("NID_SERIAL_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NtwkCmpltCd).HasColumnName("NTWK_CMPLT_CD");

                entity.Property(e => e.NtwkCustNme)
                    .HasColumnName("NTWK_CUST_NME")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NtwkEventTypeId).HasColumnName("NTWK_EVENT_TYPE_ID");

                entity.Property(e => e.NtwkH1)
                    .HasColumnName("NTWK_H1")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.NtwkH6)
                    .HasColumnName("NTWK_H6")
                    .IsUnicode(false);

                entity.Property(e => e.OdieCustId)
                    .HasColumnName("ODIE_CUST_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OldEventStusId).HasColumnName("OLD_EVENT_STUS_ID");

                entity.Property(e => e.OnlineMeetingAdr)
                    .HasColumnName("ONLINE_MEETING_ADR")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.OptOutBusJustnTxt)
                    .HasColumnName("OPT_OUT_BUS_JUSTN_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.OptOutReasId).HasColumnName("OPT_OUT_REAS_ID");

                entity.Property(e => e.PreCfgCmpltCd).HasColumnName("PRE_CFG_CMPLT_CD");

                entity.Property(e => e.PrimReqDt)
                    .HasColumnName("PRIM_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PubEmailCcTxt)
                    .HasColumnName("PUB_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReqorUserCellPhnNbr)
                    .HasColumnName("REQOR_USER_CELL_PHN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqorUserId).HasColumnName("REQOR_USER_ID");

                entity.Property(e => e.RltdCmpsNcrCd).HasColumnName("RLTD_CMPS_NCR_CD");

                entity.Property(e => e.RltdCmpsNcrNme)
                    .HasColumnName("RLTD_CMPS_NCR_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ScndyReqDt)
                    .HasColumnName("SCNDY_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ShipCustEmailAdr)
                    .HasColumnName("SHIP_CUST_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShipCxrNme)
                    .HasColumnName("SHIP_CXR_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShipDevSerialNbr)
                    .HasColumnName("SHIP_DEV_SERIAL_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShipTrkRefrNbr)
                    .HasColumnName("SHIP_TRK_REFR_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShippedDt)
                    .HasColumnName("SHIPPED_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ShrtDes)
                    .HasColumnName("SHRT_DES")
                    .HasMaxLength(2500)
                    .IsUnicode(false);

                entity.Property(e => e.SiteIdTxt)
                    .HasColumnName("SITE_ID_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SoftAssignCd).HasColumnName("SOFT_ASSIGN_CD");

                entity.Property(e => e.SprintCpeNcrId).HasColumnName("SPRINT_CPE_NCR_ID");

                entity.Property(e => e.SrvcAssrnPocCellPhnNbr)
                    .HasColumnName("SRVC_ASSRN_POC_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnPocIntlCellPhnCd)
                    .HasColumnName("SRVC_ASSRN_POC_INTL_CELL_PHN_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnPocIntlPhnCd)
                    .HasColumnName("SRVC_ASSRN_POC_INTL_PHN_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnPocNme)
                    .HasColumnName("SRVC_ASSRN_POC_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnPocPhnNbr)
                    .HasColumnName("SRVC_ASSRN_POC_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAvlbltyHrs)
                    .HasColumnName("SRVC_AVLBLTY_HRS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcTmeZnCd)
                    .HasColumnName("SRVC_TME_ZN_CD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAdr)
                    .HasColumnName("STREET_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SttPrvnNme)
                    .HasColumnName("STT_PRVN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TmeSlotId).HasColumnName("TME_SLOT_ID");

                entity.Property(e => e.UsIntlCd)
                    .HasColumnName("US_INTL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VasCmpltCd).HasColumnName("VAS_CMPLT_CD");

                entity.Property(e => e.VpnPltfrmTypeId).HasColumnName("VPN_PLTFRM_TYPE_ID");

                entity.Property(e => e.WoobIpAdr)
                    .HasColumnName("WOOB_IP_ADR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.Property(e => e.WrlesTrptReqrCd).HasColumnName("WRLES_TRPT_REQR_CD");

                entity.Property(e => e.ZipCd)
                    .HasColumnName("ZIP_CD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.CnfrcBrdg)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.CnfrcBrdgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK13_MDS_EVENT");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.MdsEventCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK09_MDS_EVENT");

                entity.HasOne(d => d.EsclReas)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.EsclReasId)
                    .HasConstraintName("FK06_MDS_EVENT");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.MdsEvent)
                    .HasForeignKey<MdsEvent>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.MdsEventEventStus)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MDS_EVENT");

                entity.HasOne(d => d.FailReas)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.FailReasId)
                    .HasConstraintName("FK12_MDS_EVENT");

                entity.HasOne(d => d.MdsActyType)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.MdsActyTypeId)
                    .HasConstraintName("FK04_MDS_EVENT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.MdsEventModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK10_MDS_EVENT");

                entity.HasOne(d => d.NtwkEventType)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.NtwkEventTypeId)
                    .HasConstraintName("FK14_MDS_EVENT");

                entity.HasOne(d => d.OldEventStus)
                    .WithMany(p => p.MdsEventOldEventStus)
                    .HasForeignKey(d => d.OldEventStusId)
                    .HasConstraintName("FK08_MDS_EVENT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK11_MDS_EVENT");

                entity.HasOne(d => d.ReqorUser)
                    .WithMany(p => p.MdsEventReqorUser)
                    .HasForeignKey(d => d.ReqorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK07_MDS_EVENT");

                entity.HasOne(d => d.SprintCpeNcr)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.SprintCpeNcrId)
                    .HasConstraintName("FK05_MDS_EVENT");

                entity.HasOne(d => d.VpnPltfrmType)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.VpnPltfrmTypeId)
                    .HasConstraintName("FK15_MDS_EVENT");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.MdsEvent)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_MDS_EVENT");
            });

            modelBuilder.Entity<MdsEventDslSbicCustTrpt>(entity =>
            {
                entity.HasKey(e => e.DslSbicCustTrptId);

                entity.ToTable("MDS_EVENT_DSL_SBIC_CUST_TRPT", "dbo");

                entity.Property(e => e.DslSbicCustTrptId).HasColumnName("DSL_SBIC_CUST_TRPT_ID");

                entity.Property(e => e.BwEsnMeid)
                    .HasColumnName("BW_ESN_MEID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.IpAdr)
                    .HasColumnName("IP_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsWirelessCd)
                    .IsRequired()
                    .HasColumnName("IS_WIRELESS_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MdsTrnsprtType)
                    .IsRequired()
                    .HasColumnName("MDS_TRNSPRT_TYPE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NxthopGtwyAdr)
                    .HasColumnName("NXTHOP_GTWY_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PrimBkupCd)
                    .IsRequired()
                    .HasColumnName("PRIM_BKUP_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ScaNbr)
                    .HasColumnName("SCA_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SprintMngdCd)
                    .IsRequired()
                    .HasColumnName("SPRINT_MNGD_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SubnetMaskAdr)
                    .HasColumnName("SUBNET_MASK_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndrPrvdrNme)
                    .HasColumnName("VNDR_PRVDR_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VndrPrvdrTrptCktId)
                    .HasColumnName("VNDR_PRVDR_TRPT_CKT_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventDslSbicCustTrpt)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_DSL_SBIC_CUST_TRPT");
            });

            modelBuilder.Entity<MdsEventFormChng>(entity =>
            {
                entity.ToTable("MDS_EVENT_FORM_CHNG", "dbo");

                entity.HasIndex(e => e.MdsEventId)
                    .HasName("IX01_MDS_EVENT_FORM_CHNG");

                entity.Property(e => e.MdsEventFormChngId).HasColumnName("MDS_EVENT_FORM_CHNG_ID");

                entity.Property(e => e.ChngTypeNme)
                    .IsRequired()
                    .HasColumnName("CHNG_TYPE_NME")
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MdsEventId).HasColumnName("MDS_EVENT_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.MdsEventFormChng)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK02_MDS_EVENT_FORM_HIST");

                entity.HasOne(d => d.MdsEvent)
                    .WithMany(p => p.MdsEventFormChng)
                    .HasForeignKey(d => d.MdsEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_FORM_HIST");
            });

            modelBuilder.Entity<MdsEventMacActy>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.MdsMacActyId });

                entity.ToTable("MDS_EVENT_MAC_ACTY", "dbo");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.MdsMacActyId).HasColumnName("MDS_MAC_ACTY_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventMacActy)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_MAC_ACTY");

                entity.HasOne(d => d.MdsMacActy)
                    .WithMany(p => p.MdsEventMacActy)
                    .HasForeignKey(d => d.MdsMacActyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MDS_EVENT_MAC_ACTY");
            });

            modelBuilder.Entity<MdsEventNew>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("MDS_EVENT_NEW", "dbo");

                entity.HasIndex(e => e.CsgLvlCd)
                    .HasName("IX13_MDS_EVENT_NEW");

                entity.HasIndex(e => e.EsclReasId)
                    .HasName("IX12_MDS_EVENT_NEW");

                entity.HasIndex(e => e.EventTypeId)
                    .HasName("IX07_MDS_EVENT_NEW");

                entity.HasIndex(e => e.FailReasId)
                    .HasName("IX10_MDS_EVENT_NEW");

                entity.HasIndex(e => e.MdsActyTypeId)
                    .HasName("IX02_MDS_EVENT_NEW");

                entity.HasIndex(e => e.MdsFastTrkTypeId)
                    .HasName("IX06_MDS_EVENT_NEW");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX05_MDS_EVENT_NEW");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_MDS_EVENT_NEW");

                entity.HasIndex(e => e.ReqorUserId)
                    .HasName("IX09_MDS_EVENT_NEW");

                entity.HasIndex(e => e.WrkflwStusId)
                    .HasName("IX11_MDS_EVENT_NEW");

                entity.HasIndex(e => new { e.EventStusId, e.OldEventStusId })
                    .HasName("IX01_MDS_EVENT_NEW");

                entity.HasIndex(e => new { e.TmeSlotId, e.EventTypeId })
                    .HasName("IX08_MDS_EVENT_NEW");

                entity.HasIndex(e => new { e.CreatByUserId, e.ModfdByUserId, e.ReqorUserId })
                    .HasName("IX04_MDS_EVENT_NEW");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AltCnfrcBrgNme)
                    .HasColumnName("ALT_CNFRC_BRG_NME")
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.BusJustnCmntTxt)
                    .HasColumnName("BUS_JUSTN_CMNT_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.BusJustnTxt)
                    .HasColumnName("BUS_JUSTN_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltdEmailCcTxt)
                    .HasColumnName("CMPLTD_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcBrdgNbr)
                    .HasColumnName("CNFRC_BRDG_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcPinNbr)
                    .HasColumnName("CNFRC_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CpeDspchCmntTxt)
                    .HasColumnName("CPE_DSPCH_CMNT_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CpeDspchEmailAdr)
                    .HasColumnName("CPE_DSPCH_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CsgLvlCd)
                    .HasColumnName("CSG_LVL_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CustAcctTeamPdlNme)
                    .HasColumnName("CUST_ACCT_TEAM_PDL_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustEmailAdr)
                    .HasColumnName("CUST_EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.DevCnt).HasColumnName("DEV_CNT");

                entity.Property(e => e.DevSerialNbr)
                    .HasColumnName("DEV_SERIAL_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DiscNtfyPdlNme)
                    .HasColumnName("DISC_NTFY_PDL_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DiscoDevCnt).HasColumnName("DISCO_DEV_CNT");

                entity.Property(e => e.DsgnDocLocTxt)
                    .IsRequired()
                    .HasColumnName("DSGN_DOC_LOC_TXT")
                    .HasMaxLength(1000);

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EsclBusJustTxt)
                    .HasColumnName("ESCL_BUS_JUST_TXT")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EsclCd).HasColumnName("ESCL_CD");

                entity.Property(e => e.EsclReasId).HasColumnName("ESCL_REAS_ID");

                entity.Property(e => e.EventDrtnInMinQty).HasColumnName("EVENT_DRTN_IN_MIN_QTY");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt)
                    .HasColumnName("EVENT_TITLE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.EventTypeId)
                    .HasColumnName("EVENT_TYPE_ID")
                    .HasDefaultValueSql("((5))");

                entity.Property(e => e.ExtraDrtnTmeAmt).HasColumnName("EXTRA_DRTN_TME_AMT");

                entity.Property(e => e.FailReasId).HasColumnName("FAIL_REAS_ID");

                entity.Property(e => e.FulfilledDt)
                    .HasColumnName("FULFILLED_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FullCustDiscCd).HasColumnName("FULL_CUST_DISC_CD");

                entity.Property(e => e.H1Id)
                    .IsRequired()
                    .HasColumnName("H1_ID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.MdsActyTypeId).HasColumnName("MDS_ACTY_TYPE_ID");

                entity.Property(e => e.MdsBrdgNeedCd).HasColumnName("MDS_BRDG_NEED_CD");

                entity.Property(e => e.MdsBrdgNeedId).HasColumnName("MDS_BRDG_NEED_ID");

                entity.Property(e => e.MdsFastTrkCd).HasColumnName("MDS_FAST_TRK_CD");

                entity.Property(e => e.MdsFastTrkTypeId)
                    .HasColumnName("MDS_FAST_TRK_TYPE_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MnsPmId)
                    .HasColumnName("MNS_PM_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OdieCustId)
                    .HasColumnName("ODIE_CUST_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OldEventStusId).HasColumnName("OLD_EVENT_STUS_ID");

                entity.Property(e => e.OnlineMeetingAdr)
                    .HasColumnName("ONLINE_MEETING_ADR")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.OptOutReasTxt)
                    .HasColumnName("OPT_OUT_REAS_TXT")
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.PreCfgCmpltCd).HasColumnName("PRE_CFG_CMPLT_CD");

                entity.Property(e => e.PrimReqDt)
                    .HasColumnName("PRIM_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PubEmailCcTxt)
                    .HasColumnName("PUB_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReqorCellPhnNbr)
                    .HasColumnName("REQOR_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ReqorUserId).HasColumnName("REQOR_USER_ID");

                entity.Property(e => e.ScndyReqDt)
                    .HasColumnName("SCNDY_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ShipCustEmailAdr)
                    .HasColumnName("SHIP_CUST_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShipCustNme)
                    .HasColumnName("SHIP_CUST_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShipCustPhnNbr)
                    .HasColumnName("SHIP_CUST_PHN_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ShipTrkRefrNbr)
                    .HasColumnName("SHIP_TRK_REFR_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShippedDt)
                    .HasColumnName("SHIPPED_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ShrtDes)
                    .HasColumnName("SHRT_DES")
                    .HasMaxLength(2500)
                    .IsUnicode(false);

                entity.Property(e => e.SowsEventId).HasColumnName("SOWS_EVENT_ID");

                entity.Property(e => e.SprintCpeNcrId).HasColumnName("SPRINT_CPE_NCR_ID");

                entity.Property(e => e.SprsEmailOnPubCd)
                    .HasColumnName("SPRS_EMAIL_ON_PUB_CD")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SrvcAssrnSiteSuppId).HasColumnName("SRVC_ASSRN_SITE_SUPP_ID");

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.TmeSlotId).HasColumnName("TME_SLOT_ID");

                entity.Property(e => e.WoobIpAdr)
                    .HasColumnName("WOOB_IP_ADR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WrkflwStusId)
                    .HasColumnName("WRKFLW_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.MdsEventNewCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK08_MDS_EVENT_NEW");

                entity.HasOne(d => d.EsclReas)
                    .WithMany(p => p.MdsEventNew)
                    .HasForeignKey(d => d.EsclReasId)
                    .HasConstraintName("FK12_MDS_EVENT_NEW");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.MdsEventNew)
                    .HasForeignKey<MdsEventNew>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_NEW");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.MdsEventNewEventStus)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_MDS_EVENT_NEW");

                entity.HasOne(d => d.FailReas)
                    .WithMany(p => p.MdsEventNew)
                    .HasForeignKey(d => d.FailReasId)
                    .HasConstraintName("FK07_MDS_EVENT_NEW");

                entity.HasOne(d => d.MdsBrdgNeed)
                    .WithMany(p => p.MdsEventNew)
                    .HasForeignKey(d => d.MdsBrdgNeedId)
                    .HasConstraintName("FK14_MDS_EVENT_NEW");

                entity.HasOne(d => d.MdsFastTrkType)
                    .WithMany(p => p.MdsEventNew)
                    .HasForeignKey(d => d.MdsFastTrkTypeId)
                    .HasConstraintName("FK02_MDS_EVENT_NEW");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.MdsEventNewModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK09_MDS_EVENT_NEW");

                entity.HasOne(d => d.OldEventStus)
                    .WithMany(p => p.MdsEventNewOldEventStus)
                    .HasForeignKey(d => d.OldEventStusId)
                    .HasConstraintName("FK04_MDS_EVENT_NEW");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.MdsEventNew)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK10_MDS_EVENT_NEW");

                entity.HasOne(d => d.ReqorUser)
                    .WithMany(p => p.MdsEventNewReqorUser)
                    .HasForeignKey(d => d.ReqorUserId)
                    .HasConstraintName("FK13_MDS_EVENT_NEW");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.MdsEventNew)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_MDS_EVENT_NEW");

                entity.HasOne(d => d.LkEventTypeTmeSlot)
                    .WithMany(p => p.MdsEventNew)
                    .HasForeignKey(d => new { d.TmeSlotId, d.EventTypeId })
                    .HasConstraintName("FK11_MDS_EVENT_NEW");
            });

            modelBuilder.Entity<MdsEventNtwkActy>(entity =>
            {
                entity.ToTable("MDS_EVENT_NTWK_ACTY", "dbo");

                entity.HasIndex(e => new { e.EventId, e.NtwkActyTypeId })
                    .HasName("UX01_MDS_EVENT_NTWK_ACTY")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.NtwkActyTypeId).HasColumnName("NTWK_ACTY_TYPE_ID");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventNtwkActy)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_NTWK_ACTY");

                entity.HasOne(d => d.NtwkActyType)
                    .WithMany(p => p.MdsEventNtwkActy)
                    .HasForeignKey(d => d.NtwkActyTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MDS_EVENT_NTWK_ACTY");
            });

            modelBuilder.Entity<MdsEventNtwkCust>(entity =>
            {
                entity.ToTable("MDS_EVENT_NTWK_CUST", "dbo");

                entity.HasIndex(e => new { e.RoleNme, e.EventId })
                    .HasName("IX01_MDS_EVENT_NTWK_CUST");

                entity.Property(e => e.MdsEventNtwkCustId).HasColumnName("MDS_EVENT_NTWK_CUST_ID");

                entity.Property(e => e.AssocH6)
                    .IsRequired()
                    .HasColumnName("ASSOC_H6")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.InstlSitePocEmail)
                    .HasColumnName("INSTL_SITE_POC_EMAIL")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocNme)
                    .HasColumnName("INSTL_SITE_POC_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocPhn)
                    .HasColumnName("INSTL_SITE_POC_PHN")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RoleNme)
                    .IsRequired()
                    .HasColumnName("ROLE_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventNtwkCust)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_NTWK_CUST");
            });

            modelBuilder.Entity<MdsEventNtwkTrpt>(entity =>
            {
                entity.ToTable("MDS_EVENT_NTWK_TRPT", "dbo");

                entity.HasIndex(e => new { e.Mach5OrdrNbr, e.AssocH6, e.EventId })
                    .HasName("IX01_MDS_EVENT_NTWK_TRPT");

                entity.Property(e => e.MdsEventNtwkTrptId).HasColumnName("MDS_EVENT_NTWK_TRPT_ID");

                entity.Property(e => e.AssocH6)
                    .HasColumnName("ASSOC_H6")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.BdwdNme)
                    .HasColumnName("BDWD_NME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CeSrvcId)
                    .HasColumnName("CE_SRVC_ID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DdAprvlNbr)
                    .HasColumnName("DD_APRVL_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.IpNuaAdr)
                    .HasColumnName("IP_NUA_ADR")
                    .IsUnicode(false);

                entity.Property(e => e.IpVer)
                    .HasColumnName("IP_VER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LecCntctInfo)
                    .HasColumnName("LEC_CNTCT_INFO")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.LecPrvdrNme)
                    .HasColumnName("LEC_PRVDR_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LocCity)
                    .HasColumnName("LOC_CITY")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LocCtry)
                    .HasColumnName("LOC_CTRY")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LocSttPrvn)
                    .HasColumnName("LOC_STT_PRVN")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Mach5OrdrNbr)
                    .HasColumnName("MACH5_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MdsTrnsprtType)
                    .IsRequired()
                    .HasColumnName("MDS_TRNSPRT_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.MplsAccsBdwd)
                    .HasColumnName("MPLS_ACCS_BDWD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MultiVrfReq)
                    .HasColumnName("MULTI_VRF_REQ")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ScaNbr)
                    .HasColumnName("SCA_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TaggdCd).HasColumnName("TAGGD_CD");

                entity.Property(e => e.VasType)
                    .HasColumnName("VAS_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VlanNbr)
                    .HasColumnName("VLAN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventNtwkTrpt)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_NTWK_TRPT");
            });

            modelBuilder.Entity<MdsEventOdieDev>(entity =>
            {
                entity.ToTable("MDS_EVENT_ODIE_DEV", "dbo");

                entity.HasIndex(e => e.OdieDevNme)
                    .HasName("IX03_MDS_EVENT_ODIE_DEV");

                entity.HasIndex(e => e.RdsnNbr)
                    .HasName("IX02_MDS_EVENT_ODIE_DEV");

                entity.Property(e => e.MdsEventOdieDevId).HasColumnName("MDS_EVENT_ODIE_DEV_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DevModelId).HasColumnName("DEV_MODEL_ID");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.FastTrkCd)
                    .HasColumnName("FAST_TRK_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FrwlProdCd)
                    .HasColumnName("FRWL_PROD_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IntlCtryCd)
                    .IsRequired()
                    .HasColumnName("INTL_CTRY_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ManfId).HasColumnName("MANF_ID");

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OptoutCd)
                    .HasColumnName("OPTOUT_CD")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PhnNbr)
                    .HasColumnName("PHN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RdsnExpDt)
                    .HasColumnName("RDSN_EXP_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RdsnNbr)
                    .IsRequired()
                    .HasColumnName("RDSN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ScCd)
                    .HasColumnName("SC_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnSiteSuppId).HasColumnName("SRVC_ASSRN_SITE_SUPP_ID");

                entity.Property(e => e.SysCd).HasColumnName("SYS_CD");

                entity.Property(e => e.WoobCd)
                    .HasColumnName("WOOB_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.DevModel)
                    .WithMany(p => p.MdsEventOdieDev)
                    .HasForeignKey(d => d.DevModelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MDS_EVENT_ODIE_DEV");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventOdieDev)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_ODIE_DEV");

                entity.HasOne(d => d.Manf)
                    .WithMany(p => p.MdsEventOdieDev)
                    .HasForeignKey(d => d.ManfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_MDS_EVENT_ODIE_DEV");

                entity.HasOne(d => d.SrvcAssrnSiteSupp)
                    .WithMany(p => p.MdsEventOdieDev)
                    .HasForeignKey(d => d.SrvcAssrnSiteSuppId)
                    .HasConstraintName("FK04_MDS_EVENT_ODIE_DEV");
            });

            modelBuilder.Entity<MdsEventPortBndwd>(entity =>
            {
                entity.HasKey(e => e.PortBndwdId);

                entity.ToTable("MDS_EVENT_PORT_BNDWD", "dbo");

                entity.Property(e => e.PortBndwdId).HasColumnName("PORT_BNDWD_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.M5OrdrCmpntId)
                    .HasColumnName("M5_ORDR_CMPNT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.M5OrdrNbr)
                    .HasColumnName("M5_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nua)
                    .HasColumnName("NUA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PortBndwd)
                    .HasColumnName("PORT_BNDWD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventPortBndwd)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_PORT_BNDWD");
            });

            modelBuilder.Entity<MdsEventSiteSrvc>(entity =>
            {
                entity.ToTable("MDS_EVENT_SITE_SRVC", "dbo");

                entity.Property(e => e.MdsEventSiteSrvcId).HasColumnName("MDS_EVENT_SITE_SRVC_ID");

                entity.Property(e => e.ActvDt)
                    .HasColumnName("ACTV_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailCd).HasColumnName("EMAIL_CD");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.M5OrdrNbr)
                    .HasColumnName("M5_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mach5SrvcOrdrId)
                    .IsRequired()
                    .HasColumnName("MACH5_SRVC_ORDR_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcTypeId).HasColumnName("SRVC_TYPE_ID");

                entity.Property(e => e.ThrdPartyId)
                    .HasColumnName("THRD_PARTY_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ThrdPartySrvcLvlId).HasColumnName("THRD_PARTY_SRVC_LVL_ID");

                entity.Property(e => e.ThrdPartyVndrId).HasColumnName("THRD_PARTY_VNDR_ID");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventSiteSrvc)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_SITE_SRVC");

                entity.HasOne(d => d.SrvcType)
                    .WithMany(p => p.MdsEventSiteSrvc)
                    .HasForeignKey(d => d.SrvcTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MDS_EVENT_SITE_SRVC");

                entity.HasOne(d => d.ThrdPartySrvcLvl)
                    .WithMany(p => p.MdsEventSiteSrvc)
                    .HasForeignKey(d => d.ThrdPartySrvcLvlId)
                    .HasConstraintName("FK04_MDS_EVENT_SITE_SRVC");

                entity.HasOne(d => d.ThrdPartyVndr)
                    .WithMany(p => p.MdsEventSiteSrvc)
                    .HasForeignKey(d => d.ThrdPartyVndrId)
                    .HasConstraintName("FK03_MDS_EVENT_SITE_SRVC");
            });

            modelBuilder.Entity<MdsEventSlnkWiredTrpt>(entity =>
            {
                entity.HasKey(e => e.SlnkWiredTrptId);

                entity.ToTable("MDS_EVENT_SLNK_WIRED_TRPT", "dbo");

                entity.Property(e => e.SlnkWiredTrptId).HasColumnName("SLNK_WIRED_TRPT_ID");

                entity.Property(e => e.BdwdChnlNme)
                    .HasColumnName("BDWD_CHNL_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DedAccsCd)
                    .HasColumnName("DED_ACCS_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DlciVpiVci)
                    .HasColumnName("DLCI_VPI_VCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.FmsNbr)
                    .HasColumnName("FMS_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.H6)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.IpNuaAdr)
                    .HasColumnName("IP_NUA_ADR")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MdsTrnsprtType)
                    .IsRequired()
                    .HasColumnName("MDS_TRNSPRT_TYPE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MultiLinkCktCd)
                    .HasColumnName("MULTI_LINK_CKT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OldCktId)
                    .HasColumnName("OLD_CKT_ID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OptInCktCd)
                    .HasColumnName("OPT_IN_CKT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OptInHCd)
                    .HasColumnName("OPT_IN_H_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PlNbr)
                    .HasColumnName("PL_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PrimBkupCd)
                    .IsRequired()
                    .HasColumnName("PRIM_BKUP_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReadyBeginCd)
                    .HasColumnName("READY_BEGIN_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SpaNua)
                    .HasColumnName("SPA_NUA")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SprintMngdCd)
                    .IsRequired()
                    .HasColumnName("SPRINT_MNGD_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VlanNbr)
                    .HasColumnName("VLAN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndrPrvdrNme)
                    .HasColumnName("VNDR_PRVDR_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VndrPrvdrTrptCktId)
                    .HasColumnName("VNDR_PRVDR_TRPT_CKT_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventSlnkWiredTrpt)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_EVENT_SLNK_WIRED_TRPT");
            });

            modelBuilder.Entity<MdsEventSrvc>(entity =>
            {
                entity.HasKey(e => e.MdsEventSrvcTypeId);

                entity.ToTable("MDS_EVENT_SRVC", "dbo");

                entity.HasIndex(e => e.FsaMdsEventId)
                    .HasName("FK01_MDS_EVENT_SRVC");

                entity.HasIndex(e => e.SrvcTypeId)
                    .HasName("FK02_MDS_EVENT_SRVC");

                entity.HasIndex(e => e.ThrdPartySrvcLvlId)
                    .HasName("FK04_MDS_EVENT_SRVC");

                entity.HasIndex(e => e.ThrdPartyVndrId)
                    .HasName("FK03_MDS_EVENT_SRVC");

                entity.Property(e => e.MdsEventSrvcTypeId).HasColumnName("MDS_EVENT_SRVC_TYPE_ID");

                entity.Property(e => e.ActvDt)
                    .HasColumnName("ACTV_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.EmailCd).HasColumnName("EMAIL_CD");

                entity.Property(e => e.FsaMdsEventId).HasColumnName("FSA_MDS_EVENT_ID");

                entity.Property(e => e.Mach5H6SrvcId).HasColumnName("MACH5_H6_SRVC_ID");

                entity.Property(e => e.SrvcTypeId).HasColumnName("SRVC_TYPE_ID");

                entity.Property(e => e.TabSeqNbr).HasColumnName("TAB_SEQ_NBR");

                entity.Property(e => e.ThrdPartyId)
                    .HasColumnName("THRD_PARTY_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ThrdPartySrvcLvlId).HasColumnName("THRD_PARTY_SRVC_LVL_ID");

                entity.Property(e => e.ThrdPartyVndrId).HasColumnName("THRD_PARTY_VNDR_ID");

                entity.HasOne(d => d.SrvcType)
                    .WithMany(p => p.MdsEventSrvc)
                    .HasForeignKey(d => d.SrvcTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MDS_EVENT_SRVC");

                entity.HasOne(d => d.ThrdPartySrvcLvl)
                    .WithMany(p => p.MdsEventSrvc)
                    .HasForeignKey(d => d.ThrdPartySrvcLvlId)
                    .HasConstraintName("FK04_MDS_EVENT_SRVC");

                entity.HasOne(d => d.ThrdPartyVndr)
                    .WithMany(p => p.MdsEventSrvc)
                    .HasForeignKey(d => d.ThrdPartyVndrId)
                    .HasConstraintName("FK03_MDS_EVENT_SRVC");
            });

            modelBuilder.Entity<MdsEventWrlsTrpt>(entity =>
            {
                entity.ToTable("MDS_EVENT_WRLS_TRPT", "dbo");

                entity.HasIndex(e => e.FsaMdsEventId)
                    .HasName("IX01_MDS_EVENT_WRLS_TRPT");

                entity.Property(e => e.MdsEventWrlsTrptId).HasColumnName("MDS_EVENT_WRLS_TRPT_ID");

                entity.Property(e => e.AcctPin)
                    .HasColumnName("ACCT_PIN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillAcctNbr)
                    .HasColumnName("BILL_ACCT_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BillAcctNme)
                    .HasColumnName("BILL_ACCT_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EsnMacId)
                    .IsRequired()
                    .HasColumnName("ESN_MAC_ID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.FsaMdsEventId).HasColumnName("FSA_MDS_EVENT_ID");

                entity.Property(e => e.Imei)
                    .HasColumnName("IMEI")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OdieDevNme)
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PricePln)
                    .HasColumnName("PRICE_PLN")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrimBkupCd)
                    .IsRequired()
                    .HasColumnName("PRIM_BKUP_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SalsCd)
                    .HasColumnName("SALS_CD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Soc)
                    .HasColumnName("SOC")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StaticIpAdr)
                    .HasColumnName("STATIC_IP_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TabSeqNbr).HasColumnName("TAB_SEQ_NBR");

                entity.Property(e => e.UiccIdNbr)
                    .HasColumnName("UICC_ID_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WrlsTypeCd)
                    .HasColumnName("WRLS_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MdsEventWrlsTrpt)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK02_MDS_EVENT_WRLS_TRPT");
            });

            modelBuilder.Entity<MdsFastTrkType>(entity =>
            {
                entity.ToTable("MDS_FAST_TRK_TYPE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_MDS_FAST_TRACK_TYPE");

                entity.HasIndex(e => e.MdsFastTrkTypeDes)
                    .HasName("UX01_MDS_FAST_TRACK_TYPE")
                    .IsUnique();

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_MDS_FAST_TRACK_TYPE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_MDS_FAST_TRACK_TYPE");

                entity.Property(e => e.MdsFastTrkTypeId)
                    .HasColumnName("MDS_FAST_TRK_TYPE_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MdsFastTrkTypeDes)
                    .IsRequired()
                    .HasColumnName("MDS_FAST_TRK_TYPE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.MdsFastTrkTypeCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MDS_FAST_TRK_TYPE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.MdsFastTrkTypeModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_MDS_FAST_TRK_TYPE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.MdsFastTrkType)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MDS_FAST_TRK_TYPE");
            });

            modelBuilder.Entity<MplsAccs>(entity =>
            {
                entity.ToTable("MPLS_ACCS", "dbo");

                entity.Property(e => e.MplsAccsId).HasColumnName("MPLS_ACCS_ID");

                entity.Property(e => e.CktId)
                    .HasColumnName("CKT_ID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.NwUserAdr)
                    .HasColumnName("NW_USER_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SipTrnkGrpId).HasColumnName("SIP_TRNK_GRP_ID");

                entity.HasOne(d => d.SipTrnkGrp)
                    .WithMany(p => p.MplsAccs)
                    .HasPrincipalKey(p => p.SipTrnkGrpId)
                    .HasForeignKey(d => d.SipTrnkGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MPLS_ACCS");
            });

            modelBuilder.Entity<MplsEvent>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("MPLS_EVENT", "dbo");

                entity.HasIndex(e => e.ActyLocaleId)
                    .HasName("IX07_MPLS_EVENT");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX15_MPLS_EVENT");

                entity.HasIndex(e => e.EsclReasId)
                    .HasName("IX12_MPLS_EVENT");

                entity.HasIndex(e => e.EventStusId)
                    .HasName("IX01_MPLS_EVENT");

                entity.HasIndex(e => e.Ftn)
                    .HasName("IX17_MPLS_EVENT_FTN");

                entity.HasIndex(e => e.IpVerId)
                    .HasName("IX06_MPLS_EVENT");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX16_MPLS_EVENT");

                entity.HasIndex(e => e.MplsCxrPtnrId)
                    .HasName("IX10_MPLS_EVENT");

                entity.HasIndex(e => e.MplsEventTypeId)
                    .HasName("IX04_MPLS_EVENT");

                entity.HasIndex(e => e.MplsMgrtnTypeId)
                    .HasName("IX11_MPLS_EVENT");

                entity.HasIndex(e => e.MultiVrfReqId)
                    .HasName("IX08_MPLS_EVENT");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX14_MPLS_EVENT");

                entity.HasIndex(e => e.ReqorUserId)
                    .HasName("IX02_MPLS_EVENT");

                entity.HasIndex(e => e.SalsUserId)
                    .HasName("IX03_MPLS_EVENT");

                entity.HasIndex(e => e.VpnPltfrmTypeId)
                    .HasName("IX05_MPLS_EVENT");

                entity.HasIndex(e => e.WhlslPtnrId)
                    .HasName("IX09_MPLS_EVENT");

                entity.HasIndex(e => e.WrkflwStusId)
                    .HasName("IX13_MPLS_EVENT");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActyLocaleId)
                    .HasColumnName("ACTY_LOCALE_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.AddE2eMontrgCd).HasColumnName("ADD_E2E_MONTRG_CD");

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltdEmailCcTxt)
                    .HasColumnName("CMPLTD_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcBrdgNbr)
                    .HasColumnName("CNFRC_BRDG_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcPinNbr)
                    .HasColumnName("CNFRC_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustCntctCellPhnNbr)
                    .HasColumnName("CUST_CNTCT_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctNme)
                    .HasColumnName("CUST_CNTCT_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPgrNbr)
                    .HasColumnName("CUST_CNTCT_PGR_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPgrPinNbr)
                    .HasColumnName("CUST_CNTCT_PGR_PIN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPhnNbr)
                    .HasColumnName("CUST_CNTCT_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustEmailAdr)
                    .HasColumnName("CUST_EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CxrPtnrCd).HasColumnName("CXR_PTNR_CD");

                entity.Property(e => e.DdAprvlNbr)
                    .IsRequired()
                    .HasColumnName("DD_APRVL_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DesCmntTxt)
                    .IsRequired()
                    .HasColumnName("DES_CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.DocLinkTxt)
                    .HasColumnName("DOC_LINK_TXT")
                    .HasMaxLength(1000);

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EsclCd).HasColumnName("ESCL_CD");

                entity.Property(e => e.EsclReasId).HasColumnName("ESCL_REAS_ID");

                entity.Property(e => e.EventDes)
                    .HasColumnName("EVENT_DES")
                    .IsUnicode(false);

                entity.Property(e => e.EventDrtnInMinQty).HasColumnName("EVENT_DRTN_IN_MIN_QTY");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt)
                    .HasColumnName("EVENT_TITLE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraDrtnTmeAmt).HasColumnName("EXTRA_DRTN_TME_AMT");

                entity.Property(e => e.Ftn)
                    .HasColumnName("FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.H1)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.H6).HasMaxLength(9);

                entity.Property(e => e.IpVerId).HasColumnName("IP_VER_ID");

                entity.Property(e => e.MdsDlci)
                    .HasColumnName("MDS_DLCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MdsIpAdr)
                    .HasColumnName("MDS_IP_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MdsMngdCd).HasColumnName("MDS_MNGD_CD");

                entity.Property(e => e.MdsStcRteDes)
                    .HasColumnName("MDS_STC_RTE_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MdsVrfNme)
                    .HasColumnName("MDS_VRF_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MgrtnCd).HasColumnName("MGRTN_CD");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MplsCxrPtnrId)
                    .HasColumnName("MPLS_CXR_PTNR_ID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MplsEventTypeId).HasColumnName("MPLS_EVENT_TYPE_ID");

                entity.Property(e => e.MplsMgrtnTypeId).HasColumnName("MPLS_MGRTN_TYPE_ID");

                entity.Property(e => e.MultiVrfReqId)
                    .HasColumnName("MULTI_VRF_REQ_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.NddUpdtdCd).HasColumnName("NDD_UPDTD_CD");

                entity.Property(e => e.NniDsgnDocNme)
                    .HasColumnName("NNI_DSGN_DOC_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OffNetMontrgCd).HasColumnName("OFF_NET_MONTRG_CD");

                entity.Property(e => e.OnNetMontrgCd).HasColumnName("ON_NET_MONTRG_CD");

                entity.Property(e => e.PrimReqDt)
                    .HasColumnName("PRIM_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PubEmailCcTxt)
                    .HasColumnName("PUB_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReltdCmpsNcrCd).HasColumnName("RELTD_CMPS_NCR_CD");

                entity.Property(e => e.ReltdCmpsNcrNme)
                    .HasColumnName("RELTD_CMPS_NCR_NME")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ReqorUserId).HasColumnName("REQOR_USER_ID");

                entity.Property(e => e.SalsUserId).HasColumnName("SALS_USER_ID");

                entity.Property(e => e.ScndyReqDt)
                    .HasColumnName("SCNDY_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SowsEventId).HasColumnName("SOWS_EVENT_ID");

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.VpnPltfrmTypeId).HasColumnName("VPN_PLTFRM_TYPE_ID");

                entity.Property(e => e.VrfNme)
                    .HasColumnName("VRF_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WhlslPtnrCd).HasColumnName("WHLSL_PTNR_CD");

                entity.Property(e => e.WhlslPtnrId).HasColumnName("WHLSL_PTNR_ID");

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.HasOne(d => d.ActyLocale)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.ActyLocaleId)
                    .HasConstraintName("FK12_MPLS_EVENT");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.MplsEventCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_MPLS_EVENT");

                entity.HasOne(d => d.EsclReas)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.EsclReasId)
                    .HasConstraintName("FK10_MPLS_EVENT");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.MplsEvent)
                    .HasForeignKey<MplsEvent>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK08_MPLS_EVENT");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK07_MPLS_EVENT");

                entity.HasOne(d => d.IpVer)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.IpVerId)
                    .HasConstraintName("FK09_MPLS_EVENT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.MplsEventModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_MPLS_EVENT");

                entity.HasOne(d => d.MplsCxrPtnr)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.MplsCxrPtnrId)
                    .HasConstraintName("FK11_MPLS_EVENT");

                entity.HasOne(d => d.MplsEventType)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.MplsEventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK14_MPLS_EVENT");

                entity.HasOne(d => d.MplsMgrtnType)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.MplsMgrtnTypeId)
                    .HasConstraintName("FK15_MPLS_EVENT");

                entity.HasOne(d => d.MultiVrfReq)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.MultiVrfReqId)
                    .HasConstraintName("FK18_MPLS_EVENT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MPLS_EVENT");

                entity.HasOne(d => d.ReqorUser)
                    .WithMany(p => p.MplsEventReqorUser)
                    .HasForeignKey(d => d.ReqorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_MPLS_EVENT");

                entity.HasOne(d => d.SalsUser)
                    .WithMany(p => p.MplsEventSalsUser)
                    .HasForeignKey(d => d.SalsUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MPLS_EVENT");

                entity.HasOne(d => d.VpnPltfrmType)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.VpnPltfrmTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK17_MPLS_EVENT");

                entity.HasOne(d => d.WhlslPtnr)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.WhlslPtnrId)
                    .HasConstraintName("FK13_MPLS_EVENT");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.MplsEvent)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .HasConstraintName("FK06_MPLS_EVENT");
            });

            modelBuilder.Entity<MplsEventAccsTag>(entity =>
            {
                entity.ToTable("MPLS_EVENT_ACCS_TAG", "dbo");

                entity.HasIndex(e => e.CtryCd)
                    .HasName("IX01_MPLS_EVENT_ACCS_TAG");

                entity.HasIndex(e => e.MnsOffrgRoutrId)
                    .HasName("IX03_MPLS_EVENT_ACCS_TAG");

                entity.HasIndex(e => e.MnsPrfmcRptId)
                    .HasName("IX05_MPLS_EVENT_ACCS_TAG");

                entity.HasIndex(e => e.MnsRoutgTypeId)
                    .HasName("IX04_MPLS_EVENT_ACCS_TAG");

                entity.HasIndex(e => e.MplsAccsBdwdId)
                    .HasName("IX02_MPLS_EVENT_ACCS_TAG");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX06_MPLS_EVENT_ACCS_TAG");

                entity.Property(e => e.MplsEventAccsTagId).HasColumnName("MPLS_EVENT_ACCS_TAG_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.LocCtyNme)
                    .HasColumnName("LOC_CTY_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocSttNme)
                    .HasColumnName("LOC_STT_NME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MnsOffrgRoutrId).HasColumnName("MNS_OFFRG_ROUTR_ID");

                entity.Property(e => e.MnsPrfmcRptId).HasColumnName("MNS_PRFMC_RPT_ID");

                entity.Property(e => e.MnsRoutgTypeId)
                    .HasColumnName("MNS_ROUTG_TYPE_ID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MnsSipAdr)
                    .HasColumnName("MNS_SIP_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MplsAccsBdwdId).HasColumnName("MPLS_ACCS_BDWD_ID");

                entity.Property(e => e.PlDalCktNbr)
                    .HasColumnName("PL_DAL_CKT_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TrnsprtOeFtnNbr)
                    .HasColumnName("TRNSPRT_OE_FTN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrsNet449Adr)
                    .HasColumnName("TRS_NET_449_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VasSolNbr)
                    .HasColumnName("VAS_SOL_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.MplsEventAccsTag)
                    .HasForeignKey(d => d.CtryCd)
                    .HasConstraintName("FK05_MPLS_EVENT_ACCS_TAG");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MplsEventAccsTag)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_MPLS_EVENT_ACCS_TAG");

                entity.HasOne(d => d.MnsOffrgRoutr)
                    .WithMany(p => p.MplsEventAccsTag)
                    .HasForeignKey(d => d.MnsOffrgRoutrId)
                    .HasConstraintName("FK06_MPLS_EVENT_ACCS_TAG");

                entity.HasOne(d => d.MnsPrfmcRpt)
                    .WithMany(p => p.MplsEventAccsTag)
                    .HasForeignKey(d => d.MnsPrfmcRptId)
                    .HasConstraintName("FK07_MPLS_EVENT_ACCS_TAG");

                entity.HasOne(d => d.MnsRoutgType)
                    .WithMany(p => p.MplsEventAccsTag)
                    .HasForeignKey(d => d.MnsRoutgTypeId)
                    .HasConstraintName("FK08_MPLS_EVENT_ACCS_TAG");

                entity.HasOne(d => d.MplsAccsBdwd)
                    .WithMany(p => p.MplsEventAccsTag)
                    .HasForeignKey(d => d.MplsAccsBdwdId)
                    .HasConstraintName("FK09_MPLS_EVENT_ACCS_TAG");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.MplsEventAccsTag)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MPLS_EVENT_ACCS_TAG");
            });

            modelBuilder.Entity<MplsEventActyType>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.MplsActyTypeId });

                entity.ToTable("MPLS_EVENT_ACTY_TYPE", "dbo");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.MplsActyTypeId).HasColumnName("MPLS_ACTY_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MplsEventActyType)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MPLS_EVENT_ACTY_TYPE");

                entity.HasOne(d => d.MplsActyType)
                    .WithMany(p => p.MplsEventActyType)
                    .HasForeignKey(d => d.MplsActyTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MPLS_EVENT_ACTY_TYPE");
            });

            modelBuilder.Entity<MplsEventVasType>(entity =>
            {
                entity.ToTable("MPLS_EVENT_VAS_TYPE", "dbo");

                entity.HasIndex(e => new { e.EventId, e.VasTypeId })
                    .HasName("UX01_MPLS_EVENT_VAS_TYPE")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.VasTypeId).HasColumnName("VAS_TYPE_ID");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.MplsEventVasType)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_MPLS_EVENT_VAS_TYPE");

                entity.HasOne(d => d.VasType)
                    .WithMany(p => p.MplsEventVasType)
                    .HasForeignKey(d => d.VasTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_MPLS_EVENT_VAS_TYPE");
            });

            modelBuilder.Entity<NccoOrdr>(entity =>
            {
                entity.HasKey(e => e.OrdrId);

                entity.ToTable("NCCO_ORDR", "dbo");

                entity.Property(e => e.OrdrId)
                    .HasColumnName("ORDR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BillCycNbr)
                    .HasColumnName("BILL_CYC_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CcsDt)
                    .HasColumnName("CCS_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CwdDt)
                    .HasColumnName("CWD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EmailAdr)
                    .HasColumnName("EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.H5AcctNbr).HasColumnName("H5_ACCT_NBR");

                entity.Property(e => e.OeNme)
                    .HasColumnName("OE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrByLassieCd)
                    .HasColumnName("ORDR_BY_LASSIE_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrTypeId).HasColumnName("ORDR_TYPE_ID");

                entity.Property(e => e.PlNbr)
                    .HasColumnName("PL_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProdTypeId).HasColumnName("PROD_TYPE_ID");

                entity.Property(e => e.SiteCityNme)
                    .HasColumnName("SITE_CITY_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SolNme)
                    .HasColumnName("SOL_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SotsNbr)
                    .HasColumnName("SOTS_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VndrCd)
                    .HasColumnName("VNDR_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.NccoOrdr)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_NCCO_ORDR");

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.NccoOrdr)
                    .HasForeignKey(d => d.CtryCd)
                    .HasConstraintName("FK02_NCCO_ORDR");

                entity.HasOne(d => d.Ordr)
                    .WithOne(p => p.NccoOrdr)
                    .HasForeignKey<NccoOrdr>(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_NCCO_ORDR");

                entity.HasOne(d => d.OrdrType)
                    .WithMany(p => p.NccoOrdr)
                    .HasForeignKey(d => d.OrdrTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_NCCO_ORDR");

                entity.HasOne(d => d.ProdType)
                    .WithMany(p => p.NccoOrdr)
                    .HasForeignKey(d => d.ProdTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_NCCO_ORDR");

                entity.HasOne(d => d.VndrCdNavigation)
                    .WithMany(p => p.NccoOrdr)
                    .HasForeignKey(d => d.VndrCd)
                    .HasConstraintName("FK06_NCCO_ORDR");
            });

            modelBuilder.Entity<NgvnEvent>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("NGVN_EVENT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX08_NGVN_EVENT");

                entity.HasIndex(e => e.EsclReasId)
                    .HasName("IX05_NGVN_EVENT");

                entity.HasIndex(e => e.EventStusId)
                    .HasName("IX01_NGVN_EVENT");

                entity.HasIndex(e => e.Ftn)
                    .HasName("IX12_NGVN_EVENT_FTN");

                entity.HasIndex(e => e.IpVerId)
                    .HasName("IX11_NGVN_EVENT");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX09_NGVN_EVENT");

                entity.HasIndex(e => e.NgvnProdTypeId)
                    .HasName("IX02_NGVN_EVENT");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX07_NGVN_EVENT");

                entity.HasIndex(e => e.ReqorUserId)
                    .HasName("IX03_NGVN_EVENT");

                entity.HasIndex(e => e.SalsUserId)
                    .HasName("IX04_NGVN_EVENT");

                entity.HasIndex(e => e.WrkflwStusId)
                    .HasName("IX06_NGVN_EVENT");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CablAddDueDt)
                    .HasColumnName("CABL_ADD_DUE_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CablPrcolTypeNme)
                    .IsRequired()
                    .HasColumnName("CABL_PRCOL_TYPE_NME")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltdEmailCcTxt)
                    .HasColumnName("CMPLTD_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcBrdgNbr)
                    .HasColumnName("CNFRC_BRDG_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcPinNbr)
                    .HasColumnName("CNFRC_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustCntctCellPhnNbr)
                    .HasColumnName("CUST_CNTCT_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctNme)
                    .HasColumnName("CUST_CNTCT_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPgrNbr)
                    .HasColumnName("CUST_CNTCT_PGR_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPgrPinNbr)
                    .HasColumnName("CUST_CNTCT_PGR_PIN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPhnNbr)
                    .HasColumnName("CUST_CNTCT_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustEmailAdr)
                    .HasColumnName("CUST_EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.DsgnCmntTxt)
                    .IsRequired()
                    .HasColumnName("DSGN_CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EsclCd).HasColumnName("ESCL_CD");

                entity.Property(e => e.EsclReasId).HasColumnName("ESCL_REAS_ID");

                entity.Property(e => e.EventDes)
                    .HasColumnName("EVENT_DES")
                    .IsUnicode(false);

                entity.Property(e => e.EventDrtnInMinQty).HasColumnName("EVENT_DRTN_IN_MIN_QTY");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt)
                    .HasColumnName("EVENT_TITLE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraDrtnTmeAmt).HasColumnName("EXTRA_DRTN_TME_AMT");

                entity.Property(e => e.Ftn)
                    .HasColumnName("FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GsrCfgrnDes)
                    .IsRequired()
                    .HasColumnName("GSR_CFGRN_DES")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.H1)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.H6).HasMaxLength(9);

                entity.Property(e => e.IpVerId).HasColumnName("IP_VER_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NgvnProdTypeId).HasColumnName("NGVN_PROD_TYPE_ID");

                entity.Property(e => e.PrimReqDt)
                    .HasColumnName("PRIM_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PubEmailCcTxt)
                    .HasColumnName("PUB_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReltdCmpsNcrCd).HasColumnName("RELTD_CMPS_NCR_CD");

                entity.Property(e => e.ReltdCmpsNcrNme)
                    .HasColumnName("RELTD_CMPS_NCR_NME")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ReqorUserId).HasColumnName("REQOR_USER_ID");

                entity.Property(e => e.SalsUserId).HasColumnName("SALS_USER_ID");

                entity.Property(e => e.SbcPairDes)
                    .IsRequired()
                    .HasColumnName("SBC_PAIR_DES")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ScndyReqDt)
                    .HasColumnName("SCNDY_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SowsEventId).HasColumnName("SOWS_EVENT_ID");

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.NgvnEventCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_NGVN_EVENT");

                entity.HasOne(d => d.EsclReas)
                    .WithMany(p => p.NgvnEvent)
                    .HasForeignKey(d => d.EsclReasId)
                    .HasConstraintName("FK06_NGVN_EVENT");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.NgvnEvent)
                    .HasForeignKey<NgvnEvent>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK09_NGVN_EVENT");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.NgvnEvent)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_NGVN_EVENT");

                entity.HasOne(d => d.IpVer)
                    .WithMany(p => p.NgvnEvent)
                    .HasForeignKey(d => d.IpVerId)
                    .HasConstraintName("FK12_NGVN_EVENT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.NgvnEventModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK07_NGVN_EVENT");

                entity.HasOne(d => d.NgvnProdType)
                    .WithMany(p => p.NgvnEvent)
                    .HasForeignKey(d => d.NgvnProdTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK10_NGVN_EVENT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.NgvnEvent)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_NGVN_EVENT");

                entity.HasOne(d => d.ReqorUser)
                    .WithMany(p => p.NgvnEventReqorUser)
                    .HasForeignKey(d => d.ReqorUserId)
                    .HasConstraintName("FK08_NGVN_EVENT");

                entity.HasOne(d => d.SalsUser)
                    .WithMany(p => p.NgvnEventSalsUser)
                    .HasForeignKey(d => d.SalsUserId)
                    .HasConstraintName("FK05_NGVN_EVENT");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.NgvnEvent)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .HasConstraintName("FK01_NGVN_EVENT");
            });

            modelBuilder.Entity<NgvnEventCktIdNua>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.CktIdNua });

                entity.ToTable("NGVN_EVENT_CKT_ID_NUA", "dbo");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.CktIdNua)
                    .HasColumnName("CKT_ID_NUA")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.NgvnEventCktIdNua)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_NGVN_EVENT_CKT_ID_NUA");
            });

            modelBuilder.Entity<NgvnEventSipTrnk>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.SipTrnkNme });

                entity.ToTable("NGVN_EVENT_SIP_TRNK", "dbo");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.SipTrnkNme)
                    .HasColumnName("SIP_TRNK_NME")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.NgvnEventSipTrnk)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_NGVN_EVENT_SIP_TRNK");
            });

            modelBuilder.Entity<NidActy>(entity =>
            {
                entity.ToTable("NID_ACTY", "dbo");

                entity.HasIndex(e => e.FsaCpeLineItemId)
                    .HasName("IX01_NID_ACTY");

                entity.Property(e => e.NidActyId).HasColumnName("NID_ACTY_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DdAprvlNbr)
                    .HasColumnName("DD_APRVL_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.FsaCpeLineItemId).HasColumnName("FSA_CPE_LINE_ITEM_ID");

                entity.Property(e => e.H6)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.M5OrdrNbr)
                    .HasColumnName("M5_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.NidHostNme)
                    .HasColumnName("NID_HOST_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NidSerialNbr)
                    .IsRequired()
                    .HasColumnName("NID_SERIAL_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((251))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.NidActyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_NID_ACTY");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.NidActy)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK02_NID_ACTY");

                entity.HasOne(d => d.FsaCpeLineItem)
                    .WithMany(p => p.NidActy)
                    .HasForeignKey(d => d.FsaCpeLineItemId)
                    .HasConstraintName("FK01_NID_ACTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.NidActyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK04_NID_ACTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.NidActy)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_NID_ACTY");
            });

            modelBuilder.Entity<NrmCkt>(entity =>
            {
                entity.ToTable("NRM_CKT", "dbo");

                entity.HasIndex(e => e.SrvcInstcObjId)
                    .HasName("IX01_NRM_CKT");

                entity.Property(e => e.NrmCktId).HasColumnName("NRM_CKT_ID");

                entity.Property(e => e.CktNme)
                    .HasColumnName("CKT_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CktObjId)
                    .HasColumnName("CKT_OBJ_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FmsCktId)
                    .HasColumnName("FMS_CKT_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FmsSiteId)
                    .HasColumnName("FMS_SITE_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ftn)
                    .HasColumnName("FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NuaAdr)
                    .HasColumnName("NUA_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PlnNme)
                    .HasColumnName("PLN_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PlnSeqNbr)
                    .HasColumnName("PLN_SEQ_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PortAsmtDes)
                    .HasColumnName("PORT_ASMT_DES")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.SrvcInstcNme)
                    .HasColumnName("SRVC_INSTC_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcInstcObjId).HasColumnName("SRVC_INSTC_OBJ_ID");

                entity.Property(e => e.TocAdr)
                    .HasColumnName("TOC_ADR")
                    .HasMaxLength(1500)
                    .IsUnicode(false);

                entity.Property(e => e.TocAdr1)
                    .HasColumnName("TOC_ADR_1")
                    .HasMaxLength(1500)
                    .IsUnicode(false);

                entity.Property(e => e.TocAdr2)
                    .HasColumnName("TOC_ADR_2")
                    .HasMaxLength(1500)
                    .IsUnicode(false);

                entity.Property(e => e.TocCtryCd)
                    .HasColumnName("TOC_CTRY_CD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TocCtryNme)
                    .HasColumnName("TOC_CTRY_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TocCtyNme)
                    .HasColumnName("TOC_CTY_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TocDmstcSttNme)
                    .HasColumnName("TOC_DMSTC_STT_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TocNme)
                    .HasColumnName("TOC_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TocZipCd)
                    .HasColumnName("TOC_ZIP_CD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.NrmCkt)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_NRM_CKT");
            });

            modelBuilder.Entity<NrmSrvcInstc>(entity =>
            {
                entity.HasKey(e => e.NrmSrvcInstcId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("NRM_SRVC_INSTC", "dbo");

                entity.Property(e => e.NrmSrvcInstcId).HasColumnName("NRM_SRVC_INSTC_ID");

                entity.Property(e => e.ActvDt)
                    .HasColumnName("ACTV_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.BillClearDt)
                    .HasColumnName("BILL_CLEAR_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DscnctDt)
                    .HasColumnName("DSCNCT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.Ftn)
                    .HasColumnName("FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.H5H6Id)
                    .HasColumnName("H5_H6_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LoopAcptDt)
                    .HasColumnName("LOOP_ACPT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.MdsCd).HasColumnName("MDS_CD");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NuaAdr)
                    .HasColumnName("NUA_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrCmntTxt)
                    .HasColumnName("ORDR_CMNT_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrStusDes)
                    .HasColumnName("ORDR_STUS_DES")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.SrvcInstcNme)
                    .HasColumnName("SRVC_INSTC_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcInstcObjId).HasColumnName("SRVC_INSTC_OBJ_ID");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.NrmSrvcInstc)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_NRM_SRVC_INSTC");
            });

            modelBuilder.Entity<NrmVndr>(entity =>
            {
                entity.HasKey(e => e.NmsVndrId);

                entity.ToTable("NRM_VNDR", "dbo");

                entity.HasIndex(e => e.SrvcInstcObjId)
                    .HasName("IX01_NRM_VNDR");

                entity.Property(e => e.NmsVndrId).HasColumnName("NMS_VNDR_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Ftn)
                    .HasColumnName("FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.H6H5Id)
                    .HasColumnName("H6_H5_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LecId)
                    .HasColumnName("LEC_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.SegTypeId)
                    .HasColumnName("SEG_TYPE_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcInstcObjId).HasColumnName("SRVC_INSTC_OBJ_ID");

                entity.Property(e => e.VndrCktId)
                    .HasColumnName("VNDR_CKT_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.VndrNme)
                    .HasColumnName("VNDR_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.VndrTypeId)
                    .HasColumnName("VNDR_TYPE_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.NrmVndr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_NRM_VNDR");
            });

            modelBuilder.Entity<OdieReq>(entity =>
            {
                entity.HasKey(e => e.ReqId);

                entity.ToTable("ODIE_REQ", "dbo");

                entity.HasIndex(e => e.MdsEventId)
                    .HasName("IX04_ODIE_REQ");

                entity.HasIndex(e => e.OdieMsgId)
                    .HasName("IX01_ODIE_REQ");

                entity.HasIndex(e => e.OrdrId)
                    .HasName("IX03_ODIE_REQ");

                entity.HasIndex(e => e.StusId)
                    .HasName("IX02_ODIE_REQ");

                entity.Property(e => e.ReqId).HasColumnName("REQ_ID");

                entity.Property(e => e.CptId).HasColumnName("CPT_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.DevFltr)
                    .HasColumnName("DEV_FLTR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.H1CustId)
                    .HasColumnName("H1_CUST_ID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.MdsEventId).HasColumnName("MDS_EVENT_ID");

                entity.Property(e => e.OdieCustId)
                    .HasColumnName("ODIE_CUST_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OdieCustInfoReqCatTypeId).HasColumnName("ODIE_CUST_INFO_REQ_CAT_TYPE_ID");

                entity.Property(e => e.OdieMsgId).HasColumnName("ODIE_MSG_ID");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.StusId)
                    .HasColumnName("STUS_ID")
                    .HasDefaultValueSql("((10))");

                entity.Property(e => e.StusModDt)
                    .HasColumnName("STUS_MOD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.TabSeqNbr).HasColumnName("TAB_SEQ_NBR");

                entity.HasOne(d => d.Cpt)
                    .WithMany(p => p.OdieReq)
                    .HasForeignKey(d => d.CptId)
                    .HasConstraintName("FK05_ODIE_REQ");

                entity.HasOne(d => d.CsgLvl)
                    .WithMany(p => p.OdieReq)
                    .HasForeignKey(d => d.CsgLvlId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK07_ODIE_REQ");

                entity.HasOne(d => d.MdsEvent)
                    .WithMany(p => p.OdieReq)
                    .HasForeignKey(d => d.MdsEventId)
                    .HasConstraintName("FK04_ODIE_REQ");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OdieReq)
                    .HasForeignKey(d => d.OrdrId)
                    .HasConstraintName("FK02_ODIE_REQ");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.OdieReq)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_ODIE_REQ");
            });

            modelBuilder.Entity<OdieRspn>(entity =>
            {
                entity.HasKey(e => e.RspnId);

                entity.ToTable("ODIE_RSPN", "dbo");

                entity.Property(e => e.RspnId).HasColumnName("RSPN_ID");

                entity.Property(e => e.AckCd).HasColumnName("ACK_CD");

                entity.Property(e => e.ActCd)
                    .IsRequired()
                    .HasColumnName("ACT_CD")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CustTeamPdl)
                    .HasColumnName("CUST_TEAM_PDL")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DocUrlAdr)
                    .HasColumnName("DOC_URL_ADR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MnspmId)
                    .HasColumnName("MNSPM_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NteId)
                    .HasColumnName("NTE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OdieCustId)
                    .HasColumnName("ODIE_CUST_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ReqId).HasColumnName("REQ_ID");

                entity.Property(e => e.RspnDt)
                    .HasColumnName("RSPN_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RspnErrorTxt)
                    .HasColumnName("RSPN_ERROR_TXT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SdeAssigned)
                    .HasColumnName("SDE_Assigned")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SowsFoldrPathNme)
                    .HasColumnName("SOWS_FOLDR_PATH_NME")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.OdieRspn)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ODIE_RSPN");
            });

            modelBuilder.Entity<OdieRspnInfo>(entity =>
            {
                entity.HasKey(e => e.RspnInfoId);

                entity.ToTable("ODIE_RSPN_INFO", "dbo");

                entity.HasIndex(e => e.EventId)
                    .HasName("IX03_ODIE_RSPN_INFO");

                entity.HasIndex(e => e.FsaMdsEventId)
                    .HasName("IX02_ODIE_RSPN_INFO");

                entity.HasIndex(e => e.ReqId)
                    .HasName("IX01_ODIE_RSPN_INFO");

                entity.Property(e => e.RspnInfoId).HasColumnName("RSPN_INFO_ID");

                entity.Property(e => e.BusJustnTxt)
                    .HasColumnName("BUS_JUSTN_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DevId)
                    .HasColumnName("DEV_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DspchRdyCd).HasColumnName("DSPCH_RDY_CD");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.FastTrkCd).HasColumnName("FAST_TRK_CD");

                entity.Property(e => e.FrwlProdCd)
                    .HasColumnName("FRWL_PROD_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FsaMdsEventId).HasColumnName("FSA_MDS_EVENT_ID");

                entity.Property(e => e.H6CustId)
                    .HasColumnName("H6_CUST_ID")
                    .HasMaxLength(9);

                entity.Property(e => e.ManfId)
                    .HasColumnName("MANF_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MgtTxt)
                    .HasColumnName("MGT_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModelId)
                    .HasColumnName("MODEL_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OneMbTxt)
                    .HasColumnName("ONE_MB_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OptOutCd)
                    .HasColumnName("OPT_OUT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.OptOutReasTxt)
                    .HasColumnName("OPT_OUT_REAS_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RdsnNbr)
                    .HasColumnName("RDSN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqId).HasColumnName("REQ_ID");

                entity.Property(e => e.RspnInfoDt)
                    .HasColumnName("RSPN_INFO_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SerialNo)
                    .HasColumnName("SERIAL_NO")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.SlctdCd).HasColumnName("SLCTD_CD");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.OdieRspnInfo)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK03_ODIE_RSPN_INFO");

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.OdieRspnInfo)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ODIE_RSPN_INFO");
            });

            modelBuilder.Entity<Ordr>(entity =>
            {
                entity.ToTable("ORDR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_ORDR");

                entity.HasIndex(e => e.DmstcCd)
                    .HasName("IX11_ORDR");

                entity.HasIndex(e => e.H5FoldrId)
                    .HasName("IX04_ORDR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_ORDR");

                entity.HasIndex(e => e.OrdrCatId)
                    .HasName("IX01_ORDR");

                entity.HasIndex(e => e.OrdrStusId)
                    .HasName("IX10_ORDR");

                entity.HasIndex(e => e.PltfrmCd)
                    .HasName("IX09_ORDR");

                entity.HasIndex(e => e.PprtId)
                    .HasName("IX05_ORDR");

                entity.HasIndex(e => e.PrntOrdrId)
                    .HasName("IX06_ORDR");

                entity.HasIndex(e => e.RgnId)
                    .HasName("IX08_ORDR");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.CcdUpdtCd).HasColumnName("CCD_UPDT_CD");

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CpeClli)
                    .HasColumnName("CPE_CLLI")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.CustCmmtDt)
                    .HasColumnName("CUST_CMMT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.DlvyClli)
                    .HasColumnName("DLVY_CLLI")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DmstcCd).HasColumnName("DMSTC_CD");

                entity.Property(e => e.FsaExpTypeCd)
                    .HasColumnName("FSA_EXP_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.H5FoldrId).HasColumnName("H5_FOLDR_ID");

                entity.Property(e => e.H5H6CustId).HasColumnName("H5_H6_CUST_ID");

                entity.Property(e => e.InstlEsclCd)
                    .HasColumnName("INSTL_ESCL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrCatId).HasColumnName("ORDR_CAT_ID");

                entity.Property(e => e.OrdrCmpltnDrtnTmeTxt)
                    .HasColumnName("ORDR_CMPLTN_DRTN_TME_TXT")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrStusId)
                    .HasColumnName("ORDR_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PltfrmCd)
                    .HasColumnName("PLTFRM_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PprtId).HasColumnName("PPRT_ID");

                entity.Property(e => e.PrntOrdrId).HasColumnName("PRNT_ORDR_ID");

                entity.Property(e => e.ProdId)
                    .HasColumnName("PROD_ID")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RasDt)
                    .HasColumnName("RAS_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RgnId).HasColumnName("RGN_ID");

                entity.Property(e => e.SlaVltdCd).HasColumnName("SLA_VLTD_CD");

                entity.Property(e => e.SmrNmr)
                    .HasColumnName("SMR_NMR")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.OrdrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_ORDR");

                entity.HasOne(d => d.CsgLvl)
                    .WithMany(p => p.Ordr)
                    .HasForeignKey(d => d.CsgLvlId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK08_ORDR");

                entity.HasOne(d => d.H5Foldr)
                    .WithMany(p => p.Ordr)
                    .HasForeignKey(d => d.H5FoldrId)
                    .HasConstraintName("FK05_ORDR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.OrdrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK04_ORDR");

                entity.HasOne(d => d.OrdrCat)
                    .WithMany(p => p.Ordr)
                    .HasForeignKey(d => d.OrdrCatId)
                    .HasConstraintName("FK01_ORDR");

                entity.HasOne(d => d.OrdrStus)
                    .WithMany(p => p.Ordr)
                    .HasForeignKey(d => d.OrdrStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK11_ORDR");

                entity.HasOne(d => d.PltfrmCdNavigation)
                    .WithMany(p => p.Ordr)
                    .HasForeignKey(d => d.PltfrmCd)
                    .HasConstraintName("FK10_ORDR");

                entity.HasOne(d => d.Pprt)
                    .WithMany(p => p.Ordr)
                    .HasForeignKey(d => d.PprtId)
                    .HasConstraintName("FK06_ORDR");

                entity.HasOne(d => d.PrntOrdr)
                    .WithMany(p => p.InversePrntOrdr)
                    .HasForeignKey(d => d.PrntOrdrId)
                    .HasConstraintName("FK07_ORDR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.Ordr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_ORDR");

                entity.HasOne(d => d.Rgn)
                    .WithMany(p => p.Ordr)
                    .HasForeignKey(d => d.RgnId)
                    .HasConstraintName("FK09_ORDR");
            });

            modelBuilder.Entity<OrdrAdr>(entity =>
            {
                entity.ToTable("ORDR_ADR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX03_ORDR_ADR");

                entity.HasIndex(e => e.CtryCd)
                    .HasName("IX01_ORDR_ADR");

                entity.HasIndex(e => new { e.OrdrId, e.AdrTypeId, e.CisLvlType, e.FsaMdulId })
                    .HasName("UX01_ORDR_ADR")
                    .IsUnique();

                entity.Property(e => e.OrdrAdrId).HasColumnName("ORDR_ADR_ID");

                entity.Property(e => e.AdrTypeId).HasColumnName("ADR_TYPE_ID");

                entity.Property(e => e.BldgNme)
                    .HasColumnName("BLDG_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CisLvlType)
                    .HasColumnName("CIS_LVL_TYPE")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FlrId)
                    .HasColumnName("FLR_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FsaMdulId)
                    .HasColumnName("FSA_MDUL_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.HierLvlCd).HasColumnName("HIER_LVL_CD");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.PrvnNme)
                    .HasColumnName("PRVN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RmNbr)
                    .HasColumnName("RM_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAdr1)
                    .HasColumnName("STREET_ADR_1")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAdr2)
                    .HasColumnName("STREET_ADR_2")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAdr3)
                    .HasColumnName("STREET_ADR_3")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.SttCd)
                    .HasColumnName("STT_CD")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ZipPstlCd)
                    .HasColumnName("ZIP_PSTL_CD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.AdrType)
                    .WithMany(p => p.OrdrAdr)
                    .HasForeignKey(d => d.AdrTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ORDR_ADR");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.OrdrAdr)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_ORDR_ADR");

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.OrdrAdr)
                    .HasForeignKey(d => d.CtryCd)
                    .HasConstraintName("FK02_ORDR_ADR");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OrdrAdr)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_ORDR_ADR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.OrdrAdr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_ORDR_ADR");
            });

            modelBuilder.Entity<OrdrCktChg>(entity =>
            {
                entity.HasKey(e => new { e.OrdrId, e.CktChgTypeId, e.VerId, e.TrmtgCd });

                entity.ToTable("ORDR_CKT_CHG", "dbo");

                entity.HasIndex(e => e.ChgCurId)
                    .HasName("IX02_ORDR_CKT_CHG");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_ORDR_CKT_CHG");

                entity.HasIndex(e => e.SalsStusId)
                    .HasName("IX03_ORDR_CKT_CHG");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.CktChgTypeId).HasColumnName("CKT_CHG_TYPE_ID");

                entity.Property(e => e.VerId).HasColumnName("VER_ID");

                entity.Property(e => e.TrmtgCd).HasColumnName("TRMTG_CD");

                entity.Property(e => e.ChgCurId)
                    .HasColumnName("CHG_CUR_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ChgNrcAmt)
                    .HasColumnName("CHG_NRC_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.ChgNrcInUsdAmt)
                    .HasColumnName("CHG_NRC_IN_USD_AMT")
                    .HasColumnType("money");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NteTxt)
                    .HasColumnName("NTE_TXT")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ReqrBillOrdrCd)
                    .HasColumnName("REQR_BILL_ORDR_CD")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SalsStusId).HasColumnName("SALS_STUS_ID");

                entity.Property(e => e.TaxRtPctQty)
                    .HasColumnName("TAX_RT_PCT_QTY")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.XpirnDt)
                    .HasColumnName("XPIRN_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.ChgCur)
                    .WithMany(p => p.OrdrCktChg)
                    .HasForeignKey(d => d.ChgCurId)
                    .HasConstraintName("FK03_ORDR_CKT_CHG");

                entity.HasOne(d => d.CktChgType)
                    .WithMany(p => p.OrdrCktChg)
                    .HasForeignKey(d => d.CktChgTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_ORDR_CKT_CHG");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.OrdrCktChg)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_ORDR_CKT_CHG");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OrdrCktChg)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ORDR_CKT_CHG");

                entity.HasOne(d => d.SalsStus)
                    .WithMany(p => p.OrdrCktChg)
                    .HasForeignKey(d => d.SalsStusId)
                    .HasConstraintName("FK05_ORDR_CKT_CHG");
            });

            modelBuilder.Entity<OrdrCntct>(entity =>
            {
                entity.ToTable("ORDR_CNTCT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_ORDR_CNTCT");

                entity.HasIndex(e => e.OrdrId)
                    .HasName("IX05_ORDR_CNTCT");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_ORDR_CNTCT");

                entity.HasIndex(e => e.TmeZoneId)
                    .HasName("IX04_ORDR_CNTCT");

                entity.Property(e => e.OrdrCntctId).HasColumnName("ORDR_CNTCT_ID");

                entity.Property(e => e.CisLvlType)
                    .HasColumnName("CIS_LVL_TYPE")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CntctHrTxt)
                    .HasColumnName("CNTCT_HR_TXT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CntctNme)
                    .HasColumnName("CNTCT_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CntctTypeId).HasColumnName("CNTCT_TYPE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtyCd)
                    .HasColumnName("CTY_CD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAdr)
                    .HasColumnName("EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FaxNbr)
                    .HasColumnName("FAX_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FrstNme)
                    .HasColumnName("FRST_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FsaMdulId)
                    .HasColumnName("FSA_MDUL_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.FsaTmeZoneCd)
                    .HasColumnName("FSA_TME_ZONE_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IntprtrCd).HasColumnName("INTPRTR_CD");

                entity.Property(e => e.IsdCd)
                    .HasColumnName("ISD_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.LstNme)
                    .HasColumnName("LST_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Npa)
                    .HasColumnName("NPA")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Nxx)
                    .HasColumnName("NXX")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.PhnExtNbr)
                    .HasColumnName("PHN_EXT_NBR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PhnNbr)
                    .HasColumnName("PHN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RoleId).HasColumnName("ROLE_ID");

                entity.Property(e => e.StnNbr)
                    .HasColumnName("STN_NBR")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.SuppdLangNme)
                    .HasColumnName("SUPPD_LANG_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TmeZoneId)
                    .HasColumnName("TME_ZONE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.CntctType)
                    .WithMany(p => p.OrdrCntct)
                    .HasForeignKey(d => d.CntctTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ORDR_CNTCT");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.OrdrCntct)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_ORDR_CNTCT");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OrdrCntct)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_ORDR_CNTCT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.OrdrCntct)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_ORDR_CNTCT");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.OrdrCntct)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK07_ORDR_CNTCT");
            });

            modelBuilder.Entity<OrdrJprdy>(entity =>
            {
                entity.HasKey(e => new { e.OrdrId, e.JprdyCd, e.NteId });

                entity.ToTable("ORDR_JPRDY", "dbo");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.JprdyCd)
                    .HasColumnName("JPRDY_CD")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.NteId).HasColumnName("NTE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Nte)
                    .WithMany(p => p.OrdrJprdy)
                    .HasForeignKey(d => d.NteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_ORDR_JPRDY");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OrdrJprdy)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_ORDR_JPRDY");
            });

            modelBuilder.Entity<OrdrMs>(entity =>
            {
                entity.HasKey(e => new { e.OrdrId, e.VerId });

                entity.ToTable("ORDR_MS", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_ORDR_MS");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.VerId).HasColumnName("VER_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustAcptcTurnupDt)
                    .HasColumnName("CUST_ACPTC_TURNUP_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrBillClearInstlDt)
                    .HasColumnName("ORDR_BILL_CLEAR_INSTL_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrDscnctDt)
                    .HasColumnName("ORDR_DSCNCT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PreSbmtDt)
                    .HasColumnName("PRE_SBMT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RnlDtReqrCd)
                    .HasColumnName("RNL_DT_REQR_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SbmtDt)
                    .HasColumnName("SBMT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.VldtdDt)
                    .HasColumnName("VLDTD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.VndrCnclnDt)
                    .HasColumnName("VNDR_CNCLN_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.XnciCnfrmRnlDt)
                    .HasColumnName("XNCI_CNFRM_RNL_DT")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.OrdrMs)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ORDR_MS");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OrdrMs)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_ORDR_MS");
            });

            modelBuilder.Entity<OrdrNte>(entity =>
            {
                entity.HasKey(e => e.NteId);

                entity.ToTable("ORDR_NTE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX03_ORDR_NTE");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX04_ORDR_NTE");

                entity.HasIndex(e => e.NteTypeId)
                    .HasName("IX01_ORDR_NTE");

                entity.HasIndex(e => e.OrdrId)
                    .HasName("IX02_ORDR_NTE");

                entity.Property(e => e.NteId).HasColumnName("NTE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NteTxt)
                    .IsRequired()
                    .HasColumnName("NTE_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.NteTypeId).HasColumnName("NTE_TYPE_ID");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.OrdrNteCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_ORDR_NTE");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.OrdrNteModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_ORDR_NTE");

                entity.HasOne(d => d.NteType)
                    .WithMany(p => p.OrdrNte)
                    .HasForeignKey(d => d.NteTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ORDR_NTE");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OrdrNte)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_ORDR_NTE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.OrdrNte)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_ORDR_NTE");
            });

            modelBuilder.Entity<OrdrRecLock>(entity =>
            {
                entity.HasKey(e => e.OrdrId);

                entity.ToTable("ORDR_REC_LOCK", "dbo");

                entity.HasIndex(e => e.LockByUserId)
                    .HasName("IX01_ORDR_REC_LOCK");

                entity.Property(e => e.OrdrId)
                    .HasColumnName("ORDR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LockByUserId).HasColumnName("LOCK_BY_USER_ID");

                entity.Property(e => e.StrtRecLockTmst)
                    .HasColumnName("STRT_REC_LOCK_TMST")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.LockByUser)
                    .WithMany(p => p.OrdrRecLock)
                    .HasForeignKey(d => d.LockByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_ORDR_REC_LOCK");

                entity.HasOne(d => d.Ordr)
                    .WithOne(p => p.OrdrRecLock)
                    .HasForeignKey<OrdrRecLock>(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ORDR_REC_LOCK");
            });

            modelBuilder.Entity<OrdrStdiHist>(entity =>
            {
                entity.HasKey(e => e.OrdrStdiId);

                entity.ToTable("ORDR_STDI_HIST", "dbo");

                entity.Property(e => e.OrdrStdiId).HasColumnName("ORDR_STDI_ID");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.StdiDt)
                    .HasColumnName("STDI_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.StdiReasId).HasColumnName("STDI_REAS_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.OrdrStdiHistCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_ORDR_STDI_HIST");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.OrdrStdiHistModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK04_ORDR_STDI_HIST");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OrdrStdiHist)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ORDR_STDI_HIST");

                entity.HasOne(d => d.StdiReas)
                    .WithMany(p => p.OrdrStdiHist)
                    .HasForeignKey(d => d.StdiReasId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_ORDR_STDI_HIST");
            });

            modelBuilder.Entity<OrdrVlan>(entity =>
            {
                entity.ToTable("ORDR_VLAN", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_ORDR_VLAN");

                entity.Property(e => e.OrdrVlanId).HasColumnName("ORDR_VLAN_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.TrmtgCd).HasColumnName("TRMTG_CD");

                entity.Property(e => e.VlanId)
                    .IsRequired()
                    .HasColumnName("VLAN_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VlanPctQty).HasColumnName("VLAN_PCT_QTY");

                entity.Property(e => e.VlanSrcNme)
                    .HasColumnName("VLAN_SRC_NME")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.OrdrVlan)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_ORDR_VLAN");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.OrdrVlan)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_ORDR_VLAN");
            });

            modelBuilder.Entity<PltfrmMapng>(entity =>
            {
                entity.ToTable("PLTFRM_MAPNG", "dbo");

                entity.HasIndex(e => e.FsaProdTypeCd)
                    .HasName("IX01_PLTFRM_MAPNG");

                entity.HasIndex(e => new { e.PltfrmCd, e.ProdNme })
                    .HasName("UX01_PLTFRM_MAPNG")
                    .IsUnique();

                entity.Property(e => e.PltfrmMapngId).HasColumnName("PLTFRM_MAPNG_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CxrPtnrAvalCd)
                    .IsRequired()
                    .HasColumnName("CXR_PTNR_AVAL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FsaProdTypeCd)
                    .HasColumnName("FSA_PROD_TYPE_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PltfrmCd)
                    .HasColumnName("PLTFRM_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ProdNme)
                    .IsRequired()
                    .HasColumnName("PROD_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.FsaProdTypeCdNavigation)
                    .WithMany(p => p.PltfrmMapng)
                    .HasForeignKey(d => d.FsaProdTypeCd)
                    .HasConstraintName("FK03_PLTFRM_MAPNG");

                entity.HasOne(d => d.PltfrmCdNavigation)
                    .WithMany(p => p.PltfrmMapng)
                    .HasForeignKey(d => d.PltfrmCd)
                    .HasConstraintName("FK01_PLTFRM_MAPNG");

                entity.HasOne(d => d.ProdNmeNavigation)
                    .WithMany(p => p.PltfrmMapng)
                    .HasForeignKey(d => d.ProdNme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_PLTFRM_MAPNG");
            });

            modelBuilder.Entity<PsReqHdrQueue>(entity =>
            {
                entity.HasKey(e => e.ReqHdrId)
                    .HasName("PK_PS_REQ_HDR");

                entity.ToTable("PS_REQ_HDR_QUEUE", "dbo");

                entity.HasIndex(e => new { e.OrdrId, e.ReqstnNbr })
                    .HasName("UX01_PS_REQ_HDR")
                    .IsUnique();

                entity.Property(e => e.ReqHdrId).HasColumnName("REQ_HDR_ID");

                entity.Property(e => e.BatchSeq).HasColumnName("BATCH_SEQ");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CsgLvl)
                    .HasColumnName("CSG_LVL")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CustElid)
                    .HasColumnName("CUST_ELID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyAddr1)
                    .HasColumnName("DLVY_ADDR1")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyAddr2)
                    .HasColumnName("DLVY_ADDR2")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyAddr3)
                    .HasColumnName("DLVY_ADDR3")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyAddr4)
                    .HasColumnName("DLVY_ADDR4")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyBldg)
                    .HasColumnName("DLVY_BLDG")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyClli)
                    .HasColumnName("DLVY_CLLI")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyCnty)
                    .HasColumnName("DLVY_CNTY")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyCty)
                    .HasColumnName("DLVY_CTY")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyFlr)
                    .HasColumnName("DLVY_FLR")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyNme)
                    .HasColumnName("DLVY_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyPhnNbr)
                    .HasColumnName("DLVY_PHN_NBR")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyRm)
                    .HasColumnName("DLVY_RM")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.DlvySt)
                    .HasColumnName("DLVY_ST")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.DlvyZip)
                    .HasColumnName("DLVY_ZIP")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.InstClli)
                    .HasColumnName("INST_CLLI")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MrkPkg)
                    .HasColumnName("MRK_PKG")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RefNbr)
                    .HasColumnName("REF_NBR")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ReqstnDt)
                    .HasColumnName("REQSTN_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.ReqstnNbr)
                    .IsRequired()
                    .HasColumnName("REQSTN_NBR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RfqIndctr)
                    .HasColumnName("RFQ_INDCTR")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SentDt)
                    .HasColumnName("SENT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShipCmmts)
                    .HasColumnName("SHIP_CMMTS")
                    .HasMaxLength(529)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PsReqLineItmQueue>(entity =>
            {
                entity.HasKey(e => e.ReqLineItmId)
                    .HasName("PK_PS_REQ_LINE_ITM");

                entity.ToTable("PS_REQ_LINE_ITM_QUEUE", "dbo");

                entity.HasIndex(e => e.ReqLineNbr)
                    .HasName("IX01_PS_REQ_LINE_ITM_QUEUE");

                entity.HasIndex(e => new { e.ReqLineItmId, e.OrdrId, e.ReqstnNbr })
                    .HasName("UX01_PS_REQ_LINE_ITM")
                    .IsUnique();

                entity.Property(e => e.ReqLineItmId).HasColumnName("REQ_LINE_ITM_ID");

                entity.Property(e => e.Acct)
                    .HasColumnName("ACCT")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Actvy)
                    .HasColumnName("ACTVY")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Afflt)
                    .HasColumnName("AFFLT")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.AxlryId)
                    .HasColumnName("AXLRY_ID")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.BusUntGl)
                    .HasColumnName("BUS_UNT_GL")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.BusUntPc)
                    .HasColumnName("BUS_UNT_PC")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CmpntId).HasColumnName("CMPNT_ID");

                entity.Property(e => e.CntrctId)
                    .HasColumnName("CNTRCT_ID")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.CntrctLnNbr).HasColumnName("CNTRCT_LN_NBR");

                entity.Property(e => e.Comments)
                    .HasColumnName("COMMENTS")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.CostCntr)
                    .HasColumnName("COST_CNTR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DlvyClli)
                    .HasColumnName("DLVY_CLLI")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EqptTypeId)
                    .HasColumnName("EQPT_TYPE_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FsaCpeLineItemId).HasColumnName("FSA_CPE_LINE_ITEM_ID");

                entity.Property(e => e.InstCd)
                    .HasColumnName("INST_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ItmDes)
                    .HasColumnName("ITM_DES")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.ManfDiscntCd)
                    .HasColumnName("MANF_DISCNT_CD")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ManfId)
                    .HasColumnName("MANF_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ManfPartNbr)
                    .HasColumnName("MANF_PART_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MatCd)
                    .HasColumnName("MAT_CD")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mrkt)
                    .HasColumnName("MRKT")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.OrdrQty).HasColumnName("ORDR_QTY");

                entity.Property(e => e.Prodct)
                    .HasColumnName("PRODCT")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ProjId)
                    .HasColumnName("PROJ_ID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.RasDt)
                    .HasColumnName("RAS_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Regn)
                    .HasColumnName("REGN")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ReqLineNbr).HasColumnName("REQ_LINE_NBR");

                entity.Property(e => e.ReqstnNbr)
                    .IsRequired()
                    .HasColumnName("REQSTN_NBR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RsrcCat)
                    .HasColumnName("RSRC_CAT")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RsrcSub)
                    .HasColumnName("RSRC_SUB")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SentDt)
                    .HasColumnName("SENT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.SourceTyp)
                    .HasColumnName("SOURCE_TYP")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.UntMsr)
                    .HasColumnName("UNT_MSR")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.UntPrice)
                    .HasColumnName("UNT_PRICE")
                    .HasColumnType("decimal(15, 5)");

                entity.Property(e => e.VndrNme)
                    .HasColumnName("VNDR_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QlfctnDedctdCust>(entity =>
            {
                entity.HasKey(e => new { e.QlfctnId, e.CustId });

                entity.ToTable("QLFCTN_DEDCTD_CUST", "dbo");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.CustId).HasColumnName("CUST_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cust)
                    .WithMany(p => p.QlfctnDedctdCust)
                    .HasForeignKey(d => d.CustId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_QLFCTN_DEDCTD_CUST");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnDedctdCust)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_QLFCTN_DEDCTD_CUST");
            });

            modelBuilder.Entity<QlfctnDev>(entity =>
            {
                entity.HasKey(e => new { e.QlfctnId, e.DevId });

                entity.ToTable("QLFCTN_DEV", "dbo");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.DevId).HasColumnName("DEV_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Dev)
                    .WithMany(p => p.QlfctnDev)
                    .HasForeignKey(d => d.DevId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_QLFCTN_DEV");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnDev)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_QLFCTN_DEV");
            });

            modelBuilder.Entity<QlfctnEnhncSrvc>(entity =>
            {
                entity.HasKey(e => new { e.QlfctnId, e.EnhncSrvcId });

                entity.ToTable("QLFCTN_ENHNC_SRVC", "dbo");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.EnhncSrvcId).HasColumnName("ENHNC_SRVC_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.EnhncSrvc)
                    .WithMany(p => p.QlfctnEnhncSrvc)
                    .HasForeignKey(d => d.EnhncSrvcId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_QLFCTN_ENHNC_SRVC");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnEnhncSrvc)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_QLFCTN_ENHNC_SRVC");
            });

            modelBuilder.Entity<QlfctnEventType>(entity =>
            {
                entity.HasKey(e => new { e.EventTypeId, e.QlfctnId });

                entity.ToTable("QLFCTN_EVENT_TYPE", "dbo");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.QlfctnEventType)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_QLFCTN_EVENT_TYPE");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnEventType)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_QLFCTN_EVENT_TYPE");
            });

            modelBuilder.Entity<QlfctnIpVer>(entity =>
            {
                entity.HasKey(e => new { e.QlfctnId, e.IpVerId });

                entity.ToTable("QLFCTN_IP_VER", "dbo");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.IpVerId).HasColumnName("IP_VER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IpVer)
                    .WithMany(p => p.QlfctnIpVer)
                    .HasForeignKey(d => d.IpVerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_QLFCTN_IP_VER");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnIpVer)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_QLFCTN_IP_VER");
            });

            modelBuilder.Entity<QlfctnMplsActyType>(entity =>
            {
                entity.HasKey(e => new { e.MplsActyTypeId, e.QlfctnId });

                entity.ToTable("QLFCTN_MPLS_ACTY_TYPE", "dbo");

                entity.Property(e => e.MplsActyTypeId).HasColumnName("MPLS_ACTY_TYPE_ID");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.MplsActyType)
                    .WithMany(p => p.QlfctnMplsActyType)
                    .HasForeignKey(d => d.MplsActyTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_QLFCTN_MPLS_ACTY_TYPE");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnMplsActyType)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_QLFCTN_MPLS_ACTY_TYPE");
            });

            modelBuilder.Entity<QlfctnNtwkActyType>(entity =>
            {
                entity.HasKey(e => new { e.NtwkActyTypeId, e.QlfctnId });

                entity.ToTable("QLFCTN_NTWK_ACTY_TYPE", "dbo");

                entity.Property(e => e.NtwkActyTypeId).HasColumnName("NTWK_ACTY_TYPE_ID");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.NtwkActyType)
                    .WithMany(p => p.QlfctnNtwkActyType)
                    .HasForeignKey(d => d.NtwkActyTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_QLFCTN_NTWK_ACTY_TYPE");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnNtwkActyType)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_QLFCTN_NTWK_ACTY_TYPE");
            });

            modelBuilder.Entity<QlfctnSpclProj>(entity =>
            {
                entity.HasKey(e => new { e.QlfctnId, e.SpclProjId });

                entity.ToTable("QLFCTN_SPCL_PROJ", "dbo");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.SpclProjId).HasColumnName("SPCL_PROJ_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnSpclProj)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_QLFCTN_SPCL_PROJ");

                entity.HasOne(d => d.SpclProj)
                    .WithMany(p => p.QlfctnSpclProj)
                    .HasForeignKey(d => d.SpclProjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_QLFCTN_SPCL_PROJ");
            });

            modelBuilder.Entity<QlfctnVndrModel>(entity =>
            {
                entity.HasKey(e => new { e.QlfctnId, e.DevModelId });

                entity.ToTable("QLFCTN_VNDR_MODEL", "dbo");

                entity.Property(e => e.QlfctnId).HasColumnName("QLFCTN_ID");

                entity.Property(e => e.DevModelId).HasColumnName("DEV_MODEL_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.DevModel)
                    .WithMany(p => p.QlfctnVndrModel)
                    .HasForeignKey(d => d.DevModelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_QLFCTN_VNDR_MODEL");

                entity.HasOne(d => d.Qlfctn)
                    .WithMany(p => p.QlfctnVndrModel)
                    .HasForeignKey(d => d.QlfctnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_QLFCTN_VNDR_MODEL");
            });

            modelBuilder.Entity<Redsgn>(entity =>
            {
                entity.ToTable("REDSGN", "dbo");

                entity.HasIndex(e => e.RedsgnNbr)
                    .HasName("IX01_REDSGN");

                entity.Property(e => e.RedsgnId).HasColumnName("REDSGN_ID");

                entity.Property(e => e.AddlDocTxt)
                    .HasColumnName("ADDL_DOC_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.ApprovNbr)
                    .HasColumnName("APPROV_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillOvrrdnAmt)
                    .HasColumnName("BILL_OVRRDN_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.BillOvrrdnCd).HasColumnName("BILL_OVRRDN_CD");

                entity.Property(e => e.BpmRedsgnNbr)
                    .HasColumnName("BPM_REDSGN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CiscSmrtLicCd).HasColumnName("CISC_SMRT_LIC_CD");

                entity.Property(e => e.CntctEmailList)
                    .HasColumnName("CNTCT_EMAIL_LIST")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CostAmt)
                    .HasColumnName("COST_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CretdByCd).HasColumnName("CRETD_BY_CD");

                entity.Property(e => e.CretdDt)
                    .HasColumnName("CRETD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.CustEmailAdr)
                    .HasColumnName("CUST_EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.DsgnDocLocTxt)
                    .HasColumnName("DSGN_DOC_LOC_TXT")
                    .HasMaxLength(2000);

                entity.Property(e => e.DsgnDocNbrBpm)
                    .HasColumnName("DSGN_DOC_NBR_BPM")
                    .HasMaxLength(26)
                    .IsUnicode(false);

                entity.Property(e => e.EntireNwCkdId).HasColumnName("ENTIRE_NW_CKD_ID");

                entity.Property(e => e.ExprtnDt)
                    .HasColumnName("EXPRTN_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ExtXpirnFlgCd).HasColumnName("EXT_XPIRN_FLG_CD");

                entity.Property(e => e.H1Cd)
                    .IsRequired()
                    .HasColumnName("H1_CD")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.IsSdwan).HasColumnName("IS_SDWAN");

                entity.Property(e => e.ModSubmitDt)
                    .HasColumnName("MOD_SUBMIT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ModfdByCd).HasColumnName("MODFD_BY_CD");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MssImplEstAmt)
                    .HasColumnName("MSS_IMPL_EST_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NeAsnNme)
                    .HasColumnName("NE_ASN_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NeLvlEffortAmt)
                    .HasColumnName("NE_LVL_EFFORT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NeOtLvlEffortAmt)
                    .HasColumnName("NE_OT_LVL_EFFORT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NteAssigned)
                    .HasColumnName("NTE_ASSIGNED")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NteLvlEffortAmt)
                    .HasColumnName("NTE_LVL_EFFORT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.NteOtLvlEffortAmt)
                    .HasColumnName("NTE_OT_LVL_EFFORT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.OdieCustId)
                    .HasColumnName("ODIE_CUST_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PmAssigned)
                    .HasColumnName("PM_ASSIGNED")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PmLvlEffortAmt)
                    .HasColumnName("PM_LVL_EFFORT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.PmOtLvlEffortAmt)
                    .HasColumnName("PM_OT_LVL_EFFORT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.RedsgnCatId).HasColumnName("REDSGN_CAT_ID");

                entity.Property(e => e.RedsgnDevStusId).HasColumnName("REDSGN_DEV_STUS_ID");

                entity.Property(e => e.RedsgnNbr)
                    .IsRequired()
                    .HasColumnName("REDSGN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RedsgnTypeId).HasColumnName("REDSGN_TYPE_ID");

                entity.Property(e => e.SdeAsnNme)
                    .HasColumnName("SDE_ASN_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SlaDt)
                    .HasColumnName("SLA_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SmrtAccntDmn)
                    .HasColumnName("SMRT_ACCNT_DMN")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SowsFoldrPathNme)
                    .HasColumnName("SOWS_FOLDR_PATH_NME")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SpsLvlEffortAmt)
                    .HasColumnName("SPS_LVL_EFFORT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SpsOtLvlEffortAmt)
                    .HasColumnName("SPS_OT_LVL_EFFORT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.StusId).HasColumnName("STUS_ID");

                entity.Property(e => e.SubmitDt)
                    .HasColumnName("SUBMIT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.VrtlAccnt)
                    .HasColumnName("VRTL_ACCNT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CretdByCdNavigation)
                    .WithMany(p => p.RedsgnCretdByCdNavigation)
                    .HasForeignKey(d => d.CretdByCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_REDSGN");

                entity.HasOne(d => d.CsgLvl)
                    .WithMany(p => p.Redsgn)
                    .HasForeignKey(d => d.CsgLvlId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK08_REDSGN");

                entity.HasOne(d => d.ModfdByCdNavigation)
                    .WithMany(p => p.RedsgnModfdByCdNavigation)
                    .HasForeignKey(d => d.ModfdByCd)
                    .HasConstraintName("FK07_REDSGN");

                entity.HasOne(d => d.RedsgnCat)
                    .WithMany(p => p.Redsgn)
                    .HasForeignKey(d => d.RedsgnCatId)
                    .HasConstraintName("FK09_REDSGN");

                entity.HasOne(d => d.RedsgnDevStus)
                    .WithMany(p => p.RedsgnRedsgnDevStus)
                    .HasForeignKey(d => d.RedsgnDevStusId)
                    .HasConstraintName("FK05_REDSGN");

                entity.HasOne(d => d.RedsgnType)
                    .WithMany(p => p.Redsgn)
                    .HasForeignKey(d => d.RedsgnTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_REDSGN");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.RedsgnStus)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_REDSGN");
            });

            modelBuilder.Entity<RedsgnCustBypass>(entity =>
            {
                entity.ToTable("REDSGN_CUST_BYPASS", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.H1Cd)
                    .IsRequired()
                    .HasColumnName("H1_CD")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.HasOne(d => d.CsgLvl)
                    .WithMany(p => p.RedsgnCustBypass)
                    .HasForeignKey(d => d.CsgLvlId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_REDSGN_CUST_BYPASS");
            });

            modelBuilder.Entity<RedsgnDevEventHist>(entity =>
            {
                entity.ToTable("REDSGN_DEV_EVENT_HIST", "dbo");

                entity.Property(e => e.RedsgnDevEventHistId).HasColumnName("REDSGN_DEV_EVENT_HIST_ID");

                entity.Property(e => e.CmpltdDt)
                    .HasColumnName("CMPLTD_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.RedsgnDevId).HasColumnName("REDSGN_DEV_ID");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.RedsgnDevEventHist)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_REDSGN_DEV_EVENT_HIST");

                entity.HasOne(d => d.RedsgnDev)
                    .WithMany(p => p.RedsgnDevEventHist)
                    .HasForeignKey(d => d.RedsgnDevId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_REDSGN_DEV_EVENT_HIST");
            });

            modelBuilder.Entity<RedsgnDevicesInfo>(entity =>
            {
                entity.HasKey(e => e.RedsgnDevId);

                entity.ToTable("REDSGN_DEVICES_INFO", "dbo");

                entity.HasIndex(e => new { e.RedsgnId, e.DevNme })
                    .HasName("UX01_REDSGN_DEVICES_INFO")
                    .IsUnique();

                entity.Property(e => e.RedsgnDevId).HasColumnName("REDSGN_DEV_ID");

                entity.Property(e => e.CretdByCd).HasColumnName("CRETD_BY_CD");

                entity.Property(e => e.CretdDt)
                    .HasColumnName("CRETD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.DevBillDt)
                    .HasColumnName("DEV_BILL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.DevCmpltnCd).HasColumnName("DEV_CMPLTN_CD");

                entity.Property(e => e.DevNme)
                    .HasColumnName("DEV_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DspchRdyCd).HasColumnName("DSPCH_RDY_CD");

                entity.Property(e => e.EventCmpltnDt)
                    .HasColumnName("EVENT_CMPLTN_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FastTrkCd).HasColumnName("FAST_TRK_CD");

                entity.Property(e => e.H6CustId)
                    .HasColumnName("H6_CUST_ID")
                    .HasMaxLength(9);

                entity.Property(e => e.ModfdByCd).HasColumnName("MODFD_BY_CD");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NteChrgCd).HasColumnName("NTE_CHRG_CD");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.RedsgnId).HasColumnName("REDSGN_ID");

                entity.Property(e => e.ScCd)
                    .HasColumnName("SC_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SeqNbr).HasColumnName("SEQ_NBR");

                entity.HasOne(d => d.CretdByCdNavigation)
                    .WithMany(p => p.RedsgnDevicesInfoCretdByCdNavigation)
                    .HasForeignKey(d => d.CretdByCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_REDSGN_DEVICES_INFO");

                entity.HasOne(d => d.ModfdByCdNavigation)
                    .WithMany(p => p.RedsgnDevicesInfoModfdByCdNavigation)
                    .HasForeignKey(d => d.ModfdByCd)
                    .HasConstraintName("FK04_REDSGN_DEVICES_INFO");

                entity.HasOne(d => d.Redsgn)
                    .WithMany(p => p.RedsgnDevicesInfo)
                    .HasForeignKey(d => d.RedsgnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_REDSGN_DEVICES_INFO");
            });

            modelBuilder.Entity<RedsgnDoc>(entity =>
            {
                entity.ToTable("REDSGN_DOC", "dbo");

                entity.Property(e => e.RedsgnDocId).HasColumnName("REDSGN_DOC_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FileCntnt)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FileSizeQty).HasColumnName("FILE_SIZE_QTY");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.RedsgnDevStusId).HasColumnName("REDSGN_DEV_STUS_ID");

                entity.Property(e => e.RedsgnId).HasColumnName("REDSGN_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.RedsgnDoc)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_REDSGN_DOC");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.RedsgnDoc)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_REDSGN_DOC");

                entity.HasOne(d => d.Redsgn)
                    .WithMany(p => p.RedsgnDoc)
                    .HasForeignKey(d => d.RedsgnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_REDSGN_DOC");
            });

            modelBuilder.Entity<RedsgnEmailNtfctn>(entity =>
            {
                entity.ToTable("REDSGN_EMAIL_NTFCTN", "dbo");

                entity.Property(e => e.RedsgnEmailNtfctnId).HasColumnName("REDSGN_EMAIL_NTFCTN_ID");

                entity.Property(e => e.CretdDt)
                    .HasColumnName("CRETD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CretdUserId).HasColumnName("CRETD_USER_ID");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.RedsgnId).HasColumnName("REDSGN_ID");

                entity.Property(e => e.UserIdntfctnEmail)
                    .IsRequired()
                    .HasColumnName("USER_IDNTFCTN_EMAIL")
                    .IsUnicode(false);

                entity.HasOne(d => d.CretdUser)
                    .WithMany(p => p.RedsgnEmailNtfctn)
                    .HasForeignKey(d => d.CretdUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_REDSGN_EMAIL_NTFCTN");

                entity.HasOne(d => d.Redsgn)
                    .WithMany(p => p.RedsgnEmailNtfctn)
                    .HasForeignKey(d => d.RedsgnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_REDSGN_EMAIL_NTFCTN");
            });

            modelBuilder.Entity<RedsgnH1MrcValues>(entity =>
            {
                entity.HasKey(e => new { e.H1Id, e.RedsgnCatId });

                entity.ToTable("REDSGN_H1_MRC_VALUES", "dbo");

                entity.Property(e => e.H1Id)
                    .HasColumnName("H1_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RedsgnCatId).HasColumnName("REDSGN_CAT_ID");

                entity.Property(e => e.MrcRateVsNrc)
                    .HasColumnName("MRC_RATE_VS_NRC")
                    .HasColumnType("decimal(5, 4)");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.HasOne(d => d.RedsgnCat)
                    .WithMany(p => p.RedsgnH1MrcValues)
                    .HasForeignKey(d => d.RedsgnCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_REDSGN_H1_MRC_VALUES");
            });

            modelBuilder.Entity<RedsgnNotes>(entity =>
            {
                entity.ToTable("REDSGN_NOTES", "dbo");

                entity.Property(e => e.RedsgnNotesId).HasColumnName("REDSGN_NOTES_ID");

                entity.Property(e => e.CretdByCd).HasColumnName("CRETD_BY_CD");

                entity.Property(e => e.CretdDt)
                    .HasColumnName("CRETD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Notes)
                    .HasColumnName("NOTES")
                    .IsUnicode(false);

                entity.Property(e => e.RedsgnId).HasColumnName("REDSGN_ID");

                entity.Property(e => e.RedsgnNteTypeId).HasColumnName("REDSGN_NTE_TYPE_ID");

                entity.HasOne(d => d.CretdByCdNavigation)
                    .WithMany(p => p.RedsgnNotes)
                    .HasForeignKey(d => d.CretdByCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_REDSGN_NOTES");

                entity.HasOne(d => d.Redsgn)
                    .WithMany(p => p.RedsgnNotes)
                    .HasForeignKey(d => d.RedsgnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_REDSGN_NOTES");

                entity.HasOne(d => d.RedsgnNteType)
                    .WithMany(p => p.RedsgnNotes)
                    .HasForeignKey(d => d.RedsgnNteTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_REDSGN_NOTES");
            });

            modelBuilder.Entity<RedsgnRecLock>(entity =>
            {
                entity.HasKey(e => e.RedsgnId);

                entity.ToTable("REDSGN_REC_LOCK", "dbo");

                entity.Property(e => e.RedsgnId)
                    .HasColumnName("REDSGN_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.LockByUserId).HasColumnName("LOCK_BY_USER_ID");

                entity.Property(e => e.StrtRecLockTmst)
                    .HasColumnName("STRT_REC_LOCK_TMST")
                    .HasColumnType("smalldatetime");

                entity.HasOne(d => d.Redsgn)
                    .WithOne(p => p.RedsgnRecLock)
                    .HasForeignKey<RedsgnRecLock>(d => d.RedsgnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_REDSGN_REC_LOCK");
            });

            modelBuilder.Entity<RedsgnWrkflw>(entity =>
            {
                entity.ToTable("REDSGN_WRKFLW", "dbo");

                entity.HasIndex(e => new { e.PreWrkflwStusId, e.DesrdWrkflwStusId })
                    .HasName("UX01_REDSGN_WRKFLW")
                    .IsUnique();

                entity.Property(e => e.RedsgnWrkflwId).HasColumnName("REDSGN_WRKFLW_ID");

                entity.Property(e => e.CretdDt)
                    .HasColumnName("CRETD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.DesrdWrkflwStusId).HasColumnName("DESRD_WRKFLW_STUS_ID");

                entity.Property(e => e.PreWrkflwStusId).HasColumnName("PRE_WRKFLW_STUS_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.DesrdWrkflwStus)
                    .WithMany(p => p.RedsgnWrkflwDesrdWrkflwStus)
                    .HasForeignKey(d => d.DesrdWrkflwStusId)
                    .HasConstraintName("FK02_REDSGN_WRKFLW");

                entity.HasOne(d => d.PreWrkflwStus)
                    .WithMany(p => p.RedsgnWrkflwPreWrkflwStus)
                    .HasForeignKey(d => d.PreWrkflwStusId)
                    .HasConstraintName("FK01_REDSGN_WRKFLW");
            });

            modelBuilder.Entity<ReltdEvent>(entity =>
            {
                entity.ToTable("RELTD_EVENT", "dbo");

                entity.HasIndex(e => new { e.ReltdEventId, e.EventId, e.EventTypeId })
                    .HasName("UX01_RELTD_EVENT")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.EventTypeId).HasColumnName("EVENT_TYPE_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ReltdEventId).HasColumnName("RELTD_EVENT_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.ReltdEventCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_RELTD_EVENT");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.ReltdEventEvent)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_RELTD_EVENT");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.ReltdEvent)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_RELTD_EVENT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.ReltdEventModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_RELTD_EVENT");

                entity.HasOne(d => d.ReltdEventNavigation)
                    .WithMany(p => p.ReltdEventReltdEventNavigation)
                    .HasForeignKey(d => d.ReltdEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_CAND_RELTD_EVENT");
            });

            modelBuilder.Entity<SdeOpptnty>(entity =>
            {
                entity.ToTable("SDE_OPPTNTY", "dbo");

                entity.Property(e => e.SdeOpptntyId).HasColumnName("SDE_OPPTNTY_ID");

                entity.Property(e => e.AsnToUserId).HasColumnName("ASN_TO_USER_ID");

                entity.Property(e => e.CcAdr)
                    .HasColumnName("CC_ADR")
                    .IsUnicode(false);

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MngdSrvcCustCd).HasColumnName("MNGD_SRVC_CUST_CD");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RfpCd).HasColumnName("RFP_CD");

                entity.Property(e => e.Rgn)
                    .IsRequired()
                    .HasColumnName("RGN")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SaleType)
                    .IsRequired()
                    .HasColumnName("SALE_TYPE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SdeStusId).HasColumnName("SDE_STUS_ID");

                entity.Property(e => e.WrlneCustCd).HasColumnName("WRLNE_CUST_CD");

                entity.HasOne(d => d.AsnToUser)
                    .WithMany(p => p.SdeOpptntyAsnToUser)
                    .HasForeignKey(d => d.AsnToUserId)
                    .HasConstraintName("FK05_SDE_OPPTNTY");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.SdeOpptntyCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SDE_OPPTNTY");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.SdeOpptntyModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK02_SDE_OPPTNTY");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.SdeOpptnty)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_SDE_OPPTNTY");

                entity.HasOne(d => d.SdeStus)
                    .WithMany(p => p.SdeOpptnty)
                    .HasForeignKey(d => d.SdeStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SDE_OPPTNTY");
            });

            modelBuilder.Entity<SdeOpptntyDoc>(entity =>
            {
                entity.ToTable("SDE_OPPTNTY_DOC", "dbo");

                entity.Property(e => e.SdeOpptntyDocId).HasColumnName("SDE_OPPTNTY_DOC_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileCntnt)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.FileSizeQty).HasColumnName("FILE_SIZE_QTY");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SdeOpptntyId).HasColumnName("SDE_OPPTNTY_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.SdeOpptntyDoc)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SDE_OPPTNTY_DOC");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.SdeOpptntyDoc)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SDE_OPPTNTY_DOC");

                entity.HasOne(d => d.SdeOpptnty)
                    .WithMany(p => p.SdeOpptntyDoc)
                    .HasForeignKey(d => d.SdeOpptntyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SDE_OPPTNTY_DOC");
            });

            modelBuilder.Entity<SdeOpptntyNte>(entity =>
            {
                entity.ToTable("SDE_OPPTNTY_NTE", "dbo");

                entity.Property(e => e.SdeOpptntyNteId).HasColumnName("SDE_OPPTNTY_NTE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SdeOpptntyId).HasColumnName("SDE_OPPTNTY_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.SdeOpptntyNte)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SDE_OPPTNTY_NTE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.SdeOpptntyNte)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SDE_OPPTNTY_NTE");

                entity.HasOne(d => d.SdeOpptnty)
                    .WithMany(p => p.SdeOpptntyNte)
                    .HasForeignKey(d => d.SdeOpptntyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SDE_OPPTNTY_NTE");
            });

            modelBuilder.Entity<SdeOpptntyPrdct>(entity =>
            {
                entity.ToTable("SDE_OPPTNTY_PRDCT", "dbo");

                entity.Property(e => e.SdeOpptntyPrdctId).HasColumnName("SDE_OPPTNTY_PRDCT_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Qty).HasColumnName("QTY");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SdeOpptntyId).HasColumnName("SDE_OPPTNTY_ID");

                entity.Property(e => e.SdePrdctTypeId).HasColumnName("SDE_PRDCT_TYPE_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.SdeOpptntyPrdct)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SDE_OPPTNTY_PRDCT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.SdeOpptntyPrdct)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_SDE_OPPTNTY_PRDCT");

                entity.HasOne(d => d.SdeOpptnty)
                    .WithMany(p => p.SdeOpptntyPrdct)
                    .HasForeignKey(d => d.SdeOpptntyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SDE_OPPTNTY_PRDCT");

                entity.HasOne(d => d.SdePrdctType)
                    .WithMany(p => p.SdeOpptntyPrdct)
                    .HasForeignKey(d => d.SdePrdctTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SDE_OPPTNTY_PRDCT");
            });

            modelBuilder.Entity<SipTrnkGrp>(entity =>
            {
                entity.HasKey(e => new { e.OrdrId, e.CmpntId, e.IsInstlCd });

                entity.ToTable("SIP_TRNK_GRP", "dbo");

                entity.HasIndex(e => e.SipTrnkGrpId)
                    .HasName("UX01_SIP_TRNK_GRP")
                    .IsUnique();

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.CmpntId).HasColumnName("CMPNT_ID");

                entity.Property(e => e.IsInstlCd)
                    .HasColumnName("IS_INSTL_CD")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.NwUserAdr)
                    .HasColumnName("NW_USER_ADR")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SipTrnkGrpId)
                    .HasColumnName("SIP_TRNK_GRP_ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SipTrnkQty).HasColumnName("SIP_TRNK_QTY");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.SipTrnkGrp)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SIP_TRNK_GRP");
            });

            modelBuilder.Entity<SiptEvent>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("SIPT_EVENT", "dbo");

                entity.HasIndex(e => e.CreatDt)
                    .HasName("IX03_SIPT_EVENT");

                entity.HasIndex(e => e.EventStusId)
                    .HasName("IX02_SIPT_EVENT");

                entity.HasIndex(e => e.M5OrdrNbr)
                    .HasName("IDX_SIPT_EVENT_01");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryRgnNme)
                    .HasColumnName("CTRY_RGN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CustReqEndDt)
                    .HasColumnName("CUST_REQ_END_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CustReqStDt)
                    .HasColumnName("CUST_REQ_ST_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt)
                    .HasColumnName("EVENT_TITLE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.FlrBldgNme)
                    .HasColumnName("FLR_BLDG_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FocDt)
                    .HasColumnName("FOC_DT")
                    .HasColumnType("date");

                entity.Property(e => e.H1)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.H6)
                    .IsRequired()
                    .HasMaxLength(9);

                entity.Property(e => e.M5OrdrNbr)
                    .HasColumnName("M5_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.NtwkEngrId).HasColumnName("NTWK_ENGR_ID");

                entity.Property(e => e.NtwkTechEngrId).HasColumnName("NTWK_TECH_ENGR_ID");

                entity.Property(e => e.PmId).HasColumnName("PM_ID");

                entity.Property(e => e.PrjId)
                    .HasColumnName("PRJ_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReqorUserId).HasColumnName("REQOR_USER_ID");

                entity.Property(e => e.SiptDesgnDoc)
                    .HasColumnName("SIPT_DESGN_DOC")
                    .IsUnicode(false);

                entity.Property(e => e.SiptDocLocTxt)
                    .HasColumnName("SIPT_DOC_LOC_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.SiptProdTypeId)
                    .HasColumnName("SIPT_PROD_TYPE_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SiteCntctEmailAdr)
                    .HasColumnName("SITE_CNTCT_EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SiteCntctHrNme)
                    .HasColumnName("SITE_CNTCT_HR_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SiteCntctNme)
                    .HasColumnName("SITE_CNTCT_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SiteCntctPhnNbr)
                    .HasColumnName("SITE_CNTCT_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .IsRequired()
                    .HasColumnName("SITE_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAdr)
                    .HasColumnName("STREET_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SttPrvnNme)
                    .HasColumnName("STT_PRVN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TeamPdlNme)
                    .HasColumnName("TEAM_PDL_NME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TnsTgCd).HasColumnName("TNS_TG_CD");

                entity.Property(e => e.UsIntlCd)
                    .HasColumnName("US_INTL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.WrkDes)
                    .HasColumnName("WRK_DES")
                    .IsUnicode(false);

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.Property(e => e.ZipCd)
                    .HasColumnName("ZIP_CD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.SiptEventCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_SIPT_EVENT");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.SiptEvent)
                    .HasForeignKey<SiptEvent>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SIPT_EVENT");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.SiptEvent)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SIPT_EVENT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.SiptEventModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_SIPT_EVENT");

                entity.HasOne(d => d.NtwkEngr)
                    .WithMany(p => p.SiptEventNtwkEngr)
                    .HasForeignKey(d => d.NtwkEngrId)
                    .HasConstraintName("FK06_SIPT_EVENT");

                entity.HasOne(d => d.NtwkTechEngr)
                    .WithMany(p => p.SiptEventNtwkTechEngr)
                    .HasForeignKey(d => d.NtwkTechEngrId)
                    .HasConstraintName("FK07_SIPT_EVENT");

                entity.HasOne(d => d.Pm)
                    .WithMany(p => p.SiptEventPm)
                    .HasForeignKey(d => d.PmId)
                    .HasConstraintName("FK08_SIPT_EVENT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.SiptEvent)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK09_SIPT_EVENT");

                entity.HasOne(d => d.SiptProdType)
                    .WithMany(p => p.SiptEvent)
                    .HasForeignKey(d => d.SiptProdTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK10_SIPT_EVENT");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.SiptEvent)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SIPT_EVENT");
            });

            modelBuilder.Entity<SiptEventActy>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.SiptActyTypeId });

                entity.ToTable("SIPT_EVENT_ACTY", "dbo");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.SiptActyTypeId).HasColumnName("SIPT_ACTY_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.SiptEventActy)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SIPT_EVENT_ACTY");

                entity.HasOne(d => d.SiptActyType)
                    .WithMany(p => p.SiptEventActy)
                    .HasForeignKey(d => d.SiptActyTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SIPT_EVENT_ACTY");
            });

            modelBuilder.Entity<SiptEventDoc>(entity =>
            {
                entity.ToTable("SIPT_EVENT_DOC", "dbo");

                entity.Property(e => e.SiptEventDocId).HasColumnName("SIPT_EVENT_DOC_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.FileCntnt)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.FileSizeQty).HasColumnName("FILE_SIZE_QTY");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.SiptEventDoc)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SIPT_EVENT_DOC");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.SiptEventDoc)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SIPT_EVENT_DOC");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.SiptEventDoc)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SIPT_EVENT_DOC");
            });

            modelBuilder.Entity<SiptEventTollType>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.SiptTollTypeId });

                entity.ToTable("SIPT_EVENT_TOLL_TYPE", "dbo");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.SiptTollTypeId).HasColumnName("SIPT_TOLL_TYPE_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.SiptEventTollType)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SIPT_EVENT_TOLL_TYPE");

                entity.HasOne(d => d.SiptTollType)
                    .WithMany(p => p.SiptEventTollType)
                    .HasForeignKey(d => d.SiptTollTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SIPT_EVENT_TOLL_TYPE");
            });

            modelBuilder.Entity<SiptReltdOrdr>(entity =>
            {
                entity.ToTable("SIPT_RELTD_ORDR", "dbo");

                entity.HasIndex(e => new { e.M5OrdrNbr, e.EventId })
                    .HasName("UX01_SIPT_RELTD_ORDR")
                    .IsUnique();

                entity.Property(e => e.SiptReltdOrdrId).HasColumnName("SIPT_RELTD_ORDR_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.M5OrdrNbr)
                    .IsRequired()
                    .HasColumnName("M5_ORDR_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.SiptReltdOrdr)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SIPT_RELTD_ORDR");
            });

            modelBuilder.Entity<Sm>(entity =>
            {
                entity.HasKey(e => new { e.PreReqstWgPtrnId, e.DesrdWgPtrnId, e.SmId });

                entity.ToTable("SM", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_SM");

                entity.Property(e => e.PreReqstWgPtrnId).HasColumnName("PRE_REQST_WG_PTRN_ID");

                entity.Property(e => e.DesrdWgPtrnId).HasColumnName("DESRD_WG_PTRN_ID");

                entity.Property(e => e.SmId).HasColumnName("SM_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.Sm)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SM");
            });

            modelBuilder.Entity<SplkEvent>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("SPLK_EVENT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX10_SPLK_EVENT");

                entity.HasIndex(e => e.EsclReasId)
                    .HasName("IX07_SPLK_EVENT");

                entity.HasIndex(e => e.EventStusId)
                    .HasName("IX01_SPLK_EVENT");

                entity.HasIndex(e => e.Ftn)
                    .HasName("IX12_SPLK_EVENT_FTN");

                entity.HasIndex(e => e.IpVerId)
                    .HasName("IX06_SPLK_EVENT");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX11_SPLK_EVENT");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX09_SPLK_EVENT");

                entity.HasIndex(e => e.ReqorUserId)
                    .HasName("IX02_SPLK_EVENT");

                entity.HasIndex(e => e.SalsUserId)
                    .HasName("IX03_SPLK_EVENT");

                entity.HasIndex(e => e.SplkActyTypeId)
                    .HasName("IX05_SPLK_EVENT");

                entity.HasIndex(e => e.SplkEventTypeId)
                    .HasName("IX04_SPLK_EVENT");

                entity.HasIndex(e => e.WrkflwStusId)
                    .HasName("IX08_SPLK_EVENT");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltdEmailCcTxt)
                    .HasColumnName("CMPLTD_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcBrdgNbr)
                    .HasColumnName("CNFRC_BRDG_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcPinNbr)
                    .HasColumnName("CNFRC_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustCntctCellPhnNbr)
                    .HasColumnName("CUST_CNTCT_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctNme)
                    .HasColumnName("CUST_CNTCT_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPgrNbr)
                    .HasColumnName("CUST_CNTCT_PGR_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPgrPinNbr)
                    .HasColumnName("CUST_CNTCT_PGR_PIN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntctPhnNbr)
                    .HasColumnName("CUST_CNTCT_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustEmailAdr)
                    .HasColumnName("CUST_EMAIL_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.DsgnCmntTxt)
                    .IsRequired()
                    .HasColumnName("DSGN_CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EsclCd).HasColumnName("ESCL_CD");

                entity.Property(e => e.EsclReasId).HasColumnName("ESCL_REAS_ID");

                entity.Property(e => e.EventDes)
                    .HasColumnName("EVENT_DES")
                    .IsUnicode(false);

                entity.Property(e => e.EventDrtnInMinQty).HasColumnName("EVENT_DRTN_IN_MIN_QTY");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt)
                    .HasColumnName("EVENT_TITLE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraDrtnTmeAmt).HasColumnName("EXTRA_DRTN_TME_AMT");

                entity.Property(e => e.Ftn)
                    .HasColumnName("FTN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.H1)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.H6).HasMaxLength(9);

                entity.Property(e => e.IpVerId).HasColumnName("IP_VER_ID");

                entity.Property(e => e.MdsMngdCd).HasColumnName("MDS_MNGD_CD");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PrimReqDt)
                    .HasColumnName("PRIM_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PubEmailCcTxt)
                    .HasColumnName("PUB_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReltdCmpsNcrCd).HasColumnName("RELTD_CMPS_NCR_CD");

                entity.Property(e => e.ReltdCmpsNcrNme)
                    .HasColumnName("RELTD_CMPS_NCR_NME")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ReqorUserId).HasColumnName("REQOR_USER_ID");

                entity.Property(e => e.SalsUserId).HasColumnName("SALS_USER_ID");

                entity.Property(e => e.ScndyReqDt)
                    .HasColumnName("SCNDY_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SowsEventId).HasColumnName("SOWS_EVENT_ID");

                entity.Property(e => e.SplkActyTypeId).HasColumnName("SPLK_ACTY_TYPE_ID");

                entity.Property(e => e.SplkEventTypeId).HasColumnName("SPLK_EVENT_TYPE_ID");

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.SplkEventCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_SPLK_EVENT");

                entity.HasOne(d => d.EsclReas)
                    .WithMany(p => p.SplkEvent)
                    .HasForeignKey(d => d.EsclReasId)
                    .HasConstraintName("FK12_SPLK_EVENT");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.SplkEvent)
                    .HasForeignKey<SplkEvent>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK08_SPLK_EVENT");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.SplkEvent)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SPLK_EVENT");

                entity.HasOne(d => d.IpVer)
                    .WithMany(p => p.SplkEvent)
                    .HasForeignKey(d => d.IpVerId)
                    .HasConstraintName("FK09_SPLK_EVENT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.SplkEventModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK07_SPLK_EVENT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.SplkEvent)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SPLK_EVENT");

                entity.HasOne(d => d.ReqorUser)
                    .WithMany(p => p.SplkEventReqorUser)
                    .HasForeignKey(d => d.ReqorUserId)
                    .HasConstraintName("FK06_SPLK_EVENT");

                entity.HasOne(d => d.SalsUser)
                    .WithMany(p => p.SplkEventSalsUser)
                    .HasForeignKey(d => d.SalsUserId)
                    .HasConstraintName("FK05_SPLK_EVENT");

                entity.HasOne(d => d.SplkActyType)
                    .WithMany(p => p.SplkEvent)
                    .HasForeignKey(d => d.SplkActyTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK11_SPLK_EVENT");

                entity.HasOne(d => d.SplkEventType)
                    .WithMany(p => p.SplkEvent)
                    .HasForeignKey(d => d.SplkEventTypeId)
                    .HasConstraintName("FK10_SPLK_EVENT");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.SplkEvent)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .HasConstraintName("FK01_SPLK_EVENT");
            });

            modelBuilder.Entity<SplkEventAccs>(entity =>
            {
                entity.ToTable("SPLK_EVENT_ACCS", "dbo");

                entity.HasIndex(e => new { e.EventId, e.OldPlNbr, e.OldPortSpeedDes, e.NewPlNbr, e.NewPortSpeedDes })
                    .HasName("UX01_SPLK_EVENT_ACCS")
                    .IsUnique();

                entity.Property(e => e.SplkEventAccsId).HasColumnName("SPLK_EVENT_ACCS_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.NewPlNbr)
                    .HasColumnName("NEW_PL_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewPortSpeedDes)
                    .HasColumnName("NEW_PORT_SPEED_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewVpiVciDlciNme)
                    .HasColumnName("NEW_VPI_VCI_DLCI_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldPlNbr)
                    .HasColumnName("OLD_PL_NBR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldPortSpeedDes)
                    .HasColumnName("OLD_PORT_SPEED_DES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldVpiVciDlciNme)
                    .HasColumnName("OLD_VPI_VCI_DLCI_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.SplkEventAccs)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SPLK_EVENT_ACCS");
            });

            modelBuilder.Entity<SrcCurFile>(entity =>
            {
                entity.ToTable("SRC_CUR_FILE", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_SRC_CUR_FILE");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_SRC_CUR_FILE");

                entity.Property(e => e.SrcCurFileId).HasColumnName("SRC_CUR_FILE_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileCntntBtsm)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT_BTSM");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrcsdDt)
                    .HasColumnName("PRCSD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PrsdCurListTxt)
                    .IsRequired()
                    .HasColumnName("PRSD_CUR_LIST_TXT")
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.SrcCurFile)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_SRC_CUR_FILE");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.SrcCurFile)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_SRC_CUR_FILE");
            });

            modelBuilder.Entity<SstatReq>(entity =>
            {
                entity.HasKey(e => e.TranId);

                entity.ToTable("SSTAT_REQ", "dbo");

                entity.Property(e => e.TranId).HasColumnName("TRAN_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailReqId).HasColumnName("EMAIL_REQ_ID");

                entity.Property(e => e.Ftn)
                    .HasColumnName("FTN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.SstatMsgId).HasColumnName("SSTAT_MSG_ID");

                entity.Property(e => e.StusId)
                    .HasColumnName("STUS_ID")
                    .HasDefaultValueSql("((10))");

                entity.Property(e => e.StusModDt)
                    .HasColumnName("STUS_MOD_DT")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.SstatReq)
                    .HasForeignKey(d => d.OrdrId)
                    .HasConstraintName("FK02_SSTAT_REQ");

                entity.HasOne(d => d.Stus)
                    .WithMany(p => p.SstatReq)
                    .HasForeignKey(d => d.StusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_SSTAT_REQ");
            });

            modelBuilder.Entity<TrptOrdr>(entity =>
            {
                entity.HasKey(e => e.OrdrId);

                entity.ToTable("TRPT_ORDR", "dbo");

                entity.HasIndex(e => e.AEndRgnId)
                    .HasName("IX09_TRPT_ORDR");

                entity.HasIndex(e => e.AccsCtySiteId)
                    .HasName("IX12_TRPT_ORDR");

                entity.HasIndex(e => e.CcdMissdReasId)
                    .HasName("IX06_TRPT_ORDR");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_TRPT_ORDR");

                entity.HasIndex(e => e.CustBillCurId)
                    .HasName("IX08_TRPT_ORDR");

                entity.HasIndex(e => e.IptAvlbltyId)
                    .HasName("IX07_TRPT_ORDR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_TRPT_ORDR");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX03_TRPT_ORDR");

                entity.HasIndex(e => e.SalsChnlId)
                    .HasName("IX04_TRPT_ORDR");

                entity.HasIndex(e => e.VndrLocalAccsPrvdrGrpNme)
                    .HasName("IX11_TRPT_ORDR");

                entity.HasIndex(e => e.VndrTypeId)
                    .HasName("IX10_TRPT_ORDR");

                entity.HasIndex(e => e.XnciAsnMgrId)
                    .HasName("IX05_TRPT_ORDR");

                entity.Property(e => e.OrdrId)
                    .HasColumnName("ORDR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AEndNewCktCd).HasColumnName("A_END_NEW_CKT_CD");

                entity.Property(e => e.AEndRegion)
                    .HasColumnName("A_END_REGION")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AEndRgnId).HasColumnName("A_END_RGN_ID");

                entity.Property(e => e.AEndSpclCustAccsDetlTxt)
                    .HasColumnName("A_END_SPCL_CUST_ACCS_DETL_TXT")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.AccsCtySiteId).HasColumnName("ACCS_CTY_SITE_ID");

                entity.Property(e => e.AccsInMbValuQty).HasColumnName("ACCS_IN_MB_VALU_QTY");

                entity.Property(e => e.AccsMbDes)
                    .HasColumnName("ACCS_MB_DES")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BEndNewCktCd).HasColumnName("B_END_NEW_CKT_CD");

                entity.Property(e => e.BEndSpclCustAccsDetlTxt)
                    .HasColumnName("B_END_SPCL_CUST_ACCS_DETL_TXT")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.BillClearDt)
                    .HasColumnName("BILL_CLEAR_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CcdMissdReasId).HasColumnName("CCD_MISSD_REAS_ID");

                entity.Property(e => e.CktLgthInKmsQty).HasColumnName("CKT_LGTH_IN_KMS_QTY");

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CnfrmRnlDt)
                    .HasColumnName("CNFRM_RNL_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustAcptdDt)
                    .HasColumnName("CUST_ACPTD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CustBillCurId)
                    .HasColumnName("CUST_BILL_CUR_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CustCntrcXpirDt)
                    .HasColumnName("CUST_CNTRC_XPIR_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.IptAvlbltyId).HasColumnName("IPT_AVLBLTY_ID");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NrmUpdtCd)
                    .HasColumnName("NRM_UPDT_CD")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.OrdrAsnVwDt)
                    .HasColumnName("ORDR_ASN_VW_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SalsChnlId).HasColumnName("SALS_CHNL_ID");

                entity.Property(e => e.StdiDt)
                    .HasColumnName("STDI_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.StdiReasId).HasColumnName("STDI_REAS_ID");

                entity.Property(e => e.TotHoldDrtnTmeTxt)
                    .HasColumnName("TOT_HOLD_DRTN_TME_TXT")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.VndrLocalAccsPrvdrGrpNme)
                    .HasColumnName("VNDR_LOCAL_ACCS_PRVDR_GRP_NME")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VndrOrdrId)
                    .HasColumnName("VNDR_ORDR_ID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VndrTypeId).HasColumnName("VNDR_TYPE_ID");

                entity.Property(e => e.XnciAsnMgrId).HasColumnName("XNCI_ASN_MGR_ID");

                entity.HasOne(d => d.AEndRgn)
                    .WithMany(p => p.TrptOrdr)
                    .HasForeignKey(d => d.AEndRgnId)
                    .HasConstraintName("FK05_TRPT_ORDR");

                entity.HasOne(d => d.AccsCtySite)
                    .WithMany(p => p.TrptOrdr)
                    .HasForeignKey(d => d.AccsCtySiteId)
                    .HasConstraintName("FK13_TRPT_ORDR");

                entity.HasOne(d => d.CcdMissdReas)
                    .WithMany(p => p.TrptOrdr)
                    .HasForeignKey(d => d.CcdMissdReasId)
                    .HasConstraintName("FK01_TRPT_ORDR");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.TrptOrdrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK10_TRPT_ORDR");

                entity.HasOne(d => d.CustBillCur)
                    .WithMany(p => p.TrptOrdr)
                    .HasForeignKey(d => d.CustBillCurId)
                    .HasConstraintName("FK02_TRPT_ORDR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.TrptOrdrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK11_TRPT_ORDR");

                entity.HasOne(d => d.Ordr)
                    .WithOne(p => p.TrptOrdr)
                    .HasForeignKey<TrptOrdr>(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK09_TRPT_ORDR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.TrptOrdr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_TRPT_ORDR");

                entity.HasOne(d => d.StdiReas)
                    .WithMany(p => p.TrptOrdr)
                    .HasForeignKey(d => d.StdiReasId)
                    .HasConstraintName("FK14_TRPT_ORDR");

                entity.HasOne(d => d.XnciAsnMgr)
                    .WithMany(p => p.TrptOrdrXnciAsnMgr)
                    .HasForeignKey(d => d.XnciAsnMgrId)
                    .HasConstraintName("FK07_TRPT_ORDR");
            });

            modelBuilder.Entity<UcaaSEvent>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("UCaaS_EVENT", "dbo");

                entity.HasIndex(e => e.CreatDt)
                    .HasName("IX02_UCaaS_EVENT");

                entity.HasIndex(e => e.EventStusId)
                    .HasName("IX01_UCaaS_EVENT");

                entity.Property(e => e.EventId)
                    .HasColumnName("EVENT_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BusJustnTxt)
                    .HasColumnName("BUS_JUSTN_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Ccd)
                    .HasColumnName("CCD")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CharsId)
                    .HasColumnName("CHARS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .IsUnicode(false);

                entity.Property(e => e.CmpltdEmailCcTxt)
                    .HasColumnName("CMPLTD_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcBrdgId).HasColumnName("CNFRC_BRDG_ID");

                entity.Property(e => e.CnfrcBrdgNbr)
                    .HasColumnName("CNFRC_BRDG_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CnfrcPinNbr)
                    .HasColumnName("CNFRC_PIN_NBR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CpeDspchCmntTxt)
                    .HasColumnName("CPE_DSPCH_CMNT_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CpeDspchEmailAdr)
                    .HasColumnName("CPE_DSPCH_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryRgnNme)
                    .HasColumnName("CTRY_RGN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustAcctTeamPdlNme)
                    .HasColumnName("CUST_ACCT_TEAM_PDL_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustNme)
                    .HasColumnName("CUST_NME")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CustSowLocTxt)
                    .IsRequired()
                    .HasColumnName("CUST_SOW_LOC_TXT")
                    .HasMaxLength(1000);

                entity.Property(e => e.DiscMgmtCd)
                    .HasColumnName("DISC_MGMT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EndTmst)
                    .HasColumnName("END_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EsclCd).HasColumnName("ESCL_CD");

                entity.Property(e => e.EsclReasId).HasColumnName("ESCL_REAS_ID");

                entity.Property(e => e.EventDrtnInMinQty).HasColumnName("EVENT_DRTN_IN_MIN_QTY");

                entity.Property(e => e.EventStusId).HasColumnName("EVENT_STUS_ID");

                entity.Property(e => e.EventTitleTxt)
                    .HasColumnName("EVENT_TITLE_TXT")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraDrtnTmeAmt).HasColumnName("EXTRA_DRTN_TME_AMT");

                entity.Property(e => e.FailReasId).HasColumnName("FAIL_REAS_ID");

                entity.Property(e => e.FlrBldgNme)
                    .HasColumnName("FLR_BLDG_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FullCustDiscCd).HasColumnName("FULL_CUST_DISC_CD");

                entity.Property(e => e.FullCustDiscReasTxt)
                    .HasColumnName("FULL_CUST_DISC_REAS_TXT")
                    .HasMaxLength(2500)
                    .IsUnicode(false);

                entity.Property(e => e.H1)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.H6).HasMaxLength(9);

                entity.Property(e => e.InstlSitePocCellPhnNbr)
                    .HasColumnName("INSTL_SITE_POC_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocIntlCellPhnCd)
                    .HasColumnName("INSTL_SITE_POC_INTL_CELL_PHN_CD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocIntlPhnCd)
                    .HasColumnName("INSTL_SITE_POC_INTL_PHN_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocNme)
                    .HasColumnName("INSTL_SITE_POC_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.InstlSitePocPhnNbr)
                    .HasColumnName("INSTL_SITE_POC_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MnsPmId)
                    .HasColumnName("MNS_PM_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.OldEventStusId).HasColumnName("OLD_EVENT_STUS_ID");

                entity.Property(e => e.OnlineMeetingAdr)
                    .HasColumnName("ONLINE_MEETING_ADR")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.PrimReqDt)
                    .HasColumnName("PRIM_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PubEmailCcTxt)
                    .HasColumnName("PUB_EMAIL_CC_TXT")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReqorUserCellPhnNbr)
                    .HasColumnName("REQOR_USER_CELL_PHN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqorUserId).HasColumnName("REQOR_USER_ID");

                entity.Property(e => e.ScndyReqDt)
                    .HasColumnName("SCNDY_REQ_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ShrtDes)
                    .HasColumnName("SHRT_DES")
                    .HasMaxLength(2500)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasColumnName("SITE_ID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SprintCpeNcrId).HasColumnName("SPRINT_CPE_NCR_ID");

                entity.Property(e => e.SrvcAssrnPocCellPhnNbr)
                    .HasColumnName("SRVC_ASSRN_POC_CELL_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnPocIntlCellPhnCd)
                    .HasColumnName("SRVC_ASSRN_POC_INTL_CELL_PHN_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnPocIntlPhnCd)
                    .HasColumnName("SRVC_ASSRN_POC_INTL_PHN_CD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnPocNme)
                    .HasColumnName("SRVC_ASSRN_POC_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SrvcAssrnPocPhnNbr)
                    .HasColumnName("SRVC_ASSRN_POC_PHN_NBR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAdr)
                    .HasColumnName("STREET_ADR")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StrtTmst)
                    .HasColumnName("STRT_TMST")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SttPrvnNme)
                    .HasColumnName("STT_PRVN_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UcaaSActyTypeId).HasColumnName("UCaaS_ACTY_TYPE_ID");

                entity.Property(e => e.UcaaSDesgnDoc)
                    .HasColumnName("UCaaS_DESGN_DOC")
                    .IsUnicode(false);

                entity.Property(e => e.UcaaSPlanTypeId).HasColumnName("UCaaS_PLAN_TYPE_ID");

                entity.Property(e => e.UcaaSProdTypeId).HasColumnName("UCaaS_PROD_TYPE_ID");

                entity.Property(e => e.UsIntlCd)
                    .HasColumnName("US_INTL_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.WrkflwStusId).HasColumnName("WRKFLW_STUS_ID");

                entity.Property(e => e.ZipCd)
                    .HasColumnName("ZIP_CD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.CnfrcBrdg)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.CnfrcBrdgId)
                    .HasConstraintName("FK15_UCaaS_EVENT");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.UcaaSEventCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK11_UCaaS_EVENT");

                entity.HasOne(d => d.EsclReas)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.EsclReasId)
                    .HasConstraintName("FK08_UCaaS_EVENT");

                entity.HasOne(d => d.Event)
                    .WithOne(p => p.UcaaSEvent)
                    .HasForeignKey<UcaaSEvent>(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_UCaaS_EVENT");

                entity.HasOne(d => d.EventStus)
                    .WithMany(p => p.UcaaSEventEventStus)
                    .HasForeignKey(d => d.EventStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_UCaaS_EVENT");

                entity.HasOne(d => d.FailReas)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.FailReasId)
                    .HasConstraintName("FK14_UCaaS_EVENT");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.UcaaSEventModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK12_UCaaS_EVENT");

                entity.HasOne(d => d.OldEventStus)
                    .WithMany(p => p.UcaaSEventOldEventStus)
                    .HasForeignKey(d => d.OldEventStusId)
                    .HasConstraintName("FK10_UCaaS_EVENT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK13_UCaaS_EVENT");

                entity.HasOne(d => d.ReqorUser)
                    .WithMany(p => p.UcaaSEventReqorUser)
                    .HasForeignKey(d => d.ReqorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK09_UCaaS_EVENT");

                entity.HasOne(d => d.SprintCpeNcr)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.SprintCpeNcrId)
                    .HasConstraintName("FK07_UCaaS_EVENT");

                entity.HasOne(d => d.UcaaSActyType)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.UcaaSActyTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_UCaaS_EVENT");

                entity.HasOne(d => d.UcaaSPlanType)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.UcaaSPlanTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_UCaaS_EVENT");

                entity.HasOne(d => d.UcaaSProdType)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.UcaaSProdTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_UCaaS_EVENT");

                entity.HasOne(d => d.WrkflwStus)
                    .WithMany(p => p.UcaaSEvent)
                    .HasForeignKey(d => d.WrkflwStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_UCaaS_EVENT");
            });

            modelBuilder.Entity<UcaaSEventBilling>(entity =>
            {
                entity.ToTable("UCaaS_EVENT_BILLING", "dbo");

                entity.Property(e => e.UcaaSEventBillingId).HasColumnName("UCaaS_EVENT_BILLING_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DcrQty).HasColumnName("DCR_QTY");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.IncQty).HasColumnName("INC_QTY");

                entity.Property(e => e.UcaaSBillActyId).HasColumnName("UCaaS_BILL_ACTY_ID");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.UcaaSEventBilling)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_UCaaS_EVENT_BILLING");

                entity.HasOne(d => d.UcaaSBillActy)
                    .WithMany(p => p.UcaaSEventBilling)
                    .HasForeignKey(d => d.UcaaSBillActyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_UCaaS_EVENT_BILLING");
            });

            modelBuilder.Entity<UcaaSEventOdieDev>(entity =>
            {
                entity.ToTable("UCaaS_EVENT_ODIE_DEV", "dbo");

                entity.Property(e => e.UcaaSEventOdieDevId).HasColumnName("UCaaS_EVENT_ODIE_DEV_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DevModelId).HasColumnName("DEV_MODEL_ID");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.ManfId).HasColumnName("MANF_ID");

                entity.Property(e => e.OdieDevNme)
                    .IsRequired()
                    .HasColumnName("ODIE_DEV_NME")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RdsnExpDt)
                    .HasColumnName("RDSN_EXP_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RdsnNbr)
                    .IsRequired()
                    .HasColumnName("RDSN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.DevModel)
                    .WithMany(p => p.UcaaSEventOdieDev)
                    .HasForeignKey(d => d.DevModelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_UCaaS_EVENT_ODIE_DEV");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.UcaaSEventOdieDev)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_UCaaS_EVENT_ODIE_DEV");

                entity.HasOne(d => d.Manf)
                    .WithMany(p => p.UcaaSEventOdieDev)
                    .HasForeignKey(d => d.ManfId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_UCaaS_EVENT_ODIE_DEV");
            });

            modelBuilder.Entity<UserCpeTech>(entity =>
            {
                entity.ToTable("USER_CPE_TECH", "dbo");

                entity.Property(e => e.UserCpeTechId).HasColumnName("USER_CPE_TECH_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.UserCpeTechAdid)
                    .IsRequired()
                    .HasColumnName("USER_CPE_TECH_ADID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserCpeTechNme)
                    .IsRequired()
                    .HasColumnName("USER_CPE_TECH_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserCsgLvl>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.CsgLvlId });

                entity.ToTable("USER_CSG_LVL", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_USER_CSG_LVL");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_USER_CSG_LVL");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX01_USER_CSG_LVL");

                entity.Property(e => e.UserId).HasColumnName("USER_ID");

                entity.Property(e => e.CsgLvlId).HasColumnName("CSG_LVL_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmplActCd)
                    .IsRequired()
                    .HasColumnName("EMPL_ACT_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.UserCsgLvlCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_USER_CSG_LVL");

                entity.HasOne(d => d.CsgLvl)
                    .WithMany(p => p.UserCsgLvl)
                    .HasForeignKey(d => d.CsgLvlId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_USER_CSG_LVL");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.UserCsgLvlModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK04_USER_CSG_LVL");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.UserCsgLvl)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_USER_CSG_LVL");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserCsgLvlUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_USER_CSG_LVL");
            });

            modelBuilder.Entity<UserH5Wfm>(entity =>
            {
                entity.HasKey(e => e.UserH5Id)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("USER_H5_WFM", "dbo");

                entity.Property(e => e.UserH5Id).HasColumnName("USER_H5_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustId).HasColumnName("CUST_ID");

                entity.Property(e => e.IsipCd)
                    .HasColumnName("ISIP_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MnsCd)
                    .HasColumnName("MNS_CD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId).HasColumnName("REC_STUS_ID");

                entity.Property(e => e.SegNme)
                    .HasColumnName("SEG_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("USER_ID");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.UserH5Wfm)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_USER_H5_WFM");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserH5Wfm)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_USER_H5_WFM");
            });

            modelBuilder.Entity<UserWfm>(entity =>
            {
                entity.HasKey(e => e.WfmId);

                entity.ToTable("USER_WFM", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX13_USER_WFM");

                entity.HasIndex(e => e.GrpId)
                    .HasName("IX02_USER_WFM");

                entity.HasIndex(e => e.IplTrmtgCtryCd)
                    .HasName("IX08_USER_WFM");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX12_USER_WFM");

                entity.HasIndex(e => e.OrdrActnId)
                    .HasName("IX10_USER_WFM");

                entity.HasIndex(e => e.OrdrTypeId)
                    .HasName("IX04_USER_WFM");

                entity.HasIndex(e => e.OrgtngCtryCd)
                    .HasName("IX09_USER_WFM");

                entity.HasIndex(e => e.PltfrmCd)
                    .HasName("IX06_USER_WFM");

                entity.HasIndex(e => e.ProdTypeId)
                    .HasName("IX05_USER_WFM");

                entity.HasIndex(e => e.RecStusId)
                    .HasName("IX11_USER_WFM");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IX03_USER_WFM");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX01_USER_WFM");

                entity.HasIndex(e => e.VndrCd)
                    .HasName("IX07_USER_WFM");

                entity.Property(e => e.WfmId).HasColumnName("WFM_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GrpId).HasColumnName("GRP_ID");

                entity.Property(e => e.IplTrmtgCtryCd)
                    .HasColumnName("IPL_TRMTG_CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrActnId).HasColumnName("ORDR_ACTN_ID");

                entity.Property(e => e.OrdrTypeId).HasColumnName("ORDR_TYPE_ID");

                entity.Property(e => e.OrgtngCtryCd)
                    .HasColumnName("ORGTNG_CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PltfrmCd)
                    .HasColumnName("PLTFRM_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ProdTypeId).HasColumnName("PROD_TYPE_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RoleId).HasColumnName("ROLE_ID");

                entity.Property(e => e.UserId).HasColumnName("USER_ID");

                entity.Property(e => e.UsrPrfId).HasColumnName("USR_PRF_ID");

                entity.Property(e => e.VndrCd)
                    .HasColumnName("VNDR_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.WfmAsmtLvlId).HasColumnName("WFM_ASMT_LVL_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.UserWfmCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_USER_WFM");

                entity.HasOne(d => d.Grp)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.GrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK11_USER_WFM");

                entity.HasOne(d => d.IplTrmtgCtryCdNavigation)
                    .WithMany(p => p.UserWfmIplTrmtgCtryCdNavigation)
                    .HasForeignKey(d => d.IplTrmtgCtryCd)
                    .HasConstraintName("FK12_USER_WFM");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.UserWfmModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK03_USER_WFM");

                entity.HasOne(d => d.OrdrActn)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.OrdrActnId)
                    .HasConstraintName("FK10_USER_WFM");

                entity.HasOne(d => d.OrdrType)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.OrdrTypeId)
                    .HasConstraintName("FK09_USER_WFM");

                entity.HasOne(d => d.OrgtngCtryCdNavigation)
                    .WithMany(p => p.UserWfmOrgtngCtryCdNavigation)
                    .HasForeignKey(d => d.OrgtngCtryCd)
                    .HasConstraintName("FK13_USER_WFM");

                entity.HasOne(d => d.PltfrmCdNavigation)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.PltfrmCd)
                    .HasConstraintName("FK08_USER_WFM");

                entity.HasOne(d => d.ProdType)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.ProdTypeId)
                    .HasConstraintName("FK07_USER_WFM");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_USER_WFM");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK05_USER_WFM");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserWfmUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_USER_WFM");

                entity.HasOne(d => d.UsrPrf)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.UsrPrfId)
                    .HasConstraintName("FK14_USER_WFM");

                entity.HasOne(d => d.VndrCdNavigation)
                    .WithMany(p => p.UserWfm)
                    .HasForeignKey(d => d.VndrCd)
                    .HasConstraintName("FK01_USER_WFM");
            });

            modelBuilder.Entity<UserWfmAsmt>(entity =>
            {
                entity.HasKey(e => new { e.OrdrId, e.GrpId, e.AsnUserId });

                entity.ToTable("USER_WFM_ASMT", "dbo");

                entity.HasIndex(e => e.AsnByUserId)
                    .HasName("IX02_USER_WFM_ASMT");

                entity.HasIndex(e => e.AsnUserId)
                    .HasName("IX01_USER_WFM_ASMT");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.GrpId).HasColumnName("GRP_ID");

                entity.Property(e => e.AsnUserId).HasColumnName("ASN_USER_ID");

                entity.Property(e => e.AsmtDt)
                    .HasColumnName("ASMT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.AsnByUserId).HasColumnName("ASN_BY_USER_ID");

                entity.Property(e => e.CmpltTm)
                    .HasColumnName("CMPLT_TM")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DsptchTm)
                    .HasColumnName("DSPTCH_TM")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventId).HasColumnName("EVENT_ID");

                entity.Property(e => e.InrteTm)
                    .HasColumnName("INRTE_TM")
                    .HasColumnType("datetime");

                entity.Property(e => e.OnsiteTm)
                    .HasColumnName("ONSITE_TM")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrdrHighlightCd)
                    .HasColumnName("ORDR_HIGHLIGHT_CD")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.OrdrHighlightModfdDt)
                    .HasColumnName("ORDR_HIGHLIGHT_MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.UsrPrfId).HasColumnName("USR_PRF_ID");

                entity.HasOne(d => d.AsnByUser)
                    .WithMany(p => p.UserWfmAsmtAsnByUser)
                    .HasForeignKey(d => d.AsnByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_USER_WFM_ASMT");

                entity.HasOne(d => d.AsnUser)
                    .WithMany(p => p.UserWfmAsmtAsnUser)
                    .HasForeignKey(d => d.AsnUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_USER_WFM_ASMT");

                entity.HasOne(d => d.Grp)
                    .WithMany(p => p.UserWfmAsmt)
                    .HasForeignKey(d => d.GrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_USER_WFM_ASMT");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.UserWfmAsmt)
                    .HasForeignKey(d => d.OrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_USER_WFM_ASMT");

                entity.HasOne(d => d.UsrPrf)
                    .WithMany(p => p.UserWfmAsmt)
                    .HasForeignKey(d => d.UsrPrfId)
                    .HasConstraintName("FK05_USER_WFM_ASMT");
            });

            modelBuilder.Entity<VndrFoldr>(entity =>
            {
                entity.ToTable("VNDR_FOLDR", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX02_VNDR_FOLDR");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX03_VNDR_FOLDR");

                entity.HasIndex(e => e.SttCd)
                    .HasName("IX01_VNDR_FOLDR");

                entity.HasIndex(e => new { e.VndrCd, e.CtryCd })
                    .HasName("UX01_VNDR_FOLDR")
                    .IsUnique();

                entity.Property(e => e.VndrFoldrId).HasColumnName("VNDR_FOLDR_ID");

                entity.Property(e => e.BldgNme)
                    .HasColumnName("BLDG_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CmntTxt)
                    .HasColumnName("CMNT_TXT")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CtryCd)
                    .HasColumnName("CTRY_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CtyNme)
                    .HasColumnName("CTY_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FlrId)
                    .HasColumnName("FLR_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PrvnNme)
                    .HasColumnName("PRVN_NME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.RmNbr)
                    .HasColumnName("RM_NBR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAdr1)
                    .HasColumnName("STREET_ADR_1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAdr2)
                    .HasColumnName("STREET_ADR_2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SttCd)
                    .HasColumnName("STT_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.VndrCd)
                    .IsRequired()
                    .HasColumnName("VNDR_CD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VndrEmailListTxt)
                    .HasColumnName("VNDR_EMAIL_LIST_TXT")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ZipPstlCd)
                    .HasColumnName("ZIP_PSTL_CD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.VndrFoldrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_VNDR_FOLDR");

                entity.HasOne(d => d.CtryCdNavigation)
                    .WithMany(p => p.VndrFoldr)
                    .HasForeignKey(d => d.CtryCd)
                    .HasConstraintName("FK01_VNDR_FOLDR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.VndrFoldrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_VNDR_FOLDR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.VndrFoldr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_VNDR_FOLDR");

                entity.HasOne(d => d.SttCdNavigation)
                    .WithMany(p => p.VndrFoldr)
                    .HasPrincipalKey(p => p.UsStateId)
                    .HasForeignKey(d => d.SttCd)
                    .HasConstraintName("FK03_VNDR_FOLDR");

                entity.HasOne(d => d.VndrCdNavigation)
                    .WithMany(p => p.VndrFoldr)
                    .HasForeignKey(d => d.VndrCd)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_VNDR_FOLDR");
            });

            modelBuilder.Entity<VndrFoldrCntct>(entity =>
            {
                entity.HasKey(e => e.CntctId);

                entity.ToTable("VNDR_FOLDR_CNTCT", "dbo");

                entity.HasIndex(e => e.VndrFoldrId)
                    .HasName("IX01_VNDR_FOLDR_CNTCT");

                entity.Property(e => e.CntctId).HasColumnName("CNTCT_ID");

                entity.Property(e => e.CntctEmailAdr)
                    .HasColumnName("CNTCT_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CntctFrstNme)
                    .HasColumnName("CNTCT_FRST_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CntctLstNme)
                    .HasColumnName("CNTCT_LST_NME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CntctPhnNbr)
                    .HasColumnName("CNTCT_PHN_NBR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CntctProdRoleTxt)
                    .HasColumnName("CNTCT_PROD_ROLE_TXT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CntctProdTypeTxt)
                    .HasColumnName("CNTCT_PROD_TYPE_TXT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.VndrFoldrId).HasColumnName("VNDR_FOLDR_ID");

                entity.HasOne(d => d.VndrFoldr)
                    .WithMany(p => p.VndrFoldrCntct)
                    .HasForeignKey(d => d.VndrFoldrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_VNDR_FOLDR_CNTCT");
            });

            modelBuilder.Entity<VndrOrdr>(entity =>
            {
                entity.ToTable("VNDR_ORDR", "dbo");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX04_VNDR_ORDR");

                entity.HasIndex(e => e.OrdrId)
                    .HasName("IX01_VNDR_ORDR");

                entity.HasIndex(e => e.VndrFoldrId)
                    .HasName("IX02_VNDR_ORDR");

                entity.HasIndex(e => e.VndrOrdrTypeId)
                    .HasName("IX05_VNDR_ORDR");

                entity.HasIndex(e => new { e.CreatByUserId, e.ModfdByUserId })
                    .HasName("IX03_VNDR_ORDR");

                entity.Property(e => e.VndrOrdrId)
                    .HasColumnName("VNDR_ORDR_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BypasVndrOrdrMsCd)
                    .HasColumnName("BYPAS_VNDR_ORDR_MS_CD")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.PrevOrdrId).HasColumnName("PREV_ORDR_ID");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TrmtgCd).HasColumnName("TRMTG_CD");

                entity.Property(e => e.VndrFoldrId).HasColumnName("VNDR_FOLDR_ID");

                entity.Property(e => e.VndrOrdrTypeId).HasColumnName("VNDR_ORDR_TYPE_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.VndrOrdrCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_VNDR_ORDR");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.VndrOrdrModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_VNDR_ORDR");

                entity.HasOne(d => d.Ordr)
                    .WithMany(p => p.VndrOrdrOrdr)
                    .HasForeignKey(d => d.OrdrId)
                    .HasConstraintName("FK02_VNDR_ORDR");

                entity.HasOne(d => d.PrevOrdr)
                    .WithMany(p => p.VndrOrdrPrevOrdr)
                    .HasForeignKey(d => d.PrevOrdrId)
                    .HasConstraintName("FK08_VNDR_ORDR");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.VndrOrdr)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_VNDR_ORDR");

                entity.HasOne(d => d.VndrFoldr)
                    .WithMany(p => p.VndrOrdr)
                    .HasForeignKey(d => d.VndrFoldrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_VNDR_ORDR");

                entity.HasOne(d => d.VndrOrdrNavigation)
                    .WithOne(p => p.VndrOrdrVndrOrdrNavigation)
                    .HasForeignKey<VndrOrdr>(d => d.VndrOrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK06_VNDR_ORDR");

                entity.HasOne(d => d.VndrOrdrType)
                    .WithMany(p => p.VndrOrdr)
                    .HasForeignKey(d => d.VndrOrdrTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK07_VNDR_ORDR");
            });

            modelBuilder.Entity<VndrOrdrEmail>(entity =>
            {
                entity.ToTable("VNDR_ORDR_EMAIL", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_VNDR_ORDR_EMAIL");

                entity.HasIndex(e => e.ModfdByUserId)
                    .HasName("IX02_VNDR_ORDR_EMAIL");

                entity.HasIndex(e => e.VndrEmailTypeId)
                    .HasName("IX03_VNDR_ORDR_EMAIL");

                entity.HasIndex(e => new { e.VndrOrdrId, e.VerNbr })
                    .HasName("UX01_VNDR_ORDR_EMAIL")
                    .IsUnique();

                entity.Property(e => e.VndrOrdrEmailId).HasColumnName("VNDR_ORDR_EMAIL_ID");

                entity.Property(e => e.CcEmailAdr)
                    .IsRequired()
                    .HasColumnName("CC_EMAIL_ADR")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailBody)
                    .HasColumnName("EMAIL_BODY")
                    .IsUnicode(false);

                entity.Property(e => e.EmailStusId).HasColumnName("EMAIL_STUS_ID");

                entity.Property(e => e.EmailSubjTxt)
                    .HasColumnName("EMAIL_SUBJ_TXT")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FromEmailAdr)
                    .IsRequired()
                    .HasColumnName("FROM_EMAIL_ADR")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModfdByUserId).HasColumnName("MODFD_BY_USER_ID");

                entity.Property(e => e.ModfdDt)
                    .HasColumnName("MODFD_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SentDt)
                    .HasColumnName("SENT_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.ToEmailAdr)
                    .HasColumnName("TO_EMAIL_ADR")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.VerNbr).HasColumnName("VER_NBR");

                entity.Property(e => e.VndrEmailTypeId).HasColumnName("VNDR_EMAIL_TYPE_ID");

                entity.Property(e => e.VndrOrdrId).HasColumnName("VNDR_ORDR_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.VndrOrdrEmailCreatByUser)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_VNDR_ORDR_EMAIL");

                entity.HasOne(d => d.EmailStus)
                    .WithMany(p => p.VndrOrdrEmail)
                    .HasForeignKey(d => d.EmailStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_VNDR_ORDR_EMAIL");

                entity.HasOne(d => d.ModfdByUser)
                    .WithMany(p => p.VndrOrdrEmailModfdByUser)
                    .HasForeignKey(d => d.ModfdByUserId)
                    .HasConstraintName("FK05_VNDR_ORDR_EMAIL");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.VndrOrdrEmail)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_VNDR_ORDR_EMAIL");

                entity.HasOne(d => d.VndrEmailType)
                    .WithMany(p => p.VndrOrdrEmail)
                    .HasForeignKey(d => d.VndrEmailTypeId)
                    .HasConstraintName("FK06_VNDR_ORDR_EMAIL");

                entity.HasOne(d => d.VndrOrdr)
                    .WithMany(p => p.VndrOrdrEmail)
                    .HasForeignKey(d => d.VndrOrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_VNDR_ORDR_EMAIL");
            });

            modelBuilder.Entity<VndrOrdrEmailAtchmt>(entity =>
            {
                entity.ToTable("VNDR_ORDR_EMAIL_ATCHMT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_VNDR_ORDR_EMAIL_ATCHMT");

                entity.HasIndex(e => new { e.VndrOrdrEmailId, e.FileNme })
                    .HasName("UX01_VNDR_ORDR_EMAIL_ATCHMT")
                    .IsUnique();

                entity.Property(e => e.VndrOrdrEmailAtchmtId).HasColumnName("VNDR_ORDR_EMAIL_ATCHMT_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileCntnt)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FileSizeQty).HasColumnName("FILE_SIZE_QTY");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.VndrOrdrEmailId).HasColumnName("VNDR_ORDR_EMAIL_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.VndrOrdrEmailAtchmt)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_VNDR_ORDR_EMAIL_ATCHMT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.VndrOrdrEmailAtchmt)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_VNDR_ORDR_EMAIL_ATCHMT");

                entity.HasOne(d => d.VndrOrdrEmail)
                    .WithMany(p => p.VndrOrdrEmailAtchmt)
                    .HasForeignKey(d => d.VndrOrdrEmailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_VNDR_ORDR_EMAIL_ATCHMT");
            });

            modelBuilder.Entity<VndrOrdrForm>(entity =>
            {
                entity.ToTable("VNDR_ORDR_FORM", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_VNDR_ORDR_FORM");

                entity.HasIndex(e => e.TmpltId)
                    .HasName("IX02_VNDR_ORDR_FORM");

                entity.Property(e => e.VndrOrdrFormId).HasColumnName("VNDR_ORDR_FORM_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileCntnt)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FileSizeQty).HasColumnName("FILE_SIZE_QTY");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TmpltCd).HasColumnName("TMPLT_CD");

                entity.Property(e => e.TmpltId).HasColumnName("TMPLT_ID");

                entity.Property(e => e.VndrOrdrId).HasColumnName("VNDR_ORDR_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.VndrOrdrForm)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_VNDR_ORDR_FORM");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.VndrOrdrForm)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_VNDR_ORDR_FORM");

                entity.HasOne(d => d.Tmplt)
                    .WithMany(p => p.InverseTmplt)
                    .HasForeignKey(d => d.TmpltId)
                    .HasConstraintName("FK04_VNDR_ORDR_FORM");

                entity.HasOne(d => d.VndrOrdr)
                    .WithMany(p => p.VndrOrdrForm)
                    .HasForeignKey(d => d.VndrOrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_VNDR_ORDR_FORM");
            });

            modelBuilder.Entity<VndrOrdrMs>(entity =>
            {
                entity.ToTable("VNDR_ORDR_MS", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_VNDR_ORDR_MS");

                entity.HasIndex(e => new { e.VndrOrdrId, e.VerId, e.VndrOrdrEmailId })
                    .HasName("UX01_VNDR_ORDR_MS")
                    .IsUnique();

                entity.Property(e => e.VndrOrdrMsId).HasColumnName("VNDR_ORDR_MS_ID");

                entity.Property(e => e.AckByVndrDt)
                    .HasColumnName("ACK_BY_VNDR_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SentToVndrDt)
                    .HasColumnName("SENT_TO_VNDR_DT")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.VerId).HasColumnName("VER_ID");

                entity.Property(e => e.VndrOrdrEmailId).HasColumnName("VNDR_ORDR_EMAIL_ID");

                entity.Property(e => e.VndrOrdrId).HasColumnName("VNDR_ORDR_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.VndrOrdrMs)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_VNDR_ORDR_MS");

                entity.HasOne(d => d.VndrOrdrEmail)
                    .WithMany(p => p.VndrOrdrMs)
                    .HasForeignKey(d => d.VndrOrdrEmailId)
                    .HasConstraintName("FK01_VNDR_ORDR_MS");

                entity.HasOne(d => d.VndrOrdr)
                    .WithMany(p => p.VndrOrdrMs)
                    .HasForeignKey(d => d.VndrOrdrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_VNDR_ORDR_MS");
            });

            modelBuilder.Entity<VndrTmplt>(entity =>
            {
                entity.ToTable("VNDR_TMPLT", "dbo");

                entity.HasIndex(e => e.CreatByUserId)
                    .HasName("IX01_VNDR_TMPLT");

                entity.HasIndex(e => new { e.VndrFoldrId, e.FileNme })
                    .HasName("UX01_VNDR_TMPLT")
                    .IsUnique();

                entity.Property(e => e.VndrTmpltId).HasColumnName("VNDR_TMPLT_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileCntnt)
                    .IsRequired()
                    .HasColumnName("FILE_CNTNT");

                entity.Property(e => e.FileNme)
                    .IsRequired()
                    .HasColumnName("FILE_NME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FileSizeQty).HasColumnName("FILE_SIZE_QTY");

                entity.Property(e => e.RecStusId)
                    .HasColumnName("REC_STUS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.VndrFoldrId).HasColumnName("VNDR_FOLDR_ID");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.VndrTmplt)
                    .HasForeignKey(d => d.CreatByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK02_VNDR_TMPLT");

                entity.HasOne(d => d.RecStus)
                    .WithMany(p => p.VndrTmplt)
                    .HasForeignKey(d => d.RecStusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_VNDR_TMPLT");

                entity.HasOne(d => d.VndrFoldr)
                    .WithMany(p => p.VndrTmplt)
                    .HasForeignKey(d => d.VndrFoldrId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK01_VNDR_TMPLT");
            });

            modelBuilder.Entity<WgProfSm>(entity =>
            {
                entity.HasKey(e => new { e.WgProfId, e.PreReqstTaskId, e.DesrdTaskId, e.OptId });

                entity.ToTable("WG_PROF_SM", "dbo");

                entity.Property(e => e.WgProfId).HasColumnName("WG_PROF_ID");

                entity.Property(e => e.PreReqstTaskId).HasColumnName("PRE_REQST_TASK_ID");

                entity.Property(e => e.DesrdTaskId).HasColumnName("DESRD_TASK_ID");

                entity.Property(e => e.OptId).HasColumnName("OPT_ID");

                entity.Property(e => e.CreatByUserId).HasColumnName("CREAT_BY_USER_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PreReqstTaskGrpReqrCd).HasColumnName("PRE_REQST_TASK_GRP_REQR_CD");

                entity.HasOne(d => d.CreatByUser)
                    .WithMany(p => p.WgProfSm)
                    .HasForeignKey(d => d.CreatByUserId)
                    .HasConstraintName("FK02_WG_PROF_SM");

                entity.HasOne(d => d.DesrdTask)
                    .WithMany(p => p.WgProfSmDesrdTask)
                    .HasForeignKey(d => d.DesrdTaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK04_WG_PROF_SM");

                entity.HasOne(d => d.PreReqstTask)
                    .WithMany(p => p.WgProfSmPreReqstTask)
                    .HasForeignKey(d => d.PreReqstTaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK03_WG_PROF_SM");
            });

            modelBuilder.Entity<WgPtrnSm>(entity =>
            {
                entity.HasKey(e => new { e.WgPtrnId, e.PreReqstWgProfId, e.DesrdWgProfId });

                entity.ToTable("WG_PTRN_SM", "dbo");

                entity.Property(e => e.WgPtrnId).HasColumnName("WG_PTRN_ID");

                entity.Property(e => e.PreReqstWgProfId).HasColumnName("PRE_REQST_WG_PROF_ID");

                entity.Property(e => e.DesrdWgProfId).HasColumnName("DESRD_WG_PROF_ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CREAT_DT")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");
            });
        }
    }
}
