﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class SiptReltdOrdr
    {
        public int SiptReltdOrdrId { get; set; }
        public int EventId { get; set; }
        public string M5OrdrNbr { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual Event Event { get; set; }
    }
}
