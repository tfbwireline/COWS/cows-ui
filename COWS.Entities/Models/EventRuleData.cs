﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class EventRuleData
    {
        public int Id { get; set; }
        public int EventRuleId { get; set; }
        public string EventDataCd { get; set; }
        public byte? EmailmemCd { get; set; }
        public byte? EmailrevCd { get; set; }
        public byte? EmailactCd { get; set; }
        public bool? EmailpubccCd { get; set; }
        public bool? EmailcmplccCd { get; set; }
        public bool? EmailoldactCd { get; set; }
        public string EmailSubj { get; set; }
        public string EmailccAdr { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public virtual LkEventRule EventRule { get; set; }
        public virtual LkRecStus RecStus { get; set; }
    }
}
