﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.Models
{
    public partial class CptUserAsmt
    {
        public int CptUserAsmtId { get; set; }
        public int CptId { get; set; }
        public int CptUserId { get; set; }
        public byte RoleId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime ModfdDt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public short? UsrPrfId { get; set; }

        public virtual Cpt Cpt { get; set; }
        public virtual LkUser CptUser { get; set; }
        public virtual LkUser CreatByUser { get; set; }
        public virtual LkUser ModfdByUser { get; set; }
        public virtual LkUsrPrf UsrPrf { get; set; }
    }
}
