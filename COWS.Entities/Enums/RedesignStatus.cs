﻿namespace COWS.Entities.Enums
{
    public enum RedesignStatus
    {
        Draft = 220,
        Submitted = 221,
        Reviewing = 222,
        Pending = 223,
        Approved = 225,
        Cancelled = 226,
        Completed = 227,
        Delete = 228,
        Cancellation_Reminder = 229
    }
}