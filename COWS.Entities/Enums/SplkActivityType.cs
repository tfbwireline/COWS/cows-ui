﻿namespace COWS.Entities.Enums
{
    public enum SplkActivityType
    {
        Conversion = 1,
        DLCIChange = 2,
        Downgrade = 3,
        PlugNPlayIssue = 4,
        Rehome = 5,
        Move = 6,
        Change = 7,
        Upgrade = 8
    }
}