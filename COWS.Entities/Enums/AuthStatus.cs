﻿namespace COWS.Entities.Enums
{
    public enum AuthStatus
    {
        Authorized = 1,
        UnAuthorized,
        NonSensitive
    }
}