﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.Enums
{
    public enum OdieCustInfoReqCatType
    {
        DataConverged = 1,
        VoiceConverged = 2,
        DataVoiceConverged = 3
    }
}
