﻿namespace COWS.Entities.Enums
{
    public enum CalendarEventType
    {
        ADB = 1,
        ADN = 2,
        ADG = 3,
        MDS = 4,
        MDSFT = 5,
        MPLS = 6,
        NGVN = 7,
        SLNK = 8,
        OnShift = 9,
        ADShift = 10,
        ATMShift = 11,
        MDSScheduledShift = 12,
        MDSFastTrackShift = 13,
        NGVNShift = 14,
        MPLSShift = 15,
        SprintLinkShift = 16,
        ADI = 28,
        MPLSVAS = 29,
        Fedline = 30,
        ADTMT = 32,
        UCaaSShift = 33,
        UCaaS = 34,
        SIPT = 35,
        NtwkIntl = 37
    }
}