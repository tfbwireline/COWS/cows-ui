﻿namespace COWS.Entities.Enums
{
    public class CustomerMaskedData
    {
        public const string customerName = "Private Customer";
        public const string customerEmailAddress = "Private@t-mobile.com";
        public const string address = "6200 Sprint Parkway";
        public const string floor = "";
        public const string city = "Overland Park";
        public const string state = "KS";
        public const string country = "USA";
        public const string zipCode = "662510000";
        public const string installSitePOC = "Private Customer";
        public const string installSitePOCIntlPhoneCd = "99";
        public const string installSitePOCPhone = "9999999999";
        public const string installSitePOCIntlCellCd = "99";
        public const string installSitePOCCell = "9999999999";
        public const string serviceAssurancePOC = "Private Customer";
        public const string serviceAssurancePOCIntlPhoneCd = "99";
        public const string serviceAssurancePOCPhone = "9999999999";
        public const string serviceAssurancePOCIntlCellCd = "99";
        public const string serviceAssurancePOCCell = "9999999999";
        public const string serviceAssurancePOCContactHrs = "AM-PM";
        public const string serviceAssurancePOCTimeZone = "ST";
        public const string blank = "";
    }
}