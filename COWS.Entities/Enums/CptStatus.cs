﻿namespace COWS.Entities.Enums
{
    public enum CptStatus
    {
        Submitted = 305,
        Provisioned = 306,
        Assigned = 307,
        Deleted = 308,
        Completed = 309,
        ReturnedToSDE = 310,
        Cancelled = 311,
        ReviewedByPM = 312
    }

    public enum CptProvisionStatus
    {
        PendingPickup = 301,
        Pickup = 302,
        Success = 303,
        Error = 304
    }
}
