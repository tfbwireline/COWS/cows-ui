﻿namespace COWS.Entities.Enums
{
    public enum EnhanceService
    {
        Standard = 1,
        VAS = 2,
        SLNKStandard = 3,
        NGVNStandard = 4,
        MDS = 5,
        FirewallSecurity = 6,
        ADBroadband = 7,
        ADNarrowband = 8,
        ADGovernment = 9,
        MSS = 10,
        ADInternational = 11,
        ADTMT = 12,
        UCaaS = 13
    }
}