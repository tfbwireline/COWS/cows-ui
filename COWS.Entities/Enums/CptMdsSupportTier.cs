﻿namespace COWS.Entities.Enums
{
    public enum CptMdsSupportTier
    {
        Design = 1,
        Implementation = 2,
        MonitorNotify = 3
    }
}
