﻿namespace COWS.Entities.Enums
{
    public enum CptPrimSite
    {
        FMNS = 1,
        IAAS,
        MNSD,
        MNSW,
        MVSC
    }
}
