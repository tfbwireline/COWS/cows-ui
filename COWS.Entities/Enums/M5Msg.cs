﻿namespace COWS.Entities.Enums
{
    public enum M5Msg
    {
        CPEComplete = 1,
        MNSComplete = 2,
        MSSComplete = 3,
        CPERTS = 4,
        CPEPO = 5,
        ACCESSComplete = 6
    }
}
