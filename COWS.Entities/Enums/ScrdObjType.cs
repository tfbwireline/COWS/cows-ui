﻿namespace COWS.Entities.Enums
{
    public enum ScrdObjType
    {
        AD_EVENT = 1,
        CPT,
        FSA_MDS_EVENT,
        FSA_MDS_EVENT_NEW,
        FSA_ORDR_CUST,
        H5_FOLDR,
        MDS_EVENT,
        MDS_EVENT_NEW,
        MPLS_EVENT,
        NCCO_ORDR,
        NGVN_EVENT,
        ODIE_REQ,
        ODIE_RSPN,
        ORDR_ADR,
        ORDR_CNTCT,
        REDSGN,
        REDSGN_CUST_BYPASS,
        SIPT_EVENT,
        SPLK_EVENT,
        UCaaS_EVENT,
        FEDLINE_EVENT_ADR,
        FEDLINE_EVENT_CNTCT,
        FEDLINE_EVENT_TADPOLE_DATA,
        FEDLINE_EVENT_TADPOLE_XML,
        FEDLINE_EVENT_USER_DATA,
        SDE_OPPTNTY,
        SDE_OPPTNTY_NTE,
        MPLS_EVENT_ACCS_TAG,
        NRM_CKT,
        MDS_EVENT_NTWK_CUST,
        MDS_EVENT_NTWK_TRPT
    }
}