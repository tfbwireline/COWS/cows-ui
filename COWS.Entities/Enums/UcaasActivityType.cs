﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.Enums
{
    public enum UcaasActivityType
    {
        Install = 1,
        Move,
        Disconnect,
        ChangeAddRemoveUser,
        ChangeUserFeatureGroupChange,
        ChangeProgrammingChange,
        ChangeOther,
        ChangeSPS,
    }
}
