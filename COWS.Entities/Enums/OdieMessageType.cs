﻿namespace COWS.Entities.Enums
{
    public enum OdieMessageType
    {
        CustomerNameRequest = 1,
        CustomerInfoRequest = 2,
        DesignComplete = 3,
        DisconnectResponse = 4,
        CPEInfo = 5,
        CPEStatus = 6,
        ShortNameValidationRequest = 7
    }
}