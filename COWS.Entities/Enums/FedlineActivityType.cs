﻿namespace COWS.Entities.Enums
{
    public enum FedlineActivityType
    {
        Install,
        Disconnect,
        HeadendInstall,
        HeadendMAC,
        HeadendDisconnect,
        HeadendCreate,
        Refresh,
    }
}