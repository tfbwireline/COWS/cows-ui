﻿namespace COWS.Entities.Enums
{
    public enum CSGLevel
    {
        CSGLevelNone = 0,
        CSGLevel1 = 1,
        CSGLevel2 = 2,
        CSGLevel3 = 3,
        CSGLevel4 = 4
    }
}