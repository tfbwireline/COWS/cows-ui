﻿namespace COWS.Entities.Enums
{
    public enum MplsActivityType
    {
        Cos = 1,
        Downgrade = 2,
        Dsl = 3,
        HotCut = 4,
        Migration = 6,
        Multipath = 7,
        ReHome = 8,
        Routing = 9,
        RoutingStatic = 10,
        SpecialProject = 11,
        TurnUp = 12,
        Upgrade = 13
    }
}