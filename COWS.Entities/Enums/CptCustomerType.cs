﻿namespace COWS.Entities.Enums
{
    public enum CptCustomerType
    {
        ManagedData = 1,
        ManagedSecurity = 2,
        ManagedVoice = 3,
        UnmanagedE2e = 4,
        Sps = 5,
    }
}
