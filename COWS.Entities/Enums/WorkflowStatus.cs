﻿namespace COWS.Entities.Enums
{
    public enum WorkflowStatus
    {
        Visible = 1,
        Submit = 2,
        Retract = 3,
        Publish = 4,
        Reschedule = 5,
        InProgress = 6,
        Complete = 7,
        Return = 8,
        Reject = 9,
        OnHold = 10,
        Delete = 11,
        Fulfilling = 12,
        Shipped = 13,
        PendingDeviceReturn = 14,
        CompletePendingUAT = 15,
        InprogressUAT = 16
    }
}