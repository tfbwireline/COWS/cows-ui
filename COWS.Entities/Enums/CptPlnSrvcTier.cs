﻿namespace COWS.Entities.Enums
{
    public enum CptPlnSrvcTier
    {
        Mss = 1,
        MdsComplete = 2,
        MdsSupport = 3,
        WholesaleCarrier = 4,
        WholesaleVar = 5
    }
}
