﻿namespace COWS.Entities.Enums
{
    public enum SecuredObjectType
    {
        AD_EVENT = 1,
        CPT = 2,
        FSA_MDS_EVENT,
        FSA_MDS_EVENT_NEW,
        FSA_ORDR_CUST,
        H5_FOLDR,
        MDS_EVENT = 7,
        MDS_EVENT_NEW = 8,
        MPLS_EVENT = 9,
        NCCO_ORDR = 10,
        NGVN_EVENT = 11,
        ODIE_REQ,
        ODIE_RSPN,
        ORDR_ADR,
        ORDR_CNTCT,
        REDSGN,
        REDSGN_CUST_BYPASS,
        SIPT_EVENT = 18,
        SPLK_EVENT = 19,
        UCaaS_EVENT = 20,
        FEDLINE_EVENT_ADR,
        FEDLINE_EVENT_CNTCT,
        FEDLINE_EVENT_TADPOLE_DATA,
        FEDLINE_EVENT_TADPOLE_XML,
        FEDLINE_EVENT_USER_DATA,
        SDE_OPPTNTY,
        SDE_OPPTNTY_NTE,
        MPLS_EVENT_ACCS_TAG,
        NRM_CKT,
        MDS_EVENT_NTWK_CUST,
        MDS_EVENT_NTWK_TRPT
    }

    /*

    SCRD_OBJ_TYPE_ID	SCRD_OBJ_TYPE
    1	                AD_EVENT
    2	                CPT
    3	                FSA_MDS_EVENT
    4	                FSA_MDS_EVENT_NEW
    5	                FSA_ORDR_CUST
    6	                H5_FOLDR
    7	                MDS_EVENT
    8	                MDS_EVENT_NEW
    9	                MPLS_EVENT
    10	                NCCO_ORDR
    11	                NGVN_EVENT
    12	                ODIE_REQ
    13	                ODIE_RSPN
    14	                ORDR_ADR
    15	                ORDR_CNTCT
    16	                REDSGN
    17	                REDSGN_CUST_BYPASS
    18	                SIPT_EVENT
    19	                SPLK_EVENT
    20	                UCaaS_EVENT
    21	                FEDLINE_EVENT_ADR
    22	                FEDLINE_EVENT_CNTCT
    23	                FEDLINE_EVENT_TADPOLE_DATA
    24	                FEDLINE_EVENT_TADPOLE_XML
    25	                FEDLINE_EVENT_USER_DATA
    26	                SDE_OPPTNTY
    27	                SDE_OPPTNTY_NTE
    28	                MPLS_EVENT_ACCS_TAG
    29	                NRM_CKT
    30	                MDS_EVENT_NTWK_CUST
    31	                MDS_EVENT_NTWK_TRPT

    */
}