﻿namespace COWS.Entities.Enums
{
    public enum EventStatus
    {
        Visible = 1,
        Pending = 2,
        Rework = 3,
        Published = 4,
        InProgress = 5,
        Completed = 6,
        OnHold = 7,
        Delete = 8,
        Fulfilling = 9,
        Shipped = 10,
        Cancelled = 13,
        PendingDeviceReturn = 16,
        Submitted = 17,
        CompletePendingUAT = 18,
        InProgressUAT = 19,
        OnHoldUAT = 20
    }
}