﻿namespace COWS.Entities.Enums
{
    public enum ERecStatus
    {
        InActive = 0,
        Active = 1,
        OnVacation = 2,
        DeleteHold = 3
    }
}