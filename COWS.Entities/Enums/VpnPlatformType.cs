﻿namespace COWS.Entities.Enums
{
    public enum VpnPlatformType
    {
        MplsNonVas = 1,
        MplsCiscoIpsec = 2,
        MplsJuniper = 3,
        MdsNonVas = 4,
        MdsVas = 5
    }
}