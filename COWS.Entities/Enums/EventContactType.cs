﻿namespace COWS.Entities.Enums
{
    public enum EventContactType
    {
        Phone,
        CellPhone,
        PhoneCD,
        CellPhoneCD,
        Email,
        PagerNumber,
        PagerPinNumber,
        ContactHrs,
        TimeZone
    }
}