﻿namespace COWS.Entities.Enums
{
    public enum RedesignCategory
    {
        Voice = 1,
        Data = 2,
        Security = 3
    }
}