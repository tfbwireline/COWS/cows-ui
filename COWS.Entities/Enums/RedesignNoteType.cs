﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.Enums
{
    public enum RedesignNoteType
    {
        Status = 1,
        Notes = 2,
        Caveats = 3,
        System = 4
    }
}
