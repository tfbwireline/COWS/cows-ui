﻿namespace COWS.Entities.Enums
{
    public enum MplsEventType
    {
        ScheduledImplementation = 1,
        VASScheduledPreConfiguration = 2,
        VASScheduledImplementation = 3
    }
}