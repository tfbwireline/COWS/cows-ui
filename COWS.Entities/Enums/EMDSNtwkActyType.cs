﻿namespace COWS.Entities.Enums
{
    public enum EMDSNtwkActyType
    {
        MDSOnly = 1,
        NetworkOnly,
        NetworkAndMDS
    }
}