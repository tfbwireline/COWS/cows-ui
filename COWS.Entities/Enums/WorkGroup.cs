﻿namespace COWS.Entities.Enums
{
    public enum WorkGroup
    {
        GOM = 114,//1
        MDS = 128,//2
        CSC = 58,//2
        RTS = 156,//4
        xNCIAmerica = 2,//5
        xNCIAsia = 16,//6
        xNCIEurope = 100,//7
        UCaaS = 170,
        DCPE = 86,//13
        //VCPE = 14,//14
        CPETech = 44,//15
        //DBB = 72//16

        OLDGOM = 1,
    }
}