﻿namespace COWS.Entities.Enums
{
    public enum VasType
    {
        FirewallInternet = 1,
        RemoteAccess = 2,
        Tunnels = 3,
        HalfTunnel = 4,
        RAS = 5,
        SIAFirewall = 6
    }
}