﻿namespace COWS.Entities.Enums
{
    public enum EventType
    {
        AD = 1,
        NGVN = 2,
        MPLS = 3,
        SprintLink = 4,
        MDS = 5,
        Fedline = 9,
        SIPT = 10,
        UCaaS = 19,
        All = 99
    }
}