﻿namespace COWS.Entities.Enums
{
    public enum VendorEmailTypes
    {
        VendorOrder = 1,
        CCD = 2,
        ACR = 3,
        VendOrdConfirmation = 4,
        VendOrdDiscConfirmation = 5,
        ASR = 6
    }

    public enum OrderProduct
    {
        IPLCanada = 1,
        IPLMexico = 2,
        OPLOffshore = 3,
        IPLBilateral = 4,
        IPLLeased = 5,
        IPLResale = 6,
        IPLE2E = 7,
        IPLICBH = 8,
        CPE = 9,
        DIAOnnet = 10,
        DIAOffnet = 11,
        GenericProduct = 12,
        DedicatedIP = 13,
        ManagedNetworkServices = 14,
        MPLSOnnet = 15,
        ManagedSecurityServices = 16,
        SLFROnnet = 17,
        SLFROffnet = 18,
        MPLSOffnet = 19
    }

    public enum OrderTypes
    {
        Install = 1,
        Upgrade = 2,
        Downgrade = 3,
        Move = 4,
        Change = 5,
        BillingChange = 6,
        Disconnect = 7,
        Cancel = 8
    }
}