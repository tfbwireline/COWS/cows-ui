﻿namespace COWS.Entities.Enums
{
    public enum SdeStatus
    {
        Unassigned = 501,
        Draft,
        Assigned,
        Win,
        Lost,
        Ongoing,
        Close,
    }
}