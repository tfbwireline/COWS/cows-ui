﻿namespace COWS.Entities.Enums
{
    public enum WebActyType
    {
        OrderSearch = 1,
        EventSearch = 2,
        RedesignSearch = 3,
        OrderDetails = 4,
        EventDetails = 5,
        RedesignDetails = 6,
        H5FolderSearch = 7,
        H5FolderDetails = 8,
        VendorOrderSearch = 9,
        VendorOrderDetails = 10,
        CPTSearch = 11,
        CPTDetails = 12
    }
}