﻿namespace COWS.Entities.Enums
{
    public enum CptProvision
    {
        Spectrum = 1,
        Voyence,
        Qip,
        Tacacs,
        Odie
    }

    public enum ProvisionStatus
    {
        PendingPickUp = 301,
        PickedUp,
        Success,
        Errored
    }
}
