﻿namespace COWS.Entities.Enums
{
    public enum CptTransportType
    {
        IPSprintLink = 1,
        GMPLS,
        PIP,
        SprintLinkFR,
        IPSecVPN,
        GMPLSBroadband,
        WirelessWANWDLS
    }
}
