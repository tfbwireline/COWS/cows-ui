﻿namespace COWS.Entities.Enums
{
    public enum EmailStatus
    {
        OnHold = 9,
        Pending = 10,
        Success = 11,
        Failure = 12,
        #region VENDOR EMAIL STATUS
        Draft = 13,
        SentUnenc = 14,
        ErrorUnenc = 15,
        SentEnc = 16,
        ErrorEnc = 17
        #endregion
    }
}