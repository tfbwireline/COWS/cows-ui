﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class MdsEventOdieDevView
    {
        public int MdsEventOdieDevId { get; set; }
        public int EventId { get; set; }
        public string RdsnNbr { get; set; }
        public DateTime RdsnExpDt { get; set; }
        public string OdieDevNme { get; set; }
        public short DevModelId { get; set; }
        public short ManfId { get; set; }
        public string FrwlProdCd { get; set; }
        public string IntlCtryCd { get; set; }
        public string PhnNbr { get; set; }
        public string FastTrkCd { get; set; }
        public string WoobCd { get; set; }
        public bool OptoutCd { get; set; }
        public byte? SrvcAssrnSiteSuppId { get; set; }
        public DateTime CreatDt { get; set; }
        public bool SysCd { get; set; }
        public string ScCd { get; set; }

        // Additional
        public string Vendor { get; set; }
    }
}