﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public class H5FolderView
    {
        public int H5FoldrId { get; set; }
        public int CustId { get; set; }
        public string CustCtyNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public string ModfdByUserAdId { get; set; }
        public string CreatByUserAdId { get; set; }
        public byte RecStusId { get; set; }
        public string CtryCd { get; set; }
        public byte CsgLvlId { get; set; }
        public string CustNme { get; set; }
        public int H5DocCnt { get; set; }

        public ICollection<H5Doc> H5Docs { get; set; }
    }
}
