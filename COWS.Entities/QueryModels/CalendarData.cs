﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class CalendarData
    {
        [Column("SUBJ_TXT")]
        public string SubjTxt { get; set; }

        [Column("DES")]
        public string Des { get; set; }

        [Column("STRT_TMST")]
        public DateTime StrtTmst { get; set; }

        [Column("END_TMST")]
        public DateTime EndTmst { get; set; }

        [Column("APPT_LOC_TXT")]
        public string ApptLocTxt { get; set; }

        [Column("RCURNC_DES_TXT")]
        public string RcurncDesTxt { get; set; }

        [Column("APPT_ID")]
        public int ApptId { get; set; }

        [Column("APPT_TYPE_ID")]
        public int ApptTypeId { get; set; }

        [Column("ASN_TO_USER_ID_LIST_TXT")]
        public string AsnToUserIdListTxt { get; set; }

        [Column("RCURNC_CD")]
        public byte RcurncCd { get; set; }

        [Column("CREAT_BY_USER_ID")]
        public int CreatByUserId { get; set; }

        [Column("MODFD_BY_USER_ID")]
        public int ModfdByUserId { get; set; }

        [Column("DEL_FLG")]
        public byte DelFlg { get; set; }
    }
}