﻿namespace COWS.Entities.QueryModels
{
    public class CptUser
    {
        public int UserId { get; set; }
        public string UserAdId { get; set; }
        public string FullName { get; set; }
        public string DisplayField { get; set; }
        public string Email { get; set; }
        public short UsrPrfId { get; set; }
        public string UsrPrfName { get; set; }
        public decimal Weighted { get; set; }   // For Pooled NTE and MNS PM (1.5 * tasks * devices) 
        public decimal CptQty { get; set; }     // (# of CPT in 30 days + # of devices) * 1.5
        public bool PoolCd { get; set; }
        public int Devices { get; set; }        // Total number of devices per user assigned
        public int Tasks { get; set; }          // Total number of CPT per user assigned
    }
}
