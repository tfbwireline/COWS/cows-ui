﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class GetOdieDiscoResponse
    {
        [Column("ODIE_DISC_REQ_ID")]
        public int OdieDiscReqId { get; set; }

        [Column("H5_H6_CUST_ID")]
        public int H5H6CustId { get; set; }

        [Column("MODEL_NME")]
        public string ModelName { get; set; }

        [Column("VNDR_NME")]
        public string VendorName { get; set; }

        [Column("DEV_MODEL_ID")]
        public Int16 DevModelId { get; set; }

        [Column("REDSGN_NBR")]
        public string RedesignNumber { get; set; }

        [Column("ODIE_DEV_NME")]
        public string OdieDevName { get; set; }

        [Column("MANF_ID")]
        public Int16 ManfId { get; set; }

        [Column("SERIAL_NBR")]
        public string SerialNumber { get; set; }

        [Column("M5_DEVICE_ID")]
        public string M5DeviceId { get; set; }

        [Column("SITE_ID")]
        public string SiteId { get; set; }

        [Column("CTRCT_NBR")]
        public string ContractNumber { get; set; }

        [Column("OPT_IN_H_CD")]
        public string OptInHCd { get; set; }

        [Column("OPT_IN_CKT_CD")]
        public string OptInCktCd { get; set; }

        [Column("READY_BEGIN_FLG_NME")]
        public string ReadyBeginFlagName { get; set; }
    }
}