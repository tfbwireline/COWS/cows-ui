﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public partial class AdvancedSearchEvent
    {
      
        [Column("Event ID")]
        public string EventID { get; set; }
        [Column("Event Type")]
        public string EventType { get; set; }
        [Column("Event Status")]
        public string EventStatus { get; set; }
        [Column("FTN")]
        public string FTN { get; set; }
        [Column("Assigned To")]
        public string AssignedTo { get; set; }
        [Column("Requested By")]
        public string RequestedBy { get; set; }
        [Column("Start Date")]
        public DateTime? StartDate { get; set; }
        [Column("Modified Date")]
        public DateTime? ModifiedDate { get; set; }

        [Column("MDS_Type")]
        public string MDS_Type { get; set; }
        [Column("CSG_LVL_ID")]
        public byte CSG_LVL_ID { get; set; }
        [Column("H5 H6")]
        public string H5_H6 { get; set; }
        [Column("H1")]
        public string H1 { get; set; }
        [Column("Customer Name")]
        public string CustomerName { get; set; }
        [Column("City")]
        public string City { get; set; }
        [Column("State")]
        public string State { get; set; }
        [Column("Device Name")]
        public string DesignName { get; set; }
        [Column("Redesign Number")]
        public string RedesignNumber { get; set; }



        // public string SOWSIds { get; set; }
        // //public string ADType { get; set; }
        // public string FMSCkt { get; set; }
        // //public string ActionType { get; set; }
        // public string FRBID { get; set; }
        // public string Serial { get; set; }
        //// public string DesignType { get; set; }
        // public string OrgID { get; set; }
        // //public string ActType { get; set; }
        // //public string Status { get; set; }
        // //public string apptStartOp { get; set; }
        // //public string  apptStartOption { get; set; }
        // //public string apptEndOp { get; set; }
        // //public string apptEndOption { get; set; }
        // //public string MDSActivityType { get; set; }
        // //public int REQ_BY_USR_ID { get; set; }
        // //public string RptSchedule { get; set; }
        // //public string AdhocEmailChk { get; set; }


    }
}
