﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class MdsEventDevSrvcMgmtView
    {
        public int EventDevSrvcMgmtId { get; set; }
        public int EventId { get; set; }
        public string DeviceId { get; set; }
        public byte? MnsSrvcTierId { get; set; }
        public string OdieDevNme { get; set; }
        public DateTime CreatDt { get; set; }
        public string MnsOrdrNbr { get; set; }
        public bool? OdieCd { get; set; }
        public string CmpntStus { get; set; }
        public string CmpntNme { get; set; }
        public string CmpntTypeCd { get; set; }
        public string M5OrdrCmpntId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }

        // Additional
        public string MnsSrvcTier { get; set; }
        public string RltdCmpntID { get; set; }
        public string AccsCarrier { get; set; }
        public string AssocTrptType { get; set; }
        public int MNSOrdrID { get; set; }
    }
}