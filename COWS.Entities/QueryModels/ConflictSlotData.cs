﻿using System;

namespace COWS.Entities.QueryModels
{
    public class ConflictSlotData
    {
        public ConflictSlotData(string uid, DateTime bsdt, DateTime bedt, DateTime wsdt, DateTime wedt, DateTime asdt, DateTime aedt)
        {
            UserID = uid;
            _BusyStartDt = bsdt;
            _BusyEndDt = bedt;
            _WFStartDt = wsdt;
            _WFEndDt = wedt;
            _ActStartDt = asdt;
            _ActEndDt = aedt;
        }

        #region "Private Members"

        private string _UserID;
        private DateTime _BusyStartDt;
        private DateTime _BusyEndDt;
        private DateTime _WFStartDt;
        private DateTime _WFEndDt;
        private DateTime _ActStartDt;
        private DateTime _ActEndDt;

        #endregion "Private Members"

        #region "Public Properties"

        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        public DateTime BusyStartDt
        {
            get { return _BusyStartDt; }
            set { _BusyStartDt = value; }
        }

        public DateTime BusyEndDt
        {
            get { return _BusyEndDt; }
            set { _BusyEndDt = value; }
        }

        public DateTime WFStartDt
        {
            get { return _WFStartDt; }
            set { _WFStartDt = value; }
        }

        public DateTime WFEndDt
        {
            get { return _WFEndDt; }
            set { _WFEndDt = value; }
        }

        public DateTime ActStartDt
        {
            get { return _ActStartDt; }
            set { _ActStartDt = value; }
        }

        public DateTime ActEndDt
        {
            get { return _ActEndDt; }
            set { _ActEndDt = value; }
        }

        #endregion "Public Properties"
    }
}