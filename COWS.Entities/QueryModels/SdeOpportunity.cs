﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public class SdeOpportunity : EntityBaseModel
    {
        public int SDEOpportunityID { get; set; }

        public int StatusID { get; set; }
        public string Status { get; set; }
        public string StatusDesc { get; set; }
        public string CompanyName { get; set; }

        public string Address { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        public string ContactName { get; set; }

        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public String SaleType { get; set; }
        public string SaleTypeDesc { get; set; }
        public bool IsRFP { get; set; }
        public bool IsWirelineCustomer { get; set; }

        public bool IsManagedServicesCustomer { get; set; }
        public string Comments { get; set; }
        public string RequestorLocation { get; set; }
        public string AssignedTo { get; set; }
        public int AssignedToId { get; set; }
        public string CcEmail { get; set; }
        //public DateTime CreatedDateTime { get; set; }
        //public string CreatedByFullName { get; set; }
        //public int CreatedByUserID { get; set; }

        // public SDEOpportunityDoc SdeDocs { get; set; }
        public List<SDEOpportunityDoc> SdeDocs { get; set; }

        public List<SDEOpportunityProduct> SdeProducts { get; set; }
        public List<SDEOpportunityNote> SdeNotes { get; set; }

        //public SDEOpportunityProduct SdeProducts { get; set; }
        // public SDEOpportunityNote SdeNotes { get; set; }

        //public SdeOpportunity()
        //{
        //    SdeDocs = new SDEOpportunityDoc();
        //    SdeProducts = new SDEOpportunityProduct();
        //    SdeNotes = new SDEOpportunityNote();
        //}
    }
}