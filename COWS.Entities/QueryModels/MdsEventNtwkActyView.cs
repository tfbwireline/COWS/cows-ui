﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class MdsEventNtwkActyView
    {
        public long Id { get; set; }
        public int EventId { get; set; }
        public byte NtwkActyTypeId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}