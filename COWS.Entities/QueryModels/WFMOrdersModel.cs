﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public class WFMOrdersModel
    {
        public int ORDR_ID { get; set; }
        public string FTN { get; set; }
        public string PROD_TYPE { get; set; }
        public string ORDR_TYPE { get; set; }
        public string PLTFM_TYPE { get; set; }
        public string COUNTRY { get; set; }
        public string ASSIGNED_USER { get; set; }
        public string ASSIGNED_BY { get; set; }
        public DateTime? ASSIGN_DT { get; set; }
        public string NEWASSIGNEE { get; set; }
        public string RTS { get; set; }
        public string CUST_NME { get; set; }
        public string CUST_ADDR { get; set; }
        public string CITY { get; set; }
        public string REGION { get; set; }
        public List<AssignedUsers> Assigned_User_List { get; set; }
        //GOM WFM Fileds
        public string SUB_STATUS { get; set; }
        public string MGR { get; set; }
        public string VNDR_NAME { get; set; }
        public DateTime? ASMT_DT { get; set; }
        public DateTime? ORDR_RCVD_DT { get; set; }
    }

    public class AssignedUsers
    {
        public string ASSIGNED_USER { get; set; }
        public string ASSIGNED_USER_COUNT { get; set; }
    }

}