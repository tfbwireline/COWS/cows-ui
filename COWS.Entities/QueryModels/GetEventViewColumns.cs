﻿namespace COWS.Entities.QueryModels
{
    public class GetEventViewColumns
    {
        public int DSPL_COL_ID { get; set; }
        public string DSPL_COL_NME { get; set; }
        public string SRC_TBL_COL_NME { get; set; }
        public byte DSPL_POS_FROM_LEFT_NBR { get; set; }
    }
}