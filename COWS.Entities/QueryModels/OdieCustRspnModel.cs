﻿namespace COWS.Entities.Models
{
    public partial class OdieCustRspnModel
    {
        public int RspnId { get; set; }
        public string CustTeamPdl { get; set; }
        public string MnspmId { get; set; }
        public string SowsFoldrPathNme { get; set; }
    }
}