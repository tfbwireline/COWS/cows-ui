﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public class GetVendorFolder
    {
        public int VNDR_FOLDR_ID { get; set; }
        public string VNDR_CD { get; set; }
        public string VNDR_NME { get; set; }
        public string CTRY_CD { get; set; }
        public string CTRY_NME { get; set; }
        public int VNDR_TMPLT_CNT { get; set; }

        //public int VendorFolderID { get; set; }
        //public string VendorCode { get; set; }
        //public string VendorName { get; set; }
        //public string VendorStAdd1 { get; set; }
        //public string VendorStAdd2 { get; set; }
        //public string City { get; set; }
        //public string Province { get; set; }
        //public string StateCode { get; set; }
        //public string PostalCode { get; set; }
        //public string CountryCD { get; set; }
        //public string CountryName { get; set; }
        //public string Comments { get; set; }
        //public string VenEmailList { get; set; }
        //public string Building { get; set; }
        //public string Floor { get; set; }
        //public string Room { get; set; }
        //public int TemplateCount { get; set; }
    }
}
