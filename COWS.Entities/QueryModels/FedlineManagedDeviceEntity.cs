﻿using System.Runtime.Serialization;

namespace COWS.Entities.QueryModels
{
    public class FedlineManagedDeviceEntity
    {
        public string DeviceName { get; set; }
        public string SerialNum { get; set; }
        public string Vendor { get; set; }
        public string Model { get; set; }
        public string DeviceType { get; set; }
        //public string ConfigData { get; set; }

        public FedlineManagedDeviceEntity()
        {
        }

        //Deserialization constructor.
        public FedlineManagedDeviceEntity(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            DeviceName = (string)info.GetValue("DeviceName", typeof(string));
            SerialNum = (string)info.GetValue("SerialNum", typeof(string));
            Vendor = (string)info.GetValue("Vendor", typeof(string));
            Model = (string)info.GetValue("Model", typeof(string));
            DeviceType = (string)info.GetValue("DeviceType", typeof(string));
            //ConfigData = (string)info.GetValue("ConfigData", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("DeviceName", DeviceName);
            info.AddValue("SerialNum", SerialNum);
            info.AddValue("Vendor", Vendor);
            info.AddValue("Model", Model);
            info.AddValue("DeviceType", DeviceType);
            //info.AddValue("ConfigData", ConfigData);
        }
    }
}