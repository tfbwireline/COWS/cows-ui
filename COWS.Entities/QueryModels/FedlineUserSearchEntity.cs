﻿namespace COWS.Entities.QueryModels
{
    public class FedlineUserSearchEntity
    {
        public int UserID { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
    }
}