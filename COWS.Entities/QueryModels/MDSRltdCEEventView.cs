﻿using System;

namespace COWS.Entities.QueryModels
{
    public class MDSRltdCEEventView
    {
        public string CEServiceID { get; set; }
        public int RltdCEEventID { get; set; }
    }
}