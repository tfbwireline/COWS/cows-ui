﻿using System;

namespace COWS.Entities.QueryModels
{
    public class GetNRMBPMInterfaceView
    {
        public string CPE_DEVICE_ID { get; set; }
        public string M5_ORDR_NBR { get; set; }
        public string M5_RLTD_ORDR_NBR { get; set; }
        public string SUB_TYPE_NME { get; set; }
        public string TYPE_NME { get; set; }
        public string PROD_CD { get; set; }
        public string XTRCT_CD { get; set; }
        public DateTime? XTRCT_DT { get; set; }
    }
}