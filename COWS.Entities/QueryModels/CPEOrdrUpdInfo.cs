﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{

    public class CPEOrdrUpdInfo
    {
        public int CPEOrdrID { get; set; }
        public int CPELineItemID { get; set; }
        public string FTN { get; set; }
        public string ItemReceiveDate { get; set; }
        public string OrderShipDate { get; set; }
        public string HrdwareShipDate { get; set; }
        public string USDistReceiveDate { get; set; }
        public string CustDelvryDate { get; set; }
        public string EquipTypeCd { get; set; }        
        public string EquipID { get; set; }
        public string EquipDesc { get; set; }
        public string EquipManf { get; set; }
        public string ManfPartCd { get; set; }
        public string PONbr { get; set; }
        public string ShipTrkNbr { get; set; }
        public string CarrierNme { get; set; }
        public string DeviceId { get; set; }
    }
}