﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public class MdsSiteLookupView
    {
        public string H6 { get; set; }
        public DateTime? Ccd { get; set; }

        #region SITE INFO

        public string InstlSitePocNme { get; set; }
        public string InstlSitePocIntlPhnCd { get; set; }
        public string InstlSitePocPhnNbr { get; set; }
        public string SrvcAssrnPocNme { get; set; }
        public string SrvcAssrnPocIntlPhnCd { get; set; }
        public string SrvcAssrnPocPhnNbr { get; set; }
        public string SiteIdTxt { get; set; }
        public string StreetAdr { get; set; }
        public string SttPrvnNme { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string CtyNme { get; set; }
        public string ZipCd { get; set; }
        public string UsIntlCd { get; set; }
        public string SrvcAvlbltyHrs { get; set; }
        public string SrvcTmeZnCd { get; set; }
        public string CpeDispatchEmail { get; set; }

        #endregion SITE INFO

        #region SITE INFO TABLES

        public ICollection<MdsEventCpeDevView> CpeDevices { get; set; } // Custom model
        public ICollection<MdsEventDevSrvcMgmtView> MnsOrders { get; set; } // Custom model
        public ICollection<MdsEventDevSrvcMgmtView> MnsAsasTypes { get; set; } // Custom model
        public ICollection<MdsEventDevSrvcMgmtView> RelatedMnsOrders { get; set; } // Custom model
        public ICollection<MDSEventDslSbicCustTrptView> ThirdParty { get; set; }
        public ICollection<MDSEventSlnkWiredTrptView> WiredDeviceTransports { get; set; }
        public ICollection<MDSEventPortBndwdView> PortBandwidth { get; set; }
       // public ICollection<CPEDispatchEmailResult> CpeDispatchEmail { get; set; }

        #endregion SITE INFO TABLES
    }
}