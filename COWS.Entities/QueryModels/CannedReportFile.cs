﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public class CannedFile
    {
        [Column("FileName")]
        public string Name { get; set; }

        [Column("FileSizeMb")]
        public double? Size { get; set; }

        [Column("FileDate")]
        public DateTime? DateCreated { get; set; }

        [Column("FileNamePattern")]
        public string NamePattern { get; set; }

        [Column("FullPath")]
        public string FullPath { get; set; }
    }
}
