﻿using COWS.Entities.Models;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public class MdsH6LookupView
    {
        #region BASIC INFO

        public string H6 { get; set; }
        public string H1 { get; set; }
        public string H1CustNme { get; set; }
        public string CustomerName { get; set; }
        public byte? NtwkEventTypeId { get; set; }
        public byte? VpnPltfrmTypeId { get; set; }
        public IEnumerable<MplsEventActyType> MplsEventActyType { get; set; }

        #endregion BASIC INFO

        #region DESIGN

        public string DesignDocumentApprovalNumber { get; set; }
        public string SalesEngineerEmail { get; set; }
        public string SalesEngineerPhone { get; set; }

        #endregion DESIGN

        public byte CsgLvlId { get; set; }
        public string GroupName { get; set; }
        public IEnumerable<MdsEventNtwkCustView> NetworkCustomer { get; set; }
        public IEnumerable<MdsEventNtwkTrptView> NetworkTransport { get; set; }
        public IEnumerable<MDSRltdCEEventView> RelatedCEEvents { get; set; }
    }
}