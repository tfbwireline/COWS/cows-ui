﻿namespace COWS.Entities.QueryModels
{
    public class GetEventDecryptValues
    {
        public string EVENT_TITLE_TXT { get; set; }
        public string EVENT_DES { get; set; }
        public string CUST_NME { get; set; }
        public string CUST_EMAIL_ADR { get; set; }
        public string CUST_CNTCT_NME { get; set; }
        public string CUST_CNTCT_PHN_NBR { get; set; }
        public string CUST_CNTCT_CELL_PHN_NBR { get; set; }
        public string CUST_CNTCT_PGR_NBR { get; set; }
        public string CUST_CNTCT_PGR_PIN_NBR { get; set; }
        public string SRVC_ASSRN_POC_NME { get; set; }
        public string SRVC_ASSRN_POC_PHN_NBR { get; set; }
        public string SRVC_ASSRN_POC_CELL_PHN_NBR { get; set; }
        public string BLDG_NME { get; set; }
        public string CTY_NME { get; set; }
        public string FLR_ID { get; set; }
        public string STT_PRVN_NME { get; set; }
        public string RM_NBR { get; set; }
        public string SITE_ADR { get; set; }
        public string STREET_ADR_1 { get; set; }
        public string STREET_ADR_2 { get; set; }
        public string STREET_ADR_3 { get; set; }
        public string STT_CD { get; set; }
        public string ZIP_PSTL_CD { get; set; }
        public string CTRY_RGN_NME { get; set; }
        public string FRST_NME { get; set; }
        public string LST_NME { get; set; }
        public string NTE_TXT { get; set; }
        public string CTRY_CD { get; set; }
    }
}