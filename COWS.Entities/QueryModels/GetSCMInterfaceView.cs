﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class GetSCMInterfaceView
    {
        public string FTN { get; set; }
        public string DEVICE_ID { get; set; }
        public string PLSFT_RQSTN_NBR { get; set; }
        public DateTime? RQSTN_DT { get; set; }
        public string PRCH_ORDR_NBR { get; set; }
        public string OrderStatus { get; set; }
    }
}