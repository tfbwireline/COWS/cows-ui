﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public class EventWorkflow
    {
        // General
        public int EventId { get; set; }

        public string EventTitle { get; set; }
        public int EventStatusId { get; set; }
        public int EventTypeId { get; set; }
        public int WorkflowId { get; set; }
        public int RequestorId { get; set; }
        public int ReviewerId { get; set; }
        public int UserId { get; set; } // Logged on user that can be used as ModifiedByUserId
        public string Profile { get; set; }
        public bool NewEvent { get; set; }
        public LkEventRule EventRule { get; set; }
        public List<EventAsnToUser> AssignUser { get; set; }

        // For Calendar Entry
        public int EnhanceServiceId { get; set; }

        public int MplsEventTypeId { get; set; }
        public string Comments { get; set; }
        public string ConferenceBridgeNbr { get; set; }
        public string ConferenceBridgePin { get; set; }
        public int CalendarEventTypeId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime OldStartTime { get; set; }
        public DateTime OldEndTime { get; set; }
        public bool IsEscalation { get; set; }

        // For Email Entry
        public string EventType { get; set; }

        public string SOWSEventID { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public string TeamPdlNme { get; set; }
        public short SiptProdTypeId { get; set; }
        public string H1 { get; set; }
        public string SiteId { get; set; }
        public string H6 { get; set; }
        public string M5OrdrNbr { get; set; }
        public string SiteAdr { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtyNme { get; set; }
        public string SttPrvnNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string ZipCd { get; set; }
        public string UsIntlCd { get; set; }
        public string SiteCntctNme { get; set; }
        public string SiteCntctPhnNbr { get; set; }
        public string SiteCntctHrNme { get; set; }
        public string SiteCntctEmailAdr { get; set; }
        public string SiptDesgnDoc { get; set; }
        public List<int> SiptEventActyIds { get; set; }
        public List<int> SiptEventTollTypeIds { get; set; }
        public List<int> SiptReltdOrdrIds { get; set; }
        public List<string> SiptReltdOrdrNos { get; set; }

        // For MDS Specific Email Entry
        public bool IsSrvcActvEmail { get; set; }

        // For Network Intl
        public bool IsNetworkIntl { get; set; }
        public bool IsNetwork { get; set; }

        public bool IsSrvcCmntEmail { get; set; }
        public bool IsSupressEmail { get; set; }
        public string ShipCustEmailAddr { get; set; }
        public string CPEDispatchEmail { get; set; }
        public short? FailReasId { get; set; }
        public string OnlineMeetingAdr { get; set; }
        public string M5OrdrCmpntId { get; set; }
        public string IsSrvcGuestWiFiPubEmail { get; set; }
        public byte CsgLvlId { get; set; }    //MdsEvent.Event.CsgLvlId
        public string EventStusDes { get; set; }  //MdsEvent.EventStus.EventStusDes
        public string WrkflwStusDes { get; set; } //MdsEvent.WrkflwStus.WrkflwStusDes
        public int ActivatorUserId { get; set; }
        public string ReviewerAdId { get; set; } // To be used as holder for MNSPM value
        public bool IsGoodman { get; set; } = false;
        public List<MdsEventDevSrvcMgmtView> MdsEventDevSrvcMgmtView { get; set; }
        public List<MdsEventDevSrvcMgmtView> MdsEventRltdDevSrvcMgmtView { get; set; }
        public List<MdsEventWrlsTrpt> MdsEventWrlsTrpt { get; set; }
        public List<MdsEventNtwkTrpt> MdsEventNtwkTrpt { get; set; }
        public List<MdsEventNtwkCust> MdsEventNtwkCust { get; set; }
        public List<MdsEventPortBndwd> MdsEventPortBndwd { get; set; }
        public List<MdsEventSiteSrvc> MdsEventSiteSrvc { get; set; }
        public List<MdsEventSlnkWiredTrpt> MdsEventSlnkWiredTrpt { get; set; }
        public List<MdsEventDslSbicCustTrpt> MdsEventDslSbicCustTrpt { get; set; }
        public List<EventCpeDev> EventCpeDev { get; set; }
        public List<EventDevCmplt> EventDevCmplt { get; set; }
        public List<MdsEventOdieDev> MdsEventOdieDev { get; set; }
        public List<EventDiscoDev> EventDiscoDev { get; set; }
    }
}