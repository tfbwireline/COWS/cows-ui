﻿using System;
using System.Runtime.Serialization;

namespace COWS.Entities.QueryModels
{
    public class FedlineUserDataEntity
    {
        public string EventTitle { get; set; }
        public string EmailCCPDL { get; set; }
        public string ShippingCarrier { get; set; }
        public string TrackingNumber { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public bool PreStageComplete { get; set; }
        public int? PreStagingEngineerUserID { get; set; }
        public string PreStagingEngineerUserDisplay { get; set; }
        public int? MNSActivatorUserID { get; set; }
        public string MNSActivatorUserDisplay { get; set; }
        public string DeviceType { get; set; }
        public string SOWLocation { get; set; }
        public string Redesign { get; set; }
        public string TunnelStatus { get; set; }
        public int? FailCodeID { get; set; }
        public string SlaCd { get; set; }

        public FedlineUserDataEntity()
        {
        }

        //Deserialization constructor.
        public FedlineUserDataEntity(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            EventTitle = (string)info.GetValue("EventTitle", typeof(string));
            EmailCCPDL = (string)info.GetValue("EmailCCPDL", typeof(string));
            ShippingCarrier = (string)info.GetValue("ShippingCarrier", typeof(string));
            TrackingNumber = (string)info.GetValue("TrackingNumber", typeof(string));
            ArrivalDate = (DateTime?)info.GetValue("ArrivalDate", typeof(DateTime?));
            PreStageComplete = (bool)info.GetValue("PreStageComplete", typeof(bool));
            PreStagingEngineerUserID = (int?)info.GetValue("PreStagingEngineerUserID", typeof(int?));
            PreStagingEngineerUserDisplay = (string)info.GetValue("PreStagingEngineerUserDisplay", typeof(string));
            MNSActivatorUserID = (int?)info.GetValue("MNSActivatorUserID", typeof(int?));
            MNSActivatorUserDisplay = (string)info.GetValue("MNSActivatorUserDisplay", typeof(string));
            DeviceType = (string)info.GetValue("DeviceType", typeof(string));
            SOWLocation = (string)info.GetValue("SOWLocation", typeof(string));
            Redesign = (string)info.GetValue("Redesign", typeof(string));
            TunnelStatus = (string)info.GetValue("TunnelStatus", typeof(string));
            FailCodeID = (short?)info.GetValue("FailCodeID", typeof(short?));
            SlaCd = (string)info.GetValue("SlaCd", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("EventTitle", EventTitle);
            info.AddValue("EmailCCPDL", EmailCCPDL);
            info.AddValue("ShippingCarrier", ShippingCarrier);
            info.AddValue("TrackingNumber", TrackingNumber);
            info.AddValue("ArrivalDate", ArrivalDate);
            info.AddValue("PreStageComplete", PreStageComplete);
            info.AddValue("PreStagingEngineerUserID", PreStagingEngineerUserID);
            info.AddValue("PreStagingEngineerUserDisplay", PreStagingEngineerUserDisplay);
            info.AddValue("MNSActivatorUserID", MNSActivatorUserID);
            info.AddValue("MNSActivatorUserDisplay", MNSActivatorUserDisplay);
            info.AddValue("DeviceType", DeviceType);
            info.AddValue("SOWLocation", SOWLocation);
            info.AddValue("Redesign", Redesign);
            info.AddValue("TunnelStatus", TunnelStatus);
            info.AddValue("FailCodeID", FailCodeID);
            info.AddValue("SlaCd", SlaCd);
        }
    }
}