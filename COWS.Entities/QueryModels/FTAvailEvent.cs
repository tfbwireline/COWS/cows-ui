﻿using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class FTAvailEvent
    {
        [Column("EVENT_ID")]
        public int EventID { get; set; }

        [Column("ESCLYN")]
        public string EscYN { get; set; }

        [Column("CUST_NME")]
        public string CustName { get; set; }

        public string AssignedUsers { get; set; }

        [Column("EVENT_STUS_DES")]
        public string EventStatus { get; set; }

        public int IsMDSNew { get; set; }
    }
}