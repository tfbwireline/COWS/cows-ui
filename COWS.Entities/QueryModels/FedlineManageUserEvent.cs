﻿using System;

namespace COWS.Entities.QueryModels
{
    public class FedlineManageUserEvent
    {
        public int EventID { get; set; }
        public int FRBID { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Activator { get; set; }
        public string PreConfigEng { get; set; }
        public string Activity { get; set; }
        public string Status { get; set; }
    }
}