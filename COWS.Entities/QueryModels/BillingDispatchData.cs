﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public class BillingDispatchData
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("CUST_ID")]
        public string CustomerId { get; set; }
        
        [Column("CLEAR_LOC")]
        public string TicketLocation { get; set; }
        
        [Column("DISP_CD")]
        public string DispositionCode { get; set; }
        
        [Column("DISP_CAT_NAME")]
        public string TicketCategoryName { get; set; }
        
        [Column("DISP_SUBCAT_NAME")]
        public string TicketSubCategoryName { get; set; }
        
        [Column("DISP_CA_NAME")]
        public string ActionTaken { get; set; }

        [Column("CLS_DT")]
        public DateTime? CloseDate { get; set; }

        [Column("TICK_NBR")]
        public string TicketNumber { get; set; }

        [MaxLength(1)]
        [Column("ACPT_RJCT_CD")]
        public string AcceptRejectCode { get; set; }

        [MaxLength(10)]
        [Column("ACPT_RJCT")]
        public string AcceptReject { get; set; }

        [Column("CMNT_TXT")]
        public string Comment { get; set; }
        
        [Column("STUS_ID")]
        public int? StatusId { get; set; }
        
        [Column("STUS_DES")]
        public string StatusDescription { get; set; }

        [Column("CREAT_BY_USER_ID")]
        public int? CreatedByUserId { get; set; }

        [Column("CREAT_BY_USER_NME")]
        public string CreatedBy { get; set; }

        [Column("MODFD_BY_USER_ID")]
        public int? ModifiedByUserId { get; set; }

        [Column("MODFD_BY_USER_NME")]
        public string ModifiedBy { get; set; }

        [Column("CREAT_DT")]
        public DateTime? CreatedDate { get; set; }

        [Column("MODFD_DT")]
        public DateTime? ModifiedDate { get; set; }
    }
}
