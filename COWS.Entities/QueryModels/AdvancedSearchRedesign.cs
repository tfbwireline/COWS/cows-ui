﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public partial class AdvancedSearchRedesign
    {
        [Column("Redesign ID")]
        public string RedesignID { get; set; }
        [Column("Redesign Number")]
        public string RedesignNumber { get; set; }
        [Column("Expiration Date")]
        public DateTime? ExpirationDate { get; set; }
        [Column("H1")]
        public string H1 { get; set; }
        [Column("Customer Name")]
        public string CustomerName { get; set; }
        [Column("Device Name")]
        public string DeviceName { get; set; }
        [Column("Fast Track Flag")]
        public bool? FastTrackFlag { get; set; }
        [Column("Dispatch Ready Flag")]
        public bool? DispatchReadyFlag { get; set; }
        [Column("Completion Flag")]
        public bool? CompletionFlag { get; set; }
        [Column("Event ID")]
        public int? EventID { get; set; }
        [Column("CSG_LVL_ID")]
        public byte CSG_LVL_ID { get; set; }
    }
}
