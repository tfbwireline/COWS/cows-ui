﻿using System.Runtime.Serialization;

namespace COWS.Entities.QueryModels
{
    public class FedlineSiteLocationEntity
    {
        public string SiteAddress1 { get; set; }
        public string SiteAddress2 { get; set; }
        public string City { get; set; }
        public string StateProv { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string PrimaryPOCName { get; set; }
        public string PrimaryPOCPhone { get; set; }
        public string PrimaryPOCPhoneExtension { get; set; }
        public string PrimaryPOCEmail { get; set; }
        public string SecondaryPOCName { get; set; }
        public string SecondaryPOCPhone { get; set; }
        public string SecondaryPOCPhoneExtension { get; set; }
        public string SecondaryPOCEmail { get; set; }

        public FedlineSiteLocationEntity()
        {
        }

        //Deserialization constructor.
        public FedlineSiteLocationEntity(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            SiteAddress1 = (string)info.GetValue("SiteAddress1", typeof(string));
            SiteAddress2 = (string)info.GetValue("SiteAddress2", typeof(string));
            City = (string)info.GetValue("City", typeof(string));
            StateProv = (string)info.GetValue("StateProv", typeof(string));
            Country = (string)info.GetValue("Country", typeof(string));
            Zip = (string)info.GetValue("Zip", typeof(string));
            PrimaryPOCName = (string)info.GetValue("PrimaryPOCName", typeof(string));
            PrimaryPOCPhone = (string)info.GetValue("PrimaryPOCPhone", typeof(string));
            PrimaryPOCPhoneExtension = (string)info.GetValue("PrimaryPOCPhoneExtension", typeof(string));
            PrimaryPOCEmail = (string)info.GetValue("PrimaryPOCEmail", typeof(string));
            SecondaryPOCName = (string)info.GetValue("SecondaryPOCName", typeof(string));
            SecondaryPOCPhone = (string)info.GetValue("SecondaryPOCPhone", typeof(string));
            SecondaryPOCPhoneExtension = (string)info.GetValue("SecondaryPOCPhoneExtension", typeof(string));
            SecondaryPOCEmail = (string)info.GetValue("SecondaryPOCEmail", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("SiteAddress1", SiteAddress1);
            info.AddValue("SiteAddress2", SiteAddress2);
            info.AddValue("City", City);
            info.AddValue("StateProv", StateProv);
            info.AddValue("Country", Country);
            info.AddValue("Zip", Zip);
            info.AddValue("PrimaryPOCName", PrimaryPOCName);
            info.AddValue("PrimaryPOCPhone", PrimaryPOCPhone);
            info.AddValue("PrimaryPOCPhoneExtension", PrimaryPOCPhoneExtension);
            info.AddValue("PrimaryPOCEmail", PrimaryPOCEmail);
            info.AddValue("SecondaryPOCName", SecondaryPOCName);
            info.AddValue("SecondaryPOCPhone", SecondaryPOCPhone);
            info.AddValue("SecondaryPOCPhoneExtension", SecondaryPOCPhoneExtension);
            info.AddValue("SecondaryPOCEmail", SecondaryPOCEmail);
        }
    }
}