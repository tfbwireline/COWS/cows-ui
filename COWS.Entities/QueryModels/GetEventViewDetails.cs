﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class GetEventViewDetails
    {
        [Column("Event ID")]
        public int EventID { get; set; }

        public byte CSG_LVL_ID { get; set; }

        [Column("Event Title")]
        public string EventTitle { get; set; }

        [Column("AD Type")]
        [NotMapped]
        public string ADType { get; set; }

        [Column("Event Status")]
        public string EventStatus { get; set; }

        [Column("Workflow Status")]
        public string WorkflowStatus { get; set; }

        [Column("Assigned To")]
        public string AssignedTo { get; set; }

        [Column("Created By")]
        public string CreatedBy { get; set; }

        [Column("Customer Name")]
        public string CustomerName { get; set; }

        [Column("Site ID")]
        [NotMapped]
        public string SiteID { get; set; }

        [Column("M5 #")]
        [NotMapped]
        public string M5Number { get; set; }

        [Column("CHARS ID")]
        [NotMapped]
        public string CharID { get; set; }

        public string H1 { get; set; }

        [NotMapped]
        public string H6 { get; set; }

        [Column("Start Time")]
        [NotMapped]
        public DateTime StartTime { get; set; }

        [Column("End Time")]
        [NotMapped]
        public DateTime EndTime { get; set; }

        [Column("Is Escalation")]
        [NotMapped]
        public bool IsEscalation { get; set; }

        [Column("Event Status ID")]
        [NotMapped]
        public int EventStatusID { get; set; }

        [Column("Customer Request Start Date")]
        [NotMapped]
        public DateTime CustomerRequestStartDate { get; set; }

        [Column("Customer Request End Date")]
        [NotMapped]
        public DateTime CustomerRequestEndStartDate { get; set; }

        [Column("SORT1")]
        public string Sort1 { get; set; }

        [Column("SORT2")]
        public string Sort2 { get; set; }
    }
}