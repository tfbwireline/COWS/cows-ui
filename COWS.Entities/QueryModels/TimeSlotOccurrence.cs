﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class TimeSlotOccurrence
    {
        public byte TimeSlotID { get; set; }
        public short CurFilledTimeSlots { get; set; }
        public short MaxTimeSlots { get; set; }
        public short TotalTimeSlots { get; set; }
        public DateTime Day { get; set; }
        public int RsrcAvlbltyID { get; set; }
        public short CurAvailTimeSlots { get; set; }
        public string DisplayTimeSlot { get; set; }
        public string EventTypeDes { get; set; }
        public byte RAP { get; set; }
        public TimeSpan TimeSlotEnd { get; set; }
        public TimeSpan TimeSlotStart { get; set; }
        public int EventTypeID { get; set; }

        public bool IsOverbooked
        {
            get
            {
                if (CurAvailTimeSlots > MaxTimeSlots)
                    return true;
                return false;
            }
        }
    }
}