﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class TransportOrderDetails
    {
        public IEnumerable<TransportOrder> TransportOrders { get; set; }
        public IEnumerable<OrderMilestone> OrderMilestones { get; set; }
        public IEnumerable<CircuitMilestone> CircuitMilestones { get; set; }
        public IEnumerable<ContactInfo> AccountTeam { get; set; }
        public IEnumerable<ContactInfo> AlternateCustomer { get; set; }
    }

    public class TransportOrder
    {
        public int OrderId { get; set; }
        public string Interface { get; set; }
        public string AccessSpeed { get; set; }
        public int SalesChannelID { get; set; }
        public string OrderAssignedDate { get; set; }
        public string CustBillCurrency { get; set; }
        public int LengthOfCkt { get; set; }
        public string AccessMBValue { get; set; }
        public string VAPGName { get; set; }
        public int VendorID { get; set; }
        public int IPT { get; set; }
        public string SCADText { get; set; }
        public string IsNewCkt { get; set; }
        public int ARgnID { get; set; }
        public string Vendor { get; set; }
        public string ManagerName { get; set; }
        public string VendorOrder { get; set; }
        public int AccCitySite { get; set; }
        public string PRS_QOT_NBR { get; set; }
        public string GCS_NBR { get; set; }
        public string RFQ_NBR { get; set; }
        public string NRM_UPDT_CD { get; set; }
        public string CNFRM_RNL_DT { get; set; }
        public string PreQualNumber { get; set; }
    }
    public class OrderMilestone
    {
        public int OrderID { get; set; }
        public int VersionID { get; set; }
        public string CCD { get; set; }
        public string CWD { get; set; }
        public string PreSubmitDate { get; set; }
        public string SubmitDate { get; set; }
        public string ValidatedDate { get; set; }
        public string BillClearDate { get; set; }
        public string OrdrDiscDate { get; set; }
        public string VendorCancellationDate { get; set; }
        public string CUST_ACPTC_TURNUP_DT { get; set; }
        public int CreatedUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public string XNCI_CNFRM_RNL_DT { get; set; }
        public string RNL_DT_REQR_CD { get; set; }
    }
    public class CircuitMilestone
    {
        public int CktID { get; set; }
        public int OrderID { get; set; }
        public int VersionID { get; set; }
        public string VendorCktID { get; set; }
        public string PLSeq { get; set; }
        public string LECID { get; set; }
        public string TDDR { get; set; }
        public string TDD { get; set; }
        public string ADD { get; set; }
        public string AAD { get; set; }
        public string VCDD { get; set; }
        public string TTRPT_ACCS_CNTRC_TERM_CD { get; set; }
        public string CNTRC_STRT_DT { get; set; }
        public string CNTRC_END_DT { get; set; }
        public string RNL_DT { get; set; }
        public bool TRMTG_CD { get; set; }
        public string RLTD_VNDR_ORDR_ID { get; set; }
        public string IP_ADR_QTY { get; set; }
        public string RTNG_PRCOL { get; set; }
        public string CUST_IP_ADR { get; set; }
        public string SCA_DET { get; set; }
        public string XcnnctPrvdr { get; set; }
        public string TdwnAssn { get; set; }
        public string TdwnReq { get; set; }
    }

    public class ContactInfo
    {
        public int RoleID { get; set; }
        public string RoleTypeID { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int ORDR_CNTCT_ID { get; set; }
        public string PHN_NBR { get; set; }
    }
    
    // Last two resultset are mirror of ORDR_HOLD_MS and ORDR_VLAN
    //public class SlaInfo
    //{
    //    public string ORDR_ID { get; set; }
    //    public string VER_ID { get; set; }
    //    public string HOLD_DT { get; set; }
    //    public string HOLD_REAS_ID { get; set; }
    //    public string HOLD_REAS_CMNT_TXT { get; set; }
    //    public string HOLD_BY_USER_ID { get; set; }
    //    public string RELS_DT { get; set; }
    //    public string RELS_BY_USER_ID { get; set; }
    //    public string CREAT_DT { get; set; }
    //}
}
