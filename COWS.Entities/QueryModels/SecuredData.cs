﻿using COWS.Entities.Models;

namespace COWS.Entities.QueryModels
{
    public class SecuredData
    {
        public string CustNme { get; set; }
        public string CustCntctNme { get; set; }
        public string CustCntctPhnNbr { get; set; }
        public string CustEmailAdr { get; set; }
        public string CustCntctCellPhnNbr { get; set; }
        public string CustCntctPgrNbr { get; set; }
        public string CustCntctPgrPinNbr { get; set; }
        public string EventTitleTxt { get; set; }
        public string EventDes { get; set; }

        public string TeamPdlNme { get; set; }
        public string StreetAdr { get; set; }

        //public string EventTitleTxt { get; set; }
        //public string CustNme { get; set; }
        public string SiteCntctNme { get; set; }

        public string SiteCntctPhnNbr { get; set; }
        public string SiteCntctHrNme { get; set; }
        public string SiteCntctEmailAdr { get; set; }
        public string SiteAdr { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtyNme { get; set; }
        public string SttPrvnNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string ZipCd { get; set; }
        public string SaCntctNme { get; set; }
        public string SaCntctPhnNbr { get; set; }
        public string SaCntctCellPhnNbr { get; set; }
        public string AltStreetAdr1 { get; set; }
        public string AltFlrBldgNme { get; set; }
        public string AltCtyNme { get; set; }
        public string AltSttPrvnNme { get; set; }
        public string AltCtryRgnNme { get; set; }
        public string AltZipCd { get; set; }
        public string AltCntctNme { get; set; }
        public string AltCntctPhnNbr { get; set; }
        public string AltCntctEmail { get; set; }
        public string NetworkCustNme { get; set; }

        //public SecuredData(dynamic entity)
        //{
        //    CustNme = entity.CustNme;
        //    CustCntctNme = entity.CustCntctNme;
        //    CustCntctPhnNbr = entity.CustCntctPhnNbr;
        //    CustEmailAdr = entity.CustEmailAdr;
        //    CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
        //    CustCntctPgrNbr = entity.CustCntctPgrNbr;
        //    CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
        //    EventTitleTxt = entity.EventTitleTxt;
        //    EventDes = entity.EventDes;
        //}

        public SecuredData(AdEvent entity)
        {
            CustNme = entity.CustNme;
            CustCntctNme = entity.CustCntctNme;
            CustCntctPhnNbr = entity.CustCntctPhnNbr;
            CustEmailAdr = entity.CustEmailAdr;
            CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
            CustCntctPgrNbr = entity.CustCntctPgrNbr;
            CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
            EventTitleTxt = entity.EventTitleTxt;
            EventDes = entity.EventDes;
        }

        public SecuredData(MdsEvent entity)
        {
            CustNme = entity.CustNme;
            CustCntctNme = entity.InstlSitePocNme;
            CustCntctPhnNbr = entity.InstlSitePocPhnNbr;
            CustCntctCellPhnNbr = entity.InstlSitePocCellPhnNbr;
            SaCntctNme = entity.SrvcAssrnPocNme;
            SaCntctPhnNbr = entity.SrvcAssrnPocPhnNbr;
            SaCntctCellPhnNbr = entity.SrvcAssrnPocCellPhnNbr;
            StreetAdr = entity.StreetAdr;
            FlrBldgNme = entity.FlrBldgNme;
            CtyNme = entity.CtyNme;
            SttPrvnNme = entity.SttPrvnNme;
            CtryRgnNme = entity.CtryRgnNme;
            ZipCd = entity.ZipCd;
            AltStreetAdr1 = entity.AltShipStreetAdr;
            AltFlrBldgNme = entity.AltShipFlrBldgNme;
            AltCtyNme = entity.AltShipCtyNme;
            AltSttPrvnNme = entity.AltShipSttPrvnNme;
            AltCtryRgnNme = entity.AltShipCtryRgnNme;
            AltZipCd = entity.AltShipZipCd;
            AltCntctNme = entity.AltShipPocNme;
            AltCntctPhnNbr = entity.AltShipPocPhnNbr;
            AltCntctEmail = entity.AltShipPocEmail;

            EventTitleTxt = entity.EventTitleTxt;
            CustEmailAdr = entity.CustAcctTeamPdlNme;
            NetworkCustNme = entity.NtwkCustNme;
        }

        public SecuredData(MplsEvent entity)
        {
            CustNme = entity.CustNme;
            CustCntctNme = entity.CustCntctNme;
            CustCntctPhnNbr = entity.CustCntctPhnNbr;
            CustEmailAdr = entity.CustEmailAdr;
            CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
            CustCntctPgrNbr = entity.CustCntctPgrNbr;
            CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
            EventTitleTxt = entity.EventTitleTxt;
            EventDes = entity.EventDes;
        }

        public SecuredData(NgvnEvent entity)
        {
            CustNme = entity.CustNme;
            CustCntctNme = entity.CustCntctNme;
            CustCntctPhnNbr = entity.CustCntctPhnNbr;
            CustEmailAdr = entity.CustEmailAdr;
            CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
            CustCntctPgrNbr = entity.CustCntctPgrNbr;
            CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
            EventTitleTxt = entity.EventTitleTxt;
            EventDes = entity.EventDes;
        }

        public SecuredData(SplkEncryptContactModel entity)
        {
            CustNme = entity.CustNme;
            CustCntctNme = entity.CustCntctNme;
            CustCntctPhnNbr = entity.CustCntctPhnNbr;
            CustEmailAdr = entity.CustEmailAdr;
            CustCntctCellPhnNbr = entity.CustCntctCellPhnNbr;
            CustCntctPgrNbr = entity.CustCntctPgrNbr;
            CustCntctPgrPinNbr = entity.CustCntctPgrPinNbr;
            EventTitleTxt = entity.EventTitleTxt;
            EventDes = entity.EventDes;
        }

        public SecuredData(SiptEncryptContactModel entity)
        {
            CustNme = entity.CustNme;
            CustCntctNme = entity.SiteCntctNme;
            CustCntctPhnNbr = entity.SiteCntctPhnNbr;
            CustEmailAdr = entity.SiteCntctEmailAdr;
            SiteAdr = entity.SiteAdr;
            FlrBldgNme = entity.FlrBldgNme;
            CtyNme = entity.CtyNme;
            SttPrvnNme = entity.SttPrvnNme;
            CtryRgnNme = entity.CtryRgnNme;
            ZipCd = entity.ZipCd;
            EventTitleTxt = entity.EventTitleTxt;
            StreetAdr = entity.StreetAdr;
        }
    }
}