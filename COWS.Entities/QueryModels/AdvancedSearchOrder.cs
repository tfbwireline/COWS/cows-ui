﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public partial class AdvancedSearchOrder
    {
        //[Column("order Id")]
        public int ORDR_ID { get; set; }
       
        public string FTN { get; set; }
        public int? H1 { get; set; }
        public string CountryName { get; set; }
        public string PrivateLine { get; set; }
        public string CustomerName { get; set; }
        public DateTime? CCD { get; set; }
        public string OrderType { get; set; }
        public string VendorCircuitID { get; set; }
        public string ProductType { get; set; }
        public string PlatformType { get; set; }
        public string OrderSubType { get; set; }

        public string DomesticFlag { get; set; }
        [Column("H5/H6")]
        public string H5_H6 { get; set; }
       
       
        public string PRSQuote { get; set; }
        public string SOI { get; set; }
        
        
        public string VendorName { get; set; }
        public string Region { get; set; }
        public string OrderTypeDesc { get; set; }
        public string WorkGroup { get; set; }
        public string OrderStatus { get; set; }

        public int WGID { get; set; }
        public int TASK_ID { get; set; }
        public int CAT_ID { get; set; }
        public byte CSG_LVL_ID { get; set; }
        public int H5_FOLDR_ID { get; set; }
        public string PROD_TYPE { get; set; }
        public string ORDR_STUS { get; set; }
        public string ORDR_TYPE { get; set; }
        public string PLTFRM_NME { get; set; }
        
        public string CPEVendor { get; set; }
        public string DeviceID { get; set; }
       

    }
}
