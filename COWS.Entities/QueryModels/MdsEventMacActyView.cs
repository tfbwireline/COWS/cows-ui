﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class MdsEventMacActyView
    {
        public int EventId { get; set; }
        public int MdsMacActyId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}