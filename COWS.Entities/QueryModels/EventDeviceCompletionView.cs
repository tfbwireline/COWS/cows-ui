﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class EventDeviceCompletionView
    {
        public int EventDevCmpltId { get; set; }
        public int EventId { get; set; }
        public string OdieDevNme { get; set; }
        public string H6 { get; set; }
        public bool CmpltdCd { get; set; }
        public DateTime CreatDt { get; set; }
        public int? RedsgnDevId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? OdieSentDt { get; set; }
    }
}