﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public partial class MdsEventView
    {
        public int EventId { get; set; }
        public byte NtwkActyTypeId { get; set; }
        public byte EventStusId { get; set; }
        public string EventTitleTxt { get; set; }
        public string ShrtDes { get; set; }
        public string H1 { get; set; }
        public string CustNme { get; set; }
        public string CustAcctTeamPdlNme { get; set; }
        public string CustSowLocTxt { get; set; }
        public string MnsPmId { get; set; }
        public byte? MdsActyTypeId { get; set; }
        public string NtwkH6 { get; set; }
        public string NtwkH1 { get; set; }
        public string NtwkCustNme { get; set; }
        public byte? NtwkEventTypeId { get; set; }
        public byte? VpnPltfrmTypeId { get; set; }
        public string DdAprvlNbr { get; set; }
        //public string SalsEngrPhn { get; set; }
        //public string SalsEngrEmail { get; set; }
        public int ReqorUserId { get; set; }
        public string ReqorUserCellPhnNbr { get; set; }
        public string H6 { get; set; }
        public DateTime? Ccd { get; set; }
        public string StreetAdr { get; set; }
        public string SttPrvnNme { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string CtyNme { get; set; }
        public string ZipCd { get; set; }
        public string UsIntlCd { get; set; }
        public string SrvcAvlbltyHrs { get; set; }
        public string SrvcTmeZnCd { get; set; }
        public string InstlSitePocNme { get; set; }
        public string InstlSitePocIntlPhnCd { get; set; }
        public string InstlSitePocPhnNbr { get; set; }
        public string InstlSitePocIntlCellPhnCd { get; set; }
        public string InstlSitePocCellPhnNbr { get; set; }
        public string SrvcAssrnPocNme { get; set; }
        public string SrvcAssrnPocIntlPhnCd { get; set; }
        public string SrvcAssrnPocPhnNbr { get; set; }
        public string SrvcAssrnPocIntlCellPhnCd { get; set; }
        public string SrvcAssrnPocCellPhnNbr { get; set; }
        public byte? SprintCpeNcrId { get; set; }
        public DateTime? ShippedDt { get; set; }
        public string ShipTrkRefrNbr { get; set; }
        public string ShipCustEmailAdr { get; set; }
        public string ShipDevSerialNbr { get; set; }
        public string ShipCxrNme { get; set; }
        public string DiscMgmtCd { get; set; }
        public bool? FullCustDiscCd { get; set; }
        public string FullCustDiscReasTxt { get; set; }
        public bool EsclCd { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public string EsclBusJustnTxt { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public DateTime StrtTmst { get; set; }
        public DateTime? EndTmst { get; set; }
        public short? ExtraDrtnTmeAmt { get; set; }
        public short? EventDrtnInMinQty { get; set; }
        public string NidHostNme { get; set; }
        public bool SoftAssignCd { get; set; }
        public string AltShipPocNme { get; set; }
        public string AltShipPocPhnNbr { get; set; }
        public string AltShipPocEmail { get; set; }
        public string AltShipStreetAdr { get; set; }
        public string AltShipFlrBldgNme { get; set; }
        public string AltShipCtyNme { get; set; }
        public string AltShipSttPrvnNme { get; set; }
        public string AltShipCtryRgnNme { get; set; }
        public string AltShipZipCd { get; set; }
        public string NidSerialNbr { get; set; }
        public string IpmDes { get; set; }
        public bool? RltdCmpsNcrCd { get; set; }
        public string RltdCmpsNcrNme { get; set; }

        #region TABLES

        public ICollection<MdsEventNtwkCustView> NetworkCustomer { get; set; }
        public ICollection<MdsEventNtwkTrptView> NetworkTransport { get; set; }
        public ICollection<MdsEventOdieDevView> MdsRedesignDevInfo { get; set; }
        public ICollection<MdsEventDiscOdieDevView> EventDiscoDev { get; set; }
        public ICollection<EventDeviceCompletionView> DevCompletion { get; set; }
        public ICollection<MdsEventCpeDevView> CpeDevice { get; set; }
        public ICollection<MdsEventDevSrvcMgmtView> MnsOrder { get; set; }
        public ICollection<MdsEventSiteSrvcView> SiteService { get; set; }
        public ICollection<MDSEventDslSbicCustTrptView> ThirdParty { get; set; }
        public ICollection<MDSEventSlnkWiredTrptView> WiredTransport { get; set; }
        public ICollection<MDSEventWrlsTrptView> WirelessTransport { get; set; }
        public ICollection<MDSEventPortBndwdView> PortBandwidth { get; set; }
        public ICollection<MdsEventNtwkActyView> MdsEventNtwkActy { get; set; }

        #endregion TABLES

        // Additional
        public List<int> MdsEventNtwkActyIds { get; set; }
        public List<int> MdsEventMacActyIds { get; set; }
        public List<int> MplsEventActyTypeIds { get; set; }
        public List<int> Activators { get; set; }

        public ICollection<MdsEventMacActyView> MdsEventMacActy { get; set; }
        public ICollection<MplsActivityTypeView> MplsEventActyType { get; set; }
        //public ICollection<EventAsnToUserViewModel> EventAsnToUser { get; set; }

        public string CpeDspchEmailAdr { get; set; }
        public string CpeDspchCmntTxt { get; set; }
        public string CharsId { get; set; }
        public byte? OptOutReasId { get; set; }
        public string SiteIdTxt { get; set; }
        public byte CnfrcBrdgId { get; set; }
        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public string OnlineMeetingAdr { get; set; }
        public string CmntTxt { get; set; }
        public string MdsFastTrkTypeId { get; set; }
        public byte? TmeSlotId { get; set; }
        public byte WrkflwStusId { get; set; }
        public byte? OldEventStusId { get; set; }
        public short? FailReasId { get; set; }
        public bool? CustTrptReqrCd { get; set; }
        public bool? WrlesTrptReqrCd { get; set; }
        public string WoobIpAdr { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public string EsclPolicyLocTxt { get; set; }
        public string OptOutBusJustnTxt { get; set; }
        public string BusJustnTxt { get; set; }
        public bool PreCfgCmpltCd { get; set; }
        public bool? CpeOrdrCd { get; set; }
        public bool? MnsOrdrCd { get; set; }
        public bool CrdlepntCd { get; set; }

        // Additional
        public bool IsSuppressedEmails { get; set; }

        public int AuthStatus { get; set; }
        public byte EventCsgLvlId { get; set; }

        public int ReviewerUserId { get; set; }
        public string ReviewerComments { get; set; }
        public int ActivatorUserId { get; set; }
        public string ActivatorComments { get; set; }

        public List<int> EventSucssActyIds { get; set; }
        public List<int> EventFailActyIds { get; set; }
        public string PreCfgConfgCode { get; set; }
        public string Profile { get; set; }
        public int FailCode { get; set; }

        public bool? FrcdftCd { get; set; }


        public int Mode { get; set; }
        public int ApptId { get; set; }
        public string OdieCustId { get; set; }


        // Design Doc Detail
        public string DD { get; set; }
        public int VASCEFlg { get; set; }
        public bool? CEChngFlg { get; set; }
        public bool MdsCmpltCd { get; set; }
        public bool NtwkCmpltCd { get; set; }
        public bool VasCmpltCd { get; set; }
        public MdsSiteLookupView H6Lookup { get; set; }
        public string CntctEmailList { get; set; }

        public string MdrNumber { get; set; }
        public ICollection<ContactDetailView> ContactDetails { get; set; }

        public bool? CustLtrOptOut { get; set; }
    }

    public class ContactDetailView
    {
        public int Id { get; set; }
        public int ObjId { get; set; }
        // ObjTypCd Values: E (Event), R (Redesign), C (CPT)
        public string ObjTypCd { get; set; }
        // HierLvlCd Values: H1, H2, H4, H6
        public string HierLvlCd { get; set; }
        public string HierId { get; set; }
        public byte RoleId { get; set; }
        public string RoleNme { get; set; }
        public string EmailAdr { get; set; }
        public string PhnNbr { get; set; }
        // AutoRfrshCd Values: 1 - By default refresh this contact details from Mach5 based on Role_CD, Hier_lvl_cd and HIER_ID;
        // For MNS contacts, get the details from ODIE
        public bool? AutoRfrshCd { get; set; }
        // EmailCd Values: NULL : Copy on all Emails(based on workflow);
        // |3|4|6| : Copy on Rework, Published (this is just example);
        // |0| : Dont copy this contact
        public string EmailCd { get; set; }
        public short RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public bool SuprsEmail { get; set; }
    }
}