﻿using COWS.Entities.Enums;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace COWS.Entities.Models
{
    public partial class FedlineTadpoleDataEntity
    {
        public int CowsFedlineID { get; set; }
        public int FRBRequestID { get; set; }
        public int Version { get; set; }
        public string CustName { get; set; }
        public string CustOrgID { get; set; }
        public string DesignType { get; set; }
        public FedlineActivityType? MACType { get; set; }
        public string OrderTypeCode { get; set; }
        public string MoveType { get; set; }
        public string RelatedEvent { get; set; }
        public string InstallType { get; set; }
        public string OrderSubType { get; set; }
        public string ReorderReason { get; set; }
        public FedlineManagedDeviceEntity ManagedDeviceData { get; set; }
        public FedlineOrigDeviceEntity OrigDeviceData { get; set; }
        public FedlineSiteLocationEntity SiteInstallInfo { get; set; }
        public FedlineSiteLocationEntity SiteShippingInfo { get; set; }
        public FedlineSiteLocationEntity SiteDeactivationInfo { get; set; }
        public List<FedlineConfigEntity> Configs { get; set; }
        public FedlineConfigEntity WAN { get { return Configs.Where(a => a.PortName == "WAN1").FirstOrDefault(); } }
        public FedlineConfigEntity LAN { get { return Configs.Where(a => a.PortName == "PORT1").FirstOrDefault(); } }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? FRBSentDate { get; set; }
        public DateTime? FRBModifyDate { get; set; }
        public bool isMove { get; set; }
        public bool isExpedite { get; set; }
        public string existingDevice { get; set; }

        public string TimeBlock
        {
            get
            {
                return ((StartDate == null) ? "?? - " : string.Format("{0:hh:mm tt} - ", StartDate)) +
                    ((EndDate == null) ? "??" : string.Format("{0:hh:mm tt}", EndDate));
            }
        }

        public DateTime? ActivityDate
        {
            get
            {
                if (StartDate.HasValue)
                    return StartDate.Value.Date;
                if (EndDate.HasValue)
                    return EndDate.Value.Date;
                return null;
            }
        }

        public string DesignDisplay
        {
            get
            {
                if (DesignType == "A" || DesignType == "1")
                    return "1 - Stand Alone";
                else if (DesignType == "B" || DesignType == "2")
                    return "2 - Single Interface";
                else if (DesignType == "C" || DesignType == "3")
                    return "3 - Dual Interface";
                else
                    return "";
            }
        }

        public bool isInstallorHeadend
        {
            get
            {
                if (MACType.HasValue && (MACType.Value == FedlineActivityType.Install || isHeadend))
                    return true;
                return false;
            }
        }

        public bool isDisconnect
        {
            get
            {
                if (MACType.HasValue && MACType.Value == FedlineActivityType.Disconnect)
                    return true;
                return false;
            }
        }

        public bool isRefresh
        {
            get
            {
                if (MACType.HasValue && MACType.Value == FedlineActivityType.Refresh)
                    return true;
                return false;
            }
        }

        public bool isHeadend
        {
            get
            {
                if (MACType.HasValue && (MACType.Value == FedlineActivityType.HeadendCreate || MACType.Value == FedlineActivityType.HeadendDisconnect || MACType.Value == FedlineActivityType.HeadendInstall || MACType.Value == FedlineActivityType.HeadendMAC))
                    return true;
                return false;
            }
        }

        public FedlineTadpoleDataEntity()
        {
            ManagedDeviceData = new FedlineManagedDeviceEntity();
            OrigDeviceData = new FedlineOrigDeviceEntity();
            SiteInstallInfo = new FedlineSiteLocationEntity();
            SiteShippingInfo = new FedlineSiteLocationEntity();
            SiteDeactivationInfo = new FedlineSiteLocationEntity();
            Configs = new List<FedlineConfigEntity>();
        }

        //Deserialization constructor.
        public FedlineTadpoleDataEntity(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            CowsFedlineID = (int)info.GetValue("CowsFedlineID", typeof(int));
            FRBRequestID = (int)info.GetValue("FRBRequestID", typeof(int));
            Version = (int)info.GetValue("Version", typeof(int));
            CustName = (string)info.GetValue("CustName", typeof(string));
            CustOrgID = (string)info.GetValue("CustOrgID", typeof(string));
            DesignType = (string)info.GetValue("DesignType", typeof(string));
            MACType = (FedlineActivityType)info.GetValue("MACType", typeof(FedlineActivityType));
            OrderTypeCode = (string)info.GetValue("OrderTypeCode", typeof(string));
            MoveType = (string)info.GetValue("MoveType", typeof(string));
            RelatedEvent = (string)info.GetValue("RelatedEvent", typeof(string));
            InstallType = (string)info.GetValue("InstallType", typeof(string));
            OrderSubType = (string)info.GetValue("OrderSubType", typeof(string));
            ReorderReason = (string)info.GetValue("ReorderReason", typeof(string));
            ManagedDeviceData = (FedlineManagedDeviceEntity)info.GetValue("ManagedDeviceData", typeof(FedlineManagedDeviceEntity));
            OrigDeviceData = (FedlineOrigDeviceEntity)info.GetValue("OrigDeviceData", typeof(FedlineOrigDeviceEntity));
            SiteInstallInfo = (FedlineSiteLocationEntity)info.GetValue("SiteInstallInfo", typeof(FedlineSiteLocationEntity));
            SiteShippingInfo = (FedlineSiteLocationEntity)info.GetValue("SiteShippingInfo", typeof(FedlineSiteLocationEntity));
            SiteDeactivationInfo = (FedlineSiteLocationEntity)info.GetValue("SiteDeactivationInfo", typeof(FedlineSiteLocationEntity));
            Configs = (List<FedlineConfigEntity>)info.GetValue("Configs", typeof(List<FedlineConfigEntity>));
            StartDate = (DateTime?)info.GetValue("StartDate", typeof(DateTime?));
            EndDate = (DateTime?)info.GetValue("EndDate", typeof(DateTime?));
            FRBSentDate = (DateTime?)info.GetValue("FRBSentDate", typeof(DateTime?));
            FRBModifyDate = (DateTime?)info.GetValue("FRBModifyDate", typeof(DateTime?));
            isMove = (bool)info.GetValue("isMove", typeof(bool));
            isExpedite = (bool)info.GetValue("isExpedite", typeof(bool));
            existingDevice = (string)info.GetValue("existingDevice", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("CowsFedlineID", CowsFedlineID);
            info.AddValue("FRBRequestID", FRBRequestID);
            info.AddValue("Version", Version);
            info.AddValue("CustName", CustName);
            info.AddValue("CustOrgID", CustOrgID);
            info.AddValue("DesignType", DesignType);
            info.AddValue("MACType", MACType);
            info.AddValue("OrderTypeCode", OrderTypeCode);
            info.AddValue("MoveType", MoveType);
            info.AddValue("RelatedEvent", RelatedEvent);
            info.AddValue("InstallType", InstallType);
            info.AddValue("OrderSubType", OrderSubType);
            info.AddValue("ReorderReason", ReorderReason);
            info.AddValue("ManagedDeviceData", ManagedDeviceData);
            info.AddValue("OrigDeviceData", OrigDeviceData);
            info.AddValue("SiteInstallInfo", SiteInstallInfo);
            info.AddValue("SiteShippingInfo", SiteShippingInfo);
            info.AddValue("SiteDeactivationInfo", SiteDeactivationInfo);
            info.AddValue("Configs", Configs);
            info.AddValue("WAN", WAN);
            info.AddValue("LAN", LAN);
            info.AddValue("StartDate", StartDate);
            info.AddValue("EndDate", EndDate);
            info.AddValue("FRBSentDate", FRBSentDate);
            info.AddValue("FRBModifyDate", FRBModifyDate);
            info.AddValue("isMove", isMove);
            info.AddValue("isExpedite", isExpedite);
            info.AddValue("existingDevice", existingDevice);
        }
    }
}