﻿namespace COWS.Entities.QueryModels
{
    public class StateMachine
    {
        public int OrderId { get; set; }
        public int TaskId { get; set; }
        public int TaskStatus { get; set; }
        public string Comments { get; set; }
        public int UserId { get; set; }
        public int NoteTypeId { get; set; }
    }
}
