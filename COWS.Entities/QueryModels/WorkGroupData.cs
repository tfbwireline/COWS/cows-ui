﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public partial class WorkGroupData
    {
        [Column("ORDR_ID")]
        public int OrdrId { get; set; }
        [Column("CAT_ID")]
        public string CatId { get; set; }
        [Column("H5_FOLDR_ID")]
        public string H5FoldrId{ get; set; }
        [Column("TASK_ID")]
        public string TaskId{ get; set; }
        [Column("ORDR_ACTN_ID")]
        public string OrdrActnId{ get; set; }
        [Column("PRNT_FTN")]
        public string PrntFtn{ get; set; }
        [Column("FTN")]
        public string Ftn{ get; set; }
        [Column("H1")]
        public string H1{ get; set; }
        [Column("H5_H6_CUST_ID")]
        public string H5H6CustId{ get; set; }
        [Column("ORDR_ACTN_DES")]
        public string OrdrActnDes{ get; set; }
        [Column("CUST_NME")]
        public string CustNme{ get; set; }
        [Column("H5_H6_CUST_NME")]
        public string H5H6CustNme{ get; set; }
        [Column("ORDR_TYPE")]
        public string OrdrType{ get; set; }
        [Column("ORDR_SUB_TYPE")]
        public string OrdrSubType{ get; set; }
        [Column("PROD_TYPE")]
        public string ProdType{ get; set; }
        [Column("SRVC_SUB_TYPE")]
        public string SrvcSubType{ get; set; }
        [Column("DMSTC_CD")]
        public string DmstcCd{ get; set; }
        [Column("CUST_CMMT_DT")]
        public string CustCmmtDt{ get; set; }
        [Column("CSG_LVL_ID")]
        public string CsgLvlId{ get; set; }
        [Column("ORDR_STUS")]
        public string OrdrStus{ get; set; }
        [Column("TSUP_PRS_QOT_NBR")]
        public string TsupPrsQotNbr{ get; set; }
        [Column("SOI_CD")]
        public string SoiCd{ get; set; }
        [Column("INSTL_ESCL_CD")]
        public string InstlEsclCd{ get; set; }
        [Column("FSA_EXP_TYPE_CD")]
        public string FsaExpTypeCd{ get; set; }
        [Column("ASN_USER")]
        public string AsnUser{ get; set; }
        [Column("CHNGD_CCD")]
        public string ChngdCcd{ get; set; }
        [Column("CHNGD_CCD_ORG")]
        public string ChngdCcdOrg{ get; set; }
        [Column("VNDR_NME")]
        public string VndrNme{ get; set; }
        [Column("TTRPT_SPD_OF_SRVC_BDWD_DES")]
        public string TtrptSpdOfSrvcBdwdDes{ get; set; }
        [Column("CTY_NME")]
        public string CtyNme{ get; set; }
        [Column("STT_NME")]
        public string SttNme{ get; set; }
        [Column("CTRY_NME")]
        public string CtryNme{ get; set; }
        [Column("PLTFRM_NME")]
        public string PltfrmNme{ get; set; }
        [Column("SLA_VLTD_CD")]
        public string SlaVltdCd{ get; set; }
        [Column("PRNT_ORDR_ID")]
        public string PrntOrdrId{ get; set; }
        [Column("Exp_Sort")]
        public string ExpSort{ get; set; }
        [Column("USR_PRF_ID")]
        public int? UsrPrfId{ get; set; }
        [Column("RTSSLA")]
        public string RtsSla{ get; set; }
        [Column("RTS_FLG")]
        public string RtsFlg{ get; set; }
        [Column("RTS_DT")]
        public string RtsDt{ get; set; }
        [Column("RTS_DESC")]
        public string RtsDesc{ get; set; }
        [Column("ORDR_RCVD_DT")]
        public string OrdrRcvdDt{ get; set; }
        [Column("IS_EXP")]
        public string IsExp{ get; set; }
        [Column("ORDR_SBMT_DT")]
        public string OrdrSbmtDt{ get; set; }
        [Column("XNCI_UPDT_DT")]
        public string XnciUpdtDt{ get; set; }
        [Column("ADR")]
        public string Adr{ get; set; }
        [Column("ZIP_CD")]
        public string ZipCd { get; set; }
        [Column("TRGT_DLVRY_DT")]
        public string TrgtDlvryDt{ get; set; }
        [Column("CUST_WANT_DT")]
        public string CustWantDt{ get; set; }
        [Column("TAC_Primary")]
        public string TacPrimary{ get; set; }
        [Column("ORDR_HIGHLIGHT_CD")]
        public string OrdrHighlightCd{ get; set; }
        [Column("RTS_CCD_HIGHLIGHT_CD")]
        public string RtsCcsHighlightCd{ get; set; }
        [Column("CPE_EVENT_ID")]
        public string CpeEventId{ get; set; }
        [Column("EVENT_START_TIME")]
        public string EventStartTime{ get; set; }
        [Column("CPE_ORDR_STUS_DES")]
        public string CpeOrdrStusDes{ get; set; }
        [Column("EVENT_STUS_DES")]
        public string EventStusDes{ get; set; }
        [Column("CNTRC_TYPE_ID")]
        public string CntrcTypeId{ get; set; }
        [Column("PRCH_ORDR_NBR")]
        public string PrchOrdrNbr{ get; set; }
        [Column("ITM_STUS")]
        public string ItmStus{ get; set; }
        [Column("PLSFT_RQSTN_NBR")]
        public string PlsftRqstnNbr { get; set; }
        [Column("SITE_ID")]
        public string SiteId{ get; set; }
        [Column("JPRDY_CD")]
        public string JprdyCd{ get; set; }
        [Column("JPRDY_DES")]
        public string JprdyDes{ get; set; }
        [Column("INBOX")]
        public string Inbox{ get; set; }
        [Column("HOLD")]
        public string Hold{ get; set; }
        [Column("DLVY_CLLI")]
        public string DlvyClli{ get; set; }
        [Column("CPE_VNDR")]
        public string CpeVndr{ get; set; }
    }
}
