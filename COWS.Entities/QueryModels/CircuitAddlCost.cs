﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class CircuitAddlCost : EntityBaseModel
    {
        public bool IsTerm { get; set; }
        public DateTime? ExpirationTime { get; set; }
        public bool BillOnly { get; set; }
        public string SalesSupportStatusDes { get; set; }
        public short SalesSupportStatusID { get; set; }
        public string Note { get; set; }
        public decimal TaxRate { get; set; }
        public string CurrencyCd { get; set; }
        public short Version { get; set; }
        public int OrderID { get; set; }
        public decimal ChargeNRC { get; set; }
        public decimal ChargeNRCUSD { get; set; }
        public short ChargeTypeID { get; set; }
        public string ChargeTypeDesc { get; set; }
        public short StatusID { get; set; }
        public string StatusDesc { get; set; }
        public string StatusTypeID { get; set; }
        public string Currency { get; set; }
        public string CurrencyDesc { get; set; }
        public double Factor { get; set; }
        public bool? IsUpdated { get; set; }

        public string CreatedByDspNme { get; set; }
        public string CreatedByUserID { get; set; }
        
    }
}
