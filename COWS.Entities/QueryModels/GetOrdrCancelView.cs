﻿namespace COWS.Entities.QueryModels
{
    public class GetOrdrCancelView
    {
        public string FTN { get; set; }   /*Varchar 50*/
        public int H5_H6 { get; set; }
        public string ORDR_TYPE { get; set; }   /*Varchar 100*/
        public string PROD_TYPE { get; set; }  /*Varchar 100*/
        public string CUST_NME { get; set; } /*Varchar 1000*/
        public string CCD { get; set; } /*Varchar 10     10/29/2018   */
        public string NOTES { get; set; } /*Varchar 500   */
    }
}