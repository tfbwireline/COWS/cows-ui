﻿using COWS.Entities.Models;
using System.Runtime.Serialization;

namespace COWS.Entities.QueryModels
{
    public class FedlineEvent
    {
        public int EventID { get; set; }
        public byte EventStatusID { get; set; }
        public string EventStatus { get; set; }
        public byte WorkflowStatusID { get; set; }
        public string WorkflowStatus { get; set; }
        public short FailCodeID { get; set; }
        public string FailCode { get; set; }
        public string StatusComments { get; set; }
        public FedlineTadpoleDataEntity TadpoleData { get; set; }
        public FedlineUserDataEntity UserData { get; set; }
        public bool HeadendEditable { get; set; }

        public FedlineEvent()
        {
            TadpoleData = new FedlineTadpoleDataEntity();
            UserData = new FedlineUserDataEntity();
            HeadendEditable = false;
        }

        public bool isNewOrder
        {
            get
            {
                //if (TadpoleData.MACType.HasValue && TadpoleData.MACType.Value == EFedlineActivityType.HeadendCreate)
                //    return true;
                return false;
            }
        }

        //Deserialization constructor.
        public FedlineEvent(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            EventID = (int)info.GetValue("EventID", typeof(int));
            EventStatusID = (byte)info.GetValue("EventStatusID", typeof(byte));
            EventStatus = (string)info.GetValue("EventStatus", typeof(string));
            TadpoleData = (FedlineTadpoleDataEntity)info.GetValue("TadpoleData", typeof(FedlineTadpoleDataEntity));
            UserData = (FedlineUserDataEntity)info.GetValue("UserData", typeof(FedlineUserDataEntity));
            WorkflowStatusID = (byte)info.GetValue("WorkflowStatusID", typeof(byte));
            WorkflowStatus = (string)info.GetValue("WorkflowStatus", typeof(string));
            FailCodeID = (short)info.GetValue("FailCodeID", typeof(short));
            FailCode = (string)info.GetValue("FailCode", typeof(string));
            StatusComments = (string)info.GetValue("StatusComments", typeof(string));
            HeadendEditable = (bool)info.GetValue("HeadendEditable", typeof(bool));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("EventID", EventID);
            info.AddValue("EventStatusID", EventStatusID);
            info.AddValue("EventStatus", EventStatus);
            info.AddValue("TadpoleData", TadpoleData);
            info.AddValue("UserData", UserData);
            info.AddValue("WorkflowStatusID", WorkflowStatusID);
            info.AddValue("WorkflowStatus", WorkflowStatus);
            info.AddValue("FailCodeID", FailCodeID);
            info.AddValue("FailCode", FailCode);
            info.AddValue("StatusComments", StatusComments);
            info.AddValue("HeadendEditable", HeadendEditable);
        }
    }
}