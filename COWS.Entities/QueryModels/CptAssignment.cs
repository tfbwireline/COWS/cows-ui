﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public class CptAssignment
    {
        public int CptId { get; set; }
        public string CptCustNumber { get; set; }
        public int CptStusId { get; set; }
        public string CustShortName { get; set; }
        public string CompanyName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CustTypeId { get; set; }
        public string CustTypeName { get; set; }
        public int SrvcTierId { get; set; }
        public string SrvcTierName { get; set; }
        public bool IsGatekeeperNeeded { get; set; }
        public int GatekeeperId { get; set; }
        public string GatekeeperName { get; set; }
        public bool IsManagerNeeded { get; set; }
        public int ManagerId { get; set; }
        public string ManagerName { get; set; }
        public bool IsPmNeeded { get; set; }
        public int PmId { get; set; }
        public string PmName { get; set; }
        public bool ReturnedToSde { get; set; }
    }
}
