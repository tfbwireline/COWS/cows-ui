﻿using System;

namespace COWS.Entities.QueryModels
{
    public class InsertWFMUserAssignments
    {
        public int UserId { get; set; }
        public short GrpId { get; set; }
        public byte? WfmAsmtLvlId { get; set; }
        public string RoleList { get; set; }
        public string OrderActionList { get; set; }
        public string ProdPlatList { get; set; }
        public string CountryOrigList { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int UsrPrfId { get; set; }
    }
}