﻿using COWS.Entities.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class NidSerialView
    {
        [Column("h6")]
        public string H6 { get; set; }
        [Column("nidactyid")]
        public int NidActyId { get; set; }
        [Column("nidserialnbr")]
        public string NidSerialNbr { get; set; }
        [Column("nidhostname")]
        public string NidHostname { get; set; }
        [Column("nidipadr")]
        public string NidIpAdr { get; set; }
    }
}