﻿using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class OrderBasic
    {
        [Column("ORDR_ID")]
        public int OrdrId { get; set; }
        [Column("ORDR_CAT_ID")]
        public int OrdrCatId { get; set; }
        [Column("ORDR_LBL_1")]
        public string OrdrLbl1 { get; set; }
        [Column("ORDR_VAL_1")]
        public string OrdrVal1 { get; set; }
        [Column("INSTL_VNDR_CD")]
        public string InstlVndrCd { get; set; }
        [Column("VNDR_VPN_CD")]
        public string VndrVpnCd { get; set; }
        [Column("CXR_ACCS_CD")]
        public string CxrAccsCd { get; set; }
        [Column("PROD_TYPE_ID")]
        public int ProdTypeId { get; set; }
        [Column("PROD_TYPE_DES")]
        public string ProdTypeDes { get; set; }
        [Column("ORDR_TYPE_ID")]
        public int OrdrTypeId { get; set; }
        [Column("ORDR_TYPE_DES")]
        public string OrdrTypeDes { get; set; }
        [Column("PLTFRM_CD")]
        public string PltfrmCd { get; set; } 
        [Column("PLTFRM_NME")]
        public string PltfrmNme { get; set; } 
    }
}
