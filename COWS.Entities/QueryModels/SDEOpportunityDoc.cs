﻿using COWS.Entities.Enums;

namespace COWS.Entities.QueryModels
{
    public partial class SDEOpportunityDoc : EntityBaseModel
    {
        public int SdeOpptntyDocId { get; set; }
        public int SdeDocID { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public int FileSizeQuantity { get; set; }
        public ERecStatus RecordStatus { get; set; }
    }
}