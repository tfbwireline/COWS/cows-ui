﻿namespace COWS.Entities.QueryModels
{
    public partial class SiptEncryptContactModel
    {
        public string TeamPdlNme { get; set; }
        public string StreetAdr { get; set; }
        public string EventTitleTxt { get; set; }
        public string CustNme { get; set; }
        public string SiteCntctNme { get; set; }
        public string SiteCntctPhnNbr { get; set; }
        public string SiteCntctHrNme { get; set; }
        public string SiteCntctEmailAdr { get; set; }
        public string SiteAdr { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtyNme { get; set; }
        public string SttPrvnNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string ZipCd { get; set; }
    }
}