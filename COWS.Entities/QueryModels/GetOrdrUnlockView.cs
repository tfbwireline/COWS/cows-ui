﻿namespace COWS.Entities.QueryModels
{
    public class GetOrdrUnlockView
    {
        public int ORDR_ID { get; set; }   /* for Ordr/Event its integer  for Redsgn/CPT  its string  so convert to int that time*/
        public int LOCK_BY_USER_ID { get; set; }
        public string FULL_NME { get; set; }   /*Varchar 100*/
        public string FTN { get; set; }  /*Varchar 50*/
        public string PROD_TYPE_DES { get; set; } /*Varchar 100*/
        public string ORDR_TYPE_DES { get; set; } /*Varchar 100*/
    }
}