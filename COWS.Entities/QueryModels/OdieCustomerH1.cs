﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public partial class OdieCustomerH1
    {
        public string Customer { get; set; }
        public string H1 { get; set; }
    }
}
