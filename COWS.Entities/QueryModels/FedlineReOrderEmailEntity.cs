﻿namespace COWS.Entities.QueryModels
{
    public class FedlineReOrderEmailEntity
    {
        public string SerialNum { get; set; }
        public string DeviceName { get; set; }
        public string OrdrId { get; set; }
        public string OrdrTyp { get; set; }
        public string EventId { get; set; }
    }
}