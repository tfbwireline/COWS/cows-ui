﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class LogicallisData
    {
        [Column("Customer")]
        public string Customer { get; set; }
        [Column("Address1")]
        public string Address1 { get; set; }
        [Column("Address2")]
        public string Address2 { get; set; }
        [Column("Address3")]
        public string Address3 { get; set; }
        [Column("CITY")]
        public string City { get; set; }
        [Column("COUNTRY")]
        public string Country { get; set; }
        [Column("PRVN")]
        public string Province { get; set; }
        [Column("STATE")]
        public string State { get; set; }
        [Column("ZIP")]
        public string Zip { get; set; }
        [Column("BLDG")]
        public string Building { get; set; }
        [Column("FLOOR")]
        public string Floor { get; set; }
        [Column("ROOM")]
        public string Room { get; set; }
        [Column("SiteContact")]
        public string SiteContact { get; set; }
        [Column("SiteOfficePhone")]
        public string SiteOfficePhone { get; set; }
        [Column("SiteMobilePhone")]
        public string SiteMobilePhone { get; set; }
        [Column("Siteemail")]
        public string SiteEmail { get; set; }
        [Column("OriginalPO")]
        public string OriginalPO { get; set; }
        [Column("DsiconnectionDate")]
        public string DisconnectionDate { get; set; }
    }
}