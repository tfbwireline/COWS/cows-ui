﻿using System;

namespace COWS.Entities.QueryModels
{
    public class GetSdeView
    {
        public int SDEOpportunityID { get; set; }

        public int StatusID { get; set; }

        public string StatusDesc { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime DisplayDateTime { get; set; }
        public string CreatedByFullName { get; set; }
        public int CreatedByUserID { get; set; }
        public int AssignedToID { get; set; }
        public string AssignedTo { get; set; }
    }
}