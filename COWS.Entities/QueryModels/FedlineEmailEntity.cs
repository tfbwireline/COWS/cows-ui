﻿namespace COWS.Entities.QueryModels
{
    public class FedlineEmailEntity
    {
        public string AssignedActivator { get; set; }
        public string ScheduledEventDateTime { get; set; }
        public string EventID { get; set; }
        public string EventTitle { get; set; }
        public string CustomerName { get; set; }
        public string MDSActivityType { get; set; }
        public string InstallSitePOC { get; set; }
        public string InstallSitePOCPhone { get; set; }
        public string InstallSiteBackupPOCPhone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State_Province { get; set; }
        public string Country_Region { get; set; }
        public string Zip { get; set; }
        public string US_International { get; set; }
        public string EventLink { get; set; }
        public string CalendarEntryURL { get; set; }
    }
}