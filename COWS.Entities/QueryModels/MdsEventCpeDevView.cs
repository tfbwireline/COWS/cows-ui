﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class MdsEventCpeDevView
    {
        public int EventCpeDevId { get; set; }
        public int EventId { get; set; }
        public string CpeOrdrId { get; set; }
        public string DeviceId { get; set; }
        public string AssocH6 { get; set; }
        public string MaintVndrPo { get; set; }
        public string ReqstnNbr { get; set; }
        public DateTime? Ccd { get; set; }
        public string OdieDevNme { get; set; }
        public string OrdrStus { get; set; }
        public string RcptStus { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public bool? OdieCd { get; set; }
        public string CpeOrdrNbr { get; set; }
        public DateTime? ModfdDt { get; set; }

        // Additional
        public string DlvyClli { get; set; }

        public string CpeCmpntFmly { get; set; }
        public int? OrdrId { get; set; }
    }
}