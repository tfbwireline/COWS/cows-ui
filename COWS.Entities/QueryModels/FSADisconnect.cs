﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public class FSADisconnect
    {
        public string RoleName { get; set; }

        public string ContactType { get; set; }

        public string FrstNme { get; set; }

        public string LstNme { get; set; }

        public string Nme { get; set; }

        public string EmailAdr { get; set; }

        public string IsdCd { get; set; }

        public string StnNbr { get; set; }

        public string CtyCd { get; set; }

        public string Npa { get; set; }

        public string Nxx { get; set; }

        public string PhnNbr { get; set; }

    }
}
