﻿namespace COWS.Entities.QueryModels
{
    public class InstallPort
    {
        public string M5PortStatus { get; set; }
        public string TtrptSpdOfSrvcBdwdDes { get; set; }
        public string TportEthAccsTypeCd { get; set; }
        public string SpaAccsIndcrDes { get; set; }
        public string TportVndrQuoteId { get; set; }
        public string TportEthrntNrfcIndcr { get; set; }
        public string TportCnctrTypeId { get; set; }
        public string CustRoutrTagTxt { get; set; }
        public string CustRoutrAutoNegotCd { get; set; }
        public string IpVerTypeCd { get; set; }
        public string Ipv4AdrPrvdrCd { get; set; }
        public string Ipv4AdrQty { get; set; }
        public string Ipv6AdrPrvdrCd { get; set; }
        public string Ipv6AdrQty { get; set; }
        public string VlanQty { get; set; }
        public string DiaNocToNocTxt { get; set; }
        public string ProjDes { get; set; }
        public string MdsDes { get; set; }
    }
}
