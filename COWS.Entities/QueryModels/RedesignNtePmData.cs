﻿namespace COWS.Entities.QueryModels
{
    public class RedesignNtePmData
    {
        public string RedsgnNbr { get; set; }
        public string NteAssigned { get; set; }
        public int NteAssignedUserId { get; set; }
        public string NteAssignedEmail { get; set; }
        public string PmAssigned { get; set; }
        public int PmAssignedUserId { get; set; }
        public string PmAssignedEmail { get; set; }
    }
}
