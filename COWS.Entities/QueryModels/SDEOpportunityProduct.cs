﻿namespace COWS.Entities.QueryModels
{
    public partial class SDEOpportunityProduct
    {
        public int ProductTypeID { get; set; }
        public string ProductType { get; set; }
        public string ProductTypeDesc { get; set; }
        public int Qty { get; set; }
    }
}