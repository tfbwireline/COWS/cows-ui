﻿using COWS.Entities.Models;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public class MdsEventFormChngComparer : IEqualityComparer<MdsEventFormChng>
    {
        public bool Equals(MdsEventFormChng obj1, MdsEventFormChng obj2)
        {
            // Check whether the compared object references the same data.
            if (object.ReferenceEquals(obj1, obj2)) return true;

            // Check whether the products' properties are equal.
            if ((MdsEventFormChng)obj1 == null)
                return false;
            else if ((MdsEventFormChng)obj2 == null)
                return false;
            else
            {
                MdsEventFormChng dc1 = (MdsEventFormChng)obj1;
                MdsEventFormChng dc2 = (MdsEventFormChng)obj2;
                if (dc1.MdsEventId == dc2.MdsEventId
                    && dc1.ChngTypeNme == dc2.ChngTypeNme
                    && dc1.CreatDt == dc2.CreatDt)
                    return true;
                else
                    return false;
            }
        }

        // If Equals returns true for a pair of objects,
        // GetHashCode must return the same value for these objects.

        public int GetHashCode(MdsEventFormChng obj)
        {
            int hashMdsEventId = obj.MdsEventId == null ? 0 : obj.MdsEventId.GetHashCode();
            int hashChngTypeNme = obj.ChngTypeNme == null ? 0 : obj.ChngTypeNme.GetHashCode();
            int hashCreatDt = obj.CreatDt == null ? 0 : obj.CreatDt.GetHashCode();
            // Calculate the hash code for the product.
            return hashMdsEventId ^ hashChngTypeNme ^ hashCreatDt;
        }
    }
}
