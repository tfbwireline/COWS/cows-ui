﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public class CSGLevel
    {
        public byte CsgLevelID { get; set; }
        public string Description { get; set; }
    }
}
