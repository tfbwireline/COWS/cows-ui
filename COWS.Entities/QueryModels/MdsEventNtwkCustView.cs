﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class MdsEventNtwkCustView
    {
        public int MdsEventNtwkCustId { get; set; }
        public int EventId { get; set; }
        public string InstlSitePocNme { get; set; }
        public string InstlSitePocPhn { get; set; }
        public string InstlSitePocEmail { get; set; }
        public string RoleNme { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string AssocH6 { get; set; }
        public string Guid { get; set; }
    }
}