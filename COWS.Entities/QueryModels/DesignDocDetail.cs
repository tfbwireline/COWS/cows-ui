﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public partial class DesignDocDetail
    {
        public IEnumerable<DesignDocDetailTable1> Table1 { get; set; }
        public IEnumerable<DesignDocDetailTable2> Table2 { get; set; }
        public IEnumerable<DesignDocDetailTable3> Table3 { get; set; }
        public IEnumerable<DesignDocDetailTable4> Table4 { get; set; }
    }

    public class DesignDocDetailTable1
    {
        [Column("SITEID")]
        public string SiteId { get; set; }
        [Column("H1")]
        public string H1 { get; set; }
        [Column("H1_CUST_NME")]
        public string H1_CUST_NME { get; set; }
        [Column("H6")]
        public string H6 { get; set; }
        [Column("CustomerName")]
        public string CustomerName { get; set; }
        [Column("CSG_LVL_ID")]
        public int? CsgLvlId { get; set; }
    }
    public class DesignDocDetailTable2
    {
        [Column("H6")]
        public string H6 { get; set; }
        [Column("CustomerContactName")]
        public string CustomerContactName { get; set; }
        [Column("CustomerContactPhone")]
        public string CustomerContactPhone { get; set; }
        [Column("CustomerEmail")]
        public string CustomerEmail { get; set; }
        [Column("RoleNme")]
        public string RoleName { get; set; }
    }
    public class DesignDocDetailTable3
    {
        [Column("DD_NBR")]
        public string DdNbr { get; set; }

        [Column("ACCT_ROLE_NME")]
        public string AcctRoleNme { get; set; }

        [Column("WRK_PHN_NBR")]
        public string WrkPhnNbr { get; set; }

        [Column("EMAIL_ADR")]
        public string EmailAdr { get; set; }

        [Column("PROD_ID")]
        public string ProdId { get; set; }

        [Column("ACCS_VNDR_NME")]
        public string AccsVndrNme { get; set; }

        [Column("FTN_NBR")]
        public string FtnNbr { get; set; }

        [Column("H6_H5_ID")]
        public string H6H5Id { get; set; }

        [Column("SCA_NBR")]
        public string ScaNbr { get; set; }

        [Column("VLAN_ID")]
        public string VlanId { get; set; }

        [Column("ADR_FMLY_TYPE_NME")]
        public string AdrFmlyTypeNme { get; set; }

        [Column("GRP_NME")]
        public string GrpNme { get; set; }

        [Column("NUA_ADR")]
        public string NuaAdr { get; set; }

        [Column("LOC_CITY")]
        public string LocCity { get; set; }

        [Column("LOC_STT")]
        public string LocStt { get; set; }

        [Column("LOC_CTRY")]
        public string LocCtry { get; set; }

        [Column("SUB_TYPE")]
        public string SubType { get; set; }

        [Column("VAS_CD")]
        public string VasCd { get; set; }

        [Column("CE_SRVC_ID")]
        public string CeSrvcId { get; set; }

        [Column("NID_HOST_NME")]
        public string NidHostNme { get; set; }

        [Column("NID_SERIAL_NBR")]
        public string NidSerialNbr { get; set; }

        [Column("NID_IP_ADR")]
        public string NidIpAdr { get; set; }

        [Column("BDWD_NME")]
        public string BdwdNme { get; set; }

        [Column("CUST_NME")]
        public string CustNme { get; set; }

        [Column("TASK_ID")]
        public int TaskId{ get; set; }

        [Column("TASK_NME")]
        public string TaskName { get; set; }

        [Column("DMSTC_CD")]
        public string DmstcCd { get; set; }

    }
    public class DesignDocDetailTable4
    {
        [Column("CE_SRVC_ID")]
        public int CeSrvcId { get; set; }
        [Column("EVENT_ID")]
        public int EventId { get; set; }
    }
}
