﻿using System;

namespace COWS.Entities.QueryModels
{
    public class NRMNotes
    {
        public DateTime CreateDate { get; set; }
        public string Note { get; set; }
        public string NoteTypeDesc { get; set; }
    }
}