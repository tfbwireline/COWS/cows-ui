﻿using System;

namespace COWS.Entities.QueryModels
{
    public partial class MdsEventSiteSrvcView
    {
        public int MdsEventSiteSrvcId { get; set; }
        public int EventId { get; set; }
        public string Mach5SrvcOrdrId { get; set; }
        public short SrvcTypeId { get; set; }
        public short? ThrdPartyVndrId { get; set; }
        public string ThrdPartyId { get; set; }
        public short? ThrdPartySrvcLvlId { get; set; }
        public DateTime? ActvDt { get; set; }
        public string CmntTxt { get; set; }
        public bool EmailCd { get; set; }
        public DateTime CreatDt { get; set; }
        public string OdieDevNme { get; set; }
        public string M5OrdrNbr { get; set; }
    }
}