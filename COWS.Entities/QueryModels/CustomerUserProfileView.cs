﻿using System;

namespace COWS.Entities.QueryModels
{
    public class CustomerUserProfileView
    {
        public int usrH5Id { get; set; }
        public int custID { get; set; }
        public string custNme { get; set; }
        public string crtyNme { get; set; }
        public string usrNme { get; set; }
        public int userId { get; set; }
        public string mns { get; set; }
        public string isip { get; set; }
        public string status { get; set; }
        public int mnsCd { get; set; }
        public int isipCd { get; set; }
        public int statusId { get; set; }
        public DateTime? assignDT { get; set; }
        //public int ConfigID { get; set; }
        //public string UserADID { get; set; }   /*Varchar 10*/
        //public string UserName { get; set; }   /*Varchar 100*/
        //public int CUST_ID
        //{
        //    get;
        //    set;
        //}

        //public string CUST_NME
        //{
        //    get;
        //    set;
        //}

        //public string CRTY_NME
        //{
        //    get;
        //    set;
        //}

        //public string User_NME
        //{
        //    get;
        //    set;
        //}

        //public string MNS
        //{
        //    get;
        //    set;
        //}

        //public string ISIP
        //{
        //    get;
        //    set;
        //}

        //public string Status
        //{
        //    get;
        //    set;
        //}

        //public DateTime? AssignDT
        //{
        //    get;
        //    set;
        //}
    }
}