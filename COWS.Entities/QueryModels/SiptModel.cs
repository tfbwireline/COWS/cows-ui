﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public partial class SiptModel
    {
        public int EventId { get; set; }
        public string M5OrdrNbr { get; set; }
        public string H1 { get; set; }
        public string SiteId { get; set; }
        public string H6 { get; set; }
        public byte EventCsgLvlId { get; set; }
        public string CharsId { get; set; }
        public string TeamPdlNme { get; set; }
        public string SiptDocLocTxt { get; set; }
        public DateTime? CustReqStDt { get; set; }
        public DateTime? CustReqEndDt { get; set; }
        public string WrkDes { get; set; }
        public string ReviewCmntTxt { get; set; }
        public int? ReqorUserId { get; set; }
        public int? NtwkEngrId { get; set; }
        public int? NtwkTechEngrId { get; set; }
        public int? PmId { get; set; }
        public byte EventStusId { get; set; }
        public byte WrkflwStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public short SiptProdTypeId { get; set; }
        public bool? TnsTgCd { get; set; }
        public string PrjId { get; set; }
        public DateTime? FocDt { get; set; }
        public string UsIntlCd { get; set; }
        public string SiptDesgnDoc { get; set; }
        public string StreetAdr { get; set; }
        public string EventTitleTxt { get; set; }
        public string CustNme { get; set; }
        public string SiteCntctNme { get; set; }
        public string SiteCntctPhnNbr { get; set; }
        public string SiteCntctHrNme { get; set; }
        public string SiteCntctEmailAdr { get; set; }
        public string SiteAdr { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtyNme { get; set; }
        public string SttPrvnNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string ZipCd { get; set; }

        //public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;

        //public int CreatByUserId { get; set; }
        //public int? ModfdByUserId { get; set; }
        public string CreatedByAdId { get; set; }

        public string ModifiedByAdId { get; set; }
        public string CreatedByDisplayName { get; set; }
        public string ModifiedByDisplayName { get; set; }

        //public DateTime? ModfdDt { get; set; }
        //public DateTime CreatDt { get; set; }
        //public int? SowsEventId { get; set; }
        //public List<MplsAccessBandwidthViewModel> MplsEventAccsTag { get; set; }
        public List<int> SiptEventActyIds { get; set; }

        public List<int> SiptEventTollTypeIds { get; set; }
        public List<int> SiptReltdOrdrIds { get; set; }
        public List<string> SiptReltdOrdrNos { get; set; }
        public List<SiptEventDoc> SiptEventDoc { get; set; }
    }
}