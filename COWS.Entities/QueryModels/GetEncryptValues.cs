﻿namespace COWS.Entities.QueryModels
{
    public class GetEncryptValues
    {
        public byte[] Item { get; set; }
    }
}