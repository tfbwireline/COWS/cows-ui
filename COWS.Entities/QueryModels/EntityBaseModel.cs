﻿using System;

namespace COWS.Entities.QueryModels
{
    public class EntityBaseModel
    {
        public byte? RecStatusId { get; set; }
        public bool RecStatus => RecStatusId == 1 ? true : false;
        public int CreatedByUserId { get; set; }
        public string CreatedByUserName { get; set; }
        public string CreatedByFullName { get; set; }
        public string CreatedByEmail { get; set; }
        public string CreatedByDisplayName { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int? ModifiedByUserId { get; set; }
        public string ModifiedByUserName { get; set; }
        public string ModifiedByFullName { get; set; }
        public string ModifiedByDisplayName { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
    }
}