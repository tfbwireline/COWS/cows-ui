﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public class NccoOrderView
    {
        public int OrdrId { get; set; }
        public string SiteCityNme { get; set; }
        public string CtryCd { get; set; }
        public int? H5AcctNbr { get; set; }
        public string SolNme { get; set; }
        public string OeNme { get; set; }
        public string SotsNbr { get; set; }
        public string BillCycNbr { get; set; }
        public byte ProdTypeId { get; set; }
        public byte OrdrTypeId { get; set; }
        public DateTime? CwdDt { get; set; }
        public DateTime? CcsDt { get; set; }
        public string EmailAdr { get; set; }
        public string CmntTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public string PlNbr { get; set; }
        public string OrdrByLassieCd { get; set; }
        public string VndrCd { get; set; }
        public string CustNme { get; set; }
        public string ProdType { get; set; }
        public string OrdrType { get; set; }
        public string Ctry { get; set; }
        public string OrdrStatus { get; set; }
        public byte CsgLvlId { get; set; }
        public List<int> PLNumberId { get; set; }
        public List<string> PLNumber { get; set; }
    }
}