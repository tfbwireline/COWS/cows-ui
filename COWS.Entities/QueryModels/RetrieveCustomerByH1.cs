﻿namespace COWS.Entities.QueryModels
{
    public class RetrieveCustomerByH1
    {
        public string CUST_CHARS_ID { get; set; }
        public string SCTY_GRP_CD { get; set; }
        public string RLAT_KEY_ID { get; set; }
    }
}