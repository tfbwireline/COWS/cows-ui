﻿namespace COWS.Entities.QueryModels
{
    public partial class SDEOpportunityNote : EntityBaseModel
    {
        public int SdeNoteID { get; set; }
        public string NoteText { get; set; }
    }
}