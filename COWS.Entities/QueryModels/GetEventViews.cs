﻿namespace COWS.Entities.QueryModels
{
    public class GetEventViews
    {
        public int DSPL_VW_ID { get; set; }
        public string DSPL_VW_NME { get; set; }
        public string PBLC_VW_CD { get; set; }
        public string DFLT_VW_CD { get; set; }
        public byte SEQ_NBR { get; set; }
    }
}