﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public partial class GetM5CPEOrderDataResults
    {
        public IEnumerable<GetM5CPEOrderDataResult1> Result1 { get; set; }
        public IEnumerable<GetM5CPEOrderDataResult2> Result2 { get; set; }
        public IEnumerable<GetM5CPEOrderDataResult3> Result3 { get; set; }
    }

    public partial class GetM5CPEOrderDataResult1
    {
        public string DevID { get; set; }
        public string OdieDevNme { get; set; }
    }

    public partial class GetM5CPEOrderDataResult2
    {
        public string M5CPEOrdrNbr { get; set; }
        public string DevID { get; set; }
        public int OrderQty { get; set; }
        public string Descrption { get; set; }
        public string OrdrExcp { get; set; }
        public string CntrctType { get; set; }
        public string VndrPO { get; set; }
        public string EquipRcvdDt { get; set; }
        public string CPECmpntFmly { get; set; }
        public string RcptStus { get; set; }
        public string RecordOnly { get; set; }
    }

    public partial class GetM5CPEOrderDataResult3
    {
        public string CPE_DEVICE_ID { get; set; }
        public string ORDER_NBR { get; set; }
        public string NME { get; set; }
        public DateTime INST_DT { get; set; }
    }
}