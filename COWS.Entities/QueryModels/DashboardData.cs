﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COWS.Entities.QueryModels
{
    public class DashboardData
    {
        //the procedure returns 4 datasets
        //MRC-NRC 1. MonthYear	AMNCIMRC	AMNCINRC	ENCIMRC	ENCINRC	ANCIMRC	ANCINRC
        //AvgInstallation 2. MonthYear	AverageInstallationInterval
        /*New Orders 3, Install Orders 4. MonthYear	AMNCI	ENCI	ANCI */

        public IEnumerable<MRCNRCData> MRCNRC { get; set; } = new List<MRCNRCData>();
        public IEnumerable<AverageInstallationData> AverageInstallations { get; set; } = new List<AverageInstallationData>();
        public IEnumerable<OrderData> NewOrders { get; set; } = new List<OrderData>();
        public IEnumerable<OrderData> InstallOrders { get; set; } = new List<OrderData>();

    }

    public class AverageInstallationData
    {
        public string MonthYear { get; set; }
        public int AverageInstallationInterval { get; set; }
    }

    public class MRCNRCData
    {
        public string MonthYear { get; set; }
        public int AMNCIMRC { get; set; }
        public int AMNCINRC { get; set; }
        public int ENCINRC { get; set; }
        public int ANCIMRC { get; set; }
        public int ANCINRC { get; set; }

    }

    public class OrderData
    {
        public string MonthYear { get; set; }
        public int AMNCI { get; set; }
        public int ENCI { get; set; }
        public int ANCI { get; set; }
    }
}
