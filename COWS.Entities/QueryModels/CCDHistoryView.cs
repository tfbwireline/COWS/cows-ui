﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{

    public class CCDHistory
    {
        private List<CCDReasons> _reasons = new List<CCDReasons>();
        private string _reasonCSV = string.Empty;
        private string _reasonPipeSV = string.Empty;
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int NoteID { get; set; }
        public string Note { get; set; }
        public DateTime CCDOld { get; set; }
        public DateTime CCDNew { get; set; }
        public DateTime VndrSent { get; set; }
        public DateTime VndrAck { get; set; }
        public int FTN { get; set; }
        public List<CCDReasons> Reasons
        {
            get { return _reasons; }
            set { _reasons = value; }
        }
        public string ReasonCSV {
            get
            {
                if (_reasonCSV.Equals(string.Empty))
                    SetReasonCSV();
                return _reasonCSV;
            }
        }
        public string ReasonPipeSV
        {
            get
            {
                if (_reasonPipeSV.Equals(string.Empty))
                    SetReasonPSV();
                return _reasonPipeSV;
            }
        }
        public DateTime CreatedDateTime { get; set; }
        public string CreatedByDspNme { get; set; }
        public string CreatedByUserName { get; set; }
        public void SetReasonCSV()
        {
            foreach (CCDReasons cr in _reasons)
            {
                _reasonCSV = _reasonCSV + cr.CCDMRDes + ",";
            }

            if (_reasonCSV.Length > 0)
                _reasonCSV = _reasonCSV.Substring(0, _reasonCSV.Length - 1);
        }
        public void SetReasonPSV()
        {
            foreach (CCDReasons cr in _reasons)
            {
                _reasonPipeSV = _reasonPipeSV + cr.CCDMRDes + "|";
            }

            if (_reasonPipeSV.Length > 0)
                _reasonPipeSV = _reasonPipeSV.Substring(0, _reasonPipeSV.Length - 1);
        }
    }

    public class CCDReasons
    {
        public short CCDMRID { get; set; }
        public string CCDMRDes { get; set; }
    }
}