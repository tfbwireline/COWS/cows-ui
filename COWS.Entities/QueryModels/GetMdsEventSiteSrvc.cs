﻿namespace COWS.Entities.Models
{
    public partial class GetMdsEventSiteSrvc
    {
        public string Mach5SrvcID { get; set; }
        public string M5OrdrNbr { get; set; }
        public string SrvcTypeID { get; set; }
        public string SrvcType { get; set; }
        public string ThirdPartyVndrID { get; set; }
        public string ThirdPartyVndr { get; set; }
        public string ThirdParty { get; set; }
        public string ThirdPartySrvcLvlID { get; set; }
        public string ThirdPartySrvcLvl { get; set; }
        public string ActvnDt { get; set; }
        public string Comments { get; set; }
        public string ODIEDevNme { get; set; }
        public bool EmailFlg { get; set; }
    }
}