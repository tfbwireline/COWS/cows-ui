﻿using System.Runtime.Serialization;

namespace COWS.Entities.QueryModels
{
    public class FedlineOrigDeviceEntity
    {
        public string DeviceName { get; set; }
        public string SerialNum { get; set; }
        public int? OrigReqID { get; set; }
        public string Model { get; set; }

        public FedlineOrigDeviceEntity()
        {
        }

        //Deserialization constructor.
        public FedlineOrigDeviceEntity(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            DeviceName = (string)info.GetValue("DeviceName", typeof(string));
            SerialNum = (string)info.GetValue("SerialNum", typeof(string));
            OrigReqID = (int?)info.GetValue("OrigReqID", typeof(int));
            Model = (string)info.GetValue("Model", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("DeviceName", DeviceName);
            info.AddValue("SerialNum", SerialNum);
            info.AddValue("OrigReqID", OrigReqID);
            info.AddValue("Model", Model);
        }
    }
}