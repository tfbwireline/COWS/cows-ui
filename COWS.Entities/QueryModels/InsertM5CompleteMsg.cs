﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class InsertM5CompleteMsg
    {
        //[Column("FTN")]
        public string FTN { get; set; }
        public int MsgType { get; set; }
    }
}
