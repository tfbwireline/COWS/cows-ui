﻿using System;
using System.Collections.Generic;

namespace COWS.Entities.QueryModels
{
    public partial class GetM5EventDatabyH6Results
    {
        public IEnumerable<M5CpeTableResult> M5CpeTable { get; set; }
        public IEnumerable<M5CustTableResult> M5CustTable { get; set; }
        public IEnumerable<M5AdrTableResult> M5AdrTable { get; set; }
        public IEnumerable<M5MnsTableResult> M5MnsTable { get; set; }
        public IEnumerable<M5RelMnsTableResult> M5RelMnsTable { get; set; }
        public IEnumerable<M5DslsbicTableResult> M5DslsbicTable { get; set; }
        public IEnumerable<M5CpeAsasTableResult> M5CpeAsasTable { get; set; }
        public IEnumerable<M5PortTableResult> M5PortTable { get; set; }
        public IEnumerable<NrmWiredTableResult> NrmWiredTable { get; set; }
        public IEnumerable<M5H1BasedOnH6Result> M5H1BasedOnH6 { get; set; }
        public IEnumerable<CPEDispatchEmailResult> CPEDispatchEmail { get; set; }
    }

    public partial class M5CpeTableResult
    {
        public string M5OrdrNbr { get; set; }
        public int CPEOrdrID { get; set; }
        public int? OrdrID { get; set; }
        public string DeviceID { get; set; }
        public string AssocH6 { get; set; }
        public string MntVndrPO { get; set; }
        public string RqstnNbr { get; set; }
        public string CCD { get; set; } = string.Empty;
        public string CmpntStus { get; set; } = string.Empty;
        public string RcptStus { get; set; } = string.Empty;
        public string DLVY_CLLI { get; set; } = string.Empty;
        public string CPE_CMPN_FMLY { get; set; } = string.Empty;
        public string NID_SERIAL_NBR { get; set; } = string.Empty;
    }

    public partial class M5CustTableResult
    {
        public string InstallSitePOCName { get; set; }
        public string InstallSitePOCPhn { get; set; }
        public string InstallSitePOCPhnCD { get; set; }
        public string SrvcAssurncePOCName { get; set; }
        public string SrvcAssurncePOCPhn { get; set; }
        public string SrvcAssurncePOCPhnCD { get; set; }
        public string SrvcAssuranceTimeZone { get; set; } = string.Empty;
        public string SrvcAssuranceAvlbltyHrs { get; set; } = string.Empty;
    }

    public partial class M5AdrTableResult
    {
        public string SiteID { get; set; }
        public string StAddr { get; set; }
        public string FlrBldg { get; set; }
        public string ProvState { get; set; }
        public string City { get; set; }
        public string ZipCD { get; set; }
        public string Ctry { get; set; }
    }

    public partial class M5MnsTableResult
    {
        public string M5_ORDR_NBR { get; set; }
        public string M5_ORDR_ID { get; set; }
        public string CUST_ACCT_ID { get; set; }
        public string DEVICE_ID { get; set; }
        public string ORDR_CMPNT_ID { get; set; }
        public string RLTD_CMPNT_ID { get; set; }
        public string MNS_SRVC_TIER { get; set; }
        public Int16 MDS_SRVC_TIER_ID { get; set; }
        public string ASSOC_TRNSPRT_TYPE { get; set; }
        public string ACCS_CXR_NME { get; set; }
        public string CMPNT_STUS { get; set; }
        public string CMPNT_TYPE_CD { get; set; }
        public string NME { get; set; }
    }

    public partial class M5RelMnsTableResult
    {
        public string M5_ORDR_NBR { get; set; }
        public string M5_ORDR_ID { get; set; }
        public string CUST_ACCT_ID { get; set; }
        public string DEVICE_ID { get; set; }
        public string ORDR_CMPNT_ID { get; set; }
        public string RLTD_CMPNT_ID { get; set; }
        public string MNS_SRVC_TIER { get; set; }
        public Int16 MDS_SRVC_TIER_ID { get; set; }
        public string CMPNT_STUS { get; set; }
        public string CMPNT_TYPE_CD { get; set; }
        public string NME { get; set; }
    }

    public partial class M5DslsbicTableResult
    {
        public string M5_ORDR_NBR { get; set; }
        public string M5_ORDR_ID { get; set; }
        public string ORDR_CMPNT_ID { get; set; }
        public string TRNSPRT_ACCT_NBR { get; set; }
        public string TRNSPRT_TYPE { get; set; }
        public string CMPNT_STUS { get; set; }
    }

    public partial class M5CpeAsasTableResult
    {
        //public string M5_ORDR_NBR { get; set; }
        //public int COWS_ORDR_ID { get; set; }
        //public string ORDR_ID { get; set; }
        //public string CUST_ACCT_ID { get; set; }
        //public string DEVICE_ID { get; set; }
        //public string ASSOC_H6 { get; set; }
        //public string VNDR_PO { get; set; }
        //public string RQSTN_NBR { get; set; }
        //public string CTRCT_TYPE { get; set; }
        //public string CCD { get; set; }
        //public string CMPNT_STUS { get; set; }
        //public string RCPT_STUS { get; set; }
        //public string SITE_ID { get; set; }
        //public string DLVY_CLLI { get; set; }
        //public string CPE_CMPN_FMLY { get; set; }
        //public string CPE_MFR_PART_NBR { get; set; }
        public string M5_ORDR_NBR { get; set; }

        public string M5_ORDR_ID { get; set; }
        public string CUST_ACCT_ID { get; set; }
        public string DEVICE_ID { get; set; }
        public string ORDR_CMPNT_ID { get; set; }
        public string RLTD_CMPNT_ID { get; set; }
        public string MNS_SRVC_TIER { get; set; }
        public Int16 MDS_SRVC_TIER_ID { get; set; }
        public string ASSOC_TRNSPRT_TYPE { get; set; }
        public string ACCS_CXR_NME { get; set; }
        public string CMPNT_STUS { get; set; }
        public string CMPNT_TYPE_CD { get; set; }
        public string NME { get; set; }
    }

    public partial class M5PortTableResult
    {
        public string M5_ORDR_NBR { get; set; }
        public string M5_ORDR_CMPNT_ID { get; set; }
        public string CUST_ACCT_ID { get; set; }
        public string NUA { get; set; }
        public string PORT_BNDWD { get; set; }
    }

    public partial class NrmWiredTableResult
    {
        public string H6_ID { get; set; }
        public string PROD_ID { get; set; }
        public string VEN_CIRCUIT_ID { get; set; }
        public string VNDR_NME { get; set; }
        public string BDWD_NME { get; set; }
        public string PLN { get; set; }
        public string SPA_NUA { get; set; }
        public string MULTILINK_CIRCUIT { get; set; }
        public string DEDCTD_ACCS_CD { get; set; }
        public string FMS_CKT_ID { get; set; }
        public string VLAN_ID { get; set; }
        public string IP_NUA { get; set; }
        public string SPRINT_MNGE_NME { get; set; }
        public string DLCI_VPI { get; set; }
        public string READY_BEGIN_FLG_NME { get; set; }
        public string OPT_IN_CKT_CD { get; set; }
        public string OPT_IN_H_CD { get; set; }
    }

    public partial class M5H1BasedOnH6Result
    {
        public string CustName { get; set; }
    }

    public partial class CPEDispatchEmailResult
    {
        public string CPEDispatchEmail { get; set; }
    }
}