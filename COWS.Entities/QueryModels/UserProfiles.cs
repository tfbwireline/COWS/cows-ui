﻿namespace COWS.Entities.QueryModels
{
    public class UserProfiles
    {
        public int UserId { get; set; }
        public short UsrPrfId { get; set; }
        public string UsrPrfDes { get; set; }
        public int RecStusId { get; set; }
        public int? PoolCd { get; set; }
    }
}