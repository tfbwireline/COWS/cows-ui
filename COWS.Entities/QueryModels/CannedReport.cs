﻿using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public class Report
    {
        [Column("RPT_TYPE_ID")]
        public int ReportTypeId { get; set; }

        [Column("RPT_NME")]
        public string Name { get; set; }

        [Column("RPT_DES")]
        public string Description { get; set; }

        [Column("REPORTID")]
        public int Id { get; set; }

        [Column("GroupID")]
        public int GroupId { get; set; }

        [Column("IntlCD")]
        public string IntervalCode { get; set; }

        [Column("ScheduleName")]
        public string ScheduleName { get; set; }

        [Column("IntervalDesc")]
        public string IntervalDescription { get; set; }

        [Column("GRP_NME")]
        public string GroupName { get; set; }

        [Column("OwnerName")]
        public string OwnerName { get; set; }

        [Column("Encrypted")]
        public byte IsEncrypted { get; set; }

        [Column("FolderTypeCD")]
        public string FolderTypeCode { get; set; }

    }
}
