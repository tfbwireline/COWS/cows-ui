﻿using System;

namespace COWS.Entities.QueryModels
{
    public class DocEntity
    {
        public int DocId { get; set; }
        public int Id { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public string CmntTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatByAdId { get; set; }
        public int? ModfdByUserId { get; set; }
        public int? ModfdByUserAdId { get; set; }
        public byte RecStusId { get; set; }
        public string Base64string { get; set; }
    }
}