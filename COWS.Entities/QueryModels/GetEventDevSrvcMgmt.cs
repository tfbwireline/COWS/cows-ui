﻿namespace COWS.Entities.QueryModels
{
    public class GetEventDevSrvcMgmt
    {
        public int DevSrvcMgmtID { get; set; }
        public int EventID { get; set; }
        public string DeviceID { get; set; }   /*Varchar 200*/
        public int MNSOrdrID { get; set; }
        public string MNSOrdrNbr { get; set; } /*Varchar 255*/
        public string OdieDevNme { get; set; } /*Varchar 255   */
        public byte ServiceTierID { get; set; } /*TinyInt   */
        public string ServiceTier { get; set; }   /*Varchar 255*/
        public string CmpntID { get; set; } /*Varchar 255*/
        public string CmpntStus { get; set; }   /*Varchar 255*/
        public string CmpntType { get; set; }  /*Varchar 4*/
        public string CmpntNme { get; set; } /*Varchar 255*/
        public bool SendToOdie { get; set; } /*Bit   */
        public string CreatDt { get; set; } /*DateTime  */
    }
}