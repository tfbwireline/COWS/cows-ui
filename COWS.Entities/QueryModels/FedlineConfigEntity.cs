﻿using System.Runtime.Serialization;

namespace COWS.Entities.QueryModels
{
    public class FedlineConfigEntity
    {
        public int FedlineConfigID { get; set; }
        public int FedlineEventID { get; set; }
        public string PortName { get; set; }
        public string IPAddress { get; set; }
        public string SubnetMask { get; set; }
        public string DefaultGateway { get; set; }
        public string SPEED { get; set; }
        public string Duplex { get; set; }
        public string MACAddress { get; set; }

        public FedlineConfigEntity()
        {
        }

        //Deserialization constructor.
        public FedlineConfigEntity(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            FedlineConfigID = (int)info.GetValue("FedlineConfigID", typeof(int));
            FedlineEventID = (int)info.GetValue("FedlineEventID", typeof(int));
            PortName = (string)info.GetValue("PortName", typeof(string));
            IPAddress = (string)info.GetValue("IPAddress", typeof(string));
            DefaultGateway = (string)info.GetValue("DefaultGateway", typeof(string));
            SPEED = (string)info.GetValue("SPEED", typeof(string));
            Duplex = (string)info.GetValue("Duplex", typeof(string));
            MACAddress = (string)info.GetValue("MACAddress", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("FedlineConfigID", FedlineConfigID);
            info.AddValue("FedlineEventID", FedlineEventID);
            info.AddValue("PortName", PortName);
            info.AddValue("IPAddress", IPAddress);
            info.AddValue("DefaultGateway", DefaultGateway);
            info.AddValue("SPEED", SPEED);
            info.AddValue("Duplex", Duplex);
            info.AddValue("MACAddress", MACAddress);
        }
    }
}