﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace COWS.Entities.QueryModels
{
    public partial class GetM5CANDEventDataResults
    {
        public IEnumerable<GetM5CANDEventDataResult1> Result1 { get; set; }
        public IEnumerable<GetM5CANDEventDataResult2> Result2 { get; set; }
        public IEnumerable<GetM5CANDEventDataResult3> Result3 { get; set; }
        public IEnumerable<GetM5CANDEventDataResult4> Result4 { get; set; }
        public IEnumerable<GetM5CANDEventDataResult5> Result5 { get; set; }
        public IEnumerable<GetM5CANDEventDataResult6> Result6 { get; set; }
        public IEnumerable<GetM5CANDEventDataResult7> Result7 { get; set; }
    }

    public partial class GetM5CANDEventDataResult1
    {
        [Column("FTN")]
        public string Ftn { get; set; }
        [Column("CHARSID")]
        public string CharsId { get; set; }
        [Column("SITEID")]
        public string SiteId { get; set; }
        public DateTime Ccd { get; set; }
        public string H1 { get; set; }
        public string H6 { get; set; }
        public string CustomerName { get; set; }
        public string CustomerContactName { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string IsEscalation { get; set; }
        public bool CarrierWholesaleType { get; set; }
        public string IpVersion { get; set; }
        public string DesignDocumentApprovalNumber { get; set; }
        public string MdsManaged { get; set; }
        public int CsgLvlId { get; set; }
    }

    public partial class GetM5CANDEventDataResult2
    {
        [Column("FTN")]
        public string Ftn { get; set; }
        public int? CircuitId { get; set; }
    }

    public partial class GetM5CANDEventDataResult3
    {
        [Column("SiteID")]
        public string SiteId { get; set; }
        public string StAddr { get; set; }
        public string FlrBldg { get; set; }
        public string ProvState { get; set; }
        public string City { get; set; }
        [Column("ZipCD")]
        public string ZipCd { get; set; }
        public string Ctry { get; set; }
    }

    public partial class GetM5CANDEventDataResult4
    {
        public string SiteId { get; set; }
        public string H1 { get; set; }
        public string H1_CUST_NME { get; set; }
        public string H6 { get; set; }
        public string CustomerName { get; set; }

        [Column("CSG_LVL_ID")]
        public byte CsgLvlId { get; set; }
    }

    public partial class GetM5CANDEventDataResult5
    {
        public string H6 { get; set; }
        public string CustomerContactName { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string RoleNme { get; set; }
    }

    public partial class GetM5CANDEventDataResult6
    {
        [Column("DD_NBR")]
        public string DdNbr { get; set; }
        [Column("ACCT_ROLE_NME")]
        public string AcctRoleNme { get; set; }
        [Column("WRK_PHN_NBR")]
        public string WrkPhnNbr { get; set; }
        [Column("EMAIL_ADR")]
        public string EmailAdr { get; set; }
        [Column("PROD_ID")]
        public string ProdId { get; set; }
        [Column("ACCS_VNDR_NME")]
        public string AccsVndrNme { get; set; }
        [Column("FTN_NBR")]
        public string FtnNbr { get; set; }
        [Column("CUST_NME")]
        public string CustNme { get; set; }
        [Column("SRVC_ORDR_STUS_NME")]
        public string SrvcOrdrStusNme { get; set; }
        [Column("H6_H5_ID")]
        public string H6h5Id { get; set; }
        [Column("BDWD_NME")]
        public string BdwdNme { get; set; }
        [Column("SCA_NBR")]
        public string ScaNbr { get; set; }
        //[Column("CUST_ROUTR_TAG_NME")]
        //public string CustRoutrTagNme { get; set; }
        [Column("CUST_PRVD_IP_NME")]
        public string CustPrvdIpNme { get; set; }
        [Column("VLAN_ID")]
        public string VlanId { get; set; }
        //[Column("VRF_NME")]
        //public string VrfNme { get; set; }
        [Column("ROUTG_TYPE_NME")]
        public string RoutgTypeNme { get; set; }
        [Column("ADR_FMLY_TYPE_NME")]
        public string AdrFmlyTypeNme { get; set; }
        [Column("COS_CD")]
        public string CosCd { get; set; }
        [Column("MGT_RTE_TRFT_NME")]
        public string MgtRteTrgtNme { get; set; }
        [Column("FUT_PIM_BSR_BORDER_NME")]
        public string FutPimBsrBorderNme { get; set; }
        [Column("MULTPATH_SRVC_NME")]
        public string MultpathSrcNme { get; set; }
        [Column("MULTCST_IPV4_ADR")]
        public string MultcstIpv4Adr { get; set; }
        [Column("MULTCST_IPV6_ADR")]
        public string MultcstIpv6Adr { get; set; }
        [Column("DET_ROUTG_COMM_STRNG_NME")]
        public string DetRoutgCommStrngNme { get; set; }
        [Column("GRP_NME")]
        public string GrpNme { get; set; }
        [Column("NUA_ADR")]
        public string NuaAdr { get; set; }
        [Column("LOC_CITY")]
        public string LocCity { get; set; }
        [Column("LOC_STT")]
        public string LocStt { get; set; }
        [Column("LOC_CTRY")]
        public string LocCtry { get; set; }
        [Column("SUB_TYPE")]
        public string SubType { get; set; }
        [Column("VAS_CD")]
        public string VasCd { get; set; }
        [Column("CE_SRVC_ID")]
        public string CeSrvcId { get; set; }
        [Column("NID_HOST_NME")]
        public string NidHostNme { get; set; }
        [Column("NID_SERIAL_NBR")]
        public string NidSerialNbr { get; set; }
        [Column("NID_IP_ADR")]
        public string NidIpAdr { get; set; }
    }

    public partial class GetM5CANDEventDataResult7
    {
        [Column("CE_SRVC_ID")]
        public string CeSrvcId { get; set; }
        [Column("EVENT_ID")]
        public int EventId { get; set; }
    }
}