﻿namespace COWS.Entities.QueryModels
{
    public class WFMUserAssignment
    {
        public int WfmId { get; set; }
        public int UserId { get; set; }
        public short GrpId { get; set; }
        public byte RoleId { get; set; }
        public byte? OrdrTypeId { get; set; }
        public string OrdrTypeDes { get; set; }
        public byte? ProdTypeId { get; set; }
        public string ProdTypeDes { get; set; }
        public string PltfrmCd { get; set; }
        public string PltfrmNme { get; set; }
        public string VndrCd { get; set; }
        public string OrgtngCtryCd { get; set; }
        public string OrgtngCtryNme { get; set; }
        public string IplTrmtgCtryCd { get; set; }
        public string IplTrmtgCtryNme { get; set; }
        public byte? OrdrActnId { get; set; }
        public string OrdrActnDes { get; set; }
        public byte? WfmAsmtLvlId { get; set; }
        public string DsplNme { get; set; }
        public short? UsrPrfId { get; set; }
    }
}