import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class CurrencyFileService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/CurrencyFiles');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/CurrencyFiles/' + id);
  }

  public download(id: number): Observable<any> {
    return this.api.get('api/CurrencyFiles/' + id + '/download', { responseType: "blob" });
  }

  public create(model): Observable<any> {
    return this.api.post('api/CurrencyFiles/', model);
  }

  public create2(file, byteArray): Observable<any> {
    return this.api.post('api/CurrencyFiles/', null, { params: { filename: file.name, byteArray: byteArray }});
  }
}
