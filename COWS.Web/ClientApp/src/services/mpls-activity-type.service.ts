import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';
import { EEventType, KeyValuePair } from './../shared/global';

@Injectable({
  providedIn: 'root'
})

export class MplsActivityTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MplsActivityTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MplsActivityTypes/' + id);
  }

  public getByEventId(id: number): Observable<any> {
    return this.api.get('api/MplsActivityTypes/getByEventId/' + id).pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.mplsActyTypeDes, item.mplsActyTypeId));
      }));
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/MplsActivityTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.mplsActyTypeId, item.mplsActyTypeDes));
      }));
  }

  public getMplsActivityTypeForMDS(): Observable<any> {
    return this.api.get('api/MplsActivityTypes/getByEventId/' + EEventType.MDS).pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.mplsActyTypeId, item.mplsActyTypeDes));
      }));
  }
}
