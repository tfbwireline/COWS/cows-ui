import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class EnhncSrvcService {

  constructor(private api: ApiHelperService) { }

  public getEnhncSrvc(): any {
    return this.api.get('api/EnhncSrvc');
  }

  public getEnhncSrvcById(id: number): Observable<any> {
    return this.api.get('api/EnhncSrvc/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/EnhncSrvc').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.enhncSrvcId, item.enhncSrvcNme));
      }));
  }

  public createEnhncSrvc(service): Observable<any> {
    return this.api.post('api/EnhncSrvc/', service);
  }

  public updateEnhncSrvc(id: number, service): Observable<any> {
    return this.api.put('api/EnhncSrvc/' + id, service);
  }

  public deleteEnhncSrvc(id: number): Observable<any> {
    return this.api.delete('api/EnhncSrvc/' + id);
  }
}
