import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class RedesignCustBypassService {

  constructor(private api: ApiHelperService) { }

  public getCustomersBypass(): Observable<any> {
    return this.api.get('api/RedesignCustomerBypass/GetCustomersBypass');
  }

  public getCustomersBypassByID(id: number): Observable<any> {
    return this.api.get('api/RedesignCustomerBypass/GetCustomersBypassByID/' + id);
  }

  public createCustomerByPass(custByPass): Observable<any> {
    return this.api.post('api/RedesignCustomerBypass/CreateCustomerByPass', custByPass);
  }

  public create(custByPass): Observable<any> {
    return this.api.post('api/RedesignCustomerBypass/', custByPass);
  }

  public update(custByPassId: number, custByPass): Observable<any> {
    return this.api.put('api/RedesignCustomerBypass/' + custByPassId, custByPass);
  }

  public CheckRedesignCustomerBypassCDIfExist(h1: string, id : number = 0): Observable<any> {
    const queryString = `?h1=${h1}&id=${id}`
    return this.api.get('api/RedesignCustomerBypass/CheckRedesignCustomerBypassCDIfExist' + queryString);
  }

}

