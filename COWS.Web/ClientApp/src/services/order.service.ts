import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { Global, WorkGroup, EordrCat, ETasks } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class OrderService {
  constructor(private api: ApiHelperService) { }

  public getById(id: number): Observable<any> {
    var data = this.api.get(`api/Orders/${id}`);
    return data;
  }

  public getById2(id: number): Observable<any> {
    var data = this.api.get(`api/Orders/${id}/Version2`);
    return data;
  }
  public updateNrm(id: number, nrmupdate:boolean): Observable<any> {
   let params = {
      nrmupdate: nrmupdate    
    }
   return this.api.put(`api/Orders/${id}/UpdateNrm`,{ params: params });
   
  }


  public getById3(id: number, wgId: number, taskId: number): Observable<any> {
    let params = {
      wgId: wgId,
      taskId: taskId
    }
    return this.api.get(`api/Orders/${id}/Version3`, { params: params });
  }

  public isDynamic(id: number): Observable<any> {
    var data = this.api.get(`api/Orders/${id}/IsDynamic`);
    return data;
  }

  public update(id: number, model: any): Observable<any> {
    return this.api.put(`api/Orders/${id}`, model);
  }

  public requestToLogicallis(id: number, notes: string) {
    let params = {
      ordrId: id,
      nteTxt: notes
    }
    return this.api.post(`api/Orders/RequestToLogicallis`, params);
  }

  public loadRtsTask(id: number, model: any): Observable<any> {
    return this.api.put(`api/Orders/${id}/LoadRtsTask`, model);
  }

  public moveToGom(id: number, model: any): Observable<any> {
    return this.api.put(`api/Orders/${id}/MoveToGom`, model);
  }

  public clearAcrTask(id: number, taskId: any): Observable<any> {
    return this.api.put(`api/Orders/${id}/ClearAcrTask`, taskId);
  }

  public getWgData(id: number, wg: number = 0): Observable<any> {
    let params = {
      usrPrfId: wg
    }
    return this.api.get(`api/Orders/${id}/WgData`, { params: params });
  }

  public GetOrderPPRT(id: number): Observable<any> {
    return this.api.get(`api/Orders/GetOrderPPRT/${id}`);
  }
  
  public attachH5Folder(id: number, h5FolderId: number): Observable<any> {
    let params = {
      h5FolderId: h5FolderId
    }
    return this.api.get(`api/Orders/${id}/AttachH5Folder`, { params: params });
  }

  public getOrderUrl({ orderId = 0, wg = 0, orderCategoryId = 0, taskId = 0, purchaseOrderNo = null }) {
    let url = {
      link: null,
      param: null
    }

    //console.log(wg)
    if (wg == WorkGroup.CSC) {
      
    } else if (wg == WorkGroup.GOM) {
      if (orderCategoryId == EordrCat.NCCO) {
        // return ncco url
      } else {
        url.link = `/order/gom/${wg}/${orderId}`
      }
    } else if (wg == WorkGroup.MDS) {
      if (taskId == ETasks.EquipReceipt) {
        url.link = `/order/cpe-receipts/${wg}/${orderId}/${taskId}/${purchaseOrderNo}`
      } else {
        // return mds order detail
      }  
    } else if (wg == WorkGroup.RTS) {
      url.link = `/order/rts/${wg}/${orderId}/${taskId}`
    } else if (wg == WorkGroup.xNCIAmerica || WorkGroup.xNCIAsia || WorkGroup.xNCIEurope) {
      //console.log(wg)
      if (orderCategoryId == EordrCat.NCCO && taskId == ETasks.xNCIReady) {
        // return ncco url
      } else {
        //console.log(wg)
        if (wg == WorkGroup.xNCIAmerica) {
          url.link = `/order/amnci/${wg}/${orderId}`
          url.param = {
            queryParams: {
              taskId: taskId
            }
          }
        } else if (wg == WorkGroup.xNCIAsia) {
          url.link = `/order/anci/${wg}/${orderId}`
          url.param = {
            queryParams: {
              taskId: taskId
            }
          }
        } else if (wg == WorkGroup.xNCIEurope) {
          url.link = `/order/enci/${wg}/${orderId}`
          url.param = {
            queryParams: {
              taskId: taskId
            }
          }
        } else {
          //console.log("ASD")
        }
      }
    } else if (wg == WorkGroup.DCPE) {
      url.link = `/order/dcpe/${wg}/${orderId}`
    } else if (wg == WorkGroup.UCaaS) {
      // return 
    } else if (wg == WorkGroup.CPETech) {
      url.link = `/order/3rd-party-cpe-tech/${wg}/${orderId}`
    }

    return url
  }


  public getTransportOrder(id: number): Observable<any> {
    return this.api.get(`api/Orders/${id}/TransportOrder`);
  }

  public getParentOrderType(id: number): Observable<any> {
    return this.api.get(`api/Orders/${id}/ParentOrderType`);
  }

  public getRelatedOrderId(id: number): Observable<any> {
    return this.api.get(`api/Orders/${id}/RelatedOrderId`);
  } 

  public UpdateCustomerTurnUpTask(id: number): Observable<any> {
    return this.api.get(`api/Orders/UpdateCustomerTurnUpTask/${id}`);
  } 

  public checkIfOrderIsBAR(orderId: number) {
    return this.api.get(`api/Orders/CheckIfOrderIsBAR/${orderId}`);
  }
}

