import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { VendorOrderMs } from '../models';

@Injectable({
  providedIn: 'root'
})

export class VendorOrderMsService {
  constructor(private api: ApiHelperService) { }

  public create(model: VendorOrderMs): Observable<any> {
    return this.api.post('api/VendorOrderMs/', model);
  }

  public update(id: number, model: VendorOrderMs): Observable<any> {
    return this.api.put(`api/VendorOrderMs/${id}`, model);
  }
}
