import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class EventStatusService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/EventStatus');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/EventStatus/' + id);
  }
}
