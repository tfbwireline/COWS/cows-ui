import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { filter, map } from 'rxjs/operators';
import { Calendar } from '../models';

@Injectable({
  providedIn: 'root'
})

export class CalendarService {

  constructor(private api: ApiHelperService) { }

  public getCalendarData(isWFCalendar: number, userId: number, apptTypeId: number): Observable<any> {
    let params = {
      isWFCalendar: isWFCalendar,
      userId: userId,
      apptTypeId: apptTypeId
    }
    return this.api.get('api/Calendar/GetCalendarData', { params: params });
  }

  public createUpdateCalendarData(model): Observable<any> {
    return this.api.post('api/Calendar/', model);
  }

  public getApptTypes(isMain: number, isWF: number = 0): Observable<any> {
    let params = {
      isMain: isMain,
      isWF: isWF
    }
    return this.api.get('api/Calendar/GetApptTypes', { params: params });
  }
}
