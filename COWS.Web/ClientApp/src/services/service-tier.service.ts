import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators';
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class ServiceTierService {

  constructor(private api: ApiHelperService) { }

  public getServiceTier(): Observable<any> {
    return this.api.get('api/ServiceTier');
  }

  public getForLookup(): Observable<any> {
    return this.api.get('api/ServiceTier/GetForLookup');
  }

  public getServiceTierById(id: number): Observable<any> {
    return this.api.get('api/ServiceTier/' + id);
  }

  public createServiceTier(service): Observable<any> {
    return this.api.post('api/ServiceTier/', service);
  }

  public updateServiceTier(id: number, service): Observable<any> {
    return this.api.put('api/ServiceTier/' + id, service);
  }

  public deleteServiceTier(id: number): Observable<any> {
    return this.api.delete('api/ServiceTier/' + id);
  }

  public getForControlByEventTypeId(eventTypeId: number): Observable<any> {
    return this.api.get('api/ServiceTier/GetForLookup').pipe(
      map((res: any) => res.filter(i => i.eventTypeId == eventTypeId)),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.mdsSrvcTierDes, item.mdsSrvcTierId));
      }));
  }
}
