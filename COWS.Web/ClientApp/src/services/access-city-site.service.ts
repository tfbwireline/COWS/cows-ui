import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class AccessCitySiteService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/AccessCitySites');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/AccessCitySites/' + id);
  }

  public create(city): Observable<any> {
    return this.api.post('api/AccessCitySites/', city);
  }

  public update(id: number, city): Observable<any> {
    return this.api.put('api/AccessCitySites/' + id, city);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/AccessCitySites/' + id);
  }
}
