import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class ProductTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/ProductTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/ProductTypes/' + id);
  }

  public getProductTypeByOrdrCatId(id: number): Observable<any> {
    return this.api.get('api/ProductTypes/GetProductTypeByOrdrCatId/' + id);
  }

  public getProductPlatformCombinations(): Observable<any> {
    return this.api.get('api/ProductTypes/getProductPlatformCombinations');
  }
}
