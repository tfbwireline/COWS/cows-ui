import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class OrderLockService {

  constructor(private api: ApiHelperService) { }

  public checkLock(orderId: number): any {
    return this.api.get('api/OrderLock/CheckLock/' + orderId);
  }

  public lock(orderId: number): any {
    return this.api.get('api/OrderLock/Lock/' + orderId);
  }

  public unlock(orderId: number): any {
    return this.api.get('api/OrderLock/Unlock/' + orderId);
  }

  public lockUnlockOrdersEvents(orderId: number, eventId: number, userId: number, isOrder: boolean, unlock: boolean, isLocked: number): any {
    let params = {
      orderId: orderId,
      eventId: eventId,
      userId: userId,
      isOrder: isOrder,
      unlock: unlock,
      isLocked: isLocked
    }
    return this.api.get('api/OrderLock/LockUnlockOrdersEvents', { params: params });
  }
}
