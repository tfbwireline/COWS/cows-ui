import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';
import { KeyValuePair } from './../shared/global';

@Injectable({
  providedIn: 'root'
})

export class MplsEventTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MplsEventTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MplsEventTypes/' + id);
  }

  public getByEventId(id: number): Observable<any> {
    return this.api.get('api/MplsEventTypes/getByEventId/' + id).pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.mplsEventTypeDes, item.mplsEventTypeId));
      }));
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/MplsEventTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.mplsEventTypeId, item.mplsEventTypeDes));
      }));
  }
}
