import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { ApiHelperService } from "../shared/index";
import { map, catchError } from 'rxjs/operators';

import { of, Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})

export class CpeClliService {

    constructor(private api: ApiHelperService) { }

    public get(formData: any): Observable<any>  {
        const statusId = formData['status'];
        const costumFilter = `&${formData['filter']}=${formData['filterValue']}`;
        return this.api.get(`api/CPECLLI?statusId=${statusId}${costumFilter}`);
    }

    public getById(id: number): Observable<any> {
        return this.api.get(`api/CPECLLI/${id}`);
    }

    public create(formData: any): Observable<any> {
        return this.api.post('api/CPECLLI', formData);
      }
    
    public update(formData: any): Observable<any> {
        const id = formData['cpeClliId']
        return this.api.put('api/CPECLLI/' + id, formData);
    }

    public getClliID(cityNme : string, stateCode: string): Observable<any> {
        return this.api.get(`api/CPECLLI/GetClliID?cityNme=${cityNme}&stateCode=${stateCode}`, {
            responseType: 'text',
          });
    }

    public checkClliIdIfExist(cpeClliId: number, clliId : string): Observable<any> {
        return this.api.get(`api/CPECLLI/CheckClliIdIfExist?cpeClliId=${cpeClliId}&clliId=${clliId}`);
    }

    // public validateCheckClliIdIfExist(): AsyncValidatorFn {
    //     return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
    //       if (control.dirty) {
    //         if (control.value) {
    //           const val = control.value;
    //           return this.checkClliIdIfExist(val).pipe(
    //             map(status => {
    //               if (status) {
    //                 // return error
    //                 return { taxAccntDuplicate: true };
    //               }
    //             }),
    //             catchError(error => null)
    //           );
    //         }
    //       }
    //       return of(null);
    //     };
    //   }
}