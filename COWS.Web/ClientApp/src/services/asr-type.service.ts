import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class AsrTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/AsrTypes');
  }

  public saveAsr(obj: any): Observable<any> {
    return this.api.post('api/AsrTypes/SaveAsr', obj);
  }
}
