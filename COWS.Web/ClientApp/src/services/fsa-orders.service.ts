import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class FsaOrdersService {

  constructor(private api: ApiHelperService) { }

  //public get(): Observable<any> {
  //  return this.api.get('api/Events/Fedline');
  //}

  public getById(id: number): Observable<any> {
    return this.api.get('api/FsaOrders/' + id);
  }

  public getFtnList(id: number): Observable<any> {
    return this.api.get('api/FsaOrders/' + id + '/List');
  }

  //public getRelatedOrders(ftn: string): Observable<any> {
  //  return this.api.get(`api/FsaOrders/Ftn/${ftn}`);
  //}
  public getRelatedOrdersByFtn(ftn: string): Observable<any> {
    return this.api.get(`api/FsaOrders/related/ftn/${ftn}`);
  }

  public getRelatedOrdersById(id: number): Observable<any> {
    return this.api.get(`api/FsaOrders/related/${id}`);
  }

  public update(id: number, model: any): Observable<any> {
    return this.api.put(`api/FsaOrders/${id}`, model);
  }

  public getFSADisconnectDetails(id: number): Observable<any> {
    return this.api.get(`api/FsaOrders/getFSADisconnectDetails/${id}`);
  }

  public UpdateVendorAccessCostInfo(id: number, model: any): Observable<any> {
    return this.api.put(`api/FsaOrders/UpdateVendorAccessCostInfo/${id}`, model);
  }
}
