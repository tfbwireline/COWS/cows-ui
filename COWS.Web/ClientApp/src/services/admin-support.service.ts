import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class AdminSupportService {

  constructor(private api: ApiHelperService) { }

  public getEventByEventId(eventId: number): Observable<any> {
    return this.api.get('api/AdminSupport/Event/' + eventId);
  }

  public getOrderByFtn(ftn: string): Observable<any> {
    return this.api.get('api/AdminSupport/Order/' + ftn);
  }

  public getCPTByCPTNo(cptNo: string): Observable<any> {
    return this.api.get('api/AdminSupport/CPT/' + cptNo);
  }

  public getRedesign(redesign: string): Observable<any> {
    return this.api.get('api/AdminSupport/Redesign/' + redesign);
  }

  public getBPM(h6: string): Observable<any> {
    return this.api.get('api/AdminSupport/BPM/' + h6);
  }

  public deleteOrder(orderId: number): Observable<any> {
    return this.api.delete('api/AdminSupport/Order/' + orderId + '/delete');
  }

  public extractOrder(params: any): Observable<any> {
    return this.api.post('api/AdminSupport/Order/Extract', params);
  }
}
