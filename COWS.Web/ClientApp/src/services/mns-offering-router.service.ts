import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class MnsOfferingRouterService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MnsOfferingRouters');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MnsOfferingRouters/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/MnsOfferingRouters').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.mnsOffrgRoutrDes, item.mnsOffrgRoutrId));
      }));
  }
}
