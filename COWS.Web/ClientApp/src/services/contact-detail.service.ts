import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class ContactDetailService {
  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/ContactDetails');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/ContactDetails/' + id);
  }

  public getContactDetails(objId: number, objType: string, hierId: string = '', hierLvl: string = '', odieCustId: string = '', isNetworkOnly: boolean = false): Observable<any> {
    const params = {
      objId: objId,
      objType: objType,
      hierId: hierId,
      hierLvl: hierLvl,
      odieCustId: odieCustId,
      isNetworkOnly: isNetworkOnly
    }
    console.log(['odieCustId', odieCustId])
    console.log(['params', params])
    return this.api.get('api/ContactDetails/GetContactDetails', { params: params });
  }
}
