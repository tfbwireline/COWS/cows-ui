import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})
export class MssService {

  constructor(private api: ApiHelperService) { }
  public getSdeView(statusID: number, userID: number): Observable<any> {
    let params = {
      statusID: statusID,
      userID: userID
    }
    return this.api.get('api/MSS/GetSdeView', { params: params });
  }
  public getAvailableUsers(): any {

    return this.api.get('api/MSS/GetAvailableUsers');
  }

  public getViewOptions(): any {

    return this.api.get('api/MSS/GetViewOptions');
  }
  public getSDEProductTypesForAdmin(): any {

    return this.api.get('api/MSS/GetSDEProductTypesForAdmin');
  }
  public getSDEProductTypesByIDForAdmin(id: number): Observable<any> {
    return this.api.get('api/MSS/GetSDEProductTypesByIDForAdmin/' + id);

  }
  public insertProductType(mss): Observable<any> {
    return this.api.post('api/MSS/', mss);
  }

  public updateProductType(id: number, mss): Observable<any> {
    return this.api.put('api/MSS/' + id, mss);
  }
  public deleteProductType(id: number): Observable<any> {
    return this.api.delete('api/MSS/' + id);
  }
  public getByID(sdeID: number): Observable<any> {
    return this.api.get('api/MSS/GetByID/' + sdeID);
  }


  public getAllSdeProductType(): Observable<any> {
    return this.api.get('api/MSS/GetAllSdeProductType');
  }

  public getSDEProductTypes(): Observable<any> {
    return this.api.get('api/MSS/GetSDEProductTypes');
  }
  public getWorkflowStatus(statusID: number): Observable<any> {
    return this.api.get('api/MSS/GetWorkflowStatus/' + statusID);
  }
  public getSDENotes(sdeID: number): Observable<any> {
    return this.api.get('api/MSS/GetSDENotes/' + sdeID);
  }
  public downloadSDEDocsById(sdeID: number): Observable<any> {
    return this.api.get('api/MSS/DownloadSdeDoc/' + sdeID, { responseType: "blob" });
  }
  public getSDEProds(sdeID: number): Observable<any> {
    return this.api.get('api/MSS/GetSDEProds/' + sdeID);
  }
  public getSDEDocs(sdeID: number): Observable<any> {
    return this.api.get('api/MSS/GetSDEDocs/' + sdeID);
  }
  public getCowsAppCfgValue(cfgKeyName: string): Observable<any> {
    let params = {
      cfgKeyName: cfgKeyName
    }
    return this.api.get('api/MSS/GetCowsAppCfgValue', { params: params });
  }
  public insert(SdeOpportunity): Observable<any> {
    return this.api.post('api/MSS/Insert/', SdeOpportunity);
  }
  public update(SdeOpportunity): Observable<any> {
    return this.api.put('api/MSS/Update/', SdeOpportunity);
  }
  public delete(sdeID: number): Observable<any> {
    return this.api.delete('api/MSS/DeleteSde/' + sdeID);
  }
  //public delete(int[] Ids);
}


