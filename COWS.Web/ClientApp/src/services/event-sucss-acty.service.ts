import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class EventSucssActyService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/EventSucssActy');
  }

  public getBySucssActyId(id: number): any {
    return this.api.get('api/EventSucssActy/' + id);
  }
  public getSucssActyByEventHistId(id: number): any {
    return this.api.get('api/EventSucssActy/GetbyHistId/' + id);
  }
  public getSucssActyByEventId(id: number): any {
    return this.api.get('api/EventSucssActy/GetbyEventId/' + id);
  }
}
