import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})
export class PeopleSoftInterfaceViewService {

  constructor(private api: ApiHelperService) {
  }

  public getSCMInterfaceView(): Observable<any> {
    return this.api.get('api/PeopleSoftInterface');
  }
}
