import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class EventHistoryService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/EventHistory');
  }

  public getByEventId(id: number): Observable<any> {
    return this.api.get('api/EventHistory/' + id);
  }

  //public create(model): any {
  //  return this.api.post('api/Events/AccessDeliveries/', model);
  //}

  //public update(id: number, model): any {
  //  return this.api.put('api/Events/AccessDeliveries/' + id, model);
  //}

  //public delete(id: number): any {
  //  return this.api.delete('api/Events/AccessDeliveries/' + id);
  //}
}
