import { Injectable } from '@angular/core';
import { ApiHelperService } from '../shared';
import { BillingDispatch } from '../models/billing-dispatch.model';
import { Observable } from 'rxjs';
//import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: "root"
})
export class BillingDispatchService {
  constructor(private api: ApiHelperService) {}

  public get(includeHistory: boolean): Observable<BillingDispatch[]> {
    //return this.api.get('api/BillingDispatch');
    return this.api.get('api/BillingDispatch', { params: { history: includeHistory }});
  }

  public getById(id: number, fromHistory: boolean): Observable<BillingDispatch> {
    return this.api.get('api/BillingDispatch/' + id, { params: { fromHistory: fromHistory }})
  }

  public post(model : BillingDispatch) : any {
    return this.api.post('api/BillingDispatch', model);
  }
}
