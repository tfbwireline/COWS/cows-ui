import { Injectable } from '@angular/core';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class RedesignRecLockService {

  constructor(private api: ApiHelperService) { }

  public checkLock(redesignId: number): any {
    return this.api.get('api/RedesignRecLock/CheckLock/' + redesignId);
  }

  public lock(redesignId: number): any {
    return this.api.get('api/RedesignRecLock/Lock/' + redesignId);
  }

  public unlock(redesignId: number): any {
    return this.api.get('api/RedesignRecLock/Unlock/' + redesignId);
  }
}
