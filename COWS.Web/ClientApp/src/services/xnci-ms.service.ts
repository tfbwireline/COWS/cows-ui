import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class XnciMsService {

  constructor(private api: ApiHelperService) { }

  public getXnciMs(): Observable<any> {
    return this.api.get('api/XnciMs');
  }

  public getXnciMsById(id: number): Observable<any> {
    return this.api.get('api/XnciMs/' + id);
  }

  public createXnciMs(service): Observable<any> {
    return this.api.post('api/XnciMs/', service);
  }

  public updateXnciMs(id: number, service): Observable<any> {
    return this.api.put('api/XnciMs/' + id, service);
  }

  public deleteXnciMs(id: number): Observable<any> {
    return this.api.delete('api/XnciMs/' + id);
  }
}
