import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class MnsPerformanceReportingService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MnsPerformanceReporting');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MnsPerformanceReporting/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/MnsPerformanceReporting').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.mnsPrfmcRptDes, item.mnsPrfmcRptId));
      }));
  }
}
