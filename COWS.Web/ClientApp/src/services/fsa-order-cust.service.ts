import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class FsaOrderCustService {

  constructor(private api: ApiHelperService) { }

  //public get(): Observable<any> {
  //  return this.api.get('api/Events/Fedline');
  //}

  public getByCisLevelIdAndId(id: number, cisLevelId: string): Observable<any> {
    return this.api.get('api/FsaOrderCust/' + cisLevelId + '/' + id);
  }
}
