import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class XnciRegionService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/XnciRegion');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/XnciRegion/' + id);
  }

  public create(service): Observable<any> {
    return this.api.post('api/XnciRegion/', service);
  }

  public update(id: number, service): Observable<any> {
    return this.api.put('api/XnciRegion/' + id, service);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/XnciRegion/' + id);
  }
}
