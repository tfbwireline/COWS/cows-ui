import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})
export class getCancelOrdrViewService {

  buttonOptions: any = {
    text: "Search",
    type: "success",
    useSubmitBehavior: true
  }
  constructor(private api: ApiHelperService) {
  }

  public getCancelOrdr(ftn, h5_H6): Observable<any> {
    return this.api.get('api/CancelOrdr', { params: { ftn: ftn, h5_H6: h5_H6} });
  }
  public cancelOrdr(service): Observable<any> {
    return this.api.post('api/CancelOrdr/', service);
  }

}
