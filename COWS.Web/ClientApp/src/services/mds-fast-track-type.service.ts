import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MDSFastTrackTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MDSFastTrackTypes');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/MDSFastTrackTypes/' + id);
  }
}
