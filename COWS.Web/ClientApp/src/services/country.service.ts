import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CountryService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/Countries');
  }

  public getCountriesByRegion(id: number): Observable<any> {
    return this.api.get('api/Countries')
      .pipe(map(data =>
        data.filter(resp => resp.rgnId == id)
      ));
  }

  public getByCode(code: string): Observable<any> {
    return this.api.get('api/Countries/' + code);
  }

  public updateCountryRegion(code: string, regionId: number): Observable<any> {
    return this.api.post('api/Countries/' + code + '/Region', regionId);
  }

  public getAllForLookup(): Observable<any> {
    return this.api.get('api/Countries/Lookup');
  }
}
