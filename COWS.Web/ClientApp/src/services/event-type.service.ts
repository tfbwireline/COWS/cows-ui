import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class EventTypeService {

  constructor(private api: ApiHelperService) { }

  public getEventTypes(): Observable<any> {
    return this.api.get('api/EventTypes');
  }

  public getEventTypeById(id: number): Observable<any> {
    return this.api.get('api/EventTypes/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/EventTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.eventTypeId, item.eventTypeNme));
      }));
  }

  public createEventType(type): Observable<any> {
    return this.api.post('api/EventTypes/', type);
  }

  public updateEventType(id: number, type): Observable<any> {
    return this.api.put('api/EventTypes/' + id, type);
  }

  public deleteEventType(id: number): Observable<any> {
    return this.api.delete('api/EventTypes/' + id);
  }
}
