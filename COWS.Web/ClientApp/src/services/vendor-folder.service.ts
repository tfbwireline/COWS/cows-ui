import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class VendorFolderService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/VendorFolder');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/VendorFolder/' + id);
  }

  public getVendorFolder(vendorName, countryCode): Observable<any> {
    let params = {
      vendorName: vendorName,
      countryCode: countryCode
    }
    return this.api.get('api/VendorFolder/GetVendorFolder/', { params: params });
  }

  public create(vndrFolder): Observable<any> {
    return this.api.post('api/VendorFolder/', vndrFolder);
  }

  public update(id: number, vndrFolder): Observable<any> {
    return this.api.put('api/VendorFolder/' + id, vndrFolder);
  }

  public getContactsByFolderId(id: number): Observable<any> {
    return this.api.get('api/VendorFolder/' + id + '/Contacts');
  }

  public getTemplatesByFolderId(id: number): Observable<any> {
    return this.api.get('api/VendorFolder/' + id + '/Templates');
  }

  //public getVendorOrderEmails(vendorOrderID): Observable<any> {
  //  let params = {
  //    vendorOrderID: vendorOrderID
  //  }
  //  return this.api.get('api/VendorFolder/GetVendorOrderEmails/', { params: params });
  //}
}
