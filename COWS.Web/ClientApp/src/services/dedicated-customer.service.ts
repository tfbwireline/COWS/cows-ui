import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class DedicatedCustomerService {

  constructor(private api: ApiHelperService) { }

  public getDedicatedCustomers(): Observable<any> {
    return this.api.get('api/DedicatedCustomers');
  }

  public getDedicatedCustomerById(id: number): Observable<any> {
    return this.api.get('api/DedicatedCustomers/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/DedicatedCustomers').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.custId, item.custNme));
      }));
  }

  public createDedicatedCustomer(customer): Observable<any> {
    return this.api.post('api/DedicatedCustomers/', customer);
  }

  public updateDedicatedCustomer(id: number, customer): Observable<any> {
    return this.api.put('api/DedicatedCustomers/' + id, customer);
  }

  public deleteDedicatedCustomer(id: number): Observable<any> {
    return this.api.delete('api/DedicatedCustomers/' + id);
  }
}
