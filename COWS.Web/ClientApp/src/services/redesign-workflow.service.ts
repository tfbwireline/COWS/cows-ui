import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class RedesignWorkflowService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/RedesignWorkflow');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/RedesignWorkflow/' + id);
  }

  public getByStatus(id: number): Observable<any> {
    return this.api.get('api/RedesignWorkflow/GetByStatus/' + id).pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.desrdWrkflwStus, item.desrdWrkflwStusId));
      }));
  }
}
