import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class EventViewsService {

  constructor(private api: ApiHelperService) {
  }

  public getEventViews(userId: number, siteCntnt: number): Observable<any> {
    let params = {
      userId: userId,
      siteCntnt: siteCntnt
    }
    return this.api.get('api/EventViews/GetEventViews', { params: params });
  }

  public getEventViewDetails(viewId: number, siteCntnt: number, userId: number, newMDS: string, ): Observable<any> {
    let params = {
      viewId: viewId,
      siteCntnt: siteCntnt,
      userId: userId,
      newMDS: newMDS
    }
    return this.api.get('api/EventViews/GetEventViewDetails', { params: params });
  }

  public getCalendarViewDetails(viewId: number, eID: number, date: string = null, type: string = '', user: string = '', group: string = ''): Observable<any> {
    let params = {
      viewId: viewId,
      eID: eID,
      date: date,
      type: type,
      user: user,
      group: group
    }
    return this.api.get('api/EventViews/GetCalendarViewDetails', { params: params });
  }

  public getEventViewColumns(grpId: number, siteCntnt: number): Observable<any> {
    let params = {
      grpId: grpId,
      siteCntnt: siteCntnt
    }
    return this.api.get('api/EventViews/GetEventViewColumns', { params: params });
  }

  public GetEventViewInfo(viewId: number): Observable<any> {
    let params = {
      viewId: viewId
    }
    return this.api.get('api/EventViews/GetEventViewInfo', { params: params });
  }

  public DeleteView(viewId: number): Observable<any> {
    return this.api.delete('api/EventViews/' + viewId);
  }

  public GetFilterOperators(): Observable<any> {
    return this.api.get('api/EventViews/GetFilterOperators');
  }

  public GetLogicOperators(): Observable<any> {
    return this.api.get('api/EventViews/GetLogicOperators');
  }

  public CreateView(eventView): Observable<any> {
    //viewId: number, userId: number, viewName: string, siteCntntId: number, dfltFlagCd: string, pblFlagCd: string, fltColOpr1: string,
    //  fltColOpr2: string, fltCol1: number, fltCol2: number, fltColTxt1Val: string, fltColTxt2Val: string, fltFlag: string, sortCol1: number, sortCol1Flag: string,
    //    sortCol2: number, sortCol2Flag: string, fltLogicOpr: string, colIDs: string, colPos: string
    //let params = {
    //  viewId: viewId,
    //  userId: userId,
    //  viewName: viewName,
    //  siteCntntId: siteCntntId,
    //  dfltFlagCd: dfltFlagCd,
    //  pblFlagCd: pblFlagCd,
    //  fltColOpr1: fltColOpr1,
    //  fltColOpr2: fltColOpr2,
    //  fltCol1: fltCol1,
    //  fltCol2: fltCol2,
    //  fltColTxt1Val: fltColTxt1Val,
    //  fltColTxt2Val: fltColTxt2Val,
    //  fltFlag: fltFlag,
    //  sortCol1: sortCol1,
    //  sortCol1Flag: sortCol1Flag,
    //  sortCol2: sortCol2,
    //  sortCol2Flag: sortCol2Flag,
    //  fltLogicOpr: fltLogicOpr,
    //  colIDs: colIDs,
    //  colPos: colPos
    //}
    return this.api.post('api/EventViews/', eventView);
  }

  public UpdateView(viewId:number, eventView): Observable<any> {
   
    return this.api.put('api/EventViews/' + viewId, eventView);
  }
}
