import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class CurrencyListService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/CurrencyList');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/CurrencyList/' + id);
  }

  public getCurrencyList(): Observable<any> {
    return this.api.get('api/CurrencyList/GetCurrencyList');
  }
}
