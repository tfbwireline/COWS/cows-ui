import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { TimeSlotOccurence } from "./../models";

@Injectable({
  providedIn: 'root'
})

export class TimeSlotService {

  constructor(private api: ApiHelperService) { }

  public getTimeSlotOccurances(eventId: number, bShowAfterHrsSlots: boolean): Observable<any> {
    let params = {
      eventId: eventId,
      bShowAfterHrsSlots: bShowAfterHrsSlots
    }
    return this.api.get('api/TimeSlotOccurrence', { params: params });
  }

  public getFTEventsForTS(resourceID: number): Observable<any> {
    return this.api.get('api/TimeSlotOccurrence/getFTEventsForTS/' + resourceID);
  }

  public updateResourceAvailability(updResultset: string): Observable<any> {
    return this.api.get('api/TimeSlotOccurrence/updateResourceAvailability/'+ updResultset);
  }

  public checkUserAccessToAfterHrsSlots(adid: string): Observable<any> {
    return this.api.get('api/TimeSlotOccurrence/checkUserAccessToAfterHrsSlots/' + adid);
  }

  public getInvalidTSEvents(): Observable<any> {
    return this.api.get('api/TimeSlotOccurrence/getInvalidTSEvents');
  }

  public getDisconnectEvents(): Observable<any> {
    return this.api.get('api/TimeSlotOccurrence/getDisconnectEvents');
  }

  public getFedEventsForTS(resourceID: number): Observable<any> {
    return this.api.get('api/TimeSlotOccurrence/getFedEventsForTS/' + resourceID);
  }
}
