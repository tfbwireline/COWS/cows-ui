import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class SetSlasService {

  constructor(private api: ApiHelperService) { }

  public getSetSlas(): Observable<any> {
    return this.api.get('api/SetSlas');
  }

  public getSetSlasById(id: number): Observable<any> {
    return this.api.get('api/SetSlas/' + id);
  }

  public createSetSlas(service): Observable<any> {
    return this.api.post('api/SetSlas/', service);
  }

  public updateSetSlas(id: number, service): Observable<any> {
    return this.api.put('api/SetSlas/' + id, service);
  }

  public deleteSetSlas(id: number): Observable<any> {
    return this.api.delete('api/SetSlas/' + id);
  }
}
