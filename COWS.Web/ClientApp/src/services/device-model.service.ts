import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class DeviceModelService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/DeviceModels');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/DeviceModels/' + id);
  }

  public getForLookup(): Observable<any> {
    return this.api.get('api/DeviceModels/GetForLookup');
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/DeviceModels/').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.devModelId, item.manfModelName));
      }));
  }

  public create(dev): Observable<any> {
    return this.api.post('api/DeviceModels/', dev);
  }

  public update(id: number, dev): Observable<any> {
    return this.api.put('api/DeviceModels/' + id, dev);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/DeviceModels/' + id);
  }
}
