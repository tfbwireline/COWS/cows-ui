import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class UcaasBillActivityService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/UcaasBillActivities');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/UcaasBillActivities/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/UcaasBillActivities').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.ucaaSBillActyDes, item.ucaaSBillActyId));
      }));
  }

  public getByUcaasPlanTypeId(ucaasPlanTypeId: number): Observable<any> {
    return this.api.get('api/UcaasBillActivities').pipe(
      map((res: any) => res.filter(i => i.ucaaSPlanTypeId == ucaasPlanTypeId)));
  }
}
