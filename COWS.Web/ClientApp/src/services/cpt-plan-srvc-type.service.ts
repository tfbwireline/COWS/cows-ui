import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators';
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class CptPlanSrvcTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/CPT/PlanServiceTypes');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/CPT/PlanServiceTypes/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/CPT/PlanServiceTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.cptPlnSrvcTypeId, item.cptPlnSrvcType));
      }));
  }
}
