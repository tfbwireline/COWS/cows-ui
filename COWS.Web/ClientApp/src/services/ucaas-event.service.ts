import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class UcaasEventService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/Events/Ucaas');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/Events/Ucaas/' + id);
  }

  public create(ucaas): Observable<any> {
    return this.api.post('api/Events/Ucaas/', ucaas);
  }

  public update(id: number, ucaas): Observable<any> {
    return this.api.put('api/Events/Ucaas/' + id, ucaas);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/Events/Ucaas/' + id);
  }
}
