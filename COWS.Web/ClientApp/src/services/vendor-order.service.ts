import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { VendorOrder } from '../models';

@Injectable({
  providedIn: 'root'
})

export class VendorOrderService {
  constructor(private api: ApiHelperService) { }

  public get(vendorOrderId: number): Observable<any> {
    return this.api.get('api/VendorOrder/' + vendorOrderId);
  }

  public get2(vendorOrderId: number): Observable<any> {
    return this.api.get('api/VendorOrder/' + vendorOrderId + '/Version2');
  }

  public getEmailsByVendorOrderId(vendorOrderId: number): Observable<any> {
    return this.api.get('api/VendorOrder/' + vendorOrderId + '/Emails');
  }

  public getFormsByVendorOrderId(vendorOrderId: number): Observable<any> {
    return this.api.get('api/VendorOrder/' + vendorOrderId + '/Forms');
  }

  public getByOrderId(orderId: number): Observable<any> {
    return this.api.get('api/VendorOrder/Order/' + orderId);
  }

  public getVendorOrderTerminating(vendorOrderId: string, h5FolderCustomerId: number, ftn: string, isTerminating: boolean): Observable<any> {
    let params = {
      vendorOrderId: vendorOrderId,
      h5FolderCustomerId: h5FolderCustomerId,
      ftn: ftn,
      isTerminating: isTerminating
    }
    return this.api.get('api/VendorOrder/GetVendorOrderTerminating/', { params: params });
  }

  public getVendorOrder(vendorOrderId: string, h5FolderCustomerId: number, ftn: string): Observable<any> {
    let params = {
      vendorOrderId: vendorOrderId,
      h5FolderCustomerId: h5FolderCustomerId,
      ftn: ftn
    }
    return this.api.get('api/VendorOrder/GetVendorOrder/', { params: params });
  }

  public create(vendorOrder: VendorOrder): Observable<any> {
    return this.api.post('api/VendorOrder/', vendorOrder);
  }

  public update(id: number, vendorOrder: VendorOrder): Observable<any> {
    return this.api.put('api/VendorOrder/' + id, vendorOrder);
  }
}
