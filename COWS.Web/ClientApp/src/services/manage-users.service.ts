import { Injectable } from '@angular/core';
import { ApiHelperService } from "../shared/index";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManageUsersService {

  constructor(private api: ApiHelperService) { }

  public getUserProfiles(): Observable<any> {
    return this.api.get('api/ManageUsers');
  }

  public getUserProfilesByUserID(userId: string): Observable<any> {
    return this.api.get('api/ManageUsers/' + userId);
  }

  public getOrderActions(): Observable<any> {
    return this.api.get('api/ManageUsers/getOrderActions/');
  }

  public getWFMUserAssignments(userID: number, userProfileID: number): Observable<any> {
    let params = {
      userID: userID,
      userProfileID: userProfileID
    }
    return this.api.get('api/ManageUsers/getWFMUserAssignments', { params: params });
  }

  public updateWFMUserAssignments(selectedAssignmentKeys: string): Observable<any> {
    return this.api.get('api/ManageUsers/updateWFMUserAssignments/' + selectedAssignmentKeys);
  }

    public insertWFMAssignment(wfmUserAssignments): Observable<any> {
    return this.api.post('api/ManageUsers/', wfmUserAssignments);
  }

  public updateUserPermissions(userID: number, newStatus: string, newManagerAdid: string, newAutoAssignment: string): Observable<any> {
    let model = {
      userID: userID,
      newStatus: newStatus,
      newManagerAdid: newManagerAdid,
      newAutoAssignment: newAutoAssignment
    }
    return this.api.post('api/ManageUsers/updateUserPermissions', model);
  }
}
