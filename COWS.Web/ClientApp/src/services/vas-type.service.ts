import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';
import { KeyValuePair } from './../shared/global';

@Injectable({
  providedIn: 'root'
})

export class VasTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/VasTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/VasTypes/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/VasTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.vasTypeDes, item.vasTypeId));
      }));
  }
}
