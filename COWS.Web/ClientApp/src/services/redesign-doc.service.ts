import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { DocEntity } from '../models/doc-entity.model';

@Injectable({
  providedIn: 'root'
})

export class RedesignDocService {

  constructor(private api: ApiHelperService) { }

  //public getRedesignDocsByRedesignId(rId: number): Observable<any> {
  //  let params = {
  //    rId: rId
  //  }
  //  return this.api.get('api/RedesignDoc/GetRedesignDocsByRedesignId', { params: params });
  //}
 
  //public getRedesignDocByRedesignDocId(rDocId: number): Observable<any> {
  //  let params = {
  //    rDocId: rDocId
  //  }
  //  var data = this.api.get('api/RedesignDoc/GetRedesignDocByRedesignDocId', { params: params });
  //  return data;
  //}

  public get(): Observable<any> {
    return this.api.get('api/RedesignDoc');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/RedesignDoc/' + id);
  }

  public create(model): Observable<any> {
    return this.api.post('api/RedesignDoc/', model);
  }

  public deleteRedesignDocByRedesignDocId(rDocId: number): Observable<any> {
    return this.api.delete('api/RedesignDoc/' + rDocId);
  }

  public download(fileNme: string, base64string: string): Observable<any> {
    let params = {
      fileName: fileNme,
      base64string: base64string
    }
    return this.api.get('api/RedesignDoc/Download', { params: params, responseType: "blob" });
  }

  // Used for Doc-Download component
  public getRedesignDocumentById(id: number): Observable<any> {
    return this.api.get('api/RedesignDoc/GetDocument/' + id);
  }
}

