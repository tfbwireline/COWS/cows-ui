import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MDS3rdPartyVendorService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MDS3rdPartyVendors');
  }

  public getForLookup(): Observable<any> {
    return this.api.get('api/MDS3rdPartyVendors/GetForLookup');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MDS3rdPartyVendors/' + id);
  }

  public create(vndr): Observable<any> {
    return this.api.post('api/MDS3rdPartyVendors/', vndr);
  }

  public update(id: number, vndr): Observable<any> {
    return this.api.put('api/MDS3rdPartyVendors/' + id, vndr);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/MDS3rdPartyVendors/' + id);
  }
}
