import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { SiptEventDoc } from '../models';
import { KeyValuePair } from './../shared/global';

@Injectable({
  providedIn: 'root'
})

export class SiptEventService {

  constructor(private api: ApiHelperService) { }

  public getById(id: number): Observable<any> {
    return this.api.get('api/SiptEvent/' + id);
  }

  public create(model): any {
    return this.api.post('api/SiptEvent/', model);
  }

  public update(id: number, model): any {
    return this.api.put('api/SiptEvent/' + id, model);
  }

  public getSiptProdType(): Observable<any> {
    return this.api.get('api/SiptEvent/getSiptProdType').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.siptProdTypeDes, item.siptProdTypeId));
      }));
  }

  public getSiptTollType(): Observable<any> {
    return this.api.get('api/SiptEvent/getSiptTollType').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.siptTollTypeDes, item.siptTollTypeId));
      }));
  }

  public getSiptActyType(): Observable<any> {
    return this.api.get('api/SiptEvent/getSiptActyType').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.siptActyTypeDes, item.siptActyTypeId));
      }));
  }

  public download(doc: SiptEventDoc): Observable<any> {
    let model = {
      fileName: doc.fileNme,
      base64string: doc.base64string
    }

    return this.api.post('api/SiptEvent/Download',  model, { responseType: "blob" });
  }

  // Used for Doc-Download component
  public getSiptDocumentById(id: number): Observable<any> {
    return this.api.get('api/SiptEvent/GetDocument/' + id);
  }
}
