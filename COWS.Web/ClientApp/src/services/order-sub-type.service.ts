import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class OrderSubTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/OrderSubTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/OrderSubTypes/' + id);
  }
}
