import { HttpClient, HttpEvent, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiHelperService } from "../shared/index";
import { VendorFolder, VendorFolderContact, VendorFolderTemplate } from '../models/index';

@Injectable({
    providedIn: 'root'
})

export class VendorOrderDetailsService {
    constructor(private api: ApiHelperService, private http: HttpClient) { }

    public getVendorOrderDetailsByOrderId(vendorOrderId): Observable<any> {
        let params = {
            vendorOrderId: vendorOrderId
        }
        return this.api.get('api/VendorOrder/GetVendorOrderDetailsByOrderId/', { params: params });
    }

    public getEmailAttachment(vendorOrderId): Observable<any> {
        let params = {
            vendorOrderId: vendorOrderId
        }
        return this.api.get('api/VendorOrder/GetEmailAttachment/', { params: params });
    }

    public getVendorOrderEmailType(): Observable<any> {
        return this.api.get('api/VendorOrder/GetVendorOrderEmailType');
    }
}
