import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { UserProfile, IUserProfileResponse } from "./../models";
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

@Injectable({
  providedIn: 'root'
})

export class UserProfileService {

  constructor(private api: ApiHelperService, private http: HttpClient) {
  }
  
  public getUserProfiles(): Observable<any> {
    return this.api.get('api/UserProfile');
  }

  public getUserProfileByUserId(userId: number, loggedInUserId: number): Observable<any> {
    let params = {
      userId: userId,
      loggedInUserId: loggedInUserId
    }
    return this.api.get('api/UserProfile/getUserProfileByUserId', { params: params });
  }

  public updateUserProfile(userProfile): Observable<any> {
    return this.api.post('api/UserProfile/updateUserProfile', userProfile);
  }

  public isAdminUserProfile(id: number): Observable<boolean> {
    return this.api.get('api/UserProfile/isAdminUserProfile/' + id);
  }

  public isEventUserProfile(userId: number, eventTypeId: number): Observable<boolean> {
    return this.api.get('api/UserProfile/isEventUserProfile/' + userId + '/EventType/' + eventTypeId);
  }

  public getMapUserProfilesByUserId(userId: number): Observable<any> {
    let params = {
      userId: userId
    }
    return this.api.get('api/UserProfile/getMapUserProfilesByUserId', { params: params });
  }
}
