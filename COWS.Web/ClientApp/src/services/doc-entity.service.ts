import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiHelperService } from "../shared/index";
import { DocEntity } from '../models/index';

@Injectable({
  providedIn: 'root'
})

export class DocEntityService {

  constructor(private api: ApiHelperService, private http: HttpClient) { }

  public download(doc: DocEntity): Observable<any> {
    return this.api.post('api/DocEntity/Download', doc, { responseType: "blob" });
  }

  
}
