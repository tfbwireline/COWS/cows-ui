import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators';
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class ActivatorsService {

  constructor(private api: ApiHelperService) {
  }  

  public GetAllFailCodes(): Observable<any> {
    return this.api.get('api/Activators/GetAllFailCodes');
  }

  public GetAllFailActivities(eventTypeId: number): Observable<any> {
    return this.api.get('api/Activators/GetAllFailActy/' + eventTypeId).pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.failActyId, item.failActyDes));
      }));
  }

  public GetAllSuccessActivities(eventTypeId: number): Observable<any> {
    return this.api.get('api/Activators/GetAllSuccActy/' + eventTypeId).pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.sucssActyId, item.sucssActyDes));
      }));
  }
}
