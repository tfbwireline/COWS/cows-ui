import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class WorldHolidayService {

  constructor(private api: ApiHelperService) { }

  public get(includePast?: boolean): Observable<any> {
    if (includePast === true) {
      let params = new HttpParams().set('includePast', 'true');
      return this.api.get('api/WorldHolidays', { params: params })
    }
    return this.api.get('api/WorldHolidays');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/WorldHolidays/' + id);
  }

  public create(hday): Observable<any> {
    return this.api.post('api/WorldHolidays/', hday);
  }

  public update(id: number, hday): Observable<any> {
    return this.api.put('api/WorldHolidays/' + id, hday);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/WorldHolidays/' + id);
  }
}
