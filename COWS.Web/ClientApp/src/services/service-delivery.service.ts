import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators';
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class ServiceDeliveryService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/ServiceDelivery');
  }

  public getForLookup(): Observable<any> {
    return this.api.get('api/ServiceDelivery/GetForLookup');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/ServiceDelivery/' + id);
  }

  public create(obj): Observable<any> {
    return this.api.post('api/ServiceDelivery/', obj);
  }

  public update(id: number, obj): Observable<any> {
    return this.api.put('api/ServiceDelivery/' + id, obj);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/ServiceDelivery/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/ServiceDelivery/GetForLookup').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.sprintCpeNcrDes, item.sprintCpeNcrId));
      }));
  }
}
