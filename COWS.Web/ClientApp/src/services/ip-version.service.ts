import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class IPVersionService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/IPVersions');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/IPVersions/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/IPVersions').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.ipVerId, item.ipVerNme));
      }));
  }
}
