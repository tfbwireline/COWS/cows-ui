import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class CcdService {

  constructor(private api: ApiHelperService) { }

  public getCcdReason(): Observable<any> {
    return this.api.get('api/Ccd/GetCcdReason').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.ccdMissdReasDes, item.ccdMissdReasId));
      }));
  }

  public searchCcd(m5Ctn: string, h5: number): Observable<any> {
    let params = {
      m5Ctn: m5Ctn,
      h5: h5
    }
    return this.api.get('api/Ccd/SearchCcd', { params: params });
  }

  public changeCcd(ccd): Observable<any> {
    return this.api.post('api/Ccd/', ccd);
  }
}
