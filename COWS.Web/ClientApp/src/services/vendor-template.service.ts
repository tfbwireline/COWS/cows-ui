import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class VendorTemplateService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/VendorTemplate');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/VendorTemplate/' + id);
  }

  public download(id: number): Observable<any> {
    return this.api.get('api/VendorTemplate/' + id + '/download', { responseType: "blob" });
  }

  public create(vndrTemplate): Observable<any> {
    return this.api.post('api/VendorTemplate/', vndrTemplate);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/VendorTemplate/' + id);
  }
}
