import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { VendorOrderEmail } from '../models';

@Injectable({
  providedIn: 'root'
})

export class VendorOrderEmailService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/VendorOrderEmails');
  }

  public create(vendorOrderEmail: VendorOrderEmail): Observable<any> {
    return this.api.post('api/VendorOrderEmails/', vendorOrderEmail);
  }

  public update(id: number, vendorOrderEmail: VendorOrderEmail): Observable<any> {
    return this.api.put('api/VendorOrderEmails/' + id, vendorOrderEmail);
  }

  public sendEmail(id: number, vendorOrderEmail: VendorOrderEmail): Observable<any> {
    return this.api.put('api/VendorOrderEmails/' + id + '/Send', vendorOrderEmail);
  }

  public delete(id: number): any {
    return this.api.delete('api/VendorOrderEmails/' + id);
  }

  public addVendorOrderEmail(vendorOrderID: number, emailTypeID: number, vendorEmailLangID: number): Observable<any> {
    let params = {
      vendorOrderID: vendorOrderID,
      emailTypeID: emailTypeID,
      vendorEmailLangID: vendorEmailLangID
    }
    return this.api.get('api/VendorOrderEmails/AddVendorOrderEmail', { params: params });
  }

  public getVendorOrderEmails(vendorOrderID: number): Observable<any> {
    let params = {
      vendorOrderID: vendorOrderID
    }
    return this.api.get('api/VendorOrderEmails/GetVendorOrderEmails', { params: params });
  }

  public updateEmailAckDate(id: number, vendorOrderMs: any): Observable<any> {
    return this.api.put('api/VendorOrderEmails/' + id + '/UpdateEmailAckDate', vendorOrderMs);
  }
}
