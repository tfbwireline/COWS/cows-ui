import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { VendorOrderEmail } from '../models';

@Injectable({
  providedIn: 'root'
})

export class VendorFormService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/VendorOrderForms');
  }

  public download(id: number): Observable<any> {
    return this.api.get('api/VendorOrderForms/' + id + '/download', { responseType: "blob" });
  }

  public create(obj: any): Observable<any> {
    return this.api.post('api/VendorOrderForms', obj);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/VendorOrderForms/' + id);
  }
}
