import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class SystemConfigService {

  constructor(private api: ApiHelperService) { }

  public getSysCfgByName(name: string): Observable<any> {
    return this.api.get('api/Config/GetByName/' + name);
  }

  public updateSysCfgValue(name: string, value: string): Observable<any> {
    let params = {
      value: value
    }
    return this.api.get('api/Config/UpdateSysCfgValue/' + name, { params: params });
  }
}
