import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { H5Doc } from '../models';

@Injectable({
  providedIn: 'root'
})

export class H5DocService {

  constructor(private api: ApiHelperService) { }

  public getByOrderId(id: number): Observable<any> {
    return this.api.get('api/H5Docs/order/' + id);
  }

  public getByFolderId(id: number): Observable<any> {
    return this.api.get('api/H5Docs/folder/' + id);
  }
}
