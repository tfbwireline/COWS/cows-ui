import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class UcaasProdTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/UcaasProdTypes');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/UcaasProdTypes/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/UcaasProdTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.ucaaSProdType, item.ucaaSProdTypeId));
      }));
  }
}
