import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class EventLockService {

  constructor(private api: ApiHelperService) { }

  public checkLock(eventId: number): any {
    return this.api.get('api/EventLock/CheckLock/' + eventId);
  }

  public lock(eventId: number): any {
    return this.api.get('api/EventLock/Lock/' + eventId);
  }

  public unlock(eventId: number): any {
    return this.api.get('api/EventLock/Unlock/' + eventId);
  }
}
