import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { CptDoc } from '../models';

@Injectable({
  providedIn: 'root'
})

export class CptService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/CPT');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/CPT/' + id);
  }

  public getReturnedToSde(): Observable<any> {
    return this.api.get('api/CPT/GetReturnedToSde');
  }

  public search(cptCustNbr: string, companyName: string, custShortName: string): Observable<any> {
    let params = {
      cptCustNbr: cptCustNbr,
      companyName: companyName,
      custShortName: custShortName
    }

    return this.api.get('api/CPT/SearchByParams', { params: params });
  }

  //public getCustomerH1Data(): Observable<any> {
  //  return this.api.get('api/CPT/GetCustomerH1Data');
  //}

  public getCustomerH1Data(h1: string, custName: string): Observable<any> {
    let params = {
      h1: h1,
      custName: custName
    }
    return this.api.get('api/CPT/GetCustomerH1Data', { params: params });
  }

  public getAssignmentByProfileId(usrPrfId: number): Observable<any> {
    return this.api.get('api/CPT/GetAssignmentByProfileId/' + usrPrfId);
  }

  public getCptAssignmentStats(): Observable<any> {
    return this.api.get('api/CPT/GetCptAssignmentStats');
  }

  public getCptCancelStat(): Observable<any> {
    return this.api.get('api/CPT/GetCptCancelStat');
  }

  public getAutoAssignNTE(): Observable<any> {
    return this.api.get('api/CPT/GetAutoAssignNTE');
  }

  public create(cpt): Observable<any> {
    return this.api.post('api/CPT/', cpt);
  }

  public update(cptId: number, cpt): Observable<any> {
    return this.api.put('api/CPT/' + cptId, cpt);
  }

  public checkLock(cptId: number): any {
    return this.api.get('api/CPT/CheckLock/' + cptId);
  }

  public lock(cptId: number): any {
    return this.api.get('api/CPT/Lock/' + cptId);
  }

  public unlock(cptId: number): any {
    return this.api.get('api/CPT/Unlock/' + cptId);
  }
}
