import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class VendorService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/Vendors');
  }

  public getByCode(code: string): Observable<any> {
    return this.api.get('api/Vendors/' + code);
  }

  public create(vendor): Observable<any> {
    return this.api.post('api/Vendors/', vendor);
  }

  public update(code: string, vendor): Observable<any> {
    return this.api.put('api/Vendors/' + code, vendor);
  }

  public delete(code: string): Observable<any> {
    return this.api.delete('api/Vendors/' + code);
  }
}
