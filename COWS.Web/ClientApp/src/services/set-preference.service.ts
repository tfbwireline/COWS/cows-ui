import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class SetPreferenceService {

  constructor(private api: ApiHelperService) { }

  public getSetPreference(): Observable<any> {
    return this.api.get('api/SetPreference');
  }

  public getSetPreferenceById(id: number): Observable<any> {
    return this.api.get('api/SetPreference/' + id);
  }

  public createSetPreference(service): Observable<any> {
    return this.api.post('api/SetPreference/', service);
  }

  public updateSetPreference(id: number, service): Observable<any> {
    return this.api.put('api/SetPreference/' + id, service);
  }

  public deleteSetPreference(id: number): Observable<any> {
    return this.api.delete('api/SetPreference/' + id);
  }
}
