import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class TelcoService {

  constructor(private api: ApiHelperService) { }

  public getTelco(): Observable<any> {
    return this.api.get('api/Telco');
  }

  public getTelcoById(id: number): Observable<any> {
    return this.api.get('api/Telco/' + id);
  }

  public createTelco(service): Observable<any> {
    return this.api.post('api/Telco/', service);
  }

  public updateTelco(id: number, service): Observable<any> {
    return this.api.put('api/Telco/' + id, service);
  }

  public deleteTelco(id: number): Observable<any> {
    return this.api.delete('api/Telco/' + id);
  }
}
