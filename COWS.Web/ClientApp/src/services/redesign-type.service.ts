import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})

export class RedesignTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/RedesignTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/RedesignTypes/' + id);
  }

  public getByRedesignCategoryId(catId: number): Observable<any> {
    return this.api.get('api/RedesignTypes').pipe(
      map((res: any) => res.filter(i => i.redsgnCatId == catId)));
  }
}
