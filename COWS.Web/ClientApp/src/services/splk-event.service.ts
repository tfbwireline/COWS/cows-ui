import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';
import { KeyValuePair } from './../shared/global';

@Injectable({
  providedIn: 'root'
})

export class SplkEventService {

  constructor(private api: ApiHelperService) { }

  //public get(): Observable<any> {
  //  return this.api.get('api/MplsEventTypes');
  //}
  public create(model): any {
    return this.api.post('api/SplkEvent/', model);
  }
  //public getSiptRltdOrdr(id: number): Observable<any> {
  //  return this.api.get('api/SplkEvent/getSiptRltdOrdr/' + id);
  //}
  public getSysCfgValue(param: string): Observable<any> {
    return this.api.get('api/SplkEvent/getSysCfgValue/' + param);
  }

  public GetSplkEventById(id: number): Observable<any> {
    return this.api.get('api/SplkEvent/GetSplkEventById/' + id);
  }
  public update(id: number, model): any {
    return this.api.put('api/SplkEvent/' + id, model);
  }
  //public getSiptTollType(): Observable<any> {
  //  return this.api.get('api/SplkEvent/getSiptTollType').pipe(
  //    map((res: any) => res),
  //    map((data) => {
  //      return data.map((item) => new KeyValuePair(item.siptTollTypeDes, item.siptTollTypeId));
  //    }));
  //}
  //public getSplkEventType(): Observable<any> {
  //  return this.api.get('api/SplkEvent/getSplkEventType').pipe(
  //    map((res: any) => res),
  //    map((data) => {
  //      return data.map((item) => new FormControlItem(item.splkEventTypeId, item.splkEventTypeDes));
  //    }));
  //}
  public getSplkEventType(): Observable<any> {
    return this.api.get('api/SplkEvent/getSplkEventType').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.splkEventTypeDes, item.splkEventTypeId));
      }));
  }
  public getSplkActyType(): Observable<any> {
    return this.api.get('api/SplkEvent/getSplkActyType').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.splkActyTypeId, item.splkActyTypeDes));
      }));
  }

  //public getSiptActyType(): Observable<any> {
  //  return this.api.get('api/SplkEvent/getSplkActyType').pipe(
  //    map((res: any) => res),
  //    map((data) => {
  //      return data.map((item) => new KeyValuePair(item.siptActyTypeDes, item.siptActyTypeId));
  //    }));
  //}
  //public KeyValuepair(item): Observable<any> {
  //      return map((item) => new KeyValuePair(item.siptActyTypeDes, item.siptActyTypeId));

  //}
}
