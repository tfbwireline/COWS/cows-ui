import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class MplsAccessBandwidthService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MplsAccessBandwidth');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MplsAccessBandwidth/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/MplsAccessBandwidth').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.mplsAccsBdwdDes, item.mplsAccsBdwdId));
      }));
  }
}
