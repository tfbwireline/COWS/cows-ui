import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class CSCService {

  constructor(private api: ApiHelperService) {
  }

  public saveCSCSpecificData(model): Observable<any> {
    return this.api.post('api/CSC/SaveCSCSpecificData/', model);
  }

}
