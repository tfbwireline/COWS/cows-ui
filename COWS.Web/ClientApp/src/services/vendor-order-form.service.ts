import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { FormGroup, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})

export class VendorOrderFormService {
  form: FormGroup = new FormGroup({
    vendorFolder: new FormGroup({
      vendorName: new FormControl({ value: '', disabled: false }),
      country: new FormControl({ value: '', disabled: false })
    }),
    vendorOrder: new FormGroup({
      vendorOrderNo: new FormControl({ value: '', disabled: false }),
      customerId: new FormControl({ value: '', disabled: false }),
      ctn: new FormControl({ value: '', disabled: false })
    }),
    general: new FormGroup({
      vendorName: new FormControl({ value: null, disabled: false }),
      emailPdl: new FormControl({ value: null, disabled: false }),
      streetAddress1: new FormControl({ value: null, disabled: false }),
      streetAddress2: new FormControl({ value: null, disabled: false }),
      building: new FormControl({ value: null, disabled: false }),
      floor: new FormControl({ value: null, disabled: false }),
      room: new FormControl({ value: null, disabled: false }),
      city: new FormControl({ value: null, disabled: false }),
      state: new FormControl({ value: null, disabled: false }),
      country: new FormControl({ value: null, disabled: false }),
      zip: new FormControl({ value: null, disabled: false }),
      isTerminating: new FormControl({ value: null, disabled: false }),
      comments: new FormControl({ value: null, disabled: false }),
    }),
    h5FolderSearch: new FormGroup({
      customerId: new FormControl({ value: '', disabled: false }),
      customerName: new FormControl({ value: '', disabled: false }),
      city: new FormControl({ value: '', disabled: false }),
      country: new FormControl({ value: '', disabled: false }),
    }),
    h5Folder: new FormGroup({
      customerId: new FormControl({ value: '', disabled: false }),
      customerName: new FormControl({ value: '', disabled: false }),
      city: new FormControl({ value: '', disabled: false }),
      country: new FormControl({ value: '', disabled: false }),
    }),
    email: new FormGroup({
      emailType: new FormControl({ value: null, disabled: false })
    }),
    emailUpdate: new FormGroup({
      emailType: new FormControl({ value: null, disabled: false }),
      to: new FormControl({ value: null, disabled: false }),
      cc: new FormControl({ value: null, disabled: false }),
      subject: new FormControl({ value: null, disabled: false }),
      body: new FormControl({ value: null, disabled: false })
    })
  })

  constructor() {
  }

  getValue(name: string) {
    return this.form.get(name).value
  }

  setValue(name: string, value: any) {
    this.form.get(name).setValue(value)
  }

  clear() {
    this.form.reset()
  }
}
