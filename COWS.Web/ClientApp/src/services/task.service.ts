import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class TaskService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/Tasks/');
  }

  public getById(id: number): Observable<any> {
    return this.api.get(`api/Tasks/${id}`);
  }

  public getCurrentStatus(id: number, wgId: number, orderId: number): Observable<any> {
    let params = {
      wgId: wgId,
      orderId: orderId
    }
    return this.api.get(`api/Tasks/${id}/CurrentStatus`, { params: params });
  }
}
