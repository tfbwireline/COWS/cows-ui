import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class StateService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/States');
  }
}
