import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { KeyValuePair } from "../shared/global";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CptPrimSecondaryTransportService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/CPT/PrimSecondaryTransports');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/CPT/PrimSecondaryTransports/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/CPT/PrimSecondaryTransports').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.cptPrimScndyTprt, item.cptPrimScndyTprtId));
      }));
  }
}
