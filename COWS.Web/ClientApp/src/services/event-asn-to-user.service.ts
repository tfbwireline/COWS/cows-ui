import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { EventInterval } from "./../models";

@Injectable({
  providedIn: 'root'
})

export class EventAsnToUserService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/EventAsnToUsers');
  }

  public getByEventId(id: number): any {
    return this.api.get('api/EventAsnToUsers/Event/' + id);
  }
}
