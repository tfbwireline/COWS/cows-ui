import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MDSMACActivityService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MDSMACActivities');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MDSMACActivities/' + id);
  }

  public getForLookup(): Observable<any> {
    return this.api.get('api/MDSMACActivities/GetForLookup');
  }

  public create(activity): Observable<any> {
    return this.api.post('api/MDSMACActivities/', activity);
  }

  public update(id: number, activity): Observable<any> {
    return this.api.put('api/MDSMACActivities/' + id, activity);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/MDSMACActivities/' + id);
  }

  public updateSysCfgValue(name: string, value: string): Observable<any> {
    let params = {
      value: value
    }
    return this.api.get('api/MDSMACActivities/SysCfg/' + name, { params: params });
  }
}
