import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class MDSNetworkActivityTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MDSNetworkActivityTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MDSNetworkActivityTypes/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/MDSNetworkActivityTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.ntwkActyTypeId, item.ntwkActyTypeDes));
      }));
  }
}
