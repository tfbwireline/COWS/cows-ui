import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators';
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class CptProvTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/CPT/ProvisionTypes');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/CPT/ProvisionTypes/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/CPT/ProvisionTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.cptPrvsnTypeId, item.cptPrvsnType));
      }));
  }
}
