import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class ServiceAssuranceSiteSupportService {

  constructor(private api: ApiHelperService) { }

  public getServiceAssuranceSiteSupport(): Observable<any> {
    return this.api.get('api/ServiceAssuranceSiteSupport');
  }

  public getForLookup(): Observable<any> {
    return this.api.get('api/ServiceAssuranceSiteSupport/GetForLookup');
  }

  public getServiceAssuranceSiteSupportById(id: number): Observable<any> {
    return this.api.get('api/ServiceAssuranceSiteSupport/' + id);
  }

  public createServiceAssuranceSiteSupport(service): Observable<any> {
    return this.api.post('api/ServiceAssuranceSiteSupport/', service);
  }

  public updateServiceAssuranceSiteSupport(id: number, service): Observable<any> {
    return this.api.put('api/ServiceAssuranceSiteSupport/' + id, service);
  }

  public deleteServiceAssuranceSiteSupport(id: number): Observable<any> {
    return this.api.delete('api/ServiceAssuranceSiteSupport/' + id);
  }
}
