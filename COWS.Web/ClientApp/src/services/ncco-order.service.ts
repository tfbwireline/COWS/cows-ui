import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';
import { KeyValuePair } from './../shared/global';

@Injectable({
  providedIn: 'root'
})

export class NccoOrderService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/NccoOrder');
  }

  public getByOrderId(id: number): any {
    return this.api.get('api/NccoOrder/' + id);
  }
  public FindOrdNccoOrder(sortExpression: string, searchCriteria: string): Observable<any> {
    let params = {
      sortExpression: sortExpression,
      searchCriteria: searchCriteria
    }
    return this.api.get('api/NccoOrder/FindOrdNccoOrder', { params: params });
  }
  public FindAllNccoOrder(sortExpression: string, searchCriteria: string): Observable<any> {
    let params = {
      sortExpression: sortExpression,
      searchCriteria: searchCriteria
    }
    return this.api.get('api/NccoOrder/FindAllNccoOrder', { params: params });
  }

  public getNCCOOrderData(id: number): Observable<any> {
    return this.api.get('api/NccoOrder/GetNCCOOrderData/' + id);
  }

  public passUserCSGLevel(csgLvlId: number): any {
    return this.api.get('api/NccoOrder/PassUserCSGLevel/' + csgLvlId);
  }

  public getNCCOWGDetails(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/NccoOrder/GetNCCOWGDetails', { params: params });
  }
  public create(model): any {
    return this.api.post('api/NccoOrder/', model);
  }
  public update(id: number, model): any {
    return this.api.put('api/NccoOrder/' + id, model);
  }
}
