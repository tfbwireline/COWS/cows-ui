import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class NgvnEventService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/Events/Ngvn/');
  }

  public getById(id: number): any {
    return this.api.get('api/Events/Ngvn/' + id);
  }

  public create(model): any {
    return this.api.post('api/Events/Ngvn/', model);
  }

  public update(id: number, model): any {
    return this.api.put('api/Events/Ngvn/' + id, model);
  }

  public delete(id: number): any {
    return this.api.delete('api/Events/Ngvn/' + id);
  }

  public getNgvnProdType(): any {
    return this.api.get('api/Events/Ngvn/GetNgvnProdType/');
  }
}
