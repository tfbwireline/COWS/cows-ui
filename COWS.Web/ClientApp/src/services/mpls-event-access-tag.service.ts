import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MplsEventAccessTagService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MplsEventAccessTags');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MplsEventAccessTags/' + id);
  }

  public getByEventId(id: number): Observable<any> {
    return this.api.get('api/MplsEventAccessTags/getByEventId/' + id);
  }

  
}
