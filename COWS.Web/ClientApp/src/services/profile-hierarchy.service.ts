import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { ProfileHierarchy } from '../models';

@Injectable({
  providedIn: 'root'
})

export class ProfileHierarchyService {
  profileHierarchy: ProfileHierarchy[]

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/ProfileHierarchy');
  }
}
