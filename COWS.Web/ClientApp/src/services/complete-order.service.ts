import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { Global, WorkGroup, EordrCat, ETasks } from '../shared/global';
import { CompleteOrderEvent } from '../models/complete-order-event.model';
@Injectable({
  providedIn: 'root'
})

export class CompleteOrderService {
  constructor(private api: ApiHelperService) { }
 //public M5CmpltMsg(FTN: string , MsgType: number): Observable<any> {
  //return this.api.post('api/CompleteOrder/',{ params: { FTN: FTN,MsgType: MsgType} });
  //}
public M5CmpltMsg(model : CompleteOrderEvent) : any {
    return this.api.post('api/CompleteOrder', model);
 }
public GetAdminExcPerm(name: string): Observable<any> {
    return this.api.get('api/CompleteOrder',{ params: {name:name}});
  }
}

