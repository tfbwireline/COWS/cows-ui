import { Injectable, Inject } from '@angular/core';
import { Observable, zip } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { filter, map } from "rxjs/operators";
import { NavLink } from "../models";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class NavLinkService {
  menu: NavLink[];
  
  constructor(private api: ApiHelperService) {
  }

  public getMenus(): Observable<any> {
    return this.api.get('api/Menu')
  }

  public validateMenu(url: string[]): Observable<string> {
    return this.api.get('api/Menu/' + url)
  }

  getMenu() {
    this.getMenus().subscribe(res => {
      this.menu = res;

      localStorage.removeItem('menuCount');
      localStorage.setItem('menuCount', this.menu.length.toString());
    });
  }
}
