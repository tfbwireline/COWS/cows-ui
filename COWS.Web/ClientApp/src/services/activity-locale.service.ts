import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class ActivityLocaleService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/ActivityLocale');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/ActivityLocale/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/ActivityLocale').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.actyLocaleDes, +item.actyLocaleId));
      }));
  }
}
