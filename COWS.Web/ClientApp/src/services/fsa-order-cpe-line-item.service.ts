import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class FsaOrderCpeLineItemService {

  constructor(private api: ApiHelperService) { }

  public getByOrderId(id: number): Observable<any> {
    return this.api.get('api/FsaOrderCpeLineItem/order/' + id);
  }
}
