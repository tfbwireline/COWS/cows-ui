import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class QualificationService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/Qualifications');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/Qualifications/' + id);
  }

  public create(qualification): Observable<any> {
    return this.api.post('api/Qualifications/', qualification);
  }

  public update(id: number, qualification): Observable<any> {
    return this.api.put('api/Qualifications/' + id, qualification);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/Qualifications/' + id);
  }

  public checkDuplicate(assignedUserId: number): Observable<any> {
    let params = {
      assignedUserId: assignedUserId
    }
    return this.api.get('api/Qualifications/CheckDuplicate', { params: params });
  }
}
