import { Injectable } from '@angular/core';
import { ApiHelperService } from "../shared/index";
import { map, catchError } from 'rxjs/operators';

import { of, Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})

export class AdditionalCustomerChargeService {
    constructor(private api: ApiHelperService) {}

    public search(ftn: string): Observable<any> {
        return this.api.get(`api/AdditionalCustomerCharge/searchFTN?ftn=${ftn}`);
    }

    public getAdditionalCost(orderId: number, isTerm: boolean): Observable<any> {
        return this.api.get(`api/AdditionalCustomerCharge/getAdditionalCharges?orderId=${orderId}&isTerm=${isTerm.toString()}`);
    }

    public getAdditionalCostHistory(orderId: number, chargeTypeId: number, isTerm: boolean): Observable<any> {
        return this.api.get(`api/AdditionalCustomerCharge/getAdditionalCharges/history?orderId=${orderId}&chargeTypeId=${chargeTypeId}&isTerm=${isTerm.toString()}`);
    }

}