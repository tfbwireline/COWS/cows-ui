import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class CircuitService {

  constructor(private api: ApiHelperService) {
  }

  public GetAddlCharges(orderId: number, isTerm: boolean): Observable<any> {
    let params = {
      orderId: orderId,
      isTerm: isTerm
    }
    return this.api.get('api/Circuit/GetAddlCharges', { params: params });
  }

  public GetAddlChargeHistory(orderId: number, chargeTypeId: number, isTerm: boolean): Observable<any> {
    let params = {
      orderId: orderId,
      chargeTypeId: chargeTypeId,
      isTerm: isTerm
    }
    return this.api.get('api/Circuit/GetAddlChargeHistory', { params: params });
  }

  public GetAddlCostTypes(): Observable<any> {
    return this.api.get('api/Circuit/GetAddlCostTypes');
  }

  public GetStatus(): Observable<any> {
    return this.api.get('api/Circuit/GetStatus');
  }

  public GetCurrencyList(): Observable<any> {
    return this.api.get('api/Circuit/GetCurrencyList');
  }

  public UpdateAdditionalCosts(orderId, model): Observable<any> {
    return this.api.put('api/Circuit/'+ orderId, model);
  }

  public AcrOrderStatus(orderId: number, isTerm: boolean): Observable<any> {
    let params = {
      orderId: orderId,
      isTerm: isTerm
    }
    return this.api.get('api/Circuit/AcrOrderStatus', { params: params });
  }

  public LoadRTSTask(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/Circuit/LoadRTSTask', { params: params });
  }

  public SetJeopardy(orderId: number, noteId: number, jepCode: string): Observable<any> {
    let params = {
      orderId: orderId,
      noteId: noteId,
      jepCode: jepCode
    }
    return this.api.get('api/Circuit/SetJeopardy', { params: params });
  }

  public NotifyNCI(orderId: number, taskId: number): Observable<any> {
    let params = {
      orderId: orderId,
      taskId: taskId
    }
    return this.api.get('api/Circuit/NotifyNCI', { params: params });
  }

  public InsertGOMBillingTask(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/Circuit/InsertGOMBillingTask', { params: params });
  }

  public GetPreQualLineInfo(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/Circuit/GetPreQualLineInfo', { params: params });
  }
}
