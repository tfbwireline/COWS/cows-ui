import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class CustomerUserProfileService {

  constructor(private api: ApiHelperService) { }

  public getCustomerUserProfile(): Observable<any> {
    return this.api.get('api/CustomerUserProfile');
  }

  public CheckH5ById(id: number): Observable<any> {
    return this.api.get('api/CustomerUserProfile/CheckH5/' + id);
  }
  /*
   * here checkUserAccessToAfterHrsSlots   returns boolean
   * public getTimeSlotOccurrences() {
    this.userService.getLoggedInUser().toPromise().then(
      res => {
        this.spinner.show();
        let adid = res.userAdid;
        this.service.checkUserAccessToAfterHrsSlots(adid).toPromise().then(
          res => {
            let bShowAfterHrsSlots = res;
            this.service.getTimeSlotOccurances(5, bShowAfterHrsSlots).subscribe(
              res => {
                this.timeSlots = res;
                this.spinner.hide();
              },
              error => {
                this.spinner.hide();
              });
          });
      });
  }
  */
  public createCustomerUserProfile(service): Observable<any> {
    return this.api.post('api/CustomerUserProfile/', service);
  }

  public updateCustomerUserProfile(id: number, service): Observable<any> {
    return this.api.put('api/CustomerUserProfile/' + id, service);
  }

  //public deleteTelco(id: number): Observable<any> {
  //  return this.api.delete('api/CustomerUserProfile/' + id);
  //}
}
