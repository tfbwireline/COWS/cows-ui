import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class OrderNoteService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/OrderNote');
  }

  public getByNoteId(id: number): any {
    return this.api.get('api/OrderNote/' + id);
  }

  public FindForGivenOrderAndNoteType(orderId: number, noteTypeId: number, sortExpression: string, prntPrfID: number, userID: number): Observable<any> {
    let params = {
      orderId: orderId,
      noteTypeId: noteTypeId,
      sortExpression: sortExpression,
      prntPrfID: prntPrfID,
      userID:userID
    }
    return this.api.get('api/OrderNote/FindForGivenOrderAndNoteType', { params: params });
  }

  public getByOrderId(id: number): Observable<any> {
    return this.api.get('api/OrderNote/Order/' + id);
  }

  public getByOrderId2(id: number): Observable<any> {
    return this.api.get('api/OrderNote/Order2/' + id);
  }


  public create(model): Observable<any>  {
    return this.api.post('api/OrderNote/', model);
  }

  public createNote(model): Observable<any> {
    return this.api.post('api/OrderNote/CreateNote', model);
  }
  //public update(id: number, model): any {
  //  return this.api.put('api/Events/AccessDeliveries/' + id, model);
  //}

  public delete(id: number): any {
    return this.api.delete('api/OrderNote/' + id);
  }
}
