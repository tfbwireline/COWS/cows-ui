import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class H5FolderService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/H5Folders');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/H5Folders/' + id);
  }

  public create(h5): Observable<any> {
    return this.api.post('api/H5Folders/', h5);
  }

  public update(h5FolderId: number, h5): Observable<any> {
    return this.api.put('api/H5Folders/' + h5FolderId, h5);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/H5Folders/' + id);
  }

  public getByOrderId(id: number): Observable<any> {
    return this.api.get('api/H5Folders/order/' + id);
  }

  public search(custId: number, custNme: string, m5Ctn: string, city: string, country: string): Observable<any> {
    let params = {
      custId: custId,
      custNme: custNme,
      m5Ctn: m5Ctn,
      city: city,
      country: country
    }

    return this.api.get('api/H5Folders/SearchByParams', { params: params });
  }

  public getOrderByH5FolderId(id: number): Observable<any> {
    return this.api.get('api/H5Folders/GetOrderByH5FolderId/' + id);
  }
}
