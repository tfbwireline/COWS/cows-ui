import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class CacheAdminService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/CacheAdmin');
  }

  public removeCacheObject(name: string): Observable<any> {
    return this.api.get('api/CacheAdmin/RemoveCacheObject/' + name);
  }
}




