import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MDS3rdPartyServiceService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MDS3rdPartyServices');
  }

  public getForLookup(): Observable<any> {
    return this.api.get('api/MDS3rdPartyServices/GetForLookup');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/MDS3rdPartyServices/' + id);
  }

  public create(srvc): Observable<any> {
    return this.api.post('api/MDS3rdPartyServices/', srvc);
  }

  public update(id: number, srvc): Observable<any> {
    return this.api.put('api/MDS3rdPartyServices/' + id, srvc);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/MDS3rdPartyServices/' + id);
  }
}
