import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})
export class NRMService {

  constructor(private api: ApiHelperService) {
  }

  public GetNRMServiceInstanceList(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/NRM/GetNRMServiceInstanceList', { params: params });
  }

  public GetNRMCircuitList(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/NRM/GetNRMCircuitList', { params: params });
  }

  public GetNRMVendorList(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/NRM/GetNRMVendorList', { params: params });
  }

  public GetNRMNotes(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/NRM/GetNRMNotes', { params: params });
  }
}
