import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})
export class OrderStdiHistoryService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/StdiHistory');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/StdiHistory/' + id);
  }

  public getByOrderId(id: number): Observable<any> {
    return this.api.get('api/StdiHistory/order/' + id);
  }

  public create(model: any): Observable<any> {
    return this.api.post('api/StdiHistory/', model);
  }
}
