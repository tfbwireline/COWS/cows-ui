import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class OdieRspnInfoService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/OdieRspnInfo');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/OdieRspnInfo/' + id);
  }

  public getByReqId(reqId: number): Observable<any> {
    return this.api.get('api/OdieRspnInfo/GetByReqId/' + reqId);
  }
}
