import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
//import { EventInterval } from "./../models";

@Injectable({
  providedIn: 'root'
})

export class WorkflowService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/Workflows');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/Workflows/' + id);
  }

  public getForEvent(eventTypeId: number, eventStusId: number, orderType: number = 0): Observable<any> {
    return this.api.get('api/Workflows/Events', { params: { eventTypeId: eventTypeId, eventStusId: eventStusId, orderType: orderType } });
  }
}
