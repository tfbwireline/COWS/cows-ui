import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class SprintHolidayService {

  constructor(private api: ApiHelperService) { }

  public get(includePast?: boolean): Observable<any> {
    if (includePast === true) {
      let params = new HttpParams().set('includePast', 'true');
      return this.api.get('api/SprintHolidays', { params: params })
    }
    return this.api.get('api/SprintHolidays');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/SprintHolidays/' + id);
  }

  public create(proj): Observable<any> {
    return this.api.post('api/SprintHolidays/', proj);
  }

  public update(id: number, proj): Observable<any> {
    return this.api.put('api/SprintHolidays/' + id, proj);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/SprintHolidays/' + id);
  }
}
