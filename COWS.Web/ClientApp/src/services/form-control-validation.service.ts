import { AbstractControl, ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FormControlValidationService {

  static getValidatorErrorMessage(
    validatorName: string,
    validatorValue?: any,
    sourceName?: string,
    customMessage?: string,
    customMessageObj?: object
  ) {
    const config = {
      required: `${sourceName} is required`,
      invalidEmailAddress: 'Enter a valid email address using the example@domain.com format.',
      minlength: `Minimum length ${validatorValue.requiredLength}`,
      maxlength: `Maximum length ${validatorValue.requiredLength}`,
      min: `Min value is ${validatorValue.min}`,
      max: `Max value is ${validatorValue.max}`,
      invalidDateStart: `${validatorValue.field1} must be less than ${validatorValue.field2}`,
      invalidDateEnd: `${validatorValue.field2} must be greater than ${validatorValue.field1}`,
      invalidEmail:  `${sourceName === '' ? 'Contact Email' : sourceName} is not a valid email address`,
      invalidNumber: `${sourceName === '' ? 'Contact Phone' : sourceName} is not a valid phone number`,
      invalidMultipleEmailAddress: ` Please enter valid address in ${sourceName === '' ? 'Email Notification' : sourceName}`,   
    };

    if(customMessage) {
      return customMessage;
    } else if(customMessageObj) { // Specify custom message to return base on validator name
      if(customMessageObj[validatorName] !== undefined) {
        return customMessageObj[validatorName];
      } else {
        return config[validatorName]; // Return default if object dont have the custom message for the validatorName
      }
    }

  

    return config[validatorName];
  }

  static action(sourceType): string {
    if (sourceType === 'select') {
      return 'Select';
    } else if (sourceType === 'input') {
      return 'Enter';
    } else {
      return null;
    }
  }

  static emailValidator(control) {
    if (control.value !== undefined && control.value !== null && control.dirty) {
      if (
        control.value === '' ||
        control.value.match(
          /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
        )
      ) {
        return null;
      } else {
        return { invalidEmailAddress: true };
      }
    }
  }

  static multipleEmailValidator(control) {
    if (control.value !== undefined && control.value !== null && control.dirty) {
      const val = control.value;
      const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      if(val !== '') {
        if(val.includes(';')) {
          var emailArray = val.split(";");
          for (var i = 0; i <= (emailArray.length - 1); i++) {
            if (!emailArray[i].match(emailRegex)) {
              return { invalidMultipleEmailAddress: true };
            }
          }
          return null;
        } else {
          if(!val.match(emailRegex)) {
            return { invalidMultipleEmailAddress: true };
          }
        }
      }
      return null;
    }
  }


  //   static checkStartDateIsValid(detail: object): ValidatorFn {
  //     return (control: AbstractControl): { [key: string]: object } | null => {
  //       const parent = control.parent;
  //       if (parent !== undefined) {
  //         // Purpose of detail : Object so you can specify the control name and the String display message
  //         const dateStart = ConvertBentoDateModelToDate(parent.get(detail['keys'][0]).value);
  //         const dateEnd = ConvertBentoDateModelToDate(parent.get(detail['keys'][1]).value);

  //         if (dateStart && dateEnd) {
  //           if (dateStart > dateEnd) {
  //             return { invalidDateStart: { field1: detail['fieldName'][0], field2: detail['fieldName'][1] } };
  //           }
  //         }
  //       }
  //       return null;
  //     };
  //   }

  //   static checkEndDateIsValid(detail: object): ValidatorFn {
  //     return (control: AbstractControl): { [key: string]: object } | null => {
  //       const parent = control.parent;
  //       if (parent !== undefined) {
  //         // Purpose of detail : Object so you can specify the control name and the String display message
  //         const dateStart = ConvertBentoDateModelToDate(parent.get(detail['keys'][0]).value);
  //         const dateEnd = ConvertBentoDateModelToDate(parent.get(detail['keys'][1]).value);

  //         if (dateStart && dateEnd) {
  //           if (dateEnd < dateStart) {
  //             return { invalidDateEnd: { field1: detail['fieldName'][0], field2: detail['fieldName'][1] } };
  //           }
  //         }
  //       }
  //       return null;
  //     };
  //   }
}
