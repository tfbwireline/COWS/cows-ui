import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class DCPEService {

  constructor(private api: ApiHelperService) {
  }

  public GetFTNData(orderId: number, usrPrfId: number): Observable<any> {
    let params = {
      orderId: orderId,
      usrPrfId: usrPrfId
    }
    return this.api.get('api/DCPE/GetFTNData', { params: params });
  }

  public GetCPEOrderInfo_V5U(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetCPEOrderInfo_V5U', { params: params });
  }

  public GetTasksList(): Observable<any> {
    return this.api.get('api/DCPE/GetTasksList');
  }

  public GetJeopardyCodeList(): Observable<any> {
    return this.api.get('api/DCPE/GetJeopardyCodeList');
  }

  public GetCpeAcctCntct_V5U(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetCpeAcctCntct_V5U', { params: params });
  }

  public GetAssignedTechList(): Observable<any> {
    return this.api.get('api/DCPE/GetAssignedTechList');
  }

  public GetTechAssignment_V5U(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetTechAssignment_V5U', { params: params });
  }

  public GetCPELineItems_V5U(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetCPELineItems_V5U', { params: params });
  }

  public GetEquipOnlyInfo(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetEquipOnlyInfo', { params: params });
  }

  public GetRequisitionNumber_V5U(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetRequisitionNumber_V5U', { params: params });
  }

  public GetReqHeaderQueueByOrderID(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetReqHeaderQueueByOrderID', { params: params });
  }

  public GetCpeReqHeaderAcctCodes_V5U(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetCpeReqHeaderAcctCodes_V5U', { params: params });
  }

  public GetCPEPidByOrderID(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetCPEPidByOrderID', { params: params });
  }

  public GetCPEReqLineItems_V5U(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetCPEReqLineItems_V5U', { params: params });
  }

  public GetVendorsList(): Observable<any> {
    return this.api.get('api/DCPE/GetVendorsList');
  }

  public GetDomesticClliCode(): Observable<any> {
    return this.api.get('api/DCPE/GetDomesticClliCode');
  }

  public GetInternationalClliCode(): Observable<any> {
    return this.api.get('api/DCPE/GetInternationalClliCode');
  }

  public GetCpeCmplLineItems_V5U(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetCpeCmplLineItems_V5U', { params: params });
  }

  public InsertODIEReq_V5U(orderId: number): Observable<any>  {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/InsertODIEReq_V5U', { params: params });
  }

  public InsertDmstcStndMatReq_V5U(orderId: number): Observable<any>  {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/InsertDmstcStndMatReq_V5U', { params: params });
  }

  public InsertSSTATReq(orderId: number, msgId: number, ftn: string): Observable<any>  {
    let params = {
      orderId: orderId,
      msgId: msgId,
      ftn: ftn
    }
    return this.api.get('api/DCPE/InsertSSTATReq', { params: params });
  }

  public InsertEmailRequest_V5U(orderId: number, deviceID: string, emailReqTypeID: number, statusID: number, fTN: string, primaryEmail: string, secondaryEmail: string, emailNote: string, emailJeopardyNote: string): Observable<any> {
    let params = {
      orderId: orderId,
      deviceID: deviceID,
      emailReqTypeID: emailReqTypeID,
      statusID: statusID,
      fTN: fTN,
      primaryEmail: primaryEmail,
      secondaryEmail: secondaryEmail,
      emailNote: emailNote,
      emailJeopardyNote: emailJeopardyNote
    }
    return this.api.get('api/DCPE/InsertEmailRequest_V5U', { params: params });
  }

  public InsertRTS_V5U(orderId: number, deviceID: string, jeopardyCode: string, note: string): Observable<any> {
    let params = {
      orderId: orderId,
      deviceID: deviceID,
      jeopardyCode: jeopardyCode,
      note: note
    }
    return this.api.get('api/DCPE/InsertRTS_V5U', { params: params });
  }

  public UpdateCPEOrdr_V5U(orderId: number, modType: number, adid: string, jCode: string, comments: string, clli: string): Observable<any> {
    let params = {
      orderId: orderId,
      modType: modType,
      adid: adid,
      jCode: jCode,
      comments: comments,
      clli: clli
    }
    return this.api.get('api/DCPE/UpdateCPEOrdr_V5U', { params: params });
  }

  public UpdateDmstcWFM_V5U(orderId: number, adid: string, assignerADID: string, comments: string): Observable<any> {
    let params = {
      orderId: orderId,
      adid: adid,
      assignerADID: assignerADID,
      comments: comments
    }
    return this.api.get('api/DCPE/UpdateDmstcWFM_V5U', { params: params });
  }

  public ProcessCPEMve_V5U(orderId: number, adid: string, taskName: string, comments: string): Observable<any> {
    let params = {
      orderId: orderId,
      adid: adid,
      taskName: taskName,
      comments: comments
    }
    return this.api.get('api/DCPE/ProcessCPEMve_V5U', { params: params });
  }

  public InsertReqLineItem_V5U(model): any {
    return this.api.post('api/DCPE/InsertReqLineItem_V5U/', model);
  }

  public InsertRequisitionHeader_V5U(model: any): Observable<any> {
    return this.api.post('api/DCPE/InsertRequisitionHeader_V5U/', model);
  }

  public InsertEqpRecCmpl_V5U(adid: string, orderId: number): Observable<any> {
    let params = {
      adid: adid,
      orderId: orderId
    }
    return this.api.get('api/DCPE/InsertEqpRecCmpl_V5U', { params: params });
  }

  public InsertEqpReceipt_V5U(fsaCpeLineItemId: number, partialQty: number, action: string, adid: string, orderId: number): Observable<any> {
    let params = {
      fsaCpeLineItemId: fsaCpeLineItemId,
      partialQty: partialQty,
      action: action,
      adid: adid,
      orderId: orderId
    }
    return this.api.get('api/DCPE/InsertEqpReceipt_V5U', { params: params });
  }


  public InsertTechAssignment_V5U(model): Observable<any> {
    return this.api.post('api/DCPE/InsertTechAssignment_V5U/', model);
  }

  public InsertOrdrCmplPartial_V5U(model): Observable<any> {
    return this.api.post('api/DCPE/InsertOrdrCmplPartial_V5U/', model);
  }

  public InsertOrdrCmplAll_V5U(model): Observable<any> {
    return this.api.post('api/DCPE/InsertOrdrCmplAll_V5U/', model);
  }

  public completeODIETask(model): Observable<any> {
    return this.api.post('api/DCPE/CompleteODIETask/', model);
  }

  public completeMatReq(model): Observable<any> {
    return this.api.post('api/DCPE/CompleteMatReq/', model);
  }

  public completeEquipReceipts(model): Observable<any> {
    return this.api.post('api/DCPE/CompleteEquipReceipts/', model);
  }

  public orderComplete(model): Observable<any> {
    return this.api.post('api/DCPE/OrderComplete/', model);
  }

  public RTSClick(model): Observable<any> {
    return this.api.post('api/DCPE/RTSClick/', model);
  }

  public CompleteGOMTask(model): Observable<any> {
    return this.api.post('api/DCPE/CompleteGOMTask/', model);
  }

  public SaveGOMSpecificData(model): Observable<any> {
    return this.api.post('api/DCPE/SaveGOMSpecificData/', model);
  }
 public SaveAMNCISpecificData(model): Observable<any> {
    return this.api.post('api/DCPE/SaveAMNCISpecificData/', model);
  }

  public GOMRTSClick(model): Observable<any> {
    return this.api.post('api/DCPE/GOMRTSClick/', model);
  }

  public IsNIDDevice(orderId: number, deviceId: string): Observable<any> {
    let params = {
      orderId: orderId,
      deviceId: deviceId
    }
    return this.api.get('api/DCPE/IsNIDDevice', { params: params });
  }

  public UpdateNIDSerialNbr(orderId: number, ftn: string, deviceId: string, nidSerialNbr: string): Observable<any> {
    let params = {
      orderId: orderId,
      ftn: ftn,
      deviceId: deviceId,
      nidSerialNbr: nidSerialNbr
    }
    return this.api.get('api/DCPE/UpdateNIDSerialNbr', { params: params });
  }

  public UpdateNuaCkt(orderId: number, nuaCkt: string): Observable<any> {
    let params = {
      orderId: orderId,
      nuaCkt: nuaCkt
    }
    return this.api.get('api/DCPE/UpdateNuaCkt', { params: params });
  }

  public GetShippingInstr(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/DCPE/GetShippingInstr', { params: params });
  }

  public GetSerialNumberAndIP(ordrId: number, deviceId: string): Observable<any> {
    let params = {
      ordrId: ordrId,
      deviceId: deviceId
    }
    return this.api.get('api/NidActy/GetSerialNumberAndIP', { params: params });
  }

  public isNidAlreadyUsed(orderId: number, nidSerialNbr: string): Observable<any> {
    let params = {
      orderId: orderId,
      nidSerialNbr: nidSerialNbr
    }
    return this.api.get('api/DCPE/IsNIDAlreadyUsed', { params: params });
  }

}
