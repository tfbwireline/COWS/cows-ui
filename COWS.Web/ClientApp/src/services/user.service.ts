import { Injectable } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { Observable, zip, BehaviorSubject } from 'rxjs';
import { ApiHelperService, Helper } from "../shared/index";
import { User, IUserResponse, UserProfile, LoggedInUser, SearchCode } from "./../models";
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';
import { MenuService } from './../services/menu.service';
import { NavLinkService } from './nav-link.service';
import { SearchTypes } from '../app/search/search.criteria';
import { ELkSysCfgMenus, ELkSysCfgAccessKeys, KeyValuePair } from '../shared/global';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  loggedInUser = new LoggedInUser()
  searchCode = new SearchCode();
  isMember: boolean = false;
  isReviewer: boolean = false;
  isActivator: boolean = false;
  isCheckCurrentSlotShown: boolean = false;
  isSlotPickerShown: boolean = false;

  srchCd = new BehaviorSubject<SearchCode>(this.searchCode);

  constructor(private api: ApiHelperService, private http: HttpClient, private helper: Helper, private spinner: NgxSpinnerService,
    private menuService: MenuService, private navLinkService: NavLinkService) {
    //this.httpHeaders = new HttpHeaders({
    //  'Content-Type': 'application/json',
    //  'withCredentials': 'true'
    //});
  }

  getSearchCode(): Observable<SearchCode> {
    return this.srchCd.asObservable();
  }

  public search(filter: { name: string } = { name: '' }, page = 1): Observable<any> {
    return this.http.get<IUserResponse>('/api/Users')
      .pipe(
      tap((response: IUserResponse) => {
        response = response
          .map(user => new User(user.userId, user.userAdid, user.fullNme, user.menuPref))
            // Not filtering in the server since in-memory-web-api has somewhat restricted api
            .filter(user => user.fullNme.includes(filter))

          return response;
        })
      );
  }

  // debounceTime should be handled on specific page that requires it
  // so other page can use this function as it normally would
  // besides, even with debounceTime, this sends a request for each key stroke
  // it should have been, valueChanges().pipe(debounceTime(500)), only then call the request
  public searchUsers(term) {
    var listOfUsers = this.http.get('/api/Users/GetUsersByNameOrADID/' + term)
      .pipe(
        debounceTime(500),  // WAIT FOR 500 MILISECONDS ATER EACH KEY STROKE.
        map(
          (data: any) => {
            return (
              data.length != 0 ? data as any[] : [{ "UserName": "No Record Found" } as any]
            );
          }
        ));

    return listOfUsers;
  }

  public getByNameOrADID(term, isLIKE?) {
    let params = {
      isLIKE: isLIKE || false
    }

    return this.api.get('api/Users/GetUsersByNameOrADID/' + term, { params: params });
  }

  public setAsBlindRequest() {
    this.api.blindRequest = true;
  }

  public getUsers(): Observable<any> {
    return this.api.get('api/Users');
  }

  public getAllUsers(): Observable<any> {
    return this.api.get('api/Users/GetAllUsers');
  }

  public getLoggedInUser() {
    return this.api.get('api/Users/GetLoggedInUserDetails')
    //return this.api.get('api/Menu/IsAdmin', { headers: this.httpHeaders })
    //return this.api.get('api/Menu/GetLoggedInUserDetails', { headers: this.httpHeaders })
  }

  public getLoggedInUser2(): Promise<any> {
    return new Promise(resolve => {
      this.spinner.show();
      this.api.get('api/Users/GetLoggedInUserDetails').toPromise().then(
        res => {
          if (res != null) {
            let adid = res.userAdid;
            this.loggedInUser.userId = res.userId
            this.loggedInUser.adid = res.userAdid
            this.loggedInUser.isAuthorize = true

            localStorage.setItem('userID', res.userId);
            localStorage.setItem('userADID', res.userAdid);
            localStorage.setItem('validUser', "true");

            let data = zip(
              this.getUserByADID(adid),
              this.getUserProfilesByADID(adid)
            );

            data.toPromise().then(
              res2 => {
                let user = res2[0] as User
                let userProfile = res2[1] as UserProfile[]

                if (user != null && userProfile != null) {
                  this.navLinkService.getMenu()
                  this.loggedInUser.user = user
                  this.loggedInUser.profileIds = userProfile.map(a => a.usrPrfId);

                  // Trim dsplNme to remove [] like Sandoval, Sarah C [IBM Contractor for Sprint]
                  var dsplNme = this.loggedInUser.user.dsplNme;
                  this.loggedInUser.user.dsplNme = dsplNme.indexOf("[") == -1 ? dsplNme : dsplNme.slice(0, dsplNme.indexOf("["))

                  const userProfiles = userProfile.map(a => a.usrPrfDes);
                  this.loggedInUser.profiles = userProfiles;
                  this.loggedInUser.profilesDisp = this.helper.ArrayValueStringReplace(userProfiles);
                  this.isMember = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes("MEMBER")).length > 0
                  this.isReviewer = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes("REVIEWER")).length > 0
                  this.isActivator = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes("ACTIVATOR")).length > 0

                  let candProfiles = this.loggedInUser.profiles.filter(i => i.indexOf('CAND') > -1);
                  this.searchCode.isCandUser = candProfiles.length == this.loggedInUser.profiles.length;
                  this.searchCode.isOrderUser = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes("GOM")
                    || a.toUpperCase().includes("NCI") || a.toUpperCase().includes("DCPE")
                    || a.includes("Read Only Access to Orders")).length > 0

                  // Removed by Sarah Sandoval [20200416] - Make it DB Driven as seen below
                  //this.allowSearch = !(this.loggedInUser.profiles.filter(a =>
                  //  a.toUpperCase().includes("BILLABLE DISPATCH ADMIN")
                  //  || a.toUpperCase().includes("BILLABLE DISPATCH READONLY")
                  //  || a.toUpperCase().includes("CAND")).length > 0)

                  // Added by Sarah Sandoval [20200413] - Get the profile search codes
                  // Search Code values: NULL/0 (No Search options at all), 1 (Event), 2 (Order), 3 (Redesign),
                  // 4 (Event + Order), 5 (Event + Redesign), 6 (Order + Redesign), 7 (All)
                  this.searchCode.codes = []; // Clear codes on every call of this function
                  userProfile.forEach(i => {
                    if (i.srchCd != null && !this.searchCode.codes.includes(i.srchCd)) {
                      this.searchCode.codes.push(i.srchCd);
                    }
                  });
                  // Get distinct values
                  //this.searchCodes = Array.from(new Set(this.codes.map(i => i)));
                  this.searchCode.allowEventSearch = this.searchCode.codes.length > 0
                    ? this.searchCode.codes.filter(i => i == 1 || i == 4 || i == 5 || i == 7).length > 0 : false;
                  this.searchCode.allowOrderSearch = this.searchCode.codes.length > 0
                    ? this.searchCode.codes.filter(i => i == 2 || i == 4 || i == 6 || i == 7).length > 0 : false;
                  this.searchCode.allowRedesignSearch = this.searchCode.codes.length > 0
                    ? this.searchCode.codes.filter(i => i == 3 || i == 5 || i == 6 || i == 7).length > 0 : false;
                  this.searchCode.allowSearch = this.searchCode.allowEventSearch
                    || this.searchCode.allowOrderSearch || this.searchCode.allowRedesignSearch;

                  this.searchCode.types = this.getSearchOption();

                  this.srchCd.next(this.searchCode)

                  this.loggedInUser.fullName = user.fullNme
                  localStorage.setItem('userFullName', user.fullNme);
                  this.menuService.navMenu();
                  //this.loggedInUser.isSideNav = user.menuPref == 'V' ? true : false
                  //if (user.menuPref == 'V') {
                  //  this.menuService.sideNav()
                  //} else {
                  //  this.menuService.navMenu()
                  //}
                } else {
                  this.loggedInUser.isAuthorize = false
                  localStorage.setItem('validUser', "false");
                }
              },
              error => {
                this.loggedInUser.isAuthorize = false
                localStorage.setItem('validUser', "false");
                //this.loggedInUser.isAuthorize = true
              });
          }
          else {
            this.loggedInUser.isAuthorize = false
            localStorage.setItem('validUser', "false");
          }
          resolve(this.searchCode);
        },
        error => {
          resolve(null);
        }).then(() => { this.spinner.hide(); });
    });
  }

  public getByUserID(id: number): Observable<any> {
    return this.api.get('api/Users/getByUserID/' + id);
  }

  public getUserWithCsgLvlByUserID(id: number): Observable<any> {
    return this.api.get('api/Users/getUserWithCsgLvlByUserID/' + id);
  }

  public getUserByADID(adid: string): Observable<any> {
    return this.api.get('api/Users/' + adid);
  }

  public getUserProfilesByADID(adid: string): Observable<any> {
    return this.api.get('api/Users/' + adid + '/Profiles');
  }

  public getFinalUserProfile(adid: string, eventTypeId: number, isEvent: boolean = true): Observable<any> {
    return this.api.get('api/Users/' + adid + '/Profile', { params: { eventTypeId: eventTypeId, isEvent: isEvent }});
  }

  public getActiveUserProfiles(adid: string, eventTypeId: number): Observable<any> {
    return this.api.get('api/Users/' + adid + '/GetActiveUserProfiles', { params: { eventTypeId: eventTypeId } });
  }

  public getConfBridgeByUserId(id: number): Observable<any> {
    return this.api.get('api/Users/Bridge/' + id);
  }

  public updateUser(id: number, user: User): Observable<any> {
    return this.api.put('api/Users/' + id, user);
  }

  public InsertUser(adid: string): Observable<any> {
    return this.api.get('api/Users/InsertUser/' + adid);
  }

  public isCSGUser(adid: string, csgLevel: number): Observable<any> {
    let params = {
      adid: adid,
      csgLevel: csgLevel
    }
    return this.api.get('api/Users/IsCSGLevelUser', { params: params });
  }

  public getUsersByProfileName(profileName: string): Observable<any> {
    return this.api.get('api/Users/GetUsersByProfileName/' + profileName);
  }

  public getEventActivator(type, params): Observable<any> {
    return this.api.post('api/Users/EventActivator/' + type, params);
  }

  public checkCurrentSlots(params): Observable<any> {
    return this.api.post('api/SlotPicker/CheckCurrentSlots', params);
  }

  public checkSlotIfAvailable(params): Observable<any> {
    return this.api.post('api/SlotPicker/checkSlotIfAvailable', params);
  }

  public checkForDoubleBooking(start: Date, end: Date, uID: string, eventId: number): Observable<any> {
    let params = {
      start: start,
      end: end,
      uID: uID,
      eventId: eventId,
    }

    return this.api.post('api/SlotPicker/ChkForDblBooking', params);
  }

  public getLaunchPicker(params): Observable<any> {
    return this.api.post('api/SlotPicker/GetLaunchPicker', params);
  }

  public getOpenSlots(params): Observable<any> {
    return this.api.post('api/SlotPicker/GetOpenSlots', params);
  }

  public getNtwkIntlDuration(params): Observable<any> {
    return this.api.post('api/SlotPicker/GetNtwkIntlDuration', params);
  }

  public userActiveWithProfileName(userId: number, profileName: string): Observable<any> {
    let params = {
      userId: userId,
      profileName: profileName
    }
    return this.api.get('api/Users/UserActiveWithProfileName', { params: params });
  }

  public GetConferenceBridgeInfo(userId: number): Observable<any> {
    return this.api.get('api/Users/GetConferenceBridgeInfo/' + userId);
  }

  public isActivatorByKeyword(keyword: string): boolean {
    let profiles = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes(keyword.toUpperCase())) as string[]

    return profiles.filter(a => a.toUpperCase().includes("ACTIVATOR")).length > 0
  }

  public isReviewerByKeyword(keyword: string): boolean {
    let profiles = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes(keyword.toUpperCase())) as string[]

    return profiles.filter(a => a.toUpperCase().includes("REVIEWER")).length > 0
  }

  public isUpdaterByKeyword(keyword: string): boolean {
    let profiles = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes(keyword.toUpperCase())) as string[]

    return profiles.filter(a => a.toUpperCase().includes("UPDATER")).length > 0
  }

  public isRTSByKeyword(keyword: string): boolean {
    let profiles = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes(keyword.toUpperCase())) as string[]

    return profiles.filter(a => a.toUpperCase().includes("RTS")).length > 0
  }

  public isMemberByKeyword(keyword: string): boolean {
    let profiles = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes(keyword.toUpperCase())) as string[]

    return profiles.filter(a => a.toUpperCase().includes("MEMBER")).length > 0
  }

  public isReadonlyByKeyword(keyword: string): boolean {
    let profiles = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes(keyword.toUpperCase())) as string[]

    return profiles.filter(a => a.toUpperCase().includes("READ ONLY")).length > 0
  }

  public hasMultipleRoleInGroup(group: string): boolean {
    let profiles = this.loggedInUser.profiles.filter(a => a.toUpperCase().includes(group.toUpperCase())) as string[]

    return profiles.length > 1;
  }

  public getSDEViewUsers(): Observable<any> {
    return this.api.get('api/Users/GetSDEViewUsers');
  }

  public GetAdminExcPerm(name: string): Observable<any> {
    return this.api.get(`api/Users/GetAdminExcPerm?name=${name}`);
  }

  public getSearchOption() {
    let types: SearchTypes[] = []
    if (this.searchCode.allowEventSearch && types.findIndex(i => i.value == 1) == -1) {
      types.push(new SearchTypes("Event", 1,
        [
          new KeyValuePair("Event ID", 1),
          new KeyValuePair("M5 #", 2),
          //new KeyValuePair("AD Type", 3),
          new KeyValuePair("Customer Name", 4),
          new KeyValuePair("Assigned Activator", 5),
          //new KeyValuePair("SOWS Event ID", 6),
          new KeyValuePair("Requestor Name", 7),
          new KeyValuePair("Event Type", 8),
          new KeyValuePair("NUA", 9),
          new KeyValuePair("FRB ID", 10),
        ]));
    }

    if (this.searchCode.allowOrderSearch && types.findIndex(i => i.value == 2) == -1) {
      types.push(new SearchTypes("Order", 2,
        [
          new KeyValuePair("M5 #", 1),
          new KeyValuePair("H1 Customer Name", 2),
          new KeyValuePair("H5 Customer Name", 3),
          new KeyValuePair("Order Type", 4),
          new KeyValuePair("Order Sub Type", 5),
          new KeyValuePair("CCD", 6)
        ]));
    }

    if (this.searchCode.allowRedesignSearch && types.findIndex(i => i.value == 3) == -1) {
      types.push(new SearchTypes("Redesign", 3,
        [
          new KeyValuePair("Redesign Number", 1),
          new KeyValuePair("Customer Name", 2),
          new KeyValuePair("NTE Name", 3),
          new KeyValuePair("ODIE Device", 4),
          new KeyValuePair("PM Name", 5)
        ]));
    }

    return types;
  }

  public isMdsMember() {
    let profiles = this.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI EVENT MEMBER") ||
        a.toUpperCase().includes("SALES SUPPORT EVENT MEMBER") ||
        a.toUpperCase().includes("MDS EVENT MEMBER")
    ) as string[]

    return profiles.length > 0
  }

  public isMdsReviewer() {
    let profiles = this.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI EVENT REVIEWER") ||
        a.toUpperCase().includes("SALES SUPPORT EVENT REVIEWER") ||
        a.toUpperCase().includes("MDS EVENT REVIEWER")
    ) as string[]

    return profiles.length > 0
  }

  public isMdsActivator() {
    let profiles = this.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI EVENT ACTIVATOR") ||
        a.toUpperCase().includes("SALES SUPPORT EVENT ACTIVATOR") ||
        a.toUpperCase().includes("MDS EVENT ACTIVATOR")
    ) as string[]

    return profiles.length > 0
  }

  public isSIPTnUCaaSMember() {
    let profiles = this.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI EVENT MEMBER") ||
        a.toUpperCase().includes("SALES SUPPORT EVENT MEMBER") ||
        a.toUpperCase().includes("SIPTNUCAAS EVENT MEMBER")
    ) as string[]

    return profiles.length > 0
  }

  public isSIPTnUCaaSReviewer() {
    let profiles = this.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI EVENT REVIEWER") ||
        a.toUpperCase().includes("SALES SUPPORT EVENT REVIEWER") ||
        a.toUpperCase().includes("SIPTNUCAAS EVENT REVIEWER")
    ) as string[]

    return profiles.length > 0
  }

  public isSIPTnUCaaSActivator() {
    let profiles = this.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI EVENT ACTIVATOR") ||
        a.toUpperCase().includes("SALES SUPPORT EVENT ACTIVATOR") ||
        a.toUpperCase().includes("SIPTNUCAAS EVENT ACTIVATOR")
    ) as string[]

    return profiles.length > 0
  }

  public checkPermissionForBAROrders() {
    return this.api.get(`api/Users/GetAdminExcPerm?name=${ELkSysCfgAccessKeys.xNCIAdminEdit}`);
  }

  public hasProfiles
}
