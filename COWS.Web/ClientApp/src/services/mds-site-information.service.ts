import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MdsSiteInformation {

  constructor(private api: ApiHelperService) { }
  
  public getEventCpeDevByEventId(eventId: string): Observable<any> {
    return this.api.get('api/EventCpeDev/' + eventId);
  }

  public getEventDevSrvcMgmtByEventId(eventId: string): Observable<any> {
    return this.api.get('api/EventDevSrvcMgmt/' + eventId);
  }

  public getMDSEventSiteSrvcByEventId(eventId: string): Observable<any> {
    return this.api.get('api/MDSEventSiteSrvc/' + eventId);
  }

  public getMDSEventDslSbicCustTrptByEventId(eventId: string): Observable<any> {
    return this.api.get('api/MDSEventDslSbicCustTrpt/' + eventId);
  }

  public getMDSEventSlnkWiredTrptByEventId(eventId: string): Observable<any> {
    return this.api.get('api/MDSEventSlnkWiredTrpt/' + eventId);
  }

  public getMDSEventWrlsTrptByEventId(eventId: string): Observable<any> {
    return this.api.get('api/MDSEventWrlsTrpt/' + eventId);
  }

  public getMDSEventPortBndwdByEventId(eventId: string): Observable<any> {
    return this.api.get('api/MDSEventPortBndwd/' + eventId);
  }
}
