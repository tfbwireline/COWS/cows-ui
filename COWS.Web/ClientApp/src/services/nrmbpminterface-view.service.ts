import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})
export class NRMBPMInterfaceViewService {

  constructor(private api: ApiHelperService) {
  }

  public getNRMBPMInterfaceView(): Observable<any> {
    return this.api.get('api/NRMBPMInterface');
  }
}
