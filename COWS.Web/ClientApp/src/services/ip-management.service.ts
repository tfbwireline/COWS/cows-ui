import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})

export class IPManagementService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/IPManagement');
  }

  public GetIPRecords(id: number): Observable<any> {
    return this.api.get('api/IPManagement/GetIPRecords/' + id);
  }

  public GetIpMstrRelatedAdress(id: number): Observable<any> {
    return this.api.get('api/IPManagement/GetIpMstrRelatedAdress/' + id);
  }

  public GetRelatedActyForSelectedIPAddress(id: number, type: number): Observable<any> {
    const queryString = `id=${id}&type=${type}`;
    return this.api.get('api/IPManagement/GetRelatedActyForSelectedIPAddress?' + queryString);
  }
  

}
