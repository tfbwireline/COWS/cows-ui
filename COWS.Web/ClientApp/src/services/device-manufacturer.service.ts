import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class DeviceManufacturerService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/DeviceManufacturers');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/DeviceManufacturers/' + id);
  }

  public getByName(name: string): Observable<any> {
    return this.api.get('api/DeviceManufacturers/GetByName/' + name);
  }

  public create(manf): Observable<any> {
    return this.api.post('api/DeviceManufacturers/', manf);
  }

  public update(id: number, manf): Observable<any> {
    return this.api.put('api/DeviceManufacturers/' + id, manf);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/DeviceManufacturers/' + id);
  }
}
