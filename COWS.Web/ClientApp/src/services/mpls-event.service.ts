import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MplsEventService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/Events/Mpls');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/Events/Mpls/' + id);
  }

  public create(mpls): Observable<any> {
    return this.api.post('api/Events/Mpls/', mpls);
  }

  public update(id: number, mpls): Observable<any> {
    return this.api.put('api/Events/Mpls/' + id, mpls);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/Events/Mpls/' + id);
  }
}
