import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MDSNetworkInformationService {

  constructor(private api: ApiHelperService) { }

  public getMDSEventNtwkCustByEventId(eventId: string): Observable<any> {
    return this.api.get('api/MDSEventNtwkCust/' + eventId);
  }

  public getMDSEventNtwkTrptByEventId(eventId: string): Observable<any> {
    return this.api.get('api/MDSEventNtwkTrpt/' + eventId);
  }
}
