import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class WorkGroupService {

  constructor(private api: ApiHelperService) {
  }

  public GetOrderDisplayViewData(userId: number, usrPrfId: number): Observable<any> {
    let params = {
      userId: userId,
      usrPrfId: usrPrfId
    }
    return this.api.get('api/WorkGroup/GetOrderDisplayViewData', { params: params });
  }

  public GetWGData(usrPrfId: number, userId: number, orderId: number, isCmplt: boolean, filter: string): Observable<any> {
    let params = {      
      usrPrfId: usrPrfId,
      userId: userId,
      orderId: orderId,
      isCmplt: isCmplt,
      filter: filter
    }
    return this.api.get('api/WorkGroup/GetWGData', { params: params });
  }

  public VerifyUserIsAdminToEditCompletedOrder(userADID: string, parameter: string): Observable<any> {
    let params = {
      userADID: userADID,
      parameter: parameter
    }
    return this.api.get('api/WorkGroup/VerifyUserIsAdminToEditCompletedOrder', { params: params });
  }

  public GetFTNListDetails(orderId: number, usrPrfId: number): Observable<any> {
    let params = {
      orderId: orderId,
      usrPrfId: usrPrfId
      
    }
    return this.api.get('api/WorkGroup/GetFTNListDetails', { params: params });
  }

  public CompleteActiveTask(orderID: number, taskID: number, taskStatus: number, comments: string, userID: number): any {
    let params = {
      orderID: orderID,
      taskID: taskID,
      taskStatus: taskStatus,
      comments: comments,
      userID: userID
    }
    return this.api.get('api/WorkGroup/CompleteActiveTask', { params: params });
  }

  public GetLatestNonSystemOrderNoteInfo(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/GetLatestNonSystemOrderNoteInfo', { params: params });
  }

  public GetPreSubmitRTSStatus(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/GetPreSubmitRTSStatus', { params: params });
  }

  public OrderExistsInSalesSupportWG(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/OrderExistsInSalesSupportWG', { params: params });
  }

  public HasBillMissingTask(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/HasBillMissingTask', { params: params });
  }

  public GetH5FolderInfo(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/GetH5FolderInfo', { params: params });
  }

  public CompleteGOMIBillTask(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/CompleteGOMIBillTask', { params: params });
  }

  public GetCPEUpdtInfo(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/GetCPEUpdtInfo', { params: params });
  }

  public GetGOMSpecificData(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/GetGOMSpecificData', { params: params });
  }

  public IsACRRTS(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/IsACRRTS', { params: params });
  }

  public CompleteACR(orderId: number, notes: string): any {
    let params = {
      orderId: orderId,
      notes: notes
    }
    return this.api.get('api/WorkGroup/CompleteACR', { params: params });
  }

  public LoadNCITask(orderId: number, taskId: number): any {
    let params = {
      orderId: orderId,
      taskId: taskId
    }
    return this.api.get('api/WorkGroup/LoadNCITask', { params: params });
  }

  public GetCSCSpecificData(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/GetCSCSpecificData', { params: params });
  }

  public GetMDSEventData(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/WorkGroup/GetMDSEventData', { params: params });
  }

  public GetAllSrvcSiteSupport(): Observable<any> {
    return this.api.get('api/WorkGroup/GetAllSrvcSiteSupport');
  }

  public GetIsOrderExistInSalesSupportWG(id: number): Observable<any> {
    return this.api.get(`api/WorkGroup/IsOrderExistInSalesSupportWG/${id}`);
  }
}
