import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})
export class getUnlockItemsViewService {

  buttonOptions: any = {
    text: "Search",
    type: "success",
    useSubmitBehavior: true
  }
  constructor(private api: ApiHelperService) {
  }

  //public getUnlockItemsOrdrView(): Observable<any> {
  //  return this.api.get('api/UnlockItems');
  //}
  public getlockItems( refId,flag): Observable<any> {
    return this.api.get('api/UnlockItems', { params: { refId: refId,flag: flag} });
  }
  public unlockItems(service): Observable<any> {
    return this.api.post('api/UnlockItems/', service);
  }
  //public unlockItems(ordrId, userId, itemType): Observable<any> {
  //  return this.api.get('api/UnlockItems', { params: { ordrId: ordrId, userId: userId, itemType: itemType } });
  //}
}
