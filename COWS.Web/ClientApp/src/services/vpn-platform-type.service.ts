import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';
import { KeyValuePair } from './../shared/global';

@Injectable({
  providedIn: 'root'
})

export class VpnPlatformTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/VpnPlatformTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/VpnPlatformTypes/' + id);
  }

  public getByEventId(id: number): Observable<any> {
    return this.api.get('api/VpnPlatformTypes/getByEventId/' + id).pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.vpnPltfrmTypeDes, item.vpnPltfrmTypeId));
      }));
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/VpnPlatformTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.vpnPltfrmTypeId, item.vpnPltfrmTypeDes));
      }));
  }
}
