import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import {
  AdvancedSearchEvent, AdvancedSearchOrder, AdvancedSearchRedesign, EventDiscoDev, UserProfile
} from "./../models";
import { EEventType, WorkGroup, EordrCat, ETasks, OldWorkGroupStr, OldWorkGroup, EProdType } from '../shared/global';
import { Helper } from '../shared/helper'
import { map } from 'rxjs/operators';
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class SearchService {

  constructor(private api: ApiHelperService, private helper: Helper) { }

  public getEvent(event: AdvancedSearchEvent): Observable<any> {
    return this.api.post('api/Search/Event', event);
  }

  public getOrder(order: AdvancedSearchOrder): Observable<any> {
    return this.api.post('api/Search/Order', order);
  }

  public getRedesign(redesign: AdvancedSearchRedesign): Observable<any> {
    return this.api.post('api/Search/Redesign', redesign);
  }

  public getM5Lookup(m5: string, eventType: number): Observable<any> {
    return this.api.get('api/Search/Lookup/EventType/'+ eventType + '/M5/' + m5);
  }

  public getH6Lookup(h6: string, eventType: number, vasCd: number, isCEChng: boolean): Observable<any> {
    let params = {
      eventType: eventType,
      vasCd: vasCd,
      isCEChng: isCEChng
    }
    return this.api.get('api/Search/Lookup/EventType/' + h6, { params: params });
  }

  public GetLookupH6Sipt(h6: string): Observable<any> {
    return this.api.get('api/Search/Lookup/H6Sipt/' + h6);
  }

  public getSiteLookup(h6: string, ccd: string, ucaasCd: string, isCarrierEthernet: boolean): Observable<any> {
    let params = {
      ccd: ccd,
      ucaasCd: ucaasCd,
      isCarrierEthernet: isCarrierEthernet
    }
    return this.api.get('api/Search/Lookup/Site/' + h6, { params: params });
  }

  public getH1Lookup(h1: string): Observable<any> {
    return this.api.get('api/Search/Lookup/H1/' + h1);
  }

  public viewOrderDetails(h6: string, ccd: string, ucaasCd: string, eventId: number): Observable<any> {
    let params = {
      ccd: ccd,
      ucaasCd: ucaasCd,
      eventId: eventId
    }
    return this.api.get('api/Search/Lookup/CPEOrder/' + h6, { params: params });
  }

  public retrieveRedesign(h1: string, custName: string, eventTypeId: number): Observable<any> {
    let params = {
      custName: custName,
      eventTypeId: eventTypeId
    }
    return this.api.get('api/Search/Redesign/OdieDeviceInfo/' + h1, { params: params });
  }

  public getOdieDiscoDevData(h1: string, discoDevList: EventDiscoDev[]): Observable<any> {
    let body = {
      h1: h1,
      eventDiscoDev: discoDevList
    }
    console.log(discoDevList)
    return this.api.post('api/Search/Redesign/OdieDiscoInfo', body);
  }

  /** Get Customer H1 for Customer Name - H1 dropdown */
  public getODIECustomerH1Data(): Observable<any> {
    return this.api.get('api/Search/Lookup/OdieCustomerH1Data');
  }

  public getSysCfgValue(name: string): Observable<any> {
    return this.api.get('api/Search/Lookup/SysCfg/' + name);
  }

  //public getRolesByRoleCodes(roleCds: string): Observable<any> {
  //  return this.api.get('api/Search/Lookup/Role/' + roleCds);
  //}

  public getRolesByRoleCodes(roleCds: string): Observable<any> {
    return this.api.get('api/Search/Lookup/Role/' + roleCds).pipe(
      map((data) => {
        return data.map((item) => new KeyValuePair(item.displayText, item.roleId));
      }));
  }

  public getXNCIWGID(regionId : number) {
    switch(regionId) {
      case 1:
        return WorkGroup.xNCIAmerica
      case 2:
        return WorkGroup.xNCIEurope
      case 3:
        return WorkGroup.xNCIAsia
    }
  }

  public determineWGIDForSystemOrCompletedOrders(order : number, profiles: Array<any>) {

    let orderProperty = order['pprt'];
    let wgId = 0;
    
    let workGroupEnum = OldWorkGroupStr;
    let workGroups = []
    let userGroups = [];
    for (let key in OldWorkGroupStr) {
      if (isNaN(Number(key))) {
          workGroups.push(workGroupEnum[key]);
      }
    }

    for(let i = 0; i < workGroups.length; i++) {
      const isInGroup = profiles.filter(x => x.includes(workGroups[i])).length > 0;
      if(isInGroup) {
        userGroups.push(workGroups[i]);
      }
    }
    if(userGroups.length == 0) {
      return wgId;
    } else if (userGroups.length == 1 && userGroups[0] !== OldWorkGroupStr.AMNCI && userGroups[0] !== OldWorkGroupStr.ANCI && userGroups[0] !== OldWorkGroupStr.ENCI) {
      return  this.helper.getReWriteWorkGroup(userGroups[0]);
    } else if (userGroups.length == 1 && (userGroups[0] === OldWorkGroupStr.AMNCI || userGroups[0] === OldWorkGroupStr.ANCI || userGroups[0] === OldWorkGroupStr.ENCI)) {
      return this.getXNCIWGID(order['rgnId']);
    } else if(userGroups.length > 1) 
    {
      if (orderProperty.prodTypeId === 1 || orderProperty.prodTypeId === 2 || orderProperty.prodTypeId === 3 || orderProperty.prodTypeId === 4 || orderProperty.prodTypeId === 5
      || orderProperty.prodTypeId === 6 || orderProperty.prodTypeId === 7 || orderProperty.prodTypeId === 8 || orderProperty.prodTypeId === 10 || orderProperty.prodTypeId === 11
        || orderProperty.prodTypeId === 15 || orderProperty.prodTypeId === 17 || orderProperty.prodTypeId === 18 || orderProperty.prodTypeId === 19 || orderProperty.prodTypeId === 20
        || orderProperty.prodTypeId === 21 || orderProperty.prodTypeId === 22 || orderProperty.prodTypeId === 23 || orderProperty.prodTypeId === 24 || orderProperty.prodTypeId === 25
        || orderProperty.prodTypeId === 30) {
        if (userGroups.includes(OldWorkGroupStr.AMNCI) || userGroups.includes(OldWorkGroupStr.ANCI) || userGroups.includes(OldWorkGroupStr.ENCI)) {
          return this.getXNCIWGID(order['rgnId']);
        }
        else if (userGroups.includes(OldWorkGroupStr.GOM) && !orderProperty.mdsCd) {
          return WorkGroup.GOM;
        }
        else if (orderProperty.mdsCd && userGroups.includes(OldWorkGroupStr.MDS)) {
          return WorkGroup.MDS;
        }
      }
      else if(orderProperty.prodTypeId === 14 || orderProperty.prodTypeId === 16) {
        if(userGroups.includes(OldWorkGroupStr.MDS)) {
          return WorkGroup.MDS;
        }
      } else if(orderProperty.prodTypeId === 9) {
        if(orderProperty.mdsCd || !(orderProperty.intlCd)) {
          if(userGroups.includes(OldWorkGroupStr.MDS)) {
            return WorkGroup.MDS;
          }
        } else if(orderProperty.intlCd) {
          if(userGroups.includes(OldWorkGroupStr.GOM)) {
            return WorkGroup.GOM;
          } else if(userGroups.includes(OldWorkGroupStr.CSC)) {
            return WorkGroup.CSC
          } else if(userGroups.includes(OldWorkGroupStr.AMNCI)) {
            return this.getXNCIWGID(order['rgnId']);
          }
        } else {
          if(userGroups.includes(OldWorkGroupStr.AMNCI)) {
            return this.getXNCIWGID(order['rgnId']);
          } else if(userGroups.includes(OldWorkGroupStr.GOM) && (!orderProperty.mdsCd)) {
            return WorkGroup.GOM;
          } else if(userGroups.includes(OldWorkGroupStr.MDS) && (orderProperty.mdsCd)) {
            return WorkGroup.MDS;
          }
        }
      }
    }
    return 0;
  }

  public getEventUrl({ eventId = 0, eventType = null, eventTypeId = 0 }) {
    let url = {
      link: null,
      param: null
    }

    if (eventType == "AD" || eventTypeId == EEventType.AD) {
      url.link = 'event/access-delivery/' + eventId;
    }
    else if (eventType == "Fedline" || eventTypeId == EEventType.Fedline) {
      url.link = 'event/fedline/' + eventId;
    }
    else if (eventType == "IPSD" || eventType.indexOf("MDS") >= 0 || eventTypeId == EEventType.MDS) {
      url.link = 'event/mds/' + eventId;
    }
    else if (eventType == "MPLS" || eventTypeId == EEventType.MPLS) {
      url.link = 'event/mpls/' + eventId;
    }
    else if (eventType == "NGVN" || eventTypeId == EEventType.NGVN) {
      url.link = 'event/ngvn/' + eventId;
    }
    else if (eventType == "SIPT" || eventTypeId == EEventType.SIPT) {
      url.link = 'event/sipt/' + eventId;
    }
    else if (eventType == "SprintLink" || eventTypeId == EEventType.SprintLink) {
      url.link = 'event/sprint-link/' + eventId;
    }
    else if (eventType == "UCaaS" || eventTypeId == EEventType.UCaaS) {
      url.link = 'event/ucaas/' + eventId;
    }

    return url
  }

  public getOrderUrl({ orderId = 0, wg = 0, wgName = null, orderCategoryId = 0, taskId = 0, purchaseOrderNo = null, systemOriginalWorkgroup = 0 }) {
    let url = {
      link: null,
      param: null
    }
    
    if (wg == WorkGroup.xNCIAmerica || wgName == "AMNCI"
      || wg == WorkGroup.xNCIAsia || wgName == "ANCI"
      || wg == WorkGroup.xNCIEurope || wgName == "ENCI") {
      if (orderCategoryId == EordrCat.NCCO && taskId == ETasks.xNCIReady) {
        url.link = `/ncco/ncco-order/${orderId}/3`
      }
      else {
        if (wg == WorkGroup.xNCIAmerica || wg == WorkGroup.xNCIAsia || wg == WorkGroup.xNCIEurope) {
          switch (wg) {
            case WorkGroup.xNCIAmerica:
              url.link = `/order/amnci/${wg}/${orderId}`
              url.param = {
                queryParams: {
                  taskId: taskId
                }
              }
              break;
            case WorkGroup.xNCIAsia:
              url.link = `/order/anci/${wg}/${orderId}`
              url.param = {
                queryParams: {
                  taskId: taskId
                }
              }
              break;
            case WorkGroup.xNCIEurope:
              url.link = `/order/enci/${wg}/${orderId}`
              url.param = {
                queryParams: {
                  taskId: taskId
                }
              }
              break;
            default:
              break;
          }
        }
        else if (wgName == "AMNCI" || wgName == "ANCI" || wgName == "ENCI") {

          switch (wgName) {
            case "AMNCI":
              url.link = `/order/amnci/${WorkGroup.xNCIAmerica}/${orderId}`
              url.param = {
                queryParams: {
                  taskId: taskId
                }
              }
              break;
            case "ANCI":
              url.link = `/order/anci/${WorkGroup.xNCIAsia}/${orderId}`
              url.param = {
                queryParams: {
                  taskId: taskId
                }
              }
              break;
            case "ENCI":
              url.link = `/order/enci/${WorkGroup.xNCIEurope}/${orderId}`
              url.param = {
                queryParams: {
                  taskId: taskId
                }
              }
              break;
            default:
              break;
          }
        }
      }
    }
    else if (wg == WorkGroup.GOM || wgName == "GOM") {
      if (orderCategoryId == EordrCat.NCCO) {
        url.link = `/ncco/ncco-order/${orderId}/3`
      }
      else {
        url.link = `/order/gom/${WorkGroup.GOM}/${orderId}`
      }
    }
    else if (wg == WorkGroup.DCPE || wgName == "DCPE") {
      url.link = `/order/dcpe/${WorkGroup.DCPE}/${orderId}`
    }
    else if (wg == WorkGroup.CPETech || wgName == "CPE TECH") {
      url.link = `/order/3rd-party-cpe-tech/${WorkGroup.CPETech}/${orderId}`
    }
    else if (wg == WorkGroup.UCaaS || wgName == "UCaaS" || wgName == "VCPE") {
      url.link = `/order/vcpe/${WorkGroup.UCaaS}/${orderId}`
    }
    else if (wg == WorkGroup.RTS || wgName == "RTS" || wgName == "Sales Support") {
      url.link = `/order/rts/${WorkGroup.RTS}/${orderId}/${taskId}`
    }
    else if (wg == WorkGroup.CSC || wgName == "CSC") {
      console.log('getOrderUrl')
      url.link = `/order/csc/${WorkGroup.CSC}/${orderId}`
    }
    else if (wg == WorkGroup.System || wgName == "System" || wg == 0) {
      url.link = `/order/system/${orderId}`
      url.param = {
        queryParams: {
          taskId: taskId,
          systemOriginalWorkgroup: systemOriginalWorkgroup
        }
      }
    }
    else if (wg == WorkGroup.MDS || wgName == "MDS") {
      if (taskId == ETasks.EquipReceipt) {
        url.link = `/order/cpe-receipts/${WorkGroup.MDS}/${orderId}/${taskId}/${purchaseOrderNo}`
      }
      else {
        url.link = `/order/system/${orderId}`
      }
    }
    else if (orderCategoryId == EordrCat.NCCO) {
      url.link = `/ncco/ncco-order/${orderId}/3`
    }
    
    return url
  }
}
