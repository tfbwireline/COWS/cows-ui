import { Injectable, Inject } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class MdsEventService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/Events/MDS/');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/Events/MDS/' + id);
  }

  public create(model): any {
    return this.api.post('api/Events/MDS/', model);
  }

  public update(id: number, model): any {
    return this.api.put('api/Events/MDS/' + id, model);
  }

  public delete(id: number): any {
    return this.api.delete('api/Events/MDS/' + id);
  }

  public getDesignDoc(h6, dd, vasCeFlg, CeChngFlg) {
    let params = {
      H6: h6,
      DD: dd,
      VasCeFlg: vasCeFlg,
      CeChngFlg: CeChngFlg
    }

    return this.api.post('api/Events/MDS/DesignDocDetail', params);
  }

  public getVASSubTypes(): Observable<any> {
    return this.api.get('api/Events/MDS/GetVASSubTypes');
  }

  public generateICSFile(eventId: number, mode: number, apptId: number): Observable<any> {

    let fileName = (eventId > 0) ? `${eventId}.ics` : `${apptId}.ics` 
    let queryString = `eventId=${eventId}&mode=${mode}`;
    if (apptId > 0) {
      queryString = queryString + `&apptId=${apptId}`;
    }
    //if (userIds != "") {
    //  queryString = queryString + `&userIds=${userIds}`;
    //}
    const headers = new HttpHeaders().set('Content-Disposition', `attachment;filename=${fileName}`);
    let url = `api/Events/MDS/GenerateICSFile/${eventId}?${queryString}`;
    return this.api.get(url, 
    { 
      headers,
      responseType: "blob",
    });
    
  }

  public isEventMailSent(eventId: number, eventReqTypeId: number) {
    let params = {
      eventId: eventId,
      emailReqTypeId: eventReqTypeId
    }

    return this.api.get('api/Events/MDS/IsEventEmailSent', { params: params });
  }

  public calDtwithBusiDays(inputDays: string) {
    let params = {
      inputDays: inputDays
    }

    return this.api.get('api/Events/MDS/CalDtwithBusiDays', { params: params });
  }

  public calDtwithBusiDaysIncludingWeekend(inputDays: string) {
    let params = {
      inputDays: inputDays
    }

    return this.api.get('api/Events/MDS/CalDtwithBusiDaysIncludingWeekend', { params: params });
  }

  public isSprintHoliday(dtInput: string) {
    let params = {
      dtInput: dtInput
    }

    return this.api.get('api/Events/MDS/CheckIfSprintHoliday', { params: params });
  }

}
