import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { TimeSlotOccurence } from "./../models";

@Injectable({
  providedIn: 'root'
})

export class CCDBypassService {

  constructor(private api: ApiHelperService) { }

  //public getTimeSlotOccurances(eventId: number, bShowAfterHrsSlots: boolean): Observable<any> {
  //  return this.api.get('api/TimeSlotOccurrence', { params: { eventId: eventId, bShowAfterHrsSlots: bShowAfterHrsSlots } });
  //}

  //public getFTEventsForTS(resourceID: number): Observable<any> {
  //  return this.api.get('api/TimeSlotOccurrence/getFTEventsForTS/' + resourceID);
  //}

  //public updateResourceAvailability(updResultset: string): Observable<any> {
  //  return this.api.get('api/TimeSlotOccurrence/updateResourceAvailability/'+ updResultset);
  //}

  //public checkUserAccessToAfterHrsSlots(adid: string): Observable<any> {
  //  return this.api.get('api/TimeSlotOccurrence/checkUserAccessToAfterHrsSlots/' + adid);
  //}

  //public getInvalidTSEvents(): Observable<any> {
  //  return this.api.get('api/TimeSlotOccurrence/getInvalidTSEvents');
  //}

  //public getDisconnectEvents(): Observable<any> {
  //  return this.api.get('api/TimeSlotOccurrence/getDisconnectEvents');
  //}
  //public findUser(searchString: string, userId: number): Observable<any> {
  //  return this.api.get('api/CCDBypass', { params: { searchString: searchString, userId: userId } });
  //}
  public deleteCCDBypass(id: number): Observable<any> {
    return this.api.delete('api/CCDBypass/' + id);
  }
  public create(service): Observable<any> {
    return this.api.post('api/CCDBypass/', service);
  }
  public findUser(searchString: string): Observable<any> {
    return this.api.get('api/CCDBypass/findUser/' + searchString);
  }
  public getBypassUserList(): Observable<any> {
    return this.api.get('api/CCDBypass/GetBypassUserList');
  }

  public GetCCDHistory(orderId: number): Observable<any> {
    let params = {
      orderId: orderId
    }
    return this.api.get('api/CCDBypass/GetCCDHistory', { params: params });
  }
}
