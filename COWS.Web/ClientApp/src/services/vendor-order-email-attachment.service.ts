import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { VendorOrderEmail, VendorOrderEmailAttachment } from '../models';

@Injectable({
  providedIn: 'root'
})

export class VendorOrderEmailAttachmentService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/VendorOrderEmailAttachments');
  }

  public download(id: number): Observable<any> {
    return this.api.get('api/VendorOrderEmailAttachments/' + id + '/download', { responseType: "blob" });
  }

  public create(vendorOrderEmailAttachment: VendorOrderEmailAttachment): Observable<any> {
    return this.api.post('api/VendorOrderEmailAttachments/', vendorOrderEmailAttachment);
  }

  public delete(id: number, vendorOrderEmail: VendorOrderEmail): any {
    return this.api.put('api/VendorOrderEmailAttachments/' + id + '/Delete', vendorOrderEmail);
  }

  public uploadOffnetForm(id: number): Observable<any> {
    return this.api.get(`api/VendorOrderEmailAttachments/uploadOffnetForm/${id}`);
  } 
}
