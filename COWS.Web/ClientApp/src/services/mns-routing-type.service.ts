import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { KeyValuePair } from '../shared/global';

@Injectable({
  providedIn: 'root'
})

export class MnsRoutingTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/MnsRoutingTypes');
  }

  public getById(id: string): Observable<any> {
    return this.api.get('api/MnsRoutingTypes/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/MnsRoutingTypes').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new KeyValuePair(item.mnsRoutgTypeDes, +item.mnsRoutgTypeId));
      }));
  }
}
