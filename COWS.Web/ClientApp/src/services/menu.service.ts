import { Injectable, Inject } from '@angular/core';
import { Params } from '@angular/router';
import { ApiHelperService } from "../shared/index";
import { Observable } from 'rxjs';

interface IBreadcrumb {
  label: string;
  params: Params;
  url: string;
  isLink: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  forcedHorizontal: boolean = false
  isTopNav: boolean = false
  isSideNav: boolean = true
  isContentCollapsed: boolean
  breadcrumbs: IBreadcrumb[];
  urlAddress: string = "";

  constructor(private api: ApiHelperService) {
    //this.httpHeaders = new HttpHeaders({
    //  'Content-Type': 'application/json',
    //  'withCredentials': 'true'
    //});
  }

  refresh() {
    this.forcedHorizontal = window.innerWidth < 768 ? true : false

    if (this.forcedHorizontal) {
      this.navMenu()
    } else {
      this.sideNav()
    }
  }

  sideNav() {
    if (this.forcedHorizontal) {
      this.navMenu()
    } else {
      this.isContentCollapsed = false
      this.isSideNav = true
      this.isTopNav = false
    }
  }

  navMenu() {
    this.isContentCollapsed = true
    this.isSideNav = false
    this.isTopNav = true
  }

  public getOldSiteURL(): Observable<any> {
    return this.api.get('api/Menu/GetOldSiteURL');
  }

  public getExternalLinks(): Observable<any> {
    return this.api.get('api/Menu/GetExternalLinks');
  }


  getTitle(state, parent, i) {

    var data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.title) {
      //let urlAddress = "";
      if (parent.snapshot._lastPathIndex == 0)
        this.urlAddress = "";

      if (parent.snapshot.routeConfig.path != "") {
        let exception = ["Home", "Admin", "Orders", "Events", "CPT", "Tools", "NCCO"]
        //get the route's URL segment
        let routeURL: string = parent.snapshot.url.map(segment => segment.path).join("/");

        //append route URL to URL
        this.urlAddress += `/${routeURL}`;

        //add breadcrumb
        let breadcrumb: IBreadcrumb = {
          label: parent.snapshot.data.title,
          params: parent.snapshot.params,
          url: this.urlAddress,
          isLink: exception.includes(parent.snapshot.data.title) ? false : true
        };
        data.push(breadcrumb);
      }
    }

    if (state && parent) {
      for (let val of this.getTitle(state, state.firstChild(parent), i)) {
        if (!data.includes(val)) {
          data.push(val);
        }
      }
    }
    this.breadcrumbs = data
    return data
  }
}
