import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class RedesignService {

  constructor(private api: ApiHelperService) { }

  public getById(id: number): Observable<any> {
    return this.api.get('api/Redesign/' + id);
  }

  public create(redesign): Observable<any> {
    return this.api.post('api/Redesign/', redesign);
  }

  public update(redesignId: number, redesign): Observable<any> {
    return this.api.put('api/Redesign/' + redesignId, redesign);
  }

  /**
   * Get Customer Information like PDL, Email assignment based on Customer Name
   * @param rspnId
   */
  public getCustNameDetails(rspnId: number): Observable<any> {
    return this.api.get('api/Redesign/GetCustNameDetails/' + rspnId);
  }

  /**
   * Get ODIE ResponseId for ODIE Devices Info based on the ff parameters:
   * @param h1
   * @param custName
   * @param odieCustId
   * @param deviceFilter
   * @param redesignCatId
   */
  public chkODIEResponse(h1: string, custName: string, odieCustId: string,
    deviceFilter: string, redesignCatId?: number): Observable<any> {
    let params = {
      h1: h1,
      custName: custName,
      odieCustId: odieCustId,
      deviceFilter: deviceFilter,
      redesignCatId: redesignCatId
    }
    return this.api.get('api/Redesign/ChkODIEResponse', { params: params });
  }
  
  /**
   * Used to Filter devices
   * @param h1
   * @param custName
   * @param odieCustId
   * @param deviceFilter
   * @param redesignCatId
   * @param redesignId
   * @param filterOnDeviceInfo
   */
  public getODIEDevices(h1: string, custName: string, odieCustId: string, deviceFilter: string,
    redesignCatId?: number, redesignId?: number, filterOnDeviceInfo?: boolean): Observable<any> {
    let params = {
      h1: h1,
      custName: custName,
      odieCustId: odieCustId,
      deviceFilter: deviceFilter,
      redesignCatId: redesignCatId,
      redesignId: redesignId
      //filterOnDeviceInfo: filterOnDeviceInfo
    }
    return this.api.get('api/Redesign/GetODIEDevices', { params: params });
  }

  public getRedesignCustomerBypassCD(h1: string, custName: string): Observable<any> {
    let params = {
      h1: h1,
      custName: custName
    }
    return this.api.get('api/Redesign/GetRedesignCustomerBypassCD', { params: params });
  }

  public getRedesignData(redesignId: number, statusId: number): Observable<any> {
    let params = {
      redesignId: redesignId,
      statusId: statusId
    }
    return this.api.get('api/Redesign/GetRedesignData', { params: params });
  }
  
  public lockUnlockUser(redesignID: number, userID: number, lockUser: boolean): Observable<boolean> {
    let params = {
      redesignID: redesignID,
      userID: userID,
      lockUser: lockUser
    }
    return this.api.get('api/Redesign/LockUnlockUser', { params: params });
  }

  public getRedesignSearchResults(rdsgnNos: string, custName: string, nte: string,
    devices: string, pm: string): Observable<any> {
    let params = {
      rdsgnNos: rdsgnNos,
      custName: custName,
      nte: nte,
      devices: devices,
      pm: pm
    }
    return this.api.get('api/Redesign/GetRedesignSearchResults', { params: params });
  }
}

