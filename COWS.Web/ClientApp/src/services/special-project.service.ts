import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { map } from 'rxjs/operators'
import { FormControlItem } from '../models';

@Injectable({
  providedIn: 'root'
})

export class SpecialProjectService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/SpecialProjects');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/SpecialProjects/' + id);
  }

  public getForControl(): Observable<any> {
    return this.api.get('api/SpecialProjects').pipe(
      map((res: any) => res),
      map((data) => {
        return data.map((item) => new FormControlItem(item.spclProjId, item.spclProjNme));
      }));
  }

  public create(proj): Observable<any> {
    return this.api.post('api/SpecialProjects/', proj);
  }

  public update(id: number, proj): Observable<any> {
    return this.api.put('api/SpecialProjects/' + id, proj);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/SpecialProjects/' + id);
  }
}
