import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { EventInterval } from "./../models";

@Injectable({
  providedIn: 'root'
})

export class EventIntervalService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/EventIntervals');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/EventIntervals/' + id);
  }

  public update(id: number, model: EventInterval): Observable<any> {
    return this.api.put('api/EventIntervals/' + id, model);
  }
}
