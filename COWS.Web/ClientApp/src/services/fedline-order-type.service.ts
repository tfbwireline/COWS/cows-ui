import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class FedlineOrderTypeService {

  constructor(private api: ApiHelperService) { }

  public get(): Observable<any> {
    return this.api.get('api/FedlineOrderTypes');
  }

  public getById(id: number): Observable<any> {
    return this.api.get('api/FedlineOrderTypes/' + id);
  }
}
