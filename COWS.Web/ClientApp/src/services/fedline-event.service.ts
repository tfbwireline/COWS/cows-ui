import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class FedlineEventService {

  constructor(private api: ApiHelperService) { }

  //public get(): Observable<any> {
  //  return this.api.get('api/Events/Fedline');
  //}

  public getById(id: number): Observable<any> {
    return this.api.get('api/Events/Fedline/' + id);
  }

  public create(fedline): Observable<any> {
    return this.api.post('api/Events/Fedline/', fedline);
  }

  public update(id: number, fedline): Observable<any> {
    return this.api.put('api/Events/Fedline/' + id, fedline);
  }

  public delete(id: number): Observable<any> {
    return this.api.delete('api/Events/Fedline/' + id);
  }

  public getEventUpdateHistByEventID(id: number): Observable<any> {
    return this.api.get('api/Events/Fedline/getEventUpdateHistByEventID/' + id);
  }

  public getCCDHistory(id: number): Observable<any> {
    return this.api.get('api/Events/Fedline/getCCDHistory/' + id);
  }

  public getFedlineEventFailCodes(id: number): Observable<any> {
    return this.api.get('api/Events/Fedline/getFedlineEventFailCodes/' + id);
  }

  public checkCancelExists(id: number): Observable<any> {
    return this.api.get('api/Events/Fedline/checkCancelExists/' + id);
  }

  public searchUsers(input: string, profileId: number): Observable<any> {
    let params = {
      input: input,
      profileId: profileId
    }
    return this.api.get('api/Events/Fedline/searchUsers', { params: params });
  }
}
