import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
import { zip, of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup } from '@angular/forms';
import { FsaOrdersService } from './fsa-orders.service';
import { FsaOrderCustService } from './fsa-order-cust.service';
import { OrderAddressService } from './order-address.service';
import { OrderContactService } from './order-contact.service';
import { FsaOrderCpeLineItemService } from './fsa-order-cpe-line-item.service';
import { OrderVlanService } from './order-vlan.service';
import { formatDate } from '@angular/common';
import { FsaOrderCpeLineItems } from '../models';
import { HierLevel } from '../shared/global';
//import { disconnect } from 'cluster';

@Injectable({
  providedIn: 'root'
})

export class AmnciService {
  orderCatId = 0

  constructor(private api: ApiHelperService, private spinner: NgxSpinnerService,
    private fsaOrdersService: FsaOrdersService, private fsaOrderCustService: FsaOrderCustService, private orderAddressService: OrderAddressService, private orderContactService: OrderContactService,
    private orderVlanService: OrderVlanService, private fsaOrderCpeLineItemService: FsaOrderCpeLineItemService) { }

  getFsaDetails(orderId: number, parent: any) {
    let subs = zip(
      this.fsaOrdersService.getById(orderId),
      this.fsaOrderCustService.getByCisLevelIdAndId(orderId, 'H1'),
      this.fsaOrderCustService.getByCisLevelIdAndId(orderId, 'H4'),
      this.orderAddressService.getByOrderId(orderId),
      this.orderContactService.getByOrderId(orderId),
      this.fsaOrderCustService.getByCisLevelIdAndId(orderId, 'H6'),
      this.orderVlanService.getByOrderId(orderId),
      this.orderContactService.getByOrderId(orderId),
      this.fsaOrderCpeLineItemService.getByOrderId(orderId),
      this.fsaOrdersService.getFSADisconnectDetails(orderId)
    )

    this.spinner.show()
    subs.subscribe(res => {
      console.log(res)
      let order = res[0]
      let h1 = res[1]
      let h4 = res[2]
      let orderAddress = res[3] as any[]
      let orderContact = res[4] as any[]
      let h6 = res[5]
      let orderVlanList = (res[6] as any[]).filter(a => a.vlanSrcNme != "COWS")
      let accountTeam = res[7] as any[]
      let cpeLineItemList = res[8] as any[]
      let disconnectDetails = res[9]

      console.log(h6)
      // Order Information
      if (order != null) {
        this.orderCatId = order.ordrCatId
        parent.form.get("order.orderAction").setValue(order.ordrActnDes)
        parent.form.get("order.ftn").setValue(order.ftn)
        parent.form.get("order.orderTypeCode").setValue(order.ordrTypeDes)
        parent.form.get("order.orderSubTypeCode").setValue(order.ordrSubTypeCd)
        parent.form.get("order.productTypeCode").setValue(order.prodTypeDes)
        parent.form.get("order.parentFtn").setValue(order.prntFtn)
        parent.form.get("order.relatedFtn").setValue(order.reltdFtn)
        parent.form.get("order.telecomServicePriority").setValue(order.tspCd)
        parent.form.get("order.carrierPartnerWholesaleType").setValue(order.cpwType)

        if (order.custCmmtDt != null)
          parent.form.get("order.customerCommitDate").setValue(formatDate(order.custCmmtDt, 'MM/dd/yyyy', 'en'))
        if (order.custWantDt != null)
          parent.form.get("order.customerWantDate").setValue(formatDate(order.custWantDt, 'MM/dd/yyyy', 'en'))
        if (order.custOrdrSbmtDt != null)
          parent.form.get("order.customerOrderSubmitDate").setValue(formatDate(order.custOrdrSbmtDt, 'MM/dd/yyyy', 'en'))
        if (order.custSignedDt != null)
          parent.form.get("order.customerSignedDate").setValue(formatDate(order.custSignedDt, 'MM/dd/yyyy', 'en'))
        //if (order.ordrSbmtDt != null)
        //  parent.form.get("order.orderSubmitDate").setValue(formatDate(order.ordrSbmtDt, 'MM/dd/yyyy', 'en'))

        parent.form.get("order.customerPremiseCurrentlyOccupiedFlag").setValue(order.custPrmsOcpyCd)
        parent.form.get("order.willCustomerAcceptServiceEarlyFlag").setValue(order.custAcptErlySrvcCd)
        parent.form.get("order.multipleOrderFlag").setValue(order.multCustOrdrCd)
        //parent.form.get("order.multipleOrderIndex").setValue(order.multOrdrIndexNbr)
        //parent.form.get("order.multipleOrderTotal").setValue(order.multOrdrTotCnt)
        parent.form.get("order.escalatedFlag").setValue(order.instlEsclCd)
        parent.form.get("order.expediteTypeCode").setValue(order.fsaExpTypeCd)
        parent.form.get("order.govtTypeCode").setValue(order.gvrnmntTypeId)
        //parent.form.get("order.vendorVpnCode").setValue(order.VndrVpnCd)
        parent.form.get("order.prequalNo").setValue(order.preQualNbr)
        parent.form.get("order.scaNumber").setValue(order.scaNbr)
      }

      // Customer Information
      // H1
      if (h1 != null) {
        parent.form.get("customer.h1.customerId").setValue(h1.custId)
        parent.form.get("customer.h1.customerName").setValue(h1.custNme)
        parent.form.get("customer.h1.currentBillingCycleCode").setValue(h1.currBillCycCd)
        parent.form.get("customer.h1.futureBillingCycleCode").setValue(h1.futBillCycCd)
        parent.form.get("customer.h1.branchCode").setValue(h1.brCd)
        parent.form.get("customer.h1.taxExemption").setValue(h1.taxXmptdCd)
        parent.form.get("customer.h1.charsId").setValue(h1.charsCustId)
        parent.form.get("customer.h1.siteId").setValue(h1.siteId)

        if(parent.form.get("display") != null) {
          parent.form.get("display.customerNameH1").setValue(h1.custNme)
        }
      }

      // H4
      if (h4 != null) {
        parent.form.get("customer.h4.customerId").setValue(h4.custId)
        parent.form.get("customer.h4.customerName").setValue(h4.custNme)
        parent.form.get("customer.h4.currentBillingCycleCode").setValue(h4.currBillCycCd)
        parent.form.get("customer.h4.futureBillingCycleCode").setValue(h4.futBillCycCd)
        parent.form.get("customer.h4.taxExemption").setValue(h4.taxXmptdCd)
        parent.form.get("customer.h4.charsId").setValue(h4.charsCustId)
        parent.form.get("customer.h4.siteId").setValue(h4.siteId)
      }

      // H4 Billing Address
      let h4Address = orderAddress.find(a => a.cisLvlType == "H4")
      if (h4Address != null) {
        parent.form.get("customer.h4Address.addressType").setValue(h4Address.adrTypeDes)
        parent.form.get("customer.h4Address.addressLine1").setValue(h4Address.streetAdr1)
        parent.form.get("customer.h4Address.addressLine2").setValue(h4Address.streetAdr2)
        parent.form.get("customer.h4Address.addressLine3").setValue(h4Address.streetAdr3)
        //parent.form.get("customer.h4Address.suite").setValue("")
        parent.form.get("customer.h4Address.city").setValue(h4Address.ctyNme)
        parent.form.get("customer.h4Address.stateCode").setValue(h4Address.sttCd)
        parent.form.get("customer.h4Address.zipCode").setValue(h4Address.zipPstlCd)
        parent.form.get("customer.h4Address.countryCode").setValue(h4Address.ctryCd)
        parent.form.get("customer.h4Address.provinceMunicipality").setValue(h4Address.prvnNme)
      }

      // H4 Billing Contact
      let h4Contact = orderContact.find(a => a.cisLvlType == "H4" && a.cntctTypeId == 1)
      if (h4Contact != null) {
        parent.form.get("customer.h4Contact.type").setValue(h4Contact.roleNme)
        parent.form.get("customer.h4Contact.contactType").setValue(h4Contact.cntctTypeDes)
        parent.form.get("customer.h4Contact.firstName").setValue(h4Contact.frstNme)
        parent.form.get("customer.h4Contact.lastName").setValue(h4Contact.lstNme)
        parent.form.get("customer.h4Contact.name").setValue(h4Contact.cntctNme)
        parent.form.get("customer.h4Contact.emailAddress").setValue(h4Contact.emailAdr)
        parent.form.get("customer.h4Contact.cityCode").setValue(h4Contact.ctyCd)
        parent.form.get("customer.h4Contact.npa").setValue(h4Contact.npa)
        parent.form.get("customer.h4Contact.nxx").setValue(h4Contact.nxx)
        parent.form.get("customer.h4Contact.station").setValue(h4Contact.stnNbr)
        parent.form.get("customer.h4Contact.phoneNumber").setValue(h4Contact.phnNbr)
        parent.form.get("customer.h4Contact.faxNumber").setValue(h4Contact.faxNbr)
      }

      // H6
      let h6Address = orderAddress.find(a => a.cisLvlType == "H6")
      let h6Contact = orderContact.find(a => a.cisLvlType == "H6" && a.cntctNme == h6.custNme)
      if (h6 != null) {
        parent.form.get("customer.h6.customerId").setValue(h6.custId)
        parent.form.get("customer.h6.customerName").setValue(h6.custNme)
        parent.form.get("customer.h6.currentBillingCycleCode").setValue(h6.currBillCycCd)
        parent.form.get("customer.h6.futureBillingCycleCode").setValue(h6.futBillCycCd)
        parent.form.get("customer.h6.branchCode").setValue(h1.brCd)
        parent.form.get("customer.h6.taxExemption").setValue(h6.taxXmptdCd)
        parent.form.get("customer.h6.charsId").setValue(h6.charsCustId)
        parent.form.get("customer.h6.siteId").setValue(h6.siteId)
        parent.form.get("customer.h6.serviceSubType").setValue(h6.srvcSubTypeId)
        parent.form.get("customer.h6.salesOfficeIdCode").setValue(h6.soiCd)
        parent.form.get("customer.h6.salesPersonPrimaryCid").setValue(h6.salsPersnPrimCid)
        parent.form.get("customer.h6.salesPersonSecondaryCid").setValue(h6.salsPersnScndyId)
        parent.form.get("customer.h6.cllidCode").setValue(h6.clliCd)
      }
      if (h6Address != null) {
        parent.form.get("customer.h6.addressType").setValue(h6Address.adrTypeDes)
        parent.form.get("customer.h6.addressLine1").setValue(h6Address.streetAdr1)
        parent.form.get("customer.h6.addressLine2").setValue(h6Address.streetAdr2)
        parent.form.get("customer.h6.addressLine3").setValue(h6Address.streetAdr3)
        //parent.form.get("customer.h6.suite").setValue("")
        parent.form.get("customer.h6.city").setValue(h6Address.ctyNme)
        parent.form.get("customer.h6.stateCode").setValue(h6Address.sttCd)
        parent.form.get("customer.h6.zipCode").setValue(h6Address.zipPstlCd)
        parent.form.get("customer.h6.countryCode").setValue(h6Address.ctryCd)
        parent.form.get("customer.h6.provinceMunicipality").setValue(h6Address.prvnNme)
        parent.form.get("customer.h6.buildingName").setValue(h6Address.bldgNme)
        parent.form.get("customer.h6.floor").setValue(h6Address.flrId)
        parent.form.get("customer.h6.roomNumber").setValue(h6Address.rmNbr)

        if(parent.form.get("display") != null) {
          parent.form.get("display.h6AddressLine1").setValue(h6Address.streetAdr1)
          parent.form.get("display.h6City").setValue(h6Address.ctyNme)
          parent.form.get("display.h6Country").setValue(h6Address.ctryCd)
        }
      }
      if (h6Contact != null) {
        parent.form.get("customer.h6.npa").setValue(h6Contact.npa)
        parent.form.get("customer.h6.nxx").setValue(h6Contact.nxx)
        parent.form.get("customer.h6.station").setValue(h6Contact.stnNbr)
        parent.form.get("customer.h6.phoneNumber").setValue(h6Contact.phnNbr)
        parent.form.get("customer.h6.faxNumber").setValue(h6Contact.faxNbr)
      }

      // H6 Contact
      let h6Contact2 = orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 92)
      if (h6Contact2 != null) {
        parent.form.get("customer.h6Contact.type").setValue(h6Contact2.roleNme)
        parent.form.get("customer.h6Contact.contactType").setValue(h6Contact2.cntctTypeDes)
        parent.form.get("customer.h6Contact.firstName").setValue(h6Contact2.frstNme)
        parent.form.get("customer.h6Contact.lastName").setValue(h6Contact2.lstNme)
        parent.form.get("customer.h6Contact.name").setValue(h6Contact2.cntctNme)
        parent.form.get("customer.h6Contact.emailAddress").setValue(h6Contact2.emailAdr)
        parent.form.get("customer.h6Contact.cityCode").setValue(h6Contact2.ctyCd)
        parent.form.get("customer.h6Contact.npa").setValue(h6Contact2.npa)
        parent.form.get("customer.h6Contact.nxx").setValue(h6Contact2.nxx)
        parent.form.get("customer.h6Contact.station").setValue(h6Contact2.stnNbr)
        parent.form.get("customer.h6Contact.phoneNumber").setValue(h6Contact2.phnNbr)
        parent.form.get("customer.h6Contact.faxNumber").setValue(h6Contact2.faxNbr)
      }

      // Install Information
      // MDS Installation Information
      if (order != null) {
        parent.form.get("install.mdsInstallationInformation.solutionService").setValue(order.instlSoluSrvcDes)
        parent.form.get("install.mdsInstallationInformation.networkTypeCode").setValue(order.instlNwTypeCd)
        parent.form.get("install.mdsInstallationInformation.serviceTierCode").setValue(order.instlSrvcTierCd)
        parent.form.get("install.mdsInstallationInformation.transportOrderTypeCode").setValue(order.instlTrnsprtTypeCd)
        parent.form.get("install.mdsInstallationInformation.designDocumentNo").setValue(order.instlDsgnDocNbr)
        parent.form.get("install.mdsInstallationInformation.vendorCode").setValue(order.instlVndrCd)
      }
   //milestone customer term
if (order != null) {
        let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
        let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems
        
  if (parent.form.get("mileStoneInformation.customerContractTerm") != null) {
    parent.form.get("mileStoneInformation.customerContractTerm").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsTermDes || "")
  }

}

      // Transport
      if (order != null) {
        let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
        let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems
        
        parent.form.get("install.mdsInstallationInformation.designDocumentNo").setValue((prt || acs || new FsaOrderCpeLineItems({})).instlDsgnDocNbr || "")

        parent.form.get("install.transport.accessTypeCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsTypeCd || "")
        parent.form.get("install.transport.accessArrangementCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsArngtCd || "")
        parent.form.get("install.transport.encapsulationCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptEncapCd || "")
        parent.form.get("install.transport.fiberHandoffCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptFbrHandOffCd || "")
        parent.form.get("install.transport.carrierAccessCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).cxrAccsCd || "")
        parent.form.get("install.transport.circuitId").setValue(order.ttrptCktId)
        parent.form.get("install.transport.serviceTerm").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsTermDes || "")
        parent.form.get("install.transport.quoteExpirationDate").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptQotXpirnDt || null)
        parent.form.get("install.transport.accessBandwidth").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsBdwdType || "")
        parent.form.get("install.transport.privateLineNo").setValue((acs || prt || new FsaOrderCpeLineItems({})).plNbr || "")
        //parent.form.get("install.transport.m5AccessStatus").setValue("")
        parent.form.get("install.transport.classOfService").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptCosCd || "")
      
        if(parent.form.get("display") != null) {
          parent.form.get("display.accessBandwidth").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsBdwdType || "")
        }
      }

      // CPE Installation Information
      if (order != null) {
        parent.form.get("install.cpeInstallationInformation.cpeOrderTypeCode").setValue(order.cpeCpeOrdrTypeCd)
        parent.form.get("install.cpeInstallationInformation.equipmentOnlyFlagCode").setValue(order.cpeEqptOnlyCd)
        parent.form.get("install.cpeInstallationInformation.accessProvideCode").setValue(order.cpeAccsPrvdrCd)
        parent.form.get("install.cpeInstallationInformation.phoneNumberType").setValue(order.cpePhnNbrTypeCd)
        parent.form.get("install.cpeInstallationInformation.phoneNumber").setValue(order.cpePhnNbr)
        parent.form.get("install.cpeInstallationInformation.eccktIdentifier").setValue(order.cpeEccktId)
        parent.form.get("install.cpeInstallationInformation.managedServicesChannelProgramCode").setValue(order.cpeMscpCd)
        parent.form.get("install.cpeInstallationInformation.deliveryDuties").setValue(order.cpeDlvryDutyId)
        parent.form.get("install.cpeInstallationInformation.deliveryDutyAmount").setValue(order.cpeDlvryDutyAmt)
        parent.form.get("install.cpeInstallationInformation.shippingChargeAmount").setValue(order.cpeShipChgAmt)
        parent.form.get("install.cpeInstallationInformation.recordOnly").setValue(order.cpeRecOnlyCd)
        parent.form.get("install.cpeInstallationInformation.nuaOrCkt").setValue(order.ckt || order.nua || '')
      }

      // CPE Line Items
      parent.cpeLineItemList = cpeLineItemList

      // Port
      //console.log(order.fsaOrdrCpeLineItem)
      if (order != null) {
        let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
        let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems

        parent.installPortList = [{
          m5PortStatus: "",
          ttrptSpdOfSrvcBdwdDes: (prt || acs || new FsaOrderCpeLineItems({})).ttrptSpdOfSrvcBdwdDes || "",
          tportEthAccsTypeCd: (prt || acs || new FsaOrderCpeLineItems({})).tportEthAccsTypeCd || "",
          spaAccsIndcrDes: (prt || acs || new FsaOrderCpeLineItems({})).spaAccsIndcrDes || "",
          tportVndrQuoteId: (prt || acs || new FsaOrderCpeLineItems({})).tportVndrQuoteId || "",
          tportEthrntNrfcIndcr: (prt || acs || new FsaOrderCpeLineItems({})).tportEthrntNrfcIndcr || "",
          tportCnctrTypeId: (prt || acs || new FsaOrderCpeLineItems({})).tportCnctrTypeId || "",
          custRoutrTagTxt: (prt || acs || new FsaOrderCpeLineItems({})).tportCustRoutrTagTxt || "",
          custRoutrAutoNegotCd: (prt || acs || new FsaOrderCpeLineItems({})).tportCustRoutrAutoNegotCd || "",
          ipVerTypeCd: (prt || acs || new FsaOrderCpeLineItems({})).tportIpVerTypeCd || "",
          ipv4AdrPrvdrCd: (prt || acs || new FsaOrderCpeLineItems({})).tportIpv4AdrPrvdrCd || "",
          ipv4AdrQty: (prt || acs || new FsaOrderCpeLineItems({})).tportIpv4AdrQty || "",
          ipv6AdrPrvdrCd: (prt || acs || new FsaOrderCpeLineItems({})).tportIpv4AdrQty || "",
          ipv6AdrQty: (prt || acs || new FsaOrderCpeLineItems({})).tportIpv4AdrQty || "",
          vlanQty: (prt || acs || new FsaOrderCpeLineItems({})).tportVlanQty,
          diaNocToNocTxt: (prt || acs || new FsaOrderCpeLineItems({})).tportDiaNocToNocTxt || "",
          projDes: (prt || acs || new FsaOrderCpeLineItems({})).tportProjDes || ""
        }]

        let spaAccsIndcrDes = (prt || acs || new FsaOrderCpeLineItems({})).spaAccsIndcrDes || ""
        let asrType = (spaAccsIndcrDes == "A" || spaAccsIndcrDes == "Aggregated") ? 1 :
          (spaAccsIndcrDes == "D" || spaAccsIndcrDes == "Dedicated") ? 2 : null
        if (parent.form.get("asr") != null) {
          parent.form.get("asr.asrType").setValue(asrType)
        }
      }

      // Vlan
      parent.installVlanList = orderVlanList

      // Change Information
      if (order != null) {
        let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
        let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems

        parent.form.get("install.changeInformation.changeDescription").setValue(order.ordrChngDes)
        parent.form.get("install.changeInformation.carrierServiceId").setValue(order.cxrSrvcId)
        parent.form.get("install.changeInformation.carrierCircuitId").setValue(order.cxrCktId)
        parent.form.get("install.changeInformation.originalInstallOrderId").setValue(order.fsaOrgnlInstlOrdrId)
        parent.form.get("install.changeInformation.networkUserAddress").setValue(order.nwUserAdr)
        parent.form.get("install.changeInformation.portRateTypeCode").setValue((prt || acs || new FsaOrderCpeLineItems({})).portRtTypeCd || "")
      }

      console.log(accountTeam)
      parent.orderContactList = accountTeam.filter(a => a.cisLvlType == HierLevel.OD)
      console.log(parent.orderContactList)

      // Account Team
      parent.accountTeamList = accountTeam

      // Disconnect Information
      if (order != null) {
        parent.form.get("disconnect.reasonCode").setValue(order.discReasCd)
        parent.form.get("disconnect.cancelBeforeStartReasonCode").setValue(order.discCnclBefStrtReasCd)
        parent.form.get("disconnect.cancelBeforeStartReasonText").setValue(order.discCnclBefReasDtlTxt)
      }

      // Disconnect Contact
      if (order != null) {
        let disconnectContact = null
        if (order.orderCatId == 6 && orderContact.find(a => a.roleId == 99) != null) {
          disconnectContact = orderContact.find(a => a.roleId == 99)
        } else if (orderContact.find(a => a.fsaMdulId == "DSC") != null) {
          disconnectContact = orderContact.find(a => a.fsaMdulId == "DSC")
        }

        if (disconnectContact != null) {
          parent.form.get("customer.h6Contact.type").setValue(disconnectContact.roleNme)
          parent.form.get("customer.h6Contact.contactType").setValue(disconnectContact.cntctTypeDes)
          parent.form.get("customer.h6Contact.firstName").setValue(disconnectContact.frstNme)
          parent.form.get("customer.h6Contact.lastName").setValue(disconnectContact.lstNme)
          parent.form.get("customer.h6Contact.name").setValue(disconnectContact.cntctNme)
          parent.form.get("customer.h6Contact.emailAddress").setValue(disconnectContact.emailAdr)
          parent.form.get("customer.h6Contact.cityCode").setValue(disconnectContact.ctyCd)
          parent.form.get("customer.h6Contact.npa").setValue(disconnectContact.npa)
          parent.form.get("customer.h6Contact.nxx").setValue(disconnectContact.nxx)
          parent.form.get("customer.h6Contact.station").setValue(disconnectContact.stnNbr)
          parent.form.get("customer.h6Contact.phoneNumber").setValue(disconnectContact.phnNbr)
          parent.form.get("customer.h6Contact.faxNumber").setValue(disconnectContact.faxNbr)
        }
      }


      if(disconnectDetails != null) {

        parent.form.get("disconnect.contact.type").setValue(disconnectDetails.roleName);
        parent.form.get("disconnect.contact.contactType").setValue(disconnectDetails.contactType);
        parent.form.get("disconnect.contact.firstName").setValue(disconnectDetails.frstNme);
        parent.form.get("disconnect.contact.lastName").setValue(disconnectDetails.lstNme);
        parent.form.get("disconnect.contact.name").setValue(disconnectDetails.cntctNme);
        parent.form.get("disconnect.contact.emailAddress").setValue(disconnectDetails.emailAdr);
        // parent.form.get("disconnect.countryCode").setValue();
        // parent.form.get("disconnect.cityCode").setValue();
        parent.form.get("disconnect.contact.npa").setValue(disconnectDetails.npa);
        parent.form.get("disconnect.contact.nxx").setValue(disconnectDetails.nxx);
        parent.form.get("disconnect.contact.station").setValue(disconnectDetails.stnNbr);
        parent.form.get("disconnect.contact.phoneNumber").setValue(disconnectDetails.phnNbr);
        // parent.form.get("disconnect.faxNumber").setValue();
        // parent.form.get("disconnect.extension").setValue();
        // ctyCd: ""
        // isdCd: ""
      }

      if (parent.fsaDetails != null) {
        parent.fsaDetails.show(orderId)
      }
    }, error => {
        console.log(error)
      this.spinner.hide();
    }, () => this.spinner.hide())
  }



  getFsaDetails2(orderId: number, parent: any) {
    console.log(parent.orderResult)
    let orderResult = parent.orderResult
    let order = orderResult.orderInfo.fsaOrdr
    let installPortList = orderResult.installPort
    let h1 = orderResult.h1Info
    let h4 = orderResult.h4Info
    let orderAddress = orderResult.orderInfo.ordrAdr
    let orderContact = orderResult.orderInfo.ordrCntct
    let h6 = orderResult.h6Info
    let h6Contact = orderResult.orderInfo.h6Contact
    //let h6Address = orderResult.h6Address
    let orderVlanList = orderResult.orderInfo.ordrVlan
    let accountTeam = orderResult.orderInfo.ordrCntct
    let cpeLineItemList = order.fsaOrdrCpeLineItem
    let disconnectDetails = orderResult.fsaDisconnectDetails

    // Order Information
    if (order != null) {
      this.orderCatId = order.ordrCatId
      parent.form.get("order.orderAction").setValue(order.ordrActnDes)
      parent.form.get("order.ftn").setValue(order.ftn)
      parent.form.get("order.orderTypeCode").setValue(order.ordrTypeDes)
      parent.form.get("order.orderSubTypeCode").setValue(order.ordrSubTypeCd)
      parent.form.get("order.productTypeCode").setValue(order.prodTypeDes)
      parent.form.get("order.parentFtn").setValue(order.prntFtn)
      parent.form.get("order.relatedFtn").setValue(order.reltdFtn)
      parent.form.get("order.telecomServicePriority").setValue(order.tspCd)
      parent.form.get("order.carrierPartnerWholesaleType").setValue(order.cpwType)

      if (order.custCmmtDt != null)
        parent.form.get("order.customerCommitDate").setValue(formatDate(order.custCmmtDt, 'MM/dd/yyyy', 'en'))
      if (order.custWantDt != null)
        parent.form.get("order.customerWantDate").setValue(formatDate(order.custWantDt, 'MM/dd/yyyy', 'en'))
      if (order.custOrdrSbmtDt != null)
        parent.form.get("order.customerOrderSubmitDate").setValue(formatDate(order.custOrdrSbmtDt, 'MM/dd/yyyy', 'en'))
      if (order.custSignedDt != null)
        parent.form.get("order.customerSignedDate").setValue(formatDate(order.custSignedDt, 'MM/dd/yyyy', 'en'))
      //if (order.ordrSbmtDt != null)
      //  parent.form.get("order.orderSubmitDate").setValue(formatDate(order.ordrSbmtDt, 'MM/dd/yyyy', 'en'))

      parent.form.get("order.customerPremiseCurrentlyOccupiedFlag").setValue(order.custPrmsOcpyCd)
      parent.form.get("order.willCustomerAcceptServiceEarlyFlag").setValue(order.custAcptErlySrvcCd)
      parent.form.get("order.multipleOrderFlag").setValue(order.multCustOrdrCd)
      //parent.form.get("order.multipleOrderIndex").setValue(order.multOrdrIndexNbr)
      //parent.form.get("order.multipleOrderTotal").setValue(order.multOrdrTotCnt)
      parent.form.get("order.escalatedFlag").setValue(order.instlEsclCd)
      parent.form.get("order.expediteTypeCode").setValue(order.fsaExpTypeCd)
      parent.form.get("order.govtTypeCode").setValue(order.gvrnmntTypeId)
      //parent.form.get("order.vendorVpnCode").setValue(order.VndrVpnCd)
      parent.form.get("order.prequalNo").setValue(order.preQualNbr)
      parent.form.get("order.scaNumber").setValue(order.scaNbr)
    }

    // Customer Information
    // H1
    if (h1 != null) {
      parent.form.get("customer.h1.customerId").setValue(h1.custId)
      parent.form.get("customer.h1.customerName").setValue(h1.custNme)
      parent.form.get("customer.h1.currentBillingCycleCode").setValue(h1.currBillCycCd)
      parent.form.get("customer.h1.futureBillingCycleCode").setValue(h1.futBillCycCd)
      parent.form.get("customer.h1.branchCode").setValue(h1.brCd)
      parent.form.get("customer.h1.taxExemption").setValue(h1.taxXmptdCd)
      parent.form.get("customer.h1.charsId").setValue(h1.charsCustId)
      parent.form.get("customer.h1.siteId").setValue(h1.siteId)

      if(parent.form.get("display") != null) {
        parent.form.get("display.customerNameH1").setValue(h1.custNme)
      }
    }

    // H4
    if (h4 != null) {
      parent.form.get("customer.h4.customerId").setValue(h4.custId)
      parent.form.get("customer.h4.customerName").setValue(h4.custNme)
      parent.form.get("customer.h4.currentBillingCycleCode").setValue(h4.currBillCycCd)
      parent.form.get("customer.h4.futureBillingCycleCode").setValue(h4.futBillCycCd)
      parent.form.get("customer.h4.taxExemption").setValue(h4.taxXmptdCd)
      parent.form.get("customer.h4.charsId").setValue(h4.charsCustId)
      parent.form.get("customer.h4.siteId").setValue(h4.siteId)
    }

    // H4 Billing Address
    let h4Address = orderAddress.find(a => a.cisLvlType == "H4")
    if (h4Address != null) {
      parent.form.get("customer.h4Address.addressType").setValue(h4Address.adrTypeDes)
      parent.form.get("customer.h4Address.addressLine1").setValue(h4Address.streetAdr1)
      parent.form.get("customer.h4Address.addressLine2").setValue(h4Address.streetAdr2)
      parent.form.get("customer.h4Address.addressLine3").setValue(h4Address.streetAdr3)
      //parent.form.get("customer.h4Address.suite").setValue("")
      parent.form.get("customer.h4Address.city").setValue(h4Address.ctyNme)
      parent.form.get("customer.h4Address.stateCode").setValue(h4Address.sttCd)
      parent.form.get("customer.h4Address.zipCode").setValue(h4Address.zipPstlCd)
      parent.form.get("customer.h4Address.countryCode").setValue(h4Address.ctryCd)
      parent.form.get("customer.h4Address.provinceMunicipality").setValue(h4Address.prvnNme)
    }

    // H4 Billing Contact
    let h4Contact = orderContact.find(a => a.cisLvlType == "H4" && a.cntctTypeId != 17 && a.fsaMdulId == 'CIS')
    if (h4Contact != null) {
      parent.form.get("customer.h4Contact.type").setValue(h4Contact.roleNme)
      parent.form.get("customer.h4Contact.contactType").setValue(h4Contact.cntctTypeDes)
      parent.form.get("customer.h4Contact.firstName").setValue(h4Contact.frstNme)
      parent.form.get("customer.h4Contact.lastName").setValue(h4Contact.lstNme)
      parent.form.get("customer.h4Contact.name").setValue(h4Contact.cntctNme)
      parent.form.get("customer.h4Contact.emailAddress").setValue(h4Contact.emailAdr)
      parent.form.get("customer.h4Contact.cityCode").setValue(h4Contact.ctyCd)
      parent.form.get("customer.h4Contact.npa").setValue(h4Contact.npa)
      parent.form.get("customer.h4Contact.nxx").setValue(h4Contact.nxx)
      parent.form.get("customer.h4Contact.station").setValue(h4Contact.stnNbr)
      parent.form.get("customer.h4Contact.phoneNumber").setValue(h4Contact.phnNbr)
      parent.form.get("customer.h4Contact.faxNumber").setValue(h4Contact.faxNbr)
    }

    // H6
    let h6Address = orderAddress.find(a => a.cisLvlType == "H6")
    //let h6Contact = orderContact.find(a => a.cisLvlType == "H6" && a.cntctNme == h6.custNme)
    if (h6 != null) {
      parent.form.get("customer.h6.customerId").setValue(h6.custId)
      parent.form.get("customer.h6.customerName").setValue(h6.custNme)
      parent.form.get("customer.h6.currentBillingCycleCode").setValue(h6.currBillCycCd)
      parent.form.get("customer.h6.futureBillingCycleCode").setValue(h6.futBillCycCd)
      parent.form.get("customer.h6.branchCode").setValue(h1.brCd)
      parent.form.get("customer.h6.taxExemption").setValue(h6.taxXmptdCd)
      parent.form.get("customer.h6.charsId").setValue(h6.charsCustId)
      parent.form.get("customer.h6.siteId").setValue(h6.siteId)
      parent.form.get("customer.h6.serviceSubType").setValue(h6.srvcSubTypeId)
      parent.form.get("customer.h6.salesOfficeIdCode").setValue(h6.soiCd)
      parent.form.get("customer.h6.salesPersonPrimaryCid").setValue(h6.salsPersnPrimCid)
      parent.form.get("customer.h6.salesPersonSecondaryCid").setValue(h6.salsPersnScndyId)
      parent.form.get("customer.h6.cllidCode").setValue(h6.clliCd)
    }
    if (h6Address != null) {
      parent.form.get("customer.h6.addressType").setValue(h6Address.adrTypeDes)
      parent.form.get("customer.h6.addressLine1").setValue(h6Address.streetAdr1)
      parent.form.get("customer.h6.addressLine2").setValue(h6Address.streetAdr2)
      parent.form.get("customer.h6.addressLine3").setValue(h6Address.streetAdr3)
      //parent.form.get("customer.h6.suite").setValue("")
      parent.form.get("customer.h6.city").setValue(h6Address.ctyNme)
      parent.form.get("customer.h6.stateCode").setValue(h6Address.sttCd)
      parent.form.get("customer.h6.zipCode").setValue(h6Address.zipPstlCd)
      parent.form.get("customer.h6.countryCode").setValue(h6Address.ctryCd)
      parent.form.get("customer.h6.provinceMunicipality").setValue(h6Address.prvnNme)
      parent.form.get("customer.h6.buildingName").setValue(h6Address.bldgNme)
      parent.form.get("customer.h6.floor").setValue(h6Address.flrId)
      parent.form.get("customer.h6.roomNumber").setValue(h6Address.rmNbr)

      if (parent.form.get("display") != null) {
        parent.form.get("display.h6AddressLine1").setValue(h6Address.streetAdr1)
        parent.form.get("display.h6City").setValue(h6Address.ctyNme)
        parent.form.get("display.h6Country").setValue(h6Address.ctryCd)
      }
    }
    let h6ContactAddress = orderAddress.find(a => a.cisLvlType == "H6" && a.hierLvlCd == 0)
    if (h6ContactAddress != null) {
      parent.form.get("customer.h6.addressType").setValue(h6ContactAddress.adrTypeDes)
      parent.form.get("customer.h6.addressLine1").setValue(h6ContactAddress.streetAdr1)
      parent.form.get("customer.h6.addressLine2").setValue(h6ContactAddress.streetAdr2)
      parent.form.get("customer.h6.addressLine3").setValue(h6ContactAddress.streetAdr3)
      //parent.form.get("customer.h6.suite").setValue("")
      parent.form.get("customer.h6.city").setValue(h6ContactAddress.ctyNme)
      parent.form.get("customer.h6.stateCode").setValue(h6ContactAddress.sttCd)
      parent.form.get("customer.h6.zipCode").setValue(h6ContactAddress.zipPstlCd)
      parent.form.get("customer.h6.countryCode").setValue(h6ContactAddress.ctryCd)
      parent.form.get("customer.h6.provinceMunicipality").setValue(h6ContactAddress.prvnNme)
      parent.form.get("customer.h6.buildingName").setValue(h6ContactAddress.bldgNme)
      parent.form.get("customer.h6.floor").setValue(h6ContactAddress.flrId)
      parent.form.get("customer.h6.roomNumber").setValue(h6ContactAddress.rmNbr)
    }
    if (h6Contact != null) {
      parent.form.get("customer.h6.npa").setValue(h6Contact.npa)
      parent.form.get("customer.h6.nxx").setValue(h6Contact.nxx)
      parent.form.get("customer.h6.station").setValue(h6Contact.stnNbr)
      parent.form.get("customer.h6.phoneNumber").setValue(h6Contact.phnNbr)
      parent.form.get("customer.h6.faxNumber").setValue(h6Contact.faxNbr)
      parent.form.get("customer.h6Contact.type").setValue(h6Contact.roleNme)
      parent.form.get("customer.h6Contact.contactType").setValue(h6Contact.cntctTypeDes)
      parent.form.get("customer.h6Contact.firstName").setValue(h6Contact.frstNme)
      parent.form.get("customer.h6Contact.lastName").setValue(h6Contact.lstNme)
      parent.form.get("customer.h6Contact.name").setValue(h6Contact.cntctNme)
      parent.form.get("customer.h6Contact.emailAddress").setValue(h6Contact.emailAdr)
      parent.form.get("customer.h6Contact.cityCode").setValue(h6Contact.ctyCd)
      parent.form.get("customer.h6Contact.npa").setValue(h6Contact.npa)
      parent.form.get("customer.h6Contact.nxx").setValue(h6Contact.nxx)
      parent.form.get("customer.h6Contact.station").setValue(h6Contact.stnNbr)
      parent.form.get("customer.h6Contact.phoneNumber").setValue(h6Contact.phnNbr)
      parent.form.get("customer.h6Contact.faxNumber").setValue(h6Contact.faxNbr)
    }

    // H6 Contact
    //if (order != null) {
    //  //let h6Contact2 = null
    //  //if (orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 92) != null) {
    //  //  h6Contact2 = orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 92)
    //  //} else if (orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 94) != null) {
    //  //  h6Contact2 = orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 94)
    //  //}
    //  //let h6Contact2 = orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 92)
    //  // let h6Contact3 = orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 94)
    //  if (h6Contact2 != null) {
    //    parent.form.get("customer.h6Contact.type").setValue(h6Contact2.roleNme)
    //    parent.form.get("customer.h6Contact.contactType").setValue(h6Contact2.cntctTypeDes)
    //    parent.form.get("customer.h6Contact.firstName").setValue(h6Contact2.frstNme)
    //    parent.form.get("customer.h6Contact.lastName").setValue(h6Contact2.lstNme)
    //    parent.form.get("customer.h6Contact.name").setValue(h6Contact2.cntctNme)
    //    parent.form.get("customer.h6Contact.emailAddress").setValue(h6Contact2.emailAdr)
    //    parent.form.get("customer.h6Contact.cityCode").setValue(h6Contact2.ctyCd)
    //    parent.form.get("customer.h6Contact.npa").setValue(h6Contact2.npa)
    //    parent.form.get("customer.h6Contact.nxx").setValue(h6Contact2.nxx)
    //    parent.form.get("customer.h6Contact.station").setValue(h6Contact2.stnNbr)
    //    parent.form.get("customer.h6Contact.phoneNumber").setValue(h6Contact2.phnNbr)
    //    parent.form.get("customer.h6Contact.faxNumber").setValue(h6Contact2.faxNbr)
    //  }
    //}
    // Install Information
    // MDS Installation Information
    if (order != null) {
      parent.form.get("install.mdsInstallationInformation.solutionService").setValue(order.instlSoluSrvcDes)
      parent.form.get("install.mdsInstallationInformation.networkTypeCode").setValue(order.instlNwTypeCd)
      parent.form.get("install.mdsInstallationInformation.serviceTierCode").setValue(order.instlSrvcTierCd)
      parent.form.get("install.mdsInstallationInformation.transportOrderTypeCode").setValue(order.instlTrnsprtTypeCd)
      parent.form.get("install.mdsInstallationInformation.designDocumentNo").setValue(order.instlDsgnDocNbr)
      parent.form.get("install.mdsInstallationInformation.vendorCode").setValue(order.instlVndrCd)


      if(parent.vendorOrder == null) {
        let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
        let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems
        const code = order.instlVndrCd ||  (acs || prt || new FsaOrderCpeLineItems({})).cxrAccsCd;
        const vendorName = parent.form.get("vendorOrderSearch.vendor").value

        let final = ""
        if (vendorName != null && code != null) {
          final = `${vendorName} - ${code}`;
        } else if (vendorName != null) {
          final = vendorName
        } else if (code != null) {
          final = code
        } else {
          final = ""
        }

        parent.form.get("vendorOrderSearch.vendor").setValue(final);
        parent.form.get("display.vendorName").setValue(final);
      }

    }
// m5 customer term
 if (order != null) {
        let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
        let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems
        parent.form.get("mileStoneInformation.customerContractTerm").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsTermDes || "")

}

    // Transport
    if (order != null) {
      let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
      let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems

      parent.form.get("install.mdsInstallationInformation.designDocumentNo").setValue((prt || acs || new FsaOrderCpeLineItems({})).instlDsgnDocNbr || "")

      parent.form.get("install.transport.accessTypeCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsTypeCd || "")
      parent.form.get("install.transport.accessArrangementCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsArngtCd || "")
      parent.form.get("install.transport.encapsulationCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptEncapCd || "")
      parent.form.get("install.transport.fiberHandoffCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptFbrHandOffCd || "")
      parent.form.get("install.transport.carrierAccessCode").setValue((acs || prt || new FsaOrderCpeLineItems({})).cxrAccsCd || "")
      parent.form.get("install.transport.circuitId").setValue(order.ttrptCktId)
      parent.form.get("install.transport.serviceTerm").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsTermDes || "")
      parent.form.get("install.transport.quoteExpirationDate").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptQotXpirnDt || null)
      parent.form.get("install.transport.accessBandwidth").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsBdwdType || "")
      parent.form.get("install.transport.privateLineNo").setValue((acs || prt || new FsaOrderCpeLineItems({})).plNbr || "")
      //parent.form.get("install.transport.m5AccessStatus").setValue("")
      parent.form.get("install.transport.classOfService").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptCosCd || "")
   
      if(parent.form.get("display") != null) {
        parent.form.get("display.accessBandwidth").setValue((acs || prt || new FsaOrderCpeLineItems({})).ttrptAccsBdwdType || "")
      }
    }

    // CPE Installation Information
    if (order != null) {
      parent.form.get("install.cpeInstallationInformation.cpeOrderTypeCode").setValue(order.cpeCpeOrdrTypeCd)
      parent.form.get("install.cpeInstallationInformation.equipmentOnlyFlagCode").setValue(order.cpeEqptOnlyCd)
      parent.form.get("install.cpeInstallationInformation.accessProvideCode").setValue(order.cpeAccsPrvdrCd)
      parent.form.get("install.cpeInstallationInformation.phoneNumberType").setValue(order.cpePhnNbrTypeCd)
      parent.form.get("install.cpeInstallationInformation.phoneNumber").setValue(order.cpePhnNbr)
      parent.form.get("install.cpeInstallationInformation.eccktIdentifier").setValue(order.cpeEccktId)
      parent.form.get("install.cpeInstallationInformation.managedServicesChannelProgramCode").setValue(order.cpeMscpCd)
      parent.form.get("install.cpeInstallationInformation.deliveryDuties").setValue(order.cpeDlvryDutyId)
      parent.form.get("install.cpeInstallationInformation.deliveryDutyAmount").setValue(order.cpeDlvryDutyAmt)
      parent.form.get("install.cpeInstallationInformation.shippingChargeAmount").setValue(order.cpeShipChgAmt)
      parent.form.get("install.cpeInstallationInformation.recordOnly").setValue(order.cpeRecOnlyCd)
      parent.form.get("install.cpeInstallationInformation.nuaOrCkt").setValue(order.ckt || order.nua || '')
    }

    // CPE Line Items
    parent.cpeLineItemList = cpeLineItemList

    // Port
    //console.log(order.fsaOrdrCpeLineItem)
    if (order != null) {
      let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
      let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems

      parent.installPortList = installPortList

      let spaAccsIndcrDes = (prt || acs || new FsaOrderCpeLineItems({})).spaAccsIndcrDes || ""
      let asrType = (spaAccsIndcrDes == "A" || spaAccsIndcrDes == "Aggregated") ? 1 :
        (spaAccsIndcrDes == "D" || spaAccsIndcrDes == "Dedicated") ? 2 : null
      if (parent.form.get("asr") != null) {
        parent.form.get("asr.asrType").setValue(asrType)
      }
    }

    // Vlan
    parent.installVlanList = orderVlanList

    // Change Information
    if (order != null) {
      let acs = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "ACS") as FsaOrderCpeLineItems
      let prt = order.fsaOrdrCpeLineItem.find(a => a.lineItemCd == "PRT") as FsaOrderCpeLineItems

      parent.form.get("install.changeInformation.changeDescription").setValue(order.ordrChngDes)
      parent.form.get("install.changeInformation.carrierServiceId").setValue(order.cxrSrvcId)
      parent.form.get("install.changeInformation.carrierCircuitId").setValue(order.cxrCktId)
      parent.form.get("install.changeInformation.originalInstallOrderId").setValue(order.fsaOrgnlInstlOrdrId)
      parent.form.get("install.changeInformation.networkUserAddress").setValue(order.nwUserAdr)
      parent.form.get("install.changeInformation.portRateTypeCode").setValue((prt || acs || new FsaOrderCpeLineItems({})).portRtTypeCd || "")
    }

    console.log(accountTeam)
    parent.orderContactList = accountTeam.filter(a => a.cisLvlType == HierLevel.OD)
    console.log(parent.orderContactList)
    //[{
    //  roleNme: "Implementation Project Manager",
    //  cntctTypeDes: "Primary",
    //  frstNme: "Ratnamala",
    //  lstNme: "Mallru",
    //  cntctNme: "Ratnamala Mallru",
    //  emailAdr: "ratnamala.mallru@sprint.com",
    //  isdCd: "",
    //  ctyCd: "",
    //  npa: "321",
    //  nxx: "456",
    //  stnNbr: "1111",
    //  phnNbr: "3214561111",
    //  faxNbr: "",
    //  phnExtNbr: ""
    //}]

    // Account Team
    parent.accountTeamList = accountTeam

    // Disconnect Information
    if (order != null) {
      parent.form.get("disconnect.reasonCode").setValue(order.discReasCd)
      parent.form.get("disconnect.cancelBeforeStartReasonCode").setValue(order.discCnclBefStrtReasCd)
      parent.form.get("disconnect.cancelBeforeStartReasonText").setValue(order.discCnclBefReasDtlTxt)
    }

    // Disconnect Contact
   // if (order != null) {
    //  let disconnectContact = null
     // if (order.orderCatId == 6 && orderContact.find(a => a.roleId == 99) != null) {
      //  disconnectContact = orderContact.find(a => a.roleId == 99)
     // } else if (orderContact.find(a => a.fsaMdulId == "DSC") != null) {
     //   disconnectContact = orderContact.find(a => a.fsaMdulId == "DSC")
     // }

    //  if (disconnectContact != null) {
     //  parent.form.get("customer.h6Contact.type").setValue(disconnectContact.roleNme)
     //   parent.form.get("customer.h6Contact.contactType").setValue(disconnectContact.cntctTypeDes)
     //   parent.form.get("customer.h6Contact.firstName").setValue(disconnectContact.frstNme)
     //   parent.form.get("customer.h6Contact.lastName").setValue(disconnectContact.lstNme)
     //   parent.form.get("customer.h6Contact.name").setValue(disconnectContact.cntctNme)
     //   parent.form.get("customer.h6Contact.emailAddress").setValue(disconnectContact.emailAdr)
     //   parent.form.get("customer.h6Contact.cityCode").setValue(disconnectContact.ctyCd)
     //   parent.form.get("customer.h6Contact.npa").setValue(disconnectContact.npa)
     //   parent.form.get("customer.h6Contact.nxx").setValue(disconnectContact.nxx)
     //   parent.form.get("customer.h6Contact.station").setValue(disconnectContact.stnNbr)
     //   parent.form.get("customer.h6Contact.phoneNumber").setValue(disconnectContact.phnNbr)
     //   parent.form.get("customer.h6Contact.faxNumber").setValue(disconnectContact.faxNbr)
    //  }
    // }


     if (order != null){
     let disconnectDetails1 = null
      if (orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 92) != null) {
        disconnectDetails1 = orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 92)
      } else if (orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 94) != null) {
        disconnectDetails1 = orderContact.find(a => a.cisLvlType == "H6" && a.cntctTypeId == 1 && a.roleId == 94)
      }
    if (disconnectDetails1 != null) {

      parent.form.get("disconnect.contact.type").setValue(disconnectDetails1.roleNme);
      parent.form.get("disconnect.contact.contactType").setValue(disconnectDetails1.cntctTypeDes);
      parent.form.get("disconnect.contact.firstName").setValue(disconnectDetails1.frstNme);
      parent.form.get("disconnect.contact.lastName").setValue(disconnectDetails1.lstNme);
      parent.form.get("disconnect.contact.name").setValue(disconnectDetails1.cntctNme);
      parent.form.get("disconnect.contact.emailAddress").setValue(disconnectDetails1.emailAdr);
      // parent.form.get("disconnect.countryCode").setValue();
      // parent.form.get("disconnect.cityCode").setValue();
      parent.form.get("disconnect.contact.npa").setValue(disconnectDetails1.npa);
      parent.form.get("disconnect.contact.nxx").setValue(disconnectDetails1.nxx);
      parent.form.get("disconnect.contact.station").setValue(disconnectDetails1.stnNbr);
      parent.form.get("disconnect.contact.phoneNumber").setValue(disconnectDetails1.phnNbr);
      // parent.form.get("disconnect.faxNumber").setValue();
      // parent.form.get("disconnect.extension").setValue();
      // ctyCd: ""
      // isdCd: ""
    }
}

    if (parent.fsaDetails != null) {
      parent.fsaDetails.show(orderId)
    }
  }
}
