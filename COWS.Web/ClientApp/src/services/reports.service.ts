import { Injectable } from "@angular/core";
import { ApiHelperService } from "../shared";
import { Observable } from "rxjs";
import { DashboardData, CannedReport } from "../models";

@Injectable({
  providedIn: "root"
})
export class ReportsService {
  constructor(private api: ApiHelperService) {}

  public getDashboard(): Observable<DashboardData> {
    return this.api.get("api/reports/dashboard");
  }
  public getCannedDaily(startDate?: string, endDate?: string): Observable<any> {
    let params = { reportType: "D", startDate: startDate, endDate: endDate }
    return this.api.post("api/reports/cannedreport", params);
  }

  public getCannedWeekly(startDate?: string, endDate?: string): Observable<any> {
    let params = { reportType: "W", startDate: startDate, endDate: endDate }
    return this.api.post("api/reports/cannedreport", params);
  }

  public getCannedMonthly(startDate?: string, endDate?: string): Observable<any> {
    let params = { reportType: "M", startDate: startDate, endDate: endDate }
    return this.api.post("api/reports/cannedreport", params);
  }

  public downloadCannedReportFile(file: any) {
    let params = { filePath: file.fullPath, fileName: file.name }
    return this.api.post("api/reports/DownloadCannedReportFile", params, { responseType: "blob" });
  }

  public reloadReports(reportId: number, reportType: string, startDate: string, endDate: string): Observable<any> {
    let params = { reportId: reportId, reportType: reportType, startDate: startDate, endDate: endDate }
    return this.api.post("api/reports/reloadReports", params);
  }
}
