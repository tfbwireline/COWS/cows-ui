import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class EventFailActyService {

  constructor(private api: ApiHelperService) { }

  public get(): any {
    return this.api.get('api/EventFailActy');
  }

  public getByFailActyId(id: number): any {
    return this.api.get('api/EventFailActy/' + id);
  }
  public getFailActyByEventHistId(id: number): any {
    return this.api.get('api/EventFailActy/GetbyHistId/' + id);
  }
  public getFailActyByEventId(id: number): any {
    return this.api.get('api/EventFailActy/GetbyEventId/' + id);
  }
}
