import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";

@Injectable({
  providedIn: 'root'
})

export class EventDeviceMgmtService {

  constructor(private api: ApiHelperService) { }

  public getByEventId(eventId: number): Observable<any> {
    return this.api.get('api/EventDevSrvcMgmt/Event/' + eventId);
  }
}
