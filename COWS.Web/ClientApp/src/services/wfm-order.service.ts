import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiHelperService } from "../shared/index";
//import { EventInterval } from "./../models";

@Injectable({
  providedIn: 'root'
})

export class WFMOrderService {

  constructor(private api: ApiHelperService) { }


  public GetWFMRole(userId: number, profile: string): Observable<any> {
    let params = {
      userId: userId,
      profile: profile
    }
    return this.api.get('api/WFMOrder/GetWFMRole', { params: params });
  }

  public GetWFMprofile(userId: number, profile: string): Observable<any> {
    let params = {
      userId: userId,
      profile: profile
    }
    return this.api.get('api/WFMOrder/GetWFMprofile', { params: params });
  }

  public GetNCIUser(userId: number): Observable<any> {
    let params = {
      userId: userId
    }
    return this.api.get('api/WFMOrder/GetNCIUser', { params: params });
  }

  public UserValid(userId: number, prodType: string, pltfrmType: string, subStatus: string): Observable<any> {
    let params = {
      userId: userId,
      prodType: prodType,
      pltfrmType: pltfrmType,
      subStatus: subStatus
    }
    return this.api.get('api/WFMOrder/UserValid', { params: params });
  }

  public Select(sortExpression: string, searchCriteria: string, view: number, profile: string): Observable<any> {
    let params = {
      sortExpression: sortExpression,
      searchCriteria: searchCriteria,
      view: view,
      profile: profile
    }
    return this.api.get('api/WFMOrder/Select', { params: params });
  }

  public GetWFMData(sortExpression: string, searchCriteria: string, view: number, profile: string): Observable<any> {
    let params = {
      sortExpression: sortExpression,
      searchCriteria: searchCriteria,
      view: view,
      profile: profile
    }
    return this.api.get('api/WFMOrder/GetWFMData', { params: params });
  }

  public MakeNCIAssignments(model): any {
    return this.api.post('api/WFMOrder/MakeNCIAssignments/', model);
  }

  public MakeNCIUnAssignments(model): any {
    return this.api.post('api/WFMOrder/MakeNCIUnAssignments/', model);
  }

  public MakeGOMAssignments(model): any {
    return this.api.post('api/WFMOrder/MakeGOMAssignments/', model);
  }

  public MakeGOMUnAssignments(model): any {
    return this.api.post('api/WFMOrder/MakeGOMUnAssignments/', model);
  }

  public InsertOrderNotes(orderID: number, noteTypeID: number, userID: number, notes: string): Observable<any> {
    let params = {
      orderID: orderID,
      noteTypeID: noteTypeID,
      userID: userID,
      notes: notes
    }
    return this.api.get('api/WFMOrder/InsertOrderNotes', { params: params });
  }

  public GetXNCIOrderWeightageNumber(model): any {
    return this.api.post('api/WFMOrder/GetXNCIOrderWeightageNumber/', model);
  }

  public assignClick(model): Observable<any> {
    return this.api.post('api/WFMOrder/AssignClick/', model);
  }

  public unAssignClick(model): Observable<any> {
    return this.api.post('api/WFMOrder/UnAssignClick/', model);
  }

  public transferClick(model): Observable<any> {
    return this.api.post('api/WFMOrder/TransferClick/', model);
  }
}
