import { Component, OnInit } from '@angular/core';
import { CptService } from '../../../services/index';
import { SpinnerAsync } from "./../../../shared";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cpt-report',
  templateUrl: './cpt-report.component.html'
})
export class CptReportComponent implements OnInit {
  gkList: any[] = [];
  managerList: any[] = [];
  nteList: any[] = [];
  pmList: any[] = [];
  crmList: any[] = [];
  cptList: any[] = [];
  spnr: SpinnerAsync;

  constructor(private cptSrvc: CptService, private spinner: NgxSpinnerService) { 
    this.spnr = new SpinnerAsync(spinner);
  }

  ngOnInit() {
    this.spnr.manageSpinner('show');
    // MDS Gatekeeper (212)
    this.cptSrvc.getAssignmentByProfileId(212).subscribe(res => {
      this.gkList = res.filter(i => i.poolCd);
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });
    this.spnr.manageSpinner('show');
    // MDS Manager (211)
    this.cptSrvc.getAssignmentByProfileId(211).subscribe(res => {
      this.managerList = res.filter(i => i.poolCd);
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });
    this.spnr.manageSpinner('show');
    // NTE / MDS Event Activator (130)
    this.cptSrvc.getAssignmentByProfileId(130).subscribe(res => {
      this.nteList = res.filter(i => i.poolCd);
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });
    this.spnr.manageSpinner('show');
    // PM / MDS Event Reviewer (132)
    this.cptSrvc.getAssignmentByProfileId(132).subscribe(res => {
      this.pmList = res.filter(i => i.poolCd);
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });
    this.spnr.manageSpinner('show');
    // MDS CRM (213)
    this.cptSrvc.getAssignmentByProfileId(213).subscribe(res => {
      this.crmList = res.filter(i => i.poolCd);
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });
  }

}
