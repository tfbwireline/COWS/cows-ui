import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, of } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { Helper } from "./../../../shared";
import { User, Cpt, CptRelatedInfo, CptPrimWan, CptDoc, CptProvision, DocEntity, ContactDetail } from '../../../models';
import {
  UserService, CptService, CptCustomerTypeService, CptHostManagedService, CptPlanSrvcTierService,
  CptPlanSrvcTypeService, CptPrimSecondaryTransportService, CptMdsSupportTierService,
  CptManagedAuthProdService, CptMvsProdTypeService, CptPrimSiteService, ContactDetailService
} from '../../../services';
import { CustomValidator } from '../../../shared/validator/custom-validator';
import {
  KeyValuePair, RB_CONDITION, Global, ECPTAssignmentProfileId, ECPTCustomerType,
  ECPTPlanServiceTier, ECPTStatus, ECPTPrimTransport, ECPTMdsSupportTier, ECPTHostedManagedSrvc, ContactDetailObjType, HierLevel, CPT_STATUS_EMAIL_CD
} from '../../../shared/global';
import { saveAs } from 'file-saver';
import DataSource from 'devextreme/data/data_source';
import { ContactListComponent } from '../../custom/contact-list/contact-list.component';
declare let $: any;

@Component({
  selector: 'app-cpt-form',
  templateUrl: './cpt-form.component.html'
})
export class CptFormComponent implements OnInit, OnDestroy {
  @ViewChild('cntctList', { static: false }) cntctList: ContactListComponent;
  id: number = 0;
  cptData: Cpt;
  h1CustData: any;
  form: FormGroup;
  option: any;
  errors: string[] = [];
  isSubmitted: boolean = false;
  sdwanCustOnly: number = 0;
  viewOnly: boolean = false;        // Used when user doesn't have profiles to create and edit CPT
  allowEdit: boolean = true;        // Used to allow edit based on CPT Status 
  editMode: boolean = false;        // Used to flag the form for edit
  isLocked: boolean = false;        // Used to lock the form
  lockedByUser: string;             // Used as a placeholder for locked by user adid
  allowCreate: boolean = false;     // Used for Profiles that doesn't allow Create/CPT Details Section
  allowProvision: boolean = false;  // Used for Profiles that doesn't allow Provisioning Section
  hasProvisioning: boolean = true;  // Used for CPT Types that doesn't have Provisioning Section
  allowAssignment: boolean = false; // Used for Profiles that doesn't allow Assignment Section
  hasAssignment: boolean = true;    // Used for CPT Types that doesn't have Assignment Section

  // For Field Disabled
  showIpAddress: boolean = false;
  disableGk: boolean = false;
  disableManager: boolean = false;
  disableNte: boolean = false;
  disablePm: boolean = false;
  disableCrm: boolean = false;
  showReviewProvisionError: boolean = false;

  // For Dropdown Values
  customerList: any[] = [];
  customerListDS: DataSource;
  customerTypeList: any[] = [];
  yesOrNoList: KeyValuePair[] = [];
  hostManagedList: any[] = [];
  plannedSrvcTiersList: any[] = [];
  plannedSrvcTypesList: any[] = [];
  primTransportList: KeyValuePair[] = [];
  mssPrimTransportList: CptPrimWan[] = [];
  mdsCmpltPrimTransportList: CptPrimWan[] = [];
  mdsSupportPrimTransportList: CptPrimWan[] = [];
  mdsSupportTiersList: any[] = [];
  mdsWholesaleCarrierPrimTransportList: CptPrimWan[] = [];
  mdsWholesaleVarPrimTransportList: CptPrimWan[] = [];
  mngdAuthenticationProdList: any[] = [];
  mvsProdTypesList: any[] = [];
  docList: DocEntity[] = [];
  primeSitesList: any[] = [];
  provisioningList: CptProvision[] = [];
  gkList: any[] = [];
  managerList: any[] = [];
  mssList: any[] = [];
  nteList: any[] = [];
  pmList: any[] = [];
  crmList: any[] = [];

  // Form Fields
  get customerTypes() { return this.form.get("customerTypes") }
  get plannedSrvcTiers() { return this.form.get('plannedSrvcTiers') }
  get status() { return this.form.get('status') }
  get seEmail() { return this.form.get("sprintSalesTeam.seEmail") }
  get amEmail() { return this.form.get("sprintSalesTeam.amEmail") }
  get ipmEmail() { return this.form.get("sprintSalesTeam.ipmEmail") }
  get customer() { return this.form.get("sprintSalesTeam.customer") }
  get h1() { return this.form.get("sprintSalesTeam.h1") }
  get noOfDevices() { return this.form.get("sprintSalesTeam.noOfDevices") }
  get contactEmails() { return this.form.get("sprintSalesTeam.contactEmails") }
  get company() { return this.form.get("customer.company") }
  get address() { return this.form.get("customer.address") }
  get city() { return this.form.get("customer.city") }
  get state() { return this.form.get("customer.state") }
  get country() { return this.form.get("customer.country") }
  get zip() { return this.form.get("customer.zip") }
  get isSprintWholesaleReseller() { return this.form.get("customer.isSprintWholesaleReseller") }
  get wholesaleResellerName() { return this.form.get("customer.wholesaleResellerName") }
  get isGovernmentCustomer() { return this.form.get("customer.isGovernmentCustomer") }
  get isSecurityClearance() { return this.form.get("customer.isSecurityClearance") }
  get isTacacs() { return this.form.get("requiredInfo.isTacacs") }
  get isEncryptedModems() { return this.form.get("requiredInfo.isEncryptedModems") }
  get isHostedManagedService() { return this.form.get("requiredInfo.isHostedManagedService") }
  get isSdwanOrMeraki() { return this.form.get("requiredInfo.isSdwanOrMeraki") }
  get mdsMssNddUrl() { return this.form.get('mdsMss.networkDesignDocUrl') }
  get isSdwanMerakiOnlyCustomer() { return this.form.get('mdsComplete.isSdwanMerakiOnlyCustomer') }
  get mdsCmpltNddUrl() { return this.form.get('mdsComplete.networkDesignDocUrl') }
  get mdsSupportNddUrl() { return this.form.get('mdsSupport.networkDesignDocUrl') }
  get mdsSupportTiers() { return this.form.get('mdsSupport.mdsSupportTiers') }
  get mdsWholesaleCarrierNddUrl() { return this.form.get('mdsWholesaleCarrier.networkDesignDocUrl') }
  get mdsWholesaleVarNddUrl() { return this.form.get('mdsWholesaleVar.networkDesignDocUrl') }
  get plannedSrvcTypes() { return this.form.get('mss.plannedSrvcTypes') }
  get mngdAuthenticationProd() { return this.form.get('mss.mngdAuthenticationProd') }
  get mvsProdTypes() { return this.form.get('mvs.mvsProdTypes') }
  get e2eNddUrl() { return this.form.get('e2e.networkDesignDocUrl') }
  get spsNddUrl() { return this.form.get('sps.networkDesignDocUrl') }
  get provisioning() { return this.form.get('provisioning') }
  get shortName() { return this.form.get('provisioning.shortName') }
  get primeSite() { return this.form.get('provisioning.primeSite') }
  get ipSubnet() { return this.form.get('provisioning.ipSubnet') }
  get startIp() { return this.form.get('provisioning.startIp') }
  get endIp() { return this.form.get('provisioning.endIp') }

  // Contact Details
  objType: string;
  additionalEmails: string[] = [];
  contactList: ContactDetail[] = [];

  constructor(private avRoute: ActivatedRoute, private router: Router,
    private spinner: NgxSpinnerService, private helper: Helper,
    private userSrvc: UserService, private cptSrvc: CptService,
    private cptCustTypeSrvc: CptCustomerTypeService,
    private cptHostManagedSrvc: CptHostManagedService,
    private cptPlanSrvcTierSrvc: CptPlanSrvcTierService,
    private cptPrimSecTrnsprtSrvc: CptPrimSecondaryTransportService,
    private cptMdsSupportTierSrvc: CptMdsSupportTierService,
    private cptPlanSrvcTypeSrvc: CptPlanSrvcTypeService,
    private cptMangdAuthProdSrvc: CptManagedAuthProdService,
    private cptMvsProdTypeSrvc: CptMvsProdTypeService,
    private cptPrimSiteSrvc: CptPrimSiteService,
    private cntctDtlSrvc: ContactDetailService) { }

/** Clear the form and remove unlock when the user navigate away from the page. **/
  ngOnDestroy() {
    this.helper.form = null;
    //this.unlock();
  }

/** Load form fields and details as needed on page initial load. **/
  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"] || 0;
    this.editMode = this.id > 0 ? false : true;
    this.objType = ContactDetailObjType.CPT;
    this.form = new FormGroup({
      customerTypes: new FormControl(null, Validators.compose([Validators.required, CustomValidator.cptCustTypeValidator])),
      plannedSrvcTiers: new FormControl(null, Validators.required),
      cancelCpt: new FormControl(false),
      notes: new FormControl(null),
      status: new FormControl(null),
      requestor: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        email: new FormControl(),
        phone: new FormControl(),
      }),
      sprintSalesTeam: new FormGroup({
        seEmail: new FormControl(null, Validators.compose([Validators.required, CustomValidator.emailValidator])),
        amEmail: new FormControl(null, Validators.compose([Validators.required, CustomValidator.emailValidator])),
        ipmEmail: new FormControl(null, Validators.compose([Validators.required, CustomValidator.emailValidator])),
        customer: new FormControl(null),
        h1: new FormControl(null, Validators.compose([Validators.required, CustomValidator.h1Validator])),
        noOfDevices: new FormControl(null, Validators.compose([Validators.required])),
        contactEmails: new FormControl(),
      }),
      customer: new FormGroup({
        company: new FormControl(null, Validators.required),
        address: new FormControl(null, Validators.required),
        bldgRmFlr: new FormControl(null),
        city: new FormControl(null, Validators.required),
        state: new FormControl(null, Validators.required),
        country: new FormControl(null, Validators.required),
        zip: new FormControl(null, Validators.required),
        isSprintWholesaleReseller: new FormControl(null, Validators.required),
        wholesaleResellerName: new FormControl(null, Validators.required),
        isGovernmentCustomer: new FormControl(null, Validators.required),
        isSecurityClearance: new FormControl(null, Validators.required)
      }),
      requiredInfo: new FormGroup({
        isTacacs: new FormControl(null, Validators.required),
        isEncryptedModems: new FormControl(null, Validators.required),
        isHostedManagedService: new FormControl(null, Validators.required),
        isSdwanOrMeraki: new FormControl(null, Validators.required),
        hstMngdSrvc: new FormControl(null)
      }),
      mdsMss: new FormGroup({
        secTrnsprtTypes: new FormControl(null),
        isHostedUC: new FormControl(null, Validators.required),
        isCubes: new FormControl(null, Validators.required),
        networkDesignDocUrl: new FormControl(null),
        comments: new FormControl(null),
      }),
      mdsComplete: new FormGroup({
        secTrnsprtTypes: new FormControl(null),
        isHostedUC: new FormControl(null, Validators.required),
        isCubes: new FormControl(null, Validators.required),
        isSdwanMerakiOnlyCustomer: new FormControl(null, Validators.required),
        networkDesignDocUrl: new FormControl(null),
        comments: new FormControl(null),
      }),
      mdsSupport: new FormGroup({
        secTrnsprtTypes: new FormControl(null),
        mdsSupportTiers: new FormControl(null, Validators.required),
        isSupplementalEthernetSrvc: new FormControl(null, Validators.required),
        networkDesignDocUrl: new FormControl(null),
        comments: new FormControl(null),
      }),
      mdsWholesaleCarrier: new FormGroup({
        secTrnsprtTypes: new FormControl(null),
        networkDesignDocUrl: new FormControl(null),
        comments: new FormControl(null),
      }),
      mdsWholesaleVar: new FormGroup({
        secTrnsprtTypes: new FormControl(null),
        networkDesignDocUrl: new FormControl(null),
        comments: new FormControl(null),
      }),
      mss: new FormGroup({
        plannedSrvcTypes: new FormControl(null, Validators.required),
        mngdAuthenticationProd: new FormControl(null, Validators.required),
        comments: new FormControl()
      }),
      mvs: new FormGroup({
        isUCDevices: new FormControl(null, Validators.required),
        mvsProdTypes: new FormControl(null, Validators.required),
        comments: new FormControl()
      }),
      e2e: new FormGroup({
        isNddToDocshare: new FormControl(null, Validators.required),
        networkDesignDocUrl: new FormControl(null),
        comments: new FormControl(null)
      }),
      sps: new FormGroup({
        isSpsSowToDocshare: new FormControl(null, Validators.required),
        networkDesignDocUrl: new FormControl(null),
        comments: new FormControl()
      }),
      provisioning: new FormGroup({
        shortName: new FormControl(null),
        primeSite: new FormControl({ value: null, disabled: true }),
        ipSubnet: new FormControl(null, Validators.compose([CustomValidator.ipValidator])),
        startIp: new FormControl(null, Validators.compose([CustomValidator.ipValidator])),
        endIp: new FormControl(null, Validators.compose([CustomValidator.ipValidator])),
        qipSubnet: new FormControl({ value: null, disabled: true }),
        preSharedKey: new FormControl(null),
      }),
      assignment: new FormGroup({
        gatekeeper: new FormControl(null),
        manager: new FormControl(null),
        mss: new FormControl(null),
        nte: new FormControl(null),
        pm: new FormControl(null),
        crm: new FormControl(null),
        reviewedByPm: new FormControl(false),
        returnedToSde: new FormControl(false)
      })
    });
    this.option = {
      requestor: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["email", "emailAdr"],
            ["phone", "phnNbr"]
          ]
        },
        name: {
          isShown: true
        },
        email: {
          isShown: true
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: false
        },
        pager: {
          isShown: false
        },
        pagerPin: {
          isShown: false
        },
        bridgeNumber: {
          isShown: false
        },
        bridgePin: {
          isShown: false
        }
      },
      sprintSalesTeam: {
        userFinderForSeEmail: {
          isEnabled: true,
          searchQuery: "seEmail",
          mappings: [
            ["seEmail", "emailAdr"]
          ]
        },
        userFinderForAmEmail: {
          isEnabled: true,
          searchQuery: "amEmail",
          mappings: [
            ["amEmail", "emailAdr"]
          ]
        },
        userFinderForIpmEmail: {
          isEnabled: true,
          searchQuery: "ipmEmail",
          mappings: [
            ["ipmEmail", "emailAdr"]
          ]
        },
        userFinderForContactEmailAddresses: {
          isEnabled: true,
          isMultiple: true,
          searchQuery: "contactEmails",
          mappings: [
            ["contactEmails", "emailAdr"]
          ]
        }
      },
      documentEntity: {
        inputFileControl: {
          inputFileField: {
            label: "Upload Documents",
            isShown: true
          }
        },
        grid: {
          column: {
            fileName: {
              caption: "Name"
            },
            fileSize: {
              byte: true
            },
            date: {
              caption: "Attached Date"
            },
            comment: {
              isShown: true
            }
          }
        }
      }
    }

    // Set the helper form to this form
    this.helper.form = this.form;
    //this.getUserAccess();
    this.setInitialData();
  }

/** Get user access and make page restriction as necessary. **/
  getUserAccess() {
    let profiles = this.userSrvc.loggedInUser.profiles;
    if (this.id > 0) {
      // Access on Update CPT
      if (profiles.includes('Sales Support Sales Design Engineer')
        || profiles.includes('MDS Admin') || profiles.includes('MDS CPT GateKeeper')
        || profiles.includes('MDS CPT Manager')) {
        this.allowCreate = true; // Allow update on CPT details (upper part of the form)
      }
      if (profiles.includes('MDS Admin') || profiles.includes('MDS CPT NOS')) {
        this.allowProvision = true;
      }
      if (profiles.includes('MDS Admin')|| profiles.includes('MDS CPT GateKeeper')
        || profiles.includes('MDS CPT Manager') || profiles.includes('MDS Event Reviewer')) {
        this.allowAssignment = true;
      }

      this.viewOnly = !this.allowCreate && !this.allowProvision && !this.allowAssignment;
    }
    else {
      // Access on Create CPT
      if (profiles.includes('Sales Support Sales Design Engineer')
        || profiles.includes('MDS Admin') || profiles.includes('MDS CPT Manager')) {
        this.allowCreate = true;
      }
      else {
        this.router.navigate(['/sorry']);
      }
    }
  }

/** Load all dropdown related values on the form. **/
  setInitialData() {
    this.spinner.show();
    this.yesOrNoList = this.helper.getEnumKeyValuePair(RB_CONDITION);
    let data = zip(
      this.userSrvc.getLoggedInUser(),
      this.cptSrvc.getCustomerH1Data('', ''),
      this.cptCustTypeSrvc.get(),
      this.cptHostManagedSrvc.get(),
      this.cptPlanSrvcTierSrvc.get(),
      this.cptPlanSrvcTypeSrvc.get(),
      this.cptMangdAuthProdSrvc.get(),
      this.cptMvsProdTypeSrvc.get(),
      this.cptPrimSecTrnsprtSrvc.getForControl(),
      this.cptMdsSupportTierSrvc.get(),
      this.cptPrimSiteSrvc.get()
    )

    data.toPromise().then(
      res => {
        let user = res[0] as User;
        if (this.id == 0) {
          this.form.get("requestor.id").setValue(user.userId);
          this.form.get("requestor.name").setValue(user.fullNme);
          this.form.get("requestor.email").setValue(user.emailAdr);
          this.form.get("requestor.phone").setValue(user.phnNbr);
        }

        this.form.get("requestor").disable();
        this.option.requestor.userFinder.isEnabled = false;

        this.customerList = res[1];
        this.customerListDS = new DataSource({
          store: {
            data: this.customerList,
            type: 'array',
            key: 'h1'
          }
        });
        this.customerTypeList = res[2];
        this.hostManagedList = res[3];
        this.plannedSrvcTiersList = res[4];
        this.plannedSrvcTypesList = res[5];
        this.mngdAuthenticationProdList = res[6];
        this.mvsProdTypesList = res[7];
        this.primTransportList = res[8] as KeyValuePair[];
        this.mdsSupportTiersList = res[9];
        this.primeSitesList = res[10];
      },
      error => {
        this.spinner.hide();
        this.helper.notify('Failed to load initial CPT Data', Global.NOTIFY_TYPE_ERROR);
      }).then(() => {
        this.getUserAccess();
        if (this.id > 0) {
          this.setCPTFormData();
        }
        else {
          this.spinner.hide();
        }
      });
  }

/** Set CPT details on form fields. **/
  setCPTFormData() {
    let data = zip(
      this.cptSrvc.checkLock(this.id),
      this.cptSrvc.getById(this.id).pipe(
        concatMap(
          res => zip(
            of(res as Cpt),
            this.cptSrvc.getAssignmentByProfileId(ECPTAssignmentProfileId.Gatekeeper), // For MDS GK
            this.cptSrvc.getAssignmentByProfileId(ECPTAssignmentProfileId.Manager), // For MDS Manager
            this.cptSrvc.getAssignmentByProfileId(ECPTAssignmentProfileId.MSS), // For MDS SSE
            this.cptSrvc.getAssignmentByProfileId(ECPTAssignmentProfileId.NTE), // For NTE / MDS Event Activator
            this.cptSrvc.getAssignmentByProfileId(ECPTAssignmentProfileId.PM), // For PM / MDS Event Reviewer
            this.cptSrvc.getAssignmentByProfileId(ECPTAssignmentProfileId.CRM), // For MDS CRM
            this.cntctDtlSrvc.getContactDetails(this.id, this.objType, '', ''),
          )
        )
      )
    );

    data.toPromise().then(
      res => {
        let lockUser = res[0] as string;
        if (!this.helper.isEmpty(lockUser)) {
          this.lockedByUser = lockUser;
          this.isLocked = true;
          this.disableForm();
        }

        this.cptData = res[1][0] as Cpt;
        console.log(this.cptData)
        this.gkList = res[1][1];
        this.managerList = res[1][2];
        this.mssList = res[1][3];
        this.nteList = res[1][4];
        this.pmList = res[1][5];
        this.crmList = res[1][6];
        this.contactList = res[1][7] as ContactDetail[];
        // Construct Email Code Text on Update
        this.contactList.forEach(item => {
          if (!this.helper.isEmpty(item.emailCd)) {
            const statusText: string[] = [];
            item.emailCdIds = item.emailCd.split(',').map(Number);
            item.emailCdIds.forEach(row => {
              statusText.push(CPT_STATUS_EMAIL_CD[row])
            });
            item.emailCdTxt = statusText.join(',');
          }

          if (item.id != 0 && (item.creatByUserId > 1 && item.creatByUserId != this.userSrvc.loggedInUser.userId)) {
            item.allowEdit = false;
          } else {
            item.allowEdit = true;
          }
        });

        console.log(this.contactList)

        // Submitter
        this.setFormControlValue("requestor.id", this.cptData.submtrUserId);
        this.setFormControlValue("requestor.name", this.cptData.submitterName);
        this.setFormControlValue("requestor.email", this.cptData.submitterEmail);
        this.setFormControlValue("requestor.phone", this.cptData.submitterPhone);

        // Sprint Sales Team
        this.seEmail.setValue(this.cptData.seEmailAdr);
        this.ipmEmail.setValue(this.cptData.ipmEmailAdr);
        this.amEmail.setValue(this.cptData.amEmailAdr);
        this.customer.setValue(this.cptData.h1);
        this.h1.setValue(this.cptData.h1);
        this.noOfDevices.setValue(this.cptData.devCnt);

        // Additional Information
        this.customerTypes.setValue(this.cptData.customerTypes);

        // Customer
        this.company.setValue(this.cptData.compnyNme);
        this.address.setValue(this.cptData.custAdr);
        this.setFormControlValue('customer.bldgRmFlr', this.cptData.custFlrBldgNme);
        this.city.setValue(this.cptData.custCtyNme);
        this.state.setValue(this.cptData.custSttPrvnNme);
        this.country.setValue(this.cptData.custCtryRgnNme);
        this.zip.setValue(this.cptData.custZipCd);
        this.isSprintWholesaleReseller.setValue(this.cptData.sprntWhlslReslrCd ? 1 : 0);
        this.wholesaleResellerName.setValue(this.cptData.sprntWhlslReslrNme);
        //this.company.setValue(this.cptData.sprntWhlslReslrCd ? this.wholesaleResellerName.value + ' - ' + this.cptData.compnyNme : this.cptData.compnyNme);
        this.isGovernmentCustomer.setValue(this.cptData.govtCustCd ? 1 : 0);
        if (this.cptData.govtCustCd) {
          this.isSecurityClearance.setValue(this.cptData.scrtyClrnceCd ? 1 : 0);
        }

        // Required Information
        this.isTacacs.setValue(this.cptData.tacacsCd ? 1 : 0);
        this.isEncryptedModems.setValue(this.cptData.encMdmCd ? 1 : 0);
        this.isHostedManagedService.setValue(this.cptData.hstMgndSrvcCd ? 1 : 0);
        if (this.cptData.hstMgndSrvcCd) {
          this.setFormControlValue('requiredInfo.hstMngdSrvc', this.cptData.hostedManagedServices);
        }
        this.isSdwanOrMeraki.setValue(this.cptData.sdwanCd ? 1 : 0);

        // Managed Data
        this.plannedSrvcTiers.setValue(this.cptData.mdsPlnSrvcTiers.filter(i => i > 0));
        if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)) {
          if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MssSolution)) {
            let info = this.cptData.relatedInfos.find(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.MssSolution);
            this.mssPrimTransportList = info.cptPrimWan;
            this.setFormControlValue('mdsMss.secTrnsprtTypes', info.secTrnsprtTypes);
            this.setFormControlValue('mdsMss.isHostedUC', info.hstdUcCd ? 1 : 0);
            this.setFormControlValue('mdsMss.isCubes', info.cubeSiptSmiCd ? 1 : 0);
            this.setFormControlValue('mdsMss.networkDesignDocUrl', info.ntwkDsgnDocTxt);
            this.setFormControlValue('mdsMss.comments', info.cmntTxt);
          }
          if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsComplete)) {
            let info = this.cptData.relatedInfos.find(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.MdsComplete);
            this.mdsCmpltPrimTransportList = info.cptPrimWan;
            this.setFormControlValue('mdsComplete.secTrnsprtTypes', info.secTrnsprtTypes);
            this.setFormControlValue('mdsComplete.isHostedUC', info.hstdUcCd ? 1 : 0);
            this.setFormControlValue('mdsComplete.isCubes', info.cubeSiptSmiCd ? 1 : 0);
            this.sdwanCustOnly = info.sdwanCustCd ? 0 : 1; // Index in YerOrNoList
            this.setFormControlValue('mdsComplete.isSdwanMerakiOnlyCustomer', info.sdwanCustCd ? 1 : 0);
            this.setFormControlValue('mdsComplete.networkDesignDocUrl', info.ntwkDsgnDocTxt);
            this.setFormControlValue('mdsComplete.comments', info.cmntTxt);
          }
          if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsSupport)) {
            let info = this.cptData.relatedInfos.find(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.MdsSupport);
            this.mdsSupportPrimTransportList = info.cptPrimWan;
            this.setFormControlValue('mdsSupport.secTrnsprtTypes', info.secTrnsprtTypes);
            this.setFormControlValue('mdsSupport.mdsSupportTiers', this.cptData.mdsSupportTiers);
            this.setFormControlValue('mdsSupport.isSupplementalEthernetSrvc', info.suplmntlEthrntCd ? 1 : 0);
            this.setFormControlValue('mdsSupport.networkDesignDocUrl', info.ntwkDsgnDocTxt);
            this.setFormControlValue('mdsSupport.comments', info.cmntTxt);
          }
          if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleCarrier)) {
            let info = this.cptData.relatedInfos.find(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.WholesaleCarrier);
            this.mdsWholesaleCarrierPrimTransportList = info.cptPrimWan;
            this.setFormControlValue('mdsWholesaleCarrier.secTrnsprtTypes', info.secTrnsprtTypes);
            this.setFormControlValue('mdsWholesaleCarrier.networkDesignDocUrl', info.ntwkDsgnDocTxt);
            this.setFormControlValue('mdsWholesaleCarrier.comments', info.cmntTxt);
          }
          if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleVar)) {
            let info = this.cptData.relatedInfos.find(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.WholesaleVar);
            this.mdsWholesaleVarPrimTransportList = info.cptPrimWan;
            this.setFormControlValue('mdsWholesaleVar.secTrnsprtTypes', info.secTrnsprtTypes);
            this.setFormControlValue('mdsWholesaleVar.networkDesignDocUrl', info.ntwkDsgnDocTxt);
            this.setFormControlValue('mdsWholesaleVar.comments', info.cmntTxt);
          }
        }

        // Managed Security
        if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedSecurity)) {
          this.plannedSrvcTypes.setValue(this.cptData.mssPlnSrvcTypes);
          if (this.cptData.mssPlnSrvcTypes.includes(3)) {
            this.mngdAuthenticationProd.setValue(this.cptData.mssMngdAuthenticationProds);
          }
          this.setFormControlValue('mss.comments', this.cptData.relatedInfos.find(i => i.cptCustTypeId == ECPTCustomerType.ManagedSecurity).cmntTxt);
        }

        // Managed Voice
        if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedVoice)) {
          this.setFormControlValue('mvs.isUCDevices', this.cptData.relatedInfos.find(i => i.cptCustTypeId == ECPTCustomerType.ManagedVoice).custUcdCd ? 1 : 0);
          this.mvsProdTypes.setValue(this.cptData.mvsProdTypes);
          if (this.cptData.cptDocs != null && this.cptData.cptDocs.length > 0) {
            this.docList = this.cptData.cptDocs.map(item => {
              return new DocEntity({
                docId: item.cptDocId,
                id: item.cptId,
                fileNme: item.fileNme,
                fileCntnt: item.fileCntnt,
                fileSizeQty: item.fileSizeQty,
                cmntTxt: item.cmntTxt,
                creatDt: item.creatDt,
                creatByUserId: item.creatByUserId,
                creatByUserAdId: item.creatByAdId,
                base64string: item.base64string
              })
            })
          }
        }

        // E2E
        if (this.cptData.customerTypes.includes(ECPTCustomerType.UnmanagedE2E)) {
          this.setFormControlValue('e2e.isNddToDocshare', this.cptData.relatedInfos.find(i => i.cptCustTypeId == ECPTCustomerType.UnmanagedE2E).e2eNddCd ? 1 : 0);
          this.setFormControlValue('e2e.networkDesignDocUrl', this.cptData.relatedInfos.find(i => i.cptCustTypeId == ECPTCustomerType.UnmanagedE2E).ntwkDsgnDocTxt);
          this.setFormControlValue('e2e.comments', this.cptData.relatedInfos.find(i => i.cptCustTypeId == ECPTCustomerType.UnmanagedE2E).cmntTxt);
        }

        // SPS
        if (this.cptData.customerTypes.includes(ECPTCustomerType.Sps)) {
          this.setFormControlValue('sps.isSpsSowToDocshare', this.cptData.relatedInfos.find(i => i.cptCustTypeId == ECPTCustomerType.Sps).spsSowCd ? 1 : 0);
          this.setFormControlValue('sps.networkDesignDocUrl', this.cptData.relatedInfos.find(i => i.cptCustTypeId == ECPTCustomerType.Sps).ntwkDsgnDocTxt);
          this.setFormControlValue('sps.comments', this.cptData.relatedInfos.find(i => i.cptCustTypeId == ECPTCustomerType.Sps).cmntTxt);
        }

        // km967761 - 09/03/2021 - added to set Contact Email Addresses
        this.setFormControlValue("sprintSalesTeam.contactEmails", this.cptData.relatedInfos[0].cntctEmailList)

        // Provisioning
        this.setFormControlValue('provisioning.shortName', this.cptData.custShrtNme);
        this.setFormControlValue('provisioning.primeSite', this.cptData.primSites);
        this.setFormControlValue('provisioning.ipSubnet', this.cptData.subnetIpAdr);
        this.setFormControlValue('provisioning.startIp', this.cptData.startIpAdr);
        this.setFormControlValue('provisioning.endIp', this.cptData.endIpAdr);
        this.setFormControlValue('provisioning.qipSubnet', this.cptData.qipSubnetNme);
        this.setFormControlValue('provisioning.preSharedKey', this.cptData.preShrdKey);
        this.provisioningList = this.cptData.cptPrvsn;

        // Updated by Sarah Sandoval [20200805]
        // (1) ShowIPAddress if TACACS is true
        // (2) Primary Transport Types is not equal to IpSprintLink and WirelessWan and Ethernet Carrier
        this.showIpAddress = this.cptData.tacacsCd
          //|| (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)
          //  && !this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleCarrier))
          || ((this.mssPrimTransportList.findIndex(i =>
            i.cptPrimScndyTprtId != ECPTPrimTransport.IpSprintLink
            && i.cptPrimScndyTprtId != ECPTPrimTransport.WirelessWan
            && i.cptPrimScndyTprtId != ECPTPrimTransport.CarrierEthernet) > -1)
            || (this.mdsCmpltPrimTransportList.findIndex(i =>
              i.cptPrimScndyTprtId != ECPTPrimTransport.IpSprintLink
              && i.cptPrimScndyTprtId != ECPTPrimTransport.WirelessWan
              && i.cptPrimScndyTprtId != ECPTPrimTransport.CarrierEthernet) > -1)
            || (this.mdsSupportPrimTransportList.findIndex(i =>
              i.cptPrimScndyTprtId != ECPTPrimTransport.IpSprintLink
              && i.cptPrimScndyTprtId != ECPTPrimTransport.WirelessWan
              && i.cptPrimScndyTprtId != ECPTPrimTransport.CarrierEthernet) > -1)
            || (this.mdsWholesaleVarPrimTransportList.findIndex(i =>
              i.cptPrimScndyTprtId != ECPTPrimTransport.IpSprintLink
              && i.cptPrimScndyTprtId != ECPTPrimTransport.WirelessWan
              && i.cptPrimScndyTprtId != ECPTPrimTransport.CarrierEthernet) > -1));

        // (3) Don't show IP Address if SDWAN Only Customer is true
        if (this.cptData.relatedInfos.findIndex(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.MdsComplete) != -1
          && this.cptData.relatedInfos.find(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.MdsComplete).sdwanCustCd)
          this.showIpAddress = false;

        // Use this for testing showIpAddress
        //console.log('showIpAddress - tacacs: ' + this.cptData.tacacsCd)
        //console.log('showIpAddress - not WCarrier: ' + (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)
        //  && !this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleCarrier)))
        //console.log('mss: ' + (this.mssPrimTransportList.findIndex(i => i.cptPrimScndyTprtId != ECPTPrimTransport.IpSprintLink
        //  && i.cptPrimScndyTprtId != ECPTPrimTransport.WirelessWan) > -1))
        //console.log('mdsComplete: ' + (this.mdsCmpltPrimTransportList.findIndex(i => i.cptPrimScndyTprtId != ECPTPrimTransport.IpSprintLink
        //  && i.cptPrimScndyTprtId != ECPTPrimTransport.WirelessWan) > -1))
        //console.log('mdsSupport: ' + (this.mdsSupportPrimTransportList.findIndex(i => i.cptPrimScndyTprtId != ECPTPrimTransport.IpSprintLink
        //  && i.cptPrimScndyTprtId != ECPTPrimTransport.WirelessWan) > -1))
        //console.log('wVar: ' + (this.mdsWholesaleVarPrimTransportList.findIndex(i => i.cptPrimScndyTprtId != ECPTPrimTransport.IpSprintLink
        //  && i.cptPrimScndyTprtId != ECPTPrimTransport.WirelessWan) > -1))
        //console.log('showIpAddress - not sdwanCustCd: ' + (this.cptData.relatedInfos.findIndex(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.MdsComplete) != -1
        //  && !this.cptData.relatedInfos.find(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.MdsComplete).sdwanCustCd))

        // Add user on dropdown if not existing
        if (!this.helper.isEmpty(this.cptData.cptUserAsmt)) {
          if (!this.helper.isEmpty(this.cptData.gatekeeper)
            && this.gkList.findIndex(i => i.userId == this.cptData.gatekeeper) == -1) {
            var usr = this.cptData.cptUserAsmt.find(i => i.cptUserId == this.cptData.gatekeeper
              && i.usrPrfId == ECPTAssignmentProfileId.Gatekeeper);
            if (!this.helper.isEmpty(usr)) {
              this.gkList.push({ userId: usr.cptUserId, fullName: usr.cptUser })
            }
          }

          if (!this.helper.isEmpty(this.cptData.manager)
            && this.managerList.findIndex(i => i.userId == this.cptData.manager) == -1) {
            var usr = this.cptData.cptUserAsmt.find(i => i.cptUserId == this.cptData.manager
              && i.usrPrfId == ECPTAssignmentProfileId.Manager);
            if (!this.helper.isEmpty(usr)) {
              this.managerList.push({ userId: usr.cptUserId, fullName: usr.cptUser })
            }
          }

          if (!this.helper.isEmpty(this.cptData.mss)
            && this.mssList.findIndex(i => i.userId == this.cptData.mss) == -1) {
            var usr = this.cptData.cptUserAsmt.find(i => i.cptUserId == this.cptData.mss
              && i.usrPrfId == ECPTAssignmentProfileId.MSS);
            if (!this.helper.isEmpty(usr)) {
              this.mssList.push({ userId: usr.cptUserId, fullName: usr.cptUser })
            }
          }

          if (!this.helper.isEmpty(this.cptData.nte)
            && this.nteList.findIndex(i => i.userId == this.cptData.nte) == -1) {
            var usr = this.cptData.cptUserAsmt.find(i => i.cptUserId == this.cptData.nte
              && i.usrPrfId == ECPTAssignmentProfileId.NTE);
            if (!this.helper.isEmpty(usr)) {
              this.nteList.push({ userId: usr.cptUserId, fullName: usr.cptUser, displayField: usr.cptUser })
            }
          }

          if (!this.helper.isEmpty(this.cptData.pm)
            && this.pmList.findIndex(i => i.userId == this.cptData.pm) == -1) {
            var usr = this.cptData.cptUserAsmt.find(i => i.cptUserId == this.cptData.pm
              && i.usrPrfId == ECPTAssignmentProfileId.PM);
            if (!this.helper.isEmpty(usr)) {
              this.pmList.push({ userId: usr.cptUserId, fullName: usr.cptUser })
            }
          }

          if (!this.helper.isEmpty(this.cptData.crm)
            && this.crmList.findIndex(i => i.userId == this.cptData.crm) == -1) {
            var usr = this.cptData.cptUserAsmt.find(i => i.cptUserId == this.cptData.crm
              && i.usrPrfId == ECPTAssignmentProfileId.CRM);
            if (!this.helper.isEmpty(usr)) {
              this.crmList.push({ userId: usr.cptUserId, fullName: usr.cptUser })
            }
          }
        }

        // Assignment
        this.setFormControlValue('assignment.gatekeeper', this.cptData.gatekeeper);
        this.setFormControlValue('assignment.manager', this.cptData.manager);
        this.setFormControlValue('assignment.mss', this.cptData.mss);
        this.setFormControlValue('assignment.nte', this.cptData.nte);
        this.setFormControlValue('assignment.pm', this.cptData.pm);
        this.setFormControlValue('assignment.crm', this.cptData.crm);
        this.setFormControlValue('assignment.reviewedByPm', this.cptData.revwdPmCd);
        this.setFormControlValue('assignment.returnedToSde', this.cptData.retrnSdeCd);
      },
      error => {
        this.spinner.hide();
        this.helper.notify('Failed to load CPT Data', Global.NOTIFY_TYPE_ERROR);
      },
    ).then(() => {
      this.setCPTFormFields().then(() => this.spinner.hide());
    });
  }

/** Set CPT Form fields based on CPT Status and User Profiles */
  setCPTFormFields() {
    return new Promise(resolve => {
      this.disableForm();
      let profiles = this.userSrvc.loggedInUser.profiles;

      // Add the condition if CPT Customer types needs Provisioning and/or Assignment section
      this.hasProvisioning = (this.cptData.customerTypes != null && this.cptData.customerTypes.length == 1
        && this.cptData.customerTypes.includes(ECPTCustomerType.Sps)) ? false : true;
      this.hasAssignment = (this.cptData.customerTypes != null && this.cptData.customerTypes.length == 1
        && (this.cptData.customerTypes.includes(ECPTCustomerType.UnmanagedE2E)
          || this.cptData.customerTypes.includes(ECPTCustomerType.ManagedVoice))) ? false : true;

      if (this.editMode) {
        this.form.enable();

        // Requestor is always disabled
        this.form.get('requestor').disable();
        this.option.requestor.userFinder.isEnabled = false;

        // Primesite and QIP is always disabled
        this.form.get('provisioning.primeSite').disable();
        this.form.get('provisioning.qipSubnet').disable();

        if (!this.allowCreate) {
          this.disableForm();
          this.form.get('notes').enable(); // Allow users to add notes
        }
        else {
          if (this.isSdwanOrMeraki.value != null && this.isSdwanOrMeraki.value == 0) {
            this.isSdwanMerakiOnlyCustomer.disable();
          }
        }

        if (this.hasProvisioning && this.allowProvision) {
          this.form.get('provisioning').enable();
        }
        else {
          this.form.get('provisioning').disable();
        }

        if (this.hasAssignment && this.allowAssignment) {
          //// As of now set to true since all types is either Auto Assign or N/A
          //this.disableGk = true;
          //// Disable for types that are N/A (MDS Support Monitor&Notify, Wholesale Carrier, Wholesale Var)
          //// Condition below gets all types with enable Manager but reversed with the use of ! to get disable
          //this.disableManager = !((this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)
          //  && (this.cptData.hostedManagedServices.includes(ECPTHostedManagedSrvc.WpaaS)
          //    || this.cptData.relatedInfos.findIndex(i => i.sdwanCustCd) != -1
          //    || this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MssSolution)
          //    || this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsComplete)
          //    || (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsSupport)
          //      && (this.cptData.mdsSupportTiers.includes(ECPTMdsSupportTier.Design)
          //        || this.cptData.mdsSupportTiers.includes(ECPTMdsSupportTier.Implementation)))))
          //  || this.cptData.customerTypes.includes(ECPTCustomerType.ManagedSecurity)
          //  || this.cptData.customerTypes.includes(ECPTCustomerType.ManagedVoice)
          //  || this.cptData.customerTypes.includes(ECPTCustomerType.Sps));

          //this.cptData.mdsPlnSrvcTiers.includes(1)
          //  || this.cptData.mdsPlnSrvcTiers.includes(2) || this.cptData.mdsPlnSrvcTiers.includes(3)
          //  || this.cptData.mdsSupportTiers.includes(1) || this.cptData.mdsSupportTiers.includes(2);
          //// Disable for types that are N/A
          //let manager = this.form.get('assignment.manager').value;
          //if (!this.helper.isEmpty(manager) && manager == 3) {
          //  // NTE Pool: (MDS Support Monitor & Notify with No Ethernet, Wholesale Carrier, Wholesale Var and SDWAN Only)
          //  // Condition below gets all types with enable Manager but reversed with the use of ! to get disable
          //  this.disableNte = this.cptData.relatedInfos != null && this.cptData.relatedInfos.length > 0
          //    && (this.cptData.relatedInfos.findIndex(i => i.cptPlnSrvcTierId == ECPTPlanServiceTier.WholesaleCarrier
          //      || i.cptPlnSrvcTierId == ECPTPlanServiceTier.WholesaleVar
          //      || (i.cptPlnSrvcTierId == ECPTPlanServiceTier.MdsSupport && !i.suplmntlEthrntCd)
          //      || (i.cptPlnSrvcTierId == ECPTPlanServiceTier.MdsComplete && i.sdwanCustCd)) != -1);
          //}
          //else {
          //  // NTE: Disable for types that are N/A (MDS Support Monitor&Notify, Wholesale Carrier, Wholesale Var)
          //  // Same as Manager above
          //  this.disableNte = this.disableManager;
          //}
          //// Disable for types that are Auto Assign and N/A (Managed Data, SPS, WPaaS and SDWANCustOnly)
          //// SDWAN Cust Only is under MDS Complete Collaborative und ManagedData
          //this.disablePm = this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)
          //  || this.cptData.customerTypes.includes(ECPTCustomerType.Sps)
          //  || this.cptData.hostedManagedServices.includes(ECPTHostedManagedSrvc.WpaaS);
          //// Disable for types that are Auto Assign and N/A (Managed Data, WPaaS and SDWANCustOnly)
          //// SDWAN Cust Only is under MDS Complete Collaborative und ManagedData
          //this.disableCrm = !(this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)
          //  || this.cptData.hostedManagedServices.includes(ECPTHostedManagedSrvc.WpaaS));

          // Commented above code by Sarah Sandoval [20200817]
          // Copied below code condition on old COWS implementation
          if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedSecurity)
            || this.cptData.customerTypes.includes(ECPTCustomerType.ManagedVoice)
            || this.cptData.customerTypes.includes(ECPTCustomerType.Sps)) {
            this.disableGk = false;

            if (this.cptData.customerTypes.includes(ECPTCustomerType.Sps)) {
              this.disablePm = false;
              this.disableCrm = true;
            }
          }

          if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)) {
            if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleCarrier)
              || this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleVar)) {
              this.disableGk = true;
              this.disableManager = true;
              this.disableNte = false;
              this.disablePm = false;
              this.disableCrm = false;
              this.nteList = this.nteList.filter(i => i.poolCd);
            }

            if (this.cptData.hostedManagedServices.includes(ECPTHostedManagedSrvc.WpaaS)) {
              this.disableGk = false;
              this.disableManager = false;
              this.disableNte = false;
              this.disablePm = false;
            }

            if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsSupport)) {
              if (this.cptData.mdsSupportTiers.includes(ECPTMdsSupportTier.Design)) {
                this.disableGk = false;
                this.disablePm = true;
                this.disableCrm = false;
              }

              if (this.cptData.mdsSupportTiers.includes(ECPTMdsSupportTier.Implementation)) {
                this.disableGk = false;
                this.disablePm = false;
                this.disableCrm = false;
              }

              if (this.cptData.mdsSupportTiers.includes(ECPTMdsSupportTier.MonitorNotify)) {
                this.disableGk = true;
                this.disableManager = true;
                this.disableNte = false;
                this.disablePm = false;
                this.disableCrm = false;
                this.nteList = this.nteList.filter(i => i.poolCd);
              }
            }

            if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsComplete)
              || this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MssSolution)) {
              this.disableGk = false;
              this.disableManager = false;
              this.disableNte = false;
              this.disablePm = false;
              this.disableCrm = false;
            }
          }

          // Returned to SDE is allowed only for Admin and GK
          if (profiles.filter(i => i.toUpperCase().trim() == 'MDS ADMIN'
            || i.toUpperCase().trim() == 'MDS CPT GATEKEEPER').length > 0) {
            this.form.get('assignment.returnedToSde').enable();
          }
          else {
            this.form.get('assignment.returnedToSde').disable()
          }

          // Reviewed by PM is allowed only for Admin and PM
          if (profiles.filter(i => i.toUpperCase().trim() == 'MDS ADMIN'
            || i.toUpperCase().trim() == 'MDS EVENT REVIEWER').length > 0) {
            this.form.get('assignment.reviewedByPm').enable();
          }
          else {
            this.form.get('assignment.reviewedByPm').disable()
          }

          // For PM only, if Profiles doesn't have Admin, Gatekeeper and Manager
          var pmOnly = !profiles.includes('MDS Admin') && !profiles.includes('MDS CPT GateKeeper')
            && !profiles.includes('MDS CPT Manager') && profiles.includes('MDS Event Reviewer');
          if (pmOnly) {
            this.disableGk = this.disableManager = this.disableNte = this.disablePm = this.disableCrm = true;
          }
        }
        else {
          this.form.get('assignment').disable();
        }

        // Cancel CPT is allowed only for Admin, GK and Mmanager
        if (profiles.filter(i => i.toUpperCase().trim() == 'MDS ADMIN'
          || i.toUpperCase().trim() == 'MDS CPT GATEKEEPER'
          || i.toUpperCase().trim() == 'MDS CPT MANAGER').length > 0) {
          this.form.get('cancelCpt').enable();
        }
        else {
          this.form.get('cancelCpt').disable();
        }
      }

      // By CPT Status
      if (this.id > 0 && this.cptData != null &&
        (this.cptData.cptStusId == ECPTStatus.Deleted
          || this.cptData.cptStusId == ECPTStatus.Completed
          || this.cptData.cptStusId == ECPTStatus.Cancelled)) {
        this.allowEdit = false;
        this.disableForm();
      }

      resolve(true);
    });
  }

  disableForm() {
    this.form.disable();
    this.option.requestor.userFinder.isEnabled = false;
    this.option.sprintSalesTeam.userFinderForSeEmail.isEnabled = false;
    this.option.sprintSalesTeam.userFinderForAmEmail.isEnabled = false;
    this.option.sprintSalesTeam.userFinderForIpmEmail.isEnabled = false;
  }

  onCustomerH1Changed(e) {
    //console.log(e)
    let h1 = e.value;
    //let custName = e.selectedItem.customerName;
    let cust = this.customerList.filter(i => i.h1 === h1);
    if (!this.helper.isEmpty(cust) && cust.length > 0) {
      this.h1.setValue(h1);
      this.company.setValue(cust[0].customerName);
    }

    //// Auto populates fields
    //if (!this.helper.isEmpty(h1) && !this.helper.isEmpty(custName)) {
    //  this.cptSrvc.getCustomerH1Data(h1, custName).subscribe(
    //    res => {
    //      this.h1CustData = res;
    //      console.log(this.h1CustData)
    //      this.h1.setValue(h1);
    //      this.company.setValue(this.h1CustData.customerName);
    //      //this.seEmail.setValue(this.h1CustData.sEEmail);
    //      //this.amEmail.setValue(this.h1CustData.aMEmail);
    //      //this.ipmEmail.setValue(this.h1CustData.iPMEmail);
    //    },
    //    error => {
    //      this.h1.setValue(h1);
    //      this.company.setValue(custName);
    //    }
    //  );
    //}

    this.cntctList.refreshContact()
  }

  onCustomContactCreating(args) {
    let newValue = args.text;
    //console.log(newValue)
    if (!this.helper.isReallyEmpty(newValue)) {
      this.additionalEmails.push(newValue)
      args.customItem = newValue;
    }
  }

  onContactDetailSaved(result) {
    this.contactList = result;
  }

  onCustomerTypeChanged(e) {
    if (this.customerTypes.value != null && this.customerTypes.value.length == 1
      && (this.customerTypes.value.includes(ECPTCustomerType.ManagedVoice)
        || this.customerTypes.value.includes(ECPTCustomerType.UnmanagedE2E)
        || this.customerTypes.value.includes(ECPTCustomerType.Sps)
      )) {
      this.isSprintWholesaleReseller.setValue(0);
      this.isSprintWholesaleReseller.disable();
      this.wholesaleResellerName.setValue(null);
    }
    else {
      this.isSprintWholesaleReseller.setValue((this.cptData != null) ? (this.cptData.sprntWhlslReslrCd ? 1 : 0) : null);
      this.isSprintWholesaleReseller.enable();
      this.wholesaleResellerName.setValue((this.cptData != null && !this.helper.isEmpty(this.cptData.sprntWhlslReslrNme)) ? this.cptData.sprntWhlslReslrNme : null);
    }
  }

  onSdwanChanged(e: any) {
    //console.log(e)
    if (e.value == 1) {
      this.isSdwanMerakiOnlyCustomer.setValue(null);
    } else {
      this.isSdwanMerakiOnlyCustomer.setValue(0);
    }
  }

  openDesignDocUrl(url: string) {
    if (!this.helper.isEmpty(url)) {
      if (!url.includes(location.hostname)) {
        window.open('https://' + url, "_blank", "toolbar=0,location=0,menubar=0");
      }
      else {
        window.open(url, "_blank", "toolbar=0,location=0,menubar=0");
      }
    }
  }

  onManagerChanged(e: any) {
    //console.log(e)
    if (e.value == 3) {
      this.cptSrvc.getAutoAssignNTE().toPromise().then(res => {
        //console.log(res);
        this.form.get('assignment.nte').setValue(res);
        this.form.get('assignment.nte').disable();
      });
    }
    else {
      this.form.get('assignment.nte').setValue(0);
      this.form.get('assignment.nte').enable();
    }
  }

  onReviewedPmChanged(e: any) {
    if (e.value && this.provisioningList != null && this.provisioningList.length > 0) {
      let neededList = this.provisioningList.filter(i => i.prvsnStusCd && i.recStusId != 303);
      if (neededList != null && neededList.length > 0) {
        this.showReviewProvisionError = true;
      }
      else {
        this.showReviewProvisionError = false;
      }
    }
    else {
      this.showReviewProvisionError = false;
    }
  }

  edit() {
    this.cptSrvc.lock(this.id).subscribe(
      res => {
        this.editMode = true;
        this.setCPTFormFields();
      },
      error => {
        this.disableForm();
        //this.helper.notify('Failed to lock CPT request.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  getCPTFormData() {
    return new Promise(resolve => {
      if (this.id == 0) {
        this.cptData = new Cpt();
      }

      this.cptData.submtrUserId = this.helper.getFormControlValue("requestor.id");
      //this.cptData.seEmailAdr = this.seEmail.value;
      //this.cptData.amEmailAdr = this.amEmail.value;
      //this.cptData.ipmEmailAdr = this.ipmEmail.value;
      this.cptData.h1 = this.h1.value;
      this.cptData.devCnt = this.noOfDevices.value;
      this.cptData.customerTypes = this.customerTypes.value;
      this.cptData.compnyNme = this.company.value;
      this.cptData.custAdr = this.address.value;
      this.cptData.custFlrBldgNme = this.helper.getFormControlValue('customer.bldgRmFlr');
      this.cptData.custCtyNme = this.city.value;
      this.cptData.custSttPrvnNme = this.state.value;
      this.cptData.custCtryRgnNme = this.country.value;
      this.cptData.custZipCd = this.zip.value;
      this.cptData.sprntWhlslReslrCd = this.isSprintWholesaleReseller.value;
      this.cptData.sprntWhlslReslrNme = this.cptData.sprntWhlslReslrCd ? this.wholesaleResellerName.value : '';
      this.cptData.govtCustCd = this.isGovernmentCustomer.value;
      this.cptData.scrtyClrnceCd = this.cptData.govtCustCd ? this.isSecurityClearance.value : '';
      this.cptData.tacacsCd = this.isTacacs.value == null ? false : this.isTacacs.value;
      this.cptData.encMdmCd = this.isEncryptedModems.value == null ? false : this.isEncryptedModems.value;
      this.cptData.hstMgndSrvcCd = this.isHostedManagedService.value == null ? false : this.isHostedManagedService.value;
      this.cptData.hostedManagedServices = this.helper.getFormControlValue('requiredInfo.hstMngdSrvc');
      this.cptData.sdwanCd = this.isSdwanOrMeraki.value == null ? false : this.isSdwanOrMeraki.value;
      this.cptData.mdsPlnSrvcTiers = this.plannedSrvcTiers.value;

      if (this.cptData.customerTypes != null && this.cptData.customerTypes.length > 0) {
        let relatedInfos: CptRelatedInfo[] = [];
        let relatedInfo: CptRelatedInfo;
        if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)) {
          if (this.cptData.mdsPlnSrvcTiers != null && this.cptData.mdsPlnSrvcTiers.length > 0) {
            if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MssSolution)) {
              relatedInfo = new CptRelatedInfo();
              relatedInfo.cptCustTypeId = ECPTCustomerType.ManagedData;
              relatedInfo.cptPlnSrvcTierId = ECPTPlanServiceTier.MssSolution;
              relatedInfo.cptPrimWan = this.mssPrimTransportList;
              relatedInfo.secTrnsprtTypes = this.helper.getFormControlValue('mdsMss.secTrnsprtTypes');
              relatedInfo.hstdUcCd = this.helper.getFormControlValue('mdsMss.isHostedUC');
              relatedInfo.cubeSiptSmiCd = this.helper.getFormControlValue('mdsMss.isCubes');
              relatedInfo.ntwkDsgnDocTxt = this.mdsMssNddUrl.value;
              relatedInfo.cmntTxt = this.helper.getFormControlValue('mdsMss.comments');
              relatedInfos.push(relatedInfo);

            }
            if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsComplete)) {
              relatedInfo = new CptRelatedInfo();
              relatedInfo.cptCustTypeId = ECPTCustomerType.ManagedData;
              relatedInfo.cptPlnSrvcTierId = ECPTPlanServiceTier.MdsComplete;
              relatedInfo.cptPrimWan = this.mdsCmpltPrimTransportList;
              relatedInfo.secTrnsprtTypes = this.helper.getFormControlValue('mdsComplete.secTrnsprtTypes');
              relatedInfo.hstdUcCd = this.helper.getFormControlValue('mdsComplete.isHostedUC');
              relatedInfo.cubeSiptSmiCd = this.helper.getFormControlValue('mdsComplete.isCubes');
              relatedInfo.sdwanCustCd = this.cptData.sdwanCd ? this.helper.getFormControlValue('mdsComplete.isSdwanMerakiOnlyCustomer') : null;
              relatedInfo.ntwkDsgnDocTxt = this.mdsCmpltNddUrl.value;
              relatedInfo.cmntTxt = this.helper.getFormControlValue('mdsComplete.comments');
              relatedInfos.push(relatedInfo);
            }
            if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsSupport)) {
              relatedInfo = new CptRelatedInfo();
              relatedInfo.cptCustTypeId = ECPTCustomerType.ManagedData;
              relatedInfo.cptPlnSrvcTierId = ECPTPlanServiceTier.MdsSupport;
              relatedInfo.cptPrimWan = this.mdsSupportPrimTransportList;
              relatedInfo.secTrnsprtTypes = this.helper.getFormControlValue('mdsSupport.secTrnsprtTypes');
              relatedInfo.suplmntlEthrntCd = this.helper.getFormControlValue('mdsSupport.isSupplementalEthernetSrvc');
              relatedInfo.ntwkDsgnDocTxt = this.mdsSupportNddUrl.value;
              relatedInfo.cmntTxt = this.helper.getFormControlValue('mdsSupport.comments');
              relatedInfos.push(relatedInfo);
              this.cptData.mdsSupportTiers = this.mdsSupportTiers.value;
            }
            if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleCarrier)) {
              relatedInfo = new CptRelatedInfo();
              relatedInfo.cptCustTypeId = ECPTCustomerType.ManagedData;
              relatedInfo.cptPlnSrvcTierId = ECPTPlanServiceTier.WholesaleCarrier;
              relatedInfo.cptPrimWan = this.mdsWholesaleCarrierPrimTransportList;
              relatedInfo.secTrnsprtTypes = this.helper.getFormControlValue('mdsWholesaleCarrier.secTrnsprtTypes');
              relatedInfo.ntwkDsgnDocTxt = this.mdsWholesaleCarrierNddUrl.value;
              relatedInfo.cmntTxt = this.helper.getFormControlValue('mdsWholesaleCarrier.comments');
              relatedInfos.push(relatedInfo);
            }
            if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleVar)) {
              relatedInfo = new CptRelatedInfo();
              relatedInfo.cptCustTypeId = ECPTCustomerType.ManagedData;
              relatedInfo.cptPlnSrvcTierId = ECPTPlanServiceTier.WholesaleVar;
              relatedInfo.cptPrimWan = this.mdsWholesaleVarPrimTransportList;
              relatedInfo.secTrnsprtTypes = this.helper.getFormControlValue('mdsWholesaleVar.secTrnsprtTypes');
              relatedInfo.ntwkDsgnDocTxt = this.mdsWholesaleVarNddUrl.value;
              relatedInfo.cmntTxt = this.helper.getFormControlValue('mdsWholesaleVar.comments');
              relatedInfos.push(relatedInfo);
            }
          }
        }
        if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedSecurity)) {
          relatedInfo = new CptRelatedInfo();
          relatedInfo.cptCustTypeId = ECPTCustomerType.ManagedSecurity;
          relatedInfo.cmntTxt = this.helper.getFormControlValue('mss.comments');
          relatedInfos.push(relatedInfo);
          this.cptData.mssPlnSrvcTypes = this.plannedSrvcTypes.value;
          this.cptData.mssMngdAuthenticationProds = this.mngdAuthenticationProd.value;
        }
        if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedVoice)) {
          relatedInfo = new CptRelatedInfo();
          relatedInfo.cptCustTypeId = ECPTCustomerType.ManagedVoice;
          relatedInfo.custUcdCd = this.helper.getFormControlValue('mvs.isUCDevices');
          relatedInfo.cmntTxt = this.helper.getFormControlValue('mvs.comments');
          relatedInfos.push(relatedInfo);
          this.cptData.mvsProdTypes = this.mvsProdTypes.value;
          //this.cptData.cptDocs = this.mvsDocList;
          this.cptData.cptDocs = [];
          if (this.docList != null && this.docList.length > 0) {
            this.docList.forEach(i => {
              let doc = new CptDoc();
              doc.cptDocId = i.docId;
              doc.cptId = i.id;
              doc.fileNme = i.fileNme;
              doc.fileCntnt = i.fileCntnt;
              doc.fileSizeQty = i.fileSizeQty;
              doc.cmntTxt = i.cmntTxt,
              doc.creatByUserId = i.creatByUserId;
              doc.creatDt = i.creatDt;
              doc.base64string = i.base64string;
              this.cptData.cptDocs.push(doc);
            });
          }
        }
        if (this.cptData.customerTypes.includes(ECPTCustomerType.UnmanagedE2E)) {
          relatedInfo = new CptRelatedInfo();
          relatedInfo.cptCustTypeId = ECPTCustomerType.UnmanagedE2E;
          relatedInfo.e2eNddCd = this.helper.getFormControlValue('e2e.isNddToDocshare');
          relatedInfo.ntwkDsgnDocTxt = this.e2eNddUrl.value;
          relatedInfo.cmntTxt = this.helper.getFormControlValue('e2e.comments');
          relatedInfos.push(relatedInfo);
        }
        if (this.cptData.customerTypes.includes(ECPTCustomerType.Sps)) {
          relatedInfo = new CptRelatedInfo();
          relatedInfo.cptCustTypeId = ECPTCustomerType.Sps;
          relatedInfo.spsSowCd = this.helper.getFormControlValue('sps.isSpsSowToDocshare');
          relatedInfo.ntwkDsgnDocTxt = this.spsNddUrl.value;
          relatedInfo.cmntTxt = this.helper.getFormControlValue('sps.comments');
          relatedInfos.push(relatedInfo);
        }

        // Add Contact Email List
        if (relatedInfos.length > 0 && !this.helper.isEmpty(this.contactEmails.value)) {
          relatedInfos[0].cntctEmailList = this.contactEmails.value;
          console.log(relatedInfos);
        }

        this.cptData.relatedInfos = relatedInfos;
      }

      this.cptData.notes = this.helper.getFormControlValue('notes');

      // Added Contact Details (PJ025845)
      this.contactList.forEach(i => i.emailCd = this.helper.isEmpty(i.emailCdIds) ? '' : i.emailCdIds.join(','));
      this.cptData.contactDetails = this.contactList;

      if (this.id > 0) {
        // For Provisioning and Assignment Section
        this.cptData.custShrtNme = this.helper.getFormControlValue('provisioning.shortName');
        this.cptData.primSites = this.helper.getFormControlValue('provisioning.primeSite');
        this.cptData.subnetIpAdr = this.helper.getFormControlValue('provisioning.ipSubnet');
        this.cptData.startIpAdr = this.helper.getFormControlValue('provisioning.startIp');
        this.cptData.endIpAdr = this.helper.getFormControlValue('provisioning.endIp');
        this.cptData.qipSubnetNme = this.helper.getFormControlValue('provisioning.qipSubnet');
        this.cptData.preShrdKey = this.helper.getFormControlValue('provisioning.preSharedKey');

        this.cptData.gatekeeper = this.helper.getFormControlValue('assignment.gatekeeper');
        this.cptData.manager = this.helper.getFormControlValue('assignment.manager');
        this.cptData.mss = this.helper.getFormControlValue('assignment.mss');
        this.cptData.nte = this.helper.getFormControlValue('assignment.nte');
        this.cptData.pm = this.helper.getFormControlValue('assignment.pm');
        this.cptData.crm = this.helper.getFormControlValue('assignment.crm');
        this.cptData.revwdPmCd = this.helper.getFormControlValue('assignment.reviewedByPm');
        this.cptData.retrnSdeCd = this.helper.getFormControlValue('assignment.returnedToSde');
        this.cptData.cancelCpt = this.helper.getFormControlValue('cancelCpt');
      }

      resolve(this.cptData);
    });
  }

  validate() {
    return new Promise(resolve => {
      this.getCPTFormData().then(res => {
        this.errors = [];

        if (this.status.value == ECPTStatus.Cancelled || this.status.value == ECPTStatus.Completed
          || this.status.value == ECPTStatus.Deleted) {
          resolve(true);
        }

        // Sprint Sales Team
        //if (this.helper.isReallyEmpty(this.seEmail.value)) { this.errors.push('SE Email is required'); }
        //if (this.seEmail.invalid && this.seEmail.errors.invalidEmail) { this.errors.push('SE Email is invalid'); }
        //if (this.helper.isReallyEmpty(this.amEmail.value)) { this.errors.push('Account Manager Email is invalid'); }
        //if (this.amEmail.invalid && this.amEmail.errors.invalidEmail) { this.errors.push('Account Manager Email is invalid'); }
        //if (this.helper.isReallyEmpty(this.ipmEmail.value)) { this.errors.push('IPM Email is required'); }
        //if (this.ipmEmail.invalid && this.ipmEmail.errors.invalidEmail) { this.errors.push('IPM Email is invalid'); }
        if (this.helper.isReallyEmpty(this.h1.value)) { this.errors.push('H1 is required'); }
        if (this.h1.invalid && this.h1.errors.invalidH1) { this.errors.push('H1 field is not in correct format. H1 should contain 9 digits only.'); }
        if (this.helper.isReallyEmpty(this.noOfDevices.value)) { this.errors.push('No of Devices is required'); }

        // Customer
        if (this.helper.isReallyEmpty(this.company.value)) { this.errors.push('Company is required'); }
        if (this.helper.isReallyEmpty(this.address.value)) { this.errors.push('Address is required'); }
        if (this.helper.isReallyEmpty(this.city.value)) { this.errors.push('City is required'); }
        if (this.helper.isReallyEmpty(this.state.value)) { this.errors.push('State/Province is required'); }
        if (this.helper.isReallyEmpty(this.country.value)) { this.errors.push('Country is required'); }
        if (this.helper.isReallyEmpty(this.zip.value)) { this.errors.push('ZIP Code is required'); }
        if (this.isSprintWholesaleReseller.value === null) { this.errors.push('Sprint Wholesale Reseller is required'); }
        if (this.cptData.sprntWhlslReslrCd) {
          if (this.helper.isReallyEmpty(this.wholesaleResellerName.value)) { this.errors.push('Sprint Wholesale Reseller Name is required'); }
        }
        if (this.isGovernmentCustomer.value === null) { this.errors.push('Government Customer is required'); }
        if (this.cptData.govtCustCd) {
          if (this.isSecurityClearance.value === null) { this.errors.push('Security Clearance is required'); }
        }

        // Required Information
        if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)
          || this.cptData.customerTypes.includes(ECPTCustomerType.ManagedSecurity)) {
          if (this.isTacacs.value === null) { this.errors.push('TACACS access to devices is required'); }
          if (this.isEncryptedModems.value === null) { this.errors.push('ENCRYPTED MODEMS is required'); }
          if (this.isHostedManagedService.value === null) {
            this.errors.push('HOSTED Managed Service is required');
          } else {
            let hst = this.form.get('requiredInfo.hstMngdSrvc').value;
            if (this.isHostedManagedService.value == 1 && (hst == null || (hst != null && hst.length <= 0))) {
              this.errors.push('HOSTED Managed Service type is required');
            }
          }
          if (this.isSdwanOrMeraki.value === null) { this.errors.push('SDWAN and/or Meraki Managed Services is required'); }
        }

        // Additional Information
        if (this.customerTypes.invalid && this.customerTypes.errors.invalidCPTCustType) { this.errors.push('Multiple selections are only allowable if both Managed Data and Managed Security are selected. Any other combination is not allowed.'); }
        if (this.cptData.customerTypes != null && this.cptData.customerTypes.length > 0) {
          // Managed Data
          if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedData)) {
            if (this.cptData.mdsPlnSrvcTiers != null && this.cptData.mdsPlnSrvcTiers.length > 0) {
              // MSS Solution
              if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MssSolution)) {
                if (this.mssPrimTransportList != null && this.mssPrimTransportList.length > 0) {
                  if (this.mssPrimTransportList.filter(a => a.qty < 0).length > 0) {
                    this.errors.push("Number of Sites value for MSS Solution is invalid")
                  }

                  if (this.mssPrimTransportList.filter(a => a.qty == 0).length > 0) {
                    this.errors.push("Number of Sites value for MSS Solution is required")
                  }
                }
                else { this.errors.push('Primary Transport field for MSS Solution is required'); }
                if (this.helper.getFormControlValue('mdsMss.isHostedUC') === null) { this.errors.push('Hosted UC for MSS Solution is required'); }
                if (this.helper.getFormControlValue('mdsMss.isCubes') === null) { this.errors.push('CUBEs/SIPT/SMI for MSS Solution is required'); }
              }

              // MDS Complete/Collaborative
              if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsComplete)) {
                if (this.mdsCmpltPrimTransportList != null && this.mdsCmpltPrimTransportList.length > 0) {
                  if (this.mdsCmpltPrimTransportList.filter(a => a.qty < 0).length > 0) {
                    this.errors.push("Number of Sites value for MDS Complete/Collaborative is invalid")
                  }

                  if (this.mdsCmpltPrimTransportList.filter(a => a.qty == 0).length > 0) {
                    this.errors.push("Number of Sites value for MDS Complete/Collaborative is required")
                  }
                }
                else { this.errors.push('Primary Transport field for MDS Complete/Collaborative is required'); }
                if (this.helper.getFormControlValue('mdsComplete.isHostedUC') === null) { this.errors.push('Hosted UC for MDS Complete/Collaborative is required'); }
                if (this.helper.getFormControlValue('mdsComplete.isCubes') === null) { this.errors.push('CUBEs/SIPT/SMI for MDS Complete/Collaborative is required'); }
                if (this.cptData.sdwanCd) {
                  if (this.helper.getFormControlValue('mdsComplete.isSdwanMerakiOnlyCustomer') === null) { this.errors.push('SDWAN and/or Meraki ONLY for MDS Complete/Collaborative is required'); }
                }
              }

              // MDS Support
              if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.MdsSupport)) {
                if (this.mdsSupportPrimTransportList != null && this.mdsSupportPrimTransportList.length > 0) {
                  if (this.mdsSupportPrimTransportList.filter(a => a.qty < 0).length > 0) {
                    this.errors.push("Number of Sites value for MDS Support is invalid")
                  }

                  if (this.mdsSupportPrimTransportList.filter(a => a.qty == 0).length > 0) {
                    this.errors.push("Number of Sites value for MDS Support is required")
                  }
                }
                else { this.errors.push('Primary Transport field for MDS Support is required'); }
                if (this.mdsSupportTiersList.length > 0) {
                  if (this.mdsSupportTiersList.includes(3) && this.helper.getFormControlValue('mdsSupport.isSupplementalEthernetSrvc') === null) { this.errors.push('Supplemental Ethernet Services for MDS Support is required'); }
                }
                else {
                  this.errors.push('MDS Support Tier is required');
                }
              }

              // Wholesale Carrier
              if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleCarrier)) {
                if (this.mdsWholesaleCarrierPrimTransportList != null && this.mdsWholesaleCarrierPrimTransportList.length > 0) {
                  if (this.mdsWholesaleCarrierPrimTransportList.filter(a => a.qty < 0).length > 0) {
                    this.errors.push("Number of Sites value for Wholesale Carrier is invalid")
                  }

                  if (this.mdsWholesaleCarrierPrimTransportList.filter(a => a.qty == 0).length > 0) {
                    this.errors.push("Number of Sites value for Wholesale Carrier is required")
                  }
                }
                else { this.errors.push('Primary Transport field for Wholesale Carrier is required'); }
              }

              // Wholesale VAR
              if (this.cptData.mdsPlnSrvcTiers.includes(ECPTPlanServiceTier.WholesaleVar)) {
                if (this.mdsWholesaleVarPrimTransportList != null && this.mdsWholesaleVarPrimTransportList.length > 0) {
                  if (this.mdsWholesaleVarPrimTransportList.filter(a => a.qty < 0).length > 0) {
                    this.errors.push("Number of Sites value for Wholesale VAR is invalid")
                  }

                  if (this.mdsWholesaleVarPrimTransportList.filter(a => a.qty == 0).length > 0) {
                    this.errors.push("Number of Sites value for Wholesale VAR is required")
                  }
                }
                else { this.errors.push('Primary Transport field for Wholesale VAR is required'); }
              }
            }
            else {
              this.errors.push('Planned Service Tier is required');
            }
          }

          // Managed Security
          if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedSecurity)) {
            if (this.cptData.mssPlnSrvcTypes != null && this.cptData.mssPlnSrvcTypes.length > 0) {
              if (this.cptData.mssPlnSrvcTypes.includes(3)
                && (this.cptData.mssMngdAuthenticationProds == null
                  || (this.cptData.mssMngdAuthenticationProds != null
                    && this.cptData.mssMngdAuthenticationProds.length <= 0))) {
                this.errors.push('Managed Authentication Product is required');
              }
            }
            else {
              this.errors.push('Planned Service Type is required');
            }
          }

          // Managed Voice
          if (this.cptData.customerTypes.includes(ECPTCustomerType.ManagedVoice)) {
            if (this.helper.getFormControlValue('mvs.isUCDevices') === null) { this.errors.push('Unified Communications devices is required'); }
            if (this.cptData.mvsProdTypes == null || this.cptData.mvsProdTypes.length <= 0) { this.errors.push('MVS Product Types is required'); }
          }

          // Unmanaged E2E
          if (this.cptData.customerTypes.includes(ECPTCustomerType.UnmanagedE2E)) {
            if (this.helper.getFormControlValue('e2e.isNddToDocshare') === null) { this.errors.push('Network Design Doc (NDD) is required'); }
          }

          // Sps
          if (this.cptData.customerTypes.includes(ECPTCustomerType.Sps)) {
            if (this.helper.getFormControlValue('sps.isSpsSowToDocshare') === null) { this.errors.push('Professional Services SOW is required'); }
          }
        }
        else {
          this.errors.push('Customer Type is required');
        }

        // Validation for Provisioning/Assignment: None required
        if (this.ipSubnet.invalid && this.ipSubnet.errors.invalidIp) { this.errors.push('IP Subnet is invalid'); }
        if (this.startIp.invalid && this.startIp.errors.invalidIp) { this.errors.push('Start IP is invalid'); }
        if (this.endIp.invalid && this.endIp.errors.invalidIp) { this.errors.push('End IP is invalid'); }

        console.log('Contact List hasEditData(): ' + this.cntctList.dataGrid.instance.hasEditData())
        if (this.cntctList.dataGrid.instance.hasEditData()) {
          this.errors.push("Changes made on Contact List table is not yet saved.");
        }
        if (this.contactList.filter(a =>
          this.helper.isEmpty(a.hierId) ||
          this.helper.isEmpty(a.hierLvlCd) ||
          this.helper.isEmpty(a.roleId) ||
          this.helper.isEmpty(a.emailAdr) ||
          this.helper.isEmpty(a.emailCdIds))
          .length > 0) {
          this.errors.push("Please enter the required fields in the Contact List")
        }

        if (this.errors != null && this.errors.length > 0) {
          $("#validation-summary").modal("show");
          resolve(false);
        }
        else {
          resolve(true);
        }
      });
    });
  }

  save() {
    this.isSubmitted = true;
    //console.log(this.contactEmails.value.join(';'))
    console.log(this.contactEmails.value)
    console.log(this.contactList)

    //let getCPTFormDataPromise = this.getCPTFormData();
    //let validatePromise = this.validate();

    //Promise
    //  .all([getCPTFormDataPromise, validatePromise])
    //  .then(data => {
    //    //this.cptData = res[0] as Cpt;
    //    if (data[1] as boolean) {
    //      // Proceed to Create/Update
    //      this.spinner.show();
          
    //      if (this.id > 0) {
    //        // CPT Update
    //        return this.cptSrvc.update(this.id, this.cptData).toPromise();
    //      }
    //      else {
    //        // CPT Create
    //        return this.cptSrvc.create(this.cptData).toPromise();
    //      }
    //    }
    //  })
    //  .then(data1 => {
    //    console.log(data1)
    //    let msg = this.id > 0 ? (this.cptData.cancelCpt ? 'Successfully cancelled CPT.' : 'Successfully updated CPT.') : 'Successfully created CPT.';
    //    this.helper.notify(msg, Global.NOTIFY_TYPE_SUCCESS);
    //    this.spinner.hide();
    //    this.cancel();
    //  })
    //  .catch(err => {
    //    this.helper.notify(`An error occurred while saving CPT. ${this.helper.getErrorMessage(err)}`, Global.NOTIFY_TYPE_ERROR);
    //    this.spinner.hide();
    //    this.cancel();
    //  });

    this.validate().then(valid => {
      if (valid) {
        console.log(this.cptData)
        this.spinner.show();
        // CPT Update
        if (this.id > 0) {
          this.cptSrvc.update(this.id, this.cptData).toPromise().then(
            res => {
              this.helper.notify('Successfully updated CPT.', Global.NOTIFY_TYPE_SUCCESS);
              this.spinner.hide();
              this.cancel();
            }, error => {
              //console.log(error)
              this.helper.notify(`An error occurred while updating CPT data. ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
              this.spinner.hide();
              this.cancel();
            }
          );
        }
        // CPT Create
        else {
          this.cptSrvc.create(this.cptData).toPromise().then(
            res => {
              this.helper.notify('Successfully created CPT.', Global.NOTIFY_TYPE_SUCCESS);
              this.spinner.hide();
              this.cancel();
            }, error => {
              this.helper.notify(`An error occurred while creating CPT data. ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
              this.spinner.hide();
              this.cancel();
            }
          );
        }
      }
    });
  }

  cancel() {
    if (this.id > 0 && this.editMode) {
      this.unlock();
    }

    this.goBackToList();
  }

  unlock() {
    // Unlock CPT
    if (this.id > 0) {
      this.cptSrvc.unlock(this.id).subscribe(
        res => { this.spinner.hide(); },
        error => {
          this.helper.notify('An error occurred while unlocking the CPT.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }

  goBackToList() {
    this.router.navigate(['cpt/cpt-view']);
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value;
    }

    return value;
  }

  shortnameChange(val) {
    this.form.get('provisioning.qipSubnet').setValue(val + '.mrn');
  }
}
