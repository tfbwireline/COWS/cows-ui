import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CptService } from '../../../services/index';
import { Helper, SpinnerAsync } from "./../../../shared";
import { NgxSpinnerService } from 'ngx-spinner';
import { Global, ECPTCustomerType, ECPTPlanServiceTier, ECPTStatus } from '../../../shared/global';

@Component({
  selector: 'app-cpt-assignment',
  templateUrl: './cpt-assignment.component.html'
})
export class CptAssignmentComponent implements OnInit {
  cptList: any[] = [];
  provList: any[] = [];
  gkList: any[] = [];
  managerList: any[] = [];
  pmList: any[] = [];
  cancelList: any[] = [];
  spnr: SpinnerAsync;

  constructor(private helper: Helper, private router: Router, private cptSrvc: CptService, private spinner: NgxSpinnerService) {
    this.spnr = new SpinnerAsync(spinner);
   }

  ngOnInit() {
    this.spnr.manageSpinner('show');
    this.cptSrvc.getCptAssignmentStats().subscribe(res => {
      this.cptList = res.filter(i => i.cptStusId != ECPTStatus.Cancelled);

      this.provList = res.filter(i => i.cptStusId != ECPTStatus.Cancelled
        && this.helper.isEmpty(i.custShortName));

      this.gkList = res.filter(i => i.isGatekeeperNeeded && !this.helper.isEmpty(i.custShortName)
        && i.cptStusId != ECPTStatus.Cancelled);

      this.managerList = res.filter(i => i.isManagerNeeded && !i.isGatekeeperNeeded
        && !this.helper.isEmpty(i.custShortName) && i.cptStusId != ECPTStatus.Cancelled);

      this.pmList = res.filter(i => i.isPmNeeded && !i.isManagerNeeded && !i.isGatekeeperNeeded
        && !this.helper.isEmpty(i.custShortName) && i.cptStusId != ECPTStatus.Cancelled);
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });

    this.spnr.manageSpinner('show');
    this.cptSrvc.getCptCancelStat().subscribe(res => {
      this.cancelList = res;
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });
  }

  selectionChangedHandler(id: any) {
    this.router.navigate(['cpt/cpt-form/' + id]);
  }
}
