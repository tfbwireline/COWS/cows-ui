import { Component, OnInit } from '@angular/core';
import { CptService } from '../../../services/index';
import { Router } from '@angular/router';
import { Helper, SpinnerAsync } from "./../../../shared";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cpt-return',
  templateUrl: './cpt-return.component.html'
})
export class CptReturnComponent implements OnInit {
  cptList: any[] = [];
  spnr: SpinnerAsync;

  constructor(private cptSrvc: CptService, private router: Router, private helper: Helper, private spinner: NgxSpinnerService) {
    this.spnr = new SpinnerAsync(spinner);
   }

  ngOnInit() {
    this.spnr.manageSpinner('show');
    this.cptSrvc.getReturnedToSde().subscribe(res => {
      this.cptList = res;
      this.cptList.forEach(item => {
        var createdDateTime = new Date(item["createdDateTime"])
        item["createdDateTime"] = this.helper.formatDate(createdDateTime);
      })
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });
  }

  onRowPrepared(e: any) {
    if (e.rowType === 'data') {
      this.cptSrvc.checkLock(e.key).toPromise().then(res => {
        if (!this.helper.isEmpty(res)) {
          e.rowElement.className = e.rowElement.className + ' checked-in-row';
          e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
          e.rowElement.title = 'This CPT is locked by ' + res;
        }
      });
    }
  }

  selectionChangedHandler(id: any) {
    this.router.navigate(['cpt/cpt-form/' + id]);
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'CPT_' + this.helper.formatDate(new Date());
  }
}
