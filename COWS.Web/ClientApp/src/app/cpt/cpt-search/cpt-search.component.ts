import { Component, OnInit } from '@angular/core';
import { CptService } from '../../../services/index';
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { Helper } from "./../../../shared";
import { Global } from '../../../shared/global';

@Component({
  selector: 'app-cpt-search',
  templateUrl: './cpt-search.component.html'
})
export class CptSearchComponent implements OnInit {
  form: FormGroup;
  cptList: any[] = [];

  constructor(private router: Router, private helper: Helper, private cptSrvc: CptService) { }

  ngOnInit() {
    this.form = new FormGroup({
      cptCustNbr: new FormControl(null),
      companyName: new FormControl(null),
      custShortName: new FormControl(null)
    });
  }

  clear() {
    this.cptList = [];
  }

  search() {
    this.clear();
    let cptCustNbr = this.form.get('cptCustNbr').value;
    let companyName = this.form.get('companyName').value;
    let custShortName = this.form.get('custShortName').value;

    this.cptSrvc.search(this.helper.isEmpty(cptCustNbr) ? '' : cptCustNbr,
      this.helper.isEmpty(companyName) ? '' : companyName,
      this.helper.isEmpty(custShortName) ? '' : custShortName).subscribe(
        res => {
          if (res != null && res.length > 0) {
            this.cptList = res;
          }
          else {
            this.helper.notify('No CPT record to display.', Global.NOTIFY_TYPE_WARNING);
          }
        },
        error => {
          this.helper.notify('Error is retrieving CPT search results.', Global.NOTIFY_TYPE_ERROR);
        }
      );
  }

  onRowPrepared(e: any) {
    if (e.rowType === 'data') {
      this.cptSrvc.checkLock(e.key).toPromise().then(res => {
        if (!this.helper.isEmpty(res)) {
          e.rowElement.className = e.rowElement.className + ' checked-in-row';
          e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
          e.rowElement.title = 'This CPT is locked by ' + res;
        }
      });
    }
  }

  selectionChangedHandler(id: any) {
    this.router.navigate(['cpt/cpt-form/' + id]);
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'CPT_' + this.helper.formatDate(new Date());
  }
}
