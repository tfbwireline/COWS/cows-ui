import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-mds-list-view',
    templateUrl: './mds-list-view.component.html'
  })
  export class MdsListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.MDS;
    }  
    
  }  
