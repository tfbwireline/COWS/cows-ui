import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-ad-broadband-list-view',
    templateUrl: './ad-broadband-list-view.component.html'
  })
  export class ADBroadbandListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.ADB;
    }  
    
  }