import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-ad-broadband-calendar',
  templateUrl: './ad-broadband-calendar.component.html'
})
export class ADBroadbandCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.ADB;
  }  

}
