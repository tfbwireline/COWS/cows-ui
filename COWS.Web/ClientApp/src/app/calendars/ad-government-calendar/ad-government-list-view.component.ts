import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-ad-government-list-view',
    templateUrl: './ad-government-list-view.component.html'
  })
  export class ADGovernmentListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.ADG;
    }  
    
  }