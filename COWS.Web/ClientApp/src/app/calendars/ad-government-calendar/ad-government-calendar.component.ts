import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-ad-government-calendar',
  templateUrl: './ad-government-calendar.component.html'
})
export class ADGovernmentCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.ADG;
  }  

}
