import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-ngvn-calendar',
  templateUrl: './ngvn-calendar.component.html'
})
export class NgvnCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.NGVN;
  }  

}
