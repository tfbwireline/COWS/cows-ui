import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-ngvn-list-view',
    templateUrl: './ngvn-list-view.component.html'
  })
  export class NgvnListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.NGVN;
    }  
    
  }  
