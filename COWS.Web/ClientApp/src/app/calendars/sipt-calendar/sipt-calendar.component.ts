import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-sipt-calendar',
  templateUrl: './sipt-calendar.component.html'
})
export class SiptCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.SIPT;
  }  

}