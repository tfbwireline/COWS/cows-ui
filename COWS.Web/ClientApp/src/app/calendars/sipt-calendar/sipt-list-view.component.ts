import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-sipt-list-view',
    templateUrl: './sipt-list-view.component.html'
  })
  export class SiptListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.SIPT;
    }  
    
  }  
