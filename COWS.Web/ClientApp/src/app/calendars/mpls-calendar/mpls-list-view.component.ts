import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-mpls-list-view',
    templateUrl: './mpls-list-view.component.html'
  })
  export class MplsListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.MPLS;
    }  
    
  }  
