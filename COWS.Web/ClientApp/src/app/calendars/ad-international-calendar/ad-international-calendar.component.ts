import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-ad-international-calendar',
  templateUrl: './ad-international-calendar.component.html'
})
export class ADInternationalCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.ADI;
  }  

}
