import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-ad-international-list-view',
    templateUrl: './ad-international-list-view.component.html'
  })
  export class ADInternationalListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.ADI;
    }  
    
  }
