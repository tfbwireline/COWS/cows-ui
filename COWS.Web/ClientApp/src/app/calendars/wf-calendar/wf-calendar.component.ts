import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Calendar } from "./../../../models/index";
import { CalendarService, UserService, EventViewsService } from "../../../services/index";
import { Global } from "../../../shared/global";
import { Appointment } from './../../../models/appointment-model';
import { ApptType } from './../../../models/appointment-types.model';
import { Resource } from './../../../models/appointment-resource.model';
import { DatePipe } from '@angular/common';

@Component
  (
    {
      selector: 'app-wf-calendar',
      templateUrl: './wf-calendar.component.html',
      styleUrls: ['./wf-calendar.component.css']
    }
  )

export class WfCalendarComponent implements OnInit {
  currentView = "month";
  currentDate: Date = new Date();
  calendarData = new Calendar();
  private alive: boolean; // used to unsubscribe from the TimerObservable
  // when OnDestroy is called.
  private interval: number;

  // Data for displaying calendar appointments to dx-scheduler
  appointmentsData: Appointment[] = [];
  apptTypeData: ApptType[] = [];
  resourceData: Resource[] = [];

  appointment: Appointment;
  appointmentType: ApptType;
  appointmentResource: Resource;

  // Data from the API call
  appointmentData: any; 
  appointmentTypeData: any; 
  appointmentResouceData: any;

  defaultId: number;
  calendarViewDetailsList = <any>[];

  constructor(private router: Router, private helper: Helper, private spinner: NgxSpinnerService,
    private calendarSrvc: CalendarService, private userService: UserService, private datePipe: DatePipe
  ) {
    this.alive = true;
    this.interval = 10000;
  }

  ngOnInit() 
  {
    // For Testing Only
    //this.update();
    this.defaultId = this.userService.loggedInUser.userId;

    // Get the Appointment Types
    this.getApptTypeData();

    // Updated by Sarah Sandoval [20200611] - Incorporate the two methods
    // getApptTypeData() and getResourcesData() is calling the same api
    // Get the Resources
    //this.getResourcesData();

    // Get Calendar data
    this.getCalendarData(this.defaultId);

    // this.eventService.getCalendarViewDetails(101, 1).subscribe(
    //   data => {
    //     this.calendarViewDetailsList = data;
    //   });
  }

  getApptTypeData() 
  {
    this.calendarSrvc.getApptTypes(1, 1).subscribe(
      res => {
        this.appointmentTypeData = res.table; 
        for(var i=0; i < this.appointmentTypeData.length; i++)
        {
          this.appointmentType = new ApptType();
          this.appointmentType.id = this.appointmentTypeData[i].appT_TYPE_ID;
          this.appointmentType.text = this.appointmentTypeData[i].appT_TYPE_DES;
          this.appointmentType.color = "#fcb65e";

          this.apptTypeData.push(this.appointmentType);
        }

        this.appointmentResouceData = res.table1;
        for (var i = 0; i < this.appointmentResouceData.length; i++) {
          this.appointmentResource = new Resource();
          this.appointmentResource.id = this.appointmentResouceData[i].id;
          this.appointmentResource.text = this.appointmentResouceData[i].model;
          this.appointmentResource.color = "#fcb65e";

          this.resourceData.push(this.appointmentResource);
        }
      }
    )
  }

  getResourcesData()
  {
    this.calendarSrvc.getApptTypes(1).subscribe(
      res => {
        this.appointmentResouceData = res.table1; 
        for(var i=0; i < this.appointmentResouceData.length; i++)
        {
          this.appointmentResource = new Resource();
          this.appointmentResource.id = this.appointmentResouceData[i].id;
          this.appointmentResource.text = this.appointmentResouceData[i].model;
          this.appointmentResource.color = "#fcb65e"; 
          
          this.resourceData.push(this.appointmentResource);
        }
      }
    )
  }

  getCalendarData(userId: number) 
  {
      this.spinner.show();
      this.calendarSrvc.getCalendarData(1, userId, 0).subscribe (
      
      res => {
        this.appointmentData = res["table"];
        
        for(var i=0; i < this.appointmentData.length; i++) 
        {          
          this.appointment = new Appointment();
          
          this.appointment.appointmentId = this.appointmentData[i].appT_ID;
          this.appointment.text = this.appointmentData[i].subJ_TXT;

          let startDate = new Date(this.appointmentData[i].strT_TMST);
          let endDate = new Date(this.appointmentData[i].enD_TMST);

          this.appointment.startDate = this.appointmentData[i].strT_TMST;
          this.appointment.endDate   = this.appointmentData[i].enD_TMST;
        
          this.appointment.allDay = this.isAllday(startDate.getHours(), startDate.getMinutes(), endDate.getHours(), endDate.getMinutes() );
          
          this.appointment.description = this.appointmentData[i].des;
          
          this.appointment.recurrenceRule = this.appointmentData[i].rcurnC_DES_TXT;

          // Appointment data additonal fields.
          this.appointment.location = this.appointmentData[i].appT_LOC_TXT;
          this.appointment.apptType = Number(this.appointmentData[i].appT_TYPE_ID);          
          
          // Logic to Build Resource Object
          let currentResource: Resource; 
          let currentResourceData: Resource[] = [];

          let resourceIds = [];

          let resourceText = this.appointmentData[i].asN_TO_USER_ID_LIST_TXT;
          if (resourceText)
          {
            var dom = new DOMParser().parseFromString(resourceText, "text/xml");
            let resourceId = dom.documentElement.getElementsByTagName("ResourceId");

            for(var y=0; y < resourceId.length; y++) 
            {              
              resourceIds.push( Number(resourceId[y].getAttribute("Value")) );
            }
          }
          
          for(var x=0; x < this.resourceData.length; x++)
          {
            if (resourceIds.indexOf(this.resourceData[x].id) > -1) 
            {
              currentResource = new Resource();
              currentResource.id = this.resourceData[x].id;
              currentResource.text = this.resourceData[x].text;
              currentResource.color = "#fcb65e";

              currentResourceData.push(currentResource);
            }
          }

          this.appointment.resource =  currentResourceData;              
          // Add Calendar Appointment info to Appointments array for display.
          this.appointmentsData.push(this.appointment);

          // printLog1(x);
        }
        this.spinner.hide();
      }, err => {            
            this.helper.notifySavedFormMessage("WF Calendar", Global.NOTIFY_TYPE_ERROR, false, err.error.message);
            this.spinner.hide();
        }, () => { this.spinner.hide();} 
    );    
  }

  onAppointmentFormOpening(e) 
  {
    //console.log('Call onAppontmentFormOpening');
    let form = e.form
    //form.option("height", "80%")
    //form.option("width", "80%")
    let formItems = form.option("items")

    //Remove all day option
    formItems[0].items[2].items[0].visible = false;
    form.option('items', formItems)
    //form.itemOption('allDay', { visible: false });

    // Set the subject field as required
    if (formItems.find((i: { dataField: string; }) => i.dataField === "text"))
    {
      form.itemOption('text', 'isRequired', true);
    }

    // appointment id - apptId
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "appointmentId")) {
      formItems.push
        (
          {
            label:
            {
              text: " "
            },
            editorType: "dxNumberBox",
            dataField: "appointmentId",
            colSpan: "2",
            editorOptions:
            {
              width: "80%",
              visible: false
            },
            
          }
        );
      form.option("items", formItems);
    }

    // Location Field  
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "location")) {
      formItems.push
        (
          {
            label:
            {
              text: "Location"
            },
            editorType: "dxTextBox",
            dataField: "location",
            colSpan: "2",
            editorOptions:
            {
              width: "80%"
            }
          }
        );
      form.option("items", formItems);
    }

    // Appt Type field
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "apptType")) {
      formItems.push
        (
          {
            label:
            {
              text: "Appt Type"
            },
            editorType: "dxSelectBox",
            dataField: "apptType",
            colSpan: "2",
            isRequired: true,
            editorOptions:
            {
              items: this.apptTypeData,
              displayExpr: "text",
              valueExpr: "id",
              width: "80%",
              searchEnabled: true,
              searchMode: "startswith",
              searchExpr: "text"
            },
          }
        );
      form.option("items", formItems);
    }

    // Resource field
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "resource")) {
      formItems.push
        (
          {
            label:
            {
              text: "Resource"
            },
            editorType: "dxTagBox",
            dataField: "resource",
            colSpan: "2",
            isRequired: true,
            editorOptions:
            {
              items: this.resourceData,
              displayExpr: "text",
              valueExpr: "id",
              width: "80%",
              showSelectionControls: true,
              showClearButton: true,
              formControlName: "resource",
              searchEnabled: true,
              searchExpr: "text",
              searchMode: "contains"
            },
          }
        );
      form.option("items", formItems);
    }

    form.itemOption("startDate",
      {
        visible: true,
        colSpan: "2"
      }
    );
    form.itemOption("endDate",
      {
        visible: true,
        colSpan: "2"
      }
    );
  }

  onAppointmentUpdated(e) {
    //console.log('Call onAppointmentUpdated');
    let appointmentData = e.appointmentData;
    appointmentData.delFlg = 0;
    
    let resourceIds = "<ResourceIds>";
    for(var x=0; x < appointmentData.resource.length; x++)
    {

      if (appointmentData.resource[x].id > 1)
      {
        resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x].id  + "\" />";
      }

      if (appointmentData.resource[x] > 1)
      {
        resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x]  + "\" />";
      }
      
    }
    resourceIds = resourceIds + "</ResourceIds>";
    appointmentData.resource = resourceIds;
    
    this.printLog(appointmentData);

    // Update Appointment
    this.processAppointment(appointmentData);
  }

  onAppointmentAdded(e) 
  {
    //console.log('Call onAppointmentAdded');
    let appointmentData = e.appointmentData;
    appointmentData.appointmentId = -1;
    appointmentData.delFlg = 0;
        
    let resourceIds = "<ResourceIds>";
    for(var x=0; x < appointmentData.resource.length; x++)
    {
      resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x]  + "\" />";
    } 
    resourceIds = resourceIds + "</ResourceIds>";
    appointmentData.resource = resourceIds;

    // Create new appointment
    this.processAppointment(appointmentData);
  }

  onAppointmentDeleted(e) {
    //console.log('Call onAppointmentDeleted');
    let appointmentData = e.appointmentData;
    appointmentData.delFlg = 1;

    let resourceIds = "<ResourceIds>";
    for(var x=0; x < appointmentData.resource.length; x++)
    {
      resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x]  + "\" />";
    } 
    resourceIds = resourceIds + "</ResourceIds>";
    appointmentData.resource = resourceIds;

    // Delete appointment
    this.processAppointment(appointmentData);
  }

  onButtonClick(e) {
    //notify("Go to " + e.component.option("text") + "'s profile", "success", 600);
  }

  onItemClick(e) {
    //notify(e.itemData.text || e.itemData, "success", 600);
  }

  buttonOptions: any = {
    text: "Register",
    type: "success",
    useSubmitBehavior: true
  }

  baseOptions: Array<any> = [
    { text: "OK", type: "normal" },
    { text: "Apply", type: "success" },
    { text: "Done", type: "default" },
    { text: "Delete", type: "danger" }
  ];

  processAppointment(appointmentData: any)
  {
    this.spinner.show();
    var newAppointment = new Calendar();

    newAppointment.apptId = appointmentData.appointmentId;
    newAppointment.subjTxt = appointmentData.text;

    newAppointment.strtTmst = appointmentData.startDate;
    newAppointment.endTmst = appointmentData.endDate;
    
    newAppointment.rcurncCd = 0;   
    
    if (appointmentData.description) 
    {
      newAppointment.des = appointmentData.description;
    } 
    else 
    {
      newAppointment.des = "";
    }
        
    newAppointment.rcurncDesTxt = "";
    if (appointmentData.recurrenceRule)
    {
      newAppointment.rcurncDesTxt = this.helper.formatRecurrenceDesText(appointmentData.recurrenceRule, appointmentData.endDate);
      newAppointment.rcurncCd = 1;
    }
    
    if (appointmentData.location)
    {
      newAppointment.apptLocTxt = appointmentData.location;
    }
    else 
    {
      newAppointment.apptLocTxt = "";
    }
    
    newAppointment.apptTypeId = appointmentData.apptType;
    newAppointment.asnToUserIdListTxt = appointmentData.resource.toString();

    newAppointment.creatByUserId = this.userService.loggedInUser.userId;
    newAppointment.modfdByUserId = this.userService.loggedInUser.userId;
    newAppointment.delFlg = appointmentData.delFlg;

    //TimerObservable.create(0, this.interval)
    //  .takeWhile(() => this.alive)
    //  .subscribe(() => {
        this.calendarSrvc.createUpdateCalendarData(newAppointment).subscribe(res => {
          this.helper.notifySavedFormMessage("WF Calendar", Global.NOTIFY_TYPE_SUCCESS, false, null);
        }
          , error => {
            this.helper.notifySavedFormMessage("WF Calendar", Global.NOTIFY_TYPE_ERROR, false, error);
            this.spinner.hide();
          }
          , () => {
            this.spinner.hide();
          });
      //});
    // Get the latest appointments
    // Get Calendar data
    this.appointmentsData = [];
 
    if ((!this.defaultId) || this.defaultId == 0 || this.defaultId == -1 )
    {
      this.defaultId = this.userService.loggedInUser.userId;
    } else {
      //console.log("this.defaultId is = " + this.defaultId);
    }

    setTimeout(() => {
      this.getCalendarData(this.defaultId);
    }, 2000);
  }

  onResourceSelect(e)
  {
    this.resetAppointmentsDataView();
    let userId = this.defaultId = Number(e.value);
    this.getCalendarData(userId);
  }

  resetAppointmentsDataView(){
    this.appointmentsData = [];
  }

  isAllday(s_hour: number, s_minute: number, e_hour: number, e_minute: number) 
  {
    var allDay = ((s_hour + s_minute + e_hour + e_minute) == 0) ? true : false;
    return allDay;
  } 

  // For Testing Only
  update() {
    this.calendarData.apptId = 9138;
    this.calendarData.apptLocTxt = 'Test 2';
    this.calendarData.apptTypeId = 14;
    
    this.calendarData.asnToUserIdListTxt = '<ResourceIds><ResourceId Type="System.Int32" Value="7002" /><ResourceId Type="System.Int32" Value="7001" /><ResourceId Type="System.Int32" Value="6241" /></ResourceIds>';
    //this.calendarData.asnToUserIdListTxt = '7001,6241,7002';
    
    this.calendarData.creatByUserId = 6241;
    this.calendarData.delFlg = 0;
    this.calendarData.des = 'testing rec appt';
    this.calendarData.endTmst = new Date;
    this.calendarData.modfdByUserId = 6241;
    this.calendarData.rcurncCd = 1;
    
    this.calendarData.rcurncDesTxt = '<RecurrenceInfo Start="07/17/2019 09:00:00" End="08/30/2019 00:30:00" Id="3371cbb0-9180-45fe-8a6f-72643706660b" OccurrenceCount="45" Range="2" FirstDayOfWeek="0" Version="1" />';
    //this.calendarData.rcurncDesTxt = 'FREQ=WEEKLY;UNTIL=20190806T155959Z;BYDAY=TU';
    
    this.calendarData.strtTmst = new Date;
    this.calendarData.subjTxt = 'Test NGVN 111';

    this.calendarSrvc.createUpdateCalendarData(this.calendarData).subscribe(res => {
      this.helper.notifySavedFormMessage("WF Calendar", Global.NOTIFY_TYPE_SUCCESS, false, null);
    }, error => {
        this.helper.notifySavedFormMessage("WF Calendar", Global.NOTIFY_TYPE_ERROR, false, error);
      this.spinner.hide();
    }, () => {
      this.spinner.hide();
    });
  }

  listViewAction() {
    // window.location.href = window.location.pathname + "/" +1; //working direct URL
    // this.router.navigate(['calendars/wf-calendar', 1]); //working route path with ID
    this.router.navigate(['calendars/wf-calendar/list']); //re route to wf component -> custom all calendars 
  }
  
  printLog(appointmentData) {
    console.log("Subject => " + appointmentData.text);
    console.log("startDate => " +appointmentData.startDate);
    console.log("endDate => " +appointmentData.endDate);
    console.log("allDay => " +appointmentData.allDay);

    console.log("description => " +appointmentData.description);
    console.log("recurrenceRule => " +appointmentData.recurrenceRule);

    console.log("appointmentId => " +appointmentData.appointmentId);
    console.log("location => " +appointmentData.location);
    console.log("apptType => " +appointmentData.apptType);
    console.log("resource => " +appointmentData.resource);
  }

  printLog1(i: number){
    console.log("appT_ID => " + this.appointmentData[i].appT_ID );
    console.log("subJ_TXT ==> " +  this.appointmentData[i].subJ_TXT );
    console.log("strT_TMST ==> " +  this.appointmentData[i].strT_TMST );
    console.log("enD_TMST ==> " +  this.appointmentData[i].enD_TMST );
    console.log("des ==> " +  this.appointmentData[i].des );
    console.log("rcurnC_CD ==> " +  this.appointmentData[i].rcurnC_CD );
    console.log("rcurnC_DES_TXT ==> " +  this.appointmentData[i].rcurnC_DES_TXT );
    console.log("appT_LOC_TXT ==> " +  this.appointmentData[i].appT_LOC_TXT );
    console.log("appT_TYPE_ID ==> " +  this.appointmentData[i].appT_TYPE_ID );
    console.log("asN_TO_USER_ID_LIST_TXT ==> " +  this.appointmentData[i].asN_TO_USER_ID_LIST_TXT );
  }

  customizeDateNavigatorText = (e) => {
    if (!this.currentView || this.currentView === 'day')
      return this.datePipe.transform(e.startDate, 'EEEE, d MMMM y');

    if (this.currentView === 'month')
      return this.datePipe.transform(e.startDate, 'MMMM y');

    return e.startDate.getDate() + "-" + this.datePipe.transform(e.endDate, 'd MMMM y');
  }
}

