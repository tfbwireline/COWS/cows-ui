import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-wf-list-view',
    templateUrl: './wf-list-view.component.html'
  })
  export class WfListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}
  
    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.WF;
      // this.apptTypeId = 1;
    }  
    
  }  
