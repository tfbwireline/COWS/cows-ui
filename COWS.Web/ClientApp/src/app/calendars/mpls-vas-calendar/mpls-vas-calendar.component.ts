import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-mpls-vas-calendar',
  templateUrl: './mpls-vas-calendar.component.html'
})
export class MplsVasCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.MPLSVAS;
  }  

}
