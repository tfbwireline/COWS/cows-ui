import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-mpls-vas-list-view',
    templateUrl: './mpls-vas-list-view.component.html'
  })
  export class MplsVasListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.MPLSVAS;
    }  
    
  }  
