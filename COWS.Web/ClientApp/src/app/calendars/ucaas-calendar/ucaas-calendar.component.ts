import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-ucaas-calendar',
  templateUrl: './ucaas-calendar.component.html'
})
export class UCaaSCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.UCaaS;
  }  

}