import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-ucaas-list-view',
    templateUrl: './ucaas-list-view.component.html'
  })
  export class UCaaSListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.UCaaS;
    }  
    
  }  
