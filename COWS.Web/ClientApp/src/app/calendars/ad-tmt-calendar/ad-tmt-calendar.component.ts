import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-ad-tmt-calendar',
  templateUrl: './ad-tmt-calendar.component.html'
})
export class ADTMTCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.ADTMT;
  }  

}
