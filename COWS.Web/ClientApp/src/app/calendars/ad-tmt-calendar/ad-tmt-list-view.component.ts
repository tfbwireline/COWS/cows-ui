import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-ad-tmt-list-view',
    templateUrl: './ad-tmt-list-view.component.html'
  })
  export class ADTMTListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.ADTMT;
    }  
    
  }  
