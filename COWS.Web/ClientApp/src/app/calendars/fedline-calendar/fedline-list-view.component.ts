import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-fedline-list-view',
    templateUrl: './fedline-list-view.component.html'
  })
  export class FedlineListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.Fedline;
    }  
    
  }  
