import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Calendar } from "./../../../models/index";
import { CalendarService, UserService } from "../../../services/index";
import { Global } from "../../../shared/global";
import { Appointment } from './../../../models/appointment-model';
import { ApptType } from './../../../models/appointment-types.model';
import { Resource } from './../../../models/appointment-resource.model';
import { DatePipe } from '@angular/common';

@Component
  (
    {
      selector: 'app-es-calendar',
      templateUrl: './es-calendar.component.html',
      styleUrls: ['./es-calendar.component.css']
    }
  )

export class ESCalendarComponent implements OnInit {
  currentView = "month";
  currentDate: Date = new Date();
  calendarData = new Calendar();

  // Data for displaying calendar appointments to dx-scheduler
  appointmentsData: Appointment[] = [];
  apptTypeData: ApptType[] = [];
  resourceData: Resource[] = [];

  appointment: Appointment;
  appointmentType: ApptType;
  appointmentResource: Resource;

  // Data from the API call
  appointmentData: any = [];
  appointmentTypeData: any = [];
  appointmentResouceData: any = [];

  defaultId: number;
  allowEditing: boolean = true;

  constructor(private router: Router, private helper: Helper, private spinner: NgxSpinnerService,
    private calendarSrvc: CalendarService, private userService: UserService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.defaultId = this.userService.loggedInUser.userId;

    // Get the Appointment Types
    this.getApptTypeData();

    // Updated by Sarah Sandoval [20200611] - Incorporate the two methods
    // getApptTypeData() and getResourcesData() is calling the same api
    // Get the Resources
    //this.getResourcesData();

    // Get Calendar data
    this.getCalendarData(this.defaultId);

    console.log('currentView: ' + this.currentView)
  }

  getApptTypeData() {
    this.calendarSrvc.getApptTypes(1, 0).subscribe(
      res => {
        this.appointmentTypeData = res.table;
        for (var i = 0; i < this.appointmentTypeData.length; i++) {
          if (!(this.appointmentTypeData[i].appT_TYPE_ID == 4 || this.appointmentTypeData[i].appT_TYPE_ID == 5 || this.appointmentTypeData[i].appT_TYPE_ID == 30 || this.appointmentTypeData[i].appT_TYPE_ID == 35)) {
            this.appointmentType = new ApptType();
            this.appointmentType.id = this.appointmentTypeData[i].appT_TYPE_ID;
            this.appointmentType.text = this.appointmentTypeData[i].appT_TYPE_DES;
            this.appointmentType.color = "#fcb65e";

            this.apptTypeData.push(this.appointmentType);
          }
        }

        this.appointmentResouceData = res.table1;
        for (var i = 0; i < this.appointmentResouceData.length; i++) {
          this.appointmentResource = new Resource();
          this.appointmentResource.id = this.appointmentResouceData[i].id;
          this.appointmentResource.text = this.appointmentResouceData[i].model;
          this.appointmentResource.color = "#fcb65e";

          this.resourceData.push(this.appointmentResource);
        }
      }
    )
  }

  getResourcesData() {
    this.calendarSrvc.getApptTypes(1).subscribe(
      res => {
        this.appointmentResouceData = res.table1;

        for (var i = 0; i < this.appointmentResouceData.length; i++) {
          this.appointmentResource = new Resource();
          this.appointmentResource.id = this.appointmentResouceData[i].id;
          this.appointmentResource.text = this.appointmentResouceData[i].model;
          this.appointmentResource.color = "#fcb65e";

          this.resourceData.push(this.appointmentResource);

        }
      }
    );
  }

  getCalendarData(userId: number) {
    this.spinner.show();
    this.calendarSrvc.getCalendarData(2, userId, 0).subscribe(
      res => {
        this.appointmentData = [];
        this.appointmentData = res["table"];
        for (var i = 0; i < this.appointmentData.length; i++) {
          this.appointment = new Appointment();
          if (!this.helper.isEmpty(this.appointmentData[i].subJ_TXT)) {
            // Will break the string and get the event ID
            let eventIdLayer1 = this.appointmentData[i].subJ_TXT.split('Event ID :');
            // String must be break into 2 else there's no event ID
            if (eventIdLayer1.length > 1) {
              if (!this.helper.isEmpty(eventIdLayer1[1])) {
                // Some subject has following strings after the Event ID
                let eventIdLayer2 = eventIdLayer1[1].split(',');
                if (eventIdLayer2.length > 1) {
                  this.appointment.eventid = eventIdLayer2[0].trim();
                } else {
                  this.appointment.eventid = eventIdLayer1[1].trim();
                }
              } else {
                this.appointment.eventid = 0;
              }
            }
            else {
              this.appointment.eventid = 0;
            }
          } else {
            this.appointment.eventid = 0;
          }
          this.appointment.appointmentId = this.appointmentData[i].appT_ID;
          this.appointment.text = this.appointmentData[i].subJ_TXT;

          let startDate = new Date(this.appointmentData[i].strT_TMST);
          let endDate = new Date(this.appointmentData[i].enD_TMST);

          this.appointment.startDate = this.appointmentData[i].strT_TMST;
          this.appointment.endDate = this.appointmentData[i].enD_TMST;

          //this.appointment.allDay = this.isAllday(startDate.getHours(), startDate.getMinutes(), endDate.getHours(), endDate.getMinutes());

          this.appointment.description = this.appointmentData[i].des;

          this.appointment.createdBy = this.appointmentData[i].creaT_BY_USER;
          this.appointment.modifiedBy = this.appointmentData[i].modfD_BY_USER
          this.appointment.createdDate = this.appointmentData[i].creaT_DT != null ? this.datePipe.transform(this.appointmentData[i].creaT_DT, "MM/dd/yyyy hh:mm a") : this.appointmentData[i].creaT_DT;
          this.appointment.modifiedDate = this.appointmentData[i].modfD_DT != null ? this.datePipe.transform(this.appointmentData[i].modfD_DT, "MM/dd/yyyy hh:mm a") : this.appointmentData[i].modfD_DT;

          this.appointment.recurrenceRule = this.appointmentData[i].rcurnC_DES_TXT;

          // Appointment data additonal fields.
          this.appointment.location = this.appointmentData[i].appT_LOC_TXT;
          this.appointment.apptType = Number(this.appointmentData[i].appT_TYPE_ID);
          this.appointment.softAssigned = this.appointmentData[i].sofT_ASSIGN_CD ? "True" : "False";

          // Logic to Build Resource Object
          let currentResource: Resource;
          let currentResourceData: Resource[] = [];

          let resourceIds = [];

          let resourceText = this.appointmentData[i].asN_TO_USER_ID_LIST_TXT;
          if (resourceText) {
            var dom = new DOMParser().parseFromString(resourceText, "text/xml");
            let resourceId = dom.documentElement.getElementsByTagName("ResourceId");

            for (var y = 0; y < resourceId.length; y++) {
              resourceIds.push(Number(resourceId[y].getAttribute("Value")));
            }
          }

          for (var x = 0; x < this.resourceData.length; x++) {
            if (resourceIds.indexOf(this.resourceData[x].id) > -1) {
              currentResource = new Resource();
              currentResource.id = this.resourceData[x].id;
              currentResource.text = this.resourceData[x].text;
              currentResource.color = "#fcb65e";

              currentResourceData.push(currentResource);
            }
          }

          this.appointment.resource = currentResourceData;
          // Add Calendar Appointment info to Appointments array for display.
          this.appointmentsData.push(this.appointment);

        }
        console.log(this.appointmentsData)
        this.spinner.hide();
      }, err => {
        this.helper.notifySavedFormMessage("ES Calendar", Global.NOTIFY_TYPE_ERROR, false, err.error.message);
        this.spinner.hide();
      }, () => { this.spinner.hide(); }
    );
  }

  onAppointmentFormOpening(e) {
    //console.log('Call onAppontmentFormOpening');
    let form = e.form
    let formItems = form.option("items")

    //Remove all day option
    formItems[0].items[2].items[0].visible = false;
    form.option('items', formItems)
 

    // Set the subject field as required
    if (formItems.find((i: { dataField: string; }) => i.dataField === "text")) {
      form.itemOption('text', 'isRequired', true);
    }

    // appointment id - apptId
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "appointmentId")) {
      formItems.push
        (
          {
            label:
            {
              text: " "
            },
            editorType: "dxNumberBox",
            dataField: "appointmentId",
            colSpan: "2",
            editorOptions:
            {
              width: "80%",
              visible: false
            },

          }
        );
      form.option("items", formItems);
    }

    let eventID = e.appointmentData.eventid;
    let eventURL = "event/mds/" + eventID;

    //if (this.apptTypeId == CalendarEventType.ADB
    //  || this.apptTypeId == CalendarEventType.ADG
    //  || this.apptTypeId == CalendarEventType.ADI
    //  || this.apptTypeId == CalendarEventType.ADN
    //  || this.apptTypeId == CalendarEventType.ADTMT
    //  || this.apptTypeId == CalendarEventType.Fedline
    //  || this.apptTypeId == CalendarEventType.MDS
    //  || this.apptTypeId == CalendarEventType.MPLS
    //  || this.apptTypeId == CalendarEventType.MPLSVAS
    //  || this.apptTypeId == CalendarEventType.NGVN
    //  || this.apptTypeId == CalendarEventType.SIPT
    //  || this.apptTypeId == CalendarEventType.SLNK
    //  || this.apptTypeId == CalendarEventType.UCaaS) {
    //  let eventID = e.appointmentData.eventid;

    //  switch (this.apptTypeId) {
    //    case CalendarEventType.ADB: eventURL = eventURL + "access-delivery/" + eventID; break;
    //    case CalendarEventType.ADG: eventURL = eventURL + "access-delivery/" + eventID; break;
    //    case CalendarEventType.ADI: eventURL = eventURL + "access-delivery/" + eventID; break;
    //    case CalendarEventType.ADN: eventURL = eventURL + "access-delivery/" + eventID; break;
    //    case CalendarEventType.ADTMT: eventURL = eventURL + "access-delivery/" + eventID; break;
    //    case CalendarEventType.Fedline: eventURL = eventURL + "fedline/" + eventID; break;
    //    case CalendarEventType.MDS: eventURL = eventURL + "mds/" + eventID; break;
    //    case CalendarEventType.MPLS: eventURL = eventURL + "mpls/" + eventID; break;
    //    case CalendarEventType.MPLSVAS: eventURL = eventURL + "mpls/" + eventID; break;
    //    case CalendarEventType.NGVN: eventURL = eventURL + "ngvn/" + eventID; break;
    //    case CalendarEventType.SIPT: eventURL = eventURL + "sipt/" + eventID; break;
    //    case CalendarEventType.SLNK: eventURL = eventURL + "sprint-link/" + eventID; break;
    //    case CalendarEventType.UCaaS: eventURL = eventURL + "ucaas/" + eventID; break;
    //    default:
    //      // do nothing
    //      break;
    //  }

    // Event ID Field Link
    if (eventID == undefined) {
      this.allowEditing = true;;
    }
    else {
      if (eventID != 0) {
        this.allowEditing = false;
        if (!formItems.find((i: { dataField: string; }) => i.dataField === "eventid")) {
          formItems.push(
            {
              label:
              {
                text: "Event ID"
              },
              editorType: "dxButton",
              dataField: "eventid",
              colSpan: "2",
              editorOptions:
              {
                width: "80%",
                text: [eventID],
                disabled: false,
                stylingMode: "contained",
                type: "default",
                onClick: function (data) {
                  //window.open(eventURL, '_blank')
                  window.location.href = eventURL;
                }
              }
            });
          form.option("items", formItems);
        }
        else {
          form.itemOption("eventid",
            {
              label:
              {
                text: "Event ID"
              },
              editorType: "dxButton",
              dataField: "eventid",
              colSpan: "2",
              editorOptions:
              {
                width: "80%",
                text: [eventID],
                disabled: false,
                stylingMode: "contained",
                type: "default",
                onClick: function (data) {
                  window.location.href = eventURL;
                }
              }
            });
        }
      }
      else {
        this.allowEditing = true;
        formItems.pop(
          {
            label:
            {
              text: "Event ID"
            },
            editorType: "dxButton",
            dataField: "eventid",
            colSpan: "2",
            editorOptions:
            {
              width: "80%",
              text: [eventID],
              disabled: false,
              stylingMode: "contained",
              type: "default",
              onClick: function (data) {
                //window.open(eventURL, '_blank')
                window.location.href = eventURL;
              }
            }
          });
        form.option("items", formItems);
      }
    }
    //}
    //else {
    //  if (formItems.find((x: { dataField: string; }) => x.dataField === "eventid")) {
    //    let index = formItems.findIndex((y: { dataField: string; }) => y.dataField === "eventid");
    //    formItems.splice(index, 1);
    //  }
    //}

    // Location Field  
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "location")) {
      formItems.push
        (
          {
            label:
            {
              text: "Location"
            },
            editorType: "dxTextBox",
            dataField: "location",
            colSpan: "2",
            editorOptions:
            {
              width: "80%"
            }
          }
        );
      form.option("items", formItems);
    }

    // Appt Type field
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "apptType")) {
      formItems.push
        (
          {
            label:
            {
              text: "Appt Type"
            },
            editorType: "dxSelectBox",
            dataField: "apptType",
            colSpan: "2",
            isRequired: true,
            editorOptions:
            {
              items: this.apptTypeData,
              displayExpr: "text",
              valueExpr: "id",
              width: "80%",
              searchEnabled: true,
              searchMode: "startswith",
              searchExpr: "text"
            },
          }
        );
      form.option("items", formItems);
    }


    // Resource field
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "resource")) {
      formItems.push
        (
          {
            label:
            {
              text: "Resource"
            },
            editorType: "dxTagBox",
            dataField: "resource",
            colSpan: "2",
            isRequired: true,
            editorOptions:
            {
              items: this.resourceData,
              displayExpr: "text",
              valueExpr: "id",
              width: "80%",
              showSelectionControls: true,
              showClearButton: true,
              formControlName: "resource",
              searchEnabled: true,
              searchExpr: "text",
              searchMode: "contains"
            },
          }
        );
      form.option("items", formItems);
    }
    else {
      form.itemOption("resource",
        {
          label:
          {
            text: "Resource"
          },
          editorType: "dxTagBox",
          dataField: "resource",
          colSpan: "2",
          isRequired: true,
          editorOptions:
          {
            items: this.resourceData,
            displayExpr: "text",
            valueExpr: "id",
            width: "80%",
            showSelectionControls: true,
            showClearButton: true,
            formControlName: "resource",
            searchEnabled: true,
            searchExpr: "text",
            searchMode: "contains"
          },
        });
    }

    // Created By 
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "createdBy")) {
      formItems.push
        (
          {
            label:
            {
              text: "Created By"
            },
            editorType: "dxTextBox",
            dataField: "createdBy",
            colSpan: "2",
            editorOptions:
            {
              value: e.appointmentData.createdBy,
              readOnly: true,
              width: "80%"
            }
          }
        );
      form.option("items", formItems);
    }
    else {
      form.itemOption("createdBy",
        {
          label:
          {
            text: "Created By"
          },
          editorType: "dxTextBox",
          dataField: "createdBy",
          colSpan: "2",
          editorOptions:
          {
            value: e.appointmentData.createdBy,
            readOnly: true,
            width: "80%"
          }
        });
    }

    // Created Date 
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "createdDate")) {
      formItems.push
        (
          {
            label:
            {
              text: "Created Date"
            },
            editorType: "dxTextBox",
            dataField: "createdDate",
            colSpan: "2",
            editorOptions:
            {
              value: e.appointmentData.createdDate,
              readOnly: true,
              width: "80%"
            }
          }
        );
      form.option("items", formItems);
    }
    else {
      form.itemOption("createdDate",
        {
          label:
          {
            text: "Created Date"
          },
          editorType: "dxTextBox",
          dataField: "createdDate",
          colSpan: "2",
          editorOptions:
          {
            value: e.appointmentData.createdDate,
            readOnly: true,
            width: "80%"
          }
        });
    }

    // Modified By 
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "modifiedBy")) {
      formItems.push
        (
          {
            label:
            {
              text: "Modified By"
            },
            editorType: "dxTextBox",
            dataField: "modifiedBy",
            colSpan: "2",
            editorOptions:
            {
              value: e.appointmentData.modifiedBy,
              readOnly: true,
              width: "80%"
            }
          }
        );
      form.option("items", formItems);
    }
    else {
      form.itemOption("modifiedBy",
        {
          label:
          {
            text: "Modified By"
          },
          editorType: "dxTextBox",
          dataField: "modifiedBy",
          colSpan: "2",
          editorOptions:
          {
            value: e.appointmentData.modifiedBy,
            readOnly: true,
            width: "80%"
          }
        });
    }

    // Modified Date
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "modifiedDate")) {
      formItems.push
        (
          {
            label:
            {
              text: "Modified Date"
            },
            editorType: "dxTextBox",
            dataField: "modifiedDate",
            colSpan: "2",
            editorOptions:
            {
              value: e.appointmentData.modifiedDate,
              readOnly: true,
              width: "80%"
            }
          }
        );
      form.option("items", formItems);
    }
    else {
      form.itemOption("modifiedDate",
        {
          label:
          {
            text: "Modified Date"
          },
          editorType: "dxTextBox",
          dataField: "modifiedDate",
          colSpan: "2",
          editorOptions:
          {
            value: e.appointmentData.modifiedDate,
            readOnly: true,
            width: "80%"
          }
        });
    }
    if (eventID > 0) {
      // Soft Assign  
      // if (!formItems.find((i: { dataField: string; }) => i.dataField === "softAssigned")) {
      //   formItems.push
      //     (
      //       {
      //         label:
      //         {
      //           text: "Soft Assigned"
      //         },
      //         editorType: "dxTextBox",
      //         dataField: "softAssigned",
      //         colSpan: "2",
      //         editorOptions:
      //         {
      //           value: e.appointmentData.softAssigned,
      //           readOnly: true,
      //           width: "80%"
      //         }
      //       }
      //     );
      //   form.option("items", formItems);
      // }
      // else {
      //   form.itemOption("softAssigned",
      //     {
      //       label:
      //       {
      //         text: "Soft Assigned"
      //       },
      //       editorType: "dxTextBox",
      //       dataField: "softAssigned",
      //       colSpan: "2",
      //       editorOptions:
      //       {
      //         value: e.appointmentData.softAssigned,
      //         readOnly: true,
      //         width: "80%"
      //       }
      //     });
      // }
    }

    form.itemOption("startDate",
      {
        visible: true,
        colSpan: "2"
      }
    );
    form.itemOption("endDate",
      {
        visible: true,
        colSpan: "2"
      }
    );
  }

  onAppointmentUpdated(e) {
    //console.log('Call onAppointmentUpdated');
    let appointmentData = e.appointmentData;
    appointmentData.delFlg = 0;

    let resourceIds = "<ResourceIds>";
    for (var x = 0; x < appointmentData.resource.length; x++) {

      if (appointmentData.resource[x].id > 1) {
        resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x].id + "\" />";
      }

      if (appointmentData.resource[x] > 1) {
        resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x] + "\" />";
      }

    }
    resourceIds = resourceIds + "</ResourceIds>";
    appointmentData.resource = resourceIds;

    // Update Appointment
    this.processAppointment(appointmentData);

  }

  onAppointmentAdded(e) {
    //console.log('Call onAppointmentAdded');

    let appointmentData = e.appointmentData;
    appointmentData.appointmentId = -1;
    appointmentData.delFlg = 0;

    let resourceIds = "<ResourceIds>";
    for (var x = 0; x < appointmentData.resource.length; x++) {
      if (appointmentData.resource[x].id > 1) {
        resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x].id + "\" />";
      }

      if (appointmentData.resource[x] > 1) {
        resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x] + "\" />";
      }
      //resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x]  + "\" />";
    }
    resourceIds = resourceIds + "</ResourceIds>";
    appointmentData.resource = resourceIds;

    // Create new appointment
    this.processAppointment(appointmentData);

  }

  onAppointmentDeleted(e) {
    console.log(e)

    //console.log('Call onAppointmentDeleted');
    let appointmentData = e.appointmentData;
    appointmentData.delFlg = 1;

    let resourceIds = "<ResourceIds>";
    for (var x = 0; x < appointmentData.resource.length; x++) {
      if (appointmentData.resource[x].id > 1) {
        resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x].id + "\" />";
      }

      if (appointmentData.resource[x] > 1) {
        resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x] + "\" />";
      }
      //resourceIds = resourceIds + "<ResourceId Type=\"System.Int32\" Value=\"" + appointmentData.resource[x]  + "\" />";
    }
    resourceIds = resourceIds + "</ResourceIds>";
    appointmentData.resource = resourceIds;

    // Delete appointment
    this.processAppointment(appointmentData);
  }

  onButtonClick(e) {
    //notify("Go to " + e.component.option("text") + "'s profile", "success", 600);
  }

  onItemClick(e) {
    //notify(e.itemData.text || e.itemData, "success", 600);
  }

  buttonOptions: any = {
    text: "Register",
    type: "success",
    useSubmitBehavior: true
  }

  baseOptions: Array<any> = [
    { text: "OK", type: "normal" },
    { text: "Apply", type: "success" },
    { text: "Done", type: "default" },
    { text: "Delete", type: "danger" }
  ];


  processAppointment(appointmentData: any) {
    this.spinner.show();
    var newAppointment = new Calendar();

    newAppointment.apptId = appointmentData.appointmentId;
    newAppointment.subjTxt = appointmentData.text;

    newAppointment.strtTmst = this.helper.dateObjectToString(appointmentData.startDate);
    newAppointment.endTmst = this.helper.dateObjectToString(appointmentData.endDate);

    newAppointment.rcurncCd = 0;

    if (appointmentData.description) {
      newAppointment.des = appointmentData.description;
    }
    else {
      newAppointment.des = "";
    }

    newAppointment.rcurncDesTxt = "";
    if (appointmentData.recurrenceRule) {
      newAppointment.rcurncDesTxt = this.helper.formatRecurrenceDesText(appointmentData.recurrenceRule, appointmentData.endDate);
      newAppointment.rcurncCd = 1;
    }

    if (appointmentData.location) {
      newAppointment.apptLocTxt = appointmentData.location;
    }
    else {
      newAppointment.apptLocTxt = "";
    }

    newAppointment.apptTypeId = appointmentData.apptType;
    newAppointment.asnToUserIdListTxt = appointmentData.resource.toString();

    newAppointment.creatByUserId = this.userService.loggedInUser.userId;
    newAppointment.modfdByUserId = this.userService.loggedInUser.userId;
    newAppointment.delFlg = appointmentData.delFlg;

    this.calendarSrvc.createUpdateCalendarData(newAppointment).subscribe(res => {
      if (newAppointment.apptId == -1) {
        this.helper.notifySavedFormMessage("ES Calendar", Global.NOTIFY_TYPE_SUCCESS, true, null);
      }
      else {
        this.helper.notifySavedFormMessage("ES Calendar", Global.NOTIFY_TYPE_SUCCESS, false, null);
      }
    }
      , error => {
        this.helper.notifySavedFormMessage("ES Calendar", Global.NOTIFY_TYPE_ERROR, false, error);
        this.spinner.hide();
      }
      , () => {
        this.spinner.hide();
      });

    // Get the latest appointments
    // Get Calendar data
    this.appointmentsData = [];

    if ((!this.defaultId) || this.defaultId == 0 || this.defaultId == -1) {
      this.defaultId = this.userService.loggedInUser.userId;
    } else {
      //console.log("this.defaultId is = " + this.defaultId);
    }

    setTimeout(() => {
      //this.resetAppointmentsDataView();
      this.getCalendarData(this.defaultId);
    }, 2000);

  }

  onResourceSelect(e) {
    this.resetAppointmentsDataView();
    let userId = this.defaultId = Number(e.value);
    this.getCalendarData(userId);
  }

  resetAppointmentsDataView() {
    this.appointmentsData = [];
  }

  isAllday(s_hour: number, s_minute: number, e_hour: number, e_minute: number) {
    var allDay = ((s_hour + s_minute + e_hour + e_minute) == 0) ? true : false;

    return allDay;
  }

  // For Testing Only
  update() {
    this.calendarData.apptId = 9138;
    this.calendarData.apptLocTxt = 'Test 2';
    this.calendarData.apptTypeId = 14;

    this.calendarData.asnToUserIdListTxt = '<ResourceIds><ResourceId Type="System.Int32" Value="7002" /><ResourceId Type="System.Int32" Value="7001" /><ResourceId Type="System.Int32" Value="6241" /></ResourceIds>';
    //this.calendarData.asnToUserIdListTxt = '7001,6241,7002';

    this.calendarData.creatByUserId = 6241;
    this.calendarData.delFlg = 0;
    this.calendarData.des = 'testing rec appt';
    this.calendarData.endTmst = new Date;
    this.calendarData.modfdByUserId = 6241;
    this.calendarData.rcurncCd = 1;

    this.calendarData.rcurncDesTxt = '<RecurrenceInfo Start="07/17/2019 09:00:00" End="08/30/2019 00:30:00" Id="3371cbb0-9180-45fe-8a6f-72643706660b" OccurrenceCount="45" Range="2" FirstDayOfWeek="0" Version="1" />';
    //this.calendarData.rcurncDesTxt = 'FREQ=WEEKLY;UNTIL=20190806T155959Z;BYDAY=TU';

    this.calendarData.strtTmst = new Date;
    this.calendarData.subjTxt = 'Test NGVN 111';

    this.calendarSrvc.createUpdateCalendarData(this.calendarData).subscribe(res => {
      this.helper.notifySavedFormMessage("ES Calendar", Global.NOTIFY_TYPE_SUCCESS, false, null);
    }, error => {
      this.helper.notifySavedFormMessage("ES Calendar", Global.NOTIFY_TYPE_ERROR, false, error);
      this.spinner.hide();
    }, () => {
      this.spinner.hide();
    });
  }

  listViewAction() {
    //console.log('listViewAction');
    this.router.navigate(['/calendars/es-calendar/list']);
  }

  printLog(appointmentData) {
    console.log("Subject        => " + appointmentData.text);
    console.log("startDate      => " + appointmentData.startDate);
    console.log("endDate        => " + appointmentData.endDate);
    console.log("allDay         => " + appointmentData.allDay);

    console.log("description    => " + appointmentData.description);
    console.log("recurrenceRule => " + appointmentData.recurrenceRule);

    console.log("appointmentId  => " + appointmentData.appointmentId);
    console.log("location       => " + appointmentData.location);
    console.log("apptType       => " + appointmentData.apptType);
    console.log("resource       => " + appointmentData.resource);

    for (var x = 0; x < appointmentData.resource.length; x++) {
      console.log("resource name = " + appointmentData.resource[x].text);
    }

    console.log("=============================================================");

  }

  printLog1(i: number) {
    console.log("appT_ID => " + this.appointmentData[i].appT_ID);
    console.log("subJ_TXT ==> " + this.appointmentData[i].subJ_TXT);
    console.log("strT_TMST ==> " + this.appointmentData[i].strT_TMST);
    console.log("enD_TMST ==> " + this.appointmentData[i].enD_TMST);
    console.log("des ==> " + this.appointmentData[i].des);
    console.log("rcurnC_CD ==> " + this.appointmentData[i].rcurnC_CD);
    console.log("rcurnC_DES_TXT ==> " + this.appointmentData[i].rcurnC_DES_TXT);
    console.log("appT_LOC_TXT ==> " + this.appointmentData[i].appT_LOC_TXT);
    console.log("appT_TYPE_ID ==> " + this.appointmentData[i].appT_TYPE_ID);
    console.log("asN_TO_USER_ID_LIST_TXT ==> " + this.appointmentData[i].asN_TO_USER_ID_LIST_TXT);
  }

  deleteAppointment(e, obj) {
    e.event.stopPropagation()
    this.onAppointmentDeleted(obj)
  }

  customizeDateNavigatorText = (e) => {
    if (!this.currentView || this.currentView === 'day')
      return this.datePipe.transform(e.startDate, 'EEEE, d MMMM y');

    if (this.currentView === 'month')
      return this.datePipe.transform(e.startDate, 'MMMM y');

    return e.startDate.getDate() + "-" + this.datePipe.transform(e.endDate, 'd MMMM y');  
  }
}

