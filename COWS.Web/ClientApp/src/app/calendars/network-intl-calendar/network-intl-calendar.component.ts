import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-network-intl-calendar',
  templateUrl: './network-intl-calendar.component.html'
})
export class NetworkIntlCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor() { }

  ngOnInit() {
    this.apptTypeId = CalendarEventType.NtwkIntl;
  }

}
