import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-network-intl-list-view',
  templateUrl: './network-intl-list-view.component.html'
})
export class NetworkIntlListViewComponent implements OnInit {

  apptTypeId: number;

  constructor() { }

  ngOnInit() {
    this.apptTypeId = CalendarEventType.NtwkIntl;
  }

}  
