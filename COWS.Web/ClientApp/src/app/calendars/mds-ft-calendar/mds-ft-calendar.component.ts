import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-mds-ft-calendar',
  templateUrl: './mds-ft-calendar.component.html'
})
export class MdsftCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.MDSFT;
  }  

}
