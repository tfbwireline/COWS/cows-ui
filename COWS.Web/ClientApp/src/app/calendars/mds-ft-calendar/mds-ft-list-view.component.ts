import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-mds-ft-list-view',
    templateUrl: './mds-ft-list-view.component.html'
  })
  export class MdsftListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.MDSFT;
    }  
    
  }  
