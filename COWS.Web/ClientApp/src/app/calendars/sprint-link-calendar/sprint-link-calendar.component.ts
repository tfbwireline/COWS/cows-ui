import { NgModule, Component, OnInit, enableProdMode } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component
(
  {
    selector: 'app-sprint-link-calendar',
    templateUrl: './sprint-link-calendar.component.html'
  }
)

export class SprintLinkCalendarComponent implements OnInit 
{
  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.SLNK;
  }  
}
