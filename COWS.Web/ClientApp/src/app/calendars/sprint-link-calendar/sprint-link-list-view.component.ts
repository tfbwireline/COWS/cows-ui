import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-sprint-link-list-view',
    templateUrl: './sprint-link-list-view.component.html'
  })
  export class SprintLinkListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.SLNK;
    }  
    
  }  
