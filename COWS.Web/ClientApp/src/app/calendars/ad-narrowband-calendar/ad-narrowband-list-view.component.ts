import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
    selector: 'app-ad-narrowband-list-view',
    templateUrl: './ad-narrowband-list-view.component.html'
  })
  export class ADNarrowbandListViewComponent implements OnInit {
  
    apptTypeId: number;

    constructor(){}

    ngOnInit() 
    {
      this.apptTypeId = CalendarEventType.ADN;
    }  
    
  }
