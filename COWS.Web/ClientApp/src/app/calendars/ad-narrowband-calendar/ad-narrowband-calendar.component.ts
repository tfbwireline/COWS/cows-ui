import { Component, OnInit } from '@angular/core';
import { CalendarEventType } from '../../../shared/global';

@Component({
  selector: 'app-ad-narrowband-calendar',
  templateUrl: './ad-narrowband-calendar.component.html'
})
export class ADNarrowbandCalendarComponent implements OnInit {

  apptTypeId: number;

  constructor(){}

  ngOnInit() 
  {
    this.apptTypeId = CalendarEventType.ADN;
  }  

}
