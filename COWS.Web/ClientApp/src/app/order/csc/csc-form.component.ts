import { Component, OnInit, ViewChild } from '@angular/core';
import { WorkGroup, Global } from "./../../../shared/global";
import { Helper } from '../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { zip } from 'rxjs';
import { WorkGroupService, OrderLockService, UserProfileService, UserService, OrderNoteService, FsaOrdersService, AmnciService, CCDBypassService, FsaOrderCpeLineItemService } from '../../../services';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { String } from 'typescript-string-operations';
import { CSCService } from '../../../services/csc.service';

@Component({
  selector: 'app-csc-form',
  templateUrl: './csc-form.component.html'
})
export class CscFormComponent implements OnInit {  

  form: FormGroup
  fsaDetails = {
    id: null,
    isShown: false,
    show: (id) => {
      this.fsaDetails.id = id
      this.fsaDetails.isShown = true
    },
    hide: () => {
      this.fsaDetails.isShown = false
    }
  }
  m5DetailList = [];
  option: any

  locked: boolean = false;
  userId: number;
  userADID: string;
  workGroup: number;
  orderId: number;
  openedTaskId: number;

  hfOrderComplete: boolean = false;
  hfLockedOrderID: string = "";
  hfSelectedOrderID: string = "";
  hfSelectedDeviceOrder: string;
  hfSelectedRow: string = "";
  hfSelectedFTN: string = "";
  hfTaskID: string;
  bAdminEdit: boolean = false;
  lblError: string;
  taskID: number = 0;

  loggedInUserProfilesList: any;
  orderInfo_V5U: any;
  ftnList: any;
  tasksList: any;
  jeopardyCodeList: any;
  cpeAcctCntctsList: any;
  reAssignedTechsList: any;
  techList: any;
  cpeLineItemsList: any;
  equipOnlyInfoList: any;
  requisitionNumberList: any;
  reqHeaderQueueList: any
  reqHeaderAcctCodesList: any;
  cpepList: any;
  cpeReqLineItems: any;

  reAssignedTech: string;
  reDispatchTime: string;
  reEventId: string = "";
  selectedReAssignedTech: string = "";

  ftn: string;
  servingCPEClli: string;
  assignedOrderToADID: string;
  servingCPEClliValue: string;
  assignedOrderToADIDValue: string;
  nidSerialNumber: string;
  deviceId: string;
  eqptOnlyCD: string;
  orderType: string;
  domesticCD: string
  thirdPartySite: string;
  emailPrimaryIPMText: string;
  primaryEmail: string;
  emailPrimaryISText: string;
  secondaryEmail: string;

  selectedJeopardyCode: string = "";
  chkOrderModificationList: string[];
  selectedOrderModification: string = "";
  selectedReworkToTask: string = "";

  isEquipmentReviewDataLoaded: boolean = false;
  isMatReqDataLoaded: boolean = false;
  isEquipmentReceiptDataLoaded: boolean = false;
  isTechAssignmentDataLoaded: boolean = false;
  isOrderCompletionDataLoaded: boolean = false;
  isOrderModificationLoaded: boolean = false;
  isRTSLoaded: boolean = false;
  isReAssignLoaded: boolean = false;
  isOrderCompleted: boolean = true;
  displaySaveCommentsButton: boolean = false;
  displayOrderCompletionButtons: boolean = false;
  selectedItems: any[] = [];
  updatedItems: any[] = [];
  //matReqData = new PsReqLineItemQueue();
  noteList: [];
  ccdList: [];
  noteInfo: string = "";
  showNID: boolean = false;
  isNID: boolean = false;
  serviceSiteList: any;
  notes = '';
  installPortList = []
  installVlanList = []
  fsaOrderCpeLineItemList = []
  orderTypeDesc = "";

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute, private workGroupService: WorkGroupService,
    private orderLockService: OrderLockService, private cscService: CSCService, private userProfileService: UserProfileService,
    private userService: UserService, private router: Router, private orderNoteService: OrderNoteService, private fsaOrdersService: FsaOrdersService,
    private amnciService: AmnciService, private ccdService: CCDBypassService, private fsaOrderCpeLineItemService: FsaOrderCpeLineItemService) {
    this.bindChkOrderModificationList(0);
  }

  ngOnInit() {

    this.form = new FormGroup({
      selectedFTN: new FormControl(),
      serialNumber: new FormControl(),
      serviceLevel: new FormControl(),
      contractNumber: new FormControl(),
      vendorExpDate: new FormControl(),
      eventId: new FormControl(),
      eventStatus: new FormControl(),
      eventStartTime: new FormControl(),
      entityMaintenance: new FormControl(),
      notes: new FormControl(''),
      install: new FormGroup({
        mdsInstallationInformation: new FormGroup({
          solutionService: new FormControl(),
          networkTypeCode: new FormControl(),
          serviceTierCode: new FormControl(),
          transportOrderTypeCode: new FormControl(),
          designDocumentNo: new FormControl(),
          vendorCode: new FormControl()
        }),
        transport: new FormGroup({
          accessTypeCode: new FormControl(),
          accessArrangementCode: new FormControl(),
          encapsulationCode: new FormControl(),
          fiberHandoffCode: new FormControl(),
          carrierAccessCode: new FormControl(),
          circuitId: new FormControl(),
          serviceTerm: new FormControl(),
          quoteExpirationDate: new FormControl(),
          accessBandwidth: new FormControl(),
          privateLineNo: new FormControl(),
          m5AccessStatus: new FormControl(),
          classOfService: new FormControl()
        }),
        changeInformation: new FormGroup({
          changeDescription: new FormControl(),
          carrierServiceId: new FormControl(),
          carrierCircuitId: new FormControl(),
          originalInstallOrderId: new FormControl(),
          networkUserAddress: new FormControl(),
          portRateTypeCode: new FormControl()
        }),
        cpeInstallationInformation: new FormGroup({
          cpeOrderTypeCode: new FormControl(),
          equipmentOnlyFlagCode: new FormControl(),
          accessProvideCode: new FormControl(),
          phoneNumberType: new FormControl(),
          phoneNumber: new FormControl(),
          eccktIdentifier: new FormControl(),
          managedServicesChannelProgramCode: new FormControl(),
          deliveryDuties: new FormControl(),
          deliveryDutyAmount: new FormControl(),
          shippingChargeAmount: new FormControl(),
          recordOnly: new FormControl(),
          nuaOrCkt: new FormControl()
        })
      }),
      customer: new FormGroup({
        h1: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          branchCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl()
        }),
        h4: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl()
        }),
        h4Address: new FormGroup({
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl()
        }),
        h4Contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        }),
        h6: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          branchCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl(),
          serviceSubType: new FormControl(),
          salesOfficeIdCode: new FormControl(),
          salesPersonPrimaryCid: new FormControl(),
          salesPersonSecondaryCid: new FormControl(),
          cllidCode: new FormControl(),
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          cityCode: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl(),
          buildingName: new FormControl(),
          floor: new FormControl(),
          roomNumber: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        }),
        h6Address: new FormGroup({
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl(),
          buildingName: new FormControl(),
          floor: new FormControl(),
          roomNumber: new FormControl()
        }),
        h6Contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        })
      }),
      order: new FormGroup({
        orderAction: new FormControl(),
        ftn: new FormControl(),
        orderTypeCode: new FormControl(),
        orderSubTypeCode: new FormControl(),
        productTypeCode: new FormControl(),
        parentFtn: new FormControl(),
        relatedFtn: new FormControl(),
        telecomServicePriority: new FormControl(),
        carrierPartnerWholesaleType: new FormControl(),
        customerCommitDate: new FormControl(),
        customerWantDate: new FormControl(),
        customerOrderSubmitDate: new FormControl(),
        customerSignedDate: new FormControl(),
        orderSubmitDate: new FormControl(),
        customerPremiseCurrentlyOccupiedFlag: new FormControl(),
        willCustomerAcceptServiceEarlyFlag: new FormControl(),
        multipleOrderFlag: new FormControl(),
        multipleOrderIndex: new FormControl(),
        multipleOrderTotal: new FormControl(),
        escalatedFlag: new FormControl(),
        expediteTypeCode: new FormControl(),
        govtTypeCode: new FormControl(),
        vendorVpnCode: new FormControl(),
        prequalNo: new FormControl(),
        scaNumber: new FormControl()
      }),
      disconnect: new FormGroup({
        reasonCode: new FormControl(),
        cancelBeforeStartReasonCode: new FormControl(),
        cancelBeforeStartReasonText: new FormControl(),
        contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        })
      }),
    });

    this.userId = Number(localStorage.getItem('userID'));
    this.userADID = localStorage.getItem('userADID');
    this.workGroup = Number(this.activatedRoute.snapshot.params["WG"] || 1);
    this.orderId = Number(this.activatedRoute.snapshot.params["id"] || 0);

    this.spinner.show();

    if (this.orderId > 0) {
      this.getWGOrderData();
    }
  }

  bindChkOrderModificationList(value: number) {
    if (value == 0) {
      this.chkOrderModificationList = [
        "Add JPRDY With Hold",
        "Add JPRDY No Hold",
        "Remove JPRDY/Hold",
        "CLLI Modify",
        "Assign User",
        "Move Order Back",
        "Insert Note"
      ];
    }
    else {
      this.chkOrderModificationList = [
        "Add JPRDY With Hold",
        "Add JPRDY No Hold",
        "Remove JPRDY/Hold",
        "CLLI Modify",
        "Assign User",
        "Move Order Back",
        "Insert Note",
        "Assign NID Serial #"
      ];
    }
  }

  getWGOrderData() {
    let data = zip(
      this.workGroupService.GetWGData(this.workGroup, 0, this.orderId, false, null),
      this.workGroupService.GetFTNListDetails(this.orderId, this.workGroup),
      this.userProfileService.getMapUserProfilesByUserId(this.userId),
      this.orderNoteService.getByOrderId(this.orderId),
      this.workGroupService.GetLatestNonSystemOrderNoteInfo(this.orderId),
      this.ccdService.GetCCDHistory(this.orderId),
      this.fsaOrdersService.getById(this.orderId),
      this.fsaOrdersService.getRelatedOrdersById(this.orderId),
      this.workGroupService.GetAllSrvcSiteSupport(),
      this.fsaOrderCpeLineItemService.getByOrderId(this.orderId), // FSA CPE ORDER LINE ITEMS
    )


    data.subscribe(res => {
      let order = res[0]
      this.ftnList = res[1]
      this.loggedInUserProfilesList = res[2]
      this.noteList = res[3]
      this.noteInfo = res[4] != "" ? "Latest order notes updated by: " + res[4] : res[4]
      this.ccdList = res[5]
      let fsaOrder = res[6]
      let relatedOrdersList = res[7] as any[]
      this.serviceSiteList = res[8]
      this.fsaOrderCpeLineItemList = res[9]

      // M5 # Details
      this.m5DetailList = this.m5DetailList.concat(fsaOrder)
      this.m5DetailList = this.m5DetailList.concat(relatedOrdersList)

      // DE38942
      if(this.m5DetailList.length > 0) {
        this.orderTypeDesc = this.m5DetailList[0].ordrTypeDes.toUpperCase();
      }

      if (order.length > 0) {
        this.openedTaskId = order.map(a => a.tasK_ID)[0];
        this.hfSelectedFTN = order.map(a => a.ftn)[0];
        this.IsOrderComplete(order);

        if (!(this.hfOrderComplete)) {
          if (this.orderId != 0) {
            this.CheckIn();
          }
        }
      }

      if (!this.hfOrderComplete) {
        this.LockUnlockPageControls();

        if (!this.loggedInUserProfilesList.find(a => a.usrPrfDes.includes("CSC"))) {
          this.lblError = "Order opened in read-only mode.";
          this.LockPageControls();
        }
        else if (!(this.loggedInUserProfilesList.find(a => a.usrPrfDes === 'CSC Order RTS') || this.loggedInUserProfilesList.find(a => a.usrPrfDes === 'CSC Order Updater'))) {
          this.lblError = "User has Reviewer permissions, the order will be opened in locked mode.";
          this.LockPageControls();
        }
      }

    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      if (this.m5DetailList[0] != null) {
        this.onM5Clicked(null, this.m5DetailList[0])
      } else {
        this.spinner.hide()
      }
    });

  }

  IsOrderComplete(res: any[]) {
    let sOrderStatus = res.map(a => a.ordR_STUS)[0];
    if (sOrderStatus == null || sOrderStatus.toUpperCase() == "COMPLETED" || sOrderStatus.toUpperCase() == "CANCELLED" || sOrderStatus.toUpperCase() == "DISCONNECTED")
      this.hfOrderComplete = true;
  }

  CheckIn() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, false, 0).subscribe(
      data => {
        if (data == 1) {
          this.lblError = "This order is currently locked. Try again later.";
          this.LockPageControls();
          this.hfLockedOrderID = this.orderId.toString();
        }
      });

  }

  LockPageControls() {
    //if (!this.hfOrderComplete && this.bAdminEdit) {
    //  if (this.helper.isEmpty(this.hfSelectedOrderID))
    //    this.hfSelectedOrderID = this.orderId.toString();
    //}
    //else {
      this.form.disable();
      this.locked = true;
      //btnCancel.Enabled = true;
    //}
  }

  UnlockPageControls() {
    this.form.enable();
  }


  LockUnlockPageControls() {
    if (this.hfSelectedFTN == "") {
      this.LockPageControls();
    }
    else {
      this.EnableCSCComplete(this.hfSelectedFTN);
      if ((this.orderId.toString() != this.hfLockedOrderID) && this.taskID != 0) {
        this.UnlockPageControls();
      }
      else {
        this.LockPageControls();
      }
    }
  }

  EnableCSCComplete(ftn: string) {

    let _actionCode: number = 0;
    let _taskId: number = 0;
    let _isIntl: number = 0;
    let _prodType: string = "";
    let data: any = [];

    if (this.ftnList != null && this.ftnList.length > 0) {
      data = this.ftnList.find(a => a.ftn == ftn && a.wG_ID == this.workGroup) as any[];
    }

    if (data != null) {
      this.taskID = _taskId = data.tasK_ID;
      _actionCode = data.ordR_ACTN_ID;
      _isIntl = data.dmstC_CD == true ? 1 : 0;
      _prodType = data.proD_TYPE_CD;
    }
    if (_actionCode == 1 && _taskId == 400) {
      this.LockPageControls();
    }
    else {
      this.LoadCSCSpecificData();
    }
  }

  LoadCSCSpecificData() {
    if (this.orderId != 0) {
      this.workGroupService.GetCSCSpecificData(this.orderId).subscribe(data => {
        if (data != null) {
          this.form.get("serialNumber").setValue(data.map(a => a.serialNbr)[0]);
          this.form.get("serviceLevel").setValue(data.map(a => a.mtrlRqstnTxt)[0]);
          this.form.get("contractNumber").setValue(data.map(a => a.prchOrdrTxt)[0]);
          this.form.get("vendorExpDate").setValue(data.map(a => a.intlMntcSchrgTxt)[0]);
        }
      });

      this.workGroupService.GetMDSEventData(this.orderId).subscribe(data => {
        if (data != null) {
          this.form.get("eventId").setValue(data.map(a => a.evenT_ID)[0]);
          this.form.get("eventStatus").setValue(data.map(a => a.evenT_STUS_DES)[0]);
          this.form.get("eventStartTime").setValue(data.map(a => a.strT_TMST)[0]);
          this.form.get("entityMaintenance").setValue(data.map(a => a.srvC_ASSRN_SITE_SUPP_ID)[0]);
        }
      });
      this.form.get("selectedFTN").setValue(this.hfSelectedFTN);
    }      
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }
    return value;
  }

  //#region "Equipment Review"

  btnSave_Click() {
    let orderModel = {
      OrderId: this.orderId,
      UserId: this.userId,
      NoteTypeId: 8,
      SerialNo: this.getFormControlValue("serialNumber") == null ? String.Empty : this.getFormControlValue("serialNumber"),
      MR: this.getFormControlValue("serviceLevel") == null ? String.Empty : this.getFormControlValue("serviceLevel"),
      PO: this.getFormControlValue("contractNumber") == null ? String.Empty : this.getFormControlValue("contractNumber"),
      IMS: this.getFormControlValue("vendorExpDate") == null ? String.Empty : this.getFormControlValue("vendorExpDate"),
      NoteText: this.getFormControlValue("notes") == null ? String.Empty : this.getFormControlValue("notes"),
      IsComplete: false
    }

    this.cscService.saveCSCSpecificData(orderModel).subscribe(data => {
      if (data) {
        //this.orderCompleted();
        this.setFormControlValue("notes", "");
        this.getWGOrderData();
        this.helper.notify("CSC info has been updated.", Global.NOTIFY_TYPE_SUCCESS);
      }
      return true;
    },
      error => {
        this.helper.notify("Error while Saving CSC Specific data.", Global.NOTIFY_TYPE_ERROR);        
        return false;
      }, () => {
        this.spinner.hide();
      });
  }

  btnMove_Click() {
    let orderModel = {
      OrderId: this.orderId,
      UserId: this.userId,
      NoteTypeId: 8,
      SerialNo: this.getFormControlValue("serialNumber") == null ? String.Empty : this.getFormControlValue("serialNumber"),
      MR: this.getFormControlValue("serviceLevel") == null ? String.Empty : this.getFormControlValue("serviceLevel"),
      PO: this.getFormControlValue("contractNumber") == null ? String.Empty : this.getFormControlValue("contractNumber"),
      IMS: this.getFormControlValue("vendorExpDate") == null ? String.Empty : this.getFormControlValue("vendorExpDate"),
      NoteText: this.getFormControlValue("notes") == null ? String.Empty : this.getFormControlValue("notes"),
      IsComplete: true
    }

    this.cscService.saveCSCSpecificData(orderModel).subscribe(data => {
      if (data) {
        this.orderCompleted();
        this.helper.notify("CSC info has been updated.", Global.NOTIFY_TYPE_SUCCESS);
        this.setFormControlValue("notes", "");
      }
      return true;
    },
      error => {
        this.helper.notify("Error while Saving CSC Specific data.", Global.NOTIFY_TYPE_ERROR);  
        return false;
      }, () => {
        this.spinner.hide();
      });
  }

  btnSSNotes_Click() {
    this.router.navigate([`order/rts/156/${this.orderId}/${this.openedTaskId}`]);
  }

  getOrderNotes(nteTypeId: number, nteTxt: string) {
    return {
      ordrId: this.orderId.toString(),
      nteTypeId: nteTypeId,
      nteTxt: nteTxt == "" ? this.form.get("notes").value : nteTxt
    }
  }

  orderCompleted() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, true, 0).subscribe(
      data => {
        if (data == 0) {
            this.router.navigate(['order/csc/']);
        }
      });
  }
  //#endregion


  btnCancel_Click() {
    this.orderCompleted();    
  }
  
  onM5Clicked(e, data) {
    if (e != null) {
      (e as MouseEvent).preventDefault()
    }

    if (this.fsaDetails.isShown && this.fsaDetails.id == data.ordrId) { // Hide FSA Details
      this.fsaDetails.hide()
    } else if (!this.fsaDetails.isShown && this.fsaDetails.id == data.ordrId) { // Show already loaded
      this.fsaDetails.show(data.ordrId)
    } else { // Get new FSA Details
      this.amnciService.getFsaDetails(data.ordrId, this)
    }
  }
}
