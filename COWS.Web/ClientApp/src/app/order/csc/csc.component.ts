import { Component, OnInit } from '@angular/core';
import { WorkGroup } from "./../../../shared/global";

@Component({
  selector: 'app-csc',
  templateUrl: './csc.component.html'
})
export class CscComponent implements OnInit {
  workGroup: number;

  constructor() { }  

  ngOnInit() {
    this.workGroup = WorkGroup.CSC;
  }
}
