import { Component, OnInit, ViewChild } from '@angular/core';
import { WorkGroup, Global } from "./../../../shared/global";
import { Helper } from '../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { zip } from 'rxjs';
import { String } from 'typescript-string-operations';
import { WorkGroupService, OrderLockService, UserProfileService, UserService, OrderNoteService, FsaOrdersService, AmnciService, CCDBypassService, FsaOrderCpeLineItemService } from '../../../services';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DCPEService } from '../../../services/dcpe.service';
import { concatMap } from 'rxjs/operators';
import { AdditionalCostsComponent } from '../custom/additional-costs/additional-costs.component';

@Component({
  selector: 'app-rts-form',
  templateUrl: './rts-form.component.html'
})
export class RTSFormComponent implements OnInit {

  @ViewChild(AdditionalCostsComponent, { static: false })
  private additionalCostDetails: AdditionalCostsComponent;

  form: FormGroup
  fsaDetails = {
    id: null,
    isShown: false,
    show: (id) => {
      this.fsaDetails.id = id
      this.fsaDetails.isShown = true
    },
    hide: () => {
      this.fsaDetails.isShown = false
    }
  }
  m5DetailList = [];
  orderTypeDesc= "";
  option: any

  viewType: string = "SalesSupport"
  locked: boolean = false;
  userId: number;
  userADID: string;
  workGroup: number;
  orderId: number;
  openedTaskId: number;

  hfOrderComplete: boolean = false;
  hfLockedOrderID: string = "";
  hfSelectedOrderID: string = "";
  hfSelectedDeviceOrder: string;
  hfSelectedRow: string = "";
  hfSelectedFTN: string = "";
  hfTaskID: string;
  bAdminEdit: boolean = false;
  lblError: string;
  taskId: number = 0;

  loggedInUserProfilesList: any;
  orderInfo_V5U: any;
  ftnList: any;
  tasksList: any;
  jeopardyCodeList: any;
  cpeAcctCntctsList: any;
  reAssignedTechsList: any;
  techList: any;
  cpeLineItemsList: any;
  bUserGrpAccess: boolean = false;
  ACR: boolean = false;
  ACRTerminating: boolean = false;

  ftn: string;


  selectedItems: any[] = [];
  noteList = [];
  ccdList: [];
  noteInfo: string = "";
  notes: string = "";
  installPortList = []
  installVlanList = []
  fsaOrderCpeLineItemList = []

  get isBPARequired() {
    return this.taskId == 110 ? true : false
  }

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute, private workGroupService: WorkGroupService,
    private orderLockService: OrderLockService, private dcpeService: DCPEService, private userProfileService: UserProfileService,
    private userService: UserService, private router: Router, private orderNoteService: OrderNoteService, private fsaOrdersService: FsaOrdersService,
    private amnciService: AmnciService, private ccdService: CCDBypassService, private fsaOrderCpeLineItemService: FsaOrderCpeLineItemService) {
  }

  ngOnInit() {

    this.form = new FormGroup({
      selectedM5: new FormControl(),
      notes: new FormControl(''),
      install: new FormGroup({
        mdsInstallationInformation: new FormGroup({
          solutionService: new FormControl(),
          networkTypeCode: new FormControl(),
          serviceTierCode: new FormControl(),
          transportOrderTypeCode: new FormControl(),
          designDocumentNo: new FormControl(),
          vendorCode: new FormControl()
        }),
        transport: new FormGroup({
          accessTypeCode: new FormControl(),
          accessArrangementCode: new FormControl(),
          encapsulationCode: new FormControl(),
          fiberHandoffCode: new FormControl(),
          carrierAccessCode: new FormControl(),
          circuitId: new FormControl(),
          serviceTerm: new FormControl(),
          quoteExpirationDate: new FormControl(),
          accessBandwidth: new FormControl(),
          privateLineNo: new FormControl(),
          m5AccessStatus: new FormControl(),
          classOfService: new FormControl()
        }),
        changeInformation: new FormGroup({
          changeDescription: new FormControl(),
          carrierServiceId: new FormControl(),
          carrierCircuitId: new FormControl(),
          originalInstallOrderId: new FormControl(),
          networkUserAddress: new FormControl(),
          portRateTypeCode: new FormControl()
        }),
        cpeInstallationInformation: new FormGroup({
          cpeOrderTypeCode: new FormControl(),
          equipmentOnlyFlagCode: new FormControl(),
          accessProvideCode: new FormControl(),
          phoneNumberType: new FormControl(),
          phoneNumber: new FormControl(),
          eccktIdentifier: new FormControl(),
          managedServicesChannelProgramCode: new FormControl(),
          deliveryDuties: new FormControl(),
          deliveryDutyAmount: new FormControl(),
          shippingChargeAmount: new FormControl(),
          recordOnly: new FormControl(),
          nuaOrCkt: new FormControl()
        })
      }),
      customer: new FormGroup({
        h1: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          branchCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl()
        }),
        h4: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl()
        }),
        h4Address: new FormGroup({
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl()
        }),
        h4Contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        }),
        h6: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          branchCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl(),
          serviceSubType: new FormControl(),
          salesOfficeIdCode: new FormControl(),
          salesPersonPrimaryCid: new FormControl(),
          salesPersonSecondaryCid: new FormControl(),
          cllidCode: new FormControl(),
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          cityCode: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl(),
          buildingName: new FormControl(),
          floor: new FormControl(),
          roomNumber: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        }),
        h6Address: new FormGroup({
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl(),
          buildingName: new FormControl(),
          floor: new FormControl(),
          roomNumber: new FormControl()
        }),
        h6Contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        })
      }),
      order: new FormGroup({
        orderAction: new FormControl(),
        ftn: new FormControl(),
        orderTypeCode: new FormControl(),
        orderSubTypeCode: new FormControl(),
        productTypeCode: new FormControl(),
        parentFtn: new FormControl(),
        relatedFtn: new FormControl(),
        telecomServicePriority: new FormControl(),
        carrierPartnerWholesaleType: new FormControl(),
        customerCommitDate: new FormControl(),
        customerWantDate: new FormControl(),
        customerOrderSubmitDate: new FormControl(),
        customerSignedDate: new FormControl(),
        //orderSubmitDate: new FormControl(),
        customerPremiseCurrentlyOccupiedFlag: new FormControl(),
        willCustomerAcceptServiceEarlyFlag: new FormControl(),
        multipleOrderFlag: new FormControl(),
        //multipleOrderIndex: new FormControl(),
        //multipleOrderTotal: new FormControl(),
        escalatedFlag: new FormControl(),
        expediteTypeCode: new FormControl(),
        govtTypeCode: new FormControl(),
        //vendorVpnCode: new FormControl(),
        prequalNo: new FormControl(),
        scaNumber: new FormControl()
      }),
      disconnect: new FormGroup({
        reasonCode: new FormControl(),
        cancelBeforeStartReasonCode: new FormControl(),
        cancelBeforeStartReasonText: new FormControl(),
        contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        })
      }),
      additionalCosts: new FormGroup({
      }),
    });

    this.userId = Number(localStorage.getItem('userID'));
    this.userADID = localStorage.getItem('userADID');
    this.workGroup = Number(this.activatedRoute.snapshot.params["WG"] || 1);
    this.orderId = Number(this.activatedRoute.snapshot.params["OrderID"] || 0);
    this.taskId = Number(this.activatedRoute.snapshot.params["TaskID"] || 0);

    this.spinner.show();

    if (this.orderId > 0) {
      this.getWGOrderData();
    }

    this.GetFTNData();
  }


  getWGOrderData() {
    let data = zip(
      this.workGroupService.GetWGData(this.workGroup, 0, this.orderId, false, null),
      this.workGroupService.GetFTNListDetails(this.orderId, this.workGroup),
      this.userProfileService.getMapUserProfilesByUserId(this.userId),
      this.dcpeService.GetCPEOrderInfo_V5U(this.orderId),
      this.orderNoteService.getByOrderId(this.orderId),
      this.workGroupService.GetLatestNonSystemOrderNoteInfo(this.orderId),
      this.ccdService.GetCCDHistory(this.orderId),
      this.fsaOrdersService.getById(this.orderId),
      this.fsaOrdersService.getRelatedOrdersById(this.orderId),
      this.fsaOrderCpeLineItemService.getByOrderId(this.orderId), // FSA CPE ORDER LINE ITEMS
    )


    data.subscribe(res => {
      let order = res[0]
      this.ftnList = res[1]
      this.loggedInUserProfilesList = res[2]
      this.orderInfo_V5U = res[3]
      this.noteList = res[4]
      this.noteInfo = res[5] != "" ? "Latest order notes updated by: " + res[5] : res[5]
      this.ccdList = res[6]
      let fsaOrder = res[7]
      let relatedOrdersList = res[8] as any[]
      this.fsaOrderCpeLineItemList = res[9]

      // M5 # Details
      this.m5DetailList = this.m5DetailList.concat(fsaOrder)
      this.m5DetailList = this.m5DetailList.concat(relatedOrdersList)

     // DE38942
     if(this.m5DetailList.length > 0) {
      this.orderTypeDesc = this.m5DetailList[0].ordrTypeDes.toUpperCase();
    }

      //if (order.length > 0) {
      //this.openedTaskId = order.map(a => a.tasK_ID);
      //this.hfSelectedFTN = order.map(a => a.ftn);
      let prodType = false;
      if (order.length > 0) {
        this.IsOrderComplete(order);
        prodType = order.some(a => a.proD_TYPE === "IPL E2E");
        this.hfSelectedFTN = order.map(a => a.ftn)[0];
      }

      if (this.orderId != 0) {
        if (!(this.hfOrderComplete)) {
          this.CheckIn();
        }

        if (this.loggedInUserProfilesList != null && this.loggedInUserProfilesList.length > 0) {
          if (this.loggedInUserProfilesList.find(a => a.usrPrfDes.includes('Sales Support')))
            this.bUserGrpAccess = true;

          if (this.bUserGrpAccess) {
            if (prodType) {
              this.ACR = true;
              this.ACRTerminating = true;
            }
            else {
              this.ACRTerminating = true;
            }
          }

        }
      }
      //}

      if (!this.hfOrderComplete) {
        this.LockUnlockPageControls();

        if (!this.bUserGrpAccess) {
          this.lblError = "Order opened in read-only mode.";
          this.LockPageControls();
        }
        else if (!(this.loggedInUserProfilesList.find(a => a.usrPrfDes === 'Sales Support Order RTS') || this.loggedInUserProfilesList.find(a => a.usrPrfDes === 'Sales Support Order Updater'))) {
          this.lblError = "User has Reviewer permissions, the order will be opened in locked mode.";
          this.LockPageControls();
        }
      }
      else {
        this.LockUnlockPageControls();
        this.LockPageControls();
      }

    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      if (this.m5DetailList[0] != null) {
        this.onM5Clicked(null, this.m5DetailList[0])
      } else {
        this.spinner.hide()
      }
    });

  }

  IsOrderComplete(res: any[]) {
    let sOrderStatus = res.map(a => a.ordR_STUS)[0];
    if (sOrderStatus.toUpperCase() == "COMPLETED" || sOrderStatus.toUpperCase() == "CANCELLED" || sOrderStatus.toUpperCase() == "DISCONNECTED" || sOrderStatus.toUpperCase() == "BILL ACTIVATED PENDING")
      this.hfOrderComplete = true;
  }

  CheckIn() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, false, 0).subscribe(
      data => {
        if (data == 1) {
          this.lblError = "This order is currently locked. Try again later.";
          this.LockPageControls();
          this.hfLockedOrderID = this.orderId.toString();
        }
      });

  }

  LockPageControls() {
    this.form.disable();
    this.locked = true;
    this.form.controls['additionalCosts'].disable();
  }

  UnlockPageControls() {
    this.form.enable();
  }

  GetFTNData() {
    this.dcpeService.GetFTNData(this.orderId, this.workGroup).subscribe(
      data => {
        if (data != null && data.length > 0) {
          this.hfSelectedDeviceOrder = data.map(a => a.deviceId)[0] + "-" + data.map(a => a.ftn)[0];
          //lblDeviceOrder.Text = hfSelectedDeviceOrder.Value;
          this.hfTaskID = data.map(a => a.taskID)[0];
        }
      });
  }

  LockUnlockPageControls() {
    if (this.orderId == 0) {
      this.LockPageControls();
    }
    else {
      this.UnlockPageControls();
      this.EnableCPEComplete();
      //if ((this.orderId.toString() != this.hfLockedOrderID) && this.taskID != 0) {
      //  this.UnlockPageControls();
      //}
      //else {
      //  this.LockPageControls();
      //}
    }
  }

  orderCompleted() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, true, 0).subscribe(
      data => {
        if (data == 0) {
          this.router.navigate(['order/rts/']);
        }
      });
  }

  EnableCPEComplete() {
    if (this.taskId != 0)
      this.locked = false;
    else {
      this.form.controls['additionalCosts'].disable();
    }
  }

  BindTasks() {
    this.dcpeService.GetTasksList().subscribe(data => {
      this.tasksList = data;
    })
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }
    return value;
  }

  btnRTS_Click() {
    let addtlCostsList = this.additionalCostDetails.getAdditionalCosts() as any[]
    let ACRSaved: boolean = false;

    if (this.additionalCostDetails.updateAdditionalCosts()) {
      ACRSaved = true;
    }
    //else {
    //  return false;
    //}

    if (addtlCostsList.find(a => a.statusID == 0 && a.isAdded != true)) {
      this.helper.notify("Please select Sales Status before completing RTS Task.", Global.NOTIFY_TYPE_WARNING)
    }
    else {
      this.btnSave_Click();
      if (this.additionalCostDetails.isACR) {
        if (ACRSaved) {
          this.workGroupService.CompleteACR(this.orderId, "Completing Pending ACR Request").subscribe(data => {
            if (data != 0) {
              this.router.navigate(['order/rts/']);
            }
          });
        }
        else {
          this.workGroupService.CompleteACR(this.orderId, "RTS Task completed, loading xNCI notification.").subscribe();
          this.workGroupService.LoadNCITask(this.orderId, 219).subscribe();
          this.router.navigate(['order/rts/']);
        }
      }
      else {
        this.workGroupService.CompleteActiveTask(this.orderId, this.taskId, 4, this.notes, this.userId).subscribe(data => {
          if (data) {
            this.router.navigate(['order/rts/']);
          }
        })
      }
    }
  }

  btnClose_Click() {
    this.orderCompleted();
  }

  btnSave_Click() {
    let orderNotes = this.getOrderNotes();

    if (orderNotes.nteTxt != String.Empty) {
      let data = this.orderNoteService.create(orderNotes).pipe(
        concatMap(res => this.orderNoteService.getByOrderId(this.orderId))
      )

      this.spinner.show()
      data.subscribe(res => {
        this.noteList = res;
        this.setFormControlValue("notes", "");
      }, error => this.spinner.hide(),
        () => this.spinner.hide())
    }
  }

  getOrderNotes() {
    return {
      ordrId: this.orderId,
      nteTypeId: 14,
      nteTxt: this.getFormControlValue("notes")
    }
  }

  onM5Clicked(e, data) {
    if (e != null) {
      (e as MouseEvent).preventDefault()
    }

    if (this.fsaDetails.isShown && this.fsaDetails.id == data.ordrId) { // Hide FSA Details
      this.fsaDetails.hide()
    } else if (!this.fsaDetails.isShown && this.fsaDetails.id == data.ordrId) { // Show already loaded
      this.fsaDetails.show(data.ordrId)
    } else { // Get new FSA Details
      this.amnciService.getFsaDetails(data.ordrId, this)
    }
  }

  btnBPAComplete_Click() {
    this.btnSave_Click();
    this.workGroupService.CompleteGOMIBillTask(this.orderId).subscribe(data => {
      if (data) {
        this.router.navigate(['order/rts/']);
      }
    })
  }
}

