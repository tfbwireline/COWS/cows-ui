import { Component, OnInit, Input, ViewChild, NgModule, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Helper } from '../../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, concat, of } from 'rxjs';
import { DatePipe } from '@angular/common';
import { CircuitService, WorkGroupService, OrderNoteService, UserService } from '../../../../services';
import { RTSFormComponent } from '../../rts/rts-form.component';
import { DxDataGridComponent, DxDataGridModule } from 'devextreme-angular';
import { AdditionalCosts } from '../../../../models';
import { Global } from '../../../../shared/global';
import { concatMap } from 'rxjs/operators';
import { MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-additional-costs',
  templateUrl: './additional-costs.component.html'
})
@NgModule({
  imports: [
    DxDataGridModule
  ],
})
export class AdditionalCostsComponent implements OnInit {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]
  @Input("parent") parent: any;
  @Input("group") form: FormGroup; // FormGroup passed from parent
  @Input() isTerminatingVendor: boolean;
  @Input() viewType: string;
  @Input() locked: boolean;

  // Added by John - 03/03/2020
  @Input() isACCP: boolean = false; // Additional Cost Charge Page - [Additional Cost Charge Page]
  @Input() parentPage: string = ''  //

  @ViewChild('gridCosts', { static: true }) dataGrid: DxDataGridComponent;

  get isAddAllowed() {
    return !this.isReadOnly && this.addlCostsDataSource.length < this.addlChargeTypeDataSource.length
  }
  get isNCI() {
    return this.viewType == "NCI" ? true : false
  }
  get isSalesSupport() {
    return this.viewType == "SalesSupport" ? true : false
  }
  get isReadOnly() {
    return this.userService.isReadonlyByKeyword("ORDER")
  }

  orderId: number = 0;
  acrStatus: number = 0;
  addlChargeTypeDataSource: any[] = [];
  rtsStatusDataSource: any[] = [];
  rtsCurrencyDataSource: any[] = [];
  addlCostsDataSource: any[] = [];
  addlCostHistoryDataSource: any;
  //dropDownOptions: object;
  editorOptions: object;
  lblHeaderLabel: string = "";
  isACR: boolean = false;
  displayGrid: boolean = false;

  constructor(private helper: Helper, private spinner: NgxSpinnerService, public datepipe: DatePipe, private circuitService: CircuitService,
    private workGroupService: WorkGroupService, private orderNoteService: OrderNoteService, private userService: UserService) {
    //this.dropDownOptions = { width: 500 };
    //this.editorOptions = {
    //  format: "currency"
    //}

    //this.onEditorPreparing = this.onEditorPreparing.bind(this);
    this.onHistoryClicked = this.onHistoryClicked.bind(this);
    this.onCalculateClicked = this.onCalculateClicked.bind(this);
  }

  ngOnInit() {
    let parent = this.parent as RTSFormComponent;
    this.orderId = parent.orderId;
    console.log("A: " + this.isTerminatingVendor + " B: " + this.viewType);
    //if (this.isTerminatingVendor) {
    //  this.lblHeaderLabel = "Additional Costs - Terminating";
    //}
    //else {
    //  this.lblHeaderLabel = "Additional Costs - Originating";
    //}

    this.spinner.show()
    this.circuitService.GetAddlCharges(parent.orderId, true).subscribe(data => {
      this.addlCostsDataSource = data;
    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      let data = zip(
        this.circuitService.GetAddlCostTypes(),
        this.circuitService.GetStatus(),
        this.circuitService.GetCurrencyList(),
        this.circuitService.AcrOrderStatus(this.orderId, this.isTerminatingVendor),
        this.workGroupService.IsACRRTS(this.orderId)
      )
      data.subscribe(res => {
        this.addlChargeTypeDataSource = res[0]
        this.rtsStatusDataSource = res[1]
        this.rtsCurrencyDataSource = res[2]
        this.acrStatus = res[3]
        this.isACR = res[4]

        //let salesIDs = [1, 4, 5];

        //if (this.isNCI) {
        //  this.addlChargeTypeDataSource = this.addlChargeTypeDataSource.filter(a => !salesIDs.includes(a.chargeTypeID))
        //}
        //else if (this.isSalesSupport) {
        //  this.addlChargeTypeDataSource = this.addlChargeTypeDataSource.filter(a => salesIDs.includes(a.chargeTypeID))
        //}

        //console.log(this.addlCostsDataSource)
        //console.log(this.addlChargeTypeDataSource)
        //console.log(this.rtsCurrencyDataSource)
        //console.log(this.rtsStatusDataSource)
      }, error => {
        this.spinner.hide()
      },
        () => {
          //this.spinner.hide()
        }
      )
    });


    //this.spinner.show();
    //let data = this.circuitService.GetAddlCharges(parent.orderId, true).pipe(
    //    concatMap(
    //      res => zip(
    //        of(res),
    //        this.circuitService.GetAddlCostTypes(),
    //        this.circuitService.GetStatus(),
    //        this.circuitService.GetCurrencyList()
    //      )
    //    )
    //  )
    //data.subscribe(res => {
    //  console.log(res[0])
    //  //console.log(res[1])
    //  //console.log(res[2])
    //  //console.log(res[3])
    //  this.addlCostsDataSource = res[0] as any[];
    //  this.addlChargeTypeDataSource = res[1].map(a => ({
    //    chargeTypeID: a.chargeTypeID,
    //    chargeTypeDesc: a.chargeTypeDesc,
    //    factor: a.factor
    //  }));
    //  this.rtsStatusDataSource = res[2].map(a => ({
    //    statusID: a.statusID,
    //    statusDesc: a.statusDesc
    //  }));
    //  this.rtsCurrencyDataSource = res[3].map(a => ({
    //    currencyCd: a.currencyCd,
    //    currencyDesc: a.currencyDesc
    //  }));


    //  //let salesIDs = [1, 4, 5];

    //  //if (this.isNCI) {
    //  //  console.log("TEST")
    //  //  this.addlChargeTypeDataSource = this.addlChargeTypeDataSource.filter(a => !salesIDs.includes(a.chargeTypeID))
    //  //}
    //  //else if (this.isSalesSupport) {
    //  //  this.addlChargeTypeDataSource = this.addlChargeTypeDataSource.filter(a => salesIDs.includes(a.chargeTypeID))
    //  //}

    //  ////this.dataGrid.instance.repaint()
    //  console.log(this.addlCostsDataSource)
    //  console.log(this.addlChargeTypeDataSource)
    //  console.log(this.rtsCurrencyDataSource)
    //  console.log(this.rtsStatusDataSource)
    //}, error => {
    //  console.log(error)
    //  this.spinner.hide();
    //}, () => {
    //  this.spinner.hide()
    //});

    //this.circuitService.GetAddlCharges(parent.orderId, true).subscribe(data => {
    //  this.addlCostsDataSource = data;
    //}, error => {
    //  console.log(error)
    //  this.spinner.hide();
    //}, () => {
    //  this.spinner.hide()
    //});

  }

  isHistoryAllowed(e) {
    return e.row.data.orderID && !this.isReadOnly
  }

  isGridInEditMode(e) {
    return e.row.isEditing
  }

  // EVENT HANDLING
  onInitNewRow(e) {
    e.data.orderID = 0
    e.data.statusID = 0
    e.data.chargeNRC = 0
    e.data.taxRate = 0
    e.data.version = 1
    e.data.billOnly = false
    e.data.isUpdated = true
    e.data.isAdded = true
  }

  onEditorPreparing(e) {
    //console.log(e)

    // CHRAGE TYPE ID
    if (e.dataField == "chargeTypeID" && e.parentType == "dataRow") {
      console.log(this.addlCostsDataSource)
      let chargeTypeIds = this.addlCostsDataSource.map(a => a.chargeTypeID) as number[]

      if (e.row.inserted) {
        let salesIDs = [1, 4, 5];
        if (this.isNCI) {
          e.editorOptions.dataSource = this.addlChargeTypeDataSource.filter(a => !salesIDs.includes(a.chargeTypeID));
        }
        else if (this.isSalesSupport) {
          e.editorOptions.dataSource = this.addlChargeTypeDataSource.filter(a => salesIDs.includes(a.chargeTypeID));
        }
        //e.editorOptions.dataSource = this.addlChargeTypeDataSource.filter(a => !chargeTypeIds.includes(a.chargeTypeID));
      } else if (e.row.isEditing) {
        e.editorOptions.dataSource = this.addlChargeTypeDataSource.filter(a => !chargeTypeIds.filter(a => a != e.value).includes(a.chargeTypeID));
        e.editorOptions.disabled = e.row.data.orderID == 0 ? false : true
      }
      console.log(this.addlCostsDataSource)
    }

    // STATUS ID
    if (e.dataField == "statusID" && e.parentType == "dataRow") {
      e.editorOptions.dataSource = this.rtsStatusDataSource
      if (e.row.inserted) {
        e.editorOptions.disabled = true
      } else if (e.row.isEditing) {
        e.editorOptions.disabled = !this.isNCI && e.row.data.orderID != 0 ? false : true
      }
    }

    //// CURRENCY - not sure why, but it wont load without this
    //if (e.dataField == "currencyCd" && e.parentType == "dataRow") {
    //  e.editorOptions.dataSource = this.rtsCurrencyDataSource
    //}
  }

  onRowInserted(e) {  
    //console.log(e)
    let data = e.data
    let key = e.key
    let conversionRate = this.rtsCurrencyDataSource.find(a => a.currencyCd == data.currencyCd) ? this.rtsCurrencyDataSource.find(a => a.currencyCd == data.currencyCd).factor : 0
    let totalCost = this.calculateTotalCost(data.chargeNRC, data.taxRate, conversionRate)

    e.data.chargeNRCUSD = totalCost
    //e.component.cellValue(e.component.getRowIndexByKey(key), "chargeNRCUSD", totalCost)
  }

  onRowUpdated(e) {
    //console.log(e)
    let data = e.data
    let key = e.key
    let conversionRate = this.rtsCurrencyDataSource.find(a => a.currencyCd == data.currencyCd) ? this.rtsCurrencyDataSource.find(a => a.currencyCd == data.currencyCd).factor : 0
    let totalCost = this.calculateTotalCost(data.chargeNRC, data.taxRate, conversionRate)

    e.data.chargeNRCUSD = totalCost
    e.data.isUpdated = true;
    //e.component.cellValue(e.component.getRowIndexByKey(key), "chargeNRCUSD", totalCost)
  }

  onHistoryClicked(e) {
    e.event.preventDefault();
    this.displayGrid = true;

    if(this.isACCP) {
      console.log(e)
    }

    this.spinner.show()
    this.circuitService.GetAddlChargeHistory(this.orderId, e.row.data.chargeTypeID, true).subscribe(data => {
      console.log('data');
      console.log(data);
      this.addlCostHistoryDataSource = data.map(record => {
        record.chargeNRCUSD = `$${record.chargeNRCUSD.toFixed(2)}`
        return record;
      });
    }, error => this.spinner.hide(),
      () => this.spinner.hide());
  }

  onCalculateClicked(e) {
    e.event.preventDefault();

    let data = e.row.data
    console.log(data)
    let conversionRate = this.rtsCurrencyDataSource.find(a => a.currencyCd == data.currencyCd) ? this.rtsCurrencyDataSource.find(a => a.currencyCd == data.currencyCd).factor : 0
    console.log(this.rtsCurrencyDataSource)
    if(data.chargeNRC != null || data.chargeNRC != undefined) {
      data.chargeNRC = data.chargeNRC.toString().replace(',','');
    }
    
    let totalCost = this.calculateTotalCost(data.chargeNRC, data.taxRate, conversionRate)
    console.log(totalCost)

    e.component.cellValue(e.row.rowIndex, "chargeNRC", data.chargeNRC)
    e.component.cellValue(e.row.rowIndex, "chargeNRCUSD", totalCost)
  }

  onSaveClicked(e) {
    if (this.updateAdditionalCosts()) {
      this.helper.notifySavedFormMessage("Additional Costs successfully updated", Global.NOTIFY_TYPE_SUCCESS, false, null);
    }
  }

  updateAdditionalCosts(): boolean {
    let isSuccess = false
    let addtlCostsList = this.getAdditionalCosts() as any[]
    let bRTS = false;

    // Too Large
    if (addtlCostsList.find(a => a.chargeNRC > 920000000000000)) {
      this.helper.notify("Cost too large to save", Global.NOTIFY_TYPE_WARNING)
    }
    // Cancel Request: Order is in a completed state and no further changes can be requested
    else if (addtlCostsList.find(a => a.chargeTypeID == 5) && this.acrStatus == 2) {
      this.helper.notify("Cancel Request: Order is in a completed state and no further changes can be requested", Global.NOTIFY_TYPE_WARNING)
    }
    // needsConfirmationCancel
    else if (addtlCostsList.find(a => a.chargeTypeID == 5) && this.acrStatus == 1) {
      if (confirm("Based on the status of the order early termination penalities must be request from the vendor. Do you wish to proceed?")) {
        isSuccess = true
      }
      else {
        isSuccess = false;
      }
    }
    // Based on the status of the order early termination penalties are not applicable
    else if (addtlCostsList.find(a => a.chargeTypeID == 5) && this.acrStatus == 0) {
      this.helper.notify("Based on the status of the order early termination penalties are not applicable", Global.NOTIFY_TYPE_WARNING)
    }
    //Expedite Early Termination Request
    else if (addtlCostsList.find(a => a.chargeTypeID == 1) && this.acrStatus == 2) {
      this.helper.notify("Expedite Request: Order is in a completed state and no further changes can be requested", Global.NOTIFY_TYPE_WARNING)
    }
    else {
      isSuccess = true
    }

    if (isSuccess) {
      this.spinner.show()
      this.circuitService.UpdateAdditionalCosts(this.orderId, addtlCostsList).subscribe(data => {
        this.addlCostsDataSource = data
        return true;
      }, error => {
        this.spinner.hide()
        return false;
      },
        () => this.spinner.hide());

      if (!bRTS) {
        bRTS = !addtlCostsList.find(a => a.chargeTypeID == 0 && a.isAdded) && !addtlCostsList.find(a => a.chargeTypeID == 6 && a.isAdded) && !addtlCostsList.find(a => a.chargeTypeID == 9 && a.isAdded);
      }
      if (this.viewType == "NCI" && bRTS) {
        this.initiateRTS();
      }
      else if (this.viewType == "SalesSupport") {
        this.notifyNCI(addtlCostsList);
      }

      if (addtlCostsList.find(a => a.statusID == 30)) {
        this.circuitService.InsertGOMBillingTask(this.orderId).subscribe();
      }
    }
    else {
      return false;
    }
  }

  initiateRTS() {
    //this.circuitService.LoadRTSTask(this.orderId).subscribe();
    let orderNotes = this.getOrderNotes("Additional Cost Request submitted.", 11);
    this.orderNoteService.createNote(orderNotes).subscribe(data => {
      if (data) {
        this.circuitService.SetJeopardy(this.orderId, data, "104").subscribe();
      }
      else {
        this.helper.notify("Unable to set Jeopardy code", Global.NOTIFY_TYPE_WARNING)
      }
    });
  }

  notifyNCI(addtlCostsList: any[]) {
    if (addtlCostsList.length > 0) {
      let orderNotes = this.getOrderNotes("Additional Cost Request submitted.  Moving order to NCI workgroup.", 14);
      this.orderNoteService.createNote(orderNotes).subscribe();
    }

    if (addtlCostsList.find(a => a.statusID == 0 && a.isUpdated == true)) {
      this.circuitService.NotifyNCI(this.orderId, 217).subscribe();
    }

    if (addtlCostsList.find(a => a.isUpdated == true)) {
      this.circuitService.NotifyNCI(this.orderId, 218).subscribe();
    }
  }

  getOrderNotes(note: string, noteTypeId: number) {
    return {
      ordrId: this.orderId,
      nteTypeId: noteTypeId,
      nteTxt: note
    }
  }

  calculateTotalCost(chargeNRC: any, taxRate: any, conversionRate: number) {
    //console.log(`NRC: ${chargeNRC}`)
    //console.log(`Tax: ${taxRate}`)
    //console.log(`Conversion: ${conversionRate}`)
    let totalTax = (chargeNRC * taxRate) / 100
    let totalCost = parseFloat(chargeNRC) + totalTax
    let totalCostConverted = 0

    //console.log(`NRC: ${chargeNRC}`)
    //console.log(`Tax: ${taxRate}`)
    //console.log(`Conversion: ${conversionRate}`)
    //console.log(`Total: ${totalCost}`)
    if (!isNaN(totalCost) && conversionRate != 0 && conversionRate != null) {
      totalCostConverted = totalCost * (1 / conversionRate)
    }

    //console.log(`NRC: ${chargeNRC}`)
    //console.log(`Tax: ${taxRate}`)
    //console.log(`Conversion: ${conversionRate}`)
    //console.log(`Total: ${totalCostConverted}`)
    return totalCostConverted
  }

  //contentReadyHandler(e: any) {
  //  if (this.viewType == "ReadOnly") {
  //    this.dataGrid.instance.columnOption(5, 'visible', false);
  //    this.dataGrid.instance.columnOption(6, 'visible', false);
  //    this.dataGrid.instance.columnOption(2, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(3, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(4, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(7, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(8, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(9, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(10, 'allowEditing', false);
  //  }

  //  else if (this.viewType == "NCI") {
  //    this.dataGrid.instance.columnOption(5, 'visible', false);
  //    this.dataGrid.instance.columnOption(6, 'visible', false);

  //    this.dataGrid.instance.columnOption(9, 'allowEditing', false);
  //  }
  //  else if (this.viewType == "SalesSupport") {
  //    //this.dataGrid.instance.columnOption(1, 'visible', false);
  //    this.dataGrid.instance.columnOption(2, 'visible', false);
  //    this.dataGrid.instance.columnOption(3, 'visible', false);
  //    this.dataGrid.instance.columnOption(4, 'visible', false);
  //    this.dataGrid.instance.columnOption(5, 'visible', false);
  //    this.dataGrid.instance.columnOption(6, 'visible', false);
  //    this.dataGrid.instance.columnOption(11, 'visible', false);

  //    //this.dataGrid.instance.columnOption(1, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(7, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(10, 'allowEditing', false);
  //  }
  //  e.component.export();
  //  e.component.exportToExcel();

  //  this.dataGrid.instance.refresh();
  //}

  //viewHistory(chargeTypeId: number) {
  //  this.circuitService.GetAddlChargeHistory(this.orderId, chargeTypeId, true).subscribe(data => {
  //    this.addlCostHistoryDataSource = data;
  //  });
  //}

  //calculateToTalCost(rowData) {
  //  console.log(rowData)
  //  //rowData.chargeNRCUSD = (rowData.chargeNRC * rowData.taxRate) / 100
  //  return (rowData.chargeNRC * rowData.taxRate) / 100;
  //}
  ////calculate(chargeTypeId: number) {
  ////  //chargeNRCUSD = 10;//(chargeNRC * taxRate) / 100;
  ////  console.log("chargeTypeId: " + chargeTypeId);
  ////}

  //editRecord(e: any) {
  //  if (this.viewType == "NCI") {
  //    this.dataGrid.instance.columnOption(1, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(7, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(9, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(10, 'allowEditing', false);
  //  }
  //  else if (this.viewType == "SalesSupport") {
  //    //this.dataGrid.instance.columnOption(1, 'visible', true);
  //    //this.dataGrid.instance.columnOption(12, 'visible', false);
  //    this.dataGrid.instance.columnOption(1, 'allowEditing', false);
  //    this.dataGrid.instance.columnOption(9, 'allowEditing', true);
  //  }
  //}

  //addNewRecord(e: any) {
  //  let salesIDs = [1, 4, 5];

  //  if (this.viewType == "NCI") {
  //    this.addlChargeTypeDataSource = this.addlChargeTypeDataSource.filter(function (item) {
  //      return !salesIDs.includes(item.chargeTypeID);
  //    });

  //    this.dataGrid.instance.columnOption(1, 'allowEditing', true);
  //    this.dataGrid.instance.columnOption(9, 'allowEditing', false);
  //  }
  //  else {
  //    this.addlChargeTypeDataSource = this.addlChargeTypeDataSource.filter(function (item) {
  //      return salesIDs.includes(item.chargeTypeID);
  //    });

  //    //this.dataGrid.instance.columnOption(1, 'visible', true);
  //    //this.dataGrid.instance.columnOption(12, 'visible', false);
  //    this.dataGrid.instance.columnOption(1, 'allowEditing', true);
  //    this.dataGrid.instance.columnOption(9, 'value', 5);
  //    this.dataGrid.instance.columnOption(9, 'allowEditing', false);
  //  }    
  //}

  //btnUpdate_Click() {
  //  this.circuitService.UpdateAdditionalCosts(this.getAdditionalCosts()).subscribe(data => {
  //      this.helper.notifySavedFormMessage("Equipment Receipts", Global.NOTIFY_TYPE_SUCCESS, false, null);
  //  },
  //    error => {
  //      this.helper.notifySavedFormMessage("Equipment Receipts", Global.NOTIFY_TYPE_ERROR, true, error);
  //    });
  //  //console.log("addlCostsDataSource: " + this.addlCostsDataSource);
  //  //console.log("this.addlCostsDataSource.length: " + this.addlCostsDataSource.length);
  //}

  getAdditionalCosts() {
    return this.addlCostsDataSource
      //.filter(a => a.isUpdated)
      .map(a => new AdditionalCosts({
        //orderID: (isNaN(+a.orderID)) ? 0 : a.orderID,
        orderID: this.orderId,
        chargeTypeID: a.chargeTypeID,
        chargeNRC: a.chargeNRC,
        taxRate: a.taxRate,
        currencyCd: a.currencyCd,
        chargeNRCUSD: a.chargeNRCUSD,
        note: a.note,
        statusID: a.statusID,
        //expirationTime: a.expirationTime,
        billOnly: a.billOnly,
        isTerm: this.isTerminatingVendor,
        version: a.version,
        isUpdated: a.isUpdated,
        isAdded: a.isAdded,

        // Defaults
        createdByUserId: 1,
        createdDateTime: new Date()
      }))
  }

  //chargeTypeValue(rowData) {
  //  console.log('chargeTypeValue - this.addlChargeTypeDataSource: ' + this.addlChargeTypeDataSource);
  //  if (this.addlChargeTypeDataSource != null)
  //    return this.addlChargeTypeDataSource[rowData.chargeTypeID];
  //  else
  //    return "";
  //}

  collapse() {
    this.viewPanels
      .filter(a => a.expanded)
      .forEach(a => {
        a.close()
      })
  }
}
