import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Helper } from '../../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { DcpeFormComponent } from '../..';
import { zip } from 'rxjs';
import { DatePipe } from '@angular/common';
import { WorkGroupService } from '../../../../services';
import { DCPEService } from '../../../../services/dcpe.service';
import { GomFormComponent } from '../../gom/gom-form.component';

@Component({
  selector: 'app-gom-fields',
  templateUrl: './gom-fields.component.html'
})
export class GomFieldsComponent implements OnInit {

  @Input("parent") parent: any;
  @Input("group") form: FormGroup; // FormGroup passed from parent

  orderStatusDataSource: any[] = [];
  selectedCpeOrderStatus: number = 1;
  selectedCpeItems: any[] = [];
  cpeOrderStatusId = 1;
  cpeCompletionDate: string;
  reqAmount: string = "";
  cpeEquipType: string = "";

  //showNID: boolean = false;
  nidSerialNumber: string = "";
  nidIp: string = "";

  cpeItemsList: any;
  cpeLineItemsList: any;

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private wgService: WorkGroupService, private dcpeService: DCPEService, public datepipe: DatePipe) {
   this.orderStatusDataSource = [
      { "ID": 1, "Name": "Pending" },
      { "ID": 2, "Name": "Complete" }
    ];
  }

  ngOnInit() {
    let parent = this.parent as GomFormComponent;    

    let data = zip(
      this.wgService.GetCPEUpdtInfo(parent.orderId),
      this.wgService.GetGOMSpecificData(parent.orderId),
      //this.dcpeService.IsNIDDevice(parent.orderId, parent.deviceId)
    )

    this.spinner.show();

    data.subscribe(res => {
      this.cpeItemsList = res[0];
      this.cpeLineItemsList = res[1];
      //this.showNID = res[2] ? true : false;

      console.log(this.cpeLineItemsList)
      if (this.cpeLineItemsList != null && this.cpeLineItemsList.length > 0) {
        if (!this.helper.isEmpty(this.cpeLineItemsList.map(a => a.cpE_CMPLT_DT)[0])) {
          this.cpeCompletionDate = this.datepipe.transform(this.cpeLineItemsList.map(a => a.cpE_CMPLT_DT)[0], 'MM/dd/yyyy');
        }

        this.reqAmount = this.cpeLineItemsList.map(a => a.rqstN_AMT)[0];
        this.cpeEquipType = this.cpeLineItemsList.map(a => a.cpE_EQPT_TYPE_TXT)[0];

        console.log(this.cpeLineItemsList.map(a => a.cpE_STUS_ID)[0])
        //if (this.cpeLineItemsList.map(a => a.cpE_STUS_ID)[0] > 0) {
        //  this.cpeOrderStatusId = this.cpeLineItemsList.map(a => a.cpE_STUS_ID)[0] || 1;
        //}
        //else {
        //  this.cpeOrderStatusId = 1;
        //}

        this.cpeOrderStatusId = this.cpeLineItemsList.map(a => a.cpE_STUS_ID)[0] || 1;
        this.form.get("cpeOrderStatus").setValue(this.cpeOrderStatusId)
        console.log(this.form)
        console.log(this.cpeOrderStatusId)
      }
    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      this.spinner.hide()
    });

  }

  public changeCpeOrderStatus(event: any) {
    console.log(event)
    this.selectedCpeOrderStatus = event.value;
  }

  selectionChangedHandler(data: any) {
    this.selectedCpeItems = data.selectedRowsData;
  }
}
