import { Component, OnInit, Input, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Helper } from '../../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from 'rxjs';
import { DatePipe } from '@angular/common';
import { NRMService } from '../../../../services';
import { GomFormComponent } from '../../gom/gom-form.component';
import { MatExpansionPanel } from '@angular/material';
import { AmnciFormComponent } from '../../amnci/amnci-form.component';
import { SystemOrderDetailFormComponent } from '../../system/system-order-detail-form.component';

@Component({
  selector: 'app-nrm',
  templateUrl: './nrm.component.html'
})
export class NrmComponent implements OnInit {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]

  @Input("parent") parent: any;
  @Input("group") form: FormGroup; // FormGroup passed from parent
  @Input("orderType") orderType: string;

  nrmServiceInstanceList: any;
  nrmCircuitList: any;
  nrmVendorList: any;
  nrmNotesList: any;
  toCollapse = true

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private nrmService: NRMService, public datepipe: DatePipe) { }

  ngOnInit() {
    let parent;
    if (this.orderType == "xNCI") {
      parent = this.parent as AmnciFormComponent;
    }
    else if (this.orderType == "GOM") {
      parent = this.parent as GomFormComponent;
    }
    else if (this.orderType == "System") {
      parent = this.parent as SystemOrderDetailFormComponent;
    }


    let data = zip(
      this.nrmService.GetNRMServiceInstanceList(parent.orderId),
      this.nrmService.GetNRMCircuitList(parent.orderId),
      this.nrmService.GetNRMVendorList(parent.orderId),
      this.nrmService.GetNRMNotes(parent.orderId)
    )

    this.spinner.show();

    data.subscribe(res => {
      this.nrmServiceInstanceList = res[0];
      this.nrmCircuitList = res[1];
      this.nrmVendorList = res[2];
      this.nrmNotesList = res[3];

    }, error => {
      console.log(error)
      //this.spinner.hide();
    }, () => {
      //this.spinner.hide()
    });

  }

  collapse() {
    this.viewPanels
      //.filter(a => a.expanded)
      .forEach(a => {
        if (this.toCollapse) {
          a.close()
        } else {
          a.open()
        }
      })
    this.toCollapse = !this.toCollapse
  }
}
