import { Component, OnInit, Input, Output, EventEmitter, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Helper } from '../../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { DcpeFormComponent } from '../..';
import { DCPEService } from '../../../../services/dcpe.service';
import { zip } from 'rxjs';
import { DatePipe } from '@angular/common';
import { MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-tech-assignment',
  templateUrl: './tech-assignment.component.html'
})
export class TechAssignmentComponent implements OnInit {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]

  @Input("parent") parent: any;
  @Input("group") form: FormGroup; // FormGroup passed from parent

  deviceFtn: string;
  customerName: string = "";
  address1: string = "";
  address2: string = "";
  city: string = "";
  address3: string = "";
  state: string = "";
  building: string = "";
  zip: string = "";
  floor: string = "";
  country: string = "";
  room: string = "";

  assignedTech: string = "";
  dispatchTime: string = "";
  eventId: string = "";

  orderInfo_V5U: any;
  assignedTechList: any;
  techAssigned: any;
  selectedAssignedTech: string = "";
  toCollapse = true

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private dcpeService: DCPEService, public datepipe: DatePipe) { }

  ngOnInit() {
    this.LoadTechAssignmentData();
  }

  LoadTechAssignmentData() {
    let parent = this.parent as DcpeFormComponent;

    let data = zip(
      this.dcpeService.GetCPEOrderInfo_V5U(parent.orderId),
      this.dcpeService.GetAssignedTechList(),
      this.dcpeService.GetTechAssignment_V5U(parent.orderId)
    )

    this.spinner.show();

    data.subscribe(res => {
      this.orderInfo_V5U = res[0];
      this.assignedTechList = res[1];
      this.techAssigned = res[2];

      if (this.orderInfo_V5U != null && this.orderInfo_V5U.length > 0) {
        this.deviceFtn = this.orderInfo_V5U.map(a => a.devicE_ID_FTN)[0];
        this.customerName = this.orderInfo_V5U.map(a => a.cusT_NME)[0];
        this.address1 = this.orderInfo_V5U.map(a => a.streeT_ADR_1)[0];
        this.address2 = this.orderInfo_V5U.map(a => a.streeT_ADR_2)[0];
        this.city = this.orderInfo_V5U.map(a => a.ctY_NME)[0];
        this.address3 = this.orderInfo_V5U.map(a => a.streeT_ADR_3)[0];
        this.state = this.orderInfo_V5U.map(a => a.stT_CD)[0];
        this.building = this.orderInfo_V5U.map(a => a.bldG_NME)[0];
        this.zip = this.orderInfo_V5U.map(a => a.ziP_PSTL_CD)[0];
        this.floor = this.orderInfo_V5U.map(a => a.flR_ID)[0];
        this.room = this.orderInfo_V5U.map(a => a.rM_NBR)[0];
      }

      if (this.techAssigned != null && this.techAssigned.length > 0) {
        this.assignedTech = this.techAssigned.map(a => a.useR_CPE_TECH_NME)[0];
        this.dispatchTime = this.techAssigned.map(a => a.dsptcH_TM)[0];
        this.eventId = this.techAssigned.map(a => a.evenT_ID)[0];
      }

    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      this.spinner.hide()
    });
  }

  onAssignedTechChanged(event: any) {
    this.selectedAssignedTech = event.selectedItem.useR_CPE_TECH_NME;
  }

  standardMatlReqChanged(e) {
    //this.helper.setFormControlValue("standardMatlReq", e.value);
  }


  collapse() {
    this.viewPanels
      //.filter(a => a.expanded)
      .forEach(a => {
        if (this.toCollapse) {
          a.close()
        } else {
          a.open()
        }
      })
    this.toCollapse = !this.toCollapse
  }
}

