import { Component, OnInit, Input, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Helper } from '../../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { DcpeFormComponent } from '../..';
import { DCPEService } from '../../../../services/dcpe.service';
import { zip } from 'rxjs';
import { DatePipe } from '@angular/common';
import { MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-order-completion',
  templateUrl: './order-completion.component.html'
})
export class OrderCompletionComponent implements OnInit {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]

  @Input("parent") parent: any;
  @Input("group") form: FormGroup; // FormGroup passed from parent

  isGoodman: boolean;
  toCollapse = true
  reqNbr: string = "";
  deviceFtn: string = "";
  ftn: string = "";
  ccd: string = "";
  site: string = "";
  rasDate: string = "";
  elid: string = "";
  eqptOnlyCD: string = "";
  recordsOnly: string = "";
  customerName: string = "";
  h6CustomerID: string = "";
  address1: string = "";
  sca: string = "";
  address2: string = "";
  city: string = "";
  address3: string = "";
  state: string = "";
  building: string = "";
  zip: string = "";
  floor: string = "";
  country: string = "";
  room: string = "";
  province: string = "";
  deliveryCLLI: string = "";
  shipAddr: string = "";
  businessUnitGL: string = "";
  businessUnitPC: string = "";
  costCenter: string = "";
  region: string = "";
  market: string = "";
  thirdPartySite: string = "";
  deviceId: string = "";

  displayTechAssignment: boolean = false;
  techName: string;
  phone: string;
  email: string;
  dispatchTime: string;
  inRouteTime: string;
  onSiteTime: string;
  completedTime: string;

  orderInfo_V5U: any;
  cpeCmplLineItemsList: any;
  equipOnlyInfoList: any;
  techAssignmentList: any;

  selectedOrderCompletionLineItemKeys: any[] = [];

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private dcpeService: DCPEService, public datepipe: DatePipe) { }

  ngOnInit() {   
    this.LoadOrderCompletionData();
  }

  LoadOrderCompletionData() {
    let parent = this.parent as DcpeFormComponent;
    if(parent['isGoodman'] !== undefined) {
      this.isGoodman = parent['isGoodman'];
    }
    

    let data = zip(
      this.dcpeService.GetCPEOrderInfo_V5U(parent.orderId),
      this.dcpeService.GetTechAssignment_V5U(parent.orderId),
      this.dcpeService.GetCpeCmplLineItems_V5U(parent.orderId)
    )

    this.spinner.show();

    data.subscribe(res => {
      this.orderInfo_V5U = res[0];
      this.techAssignmentList = res[1];
      this.cpeCmplLineItemsList = res[2];

      if (this.orderInfo_V5U != null && this.orderInfo_V5U.length > 0) {
        this.eqptOnlyCD = this.orderInfo_V5U.map(a => a.cpE_EQPT_ONLY_CD)[0];
        this.deviceFtn = this.orderInfo_V5U.map(a => a.devicE_ID_FTN)[0];
        this.ftn = this.orderInfo_V5U.map(a => a.ftn)[0];

        if(this.isGoodman && this.ftn.startsWith("DC")) {
          this.cpeCmplLineItemsList = this.cpeCmplLineItemsList.filter(x => ['RNTL', 'LEAS'].includes(x["cntrC_TYPE_ID"]) && x["eqpT_TYPE_ID"] == "MJH");
        } 

        this.ccd = this.orderInfo_V5U.map(a => a.cusT_CMMT_DT)[0];
        this.site = this.orderInfo_V5U.map(a => a.sitE_ID)[0];
        if (!this.helper.isEmpty(this.orderInfo_V5U.map(a => a.raS_DT)[0])) {
          this.rasDate = this.datepipe.transform(this.orderInfo_V5U.map(a => a.raS_DT)[0], 'MM/dd/yyyy');
        }
        this.elid = this.orderInfo_V5U.map(a => a.elid)[0];
        this.recordsOnly = this.orderInfo_V5U.map(a => a.cpE_REC_ONLY_CD)[0];
        this.customerName = this.orderInfo_V5U.map(a => a.cusT_NME)[0];
        this.h6CustomerID = this.orderInfo_V5U.map(a => a.cusT_ID)[0];
        this.address1 = this.orderInfo_V5U.map(a => a.streeT_ADR_1)[0];
        this.sca = this.orderInfo_V5U.map(a => a.scA_NBR)[0];
        this.address2 = this.orderInfo_V5U.map(a => a.streeT_ADR_2)[0];
        this.city = this.orderInfo_V5U.map(a => a.ctY_NME)[0];
        this.address3 = this.orderInfo_V5U.map(a => a.streeT_ADR_3)[0];
        this.state = this.orderInfo_V5U.map(a => a.stT_CD)[0];
        this.building = this.orderInfo_V5U.map(a => a.bldG_NME)[0];
        this.zip = this.orderInfo_V5U.map(a => a.ziP_PSTL_CD)[0];
        this.floor = this.orderInfo_V5U.map(a => a.flR_ID)[0];
        this.country = this.orderInfo_V5U.map(a => a.ctrY_CD)[0];
        this.room = this.orderInfo_V5U.map(a => a.rM_NBR)[0];
        this.province = this.orderInfo_V5U.map(a => a.prvN_NME)[0];
        this.thirdPartySite = this.orderInfo_V5U.map(a => a.thirD_PARTY_SITE)[0];
        this.deviceId = this.orderInfo_V5U.map(a => a.devicE_ID)[0];
      }
      if (this.thirdPartySite == "Y") {
        this.displayTechAssignment = true;
        if (this.techAssignmentList != null && this.techAssignmentList.length > 0) {
          this.techName = this.techAssignmentList.map(a => a.useR_CPE_TECH_NME)[0];
          this.phone = this.techAssignmentList.map(a => a.phN_NBR)[0];
          this.email = this.techAssignmentList.map(a => a.emaiL_ADR)[0];
          this.dispatchTime = this.techAssignmentList.map(a => a.dsptcH_TM)[0];
          this.inRouteTime = this.techAssignmentList.map(a => a.inrtE_TM)[0];
          this.onSiteTime = this.techAssignmentList.map(a => a.onsitE_TM)[0];
          this.completedTime = this.techAssignmentList.map(a => a.cmplT_TM)[0];
        }
      }
    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      this.spinner.hide()
    });
  }

  selectionChangedHandler(data: any) {
    this.selectedOrderCompletionLineItemKeys = data.selectedRowsData;
  }

  collapse() {
    this.viewPanels
      //.filter(a => a.expanded)
      .forEach(a => {
        if (this.toCollapse) {
          a.close()
        } else {
          a.open()
        }
      })
    this.toCollapse = !this.toCollapse
  }
}
