import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Helper } from '../../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { DcpeFormComponent } from '../..';
import { DCPEService } from '../../../../services/dcpe.service';
import { zip } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { DatePipe } from '@angular/common'
import { DxDataGridComponent } from 'devextreme-angular';
import { MatExpansionPanel } from '@angular/material';
declare let $: any;

@Component({
  selector: 'app-material-requisition',
  templateUrl: './material-requisition.component.html'
})
export class MaterialRequisitionComponent implements OnInit {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]
  @Input("parent") parent: any;
  @Input("group") form: FormGroup; // FormGroup passed from parent
  @Output() selectedLineItems = new EventEmitter<any[]>()
  @ViewChild('grid', { static: true }) dataGrid: DxDataGridComponent;

  shippingInstructions = '';
  lineItemNotes = '';
  orderInfo_V5U: any;
  requisitionNumberList: any;
  reqHeaderQueueList: any
  reqHeaderAcctCodesList: any;
  cpepList: any;
  matReqLineItems: any;
  vendorList: any;
  domesticClliList: any;
  internationalClliList: any;
  shippingInstrList: any

  selectedMatReqLineItemKeys: any[] = [];
  updatedMatReqLineItemKeys: any[] = [];

  //shippingInstructions: string = "";
  reqNbr: string = "";
  deviceFtn: string = "";
  ddu: string = "";
  ddr: string = "";
  ccd: string = "";
  site: string = "";
  rasDateValue: string = "";
  elid: string = "";
  eqptOnlyCD: string = "";
  recordsOnly: string = "";
  customerName: string = "";
  h6CustomerID: string = "";
  address1: string = "";
  sca: string = "";
  address2: string = "";
  city: string = "";
  address3: string = "";
  state: string = "";
  building: string = "";
  zip: string = "";
  floor: string = "";
  country: string = "";
  room: string = "";
  province: string = "";
  deliveryCLLI: string = "";
  shipAddr: string = "";
  businessUnitGL: string = "";
  businessUnitPC: string = "";
  costCenter: string = "";
  region: string = "";
  market: string = "";

  deliveryName: string = "";
  phone: string = "";
  hdnMrkPkg: string = "";

  orderType: string = "";
  domesticCD: boolean = true;
  chkInstallAddr: boolean;
  chkShipAddr: boolean;

  selectedDomesticClliCode: string = "";
  selectedInternationalClliCode: string = "";

  eqptTypeID: string;
  pidContractType: string;

  componentId: number = 0;
  itemDescription: string = "";

  selectedVendorCode: string = "";
  toCollapse = true

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private dcpeService: DCPEService, public datepipe: DatePipe) { }

  ngOnInit() {
    this.LoadMatReqForm();
  }

  LoadMatReqForm() {
    let parent = this.parent as DcpeFormComponent;
    this.spinner.show();
    let data = zip(
      this.dcpeService.GetRequisitionNumber_V5U(parent.orderId),
      this.dcpeService.GetCPEOrderInfo_V5U(parent.orderId),
      this.dcpeService.GetCpeReqHeaderAcctCodes_V5U(parent.orderId),
      this.dcpeService.GetCPEPidByOrderID(parent.orderId),
      this.dcpeService.GetCPEReqLineItems_V5U(parent.orderId),
      this.dcpeService.GetVendorsList(),
      this.dcpeService.GetDomesticClliCode(),
      this.dcpeService.GetInternationalClliCode(),
      this.dcpeService.GetReqHeaderQueueByOrderID(parent.orderId),
      this.dcpeService.GetShippingInstr(parent.orderId)
    )

    data.subscribe(res => {
      this.requisitionNumberList = res[0]
      this.orderInfo_V5U = res[1]
      this.reqHeaderAcctCodesList = res[2]
      this.cpepList = res[3]
      this.matReqLineItems = res[4]
      this.vendorList = res[5]
      this.domesticClliList = res[6]
      this.internationalClliList = res[7]
      this.reqHeaderQueueList = res[8]
      this.shippingInstrList = res[9]

      if (this.requisitionNumberList != null && this.requisitionNumberList.length > 0) {
        this.reqNbr = this.requisitionNumberList.map(a => a.newReqNbr)[0];
      }

      if (this.orderInfo_V5U != null && this.orderInfo_V5U.length > 0) {
        this.orderType = this.orderInfo_V5U.map(a => a.act)[0];
        this.domesticCD = Boolean(this.orderInfo_V5U.map(a => a.dmstC_CD)[0]);
        this.eqptOnlyCD = this.orderInfo_V5U.map(a => a.cpE_EQPT_ONLY_CD)[0];
        this.deviceFtn = this.orderInfo_V5U.map(a => a.devicE_ID_FTN)[0];
        this.ccd = this.orderInfo_V5U.map(a => a.cusT_CMMT_DT)[0];
        this.site = this.orderInfo_V5U.map(a => a.sitE_ID)[0];
        if (!this.helper.isEmpty(this.orderInfo_V5U.map(a => a.raS_DT)[0])) {
          this.rasDateValue = this.datepipe.transform(this.orderInfo_V5U.map(a => a.raS_DT)[0], 'MM/dd/yyyy');
          //this.setFormControlValue("materialRequisition.rasDate", this.rasDateValue);
        }
        this.elid = this.orderInfo_V5U.map(a => a.elid)[0];
        //this.setFormControlValue("materialRequisition.elid", this.elid);
        this.recordsOnly = this.orderInfo_V5U.map(a => a.cpE_REC_ONLY_CD)[0];
        this.customerName = this.orderInfo_V5U.map(a => a.cusT_NME)[0];
        //this.setFormControlValue("materialRequisition.name", this.customerName);
        this.setFormControlValue("deliveryName", this.customerName);
        this.h6CustomerID = this.orderInfo_V5U.map(a => a.cusT_ID)[0];
        this.address1 = this.orderInfo_V5U.map(a => a.streeT_ADR_1)[0];
        this.sca = this.orderInfo_V5U.map(a => a.scA_NBR)[0];
        this.address2 = this.orderInfo_V5U.map(a => a.streeT_ADR_2)[0];
        this.city = this.orderInfo_V5U.map(a => a.ctY_NME)[0];
        this.address3 = this.orderInfo_V5U.map(a => a.streeT_ADR_3)[0];
        this.state = this.orderInfo_V5U.map(a => a.stT_CD)[0];
        this.building = this.orderInfo_V5U.map(a => a.bldG_NME)[0];
        this.zip = this.orderInfo_V5U.map(a => a.ziP_PSTL_CD)[0];
        this.floor = this.orderInfo_V5U.map(a => a.flR_ID)[0];
        this.country = this.orderInfo_V5U.map(a => a.ctrY_CD)[0];
        this.room = this.orderInfo_V5U.map(a => a.rM_NBR)[0];
        this.province = this.orderInfo_V5U.map(a => a.prvN_NME)[0];
        this.deliveryCLLI = this.orderInfo_V5U.map(a => a.cllI_CD)[0];
        this.shipAddr = this.orderInfo_V5U.map(a => a.shiP_STREET_ADR_1)[0];

        if (this.domesticCD) {
          if (this.orderInfo_V5U.map(a => a.ddu)[0] == "")
            this.ddu = "N";
          else
            this.ddu = "Y";

          if (this.orderInfo_V5U.map(a => a.ddr)[0] == "")
            this.ddr = "N";
          else
            this.ddr = "Y";

          this.setFormControlValue("internationalShiptoClli", this.deliveryCLLI);
        }
        else {
          this.ddu = "";
          this.ddr = "";
          this.setFormControlValue("domesticShiptoClli", this.deliveryCLLI);
        }
        //console.log(this.deliveryCLLI + " - " + this.shipAddr + " - " + this.rasDate + " - " + this.elid);
        //if (this.domesticCD == false && this.orderType == "INST") {
        //  this.domesticCD = false;
        //  //upMatReqAuto.Visible = true;
        //}

        if (this.helper.isEmpty(this.shipAddr)) {
          this.chkInstallAddr = true;
          this.chkShipAddr = false;
        }
        else {
          this.chkInstallAddr = false;
          this.chkShipAddr = true;
          this.setFormControlValue("deliveryName", this.orderInfo_V5U.map(a => a.cusT_NME)[0]);
          this.setFormControlValue("deliveryAddr1", this.orderInfo_V5U.map(a => a.shiP_STREET_ADR_1)[0]);
          this.setFormControlValue("deliveryCity", this.orderInfo_V5U.map(a => a.shiP_CTY_NME)[0]);
          this.setFormControlValue("deliveryState", this.orderInfo_V5U.map(a => a.shiP_STT_CD)[0]);
          this.setFormControlValue("deliveryZip", this.orderInfo_V5U.map(a => a.shiP_ZIP_PSTL_CD)[0]);
          this.setFormControlValue("deliveryCountry", this.orderInfo_V5U.map(a => a.shiP_CTRY_CD)[0]);
        }
      }

      if (this.shippingInstrList != null && this.shippingInstrList.length > 0) {
        this.shippingInstructions = this.shippingInstrList.map(a => a.note)[0] != null ? this.shippingInstrList.map(a => a.note)[0] : "";
      }

      if (this.reqHeaderAcctCodesList != null && this.reqHeaderAcctCodesList.length > 0) {
        this.businessUnitGL = this.reqHeaderAcctCodesList.map(a => a.buS_UNT_GL)[0];
        this.businessUnitPC = this.reqHeaderAcctCodesList.map(a => a.buS_UNT_PC)[0];
        this.costCenter = this.reqHeaderAcctCodesList.map(a => a.cosT_CNTR)[0];
        this.region = this.reqHeaderAcctCodesList.map(a => a.regn)[0];
        this.market = this.reqHeaderAcctCodesList.map(a => a.mrkt)[0];
        //this.setFormControlValue("materialRequisition.businessUnitGL", this.businessUnitGL);
        //this.setFormControlValue("materialRequisition.businessUnitPC", this.businessUnitPC);
        //this.setFormControlValue("materialRequisition.costCenter", this.costCenter);
        //this.setFormControlValue("materialRequisition.region", this.region);

      }

      if (this.reqHeaderQueueList != null && this.reqHeaderQueueList.length > 0) {
        this.deliveryName = this.reqHeaderQueueList.map(a => a.dlvyNme)[0];
        this.phone = this.reqHeaderQueueList.map(a => a.dlvyPhnNbr)[0];
        this.hdnMrkPkg = this.reqHeaderQueueList.map(a => a.mrkPkg)[0];
      }

      if (this.cpepList != null && this.cpepList.length > 0) {
        this.eqptTypeID = this.cpepList.map(a => a.eqptTypeId)[0];
        this.pidContractType = this.cpepList.map(a => a.pidCntrctType)[0];
      }
    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      this.spinner.hide()
    });
  }

  selectionChangedHandler(data: any) {
    this.selectedMatReqLineItemKeys = data.selectedRowsData;
  }

  contentReadyHandler(e: any) {
    //this.dataGrid.instance.columnOption(2, 'visible', false);
  }

  loadEditForm(e: any) {
    //this.componentId = lineItem.data.cmpnT_ID;
    //this.dataGrid.instance.columnOption(3, 'allowEditing', false);
  }

  saveForm(lineItem: any) {
    this.spinner.show();

    // For Edit
    if (this.componentId > 0) {
      //this.customer = event.oldData;
      //this.customer.custNme = this.helper.isEmpty(event.newData.custNme)
      //  ? event.oldData.custNme : event.newData.custNme;
      //this.service.updateDedicatedCustomer(this.customerId, this.customer).subscribe(
      //  data => {
      //    this.helper.notifySavedFormMessage(this.customer.custNme, Global.NOTIFY_TYPE_SUCCESS, false, null);
      //    this.clear();
      //  },
      //  error => {
      //    this.helper.notifySavedFormMessage(this.customer.custNme, Global.NOTIFY_TYPE_ERROR, false, error);
      //    this.clear();
      //  });
    }
  }

  chkInstallAddrValueChangedHandler(event: any) {
    this.chkInstallAddr = event.value ? true : false;
    this.chkShipAddr = event.value ? false : true;
  }

  chkShipAddrValueChangedHandler(event: any) {
    this.chkShipAddr = event.value ? true : false;
    this.chkInstallAddr = event.value ? false : true;
    this.setFormControlValue("deliveryName", this.orderInfo_V5U.map(a => a.cusT_NME)[0]);
  }

  public onDomesticClliChanged(event: any) {
    this.selectedDomesticClliCode = event.selectedItem.clliCd;
  }

  public onInternationalClliChanged(event: any) {
    this.selectedInternationalClliCode = event.selectedItem.clliCd;
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }
    return value;
  }

  public onVendorCodeChanged(event: any) {
    this.selectedVendorCode = event.selectedItem.vndrNme;
  }

  onSearchEditClick(e) {
    e.event.preventDefault();
  }

  onSearchCellClick(e) {
    if (e.key > 0 && (e.columnIndex == 8)) {
      let item = this.matReqLineItems.find(a => a.cmpnT_ID == e.key);

      $("#folderDetails").modal("show");
      this.dataGrid.instance.selectRows(e.key, true);

      this.componentId = item.cmpnT_ID;
      this.itemDescription = item.itM_DES;
      this.setFormControlValue("edit.unitOfMeasure", item.unT_MSR);
      this.setFormControlValue("edit.activity", item.actvy);
      this.setFormControlValue("edit.unitOfPrice", item.unT_PRICE);
      this.setFormControlValue("edit.sourceType", item.sourcE_TYP);
      this.setFormControlValue("edit.resourceCategory", item.rsrC_CAT);
      this.setFormControlValue("edit.projectId", item.proJ_ID);
      this.setFormControlValue("edit.account", item.acct);
      this.setFormControlValue("edit.product", item.prodct);
    }
  }

  cancel() {
    if (this.dataGrid != null || this.dataGrid != undefined) {
      this.dataGrid.instance.clearSelection();
    }
    this.clear();
  }

  save() {
    let item = {
      CmpntId: this.componentId,
      UntMsr: this.getFormControlValue("edit.unitOfMeasure"),
      Actvy: this.getFormControlValue("edit.activity"),
      UntPrice: this.getFormControlValue("edit.unitOfPrice"),
      SourceTyp: this.getFormControlValue("edit.sourceType"),
      VndrNme: this.selectedVendorCode,
      RsrcCat: this.getFormControlValue("edit.resourceCategory"),
      InstCd: this.getFormControlValue("edit.installCode"),
      RsrcSub: this.getFormControlValue("edit.resourceSubcategory"),
      ProjId: this.getFormControlValue("edit.projectId"),
      CntrctId: this.getFormControlValue("edit.contractId"),
      Acct: this.getFormControlValue("edit.account"),
      CntrctLnNbr: this.getFormControlValue("edit.contractLineNumber"),
      Prodct: this.getFormControlValue("edit.product"),
      AxlryId: this.getFormControlValue("edit.auxiliaryId"),
      Comments: this.getFormControlValue("edit.lineItemNotes")
    }
    if (this.updatedMatReqLineItemKeys.length > 0 && this.updatedMatReqLineItemKeys.find(a => a.CmpntId == this.componentId)) {
      this.updatedMatReqLineItemKeys.splice(this.updatedMatReqLineItemKeys.findIndex(a => a.CmpntId == this.componentId), 1);
    }
    this.updatedMatReqLineItemKeys.push(item);

    this.clear();
  }

  clear() {
    this.setFormControlValue("edit.installCode", "");
    this.setFormControlValue("edit.resourceSubcategory", "");
    this.setFormControlValue("edit.contractId", "");
    this.setFormControlValue("edit.contractLineNumber", "");
    this.setFormControlValue("edit.auxiliaryId", "");
    this.setFormControlValue("edit.lineItemNotes", "");

    this.spinner.hide();
    $("#folderDetails").modal("hide");
  }

  collapse() {
    this.viewPanels
      //.filter(a => a.expanded)
      .forEach(a => {
        if (this.toCollapse) {
          a.close()
        } else {
          a.open()
        }
      })
    this.toCollapse = !this.toCollapse
  }

}

