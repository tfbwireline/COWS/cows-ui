import { Component, OnInit, Input, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Helper } from '../../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { DcpeFormComponent } from '../..';
import { DCPEService } from '../../../../services/dcpe.service';
import { zip } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { DatePipe } from '@angular/common'
import { MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-equipment-receipt',
  templateUrl: './equipment-receipt.component.html'
})
export class EquipmentReceiptComponent implements OnInit {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]
  @Input("parent") parent: any;
  @Input("group") form: FormGroup; // FormGroup passed from parent

  orderInfo_V5U: any;
  equipmentReceiptLineItems: any;
  selectedEquipmentReceiptLineItemKeys: any[] = [];
  pattern: any = this.ValidatePartialQuantity();

  reqNbr: string;
  deviceFtn: string;
  ccd: string;
  site: string;
  rasDate: string;
  elid: string;
  eqptOnlyCD: string;
  recordsOnly: string;
  ftn: string;
  voiceOrder: string;

  orderType: string;
  domesticCD: boolean = true;
  chkInstallAddr: boolean;
  chkShipAddr: boolean;
  toCollapse = true

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private dcpeService: DCPEService, public datepipe: DatePipe) { }

  ngOnInit() {
    this.LoadEquipmentReciptsData();
  }

  LoadEquipmentReciptsData() {
    let parent = this.parent as DcpeFormComponent;
    this.spinner.show();

    let data = zip(
      this.dcpeService.GetCPEOrderInfo_V5U(parent.orderId),
      this.dcpeService.GetCPELineItems_V5U(parent.orderId)
    )

    data.subscribe(res => {
      this.orderInfo_V5U = res[0]
      this.equipmentReceiptLineItems = res[1]

      if (this.orderInfo_V5U != null && this.orderInfo_V5U.length > 0) {
        this.orderType = this.orderInfo_V5U.map(a => a.act)[0];
        this.domesticCD = Boolean(this.orderInfo_V5U.map(a => a.dmstC_CD)[0]);
        this.eqptOnlyCD = this.orderInfo_V5U.map(a => a.cpE_EQPT_ONLY_CD)[0];
        this.deviceFtn = this.orderInfo_V5U.map(a => a.devicE_ID_FTN)[0];
        this.ccd = this.datepipe.transform(this.orderInfo_V5U.map(a => a.cusT_CMMT_DT)[0], 'MM/dd/yyyy');
        this.site = this.orderInfo_V5U.map(a => a.sitE_ID)[0];
        if (!this.helper.isEmpty(this.orderInfo_V5U.map(a => a.raS_DT)[0])) {
          this.rasDate = this.datepipe.transform(this.orderInfo_V5U.map(a => a.raS_DT)[0], 'MM/dd/yyyy');
        }
        this.elid = this.orderInfo_V5U.map(a => a.elid)[0];
        this.recordsOnly = this.orderInfo_V5U.map(a => a.cpE_REC_ONLY_CD)[0];
        this.ftn = this.orderInfo_V5U.map(a => a.ftn)[0];
        this.voiceOrder = this.orderInfo_V5U.map(a => a.voicE_DATA_ORDR)[0];

      }
    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      this.spinner.hide()
    });
  }

  selectionChangedHandler(data: any) {
    this.selectedEquipmentReceiptLineItemKeys = data.selectedRowsData;
  }

  ValidatePartialQuantity() {

  }

  collapse() {
    this.viewPanels
      //.filter(a => a.expanded)
      .forEach(a => {
        if (this.toCollapse) {
          a.close()
        } else {
          a.open()
        }
      })
    this.toCollapse = !this.toCollapse
  }
}

