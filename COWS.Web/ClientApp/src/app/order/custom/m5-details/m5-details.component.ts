import { Component, OnInit, Input, AfterViewInit, ViewChildren, ViewChild, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { AmnciFormComponent } from '../../amnci/amnci-form.component';
import { MatExpansionPanel } from '@angular/material';
//import { AmnciFormComponent, AnciComponent } from '../../';

@Component({
  selector: 'app-m5-details',
  templateUrl: './m5-details.component.html'
})
export class M5DetailsComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]
  @Input("parent") parent: AmnciFormComponent
  @Input() toHideFields: Array<string> = [];
  @Input() isCollapsed: boolean = false;
  @Input() orderTypeDesc: string = "";
  @Input() showCPE: boolean = false;

  @ViewChild('vlan', { static: false }) vlan: ElementRef;
  @ViewChild('transport', { static: false }) transport: ElementRef;
  @ViewChild('mds', { static: false }) mds: ElementRef;
  @ViewChild('change', { static: false }) change: ElementRef;
  @ViewChild('installInfo', { static: false }) installInfo: ElementRef;
  @ViewChild('disconnectInfo', { static: false }) disconnectInfo: ElementRef;

  get form() { return this.parent.form }
  // INSTALL
  get install() { return this.form.get("install") }
  get cpeLineItemList() { return this.parent.fsaOrderCpeLineItemList }
  get installPortList() { return this.parent.installPortList }
  get installVlanList() { return this.parent.installVlanList }
  // CUSTOMER
  get customer() { return this.form.get("customer") }
  // ORDER
  get order() { return this.form.get("order") }
  // ORDER CONTACT
  get orderContactList() { return this.parent.orderContactList }
  // ACCOUNT TEAM
  get accountTeamList() { return this.parent.accountTeamList }
  // DISCONNECT
  get disconnect() { return this.form.get("disconnect") }

  toCollapse = true
  isShown: boolean = true


  constructor(private el : ElementRef) { }  

  ngOnChanges(changes: SimpleChanges) {
    // switch(changes.orderTypeDesc.currentValue.toUpperCase()) {
    //   case "INSTALL":
    //     this.toHideFields.push("disconnectInfo");
    //     break;
    //   case "DISCONNECT":
    //       this.toHideFields.push("installInfo");
    //       break;
    // }

    // this.hideElements();
  }

  ngOnInit() {
    console.log(this.parent.form)
    switch(this.orderTypeDesc.toUpperCase()) {
      case "INSTALL":
        this.toHideFields.push("disconnectInfo");
        break;
      case "DISCONNECT":
          this.toHideFields.push("installInfo");
          break;
    }
  }

  ngAfterViewInit() {
    this.hideElements();
  }

  hideElements() {
    for(let i = 0; i < this.toHideFields.length; i++) {
      const key = this.toHideFields[i];
      switch(key) {
        case "vlan":
          this.vlan.nativeElement.hidden = true
          break;
        case "transport":
          this.transport.nativeElement.hidden = true
          break;
        case "change":
          this.change.nativeElement.hidden = true
          break;
        case "mds":
          this.mds.nativeElement.hidden = true
          break;
        case "installInfo":
          this.installInfo.nativeElement.hidden = true
          break;
        case "disconnectInfo":
          this.disconnectInfo.nativeElement.hidden = true
          break;
      }
    }
  }

  collapse() {
    this.viewPanels
      //.filter(a => a.expanded)
      .forEach(a => {
        if (this.toCollapse) {
          a.close()
        } else {
          a.open()
        }
      })
    this.toCollapse = !this.toCollapse
  }
}
