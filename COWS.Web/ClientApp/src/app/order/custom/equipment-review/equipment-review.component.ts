import { Component, OnInit, Input, Output, EventEmitter, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Helper } from '../../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { DcpeFormComponent } from '../..';
import { DCPEService } from '../../../../services/dcpe.service';
import { zip } from 'rxjs';
import { DatePipe } from '@angular/common';
import { MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-equipment-review',
  templateUrl: './equipment-review.component.html'
})
export class EquipmentReviewComponent implements OnInit {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]

  @Input("parent") parent: any;
  @Input("group") form: FormGroup; // FormGroup passed from parent
  //@Input() option: any;
  @Output() orderTypeChild: EventEmitter<string> = new EventEmitter();
  @Output() deviceFtnChild: EventEmitter<string> = new EventEmitter();

  deviceFtn: string;
  ccd: string;
  site: string;
  rASDate: string;
  eqptOnlyCD: string;
  recordsOnly: string;
  customerName: string;
  h6CustomerID: string;
  address1: string;
  sca: string;
  address2: string;
  city: string;
  address3: string;
  state: string;
  building: string;
  zip: string;
  floor: string;
  country: string;
  room: string;
  province: string;
  deliveryCLLI: string;
  shipAddr: string;

  orderType: string;
  domesticCD: boolean;

  matReqAuto: boolean = false;

  orderInfo_V5U: any;
  cpeLineItemsList: any;
  equipOnlyInfoList: any;
  toCollapse = true

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private dcpeService: DCPEService, public datepipe: DatePipe) { }

  ngOnInit() {
    let parent = this.parent as DcpeFormComponent;

    let data = zip(
      this.dcpeService.GetCPEOrderInfo_V5U(parent.orderId),
      this.dcpeService.GetCPELineItems_V5U(parent.orderId),
      this.dcpeService.GetEquipOnlyInfo(parent.orderId)
    )

    this.spinner.show();

    data.subscribe(res => {
      this.orderInfo_V5U = res[0];
      this.cpeLineItemsList = res[1];
      this.equipOnlyInfoList = res[2];

      if (this.orderInfo_V5U != null && this.orderInfo_V5U.length > 0) {
        this.orderType = this.orderInfo_V5U.map(a => a.act)[0];
        this.domesticCD = Boolean(this.orderInfo_V5U.map(a => a.dmstC_CD)[0]);
        this.eqptOnlyCD = this.orderInfo_V5U.map(a => a.cpE_EQPT_ONLY_CD)[0];
        this.deviceFtn = this.orderInfo_V5U.map(a => a.devicE_ID_FTN)[0];
        this.ccd = this.orderInfo_V5U.map(a => a.cusT_CMMT_DT)[0];
        this.site = this.orderInfo_V5U.map(a => a.sitE_ID)[0];
        this.rASDate = this.orderInfo_V5U.map(a => a.raS_DT)[0];
        if (!this.helper.isEmpty(this.orderInfo_V5U.map(a => a.raS_DT)[0])) {
          this.rASDate = this.datepipe.transform(this.orderInfo_V5U.map(a => a.raS_DT)[0], 'MM/dd/yyyy');
        }
        this.recordsOnly = this.orderInfo_V5U.map(a => a.cpE_REC_ONLY_CD)[0];
        this.customerName = this.orderInfo_V5U.map(a => a.cusT_NME)[0];
        this.h6CustomerID = this.orderInfo_V5U.map(a => a.cusT_ID)[0];
        this.address1 = this.orderInfo_V5U.map(a => a.streeT_ADR_1)[0];
        this.sca = this.orderInfo_V5U.map(a => a.scA_NBR)[0];
        this.address2 = this.orderInfo_V5U.map(a => a.streeT_ADR_2)[0];
        this.city = this.orderInfo_V5U.map(a => a.ctY_NME)[0];
        this.address3 = this.orderInfo_V5U.map(a => a.streeT_ADR_3)[0];
        this.state = this.orderInfo_V5U.map(a => a.stT_CD)[0];
        this.building = this.orderInfo_V5U.map(a => a.bldG_NME)[0];
        this.zip = this.orderInfo_V5U.map(a => a.ziP_PSTL_CD)[0];
        this.floor = this.orderInfo_V5U.map(a => a.flR_ID)[0];
        this.country = this.orderInfo_V5U.map(a => a.ctrY_CD)[0];
        this.room = this.orderInfo_V5U.map(a => a.rM_NBR)[0];
        this.province = this.orderInfo_V5U.map(a => a.prvN_NME)[0];
        this.deliveryCLLI = this.orderInfo_V5U.map(a => a.cllI_CD)[0];
        this.shipAddr = this.helper.isEmpty(this.orderInfo_V5U.map(a => a.shiP_STREET_ADR_1)[0]) ? "N" : "Y";

        if (!this.domesticCD && this.orderType == "INST") {
          this.matReqAuto = true;
          //upMatReqAuto.Visible = true;
        }
      }

      if (this.cpeLineItemsList != null) {
        //DataTable dt = DV.ToTable();
        //BindData(dt);
      }

      if (this.eqptOnlyCD == "Y") {
        //upEqipOnlyInfo.Visible = true;
        if (this.equipOnlyInfoList != null) {
          //BindEquipOnlyData(equipOnlyInfo.ReturnValue);
        }
      }

    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      this.spinner.hide()
    });

  }

  standardMatlReqChanged(e) {
    //this.helper.setFormControlValue("standardMatlReq", e.value);
  }


  collapse() {
    this.viewPanels
      //.filter(a => a.expanded)
      .forEach(a => {
        if (this.toCollapse) {
          a.close()
        } else {
          a.open()
        }
      })
    this.toCollapse = !this.toCollapse
  }
}
