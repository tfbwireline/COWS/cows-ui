import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ViewChildren, QueryList,Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//import * as moment from 'moment';
import * as moment from 'moment';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { AmnciService, OrderService, TaskService, H5FolderService, WorkGroupService, OrderStdiHistoryService, FsaOrderCpeLineItemService, CircuitService, VendorOrderService, CCDBypassService, FsaOrdersService, AccessCitySiteService, XnciRegionService, AccessMbService, CountryService, AsrTypeService, OrderNoteService, StdiReasonService, OrderLockService, UserProfileService, UserService, NccoOrderService, VendorOrderEmailService, OrderAddressService, CurrencyListService, DocEntityService, VendorOrderMsService } from '../../../services';
import { zip, of, forkJoin} from 'rxjs';
import { DatePipe } from '@angular/common';
import { concatMap } from 'rxjs/operators';
import { String } from 'typescript-string-operations';
import { NgxSpinnerService } from 'ngx-spinner';
import { DCPEService } from '../../../services/dcpe.service';
import { VendorOrder, VendorOrderEmail, Asr, VendorOrderMs, DocEntity } from '../../../models';
import { M5DetailsComponent } from '../custom/m5-details/m5-details.component';
import { Helper } from '../../../shared';
import { Global, KeyValuePair, ETasks, OrderStatus } from '../../../shared/global';
import { CustContractLengthService } from '../../../services/cust-contract-length.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NrmComponent } from '../custom/nrm/nrm.component';
import { AdditionalCostsComponent } from '../custom/additional-costs/additional-costs.component';
import { MatExpansionPanel } from '@angular/material';
import { GomFieldsComponent } from '../custom/gom-fields/gom-fields.component';
import { DxCheckBoxModule } from "devextreme-angular";
import { FileSizePipe } from '../../../shared/pipe/file-size.pipe';
import { FsaOrderCpeLineItems } from '../../../models';

declare let $: any;

@Component({
  selector: 'app-amnci-form',
  templateUrl: './amnci-form.component.html'
})
export class AmnciFormComponent implements OnInit, AfterViewInit {
  @ViewChildren(MatExpansionPanel) viewPanels: MatExpansionPanel[]
  @ViewChild(M5DetailsComponent, { static: false }) m5DetailsComponent: M5DetailsComponent;
  //@Input("parent") parent: M5DetailsComponent
  @ViewChild(NrmComponent, { static: false }) nrmComponent: NrmComponent;
  @ViewChild(AdditionalCostsComponent, { static: false }) additionalCostComponent: AdditionalCostsComponent;
  @ViewChild(GomFieldsComponent, { static: false })
  private gomFieldDetails: GomFieldsComponent;
  // For quick accessing Form
  get closedDate() { return this.form.get("mileStoneInformation.closedDate") }
  get asrType() { return this.form.get("asr.asrType") }
  get lecNni() { return this.form.get("asr.aggregated.lecNni") }
  get masterVas() { return this.form.get("asr.offnet.masterVas") }

  get showIPL() { return [1, 5].includes(this.orderCatId) }
  get showNCCO() { return this.orderCatId == 4 }
  get showFSA() { return !this.showIPL && !this.showNCCO }
  get showCPE() { return this.productTypeCd == "CPE" }
  get showBilling() { return this.orderTypeDesc.toUpperCase() == "BILLING ONLY" || (this.orderTypeDesc.toUpperCase() == "CANCEL" && (this.parentOrderType || "").toUpperCase() == "BILLING ONLY") }
  get showVendorAdditionalCost() {
    //return (this.isReviewer || this.isUpdater) && !this.isRTS //removed as per the conversation with Jagan (additional costs missing due to RTS profile)
    return (this.isReviewer || this.isUpdater)
    //return (this.orderCatId == 2 && (["DISCONNECT", "CHANGE"].includes(this.orderTypeDesc.toUpperCase()) && (this.orderTypeDesc.toUpperCase() == "CANCEL" && (this.parentOrderType || "").toUpperCase() == "DISCONNECT"))) ||
    //  (this.orderCatId == 6 && (this.orderTypeDesc.toUpperCase() == "DISCONNECT" || (this.orderTypeDesc.toUpperCase() == "CANCEL" && (this.parentOrderType || "").toUpperCase() == "DISCONNECT")))
  }
  get showVendorAccessCost() {
    // return this.showVendorAdditionalCost || (this.showCPE && this.showBilling)
    //return !this.showVendorAdditionalCost || (this.showCPE && this.showBilling)

   // DE38942
    return !this.showCPE && this.isxNCIUser;
  }
  get showReturnToGomButton() { return !this.showCPE && this.showNCCO ? true : false }
  get showReturnToSalesButton() { return !this.showCPE && !this.showReturnToGomButton ? true : false }
  get showClearAcr() { return (this.orderStatus || "").includes("ACR") }
  get showClearRts() { return (this.orderStatus || "").includes("RTS") }
  get showClearCcd() { return (this.orderStatus || "").includes("CCD") }
  get showSalesSupportNotes() {
    return this.m5DetailList
      .filter(a => a.acT_TASK != null)
      .find(a => a.acT_TASK.includes("SS RTS Pending"))
      && this.userService.loggedInUser.profiles.includes("Sales Support")
      && !this.showReturnToGomButton
  }
  get showRequestToLogicallis() {
    return this.activeTasks
      .find(a => a.tasK_ID == 223) && this.workGroup == 16
  }

  //get isRTS() { return this.userService.isRTSByKeyword("NCI ORDER") }
  //get isReviewer() { return this.userService.isReviewerByKeyword("NCI ORDER") }
  //get isUpdater() { return this.userService.isUpdaterByKeyword("NCI ORDER") }
  get isDisconnect() { return this.orderTypeDesc.toUpperCase() == "DISCONNECT" || (this.orderTypeDesc.toUpperCase() == "CANCEL" && (this.parentOrderType || "").toUpperCase() == "DISCONNECT") }
  //get isCancel() { return !this.isDisconnect && !this.showBilling }
  get isCancel() { return this.orderTypeDesc.toUpperCase() == "CANCEL" }
  get isReadonly() { return this.userService.isReadonlyByKeyword("ORDER") }
  get isPending() { return (this.orderStatus || "").includes("Pending") }
  //get isClosed() { return this.orderStatusId == 2 }
    get isClosed() { return false }
  get hasEmptyVendorAcknowledgeDate() {
    return this.vendorOrderEmailList.length > 0 && this.vendorOrderEmailList.filter(a => a.ackByVendorDate == null).length > 0
  }
  get isReviewer() {
    let profiles = this.userService.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI ORDER REVIEWER") ||
        a.toUpperCase().includes("SALES SUPPORT ORDER REVIEWER")
    ) as string[]

    return profiles.length > 0
  }
  get isRTS() {
    let profiles = this.userService.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI ORDER RTS") ||
        a.toUpperCase().includes("SALES SUPPORT ORDER RTS")
    ) as string[]

    return profiles.length > 0
  }
  get isUpdater() {
    let profiles = this.userService.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI ORDER UPDATER") ||
        a.toUpperCase().includes("SALES SUPPORT ORDER UPDATER")
    ) as string[]

    return profiles.length > 0
  }

  get isOrderComplete() {
    // Hide buttons only if order closed date was already populated.
    return (!this.helper.isEmpty(this.rawClosedDate)) && ((this.orderStatusId === OrderStatus.Completed) || (this.orderStatusId === OrderStatus.Disconnected) ||
      (this.orderStatusId === OrderStatus.Cancelled));
  }

  get isxNCIUser() {
    let profiles = this.userService.loggedInUser.profiles.filter(
      a =>
        a.toUpperCase().includes("NCI")
    ) as string[]

    return profiles.length > 0
  }
  form: FormGroup
  logicallis: FormGroup
  userId: number = 0
  orderId: number = 0
  title: string = ""
  workGroup: number = 0
  taskId: number = 0
  nrmupdate : boolean
  checkBox_value = undefined;
  orderCatId: number = 0
  orderTypeDesc: string = ""
  parentOrderType: string = ""
  productTypeCd: string = null
  lastNoteBy: string = null
  h5Folder = null
  fsaDetails = {
    id: null,
    isShown: false,
    show: (id) => {
      this.fsaDetails.id = id
      this.fsaDetails.isShown = true
    },
    hide: () => {
      this.fsaDetails.isShown = false
    }
  }
  isH5FolderSearchShown: boolean = false
  selectedItemH5FolderKeys = []
  vendorOrder: VendorOrder = null;
  vendorOrderNo: string;
  isSubmitted: boolean = false
  toCollapse: boolean = true
  locked: boolean = false
  lblError: string = null
  //isClosed: boolean = false
  loggedInUserProfilesList: any
  nccoOrder: any = null
  orderStatus: string = null
  orderStatusId: number = null
  closedDateError: string = null
selectedCpeOrderStatus: number = 1;
  fsaOrderCpeLineItemList = []
  stdiReasonList = []
  m5DetailList = []
  activeTasks = []
  h5FolderList = []
  h5DocumentList = []
  ccdList: any[] = []
  vendorOrderEmailList: VendorOrderEmail[] = []
  vendorOrderList: VendorOrder[] = []
  vendorAccessCostInformationList = []
  vendorAccessCostList = []
  noteList = []
  cpeOrderStatusList: KeyValuePair[] = [new KeyValuePair("Pending", 1), new KeyValuePair("Complete", 2)]
  asrTypeList: any[]
  origTermList: any[] = [
    "Off Shore to US Domestic Router",
    "Off Shore to Intl Off Shore Router"
  ]
  yesNoList: any[] = [
    "Yes",
    "No"
  ]
  accessMbList = []
  selectedAccessMbList = []
  accessCityNameList = []
  stdiList = []
  custContractLengthList = []
  installPortList = []
  installVlanList = []
  accountTeamList = []
  orderContactList = []
  countryList = []
  regionList = []
  errors: string[] = [];
  h6Address: any;
  option: any = {
    validationSummaryOptions: {
      title: "Validation Summary"
    }
  }
  isGomCancelTaskCompleted: boolean = false

  orderResult: any;
  orderType: string = "xNCI";
  currencyList = []
  isOnLoad: boolean = true;


  isBAREditAllowed: boolean = false
  rawClosedDate: string = "";
  m5: string = "";
  m5Data : any;
  isBillCleared: boolean = false
  cpeLineItemsList: any;


  acs : FsaOrderCpeLineItems;
  prt : FsaOrderCpeLineItems;

  isAbleToManageVndrOrdrMs : boolean = false;
  vndrOrdrMsId : number = 0;
  //ftn: string;
  //////////////////////////////////////////////////////////////////

  //isFromNCCO: boolean = false;
  //isNCCOReadOnly: boolean = false;
  //circuitMilestone: Array<any> = [];

  //locked: boolean = false;
  //lblError: string;
  //now: any = moment()
  //noteInfo: string = "";
  //fsaOrder: any[] = []
  //accessCostList: any[] = []
  //toCollapse = true

  //// STATIC FIELDS
  //nrmList1 = []
  //nrmList2 = []
  //nrmList3 = []
  //noteList = []
  //taskIdList = []

  ////Tony Changes
  //vendorOrderDetails = [];
  //vendorOrderEmailType = [];
  //vendorOrderEmailAttachment = [];
  //vendorOrderEmailCollection = [];

  //showRevisedVedorInfo: boolean = false;

  get mileStoneInformationForm() {
    return this.form.get("mileStoneInformation");
  }

  constructor(private router: Router, public datepipe: DatePipe, private dcpeService: DCPEService,private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService, private helper: Helper, private sanitizer: DomSanitizer,
    private orderService: OrderService, private taskService: TaskService, private h5FolderService: H5FolderService, private workGroupService: WorkGroupService,
    private orderStdiHistoryService: OrderStdiHistoryService, private fsaOrderCpeLineItemService: FsaOrderCpeLineItemService, private circuitService: CircuitService,
    private vendorOrderService: VendorOrderService, private ccdService: CCDBypassService, private fsaOrderService: FsaOrdersService,
    private accessCitySiteService: AccessCitySiteService, private xnciRegionService: XnciRegionService, private accessMbService: AccessMbService,
    private countrySrvc: CountryService, private asrTypeService: AsrTypeService, private custContractLengthService: CustContractLengthService,
    private stdiSrvc: StdiReasonService, private orderNoteService: OrderNoteService, private orderLockService: OrderLockService, private userProfileService: UserProfileService,
    private userService: UserService, private nccoOrderService: NccoOrderService, private vendorOrderEmailService: VendorOrderEmailService,
    public amnciService: AmnciService, private orderAddressService: OrderAddressService, private currencyService: CurrencyListService, private docSrvc: DocEntityService,
    private fileSizePipe: FileSizePipe, private wgService: WorkGroupService, private vendorOrderMsService: VendorOrderMsService
  ) {
    this.onEmailUpdating = this.onEmailUpdating.bind(this);
    this.isEditAllowed = this.isEditAllowed.bind(this);
  }

  ngAfterViewInit() {
    console.log(this.form.value);
  }

  ngOnInit() {
    console.log(this.form);
    this.userId = Number(localStorage.getItem('userID'));
    this.orderId = this.activatedRoute.snapshot.params["id"] || 0

    this.activatedRoute.parent.url.subscribe((urlPath) => {
      this.title = urlPath[urlPath.length - 1].path.toUpperCase();
    })

    this.form = new FormGroup({
      display: new FormGroup({
        m5: new FormControl({ value: null, disabled: true }),
        customerName: new FormControl({ value: null, disabled: true }),
        orderType: new FormControl({ value: null, disabled: true }),
        orderSubType: new FormControl({ value: null, disabled: true }),
        productType: new FormControl({ value: null, disabled: true }),
        orderStatus: new FormControl({ value: null, disabled: true }),
        pendingTasks: new FormControl({ value: null, disabled: true }),
        customerNameH1: new FormControl({ value: null, disabled: true }),
        h6AddressLine1: new FormControl({ value: null, disabled: true }),
        h6City: new FormControl({ value: null, disabled: true }),
        h6Country: new FormControl({ value: null, disabled: true }),
        vendorName: new FormControl({ value: null, disabled: true }),
        accessBandwidth: new FormControl({ value: null, disabled: true }),
      }),
      general: new FormGroup({
        portSpeed: new FormControl({ value: null, disabled: true }),
        accessMegabyteValue: new FormControl(),
      }),
      stdi: new FormGroup({
        standardInterval: new FormControl(),
        reasonDescription: new FormControl(),
        comments: new FormControl()
      }),

      cpe: new FormGroup({
        cpeOrderStatus: new FormControl(1),
        cpeCompletionDate: new FormControl(),
        requisitionAmount: new FormControl(),
        cpeEquipmentType: new FormControl(),
      }),
      install: new FormGroup({
        mdsInstallationInformation: new FormGroup({
          solutionService: new FormControl(),
          networkTypeCode: new FormControl(),
          serviceTierCode: new FormControl(),
          transportOrderTypeCode: new FormControl(),
          designDocumentNo: new FormControl(),
          vendorCode: new FormControl()
        }),
        transport: new FormGroup({
          accessTypeCode: new FormControl(),
          accessArrangementCode: new FormControl(),
          encapsulationCode: new FormControl(),
          fiberHandoffCode: new FormControl(),
          carrierAccessCode: new FormControl(),
          circuitId: new FormControl(),
          serviceTerm: new FormControl(),
          quoteExpirationDate: new FormControl(),
          accessBandwidth: new FormControl(),
          privateLineNo: new FormControl(),
          m5AccessStatus: new FormControl(),
          classOfService: new FormControl()
        }),
        changeInformation: new FormGroup({
          changeDescription: new FormControl(),
          carrierServiceId: new FormControl(),
          carrierCircuitId: new FormControl(),
          originalInstallOrderId: new FormControl(),
          networkUserAddress: new FormControl(),
          portRateTypeCode: new FormControl()
        }),
        cpeInstallationInformation: new FormGroup({
          cpeOrderTypeCode: new FormControl(),
          equipmentOnlyFlagCode: new FormControl(),
          accessProvideCode: new FormControl(),
          phoneNumberType: new FormControl(),
          phoneNumber: new FormControl(),
          eccktIdentifier: new FormControl(),
          managedServicesChannelProgramCode: new FormControl(),
          deliveryDuties: new FormControl(),
          deliveryDutyAmount: new FormControl(),
          shippingChargeAmount: new FormControl(),
          recordOnly: new FormControl(),
          nuaOrCkt: new FormControl()
        })
      }),
      customer: new FormGroup({
        h1: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          branchCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl()
        }),
        h4: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl()
        }),
        h4Address: new FormGroup({
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl()
        }),
        h4Contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        }),
        h6: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          branchCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl(),
          serviceSubType: new FormControl(),
          salesOfficeIdCode: new FormControl(),
          salesPersonPrimaryCid: new FormControl(),
          salesPersonSecondaryCid: new FormControl(),
          cllidCode: new FormControl(),
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          cityCode: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl(),
          buildingName: new FormControl(),
          floor: new FormControl(),
          roomNumber: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        }),
        h6Address: new FormGroup({
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl(),
          buildingName: new FormControl(),
          floor: new FormControl(),
          roomNumber: new FormControl()
        }),
        h6Contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        })
      }),
      order: new FormGroup({
        orderAction: new FormControl(),
        ftn: new FormControl(),
        orderTypeCode: new FormControl(),
        orderSubTypeCode: new FormControl(),
        productTypeCode: new FormControl(),
        parentFtn: new FormControl(),
        relatedFtn: new FormControl(),
        telecomServicePriority: new FormControl(),
        carrierPartnerWholesaleType: new FormControl(),
        customerCommitDate: new FormControl(),
        customerWantDate: new FormControl(),
        customerOrderSubmitDate: new FormControl(),
        customerSignedDate: new FormControl(),
        //orderSubmitDate: new FormControl(),
        customerPremiseCurrentlyOccupiedFlag: new FormControl(),
        willCustomerAcceptServiceEarlyFlag: new FormControl(),
        multipleOrderFlag: new FormControl(),
        //multipleOrderIndex: new FormControl(),
        //multipleOrderTotal: new FormControl(),
        escalatedFlag: new FormControl(),
        expediteTypeCode: new FormControl(),
        govtTypeCode: new FormControl(),
        //vendorVpnCode: new FormControl(),
        prequalNo: new FormControl(),
        scaNumber: new FormControl()
      }),
      disconnect: new FormGroup({
        reasonCode: new FormControl(),
        cancelBeforeStartReasonCode: new FormControl(),
        cancelBeforeStartReasonText: new FormControl(),
        contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        })
      }),
      notes: new FormGroup({
        description: new FormControl(),
        isInternal: new FormControl()
      }),
      vendorOrderDetails: new FormGroup({
        vendorOrderNo: new FormControl({
          value: null, disabled: true
        }),
        vendorName: new FormControl({
          value: null, disabled: true
        }),
        country: new FormControl({
          value: null, disabled: true
        }),
        bypassVendorOrderCode: new FormControl({
          value: false, disabled: true
        })
      }),
      
      mileStoneInformation: new FormGroup({
        status: new FormControl({ value: null, disabled: true }),
        region: new FormControl({ value: null, disabled: true }),
        submitDate: new FormControl({ value: null, disabled: false }),
        customerCommitDate: new FormControl({ value: null, disabled: false }),
        validatedDate: new FormControl({ value: null, disabled: false }),
        customerWantDate: new FormControl({ value: null, disabled: false }),
        orderSentToVendorDate: new FormControl({ value: null, disabled: false }),
        orderAcknowledgedDate: new FormControl({ value: null, disabled: false }),
        vendorConfirmedDisconnectDate: new FormControl({ value: null, disabled: false }),
        disconnectCompletedDate: new FormControl({ value: null, disabled: false }),
        targetDeliveryDate: new FormControl({ value: null, disabled: false }),
        targetDeliveryDateReceived: new FormControl({ value: null, disabled: false }),
        accessDeliveryDate: new FormControl({ value: null, disabled: false }),
        vendorContractStartDate: new FormControl({ value: null, disabled: false }),
        accessAcceptDate: new FormControl({ value: null, disabled: false }),
        vendorContractTerm: new FormControl({ value: null, disabled: false }),
        billClearInstallDate: new FormControl({ value: null, disabled: false }),
        customerContractStartDate: new FormControl({ value: null, disabled: false }),
        closedDate: new FormControl({ value: null, disabled: false }),
        customerContractTerm: new FormControl({ value: null, disabled: true }),
        vendorCancellationDate: new FormControl({ value: null, disabled: false }),
       // nrmupdate: new FormControl({value: null, disabled: false }),
        nrmupdate:new FormControl(false),
        accessCityName: new FormControl('', [Validators.required])
      }),
      vendorCircuitInformation: new FormGroup({
        vendorOrderId: new FormControl(),
        vendorCircuitId: new FormControl(),
        lecId: new FormControl(),
        vlanId: new FormControl(),
        noOfIpAddresses: new FormControl(),
        routingProtocol: new FormControl(),
        customerIpAddresses: new FormControl(),
        specialCustomerAccessDetails: new FormControl(),
        xCnnctPrvdr: new FormControl(),
        tDwnAssn: new FormControl(),
        tDwnReq: new FormControl()
      }),
      vendorOrderSearch: new FormGroup({
        vendor: new FormControl({ value: null, disabled: true }),
        vendorOrderNo: new FormControl({ value: '', disabled: false }),
        customerId: new FormControl({ value: '', disabled: false }),
        ctn: new FormControl({ value: '', disabled: false })
      }),
      vedorAccessCostInformation: new FormGroup({
        revisedVendorTerm: new FormControl({ value: '', disabled: false }),
        revisedQuotedCurrency: new FormControl({ value: '', disabled: false }),
        revisedVendorRawMrc: new FormControl({ value: '', disabled: false }),
        revisedVendorRawNrc: new FormControl({ value: '', disabled: false })
      }),
      nccoOrderDetails: new FormGroup({
        ctn: new FormControl({ value: null, disabled: true }),
        orderType: new FormControl(),
        productType: new FormControl(),
        vendorName: new FormControl(),
        sitCountry: new FormControl(),
        sitCity: new FormControl(),
        cwd: new FormControl(),
        ccd: new FormControl(),
        custName: new FormControl(),
        h5AccountNumber: new FormControl(),
        sol: new FormControl(),
        oe: new FormControl(),
        privateLineNumber: new FormControl(),
        sotsNumber: new FormControl(),
        ordrSentLassie: new FormControl(),
        billCycleNumber: new FormControl(),
        emailNotification: new FormControl(),
        comment: new FormControl(),
        counter: new FormControl(),
      }),
      nrm: new FormGroup({}),
      h5Folder: new FormGroup({
        h5CustId: new FormControl(''),
        h5CustName: new FormControl(''),
        city: new FormControl(''),
        country: new FormControl('')
      }),
      asr: new FormGroup({
        asrType: new FormControl(''),
        origTerm: new FormControl("Off Shore to Intl Off Shore Router"),
        aggregated: new FormGroup({
          accessCityNameSiteCode: new FormControl(),
          ipNode: new FormControl(),
          accessBandwidth: new FormControl(),
          transparent: new FormControl(),
          partnerCarrierCode: new FormControl(),
          lecNni: new FormControl(),
        }),
        dedicated: new FormGroup({
          ipNode: new FormControl(),
          accessBandwidth: new FormControl(),
          partnerCarrierCode: new FormControl(),
        }),
        tdm: new FormGroup({
          ipNode: new FormControl(),
          accessBandwidth: new FormControl(),
          partnerCarrierCode: new FormControl(),
          entranceAssignment: new FormControl()
        }),
        ipl: new FormGroup({
          ipNode: new FormControl(),
          accessBandwidth: new FormControl(),
        }),
        offnet: new FormGroup({
          masterVas: new FormControl(),
          dlci: new FormControl(),
          isH1Matched: new FormControl()
        }),
        notes: new FormControl()
      })
    })
    this.logicallis = new FormGroup({
      notes: new FormControl()
    })

    this.activatedRoute.queryParams.subscribe(params => {
      this.workGroup = this.activatedRoute.snapshot.params["WG"] || 0
      this.taskId = this.activatedRoute.snapshot.queryParams['taskId'] || 0

      // NCCO Related
      this.amnciService.orderCatId = this.activatedRoute.snapshot.queryParams['catId'] || 0;

      //this.setValidations()
      this.getInitialData()
      //this.setDefaultValues()
    })
  }

  getInitialData() {
    if (this.orderId > 0) {
      //let data = zip(
      //  this.accessCitySiteService.get(), // CITY SITE
      //  this.xnciRegionService.get(), // REGION
      //  this.accessMbService.get(), // ACCESS MB
      //  this.countrySrvc.get(), // COUNTRY
      //  this.asrTypeService.get(), // ASR TYPE
      //  this.custContractLengthService.get(), // CONTRACT TERM
      //  this.stdiSrvc.get(), // STDI REASON
      //).pipe(
      //  concatMap(
      //    res => zip(
      //      of(res), // DROPDOWNS
      //      this.orderService.getById(this.orderId), // MAIN ORDER DETAILS
      //      this.orderService.getParentOrderType(this.orderId), // PARENT ORDER TYPE
      //      this.taskService.getCurrentStatus(this.taskId, this.workGroup, this.orderId), // CURRENT TASK NAME
      //      this.h5FolderService.getByOrderId(this.orderId), // H5 FOLDER
      //      this.workGroupService.GetLatestNonSystemOrderNoteInfo(this.orderId), // LAST NOTE INSERTED BY
      //      this.orderStdiHistoryService.getByOrderId(this.orderId), // ORDER STDI HISTORY
      //      this.fsaOrderCpeLineItemService.getByOrderId(this.orderId), // FSA CPE ORDER LINE ITEMS
      //      this.orderService.getTransportOrder(this.orderId), // TRANSPORT ORDER DATA
      //      this.circuitService.GetPreQualLineInfo(this.orderId), // VENDOR ACCESS COST
      //      this.vendorOrderService.getByOrderId(this.orderId), // VENDOR ORDER DETAILS
      //      this.orderService.getRelatedOrderId(this.orderId), // RELATED ORDER ID
      //      this.workGroupService.GetFTNListDetails(this.orderId, this.workGroup), // M5 DETAIL
      //      this.ccdService.GetCCDHistory(this.orderId), // CCD HISTORY
      //      this.orderNoteService.getByOrderId(this.orderId), // ORDER NOTE
      //      this.userProfileService.getMapUserProfilesByUserId(this.userId),
      //      this.nccoOrderService.getNCCOOrderData(this.orderId), // NCCO ORDER DETAILS,
      //      this.orderAddressService.getByOrderId(this.orderId),
      //    )
      //  )
      //)

      let data = zip(
        this.accessCitySiteService.get(), // CITY SITE
        this.xnciRegionService.get(), // REGION
        this.accessMbService.get(), // ACCESS MB
        this.countrySrvc.getAllForLookup(), // COUNTRY
        this.asrTypeService.get(), // ASR TYPE
        this.custContractLengthService.get(), // CONTRACT TERM
        this.stdiSrvc.get(), // STDI REASON

        this.orderService.getById3(this.orderId, this.workGroup, this.taskId), // MAIN ORDER DETAILS
        this.currencyService.get(),
        this.userService.checkPermissionForBAROrders(),
        this.orderService.checkIfOrderIsBAR(this.orderId)
      );

      this.spinner.show()
      data.subscribe(
        res => {
          //console.log(res)

          //let accessCityNameList = res[0][0]
          //let regionList = res[0][1]
          //let accessMbList = res[0][2]
          //let countryList = res[0][3]
          //let asrTypeList = res[0][4]
          //let contractTerm = res[0][5]
          //let stdiList = res[0][6]
          //console.log(res[0][7]);
          //let orderDetails = res[1]
          //let parentOrderType = res[2]
          //let orderStatus = res[3]
          //let h5Folder = res[4]
          //let lastNoteBy = res[5] != "" ? ` - (Last updated by: ${res[5]})` : res[5]
          //let orderStdiHistory = res[6]
          //let fsaOrderCpeLineItem = res[7]
          //let transportOrder = res[8]
          //let vendorAccessCost = res[9]
          //let vendorOrder = res[10]
          //let relatedOrderId = res[11]
          //let m5DetailList = res[12]
          //let ccdHistory = res[13]
          //let orderNote = res[14]
          //let loggedInUserProfilesList = res[15]
          //let nccoOrder = res[16]
          //let orderAddress = res[17] as any[]
          //this.h6Address = orderAddress.find(a => a.cisLvlType == "H6");

          let accessCityNameList = res[0]
          let regionList = res[1]
          let accessMbList = res[2]
          let countryList = res[3]
          let asrTypeList = res[4]
          let contractTerm = res[5]
          let stdiList = res[6]

          this.orderResult = res[7]
          this.currencyList = res[8]

          let orderDetails = this.orderResult.orderInfo
          console.log(this.orderResult)
          let orderStatus = this.orderResult.currentTaskName
          let transportOrder = this.orderResult.transportOrderDetails
          let vendorAccessCost = this.orderResult.preQualLineInfo
          let m5DetailList = this.orderResult.ftnListDetails
          let lastNoteBy = this.orderResult.latestNonSystemOrderNote
          //this.ftn = this.orderResult.orderInfo.fsaOrdr.ftn;
          let fsaOrderCpeLineItem = orderDetails.fsaOrdr != null
            ? orderDetails.fsaOrdr.fsaOrdrCpeLineItem as any[] : null
          let parentOrderType = orderDetails.fsaOrderTypeDesc
          let relatedOrderId = orderDetails.relatedFtn
          let h5Folder = orderDetails.h5Foldr
          let vendorOrder = orderDetails.vndrOrdr
          let orderStdiHistory = orderDetails.ordrStdiHist as any[]
          let ccdHistory = orderDetails.ccdHist as any[]
          let orderNote = orderDetails.ordrNte as any[]
          let nccoOrder = orderDetails.nccoOrdr
          let orderAddress = orderDetails.ordrAdr as any[]
          if (orderAddress != null) {
            this.h6Address = orderAddress.find(a => a.cisLvlType == "H6");
          }

          this.orderCatId = orderDetails.orderCategoryId
          this.orderTypeDesc = orderDetails.orderTypeDesc
          this.parentOrderType = parentOrderType
          this.productTypeCd = orderDetails.productTypeDesc
          this.orderStatusId = orderDetails.orderStatusId
          this.regionList = regionList
          console.log(this.orderStatusId)
          this.isGomCancelTaskCompleted = this.orderResult.isGomCancelTaskCompleted

          // Set M5 Details
          if (this.orderCatId == 4) {
            // Set NCCO
            this.nccoOrder = nccoOrder
            this.form.get("nccoOrderDetails.ctn").setValue(this.nccoOrder.ordrId);
            this.form.get("nccoOrderDetails.orderType").setValue(this.nccoOrder.ordrTypeId);
            this.form.get("nccoOrderDetails.productType").setValue(this.nccoOrder.prodTypeId);
            this.form.get("nccoOrderDetails.vendorName").setValue(this.nccoOrder.vndrCd);
            this.form.get("nccoOrderDetails.sitCountry").setValue(this.nccoOrder.ctryCd);
            this.form.get("nccoOrderDetails.sitCity").setValue(this.nccoOrder.siteCityNme);
            this.form.get("nccoOrderDetails.cwd").setValue(this.nccoOrder.cwdDt);
            this.form.get("nccoOrderDetails.ccd").setValue(this.nccoOrder.ccsDt);
            this.form.get("nccoOrderDetails.custName").setValue(this.nccoOrder.custNme);
            this.form.get("nccoOrderDetails.h5AccountNumber").setValue(this.nccoOrder.h5AcctNbr);
            this.form.get("nccoOrderDetails.sol").setValue(this.nccoOrder.solNme);
            this.form.get("nccoOrderDetails.oe").setValue(this.nccoOrder.oeNme);
            this.form.get("nccoOrderDetails.privateLineNumber").setValue(this.nccoOrder.plNbr);
            this.form.get("nccoOrderDetails.sotsNumber").setValue(this.nccoOrder.sotsNbr);
            this.form.get("nccoOrderDetails.ordrSentLassie").setValue(this.nccoOrder.ordrByLassieCd == "Y" ? 1 : 2);
            this.form.get("nccoOrderDetails.billCycleNumber").setValue(this.nccoOrder.billCycNbr);
            this.form.get("nccoOrderDetails.emailNotification").setValue(this.nccoOrder.emailAdr);
            this.form.get("nccoOrderDetails.comment").setValue(this.nccoOrder.cmntTxt);
          } else if ([2, 3, 6].includes(this.orderCatId)) {
            // Set FSA

            this.m5DetailList = m5DetailList.reduce(
              (a, b) =>
                a.find(
                  c =>
                    c.ftn === b.ftn &&
                    c.cusT_NME === b.cusT_NME &&
                    c.ordR_TYPE === b.ordR_TYPE &&
                    c.ordR_SUB_TYPE === b.ordR_SUB_TYPE &&
                    c.proD_TYPE === b.proD_TYPE &&
                    c.ordR_STUS_DES === b.ordR_STUS_DES &&
                    c.acT_TASK === b.acT_TASK
                ) ? [...a] : [...a, b], []
            )

            if(this.m5DetailList.length > 0) {
 
              // Added this line of code because some m5Details include the Related order. [Following block of code is for display only]
              m5DetailList = m5DetailList.filter( x => x.ordR_ID == this.orderId);

              this.m5Data = m5DetailList[0];
              this.m5  = this.m5Data["ftn"];
              this.form.get("display.customerName").setValue(this.m5Data["cusT_NME"]);
              this.form.get("display.orderType").setValue(this.m5Data["ordR_TYPE"]);
              this.form.get("display.orderSubType").setValue(this.m5Data["ordR_SUB_TYPE"]);
              this.form.get("display.productType").setValue(this.m5Data["proD_TYPE"]);
              this.form.get("display.orderStatus").setValue(this.m5Data["ordR_STUS_DES"]);
              this.form.get("display.pendingTasks").setValue(this.m5Data["acT_TASK"]);
            }


            this.activeTasks = m5DetailList.reduce(
              (a, b) =>
                a.find(
                  c =>
                    c.tasK_ID === b.tasK_ID
                ) ? [...a] : [...a, b], []
            )

            this.ccdList = ccdHistory
          }

          // Set General Info        
          this.orderStatus = orderStatus
          this.form.get("mileStoneInformation.status").setValue(this.orderStatus)
          console.log(this.showClearAcr)
          console.log(this.showClearRts)
          console.log(this.orderStatus)
          let transportOrderDetails = transportOrder.transportOrders.length > 0 ? transportOrder.transportOrders[0] : {}
          this.form.get("mileStoneInformation.region").setValue(transportOrderDetails.aRgnID)
          if(transportOrderDetails.nrM_UPDT_CD == "True"){
          this.form.get("mileStoneInformation.nrmupdate").setValue(true)
            }
          else{
          this.form.get("mileStoneInformation.nrmupdate").setValue(false)
            }

          // Set Order Milestone
          this.custContractLengthList = contractTerm
          let orderMilestone = transportOrder.orderMilestones.length > 0 ? transportOrder.orderMilestones[0] : {}
          this.form.get("mileStoneInformation.submitDate").setValue(orderMilestone.submitDate)
          this.form.get("mileStoneInformation.customerWantDate").setValue(orderMilestone.cwd)
          this.form.get("mileStoneInformation.customerCommitDate").setValue(orderMilestone.ccd)
          this.form.get("mileStoneInformation.validatedDate").setValue(orderMilestone.validatedDate)
          this.form.get("mileStoneInformation.billClearInstallDate").setValue(orderMilestone.billClearDate)
          this.form.get("mileStoneInformation.closedDate").setValue(orderMilestone.cusT_ACPTC_TURNUP_DT)
          this.rawClosedDate = orderMilestone.cusT_ACPTC_TURNUP_DT;
          this.form.get("mileStoneInformation.disconnectCompletedDate").setValue(orderMilestone.ordrDiscDate)
          this.form.get("mileStoneInformation.vendorCancellationDate").setValue(orderMilestone.vendorCancellationDate)
          //console.log(orderMilestone)
          // this.form.get("mileStoneInformation.vendorConfirmedDisconnectDate").setValue(orderMilestone.ven)

          //this.form.get("mileStoneInformation.customerContractTerm").setValue(install.transport.serviceTerm)

          // Set Circuit Milestones
          let circuitMilestone = transportOrder.circuitMilestones.length > 0 ? transportOrder.circuitMilestones[0] : {}
          this.form.get("mileStoneInformation.targetDeliveryDate").setValue(circuitMilestone.tdd)
          this.form.get("mileStoneInformation.targetDeliveryDateReceived").setValue(circuitMilestone.tddr)
          this.form.get("mileStoneInformation.accessAcceptDate").setValue(circuitMilestone.aad)
          this.form.get("mileStoneInformation.accessDeliveryDate").setValue(circuitMilestone.add)
          this.form.get("mileStoneInformation.vendorContractTerm").setValue(circuitMilestone.ttrpT_ACCS_CNTRC_TERM_CD)
          this.form.get("mileStoneInformation.vendorContractStartDate").setValue(circuitMilestone.cntrC_STRT_DT)
          this.form.get("mileStoneInformation.vendorConfirmedDisconnectDate").setValue(circuitMilestone.vcdd)

          // Set Order Note
          this.lastNoteBy = lastNoteBy
          this.noteList = orderNote.map(
            a => {
              a.nteTxt = this.sanitizer.bypassSecurityTrustHtml(a.nteTxt)
              return a
            }
          )

          // Set Vendor Access Cost
          this.vendorAccessCostInformationList = vendorAccessCost
          if (this.vendorAccessCostInformationList.length > 0) {
            this.form.get("vendorOrderSearch.vendor").setValue(this.vendorAccessCostInformationList[0].vendorName)
            this.form.get("vedorAccessCostInformation.revisedVendorTerm").setValue(this.vendorAccessCostInformationList[0].vndrTerm)
            this.form.get("vedorAccessCostInformation.revisedQuotedCurrency").setValue(this.vendorAccessCostInformationList[0].tportVndrRvsdQotCur)
            this.form.get("vedorAccessCostInformation.revisedVendorRawMrc").setValue(this.vendorAccessCostInformationList[0].tportVndrRawMrcAmt)
            this.form.get("vedorAccessCostInformation.revisedVendorRawNrc").setValue(this.vendorAccessCostInformationList[0].tportVndrRawNrcAmt)
          }

          // Vendor Access Cost
          this.vendorAccessCostList = this.orderResult.circuitCost.filter(a => a.vndrCktId != "")
          console.log(this.vendorAccessCostList)

          // Set CPE Line Item
          if (this.productTypeCd == 'CPE') {
            this.fsaOrderCpeLineItemList = fsaOrderCpeLineItem
            //console.log(this.fsaOrderCpeLineItemList);

           this.wgService.GetGOMSpecificData(this.orderId).subscribe(res => {
             this.cpeLineItemsList = res;
              //this.getGomSpecific(fsaOrderCpeLineItem) // COMBINATION OF FSA_ORDR_GOM_XNCI OF PROFILE_ID (2,6,100) AND 114
              console.log(this.cpeLineItemsList)
              if (this.cpeLineItemsList != null && this.cpeLineItemsList.length > 0) {
                if (!this.helper.isEmpty(this.cpeLineItemsList.map(a => a.cpE_CMPLT_DT)[0])) {
                  this.form.get("cpe.cpeCompletionDate").setValue(this.datepipe.transform(this.cpeLineItemsList.map(a => a.cpE_CMPLT_DT)[0], 'MM/dd/yyyy'));
                }

                this.form.get("cpe.requisitionAmount").setValue(this.cpeLineItemsList.map(a => a.rqstN_AMT)[0]);
                this.form.get("cpe.cpeEquipmentType").setValue(this.cpeLineItemsList.map(a => a.cpE_EQPT_TYPE_TXT)[0]);

                console.log(this.cpeLineItemsList.map(a => a.cpE_STUS_ID)[0])

                let cpeOrderStatusId = this.cpeLineItemsList.map(a => a.cpE_STUS_ID)[0] || 1;
                this.form.get("cpe.cpeOrderStatus").setValue(cpeOrderStatusId)
                console.log(this.form)
                console.log(cpeOrderStatusId)
              }
            });
            //if (gomSpecific != null) {
            //  this.form.get("cpe.cpeOrderStatus").setValue(gomSpecific.cpeStusId)
            //  //CompleteDate: this.getFormControlValue("gomFields.cpeCompletionDate") == "" ? new Date() : new Date(this.getFormControlValue("gomFields.cpeCompletionDate")),
            //  this.form.get("cpe.cpeCompletionDate").setValue(gomSpecific.cpeCmpltDt)
            //  this.form.get("cpe.requisitionAmount").setValue(gomSpecific.rqstnAmt)
            //  this.form.get("cpe.cpeEquipmentType").setValue(gomSpecific.cpeEqptTypeTxt)
            //}
          }

          // Set H5 Folder
          this.countryList = countryList
          console.log(h5Folder)
          this.h5Folder = h5Folder
          this.h5DocumentList = this.h5Folder ? this.h5Folder.h5Docs : null
          this.h5DocumentList = this.h5DocumentList.map(data => {
            data.fileSizeQtyStr = this.fileSizePipe.transform(data.fileSizeQty);
            return data;
          })
          console.log(this.h5DocumentList)

          // Set Vendor Order
          this.getVendorOrder(vendorOrder, true);
          this.form.get("mileStoneInformation.accessCityName").setValue(transportOrderDetails.accCitySite)
          this.form.get("vendorOrderSearch.vendor").setValue(transportOrderDetails.vendor)

          // Set Vendor Circuit Information
          console.log(orderDetails.ordrVlan)

          let orderVlan = [];
          if(orderDetails.ordrVlan.length > 0) {
            let filteredOrdrVlan = orderDetails.ordrVlan.filter(a => a.vlanId != "");
            if(filteredOrdrVlan.length > 0) {
              orderVlan = filteredOrdrVlan.map(a => a.vlanId);
            }
          }

          // let orderVlan = orderDetails.ordrVlan.length > 0 ? orderDetails.ordrVlan.filter(a => a.vlanId != "").map(a => a.vlanId) : []\
          console.log("orderVlan")
          console.log(orderVlan)
          let orderVlanId = (orderVlan as string[]).join(",")
          console.log(orderVlanId)
          this.form.get("vendorCircuitInformation.vendorOrderId").setValue(circuitMilestone.rltD_VNDR_ORDR_ID)
          this.form.get("vendorCircuitInformation.vendorCircuitId").setValue(circuitMilestone.vendorCktID)
          this.form.get("vendorCircuitInformation.vlanId").setValue(orderVlanId)
          this.form.get("vendorCircuitInformation.lecId").setValue(circuitMilestone.lecid)
          this.form.get("vendorCircuitInformation.noOfIpAddresses").setValue(circuitMilestone.iP_ADR_QTY)
          this.form.get("vendorCircuitInformation.routingProtocol").setValue(circuitMilestone.rtnG_PRCOL)
          this.form.get("vendorCircuitInformation.customerIpAddresses").setValue(circuitMilestone.cusT_IP_ADR)
          this.form.get("vendorCircuitInformation.specialCustomerAccessDetails").setValue(transportOrderDetails.scadText || circuitMilestone.scA_DET)
          this.form.get("vendorCircuitInformation.xCnnctPrvdr").setValue(circuitMilestone.xcnnctPrvdr)
          this.form.get("vendorCircuitInformation.tDwnAssn").setValue(circuitMilestone.tdwnAssn)
          this.form.get("vendorCircuitInformation.tDwnReq").setValue(circuitMilestone.tdwnReq)


          // Set Initiate ASR
          this.asrTypeList = [{}, ...asrTypeList]
          this.accessMbList = accessMbList
          this.accessCityNameList = accessCityNameList

          // Set STDI
          this.stdiList = stdiList
          this.stdiReasonList = orderStdiHistory

          console.log("A")
          if(this.m5Data["ordR_STUS_DES"] === 'Completed' && this.helper.isReallyEmpty(this.rawClosedDate) && this.vendorOrderEmailList.length === 0) {
            this.isAbleToManageVndrOrdrMs = true;
          }
          this.UnlockPageControls();

          if (!this.isClosed) {
            if (this.orderId != 0) {
              this.CheckIn();
            }
          } else {
            //this.isClosed = true;
            this.lblError = "Order is closed for edit.";
            this.LockPageControls();
          }
          
          if (this.isOrderReviewer()) {
            this.lblError = "User has Read Only permissions, the order will be opened in locked mode.";
            this.LockPageControls();
          }

          let profiles = this.userService.loggedInUser.profiles.filter(
            a =>
              a.toUpperCase().includes("NCI")
          ) as string[]

          let gomProfiles = this.userService.loggedInUser.profiles.filter(
            a =>
              a.toUpperCase().includes("GOM")
          ) as string[]

          if (profiles.length > 0 && gomProfiles.length <= 0) {
            this.form.get("cpe.cpeOrderStatus").disable();
            this.form.get("cpe.cpeCompletionDate").disable();
            this.form.get("cpe.requisitionAmount").disable();
            this.form.get("cpe.cpeEquipmentType").disable();
          }
          //this.loggedInUserProfilesList = loggedInUserProfilesList
          //if (this.showNCCO && !this.loggedInUserProfilesList.find(a => a.usrPrfDes.includes('NCI'))) {
          if (this.showNCCO && profiles.length <= 0) {
            this.lblError = "Order opened in read-only mode.";
            this.LockPageControls();
          }

          if (this.isReadonly) {
            this.lblError = "User has Read Only permissions, the order will be opened in locked mode.";
            this.LockPageControls();
          }

          //console.log(gomSpecific)
          this.isOnLoad = false;
          console.log(this.showCPE)

          if(this.isOrderComplete) {
            this.LockPageControls();
          }

          const hasBAREditConfig = res[9];
          //const isBARPendingOrder = res[10];
          //const isBAR = this.orderStatusId === OrderStatus.Completed && isBARPendingOrder;
          this.isBAREditAllowed = hasBAREditConfig && !this.helper.isEmpty(this.rawClosedDate);//&& isBAR;

          console.log(m5DetailList)
          console.log(this.m5DetailList)
          console.log(this.m5DetailList
            .filter(a => a.acT_TASK != null))
          console.log(this.m5DetailList
            .filter(a => a.acT_TASK != null)
            .find(a => a.tasK_ID == 223))

          console.log(this.activeTasks)
          console.log(this.activeTasks.find(a => a.tasK_ID == 223))

          console.log(this.showRequestToLogicallis && !this.isReadonly)
          console.log(this.showRequestToLogicallis)
          console.log(!this.isReadonly)
          this.spinner.hide()
        },
        error => {
          this.spinner.hide()
        },
        () => {
          //if (this.m5DetailList[0] != null) {
          if (this.orderResult != null) {
            //this.onM5Clicked(null, this.m5DetailList[0])
            this.onM5Clicked(null, this.orderResult)
          }

          //if (this.isFromNCCO) {
          //  this.formNCCO();
          //}
          this.setValidations()
          this.spinner.hide()
        }
      )
    }
  }

  // Action Buttons
  cancel() {
    this.orderCompleted();
  }
handleValueChange (e) {
this.nrmupdate = e.value;
       // const previousValue = e.previousValue;
       // const newValue = e.value;
        // this.nrmupdate = newValue;
    }

  submit() {
    //this.orderService.updateNrm(this.orderId,this.nrmupdate).subscribe(res => {})
    
    //this.isOnLoad = true;
    // this.saveStdi(null);
    let orderModel = {
      OrderId: this.orderId,
      UserId: this.userId,
      //NoteText:this.form.get("notes.description").value== null? 
      //NoteText:this.notes.description,

      NoteText: this.form.get("notes.description").value == null ? "" : this.form.get("notes.description").value,
      TaskId: this.taskId,
      //CPEStatusId: this.getFormControlValue("cpe.cpeOrderStatus"),
     // CpeEquipmentType: this.getFormControlValue("cpe.cpeEquipmentType"),
     // CompleteDate: this.getFormControlValue("cpe.cpeCompletionDate") == "" ? new Date() : new Date(this.getFormControlValue("cpe.cpeCompletionDate")),
     // ReqAmount: this.getFormControlValue("cpe.requisitionAmount")
      CPEStatusId: this.form.get("cpe.cpeOrderStatus").value== null ? 1 : this.form.get("cpe.cpeOrderStatus").value,
      CpeEquipmentType: this.form.get("cpe.cpeEquipmentType").value == "" ? null : this.form.get("cpe.cpeEquipmentType").value,
      //CompleteDate:this.form.get("cpe.cpeCompletionDate").setValue(cpe.cpeCompletionDate),
      CompleteDate: this.form.get("cpe.cpeCompletionDate").value == "" ? new Date().toLocaleDateString: this.helper.dateObjectToString(this.form.get("cpe.cpeCompletionDate").value),
      ReqAmount: this.form.get("cpe.requisitionAmount").value== "" ? null : this.form.get("cpe.requisitionAmount").value,
      GOMItems: this.fsaOrderCpeLineItemList.map(data => {
        data['custDlvryDt'] = this.helper.dateObjectToString(data['custDlvryDt']);
        return data;
      }),
      WorkGroup: this.workGroup
    }

    let orderFormInfo = {
      fsaInfo: this.getFsaInfo(),
      milestoneInfo: this.getMilestoneInfo(),
      circuitInfo: this.getCircuitInfo(),
      nteTxt: this.form.get("notes.description").value,
      userProfileId: this.workGroup,
      
    }

    console.log("orderforminfo")
    console.log(orderFormInfo)

    if (this.validateFormData(orderFormInfo)) {
      this.spinner.show();
 //if (this.showCPE) {
      let data = this.dcpeService.SaveAMNCISpecificData(orderModel).pipe(
        concatMap(res => this.orderNoteService.getByOrderId(this.orderId))
      )

      data.subscribe(res => {
        this.noteList = res
        //this.helper.notifySavedFormMessage("Order has been updated!", Global.NOTIFY_TYPE_SUCCESS, false, null);
      },
        error => {
          this.spinner.hide()
          console.log(error)
        })
    //} else {
    //  this.saveOrderNotes(null);
    //}
      let dataObs = [];
        dataObs.push(this.orderService.update(this.orderId, orderFormInfo));
        if(this.isAbleToManageVndrOrdrMs) {
          let SentToVendorDate = orderFormInfo.milestoneInfo['orderSentToVendorDate']
          let AcknowledgedDate = orderFormInfo.milestoneInfo['orderAcknowledgedDate']
          let vndrOrdrId = 0;
          if(this.vendorOrder != null) {
            vndrOrdrId = this.vendorOrder.vndrOrdrId;
          }

          let vendorOrderMs = new VendorOrderMs({
            vndrOrdrMsId: this.vndrOrdrMsId,
            vndrOrdrId: vndrOrdrId,
            verId: 99,
            vndrOrdrEmailId: null,
            sentToVndrDt: SentToVendorDate,
            ackByVndrDt: AcknowledgedDate
          })

          if(this.vndrOrdrMsId > 0) {
            dataObs.push(this.vendorOrderMsService.update(this.vndrOrdrMsId, vendorOrderMs));
          } else {
            dataObs.push(this.vendorOrderMsService.create(vendorOrderMs));
          }
        }

      forkJoin(dataObs).subscribe(response => {
        if (response[0] != 0) {
          this.router.navigate(
            [],
            {
              relativeTo: this.activatedRoute,
              queryParams: { taskId: response[0] },
              queryParamsHandling: 'merge'
            });
        }
        this.helper.notifySavedFormMessage(`Order`, Global.NOTIFY_TYPE_SUCCESS, false, null)
        this.form.get("notes.description").setValue("");
      }, error => {
        this.spinner.hide()
        this.helper.notifySavedFormMessage(`Order`, Global.NOTIFY_TYPE_ERROR, false, null)
      }, () => {
        this.spinner.hide()
      })
    }


    this.isSubmitted = true;
    //let generalInfo = this.getGeneralInfo()

    //this.spinner.show();
    //this.fsaOrdersService.update(this.orderId, generalInfo).subscribe(res => {
    //  this.helper.notifySavedFormMessage(`Order`, Global.NOTIFY_TYPE_SUCCESS, false, null)
    //}, error => {
    //  this.spinner.hide()
    //  this.helper.notifySavedFormMessage(`Order`, Global.NOTIFY_TYPE_ERROR, false, null)
    //}, () => this.spinner.hide())
  }

  editOrder() {
    this.spinner.show();
    this.orderService.UpdateCustomerTurnUpTask(this.orderId).subscribe(data => {
      if(data) {
        this.CheckIn(true);
      }
    });
  }

  completeCpe() {
    let orderFormInfo = {
      fsaInfo: this.getFsaInfo(),
      milestoneInfo: this.getMilestoneInfo(),
      circuitInfo: this.getCircuitInfo(),
      nteTxt: this.form.get("notes.description").value
    }

    this.spinner.show()
    let arr = [];
    arr.push(this.workGroupService.CompleteActiveTask(this.orderId, ETasks.xNCIReady, 2, "Completing xNCI Ready Task.", this.userId));
    if(this.showRequestToLogicallis) {
      arr.push(this.workGroupService.CompleteActiveTask(this.orderId, ETasks.xNCICPEDiscEmail, 2, "Completing xNCI Disconnect Email.", this.userId));
    }

    forkJoin(arr).subscribe( res => {
      this.cancel()
    }, error => {
      console.log(error)
      this.spinner.hide()
    })
    
  }

  returnToGom() {
    let orderFormInfo = {
      fsaInfo: this.getFsaInfo(),
      milestoneInfo: this.getMilestoneInfo(),
      circuitInfo: this.getCircuitInfo(),
      nteTxt: this.form.get("notes.description").value
    }

    this.orderService.moveToGom(this.orderId, orderFormInfo).subscribe(res => {
      //this.router.navigate([`/gom/${this.workGroup}/${this.orderId}`])
      this.cancel();
    }, error => {
      console.log(error)
      this.spinner.hide()
    }, () => this.spinner.hide())
  }

  returnToSales() {
    if((this.form.get("notes.description").value == "") || (this.form.get("notes.description").value == null))
      {
        if(confirm('No notes have been entered for Sales, do you wish to proceed?')){
          this.getReturnToSales();
        }
          else {
          console.log("Hit Cancel");
          }
      }
   else
   this.getReturnToSales();   
  }

 getReturnToSales(){

 let orderFormInfo = {
      fsaInfo: this.getFsaInfo(),
      milestoneInfo: this.getMilestoneInfo(),
      circuitInfo: this.getCircuitInfo(),
      nteTxt: this.form.get("notes.description").value
    }

    this.orderService.loadRtsTask(this.orderId, orderFormInfo).subscribe(res => {
      this.form.get("notes.description").setValue("");
      //this.router.navigate([`order/rts/${this.orderId}`])
      this.helper.notify("Updated Successfully !! Sales Support task loaded successfully.", Global.NOTIFY_TYPE_SUCCESS)
      
    }, error => {
      console.log(error)
      this.spinner.hide()
    }, () => this.spinner.hide())
    
    window.location.reload();
}

  clearAcrTask() {
    this.spinner.show()
    this.orderService.clearAcrTask(this.orderId, { taskId: this.taskId }).subscribe(res => {
      //  this.helper.notify("Updated Successfully !! Sales Support task loaded successfully.", Global.NOTIFY_TYPE_SUCCESS)
      //if (res) {
      this.cancel()
    }, error => {
      console.log(error)
      this.helper.notify("An error occurred while completing the current task.", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    })
  }

  salesSupportNotes() {
    this.router.navigate([`order/rts/156/${this.orderId}/${this.taskId}`]);
  }

  showRequestToLogicallisForm() {
    $("#logicallis").modal("show");
  }

  requestToLogicallis() {
    let notes = this.logicallis.get("notes").value

    this.spinner.show()
    this.orderService.requestToLogicallis(this.orderId, notes).subscribe(res => {
      console.log(res)
      $("#logicallis").modal("hide");
      this.cancel();
      //this.spinner.hide()
    }, error => {
      console.log(error)
      this.spinner.hide()
    })
  }
  // Action Buttons

  // CPE Line Item Section
  getGomSpecific(fsaOrderCpeLineItem) {
    //2,16,100 114
    if (fsaOrderCpeLineItem != null) {
      let fsaOrderGomXnci = fsaOrderCpeLineItem.filter(a => a.fsaOrdrGomXnci.length > 0).map(a => a.fsaOrdrGomXnci[0])
      //console.log(fsaOrderGomXnci)

      if (fsaOrderGomXnci.length > 0) {
        let top = fsaOrderGomXnci[0]
        let topGom = fsaOrderGomXnci.filter(a => a.usrPrfId == 114).length > 0 ? fsaOrderGomXnci.filter(a => a.usrPrfId == 114)[0] : top
        let topXnci = fsaOrderGomXnci.filter(a => [2, 16, 100].includes(a.usrPrfId)).length > 0 ? fsaOrderGomXnci.filter(a => [2, 16, 100].includes(a.usrPrfId))[0] : top

        return {
          cpeCmpltDt: topGom.cpeCmpltDt,
          cpeEqptTypeTxt: topGom.cpeEqptTypeTxt,
          cpeStusId: topGom.cpeStusId,
          rqstnAmt: topGom.rqstnAmt,

          //Not sure if these are still needed
          atlasWrkOrdrNbr: topGom.atlasWrkOrdrNbr,
          billFtn: topGom.billFtn,
          courierTrkNbr: topGom.courierTrkNbr,
          cpeVndrNme: topGom.cpeVndrNme,
          creatByUserId: topGom.creatByUserId,
          cstmDt: topGom.cstmDt,
          custDlvryDt: topXnci.custDlvryDt,
          custDscnctCd: topGom.custDscnctCd,
          custDscnctDt: topGom.custDscnctDt,
          custPrchCd: topGom.custPrchCd,
          custRnlCd: topGom.custRnlCd,
          custRnlTermNbr: topGom.custRnlTermNbr,
          custRntlCd: topGom.custRntlCd,
          custRntlTermNbr: topGom.custRntlTermNbr,
          drtbnShipDt: topGom.drtbnShipDt,
          eqptInstlDt: topXnci.eqptInstlDt,
          hdwrDlvrdDt: topXnci.hdwrDlvrdDt,
          logicalisMnthLeaseRtAmt: topGom.logicalisMnthLeaseRtAmt,
          logicalisRnlMnthLeaseRtAmt: topGom.logicalisRnlMnthLeaseRtAmt,
          plsftRcptId: topGom.plsftRcptId,
          plsftRelsDt: topGom.plsftRelsDt,
          plsftRelsId: topGom.plsftRelsId,
          plsftRqstnNbr: topGom.plsftRqstnNbr,
          prchOrdrBackOrdrShipDt: topGom.prchOrdrBackOrdrShipDt,
          prchOrdrNbr: topGom.prchOrdrNbr,
          rqstnDt: topGom.rqstnDt,
          shpmtSignByNme: topXnci.shpmtSignByNme,
          shpmtTrkNbr: topGom.shpmtTrkNbr,
          term60DayNtfctnDt: topGom.term60DayNtfctnDt,
          termEndDt: topGom.termEndDt,
          termStrtDt: topGom.termStrtDt,
          usrPrfId: topGom.usrPrfId,
          vndrCourierTrkNbr: topGom.vndrCourierTrkNbr,
          vndrRecvFromDt: topGom.vndrRecvFromDt,
          vndrShipDt: topGom.vndrShipDt
        }
      }
    }

    return null;
  }
  // CPE Line Item Section

  // M5 Details Section
  onM5Clicked(e, data) {
    if (e != null) {
      (e as MouseEvent).preventDefault()
    }

    console.log(data)
    var orderId = this.helper.isEmpty(data.ordR_ID) ? data.orderInfo.orderId : data.ordR_ID;
    console.log('orderId: ' + orderId)

    if (this.m5DetailsComponent.isShown && this.fsaDetails.id == orderId) { // Hide FSA Details
      this.m5DetailsComponent.isShown = false
    } else if (!this.m5DetailsComponent.isShown && this.fsaDetails.id == orderId) { // Show already loaded
      this.m5DetailsComponent.isShown = true
    } else { // Get new FSA Details
      //this.amnciService.getFsaDetails(data.ordR_ID, this)
      this.amnciService.getFsaDetails2(orderId, this)
    }

    if (this.isSubmitted) {
      this.m5DetailsComponent.isShown = true;
      this.isSubmitted = false;
    }
  }
  // M5 Details Section

  // H5 Folder Section
  showH5FolderSearch() {
    this.isH5FolderSearchShown = !this.isH5FolderSearchShown;
  }

  searchH5Folder() {
    this.spinner.show();
    let h5CustId = this.form.get('h5Folder.h5CustId').value;
    h5CustId = (this.helper.isEmpty(h5CustId)) ? 0 : h5CustId;
    let h5CustName = this.form.get('h5Folder.h5CustName').value;
    let city = this.form.get('h5Folder.city').value;
    let country = this.form.get('h5Folder.country').value;
    //console.log('Search H5 Folder: ' + h5CustId + h5CustName + city + country)
    this.h5FolderService.search(h5CustId, h5CustName, '', city, country).subscribe(
      res => {
        //console.log(res)
        this.h5FolderList = res;
      },
      error => {
        //console.log(error)
        this.helper.notify('Search H5 Folder failed.', Global.NOTIFY_TYPE_ERROR);
      },
      () => {
        this.spinner.hide();
      }
    );
  }

  selectionH5FolderChangedHandler(data: any) {
    this.selectedItemH5FolderKeys = data.selectedRowKeys;
  }

  attachH5Folder() {
    if (this.selectedItemH5FolderKeys.length > 0) {
      let h5FldrId = this.selectedItemH5FolderKeys[0];
      this.h5Folder = this.h5FolderList.find(i => i.h5FoldrId == h5FldrId);
      this.h5FolderList = [];
      this.isH5FolderSearchShown = false;
      // Call Attach H5 folder - Save H5FolderID, RGN and Modified By on ORDR Table
      //console.log('Order ID: ' + this.orderId + ' H5Folder ID: ' + h5FldrId)
      this.orderService.attachH5Folder(this.orderId, h5FldrId).subscribe(
        res => {
          this.helper.notify('Successfully attached H5 Folder to this Order.', Global.NOTIFY_TYPE_SUCCESS);
        },
        error => {
          this.helper.notify('Failed to attach H5 Folder to this Order.', Global.NOTIFY_TYPE_ERROR);
        });
    }
  }

  onH5DocumentClicked(e, data) {
    (e as MouseEvent).preventDefault()

    console.log(data)
    let doc =data as DocEntity;
    if (doc != null) {
      this.docSrvc.download(doc).subscribe(
        res => {
          saveAs(res, doc.fileNme);
        },
        error => {
          this.helper.notify("File not found", Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }
  // H5 Folder Section

  // Vendor Order Details Section
  getVendorOrder(vendorOrder: VendorOrder, onLoad: boolean = false) {

    this.vendorOrder = vendorOrder
    
    if (this.vendorOrder != null) {
      this.vendorOrderNo = 'VON' + this.vendorOrder.vndrOrdrId.toString().padStart(10, '0');
      this.form.get("vendorOrderDetails.vendorName").setValue(this.vendorOrder.vndrNme + ' - ' + this.vendorOrder.vndrCd)
      this.form.get("display.vendorName").setValue(this.vendorOrder.vndrNme + ' - ' + this.vendorOrder.vndrCd)
      //this.form.get("vendorOrderDetails.country").setValue(this.vendorOrder.ctryNme)
      if (this.h6Address != null) {
        this.form.get("vendorOrderDetails.country").setValue(this.h6Address.ctryNme);
      }
      this.form.get("vendorOrderDetails.bypassVendorOrderCode").setValue(this.vendorOrder.bypasVndrOrdrMsCd)
      this.vendorOrderEmailList = this.processVendorOrderEmailList(this.vendorOrder.vndrOrdrEmail)
  
      if (this.vendorOrder.vndrOrdrMs != null) {
        let vendorSentDate = this.vendorOrder.vndrOrdrMs.filter(a => a.sentToVndrDt != null).map(a => a.sentToVndrDt) as any
        let vendorAckDate = this.vendorOrder.vndrOrdrMs.filter(a => a.ackByVndrDt != null).map(a => a.ackByVndrDt) as any

        vendorSentDate = vendorSentDate.length > 0 ? vendorSentDate[0] : null
        vendorAckDate = vendorAckDate.length > 0 ? vendorAckDate[0] : null
        this.form.get("mileStoneInformation.orderSentToVendorDate").setValue(vendorSentDate ? moment(vendorSentDate).format("MMM/DD/YYYY") : null)
        this.form.get("mileStoneInformation.orderAcknowledgedDate").setValue(vendorAckDate ? moment(vendorAckDate).format("MMM/DD/YYYY") : null)
      }
      if(onLoad) {
        this.checkIfVendorOrderEmailHasDummyData(this.vendorOrder.vndrOrdrMs);
      }   
      //this.form.get("vendorCircuitInformation.vendorOrderId").setValue(this.vendorOrder.vndrOrdrId)
    }
  }

  checkIfVendorOrderEmailHasDummyData(vendorOrderMs: VendorOrderMs[]) {
    // Having a dummy data with Version 99 flags the order as this Order's Vendor Sent Date and Vendor Acknowledgement date can be modified by XnciAdminEdit user.
    // and should not display any record on the list
    let data = vendorOrderMs.find(x =>x.verId == 99);
    if(data != undefined) {
      this.vndrOrdrMsId = data.vndrOrdrMsId;
      this.form.get("mileStoneInformation.orderSentToVendorDate").setValue(data.sentToVndrDt ? moment(data.sentToVndrDt).format("MMM/DD/YYYY") : null)
      this.form.get("mileStoneInformation.orderAcknowledgedDate").setValue(data.ackByVndrDt ? moment(data.ackByVndrDt).format("MMM/DD/YYYY") : null)
    }
  }

  processVendorOrderEmailList(data: Array<any>) {
    return data.map(record => {
      const status = record.emailStatusDesc;
      if (status === 'Draft') {
        record.icon = '';
      } else if (status.includes('UnEnc')) {
        record.icon = 'fa-unlock';
        record.emailStatusDesc = status.includes('Sent') ? 'Sent' : 'Unsent';
      } else if (status.includes('Enc')) {
        record.icon = 'fa-lock';
        record.emailStatusDesc = status.includes('Sent') ? 'Sent' : 'Unsent';
      }
      return record;
    })
  }

  searchVendorOrder(e) {
    let vendorOrderNo: string = this.helper.ifEmpty(this.form.get("vendorOrderSearch.vendorOrderNo").value, '')
    let customerId = this.helper.ifEmpty(this.form.get("vendorOrderSearch.customerId").value, 0)
    let ctn: string = this.helper.ifEmpty(this.form.get("vendorOrderSearch.ctn").value, '')

    this.spinner.show()
    this.vendorOrderService.getVendorOrder(vendorOrderNo, customerId, ctn).subscribe(res => {
      //console.log(res)
      this.vendorOrderList = res as VendorOrder[]
    }, error => {
        console.log(error)
        this.vendorOrderList = null;
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  onVendorOrderRowClick(e) {
    //console.log(e)
    let vendorOrder = e.data as VendorOrder
    vendorOrder.ordrId = this.orderId

    let data = zip(
      this.vendorOrderService.update(vendorOrder.vndrOrdrId, vendorOrder),
      this.vendorOrderService.getEmailsByVendorOrderId(vendorOrder.vndrOrdrId)
    )

    this.spinner.show()
    data.subscribe(res => {
      //console.log(res)
      this.vendorOrder = vendorOrder
      this.vendorOrderEmailList = this.processVendorOrderEmailList(res[1]);
      this.form.get("vendorOrderDetails.vendorName").setValue(this.vendorOrder.vndrNme)
      //this.form.get("vendorOrderDetails.country").setValue(this.vendorOrder.ctryNme)
      if (this.h6Address != null) {
        this.form.get("vendorOrderDetails.country").setValue(this.h6Address.ctryNme);
      }
      this.helper.notify("Vendor Order has been successfully associated", Global.NOTIFY_TYPE_SUCCESS)
    }, error => {
      console.log(error)
      this.helper.notify("Vendor Order could not be associated", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  onEmailUpdating(e) {
    e.cancel = true
    let data = e.oldData as VendorOrderEmail


    let vendorOrderMs = new VendorOrderMs({
      vndrOrdrId: data.vndrOrdrId,
      verId: 2,
      vndrOrdrEmailId: data.vndrOrdrEmailId,
      sentToVndrDt: data.sentToVendorDate,
      ackByVndrDt: e.newData.ackByVendorDate
    })

    if (vendorOrderMs.ackByVndrDt != undefined && vendorOrderMs.ackByVndrDt != null) {
      this.spinner.show()
      this.vendorOrderEmailService.updateEmailAckDate(data.vndrOrdrEmailId, vendorOrderMs).subscribe(res => {
        
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res as VendorOrderEmail[]);

        let vendorAckDate = this.vendorOrderEmailList.filter(a => a.ackByVendorDate != null).map(a => a.ackByVendorDate) as any

        vendorAckDate = vendorAckDate.length > 0 ? vendorAckDate[0] : null
        this.form.get("mileStoneInformation.orderAcknowledgedDate").setValue(vendorAckDate ? moment(vendorAckDate).format("MMM/DD/YYYY") : null)

        this.helper.notifySavedFormMessage("Vendor Order Email Acknowledge Date", Global.NOTIFY_TYPE_SUCCESS, false, null)
      }, error => {
        console.log(error)
        this.helper.notifySavedFormMessage("Vendor Order Email Acknowledge Date", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide()
        e.cancel = false
      })
    } else {
      data.vndrOrdrMs.push(vendorOrderMs)
      this.spinner.show()
      this.vendorOrderEmailService.update(data.vndrOrdrEmailId, data).subscribe(res => {
        
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res as VendorOrderEmail[]);
        this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_SUCCESS, false, null)
      }, error => {
        console.log(error)
        this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide()
        e.cancel = false
      })
    }

  }

  isEditAllowed(e) {
    return !e.row.isEditing && e.row.data.emailStatusDesc.includes("Sent") && e.row.data.ackByVendorDate == null && !this.isReadonly
  }
  // Vendor Order Details Section

  // Vendor Access Cost Section
  saveRevisedVendorDetails() {
    let fsaOrderModel = {
      tportVndrRawMrcAmt: this.form.get("vedorAccessCostInformation.revisedVendorRawMrc").value,
      tportVndrRawNrcAmt: this.form.get("vedorAccessCostInformation.revisedVendorRawNrc").value,
      vndrTerm: this.form.get("vedorAccessCostInformation.revisedVendorTerm").value,
      tportVndrRvsdQotCur: this.form.get("vedorAccessCostInformation.revisedQuotedCurrency").value,
    }

    this.spinner.show()
    this.fsaOrderService.UpdateVendorAccessCostInfo(this.orderId, fsaOrderModel).subscribe(
      res => {
        if (res != null) {
          this.circuitService.GetPreQualLineInfo(this.orderId).subscribe(data => {
            this.vendorAccessCostInformationList = data;
          });
          this.helper.notify("Vendor Access Cost Details saved successfully", Global.NOTIFY_TYPE_SUCCESS)
        }
        this.spinner.hide();
      }, error => {
        console.log(error)
        this.helper.notify("Vendor Access Cost Details could not be saved", Global.NOTIFY_TYPE_ERROR)
        this.spinner.hide();
      }, () => {
      })
  }
  // Vendor Access Cost Section

  // Order Milestones Section
  onTargetDeliveryDateChanged(e) {
    let targetDeliveryDate = e.value

    this.form.get("mileStoneInformation.targetDeliveryDateReceived").setValue(moment().format("MMM/DD/YYYY"))
  }

  onAccessDeliveryDateChanged(e) {
    let accessDeliveryDate = e.value

    this.form.get("mileStoneInformation.vendorContractStartDate").setValue(accessDeliveryDate ? moment(accessDeliveryDate).format("MMM/DD/YYYY") : null)
  }

  onBillClearIntallDateChanged(e) {
    let billClearIntallDateChanged = e.value
    this.isBillCleared = true;

    this.form.get("mileStoneInformation.customerContractStartDate").setValue(billClearIntallDateChanged ? moment(billClearIntallDateChanged).format("MMM/DD/YYYY") : null)
  }
onVendorConfirmedDisconnectDateChanged(e) {
    let vendorConfirmedDisconnectDateChanged = e.value

    this.form.get("mileStoneInformation.vendorConfirmedDisconnectDate").setValue(vendorConfirmedDisconnectDateChanged ? moment(vendorConfirmedDisconnectDateChanged).format("MMM/DD/YYYY") : null)
  }



  getMilestoneInfo() {
    //var moment = require('moment-timezone');
    console.log(this.form.get("mileStoneInformation.vendorCancellationDate").value)
    //console.log(moment(this.form.get("mileStoneInformation.submitDate").value).tz("America/Chicago").format())

    return {
      ordrId: this.orderId,
      submitDate: this.form.get("mileStoneInformation.submitDate").value,
      customerCommitDate: this.form.get("mileStoneInformation.customerCommitDate").value,
      customerWantDate: this.form.get("mileStoneInformation.customerWantDate").value,
      validatedDate: this.form.get("mileStoneInformation.validatedDate").value,
      orderSentToVendorDate: this.helper.dateObjectToString(this.form.get("mileStoneInformation.orderSentToVendorDate").value),
      orderAcknowledgedDate: this.helper.dateObjectToString(this.form.get("mileStoneInformation.orderAcknowledgedDate").value),
      targetDeliveryDate: this.form.get("mileStoneInformation.targetDeliveryDate").value,
      targetDeliveryDateReceived: this.form.get("mileStoneInformation.targetDeliveryDateReceived").value,
      accessDeliveryDate: this.form.get("mileStoneInformation.accessDeliveryDate").value,
      vendorContractTerm: this.helper.ifEmpty(this.form.get("mileStoneInformation.vendorContractTerm").value, null),
      vendorContractStartDate: this.form.get("mileStoneInformation.vendorContractStartDate").value,
      accessAcceptDate: this.form.get("mileStoneInformation.accessAcceptDate").value,
      billClearInstallDate: this.form.get("mileStoneInformation.billClearInstallDate").value,
      //customerContractTerm: this.form.get("mileStoneInformation.customerContractTerm").setValue(this.form.get("install.transport.serviceTerm").value),
      //customerContractTerm: this.form.get("this.m5DetailsComponent.install.transport.serviceTerm").value,
      customerContractStartDate: this.form.get("mileStoneInformation.customerContractStartDate").value,
      vendorCancellationDate: this.form.get("mileStoneInformation.vendorCancellationDate").value,
      vendorConfirmedDisconnectDate: this.form.get("mileStoneInformation.vendorConfirmedDisconnectDate").value,
      disconnectCompletedDate: this.form.get("mileStoneInformation.disconnectCompletedDate").value,
      closedDate: this.form.get("mileStoneInformation.closedDate").value,
      bypassVendorOrderCode: this.form.get("vendorOrderDetails.bypassVendorOrderCode").value,
      isBillClearInstallDateChanged: this.isBillCleared
    }
  }
  // Order Milestones Section

  // Initiate ASR Section
  onAsrTypeChanged(e) {

    

    this.selectedAccessMbList = this.accessMbList.filter(a => a.asrTypeId == e.value)

    const installBandwidth = this.form.get("install.transport.accessBandwidth").value;
    const selectedBandwidth = this.selectedAccessMbList.filter(x => x.accsMbDes.includes(installBandwidth));
    let bandwidth = "";
    if(selectedBandwidth.length > 0) {
      bandwidth = selectedBandwidth[0]["accsMbDes"];
    }

    let accessCityName = this.form.get("mileStoneInformation.accessCityName").value;
    let accessCity = this.form.get("mileStoneInformation.accessCityName").value != null ? this.accessCityNameList.find(a => a.accsCtySiteId == accessCityName) : ""
    let accsCtyNmeSiteCd = accessCity != undefined ? accessCity['accsCtyNmeSiteCd'] : "";

    if (e.value == 1) {

      this.form.get("asr.aggregated.accessCityNameSiteCode").setValue(accsCtyNmeSiteCd)
      this.form.get("asr.aggregated.ipNode").setValue(accsCtyNmeSiteCd)
      this.form.get("asr.aggregated.accessBandwidth").setValue(bandwidth)
      this.form.get("asr.aggregated.partnerCarrierCode").setValue(this.form.get("install.transport.carrierAccessCode").value)
    } else if (e.value == 2) {

      this.form.get("asr.dedicated.ipNode").setValue(accsCtyNmeSiteCd)
      this.form.get("asr.dedicated.accessBandwidth").setValue(bandwidth)
      this.form.get("asr.dedicated.partnerCarrierCode").setValue(this.form.get("install.transport.carrierAccessCode").value)
    } else if (e.value == 3) {

      this.form.get("asr.tdm.ipNode").setValue(accsCtyNmeSiteCd)
      this.form.get("asr.tdm.accessBandwidth").setValue(bandwidth)
      this.form.get("asr.tdm.partnerCarrierCode").setValue(this.form.get("install.transport.carrierAccessCode").value)
    } else if (e.value == 4) {

      this.form.get("asr.ipl.ipNode").setValue(accsCtyNmeSiteCd)
      this.form.get("asr.ipl.accessBandwidth").setValue(bandwidth)
    }


  }

  asrSubmit() {
    if ((this.asrType.value == 1 || this.asrType.value == 2) && this.form.get("asr.origTerm").value == null) {
      // error
      this.helper.notify("Orig/Term is required for this ASR type.", Global.NOTIFY_TYPE_WARNING)
    } else {
      if (this.asrType.value != null) {
        //console.log(this.form.get("asr.origTerm").value)
        //console.log(this.asrTypeList)
        //console.log(this.asrType.value)

        let asrTypeDesc = this.asrTypeList.find(a => a.asrTypeId == this.asrType.value).asrTypeDes

        let asrNotes = (this.form.get("asr.notes").value || "").replace(/\r?\n/g, "<br/>")
        let html = `ASR Details: </br>ASR Types: ${asrTypeDesc}</br>ASR Notes: ${asrNotes}</br>`
        let obj = new Asr({
          ordrId: this.orderId,
          asrTypeId: this.asrType.value,
          intlDomEmailCd: this.origTermList.findIndex(a => a == this.form.get("asr.origTerm").value).toString(),
          asrNotesTxt: this.helper.removeUnknownCharacter(asrNotes)
        })

        if (this.asrType.value == 1) {
          obj["accsCtyNmeSiteCd"] = this.form.get("asr.aggregated.accessCityNameSiteCode").value
          if (obj["accsCtyNmeSiteCd"] != null) {
            html = `${html}Access City Name & Site Code: ${obj["accsCtyNmeSiteCd"]}</br>`
          }
          obj.ipNodeTxt = this.form.get("asr.aggregated.ipNode").value
          if (obj.ipNodeTxt != null) {
            html = `${html}IP Node (TOC/Router location &amp; Site Code): ${obj.ipNodeTxt}</br>`
          }
          obj.accsBdwdDes = this.form.get("asr.aggregated.accessBandwidth").value
          if (obj.accsBdwdDes != null) {
            html = `${html}Access Bandwidth (not port or service speed): ${obj.accsBdwdDes}</br>`
          }
          obj.trnspntCd = (this.form.get("asr.aggregated.transparent").value || "No") == "Yes" ? true : false
          if (obj.trnspntCd != null) {
            html = `${html}Transparent: ${obj.trnspntCd}</br>`
          }
          obj.ptnrCxrCd = this.form.get("asr.aggregated.partnerCarrierCode").value
          if (obj.ptnrCxrCd != null) {
            html = `${html}Partner and Carrier Code (access vendor): ${obj.ptnrCxrCd}</br>`
          }
          obj.lecNniNbr = this.form.get("asr.aggregated.lecNni").value
          if (obj.lecNniNbr != null) {
            html = `${html}LEC NNI (9 digit numeric value): ${obj.lecNniNbr}</br>`
          }
        } else if (this.asrType.value == 2) {
          obj.ipNodeTxt = this.form.get("asr.dedicated.ipNode").value
          if (obj.ipNodeTxt != null) {
            html = `${html}IP Node (TOC/Router location &amp; Site Code): ${obj.ipNodeTxt}</br>`
          }
          obj.accsBdwdDes = this.form.get("asr.dedicated.accessBandwidth").value
          if (obj.accsBdwdDes != null) {
            html = `${html}Access Bandwidth (not port or service speed): ${obj.accsBdwdDes}</br>`
          }
          obj.ptnrCxrCd = this.form.get("asr.dedicated.partnerCarrierCode").value
          if (obj.ptnrCxrCd != null) {
            html = `${html}Partner and Carrier Code (access vendor): ${obj.ptnrCxrCd}</br>`
          }
        } else if (this.asrType.value == 3) {
          obj.ipNodeTxt = this.form.get("asr.tdm.ipNode").value
          if (obj.ipNodeTxt != null) {
            html = `${html}IP Node (TOC/Router location &amp; Site Code): ${obj.ipNodeTxt}</br>`
          }
          obj.accsBdwdDes = this.form.get("asr.tdm.accessBandwidth").value
          if (obj.accsBdwdDes != null) {
            html = `${html}Access Bandwidth (not port or service speed): ${obj.accsBdwdDes}</br>`
          }
          obj.ptnrCxrCd = this.form.get("asr.tdm.partnerCarrierCode").value
          if (obj.ptnrCxrCd != null) {
            html = `${html}Partner and Carrier Code (access vendor): ${obj.ptnrCxrCd}</br>`
          }
          obj.entrncAsmtTxt = this.form.get("asr.tdm.entranceAssignment").value
          if (obj.entrncAsmtTxt != null) {
            html = `${html}Entrance Assignment: ${obj.entrncAsmtTxt}</br>`
          }
        } else if (this.asrType.value == 4) {
          obj.ipNodeTxt = this.form.get("asr.ipl.ipNode").value
          if (obj.ipNodeTxt != null) {
            html = `${html}IP Node (TOC/Router location &amp; Site Code): ${obj.ipNodeTxt}</br>`
          }
          obj.accsBdwdDes = this.form.get("asr.ipl.accessBandwidth").value
          if (obj.accsBdwdDes != null) {
            html = `${html}Access Bandwidth (not port or service speed): ${obj.accsBdwdDes}</br>`
          }
        } else if (this.asrType.value == 5) {
          obj.mstrFtnCd = this.form.get("asr.offnet.masterVas").value
          if (obj.mstrFtnCd != null) {
            html = `${html}What is the Master VAS M5 #: ${obj.mstrFtnCd}</br>`
          }
          obj.h1MatchMstrVasCd = (this.form.get("asr.offnet.isH1Matched").value || "No") == "Yes" ? true : false
          if (obj.h1MatchMstrVasCd != null) {
            html = `${html}Does the H1 match the Master VAS: ${obj.h1MatchMstrVasCd}</br>`
          }
          obj.dlciDes = this.form.get("asr.offnet.dlci").value
          if (obj.dlciDes != null) {
            html = `${html}DLCI: ${obj.dlciDes}</br>`
          }
        }

        obj.notes = html

        this.spinner.show()
        this.asrTypeService.saveAsr(obj).subscribe(res => {
          let orderNote = res
          this.helper.notify("ASR Template successfully created", Global.NOTIFY_TYPE_SUCCESS)
          //this.getInitialData()

          //if (orderNote) {
          //  orderNote.nTeTxt = this.sanitizer.bypassSecurityTrustHtml(orderNote.nTeTxt)
          //  this.noteList = [orderNote, ...this.noteList]
          //}
          //if (callback != null) {
          //  callback()
          //}

          orderNote = orderNote.map(a => {
            a.nteTxt = this.sanitizer.bypassSecurityTrustHtml(a.nteTxt)

            return a
          })

          this.noteList = orderNote.reverse(a => a.creatDt)
          this.spinner.hide()
        }, error => {
          console.log(error)
          this.helper.notify("ASR Template could not be created", Global.NOTIFY_TYPE_ERROR)
          this.spinner.hide()
        })
      }
    }
  }
  // Initiate ASR Section

  // STDI Section
  getStdiInfo() {
    return {
      ordrId: this.orderId,
      stdiDt: this.helper.dateObjectToString(this.form.get("stdi.standardInterval").value),
      stdiReasId: this.form.get("stdi.reasonDescription").value,
      cmntTxt: this.form.get("stdi.comments").value
    }
  }

  saveStdi(e) {
    let stdiInfo = this.getStdiInfo()

    if (stdiInfo.cmntTxt != null && stdiInfo.stdiDt != null) {
      let data = this.orderStdiHistoryService.create(stdiInfo).pipe(
        concatMap(res => this.orderStdiHistoryService.getByOrderId(this.orderId))
      )

      this.spinner.show()
      data.subscribe(res => {
        this.stdiReasonList = res
      }, error => this.spinner.hide(),
        () => this.spinner.hide())
    } else {
      // Notify error message
      if (e != null) {
        this.helper.notify(`Please fill up the required fields for STDI`, Global.NOTIFY_TYPE_ERROR)
      }
    }
  }
  // STDI Section

  // M5/CTN/Order Notes Section
  getOrderNotes() {
    return {
      ordrId: this.orderId,
      nteTypeId: 11, // To be changed to actual value
      nteTxt: this.form.get("notes.description").value
    }
  }

  saveOrderNotes(e) {
    let orderNotes = this.getOrderNotes()

    if (orderNotes.nteTxt != null) {
      let data = this.orderNoteService.create(orderNotes).pipe(
        concatMap(res => this.orderNoteService.getByOrderId(this.orderId))
      )

      this.spinner.show()
      data.subscribe(res => {
        this.form.get("notes.description").setValue(null)
        this.noteList = res
      },
        error => this.spinner.hide(),
        //() => this.spinner.hide()
      )
    } else {
      // Notify error message
      if (e != null) {
        this.helper.notify(`Please fill up the required fields for Order Notes`, Global.NOTIFY_TYPE_ERROR)
      }
    }
  }
  // M5/CTN/Order Notes Section

  // Circuit Info
  getCircuitInfo() {
    return {
      vendorOrderId: this.vendorOrder ? this.vendorOrder.vndrOrdrId : 0,
      vendorCircuitId: this.form.get("vendorCircuitInformation.vendorCircuitId").value,
      lecId: this.form.get("vendorCircuitInformation.lecId").value,
      vlanId: this.form.get("vendorCircuitInformation.vlanId").value || "",
      noOfIpAddresses: this.form.get("vendorCircuitInformation.noOfIpAddresses").value,
      routingProtocol: this.form.get("vendorCircuitInformation.routingProtocol").value,
      customerIpAddresses: this.form.get("vendorCircuitInformation.customerIpAddresses").value,
      specialCustomerAccessDetails: this.form.get("vendorCircuitInformation.specialCustomerAccessDetails").value,
      rltdVendorOrderId: this.form.get("vendorCircuitInformation.vendorOrderId").value,
      xCnnctPrvdr: this.form.get("vendorCircuitInformation.xCnnctPrvdr").value,
      tDwnAssn: this.form.get("vendorCircuitInformation.tDwnAssn").value,
      tDwnReq: this.form.get("vendorCircuitInformation.tDwnReq").value,
    }
  }
  // Circuit Info

  // Collapse Button
  onCollapseClicked() {
    this.collapse()
    this.m5DetailsComponent.collapse()
    this.nrmComponent.collapse()
    this.additionalCostComponent.collapse()
  }

  collapse() {
    this.viewPanels
      //.filter(a => a.expanded)
      .forEach(a => {
        if (this.toCollapse) {
          a.close()
        } else {
          a.open()
        }
      })
    this.toCollapse = !this.toCollapse
  }
  // Collapse Button



  ///////////////////
  orderCompleted() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, true, 0).subscribe(
      data => {
        if (data == 0) {
          if (this.workGroup == 2)
            this.router.navigate(['order/amnci/']);
          else if (this.workGroup == 16)
            this.router.navigate(['order/anci/']);
          else if (this.workGroup == 100)
            this.router.navigate(['order/enci/']);
        }
      });
  }

  setValidations() {
    this.setValidators(this.form, "mileStoneInformation.accessCityName", [Validators.required])
    if (this.showFSA && !this.showCPE) {
      if (this.form.get("asr.asrType").value == 1) {
        this.setValidators(this.form, "asr.aggregated.lecNni", [Validators.pattern(/^\d{9}$/)])
      } else if (this.form.get("asr.asrType").value == 5) {
        this.setValidators(this.form, "asr.offnet.masterVas", [Validators.pattern(/^\d+$/)])
      }
    } else {
      this.setValidators(this.form, "asr.aggregated.lecNni", [])
      this.setValidators(this.form, "asr.offnet.masterVas", [])
    }
  }

  setValidators(form: FormGroup, controlName: string, validators: ValidatorFn[]) {
    form.get(controlName).setValidators(validators)
    form.get(controlName).updateValueAndValidity()
  }

  LockPageControls() {
    this.form.disable();
    this.locked = true;
  }

  UnlockPageControls() {
    this.form.enable();
    this.form.get("mileStoneInformation.region").disable()
    this.form.get("mileStoneInformation.submitDate").disable()
    this.form.get("mileStoneInformation.customerCommitDate").disable()
    this.form.get("mileStoneInformation.customerWantDate").disable()
    if(!this.isAbleToManageVndrOrdrMs) {
      this.form.get("mileStoneInformation.orderSentToVendorDate").disable()
      this.form.get("mileStoneInformation.orderAcknowledgedDate").disable()
    }
    //this.form.get("mileStoneInformation.targetDeliveryDateReceived").disable()
    this.form.get("mileStoneInformation.closedDate").disable()
    //this.form.get("vendorCircuitInformation.vendorOrderId").disable()
    this.form.get("nccoOrderDetails").disable();
    this.form.get("nccoOrderDetails.emailNotification").enable();
    this.form.get("nccoOrderDetails.comment").enable();

    console.log("B")
    this.enableDisableMilestone()
  }

  CheckIn(refeshPage: boolean = false) {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, false, 0).subscribe(
      data => {
        if (data == 1) {
          this.lblError = "This order is currently locked. Try again later.";
          this.LockPageControls();
          //this.hfLockedOrderID = this.orderId.toString();
        }

        if(refeshPage) {
          this.spinner.hide();
          window.location.reload();
        }
      });
  }

  getFsaInfo() {
    return {
      ordrId: this.orderId,
      //accessMegabyteValue: this.form.get("general.accessMegabyteValue").value,
      accessCityName: this.form.get("mileStoneInformation.accessCityName").value,
      custCmmtDt: this.form.get("mileStoneInformation.customerCommitDate").value,
      custWantDt: this.form.get("mileStoneInformation.customerWantDate").value,
      fsaOrdrCpeLineItem: this.fsaOrderCpeLineItemList,
      nrmupdate : this.form.get("mileStoneInformation.nrmupdate").value
    }
  }

  validateFormData(orderFormInfo) {
    this.errors = [];
    this.option.validationSummaryOptions.title = "Validation Summary";
    this.option.validationSummaryOptions.message = null;

    if (!this.isDisconnect && !this.showCPE) {
      if (this.form.get("mileStoneInformation.accessCityName").value === 0) {
        this.errors.push("Access City Name is required");
      }
    }

    if (!this.showCPE) {
      let milestone = orderFormInfo.milestoneInfo
      let vd = this.helper.ifEmpty(milestone.validatedDate, "")
      let osd = this.helper.isReallyEmpty(milestone.orderSentToVendorDate)
      let oad = this.helper.isReallyEmpty(milestone.orderAcknowledgedDate)
      let tdd = this.helper.ifEmpty(milestone.targetDeliveryDate, "")
      let tddr = this.helper.ifEmpty(milestone.targetDeliveryDateReceived, "")
      let vdd = this.helper.ifEmpty(milestone.vendorConfirmedDisconnectDate, "")
      let bypass = this.helper.ifEmpty(milestone.bypassVendorOrderCode, false)

      if (tdd != "" || tddr != "" || vdd != "") {
        if (!this.vendorOrder || (osd && bypass == false)) {
          //console.log(11)
          this.errors.push("Please attach Vendor Order and complete Vendor Email Milestones (Vendor Sent/Acknowledgement date)");
        } else {
          console.log(oad)
          console.log(bypass)
          if (oad && bypass == false) {
            //console.log(111)
            this.errors.push("Please complete Vendor Acknowledgement Milestone");
          } else {
            if (vd == "") {
              //console.log(1111)
              this.errors.push("Please complete Validated Milestone");
            }
            if (tddr != "" && tdd == "") {
              //console.log(11111)
              this.errors.push("Please complete Target Delivery Milestone");
            }
          }
        }
      }

      let vendorCircuitId = this.helper.isReallyEmpty(orderFormInfo.circuitInfo.vendorCircuitId)
      let lecId = this.helper.isReallyEmpty(orderFormInfo.circuitInfo.lecId)
      let add = this.helper.ifEmpty(milestone.accessDeliveryDate, "")
      if ((vendorCircuitId || lecId) && add != "") {
        this.errors.push(`Please populate "Partner/Vendor Circuit ID" & "LEC ID" first before saving Access Delivery Date`);
      }
    }

    this.errors = Array.from(new Set(this.errors))
    return this.errors.length == 0 ? true : false
  }

  isOrderReviewer() {
    return (this.workGroup == 2 || this.workGroup == 16 || this.workGroup == 100) && (!(this.isUpdater || this.isRTS) && this.isReviewer)
    //return (this.workGroup == 2 && this.loggedInUserProfilesList.find(a => !["AMNCI Order RTS", "AMNCI Order Updater"].includes(a.usrPrfDes) && ["AMNCI Order Reviewer"].includes(a.usrPrfDes))) ||
    //  (this.workGroup == 16 && this.loggedInUserProfilesList.find(a => !["ANCI Order RTS", "ANCI Order Updater"].includes(a.usrPrfDes) && ["ANCI Order Reviewer"].includes(a.usrPrfDes))) ||
    //  (this.workGroup == 100 && this.loggedInUserProfilesList.find(a => !["ENCI Order RTS", "ENCI Order Updater"].includes(a.usrPrfDes) && ["ENCI Order Reviewer"].includes(a.usrPrfDes)))
  }

  enableDisableMilestone() {
    console.log(this.orderTypeDesc)
    console.log(this.parentOrderType)
    console.log(this.productTypeCd)
    console.log(this.m5DetailList)
    console.log(this.form.get("mileStoneInformation.status").value)
    let tasks = this.m5DetailList.map(a => { return { task: a.acT_TASK } })
    console.log(tasks)
    if (["INSTALL", "UPGRADE", "DOWNGRADE", "MOVE"].includes(this.orderTypeDesc.toUpperCase()) && !this.helper.isEmpty(this.form.get("mileStoneInformation.billClearInstallDate").value)) {
      //console.log(4444)
      if (this.orderStatusId != null && ![0, 1].includes(this.orderStatusId)) {
        this.form.get("mileStoneInformation.closedDate").enable()
      } else {
        this.closedDateError = "Closed Date is disabled since order is not completed (Bill Activated)"
      }
    } else if (this.orderTypeDesc.toUpperCase() == "CANCEL" && !this.helper.isEmpty(this.form.get("mileStoneInformation.vendorCancellationDate").value)) {
      console.log(5555)
      let enable = (this.parentOrderType || "").toUpperCase() == "BILLING ONLY" || this.productTypeCd.toUpperCase().includes("OFFNET") || this.orderCatId == 6 || this.isGomCancelTaskCompleted
      if (enable) {
        this.form.get("mileStoneInformation.closedDate").enable()
      } else {
        this.closedDateError = "Closed Date is disabled since order is not completed (Bill Activated)"
      }
      //if (enable || tasks)
    } else if (this.orderTypeDesc.toUpperCase() == "DISCONNECT" && (this.form.get("mileStoneInformation.status").value as string || "").includes("xNCI Close Date")) {
      //console.log(1111)
      if (this.orderStatusId != null && ![0, 1].includes(this.orderStatusId)) {
        //console.log(2222)
        this.form.get("mileStoneInformation.closedDate").enable()
      } else {
        //console.log(3333)
        this.closedDateError = "Closed Date is disabled since order is not completed (Bill Activated)"
      }
    } else if (this.orderTypeDesc.toUpperCase() == "CHANGE" && (this.orderCatId == 2 || this.orderCatId == 6)) {
      //console.log(1111)
      if (this.orderStatusId != null && ![0, 1].includes(this.orderStatusId)) {
        //console.log(2222)
        this.form.get("mileStoneInformation.closedDate").enable()
      } else {
        //console.log(3333)
        this.closedDateError = "Closed Date is disabled since order is not completed (Bill Activated)"
      }
    } else if (this.orderTypeDesc.toUpperCase() == "BILLING ONLY"
      && ((this.form.get("mileStoneInformation.status").value as string || "").includes("xNCI Close Date") || (this.form.get("mileStoneInformation.status").value as string || "").includes("xNCI Confirmed Renewal Date"))) {
      //console.log(1111)
      if (this.orderStatusId != null && ![0, 1].includes(this.orderStatusId)) {
        //console.log(2222)
        this.form.get("mileStoneInformation.closedDate").enable()
      } else {
        //console.log(3333)
        this.closedDateError = "Closed Date is disabled since order is not completed (Bill Activated)"
      }
    } else {

      console.log("C")
    }
  }

  setAccessCityName(event: any) {
    // this.form.get("mileStoneInformation.accessCityName").setValue(event.value);
  }
  public changeCpeOrderStatus(event: any) {
      this.selectedCpeOrderStatus = event.value;
   // this.selectedCpeOrderStatus = event.selectedItem.id;
  }
  public onClosedDateChanged(e) {
    if (e.value != null && !this.isOnLoad) {
      //console.log(this.isOnLoad)
      alert("Warning, before populating this box please make sure that you have completed all edits to this order record because it will be closed for edits.")
      this.isOnLoad=true;
    }
  }
}
