import { Component, OnInit, ViewChild } from '@angular/core';
import { WorkGroup, Global } from "./../../../shared/global";
import { Helper } from '../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { zip } from 'rxjs';
import { WorkGroupService, OrderLockService, UserProfileService, UserService, OrderNoteService, FsaOrdersService, AmnciService, CCDBypassService, FsaOrderCpeLineItemService } from '../../../services';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DCPEService } from '../../../services/dcpe.service';
import { String } from 'typescript-string-operations';
import { concatMap } from 'rxjs/operators';
import { forEachChild, getPreEmitDiagnostics } from 'typescript';
import { MaterialRequisitionComponent } from '../custom/material-requisition/material-requisition.component';
import { EquipmentReviewComponent } from '../custom/equipment-review/equipment-review.component';
import { EquipmentReceiptComponent } from '../custom/equipment-receipt/equipment-receipt.component';
import { TechAssignmentComponent } from '../custom/tech-assignment/tech-assignment.component';
import { OrderCompletionComponent } from '../custom/order-completion/order-completion.component';
import { GomFieldsComponent } from '../custom/gom-fields/gom-fields.component';

@Component({
  selector: 'app-gom-form',
  templateUrl: './gom-form.component.html'
})
export class GomFormComponent implements OnInit {
  @ViewChild(EquipmentReviewComponent, { static: false })
  private equipReviewDetails: EquipmentReviewComponent;

  @ViewChild(MaterialRequisitionComponent, { static: false })
  private matReqDetails: MaterialRequisitionComponent;

  @ViewChild(GomFieldsComponent, { static: false })
  private gomFieldDetails: GomFieldsComponent;

  form: FormGroup
  fsaDetails = {
    id: null,
    isShown: false,
    show: (id) => {
      this.fsaDetails.id = id
      this.fsaDetails.isShown = true
    },
    hide: () => {
      this.fsaDetails.isShown = false
    }
  }
  m5DetailList = [];
  option: any
  notes = '';
  locked: boolean = false;
  userId: number;
  userADID: string;
  workGroup: number;
  orderId: number;
  taskId: number;

  hfOrderComplete: boolean = false;
  hfLockedOrderID: string = "";
  hfSelectedOrderID: string = "";
  hfSelectedDeviceOrder: string;
  hfSelectedRow: string = "";
  hfSelectedFTN: string = "";
  hfTaskID: string;
  bAdminEdit: boolean = false;
  lblError: string;
  taskID: number = 0;

  loggedInUserProfilesList: any;
  orderInfo_V5U: any;
  ftnList: any;
  tasksList: any;
  jeopardyCodeList: any;
  cpeAcctCntctsList: any;
  reAssignedTechsList: any;
  techList: any;
  cpeLineItemsList: any;
  equipOnlyInfoList: any;
  requisitionNumberList: any;
  reqHeaderQueueList: any
  reqHeaderAcctCodesList: any;
  cpepList: any;
  cpeReqLineItems: any;

  reAssignedTech: string;
  reDispatchTime: string;
  reEventId: string = "";
  selectedReAssignedTech: string = "";

  ftn: string;
  servingCPEClli: string;
  assignedOrderToADID: string;
  servingCPEClliValue: string;
  assignedOrderToADIDValue: string;
  deviceId: string;
  showNID: boolean = false;
  eqptOnlyCD: string;
  //orderType: string;
  domesticCD: string
  thirdPartySite: string;
  emailPrimaryIPMText: string;
  primaryEmail: string;
  emailPrimaryISText: string;
  secondaryEmail: string;
    
  selectedJeopardyCode: string = "";
  chkOrderModificationList: string[];
  selectedOrderModification: string = "";
  selectedReworkToTask: string;

  isEquipmentReviewDataLoaded: boolean = false;
  isMatReqDataLoaded: boolean = false;
  isEquipmentReceiptDataLoaded: boolean = false;
  isTechAssignmentDataLoaded: boolean = false;
  isOrderCompletionDataLoaded: boolean = false;
  isOrderModificationLoaded: boolean = false;
  isRTSLoaded: boolean = false;
  isReAssignLoaded: boolean = false; 
  isOrderCompleted: boolean = true;
  displaySaveCommentsButton: boolean = false;
  displayOrderCompletionButtons: boolean = false;
  selectedItems: any[] = [];
  updatedItems: any[] = [];
  //matReqData = new PsReqLineItemQueue();
  noteList: [];
  ccdList: [];
  noteInfo: string = "";

  //GOM
  lblMessage: string = "";
  isSSNLoaded: boolean = false;
  btnMoveText: string = "";
  //enablebtnSave: boolean = false;
  //enablebtnMove: boolean = false;
  //enablebtnReview: boolean = false;
  lblFTNMsg: string = "";
  isReviewDataLoaded: boolean = false;
  displaybtnReview: boolean = false;
  displaybtnRC: boolean = false;
  displaybtnMove: boolean = false;
  lblMsg: string = "";
  isGOMFieldsLoaded: boolean = false;
  displaybtnSave: boolean = false;
  displayPL: boolean = false;
  isNRMLoaded: boolean = false;
  installPortList = []
  installVlanList = []
  fsaOrderCpeLineItemList = []

  orderTypeDesc = "";
  orderType: string = "GOM";
  m5 : string = "";
  m5Data: any;

  rawNidSerialNumber: string;

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute, private workGroupService: WorkGroupService,
    private orderLockService: OrderLockService, private dcpeService: DCPEService, private userProfileService: UserProfileService,
    private userService: UserService, private router: Router, private orderNoteService: OrderNoteService, private fsaOrdersService: FsaOrdersService,
    private amnciService: AmnciService, private ccdService: CCDBypassService, private fsaOrderCpeLineItemService: FsaOrderCpeLineItemService) {
    this.bindChkOrderModificationList();
  }

  ngOnInit() {

    this.form = new FormGroup({
      selectedM5: new FormControl(),
      jeopardyCode: new FormControl(),
      emailPrimaryIPM: new FormControl(false),
      emailPrimaryIS: new FormControl(false),
      servingCPEClli: new FormControl(),
      assignedOrderToADID: new FormControl(),
      tasks: new FormControl(),
      notes: new FormControl(''),
      privateLine: new FormControl(''),
      billingFtn: new FormControl(''),
      //chkAddJPRDYWithHold: new FormControl(),
      //chkAddJPRDYNoHold: new FormControl(),
      //chkRemoveJPRDYHold: new FormControl(),
      //chkCLLIModify: new FormControl(),
      //chkAssignUser: new FormControl(),
      //chkMoveOrderBack: new FormControl(),
      //chkInsertNote: new FormControl(),
      display: new FormGroup({
        m5: new FormControl({ value: null, disabled: true }),
        customerName: new FormControl({ value: null, disabled: true }),
        orderType: new FormControl({ value: null, disabled: true }),
        orderSubType: new FormControl({ value: null, disabled: true }),
        productType: new FormControl({ value: null, disabled: true }),
        orderStatus: new FormControl({ value: null, disabled: true }),
        pendingTasks: new FormControl({ value: null, disabled: true }),
        customerNameH1: new FormControl({ value: null, disabled: true }),
        h6AddressLine1: new FormControl({ value: null, disabled: true }),
        h6City: new FormControl({ value: null, disabled: true }),
        h6Country: new FormControl({ value: null, disabled: true }),
        vendorName: new FormControl({ value: null, disabled: true }),
        accessBandwidth: new FormControl({ value: null, disabled: true }),
      }),
      gomFileds: new FormGroup({
        cpeOrderStatus: new FormControl(1),
        cpeCompletionDate: new FormControl(),
        reqAmount: new FormControl(),
        cpeEquipType: new FormControl(),
        nidSerialNumber: new FormControl(),
        nidIp: new FormControl(),
      }),
      nrm: new FormGroup({
      }),
      equipmentReview: new FormGroup({
        standardMatlReq: new FormControl(false),
        noMatlReq: new FormControl(false),
        orderType: new FormControl(),
        deviceFtn: new FormControl()
      }),
      materialRequisition: new FormGroup({
        rasDate: new FormControl(),
        elid: new FormControl(),
        domesticShiptoClli: new FormControl(),
        internationalShiptoClli: new FormControl(),
        chkInstallAddr: new FormControl(false),
        chkShipAddr: new FormControl(false),
        referenceNumber: new FormControl(),
        rfqIndicator: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        deliveryName: new FormControl(),
        deliveryAddr1: new FormControl(),
        deliveryCity: new FormControl(),
        deliveryCountry: new FormControl(),
        deliveryPhone: new FormControl(),
        deliveryAddr2: new FormControl(),
        deliveryAddr3: new FormControl(),
        deliveryState: new FormControl(),
        deliveryBldg: new FormControl(),
        deliveryZip: new FormControl(),
        deliveryFlr: new FormControl(),
        deliveryProvince: new FormControl(),
        deliveryRm: new FormControl(),
        shippingInstructions: new FormControl(''),
        businessUnitGL: new FormControl(),
        businessUnitPC: new FormControl(),
        region: new FormControl(),
        costCenter: new FormControl(),
        hdnMrkPkg: new FormControl(),
        edit: new FormGroup({
          unitOfMeasure: new FormControl(),
          activity: new FormControl(),
          unitOfPrice: new FormControl(),
          sourceType: new FormControl(),
          suggestedVendor: new FormControl(),
          resourceCategory: new FormControl(),
          installCode: new FormControl(),
          resourceSubcategory: new FormControl(),
          projectId: new FormControl(),
          contractId: new FormControl(),
          account: new FormControl(),
          contractLineNumber: new FormControl(),
          product: new FormControl(),
          auxiliaryId: new FormControl(),
          lineItemNotes: new FormControl(),
        })
      }),
      reAssignedTech: new FormControl(),
      reDispatchTime: new FormControl(),
      reEventId: new FormControl(''),
      install: new FormGroup({
        mdsInstallationInformation: new FormGroup({
          solutionService: new FormControl(),
          networkTypeCode: new FormControl(),
          serviceTierCode: new FormControl(),
          transportOrderTypeCode: new FormControl(),
          designDocumentNo: new FormControl(),
          vendorCode: new FormControl()
        }),
        transport: new FormGroup({
          accessTypeCode: new FormControl(),
          accessArrangementCode: new FormControl(),
          encapsulationCode: new FormControl(),
          fiberHandoffCode: new FormControl(),
          carrierAccessCode: new FormControl(),
          circuitId: new FormControl(),
          serviceTerm: new FormControl(),
          quoteExpirationDate: new FormControl(),
          accessBandwidth: new FormControl(),
          privateLineNo: new FormControl(),
          m5AccessStatus: new FormControl(),
          classOfService: new FormControl()
        }),
        changeInformation: new FormGroup({
          changeDescription: new FormControl(),
          carrierServiceId: new FormControl(),
          carrierCircuitId: new FormControl(),
          originalInstallOrderId: new FormControl(),
          networkUserAddress: new FormControl(),
          portRateTypeCode: new FormControl()
        }),
        cpeInstallationInformation: new FormGroup({
          cpeOrderTypeCode: new FormControl(),
          equipmentOnlyFlagCode: new FormControl(),
          accessProvideCode: new FormControl(),
          phoneNumberType: new FormControl(),
          phoneNumber: new FormControl(),
          eccktIdentifier: new FormControl(),
          managedServicesChannelProgramCode: new FormControl(),
          deliveryDuties: new FormControl(),
          deliveryDutyAmount: new FormControl(),
          shippingChargeAmount: new FormControl(),
          recordOnly: new FormControl(),
          nuaOrCkt: new FormControl()
        })
      }),
      customer: new FormGroup({
        h1: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          branchCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl()
        }),
        h4: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl()
        }),
        h4Address: new FormGroup({
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl()
        }),
        h4Contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        }),
        h6: new FormGroup({
          customerId: new FormControl(),
          customerName: new FormControl(),
          currentBillingCycleCode: new FormControl(),
          futureBillingCycleCode: new FormControl(),
          branchCode: new FormControl(),
          taxExemption: new FormControl(),
          charsId: new FormControl(),
          siteId: new FormControl(),
          serviceSubType: new FormControl(),
          salesOfficeIdCode: new FormControl(),
          salesPersonPrimaryCid: new FormControl(),
          salesPersonSecondaryCid: new FormControl(),
          cllidCode: new FormControl(),
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          cityCode: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl(),
          buildingName: new FormControl(),
          floor: new FormControl(),
          roomNumber: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        }),
        h6Address: new FormGroup({
          addressType: new FormControl(),
          addressLine1: new FormControl(),
          addressLine2: new FormControl(),
          addressLine3: new FormControl(),
          suite: new FormControl(),
          city: new FormControl(),
          stateCode: new FormControl(),
          zipCode: new FormControl(),
          countryCode: new FormControl(),
          provinceMunicipality: new FormControl(),
          buildingName: new FormControl(),
          floor: new FormControl(),
          roomNumber: new FormControl()
        }),
        h6Contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        })
      }),
      order: new FormGroup({
        orderAction: new FormControl(),
        ftn: new FormControl(),
        orderTypeCode: new FormControl(),
        orderSubTypeCode: new FormControl(),
        productTypeCode: new FormControl(),
        parentFtn: new FormControl(),
        relatedFtn: new FormControl(),
        telecomServicePriority: new FormControl(),
        carrierPartnerWholesaleType: new FormControl(),
        customerCommitDate: new FormControl(),
        customerWantDate: new FormControl(),
        customerOrderSubmitDate: new FormControl(),
        customerSignedDate: new FormControl(),
        //orderSubmitDate: new FormControl(),
        customerPremiseCurrentlyOccupiedFlag: new FormControl(),
        willCustomerAcceptServiceEarlyFlag: new FormControl(),
        multipleOrderFlag: new FormControl(),
        //multipleOrderIndex: new FormControl(),
        //multipleOrderTotal: new FormControl(),
        escalatedFlag: new FormControl(),
        expediteTypeCode: new FormControl(),
        govtTypeCode: new FormControl(),
        //vendorVpnCode: new FormControl(),
        prequalNo: new FormControl(),
        scaNumber: new FormControl()
      }),
      disconnect: new FormGroup({
        reasonCode: new FormControl(),
        cancelBeforeStartReasonCode: new FormControl(),
        cancelBeforeStartReasonText: new FormControl(),
        contact: new FormGroup({
          type: new FormControl(),
          contactType: new FormControl(),
          firstName: new FormControl(),
          lastName: new FormControl(),
          name: new FormControl(),
          emailAddress: new FormControl(),
          countryCode: new FormControl(),
          cityCode: new FormControl(),
          npa: new FormControl(),
          nxx: new FormControl(),
          station: new FormControl(),
          phoneNumber: new FormControl(),
          faxNumber: new FormControl(),
          extension: new FormControl()
        })
      }),
    });

    this.userId = Number(localStorage.getItem('userID'));
    this.userADID = localStorage.getItem('userADID');
    this.workGroup = Number(this.activatedRoute.snapshot.params["WG"] || 1);
    this.orderId = Number(this.activatedRoute.snapshot.params["id"] || 0);

    this.spinner.show();

    if (this.orderId > 0) {
      this.getWGOrderData();
    }

    this.PopulateHiddenFields();
    this.GetFTNData();
  }

  bindChkOrderModificationList() {
    this.chkOrderModificationList = [
      "Add JPRDY With Hold",
      "Add JPRDY No Hold",
      "Remove JPRDY/Hold",
      "CLLI Modify",
      "Assign User",
      "Move Order Back",
      "Insert Note",
      "None"
    ];
  }

  getWGOrderData() {
    let data = zip(
      this.workGroupService.GetWGData(this.workGroup, 0, this.orderId, false, null),
      this.workGroupService.GetFTNListDetails(this.orderId, this.workGroup),
      this.userProfileService.getMapUserProfilesByUserId(this.userId),
      this.dcpeService.GetCPEOrderInfo_V5U(this.orderId),
      this.orderNoteService.getByOrderId(this.orderId),
      this.workGroupService.GetLatestNonSystemOrderNoteInfo(this.orderId),
      this.ccdService.GetCCDHistory(this.orderId),
      this.workGroupService.OrderExistsInSalesSupportWG(this.orderId),
      this.fsaOrdersService.getById(this.orderId),
      this.fsaOrdersService.getRelatedOrdersById(this.orderId),
      this.fsaOrderCpeLineItemService.getByOrderId(this.orderId), // FSA CPE ORDER LINE ITEMS
    )


    data.subscribe(res => {
      let order = res[0]
      this.ftnList = res[1]
      this.loggedInUserProfilesList = res[2]
      let orderInfo = res[3]
      this.orderInfo_V5U = orderInfo;
      this.noteList = res[4]
      this.noteInfo = res[5] != "" ? "Latest order notes updated by: " + res[5] : res[5]
      this.ccdList = res[6]
      let fsaOrder = res[8]
      let relatedOrdersList = res[9] as any[]
      this.fsaOrderCpeLineItemList = res[10]

      // M5 # Details
      this.m5DetailList = this.m5DetailList.concat(fsaOrder)
      this.m5DetailList = this.m5DetailList.concat(relatedOrdersList)

      this.deviceId = sessionStorage.getItem('deviceId');
      this.dcpeService.IsNIDDevice(this.orderId, this.deviceId).subscribe(data => {
        this.showNID = data;
      });

      if(order.length > 0) {

        this.dcpeService.GetSerialNumberAndIP(this.orderId, this.deviceId).subscribe(data => {
          if(data) {
            const count = data.length;
            let lastIndex = count - 1;
            this.rawNidSerialNumber = data[lastIndex].nidSerialNbr
            this.form.get("gomFileds.nidSerialNumber").setValue(data[lastIndex].nidSerialNbr);
            this.form.get("gomFileds.nidIp").setValue(data[lastIndex].nidIp);
          }
        });
        
        const hasHyphen = order[0]['ftn'].includes('-');
        if(hasHyphen) {
          const arr = order[0]['ftn'].split('-');
          this.ftn = arr[0];
        }
      }

      // DE38942
      if(this.m5DetailList.length > 0) {
        this.orderTypeDesc = this.m5DetailList[0].ordrTypeDes.toUpperCase();
      }

      if (order.length > 0) {
        this.taskId = order.map(a => a.tasK_ID)[0];
        this.hfSelectedFTN = order.map(a => a.ftn)[0];
        this.IsOrderComplete(order);

        if (this.taskId == 106) {
          this.lblMessage = "";
          this.btnMoveText = "Clear CCD Notification";
        }

        if (this.taskId == 109)
          this.lblMessage = "";
        else if (this.taskId == 100) {
          this.workGroupService.GetPreSubmitRTSStatus(this.orderId).subscribe(res => {
            if (!res)
              this.isRTSLoaded = true;
          });
        }

        if (this.orderId != 0) {
          if (!res[7])
            this.isSSNLoaded = true;
        }

        if (!(this.hfOrderComplete)) {
          if (this.orderId != 0) {
            //txtNotes.Enabled = false;
            this.CheckIn();
          }
        }
        else {
          this.workGroupService.VerifyUserIsAdminToEditCompletedOrder(this.userADID, "menuGOMAdminEdit").subscribe(
            data => {
              if (data) {
                this.bAdminEdit = true;
                this.CheckIn();
              }
            });
        }

        //this.PopulateFTNGrid();
      }

      if (!this.hfOrderComplete) {
        this.LockUnlockPageControls();

        if (!(this.loggedInUserProfilesList.find(a => a.usrPrfDes === 'GOM Order RTS') || this.loggedInUserProfilesList.find(a => a.usrPrfDes === 'GOM Order Updater'))) {
          this.lblError = "User has Reviewer permissions, the order will be opened in locked mode.";
          this.LockPageControls();
        }
      }
      else {
        if (this.hfSelectedFTN != "") {
          this.EnableGOMComplete(this.hfSelectedFTN);
        }
        this.LockPageControls();
      }
    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      if (this.m5DetailList[0] != null) {
        this.m5Data = this.m5DetailList[0];
        this.m5  = this.m5Data["ftn"];
        this.form.get("display.customerName").setValue(this.m5Data["custNme"]);
        this.form.get("display.orderType").setValue(this.m5Data["ordrTypeDes"]);
        this.form.get("display.orderSubType").setValue(this.m5Data["ordrSubTypeDes"]);
        this.form.get("display.productType").setValue(this.m5Data["prodTypeDes"]);
        this.form.get("display.orderStatus").setValue(this.m5Data["ordrStusDes"]);
        this.form.get("display.pendingTasks").setValue(this.m5Data["pendingTasks"]);

        this.onM5Clicked(null, this.m5DetailList[0])
      } else {
        this.spinner.hide()
      }
    });

  }
  
  IsOrderComplete(res: any[]) {
    let sOrderStatus = res.map(a => a.ordR_STUS)[0];
    if (sOrderStatus == null || sOrderStatus.toUpperCase() == "COMPLETED")
      this.hfOrderComplete = true;
    else if (sOrderStatus.toUpperCase() == "CANCELLED" || sOrderStatus.toUpperCase() == "DISCONNECTED") {
      this.workGroupService.HasBillMissingTask(this.orderId).subscribe(res => {
        if (res)
          this.btnMoveText = "Clear Billing Missing Task";
        else
          this.hfOrderComplete = true;
      });
    }
  }

  CheckIn() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, false, 0).subscribe(
      data => {
        if (data == 1) {
          this.lblError = "This order is currently locked. Try again later.";
          this.LockPageControls();
          this.hfLockedOrderID = this.orderId.toString();
        }
      });

  }

  LockPageControls() {
    if (!this.hfOrderComplete && this.bAdminEdit) {
      if (this.helper.isEmpty(this.hfSelectedOrderID))
        this.hfSelectedOrderID = this.orderId.toString();
    }
    else {
      this.form.disable();
      this.locked = true;
      //btnCancel.Enabled = true;
    }
  }

  UnlockPageControls() {
    this.form.enable();
  }

  PopulateHiddenFields() {

  }

  GetFTNData() {
    this.dcpeService.GetFTNData(this.orderId, this.workGroup).subscribe(
      data => {
        if (data != null && data.length > 0) {
          this.hfSelectedDeviceOrder = data.map(a => a.deviceId)[0] + "-" + data.map(a => a.ftn)[0];
          //lblDeviceOrder.Text = hfSelectedDeviceOrder.Value;
          this.hfTaskID = data.map(a => a.taskID)[0];
        }
      });
  }

  LockUnlockPageControls() {
    if (this.hfSelectedFTN == "") {
      this.LockPageControls();
    }
    else {
      this.EnableGOMComplete(this.hfSelectedFTN);
      if ((this.orderId.toString() != this.hfLockedOrderID) && this.taskID != 0) {
        this.UnlockPageControls();
      }
      else {
        this.LockPageControls();
      }
    }
  }

  EnableGOMComplete(ftn: string) {

    let _actionCode: number = 0;
    let _taskId: number = 0;
    let _isIntl: number = 0;
    let _prodType: string = "";
    let data: any = [];

    if (this.ftnList != null && this.ftnList.length > 0) {
      data = this.ftnList.find(a => a.ftn == ftn && a.wG_ID == this.workGroup) as any[];
    }

    if (data != null) {
      this.taskID = _taskId = data.tasK_ID;
      _actionCode = data.ordR_ACTN_ID;
      _isIntl = data.dmstC_CD == true ? 1 : 0;
      _prodType = data.proD_TYPE_CD;
    }

    if (_taskId != 0) {
      //this.enablebtnSave = true;
      //this.enablebtnMove = true;
      //this.enablebtnReview = true;
      this.lblFTNMsg = "";
      this.LoadGOMSpecificData(_taskId, _actionCode, _isIntl, _prodType);

      if (_actionCode == 2) {
        this.PopulateNRMData();
      }
    }
    else {
      //this.lblFTNMsg = "Selected M5 # is not in the GOM workgroup.";
      this.isGOMFieldsLoaded = true;
      this.displaybtnMove = false;
      this.displaybtnSave = false;
      this.form.disable();
    }
      
  }

  LoadGOMSpecificData(taskId: number, actionCode: number, isIntl: number, prodType: string) {

    if (this.taskID == 101) {
      this.isReviewDataLoaded = true;
      this.displaybtnRC = true;

      if (actionCode == 1)
        this.displaybtnReview = true;
      else {
        this.workGroupService.GetH5FolderInfo(this.orderId).subscribe(res => {
          if (res != null && res.length > 0) {
            let OD_H5 = res.map(a => a.oD_H5)[0];
            let HF_H5 = res.map(a => a.hF_H5)[0];

            if (OD_H5 == "0")
              this.lblMsg = "H6 Folder Association missing.";
            else
              this.lblMsg = "";

            if (HF_H5 == "0") {
              this.displaybtnReview = false;
            }
            else {
              if (OD_H5 == "0") {
                this.lblMsg = "H6 Folder created manually. Please move out of Intl CPE Error.";
              }
              this.displaybtnReview = true;
            }
          }
          else {
            this.lblMsg = "";
            this.displaybtnReview = false;
          }
        });
      }
    }
    else if (this.taskID == 600) {
      //Show Equipment Review UserControl 
      this.isEquipmentReviewDataLoaded = true;
      this.displaybtnMove = false;
      this.isSSNLoaded = false;
    }
    else if (this.taskID == 601) {
      // Show Material Requisition UserControl
      this.isMatReqDataLoaded = true;
      this.displaybtnMove = false;
      this.isSSNLoaded = false;
    }
    else {
      if (isIntl == 1 && prodType == "CP" && taskId != 109) {
        this.isGOMFieldsLoaded = true;
        this.displaybtnMove = true;
        this.displaybtnSave = true;
      }
      else {
        this.isGOMFieldsLoaded = true;
        this.form.disable();
        this.displaybtnMove = false;
        this.displaybtnSave = false;
      }
      this.isReviewDataLoaded = false;

      if (actionCode == 2 && (taskId == 102 || 105 || 106 || 107 || 108 || 109 || 103 || 110 || 112)) {
        this.displaybtnMove = true;

        if (taskId == 103)
          this.displayPL = true;

        if (taskId == 112)
          this.setFormControlValue("SPECIAL INSTRUCTIONS:  Please fill in any additional requirements or information.\n 1.Reuse:  PL or M5 # (for terms reuse) \n 2.Reuse NUA: Yes or No \n 3.Port speed: T1 \n 4.IP Node(TOC):  Miami", "notes");

        //commented to not display text box for entering Billing FTN
        //else if (_TaskID == 110)
        //    trBillFTN.Visible = true;
      }
      else {
        if (actionCode == 1 && taskId == 100) {
          this.workGroupService.GetPreSubmitRTSStatus(this.orderId).subscribe(res => {
            if (!res) {
              this.isRTSLoaded = true;
            }
          });
        }
        this.displaybtnMove = false;
      }
    }
  }

  PopulateNRMData() {
    this.isNRMLoaded = true;
  }

  GetOrderInformation(ordrId: number) {
    this.dcpeService.GetCPEOrderInfo_V5U(ordrId).subscribe(
      data => {
        this.orderInfo_V5U = data;
        if (data != null && data.length > 0) {
          this.ftn = data.map(a => a.ftn)[0];
          this.form.get("servingCPEClli").setValue(data.map(a => a.cpE_CLLI)[0]);
          this.form.get("assignedOrderToADID").setValue(data.map(a => a.asN_ADID)[0]);
          this.deviceId = data.map(a => a.devicE_ID)[0];
        }
      });
  }

  BindTasks() {
    this.dcpeService.GetTasksList().subscribe(data => {
      this.tasksList = data;
    })
  }

  BindDropDownJeopardyCode() {
    this.dcpeService.GetJeopardyCodeList().subscribe(data => {
      this.jeopardyCodeList = data;
    })
  }

  GetPrimary_IPM_CDT(ordrId: number) {
    this.dcpeService.GetCpeAcctCntct_V5U(ordrId).subscribe(data => {
      this.cpeAcctCntctsList = data.filter(a => a.roleName == 'Implementation Project Manager' && (a.ciS_LVL_TYPE == 'OD'  || a.ciS_LVL_TYPE == 'H6'  || a.ciS_LVL_TYPE == 'H1'));

      if (this.cpeAcctCntctsList != null && this.cpeAcctCntctsList.length > 0) {

        let length = this.cpeAcctCntctsList.length; 
        if(length > 1) {
          let index = this.cpeAcctCntctsList.findIndex(x => x.ciS_LVL_TYPE == 'H6');
          this.cpeAcctCntctsList = this.cpeAcctCntctsList[index];
        } else {
          this.cpeAcctCntctsList = this.cpeAcctCntctsList[0];
        }

        this.primaryEmail = this.cpeAcctCntctsList['email'];
        this.emailPrimaryIPMText = "Send Email to Primary IPM: " + this.cpeAcctCntctsList['cntcT_NME'];
      }
      else {
        this.emailPrimaryIPMText = "Send Email to Primary IPM: ";
      }
    })
  }

  GetTracking_SIS_CDT(ordrId: number) {
    this.dcpeService.GetCpeAcctCntct_V5U(ordrId).subscribe(data => {
      this.cpeAcctCntctsList = data.filter(a => a.roleName == 'Implementation Specialist' && (a.ciS_LVL_TYPE == 'OD'  || a.ciS_LVL_TYPE == 'H6'  || a.ciS_LVL_TYPE == 'H1'));

      if (this.cpeAcctCntctsList != null && this.cpeAcctCntctsList.length > 0) {

        let length = this.cpeAcctCntctsList.length; 
        if(length > 1) {
          let index = this.cpeAcctCntctsList.findIndex(x => x.ciS_LVL_TYPE == 'H6');
          this.cpeAcctCntctsList = this.cpeAcctCntctsList[index];
        } else {
          this.cpeAcctCntctsList = this.cpeAcctCntctsList[0];
        }

        this.secondaryEmail = this.cpeAcctCntctsList['email'];
        this.emailPrimaryISText = "Copy Email to Primary IS: " + this.cpeAcctCntctsList['cntcT_NME'];
      }
      else {
        this.emailPrimaryISText = "Copy Email to Primary IS: ";
      }
    })
  }

  BindDropDownAssignedTech() {
    this.dcpeService.GetAssignedTechList().subscribe(data => {
      this.reAssignedTechsList = data;
    })
  }
  
  GetTechAssigned(ordrId: number) {
    this.dcpeService.GetTechAssignment_V5U(ordrId).subscribe(data => {
      this.techList = data;

      if (this.techList != null && this.techList.length > 0) {
        this.reAssignedTech = this.techList.map(a => a.useR_CPE_TECH_NME)[0];
        this.reDispatchTime = this.techList.map(a => a.dsptcH_TM)[0];
        this.reEventId = this.techList.map(a => a.evenT_ID)[0];
      }
    });
  }

  GetCPELineItems(ordrId: number) {
    this.dcpeService.GetCPELineItems_V5U(ordrId).subscribe(
      data => {
        this.cpeLineItemsList = data;
      });
  }

  GetEquipmentOnlyInfo(ordrId: number) {
    if (this.eqptOnlyCD == "Y") {
      this.dcpeService.GetEquipOnlyInfo(ordrId).subscribe(
        data => {
          this.equipOnlyInfoList = data;
        });
    }
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }
    return value;
  }

  btnReview_Click() {
    this.GOMTask(101, 3);
  }

  btnRC_Click() {
    this.orderCompleted();
  }

  btnSave_Click() {
    let cpeEquipType = this.gomFieldDetails.form.value.cpeEquipType;
    let reqAmount = this.gomFieldDetails.form.value.reqAmount;
    let cpeCompletionDate = this.gomFieldDetails.form.value.cpeCompletionDate;
    let nidSerialNumber = this.gomFieldDetails.form.value.nidSerialNumber;

    let orderModel = {
      OrderId: this.orderId,
      UserId: this.userId,
      NoteText: this.getFormControlValue("notes") == null ? String.Empty : this.getFormControlValue("notes"),
      CPEStatusId: this.gomFieldDetails.selectedCpeOrderStatus,
      CpeEquipmentType: cpeEquipType,
      CompleteGOMDate: cpeCompletionDate,
      ReqAmount: reqAmount,
      GOMItems: this.gomFieldDetails.cpeItemsList
    }

    this.dcpeService.isNidAlreadyUsed(this.orderId, nidSerialNumber).subscribe(
      res => {
        console.log(res)
        if (res) {
          //show error
          //this.spinner.hide()
          this.helper.notify(`NID Serial Number has already been used`, Global.NOTIFY_TYPE_ERROR);
        } else {
          this.dcpeService.SaveGOMSpecificData(orderModel).subscribe(data => {
            if (data) {
              if (nidSerialNumber) {
                this.updateNidSerial(nidSerialNumber, this.rawNidSerialNumber);
              }

              this.helper.notifySavedFormMessage("Intl CPE info has been updated!", Global.NOTIFY_TYPE_SUCCESS, false, null);
              this.form.get("notes").setValue("");
            }
          });
        }
      }
    )

    //this.dcpeService.SaveGOMSpecificData(orderModel).subscribe(data => {
    //  if (data) {
    //    if(nidSerialNumber) {
    //      this.updateNidSerial(nidSerialNumber , this.rawNidSerialNumber);
    //    }
        
    //    this.helper.notifySavedFormMessage("Intl CPE info has been updated!", Global.NOTIFY_TYPE_SUCCESS, false, null);
    //    this.form.get("notes").setValue("");
    //  }
    //});
    
  }

  //btnCancel_Click($event) {
  //  this.orderCompleted();
  //}
  
  btnMove_Click() {
    console.log("entered Move_Click");
    let cpeEquipType = this.gomFieldDetails.form.value.cpeEquipType;
    let reqAmount = this.gomFieldDetails.form.value.reqAmount;
    let cpeCompletionDate = this.gomFieldDetails.form.value.cpeCompletionDate;
    let nidSerialNumber = this.gomFieldDetails.form.value.nidSerialNumber;
    
    let orderModel = {
      OrderId: this.orderId,
      UserId: this.userId,
      NoteText: this.getFormControlValue("notes") == null ? String.Empty : this.getFormControlValue("notes"),
      CPEStatusId: this.gomFieldDetails.selectedCpeOrderStatus,
      CpeEquipmentType: cpeEquipType,
      CompleteGOMDate: cpeCompletionDate,
      ReqAmount: reqAmount,
      GOMItems: this.gomFieldDetails.cpeItemsList
    }
    
    this.dcpeService.SaveGOMSpecificData(orderModel).subscribe(data => {
      if (data) {
        if(nidSerialNumber) {
          this.updateNidSerial(nidSerialNumber , this.rawNidSerialNumber);
        }
        

        let ftnData: any = [];
         if (this.ftnList != null && this.ftnList.length > 0) {
          if (this.taskId == 106) {
            ftnData = this.ftnList.find(a => a.ftn == this.hfSelectedFTN && a.wG_ID == this.workGroup && a.tasK_ID == 106) as any[]
          }
          else if (this.taskId == 106) {
            ftnData = this.ftnList.find(a => a.ftn == this.hfSelectedFTN && a.wG_ID == this.workGroup && a.tasK_ID == 109) as any[]
          }
          else {
            ftnData = this.ftnList.find(a => a.ftn == this.hfSelectedFTN && a.wG_ID == this.workGroup && a.tasK_ID != 106 && a.tasK_ID != 109) as any[]
          }
        }
        let taskID = 0;
        if (ftnData != null) {
          taskID = ftnData.tasK_ID;
        }
        else {
          if (this.taskId == 106) {
            this.helper.notify(`Selected FTN does not have a Pending CCD task to clear.`, Global.NOTIFY_TYPE_WARNING);
            return;
          }
          else if (this.taskId == 109) {
            this.helper.notify(`Selected FTN does not have a Pending Intl CPE Missing Billing Order task to clear.`, Global.NOTIFY_TYPE_WARNING);
            return;
          }
        }

        this.GOMTask(taskID, 2);
      }
    });
  }

  btnRTS_Click() {
    if (this.ValidateJeopardyCodeSelections()) {
      let orderModel = {
        OrderId: this.orderId,
        NoteText: this.getFormControlValue("notes")
      }

      this.dcpeService.GOMRTSClick(orderModel).subscribe(data => {
        if (data) {
          this.orderCompleted();
        }
      });
    }
  }

  btnSSNotes_Click() {
    //string sURL = "~/WGPages/RTSOrderDetail.aspx?catid=" + 2 + "&WGID=" + 4 + "&hid=" + 0 + "&notes=" + 1;
    //Response.Redirect(sURL, false);
    this.router.navigate(['order/rts/' + this.workGroup + '/' + this.orderId + '/' + this.taskId]);
  }

  GOMTask(taskId: number, taskStatus: number) {
    let orderModel = {
      OrderId: this.orderId,
      TaskId: taskId,
      TaskStatus: taskStatus,
      UserId: this.userId,
      NoteText: this.getFormControlValue("notes") == null ? String.Empty : this.getFormControlValue("notes")
    }

    this.dcpeService.CompleteGOMTask(orderModel).subscribe(data => {
      if (data) {
        this.orderCompleted();
        this.form.get("notes").setValue("");
        //this.helper.notifySavedFormMessage("Submit To ODIE Success", Global.NOTIFY_TYPE_SUCCESS, false, null);
      }
      return true;
    },
      error => {
        //this.helper.notifySavedFormMessage("GOMTask", Global.NOTIFY_TYPE_ERROR, true, "Error while Submitting to ODIE.");
        return false;
      }, () => {
        this.spinner.hide();
      });  
  }

  //#region "Equipment Review"
  
  btnSubmitToODIE_Click() {
    let chkSMR = this.getFormControlValue("equipmentReview.standardMatlReq");
    let chkNMR = this.getFormControlValue("equipmentReview.noMatlReq");

    let orderModel = {
      OrderId: this.orderId,
      TaskId: this.taskID,
      TaskStatus: 2,
      UserId: this.userId,
      chkSMR: chkSMR,
      chkNMR: chkNMR,
      NoteText: this.getFormControlValue("notes") == null ? String.Empty : this.getFormControlValue("notes"),
      OrderType: this.equipReviewDetails.orderType,
      FTN: this.equipReviewDetails.deviceFtn
    }

    this.dcpeService.completeODIETask(orderModel).subscribe(data => {
      if (data) {
          this.orderCompleted();
          this.helper.notifySavedFormMessage("Submit To ODIE Success", Global.NOTIFY_TYPE_SUCCESS, false, null);
        }
        return true;
      },
      error => {
        this.helper.notifySavedFormMessage("completeODIETask", Global.NOTIFY_TYPE_ERROR, true, "Error while Submitting to ODIE.");
        return false;
      }, () => {
        this.spinner.hide();
      });   
  }
  
  getOrderNotes(nteTypeId: number, nteTxt: string) {
    return {
      ordrId: this.orderId.toString(),
      nteTypeId: nteTypeId,
      nteTxt: nteTxt == "" ? this.form.get("notes").value : nteTxt
    }
  }

  orderCompleted() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, true, 0).subscribe(
      data => {
        if (data == 0) {
            this.router.navigate(['order/gom/']);
        }
      });
  }
  //#endregion

  //#region "Material Requisition"
  public ValidateMatReqForm(): boolean {
    let rasDate = this.getFormControlValue("materialRequisition.rasDate") == null ? this.matReqDetails.rasDateValue : this.getFormControlValue("materialRequisition.rasDate");
    let elid = this.getFormControlValue("materialRequisition.elid") == null ? "" : this.getFormControlValue("materialRequisition.elid");
    let businessUnitGL = this.getFormControlValue("materialRequisition.businessUnitGL") == null ? this.matReqDetails.businessUnitGL : this.getFormControlValue("materialRequisition.businessUnitGL");
    let costCenter = this.getFormControlValue("materialRequisition.costCenter") == null ? this.matReqDetails.costCenter : this.getFormControlValue("materialRequisition.costCenter");
    console.log("this.matReqDetails.deliveryName: " + this.matReqDetails.deliveryName);
    console.log("this.matReqDetails.customerName: " + this.matReqDetails.customerName);
    console.log('this.getFormControlValue("materialRequisition.deliveryName"): ' + this.getFormControlValue("materialRequisition.deliveryName"));
    //let deliveryName = "";
    //if (this.matReqDetails.chkInstallAddr)
    //  deliveryName = this.matReqDetails.customerName;
    //else if (this.matReqDetails.chkShipAddr)
    let deliveryName = this.getFormControlValue("materialRequisition.deliveryName");
    console.log("deliveryName: " + deliveryName);
    console.log('this.getFormControlValue("materialRequisition.deliveryAddr1"): ' + this.getFormControlValue("materialRequisition.deliveryAddr1"));
    let deliveryAddr1 = this.getFormControlValue("materialRequisition.deliveryAddr1") == null ? "" : this.getFormControlValue("materialRequisition.deliveryAddr1");
    console.log('this.getFormControlValue("materialRequisition.deliveryCity"): ' + this.getFormControlValue("materialRequisition.deliveryCity"));
    let deliveryCity = this.getFormControlValue("materialRequisition.deliveryCity") == null ? "" : this.getFormControlValue("materialRequisition.deliveryCity");
    console.log('this.getFormControlValue("materialRequisition.deliveryCountry"): ' + this.getFormControlValue("materialRequisition.deliveryCountry"));
    let deliveryCountry = this.getFormControlValue("materialRequisition.deliveryCountry") == null ? "" : this.getFormControlValue("materialRequisition.deliveryCountry");
    
    if (this.helper.isEmpty(rasDate)) {
      this.helper.notify(`RAS Date has to be selected.`, Global.NOTIFY_TYPE_WARNING);
      return false
    }
    else if (this.helper.isEmpty(elid.trim())) {
      this.helper.notify(`ELID has to be selected.`, Global.NOTIFY_TYPE_WARNING);
      return false
    }
    else if (this.helper.isEmpty(businessUnitGL)) {
      this.helper.notify(`Business Unit has to be selected.`, Global.NOTIFY_TYPE_WARNING);
      return false
    }
    else if (this.helper.isEmpty(costCenter)) {
      this.helper.notify(`Cost Center has to be selected.`, Global.NOTIFY_TYPE_WARNING);
      return false
    }

    if (this.matReqDetails.chkShipAddr) {
      if (this.helper.isEmpty(deliveryName.trim())) {
        this.helper.notify(`Delivery Name has to be selected.`, Global.NOTIFY_TYPE_WARNING);
        return false
      }
      else if (this.helper.isEmpty(deliveryAddr1.trim())) {
        this.helper.notify(`Delivery Addr 1 has to be selected.`, Global.NOTIFY_TYPE_WARNING);
        return false
      }
      else if (this.helper.isEmpty(deliveryCity.trim())) {
        this.helper.notify(`Delivery City has to be selected.`, Global.NOTIFY_TYPE_WARNING);
        return false
      }
      else if (this.helper.isEmpty(deliveryCountry.trim())) {
        this.helper.notify(`Delivery Country has to be selected.`, Global.NOTIFY_TYPE_WARNING);
        return false
      }
    }

    let shippingInstructions = this.getFormControlValue("materialRequisition.shippingInstructions");
    if (!this.helper.isEmpty(shippingInstructions)) {
      var regExp = /^[a-z0-9]+$/;
      if (regExp.test(shippingInstructions.trim())) {
        this.helper.notify(`Shipping Instructions should only have Alphanumeric values.`, Global.NOTIFY_TYPE_WARNING);
        return false
      }
    }
    return true;
  }

  SendCompleteMatReq(isSendAndComplete: boolean) {
    if (this.ValidateMatReqForm()) {
      this.selectedItems = this.matReqDetails.selectedMatReqLineItemKeys;
      this.updatedItems = this.matReqDetails.updatedMatReqLineItemKeys;
      let items: any[] = [];
      if (this.selectedItems != null && this.selectedItems.length > 0) {

        this.selectedItems.forEach((item, index) => {
          let updatedItem = this.updatedItems.find(a => a.CmpntId == item.cmpnT_ID);

          if (updatedItem != null) {
            item.OrdrId = this.orderId;
            item.CmpntId = item.cmpnT_ID;
            item.ReqstnNbr = this.matReqDetails.reqNbr;
            item.MatCd = item.maT_CD;
            item.DlvyClli = this.matReqDetails.domesticCD ? (this.matReqDetails.chkShipAddr ? String.Empty : this.matReqDetails.selectedInternationalClliCode) : (this.matReqDetails.chkShipAddr ? String.Empty : this.matReqDetails.selectedDomesticClliCode);
            item.ItmDes = item.itM_DES;
            item.ManfPartNbr = item.manF_PART_NBR;
            item.ManfId = item.manF_ID;
            item.OrdrQty = item.ordR_QTY;
            item.UntMsr = updatedItem.UntMsr;
            item.Actvy = updatedItem.Actvy;
            item.UntPrice = updatedItem.UntPrice;
            item.RasDt = this.matReqDetails.rasDateValue;
            item.BusUntGl = this.matReqDetails.businessUnitGL;
            item.BusUntPc = this.matReqDetails.businessUnitPC;
            item.CostCntr = this.matReqDetails.costCenter;
            item.Regn = this.matReqDetails.region;
            item.Mrkt = this.matReqDetails.market;
            item.EqptTypeId = this.matReqDetails.eqptTypeID;
            item.ProjId = updatedItem.ProjId;
            item.SourceTyp = updatedItem.SourceTyp;
            item.RsrcCat = updatedItem.RsrcCat;
            item.FsaCpeLineItemId = item.fsA_CPE_LINE_ITEM_ID;
            item.ManfDiscntCd = item.manF_DISCNT_CD;
            item.ReqLineNbr = item.reQ_LINE_NBR;
            item.Afflt = String.Empty;
            item.ReqLineNbr = index + 1;
            item.VndrNme = updatedItem.VndrNme;
            item.RsrcSub = updatedItem.RsrcSub;
            item.CntrctId = updatedItem.CntrctId;
            item.Acct = updatedItem.Acct;
            item.CntrctLnNbr = updatedItem.CntrctLnNbr;
            item.Prodct = updatedItem.Prodct;
            item.AxlryId = updatedItem.AxlryId;
            item.InstCd = updatedItem.InstCd;
            item.SentDt = new Date();
            item.EqptTypeId = String.Empty;
            item.Comments = updatedItem.Comments;
          }
          else {
            item.OrdrId = this.orderId;
            item.CmpntId = item.cmpnT_ID;
            item.ReqstnNbr = this.matReqDetails.reqNbr;
            item.MatCd = item.maT_CD;
            item.DlvyClli = this.matReqDetails.domesticCD ? (this.matReqDetails.chkShipAddr ? String.Empty : this.matReqDetails.selectedInternationalClliCode) : (this.matReqDetails.chkShipAddr ? String.Empty : this.matReqDetails.selectedDomesticClliCode);
            item.ItmDes = item.itM_DES;
            item.ManfPartNbr = item.manF_PART_NBR;
            item.ManfId = item.manF_ID;
            item.OrdrQty = item.ordR_QTY;
            item.UntMsr = item.unT_MSR;
            item.Actvy = item.actvy;
            item.UntPrice = item.unT_PRICE;
            item.RasDt = this.matReqDetails.rasDateValue;
            item.BusUntGl = this.matReqDetails.businessUnitGL;
            item.BusUntPc = this.matReqDetails.businessUnitPC;
            item.CostCntr = this.matReqDetails.costCenter;
            item.Regn = this.matReqDetails.region;
            item.Mrkt = this.matReqDetails.market;
            item.EqptTypeId = this.matReqDetails.eqptTypeID;
            item.ProjId = item.proJ_ID;
            item.SourceTyp = item.sourcE_TYP;
            item.RsrcCat = item.rsrC_CAT;
            item.FsaCpeLineItemId = item.fsA_CPE_LINE_ITEM_ID;
            item.ManfDiscntCd = item.manF_DISCNT_CD;
            item.ReqLineNbr = item.reQ_LINE_NBR;
            item.Afflt = String.Empty;
            item.ReqLineNbr = index + 1;
            item.VndrNme = String.Empty;
            item.RsrcSub = String.Empty;
            item.CntrctId = String.Empty;
            item.Acct = item.acct;
            item.CntrctLnNbr = 0;
            item.Prodct = item.prodct;
            item.AxlryId = String.Empty;
            item.InstCd = String.Empty;
            item.SentDt = new Date();
            item.EqptTypeId = String.Empty;
            item.Comments = String.Empty;
          }
          items.push(item);
        });

      }

      let clliCode = "";
      if (this.matReqDetails.domesticCD)
        clliCode = this.matReqDetails.selectedInternationalClliCode;
      else
        clliCode = this.matReqDetails.selectedDomesticClliCode;

      let orderModel = {
        OrderId: this.orderId,
        TaskId: this.taskID,
        TaskStatus: 2,
        UserId: this.userId,
        NoteText: this.getFormControlValue("notes") == null ? String.Empty : this.getFormControlValue("notes"),
        PSReqLineItemQueue: items,
        DomesticCD: this.matReqDetails.domesticCD,
        InternationalCLLICd: this.matReqDetails.selectedInternationalClliCode,
        DomesticCLLICd: this.matReqDetails.selectedDomesticClliCode,
        ChkShipAddr: this.matReqDetails.chkShipAddr,
        DeliveryAddress: this.getReqHeaderShipAddress(true, String.Empty),
        InstallAddress: this.getReqHeaderShipAddress(false, clliCode),
        ShippingInstructions: this.getFormControlValue("materialRequisition.shippingInstructions") == null ? String.Empty : this.getFormControlValue("materialRequisition.shippingInstructions"),
        SendAndComplete: isSendAndComplete
      }

      this.dcpeService.completeMatReq(orderModel).subscribe(data => {
        if (data && !isSendAndComplete) {
          this.setFormControlValue("notes", "");
          this.getWGOrderData();
          this.matReqDetails.LoadMatReqForm();
          this.matReqDetails.updatedMatReqLineItemKeys = [];
          this.helper.notifySavedFormMessage("Material Requisition", Global.NOTIFY_TYPE_SUCCESS, false, null);
        }
        else if (data && isSendAndComplete) {
          this.helper.notifySavedFormMessage("Material Requisition", Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.orderCompleted();
        }
      });
    }
  }

  getReqHeaderShipAddress(chkShipAddress: boolean, clliCode: string) {
    if (chkShipAddress) {
      return {
        OrdrId: this.orderId.toString(),
        ReqstnNbr: this.matReqDetails.reqNbr,
        CustElid: this.getFormControlValue("materialRequisition.elid") == null ? String.Empty : this.getFormControlValue("materialRequisition.elid"),//this.matReqDetails.elid,
        DlvyClli: String.Empty,
        DlvyNme: this.getFormControlValue("materialRequisition.deliveryName") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryName"),
        DlvyAddr1: this.getFormControlValue("materialRequisition.deliveryAddr1") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryAddr1"),
        DlvyAddr2: this.getFormControlValue("materialRequisition.deliveryAddr2") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryAddr2"),
        DlvyAddr3: this.getFormControlValue("materialRequisition.deliveryAddr3") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryAddr3"),
        DlvyAddr4: String.Empty,
        DlvyCty: this.getFormControlValue("materialRequisition.deliveryCity") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryCity"),
        DlvyCnty: this.getFormControlValue("materialRequisition.deliveryCountry") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryCountry"),
        DlvySt: this.getFormControlValue("materialRequisition.deliveryState") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryState"),
        DlvyZip: this.getFormControlValue("materialRequisition.deliveryZip") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryZip"),
        DlvyPhnNbr: this.getFormControlValue("materialRequisition.deliveryPhone") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryPhone"),
        InstClli: String.Empty,
        MrkPkg: this.matReqDetails.hdnMrkPkg,
        RefNbr: this.getFormControlValue("materialRequisition.referenceNumber") == null ? String.Empty : this.getFormControlValue("materialRequisition.referenceNumber"),
        ShipCmmts: this.getFormControlValue("materialRequisition.shippingInstructions") == null ? String.Empty : this.getFormControlValue("materialRequisition.shippingInstructions"),
        RfqIndctr: this.getFormControlValue("materialRequisition.rfqIndicator") == null ? String.Empty : this.getFormControlValue("materialRequisition.rfqIndicator"),
        DlvyBldg: this.getFormControlValue("materialRequisition.deliveryBldg") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryBldg"),
        DlvyFlr: this.getFormControlValue("materialRequisition.deliveryFlr") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryFlr"),
        DlvyRm: this.getFormControlValue("materialRequisition.deliveryRm") == null ? String.Empty : this.getFormControlValue("materialRequisition.deliveryRm"),
        SentDt: this.matReqDetails.rasDateValue
      }
    }
    else {
      return {
        OrdrId: this.orderId.toString(),
        ReqstnNbr: this.matReqDetails.reqNbr,
        CustElid: this.matReqDetails.elid,
        DlvyClli: clliCode,
        DlvyNme: this.matReqDetails.customerName,
        DlvyAddr1: this.matReqDetails.address1,
        DlvyAddr2: this.matReqDetails.address2,
        DlvyAddr3: this.matReqDetails.address3,
        DlvyAddr4: String.Empty,
        DlvyCty: this.matReqDetails.city,
        DlvyCnty: this.matReqDetails.country,
        DlvySt: this.matReqDetails.state,
        DlvyZip: this.matReqDetails.zip,
        DlvyPhnNbr: this.matReqDetails.phone,
        InstClli: String.Empty,
        MrkPkg: this.matReqDetails.hdnMrkPkg,
        RefNbr: this.getFormControlValue("materialRequisition.referenceNumber") == null ? String.Empty : this.getFormControlValue("materialRequisition.referenceNumber"),
        ShipCmmts: this.getFormControlValue("materialRequisition.shippingInstructions") == null ? String.Empty : this.getFormControlValue("materialRequisition.shippingInstructions"),
        RfqIndctr: this.getFormControlValue("materialRequisition.rfqIndicator") == null ? String.Empty : this.getFormControlValue("materialRequisition.rfqIndicator"),
        DlvyBldg: this.matReqDetails.building,
        DlvyFlr: this.matReqDetails.floor,
        DlvyRm: this.matReqDetails.room,
        SentDt: this.matReqDetails.rasDateValue
      }
    }
  }
  
  btnSendOnlyMatReq_Click() {
    this.SendCompleteMatReq(false);
  }

  btnSendCompleteMatReq_Click() {
    this.SendCompleteMatReq(true);
  }
  //#endregion

  public onJeopardyCodeChanged(event: any) {
    this.selectedJeopardyCode = event.selectedItem.cpeJprdyCd;
  }

  public ValidateJeopardyCodeSelections(): boolean {
    if (this.selectedJeopardyCode == "") {
      this.helper.notify(`Jeopardy Code has to be selected.`, Global.NOTIFY_TYPE_WARNING);
      return false
    }
    return true;
  }




  btnOrderModification_Click() {
    if (this.ValidateJeopardyCodeSelections()) {
      let notes = this.getFormControlValue("notes");

      if (this.selectedOrderModification != "") {
        if (this.selectedOrderModification == "Add JPRDY With Hold") {
          this.dcpeService.UpdateCPEOrdr_V5U(this.orderId, 2, this.userADID, this.selectedJeopardyCode, notes, null).subscribe(data => {
            if (data)
              this.orderCompleted();
          });
        }
        else if (this.selectedOrderModification == "Add JPRDY No Hold") {
          this.dcpeService.UpdateCPEOrdr_V5U(this.orderId, 5, this.userADID, this.selectedJeopardyCode, notes, null).subscribe(data => {
            if (data)
              this.orderCompleted();
          });
        }
        else if (this.selectedOrderModification == "Remove JPRDY/Hold") {
          this.dcpeService.UpdateCPEOrdr_V5U(this.orderId, 3, this.userADID, null, notes, null).subscribe(data => {
            if (data)
              this.orderCompleted();
          });
        }
        else if (this.selectedOrderModification == "CLLI Modify") {
          this.dcpeService.UpdateCPEOrdr_V5U(this.orderId, 4, this.userADID, null, notes, this.getFormControlValue("servingCPEClli")).subscribe(data => {
            if (data)
              this.orderCompleted();
          });
        }
        else if (this.selectedOrderModification == "Assign User") {
          this.dcpeService.UpdateDmstcWFM_V5U(this.orderId, this.getFormControlValue("assignedOrderToADID"), this.userADID, notes).subscribe(data => {
            if (data)
              this.orderCompleted();
          });
        }
        else if (this.selectedOrderModification == "Insert Note") {
          this.dcpeService.UpdateCPEOrdr_V5U(this.orderId, 1, this.userADID, null, notes, null).subscribe(data => {
            if (data)
              this.orderCompleted();
          });
        }

        if (this.selectedOrderModification == "Move Order Back") {
          this.dcpeService.ProcessCPEMve_V5U(this.orderId, this.userADID, this.selectedReworkToTask, notes).subscribe(data => {
            if (data)
              this.orderCompleted();
          });
        }
      }
      else {
        this.helper.notify(`Atleast one Order Modification option has to be selected.`, Global.NOTIFY_TYPE_WARNING);
      }
    }
  }

  btnCancel_Click() {
    this.orderCompleted();
  }

  onChkOrderModificationChanged($event) {
    this.selectedOrderModification = $event.value;

    if (this.selectedOrderModification == "None") {
      this.selectedOrderModification = "";
    }
  }

  onReworkToTaskChanged($event) {
    this.selectedReworkToTask = $event.value;
  }

  onM5Clicked(e, data) {
    if (e != null) {
      (e as MouseEvent).preventDefault()
    }

    if (this.fsaDetails.isShown && this.fsaDetails.id == data.ordrId) { // Hide FSA Details
      this.fsaDetails.hide()
    } else if (!this.fsaDetails.isShown && this.fsaDetails.id == data.ordrId) { // Show already loaded
      this.fsaDetails.show(data.ordrId)
    } else { // Get new FSA Details
      this.amnciService.getFsaDetails(data.ordrId, this)
    }
  }

  updateNidSerial(nidSerial: string, rawNidSerial: string) {

    if(nidSerial != rawNidSerial) {
      this.dcpeService.UpdateNIDSerialNbr(this.orderId, this.ftn, this.deviceId, nidSerial).subscribe(data => {
        console.log(data);
      });
    }

  }

}
