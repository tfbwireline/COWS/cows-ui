import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  OrderComponent, AmnciComponent, AmnciFormComponent, AnciComponent, EnciComponent, GomComponent, GomFormComponent, DcpeComponent,
  DcpeFormComponent, VcpeComponent, VcpeFormComponent, CPEReceiptsComponent, CPEReceiptsFormComponent, CscComponent, CscFormComponent, SystemOrderDetailFormComponent,
  CustomOrderViewComponent
} from './';

import { VendorSearchComponent } from './vendor-search/vendor-search.component';
import { RTSComponent } from './rts/rts.component';
import { RTSFormComponent } from './rts/rts-form.component';
import { ThirdPartyCpeTechComponent } from './3rd-party-cpe-tech/3rd-party-cpe-tech.component';
import { ThirdPartyCpeTechFormComponent } from './3rd-party-cpe-tech/3rd-party-cpe-tech-form.component';
import { AsrTemplateComponent } from './asr/asr-template.component';

const routes: Routes = [
  //{
  //  path: '',
  //  component: OrderComponent,
  //},
  {
    path: 'amnci',
    data: { title: "AMNCI" },
    children: [
      { path: '', component: AmnciComponent },
      //{ path: 'add', component: AmnciFormComponent },
      { path: ':WG/:id', component: AmnciFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'enci',
    data: { title: "ENCI" },
    children: [
      { path: '', component: EnciComponent },
      //{ path: 'add', component: AmnciFormComponent },
      { path: ':WG/:id', component: AmnciFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'anci',
    data: { title: "ANCI" },
    children: [
      { path: '', component: AnciComponent },
      //{ path: 'add', component: AmnciFormComponent },
      { path: ':WG/:id', component: AmnciFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'gom',
    data: { title: "Intl CPE" },
    children: [
      { path: '', component: GomComponent },
      //{ path: 'add', component: AmnciFormComponent },
      { path: ':WG/:id', component: GomFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'dcpe',
    data: { title: "DCPE" },
    children: [
      { path: '', component: DcpeComponent },
      { path: 'add', component: DcpeFormComponent },
      { path: ':WG/:id', component: DcpeFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'vcpe',
    data: { title: "VCPE" },
    children: [
      { path: '', component: VcpeComponent },
      { path: 'add', component: VcpeFormComponent },
      { path: ':WG/:id', component: VcpeFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'cpe-receipts',
    data: { title: "CPE Receipts" },
    children: [
      { path: '', component: CPEReceiptsComponent },
      { path: ':WG/:OrderID/:TaskID/:PO', component: CPEReceiptsFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'rts',
    data: { title: "Sales Support" },
    children: [
      { path: '', component: RTSComponent },
      { path: ':WG/:OrderID/:TaskID', component: RTSFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: '3rd-party-cpe-tech',
    data: { title: "3rd Party CPE Tech" },
    children: [
      { path: '', component: ThirdPartyCpeTechComponent },
      { path: ':WG/:id', component: ThirdPartyCpeTechFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'csc',
    data: { title: "CSC" },
    children: [
      { path: '', component: CscComponent },
      { path: 'add', component: CscFormComponent },
      { path: ':WG/:id', component: CscFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: 'vendor-search',
    data: { title: "VENDOR SEARCH" },
    component: VendorSearchComponent
  },
  {
    path: 'asr',
    data: { title: "ASR" },
    children: [
      { path: ':id/:wg/:catId', component: AsrTemplateComponent },
    ]
  },
  {
    path: 'system/:id',
    data: { title: "System" },
    component: SystemOrderDetailFormComponent
  },
  {
    path: "custom-order-view",
    data: { title: "Custom Order View" },
    children: [
      { path: "", component: CustomOrderViewComponent },
      {
        path: ":eventTypeId/:selectedViewId",
        component: CustomOrderViewComponent,
        data: { title: "Update" }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutes { }
