import { Component, OnInit } from '@angular/core';
import { WorkGroup } from "./../../../shared/global";

@Component({
  selector: 'app-anci',
  templateUrl: './anci.component.html'
})
export class AnciComponent implements OnInit {
  workGroup: number;

  constructor() { }  

  ngOnInit() {
    this.workGroup = WorkGroup.xNCIAsia;
  }
}
