import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import {
 AmnciService, CCDBypassService, OrderService, WorkGroupService, FsaOrderCpeLineItemService
} from '../../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { zip } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrderStatus } from '../../../shared/global';

import { forkJoin } from 'rxjs';

@Component({
    selector: 'app-system-order-detail-form',
    templateUrl: './system-order-detail-form.component.html'
})

export class SystemOrderDetailFormComponent implements OnInit {


    orderId : number = 0;
    form : FormGroup;

    taskId: number = 0;
    WG: number = 0;
    deviceOrder: string;

    m5DetailList: any;
    orderTypeDesc = "";
    showSalesSupportNoteBtn: boolean = false;
    ccdList: Array<any> = [];
    noteList = [];
    noteInfo: string = "";
  toHideFields = ['vlan', 'mds', 'change'];
  orderType: string = "System";
  fsaOrderCpeLineItemList = []

    constructor(
      private spinner: NgxSpinnerService,
      private activatedRoute: ActivatedRoute, 
      private amnciService: AmnciService, 
      private ccdService: CCDBypassService,
      private orderService: OrderService,
      private workGroupService: WorkGroupService,
      private router: Router,
      private location: Location,
      private fsaOrderCpeLineItemService: FsaOrderCpeLineItemService) {
        
    }


    ngOnInit() {
      this.orderId = this.activatedRoute.snapshot.params["id"] || 0
      this.taskId = this.activatedRoute.snapshot.queryParams['taskId'] || 0;
      this.WG = this.activatedRoute.snapshot.queryParams['systemOriginalWorkgroup'] || 0;

      this.form = new FormGroup({
        install: new FormGroup({
          mdsInstallationInformation: new FormGroup({
            solutionService: new FormControl(),
            networkTypeCode: new FormControl(),
            serviceTierCode: new FormControl(),
            transportOrderTypeCode: new FormControl(),
            designDocumentNo: new FormControl(),
            vendorCode: new FormControl()
          }),
          transport: new FormGroup({
            accessTypeCode: new FormControl(),
            accessArrangementCode: new FormControl(),
            encapsulationCode: new FormControl(),
            fiberHandoffCode: new FormControl(),
            carrierAccessCode: new FormControl(),
            circuitId: new FormControl(),
            serviceTerm: new FormControl(),
            quoteExpirationDate: new FormControl(),
            accessBandwidth: new FormControl(),
            privateLineNo: new FormControl(),
            m5AccessStatus: new FormControl(),
            classOfService: new FormControl()
          }),
          changeInformation: new FormGroup({
            changeDescription: new FormControl(),
            carrierServiceId: new FormControl(),
            carrierCircuitId: new FormControl(),
            originalInstallOrderId: new FormControl(),
            networkUserAddress: new FormControl(),
            portRateTypeCode: new FormControl()
          }),
          cpeInstallationInformation: new FormGroup({
            cpeOrderTypeCode: new FormControl(),
            equipmentOnlyFlagCode: new FormControl(),
            accessProvideCode: new FormControl(),
            phoneNumberType: new FormControl(),
            phoneNumber: new FormControl(),
            eccktIdentifier: new FormControl(),
            managedServicesChannelProgramCode: new FormControl(),
            deliveryDuties: new FormControl(),
            deliveryDutyAmount: new FormControl(),
            shippingChargeAmount: new FormControl(),
            recordOnly: new FormControl(),
            nuaOrCkt: new FormControl()
          })
        }),
        order: new FormGroup({
          orderAction: new FormControl(),
          ftn: new FormControl(),
          orderTypeCode: new FormControl(),
          orderSubTypeCode: new FormControl(),
          productTypeCode: new FormControl(),
          parentFtn: new FormControl(),
          relatedFtn: new FormControl(),
          telecomServicePriority: new FormControl(),
          carrierPartnerWholesaleType: new FormControl(),
          customerCommitDate: new FormControl(),
          customerWantDate: new FormControl(),
          customerOrderSubmitDate: new FormControl(),
          customerSignedDate: new FormControl(),
          orderSubmitDate: new FormControl(),
          customerPremiseCurrentlyOccupiedFlag: new FormControl(),
          willCustomerAcceptServiceEarlyFlag: new FormControl(),
          multipleOrderFlag: new FormControl(),
          multipleOrderIndex: new FormControl(),
          multipleOrderTotal: new FormControl(),
          escalatedFlag: new FormControl(),
          expediteTypeCode: new FormControl(),
          govtTypeCode: new FormControl(),
          vendorVpnCode: new FormControl(),
          prequalNo: new FormControl(),
          scaNumber: new FormControl()
        }),
        customer: new FormGroup({
          h1: new FormGroup({
            customerId: new FormControl(),
            customerName: new FormControl(),
            currentBillingCycleCode: new FormControl(),
            futureBillingCycleCode: new FormControl(),
            branchCode: new FormControl(),
            taxExemption: new FormControl(),
            charsId: new FormControl(),
            siteId: new FormControl()
          }),
          h4: new FormGroup({
            customerId: new FormControl(),
            customerName: new FormControl(),
            currentBillingCycleCode: new FormControl(),
            futureBillingCycleCode: new FormControl(),
            taxExemption: new FormControl(),
            charsId: new FormControl(),
            siteId: new FormControl()
          }),
          h4Address: new FormGroup({
            addressType: new FormControl(),
            addressLine1: new FormControl(),
            addressLine2: new FormControl(),
            addressLine3: new FormControl(),
            suite: new FormControl(),
            city: new FormControl(),
            stateCode: new FormControl(),
            zipCode: new FormControl(),
            countryCode: new FormControl(),
            provinceMunicipality: new FormControl()
          }),
          h4Contact: new FormGroup({
            type: new FormControl(),
            contactType: new FormControl(),
            firstName: new FormControl(),
            lastName: new FormControl(),
            name: new FormControl(),
            emailAddress: new FormControl(),
            countryCode: new FormControl(),
            cityCode: new FormControl(),
            npa: new FormControl(),
            nxx: new FormControl(),
            station: new FormControl(),
            phoneNumber: new FormControl(),
            faxNumber: new FormControl(),
            extension: new FormControl()
          }),
          h6: new FormGroup({
            customerId: new FormControl(),
            customerName: new FormControl(),
            currentBillingCycleCode: new FormControl(),
            futureBillingCycleCode: new FormControl(),
            branchCode: new FormControl(),
            taxExemption: new FormControl(),
            charsId: new FormControl(),
            siteId: new FormControl(),
            serviceSubType: new FormControl(),
            salesOfficeIdCode: new FormControl(),
            salesPersonPrimaryCid: new FormControl(),
            salesPersonSecondaryCid: new FormControl(),
            cllidCode: new FormControl(),
            addressType: new FormControl(),
            addressLine1: new FormControl(),
            addressLine2: new FormControl(),
            addressLine3: new FormControl(),
            suite: new FormControl(),
            city: new FormControl(),
            cityCode: new FormControl(),
            stateCode: new FormControl(),
            zipCode: new FormControl(),
            countryCode: new FormControl(),
            provinceMunicipality: new FormControl(),
            buildingName: new FormControl(),
            floor: new FormControl(),
            roomNumber: new FormControl(),
            npa: new FormControl(),
            nxx: new FormControl(),
            station: new FormControl(),
            phoneNumber: new FormControl(),
            faxNumber: new FormControl(),
            extension: new FormControl()
          }),
          h6Address: new FormGroup({
            addressType: new FormControl(),
            addressLine1: new FormControl(),
            addressLine2: new FormControl(),
            addressLine3: new FormControl(),
            suite: new FormControl(),
            city: new FormControl(),
            stateCode: new FormControl(),
            zipCode: new FormControl(),
            countryCode: new FormControl(),
            provinceMunicipality: new FormControl(),
            buildingName: new FormControl(),
            floor: new FormControl(),
            roomNumber: new FormControl()
          }),
          h6Contact: new FormGroup({
            type: new FormControl(),
            contactType: new FormControl(),
            firstName: new FormControl(),
            lastName: new FormControl(),
            name: new FormControl(),
            emailAddress: new FormControl(),
            countryCode: new FormControl(),
            cityCode: new FormControl(),
            npa: new FormControl(),
            nxx: new FormControl(),
            station: new FormControl(),
            phoneNumber: new FormControl(),
            faxNumber: new FormControl(),
            extension: new FormControl()
          })
        }),
        disconnect: new FormGroup({
          reasonCode: new FormControl(),
          cancelBeforeStartReasonCode: new FormControl(),
          cancelBeforeStartReasonText: new FormControl(),
          contact: new FormGroup({
            type: new FormControl(),
            contactType: new FormControl(),
            firstName: new FormControl(),
            lastName: new FormControl(),
            name: new FormControl(),
            emailAddress: new FormControl(),
            countryCode: new FormControl(),
            cityCode: new FormControl(),
            npa: new FormControl(),
            nxx: new FormControl(),
            station: new FormControl(),
            phoneNumber: new FormControl(),
            faxNumber: new FormControl(),
            extension: new FormControl()
          })
        }),
        nrm: new FormGroup({}),
      })



      this.spinner.show();
      this.amnciService.getFsaDetails(this.orderId, this);

      let data = [
        this.orderService.getById2(this.orderId),
        this.ccdService.GetCCDHistory(this.orderId),
        this.workGroupService.GetLatestNonSystemOrderNoteInfo(this.orderId),
        this.workGroupService.GetIsOrderExistInSalesSupportWG(this.orderId),
        this.fsaOrderCpeLineItemService.getByOrderId(this.orderId)
      ]

      if(this.WG !== 98) {
        data.push(this.workGroupService.GetFTNListDetails(this.orderId, this.WG));
      }


      forkJoin(data).subscribe(res => {
        let order = res[0];

        console.log(order);

        this.ccdList = res[1];
        this.noteInfo = res[2] != "" ? " - (Last updated by: " + res[2] + ")" : res[2]
        // this.showSalesSupportNoteBtn = !res[3] && (order.ordrStus.ordrStusId !== OrderStatus.Completed);

        if (order != null) {
          this.noteList = order.ordrNte.reverse(a => a.creatDt)
        }
        this.fsaOrderCpeLineItemList = res[4];

        if(this.WG !== 98) {
          let fsaOrder = [];
          let fsaOrderList = res[5].filter(a => a.tasK_ID == (this.taskId as number));
  
          if (fsaOrderList != null)
            fsaOrderList.forEach((item, index) => {
              if (index == 0) {
                if(order.fsaOrdr.fsaOrdrCpeLineItem != null) {
                  const lineItems = order.fsaOrdr.fsaOrdrCpeLineItem;
                  const ftn = order.fsaOrdr.ftn
                  this.deviceOrder = `${lineItems[0].deviceId}-${ftn}`;
                }
                fsaOrder.push(item);
              }
            });
  
          // M5 # Details
          this.m5DetailList = fsaOrder

          // DE38942
          if(this.m5DetailList.length > 0) {
            this.orderTypeDesc = this.m5DetailList[0].ordR_TYPE.toUpperCase();
          }

        }
        
        this.spinner.hide();
      })

      // let data = zip(

      // );
      // data.subscribe(res => {

      // });
    }

    addSalesSupportNotes() {
      this.router.navigateByUrl(`order/rts/156/${this.orderId}/${this.taskId}`);
    }

    cancel() {
      this.location.back();
    }
}
