import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { WorkGroup, Global } from "./../../../shared/global";
import { FormControl, FormGroup } from '@angular/forms';
import { AsrTypeService, AccessMbService, OrderLockService } from '../../../services';
import { zip } from 'rxjs';
import { Helper, RoutingStateService } from '../../../shared';
import { ActivatedRoute } from '@angular/router';
import { Asr } from '../../../models';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-asr-template',
  templateUrl: './asr-template.component.html'
})
export class AsrTemplateComponent implements OnInit {
  @Input("parent") parent: any;
  get asrType() { return this.form.get("asrType") }

  form: FormGroup

  // DROPDOWNS
  asrTypeList: any[]
  origTermList: any[] = [
    "Off Shore to US Domestic Router",
    "Off Shore to Intl Off Shore Router"
  ]
  yesNoList: any[] = [
    "Yes",
    "No"
  ]
  accessMbList = []
  selectedAccessMbList = []

  orderId: number = 0
  workGroup: number = 0
  categoryId: number = 0
  userId: number = 0

  constructor(public location: Location, private helper: Helper, private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService,
    private asrTypeService: AsrTypeService, private accessMbService: AccessMbService, private orderLockService: OrderLockService, private routingStateService: RoutingStateService) { }  

  ngOnInit() {
    this.orderId = this.parent.orderId || this.activatedRoute.snapshot.params["id"] || 0
    this.workGroup = this.parent.workGroup || this.activatedRoute.snapshot.params["wg"] || 0
    this.categoryId = this.parent.orderCatId || this.activatedRoute.snapshot.params['catId'] || 0

    this.form = new FormGroup({
      asrType: new FormControl(),
      origTerm: new FormControl(),
      aggregated: new FormGroup({
        accessCityNameSiteCode: new FormControl(),
        ipNode: new FormControl(),
        accessBandwidth: new FormControl(),
        transparent: new FormControl(),
        partnerCarrierCode: new FormControl(),
        lecNni: new FormControl(),
      }),
      dedicated: new FormGroup({
        ipNode: new FormControl(),
        accessBandwidth: new FormControl(),
        partnerCarrierCode: new FormControl(),
      }),
      tdm: new FormGroup({
        ipNode: new FormControl(),
        accessBandwidth: new FormControl(),
        partnerCarrierCode: new FormControl(),
        entranceAssignment: new FormControl()
      }),
      ipl: new FormGroup({
        ipNode: new FormControl(),
        accessBandwidth: new FormControl(),
      }),
      offnet: new FormGroup({
        masterVas: new FormControl(),
        dlci: new FormControl(),
        isH1Matched: new FormControl()
      }),
      notes: new FormControl()
    })

    this.init()
    this.userId = Number(localStorage.getItem('userID'));
  }

  init() {
    let data = zip(
      this.asrTypeService.get(),
      this.accessMbService.get(),
    )

    //this.spinner.show()
    data.subscribe(res => {
      this.asrTypeList = res[0]
      this.accessMbList = res[1]
      //this.spinner.hide()
    }, error => {
      console.log(error)
      //this.spinner.hide()
    });
  }

  onAsrTypeChanged(e) {
    this.selectedAccessMbList = this.accessMbList.filter(a => a.asrTypeId == e.value)
  }

  submit() {
    if ((this.asrType.value == 1 || this.asrType.value == 2) && this.form.get("origTerm").value == null) {
      // error
      this.helper.notify("Orig/Term is required for this ASR type.", Global.NOTIFY_TYPE_WARNING)
    } else {
      if (this.asrType.value != null) {
        //console.log(this.form.get("origTerm").value)
        //console.log(this.asrTypeList)
        //console.log(this.asrType.value)

        let asrTypeDesc = this.asrTypeList.find(a => a.asrTypeId == this.asrType.value).asrTypeDes
        let html = `ASR Details: </br>ASR Types: ${asrTypeDesc}</br>ASR Notes: ${this.form.get("notes").value}</br>`
        let obj = new Asr({
          ordrId: this.orderId,
          asrTypeId: this.asrType.value,
          intlDomEmailCd: this.origTermList.findIndex(a => a == this.form.get("origTerm").value).toString(),
          asrNotesTxt: this.helper.removeUnknownCharacter(this.form.get("notes").value)
        })

        if (this.asrType.value == 1) {
          obj.accsCtyNmeSiteCd = this.form.get("aggregated.accessCityNameSiteCode").value
          if (obj.accsCtyNmeSiteCd != null) {
            html = `${html}Access City Name & Site Code: ${obj.accsCtyNmeSiteCd}</br>`
          }
          obj.ipNodeTxt = this.form.get("aggregated.ipNode").value
          if (obj.ipNodeTxt != null) {
            html = `${html}IP Node (TOC/Router location &amp; Site Code): ${obj.ipNodeTxt}</br>`
          }
          obj.accsBdwdDes = this.form.get("aggregated.accessBandwidth").value
          if (obj.accsBdwdDes != null) {
            html = `${html}Access Bandwidth (not port or service speed): ${obj.accsBdwdDes}</br>`
          }
          obj.trnspntCd = (this.form.get("aggregated.transparent").value || "No") == "Yes" ? true : false
          if (obj.trnspntCd != null) {
            html = `${html}Transparent: ${obj.trnspntCd}</br>`
          }
          obj.ptnrCxrCd = this.form.get("aggregated.partnerCarrierCode").value
          if (obj.ptnrCxrCd != null) {
            html = `${html}Partner and Carrier Code (access vendor): ${obj.ptnrCxrCd}</br>`
          }
          obj.lecNniNbr = this.form.get("aggregated.lecNni").value
          if (obj.lecNniNbr != null) {
            html = `${html}LEC NNI (9 digit numeric value): ${obj.lecNniNbr}</br>`
          }
        } else if (this.asrType.value == 2) {
          obj.ipNodeTxt = this.form.get("dedicated.ipNode").value
          if (obj.ipNodeTxt != null) {
            html = `${html}IP Node (TOC/Router location &amp; Site Code): ${obj.ipNodeTxt}</br>`
          }
          obj.accsBdwdDes = this.form.get("dedicated.accessBandwidth").value
          if (obj.accsBdwdDes != null) {
            html = `${html}Access Bandwidth (not port or service speed): ${obj.accsBdwdDes}</br>`
          }
          obj.ptnrCxrCd = this.form.get("dedicated.partnerCarrierCode").value
          if (obj.ptnrCxrCd != null) {
            html = `${html}Partner and Carrier Code (access vendor): ${obj.ptnrCxrCd}</br>`
          }
        } else if (this.asrType.value == 3) {
          obj.ipNodeTxt = this.form.get("tdm.ipNode").value
          if (obj.ipNodeTxt != null) {
            html = `${html}IP Node (TOC/Router location &amp; Site Code): ${obj.ipNodeTxt}</br>`
          }
          obj.accsBdwdDes = this.form.get("tdm.accessBandwidth").value
          if (obj.accsBdwdDes != null) {
            html = `${html}Access Bandwidth (not port or service speed): ${obj.accsBdwdDes}</br>`
          }
          obj.ptnrCxrCd = this.form.get("tdm.partnerCarrierCode").value
          if (obj.ptnrCxrCd != null) {
            html = `${html}Partner and Carrier Code (access vendor): ${obj.ptnrCxrCd}</br>`
          }
          obj.entrncAsmtTxt = this.form.get("tdm.entranceAssignment").value
          if (obj.entrncAsmtTxt != null) {
            html = `${html}Entrance Assignment: ${obj.entrncAsmtTxt}</br>`
          }
        } else if (this.asrType.value == 4) {
          obj.ipNodeTxt = this.form.get("ipl.ipNode").value
          if (obj.ipNodeTxt != null) {
            html = `${html}IP Node (TOC/Router location &amp; Site Code): ${obj.ipNodeTxt}</br>`
          }
          obj.accsBdwdDes = this.form.get("ipl.accessBandwidth").value
          if (obj.accsBdwdDes != null) {
            html = `${html}Access Bandwidth (not port or service speed): ${obj.accsBdwdDes}</br>`
          }
        } else if (this.asrType.value == 5) {
          obj.mstrFtnCd = this.form.get("offnet.masterVas").value
          if (obj.mstrFtnCd != null) {
            html = `${html}What is the Master VAS M5 #: ${obj.mstrFtnCd}</br>`
          }
          obj.h1MatchMstrVasCd = (this.form.get("offnet.isH1Matched").value || "No") == "Yes" ? true : false
          if (obj.h1MatchMstrVasCd != null) {
            html = `${html}Does the H1 match the Master VAS: ${obj.h1MatchMstrVasCd}</br>`
          }
          obj.dlciDes = this.form.get("offnet.dlci").value
          if (obj.dlciDes != null) {
            html = `${html}DLCI: ${obj.dlciDes}</br>`
          }
        }

        obj.notes = html

        this.spinner.show()
        //console.log("123")
        this.asrTypeService.saveAsr(obj).subscribe(res => {
          //console.log("QWE")
          //console.log(res)
          this.helper.notify("ASR Template successfully created", Global.NOTIFY_TYPE_SUCCESS)
          this.spinner.hide()

          //if (callback != null) {
          //  callback()
          //}
        }, error => {
          console.log(error)
          this.helper.notify("ASR Template could not be created", Global.NOTIFY_TYPE_SUCCESS)
          this.spinner.hide()
          //console.log("ASD")
        }, () => {
          //console.log("ZXC")
        })
      }
    }
  }

  return() {

  }

  close() {
    this.checkin()
  }

  cancel() {
    //this.location.back()
    console.log(this.routingStateService.getHistory())
    console.log(this.routingStateService.getPreviousUrl())
  }

  checkin() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, true, 0).subscribe(
      data => {
        if (data == 1) {
          //this.lblError = "This order is currently locked. Try again later.";
          //this.LockPageControls();
          //this.hfLockedOrderID = this.orderId.toString();
        }
      });
  }
}
