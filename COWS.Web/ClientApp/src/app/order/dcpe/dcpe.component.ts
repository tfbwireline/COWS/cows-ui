import { Component, OnInit } from '@angular/core';
import { WorkGroup } from "./../../../shared/global";

@Component({
  selector: 'app-dcpe',
  templateUrl: './dcpe.component.html'
})
export class DcpeComponent implements OnInit {
  workGroup: number;

  constructor() { }  

  ngOnInit() {
    this.workGroup = WorkGroup.DCPE;
  }
}
