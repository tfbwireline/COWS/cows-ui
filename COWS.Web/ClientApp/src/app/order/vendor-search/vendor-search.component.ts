import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Helper } from "../../../shared/helper";
import { VendorOrderService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from "rxjs";

@Component({
    selector: 'app-vendor-search',
    templateUrl: './vendor-search.component.html'
})
export class VendorSearchComponent implements OnInit {
    form: FormGroup;
    notesChar = '';
    notesMaxChars = 250;
    vendorOrderSearchResults = []; // Search result
    vendorOrderId: number = 0; // Use for search parameter
    h5FolderCustomerId: number = 0;
    ftn: string = '';
    vndrCdNavigationList = [];
    vendorOrderList = [];
    vendorOrderContacts = [];
    assocH5DocumentList = [];
   

    constructor(private helper: Helper, private vendorOrderService: VendorOrderService, private spinner: NgxSpinnerService) { }

    ngOnInit() {
        this.vendorOrderId = 13677;
        
        this.form = new FormGroup({
            vendorSearchDetails: new FormGroup({
                vendorOrderNo: new FormControl({
                    value: null, disabled: false
                }),
                h5CustomerId: new FormControl({
                    value: null, disabled: false
                }),
                m5Ctn: new FormControl({
                    value: null, disabled: false
                })
            }),
            general: new FormGroup({
                vendorOrderNoGen: new FormControl({
                    value: "VON0000456760", disabled: false
                }),
                vendorNameGen: new FormControl({
                    value: "AXTEL, S.A. DE C.V.", disabled: false
                })
            }),
            vendorOrderAddress: new FormGroup({
                streetAddress1: new FormControl({
                    value: null, disabled: false
                }),
                streetAddress2: new FormControl({
                    value: null, disabled: false
                }),
                building: new FormControl({
                    value: null, disabled: false
                }),
                floor: new FormControl({
                    value: null, disabled: false
                }),
                room: new FormControl({
                    value: null, disabled: false
                }),
                city: new FormControl({
                    value: null, disabled: false
                }),
                state: new FormControl({
                    value: null, disabled: false
                }),
                country: new FormControl({
                    value: null, disabled: false
                }),
                zipPostalCode: new FormControl({
                    value: null, disabled: false
                })
            }),
            pdl: new FormGroup({
                pdlMail: new FormControl({
                    value: null, disabled: false
                })
            }),
            notesArea: new FormGroup({
                notes: new FormControl({
                    value: null, disabled: false
                })
            }),
            vendorAssociationDetails: new FormGroup({
                assocH5CustomerId: new FormControl({
                    value: null, disabled: false
                }),
                assocH5CustomerName: new FormControl({
                    value: null, disabled: false
                }),
                assocCity: new FormControl({
                    value: null, disabled: false
                }),
                assocCountry: new FormControl({
                    value: null, disabled: false
                })
            })

        })

        this.searchVendorOrder(this.vendorOrderId); //Remove
    }

    searchVendorOrder(vendorOrderId: number) {
        //this.vendorOrderService.getVendorOrder(this.vendorOrderId, 0, '')
        //    .subscribe(
        //        result => {
        //            this.vendorOrderSearchResults = result;

        //            this.vendorOrderList = this.vendorOrderSearchResults[0].vndrFoldr;

        //            //console.log("RESULT:", this.vendorOrderSearchResults);
        //            //console.log("VENDOR:", this.vendorOrderList);
        //        });
    }
}
