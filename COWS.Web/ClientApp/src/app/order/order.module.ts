import { CommonModule, DatePipe } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatExpansionModule } from '@angular/material';
import { DevExtremeModule, DxCheckBoxModule, DxDataGridModule, DxDateBoxModule, DxSelectBoxModule, DxSpeedDialActionModule } from 'devextreme-angular';
import {
  FsaOrderCustService, FsaOrdersService, H5DocService, H5FolderService, OrderAddressService, OrderContactService, OrderNoteService, OrderStdiHistoryService, OrderVlanService,
  VendorOrderDetailsService, WorkGroupService, XnciRegionService, FsaOrderCpeLineItemService, AmnciService, VendorOrderService, OrderService, TaskService, AccessMbService, AsrTypeService
} from '../../services';
import { OrderViewsComponent } from '../custom/order-views/order-views.component';
import {
  AmnciComponent, AmnciFormComponent, AnciComponent, DcpeComponent, DcpeFormComponent, EnciComponent, EquipmentReceiptComponent, EquipmentReviewComponent, GomComponent,
  MaterialRequisitionComponent, OrderCompletionComponent, OrderComponent, TechAssignmentComponent, VcpeComponent, VcpeFormComponent, M5DetailsComponent, GomFormComponent, GomFieldsComponent, NrmComponent,
  CPEReceiptsComponent, CPEReceiptsFormComponent, RTSComponent, RTSFormComponent, AdditionalCostsComponent, ThirdPartyCpeTechComponent, ThirdPartyCpeTechFormComponent, CscComponent, CscFormComponent, AsrTemplateComponent,
  SystemOrderDetailFormComponent, CustomOrderViewComponent

} from './index';
import { OrderRoutes } from './order.routes';

import { VendorSearchComponent } from './vendor-search/vendor-search.component';
import { SharedModule } from '../shared/shared-module';





@NgModule({
  imports: [
    CommonModule, ReactiveFormsModule, SharedModule,
    //Routes
    OrderRoutes,
    // DevExtreme Modules
    DxDataGridModule, DxSelectBoxModule, DxCheckBoxModule, DxDateBoxModule, DevExtremeModule, DxSpeedDialActionModule,
    // Materialize Modules
    MatExpansionModule, MatDialogModule
  ],
  providers: [
    VendorOrderDetailsService, FsaOrdersService, FsaOrderCustService, OrderAddressService, OrderContactService, OrderVlanService, H5FolderService, H5DocService, WorkGroupService,
    XnciRegionService, OrderStdiHistoryService, OrderNoteService, DatePipe, FsaOrderCpeLineItemService, AmnciService, VendorOrderService, OrderService, TaskService, AccessMbService,
    AsrTypeService
  ],
  declarations: [
    OrderComponent,
    AmnciComponent,
    AmnciFormComponent,
    VendorSearchComponent,
    OrderViewsComponent,
    AnciComponent,
    EnciComponent,
    GomComponent,
    DcpeComponent,
    DcpeFormComponent,
    VcpeComponent,
    VcpeFormComponent,
    EquipmentReviewComponent,
    MaterialRequisitionComponent,
    EquipmentReceiptComponent,
    TechAssignmentComponent,
    OrderCompletionComponent,
    M5DetailsComponent,
    GomFormComponent,
    GomFieldsComponent,
    NrmComponent,
    CPEReceiptsComponent,
    CPEReceiptsFormComponent,
    RTSComponent,
    RTSFormComponent,
    AdditionalCostsComponent,
    ThirdPartyCpeTechComponent,
    ThirdPartyCpeTechFormComponent,
    CscComponent,
    CscFormComponent,
    AsrTemplateComponent,
    SystemOrderDetailFormComponent,
    CustomOrderViewComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrderModule { }
