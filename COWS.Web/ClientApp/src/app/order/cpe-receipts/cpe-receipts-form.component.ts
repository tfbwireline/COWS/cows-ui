import { Component, OnInit, ViewChild } from '@angular/core';
import { EquipmentReceiptComponent } from '../';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Global } from '../../../shared/global';
import { Helper } from '../../../shared';
import { OrderLockService, OrderNoteService, WorkGroupService } from '../../../services';
import { DCPEService } from '../../../services/dcpe.service';
import { zip } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cpe-receipts-form',
  templateUrl: './cpe-receipts-form.component.html'
})
export class CPEReceiptsFormComponent implements OnInit {
  @ViewChild(EquipmentReceiptComponent, { static: false })
  private equipReceiptDetails: EquipmentReceiptComponent;

  form: FormGroup
  userId: number;
  userADID: string;
  workGroup: number;
  orderId: number;
  taskId: number;
  pchOrderNbr: string;
  noteList: [];
  noteInfo: string = "";
  cpeHeader: string;
  isCPEPOReceipts: boolean = false;
  isCPECustProvReceipts: boolean = false;
  locked: boolean = false;
  lblMessage: string = "";
  notes: string = "";
  deviceId: string;
  ftn: string;
  showNID: boolean = false;

  constructor(private helper: Helper, private activatedRoute: ActivatedRoute, private router: Router, private orderLockService: OrderLockService,
    private dcpeService: DCPEService, private orderNoteService: OrderNoteService, private spinner: NgxSpinnerService, private workGroupService: WorkGroupService) { }

  ngOnInit() {
    this.form = new FormGroup({
      notes: new FormControl(''),
      hfNidSerialNumber: new FormControl(),
      nidSerialNumber: new FormControl(),
      nidIp: new FormControl(),
      equipmentReview: new FormGroup({
        standardMatlReq: new FormControl(false),
        noMatlReq: new FormControl(false),
        orderType: new FormControl(),
        deviceFtn: new FormControl()
      })
    });

    this.userId = Number(localStorage.getItem('userID'));
    this.userADID = localStorage.getItem('userADID');
    this.workGroup = Number(this.activatedRoute.snapshot.params["WG"] || 1);
    this.orderId = Number(this.activatedRoute.snapshot.params["OrderID"] || 0);
    this.taskId = Number(this.activatedRoute.snapshot.params["TaskID"] || 0);
    this.pchOrderNbr = this.activatedRoute.snapshot.params["PO"];

    if (this.pchOrderNbr != 'undefined') {
      this.isCPEPOReceipts = true;
      this.cpeHeader = "CPE PO Receipts"
    }
    else {
      this.isCPECustProvReceipts = true;
      this.cpeHeader = "CPE Customer Provided Receipts"
    }

    if (this.orderId > 0) {
      this.getWGOrderData();
    }
  }

  getWGOrderData() {
    this.spinner.show();

    let data = zip(
      this.orderNoteService.getByOrderId(this.orderId),
      this.workGroupService.GetLatestNonSystemOrderNoteInfo(this.orderId),
      this.dcpeService.GetCPEOrderInfo_V5U(this.orderId),
    );

    data.subscribe(res => {
      this.noteList = res[0]
      this.noteInfo = res[1] != "" ? "Latest order notes updated by: " + res[1] : res[1]
      let orderInfo = res[2]

      if (orderInfo.length > 0) {
        this.deviceId = orderInfo.map(a => a.devicE_ID)[0];
        this.ftn = orderInfo.map(a => a.ftn)[0];
        this.form.get("nidSerialNumber").setValue(orderInfo.map(a => a.niD_SERIAL_NBR)[0]);
        this.form.get("hfNidSerialNumber").setValue(orderInfo.map(a => a.niD_SERIAL_NBR)[0]);
        this.form.get("nidIp").setValue(orderInfo.map(a => a.niD_IP)[0]);
      }

      //NID Check
      this.dcpeService.IsNIDDevice(this.orderId, this.deviceId).subscribe(data => {
        if (data) {
          this.showNID = true;
        }
      });

    }, error => {
      console.log(error)
      this.spinner.hide();
    }, () => {
      this.spinner.hide();
    });
  }

  //#region "Equipment Receipts"

  btnInsertDateWaitSCM_Click() {
    this.CompleteEquipReceiptsTask("I");
  }

  btnRemoveDateWaitSCM_Click() {
    this.CompleteEquipReceiptsTask("D");
  }

  btnNotifySCMOnly_Click() {
    this.CompleteEquipReceiptsTask("S");
  }

  btnCompleteTaskNotifySCM_Click() {
    this.CompleteEquipReceiptsTask("C")
  }

  btnAssignNIDSerialNbr_Click() {
    if (this.form.get("nidSerialNumber").value != this.form.get("hfNidSerialNumber").value) {
      this.dcpeService.isNidAlreadyUsed(this.orderId, this.getFormControlValue("nidSerialNumber")).subscribe(
        res => {
          console.log(res)
          if (res) {
            //show error
            //this.spinner.hide()
            this.helper.notify(`NID Serial Number has already been used`, Global.NOTIFY_TYPE_ERROR);
          } else {
            this.dcpeService.UpdateNIDSerialNbr(this.orderId, this.ftn, this.deviceId, this.getFormControlValue("nidSerialNumber")).subscribe(data => {
              if (data)
                this.helper.notifySavedFormMessage("NID Serial #", Global.NOTIFY_TYPE_SUCCESS, false, null);
            });
          }
        }
      )
      //this.dcpeService.UpdateNIDSerialNbr(this.orderId, this.ftn, this.deviceId, this.getFormControlValue("nidSerialNumber")).subscribe(data => {
      //  if (data)
      //    this.helper.notifySavedFormMessage("NID Serial #", Global.NOTIFY_TYPE_SUCCESS, false, null);
      //});
    }
  }

  btnCancel_Click() {
    this.orderCompleted();
  }

  CompleteEquipReceiptsTask(action: string) {

    //Check for NID Serial # and throw error
    if (this.taskId != 0) {
      if (this.showNID && this.taskId == 602) {
        if (this.getFormControlValue("nidSerialNumber") == "") {
          this.helper.notify('NID Serial # needs to be updated.', Global.NOTIFY_TYPE_WARNING);
          return false;
        }
      }
    }

    let items: any[] = [];
    let lineItemsValid: boolean = true;
    this.equipReceiptDetails.selectedEquipmentReceiptLineItemKeys.forEach((item) => {
      item.FSACPELineItemID = Number(item.fsA_CPE_LINE_ITEM_ID);
      item.QtyOrdered = Number(item.ordR_QTY);
      item.PartialQuantity = Number(item.rcvD_QTY);
      if (item.PartialQuantity > item.QtyOrdered) {
        this.helper.notify(`Partial Qty cannot be higher than Ordered Qty.`, Global.NOTIFY_TYPE_WARNING);
        lineItemsValid = false;
      }
      items.push(item);
    });

    if (lineItemsValid) {
      let orderModel = {
        OrderId: this.orderId,
        TaskId: this.taskId,
        TaskStatus: 2,
        UserId: this.userId,
        NoteText: this.getFormControlValue("notes"),
        EquipmentReceiptsLineItems: items,
        Action: action,
        UserADID: this.userADID,
        VoiceOrder: this.equipReceiptDetails.voiceOrder,
        FTN: this.equipReceiptDetails.ftn
      }

      this.dcpeService.completeEquipReceipts(orderModel).subscribe(data => {
        if (data && action != "C") {
          this.setFormControlValue("notes", "");
          this.getWGOrderData();
          this.equipReceiptDetails.LoadEquipmentReciptsData();
        }
        else if (data && action == "C") {
          this.orderCompleted();
          this.helper.notifySavedFormMessage("Equipment Receipts", Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.form.get("notes").setValue("");
        }
      },
        error => {
          this.helper.notifySavedFormMessage("Equipment Receipts", Global.NOTIFY_TYPE_ERROR, true, error);
        });
    }
  }

  orderCompleted() {
    this.orderLockService.lockUnlockOrdersEvents(this.orderId, 0, this.userId, true, true, 0).subscribe(
      data => {
        if (data == 0) {
          this.router.navigate(['order/cpe-receipts/']);
        }
      });
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }
    return value;
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }
  //#endregion
}
