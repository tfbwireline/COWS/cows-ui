import { Component, OnInit } from '@angular/core';
import { WorkGroup } from "./../../../shared/global";

@Component({
  selector: 'app-cpe-receipts',
  templateUrl: './cpe-receipts.component.html'
})
export class CPEReceiptsComponent implements OnInit {
  workGroup: number;

  constructor() { }  

  ngOnInit() {
    this.workGroup = WorkGroup.MDS;
  }
}
