import { Component, OnInit } from '@angular/core';
import { WorkGroup } from "./../../../shared/global";

@Component({
  selector: 'app-3rd-party-cpe-tech',
  templateUrl: './3rd-party-cpe-tech.component.html'
})
export class ThirdPartyCpeTechComponent implements OnInit {
  workGroup: number;

  constructor() { }  

  ngOnInit() {
    this.workGroup = WorkGroup.CPETech;
  }
}
