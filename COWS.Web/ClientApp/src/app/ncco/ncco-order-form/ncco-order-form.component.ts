import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { SearchTypes } from "./search.criteria";
import { zip, forkJoin } from "rxjs";
import { Helper } from "../../../shared";
import { Global, OldWorkGroup, NCCOProfiles, ETasks } from "../../../shared/global";
import {
  UserService,
  OrderLockService,
  NccoOrderService,
  AmnciService,
  FormControlValidationService,
} from "../../../services";
import { User, UserProfile, NccoOrder } from "../../../models";
import { String } from 'typescript-string-operations';
import { DxDataGridComponent } from "devextreme-angular";

@Component({
  selector: 'app-ncco-order-form',
  templateUrl: './ncco-order-form.component.html'
})
export class NccoOrderFormComponent implements OnInit {
  @ViewChild("gplGrid", { static: false }) gplDataGrid: DxDataGridComponent

  mainCommentCount: number = 0
  maxchar: number = 500
  isSubmitted: boolean = false;
  isRevActTask: boolean = false;
  option: any;
  profile: string = '';
  adid: string = '';
  title: string = '';
  showEditButton: boolean = true;
  nccoOrder: NccoOrder;
  productTypes: any[];
  orderTypes: any[];
  countries: any[];
  vendors: any[];
  public events: Array<string> = [];
  public selectedItemKeys: any[] = [];
  lassies: SearchTypes[]
  actionsCCD: SearchTypes[]
  rejectReasonsXov: SearchTypes[]
  actionsXov: SearchTypes[]
  rejectReasonsGov: SearchTypes[]
  actionsGov: SearchTypes[]

  sortExpression: string = "orn.NteId ASC"
  public workGroup: number = 0

  public nccoOrderInfo: any
  public current: Date
  pattern: any = "^[a-zA-Z0-9]*$";

  public id: number = 0
  public actionId: number = 0
  public userID: number = 0
  public noteTypeId: number = 0

  public h5FoldrId: number = 0
  public taskId: number = 0



  isLocked: boolean = false;
  showForm : boolean = false;
  isRTSOrderDetails : boolean = false;
  isGOV: boolean = false;
  isXOV: boolean = false;
  isGPL: boolean = false;
  isCCD: boolean = false;
  isCPEOrderDetail: boolean = false;
  showNCCODetails: boolean = true;

  showNCCOButtons: boolean = false;
  showCancelButton: boolean = false;

  plId: number = 0;
  pl = {
    id: 0,
    name: null
  }

  public selectedPlId: Number = 0;
  public plOnEdit: boolean = false;
  public MyPLNumber: Array<any> = [];

  public showNotes : boolean = false;
  public form: FormGroup;

  public rtsOrderDetailForm: FormGroup;
  public govForm: FormGroup;
  public xovForm: FormGroup;
  public gplForm: FormGroup;
  public ccdForm: FormGroup;
  public cpeOrderDetailForm: FormGroup;
  
  public userProfiles: UserProfile[];
  // public isGOMOUpdater : boolean = false;
  // public isNCIOUpdater : boolean = false;
  // public isSIPTnUCaaSOUpdater : boolean = false;

  public hasPermission: boolean = true;

  orderNote: string = '';


  constructor(private avRoute: ActivatedRoute, private router: Router,
    private helper: Helper,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private NccoOrderService: NccoOrderService,
    private userSrvc: UserService,
    private OrderLockSrvc: OrderLockService,
    private amnciService: AmnciService
  ) { 
    this.amnciService.orderCatId = 4;
    this.createForm();
    this.initializeValue();
  }

  ngOnInit() {

    this.id = this.avRoute.snapshot.params["id"] || 0;
    this.actionId = this.avRoute.snapshot.params["actionId"] || 0;
    this.userID = parseInt(localStorage.getItem('userID'));

    if(this.id > 0) {
        this.spinner.show();
        zip(   
            this.userSrvc.getUserProfilesByADID(localStorage.getItem('userADID')),
            this.NccoOrderService.getNCCOOrderData(this.id),
            this.NccoOrderService.getNCCOWGDetails(this.id),
            this.OrderLockSrvc.checkLock(this.id)
        ).subscribe(([userProfiles, ncco, nccoDetails, notes]) => {
           
            // check user role
            this.userProfiles = userProfiles as UserProfile[];

            this.nccoOrder = ncco as NccoOrder;
            
            if(this.actionId == 3) {
              if (nccoDetails.length != 0) {
                this.setformValue();
                this.h5FoldrId = nccoDetails[0].h5FoldrId;
                this.taskId = nccoDetails[0].taskId;
                if(this.actionId == 3) { // View
                  this.showform(nccoDetails[0].grpId, nccoDetails[0].taskId);
                } else if(this.actionId == 1) { // Copy
                  this.showNCCOButtons = true;
                }
              } else  {
                this.router.navigate(['/ncco/ncco-order/add']);
                // this.taskId = ETasks.BillActivationReady;
                // this.showNCCOButtons = true;
              }
            } else {
              this.setformValue();
              this.showNCCOButtons = true;
            }




            if (notes != null) {
                this.orderNote = "This order is currently locked by " + notes['lockByFullName'];
                //this.disableForm();
            }

            this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          this.helper.notify('An error occurred while retrieving the Ncco Order data.', Global.NOTIFY_TYPE_ERROR);
        },
        () => {
          this.spinner.hide();
        });
    } else {
      this.showNCCOButtons = true;
    }
  }

  private initializeValue() {

    if (this.actionId > 0) {
      this.showEditButton = true;
      if (this.actionId == 1) { 
        this.title = "Copy";
      }
      if (this.actionId == 2) { 
        this.title = "Cancel"; 
      }  
      if (this.actionId == 3) { 
        this.title = "View"; 
      }  
    } else {
      this.showEditButton = false;
    }
    this.actionsGov = [
      new SearchTypes("Approve", 1, []),
      new SearchTypes("Approve Bypass XNCI", 2, []),
    ]
    this.rejectReasonsGov = [
      new SearchTypes("GOM reject - RTS", 102, [])
    ]
    this.actionsXov = [
      new SearchTypes("Approve", 1, []),
      new SearchTypes("Reject", 2, [])
    ]
    this.rejectReasonsXov = [
      new SearchTypes("xNCI reject - RTS", 103, [])
    ]
    this.actionsCCD = [
      new SearchTypes("Complete CCD Task", 0, [])
    ]
  }

  private showform(workGroup: OldWorkGroup, taskId: ETasks) {

    const profiles = this.userProfiles;
     

    if(workGroup === OldWorkGroup.SalesSupport) {
      this.hasPermission = profiles.filter(x => x.usrPrfNme.includes('SALESSUPPORTORTS') || x.usrPrfNme.includes('SALESSUPPORTOUpdater')).length > 0;
      if(!this.hasPermission) {
        this.rtsOrderDetailForm.disable();
      }
      this.title = "Update";
      this.isRTSOrderDetails = true;
      this.form.disable();
    }
    else if(workGroup === OldWorkGroup.GOM) {
      this.hasPermission = profiles.filter(x => x.usrPrfNme.includes('GOMOUpdater')).length > 0;
      if(!this.hasPermission) {
        this.form.disable();
        this.orderNote = "User doesn't have permission to edit/update data. order details opened in read only mode.";
      }

      if(taskId === ETasks.GOMReview || taskId === ETasks.GOMCancelReady || taskId === ETasks.GOMInitiateBillingOrder) {
        this.title = "Update";
        this.isGOV = true;
        this.form.disable();
        // this.govForm.disable();
        
      } else if(taskId === ETasks.GOMCCDReady) {
        this.title = "Update";
        this.isCCD = true;
        this.form.disable();

      } else if(taskId === ETasks.GOMPL) {
        this.title = "Update";
        this.isGPL = true;
        this.form.disable();

      } else if(taskId === ETasks.GOMNCCOErrorNotification) {
        this.showNCCOButtons = true;
      }
      
    } else if(workGroup === OldWorkGroup.AMNCI || workGroup === OldWorkGroup.ANCI || workGroup === OldWorkGroup.ENCI) {
      this.hasPermission = profiles.filter(x => x.usrPrfNme.includes('NCIOUpdater')).length > 0;
      if(!this.hasPermission) {
        this.orderNote = "User doesn't have permission to edit/update data. order details opened in read only mode.";
        this.xovForm.disable();
      }

      if(taskId === ETasks.xNCIReady) {
        this.title = "Update";
        this.isXOV = true;
        this.form.disable();
      } else if(taskId == ETasks.xNCIOrderSenttoVendorDate || taskId === ETasks.xNCIOrderValidatedDate || taskId === ETasks.xNCICnclReady) {
        this.router.navigateByUrl(`order/amnci/2/${this.id}?taskId=${taskId}`);
      }
    } else if(workGroup === OldWorkGroup.SIPTnUCaaS) {
      this.hasPermission = profiles.filter(x => x.usrPrfNme.includes('SIPTnUCaaSOUpdater')).length > 0;
      if(!this.hasPermission) {
        this.cpeOrderDetailForm.disable();
        this.orderNote = "Order opened in read-only mode. ";
      }
      this.showNCCODetails = false;
      this.isCPEOrderDetail = true;

    } else if(workGroup === OldWorkGroup.System) { 
      if(taskId === ETasks.BillActivationReady || taskId === ETasks.BillActivated) {
        // Return order details
      }
    }
  }

  //#region  Form CREATE - GET - SET

    private createForm() {

      this.form = new FormGroup({
          ctn: new FormControl({ value: null, disabled: true }),
          orderType: new FormControl(),
          productType: new FormControl(),
          vendorName: new FormControl(),
          sitCountry: new FormControl(),
          sitCity: new FormControl(),
          cwd: new FormControl(),
          ccd: new FormControl(),
          custName: new FormControl(),
          h5AccountNumber: new FormControl(),
          sol: new FormControl(),
          oe: new FormControl(),
          privateLineNumber: new FormControl(),
          sotsNumber: new FormControl(),
          ordrSentLassie: new FormControl(),
          billCycleNumber: new FormControl(),
          emailNotification: new FormControl('', [FormControlValidationService.multipleEmailValidator]),
          comment: new FormControl(),
          counter: new FormControl(),
      })


      this.rtsOrderDetailForm = this.fb.group({
        nteTxt: new FormControl(),
      })

      this.govForm = this.fb.group({
          actionGov: new FormControl('', [Validators.required]),
          noteGov: new FormControl(),
          actionRejectGov: new FormControl(),
          noteRejectGov: new FormControl()
      })

      this.xovForm = this.fb.group({
          actionXov: new FormControl(),
          rejectReasonXov: new FormControl(103),
          noteXov: new FormControl()
      })

      this.gplForm = this.fb.group({
          plNumberGpl: new FormControl(),
          noteGpl: new FormControl(),
          pLNumber: new FormControl(),
          plSequenceNumber: new FormControl()
      })

      this.ccdForm = this.fb.group({
          actionCCD: new FormControl()
      })


      this.cpeOrderDetailForm = this.fb.group({
        note: new FormControl()
     })

  }

    private setformValue() {
        this.form.controls['ctn'].patchValue(this.nccoOrder.ordrId);
        this.form.controls['orderType'].patchValue(this.nccoOrder.ordrTypeId);
        this.form.controls['productType'].patchValue(this.nccoOrder.prodTypeId);
        this.form.controls['vendorName'].patchValue(this.nccoOrder.vndrCd);
        this.form.controls['sitCountry'].patchValue(this.nccoOrder.ctryCd);
        this.form.controls['cwd'].patchValue(this.nccoOrder.cwdDt);
        this.form.controls['ccd'].patchValue(this.nccoOrder.ccsDt);
        this.form.controls['custName'].patchValue(this.nccoOrder.custNme);
        this.form.controls['h5AccountNumber'].patchValue(this.nccoOrder.h5AcctNbr);
        this.form.controls['sol'].patchValue(this.nccoOrder.solNme);
        this.form.controls['oe'].patchValue(this.nccoOrder.oeNme);
        this.form.controls['privateLineNumber'].patchValue(this.nccoOrder.plNbr);
        this.form.controls['sotsNumber'].patchValue(this.nccoOrder.sotsNbr);
        this.form.controls['ordrSentLassie'].patchValue(this.nccoOrder.ordrByLassieCd == "Y" ? 1 : 2);
        this.form.controls['billCycleNumber'].patchValue(this.nccoOrder.billCycNbr);
        this.form.controls['emailNotification'].patchValue(this.nccoOrder.emailAdr);
        this.form.controls['comment'].patchValue(this.nccoOrder.cmntTxt);
    }

    private getformValue(): NccoOrder {
        let ncco = new NccoOrder();
        ncco.ordrId = this.form.controls['ctn'].value;
        ncco.ordrTypeId = this.form.controls['orderType'].value;
        ncco.prodTypeId = this.form.controls['productType'].value;
        ncco.vndrCd = this.form.controls['vendorName'].value;
        ncco.ctryCd = this.form.controls['sitCountry'].value;
        ncco.siteCityNme = this.form.controls['sitCity'].value;
        ncco.cwdDt = this.form.controls['cwd'].value;
        ncco.ccsDt = this.form.controls['ccd'].value;
        ncco.custNme = this.form.controls['custName'].value;
        ncco.h5AcctNbr = this.form.controls['h5AccountNumber'].value;
        ncco.solNme = this.form.controls['sol'].value;
        ncco.oeNme = this.form.controls['oe'].value;
        ncco.plNbr = this.form.controls['privateLineNumber'].value;
        ncco.sotsNbr = this.form.controls['sotsNumber'].value;
        ncco.ordrByLassieCd = this.form.controls['ordrSentLassie'].value  == 1 ? "Y" : "N";
        ncco.billCycNbr = this.form.controls['billCycleNumber'].value;
        ncco.emailAdr = this.form.controls['emailNotification'].value;
        ncco.cmntTxt = this.form.controls['comment'].value;

        return ncco;
    }

    //#endregion


  //#region Save Buttons

  save() {
    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    var h5 = this.form.controls["h5AccountNumber"].value != null ? this.form.controls["h5AccountNumber"].value : "";
    var ccd = this.form.controls["ccd"].value != null ? this.form.controls["ccd"].value : "";
    if (ccd != "") {
      var GivenDate = ccd;
      var CurrentDate = new Date();
      GivenDate = new Date(GivenDate);
      if (GivenDate < CurrentDate) {
        this.helper.notify('Please select future CCD date.', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }


    this.isSubmitted = true;
    var ncco = this.getformValue();
    if (this.id > 0) {
      // Update splk Event
      //console.log(splk);
      //console.log(this.splkEventTypeList.find(a => a.value == this.getFormControlValue("event.splkEventType")).description);
      //console.log(this.splkEventTypeList);
      this.spinner.show();
      ncco.ordrId = this.id;
      if (this.actionId == 1) { //Copy
        ncco.ordrId = 0;
        this.NccoOrderService.create(ncco).subscribe(res => {
          this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_SUCCESS, true, null)
        }, error => {
          this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_ERROR, false, error)
          this.spinner.hide()
        }, () => {
          this.spinner.hide();
          this.router.navigate(['/ncco/ncco-order/']);
        });

      } else if (this.actionId == 2) {  //Cancel
        this.NccoOrderService.update(this.actionId, ncco).subscribe(res => {
          this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_SUCCESS, false, null)
        }, error => {
          this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_ERROR, false, error)
          this.spinner.hide()
        }, () => {
          this.spinner.hide();
          this.router.navigate(['/ncco/ncco-order/']);
        });
      } else if (this.actionId == 3) {  //View
        ncco.creatByUserId = 111;
        this.NccoOrderService.create(ncco).subscribe(res => {
          
        }, error => {
          this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_ERROR, false, error)
          this.spinner.hide()
        }, () => {
          this.spinner.hide();
          this.router.navigate(['/ncco/ncco-order/']);
        });
      }
    }
    else {
      // Create New NCCO
      console.log(ncco);
      ncco.ordrId = 0;
      this.spinner.show();
      if (this.actionId != 1 && this.actionId != 2 && this.actionId != 3) { //Copy
        this.NccoOrderService.create(ncco).subscribe(res => {
          this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_SUCCESS, true, null)
        }, error => {
          this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_ERROR, false, error)
          this.spinner.hide()
        }, () => {
          this.spinner.hide();
          this.router.navigate(['/ncco/ncco-order/']);
        });
      }
    }
  }


  addPl() {
    const val = this.gplForm.get('plSequenceNumber').value;

    if(this.plOnEdit) {
      const index = this.MyPLNumber.findIndex(x => x.id === this.selectedPlId);
      const valIndex = this.MyPLNumber.findIndex(x => x.name === val);

      if(index === valIndex) {
        this.MyPLNumber[index].name = val;
      }
      this.gplForm.get('plSequenceNumber').patchValue('');
      this.plOnEdit = false;
      this.gplDataGrid.instance.clearSelection();
    } else {
      if(val != '') {
        if(this.MyPLNumber.findIndex(x => x.name === val) === -1) {
          const newId = this.plId++;
          this.MyPLNumber.push({ id: newId, name: val });
        }
        this.gplForm.get('plSequenceNumber').patchValue('');
      }
    }
  }
  cancelPLAdd() {
    this.plOnEdit = false;
    this.gplForm.get('plSequenceNumber').patchValue('');
    this.gplDataGrid.instance.clearSelection();
  }

  selectionChangedHandler(data: any) {
    if(data.selectedRowsData.length === 0) {
      return;
    }

    this.selectedPlId  = data.selectedRowsData[0].id;
    this.plOnEdit = true;
    this.gplForm.get('plSequenceNumber').patchValue(data.selectedRowsData[0].name);
  }

  saveGOV() {

    if (this.govForm.invalid || !this.hasPermission) {
      return;
    }
    var actionGov = this.govForm.controls['actionGov'].value;


    this.isSubmitted = true;
    var ncco = this.getformValue();

    if (this.id > 0) {
      if (this.isGOV) {  ///  Action GOV
        this.spinner.show();
        //((this.workGroup == 1) && (this.taskId == 102 || this.taskId == 108 || this.taskId == 110 || this.taskId == 111))
        //CompleteTask(int iOrderID, int iTaskID, int iTaskStatus, string sComments, int iUserID, int noteTypeID)
        ncco.ordrId = this.id;
        ncco.solNme = "GOV";                // this is just to hold a parameter which will not be saved in DB
        ncco.creatByUserId = this.taskId;   // this is just to hold a parameter which will not be saved in DB
        ncco.ordrTypeId = actionGov;        // this is just to hold a parameter which will not be saved in DB
        ncco.cmntTxt = this.govForm.controls['noteGov'].value;  // this is just to hold a parameter which will not be saved in DB
        if (this.actionId == 3) { 
          this.NccoOrderService.update(this.actionId, ncco).subscribe(res => {
            this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_SUCCESS, false, null)
          }, error => {
            this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_ERROR, false, error)
            this.spinner.hide()
          }, () => {
            this.spinner.hide();
            this.router.navigate(['/ncco/ncco-order/']);
          });
        }
      }

    }
  }

  saveXOV() {
    if(!this.hasPermission) {
      return;
    }

    if (this.xovForm.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    var actionXov = this.xovForm.controls["actionXov"].value != null ? this.xovForm.controls["actionXov"].value : "";
    var rejectReasonXov = this.xovForm.controls["rejectReasonXov"].value != null ? this.xovForm.controls["rejectReasonXov"].value : "";
    var noteXov = this.xovForm.controls["noteXov"].value != null ? this.xovForm.controls["noteXov"].value: "";
    if ((actionXov == "")) {
      this.helper.notify('Action is required', Global.NOTIFY_TYPE_WARNING);
      return;
    } else {
      if ((actionXov == 2 || actionXov == "2")) {
        if ((rejectReasonXov == "")) {
          this.helper.notify('Reject Reason is required', Global.NOTIFY_TYPE_WARNING);
          return;
        } else {
          if ((noteXov == "")) {
            this.helper.notify('Please enter Note for Rejects', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        }
      }
    }

    this.isSubmitted = true;
    var ncco = this.getformValue();
    console.log(ncco);
    console.log(this.id);
    if (this.id > 0) {
      if (this.isXOV) {  ///  Action XOV
        this.spinner.show();
        //((this.workGroup == 1) && (this.taskId == 102 || this.taskId == 108 || this.taskId == 110 || this.taskId == 111))
        //CompleteTask(int iOrderID, int iTaskID, int iTaskStatus, string sComments, int iUserID, int noteTypeID)
        ncco.ordrId = this.id;
        ncco.solNme = "XOV";  // this is just to hold a parameter which will not be saved in DB
        ncco.creatByUserId = this.taskId; // this is just to hold a parameter which will not be saved in DB
        ncco.ordrTypeId = actionXov;// this is just to hold a parameter which will not be saved in DB
        ncco.cmntTxt = this.xovForm.controls["noteXov"].value;  //// this is just to hold a parameter which will not be saved in DB
        if (this.actionId == 3) { //UPDATE
          this.NccoOrderService.update(this.actionId, ncco).subscribe(res => {
            this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_SUCCESS, false, null)
          }, error => {
            this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_ERROR, false, error)
            this.spinner.hide()
          }, () => {
            this.spinner.hide();
            this.router.navigate(['/ncco/ncco-order/']);
          });

        }
      }

    }

  }

  saveGPL() {

    if(!this.hasPermission) {
      return;
    }

    if (this.gplForm.invalid) {
      return;
    }
    var plNumberMy = this.gplForm.controls["pLNumber"].value;

    var plNumberArray = [];
    for (let i = 0; i < this.MyPLNumber.length; i++) {
      if ((this.MyPLNumber[i]).name) {
        plNumberArray.push((this.MyPLNumber[i]).name);
      }
    }

    if (plNumberArray.length === 0) {
      this.helper.notify('Please enter at least 1 Sequence Number', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.nccoOrder.PLNumber = plNumberArray;
    this.nccoOrder.MyPLNumber = plNumberMy;

    this.isSubmitted = true;
    var ncco = this.getformValue();

    ncco.PLNumber = this.nccoOrder.PLNumber;

    if (this.id > 0) {
      if (this.isGPL) {  ///  Action GPL
        this.spinner.show();
        //((this.workGroup == 1) && (this.taskId == 102 || this.taskId == 108 || this.taskId == 110 || this.taskId == 111))
        //CompleteTask(int iOrderID, int iTaskID, int iTaskStatus, string sComments, int iUserID, int noteTypeID)
        ncco.ordrId = this.id;
        ncco.solNme = "GPL";  // this is just to hold a parameter which will not be saved in DB
        ncco.creatByUserId = this.taskId; // this is just to hold a parameter which will not be saved in DB
        ncco.ordrTypeId = 0;// this is just to hold a parameter which will not be saved in DB
        ncco.cmntTxt = this.gplForm.controls["noteGpl"].value;  //// this is just to hold a parameter which will not be saved in DB
        ncco.MyPLNumber = plNumberMy;
        if (this.actionId == 3) { //UPDATE
          this.NccoOrderService.update(this.actionId, ncco).subscribe(res => {
            this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_SUCCESS, false, null)
          }, error => {
            this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_ERROR, false, error)
            this.spinner.hide()
          }, () => {
            this.spinner.hide();
            this.router.navigate(['/ncco/ncco-order/']);
          });

        }
      }

    }

  }

  saveCCD() {
    if(!this.hasPermission) {
      return;
    }

    if (this.ccdForm.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }
    var actionCCD = this.ccdForm.controls["actionCCD"].value != null ? this.ccdForm.controls["actionCCD"].value : "";
    if ((actionCCD == "")) {
      this.helper.notify('Action is required', Global.NOTIFY_TYPE_WARNING);
      return;
    }



    this.isSubmitted = true;
    var ncco = this.getformValue();
    console.log(ncco);
    if (this.id > 0) {
      if (this.isCCD) {  ///  Action CCD
        this.spinner.show();
        ncco.ordrId = this.id;
        ncco.solNme = "CCD";  // this is just to hold a parameter which will not be saved in DB
        if (this.actionId == 3) { //UPDATE
          this.NccoOrderService.update(this.actionId, ncco).subscribe(res => {
            this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_SUCCESS, false, null)
          }, error => {
            this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_ERROR, false, error)
            this.spinner.hide()
          }, () => {
            this.spinner.hide();
            this.router.navigate(['/ncco/ncco-order/']);
          });

        }
      }

    }

  }

  //#endregion

  cancelOrder() {

   // OrderID, 0, int.Parse(GetUserInfo().UserID), true, true, 0
    this.OrderLockSrvc.lockUnlockOrdersEvents(this.id, 0, this.userID, true, true, 0).subscribe(
      res => {
        // this.helper.notifySavedFormMessage("NCCO Order", Global.NOTIFY_TYPE_SUCCESS, false, null)
        this.router.navigate(['/ncco/ncco-order/']);
      },
      error => {
        this.helper.notify('An error occurred while locking the Order.', Global.NOTIFY_TYPE_ERROR);
      }
    );
    


    //Lock Order
    // this.OrderLockSrvc.lock(this.id).subscribe(
    //   res => {
    //     this.showEditButton = false;
    //     this.form.disable();
    //     this.form.controls["comment"].enable();
    //     this.mainCommentCount = this.maxchar - (this.form.controls["comment"].value == null ? 0 : this.form.controls["comment"].value.length) - 1;
    //   },
    //   error => {
    //     this.helper.notify('An error occurred while locking the Order.', Global.NOTIFY_TYPE_ERROR);
    //   }
    // );
  }

  edit() {
    //Lock Order
    this.OrderLockSrvc.lock(this.id).subscribe(
      res => {
        this.showEditButton = false;
        this.form.enable();
        this.form.controls["ctn"].disable();
        this.mainCommentCount = this.maxchar - (this.form.controls["comment"].value == null ? 0 : this.form.controls["comment"].value.length) - 1;
      },
      error => {
        this.helper.notify('An error occurred while locking the Order.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  copyOrder() {
    //Lock Order
    this.OrderLockSrvc.lock(this.id).subscribe(
      res => {
        this.showEditButton = false;
        this.form.enable();
        this.form.controls["ctn"].disable();
        this.mainCommentCount = this.maxchar - (this.form.controls["comment"].value == null ? 0 : this.form.controls["comment"].value.length) - 1;
      },
      error => {
        this.helper.notify('An error occurred while locking the Order.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  disableForm() {
    this.showEditButton = true;
    this.isLocked = true;
    this.form.disable();
  }

  cancel() {
    // if (this.id > 0 && !this.showEditButton) {
    //   this.unlockOrder();
    // }

    this.router.navigate(['/ncco/ncco-order/']);
  }


  unlockOrder() {
    // Unlock Event
    this.OrderLockSrvc.unlock(this.id).subscribe(
      res => { },
      error => {
        this.helper.notify('An error occurred while unlocking the Order.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  checkEmail(email) {
    //var regExp = /(^[a-z]([a-z_\.]*)@([a-z_\.]*)([.][a-z]{3})$)|(^[a-z]([a-z_\.]*)@([a-z_\.]*)(\.[a-z]{3})(\.[a-z]{2})*$)/i;
    var regExp = /^([a-zA-Z0-9_\.\-&])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,10})+$/;
    return regExp.test(email.trim());
  }


  checkEmails(emails) {
    var emailOk = true;
    var emailArray = emails.split(";");
    for (var i = 0; i <= (emailArray.length - 1); i++) {
      console.log(emailArray[i]);
      if (this.checkEmail(emailArray[i])) {
        //Do what ever with the email.
        emailOk = true;
      } else {
        alert("invalid email: " + emailArray[i]);
        //console.log(emailArray[i]);
        emailOk = false;
      }
    }
    return emailOk;
  }
  CheckEmailAddressWRegex(emailadr) {
    var filter = /^([a-zA-Z0-9_\.\-&])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,10})+$/;
    if (filter.test(emailadr.trim())) {
      return true;
    }
    else { return false; }
  }

  get govCommentCharRemaining(): number {
      const text = this.govForm.get('noteGov').value;
      return (text) ? 500 - text.length : 500;
  }

  get rtsCommentCharRemaining(): number {
    const text = this.rtsOrderDetailForm.get('nteTxt').value;
    return (text) ? 500 - text.length : 500;
}
  

  logEvent(eventName) {
    this.events.unshift(eventName);
  }

  onValueXOVChanged($event) {
    if (Number($event.value) == 2) {
      this.form.controls["rejectReasonXov"].enable();
    } else {
      this.form.controls["rejectReasonXov"].disable();
    }
  }

}
