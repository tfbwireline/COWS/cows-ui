import { KeyValuePair } from '../../../shared/global';

export class SearchTypes {
  description: string
  value: number
  columns: KeyValuePair[]
  
  constructor(description: string, value: number, columns: KeyValuePair[]) {
    this.description = description;
    this.value = value;
    this.columns = columns;
  }
}

//export class SearchColumns {
//  description: string
//  value: number

//  constructor(description: string, value: number) {
//    this.description = description;
//    this.value = value;
//  }
//}
