import { Component, OnInit } from '@angular/core';
import { EEventType, Global  } from "../../../shared/global";
import { NccoOrderService, UserService } from "../../../services";
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Helper } from "../../../shared";

@Component({
  selector: 'app-ncco-order',
  templateUrl: './ncco-order.component.html'
})

export class NccoOrderComponent implements OnInit {
  loggedInUser = <any>[];
  eventViewsList = <any>[];
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;
  profile: string = '';
  id: number;
  public selectedItemKeys: any[] = [];
  viewOptions: string[];
  orderViewDetailsList = <any>[];
  csgLvlId: number;
  orderStatus: string;

  constructor(
    private nccoService: NccoOrderService,
    private spinner: NgxSpinnerService,
    private userSrvc: UserService,
    private helper: Helper,
    private router: Router) {
    //this.viewOptions = [
    //  "My NCCO Orders",
    //  "Show Everything"
    //];
    //this.viewOptions = [
    //  { "id": 1, "name": "My NCCO Orders" },
    //  { "id": 2, "name": "Show Everything" }
    //]
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'NCCO_' + this.helper.formatDate(new Date());
  }

  selectionChangedHandler(data: any) {
    //console.log(data.selectedRowsData[0].csgLvlId);
    this.selectedItemKeys = data.selectedRowKeys;
    //console.log(this.selectedItemKeys);
    //console.log(data);
    //console.log(data.selectedRowsData[0].ordrId);
    this.csgLvlId = data.selectedRowsData[0].csgLvlId;
    this.orderStatus = data.selectedRowsData[0].ordrStatus;
  }

  onValueChanged($event) {
    console.log($event);
    this.getViewData($event.value);
  }

  onSubmitCopy($event) {
    this.spinner.show();
    //this.id = 11580;
    //this.router.navigate(['ncco/ncco-order/' + this.selectedItemKeys + '/' + 1]);
    this.selectedItemKeys.forEach((key) => {
      this.nccoService.passUserCSGLevel(this.csgLvlId).subscribe(
        res => {
          //console.log(res);
          if (res) this.router.navigate(['ncco/ncco-order/' + this.selectedItemKeys + '/' + 1]);
          else {
            this.helper.notify('Insufficient CSG privileges to view secured customers order data.', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        },
        error => {
          this.spinner.hide();
        });
    });
    this.spinner.hide();
  }

  onSubmitView($event) {
    
    this.spinner.show();
    //this.id = 11580;
    //this.router.navigate(['ncco/ncco-order/' + this.selectedItemKeys + '/' + 3]);
    this.selectedItemKeys.forEach((key) => {
      this.nccoService.passUserCSGLevel(this.csgLvlId).subscribe(
          res => {
          //console.log(res);
          if (res) this.router.navigate(['ncco/ncco-order/' + this.selectedItemKeys + '/' + 3]);
            else {
            this.helper.notify('Insufficient CSG privileges to view secured customers order data.', Global.NOTIFY_TYPE_WARNING);
              return;
            }
          },
          error => {
            this.spinner.hide();
          });
    });
    this.spinner.hide();
  }

  onSubmitCancel($event) {
    if (this.orderStatus != "Pending") {
      this.helper.notify('M5 #/CTN entered is in completed/disconnected. Please choose FTN/CTN which is in pending status to be Cancelled.', Global.NOTIFY_TYPE_WARNING);
      return;
    }
    this.spinner.show();
    //this.id = 11580;
    //this.router.navigate(['ncco/ncco-order/' + this.selectedItemKeys + '/' + 2]);
    this.selectedItemKeys.forEach((key) => {
      this.nccoService.passUserCSGLevel(this.csgLvlId).subscribe(
        res => {
          //console.log(res);
          if (res) this.router.navigate(['ncco/ncco-order/' + this.selectedItemKeys + '/' + 2]);
          else {
            this.helper.notify('Insufficient CSG privileges to view secured customers order data.', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        },
        error => {
          this.spinner.hide();
        });
    });
    this.spinner.hide();
  }

  getViewData(value) {
    if (value == "Show Everything") {
      this.nccoService.FindAllNccoOrder("", "").subscribe(
        data => {
          this.orderViewDetailsList = data;
        });
    } else {

      this.nccoService.FindOrdNccoOrder("","").subscribe(
        data => {
          this.orderViewDetailsList = data;
        });
    }
  }

  ngOnInit() {

    /// See code from available-Views.component.ts
    this.eventType = EEventType.SprintLink;
    this.eventJobAid = "SprintLink Event Job Aid for IPMs/Members";
    this.eventJobAidUrl = "http://webcontoo.corp.sprint.com/webcontoo/llisapi.dll/fetch/2000/1835597/1836298/14404/106652/106324/107866/106767/6074490/0069796_Sprintlink_Events_in_Comprehensive_Order_Workflow_System_COWS__%282%29.pdf?nodeid=6450694&vernum=-2";
    this.viewOptions = [
      "My NCCO Orders",
      "Show Everything"
    ];
    this.getViewData("My NCCO Orders"); 
    //let data = zip(
    //  this.nccoService.passUserCSGLevel(),
      
    //)

    //this.spinner.show();
    //data.subscribe(res => {
    //  let user = res;
    //  console.log(res);


    //}, error => {
    //  this.spinner.hide();
    //}, () => {
    //  //this.eventFailActyListsArray = (this.form.get('activators.cblFailActArray') as FormArray).controls;
    //  //this.eventSucssActyListsArray = (this.form.get('activators.cblSuccActArray') as FormArray).controls;
    //  this.spinner.hide();
    //});
  }
}

