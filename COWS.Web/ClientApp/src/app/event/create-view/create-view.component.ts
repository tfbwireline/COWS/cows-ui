import { Component, OnInit, AfterViewInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { EventViewsService } from '../../../services';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from '../../../shared';
import { Router, ActivatedRoute } from '@angular/router';
import { Global } from '../../../shared/global';
//import { EventView } from '../../../models/event-view.model';
import { CustomValidator } from '../../../shared/validator/custom-validator';
import { DxDataGridComponent } from 'devextreme-angular';

const ValidateSortByCol1: ValidatorFn = (fg: FormGroup) => {
  const selectedColIds = fg.get('selectedColIds').value;
  const sortByCol1 = fg.get('sortByCol1').value;

  if (!selectedColIds.match(sortByCol1)) {
    return { range: true }
  }
  else {
    return null
  }
};

const ValidateSortByCol2: ValidatorFn = (fg: FormGroup) => {
  const selectedColIds = fg.get('selectedColIds').value;
  const sortByCol2 = fg.get('sortByCol2').value;

  if (!selectedColIds.match(sortByCol2)) {
    return { range: true }
  }
  else {
    return null
  }
};

const ValidateFilterCol1: ValidatorFn = (fg: FormGroup) => {
  const selectedColIds = fg.get('selectedColIds').value;
  const rbShowAllFilter = fg.get('rbShowAllFilter').value;
  const filterCol1 = fg.get('filterCol1').value;

  if (!selectedColIds.match(filterCol1) && rbShowAllFilter) {
    return { range: true }
  }
  else {
    return null
  }
};

const ValidateFilterCol2: ValidatorFn = (fg: FormGroup) => {
  const selectedColIds = fg.get('selectedColIds').value;
  const rbShowAllFilter = fg.get('rbShowAllFilter').value;
  const filterCol2 = fg.get('filterCol2').value;

  if (!selectedColIds.match(filterCol2) && rbShowAllFilter) {
    return { range: true }
  }
  else {
    return null
  }
};

@Component({
  selector: 'app-create-view',
  templateUrl: './create-view.component.html'
})
export class CreateViewComponent implements OnInit, AfterViewInit {

  @ViewChild('columnGrid', { static: false }) columnGrid: DxDataGridComponent;

  invalidColumns = [];

  form: FormGroup
  availableViewColumnsList: any
  eventView: any = {};
  eventTypeId: any
  selectedViewId: any
  btnSubmitVisible: boolean
  btnUpdateVisible: boolean
  eventViewInfo: any
  eventViewColInfo: any = null;
  filterOperators: any
  logicOperators: any
  displayViewName: string = ''
  sortByCol1: number
  sortByCol2: number
  sortCol1Order: boolean = true;
  sortCol2Order: boolean = true;
  disableCodeFilter: boolean;
  rbShowAllFilter: boolean = false;
  filterCol1: number
  filterCol2: number
  filterOperator1: string
  filterOperator2: string
  filterLogicOperator: string
  txtFilterVal1: string = ''
  txtFilterVal2: string = ''
  selectedColIds: string = ''
  selectedColPos: string = ''
  selectedDsplVwColKeys: string = '';
  inValidSortByCol1: boolean;
  inValidSortByCol2: boolean;
  inValidFilterCol1: boolean;
  inValidFilterCol2: boolean;

  public assignmentTypeOptions = [
    { "id": 0, "name": "--" },
    { "id": 1, "name": "1" },
    { "id": 2, "name": "2" },
    { "id": 3, "name": "3" },
    { "id": 4, "name": "4" },
    { "id": 5, "name": "5" },
    { "id": 6, "name": "6" },
    { "id": 7, "name": "7" },
    { "id": 8, "name": "8" },
    { "id": 9, "name": "9" },
    { "id": 10, "name": "10" },
    { "id": 11, "name": "11" },
    { "id": 12, "name": "12" },
    { "id": 13, "name": "13" },
    { "id": 14, "name": "14" },
    { "id": 15, "name": "15" },
    { "id": 16, "name": "16" },
    { "id": 17, "name": "17" },
    { "id": 18, "name": "18" },
    { "id": 19, "name": "19" },
    { "id": 20, "name": "20" },
    { "id": 21, "name": "21" },
    { "id": 22, "name": "22" },
    { "id": 23, "name": "23" },
    { "id": 24, "name": "24" },
    { "id": 25, "name": "25" }
  ];

  constructor(private fb: FormBuilder, private avRoute: ActivatedRoute, private helper: Helper, private spinner: NgxSpinnerService, private eventService: EventViewsService, private router: Router) {
    this.inValidSortByCol1 = false;
    this.inValidSortByCol2 = false;
    this.inValidFilterCol1 = false;
    this.inValidFilterCol2 = false;
  }
  ngAfterViewInit() {
    // this.setSelectedRows();
  }

  ngOnInit() {    
    this.form = this.fb.group({
      //numberInput: ['', [Validators.required, CustomValidator.numberValidator]]
      displayViewName: ['', [Validators.required]],
      selectedColIds: [''],
      rbShowAllFilter: [''],
      sortByCol1: [''],
      sortByCol2: [''],
      filterCol1: [''],
      filterCol2: [''],
      txtFilterVal1: [''],
      txtFilterVal2: ['']
    }, { validator: [ValidateSortByCol1, ValidateSortByCol2, ValidateFilterCol1, ValidateFilterCol2] });

    this.enableFilter(0);
    this.eventTypeId = this.avRoute.snapshot.params["eventTypeId"];
    this.selectedViewId = this.avRoute.snapshot.params["selectedViewId"];

    this.eventService.GetFilterOperators().toPromise().then(
      data => {
        this.filterOperators = data;
        this.filterOperator1 = this.filterOperator2 = data[0].filtrOprId;

        this.eventService.GetLogicOperators().toPromise().then(
          data => {
            this.logicOperators = data;
            this.filterLogicOperator = data[0].filtrOprId;

            this.getEventViewColumns(this.eventTypeId);

          });
      }); 
  }



  getEventViewColumns(siteCntnt: number) {
    this.eventService.GetEventViewInfo(this.selectedViewId).toPromise().then(
      eventViewData => {
        this.eventViewInfo = eventViewData.table;
        this.eventViewColInfo = eventViewData.table1;

        this.eventService.getEventViewColumns(0, siteCntnt).toPromise().then(
          data => {
            this.availableViewColumnsList = data.map(record => {
              const colInfo = this.eventViewColInfo.filter(x => x.dspL_COL_ID === record.dspL_COL_ID);
              if (colInfo.length > 0) {
                record['dspL_POS'] = colInfo[0]['dspL_POS_FROM_LEFT_NBR'];
              }
              return record;
            });
            this.filterCol1 = this.filterCol2 = this.sortByCol1 = this.sortByCol2 = data[0].dspL_COL_ID;

            if (this.selectedViewId != 0) {
              this.btnSubmitVisible = false;
              this.btnUpdateVisible = true;
              //this.displayViewName = this.eventViewInfo[0].dspL_VW_NME;
              this.form.get('displayViewName').setValue(this.eventViewInfo[0].dspL_VW_NME);
              this.sortByCol1 = this.eventViewInfo[0].sorT_BY_COL_1_ID;
              this.sortByCol2 = this.eventViewInfo[0].sorT_BY_COL_2_ID
              this.sortCol1Order = this.eventViewInfo[0].sorT_BY_COL_1_ASC_ORDR_CD == "Y" ? true : false;
              this.sortCol2Order = this.eventViewInfo[0].sorT_BY_COL_2_ASC_ORDR_CD == "Y" ? true : false;

              if (this.eventViewInfo[0].filtR_ON_CD == "Y") {
                this.enableFilter(1);
                this.rbShowAllFilter = true;
                this.form.get('rbShowAllFilter').setValue(true);

                this.filterCol1 = this.eventViewInfo[0].filtR_COL_1_ID;
                this.filterCol2 = this.eventViewInfo[0].filtR_COL_2_ID;
                this.filterOperator1 = this.eventViewInfo[0].filtR_COL_1_OPR_ID;
                this.filterOperator2 = this.eventViewInfo[0].filtR_COL_2_OPR_ID;
                this.form.get('txtFilterVal1').setValue(this.eventViewInfo[0].filtR_COL_1_VALU_TXT);
                this.form.get('txtFilterVal2').setValue(this.eventViewInfo[0].filtR_COL_2_VALU_TXT);
                //this.txtFilterVal1 = this.eventViewInfo[0].filtR_COL_1_VALU_TXT;
                //this.txtFilterVal2 = this.eventViewInfo[0].filtR_COL_2_VALU_TXT;
                this.filterLogicOperator = this.eventViewInfo[0].filtR_LOGIC_OPR_ID;
              }
              else {
                this.rbShowAllFilter = false;
                this.form.get('rbShowAllFilter').setValue(false);
                this.enableFilter(0);
              }
            }
            else {
              this.btnSubmitVisible = true;
              this.btnUpdateVisible = false;
            }
          });


      });
  }

  enableFilter(filter: number) {
    if (filter == 0) {
      this.disableCodeFilter = true;
      this.form.get('filterCol1').disable();
      this.form.get('txtFilterVal1').disable();
      this.form.get('filterCol2').disable();
      this.form.get('txtFilterVal2').disable();
      this.rbShowAllFilter = false;
      this.form.get('rbShowAllFilter').setValue(false);
    }
    else {
      this.disableCodeFilter = false;
      this.form.get('filterCol1').enable();
      this.form.get('txtFilterVal1').enable();
      this.form.get('filterCol2').enable();
      this.form.get('txtFilterVal2').enable();
      this.rbShowAllFilter = true;
      this.form.get('rbShowAllFilter').setValue(true);
    }
  }

  cancelClick(value) {
    if (value == 1)
      this.router.navigate(['event/access-delivery']);
    else if (value == 2)
      this.router.navigate(['event/ngvn']);
    else if (value == 3)
      this.router.navigate(['event/mpls']);
    else if (value == 4)
      this.router.navigate(['event/sprint-link']);
    else if (value == 5)
      this.router.navigate(['event/mds']);
    else if (value == 9)
      this.router.navigate(['event/fedline']);
    else if (value == 10)
      this.router.navigate(['event/sipt']);
    else if (value == 19)
      this.router.navigate(['event/ucaas']);
  }

  sortByCol1Changed(value) {
    if (this.selectedColIds.match(value)) {
      this.inValidSortByCol1 = false;
      this.sortByCol1 = value;
    }
    else {
      this.inValidSortByCol1 = true;
    }
  }

  sortByCol2Changed(value) {
    if (this.selectedColIds.match(value)) {
      this.inValidSortByCol2 = false;
      this.sortByCol2 = value;
    }
    else {
      this.inValidSortByCol2 = true;
    }
  }

  filterCol1Changed(value) {
    if (this.selectedColIds.match(value)) {
      this.inValidFilterCol1 = false;
      this.filterCol1 = value;
    }
    else {
      this.inValidFilterCol1 = true;
    }
  }

  filterOperator1Changed(value) {
    this.filterOperator1 = value;
  }

  filterLogicChanged(value) {
    this.filterLogicOperator = value;
  }

  filterCol2Changed(value) {
    if (this.selectedColIds.match(value)) {
      this.inValidFilterCol2 = false;
      this.filterCol2 = value;
    }
    else {
      this.inValidFilterCol2 = true;
    }
  }

  filterOperator2Changed(value) {
    this.filterOperator2 = value;
  }

  updateSortColumn1Asc(order: number) {
    if (order == 0) {
      this.sortCol1Order = false;
    }
    else {
      this.sortCol1Order = true;
    }
  }

  updateSortColumn2Asc(order: number) {
    if (order == 0) {
      this.sortCol2Order = false;
    }
    else {
      this.sortCol2Order = true;
    }
  }

  submitClick() {

    this.getColumnAndPositions();
    if(this.invalidColumns.length > 0) {
      this.helper.notify(`Please select Position for column(s): ${this.invalidColumns.join()}`, Global.NOTIFY_TYPE_ERROR);
      return;
    }

    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.spinner.show();
    this.eventView.userId = Number(localStorage.getItem('userID'));
    this.eventView.dsplVwNme = this.form.get('displayViewName').value;//this.displayViewName;
    this.eventView.siteCntntId = this.eventTypeId;
    this.eventView.dfltVwCd = "N";
    this.eventView.pblcVwCd = "N";
    this.eventView.filtrCol1OprId = this.filterOperator1;
    this.eventView.filtrCol2OprId = this.filterOperator2;
    this.eventView.filtrCol1Id = this.filterCol1;
    this.eventView.filtrCol2Id = this.filterCol2;
    this.eventView.filtrCol1ValuTxt = this.form.get('txtFilterVal1').value;//this.txtFilterVal1;
    this.eventView.filtrCol2ValuTxt = this.form.get('txtFilterVal2').value;//this.txtFilterVal2;
    this.eventView.filtrOnCd = this.form.get('rbShowAllFilter').value ? "Y" : "N";
    this.eventView.sortByCol1Id = this.sortByCol1;
    this.eventView.sortByCol1AscOrdrCd = this.sortCol1Order ? "Y" : "N";
    this.eventView.sortByCol2Id = this.sortByCol2;
    this.eventView.sortByCol2AscOrdrCd = this.sortCol2Order ? "Y" : "N";
    this.eventView.filtrLogicOprId = this.filterLogicOperator;
    this.eventView.colIDs = this.selectedColIds;
    this.eventView.colPos = this.selectedColPos;

    this.eventService.CreateView(this.eventView).toPromise().then(
      data => {
        if (data) {
          this.helper.notify(`Successfully Created New View`, Global.NOTIFY_TYPE_SUCCESS);
          setTimeout(() => {
            this.spinner.hide();
          }, 0);
          this.cancelClick(this.eventTypeId);
        }
      },
      error => {
        this.helper.notify(`Failed to Create New View. ` + error.message, Global.NOTIFY_TYPE_ERROR);
        this.spinner.hide();
      });
  }

  updateClick() {
  
    this.getColumnAndPositions();
    if(this.invalidColumns.length > 0) {
      this.helper.notify(`Please select Position for column(s): ${this.invalidColumns.join()}`, Global.NOTIFY_TYPE_ERROR);
      return;
    }
    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.spinner.show();
    this.eventView.dsplVwId = this.selectedViewId;
    this.eventView.userId = Number(localStorage.getItem('userID'));
    this.eventView.dsplVwNme = this.form.get('displayViewName').value;
    this.eventView.siteCntntId = this.eventTypeId;
    this.eventView.dfltVwCd = "N";
    this.eventView.pblcVwCd = "N";
    this.eventView.filtrCol1OprId = this.filterOperator1;
    this.eventView.filtrCol2OprId = this.filterOperator2;
    this.eventView.filtrCol1Id = this.filterCol1;
    this.eventView.filtrCol2Id = this.filterCol2;
    this.eventView.filtrCol1ValuTxt = this.form.get('txtFilterVal1').value;
    this.eventView.filtrCol2ValuTxt = this.form.get('txtFilterVal2').value
    this.eventView.filtrOnCd = this.form.get('rbShowAllFilter').value ? "Y" : "N";
    this.eventView.sortByCol1Id = this.sortByCol1;
    this.eventView.sortByCol1AscOrdrCd = this.sortCol1Order ? "Y" : "N";
    this.eventView.sortByCol2Id = this.sortByCol2;
    this.eventView.sortByCol2AscOrdrCd = this.sortCol2Order ? "Y" : "N";
    this.eventView.filtrLogicOprId = this.filterLogicOperator;
    this.eventView.colIDs = this.selectedColIds;
    this.eventView.colPos = this.selectedColPos;
    //this.selectedViewId ,Number(localStorage.getItem('userID')), this.displayViewName, this.eventTypeId, "N", "N", this.filterOperator1, this.filterOperator2, this.filterCol1, this.filterCol2, this.txtFilterVal1, this.txtFilterVal2, this.rbShowAllFilter, this.sortByCol1, this.sortCol1Order, this.sortByCol2, this.sortCol2Order, this.filterLogicOperator, this.selectedColIds, this.selectedColPos
    this.eventService.UpdateView(this.selectedViewId, this.eventView).toPromise().then(
      data => {
        if (data) {
          this.helper.notify(`Successfully Updated View for ${this.form.get('displayViewName').value}`, Global.NOTIFY_TYPE_SUCCESS);
          setTimeout(() => {
            this.spinner.hide();
          }, 0);
          this.cancelClick(this.eventTypeId);
        }
        else {
          this.helper.notify(`Failed to Updated View for ${this.form.get('displayViewName').value}`, Global.NOTIFY_TYPE_ERROR);
        }
      },
      error => {
        this.helper.notify(`Failed to Updated View for ${this.form.get('displayViewName').value}` + error.message, Global.NOTIFY_TYPE_ERROR);
        this.spinner.hide();
      });
  }

  dsplVwColSelectionChangedHandler(info) {

    if (this.selectedColIds == "") {
      this.selectedColIds = info.selectedRowKeys + ',';
    }
    else {

      let selectedArr = this.selectedColIds.split(',');
      selectedArr = selectedArr.filter(item => item);

      let selectedColPosArr = this.selectedColPos.split(',');
      selectedColPosArr = selectedColPosArr.filter(item => item);

      if(info.currentSelectedRowKeys.length > 0) {
        const value  = info.currentSelectedRowKeys[0];
        if(selectedArr.indexOf(value.toString()) === - 1) {
          selectedArr.push(info.currentSelectedRowKeys[0]);
          this.selectedColIds = selectedArr.join();
        }
      } else if (info.currentDeselectedRowKeys.length > 0) {
        const value = info.currentDeselectedRowKeys[0];
        const index = selectedArr.indexOf(value.toString());

        selectedArr.splice(index, 1);
        

        this.selectedColIds = selectedArr.join();
      }

      // this.selectedColIds = this.selectedColIds + ',' + info.currentSelectedRowKeys;
    }
    this.form.get('selectedColIds').setValue(this.selectedColIds);
  }

  dsplColPosSelectionChange(info) {
    if (this.selectedColPos == "")
      this.selectedColPos = info.value + ',';
    else {
      if(info.value != 0) {
        this.selectedColPos = this.selectedColPos + ',' + info.value;
      }
    }
  }

  selectDsplVwCols(info) {
    if (info.rowType === 'data') {      
      if (this.eventViewColInfo != null && this.eventViewColInfo.find(a => a.dspL_COL_ID === info.data.dspL_COL_ID)) {
        info.cells[0].value = true;
        let dsplPos = this.eventViewColInfo.find(a => a.dspL_COL_ID === info.data.dspL_COL_ID).dspL_POS_FROM_LEFT_NBR;
        info.cells[2].value = dsplPos;
        this.selectedColIds = this.selectedColIds + ',' + info.key;
        this.selectedColPos = this.selectedColPos + ',' + dsplPos;
      }
      this.form.get('selectedColIds').setValue(this.selectedColIds);
    }
  }


  getColumnAndPositions() {

    //const record = this.availableViewColumnsList;
    const data = this.columnGrid.instance.getSelectedRowsData();

    let columns = [];
    let positions = [];

    this.invalidColumns = [];
    for(let i = 0; i < data.length; i ++) {
      
      const record  = data[i];

      if(record['dspL_POS'] > 0) {
        columns.push(record['dspL_COL_ID']);
        positions.push(record['dspL_POS']);
      } else {
        this.invalidColumns.push(record['dspL_COL_NME']);
      }
    }
    this.selectedColIds = columns.join();
    this.selectedColPos = positions.join();

    this.form.get('selectedColIds').setValue(this.selectedColIds);
  }

  setSelectedRows() {
    if(this.eventViewColInfo) {
      const activeKeys = this.eventViewColInfo.map(x => x.dspL_COL_ID);
      this.columnGrid.instance.selectRows(activeKeys, true);
      this.form.get('selectedColIds').setValue(activeKeys.join());
    }
  }
}
