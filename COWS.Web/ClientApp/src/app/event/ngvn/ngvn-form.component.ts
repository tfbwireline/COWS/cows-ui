import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, of } from "rxjs";
import { Helper } from "./../../../shared";
import { Global, EVENT_STATUS, WORKFLOW_STATUS, KeyValuePair, RB_CONDITION, RB_CableProtocolType, EEventType, EEventProfile } from "./../../../shared/global";
import { UserService, NgvnEventService, WorkflowService, EventAsnToUserService, EventLockService } from "./../../../services/";
import { User, EnhncSrvc, Workflow, NgvnEvent, NgvnEventCktIdNua, NgvnEventSiptTrnk, EventAsnToUser, UserProfile } from "./../../../models";
import { NgvnProdType } from '../../../models/ngvn-prodtype-model';
import { CustomValidator } from '../../../shared/validator/custom-validator';
import { concatMap } from 'rxjs/operators';
import { StringBuilder } from 'typescript-string-operations';

@Component({
  selector: 'app-ngvn-form',
  templateUrl: './ngvn-form.component.html'
})
export class NgvnFormComponent implements OnInit {
  // Page properties
  eventId: number = 0
  profile: string;
  eventType: number = EEventType.NGVN;
  ngvnEvent: NgvnEvent;
  viewActivators: boolean = false;
  allowEdit: boolean = true;
  editMode: boolean = true;
  isLocked: boolean = false;
  lockedByUser: string;
  isRevActTask: boolean = false;
  isActivator: boolean = false;
  isReviewer: boolean = false;
  isMember: boolean = false;
  selectedProdType: string;

  // For UI Controls
  workflowStatusList: Workflow[] = []
  eventStatusList: KeyValuePair[]
  ngvnProdTypeList: NgvnProdType[]
  circuitList: NgvnEventCktIdNua[] = []
  relatedCompassList: KeyValuePair[];
  cableProtocolTypeList: KeyValuePair[];
  siptTrunkList: NgvnEventSiptTrnk[] = []
  activators: EventAsnToUser[];

  // For quick accessing control
  form: FormGroup
  option: any
  isSubmitted: boolean = false;
  get h1() { return this.form.get("h1") }
  get workflowStatus() { return this.form.get("workflowStatus") }
  get publishedEmailCC() { return this.form.get("design.publishedEmailCC") }
  get completeEmailCC() { return this.form.get("design.completeEmailCC") }
  get comments() { return this.form.get("event.comments") }
  get gsrConfiguration() { return this.form.get("event.gsrConfiguration") }
  get cableProtocolType() { return this.form.get("event.cableProtocolType") }
  get sbcPair() { return this.form.get("event.sbcPair") }
  get relatedCompassNcr() { return this.form.get("event.relatedCompassNcr") }
  get cableAddDueDate() { return this.form.get("event.cableAddDueDate") }
  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute, private userService: UserService,
    private ngvnEventService: NgvnEventService, private workflowService: WorkflowService,
    private router: Router, private eventAsnToUserService: EventAsnToUserService, private evntLockSrvc: EventLockService
  ) { }

  ngOnDestroy() {
    this.unlockEvent();
    this.helper.form = null;
  }

  ngOnInit() {
    this.eventId = this.activatedRoute.snapshot.params["id"] || 0;
    //this.editMode = this.eventId > 0 ? false : true;

    this.form = new FormGroup({
      itemTitle: new FormControl({ value: null, disabled: true }),
      eventDescription: new FormControl(),
      eventID: new FormControl({ value: null, disabled: true }),
      eventStatus: new FormControl({ value: EVENT_STATUS.Visible, disabled: true }),
      ngvnProdType: new FormControl(7),
      m5_no: new FormControl({ value: null, disabled: true }),
      h1: new FormControl(null, Validators.compose([Validators.required, CustomValidator.h1Validator])),
      h6: new FormControl(),
      charsID: new FormControl(),
      workflowStatus: new FormControl({ value: WORKFLOW_STATUS.Visible }),
      customer: new FormGroup({
        name: new FormControl(),
        email: new FormControl(),
        phone: new FormControl(),
        cellphone: new FormControl(),
        contactName: new FormControl(),
        pager: new FormControl(),
        pagerPin: new FormControl()
      }),
      requestor: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        cellphone: new FormControl(),
        email: new FormControl(),
        pager: new FormControl(),
        pagerPin: new FormControl(),
        bridgeNumber: new FormControl(),
        bridgePin: new FormControl(),
        eventComments: new FormControl()
      }),
      sales: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        email: new FormControl()
      }),
      design: new FormGroup({
        publishedEmailCC: new FormControl('', Validators.compose([CustomValidator.emailValidator])),
        completeEmailCC: new FormControl('', Validators.compose([CustomValidator.emailValidator]))
      }),
      schedule: new FormGroup({
        startDate: new FormControl(),
        endDate: new FormControl({ value: null, disabled: true }),
        eventDuration: new FormControl(60),
        extraDuration: new FormControl(0),
        assignedToId: new FormControl(),
        assignedTo: new FormControl(),
        displayedAssignedTo: new FormControl()
      }),
      event: new FormGroup({
        comments: new FormControl('', Validators.required),
        gsrConfiguration: new FormControl('', Validators.required),
        cableAddDueDate: new FormControl({ value: new Date().setHours(new Date().getHours(), 0, 0, 0), disabled: false }, Validators.required),
        cableProtocolType: new FormControl({ value: null, disabled: false }, Validators.required),
        sbcPair: new FormControl('', Validators.required),
        relatedCompassNcr: new FormControl({ value: null, disabled: false }, Validators.required),
        relatedCompassNcrNme: new FormControl(),
        isEscalation: new FormControl(false),
      }),
      reviewer: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        comments: new FormControl()
      }),
      activators: new FormGroup({
        successfulActivities: new FormArray([]),
        selectedSuccessfulActs: new FormControl(),
        failedActivities: new FormArray([]),
        selectedFailedActs: new FormControl(),
        failedCode: new FormControl(),
        preconfigureCompleted: new FormControl(),
        comments: new FormControl()
      }),
    });

    this.option = {
      customer: {
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Customer Name is required" }
          ]
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        contactName: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Contact Name is required" }
          ]
        },
        pager: {
          isShown: true
        },
        pagerPin: {
          isShown: true
        },
      },
      requestor: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["cellphone", "cellPhnNbr"],
            ["email", "emailAdr"],
            ["pager", "pgrNbr"],
            ["pagerPin", "pgrPinNbr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for requestor" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Requestor is required" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        },
        pager: {
          isShown: true
        },
        pagerPin: {
          isShown: true
        },
        bridgeNumber: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Conference Bridge Number is required" }
          ]
        },
        bridgePin: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Conference Bridge Pin is required" }
          ]
        }
      },
      sales: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["email", "emailAdr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for sales" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Sales Engineer name is required" }
          ]
        },
        phone: {
          isShown: true
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        }
      },
      design: {
        userFinderForPublishedEmail: {
          isEnabled: true,
          searchQuery: "publishedEmailCC",
          mappings: [
            ["publishedEmailCC", "emailAdr"]
          ]
        },
        userFinderForCompleteEmail: {
          isEnabled: true,
          searchQuery: "completeEmailCC",
          mappings: [
            ["completeEmailCC", "emailAdr"]
          ]
        },
      },
      event: {
        isEscalation: {
          isShown: true
        }
      },
      schedule: {
        userFinder: {
          isEnabled: false,
          searchQuery: "displayedAssignedTo",
          mappings: [
            ["assignedToId", "userId"],
            ["assignedTo", "dsplNme"],
            ["displayedAssignedTo", "dsplNme"]
          ]
        },
        softAssign: {
          isShown: false
        },
        checkCurrentSlot: {
          isShown: false
        },
        launchPicker: {
          isShown: false
        }
      },
      lookup_m5: {
        isEnabled: true,
        searchQuery: "m5_no",
        mappings: [
          ["h1", "h1"],
          ["h6", "h6"],
          ["customer.name", "custNme"],
          ["customer.email", "custEmailAdr"],
          ["customer.cellphone", "custCntctCellPhnNbr"],
          ["customer.phone", "custCntctPhnNbr"],
          ["customer.contactName", "custCntctNme"],
          ["customer.pager", "custCntctPgrNbr"],
          ["customer.pagerPin", "custCntctPgrPinNbr"]
        ]
      },
      reviewer: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for reviewer" }
          ]
        },
        name: {
          validators: [
            { type: Validators.required, name: "required", message: "Reviewer is required" }
          ]
        }
      },
      activator: {
        failedCode: {
          isShown: false
        }
      }
    }

    // Set the helper form to this form
    this.helper.form = this.form;
    this.spinner.show();

    // Initial list data
    this.eventStatusList = this.helper.getEnumKeyValuePair(EVENT_STATUS);
    this.relatedCompassList = this.helper.getEnumKeyValuePair(RB_CONDITION);
    this.cableProtocolTypeList = this.helper.getEnumKeyValuePair(RB_CableProtocolType);

    let data = zip(
      this.userService.getLoggedInUser(),
      this.workflowService.getForEvent(this.eventType, this.ngvnEvent == null ? EVENT_STATUS.Visible : this.ngvnEvent.eventStusId),
      this.userService.getFinalUserProfile(this.userService.loggedInUser.adid, this.eventType),
      this.ngvnEventService.getNgvnProdType()
    )

    data.subscribe(res => {
      let user = res[0] as User;
      this.helper.setFormControlValue("requestor.id", user.userId);
      this.helper.setFormControlValue("requestor.name", user.fullNme);
      this.helper.setFormControlValue("requestor.email", user.emailAdr);
      this.helper.setFormControlValue("requestor.phone", user.phnNbr);
      this.helper.setFormControlValue("requestor.cellphone", user.cellPhnNbr);
      this.helper.setFormControlValue("requestor.pager", user.pgrNbr);
      this.helper.setFormControlValue("requestor.pagerPin", user.pgrPinNbr);
      this.workflowStatusList = (res[1] as Workflow[]);
      var usrPrf = res[2] as UserProfile;
      this.ngvnProdTypeList = res[3] as NgvnProdType[];

      if (usrPrf != null) {
        this.profile = this.helper.getFinalProfileName(usrPrf);
        this.isMember = this.profile == EEventProfile.MEMBER ? true : false;
        this.isReviewer = this.profile == EEventProfile.REVIEWER ? true : false;
        this.isActivator = this.profile == EEventProfile.ACTIVATOR ? true : false;
        if(this.isMember && !this.isReviewer) {
          this.form.get("schedule.eventDuration").disable();
        }
        this.helper.setFormControlValue("workflowStatus",
        this.profile == EEventProfile.REVIEWER ? WORKFLOW_STATUS.Submit : WORKFLOW_STATUS.Visible);
      }
    }, error => {
      this.spinner.hide();
    }, () => {
      // Get NGVN Event Data to view or update
      if (this.eventId > 0) {
        this.ngvnEventService.getById(this.eventId).subscribe(
          res => {
            this.ngvnEvent = res;
            this.circuitList = this.ngvnEvent.ngvnEventCktIdNua;
            if (this.ngvnEvent.cablPrcolTypeNme == "SIP")
              this.siptTrunkList = this.ngvnEvent.ngvnEventSipTrnk

          }, error => {
            this.spinner.hide();
            this.helper.notify('An error occurred while retrieving the NGVN Event data.',
              Global.NOTIFY_TYPE_ERROR);
          }, () => {
            this.setNgvnEventFormValue();
            this.disableForm();
            this.setNgvnEventFormFields();
            this.spinner.hide();

            // Check EventLock
            //this.evntLockSrvc.checkLock(this.eventId).subscribe(
            //  res => {
            //    if (res != null) {
            //      this.lockedByUser = res.lockByFullName;
            //      this.isLocked = true;
            //    }
            //  },
            //  error => {
            //    this.spinner.hide();
            //    this.helper.notify('An error occurred while retrieving Event Lock status.',
            //      Global.NOTIFY_TYPE_ERROR);
            //  },
            //  () => {
            //    this.spinner.hide();
            //  }
            //);
          }
        );
      } else {
        this.setNgvnEventFormFields();
        this.spinner.hide();
        this.disableForm();
      }
      });
  }

  setNgvnEventFormValue() {
    if (this.eventId > 0 && this.ngvnEvent != null) {
      this.setFormControlValue("itemTitle", this.ngvnEvent.eventTitleTxt)
      this.setFormControlValue("eventDescription", this.ngvnEvent.eventDes)
      this.setFormControlValue("eventID", this.ngvnEvent.eventId)
      this.setFormControlValue("eventStatus", this.ngvnEvent.eventStusId)
      this.setFormControlValue("ngvnProdType", this.ngvnEvent.ngvnProdTypeId)
      this.setFormControlValue("m5_no", this.ngvnEvent.ftn)
      this.setFormControlValue("h1", this.ngvnEvent.h1)
      this.setFormControlValue("h6", this.ngvnEvent.h6)
      this.setFormControlValue("charsID", this.ngvnEvent.charsId)

      this.setFormControlValue("customer.name", this.ngvnEvent.custNme)
      this.setFormControlValue("customer.email", this.ngvnEvent.custEmailAdr)
      this.setFormControlValue("customer.phone", this.ngvnEvent.custCntctPhnNbr)
      this.setFormControlValue("customer.cellphone", this.ngvnEvent.custCntctCellPhnNbr)
      this.setFormControlValue("customer.contactName", this.ngvnEvent.custCntctNme)
      this.setFormControlValue("customer.pager", this.ngvnEvent.custCntctPgrNbr)
      this.setFormControlValue("customer.pagerPin", this.ngvnEvent.custCntctPgrPinNbr)

      this.setFormControlValue("requestor.id", this.ngvnEvent.reqorUser.userId)
      this.setFormControlValue("requestor.name", this.ngvnEvent.reqorUser.fullNme)
      this.setFormControlValue("requestor.email", this.ngvnEvent.reqorUser.emailAdr)
      this.setFormControlValue("requestor.cellphone", this.ngvnEvent.reqorUser.cellPhnNbr)
      this.setFormControlValue("requestor.phone", this.ngvnEvent.reqorUser.phnNbr)
      this.setFormControlValue("requestor.pager", this.ngvnEvent.reqorUser.pgrNbr)
      this.setFormControlValue("requestor.pagerPin", this.ngvnEvent.reqorUser.pgrPinNbr)
      this.setFormControlValue("requestor.bridgeNumber", this.ngvnEvent.cnfrcBrdgNbr)
      this.setFormControlValue("requestor.bridgePin", this.ngvnEvent.cnfrcPinNbr)

      this.setFormControlValue("sales.id", this.ngvnEvent.salsUser.userId)
      this.setFormControlValue("sales.name", this.ngvnEvent.salsUser.fullNme)
      this.setFormControlValue("sales.email", this.ngvnEvent.salsUser.emailAdr)

      this.helper.setFormControlValue("design.publishedEmailCC", this.ngvnEvent.pubEmailCcTxt);
      this.helper.setFormControlValue("design.completeEmailCC", this.ngvnEvent.cmpltdEmailCcTxt);

      //this.setFormControlValue("event.publishedEmailCC", this.ngvnEvent.pubEmailCcTxt)
      //this.setFormControlValue("event.completeEmailCC", this.ngvnEvent.cmpltdEmailCcTxt)
      this.setFormControlValue("event.comments", this.ngvnEvent.dsgnCmntTxt)
      this.setFormControlValue("event.gsrConfiguration", this.ngvnEvent.gsrCfgrnDes)
      this.setFormControlValue("event.cableAddDueDate", this.ngvnEvent.cablAddDueDt)
      this.setFormControlValue("event.cableProtocolType", this.ngvnEvent.cablPrcolTypeNme)
      this.setFormControlValue("event.sbcPair", this.ngvnEvent.sbcPairDes)
      this.setFormControlValue("event.relatedCompassNcr", this.ngvnEvent.reltdCmpsNcrCd ? 1 : 0);

      if (this.ngvnEvent.reltdCmpsNcrCd)
        this.setFormControlValue("event.relatedCompassNcrNme", this.ngvnEvent.reltdCmpsNcrNme);

      this.setFormControlValue("event.isEscalation", this.ngvnEvent.esclCd)
      this.setFormControlValue("event.escalationReason", this.ngvnEvent.esclReasId)
      this.setFormControlValue("event.primaryRequestDate", this.ngvnEvent.primReqDt)
      this.setFormControlValue("event.secondaryRequestDate", this.ngvnEvent.scndyReqDt)

      this.helper.setFormControlValue("schedule.startDate", this.ngvnEvent.strtTmst);
      this.helper.setFormControlValue("schedule.endDate", this.ngvnEvent.endTmst);
      this.helper.setFormControlValue("schedule.eventDuration", this.ngvnEvent.eventDrtnInMinQty);


      if (this.profile == EEventProfile.REVIEWER) {
        this.helper.setFormControlValue("reviewer.id", this.userService.loggedInUser.userId);
        this.helper.setFormControlValue("reviewer.name", this.userService.loggedInUser.fullName);
      }

      this.eventAsnToUserService.getByEventId(this.ngvnEvent.eventId).subscribe(
        res => {
          this.activators = res;
          this.helper.setFormControlValue("schedule.assignedToId", this.activators.map(a => a.asnToUserId).join(","))
          this.helper.setFormControlValue("schedule.assignedTo", this.activators.map(a => a.userDsplNme).join("; "))
          this.helper.setFormControlValue("schedule.displayedAssignedTo", this.activators.map(a => a.userDsplNme).join("; "))
        });

      this.workflowService.getForEvent(this.eventType, this.ngvnEvent.eventStusId).subscribe(
        res => {
          this.workflowStatusList = res as Workflow[];
          this.helper.setFormControlValue("workflowStatus", this.ngvnEvent.wrkflwStusId);
          this.isRevActTask = (this.eventId > 0 && this.workflowStatusList.length > 0) ? true : false;
        }
      );  
    }
  }

  getNgvnFormValues(): NgvnEvent {
    let ngvn = new NgvnEvent();
    console.log("relatedCompassNcr" + this.getFormControlValue('event.relatedCompassNcr'));
    //ngvn.eventTitleTxt = this.getFormControlValue("itemTitle")
    ngvn.eventDes = this.getFormControlValue("eventDescription")
    ngvn.eventStusId = this.getFormControlValue("eventStatus")
    ngvn.ngvnProdTypeId = this.getFormControlValue("ngvnProdType")
    ngvn.ftn = this.getFormControlValue("m5_no")
    ngvn.h1 = this.getFormControlValue("h1")
    ngvn.h6 = this.getFormControlValue("h6")
    ngvn.charsId = this.getFormControlValue("charsID")
    ngvn.wrkflwStusId = this.getFormControlValue("workflowStatus")
    

    ngvn.custNme = this.getFormControlValue("customer.name")
    ngvn.custEmailAdr = this.getFormControlValue("customer.email")
    ngvn.custCntctPhnNbr = this.getFormControlValue("customer.phone")
    ngvn.custCntctCellPhnNbr = this.getFormControlValue("customer.cellphone")
    ngvn.custCntctNme = this.getFormControlValue("customer.contactName")
    ngvn.custCntctPgrNbr = this.getFormControlValue("customer.pager")
    ngvn.custCntctPgrPinNbr = this.getFormControlValue("customer.pagerPin")

    if (ngvn.wrkflwStusId == WORKFLOW_STATUS.Visible || WORKFLOW_STATUS.Submit) {
      ngvn.eventTitleTxt = this.buildTitle(ngvn.custCntctNme);
    }

    ngvn.reqorUserId = this.getFormControlValue("requestor.id")
    ngvn.cnfrcBrdgNbr = this.getFormControlValue("requestor.bridgeNumber")
    ngvn.cnfrcPinNbr = this.getFormControlValue("requestor.bridgePin")

    ngvn.salsUserId = this.getFormControlValue("sales.id")

    ngvn.pubEmailCcTxt = this.getFormControlValue("design.publishedEmailCC")
    ngvn.cmpltdEmailCcTxt = this.getFormControlValue("design.completeEmailCC")

    ngvn.dsgnCmntTxt = this.getFormControlValue("event.comments")
    if (this.profile = 'Reviewer') ngvn.reviewerComments = this.getFormControlValue("reviewer.comments");
    ngvn.profile = this.profile;
    ngvn.gsrCfgrnDes = this.getFormControlValue("event.gsrConfiguration")
    ngvn.cablAddDueDt = new Date(this.getFormControlValue("event.cableAddDueDate"))
    ngvn.cablPrcolTypeNme = this.getFormControlValue("event.cableProtocolType")
    ngvn.sbcPairDes = this.getFormControlValue("event.sbcPair")
    ngvn.reltdCmpsNcrCd = this.getFormControlValue("event.relatedCompassNcr")

    if (ngvn.reltdCmpsNcrCd)
      ngvn.reltdCmpsNcrNme = this.getFormControlValue("event.relatedCompassNcrNme")

    ngvn.esclCd = this.getFormControlValue("event.isEscalation")
    ngvn.esclReasId = ngvn.esclCd ? this.getFormControlValue("event.escalationReason") : null
    ngvn.primReqDt = ngvn.esclCd ? new Date(this.getFormControlValue("event.primaryRequestDate")) : null
    ngvn.scndyReqDt = ngvn.esclCd ? new Date(this.getFormControlValue("event.secondaryRequestDate")) : null

    ngvn.ngvnEventCktIdNua = this.getNgvnEventCktIdNua()
    if (ngvn.cablPrcolTypeNme == "SIP")
      ngvn.ngvnEventSipTrnk = this.getNgvnEventSiptTrnk()

    ngvn.strtTmst = this.getFormControlValue("schedule.startDate")
    ngvn.endTmst = this.getFormControlValue("schedule.endDate")
    ngvn.eventDrtnInMinQty = this.getFormControlValue("schedule.eventDuration")

    ngvn.activators = this.getSelectedActivators(ngvn.wrkflwStusId);
    
    //var successfulActivities = this.getFormControlValue("activators.cblSuccActArray")
    //var failedActivities = this.getFormControlValue("activators.cblFailActArray")
    //var failedCodes = this.getFormControlValue("activators.ddlFailCodes")
    //var preconfigureOptions = this.getFormControlValue("activators.ddlPreConfig")
    //ngvn.eventSucssActyIds = successfulActivities;
    //ngvn.eventFailActyIds = failedActivities;
    //ngvn.preCfgConfgCode = preconfigureOptions;
    if (this.profile == EEventProfile.ACTIVATOR) {
      ngvn.eventSucssActyIds = this.getFormControlValue("activators.selectedSuccessfulActs")
      ngvn.eventFailActyIds = this.getFormControlValue("activators.selectedFailedActs")
      ngvn.preCfgConfgCode = this.getFormControlValue("activators.preconfigureCompleted")
      //ngvn.failCode = this.getFormControlValue("activators.failedCode")
      //ngvn.failCode = this.helper.isEmpty(ngvn.failCode) ? 0 : ngvn.failCode
      ngvn.activatorComments = this.getFormControlValue("activators.comments");
      ngvn.activatorUserId = this.userService.loggedInUser.userId
    }
    else if (this.profile == EEventProfile.REVIEWER) {
      let revId = this.helper.getFormControlValue("reviewer.id")
      ngvn.reviewerUserId = this.helper.isEmpty(revId) ? 0 : revId
      ngvn.reviewerComments = this.helper.getFormControlValue("reviewer.comments");
    }
    
    return ngvn;
  }

  setNgvnEventFormFields() {
    if (this.eventId > 0) {
      let eventStatusId = this.helper.getFormControlValue("eventStatus");
      let wfStatusId = this.helper.getFormControlValue("workflowStatus");
      if (this.profile == EEventProfile.ACTIVATOR) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Published) {
          this.allowEdit = true;
        } else {
          this.allowEdit = false;
        }
        // If not reviewer and EStus not equal to Inprogress or Published
        // Hide Launcher and Slot Picker
        if (this.editMode) {
          if (this.userService.loggedInUser.profiles.includes('CAND Event Reviewer')
            && (this.ngvnEvent.eventStusId == EVENT_STATUS.InProgress
            || this.ngvnEvent.eventStusId == EVENT_STATUS.Published)) {
            this.option.schedule.checkCurrentSlot.isShown = true;
            this.option.schedule.launchPicker.isShown = true;
          }
          else {
            this.option.schedule.checkCurrentSlot.isShown = false;
            this.option.schedule.launchPicker.isShown = false;
          }
        }
      }
      else if (this.profile == EEventProfile.REVIEWER) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Completed) {
          this.allowEdit = false;
        } else {
          this.allowEdit = true;
        }
        // Show Launcher and Slot Picker
        if (this.editMode) {
          this.option.schedule.userFinder.isEnabled = true;
          this.option.schedule.checkCurrentSlot.isShown = true;
          this.option.schedule.launchPicker.isShown = true;
        }

      }
      else if (this.profile == EEventProfile.MEMBER) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Completed) {
          this.allowEdit = false;
        }
        else if (eventStatusId == EVENT_STATUS.Published) {
          this.allowEdit = true;
        }
        else {
          if (this.ngvnEvent != null && this.ngvnEvent.entityBase != null
            && !this.helper.isEmpty(this.ngvnEvent.entityBase.createdByUserName)) {
            if (this.ngvnEvent.entityBase.createdByUserName != this.userService.loggedInUser.adid) {
              if (wfStatusId == WORKFLOW_STATUS.Visible || wfStatusId == WORKFLOW_STATUS.Submit
                || eventStatusId == EVENT_STATUS.Rework) {
                this.allowEdit = true;
              } else {
                this.allowEdit = false;
              }
            } else {
              this.allowEdit = true;
            }
          } else {
            this.allowEdit = true;
          }
        }
        // Show Slot picker, Hide Launcher
        if (this.editMode) {
          this.option.schedule.checkCurrentSlot.isShown = true;
          this.option.schedule.launchPicker.isShown = false;
        }
      }
      else {
        // Not CAND Activator, Revieiwer or Member
        this.allowEdit = false;
        // Hide Launcher and Picker
        this.option.schedule.checkCurrentSlot.isShown = false;
        this.option.schedule.launchPicker.isShown = false;
      }
    }
    else {
      // Allow Edit set to true for New Event so that the buttons are seen
      this.allowEdit = true;
      if (this.profile == EEventProfile.REVIEWER) {
        this.option.schedule.checkCurrentSlot.isShown = true;
        this.option.schedule.launchPicker.isShown = true;
      }
      else if (this.profile == EEventProfile.MEMBER) {
        this.option.schedule.checkCurrentSlot.isShown = true;
      }
    }
  }


  submit() {
    this.isSubmitted = true;
    var ngvn = this.getNgvnFormValues();

    // No Validation for Rework (Retract, Resched, Return and Reject) statuses and Delete.
    if (ngvn.wrkflwStusId != WORKFLOW_STATUS.Retract
      &&ngvn.wrkflwStusId != WORKFLOW_STATUS.Reschedule
      &&ngvn.wrkflwStusId != WORKFLOW_STATUS.Return
      &&ngvn.wrkflwStusId != WORKFLOW_STATUS.Reject
      && ngvn.wrkflwStusId != WORKFLOW_STATUS.Delete) {

      // Validate form fields
      if (this.form.invalid) {
        this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate Activators
      if (ngvn.activators.length == 0 && !ngvn.esclCd
        && (ngvn.wrkflwStusId == WORKFLOW_STATUS.Submit || ngvn.wrkflwStusId == WORKFLOW_STATUS.Publish)) {
        this.helper.notify('Assigned Activator is required.', Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate StartDate for IsEscalation
      if (!ngvn.esclCd) {
        if (ngvn.wrkflwStusId == WORKFLOW_STATUS.Submit) {
          if (this.profile == EEventProfile.MEMBER && !this.helper.checkStartTime(48, ngvn.strtTmst, ngvn.endTmst)) {
            this.helper.notify('Start Date must be greater than 48 hours.', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        }
        else if (ngvn.wrkflwStusId == WORKFLOW_STATUS.Publish) {
          if (!this.helper.checkStartTime(0, ngvn.strtTmst, ngvn.endTmst)) {
            this.helper.notify('Start Date cannot be in the past.', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        }
      }
    }

    this.spinner.show();

    if (this.eventId > 0) {
      // Validate Event Delete Status
      let error = this.helper.validateEventDeleteStatus(ngvn.wrkflwStusId, ngvn.eventStusId,
        this.ngvnEvent.entityBase.createdByUserName, this.userService.loggedInUser.adid);
      if (!this.helper.isEmpty(error)) {
        this.spinner.hide();
        this.helper.notify(error, Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate Reviewer Comments for Rejected Event
      if (this.isRevActTask && this.profile == EEventProfile.REVIEWER
        && ngvn.wrkflwStusId == WORKFLOW_STATUS.Reject && this.helper.isEmpty(ngvn.reviewerComments)) {
        this.spinner.hide();
        this.helper.notify("Reviewer Comments is required when the event is being rejected.", Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // TODO: Check Double Booking

      // Update NGVN Event
      this.spinner.show();
      this.ngvnEventService.update(this.eventId, ngvn).subscribe(res => {
        this.helper.notifySavedFormMessage("NGVN Event", Global.NOTIFY_TYPE_SUCCESS, false, null)
        this.cancel()
      }, error => {
          this.helper.notifySavedFormMessage("NGVN Event", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide();
        this.router.navigate(['/event/ngvn/']);
      });
    } else {
      //Create NGVN Event
      this.spinner.show();
      this.ngvnEventService.create(ngvn).subscribe(res => {
        this.helper.notifySavedFormMessage("NGVN Event", Global.NOTIFY_TYPE_SUCCESS, true, null)
        this.cancel()
      }, error => {
          this.helper.notifySavedFormMessage("NGVN Event", Global.NOTIFY_TYPE_ERROR, true, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide();
        this.router.navigate(['/event/ngvn/']);
      });
    }
  }

  disableForm() {
    this.editMode = false;
    this.form.disable();
    this.form.get("schedule.displayedAssignedTo").disable();
    this.option.schedule.checkCurrentSlot.isShown = false;
    this.option.schedule.launchPicker.isShown = false;
    this.option.requestor.userFinder.isEnabled = false;
    this.option.sales.userFinder.isEnabled = false;
    this.option.design.userFinderForPublishedEmail.isEnabled = false;
    this.option.design.userFinderForCompleteEmail.isEnabled = false;
    this.option.reviewer.userFinder.isEnabled = false;
    this.option.lookup_m5.isEnabled = false;
  }

  edit() {
    this.evntLockSrvc.lock(this.eventId).subscribe(
      res => {
        this.editMode = true;
        this.form.enable();
        this.form.get("itemTitle").disable();
        this.form.get("eventID").disable();
        this.form.get("eventStatus").disable();
        this.form.get("schedule.endDate").disable();
        if (this.ngvnEvent.wrkflwStusId == WORKFLOW_STATUS.Visible
          || this.ngvnEvent.eventStusId == EVENT_STATUS.Rework) {
          this.form.get("eventDescription").enable();
          this.form.get("event.comments").enable();
        }
        else if (this.ngvnEvent.wrkflwStusId == WORKFLOW_STATUS.Submit) {
          this.form.get("eventDescription").disable();
          this.form.get("event.comments").disable();
        }
        this.option.requestor.userFinder.isEnabled = true;
        this.option.sales.userFinder.isEnabled = true;
        this.option.design.userFinderForPublishedEmail.isEnabled = true;
        this.option.design.userFinderForCompleteEmail.isEnabled = true;
        this.option.reviewer.userFinder.isEnabled = true;
        this.option.lookup_m5.isEnabled = true;
        this.setNgvnEventFormFields();
      },
      error => {
        this.helper.notify('An error occurred while locking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  unlockEvent() {
    // Unlock Event
    if (this.eventId > 0) {
      this.evntLockSrvc.unlock(this.eventId).subscribe(
        res => {
          this.router.navigate(['event/ngvn'])
        },
        error => {
          this.helper.notify('An error occurred while unlocking the Event.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }

  getNgvnEventCktIdNua() {
    return this.circuitList.map(a =>
      new NgvnEventCktIdNua(
        (isNaN(+a.eventId)) ? 0 : a.eventId,
        a.cktIdNua,
        a.creatDt || new Date()
      )
    )
  }

  getNgvnEventSiptTrnk() {
    return this.siptTrunkList.map(a =>
      new NgvnEventSiptTrnk(
        (isNaN(+a.eventId)) ? 0 : a.eventId,
        a.sipTrnkNme,
        a.creatDt || new Date()
      )
    )
  }

  getSelectedActivators(wrkflwStusId: number) {
    if (wrkflwStusId != WORKFLOW_STATUS.Reject && wrkflwStusId != WORKFLOW_STATUS.Reschedule) {
      if (!this.helper.isEmpty(this.helper.getFormControlValue("schedule.displayedAssignedTo"))) {
        let ngvnActivators = this.helper.getFormControlValue("schedule.assignedToId")
        if (ngvnActivators != null) {
          let activators = ngvnActivators.toString()
          if (activators.length > 0) {
            if (activators.indexOf(',') != -1) {
              return activators.toString().split(",").map(a => parseInt(a));
            } else {
              return [ngvnActivators];
            }
          }
        }
      }
    }

    return [];
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }
    return value;
  }

  cancel() {
    if (this.eventId > 0 && this.editMode) {
      this.unlockEvent();
    } else {
      this.router.navigate(['event/ngvn']);
    }
  }

  setCableAddDueDate() {
    let end = this.helper.dateAdd(this.getFormControlValue("event.cableAddDueDate"), "day", null)
    this.setFormControlValue("event.cableAddDueDate", end)
  }

  onCableProtocolTypeChanged(e) {
    if (e.value == "SIP") {
      //alert("Selected SIP");
    }
  }

  resetAssignee(event: any) {
    this.helper.setFormControlValue("schedule.assignedToId", "");
    this.helper.setFormControlValue("schedule.assignedTo", "");
    this.helper.setFormControlValue("schedule.displayedAssignedTo", "");
  }

  buildTitle(customerName: string) {
    var str = '';
    var ngvnProdType = this.selectedProdType;
    if (ngvnProdType == null)
      ngvnProdType = '';

    str = str.concat(customerName);
    if ((customerName.length > 0) && (ngvnProdType.length > 0))
      str = str.concat(" - "); 
    str = str.concat(ngvnProdType);

    return str;
  }

  getProdType(e) {
    console.log('Prod Type' + e.component.option("text"));
    this.selectedProdType = e.component.option("text");
    }
}

