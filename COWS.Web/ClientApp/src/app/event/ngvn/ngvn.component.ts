import { Component, OnInit } from '@angular/core';
import { EEventType } from "./../../../shared/global";
import { EventViewsService } from "./../../../services/";

@Component({
  selector: 'app-ngvn',
  templateUrl: './ngvn.component.html'
})
export class NgvnComponent implements OnInit {

  loggedInUser = <any>[];
  eventViewsList = <any>[];
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;

  constructor(private eventService: EventViewsService) { }

  ngOnInit() {
    this.eventType = EEventType.NGVN;
    this.eventJobAid = "NGVN Event Job Aid for IPMs/Members"
    this.eventJobAidUrl = "http://webcontoo.corp.sprint.com/webcontoo/llisapi.dll/fetch/2000/1835597/1836298/14404/106652/106324/107866/106767/6074490/0069795_New_Generation_Voice_Network_Events_in_Comprehensive_Order_Workflow_System_COWS_.pdf?nodeid=6449363&vernum=-2";    
   }
}

