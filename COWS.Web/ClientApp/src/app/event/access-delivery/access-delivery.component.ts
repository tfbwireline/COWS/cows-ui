import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { EVENT_VIEWS, ENUM_OPTIONS, KeyValuePair, Global, EEventType } from "./../../../shared/global";

import { AdEventService, UserService, EventViewsService } from "./../../../services/";
import { AdEvent } from "./../../../models/";

@Component({
  selector: 'app-access-delivery',
  templateUrl: './access-delivery.component.html'
})
export class AccessDeliveryComponent implements OnInit {
  loggedInUser = <any>[];
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;
  eventViewsList = <any>[];
  eventDefaultViewID: number;
  adEventList: AdEvent[]
  views: KeyValuePair[] 

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private adEventService: AdEventService, private userService: UserService, private eventService: EventViewsService) { }

  ngOnInit() {
    this.eventType = EEventType.AD;
    this.eventJobAid = "AD Event Job Aid for IPMs/Members";
    this.eventJobAidUrl = "http://webcontoo.corp.sprint.com/webcontoo/llisapi.dll/fetch/2000/1835597/1836298/14404/106652/106324/107866/106767/6074490/0069791_Access_Delivery_Events_in_Comprehensive_Order_Workflow_System_COWS_.pdf?nodeid=6449467&vernum=-2";    
    this.views = this.helper.getEnumKeyValuePair(EVENT_VIEWS, ENUM_OPTIONS.Aliases)
    //console.log(this.views)
    //console.log(EVENT_VIEWS["My Events"])
    //this.spinner.show();
    //this.adEventService.get().subscribe(res => {
    //  this.adEventList = res as AdEvent[]
    //}, error => {
    //  this.spinner.hide()
    //}, () => this.spinner.hide());
  }

}
