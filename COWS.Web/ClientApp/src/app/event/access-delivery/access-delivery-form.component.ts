import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, of } from "rxjs";
import { CustomValidator } from '../../../shared/validator/custom-validator';
import { AdEvent, AdEventAccessTag, EnhncSrvc, EventAsnToUser, User, UserProfile, Workflow } from "./../../../models";
import { AdEventService, EnhncSrvcService, EventAsnToUserService, EventLockService, UserService, WorkflowService } from "./../../../services/";
import { Helper } from "./../../../shared";
import { EEnhanceService, EEventType, EVENT_STATUS, Global, KeyValuePair, WORKFLOW_STATUS, EEventProfile } from "./../../../shared/global";
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'app-access-delivery-form',
  templateUrl: './access-delivery-form.component.html'
})

export class AccessDeliveryFormComponent implements OnInit, OnDestroy {
  form: FormGroup
  option: any

  // For quick accessing control
  get h1() { return this.form.get("h1") }
  get comments() { return this.form.get("event.comments") }

  id: number = 0;
  profile: string = ''; //"Member";
  editMode: boolean = false;
  showEditButton: boolean = false;
  isRevActTask: boolean = false;
  isActivator: boolean = false;
  isReviewer: boolean = false;
  isMember: boolean = false;
  isSubmitted: boolean = false;
  eventType: number = EEventType.AD // Used to classify Event Type for Scheduling
  workflowStatusList: Workflow[]
  eventStatusList: KeyValuePair[]
  adTypeList: EnhncSrvc[]
  circuitList: AdEventAccessTag[] = []
  adEvent: AdEvent

  constructor(private helper: Helper, private spinner: NgxSpinnerService, 
    private activatedRoute: ActivatedRoute, private router: Router, private userService: UserService,
    private enhncSrvc: EnhncSrvcService, private adEventService: AdEventService,
    private evntLockSrvc: EventLockService, private workflowService: WorkflowService,
    private eventAsnToUserService: EventAsnToUserService
  ) { }

  ngOnDestroy() {
    this.unlockEvent();
    this.helper.form = null;
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params["id"] || 0;
    //this.editMode = this.id == 0 ? true : false

    this.form = new FormGroup({
      itemTitle: new FormControl({ value: null, disabled: true }),
      eventDescription: new FormControl({ value: null, disabled: true }),
      eventID: new FormControl({ value: null, disabled: true }),
      eventStatus: new FormControl({ value: EVENT_STATUS.Visible, disabled: true }),
      adType: new FormControl({ value: EEnhanceService.AD_BROADBAND, disabled: true }),
      m5_no: new FormControl({ value: null, disabled: true }),
      h1: new FormControl({ value: null, disabled: true }, Validators.compose([Validators.required, CustomValidator.h1Validator])),
      h6: new FormControl({ value: null, disabled: true }),
      charsID: new FormControl({ value: null, disabled: true }),
      workflowStatus: new FormControl({ value: WORKFLOW_STATUS.Visible, disabled: true }),
      customer: new FormGroup({
        name: new FormControl({ value: null, disabled: true }),
        email: new FormControl({ value: null, disabled: true }),
        phone: new FormControl({ value: null, disabled: true }),
        cellphone: new FormControl({ value: null, disabled: true }),
        contactName: new FormControl({ value: null, disabled: true }),
        pager: new FormControl({ value: null, disabled: true }),
        pagerPin: new FormControl({ value: null, disabled: true })
      }),
      requestor: new FormGroup({
        id: new FormControl(),
        name: new FormControl({ value: null, disabled: true }),
        phone: new FormControl({ value: null, disabled: true }),
        cellphone: new FormControl({ value: null, disabled: true }),
        email: new FormControl({ value: null, disabled: true }),
        pager: new FormControl({ value: null, disabled: true }),
        pagerPin: new FormControl({ value: null, disabled: true }),
        bridgeNumber: new FormControl({ value: null, disabled: true }),
        bridgePin: new FormControl({ value: null, disabled: true }),
        eventComments: new FormControl({ value: null, disabled: true })
      }),
      sales: new FormGroup({
        id: new FormControl(),
        name: new FormControl({ value: null, disabled: true }),
        phone: new FormControl({ value: null, disabled: true }),
        email: new FormControl({ value: null, disabled: true })
      }),
      schedule: new FormGroup({
        startDate: new FormControl({ value: null, disabled: true }),
        endDate: new FormControl({ value: null, disabled: true }),
        eventDuration: new FormControl({ value: 60, disabled: true }),
        extraDuration: new FormControl({ value: 0, disabled: true }),
        assignedToId: new FormControl(),
        assignedTo: new FormControl({ value: null, disabled: true }),
        displayedAssignedTo: new FormControl({ value: null, disabled: true }),
      }),
      event: new FormGroup({
        isEscalation: new FormControl({ value: false, disabled: true }),
        escalationReason: new FormControl({ value: 1, disabled: true }),
        primaryRequestDate: new FormControl({ value: new Date().setHours(new Date().getHours(), 0, 0, 0), disabled: true }),
        secondaryRequestDate: new FormControl({ value: new Date().setHours(new Date().getHours(), 0, 0, 0), disabled: true }),
        publishedEmailCC: new FormControl({ value: null, disabled: true }),
        completeEmailCC: new FormControl({ value: null, disabled: true }),
        comments: new FormControl({ value: null, disabled: true }, Validators.required),
        isCPEAttending: new FormControl({ value: false, disabled: true }),
        links: new FormControl({ value: null, disabled: true })
      }),
      reviewer: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        comments: new FormControl()
      }),
      activators: new FormGroup({
        successfulActivities: new FormArray([]),
        selectedSuccessfulActs: new FormControl(),
        failedActivities: new FormArray([]),
        selectedFailedActs: new FormControl(),
        failedCode: new FormControl(),
        preconfigureCompleted: new FormControl(),
        comments: new FormControl()
      })
    });

    this.option = {
      customer: {
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Customer Name is required" }
          ]
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        contactName: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Contact Name is required" }
          ]
        },
        pager: {
          isShown: true
        },
        pagerPin: {
          isShown: true
        },
      },
      requestor: {
        userFinder: {
          isEnabled: false,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["cellphone", "cellPhnNbr"],
            ["email", "emailAdr"],
            ["pager", "pgrNbr"],
            ["pagerPin", "pgrPinNbr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for requestor" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Requestor is required" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        },
        pager: {
          isShown: true
        },
        pagerPin: {
          isShown: true
        },
        bridgeNumber: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Conference Bridge Number is required" }
          ]
        },
        bridgePin: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Conference Bridge Pin is required" }
          ]
        }
      },
      sales: {
        userFinder: {
          isEnabled: false,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["email", "emailAdr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for sales" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Sales Engineer Name is required" }
          ]
        },
        phone: {
          isShown: true
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        }
      },
      event: {
        isEscalation: {
          isShown: true
        },
        escalationReason: {
          isShown: true
        },
        primaryRequestDate: {
          isShown: true
        },
        secondaryRequestDate: {
          isShown: true
        },
        userFinderForPublishedEmail: {
          isEnabled: false,
          searchQuery: "publishedEmailCC",
          mappings: [
            ["publishedEmailCC", "emailAdr"]
          ]
        },
        userFinderForCompleteEmail: {
          isEnabled: false,
          searchQuery: "completeEmailCC",
          mappings: [
            ["completeEmailCC", "emailAdr"]
          ]
        },
      },
      schedule: {
        userFinder: {
          isEnabled: false,
          searchQuery: "displayedAssignedTo",
          mappings: [
            ["assignedToId", "userId"],
            ["assignedTo", "dsplNme"],
            ["displayedAssignedTo", "dsplNme"]
          ]
        },
        softAssign: {
          isShown: false
        },
        checkCurrentSlot: {
          isShown: true
        },
        launchPicker: {
          isShown: true
        }
        //eventDuration: {
        //  validators: [
        //    { type: Validators.required, name: "required", message: "Event Duration is required" }
        //  ]
        //}
      },
      lookup_m5: {
        isEnabled: false,
        searchQuery: "m5_no",
        mappings: [
          ["h1", "h1"],
          ["h6", "h6"],
          ["customer.name", "custNme"],
          ["customer.email", "custEmailAdr"],
          ["customer.cellphone", "custCntctCellPhnNbr"],
          ["customer.phone", "custCntctPhnNbr"],
          ["customer.contactName", "custCntctNme"],
          ["customer.pager", "custCntctPgrNbr"],
          ["customer.pagerPin", "custCntctPgrPinNbr"]
        ]
      },
      reviewer: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for reviewer" }
          ]
        },
        name: {
          validators: [
            { type: Validators.required, name: "required", message: "Reviewer is required" }
          ]
        }
      },
      activator: {
        failedCode: {
          isShown: false
        }
      }
    }

    // Set the helper form to this form
    this.helper.form = this.form;
    this.init()
  }

  init() {
    let id = this.id
    if (id > 0) {
      let data = zip(
        this.adEventService.getById(id).pipe(
          concatMap(
            res => zip(
              of(res as AdEvent),
              this.userService.getByUserID((res as AdEvent).reqorUserId),
              this.userService.getByUserID((res as AdEvent).salsUserId),
              this.eventAsnToUserService.getByEventId((res as AdEvent).eventId),
              this.workflowService.getForEvent(this.eventType, (res as AdEvent).eventStusId)
            )
          )
        ),
        this.enhncSrvc.getEnhncSrvc(),
        this.workflowService.get(),
        this.userService.getFinalUserProfile(this.userService.loggedInUser.adid, this.eventType),
      )

      this.spinner.show()
      data.subscribe(res => {
        //console.log(res)
        this.adEvent = res[0][0] as AdEvent
        let requestor = res[0][1] as User
        let sales = res[0][2] as User
        let activators = res[0][3] as EventAsnToUser[]
        let workflowForEvent = res[0][4] as Workflow[]
        let adType = res[1] as EnhncSrvc[]
        let workflow = res[2] as Workflow[]
        let profile = res[3] as UserProfile

        this.helper.setFormControlValue("itemTitle", this.adEvent.eventTitleTxt)
        this.helper.setFormControlValue("eventDescription", this.adEvent.eventDes)
        this.helper.setFormControlValue("eventID", this.adEvent.eventId)
        this.helper.setFormControlValue("eventStatus", this.adEvent.eventStusId)
        this.helper.setFormControlValue("adType", this.adEvent.enhncSrvcId)
        this.helper.setFormControlValue("m5_no", this.adEvent.ftn)
        this.helper.setFormControlValue("h1", this.adEvent.h1)
        this.helper.setFormControlValue("h6", this.adEvent.h6)
        this.helper.setFormControlValue("charsID", this.adEvent.charsId)

        this.helper.setFormControlValue("customer.name", this.adEvent.custNme)
        this.helper.setFormControlValue("customer.email", this.adEvent.custEmailAdr)
        this.helper.setFormControlValue("customer.phone", this.adEvent.custCntctPhnNbr)
        this.helper.setFormControlValue("customer.cellphone", this.adEvent.custCntctCellPhnNbr)
        this.helper.setFormControlValue("customer.contactName", this.adEvent.custCntctNme)
        this.helper.setFormControlValue("customer.pager", this.adEvent.custCntctPgrNbr)
        this.helper.setFormControlValue("customer.pagerPin", this.adEvent.custCntctPgrPinNbr)

        this.helper.setFormControlValue("requestor.id", requestor.userId)
        this.helper.setFormControlValue("requestor.name", requestor.fullNme)
        this.helper.setFormControlValue("requestor.email", requestor.emailAdr)
        this.helper.setFormControlValue("requestor.cellphone", requestor.cellPhnNbr)
        this.helper.setFormControlValue("requestor.phone", requestor.phnNbr)
        this.helper.setFormControlValue("requestor.pager", requestor.pgrNbr)
        this.helper.setFormControlValue("requestor.pagerPin", requestor.pgrPinNbr)
        this.helper.setFormControlValue("requestor.bridgeNumber", this.adEvent.cnfrcBrdgNbr)
        this.helper.setFormControlValue("requestor.bridgePin", this.adEvent.cnfrcPinNbr)

        this.helper.setFormControlValue("sales.id", sales.userId)
        this.helper.setFormControlValue("sales.name", sales.fullNme)
        this.helper.setFormControlValue("sales.email", sales.emailAdr)

        this.helper.setFormControlValue("schedule.assignedToId", activators.map(a => a.asnToUserId).join(","))
        this.helper.setFormControlValue("schedule.assignedTo", activators.map(a => a.userDsplNme).join("; "))
        this.helper.setFormControlValue("schedule.displayedAssignedTo", activators.map(a => a.userDsplNme).join("; "))

        this.workflowStatusList = workflowForEvent
        if (this.workflowStatusList.filter(a => a.wrkflwStusId == this.adEvent.wrkflwStusId).length == 0) {
          this.workflowStatusList.push(workflow.find(a => a.wrkflwStusId == this.adEvent.wrkflwStusId))
        }

        this.helper.setFormControlValue("event.publishedEmailCC", this.adEvent.pubEmailCcTxt)
        this.helper.setFormControlValue("event.completeEmailCC", this.adEvent.cmpltdEmailCcTxt)
        this.helper.setFormControlValue("event.comments", this.adEvent.desCmntTxt)
        this.helper.setFormControlValue("event.isCPEAttending", this.adEvent.cpeAtndCd)
        this.helper.setFormControlValue("event.links", this.adEvent.docLinkTxt)
        this.helper.setFormControlValue("event.isEscalation", this.adEvent.esclCd)
        this.helper.setFormControlValue("event.escalationReason", this.adEvent.esclReasId)
        this.helper.setFormControlValue("event.primaryRequestDate", this.adEvent.primReqDt)
        this.helper.setFormControlValue("event.secondaryRequestDate", this.adEvent.scndyReqDt)

        this.circuitList = this.adEvent.adEventAccsTag
        this.helper.setFormControlValue("schedule.startDate", this.adEvent.strtTmst)
        this.helper.setFormControlValue("schedule.endDate", this.adEvent.endTmst)
        this.helper.setFormControlValue("schedule.eventDuration", this.adEvent.eventDrtnInMinQty)

        this.helper.setFormControlValue("workflowStatus", this.adEvent.wrkflwStusId)
        this.adTypeList = adType.filter(a => a.eventTypeId == 1) // Filter for AD Types

        // Added condition by Sarah Sandoval [20200506]
        // To cater null profiles, having no event profiles
        if (profile != null) {
          this.profile = this.helper.getFinalProfileName(profile);

          if (this.profile == EEventProfile.REVIEWER) {
            this.helper.setFormControlValue("reviewer.id", this.userService.loggedInUser.userId);
            this.helper.setFormControlValue("reviewer.name", this.userService.loggedInUser.fullName);
          }

          if (
            // Conditions of restrictions for Activator
            !(this.profile == EEventProfile.ACTIVATOR &&
              [EVENT_STATUS.Visible, EVENT_STATUS.Pending, EVENT_STATUS.Completed].includes(this.adEvent.eventStusId)
              // Conditions of restrictions for Reviewer and Member
              || this.profile != EEventProfile.ACTIVATOR &&
              [EVENT_STATUS.InProgress, EVENT_STATUS.Completed].includes(this.adEvent.eventStusId))
          ) {
            this.showEditButton = true;
          }

          this.isMember = this.profile == EEventProfile.MEMBER ? true : false;
          this.isReviewer = this.profile == EEventProfile.REVIEWER ? true : false;
          this.isActivator = this.profile == EEventProfile.ACTIVATOR ? true : false;
          this.isRevActTask = (this.id > 0 && this.workflowStatusList.length > 0) ? true : false;

          if(this.isMember && !this.isReviewer) {
            this.form.get("schedule.eventDuration").disable();
          }
        }

        setTimeout(() => {
          this.spinner.hide()
          this.disableForm()
        }, 1000)
      }, error => {
        this.spinner.hide()
      });
    } else {
      let data = zip(
        this.userService.getLoggedInUser(),
        this.enhncSrvc.getEnhncSrvc(),
        this.workflowService.getForEvent(this.eventType, EVENT_STATUS.Visible),
        this.userService.getFinalUserProfile(localStorage.getItem('userADID'), this.eventType)
      )

      this.spinner.show();
      data.subscribe(res => {
        let user = res[0] as User
        this.helper.setFormControlValue("requestor.id", user.userId)
        this.helper.setFormControlValue("requestor.name", user.fullNme)
        this.helper.setFormControlValue("requestor.email", user.emailAdr)
        this.helper.setFormControlValue("requestor.cellphone", user.cellPhnNbr)
        this.helper.setFormControlValue("requestor.phone", user.phnNbr)
        this.helper.setFormControlValue("requestor.pager", user.pgrNbr)
        this.helper.setFormControlValue("requestor.pagerPin", user.pgrPinNbr)

        this.adTypeList = (res[1] as EnhncSrvc[]).filter(a => a.eventTypeId == 1) // Filter for AD Types
        //this.workflowStatusList = (res[2] as Workflow[]).filter(a => a.wrkflwStusId == WORKFLOW_STATUS.Visible || a.wrkflwStusId == WORKFLOW_STATUS.Submit) // Filter for AD Creation; Workflow Status
        this.workflowStatusList = res[2] as Workflow[] // Filter for AD Creation; Workflow Status

        var usrPrf = res[3] as UserProfile;
        if (usrPrf != null) {
          this.profile = this.helper.getFinalProfileName(usrPrf)
        }
        this.helper.setFormControlValue("workflowStatus", this.profile == "Reviewer" ? WORKFLOW_STATUS.Submit : WORKFLOW_STATUS.Visible)
        this.setDefaultFields()
      }, error => {
        this.spinner.hide()
      }, () => this.spinner.hide());
    }

    this.eventStatusList = this.helper.getEnumKeyValuePair(EVENT_STATUS);
    this.disableForm();
  }
  
  submit() {
    this.isSubmitted = true;

    let ad = new AdEvent();
    ad = this.getFormDetails()

    // No Validation for Rework (Retract, Resched, Return and Reject) statuses and Delete.
    if (ad.wrkflwStusId != WORKFLOW_STATUS.Retract
      && ad.wrkflwStusId != WORKFLOW_STATUS.Reschedule
      && ad.wrkflwStusId != WORKFLOW_STATUS.Return
      && ad.wrkflwStusId != WORKFLOW_STATUS.Reject
      && ad.wrkflwStusId != WORKFLOW_STATUS.Delete) {

      // Validate form fields
      if (this.form.invalid) {
        this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate Activators
      if (ad.activators.length == 0 && !ad.esclCd
        && (ad.wrkflwStusId == WORKFLOW_STATUS.Submit || ad.wrkflwStusId == WORKFLOW_STATUS.Publish)) {
        this.helper.notify('Assigned Activator is required.', Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate StartDate for IsEscalation
      if (!ad.esclCd) {
        if (ad.wrkflwStusId == WORKFLOW_STATUS.Submit) {
          if (this.profile == EEventProfile.MEMBER && !this.helper.checkStartTime(48, ad.strtTmst, ad.endTmst)) {
            this.helper.notify('Start Date must be greater than 48 hours.', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        }
        else if (ad.wrkflwStusId == WORKFLOW_STATUS.Publish) {
          if (!this.helper.checkStartTime(0, ad.strtTmst, ad.endTmst)) {
            this.helper.notify('Start Date cannot be in the past.', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        }
      }
    }
    // Removing this part by Sarah Sandoval [20190715] for DE9141
    // Dropping activators by WFId is done on getSelectedActivators method
    //else {
    //  ad.activators = []
    //}

    this.spinner.show();

    if (this.id > 0) {
      // Validate Event Delete Status
      let error = this.helper.validateEventDeleteStatus(ad.wrkflwStusId, ad.eventStusId,
        this.adEvent.creatByUserAdid, this.userService.loggedInUser.adid);
      if (!this.helper.isEmpty(error)) {
        this.spinner.hide();
        this.helper.notify(error, Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate Reviewer Comments for Rejected Event
      //console.log('revTask: ' + this.isRevActTask + '; profile: ' + this.profile + '; wf: ' + ad.wrkflwStusId + '; comment: ' + ad.reviewerComments);
      if (this.isRevActTask && this.profile == EEventProfile.REVIEWER
        && ad.wrkflwStusId == WORKFLOW_STATUS.Reject && this.helper.isEmpty(ad.reviewerComments)) {
        this.spinner.hide();
        this.helper.notify("Reviewer Comments is required when the event is being rejected.", Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Update AD Event
      console.log(ad)
      this.adEventService.update(this.id, ad).subscribe(res => {
        this.helper.notifySavedFormMessage("AD Event", Global.NOTIFY_TYPE_SUCCESS, false, null)
        this.cancel()
      }, error => {
        this.helper.notifySavedFormMessage("AD Event", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => this.spinner.hide());
    } else {     
      // Create AD Event
      this.adEventService.create(ad).subscribe(res => {
        this.helper.notifySavedFormMessage("AD Event", Global.NOTIFY_TYPE_SUCCESS, true, null)
        this.cancel()
      }, error => {
        this.helper.notifySavedFormMessage("AD Event", Global.NOTIFY_TYPE_ERROR, true, error)
        this.spinner.hide()
      }, () => this.spinner.hide());
    }
  }

  getFormDetails() {
    let ad: AdEvent = new AdEvent()

    ad.eventTitleTxt = this.helper.getFormControlValue("customer.name") + " "
      + this.adTypeList.find(a => a.enhncSrvcId == this.helper.getFormControlValue("adType")).enhncSrvcNme
    ad.eventDes = this.helper.getFormControlValue("eventDescription")
    ad.eventStusId = this.helper.getFormControlValue("eventStatus")
    ad.enhncSrvcId = this.helper.getFormControlValue("adType")
    ad.ftn = this.helper.getFormControlValue("m5_no")
    ad.h1 = this.helper.getFormControlValue("h1")
    ad.h6 = this.helper.getFormControlValue("h6")
    ad.charsId = this.helper.getFormControlValue("charsID")
    ad.custNme = this.helper.getFormControlValue("customer.name")
    // This fix is removed by Sarah Sandoval [07152019]
    // AD Government type is always secured - code change is seen on controller
    // Updated by Sarah Sandoval [07082019] - Bug Fix (DE8103)
    // Fix on bug where ADType = AD Government, EventTitle should be Private Customer
    // ad.eventTitleTxt = (ad.enhncSrvcId == 9) ? "Private Customer" : ad.eventTitleTxt
    ad.custEmailAdr = this.helper.getFormControlValue("customer.email")
    ad.custCntctPhnNbr = this.helper.getFormControlValue("customer.phone")
    ad.custCntctCellPhnNbr = this.helper.getFormControlValue("customer.cellphone")
    ad.custCntctNme = this.helper.getFormControlValue("customer.contactName")
    ad.custCntctPgrNbr = this.helper.getFormControlValue("customer.pager")
    ad.custCntctPgrPinNbr = this.helper.getFormControlValue("customer.pagerPin")
    ad.reqorUserId = this.helper.getFormControlValue("requestor.id")
    ad.cnfrcBrdgNbr = this.helper.getFormControlValue("requestor.bridgeNumber")
    ad.cnfrcPinNbr = this.helper.getFormControlValue("requestor.bridgePin")
    ad.salsUserId = this.helper.getFormControlValue("sales.id")
    ad.pubEmailCcTxt = this.helper.getFormControlValue("event.publishedEmailCC")
    ad.cmpltdEmailCcTxt = this.helper.getFormControlValue("event.completeEmailCC")
    ad.desCmntTxt = this.helper.getFormControlValue("event.comments")
    ad.docLinkTxt = this.helper.getFormControlValue("event.links")
    ad.cpeAtndCd = this.helper.getFormControlValue("event.isCPEAttending")
    ad.esclCd = this.helper.getFormControlValue("event.isEscalation")
    ad.esclReasId = ad.esclCd ? this.helper.getFormControlValue("event.escalationReason") : null
    ad.primReqDt = ad.esclCd ? new Date(this.helper.getFormControlValue("event.primaryRequestDate")) : null
    ad.scndyReqDt = ad.esclCd ? new Date(this.helper.getFormControlValue("event.secondaryRequestDate")) : null
    ad.adEventAccsTag = this.getAdEventAccessTag()
    ad.strtTmst = this.helper.getFormControlValue("schedule.startDate")
    ad.endTmst = this.helper.getFormControlValue("schedule.endDate")
    ad.eventDrtnInMinQty = this.helper.getFormControlValue("schedule.eventDuration")
    ad.wrkflwStusId = this.helper.getFormControlValue("workflowStatus")
    ad.activators = this.getSelectedActivators(ad.wrkflwStusId)
    if (this.profile == EEventProfile.ACTIVATOR) {
      ad.eventSucssActyIds = this.helper.getFormControlValue("activators.selectedSuccessfulActs")
      ad.eventFailActyIds = this.helper.getFormControlValue("activators.selectedFailedActs")
      ad.preCfgConfgCode = this.helper.getFormControlValue("activators.preconfigureCompleted")
      ad.failCode = this.helper.isEmpty(ad.failCode) ? 0 : ad.failCode
      ad.activatorComments = this.helper.getFormControlValue("activators.comments")
      ad.activatorUserId = this.userService.loggedInUser.userId
    }
    else if (this.profile == EEventProfile.REVIEWER) {
      let revId = this.helper.getFormControlValue("reviewer.id")
      ad.reviewerUserId = this.helper.isEmpty(revId) ? 0 : revId
      ad.reviewerComments = this.helper.getFormControlValue("reviewer.comments");
    }

    return ad
  }

  getAdEventAccessTag() {
    return this.circuitList.map(a =>
      new AdEventAccessTag(
        (isNaN(+a.eventId)) ? 0 : a.eventId,
        a.cktId,
        a.oldCktId,
        a.creatDt || new Date()
      )
    )
  }

  getSelectedActivators(wrkflwStusId: number) {
    if (wrkflwStusId != WORKFLOW_STATUS.Reject && wrkflwStusId != WORKFLOW_STATUS.Reschedule) {
      if (!this.helper.isEmpty(this.helper.getFormControlValue("schedule.displayedAssignedTo"))) {
        let adActivators = this.helper.getFormControlValue("schedule.assignedToId")
        if (adActivators != null) {
          let activators = adActivators.toString()
          if (activators.length > 0) {
            if (activators.indexOf(',') != -1) {
              return activators.toString().split(",").map(a => parseInt(a));
            } else {
              return [adActivators];
            }
          }
        }
      }
    }

    return [];
  }

  edit() {
    this.evntLockSrvc.lock(this.id).subscribe(
      res => {
        this.editMode = true;
        let eventStatusId = this.form.get("eventStatus").value

        if (this.profile == "Member" && ![EVENT_STATUS.Visible, EVENT_STATUS.Pending, EVENT_STATUS.Rework].includes(eventStatusId)) {
          this.form.get("h1").enable();
          this.form.get("h6").enable();
          this.form.get("charsID").enable();

          this.form.get("customer.name").enable();
          this.form.get("customer.email").enable();
          this.form.get("customer.phone").enable();
          this.form.get("customer.contactName").enable();

          this.form.get("workflowStatus").enable();

          return;
        }

        this.setDefaultFields()
      },
      error => {
        this.helper.notify('An error occurred while locking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  setDefaultFields() {
    this.form.enable();
    this.form.get("itemTitle").disable();
    this.form.get("eventID").disable();
    this.form.get("eventStatus").disable();
    this.form.get("schedule.endDate").disable();

    //this.form.get("eventDescription").enable();
    //this.form.get("adType").enable();
    //this.form.get("m5_no").enable();
    //this.form.get("h1").enable();
    //this.form.get("h6").enable();
    //this.form.get("charsID").enable();

    //this.form.get("customer.name").enable();
    //this.form.get("customer.email").enable();
    //this.form.get("customer.phone").enable();
    //this.form.get("customer.cellphone").enable();
    //this.form.get("customer.contactName").enable();
    //this.form.get("customer.pager").enable();
    //this.form.get("customer.pagerPin").enable();

    //this.form.get("requestor.name").enable();
    //this.form.get("requestor.email").enable();
    //this.form.get("requestor.phone").enable();
    //this.form.get("requestor.cellphone").enable();
    //this.form.get("requestor.pager").enable();
    //this.form.get("requestor.pagerPin").enable();
    //this.form.get("requestor.bridgeNumber").enable();
    //this.form.get("requestor.bridgePin").enable();
    //this.form.get("requestor.eventComments").enable();

    //this.form.get("sales.name").enable();
    //this.form.get("sales.phone").enable();
    //this.form.get("sales.email").enable();

    //this.form.get("event.isEscalation").enable();
    //this.form.get("event.escalationReason").enable();
    //this.form.get("event.primaryRequestDate").enable();
    //this.form.get("event.secondaryRequestDate").enable();
    //this.form.get("event.publishedEmailCC").enable();
    //this.form.get("event.completeEmailCC").enable();
    //this.form.get("event.comments").enable();
    //this.form.get("event.isCPEAttending").enable();
    //this.form.get("event.links").enable();

    //this.form.get("schedule.startDate").enable();
    //this.form.get("schedule.eventDuration").enable();
    //this.form.get("schedule.extraDuration").enable();
    //this.form.get("schedule.displayedAssignedTo").enable();

    //this.form.get("workflowStatus").enable();

    this.option.lookup_m5.isEnabled = true;
    this.option.requestor.userFinder.isEnabled = true
    this.option.sales.userFinder.isEnabled = true
    this.option.event.userFinderForPublishedEmail.isEnabled = true
    this.option.event.userFinderForCompleteEmail.isEnabled = true
    this.option.schedule.userFinder.isEnabled = true
    this.option.reviewer.userFinder.isEnabled = true

    if (this.profile == EEventProfile.ACTIVATOR) {
      this.option.schedule.checkCurrentSlot.isShown = false;
      this.option.schedule.launchPicker.isShown = false;
    }
    else if (this.profile == EEventProfile.REVIEWER) {
      this.option.schedule.checkCurrentSlot.isShown = true;
      this.option.schedule.launchPicker.isShown = true;
    }
    else {
      this.option.schedule.checkCurrentSlot.isShown = true;
      this.option.schedule.launchPicker.isShown = false;
    }
  }

  disableForm() {
    this.form.disable();
    this.option.lookup_m5.isEnabled = false;
    this.option.requestor.userFinder.isEnabled = false
    this.option.sales.userFinder.isEnabled = false
    this.option.event.userFinderForPublishedEmail.isEnabled = false
    this.option.event.userFinderForCompleteEmail.isEnabled = false
    this.option.schedule.userFinder.isEnabled = false
    this.option.schedule.checkCurrentSlot.isShown = false;
    this.option.schedule.launchPicker.isShown = false;
    this.option.reviewer.userFinder.isEnabled = false;
  }

  unlockEvent() {
    // Unlock Event
    if (this.id > 0) {
      this.evntLockSrvc.unlock(this.id).subscribe(
        res => {
          this.router.navigate(['event/access-delivery'])
        },
        error => {
          this.helper.notify('An error occurred while unlocking the Event.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }

  cancel() {
    if (this.id > 0 && this.editMode) {
      this.unlockEvent();
    } else {
      this.router.navigate(['event/access-delivery']);
    }
  }
}
