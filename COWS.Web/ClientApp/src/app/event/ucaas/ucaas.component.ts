import { Component, OnInit } from '@angular/core';
import { EEventType } from "./../../../shared/global";
import { EventViewsService } from "./../../../services/";

@Component({
  selector: 'app-ucaas',
  templateUrl: './ucaas.component.html'
})
export class UcaasComponent implements OnInit {

  loggedInUser = <any>[];
  eventViewsList = <any>[];
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;

  constructor(private eventService: EventViewsService) { }

  ngOnInit() {
    this.eventType = EEventType.UCaaS;
    this.eventJobAid = "UCaaS Event Job Aid for IPMs/Members";
    this.eventJobAidUrl = "http://webcontoo.corp.sprint.com/webcontoo/llisapi.dll/9280920/9280919_COWS_Event_Form__-_Job_Aid_for_Global_Cisco_HCS_%28UCaaS%29.pdf?func=doc.Fetch&nodeid=9280920&vernum=0";    
  }
}


