import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { DxDataGridComponent } from "devextreme-angular";
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, of } from "rxjs";
import { map, concatMap } from 'rxjs/operators'
import * as moment from 'moment';
import { Helper } from "./../../../shared";
import {
  EVENT_STATUS, KeyValuePair, EEventType, RB_CONDITION, Global, WORKFLOW_STATUS, EEventProfile,
  EUcaasActivityType, EUcaasPlanType, EUcaasProdType, ErrorMsg
} from "./../../../shared/global";
import {
  UserService, UcaasActivityTypeService, UcaasBillActivityService, UcaasPlanTypeService,
  UcaasProdTypeService, ServiceDeliveryService, DeviceManufacturerService, WorkflowService,
  SearchService, ServiceTierService, DeviceModelService, UcaasEventService, EventLockService,
  EventAsnToUserService,

  SystemConfigService
} from "./../../../services/";
import { CustomValidator } from '../../../shared/validator/custom-validator';
import {
  User, UcaasEventBilling, DeviceManufacturer, OdieRspn, UcaasEvent, EventCpeDev, Workflow,
  EventAsnToUser, UserProfile, EventDevSrvcMgmt, RedesignDevicesInfo, UcaasEventOdieDevice,
  DeviceModel, EventDeviceCompletion, EventDiscoDev
} from '../../../models';
declare let $: any;

@Component({
  selector: 'app-ucaas-form',
  templateUrl: './ucaas-form.component.html'
})
export class UcaasFormComponent implements OnInit {
  @ViewChild('devComGrid', { static: false }) devComGrid: DxDataGridComponent;
  @ViewChild('gridCpe', { static: false }) gridCpe: DxDataGridComponent;
  // Page Properties
  id: number = 0
  profile: string = '';
  eventType: number = EEventType.UCaaS;
  ucaasEvent: UcaasEvent;
  editMode: boolean = true;
  lockedByUser: string;
  isLocked: boolean = false;
  isRevActTask: boolean = false;
  isActivator: boolean = false;
  isReviewer: boolean = false;
  isMember: boolean = false;
  error: string = "";
  errors: string[] = [];
  showEditButton: boolean = false;

  // Form UI Controls
  modalCpeDeviceList: any[];
  modalPendingComponentList: any[];
  modalInstalledInventoryList: any[];

  eventStatusList: KeyValuePair[] = []; // For Event Status DDL
  odieRspnList: OdieRspn[] = []; // For CustomerName DDL
  mnspmId: number = 0;
  ucaasActivityTypeList: KeyValuePair[]; // For UCaaS Activity Type DDL
  ucaasPlanTypeList: KeyValuePair[]; // For UCaaS Plan Type DDL
  ucaasProdTypeList: KeyValuePair[]; // For UCaas Prod Type DDL
  ucaasEventCpeDevList: EventCpeDev[] = []; // For CPE Table Grid
  ucaasBillActivityList: UcaasEventBilling[]; // For Billing Table Grid
  cpeDeliveryOptionList: KeyValuePair[]; // For CPE DDL
  disconnectMgmtOnlyList: KeyValuePair[];
  totalCustDisconnectList: KeyValuePair[];
  odieDiscoDevList: EventDiscoDev[] = []; // For Manage Activity Table Grid
  showDiscoDevError: boolean = false;
  hideRedesignInfo: boolean = false;
  showDeviceCompleteTable: boolean = false;

  odieRedesignDevInfoList: RedesignDevicesInfo[] = []; // For Redesign Dev Table Grid
  showRedesignError: boolean = false;
  //showReloadRedesignButton: boolean = false;
  h1ApprovedRedesignError: string = ErrorMsg.EVENT_H1_APPROVEDREDESIGN;
  selectedOdieRedesignDevKeys: any[] = []; // For Selected Redesign Dev Table Grid
  ucaasRedesignDevInfoList: UcaasEventOdieDevice[] = []; // For UCaaS Redesign Dev Table Grid
  ucaasEventOdieDevice : UcaasEventOdieDevice[] = [];
  devCompletionList: EventDeviceCompletion[] = [];
  deviceSrvcMngmtList: EventDevSrvcMgmt[] = [];
  serviceTierList: KeyValuePair[];
  deviceManfList: DeviceManufacturer[];
  deviceModelList: DeviceModel[] = [];
  workflowStatusList: Workflow[];
  activators: EventAsnToUser[];

  // Form Properties
  form: FormGroup;
  option: any;
  isSubmitted: boolean = false;
  isLookupSiteIdClicked: boolean = false;
  get h1() { return this.form.get("h1") }
  get customerName() { return this.form.get("customerName") }
  get teamPdl() { return this.form.get("teamPdl") }
  get customerSow() { return this.form.get("customerSow") }
  get ucaasProdType() { return this.form.get("ucaasProdType") }
  get ucaasPlanType() { return this.form.get("ucaasPlanType") }
  get ucaasActivityType() { return this.form.get("ucaasActivityType") }
  get shortDesc() { return this.form.get("shortDesc") }
  get workflowStatus() { return this.form.get("workflowStatus") }
  get installSitePoc() { return this.form.get("site.installSite.poc") }
  get installSitePhoneCode() { return this.form.get("site.installSite.phoneCode") }
  get installSitePhone() { return this.form.get("site.installSite.phone") }
  get serviceAssurancePoc() { return this.form.get("site.serviceAssurance.poc") }
  get serviceAssurancePhoneCode() { return this.form.get("site.serviceAssurance.phoneCode") }
  get serviceAssurancePhone() { return this.form.get("site.serviceAssurance.phone") }
  get siteAddress() { return this.form.get("site.address") }
  get siteState() { return this.form.get("site.state") }
  get siteCountry() { return this.form.get("site.country") }
  get siteCity() { return this.form.get("site.city") }
  get siteZip() { return this.form.get("site.zip") }
  get cpeDeliveryOption() { return this.form.get("cpe.cpeDeliveryOption") }
  get cpeDispatchEmail() { return this.form.get("cpe.cpeDispatchEmail") }
  get cpeDispatchComments() { return this.form.get("cpe.cpeDispatchComments") }
  get disconnectMgmtOnly() { return this.form.get("disconnectInfo.disconnectMgmtOnly") }
  get totalCustDisconnect() { return this.form.get("disconnectInfo.totalCustDisconnect") }
  get disconnectReasonDesc() { return this.form.get("disconnectInfo.disconnectReasonDesc") }

  constructor(private avRoute: ActivatedRoute, private router: Router, private helper: Helper,
    private spinner: NgxSpinnerService, private searchSrvc: SearchService, private userSrvc: UserService,
    private ucaasSrvc: UcaasEventService, private ucaasActySrvc: UcaasActivityTypeService,
    private ucaasBillActySrvc: UcaasBillActivityService, private ucaasPlanSrvc: UcaasPlanTypeService,
    private ucaasProdSrvc: UcaasProdTypeService, private cpeDeliverySrvc: ServiceDeliveryService,
    private devManfSrvc: DeviceManufacturerService, private devModelSrvc: DeviceModelService,
    private mdsTierSrvc: ServiceTierService, private evntAsnToUserSrvc: EventAsnToUserService,
    private workflowSrvc: WorkflowService, private evntLockSrvc: EventLockService,
    private systemConfigSrvc: SystemConfigService
  ) {
    this.getDeviceModelByManfId = this.getDeviceModelByManfId.bind(this);
  }

  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"] || 0;
    this.editMode = this.id > 0 ? false : true;
    this.form = new FormGroup({
      eventID: new FormControl({ value: null, disabled: true }),
      eventStatus: new FormControl({ value: EVENT_STATUS.Visible, disabled: true }),
      itemTitle: new FormControl({ value: null, disabled: true }),
      h1: new FormControl(null, Validators.compose([Validators.required, CustomValidator.h1Validator])),
      customerName: new FormControl(null, Validators.required),
      charsID: new FormControl(),
      teamPdl: new FormControl(null, Validators.required),
      customerSow: new FormControl('https://doc-share.corp.sprint.com/livelink/llisapi.dll?func=ll&objId=79254740&objAction=browse&viewType=1', Validators.required),
      ucaasProdType: new FormControl(null, Validators.required),
      ucaasPlanType: new FormControl(null, Validators.required),
      ucaasActivityType: new FormControl(null, Validators.required),
      ucaasDesignDoc: new FormControl(),
      shortDesc: new FormControl(null, Validators.required),
      workflowStatus: new FormControl(),
      suppressEmail: new FormControl(false),
      site: new FormGroup({
        h6: new FormControl(),
        ccd: new FormControl(),
        id: new FormControl(),
        siteIdAddress: new FormControl(),
        address: new FormControl(),
        state: new FormControl(),
        bldg_flr_rm: new FormControl(),
        country: new FormControl(),
        city: new FormControl(),
        zip: new FormControl(),
        installSite: new FormGroup({
          poc: new FormControl(),
          phoneCode: new FormControl(),
          phone: new FormControl(),
          cellphoneCode: new FormControl(),
          cellphone: new FormControl()
        }),
        serviceAssurance: new FormGroup({
          poc: new FormControl(),
          phoneCode: new FormControl(),
          phone: new FormControl(),
          cellphoneCode: new FormControl(),
          cellphone: new FormControl()
        })
      }),
      cpe: new FormGroup({
        cpeDeliveryOption: new FormControl(3, Validators.required), // Default Value is CPE Not Required
        cpeDispatchEmail: new FormControl(null, Validators.required),
        cpeDispatchComments: new FormControl(null, Validators.required)
      }),
      requestor: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        cellphone: new FormControl(),
        email: new FormControl(),
        eventComments: new FormControl()
      }),
      disconnectInfo: new FormGroup({
        disconnectMgmtOnly: new FormControl(null, Validators.required),
        totalCustDisconnect: new FormControl(null, Validators.required),
        disconnectReasonDesc: new FormControl(null, Validators.required)
      }),
      siteStatus: new FormGroup({
        isEscalation: new FormControl(false),
        escalationReason: new FormControl(1),
        primaryRequestDate: new FormControl(new Date().setHours(new Date().getHours(), 0, 0, 0)),
        secondaryRequestDate: new FormControl(new Date().setHours(new Date().getHours() + 1, 0, 0, 0)),
        //primaryRequestDate: new FormControl(new Date()),
        //secondaryRequestDate: new FormControl(new Date()),
        escalationBusinessJustification: new FormControl(),
        useConfBridge: new FormControl(0), // Default Value is N/A
        bridgeNumber: new FormControl(),
        bridgePin: new FormControl(),
        bridgeUrl: new FormControl(),
        publishedEmailCC: new FormControl(),
        completeEmailCC: new FormControl(),
      }),
      schedule: new FormGroup({
        startDate: new FormControl(),
        endDate: new FormControl({ value: null, disabled: true }),
        eventDuration: new FormControl(60),
        extraDuration: new FormControl(0),
        assignedToId: new FormControl(),
        assignedTo: new FormControl(),
        displayedAssignedTo: new FormControl()
      })
    });
    this.option = {
      requestor: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["cellphone", "cellPhnNbr"],
            ["email", "emailAdr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for requestor" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Requestor is required" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        email: {
          isShown: true
        },
        eventComments: {
          isShown: true
        }
      },
      siteStatus: {
        isEscalation: {
          isShown: true
        },
        escalationReason: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Escalation Reason is required" }
          ]
        },
        primaryRequestDate: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Primary Request Date is required" }
          ]
        },
        secondaryRequestDate: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Secondary Request Date is required" }
          ]
        },
        escalationBusinessJustification: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Escalation Bus. Justification is required" }
          ]
        },
        useConfBridge: {
          isShown: true
        },
        userFinderForPublishedEmail: {
          isEnabled: true,
          searchQuery: "publishedEmailCC",
          mappings: [
            ["publishedEmailCC", "emailAdr"]
          ]
        },
        userFinderForCompleteEmail: {
          isEnabled: true,
          searchQuery: "completeEmailCC",
          mappings: [
            ["completeEmailCC", "emailAdr"]
          ]
        }
      },
      schedule: {
        userFinder: {
          isEnabled: true,
          isMultiple: true,
          searchQuery: "displayedAssignedTo",
          mappings: [
            ["assignedToId", "userId"],
            ["assignedTo", "dsplNme"],
            ["displayedAssignedTo", "dsplNme"]
          ]
        },
        softAssign: {
          isShown: false
        },
        checkCurrentSlot: {
          isShown: false,
        },
        launchPicker: {
          isShown: false,
        }
      },
      lookup_h1: {
        isEnabled: true,
        searchQuery: "h1",
        //mappings: [
        //  ["h1", "h1"],
        //  ["h6", "h6"],
        //  ["customer.name", "custNme"],
        //  ["customer.email", "custEmailAdr"],
        //  ["customer.cellphone", "custCntctCellPhnNbr"],
        //  ["customer.phone", "custCntctPhnNbr"],
        //  ["customer.contactName", "custCntctNme"],
        //  ["customer.pager", "custCntctPgrNbr"],
        //  ["customer.pagerPin", "custCntctPgrPinNbr"]
        //]
      },
      lookup_site: {
        isEnabled: true,
        h6: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "H6 is required" }
          ]
        },
        ccd: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "CCD is required" }
          ]
        },
        mappings: [
          ["installSite.poc", "instlSitePocNme"],
          ["installSite.phoneCode", "instlSitePocIntlPhnCd"],
          ["installSite.phone", "instlSitePocPhnNbr"],
          ["serviceAssurance.poc", "srvcAssrnPocNme"],
          ["serviceAssurance.phoneCode", "srvcAssrnPocIntlPhnCd"],
          ["serviceAssurance.phone", "srvcAssrnPocPhnNbr"],
          ["address", "streetAdr"],
          ["bldg_flr_rm", "flrBldgNme"],
          ["city", "ctyNme"],
          ["state", "sttPrvnNme"],
          ["country", "ctryRgnNme"],
          ["zip", "zipCd"],
          ["id", "siteId"]
        ]
      }
    }

    this.init();
  }

  init() {
    this.spinner.show();
    this.loadInitialData().then(() => {
      this.loadUcaasEventData()
      //this.loadUcaasEventData().then(() => { this.spinner.hide(); });
    });
  }

  loadInitialData() {
    return new Promise(resolve => {
      this.eventStatusList = this.helper.getEnumKeyValuePair(EVENT_STATUS);
      this.disconnectMgmtOnlyList = this.helper.getEnumKeyValuePair(RB_CONDITION);
      this.totalCustDisconnectList = this.helper.getEnumKeyValuePair(RB_CONDITION);

      let data = zip(
        this.userSrvc.getLoggedInUser(),
        this.ucaasActySrvc.getForControl(),
        this.ucaasPlanSrvc.getForControl(),
        this.ucaasProdSrvc.getForControl(),
        this.cpeDeliverySrvc.getForControl(),
        this.devManfSrvc.get(),
        this.devModelSrvc.get(),
        this.mdsTierSrvc.getForControlByEventTypeId(this.eventType),
        this.workflowSrvc.getForEvent(this.eventType, this.ucaasEvent == null ? EVENT_STATUS.Visible : this.ucaasEvent.eventStusId),
        this.userSrvc.getFinalUserProfile(this.userSrvc.loggedInUser.adid, this.eventType),
        this.systemConfigSrvc.getSysCfgByName("UCaaSTeamPDL")
      )

      data.toPromise().then(
        res => {
          let user = res[0] as User;
          if (this.id == 0) {
            //console.log(user)
            //console.log(this.userSrvc.loggedInUser)
            this.setFormControlValue("requestor.id", user.userId);
            this.setFormControlValue("requestor.name", user.fullNme);
            this.setFormControlValue("requestor.email", user.emailAdr);
            this.setFormControlValue("requestor.phone", user.phnNbr);
            this.setFormControlValue("requestor.cellphone", user.cellPhnNbr);

            this.option.requestor.userFinder.isEnabled = false;
            this.form.get('requestor.name').disable();
            this.form.get('requestor.email').disable();
            this.form.get('requestor.phone').disable();
            this.form.get('requestor.cellphone').disable();
          }

          this.ucaasActivityTypeList = res[1] as KeyValuePair[];
          this.ucaasPlanTypeList = res[2] as KeyValuePair[];
          this.ucaasProdTypeList = res[3] as KeyValuePair[];
          this.cpeDeliveryOptionList = res[4] as KeyValuePair[];
          this.deviceManfList = res[5] as DeviceManufacturer[];
          this.deviceModelList = res[6] as DeviceModel[];
          this.serviceTierList = res[7] as KeyValuePair[];
          this.workflowStatusList = (res[8] as Workflow[]);
          //console.log(this.workflowStatusList)
          var usrPrf = res[9] as UserProfile;
          this.setFormControlValue("teamPdl", res[10][0].prmtrValuTxt)
          //console.log(usrPrf)
          if (usrPrf != null) {
            this.profile = this.helper.getFinalProfileName(usrPrf);
            if (this.id == 0) {
              this.setFormControlValue("workflowStatus",
                this.profile == EEventProfile.REVIEWER ? WORKFLOW_STATUS.Submit : WORKFLOW_STATUS.Visible);
            }
            //this.isMember = this.profile == EEventProfile.MEMBER ? true : false;
            //this.isReviewer = this.profile == EEventProfile.REVIEWER ? true : false;
            //this.isActivator = this.profile == EEventProfile.ACTIVATOR ? true : false;
            //this.isMember = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('SIPTnUCaaS Event Member')) != -1;
            //this.isReviewer = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('SIPTnUCaaS Event Reviewer')) != -1;
            //this.isActivator = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('SIPTnUCaaS Event Activator')) != -1;
          }

          resolve(true);
        },
        error => {
          this.helper.notify('An error occurred while loading initial data.', Global.NOTIFY_TYPE_ERROR);
          resolve(false);
        }).then(() => {
          // Get User Access
          this.isActivator = this.userSrvc.isSIPTnUCaaSActivator();
          this.isReviewer = this.userSrvc.isSIPTnUCaaSReviewer();
          this.isMember = this.userSrvc.isSIPTnUCaaSMember();

          if(this.isMember && !this.isReviewer) {
            this.form.get("schedule.eventDuration").disable();
          }

          if (this.id == 0) {
            this.spinner.hide();
          }
        });
    });
  }

  loadUcaasEventData() {
    return new Promise(resolve => {
      if (this.id > 0) {
        let data = zip(
          this.ucaasSrvc.getById(this.id).pipe(
            concatMap(
              res => zip(
                of(res as UcaasEvent),
                this.evntAsnToUserSrvc.getByEventId((res as UcaasEvent).eventId),
                this.workflowSrvc.getForEvent(this.eventType, (res as UcaasEvent).eventStusId)
              )
            )
          ),
          this.evntLockSrvc.checkLock(this.id)
        );

        data.toPromise().then(
          res => {
            this.ucaasEvent = res[0][0] as UcaasEvent;
            //console.log(this.ucaasEvent)
            this.activators = res[0][1] as EventAsnToUser[];
            this.workflowStatusList = res[0][2] as Workflow[];
            let lockUser = res[1] as any;
            //console.log(lockUser)
            if (lockUser != null) {
              this.lockedByUser = lockUser.lockByFullName;
              this.isLocked = true;
            }

            this.setFormControlValue("eventID", this.ucaasEvent.eventId);
            this.setFormControlValue("eventStatus", this.ucaasEvent.eventStusId);
            this.setFormControlValue("itemTitle", this.ucaasEvent.eventTitleTxt);

            let custOdie = new OdieRspn({ custNme: this.ucaasEvent.custNme });
            //custOdie.custNme = this.ucaasEvent.custNme;
            this.odieRspnList.push(custOdie);

            this.setFormControlValue("h1", this.ucaasEvent.h1);
            this.setFormControlValue("customerName", this.ucaasEvent.custNme);

            this.setFormControlValue("charsID", this.ucaasEvent.charsId);
            this.setFormControlValue("teamPdl", this.ucaasEvent.custAcctTeamPdlNme);
            this.setFormControlValue("customerSow", this.ucaasEvent.custSowLocTxt);
            this.setFormControlValue("ucaasProdType", this.ucaasEvent.ucaaSProdTypeId);
            this.setFormControlValue("ucaasPlanType", this.ucaasEvent.ucaaSPlanTypeId);
            this.setFormControlValue("ucaasActivityType", this.ucaasEvent.ucaaSActyTypeId);
            this.setFormControlValue("ucaasDesignDoc", this.ucaasEvent.ucaaSDesgnDoc);
            this.setFormControlValue("shortDesc", this.ucaasEvent.shrtDes);

            this.odieRedesignDevInfoList = [];
            //this.showReloadRedesignButton = this.ucaasEvent.ucaaSActyTypeId == EUcaasActivityType.Disconnect ? false : true;
            this.ucaasEventOdieDevice = this.helper.isEmpty(this.ucaasEvent.ucaasEventOdieDevice) ? [] : this.ucaasEvent.ucaasEventOdieDevice.filter(i => i.odieDevNme != "No ODIE Device");
            this.ucaasRedesignDevInfoList = this.helper.isEmpty(this.ucaasEvent.ucaasEventOdieDevice)
              || this.ucaasEvent.ucaasEventOdieDevice.length == 0 ? [] : this.ucaasEvent.ucaasEventOdieDevice.filter(i => i.odieDevNme != "No ODIE Device");
            this.ucaasBillActivityList = this.helper.isEmpty(this.ucaasEvent.ucaasEventBilling)
              || this.ucaasEvent.ucaasEventBilling.length == 0 ? [] : this.ucaasEvent.ucaasEventBilling;
            this.ucaasEventCpeDevList = this.helper.isEmpty(this.ucaasEvent.eventCpeDev)
              || this.ucaasEvent.eventCpeDev.length == 0 ? [] : this.ucaasEvent.eventCpeDev;
            //if (this.helper.isEmpty(this.ucaasEvent.eventCpeDev)
            //  || this.ucaasEvent.eventCpeDev.length <= 0) {
            //  this.ucaasRedesignDevInfoList.unshift(
            //    new UcaasEventOdieDevice({
            //      eventId: 0,
            //      rdsnNbr: null,
            //      rdsnExpDt: null,
            //      odieDevNme: "No ODIE Device",
            //      redsgnDevId: 0,
            //      devModelId: 0,
            //      manfId: 0
            //    }));
            //  this.ucaasEventCpeDevList.forEach(i => i.odieDevNme = "No ODIE Device")
            //}
            this.devCompletionList = this.helper.isEmpty(this.ucaasEvent.eventDeviceCompletion)
              || this.ucaasEvent.eventDeviceCompletion.length == 0 ? [] : this.ucaasEvent.eventDeviceCompletion;
            this.deviceSrvcMngmtList = this.helper.isEmpty(this.ucaasEvent.eventDevServiceMgmt)
              || this.ucaasEvent.eventDevServiceMgmt.length == 0 ? [] : this.ucaasEvent.eventDevServiceMgmt;
            this.odieDiscoDevList = this.helper.isEmpty(this.ucaasEvent.eventDiscoDev)
              || this.ucaasEvent.eventDiscoDev.length == 0 ? [] : this.ucaasEvent.eventDiscoDev;

            // Added by Sarah Sandoval [20210128]
            this.showDeviceCompleteTable = this.devCompletionList.length > 0;
            this.isLookupSiteIdClicked = !this.helper.isEmpty(this.ucaasEvent.h6)
              && !this.helper.isEmpty(this.ucaasEvent.h6)
              && (this.devCompletionList.length > 0
                || this.ucaasEventCpeDevList.length > 0
                || this.deviceSrvcMngmtList.length > 0);

            this.setFormControlValue("site.h6", this.ucaasEvent.h6);
            this.setFormControlValue("site.ccd", this.ucaasEvent.ccd);
            this.setFormControlValue("site.id", this.ucaasEvent.siteId);
            this.setFormControlValue("site.siteIdAddress", this.ucaasEvent.siteAdr);
            this.setFormControlValue("site.address", this.ucaasEvent.streetAdr);
            this.setFormControlValue("site.state", this.ucaasEvent.sttPrvnNme);
            this.setFormControlValue("site.bldg_flr_rm", this.ucaasEvent.flrBldgNme);
            this.setFormControlValue("site.country", this.ucaasEvent.ctryRgnNme);
            this.setFormControlValue("site.city", this.ucaasEvent.ctyNme);
            this.setFormControlValue("site.zip", this.ucaasEvent.zipCd);
            this.setFormControlValue("site.installSite.poc", this.ucaasEvent.instlSitePocNme);
            this.setFormControlValue("site.installSite.phoneCode", this.ucaasEvent.instlSitePocIntlPhnCd);
            this.setFormControlValue("site.installSite.phone", this.ucaasEvent.instlSitePocPhnNbr);
            this.setFormControlValue("site.installSite.cellphoneCode", this.ucaasEvent.instlSitePocIntlCellPhnCd);
            this.setFormControlValue("site.installSite.cellphone", this.ucaasEvent.instlSitePocCellPhnNbr);
            this.setFormControlValue("site.serviceAssurance.poc", this.ucaasEvent.srvcAssrnPocNme);
            this.setFormControlValue("site.serviceAssurance.phoneCode", this.ucaasEvent.srvcAssrnPocIntlPhnCd);
            this.setFormControlValue("site.serviceAssurance.phone", this.ucaasEvent.srvcAssrnPocPhnNbr);
            this.setFormControlValue("site.serviceAssurance.cellphoneCode", this.ucaasEvent.srvcAssrnPocIntlCellPhnCd);
            this.setFormControlValue("site.serviceAssurance.cellphone", this.ucaasEvent.srvcAssrnPocCellPhnNbr);

            this.setFormControlValue("cpe.cpeDeliveryOption", this.ucaasEvent.sprintCpeNcrId);
            this.setFormControlValue("cpe.cpeDispatchEmail", this.ucaasEvent.cpeDspchEmailAdr);
            this.setFormControlValue("cpe.cpeDispatchComments", this.ucaasEvent.cpeDspchCmntTxt);

            this.setFormControlValue("requestor.id", this.ucaasEvent.reqorUserId);
            if (this.ucaasEvent.reqorUser != null) {
              this.setFormControlValue("requestor.name", this.ucaasEvent.reqorUser.fullNme);
              this.setFormControlValue("requestor.phone", this.ucaasEvent.reqorUser.phnNbr);
              this.setFormControlValue("requestor.cellphone", this.ucaasEvent.reqorUser.cellPhnNbr);
              this.setFormControlValue("requestor.email", this.ucaasEvent.reqorUser.emailAdr);
            }

            this.setFormControlValue("disconnectInfo.disconnectMgmtOnly", this.ucaasEvent.discMgmtCd);
            this.disconnectMgmtOnly.setValue(this.ucaasEvent.discMgmtCd ? 1 : 0);
            this.setFormControlValue("disconnectInfo.totalCustDisconnect", this.ucaasEvent.fullCustDiscCd);
            this.totalCustDisconnect.setValue(this.ucaasEvent.fullCustDiscCd ? 1 : 0);
            this.setFormControlValue("disconnectInfo.disconnectReasonDesc", this.ucaasEvent.fullCustDiscReasTxt);

            this.setFormControlValue("siteStatus.isEscalation", this.ucaasEvent.esclCd);
            this.setFormControlValue("siteStatus.escalationReason", this.ucaasEvent.esclReasId);
            this.setFormControlValue("siteStatus.primaryRequestDate", this.ucaasEvent.primReqDt);
            this.setFormControlValue("siteStatus.secondaryRequestDate", this.ucaasEvent.scndyReqDt);
            this.setFormControlValue("siteStatus.escalationBusinessJustification", this.ucaasEvent.busJustnTxt);
            this.setFormControlValue("siteStatus.useConfBridge", this.ucaasEvent.cnfrcBrdgId);
            // ConfBridge#
            this.setFormControlValue("siteStatus.publishedEmailCC", this.ucaasEvent.pubEmailCcTxt);
            this.setFormControlValue("siteStatus.completeEmailCC", this.ucaasEvent.cmpltdEmailCcTxt);

            this.setFormControlValue("schedule.startDate", this.ucaasEvent.strtTmst);
            this.setFormControlValue("schedule.endDate", this.ucaasEvent.endTmst);
            this.setFormControlValue("schedule.eventDuration", this.ucaasEvent.eventDrtnInMinQty);
            this.setFormControlValue("schedule.extraDuration", this.ucaasEvent.extraDrtnTmeAmt);

            this.setFormControlValue("schedule.assignedToId", this.activators.map(a => a.asnToUserId).join(","))
            this.setFormControlValue("schedule.assignedTo", this.activators.map(a => a.userDsplNme).join("; "))
            this.setFormControlValue("schedule.displayedAssignedTo", this.activators.map(a => a.userDsplNme).join("; "))

            this.showEditButton = this.workflowStatusList.length > 0;
            if (this.workflowStatusList.findIndex(i => i.wrkflwStusId == this.ucaasEvent.wrkflwStusId) == -1) {
              this.workflowStatusList.push(new Workflow({
                wrkflwStusId: this.ucaasEvent.wrkflwStusId,
                wrkflwStusDes: WORKFLOW_STATUS[this.ucaasEvent.wrkflwStusId]
              }));
            }
            this.setFormControlValue("workflowStatus", this.ucaasEvent.wrkflwStusId);
          },
          error => {
            this.helper.notify('An error occurred while retrieving the UCaaS Event data.', Global.NOTIFY_TYPE_ERROR);
            resolve(false);
          }).then(() => {
            // Hide Redesign Info for Smart UC and Smart UC Toll Free
            if (this.ucaasProdType.value == EUcaasProdType.SMART_UC
              || this.ucaasProdType.value == EUcaasProdType.SMART_UC_TOLL_FREE) {
              this.hideRedesignInfo = true;
            }

            this.spinner.hide();
            this.disableForm();

            // Site Lookup
            //if (!this.helper.isEmpty(this.ucaasEvent.h6) && !this.helper.isEmpty(this.ucaasEvent.ccd)) {
            //  let date = moment(this.ucaasEvent.ccd).format("MM/DD/YYYY")
            //  this.searchSrvc.getSiteLookup(this.ucaasEvent.h6, date, 'Y', false).subscribe(
            //    res => {
            //      this.onSiteLookupResult(res);
                  
            //    }
            //  );
            //}
          });
      }

      resolve(true);
    });
  }

  disableForm() {
    this.editMode = false;
    this.form.disable();
    //if (!this.helper.isEmpty($('#btnSowGo')) && $('#btnSowGo').length > 0) {
    //  $('#btnSowGo')[0].disabled = true;
    //}
    this.option.requestor.userFinder.isEnabled = false;
    this.option.siteStatus.userFinderForPublishedEmail.isEnabled = false;
    this.option.siteStatus.userFinderForCompleteEmail.isEnabled = false;
    this.option.schedule.userFinder.isEnabled = false;
    this.option.lookup_h1.isEnabled = false;
    this.option.lookup_site.isEnabled = false;
  }

  onUcaasProdTypeChanged(event: any) {
    let ucaasProdTypeId = event.selectedItem.value;
    if (ucaasProdTypeId == EUcaasProdType.SMART_UC) {
      this.ucaasPlanType.setValue(EUcaasPlanType.UCB);
      this.ucaasPlanType.disable();
    }
    else if (ucaasProdTypeId == EUcaasProdType.SMART_UC_TOLL_FREE) {
      this.ucaasPlanType.setValue(EUcaasPlanType.UCT);
      this.ucaasPlanType.disable();
    }
    else {
      this.ucaasPlanType.enable();
    }

    if (ucaasProdTypeId == EUcaasProdType.SIPT || ucaasProdTypeId == EUcaasProdType.SDV) {
      this.ucaasActivityType.setValue(EUcaasActivityType.Change_SPS);
      this.ucaasActivityType.disable();
    }
    else {
      this.ucaasActivityType.enable();
    }

    if (this.ucaasRedesignDevInfoList != null && this.ucaasRedesignDevInfoList.length > 0) {
      let manf = this.deviceManfList.find(i => i.manfNme == "T-Mobile");
      this.ucaasRedesignDevInfoList.forEach(i => {
        if (i.manfId == 0) {
          i.manfId == manf.manfId;
          let modelNme: string;
          if (ucaasProdTypeId == EUcaasProdType.CISCO_HCS) {
            modelNme == "HCS";
          }
          else if (ucaasProdTypeId == EUcaasProdType.MIPT) {
            modelNme == "MIPT";
          }
          else if (ucaasProdTypeId == EUcaasProdType.SMART_UC
            || ucaasProdTypeId == EUcaasProdType.SMART_UC_TOLL_FREE) {
            modelNme == "Smart";
          }

          let model = this.deviceModelList.find(i => i.devModelNme == modelNme);
          i.devModelId = model.devModelId;
        }
      });
    }
  }

  onUcaasPlanTypeChanged(event: any) {
    let ucaasPlanTypeId = event.selectedItem.value;
    if (ucaasPlanTypeId == EUcaasPlanType.SCC || ucaasPlanTypeId == EUcaasPlanType.UC7) {
      this.ucaasBillActySrvc.getByUcaasPlanTypeId(ucaasPlanTypeId).subscribe(
        res => {
          this.ucaasBillActivityList = res;

          if (this.id > 0) {
            // For update, retrieve what is saved on DB
            //this.ucaasBillActivityList = this.ucaasEvent.ucaasEventBilling;
            this.ucaasBillActivityList.forEach(item => {
              let row = this.ucaasEvent.ucaasEventBilling.find(i => i.ucaaSBillActyId == item.ucaaSBillActyId);
              if (row != null && row.ucaaSBillActyId == item.ucaaSBillActyId) {
                item.incQty = row.incQty;
                item.dcrQty = row.dcrQty;
              }
            });
          }
          else {
            // For new Ucaas Event, create new table
            this.ucaasBillActivityList.forEach(item => {
              item.incQty = 0;
              item.dcrQty = 0;
            });
          }
        }
      );
    }
  }

  onH1LookupResult(result: OdieRspn[]) {
    this.odieRspnList = result.filter(i => !this.helper.isEmpty(i.custNme))
    if (result.length == 1) {
      this.form.get("customerName").setValue(result[0].custNme)
    }

    // Added by Sarah Sandoval for IM6034407 [20210126]
    // Smart UC and Smart UC Toll Free can populate Customer Name through H6/CCD lookup
    if (this.odieRspnList.length <= 0) {
      this.errors = [];
      this.errors.push(ErrorMsg.EVENT_H1_NOCUSTOMERNAME_UCAAS);
    }
  }

  onOdieCustomerNameChanged(e) {
    if (this.form.get("eventStatus").value != EVENT_STATUS.Completed) {
      let selected = this.odieRspnList.find(a => a.custNme == e.value) as OdieRspn;
      if (selected != null) {
        this.customerSow.setValue(selected.sowsFoldrPathNme);
        //this.teamPdl.setValue(selected.custTeamPdl);
        this.mnspmId = +selected.mnspmId;
      }

      this.reloadRedesign();
    }
  }

  reloadRedesign() {
    let custName = this.getFormControlValue("customerName");
    let h1 = this.getFormControlValue("h1");

    if (this.helper.isEmpty(custName) || this.helper.isEmpty(h1)) {
      this.helper.notify('H1 and/or Customer Name is required.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.spinner.show();
    this.searchSrvc.retrieveRedesign(h1, custName, this.eventType).subscribe(
      res => {
        if (res.length > 0) {
          this.showRedesignError = false;
          this.odieRedesignDevInfoList = res;
        } else {
          this.showRedesignError = true;
          this.odieRedesignDevInfoList = [];
          this.helper.notify(this.h1ApprovedRedesignError, Global.NOTIFY_TYPE_WARNING);
        }
      }, error => {
        this.spinner.hide();
        this.helper.notify('Error in getting approved redesign data associated for this H1.', Global.NOTIFY_TYPE_ERROR);
      }, () => {
        this.spinner.hide();
      }
    );
  }

  selectionOdieRedesignDev(data: any) {
    this.selectedOdieRedesignDevKeys = data.selectedRowKeys;
  }

  addSelectedOdieRedesignDevInfo() {
    let odieSelectedDev = this.odieRedesignDevInfoList.filter(i => this.selectedOdieRedesignDevKeys.includes(i.redsgnDevId));
    //console.log(odieSelectedDev)
    if (odieSelectedDev != [] && odieSelectedDev.length > 0) {
      let devManf = this.deviceManfList.filter(i => i.manfNme == 'T-Mobile');
      let devManfId = (devManf != null && devManf.length > 0) ? devManf[0].manfId : 0;
      devManfId = this.ucaasProdType.value == EUcaasProdType.SIPT ? 0 : devManfId;
      let devModelId = 0;
      let index = 0;
      if (this.ucaasProdType.value == EUcaasProdType.CISCO_HCS) {
        index = this.deviceModelList.findIndex(i => i.devModelNme.toUpperCase() == 'HCS');
        devModelId = index > 0 ? this.deviceModelList[index].devModelId : 0;
      }
      else if (this.ucaasProdType.value == EUcaasProdType.MIPT) {
        index = this.deviceModelList.findIndex(i => i.devModelNme.toUpperCase() == 'MIPT');
        devModelId = index > 0 ? this.deviceModelList[index].devModelId : 0;
      }
      else if (this.ucaasProdType.value == EUcaasProdType.SMART_UC
        || this.ucaasProdType.value == EUcaasProdType.SMART_UC_TOLL_FREE) {
        index = this.deviceModelList.findIndex(i => i.devModelNme.toUpperCase() == 'SMART');
        devModelId = index > 0 ? this.deviceModelList[index].devModelId : 0;
      }
      else if (this.ucaasProdType.value == EUcaasProdType.SDV) {
        index = this.deviceModelList.findIndex(i => i.devModelNme.toUpperCase() == 'SDV');
        // Added by Sarah Sandoval [20210119] - to cater the change from SDV to GST
        var gstIndex = index = this.deviceModelList.findIndex(i => i.devModelNme.toUpperCase() == 'GST');
        devModelId = index > 0 ? this.deviceModelList[index].devModelId
          : (gstIndex > 0 ? this.deviceModelList[gstIndex].devModelId : 0);
      }

      //this.hideRedesignInfo = false;
      this.ucaasRedesignDevInfoList = odieSelectedDev.map(data => {
        return new UcaasEventOdieDevice({
          eventId: this.id,
          rdsnNbr: data.redesignNbr,
          rdsnExpDt: data.expirationDate,
          odieDevNme: data.devNme,
          redsgnDevId: data.redsgnDevId,
          devModelId: devModelId,
          manfId: devManfId
        });
      });
    }
    else {
      this.helper.notify('Please select the rows before using the button.', Global.NOTIFY_TYPE_WARNING);
    }
  }

  saveRedesignData() {
    if (this.ucaasRedesignDevInfoList != [] && this.ucaasRedesignDevInfoList.length > 0) {
      this.devCompletionList = this.ucaasRedesignDevInfoList.map(item => {
        //console.log(item)
        return new EventDeviceCompletion({
          eventId: this.id,
          odieDevNme: item.odieDevNme,
          h6: this.form.get('site.h6').value,
          cmpltdCd: false,
          redsgnDevId: item.redsgnDevId,
          creatDt: new Date()
        })
      });

      if (this.devCompletionList != null && this.devCompletionList.length == 1) {
        this.ucaasEventCpeDevList.forEach(item => {
          item.odieDevNme = this.devCompletionList[0].odieDevNme;
        });
      }

      this.showDeviceCompleteTable = true;
    }
    else {
      // Just for create for the meantime; will need to change implementation for updating event
      this.devCompletionList = [];
    }
  }

  onMngdActyToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'retrieveOdieTemplate'
    });
  }

  getOdieDiscoData() {
    if (this.odieDiscoDevList != [] && this.odieDiscoDevList.length > 0) {
      this.spinner.show();
      this.searchSrvc.getOdieDiscoDevData(this.h1.value, this.odieDiscoDevList).subscribe(
        res => {
          this.odieDiscoDevList = res;
          if (this.odieDiscoDevList == [] || this.odieDiscoDevList.length <= 0) {
            this.showDiscoDevError = true;
            this.helper.notify('ODIE returned no recs for these Devices.', Global.NOTIFY_TYPE_WARNING);
          }
          else {
            this.showDiscoDevError = false;
          }
        },
        error => {
          this.spinner.hide();
          this.helper.notify(`An error occurred while retrieving ODIE Disconnect Devices Data.
            ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
        },
        () => {
          this.spinner.hide();
        });
    }
  }

  //(onRowRemoved)="onRedesignDeviceRemoved($event)"
  onRedesignDeviceRemoved(e) {
    console.log(this.ucaasRedesignDevInfoList)
    if (this.ucaasRedesignDevInfoList.length <= 0) {
      this.hideRedesignInfo = true;
      this.ucaasRedesignDevInfoList.unshift(
        new UcaasEventOdieDevice({
          eventId: 0,
          rdsnNbr: null,
          rdsnExpDt: null,
          odieDevNme: "No ODIE Device",
          redsgnDevId: 0,
          devModelId: 0,
          manfId: 0
        }));

      if (this.ucaasEventCpeDevList.length > 0) {
        this.ucaasEventCpeDevList.forEach(i => i.odieDevNme = "No ODIE Device")
      }

      if (this.devCompletionList.length > 0) {
        this.devCompletionList.forEach(i => i.odieDevNme = "No ODIE Device")
      }

      if (this.deviceSrvcMngmtList.length > 0) {
        this.deviceSrvcMngmtList.forEach(i => i.odieDevNme = "No ODIE Device")
      }
    }
  }

  onVendorPreparing(e) {
    if (e.parentType === "dataRow" && e.dataField === "devModelId") {
      e.editorOptions.disabled = (typeof e.row.data.manfId !== "number")
    }
  }

  onVendorChange(rowData: any, value: any): void {
    //rowData.manfId = value;
    rowData.devModelId = null;
    (<any>this).defaultSetCellValue(rowData, value);
  }

  getDeviceModelByManfId(options) {
    return {
      store: this.deviceModelList,
      filter: options.data ? ["manfId", "=", options.data.manfId] : null
    }
  }

  onSiteLookupResult(result: UcaasEvent) {
    this.ucaasEvent = result;
    this.isLookupSiteIdClicked = true;
    this.setFormControlValue("cpe.cpeDispatchEmail", this.ucaasEvent.cpeDspchEmailAdr);

    // Removed this condition by Sarah Sandoval [20210128]
    // Smart UC and Smart UC Toll Free can still do Site Lokkup
    // For UCaaS Prod Type Smart UC and Smart UC Toll Free
    // Device Completion, CPE Table and Device Service Management table are not needed
    //if (this.ucaasProdType.value != EUcaasProdType.SMART_UC
    //  && this.ucaasProdType.value != EUcaasProdType.SMART_UC_TOLL_FREE) {
    this.ucaasEventCpeDevList = this.ucaasEvent.event.eventCpeDev;
    //this.ucaasEventCpeDevList = this.ucaasEvent.eventCpeDev;
    if (this.ucaasEvent.event.eventCpeDev != null && this.ucaasEvent.event.eventCpeDev.length > 0) {
      this.ucaasEvent.orderCode = 1;
      if (this.ucaasRedesignDevInfoList.length <= 0) {
        this.ucaasRedesignDevInfoList.unshift(
          new UcaasEventOdieDevice({
            eventId: 0,
            rdsnNbr: null,
            rdsnExpDt: null,
            odieDevNme: "No ODIE Device",
            redsgnDevId: 0,
            devModelId: 0,
            manfId: 0
          }));
        this.hideRedesignInfo = true;
        this.ucaasEventCpeDevList.forEach(i => i.odieDevNme = "No ODIE Device")
      }
    }

    if (this.devCompletionList != null && this.devCompletionList.length > 0) {
      this.devCompletionList.forEach(item => {
        item.h6 = this.form.get('site.h6').value;
        //item.h6 = this.ucaasEvent.event.eventCpeDev.find(i => i.odieDevNme == item.odieDevNme) == null
        //  ? "" : this.ucaasEvent.event.eventCpeDev.find(i => i.odieDevNme == item.odieDevNme).assocH6;
      });

      if (this.devCompletionList.length == 1) {
        this.ucaasEventCpeDevList.forEach(item => {
          item.odieDevNme = this.devCompletionList[0].odieDevNme;
        });
      }
    }
    //}
    
    // Added by John Caranay for DE42507 [20210121]
    let lodi = this.ucaasEventOdieDevice;
    let lcdi = this.ucaasEventCpeDevList;
    let ldms = this.deviceSrvcMngmtList;
    let ldmsfinal = [];
    let workflowStatus = this.getFormControlValue("workflowStatus");
    let prodType = this.getFormControlValue("ucaasProdType");
    if (lodi.length > 0 && lcdi.length > 0
      && (ldms.length > 0 || (ldms.length == 0 && (workflowStatus == '4' || workflowStatus == '2'))))
    {
      lcdi.forEach(item => {
        let ldmsItem = ldms.find(dm => dm.odieDevNme == item.odieDevNme);
        if(prodType === 1) {
          ldmsfinal.push({
            odieCd : false,
            odieDevNme: item.odieDevNme,
            //deviceID: "",
            sendToOdie: (ldmsItem != null) ? ldmsItem.sendToOdie : false,
            mnsSrvcTierId: (ldmsItem != null) ? ldmsItem.mnsSrvcTierId : ""
          })
        } else {
          ldmsfinal.push({
            odieCd : false,
            odieDevNme : item.odieDevNme,
            deviceID : item.deviceId,
            mnsOrdrId : item.cpeOrdrId,
            mnsOrdrNbr : item.cpeOrdrNbr,
            sendToOdie: (ldmsItem != null) ? ldmsItem.sendToOdie : false,
            mnsSrvcTierId: (ldmsItem != null) ? ldmsItem.mnsSrvcTierId : ""
          })
        }
      });
    }

    if (ldmsfinal.length > 0)
    {
        if (prodType === 1) {
          //ldmsfinal = ldmsfinal.find(a => { a.DeviceID == ""; return a; });
          ldmsfinal = ldmsfinal.filter(i => this.helper.isEmpty(i.deviceID));
        }
        this.deviceSrvcMngmtList = ldmsfinal;
    }
    else {
      this.deviceSrvcMngmtList = [];
    }

    // Commented by John Caranay for DE42507 [20210121] due to additional code above
    // Device Service Management table
    //this.deviceSrvcMngmtList = this.ucaasEvent.eventDevServiceMgmt != null ? this.ucaasEvent.eventDevServiceMgmt : [];
    //console.log(this.deviceSrvcMngmtList)

    if (this.id == 0) {
      for (let field of this.option.lookup_site.mappings) {
        this.setFormControlValue(field[0], result[field[1]]);
      }
      let country = this.ucaasEvent.ctryRgnNme.trim().toUpperCase();
      if (country == "US" || country == "U.S" || country == "USA" || country == "U.S.A"
        || this.helper.isEmpty(country) || country == "UNITED STATE OF AMERICAN"
        || country == "UNITED STATES" || country == "UNITED STATES OF AMERICA") {
        this.ucaasEvent.usIntlCd = "D";
        this.ucaasEvent.instlSitePocIntlPhnCd = "";
        this.ucaasEvent.instlSitePocIntlCellPhnCd = "";
      }
      else
        this.ucaasEvent.usIntlCd = "I";
    }

    // Added by Sarah Sandoval for IM6034407 [20210126]
    // For Smart UC and Smart UC Toll Free Customer Name to be populated by H6/CCD lookup
    //    when H1 lookup does not have value from ODIE
    if (this.ucaasProdType.value == EUcaasProdType.SMART_UC
      || this.ucaasProdType.value == EUcaasProdType.SMART_UC_TOLL_FREE) {
      this.odieRspnList = [];
      this.odieRspnList.push(new OdieRspn({ custNme: this.ucaasEvent.custNme }));
      this.form.get("customerName").setValue(this.ucaasEvent.custNme)
    }
  }

  onViewOrderDetail(e, data) {
    e.preventDefault();
    //console.log(data)
    this.spinner.show();
    this.modalCpeDeviceList = [];
    this.modalPendingComponentList = [];
    this.modalInstalledInventoryList = [];
    //let h6 = this.form.get("site.h6").value || null;
    let ccd = this.form.get("site.ccd").value as Date || null;
    let date = moment(ccd).format("MM/DD/YYYY")
    //data.assocH6, data.ccd,
    this.searchSrvc.viewOrderDetails(data.assocH6, date, 'Y', this.id).subscribe(
      res => {
        //console.log(res)
        if (res != null) {
          this.modalCpeDeviceList = res.result1;
          this.modalPendingComponentList = res.result2;
          this.modalInstalledInventoryList = res.result3;
        }
      },
      error => { this.spinner.hide() },
      () => this.spinner.hide()
    );

    $("#viewCpeOrderDetails").modal("show");
  }

  onDeviceCompletionRowUpdate(e) {
    //console.log('onDeviceCompletionRowUpdate')
    //console.log(e.data)
    this.devComGrid.focusedRowIndex = -1;
    e.data.h6 = this.helper.removeSpaces(e.data.h6);  
  }

  onHistoryLookup() {
    this.helper.gotoEventHistory(this.id);
  }

  openCustomerSow() {
    let customerSowLink = this.getFormControlValue("customerSow");
    if (!this.helper.isEmpty(customerSowLink)) {
      window.open(customerSowLink, "_blank", "toolbar=0,location=0,menubar=0")
    }
  }

  onConfBridgeResult(result: any) {
    this.setFormControlValue("siteStatus.bridgeNumber", result[0].cnfrcBrdgNbr);
    this.setFormControlValue("siteStatus.bridgePin", result[0].cnfrcPinNbr);
    this.setFormControlValue("siteStatus.bridgeUrl", result[0].onlineMeetingAdr);
  }

  getUcaasEventFormValue() {
    return new Promise(resolve => {
      let ucaas = new UcaasEvent();

      ucaas.eventStusId = this.getFormControlValue("eventStatus");
      ucaas.eventTitleTxt = this.helper.isEmpty(this.getFormControlValue("ucaasProdType"))
        ? this.getFormControlValue("customerName")
        : this.getFormControlValue("customerName") + " "
        + this.ucaasProdTypeList.find(a => a.value == this.getFormControlValue("ucaasProdType")).description;
      ucaas.h1 = this.getFormControlValue("h1");
      ucaas.custNme = this.getFormControlValue("customerName");
      ucaas.charsId = this.getFormControlValue("charsID");
      ucaas.custAcctTeamPdlNme = this.getFormControlValue("teamPdl");
      ucaas.custSowLocTxt = this.getFormControlValue("customerSow");
      ucaas.ucaaSProdTypeId = this.getFormControlValue("ucaasProdType");
      ucaas.ucaaSPlanTypeId = this.getFormControlValue("ucaasPlanType");
      ucaas.ucaaSActyTypeId = this.getFormControlValue("ucaasActivityType");
      ucaas.ucaaSDesgnDoc = this.getFormControlValue("ucaasDesignDoc");
      ucaas.shrtDes = this.getFormControlValue("shortDesc");

      ucaas.h6 = this.getFormControlValue("site.h6");
      ucaas.ccd = this.helper.dateObjectToString(this.form.get("site.ccd").value);
      //ucaas.ccd = this.getFormControlValue("site.ccd");
      ucaas.siteId = this.getFormControlValue("site.id");
      ucaas.siteAdr = this.getFormControlValue("site.address");
      ucaas.streetAdr = this.getFormControlValue("site.address");
      ucaas.sttPrvnNme = this.getFormControlValue("site.state");
      ucaas.flrBldgNme = this.getFormControlValue("site.bldg_flr_rm");
      ucaas.ctryRgnNme = this.getFormControlValue("site.country");
      ucaas.ctyNme = this.getFormControlValue("site.city");
      ucaas.zipCd = this.getFormControlValue("site.zip");
      ucaas.instlSitePocNme = this.getFormControlValue("site.installSite.poc");
      ucaas.instlSitePocIntlPhnCd = this.getFormControlValue("site.installSite.phoneCode");
      ucaas.instlSitePocPhnNbr = this.getFormControlValue("site.installSite.phone");
      ucaas.instlSitePocIntlCellPhnCd = this.getFormControlValue("site.installSite.cellphoneCode");
      ucaas.instlSitePocCellPhnNbr = this.getFormControlValue("site.installSite.cellphone");

      //let country = this.ucaasEvent.ctryRgnNme.trim().toUpperCase();
      //if (country == "US" || country == "U.S" || country == "USA" || country == "U.S.A"
      //  || this.helper.isEmpty(country) || country == "UNITED STATE OF AMERICAN"
      //  || country == "UNITED STATES" || country == "UNITED STATES OF AMERICA") {
      //  this.ucaasEvent.usIntlCd = "D";
      //  this.ucaasEvent.instlSitePocIntlPhnCd = "";
      //  this.ucaasEvent.instlSitePocIntlCellPhnCd = "";
      //}
      //else
      //  this.ucaasEvent.usIntlCd = "I";

      ucaas.srvcAssrnPocNme = this.getFormControlValue("site.serviceAssurance.poc");
      ucaas.srvcAssrnPocIntlPhnCd = this.getFormControlValue("site.serviceAssurance.phoneCode");
      ucaas.srvcAssrnPocPhnNbr = this.getFormControlValue("site.serviceAssurance.phone");
      ucaas.srvcAssrnPocIntlCellPhnCd = this.getFormControlValue("site.serviceAssurance.cellphoneCode");
      ucaas.srvcAssrnPocCellPhnNbr = this.getFormControlValue("site.serviceAssurance.cellphone");

      ucaas.sprintCpeNcrId = this.getFormControlValue("cpe.cpeDeliveryOption");
      ucaas.cpeDspchEmailAdr = this.getFormControlValue("cpe.cpeDispatchEmail");
      ucaas.cpeDspchCmntTxt = this.getFormControlValue("cpe.cpeDispatchComments");

      ucaas.reqorUserId = this.getFormControlValue("requestor.id");
      ucaas.reqorUserCellPhnNbr = this.getFormControlValue("requestor.cellphone");
      ucaas.cmntTxt = this.getFormControlValue("requestor.eventComments");

      ucaas.discMgmtCd = this.getFormControlValue("disconnectInfo.disconnectMgmtOnly");
      ucaas.fullCustDiscCd = this.getFormControlValue("disconnectInfo.totalCustDisconnect");
      ucaas.fullCustDiscReasTxt = this.getFormControlValue("disconnectInfo.disconnectReasonDesc");

      ucaas.esclCd = this.getFormControlValue("siteStatus.isEscalation");
      if (ucaas.esclCd) {
        ucaas.esclReasId = this.getFormControlValue("siteStatus.escalationReason");
        ucaas.primReqDt = this.helper.dateObjectToString(this.form.get("siteStatus.primaryRequestDate").value);
        ucaas.scndyReqDt = this.helper.dateObjectToString(this.form.get("siteStatus.secondaryRequestDate").value);
        //let primReqDate = this.getFormControlValue("siteStatus.primaryRequestDate");
        //ucaas.primReqDt = this.helper.isEmpty(primReqDate) ? null : new Date(Date.parse(primReqDate));
        //let scndyReqDt = this.getFormControlValue("siteStatus.secondaryRequestDate");
        //ucaas.scndyReqDt = this.helper.isEmpty(scndyReqDt) ? null : new Date(Date.parse(scndyReqDt));
        ucaas.busJustnTxt = this.getFormControlValue("siteStatus.escalationBusinessJustification");
      }
      ucaas.cnfrcBrdgId = this.getFormControlValue("siteStatus.useConfBridge");
      ucaas.pubEmailCcTxt = this.getFormControlValue("siteStatus.publishedEmailCC");
      ucaas.cmpltdEmailCcTxt = this.getFormControlValue("siteStatus.completeEmailCC");

      ucaas.strtTmst = this.helper.dateObjectToString(this.form.get("schedule.startDate").value);
      ucaas.endTmst = this.helper.dateObjectToString(this.form.get("schedule.endDate").value);
      //ucaas.strtTmst = this.getFormControlValue("schedule.startDate");
      //ucaas.endTmst = this.getFormControlValue("schedule.endDate");
      ucaas.eventDrtnInMinQty = this.getFormControlValue("schedule.eventDuration");
      ucaas.extraDrtnTmeAmt = this.getFormControlValue("schedule.extraDuration");

      ucaas.wrkflwStusId = this.getFormControlValue("workflowStatus");
      ucaas.suppressEmail = this.getFormControlValue("suppressEmail");
      ucaas.activators = this.getSelectedActivators(ucaas.wrkflwStusId);

      if ((ucaas.ucaaSProdTypeId == 1 && (ucaas.ucaaSPlanTypeId == 1 || ucaas.ucaaSPlanTypeId == 4))
        || (ucaas.ucaaSProdTypeId == 2 && (ucaas.ucaaSPlanTypeId == 1 || ucaas.ucaaSPlanTypeId == 4)
          && (ucaas.ucaaSActyTypeId == 4 || ucaas.ucaaSActyTypeId == 5 || ucaas.ucaaSActyTypeId == 6))) {
        ucaas.ucaasEventBilling = this.ucaasBillActivityList;
      }

      //if (ucaas.ucaaSProdTypeId != 5 && ucaas.ucaaSProdTypeId != 6
      //  && ucaas.ucaaSPlanTypeId != 1 && ucaas.ucaaSPlanTypeId != 4
      //  && ucaas.ucaaSActyTypeId != 3) {
      //  ucaas.ucaasEventBilling = this.ucaasBillActivityList;
      //}

      ucaas.ucaasEventOdieDevice = this.ucaasRedesignDevInfoList.filter(i => i.odieDevNme != "No ODIE Device");
      ucaas.eventCpeDev = this.ucaasEventCpeDevList;
      ucaas.eventDeviceCompletion = this.devCompletionList;
      ucaas.eventDevServiceMgmt = this.deviceSrvcMngmtList;
      ucaas.eventDiscoDev = this.odieDiscoDevList;

      resolve(ucaas);
    });
  }

  getSelectedActivators(wrkflwStusId: number) {
    if (wrkflwStusId != WORKFLOW_STATUS.Reject && wrkflwStusId != WORKFLOW_STATUS.Reschedule) {
      if (!this.helper.isEmpty(this.getFormControlValue("schedule.displayedAssignedTo"))) {
        let mplsActivators = this.getFormControlValue("schedule.assignedToId")
        if (mplsActivators != null) {
          let activators = mplsActivators.toString()
          if (activators.length > 0) {
            if (activators.indexOf(',') != -1) {
              return activators.toString().split(",").map(a => parseInt(a));
            } else {
              return [mplsActivators];
            }
          }
        }
      }
    }

    return [];
  }

  edit() {
    // Lock Event
    this.evntLockSrvc.lock(this.id).subscribe(
      res => {
        this.editMode = true;
        this.form.enable();
        this.form.get("itemTitle").disable();
        this.form.get("eventID").disable();
        this.form.get("eventStatus").disable();
        this.option.requestor.userFinder.isEnabled = false;
        this.form.get('requestor.name').disable();
        this.form.get('requestor.email').disable();
        this.form.get('requestor.phone').disable();
        this.form.get('requestor.cellphone').disable();
        this.option.siteStatus.userFinderForPublishedEmail.isEnabled = true;
        this.option.siteStatus.userFinderForCompleteEmail.isEnabled = true;
        this.option.schedule.userFinder.isEnabled = true;
        this.option.lookup_h1.isEnabled = true;
        this.option.lookup_site.isEnabled = true;

        let ucaasProdTypeId = this.ucaasEvent.ucaaSProdTypeId;
        if (ucaasProdTypeId == EUcaasProdType.SMART_UC) {
          this.ucaasPlanType.setValue(EUcaasPlanType.UCB);
          this.ucaasPlanType.disable();
        }
        else if (ucaasProdTypeId == EUcaasProdType.SMART_UC_TOLL_FREE) {
          this.ucaasPlanType.setValue(EUcaasPlanType.UCT);
          this.ucaasPlanType.disable();
        }
        else {
          this.ucaasPlanType.enable();
        }

        if (ucaasProdTypeId == EUcaasProdType.SIPT || ucaasProdTypeId == EUcaasProdType.SDV) {
          this.ucaasActivityType.setValue(EUcaasActivityType.Change_SPS);
          this.ucaasActivityType.disable();
        }
        else {
          this.ucaasActivityType.enable();
        }
      },
      error => {
        this.helper.notify('An error occurred while locking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  // TO DO
  enableDisable() {
    let eventStus = this.getFormControlValue("eventStatus");

    if (this.profile == EEventProfile.ACTIVATOR) {

      // Allow WF, FailCodes, Edit and Cancel
      // set condition

      // EventStatus allow saving
      if (eventStus == EVENT_STATUS.Published || eventStus == EVENT_STATUS.InProgress
        || eventStus == EVENT_STATUS.Fulfilling || eventStus == EVENT_STATUS.Shipped
        || eventStus == EVENT_STATUS.OnHold) {

        // allow saving
      }

      // If EventStatus completed WF disable
      if (eventStus == EVENT_STATUS.Completed) {
        //WF disable
      }
    }
    else if (this.profile == EEventProfile.REVIEWER) {

      // H1 and Customer Name enebled when stus is visible
      if (eventStus == EVENT_STATUS.Visible) { }

      // Refresh ODIE Data enable
      if (eventStus == EVENT_STATUS.Visible || eventStus == EVENT_STATUS.Pending
        || (eventStus == EVENT_STATUS.Rework && (this.ucaasEvent != null && this.ucaasEvent.wrkflwStusId == WORKFLOW_STATUS.Retract))
        || (eventStus == EVENT_STATUS.Rework && (this.ucaasEvent != null && this.ucaasEvent.wrkflwStusId == WORKFLOW_STATUS.Return))) { }
    }
    else if (this.profile == EEventProfile.MEMBER && (eventStus == EVENT_STATUS.Published
      || eventStus == EVENT_STATUS.Shipped || eventStus == EVENT_STATUS.Pending)) {
      // Allow WF, Edit, Cancel and Save
      // set condition

    }
    else if (this.profile == EEventProfile.MEMBER
      && ((eventStus == EVENT_STATUS.Published || eventStus == EVENT_STATUS.Pending))) {
      // Allow WF, Edit, Cancel and Save
      // set condition

      // H1, CustomerName, Refresh ODIE, Ucaas Event Odie Devices
      if (eventStus == EVENT_STATUS.Visible) { }
    }
    else {
      // Saving and Edit Not Allowed; View Only

    }
  }

  validateForm() {
    return new Promise(resolve => {
      // Get original Start/End Date
      var oldStartDate = this.id > 0 ? this.helper.dateObjectToString(this.ucaasEvent.strtTmst) : null;
      var oldEndDate = this.id > 0 ? this.helper.dateObjectToString(this.ucaasEvent.endTmst) : null;

      this.getUcaasEventFormValue().then(res => {
        this.ucaasEvent = res as UcaasEvent;
        let ucaas = res as UcaasEvent;
        //console.log(ucaas)
        this.errors = [];

        // No validation on any rework statuses (Retract/Reschedule/Return/Reject/Delete)
        if (ucaas.wrkflwStusId != WORKFLOW_STATUS.Retract && ucaas.wrkflwStusId != WORKFLOW_STATUS.Reschedule
          && ucaas.wrkflwStusId != WORKFLOW_STATUS.Return && ucaas.wrkflwStusId != WORKFLOW_STATUS.Reject
          && ucaas.wrkflwStusId != WORKFLOW_STATUS.Delete) {

          if (this.h1.invalid) {
            if (this.h1.errors.required) {
              this.errors.push("H1 is required.");
            }
            else if (this.h1.errors.invalidH1) {
              this.errors.push("H1 field is not in correct format. H1 should contain 9 digits only.");
            }
          }

          if (this.customerName.invalid && this.customerName.errors.required) {
            this.errors.push("Customer Name is required.");
          }

          if (this.ucaasProdType.invalid && this.ucaasProdType.errors.required) {
            this.errors.push("UCaaS Product is required.");
          }

          if (this.ucaasPlanType.invalid && this.ucaasPlanType.errors.required) {
            this.errors.push("UCaaS Plan Type is required.");
          }

          if (this.ucaasActivityType.invalid && this.ucaasActivityType.errors.required) {
            this.errors.push("UCaaS Activity is required.");
          }

          if (this.teamPdl.invalid && this.teamPdl.errors.required) {
            this.errors.push("Team PDL is required.");
          }

          if (this.customerSow.invalid && this.customerSow.errors.required) {
            this.errors.push("Customer SOW is required.");
          }

          // For Activity Type: Disconnect
          if (this.ucaasActivityType.value == EUcaasActivityType.Disconnect) {
            if (this.ucaasProdType.value == EUcaasProdType.CISCO_HCS
              || this.ucaasProdType.value == EUcaasProdType.MIPT) {
              if (ucaas.eventDiscoDev == null || ucaas.eventDiscoDev.length <= 0) {
                this.errors.push("Disconnect Redesign/ODIE Table cannot be blank.");
              }

              if (this.disconnectMgmtOnly.invalid && this.disconnectMgmtOnly.errors.required) {
                this.errors.push("'Disconnect of Management only?' field is required.");
              }

              if (this.totalCustDisconnect.invalid && this.totalCustDisconnect.errors.required) {
                this.errors.push("'Is this a total customer disconnect?' field is required.");
              }

              if (this.disconnectReasonDesc.invalid && this.disconnectReasonDesc.errors.required) {
                if (this.helper.isEmpty(this.totalCustDisconnect.value) || this.totalCustDisconnect.value == 0)
                  this.errors.push("Disconnect Short Description is required.");
                else
                  this.errors.push("Reason for total customer disconnect is required.");
              }
            }
          }
          else {
            // For Activity Type: Install, Move, Change - Other and Change - SPS
            //console.log('ucaasActivityType: ' + this.ucaasActivityType.value)
            if (this.ucaasActivityType.value == EUcaasActivityType.Install
              || this.ucaasActivityType.value == EUcaasActivityType.Move
              || this.ucaasActivityType.value == EUcaasActivityType.Change_Other
              || this.ucaasActivityType.value == EUcaasActivityType.Change_SPS) {

              if (this.shortDesc.invalid && this.shortDesc.errors.required) {
                this.errors.push("Short Description is required.");
              }

              //console.log('ucaasProdType: ' + this.ucaasProdType.value)
              if (this.ucaasProdType.value != EUcaasProdType.SMART_UC
                && this.ucaasProdType.value != EUcaasProdType.SMART_UC_TOLL_FREE) {

                // Redesign Table Validation
                if (ucaas.ucaasEventOdieDevice != null && ucaas.ucaasEventOdieDevice.length > 0) {
                  if (ucaas.ucaasEventOdieDevice.filter(a =>
                    this.helper.isEmpty(a.rdsnNbr)
                    || this.helper.isEmpty(a.rdsnExpDt)
                    || this.helper.isReallyEmpty(a.odieDevNme)
                    || this.helper.isEmpty(a.manfId)
                    || this.helper.isEmpty(a.devModelId))
                    .length > 0) {
                    this.errors.push("Please enter the required fields in the UCaaS Redesign/ODIE Table")
                  }
                }
                else {
                  this.errors.push("UCaaS Redesign/ODIE Table is required.");
                }

                // Device Completion Table Validation
                if (ucaas.eventDeviceCompletion != null && ucaas.eventDeviceCompletion.length > 0) {
                  if (ucaas.eventDeviceCompletion.filter(a =>
                    this.helper.isReallyEmpty(a.odieDevNme)
                    || this.helper.isReallyEmpty(a.h6))
                    .length > 0) {
                    this.errors.push("Please enter the required fields in the Device Completion Table")
                  } else if (ucaas.eventDeviceCompletion.filter(a => a.h6.length != 9 ).length > 0) {
                    this.errors.push("Please enter a valid H6 in the Device Completion Table")
                  }
                }
                else {
                  this.errors.push("Device Completion Table is required.");
                }
              }

              // CPE Device Table Validation
              if (ucaas.eventCpeDev != null && ucaas.eventCpeDev.length > 0) {
                if (ucaas.eventCpeDev.filter(a =>
                  this.helper.isReallyEmpty(a.odieDevNme))
                  .length > 0) {
                  this.errors.push("Please enter the required fields in the CPE Table")
                }
                if (ucaas.eventCpeDev.filter(a =>
                  a.odieDevNme == "No ODIE Device")
                  .length > 0) {
                  this.errors.push("Please make sure ODIE Device Name is populated in CPE Device Table")
                }
              }

              // Device Service Management Table
              if (ucaas.eventDevServiceMgmt != null && ucaas.eventDevServiceMgmt.length > 0) {
                if (ucaas.eventDevServiceMgmt.filter(a =>
                  this.helper.isEmpty(a.mnsSrvcTierId) || a.mnsSrvcTierId == 0
                  || this.helper.isEmpty(a.odieDevNme))
                  .length > 0) {
                  this.errors.push("Please enter the required fields in the Device Management Table")
                }
              }
            }
            else {
              // Additional Validation added by Sarah Sandoval [20210113]
              // For Change - (Add / Remove User, User Featured Group Change and Programming Change)
              // When Lookup SiteID is clicked the following are required
              if (!this.helper.isEmpty(this.form.get('site.h6').value)
                && !this.helper.isEmpty(this.form.get('site.ccd').value)) {
                if (ucaas.ucaasEventOdieDevice != null && ucaas.ucaasEventOdieDevice.length > 0) {
                  if (ucaas.ucaasEventOdieDevice.filter(a =>
                    this.helper.isEmpty(a.rdsnNbr)
                    || this.helper.isEmpty(a.rdsnExpDt)
                    || this.helper.isReallyEmpty(a.odieDevNme)
                    || this.helper.isEmpty(a.manfId)
                    || this.helper.isEmpty(a.devModelId))
                    .length > 0) {
                    this.errors.push("Please enter the required fields in the UCaaS Redesign/ODIE Table")
                  }
                }

                // Device Completion Table Validation
                if (ucaas.eventDeviceCompletion != null && ucaas.eventDeviceCompletion.length > 0) {
                  if (ucaas.eventDeviceCompletion.filter(a =>
                    this.helper.isReallyEmpty(a.odieDevNme)
                    || this.helper.isReallyEmpty(a.h6))
                    .length > 0) {
                    this.errors.push("Please enter the required fields in the Device Completion Table")
                  } else if (ucaas.eventDeviceCompletion.filter(a => a.h6.length != 9).length > 0) {
                    this.errors.push("Please enter a valid H6 in the Device Completion Table")
                  }
                }

                // CPE Device Table Validation
                if (ucaas.eventCpeDev != null && ucaas.eventCpeDev.length > 0) {
                  if (ucaas.eventCpeDev.filter(a =>
                    this.helper.isReallyEmpty(a.odieDevNme))
                    .length > 0) {
                    this.errors.push("Please enter the required fields in the CPE Table")
                  }
                  if (ucaas.eventCpeDev.filter(a =>
                    a.odieDevNme == "No ODIE Device")
                    .length > 0) {
                    this.errors.push("Please make sure ODIE Device Name is populated in CPE Device Table")
                  }
                }

                // Device Service Management Table
                if (ucaas.eventDevServiceMgmt != null && ucaas.eventDevServiceMgmt.length > 0) {
                  if (ucaas.eventDevServiceMgmt.filter(a =>
                    this.helper.isEmpty(a.mnsSrvcTierId) || a.mnsSrvcTierId == 0
                    || this.helper.isReallyEmpty(a.odieDevNme))
                    .length > 0) {
                    this.errors.push("Please enter the required fields in the Device Management Table")
                  }
                }
              }
            }

            // Install/Service Assurance POC and Address are required when H6/CCD is given
            // or when the Activity Type is selected: Change - Add/Remove User,
            // Change - User Featured Group Change and Change - Programming Change
            if ((!this.helper.isEmpty(this.form.get('site.h6').value)
              && !this.helper.isEmpty(this.form.get('site.ccd').value)
              && this.isLookupSiteIdClicked)
              || (this.ucaasActivityType.value == EUcaasActivityType.Change_AddRemoveUser
                || this.ucaasActivityType.value == EUcaasActivityType.Change_UserFeatureGroupChange
                || this.ucaasActivityType.value == EUcaasActivityType.Change_ProgrammingChange)) {
              if (this.helper.isEmpty(this.installSitePoc.value)) {
                this.errors.push("Install Site POC Name is required.");
              }

              if (this.helper.isEmpty(this.installSitePhone.value)) {
                this.errors.push("Install Site POC Phone is required.");
              }

              if (this.helper.isEmpty(this.serviceAssurancePoc.value)) {
                this.errors.push("Service Assurance POC Name is required.");
              }

              if (this.helper.isEmpty(this.serviceAssurancePhone.value)) {
                this.errors.push("Service Assurance POC Phone is required.");
              }

              if (this.helper.isEmpty(this.siteAddress.value)) {
                this.errors.push("Address is required.");
              }

              if (this.helper.isEmpty(this.siteCity.value)) {
                this.errors.push("City is required.");
              }

              if (this.helper.isEmpty(this.siteState.value)) {
                this.errors.push("State/Province is required.");
              }

              if (this.helper.isEmpty(this.siteCountry.value)) {
                this.errors.push("Country is required.");
              }

              if (this.helper.isEmpty(this.siteZip.value)) {
                this.errors.push("Zip is required.");
              }
            }

            // CPE Option Validation
            if (this.cpeDeliveryOption.invalid && this.cpeDeliveryOption.errors.required) {
              this.errors.push("CPE Delivery Options is required.");
            }
            else {
              if (this.cpeDeliveryOption.value == 2 || this.cpeDeliveryOption.value == 9) {
                if (this.cpeDispatchEmail.invalid && this.cpeDispatchEmail.errors.required) {
                  this.errors.push("CPE Dispatch Email Address is required.");
                }

                if (this.cpeDispatchComments.invalid && this.cpeDispatchComments.errors.required) {
                  this.errors.push("CPE Dispatch Comments is required.");
                }
              }
            }
          }

          //console.log('siteStatus.isEscalation: ' + this.form.get('siteStatus.isEscalation').value)
          //console.log('siteStatus.escalationBusinessJustification: ' + this.form.get('siteStatus.escalationBusinessJustification').value)
          if (this.form.get('siteStatus.isEscalation').value
            && this.helper.isEmpty(this.form.get('siteStatus.escalationBusinessJustification').value)) {
            this.errors.push("Business Justification cannot be blank when Escalation is ON.");
          }

          if (this.helper.isEmpty(this.form.get('siteStatus.useConfBridge').value)) {
            this.errors.push("Will a Conference Bridge be used for this event field is required.");
          }

          if (ucaas.wrkflwStusId == WORKFLOW_STATUS.Publish
            && this.ucaasActivityType.value != EUcaasActivityType.Disconnect
            && this.helper.isEmpty(this.form.get('schedule.assignedToId').value)) {
            this.errors.push("Assigned Activator is required.");
          }

          // Added by Sarah Sandoval [20210305] - Added condition for past date but only for Visible/Submit Status as per Jagan
          var today = this.helper.dateObjectToString(new Date());
          var startDate = this.helper.dateObjectToString(this.form.get('schedule.startDate').value);
          var endDate = this.helper.dateObjectToString(this.form.get('schedule.endDate').value);
          //console.log('Start - old: ' + oldStartDate + '   new: ' + startDate + '     today: ' + today)
          //console.log('End - old: ' + oldEndDate + '     new: ' + endDate + '     today: ' + today)
          //console.log('id: ' + this.id + '  old != new: ' + (oldStartDate != startDate) + '  start date < today: ' + (startDate < today))
          //console.log('id: ' + this.id + '  old != new: ' + (oldEndDate != endDate) + '  end date < today: ' + (endDate < today))
          //console.log('Event Status: ' + this.workflowStatus.value + '  Visible/Submitted: '
          //  + (this.workflowStatus.value == WORKFLOW_STATUS.Visible || this.workflowStatus.value == WORKFLOW_STATUS.Submit))
          if ((this.id == 0 || (this.id > 0 && oldStartDate != startDate))
            && startDate < today
            && (this.workflowStatus.value == WORKFLOW_STATUS.Visible || this.workflowStatus.value == WORKFLOW_STATUS.Submit)) {
            this.errors.push('Start Date cannot be in the past.');
          }

          if ((this.id == 0 || (this.id > 0 && oldEndDate != endDate))
            && endDate < today
            && (this.workflowStatus.value == WORKFLOW_STATUS.Visible || this.workflowStatus.value == WORKFLOW_STATUS.Submit)) {
            this.errors.push('End Date cannot be in the past.');
          }
        }

        else if(this.isReviewer && (ucaas.wrkflwStusId == WORKFLOW_STATUS.Return || ucaas.wrkflwStusId == WORKFLOW_STATUS.Reject)) {
          // Device Service Management Table -- Needed to validate this field since this is a foreign key
          if (ucaas.eventDevServiceMgmt != null && ucaas.eventDevServiceMgmt.length > 0) {
            if (ucaas.eventDevServiceMgmt.filter(a =>
              this.helper.isReallyEmpty(a.mnsSrvcTierId) || a.mnsSrvcTierId == 0
              || this.helper.isReallyEmpty(a.odieDevNme))
              .length > 0) {
              this.errors.push("Please enter the required fields in the Device Management Table")
            }
          }
        }

        if (this.profile == EEventProfile.ACTIVATOR && ucaas.wrkflwStusId == WORKFLOW_STATUS.Complete) {
          // All the devices need to be completed in Device Completion table before completing the event.
          if (ucaas.eventDeviceCompletion != null && ucaas.eventDeviceCompletion.length > 0) {
            if (ucaas.eventDeviceCompletion.filter(i => !i.cmpltdCd).length > 0) {
              this.errors.push("All the devices need to be completed in Device Completion table before completing the event.")
            }
          }
        }


        if (this.errors != null && this.errors.length > 0) {
          resolve(false);
        }

        resolve(true);
      });
    });
  }

  save() {
    //isValid = false;
    //console.log('focusedRowIndex: ' + this.devComGrid.focusedRowIndex)
    if (this.devComGrid != null && this.devComGrid.focusedRowIndex > -1) {
      this.devComGrid.focusedRowIndex = -1;
      this.helper.notify('Changes made on Device Completion table is not yet saved. Please navigate away from the cell after updating it before clicking the Save button.', Global.NOTIFY_TYPE_WARNING);
      return;
    }
    this.spinner.show();
    this.validateForm().then(res => {
      console.log('validateForm: ' + res);
      console.log(this.ucaasEvent)
      if (!res) {
        //this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
        this.spinner.hide();
        return;
      }

      if (this.id > 0) {
        // Validate Event Delete Status
        //let error = this.helper.validateEventDeleteStatus(ucaas.wrkflwStusId, ucaas.eventStusId,
        //  this.ucaasEvent.entityBase.createdByUserName, this.userSrvc.loggedInUser.adid);
        //if (!this.helper.isEmpty(error)) {
        //  this.helper.notify(error, Global.NOTIFY_TYPE_WARNING);
        //  return;
        //}

        // Update Ucaas Event
        this.ucaasSrvc.update(this.id, this.ucaasEvent).toPromise().then(
          res => {
            this.helper.notify(`Successfully updated UCaaS Event - ${res.eventId}.`, Global.NOTIFY_TYPE_SUCCESS);
          },
          error => {
            this.helper.notify(`Failed to update UCaaS Event - ${this.id}. ${this.helper.getErrorMessage(error)}`,
              Global.NOTIFY_TYPE_ERROR);
          }
        ).then(() => {
          this.spinner.hide();
          this.cancel();
        });
      }
      else {
        // Create Ucaas Event
        this.ucaasSrvc.create(this.ucaasEvent).toPromise().then(
          res => {
            if (res != null) {
              this.helper.notify(`Successfully created UCaaS Event - ${res.eventId}.`, Global.NOTIFY_TYPE_SUCCESS);
            }
            else {
              this.helper.notifySavedFormMessage("UCaaS Event", Global.NOTIFY_TYPE_SUCCESS, true, null)
            }
          },
          error => {
            this.helper.notify(`Failed to create UCaaS Event. ${this.helper.getErrorMessage(error)}`,
              Global.NOTIFY_TYPE_ERROR);
          }
        ).then(() => {
          this.spinner.hide();
          this.cancel();
        });
      }
    });
  }

  cancel() {
    if (this.id > 0 && this.editMode) {
      this.unlockEvent();
    }

    this.goBackToList();
  }

  unlockEvent() {
    // Unlock Event
    if (this.id > 0) {
      this.evntLockSrvc.unlock(this.id).toPromise().then(
        res => { },
        error => {
          this.helper.notify('An error occurred while unlocking the Event.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }

  goBackToList() {
    this.router.navigate(['/event/ucaas/']);
  }

  handleWhiteSpaces(event: any, key: string) {
    let newData = event.currentTarget.value.trim();

    if(newData.length > 9) {
      newData = newData.slice(0,9);
    }
    event.currentTarget.value = newData
    this.form.get(key).setValue(newData);
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value;
    }

    return value;
  }

  onVendorUpdate(e) {
    e.data.h5H6 = this.helper.removeSpaces(e.data.h5H6);
  }
}
