import { Component, OnInit } from '@angular/core';
import { EEventType } from "./../../../shared/global";
import { EventViewsService, UserService } from "./../../../services/";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from '../../../shared';

@Component({
  selector: 'app-fedline',
  templateUrl: './fedline.component.html'
})
export class FedlineComponent implements OnInit {

  loggedInUser = <any>[];
  eventViewsList = <any>[];
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;
  public csgLevelUser: boolean;

  constructor(private eventService: EventViewsService, private router: Router,
    public helper: Helper, private spinner: NgxSpinnerService, public userService: UserService) { }

  ngOnInit() {
    this.userService.isCSGUser(localStorage.getItem('userADID'), 2).toPromise().then(
      res => {
        this.csgLevelUser = res;
        if (this.csgLevelUser) {
          this.eventType = EEventType.Fedline;
          this.eventJobAid = "";
          this.eventJobAidUrl = "";
        }
        else {
          this.spinner.hide();
          this.router.navigate(['/csg-error-page']);
        }
      },
      error => {
        this.spinner.hide();
      });  
  }

}


