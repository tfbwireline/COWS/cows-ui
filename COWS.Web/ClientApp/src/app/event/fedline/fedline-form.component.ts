import { formatDate, DatePipe } from "@angular/common";
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, AbstractControl } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, of } from "rxjs";
import { Helper } from "./../../../shared";
import {
  EVENT_STATUS, KeyValuePair, EEventType, Global, WORKFLOW_STATUS, FedlineActivityType, EEventProfile
} from "./../../../shared/global";
import {
  UserService, FedlineEventService, WorkflowService, EventLockService
} from "./../../../services/";
import {
  User, FormControlItem, Workflow, EventAsnToUser, FedlineEvent, FedlineManagedDevice, FedlineOrigDevice, FedlineConfig, FedlineTadpoleData, FedlineUserData, UserProfile
} from "./../../../models";
import { String, StringBuilder } from 'typescript-string-operations';
import { escapeLeadingUnderscores } from "typescript";

@Component({
  selector: 'app-fedline-form',
  templateUrl: './fedline-form.component.html',
  styleUrls: ['./fedline-form.component.css']
})

export class FedlineFormComponent implements OnInit, OnDestroy {

  // Page properties
  eventId: number = 0
  profile: string;
  eventType: number = EEventType.Fedline;
  fedlineEvent: FedlineEvent;
  editMode: boolean = true;
  isLocked: boolean = false;
  allowEdit: boolean = true;
  isDisconnect: boolean = false;
  origReqId: number = 0;
  lockedByUser: string;
  isRevActTask: boolean = false;

  isInstallorHeadend: boolean = false;
  isHeadend: boolean = false;
  tadpoleData: FedlineTadpoleData;
  frbRequestId: number;
  installType: string;
  orderSubType: string;
  reorderReason: string;
  isExpedite: boolean;
  frbSentDate: Date;
  deviceName: string;
  serialNum: string;
  model: string;
  origDeviceName: string;
  origSerialNum: string;
  origModel: string;

  portName: string;
  ipAddress: string;
  subnetMask: string;
  defaultGateway: string;
  speed: string;
  duplex: string;
  macAddress: string;

  designType: string;
  lanIpAddress: string;
  lanSubnetMask: string;
  lanSpeed: string;
  lanDuplex: string;

  // For UI Controls
  eventStatusList: KeyValuePair[];
  managedDeviceDataList: FedlineManagedDevice
  origDeviceDataList: FedlineOrigDevice;
  wanIpInformationList: FedlineConfig;
  lanIpInformationList: FedlineConfig;
  subscriberConfigsList: any;
  activatorWFStatusList: any;
  activatorFailCodesList: [];
  workflowStatusList: any;
  preConfigFailCodesList: any;
  producationTunnelStatusList: any;
  fedlineCCDHist: any;
  eventUpdateHistory: any;
  activators: EventAsnToUser[];

  isMNSPreConfigEng: boolean = false;
  isMDSPM: boolean = false;
  isMNSActivator: boolean = false;
  rules: any;
  engRules: any;
  pMRules: any;
  actRules: any;

  // Form Properties
  form: FormGroup;
  option: any;
  isSubmitted: boolean = false;

  preStagingEngineerGrid: any;
  hidePreStagingEngineerGrid: boolean = false;
  mnsActivatorGrid: any;
  hideMnsActivatorGrid: boolean = false;

  constructor(private avRoute: ActivatedRoute, private router: Router, private helper: Helper,
    //private datePipe: DatePipe,
    private spinner: NgxSpinnerService, private userSrvc: UserService, private fedlineEvntSrvc: FedlineEventService,
    private workflowSrvc: WorkflowService, private evntLockSrvc: EventLockService
  ) { }

  ngOnDestroy() {
    this.unlockEvent();
    this.helper.form = null;
  }

  ngOnInit() {
    this.eventId = this.avRoute.snapshot.params["id"] || 0;
    this.editMode = this.eventId > 0 ? false : true;
    this.form = new FormGroup({


      eventID: new FormControl({ value: null, disabled: true }),
      eventStatus: new FormControl({ value: EVENT_STATUS.Visible, disabled: true }),
      activityDueToMove: new FormControl({ value: null, disabled: true }),
      eventTitle: new FormControl({ value: null, disabled: true }),
      custName: new FormControl({ value: null, disabled: true }),
      custOrgID: new FormControl({ value: null, disabled: true }),
      mdsEventType: new FormControl({ value: null, disabled: true }),
      mdsActivityType: new FormControl({ value: null, disabled: true }),

      installation: new FormGroup({
        siteAddress1: new FormControl({ value: null, disabled: true }),
        primaryPOCName: new FormControl({ value: null, disabled: true }),
        siteAddress2: new FormControl({ value: null, disabled: true }),
        primaryPOCPhone: new FormControl({ value: null, disabled: true }),
        primaryPOCPhoneExtension: new FormControl({ value: null, disabled: true }),
        city: new FormControl({ value: null, disabled: true }),
        primaryPOCEmail: new FormControl({ value: null, disabled: true }),
        stateProv: new FormControl({ value: null, disabled: true }),
        secondaryPOCName: new FormControl({ value: null, disabled: true }),
        country: new FormControl({ value: null, disabled: true }),
        secondaryPOCPhone: new FormControl({ value: null, disabled: true }),
        secondaryPOCPhoneExtension: new FormControl({ value: null, disabled: true }),
        zip: new FormControl({ value: null, disabled: true }),
        secondaryPOCEmail: new FormControl({ value: null, disabled: true })
      }),
      shipping: new FormGroup({
        siteAddress1: new FormControl({ value: null, disabled: true }),
        primaryPOCName: new FormControl({ value: null, disabled: true }),
        siteAddress2: new FormControl({ value: null, disabled: true }),
        primaryPOCPhone: new FormControl({ value: null, disabled: true }),
        primaryPOCPhoneExtension: new FormControl({ value: null, disabled: true }),
        city: new FormControl({ value: null, disabled: true }),
        primaryPOCEmail: new FormControl({ value: null, disabled: true }),
        stateProv: new FormControl({ value: null, disabled: true }),
        country: new FormControl({ value: null, disabled: true }),
        zip: new FormControl({ value: null, disabled: true }),
        designDisplay: new FormControl({ value: null, disabled: true })
      }),
      manualData: new FormGroup({
        completeEmailCC: new FormControl(),
        comments: new FormControl(),
        workflowStatus: new FormControl({ value: null, disabled: this.editMode }),
        preStageFailCodes: new FormControl({ value: null, disabled: this.editMode }),
        shippingCarrier: new FormControl(),
        trackingNumber: new FormControl(),
        arrivalDate: new FormControl(),
        preStagingEngineer: new FormControl(),
        frbModifyDate: new FormControl({ value: null, disabled: true }),
        timeBlock: new FormControl({ value: null, disabled: true }),
        meetingSla: new FormControl({ value: null, disabled: true }),
        preStagingEngineerUserId: new FormControl(),
        activityDate: new FormControl({ value: null, disabled: true }),
        activatorWFStatus: new FormControl(),
        activatorFailCodes: new FormControl({ value: null, disabled: true }),
        producationTunnelStatus: new FormControl({ value: null, disabled: true }),
        mnsActivator: new FormControl(),
        mnsActivatorUserId: new FormControl(),
        hdnPreStageFailCodes: new FormControl(),
        hdnActivatorWFStatus: new FormControl(),
        hdnActivatorFailCodes: new FormControl()
      })
    });
    this.option = {
      manualData: {
        userFinderForCompleteEmail: {
          isEnabled: false,
          searchQuery: "completeEmailCC",
          mappings: [
            ["completeEmailCC", "emailAdr"]
          ]
        },
        //userFinderForPreStagingEngineerEmail: {
        //  isEnabled: false,
        //  searchQuery: "preStagingEngineer",
        //  mappings: [
        //    ["preStagingEngineer", "emailAdr"],
        //    ["preStagingEngineerUserId", "userId"]
        //  ]
        //},
        //userFinderForMnsActivatorEmail: {
        //  isEnabled: false,
        //  searchQuery: "mnsActivator",
        //  mappings: [
        //    ["mnsActivator", "emailAdr"],
        //    ["mnsActivatorUserId", "userId"]
        //  ]
        //}
      },
    }

    // Set the helper form to this form
    this.helper.form = this.form;
    this.spinner.show();

    // Get Fedline Event Data to view or update    
    if (this.eventId > 0) {
      this.fedlineEvntSrvc.getById(this.eventId).subscribe(
        res => {
          this.disableForm();
          this.fedlineEvent = res;

          if (this.fedlineEvent != null) {
            if (this.fedlineEvent.eventStatusId == 6 || this.fedlineEvent.eventStatusId == 14 || this.fedlineEvent.eventStatusId == 13) {
              if (this.fedlineEvent.eventStatusId == 6 && (this.fedlineEvent.tadpoleData.orderTypeCode == 'HEI' || this.fedlineEvent.tadpoleData.orderTypeCode == 'NCC'
                || this.fedlineEvent.tadpoleData.orderTypeCode == 'NCI' || this.fedlineEvent.tadpoleData.orderTypeCode == 'NCM'
                || this.fedlineEvent.tadpoleData.orderTypeCode == 'NDM' || this.fedlineEvent.tadpoleData.orderTypeCode == 'NIC'
                || this.fedlineEvent.tadpoleData.orderTypeCode == 'NIM')) {
                this.producationTunnelStatusList = [
                  { "tunnelStatusID": 0, "tunnelStatusDes": " " },
                  { "tunnelStatusID": 1, "tunnelStatusDes": "1 tunnel up – CC1" },
                  { "tunnelStatusID": 2, "tunnelStatusDes": "1 tunnel up – CC3" },
                  { "tunnelStatusID": 3, "tunnelStatusDes": "2 tunnel up" }
                ]
                this.helper.setFormControlValue("manualData.producationTunnelStatus", Number(this.fedlineEvent.userData.tunnelStatus));
              }
            }


            if (this.fedlineEvent.tadpoleData != null) {
              this.isInstallorHeadend = this.fedlineEvent.tadpoleData.isInstallorHeadend,
                this.isHeadend = this.fedlineEvent.tadpoleData.isHeadend,
                this.isDisconnect = this.fedlineEvent.tadpoleData.isDisconnect,
                this.frbRequestId = this.fedlineEvent.tadpoleData.frbRequestId,
                this.installType = this.fedlineEvent.tadpoleData.installType,
                this.orderSubType = this.fedlineEvent.tadpoleData.orderSubType,
                this.reorderReason = this.fedlineEvent.tadpoleData.reorderReason,
                this.isExpedite = this.fedlineEvent.tadpoleData.isExpedite,
                this.frbSentDate = this.fedlineEvent.tadpoleData.frbSentDate,

                this.fedlineEvntSrvc.getCCDHistory(this.fedlineEvent == null ? 0 : this.fedlineEvent.eventId).subscribe(
                  res => {
                    this.fedlineCCDHist = res
                  });
              this.fedlineEvntSrvc.getEventUpdateHistByEventID(this.fedlineEvent == null ? 0 : this.fedlineEvent.eventId).subscribe(
                res => {
                  this.eventUpdateHistory = res
                });

              this.workflowSrvc.getForEvent(this.eventType, this.fedlineEvent == null ? EVENT_STATUS.Visible : this.fedlineEvent.eventStatusId, this.fedlineEvent.tadpoleData.macType).subscribe(
                res => {
                  this.rules = res

                  let data = zip(
                    this.userSrvc.getLoggedInUser(),
                    this.userSrvc.getActiveUserProfiles(this.userSrvc.loggedInUser.adid, this.eventType),
                    this.fedlineEvntSrvc.getCCDHistory(this.fedlineEvent == null ? 0 : this.fedlineEvent.eventId),
                    this.fedlineEvntSrvc.getEventUpdateHistByEventID(this.fedlineEvent == null ? 0 : this.fedlineEvent.eventId),
                  )

                  data.subscribe(res => {
                    let user = res[0] as User;
                    this.helper.setFormControlValue("requestor.id", user.userId);
                    this.helper.setFormControlValue("requestor.name", user.fullNme);
                    this.helper.setFormControlValue("requestor.email", user.emailAdr);

                    let userProfiles = res[1];
                    this.isMNSPreConfigEng = userProfiles.filter(a => [215].includes(a)).length > 0 ? true : false;
                    this.isMDSPM = userProfiles.filter(a => [218].includes(a)).length > 0 ? true : false;
                    this.isMNSActivator = userProfiles.filter(a => [216].includes(a)).length > 0 ? true : false;

                    if ((this.isMNSPreConfigEng == false && this.isMNSActivator == false && this.isMDSPM == false) ||
                      (this.isMNSPreConfigEng == true && this.isMNSActivator == false && this.fedlineEvent.tadpoleData.isDisconnect) ||
                      this.fedlineEvent.eventStatusId == 6 || this.fedlineEvent.eventStatusId == 14 || this.fedlineEvent.eventStatusId == 13) {
                      this.allowEdit = false;
                    }

                    if (this.isMNSPreConfigEng)
                      this.engRules = this.rules.filter(a => a.usrPrfId == 215)
                    if (this.isMDSPM)
                      this.pMRules = this.rules.filter(a => a.usrPrfId == 218)
                    if (this.isMNSActivator)
                      this.actRules = this.rules.filter(a => a.usrPrfId == 216)

                    if (this.engRules != null && this.engRules.length > 0) {
                      this.workflowStatusList = this.engRules;
                    }
                    else if (this.pMRules != null && this.pMRules.length > 0) {
                      this.workflowStatusList = this.pMRules;
                    }
                    else
                      this.workflowStatusList = null;

                    if (this.actRules != null && this.actRules.length > 0) {
                      this.activatorWFStatusList = this.actRules;
                      if (this.fedlineEvent.tadpoleData.orderTypeCode == 'HEI' || this.fedlineEvent.tadpoleData.orderTypeCode == 'NCC'
                        || this.fedlineEvent.tadpoleData.orderTypeCode == 'NCI' || this.fedlineEvent.tadpoleData.orderTypeCode == 'NCM'
                        || this.fedlineEvent.tadpoleData.orderTypeCode == 'NDM' || this.fedlineEvent.tadpoleData.orderTypeCode == 'NIC'
                        || this.fedlineEvent.tadpoleData.orderTypeCode == 'NIM') {
                        this.producationTunnelStatusList = [
                          { "tunnelStatusID": 0, "tunnelStatusDes": " " },
                          { "tunnelStatusID": 1, "tunnelStatusDes": "1 tunnel up – CC1" },
                          { "tunnelStatusID": 2, "tunnelStatusDes": "1 tunnel up – CC3" },
                          { "tunnelStatusID": 3, "tunnelStatusDes": "2 tunnel up" }
                        ]
                      }
                    }

                  }, error => {
                    this.spinner.hide();
                  }, () => {
                    this.spinner.hide();
                  });
                });

              this.subscriberConfigsList = this.fedlineEvent.tadpoleData.configs,
                this.subscriberConfigsList = this.subscriberConfigsList.filter(a => a.portName != "WAN1" && a.portName != "PORT1")
            }

            if (this.fedlineEvent.tadpoleData != null && this.fedlineEvent.tadpoleData.managedDeviceData != null) {
              this.deviceName = this.fedlineEvent.tadpoleData.managedDeviceData.deviceName,
                this.serialNum = this.fedlineEvent.tadpoleData.managedDeviceData.serialNum,
                this.model = this.fedlineEvent.tadpoleData.managedDeviceData.model
            }

            if (this.fedlineEvent.tadpoleData != null && this.fedlineEvent.tadpoleData.origDeviceData != null) {
              this.origDeviceDataList = this.fedlineEvent.tadpoleData.origDeviceData,
                this.origReqId = this.fedlineEvent.tadpoleData.origDeviceData.origReqId,
                this.origDeviceName = this.fedlineEvent.tadpoleData.origDeviceData.deviceName,
                this.origSerialNum = this.fedlineEvent.tadpoleData.origDeviceData.serialNum,
                this.origModel = this.fedlineEvent.tadpoleData.origDeviceData.model
            }

            if (this.fedlineEvent.tadpoleData != null && this.fedlineEvent.tadpoleData.wan != null) {
              this.portName = this.fedlineEvent.tadpoleData.wan.portName,
                this.ipAddress = this.fedlineEvent.tadpoleData.wan.ipAddress,
                this.subnetMask = this.fedlineEvent.tadpoleData.wan.subnetMask,
                this.defaultGateway = this.fedlineEvent.tadpoleData.wan.defaultGateway,
                this.speed = this.fedlineEvent.tadpoleData.wan.speed,
                this.duplex = this.fedlineEvent.tadpoleData.wan.duplex,
                this.macAddress = this.fedlineEvent.tadpoleData.wan.macAddress
            }

            if (this.fedlineEvent.tadpoleData != null && this.fedlineEvent.tadpoleData.lan != null) {
              this.lanIpInformationList = this.fedlineEvent.tadpoleData.lan,
                this.designType = this.fedlineEvent.tadpoleData.designType,
                this.lanIpAddress = this.fedlineEvent.tadpoleData.lan.ipAddress,
                this.lanSubnetMask = this.fedlineEvent.tadpoleData.lan.subnetMask,
                this.lanSpeed = this.fedlineEvent.tadpoleData.lan.speed,
                this.lanDuplex = this.fedlineEvent.tadpoleData.lan.duplex
            }
          }
        }, error => {
          this.spinner.hide();
          this.helper.notify('An error occurred while retrieving the Fedline Event data.', Global.NOTIFY_TYPE_ERROR);
        }, () => {
          this.setFedlineEventFormValue();
          // Check EventLock
          this.evntLockSrvc.checkLock(this.eventId).subscribe(
            res => {
              if (res != null) {
                this.lockedByUser = res.lockByFullName;
                this.isLocked = true;
                this.disableForm();
              }
            },
            error => {
              this.spinner.hide();
              this.helper.notify('An error occurred while retrieving Event Lock status.', Global.NOTIFY_TYPE_ERROR);
            }
          );
        }
      );
    }

    this.spinner.hide();

    // Initial list data
    //let id = this.eventId
    //if (id > 0) {
    //  this.eventStatusList = this.helper.getEnumKeyValuePair(EVENT_STATUS);
    //  let data = zip(
    //    this.userSrvc.getLoggedInUser(),
    //    this.userSrvc.getFinalUserProfile(this.userSrvc.loggedInUser.adid, this.eventType),
    //    this.fedlineEvntSrvc.getById(id),
    //    this.fedlineEvntSrvc.getCCDHistory(id),
    //    this.fedlineEvntSrvc.getEventUpdateHistByEventID(id),
    //    //this.workflowSrvc.getForEvent(this.eventType, this.fedlineEvent == null ? EVENT_STATUS.Visible : this.fedlineEvent.eventStusId),      
    //  )


  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'FedlineActivityHistory_' + this.helper.formatDate(new Date());
  }

  onToolbarUpdatePreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExportingUpdate(e) {
    e.fileName = 'FedlineUpdateHistory_' + this.helper.formatDate(new Date());
  }

  workflowStatusChanged(value) {
    if (this.workflowStatusList.filter(a => a.endEventStusId == value) != null) {
      let eventRuleId = this.workflowStatusList.filter(a => a.endEventStusId == value)[0].eventRuleId
      this.fedlineEvntSrvc.getFedlineEventFailCodes(eventRuleId).subscribe(
        res => {
          this.preConfigFailCodesList = res;
          if (res.length > 0)
            this.form.get("manualData.preStageFailCodes").enable();
          else
            this.form.get("manualData.preStageFailCodes").disable();
        }
      );
    }
    else {
      this.form.get("manualData.preStageFailCodes").disable();
    }
  }

  setFedlineEventFormValue() {
    if (this.eventId > 0 && this.fedlineEvent != null) {
      this.helper.setFormControlValue("eventID", this.fedlineEvent.eventId);
      this.helper.setFormControlValue("eventStatus", this.fedlineEvent.eventStatus);
      this.helper.setFormControlValue("activityDueToMove", this.fedlineEvent.tadpoleData.isMove ? "Yes" : "No");
      this.helper.setFormControlValue("eventTitle", this.fedlineEvent.userData.eventTitle);
      this.helper.setFormControlValue("custName", this.fedlineEvent.tadpoleData.custName);
      this.helper.setFormControlValue("custOrgID", this.fedlineEvent.tadpoleData.custOrgId);
      this.helper.setFormControlValue("mdsEventType", "Fedline");
      this.helper.setFormControlValue("mdsActivityType", FedlineActivityType[this.fedlineEvent.tadpoleData.macType]);
      this.helper.setFormControlValue("workflowStatus", this.fedlineEvent.workflowStatusId);

      this.helper.setFormControlValue("installation.siteAddress1", this.fedlineEvent.tadpoleData.siteInstallInfo.siteAddress1);
      this.helper.setFormControlValue("installation.primaryPOCName", this.fedlineEvent.tadpoleData.siteInstallInfo.primaryPocName);
      this.helper.setFormControlValue("installation.siteAddress2", this.fedlineEvent.tadpoleData.siteInstallInfo.siteAddress2);
      this.helper.setFormControlValue("installation.primaryPOCPhone", this.fedlineEvent.tadpoleData.siteInstallInfo.primaryPocPhone);
      this.helper.setFormControlValue("installation.primaryPOCPhoneExtension", this.fedlineEvent.tadpoleData.siteInstallInfo.primaryPocPhoneExtension);
      this.helper.setFormControlValue("installation.city", this.fedlineEvent.tadpoleData.siteInstallInfo.city);
      this.helper.setFormControlValue("installation.primaryPOCEmail", this.fedlineEvent.tadpoleData.siteInstallInfo.primaryPocEmail);
      this.helper.setFormControlValue("installation.stateProv", this.fedlineEvent.tadpoleData.siteInstallInfo.stateProv);
      this.helper.setFormControlValue("installation.secondaryPOCName", this.fedlineEvent.tadpoleData.siteInstallInfo.secondaryPocName);
      this.helper.setFormControlValue("installation.country", this.fedlineEvent.tadpoleData.siteInstallInfo.country);
      this.helper.setFormControlValue("installation.secondaryPOCPhone", this.fedlineEvent.tadpoleData.siteInstallInfo.secondaryPocPhone);
      this.helper.setFormControlValue("installation.secondaryPOCPhoneExtension", this.fedlineEvent.tadpoleData.siteInstallInfo.secondaryPocPhoneExtension);
      this.helper.setFormControlValue("installation.zip", this.fedlineEvent.tadpoleData.siteInstallInfo.zip);
      this.helper.setFormControlValue("installation.secondaryPOCEmail", this.fedlineEvent.tadpoleData.siteInstallInfo.secondaryPocEmail);

      this.helper.setFormControlValue("shipping.siteAddress1", this.fedlineEvent.tadpoleData.siteShippingInfo.siteAddress1);
      this.helper.setFormControlValue("shipping.primaryPOCName", this.fedlineEvent.tadpoleData.siteShippingInfo.primaryPocName);
      this.helper.setFormControlValue("shipping.siteAddress2", this.fedlineEvent.tadpoleData.siteShippingInfo.siteAddress2);
      this.helper.setFormControlValue("shipping.primaryPOCPhone", this.fedlineEvent.tadpoleData.siteShippingInfo.primaryPocPhone);
      this.helper.setFormControlValue("shipping.primaryPOCPhoneExtension", this.fedlineEvent.tadpoleData.siteShippingInfo.primaryPocPhoneExtension);
      this.helper.setFormControlValue("shipping.city", this.fedlineEvent.tadpoleData.siteShippingInfo.city);
      this.helper.setFormControlValue("shipping.primaryPOCEmail", this.fedlineEvent.tadpoleData.siteShippingInfo.primaryPocEmail);
      this.helper.setFormControlValue("shipping.stateProv", this.fedlineEvent.tadpoleData.siteShippingInfo.stateProv);
      this.helper.setFormControlValue("shipping.country", this.fedlineEvent.tadpoleData.siteShippingInfo.country);
      this.helper.setFormControlValue("shipping.zip", this.fedlineEvent.tadpoleData.siteShippingInfo.zip);
      this.helper.setFormControlValue("shipping.designDisplay", this.fedlineEvent.tadpoleData.designDisplay);

      this.helper.setFormControlValue("manualData.completeEmailCC", this.fedlineEvent.userData.emailCcpdl);
      this.helper.setFormControlValue("manualData.shippingCarrier", this.fedlineEvent.userData.shippingCarrier);
      this.helper.setFormControlValue("manualData.trackingNumber", this.fedlineEvent.userData.trackingNumber);
      this.helper.setFormControlValue("manualData.arrivalDate", this.fedlineEvent.userData.arrivalDate);
      this.helper.setFormControlValue("manualData.preStagingEngineer", this.fedlineEvent.userData.preStagingEngineerUserDisplay);
      this.helper.setFormControlValue("manualData.preStagingEngineerUserId", this.fedlineEvent.userData.preStagingEngineerUserId);
      this.helper.setFormControlValue("manualData.activityDate", formatDate(this.fedlineEvent.tadpoleData.startDate, 'MM/dd/yyyy', 'en'));
      this.helper.setFormControlValue("manualData.frbModifyDate", this.fedlineEvent.tadpoleData.frbModifyDate);
      this.helper.setFormControlValue("manualData.timeBlock", this.fedlineEvent.tadpoleData.timeBlock);
      this.helper.setFormControlValue("manualData.meetingSla", this.fedlineEvent.userData.slaCd == "N" ? "No" : "Yes");
      this.helper.setFormControlValue("manualData.mnsActivator", this.fedlineEvent.userData.mnsActivatorUserDisplay);
      this.helper.setFormControlValue("manualData.mnsActivatorUserId", this.fedlineEvent.userData.mnsActivatorUserId);
      this.helper.setFormControlValue("manualData.producationTunnelStatus", Number(this.fedlineEvent.userData.tunnelStatus));

      //if (this.profile == EEventProfile.ACTIVATOR) {

      //}
      //else if (this.profile == EEventProfile.REVIEWER) {
      //  this.helper.setFormControlValue("reviewer.id", this.userSrvc.loggedInUser.userId);
      //  this.helper.setFormControlValue("reviewer.name", this.userSrvc.loggedInUser.fullName);
      //}

      this.workflowSrvc.getForEvent(this.eventType, this.fedlineEvent.eventStatusId).subscribe(
        res => {
          this.helper.setFormControlValue("workflowStatus", this.fedlineEvent.workflowStatusId);
        }
      );
    }
  }

  getFedlineEventFormValue(): any {
    //let fedline = new FedlineEvent;
    let changeList: Array<string> = new Array();
    this.fedlineEvent.userData.emailCcpdl = this.checkChange(this.helper.getFormControlValue("manualData.completeEmailCC"), this.fedlineEvent.userData.emailCcpdl, "Email CC PDL", changeList);//this.helper.getFormControlValue("manualData.completeEmailCC");
    //this.fedlineEvent.statusComments = this.checkChange(this.helper.getFormControlValue("manualData.comments"), this.fedlineEvent.statusComments, "Email CC PDL", changeList);//this.helper.getFormControlValue("manualData.comments");
    this.fedlineEvent.statusComments = this.helper.getFormControlValue("manualData.comments");
    //this.fedlineEvent.workflowStatusId = this.helper.getFormControlValue("manualData.workflowStatus");
    //this.fedlineEvent.failCodeId = this.helper.getFormControlValue("manualData.preStageFailCodes");

    this.fedlineEvent.userData.shippingCarrier = this.checkChange(this.helper.getFormControlValue("manualData.shippingCarrier"), this.fedlineEvent.userData.shippingCarrier, "Shipping Carrier", changeList);//this.helper.getFormControlValue("manualData.shippingCarrier");
    this.fedlineEvent.userData.trackingNumber = this.checkChange(this.helper.getFormControlValue("manualData.trackingNumber"), this.fedlineEvent.userData.trackingNumber, "Tracking NumberL", changeList);//this.helper.getFormControlValue("manualData.trackingNumber");
    this.fedlineEvent.userData.arrivalDate = this.checkDateChange(this.helper.getFormControlValue("manualData.arrivalDate"), this.fedlineEvent.userData.arrivalDate, "Arrival Date", changeList);//this.helper.getFormControlValue("manualData.arrivalDate");

    if (this.fedlineEvent.tadpoleData.isHeadend) {
      this.fedlineEvent.userData.preStagingEngineerUserId = this.checkNumberChange(this.helper.getFormControlValue("manualData.preStagingEngineerUserId"), this.helper.getFormControlValue("manualData.preStagingEngineer"), this.fedlineEvent.userData.preStagingEngineerUserId, this.fedlineEvent.userData.preStagingEngineerUserDisplay, "MDS PM", changeList);//this.helper.getFormControlValue("manualData.preStagingEngineerUserId");
      //this.fedlineEvent.userData.arrivalDate = this.checkChange(this.helper.getFormControlValue("manualData.preStagingEngineerUserId"), this.fedlineEvent.userData.preStagingEngineerUserId, "MDS PM", changeList);
    }
    else {
      this.fedlineEvent.userData.preStagingEngineerUserId = this.checkNumberChange(this.helper.getFormControlValue("manualData.preStagingEngineerUserId"), this.helper.getFormControlValue("manualData.preStagingEngineer"), this.fedlineEvent.userData.preStagingEngineerUserId, this.fedlineEvent.userData.preStagingEngineerUserDisplay, "Pre Config Engineer", changeList);//this.helper.getFormControlValue("manualData.preStagingEngineerUserId");
    }

    if (this.helper.getFormControlValue("manualData.workflowStatus") != null) {
      this.fedlineEvent.workflowStatusId = this.helper.getFormControlValue("manualData.workflowStatus");
      if (this.helper.getFormControlValue("manualData.preStageFailCodes") != null && this.helper.getFormControlValue("manualData.preStageFailCodes") != 0 && this.fedlineEvent.workflowStatusId == EVENT_STATUS.Rework) {
        if (this.isEmptyOrSpaces(this.helper.getFormControlValue("manualData.comments"))) {
          this.fedlineEvent.statusComments = this.helper.getFormControlValue("manualData.hdnPreStageFailCodes") + ".";
        }
        else {
          this.fedlineEvent.statusComments = this.helper.getFormControlValue("manualData.hdnPreStageFailCodes") + ".  " + this.helper.getFormControlValue("manualData.comments");
        }
        this.fedlineEvent.failCodeId = this.helper.getFormControlValue("manualData.preStageFailCodes");
        this.fedlineEvent.failCode = this.helper.getFormControlValue("manualData.hdnPreStageFailCodes");
      }
    }
    else if (this.helper.getFormControlValue("manualData.activatorWFStatus") != null) {
      this.fedlineEvent.statusComments = this.helper.getFormControlValue("manualData.comments");
      this.fedlineEvent.workflowStatusId = this.helper.getFormControlValue("manualData.activatorWFStatus");
      this.fedlineEvent.workflowStatus = this.helper.getFormControlValue("manualData.hdnActivatorWFStatus");

      if (this.helper.getFormControlValue("manualData.activatorFailCodes") != null && this.fedlineEvent.workflowStatusId == EVENT_STATUS.Rework) {
        if (this.isEmptyOrSpaces(this.helper.getFormControlValue("manualData.comments"))) {
          this.fedlineEvent.statusComments = this.helper.getFormControlValue("manualData.hdnActivatorFailCodes") + ". ";
        }
        else {
          this.fedlineEvent.statusComments = this.helper.getFormControlValue("manualData.hdnActivatorFailCodes") + ".  " + this.helper.getFormControlValue("manualData.comments");
        }
        this.fedlineEvent.failCodeId = this.helper.getFormControlValue("manualData.activatorFailCodes");
        this.fedlineEvent.failCode = this.helper.getFormControlValue("manualData.hdnActivatorFailCodes");
        this.fedlineEvent.userData.failCodeId = this.helper.getFormControlValue("manualData.activatorFailCodes");
      }
      else if (this.helper.getFormControlValue("manualData.activatorWFStatus") == EVENT_STATUS.CompletedDeviceInaccessible) {
        //Fail codes may or may not be used on disconnects.  If the fail code is selected, this won't be hit.
        //If it isn't used, then this will insert the failure.
        this.fedlineEvent.statusComments = "Device Inaccessible.  " + this.helper.getFormControlValue("manualData.comments");
        this.fedlineEvent.failCode = "Device Inaccessible.";
      }
      else if (this.helper.getFormControlValue("manualData.activatorWFStatus") == EVENT_STATUS.Completed && !this.fedlineEvent.tadpoleData.isDisconnect) {
        this.fedlineEvent.userData.tunnelStatus = this.helper.getFormControlValue("manualData.producationTunnelStatus");
      }
      else {
      }
    }
    this.fedlineEvent.userData.mnsActivatorUserId = this.checkNumberChange(this.helper.getFormControlValue("manualData.mnsActivatorUserId"), this.helper.getFormControlValue("manualData.mnsActivator"), this.fedlineEvent.userData.mnsActivatorUserId, this.fedlineEvent.userData.mnsActivatorUserDisplay, "MNS Activator", changeList);//this.helper.getFormControlValue("manualData.mnsActivatorUserId");

    if (this.helper.getFormControlValue("manualData.activatorWFStatus") == EVENT_STATUS.InProgress) {
      this.fedlineEvent.userData.mnsActivatorUserId = this.userSrvc.loggedInUser.userId
      this.fedlineEvent.userData.mnsActivatorUserDisplay = this.userSrvc.loggedInUser.fullName
      changeList.push(String.Format("Status updated to In Progress, MNS Activator set to: {0}", this.userSrvc.loggedInUser.fullName));
    }

    if (this.helper.getFormControlValue("manualData.workflowStatus") == EVENT_STATUS.Fulfilling) {
      this.fedlineEvent.userData.preStagingEngineerUserId = this.userSrvc.loggedInUser.userId
      this.fedlineEvent.userData.preStagingEngineerUserDisplay = this.userSrvc.loggedInUser.fullName
      if (this.fedlineEvent.tadpoleData.isHeadend)
        changeList.push(String.Format("Status updated to fulfilling, MDS PM set to: {0}", this.userSrvc.loggedInUser.fullName));
      else
        changeList.push(String.Format("Status updated to fulfilling, Pre Staging Engineer set to: {0}", this.userSrvc.loggedInUser.fullName));
    }

    if (this.helper.getFormControlValue("manualData.activatorWFStatus") == EVENT_STATUS.Rework && this.helper.getFormControlValue("manualData.activatorFailCodes") == WORKFLOW_STATUS.Reject) {
      this.fedlineEvent.userData.mnsActivatorUserId = this.userSrvc.loggedInUser.userId
      this.fedlineEvent.userData.mnsActivatorUserDisplay = this.userSrvc.loggedInUser.fullName
      changeList.push(String.Format("Status updated to Rework/Rejected, MNS Activator set to: {0}", this.userSrvc.loggedInUser.fullName));
    }

    this.fedlineEvent.changeList = changeList;
    return this.fedlineEvent;
  }

  save() {
    var fedline = this.getFedlineEventFormValue();
    this.isSubmitted = true;

    // Validate form fields
    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.validateInput(this.helper.getFormControlValue("manualData.shippingCarrier"), "Shipping Carrier", 30);
    this.validateInput(this.helper.getFormControlValue("manualData.trackingNumber"), "Tracking Number", 30);
    var arrivalDate = this.helper.getFormControlValue("manualData.arrivalDate");

    /*Case 10B: 10b) COWS will display an error message to the MDS PreConfig engineer if the event status is
         * changed to ‘shipped’ but data is missing from one of these fields:
         *   i)	    Shipping carrier
         *   ii)	Ship Tracking number
         *   iii)	Expected Arrival Date
         *
         * CR 18
         * System will not allow Fedline Preconfig Engineer to enter all zeros for the following fields:
         * 1) Shipping Carrier, 2) Shipping Tracking #, 3) Expected Arrival Date
         *
         * System will not allow Expected Arrival Date field to be current date (event edit date)
         * System will not allow Expected Arrival Date field to be a date in the past from the event edit date
        */
    //10B
    if (!this.fedlineEvent.tadpoleData.isHeadend) {
      if (this.isEmptyOrSpaces(this.helper.getFormControlValue("manualData.shippingCarrier")) && this.helper.getFormControlValue('manualData.workflowStatus') == 10) {
        this.helper.notify('Shipping Carrier is a required field when setting the status to Shipped', Global.NOTIFY_TYPE_ERROR);
        return false;
      }

      if (this.isEmptyOrSpaces(this.helper.getFormControlValue("manualData.trackingNumber")) && this.helper.getFormControlValue('manualData.workflowStatus') == 10) {
        this.helper.notify('Tracking Number is a required field when setting the status to Shipped', Global.NOTIFY_TYPE_ERROR);
        return false;
      }
      if (!this.validateDates(arrivalDate))
          return false;
    }

    //Tunnel Status Validation
    if (this.helper.getFormControlValue("manualData.activatorWFStatus") == 6 && this.helper.getFormControlValue("manualData.producationTunnelStatus") == 0
      && !this.fedlineEvent.tadpoleData.isDisconnect) {
      this.helper.notify('Please select a Tunnel Status, it is required to complete Events.', Global.NOTIFY_TYPE_ERROR);
      return false;
    }

    //Fail code validation
    if (this.helper.getFormControlValue("manualData.activatorWFStatus") == 3 && this.helper.getFormControlValue("manualData.activatorFailCodes") == null) {
      this.helper.notify('Please enter a Fail Code when moving Event to Rework.', Global.NOTIFY_TYPE_ERROR);
      return false;
    }
    else if (this.helper.getFormControlValue("manualData.activatorWFStatus") == 3 && this.helper.getFormControlValue("manualData.activatorFailCodes") == 9) {
      if (this.isEmptyOrSpaces(this.helper.getFormControlValue("manualData.comments"))) {
        this.helper.notify('Comments are required to reject an order.  Please describe in detail what is wrong with the order', Global.NOTIFY_TYPE_ERROR);
        return false;
      }
    }

    if (this.helper.getFormControlValue("manualData.workflowStatus") == 3 && this.helper.getFormControlValue("manualData.preStageFailCodes") == null) {
      this.helper.notify('Please enter a Fail Code when moving Event to Rework.', Global.NOTIFY_TYPE_ERROR);
      return false;
    }
    else if (this.helper.getFormControlValue("manualData.workflowStatus") == 3 && this.helper.getFormControlValue("manualData.preStageFailCodes") == 9) {
      if (this.isEmptyOrSpaces(this.helper.getFormControlValue("manualData.comments"))) {
        this.helper.notify('Comments are required to reject an order.  Please describe in detail what is wrong with the order', Global.NOTIFY_TYPE_ERROR);
        return false;
      }
    }

    //Disconnect Due Date
    //b.	COWS should not allow an event to be set to In Progress prior to the Disconnect Request date.
    //Check status, if they are updating status to 5 - In progress, and that the date is not greater than today.
    if (this.fedlineEvent.eventStatusId == 15 && this.helper.getFormControlValue("manualData.activatorWFStatus") == 5 &&
      this.fedlineEvent.tadpoleData.activityDate != null && this.fedlineEvent.tadpoleData.activityDate > new Date()) {
      this.helper.notify('Disconnects may not be worked before the due date.', Global.NOTIFY_TYPE_ERROR);
      return false;
    }

    //MDS PM Must Assign an Activator
    if (this.fedlineEvent.tadpoleData.isHeadend) {
      if (this.helper.getFormControlValue("manualData.workflowStatus") == 10 && this.helper.getFormControlValue("manualData.mnsActivatorUserId") == null) {
        this.helper.notify('Please assign an Activator before changing the status to Shipped.', Global.NOTIFY_TYPE_ERROR);
        return false;
      }
    }

    //Check for Cancel Recieved
    if (this.helper.getFormControlValue("manualData.workflowStatus") != 13) {
      if (!this.fedlineEvntSrvc.checkCancelExists(this.fedlineEvent == null ? 0 : this.fedlineEvent.eventId)) {
        this.helper.notify('A Cancel has been recieved on this Event and no changes will be saved."', Global.NOTIFY_TYPE_ERROR);
        return false;
      }
    }

    this.spinner.show();

    if (this.eventId > 0) {
      // Update Fedline Event
      this.fedlineEvntSrvc.update(this.eventId, fedline).subscribe(res => {
        this.helper.notifySavedFormMessage("Fedline Event", Global.NOTIFY_TYPE_SUCCESS, false, null);
      }, error => {
        this.helper.notifySavedFormMessage("Fedline Event", Global.NOTIFY_TYPE_ERROR, false, error);
        this.spinner.hide();
      }, () => {
        this.cancel();
        this.spinner.hide();
      });
    }
    else {
      // Create Fedline Event
      this.fedlineEvntSrvc.create(fedline).subscribe(res => {
        if (res != null) {
          this.helper.notify(`Successfully created Fedline Event - ${res.eventId}.`, Global.NOTIFY_TYPE_SUCCESS);
        }
        else {
          this.helper.notifySavedFormMessage("Fedline Event", Global.NOTIFY_TYPE_SUCCESS, true, null)
        }
      }, error => {
        this.helper.notifySavedFormMessage("Fedline Event", Global.NOTIFY_TYPE_ERROR, true, error);
        this.spinner.hide();
      }, () => {
        this.cancel();
        this.spinner.hide();
      });
    }
  }

  edit() {
    // Lock Event
    this.evntLockSrvc.lock(this.eventId).subscribe(
      res => {
        this.editMode = true;
        this.form.get("manualData.completeEmailCC").enable();
        this.form.get("manualData.comments").enable();
        this.form.get("manualData.workflowStatus").enable();
        this.form.get("manualData.shippingCarrier").enable();
        this.form.get("manualData.trackingNumber").enable();
        this.form.get("manualData.arrivalDate").enable();
        this.form.get("manualData.preStagingEngineer").enable();
        this.form.get("manualData.activatorWFStatus").enable();
        this.form.get("manualData.producationTunnelStatus").enable();
        this.form.get("manualData.mnsActivator").enable();
        this.option.manualData.userFinderForCompleteEmail.isEnabled = true
        this.option.manualData.userFinderForPreStagingEngineerEmail.isEnabled = true
        this.option.manualData.userFinderForMnsActivatorEmail.isEnabled = true
      },
      error => {
        this.helper.notify('An error occurred while locking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  disableForm() {
    this.editMode = false;
    this.form.disable();
  }

  cancel() {
    if (this.eventId > 0 && !this.editMode) {
      this.unlockEvent();
    } else {
      this.router.navigate(['/event/fedline/']);
    }
  }

  unlockEvent() {
    // Unlock Event
    this.evntLockSrvc.unlock(this.eventId).subscribe(
      res => {
        this.router.navigate(['event/fedline/']);
      },
      error => {
        this.helper.notify('An error occurred while unlocking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }
    return value;
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  setArrivalDate() {
    let date = this.helper.dateAdd(this.getFormControlValue("manualData.arrivalDate"), "day", null)
    this.setFormControlValue("manualData.arrivalDate", date)
  }

  checkChange(txtbox: string, entityField: string, description: string, changeList: string[]) {
    //if (this.isEmptyOrSpaces(txtbox) != this.isEmptyOrSpaces(entityField)) {
    if (txtbox != (entityField || this.isEmptyOrSpaces(txtbox))){
      if (this.isEmptyOrSpaces(entityField))
        changeList.push(String.Format("{0} set to: {1}", description, txtbox));
      else if (!this.isEmptyOrSpaces(txtbox))
        changeList.push(String.Format("{0} changed from {1} to {2}", description, entityField, txtbox));
      else
        changeList.push(String.Format("{0} cleared ({1})", description, entityField));

      return txtbox;
    }
    return entityField;
  }

  checkDateChange(txtbox: Date, entityField: Date, description: string, changeList: string[]) {
    if (txtbox != null) {
      let txtboxDateString = new Date(txtbox).toISOString().slice(0, 10);

      let entityFieldString = '';
      if (entityField != null)
        entityFieldString = new Date(entityField).toISOString().slice(0, 10);

      let dt = new Date;
      try { dt = new Date(txtbox); }
      catch
      {
        changeList.push(String.Format("Attempt to change {0} to {1}, but this was not recognized as a valid date/time", description, txtbox));
        return entityField;
      }

      if (entityField != null && txtboxDateString != entityFieldString) {
        changeList.push(String.Format("{0} changed from {1} to {2}", description, entityFieldString, txtboxDateString));
      }
      else if (entityField == null)
        changeList.push(String.Format("{0} set to {1}", description, txtboxDateString));

      return dt;
    }
    else {
      if (entityField != null) {
        let entityFieldString = new Date(entityField).toISOString().slice(0, 10);
        changeList.push(String.Format("{0} entry cleared ({1})", description, entityFieldString));
        return null;
      }
      else return entityField;
    }
  }

  checkNumberChange(hf: number, txtbox: string, entityField: number, entityDisplay: string, description: string, changeList: string[]) {
    if (hf != entityField) {
      if (entityField == null)
        changeList.push(String.Format("{0} set to {1}", description, txtbox));
      else
        changeList.push(String.Format("{0} changed from {1} to {2}", description, entityDisplay, txtbox));

      return hf;
    }
    return entityField;
  }

  validateInput(txtbox: string, description: string, maxLength: number) {
    if (!this.isEmptyOrSpaces(txtbox) && txtbox.length > maxLength) {
      var error = String.Format("{0} must be {1} characters or less, {2} characters entered", description, maxLength, txtbox.length);
      this.helper.notifySavedFormMessage("Fedline Event", Global.NOTIFY_TYPE_ERROR, false, error);
      return false;
    }
    else
      return false;
  }

  validateDates(date: string) {
    if (date != null) {
      var CurrentDate = new Date();
      var GivenDate = new Date(date);
      if (GivenDate <= CurrentDate) {
        if (!this.fedlineEvent.userData.arrivalDate == null || (this.fedlineEvent.userData.arrivalDate == null && this.fedlineEvent.userData.arrivalDate != GivenDate)) {
          this.helper.notifySavedFormMessage("Fedline Event", Global.NOTIFY_TYPE_ERROR, false, "Arrival Date may not be today or in the past");
          return false;
        }
        else if (this.helper.getFormControlValue('manualData.workflowStatus') == 10) {
          this.helper.notifySavedFormMessage("Fedline Event", Global.NOTIFY_TYPE_ERROR, false, "Arrival Date may not be today or in the past when moving to Shipped status");
          return false;
        }
      }
    }
    else if (this.helper.getFormControlValue('manualData.workflowStatus') == 10) {
      if (date == null) {
        this.helper.notifySavedFormMessage("Fedline Event", Global.NOTIFY_TYPE_ERROR, false, "Arrival Date is a required field when setting the status to Shipped");
        return false;
      }
    }
    return true;
  }

  isEmptyOrSpaces(str: string) {
    return str === null || str.match(/^ *$/) !== null;
  }

  getPreStageFailCode(e) {
    this.helper.setFormControlValue("manualData.hdnPreStageFailCodes", e.component.option("text"));
    this.fedlineEvent.failCode = e.component.option("text");
  }

  getActivatorFailCodes(e) {
    this.helper.setFormControlValue("manualData.hdnActivatorFailCodes", e.component.option("text"));
  }


  activatorWFStatusChanged(e) {
    this.helper.setFormControlValue("manualData.hdnActivatorWFStatus", e.component.option("text"));

    var eventStatusId = e.component.option("value");
    if (this.activatorWFStatusList.filter(a => a.endEventStusId == eventStatusId) != null) {
      let eventRuleId = this.activatorWFStatusList.filter(a => a.endEventStusId == eventStatusId)[0].eventRuleId
      this.fedlineEvntSrvc.getFedlineEventFailCodes(eventRuleId).subscribe(
        res => {
          this.activatorFailCodesList = res;
          if (res.length > 0)
            this.form.get("manualData.activatorFailCodes").enable();
          else
            this.form.get("manualData.activatorFailCodes").disable();
        }
      );
    }
    else {
      this.form.get("manualData.activatorFailCodes").disable();
    }
  }

  preStagingEngineerSearch() {
    let preStagingEngText = this.form.get("manualData.preStagingEngineer").value == null ? '' : this.form.get("manualData.preStagingEngineer").value
    this.fedlineEvntSrvc.searchUsers(preStagingEngText, 215).subscribe(res => {
      if (res != null && res.length > 0) {
        // Show modal
        this.hidePreStagingEngineerGrid = true;
        this.preStagingEngineerGrid = res;
      } else {
        this.hidePreStagingEngineerGrid = false;
        this.preStagingEngineerGrid = null;
        this.helper.notify("No Pre-Staging Enginner Found", Global.NOTIFY_TYPE_ERROR);
      }
    });
  }

  onPreStagingEngineerRowClick(event) {
    let user = this.preStagingEngineerGrid.find(a => a.userID == event.data.userID)
    console.log("select user: " + user);
    this.helper.setFormControlValue("manualData.preStagingEngineer", user.displayName);
    this.helper.setFormControlValue("manualData.preStagingEngineerUserId", user.userID);
    this.hidePreStagingEngineerGrid = false;
  }

  mnsActivatorSearch() {
    let mnsActivatorText = this.form.get("manualData.mnsActivator").value == null ? '' : this.form.get("manualData.mnsActivator").value
    this.fedlineEvntSrvc.searchUsers(mnsActivatorText, 216).subscribe(res => {
      if (res != null && res.length > 0) {
        // Show modal
        this.hideMnsActivatorGrid = true;
        this.mnsActivatorGrid = res;
      } else {
        this.hideMnsActivatorGrid = false;
        this.mnsActivatorGrid = null;
        this.helper.notify("No MNS Activator Found", Global.NOTIFY_TYPE_ERROR);
      }
    });
  }

  onMnsActivatorRowClick(event) {
    let user = this.mnsActivatorGrid.find(a => a.userID == event.data.userID)
    console.log("select user: " + user);
    this.helper.setFormControlValue("manualData.mnsActivator", user.displayName);
    this.helper.setFormControlValue("manualData.mnsActivatorUserId", user.userID);
    this.hideMnsActivatorGrid = false;
  }

}
