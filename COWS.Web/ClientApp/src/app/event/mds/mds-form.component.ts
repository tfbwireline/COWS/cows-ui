import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors, AbstractControl, FormArray } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, of, Subject } from 'rxjs';
import { KeyValuePair, EVENT_STATUS, EEventType, PRIMARY_OR_BACKUP, YES_OR_NO, SPRINT_OR_CUSTOMER, WORKFLOW_STATUS, Global, WIRELESS_TYPE_CD, EEventProfile, RB_CONDITION, ContactDetailObjType, HierLevel, EVENT_STATUS_EMAIL_CD } from '../../../shared/global';
import { Helper, SpinnerAsync } from '../../../shared';
import { timeout } from 'rxjs/operators';
import {
  MDSNetworkActivityType, MDSActivityType, MDSMACActivity, MDSEventDslSbicCustTrpt, MDSEventPortBndwd,
  MDSEventWrlsTrpt, MDSEventSlnkWiredTrpt, OdieRspn, MDSEventNtwkCust, MDSEventH6Lookup, RedesignDevicesInfo,
  EventCpeDev, MDSEventSiteLookup, EventDevSrvcMgmt, MdsEventSiteSrvc, MDSEventNtwkTrpt, EventDeviceCompletion,
  MdsEventOdieDevice, DeviceManufacturer, DeviceModel, MDSEventDiscOdieDev, ServiceAssuranceSiteSupport, MdsEvent, Workflow, User, MDS3rdPartyVendor, MDS3rdPartyService, EventAsnToUser, UserProfile, EventDiscoDev, MDSServiceTier, ServiceDelivery, MdsServiceType, MDSRelatedCeEvent, ContactDetail
} from '../../../models';
import {
  MDSNetworkActivityTypeService, MDSActivityTypeService, MDSMACActivityService, MplsEventTypeService, VpnPlatformTypeService,
  MplsActivityTypeService, SearchService, DeviceManufacturerService, DeviceModelService, ServiceAssuranceSiteSupportService, MdsEventService, WorkflowService, UserService, MDSServiceTypeService, MDS3rdPartyVendorService, MDS3rdPartyServiceService, EventAsnToUserService, EventLockService, ServiceTierService, ServiceDeliveryService, ActivatorsService, SystemConfigService, ContactDetailService
} from '../../../services';
import { concatMap, debounceTime } from 'rxjs/operators';
import { MDSEventNtwkActy } from '../../../models/mds-event-ntwk-acty.model';
import { DxDataGridComponent } from 'devextreme-angular';
import { MatExpansionPanel } from '@angular/material';
import { isDevMode } from '@angular/core';
declare let $: any;
import * as moment from 'moment';
import { isNullOrUndefined } from 'util';
import { ContactListComponent } from '../../custom/contact-list/contact-list.component';
import { HighlightSpanKind } from 'typescript';

@Component({
  selector: 'app-mds-form',
  templateUrl: './mds-form.component.html'
})
export class MdsFormComponent implements OnInit {
  form: FormGroup
  option: any
  @ViewChild('odieRedesignGrid', { static: false }) odieRedesignGrid: DxDataGridComponent;
  @ViewChild('odieRedesignPanel', { static: false }) odieRedesignPanel: MatExpansionPanel;
  @ViewChild('dgNetworkTransport', { static: false }) dgNetworkTransport: DxDataGridComponent;
  @ViewChild('eventDiscoDevGrid', { static: false }) eventDiscoDevGrid: DxDataGridComponent;
  @ViewChild('dgDesignDocDetail', { static: false }) dgDesignDocDetail: DxDataGridComponent;
  @ViewChild('cntctList', { static: false }) cntctList: ContactListComponent;

  // GETTERS
  get activityType() { return this.form.get("activityType"); }
  get eventStatus() { return this.form.get("eventStatus") }
  get customerSOW() { return this.form.get("customerSOW"); }
  get mdsCustomerSOW() { return this.form.get("mds.customerSOW"); }
  get shortDescription() { return this.form.get("shortDescription"); }
  get ipmDescription() { return this.form.get("ipmDescription"); }
  get networkH6() { return this.form.get("network.h6"); }
  get networkH1() { return this.form.get("network.h1"); }
  get networkCustomerName() { return this.form.get("network.customerName"); }
  get networkActivityType() { return this.form.get("network.networkActivityType"); }
  get isRelatedCompassNCR() { return this.form.get("network.isRelatedCompassNCR") }
  get compassNCR() { return this.form.get("network.compassNCR") }
  get mdsH1() { return this.form.get("mds.h1"); }
  get odieCustomerName() { return this.form.get("mds.odieCustomerName"); }
  get odieCustId() { return this.form.get("mds.odieCustId"); }
  get mdsActivityType() { return this.form.get("mds.mdsActivityType"); }
  get teamPDL() { return this.form.get("mds.teamPDL"); }
  get mdsMacActivity() { return this.form.get("mds.mdsMacActivity"); }
  get woobIpAddress() { return this.form.get("woobIpAddress"); }
  get mdsH6() { return this.form.get("site.h6"); }
  get installSitePoc() { return this.form.get("site.installSite.poc"); }
  get installSitePhone() { return this.form.get("site.installSite.phone"); }
  get serviceAssurancePoc() { return this.form.get("site.serviceAssurance.poc"); }
  get serviceAssurancePhone() { return this.form.get("site.serviceAssurance.phone"); }
  get address() { return this.form.get("site.address"); }
  get state() { return this.form.get("site.state"); }
  get country() { return this.form.get("site.country"); }
  get city() { return this.form.get("site.city"); }
  get zip() { return this.form.get("site.zip"); }
  get cpeDeliveryOption() { return this.form.get("cpe.cpeDeliveryOption"); }
  get cpeDispatchEmail() { return this.form.get("cpe.cpeDispatchEmail") }
  get cpeDispatchComment() { return this.form.get("cpe.cpeDispatchComment") }
  get shipmentCustomerEmail() { return this.form.get("shipping.shipmentCustomerEmail"); }
  get isRequireCoordination() { return this.form.get("disconnect.isRequireCoordination"); }
  get isTotalDisconnect() { return this.form.get("disconnect.isTotalDisconnect"); }
  get disconnectShortDescription() { return this.form.get("disconnect.shortDescription"); }
  get reasonForTotalDisconnect() { return this.form.get("disconnect.reasonForTotalDisconnect"); }
  get withConferenceBridge() { return this.form.get("schedule.withConferenceBridge"); }
  get onlineMeetingAdr() { return this.form.get("schedule.onlineMeetingAdr"); }
  get bridgeNumber() { return this.form.get("schedule.bridgeNumber"); }
  get bridgePin() { return this.form.get("schedule.bridgePin"); }
  get isGenericH1() { return this.form.get("isGenericH1"); }
  get fastTrackType() { return this.form.get("fastTrackType"); }
  get requestedActivityDate() { return this.form.get("requestedActivityDate"); }
  get comments() { return this.form.get("comments"); }
  get workflowStatus() { return this.form.get("workflowStatus"); }
  get mdsFailCode() { return this.form.get("mdsFailCode"); }
  get activityCompletion() { return this.form.get("activityCompletion"); }
  get isWoob() { return this.mdsRedesignDevInfoList.filter(a => a.woobCd == "Y").length > 0 ? true : false }
  get showMDS() { return this.activityType.value != null ? this.activityType.value.filter(a => [1].includes(a)).length > 0 : false }
  get showNetwork() { return this.activityType.value != null ? this.activityType.value.filter(a => [2, 3, 4].includes(a)).length > 0 : false }
  get showNetworkOnly() { return this.activityType.value != null ? (this.activityType.value || []).length == 1 && this.activityType.value.filter(a => [2].includes(a)).length > 0 : false }
  get showVas() { return this.activityType.value != null ? this.activityType.value.filter(a => [3].includes(a)).length > 0 : false }
  get showNID() { return this.showNetwork ? this.networkActivityType.value != null ? this.networkActivityType.value.filter(a => [20, 21].includes(a)).length > 0 : false : false }
  get isCEChng() { return this.networkActivityType.value != null ? this.networkActivityType.value.filter(a => [21].includes(a)).length > 0 : false }
  get isCEOnly() { return this.networkActivityType.value != null ? this.networkActivityType.value.filter(a => [20].includes(a)).length > 0 : false }
  get showDisconnect() { return this.showMDS && this.mdsActivityType.value == 3 ? true : false }
  get showCPEOption() { return [2, 9].includes(this.cpeDeliveryOption.value) ? true : false }
  get isSprintCPE() { return [2].includes(this.cpeDeliveryOption.value) ? true : false }
  get showPreStaging() { return this.showMDS && ((this.mdsActivityType.value == 5) || ((this.mdsMacActivity.value != null) && (this.mdsMacActivity.value.filter(a => [19].includes(a)).length > 0))) ? true : false }
  get hasSpclPrj() { return this.networkActivityType.value != null ? this.networkActivityType.value.filter(a => [26].includes(a)).length > 0 : false }
  get isSuppressEmailEnabled() { return (this.eventStatus.value == EVENT_STATUS.Visible || this.eventStatus.value == EVENT_STATUS.Pending || this.eventStatus.value == EVENT_STATUS.Submitted) }
  get isNetworkOnly() {
    return (this.activityType.value != null
      && (this.activityType.value || []).length == 1
      && this.activityType.value.filter(a => [2, 3, 4].includes(a)).length > 0) ? true : false }
  //get showEdit() {

  //  //let display = !(this.userService.isActivatorByKeyword("MDS") && [EVENT_STATUS.InProgress, EVENT_STATUS.Visible, EVENT_STATUS.Pending, EVENT_STATUS.Completed].includes(this.form.get("eventStatus").value))

  //  //if(!this.userService.hasMultipleRoleInGroup("MDS")) { // User has only 1 Role in a group
  //  //  if(this.userService.isMemberByKeyword("MDS"))  { // Member
  //  //    display = ![WORKFLOW_STATUS.InProgress, WORKFLOW_STATUS.Fulfilling, WORKFLOW_STATUS.Complete].includes(this.form.get("workflowStatus").value)
  //  //  }
  //  //}

  //  let display = this.userService.isActivatorByKeyword("MDS") && [EVENT_STATUS.Published, EVENT_STATUS.InProgress, EVENT_STATUS.OnHold, EVENT_STATUS.Fulfilling, EVENT_STATUS.Shipped].includes(this.form.get("eventStatus").value)

  //  //console.log(display)
  //  if (!this.userService.hasMultipleRoleInGroup("MDS")) { // User has only 1 Role in a group
  //    if (this.userService.isMemberByKeyword("MDS")) { // Member
  //      display = [EVENT_STATUS.Visible, EVENT_STATUS.Pending, EVENT_STATUS.Rework, EVENT_STATUS.Published, EVENT_STATUS.Shipped].includes(this.form.get("eventStatus").value)
  //    }

  //    if (this.userService.isReviewerByKeyword("MDS")) { // Reviewer
  //      display = [EVENT_STATUS.Visible, EVENT_STATUS.Pending, EVENT_STATUS.Rework, EVENT_STATUS.Published].includes(this.form.get("eventStatus").value)
  //    }
  //  }

  //  return display;
  //}
  //get isRework() {
  //  return this.eventStatus.value == WORKFLOW_STATUS.Retract ||
  //    this.eventStatus.value == WORKFLOW_STATUS.Reject ||
  //    this.eventStatus.value == WORKFLOW_STATUS.Return ||
  //    this.eventStatus.value == WORKFLOW_STATUS.Reschedule ||
  //    this.eventStatus.value == WORKFLOW_STATUS.Delete
  //}
  //get isWoob() { return this.mdsRedesignDevInfoList.length > 0 ? (this.mdsRedesignDevInfoList.filter(a => a.woobCd == "Y").length > 0 ? true : false) : false }
  goToLink(url: string) {
    window.open(url, "_blank");
  }
  get isPhysicalAddressRequired() { return (this.showMDS && !this.showDisconnect && (this.isCpe || this.isMns)) }// || (this.showNetwork && this.showNID) }

  get showIPMDescription() {
    return ((!this.isReviewer && !this.isActivator && this.isMember) || (this.isReviewer && this.isMember))
  }
  get isActivator() {
    return this.userService.isMdsActivator()
  }
  get isReviewer() {
    return this.userService.isMdsReviewer()
  }
  get isMember() {
    return this.userService.isMdsMember()
  }

  get isOnlyMember() {
    return this.userService.isMdsMember() && !this.userService.isMdsReviewer()
  }

  get isCustomerSOWValidURL() {
    const str = this.form.get('mds.customerSOW').value
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
  }

  //get mdsOdieDeviceNameList(): any[] {
  //  var deviceNames = this.mdsRedesignDevInfoList.map(a => {
  //    return {
  //      deviceName: a.odieDevNme,
  //      value: a.odieDevNme
  //    }
  //  }) || []
  //  return [...deviceNames, { deviceName: "Do Not Send to ODIE", value: "0" }]
  //}


  activator: FormGroup
  id: number = 0
  editMode: boolean = false
  showEditButton: boolean = false
  isSubmitted: boolean = false
  eventType: number = EEventType.MDS // Used to classify Event Type for Scheduling
  isCpe: boolean = false
  isMns: boolean = false
  userFinderForPublishedEmail: boolean = false
  showWoob: boolean = false;
  isMach5Disabled: boolean = false;
  isServiceIdDisabled: boolean = false;
  isServiceTypeDisabled: boolean = false;
  isThirdPartyVendorDisabled: boolean = false;
  isThirdPartyIdDisabled: boolean = false;
  isThirdPartyServiceLevelDisabled: boolean = false;
  isActivationDateDisabled: boolean = false;
  isCommentDisabled: boolean = false;
  isOdieDisabled: boolean = false;
  isContentReady: boolean = false
  prevUSIntlCd: string = "";
  errors: string[] = [];
  oldValue: string;
  mdsCpeVldtn = null
  mdsCpePostPublishValidation = null
  mdsCpeSubmitValidationStatuses = []
  mdsCpePublishValidationStatuses = []
  cpeMnsNotFoundMessage: string = null
  cpeMnsFoundMessage: string = null
  isPageLoad: boolean = true
  prevNTVTableCnt: number = 0
  dt48: Date
  dt72: Date
  dtNow: Date
  dt48WithWeekend: Date
  dt72WithWeekend: Date
  warning: string = "";
  spnr: SpinnerAsync
  // Manage Activity Additional Logic
  isRedesignHasChanges: boolean = false;
  // isOnRedesignCellEditting: boolean = false;
  // isManageActivityTableDoneUpdating : boolean = false;
  isAllowedForEdit: boolean = false;
  siteServiceListAllowEdit: boolean = false;
  h1CustomerName: string = ""
  h6CustomerName: string = ""
  objType: string;
  recentNetworkActivityType: any = []

  // DROPDOWNS
  activityTypeList: MDSNetworkActivityType[] = []
  activityTypeCompletionList: MDSNetworkActivityType[] = []
  eventStatusList: KeyValuePair[] = []
  networkActivityTypeList: KeyValuePair[] = []
  vasTypeList: any[] = []
  odieRspnList: OdieRspn[]
  mdsActivityTypeList: MDSActivityType[] = []
  mdsMacActivityList: MDSMACActivity[] = []
  deviceManfList: DeviceManufacturer[] = []
  deviceModelList: DeviceModel[] = []
  yesOrNoList: KeyValuePair[] = []
  saSiteSupportList: ServiceAssuranceSiteSupport[] = []
  domesticOrInternationalList: KeyValuePair[] = [new KeyValuePair("US", "D"), new KeyValuePair("Intl", "I")]
  serviceTierList: MDSServiceTier[] = []
  mdsServiceTypeList: any[] = []
  mds3rdPartyVendorList: MDS3rdPartyVendor[] = []
  mds3rdPartyServiceList: MDS3rdPartyService[] = []
  primaryOrBackupList: KeyValuePair[] = []
  sprintOrCustomerList: KeyValuePair[] = []
  wirelessTypeCdList: KeyValuePair[] = []
  serviceDeliveryList: KeyValuePair[] = []
  workflowStatusList: Workflow[] = []
  workflowStatusListFiltered: Workflow[] = []
  wirelessYesOrNoList: KeyValuePair[] = [new KeyValuePair("Yes", "Y"), new KeyValuePair("No", "N")]
  preconfigCompletedList: KeyValuePair[] = [new KeyValuePair("YES", "Y"), new KeyValuePair("NO", "N")]
  mdsFailCodeList: any[]
  odieDeviceList: any[]
  mdsOdieDeviceNameList: any[] = []
  contactList: ContactDetail[] = [];


  // TABLES
  relatedEventsList: MDSRelatedCeEvent[] = []
  networkCustomerList: MDSEventNtwkCust[] = []
  networkTransportList: MDSEventNtwkTrpt[] = []
  designDocDetailList: any[] = []
  odieRedesignDevInfoList: RedesignDevicesInfo[] = []
  mdsRedesignDevInfoList: MdsEventOdieDevice[] = []
  devCompletionList: EventDeviceCompletion[] = []
  cpeDeviceList: EventCpeDev[] = []
  deletedCpeDeviceList: any[] = []
  mnsOrderList: EventDevSrvcMgmt[] = []
  siteServiceList: MdsEventSiteSrvc[] = []
  thirdPartyList: MDSEventDslSbicCustTrpt[] = []
  wiredTransportList: MDSEventSlnkWiredTrpt[] = []
  wirelessTransportList: MDSEventWrlsTrpt[] = []
  portBandwidthList: MDSEventPortBndwd[] = []
  eventDiscoDevList: EventDiscoDev[] = []
  eventDiscoDevListFinal: EventDiscoDev[] = []
  relatedMnsOrderList: EventDevSrvcMgmt[] = []
  modalCpeDeviceList: any[] = []
  modalPendingComponentList: any[] = []
  modalInstalledInventoryList: any[] = []
  h6Lookup: MDSEventSiteLookup = new MDSEventSiteLookup({})

  // TABLE ERRORS
  contactListErrors: string[] = []
  networkTransportErrors: string[] = []
  networkCustomerErrors: string[] = []
  mdsOdieErrors: string[] = []
  cpeErrors: string[] = []
  devCompletionErrors: string[] = []
  mnsErrors: string[] = []
  siteServiceErrors: string[] = []
  thirdPartyErrors: string[] = []
  wiredTransportErrors: string[] = []
  wirelessTransportErrors: string[] = []
  portBandwidthErrors: string[] = []
  eventDiscoDevErrors: string[] = []
  h6Label: string = "H6";
  mds: MdsEvent;

  isMDSReviewer: boolean = false;

  mdsActivityTypeSub : Subject<any> = new Subject();
  displayOptOut : boolean
  activityTypeValSub : Subject<any> = new Subject();
  hasNetworkType: boolean;

  meetingUrlSub : Subject<any> = new Subject();
  meetingUrl: string;

  // For locking
  isLocked: boolean = false;
  lockMessage: string = '';

  profile: string = '';
  defaultCountry = ['US', 'USA', 'U.S.A', 'U.S', 'UNITED STATES', 'UNITED STATE OF AMERICA', 'UNITED STATES OF AMERICA'];
  oldDeliveryOption = 0;
  dblBookingMessage = "";
  ntwkIntlUser: User;
  hasDisconnectSprintHolidayError = false;
  get isNtwkIntl() { return !this.helper.isEmpty(this.activityType.value) ? this.activityType.value.filter(a => [4].includes(a)).length > 0 : false }

  constructor(private spinner: NgxSpinnerService, private router: Router, private activatedRoute: ActivatedRoute, public helper: Helper,
    public mdsNetworkActivityTypeService: MDSNetworkActivityTypeService, public mdsEventService: MdsEventService, public userService: UserService, public evntLockSrvc: EventLockService,
    public mplsActivityTypeService: MplsActivityTypeService, public mdsActivityTypeService: MDSActivityTypeService, public mdsMacActivityService: MDSMACActivityService,
    public devManfSrvc: DeviceManufacturerService, public devModelSrvc: DeviceModelService, public saSiteSupportService: ServiceAssuranceSiteSupportService, public searchService: SearchService,
    public serviceTierService: ServiceTierService, public mdsServiceTypeService: MDSServiceTypeService, public mds3rdPartyVendorService: MDS3rdPartyVendorService,
    public mds3rdPartyServiceService: MDS3rdPartyServiceService, public serviceDelivery: ServiceDeliveryService, public workflowService: WorkflowService, public eventAsnToUserService: EventAsnToUserService,
    public activatorService: ActivatorsService, private systemConfigService: SystemConfigService,
    private cntctDtlSrvc: ContactDetailService
  ) {
    this.getDeviceModelByManfId = this.getDeviceModelByManfId.bind(this)
    this.getMds3rdPartyVendorByServiceTypeId = this.getMds3rdPartyVendorByServiceTypeId.bind(this)
    this.getMds3rdPartyServiceByServiceTypeId = this.getMds3rdPartyServiceByServiceTypeId.bind(this)
    this.onServiceTypeChanged = this.onServiceTypeChanged.bind(this)
    this.onWiredTransportNewRow = this.onWiredTransportNewRow.bind(this)
    // this.onNetworkCustomerInserting = this.onNetworkCustomerInserting.bind(this)
    this.onWiredTransportEditorPreparing = this.onWiredTransportEditorPreparing.bind(this)
    this.onManageActivityTableDelete = this.onManageActivityTableDelete.bind(this)
    this.onVendorChange = this.onVendorChange.bind(this)
    this.onDeviceCompletionRowUpdating = this.onDeviceCompletionRowUpdating.bind(this)
    this.spnr = new SpinnerAsync(spinner)
  }

  ngOnInit() {    

    this.spnr.manageSpinner("show");
    this.objType = ContactDetailObjType.Event;
    this.id = this.activatedRoute.snapshot.params["id"] || 0;
    this.editMode = this.id == 0 ? true : false

    this.meetingUrlSub.subscribe(data => {
      this.meetingUrl = data;
      });

    this.activityTypeValSub.subscribe(data => {
        if(typeof(data) == 'object' && data.length > 0) {
          // Purpose is to only send an opt-out for network only but the customer requested to allow all.
          this.hasNetworkType = data.includes(4) ? false : true;
        }
      });

    this.mdsActivityTypeSub.subscribe(data => {
      if(typeof(data) == 'object') {
        this.hasNetworkType = (data.value == 3 || this.isNtwkIntl) ? false : true;
      }
    })  


    this.form = new FormGroup({
      activityType: new FormControl(),
      eventId: new FormControl(),
      eventStatus: new FormControl(EVENT_STATUS.Visible),
      itemTitle: new FormControl(),
      customerSOW: new FormControl(),
      ipmDescription: new FormControl(),
      shortDescription: new FormControl(),
      woobIpAddress: new FormControl(),
      isGenericH1: new FormControl(false),
      fastTrackType: new FormControl("At Will"),
      requestedActivityDate: new FormControl(),
      comments: new FormControl(),
      workflowStatus: new FormControl(WORKFLOW_STATUS.Visible),
      preconfigCompleted: new FormControl("N"),
      isSuppressedEmails: new FormControl(false),
      mdsFailCode: new FormControl(),
      activityCompletion: new FormControl(),
      mds: new FormGroup({
        h1: new FormControl(),
        odieCustomerName: new FormControl(),
        teamPDL: new FormControl(),
        contactEmails: new FormControl(),
        mnspmId: new FormControl(),
        mdsActivityType: new FormControl(),
        mdsMacActivity: new FormControl(),
        customerSOW: new FormControl(),
        odieCustId: new FormControl()
      }),
      network: new FormGroup({
        h6: new FormControl(),
        h1: new FormControl(),
        customerName: new FormControl(),
        networkActivityType: new FormControl(),
        isRelatedCompassNCR: new FormControl(0, Validators.required),
        compassNCR: new FormControl(null),
      }),
      site: new FormGroup({
        h6: new FormControl(),
        ccd: new FormControl(),
        siteIdAddress: new FormControl(),
        address: new FormControl(),
        state: new FormControl(),
        bldg_flr_rm: new FormControl(),
        country: new FormControl(),
        city: new FormControl(),
        zip: new FormControl(),
        us: new FormControl(),
        saContactHrs: new FormControl(),
        saTimezone: new FormControl(),
        installSite: new FormGroup({
          poc: new FormControl(),
          phoneCode: new FormControl(),
          phone: new FormControl(),
          cellphoneCode: new FormControl(),
          cellphone: new FormControl()
        }),
        serviceAssurance: new FormGroup({
          poc: new FormControl(),
          phoneCode: new FormControl(),
          phone: new FormControl(),
          cellphoneCode: new FormControl(),
          cellphone: new FormControl()
        })
      }),
      cpe: new FormGroup({
        cpeDeliveryOption: new FormControl(),
        //nidSerialNbr: new FormControl(),
        //nidHostName: new FormControl(),
        mdrNumber: new FormControl(),
        cpeDispatchEmail: new FormControl(),
        cpeDispatchComment: new FormControl()
      }),
      shipping: new FormGroup({
        shippedDate: new FormControl(),
        shipmentRefNo: new FormControl(),
        shipmentCustomerEmail: new FormControl(),
        shippedDeviceSerialNumber: new FormControl(),
        shipmentCarrierName: new FormControl(),
        isAlreadyShipped: new FormControl(),
        altShipPocNme: new FormControl(),
        altShipPocPhnNbr: new FormControl(),
        altShipPocEmail: new FormControl(),
        altShipStreetAdr: new FormControl(),
        altShipFlrBldgNme: new FormControl(),
        altShipCtyNme: new FormControl(),
        altShipSttPrvnNme: new FormControl(),
        altShipCtryRgnNme: new FormControl(),
        altShipZipCd: new FormControl()
      }),
      requestor: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        cellphone: new FormControl(),
        email: new FormControl()
      }),
      event: new FormGroup({
        isEscalation: new FormControl(false),
        escalationReason: new FormControl(1),
        escalationPolicy: new FormControl(),

        primaryRequestDate: new FormControl(new Date().setHours(new Date().getHours(), 0, 0, 0)),
        secondaryRequestDate: new FormControl(new Date().setHours(new Date().getHours() + 1, 0, 0, 0)),
        escalationBusinessJustification: new FormControl(),
        publishedEmailCC: new FormControl(),
        completeEmailCC: new FormControl()
      }),
      disconnect: new FormGroup({
        isRequireCoordination: new FormControl(),
        isTotalDisconnect: new FormControl(),
        shortDescription: new FormControl(),
        reasonForTotalDisconnect: new FormControl()
      }),
      schedule: new FormGroup({
        withConferenceBridge: new FormControl(null),
        bridgeNumber: new FormControl(),
        bridgePin: new FormControl(),
        startDate: new FormControl(),
        endDate: new FormControl(),
        eventDuration: new FormControl(60),
        extraDuration: new FormControl(0),
        assignedToId: new FormControl(),
        assignedTo: new FormControl(),
        displayedAssignedTo: new FormControl(),
        softAssign: new FormControl(),
        onlineMeetingAdr: new FormControl(),
        custLtrOptOut: new FormControl(false)
      })
    })

    this.option = {
      lookup_h6: {
        isEnabled: false,
        searchQuery: "h6"
      },
      lookup_h1: {
        isEnabled: false,
        searchQuery: "h1"
      },
      lookup_site: {
        isEnabled: false,
        mappings: [
          ["installSite.poc", "instlSitePocNme"],
          ["installSite.phoneCode", "instlSitePocIntlPhnCd"],
          ["installSite.phone", "instlSitePocPhnNbr"],
          ["serviceAssurance.poc", "srvcAssrnPocNme"],
          ["serviceAssurance.phoneCode", "srvcAssrnPocIntlPhnCd"],
          ["serviceAssurance.phone", "srvcAssrnPocPhnNbr"],
          ["address", "streetAdr"],
          ["bldg_flr_rm", "flrBldgNme"],
          ["city", "ctyNme"],
          ["state", "sttPrvnNme"],
          ["country", "ctryRgnNme"],
          ["zip", "zipCd"],
          ["saContactHrs", "srvcAvlbltyHrs"],
          ["saTimezone", "srvcTmeZnCd"]
        ]
      },
      requestor: {
        userFinder: {
          isEnabled: false,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["cellphone", "cellPhnNbr"],
            ["email", "emailAdr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for requestor" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Requestor is required" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Cellphone number is required" }
          ]
        },
        email: {
          isShown: true
        },
      },
      event: {
        isEscalation: {
          isShown: true
        },
        escalationReason: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Escalation Reason is required" }
          ]
        },
        escalationPolicy: {
          isShown: true
        },
        primaryRequestDate: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Primary Request Date is required" }
          ]
        },
        secondaryRequestDate: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Secondary Request Date is required" }
          ]
        },
        escalationBusinessJustification: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Escalation Business Justification is required" }
          ]
        },
        userFinderForPublishedEmail: {
          isEnabled: true,
          isMultiple: true,
          searchQuery: "publishedEmailCC",
          mappings: [
            ["publishedEmailCC", "emailAdr"]
          ]
        },
        userFinderForCompleteEmail: {
          isEnabled: true,
          isMultiple: true,
          searchQuery: "completeEmailCC",
          mappings: [
            ["completeEmailCC", "emailAdr"]
          ]
        }
      },
      schedule: {
        withConferenceBridge: {
          isShown: true
        },
        userFinder: {
          isEnabled: false,
          isMultiple: true,
          searchQuery: "displayedAssignedTo",
          mappings: [
            ["assignedToId", "userId"],
            ["assignedTo", "dsplNme"],
            ["displayedAssignedTo", "dsplNme"]
          ]
        },
        softAssign: {
          isShown: false
        },
        checkCurrentSlot: {
          isShown: false
        },
        launchPicker: {
          isShown: false
        }
      },
      validationSummaryOptions: {
        title: "Validation Summary"
      },
      mds: {
        userFinderForContactEmailAddresses: {
          isEnabled: true,
          isMultiple: true,
          searchQuery: "contactEmails",
          mappings: [
            ["contactEmails", "emailAdr"]
          ]
        }
      }
    }
    this.init()
    this.spnr.manageSpinner("hide");
  }

  init() {
    let id = this.id
    this.yesOrNoList = this.helper.getEnumKeyValuePair(RB_CONDITION);
    this.eventStatusList = this.helper.getEnumKeyValuePair(EVENT_STATUS)
    this.primaryOrBackupList = this.helper.getEnumKeyValuePair(PRIMARY_OR_BACKUP)
    this.sprintOrCustomerList = this.helper.getEnumKeyValuePair(SPRINT_OR_CUSTOMER)
    this.wirelessTypeCdList = this.helper.getEnumKeyValuePair(WIRELESS_TYPE_CD)
    sessionStorage.setItem('selectedEventId', id.toString())



    if (id > 0) {
      // Update
      this.spnr.manageSpinner("show");
      let data = zip(
        this.mdsEventService.getById(id).pipe(
          concatMap(
            res => zip(
              of(res as MdsEvent),
              this.userService.getByUserID((res as MdsEvent).reqorUserId),
              this.eventAsnToUserService.getByEventId((res as MdsEvent).eventId),
              this.workflowService.getForEvent(this.eventType, (res as MdsEvent).eventStusId),
              this.cntctDtlSrvc.getContactDetails(this.id, this.objType, '', '', (res as MdsEvent).odieCustId || '',
                ((res as MdsEvent).mdsEventNtwkActy != null
                  && ((res as MdsEvent).mdsEventNtwkActy || []).length == 1
                  && (res as MdsEvent).mdsEventNtwkActy.filter(a => [2, 3, 4].includes(a.ntwkActyTypeId)).length > 0) ? true : false),
              //this.nidActyService.getSerialNumberByH6()
            )
          )
        ),
        this.mdsNetworkActivityTypeService.get(),
        this.mplsActivityTypeService.getByEventId(this.eventType),
        this.mdsEventService.getVASSubTypes(),
        this.mdsActivityTypeService.get(),
        this.mdsMacActivityService.getForLookup(),//this.mdsMacActivityService.get(),
        this.devManfSrvc.get(),
        this.devModelSrvc.getForLookup(),//this.devModelSrvc.get(),
        this.saSiteSupportService.getForLookup(),//this.saSiteSupportService.getServiceAssuranceSiteSupport(),
        this.serviceTierService.getForLookup(),//this.serviceTierService.getServiceTier(),
        this.mdsServiceTypeService.get(),
        this.mds3rdPartyVendorService.getForLookup(),//this.mds3rdPartyVendorService.get(),
        this.mds3rdPartyServiceService.getForLookup(),//this.mds3rdPartyServiceService.get(),
        this.serviceDelivery.getForControl(),
        this.userService.userActiveWithProfileName(this.userService.loggedInUser.userId, "MDS Event Reviewer"),
        this.evntLockSrvc.checkLock(id),
        this.workflowService.get(),
        this.userService.getFinalUserProfile(this.userService.loggedInUser.adid, this.eventType),
        this.activatorService.GetAllFailCodes(),
        this.systemConfigService.getSysCfgByName("MDSCPEVldtn"),
        this.systemConfigService.getSysCfgByName("MDSCPEPostPblshVldtn"),
        this.systemConfigService.getSysCfgByName("MDSCPESbmtVldStuses"),
        this.systemConfigService.getSysCfgByName("MDSCPEPblshVldStuses"),
        this.mdsEventService.isEventMailSent(id, 14),
        this.mdsEventService.isEventMailSent(id, 40),
        this.mdsEventService.calDtwithBusiDays("2|3|0"),
        this.systemConfigService.getSysCfgByName("MDSESCPolicyURL"),
        this.userService.getUserByADID("NtwkIntl"),
        this.mdsEventService.calDtwithBusiDaysIncludingWeekend("2|3|0")
      )
      let isDone = false
      data.subscribe(
        res => {
          console.log([`mds result`, res])
          this.mds = res[0][0] as MdsEvent
          let requestor = res[0][1] as User
          let activators = res[0][2] as EventAsnToUser[]
          this.workflowStatusList = (res[0][3] as Workflow[]).filter(a => !a.wrkflwStusDes.includes("UAT"))
          //console.log(JSON.stringify(this.workflowStatusList));
          this.workflowStatusListFiltered = this.workflowStatusList
          this.activityTypeList = res[1] as MDSNetworkActivityType[]
          this.networkActivityTypeList = res[2] as KeyValuePair[]
          this.vasTypeList = res[3] as any[]
          this.mdsActivityTypeList = (res[4] as MDSActivityType[]).filter(a => ![1, 4, 6].includes(a.mdsActyTypeId)) // Id IN (1,4,6) is not being used anymore
          this.mdsMacActivityList = res[5] as MDSMACActivity[]
          this.deviceManfList = res[6] as DeviceManufacturer[]
          this.deviceModelList = res[7] as DeviceModel[]
          this.saSiteSupportList = res[8] as ServiceAssuranceSiteSupport[]
          this.serviceTierList = res[9] as MDSServiceTier[]
          this.mdsServiceTypeList = res[10] as MdsServiceType[]
          this.mds3rdPartyVendorList = res[11] as MDS3rdPartyVendor[]
          this.mds3rdPartyServiceList = res[12] as MDS3rdPartyService[]
          this.serviceDeliveryList = res[13] as KeyValuePair[]
          this.isMDSReviewer = res[14] as boolean;
          let lockUser = res[15] as any;
          let workflowStatus = res[16] as Workflow[]
          var usrPrf = res[17] as UserProfile;
          this.mdsFailCodeList = res[18] as any;
          this.mdsCpeVldtn = res[19][0].prmtrValuTxt
          this.mdsCpePostPublishValidation = res[20][0].prmtrValuTxt as string
          this.mdsCpeSubmitValidationStatuses = (res[21][0].prmtrValuTxt as string).split("/")
          this.mdsCpePublishValidationStatuses = (res[22][0].prmtrValuTxt as string).split("/")
          let isAlreadyShipped = res[23] == true || res[24] == true

          var dtArr = (res[25] as string).split("|");
          var dtArr2 = (res[28] as string).split("|");
          this.dtNow = new Date(dtArr[0]);
          this.dt72 = new Date(dtArr[1]);
          this.dt48 = new Date(dtArr[2]);

          // Business days including weekend
          this.dt72WithWeekend = new Date(dtArr2[1]);
          this.dt48WithWeekend = new Date(dtArr2[2]);

          let escPolicy = res[26] as any;
          if (usrPrf != null) {
            this.profile = this.helper.getFinalProfileName(usrPrf);
          }

          if (lockUser != null) {
            this.lockMessage = 'This event is currently locked by ' + lockUser.lockByFullName;
            this.isLocked = true;
          }

          this.showEditButton = (res[0][3] as Workflow[]).filter(a => !a.wrkflwStusDes.includes("UAT")).length > 0
          this.ntwkIntlUser = res[27] as User;

          // Contact Details
          this.contactList = (res[0][4] || []) as ContactDetail[];
          console.log(this.contactList)
          // Construct Email Code Text on Update
          this.contactList.forEach(item => {
            if (!this.helper.isEmpty(item.emailCd)) {
              const statusText: string[] = [];
              item.emailCdIds = item.emailCd.split(',').map(Number);
              item.emailCdIds.forEach(row => {
                statusText.push(EVENT_STATUS_EMAIL_CD[row])
              });
              item.emailCdTxt = statusText.join(',');
            }

            if (item.id != 0 && (item.creatByUserId > 1 && item.creatByUserId != this.userService.loggedInUser.userId)) {
              item.allowEdit = false;
            } else {
              item.allowEdit = true;
            }
          });

          console.log(this.contactList)

          //this.form.get("mds.mdsMacActivity").setValue(null)

          // MAIN INFO
          this.form.get("eventId").setValue(id)
          this.form.get("mds.mdsActivityType").setValue(this.mds.mdsActyTypeId)
          this.form.get("activityType").setValue(this.mds.mdsEventNtwkActy.map(a => a.ntwkActyTypeId))
          this.activityTypeValSub.next(this.mds.mdsEventNtwkActy.map(a => a.ntwkActyTypeId));
          this.form.get("eventStatus").setValue(this.mds.eventStusId)
          this.form.get("itemTitle").setValue(this.mds.eventTitleTxt)
          this.form.get("customerSOW").setValue(this.mds.custSowLocTxt)
          this.form.get("mds.customerSOW").setValue(this.mds.custSowLocTxt)
          this.form.get("ipmDescription").setValue(this.mds.ipmDes)
          this.form.get("shortDescription").setValue(this.mds.shrtDes)

          // NETWORK
          this.form.get("network.h6").setValue(this.mds.ntwkH6)
          this.form.get("network.h1").setValue(this.mds.ntwkH1)
          this.form.get("network.customerName").setValue(this.mds.ntwkCustNme)
          this.form.get("network.networkActivityType").setValue(this.mds.mplsEventActyType.map(a => a.mplsActyTypeId))
          this.form.get("network.isRelatedCompassNCR").setValue(this.mds.rltdCmpsNcrCd ? 1 : 0);
          this.form.get("network.compassNCR").setValue(this.mds.rltdCmpsNcrNme);
          this.networkCustomerList = this.mds.networkCustomer
          this.networkTransportList = this.mds.networkTransport.filter(a => a.recStusId == 1)
          console.log(this.networkTransportList)
          this.recentNetworkActivityType = (this.mds.mplsEventActyType || []).length > 0 ? this.mds.mplsEventActyType.map(a => a.mplsActyTypeId) : []

          //if (this.networkTransportList.length > 0) {
          this.prevNTVTableCnt = this.networkTransportList.length;
          // }
          let isCEChange = this.networkActivityType.value.filter(a => [21].includes(a)).length > 0 as boolean;
          if (isCEChange) {
            this.h6Label = "H6/CE ServiceID";
          }

          //if (this.showNID) {
          //  this.searchService.getH6Lookup(this.mds.ntwkH6, this.eventType, this.getVASCECd(), isCEChange).subscribe(res => {
          //    if (res.relatedCEEvents != null) {
          //      this.relatedEventsList = res.relatedCEEvents.filter(a => a.rltdCEEventID != this.id) || [];
          //    }
          //  });
          //}
          //if (this.networkTransportList.length <= 0 || (this.showNID && this.networkTransportList.some(nh => nh.nidHostName == null || nh.nidHostName.length <= 0))) {
          //  this.searchService.getH6Lookup(this.mds.ntwkH6, this.eventType, this.getVASCECd(), isCEChange).subscribe(res => {
          //    if ((res.networkTransport != null) && (res.networkTransport.length > 0) && (this.networkTransportList != null) && (this.networkTransportList.length > 0)) {
          //      let bpmNtwkTrpt = res.networkTransport as MDSEventNtwkTrpt[]
          //      for (let value of this.networkTransportList) {
          //        //console.log(`currNtwk : ` + JSON.stringify(value));
          //        let commonNti = bpmNtwkTrpt.find(item => ((item.assocH6 == value.assocH6) && (item.ddAprvlNbr == value.ddAprvlNbr) && (item.mach5OrdrNbr == value.mach5OrdrNbr)));
          //        //console.log(`cmnNtwk : ` + JSON.stringify(commonNti));
          //        value.nidHostName = (commonNti != null) ? commonNti.nidHostName : value.nidHostName;
          //        //console.log(`newHostNme : ` + value.nidHostName);
          //      }
          //    }
          //    else if ((res.networkTransport != null) && (res.networkTransport.length > 0)) {
          //      this.networkTransportList = res.networkTransport as MDSEventNtwkTrpt[]
          //    }
          //  });
          //}
          if (this.showNID
            || (this.networkTransportList.length <= 0 || (this.showNID && this.networkTransportList.some(nh => nh.nidHostName == null || nh.nidHostName.length <= 0)))
            || this.showNetworkOnly) {
            this.spnr.manageSpinner("show") 
            this.searchService.getH6Lookup(this.mds.ntwkH6, this.eventType, this.getVASCECd(), isCEChange).subscribe(res => {
              if (this.showNID) {
                if (res.relatedCEEvents != null) {
                  this.relatedEventsList = res.relatedCEEvents.filter(a => a.rltdCEEventID != this.id) || [];
                }
              }

              if (this.networkTransportList.length <= 0 || (this.showNID && this.networkTransportList.some(nh => nh.nidHostName == null || nh.nidHostName.length <= 0))) {
                if ((res.networkTransport != null) && (res.networkTransport.length > 0) && (this.networkTransportList != null) && (this.networkTransportList.length > 0)) {
                  let bpmNtwkTrpt = res.networkTransport as MDSEventNtwkTrpt[]
                  for (let value of this.networkTransportList) {
                    //console.log(`currNtwk : ` + JSON.stringify(value));
                    let commonNti = bpmNtwkTrpt.find(item => ((item.assocH6 == value.assocH6) && (item.ddAprvlNbr == value.ddAprvlNbr) && (item.mach5OrdrNbr == value.mach5OrdrNbr)));
                    //console.log(`cmnNtwk : ` + JSON.stringify(commonNti));
                    value.nidHostName = (commonNti != null) ? commonNti.nidHostName : value.nidHostName;
                    //console.log(`newHostNme : ` + value.nidHostName);
                  }
                }
                else if ((res.networkTransport != null) && (res.networkTransport.length > 0)) {
                  this.networkTransportList = res.networkTransport as MDSEventNtwkTrpt[]
                }
              }

              if (this.showNetworkOnly) {
                this.h1CustomerName = res.h1CustNme
                this.h6CustomerName = res.customerName
              }
            }, error => {
              console.log('Error on loading H6 Lookup');
            }).add(() => {
              this.spnr.manageSpinner("hide");
            });
          }
          if (this.checkMDSNtwkActy("hasNtwk") || this.checkMDSNtwkActy("hasVAS")) {
            if (this.prevNTVTableCnt != this.networkTransportList.length) {
              this.clearAssignee();
            }
            // this.showNIDData(); Not functioning due to instance not yet available
          }
          //let isCEChange = this.networkActivityType.value.filter(a => [21].includes(a)).length > 0 as boolean;        
          //this.searchService.getH6Lookup(this.mds.ntwkH6, this.eventType, this.getVASCECd(), isCEChange).subscribe(res => {
          //  if ((this.networkTransportList.length <= 0) || (this.showNID && this.mds.networkTransport.filter(a => a.recStusId == 1).some(nh => nh.nidHostName == null || nh.nidHostName.length<=0))) {
          //    if (res.networkTransport != null) {
          //      for (let value of res.networkTransport.filter(a => a.recStusId == 1)) {
          //        if (!this.customerNetworkTransport(value, this.networkTransportList)) {
          //          this.networkTransportList.push(value)
          //          console.log(value)
          //        }
          //      }
          //    }
          //  }
          //  if (res.relatedCEEvents != null) {
          //    this.relatedEventsList = res.relatedCEEvents.filter(a => a.rltdCEEventID != this.id) || [];
          //  }
          //  this.showNIDData();
          //});

          // Cuase duplicate to VAS Table - partially set to comment
          //this.networkTransportList = this.mds.networkTransport.filter(a => a.recStusId == 1)

          // MDS
          let odieRspnList: OdieRspn[] = [new OdieRspn({
            custNme: this.mds.custNme,
            sowsFoldrPathNme: this.mds.custSowLocTxt,
            custTeamPdl: this.mds.custAcctTeamPdlNme,
            mnspmId: this.mds.mnsPmId,
            odieCustId: this.mds.odieCustId
          })]

          console.log(odieRspnList)

          this.odieRspnList = odieRspnList
          this.form.get("mds.mnspmId").setValue(this.mds.mnsPmId)
          this.form.get("mds.odieCustId").setValue(this.mds.odieCustId)

          this.form.get("site.h6").setValue(this.mds.h6)
          this.form.get("site.ccd").setValue(this.mds.ccd)

          if (!this.helper.isEmpty(this.mds.h6) && !this.helper.isEmpty(this.mds.ccd)) {
            // Proceed to lookup
            if (!this.showDisconnect && this.form.get("eventStatus").value != 6) {
              this.spnr.manageSpinner("show");
              let date = moment(this.mds.ccd).format("MM/DD/YYYY")

              this.searchService.getSiteLookup(this.mds.h6, date, 'N', this.showNID).subscribe(
                res => {
                  this.isPageLoad = true
                  this.onSiteLookup(res)
                  //if (!this.helper.isEmpty(res.siteIdTxt)) {
                  //  this.form.get("site.siteIdAddress").setValue(res.siteIdTxt + '_' + res.ctyNme + ',' + res.sttPrvnNme);
                  //}
                  //else {
                  //  this.form.get("site.siteIdAddress").setValue('');
                  //}               
                }).add(() => {
                  this.spnr.manageSpinner("hide");
                });
            }
          }
          else {
            this.form.get("site.siteIdAddress").setValue('');
          }

          this.form.get("mds.h1").setValue(this.mds.h1)
          this.form.get("mds.odieCustomerName").setValue(this.mds.custNme)
          //this.form.get("mds.teamPDL").setValue(this.mds.custAcctTeamPdlNme)
          console.log(this.mds.cntctEmailList)
          this.form.get("mds.contactEmails").setValue(this.mds.cntctEmailList)
          this.form.get("mds.mdsActivityType").setValue(this.mds.mdsActyTypeId)
          //console.log("ASD")
          this.onMdsActivityType(this.mds.mdsActyTypeId)
          //console.log('mdsEventMacActyIds : ' + JSON.stringify(this.mds.mdsEventMacActyIds));
          this.form.get("mds.mdsMacActivity").setValue(this.mds.mdsEventMacActyIds)

  
          this.cpeDeviceList = this.mds.cpeDevice.filter(a => a.recStusId == 1)
          

          this.mdsRedesignDevInfoList = this.mds.mdsRedesignDevInfo
          if (this.mdsOdieDeviceNameList.length === 0 && this.showNetwork) {
            this.cpeDeviceList.map(x => {
              x["odieDevNme"] = x["odieDevNme"] == "" ? "0" : x["odieDevNme"];
            })
          }
          this.getMdsOdieDeviceNameList()

          this.devCompletionList = this.mds.devCompletion.filter(a => a.recStusId == 1)
          this.devCompletionList = this.devCompletionList.map(a => { a.isAlreadyCompleted = a.cmpltdCd; return a; })
          this.mnsOrderList = this.mds.mnsOrder.filter(a => a.recStusId == 1)
          this.siteServiceList = this.mds.siteService.map(field => {
            this.onServiceTypeChanged(field, field.srvcTypeId);

            if(field.srvcTypeId == '2') {
              field.srvcTypeId = '';
            }
            return field;
          })
          this.thirdPartyList = this.mds.thirdParty.filter(a => a.recStusId == 1)
          this.wiredTransportList = this.mds.wiredTransport.filter(a => a.recStusId == 1)
          console.log(this.wiredTransportList)
          this.wirelessTransportList = this.mds.wirelessTransport
          this.portBandwidthList = this.mds.portBandwidth.filter(a => a.recStusId == 1)
          // this.thirdPartyList = this.thirdPartyList.map(a => {
          //  a.isWirelessCd = a.isWirelessCd == "Y" ? "Yes" : "No"
          //  return a
          // })
          this.wiredTransportList = this.wiredTransportList.map(a => {
            a.readyBeginCdBoolean = a.readyBeginCd == "Y" ? true : false
            a.optInCktCdBoolean = a.optInCktCd == "Y" ? true : false
            a.optInHCdBoolean = a.optInHCd == "Y" ? true : false
            return a
          })
          console.log(this.wiredTransportList)
          this.eventDiscoDevList = this.mds.eventDiscoDev.filter(a => a.recStusId).map(a => {
            a.readyBegin = a.readyBeginCd == "Y" ? true : false
            a.optInH = a.optInHCd == "Y" ? true : false
            a.optInCkt = a.optInCktCd == "Y" ? true : false
            return a
          })

          // PHYSICAL ADDRESS
          this.form.get("site.address").setValue(this.mds.streetAdr)
          this.form.get("site.state").setValue(this.mds.sttPrvnNme)
          this.form.get("site.bldg_flr_rm").setValue(this.mds.flrBldgNme)
          this.form.get("site.country").setValue(this.mds.ctryRgnNme)
          this.form.get("site.city").setValue(this.mds.ctyNme)
          this.form.get("site.zip").setValue(this.mds.zipCd)
          this.form.get("site.us").setValue(this.mds.usIntlCd)
          this.prevUSIntlCd = this.mds.usIntlCd;
          this.form.get("site.saContactHrs").setValue(this.mds.srvcAvlbltyHrs)
          this.form.get("site.saTimezone").setValue(this.mds.srvcTmeZnCd)
          this.form.get("site.installSite.poc").setValue(this.mds.instlSitePocNme)
          this.form.get("site.installSite.phoneCode").setValue(this.mds.instlSitePocIntlPhnCd)
          this.form.get("site.installSite.phone").setValue(this.mds.instlSitePocPhnNbr)
          this.form.get("site.installSite.cellphoneCode").setValue(this.mds.instlSitePocIntlCellPhnCd)
          this.form.get("site.installSite.cellphone").setValue(this.mds.instlSitePocCellPhnNbr)
          this.form.get("site.serviceAssurance.poc").setValue(this.mds.srvcAssrnPocNme)
          this.form.get("site.serviceAssurance.phoneCode").setValue(this.mds.srvcAssrnPocIntlPhnCd)
          this.form.get("site.serviceAssurance.phone").setValue(this.mds.srvcAssrnPocPhnNbr)
          this.form.get("site.serviceAssurance.cellphoneCode").setValue(this.mds.srvcAssrnPocIntlCellPhnCd)
          this.form.get("site.serviceAssurance.cellphone").setValue(this.mds.srvcAssrnPocCellPhnNbr)

          //PreStaging Fields
          this.form.get("shipping.shippedDate").setValue(this.mds.shippedDt)
          this.form.get("shipping.shipmentRefNo").setValue(this.mds.shipTrkRefrNbr)
          this.form.get("shipping.shipmentCustomerEmail").setValue(this.mds.shipCustEmailAdr)
          this.form.get("shipping.shippedDeviceSerialNumber").setValue(this.mds.shipDevSerialNbr)
          this.form.get("shipping.shipmentCarrierName").setValue(this.mds.shipCxrNme)
          this.form.get("shipping.altShipPocNme").setValue(this.mds.altShipPocNme)
          this.form.get("shipping.altShipPocPhnNbr").setValue(this.mds.altShipPocPhnNbr)
          this.form.get("shipping.altShipPocEmail").setValue(this.mds.altShipPocEmail)
          this.form.get("shipping.altShipStreetAdr").setValue(this.mds.altShipStreetAdr)
          this.form.get("shipping.altShipSttPrvnNme").setValue(this.mds.altShipSttPrvnNme)
          this.form.get("shipping.altShipFlrBldgNme").setValue(this.mds.altShipFlrBldgNme)
          this.form.get("shipping.altShipCtyNme").setValue(this.mds.altShipCtyNme)
          this.form.get("shipping.altShipCtryRgnNme").setValue(this.mds.altShipCtryRgnNme)
          this.form.get("shipping.altShipZipCd").setValue(this.mds.altShipZipCd)
          this.form.get("shipping.isAlreadyShipped").setValue(isAlreadyShipped)

          // CPE DELIVERY OPTION
          this.form.get("cpe.cpeDeliveryOption").setValue(this.mds.sprintCpeNcrId)
          //this.form.get("cpe.nidSerialNbr").setValue(mds.nidSerialNbr)
          //this.form.get("cpe.nidHostName").setValue(mds.nidHostNme)
          this.form.get("cpe.mdrNumber").setValue(this.mds.mdrNumber)
          this.form.get("cpe.cpeDispatchEmail").setValue(this.mds.cpeDspchEmailAdr)
          this.form.get("cpe.cpeDispatchComment").setValue(this.mds.cpeDspchCmntTxt)

          // REQUESTOR
          this.form.get("requestor.id").setValue(requestor.userId)
          this.form.get("requestor.name").setValue(requestor.fullNme)
          this.form.get("requestor.email").setValue(requestor.emailAdr)
          this.form.get("requestor.phone").setValue(requestor.phnNbr)
          this.form.get("requestor.cellphone").setValue(this.mds.reqorUserCellPhnNbr)

          // EVENT
          // Commented by Sarah Sandoval [20220124] - No requirement on OLD COWS to reset Escalation on Rework 
          //if(this.mds.eventStusId != 3)  { //Rework
          //  this.form.get("event.isEscalation").setValue(this.mds.esclCd);
          //}
          this.form.get("event.isEscalation").setValue(this.mds.esclCd);
          if (this.mds.esclCd) {
            let data1 = zip(
              this.systemConfigService.getSysCfgByName("MDSESCPolicyURL")
            )

            this.spnr.manageSpinner("show");
            data1.subscribe(res => {

              this.form.get('event.escalationPolicy').setValue(res[0][0].prmtrValuTxt);
              // this.setFormControlValue("escalationPolicy", res[0][0].prmtrValuTxt)
            }).add(() => {
              this.spnr.manageSpinner("hide");
            }),
            this.form.get("event.escalationReason").setValue(this.mds.esclReasId)
            this.form.get("event.primaryRequestDate").setValue(this.mds.primReqDt)
            this.form.get("event.secondaryRequestDate").setValue(this.mds.scndyReqDt)
            this.form.get("event.escalationBusinessJustification").setValue(this.mds.esclBusJustnTxt)
          }
          // let assignedToId = activators.map(a => a.asnToUserId).join(",");
          // this.form.get("event.assignedToId").setValue(assignedToId);
          //  this.form.get("event.assignedTo").setValue(activators.map(a => a.userDsplNme).join("; "))
          //  this.form.get("event.publishedEmailCC").setValue(activators.map(a => a.userDsplNme).join("; "))
          //  assignedToId = assignedToId.toString().split(',').join('|');
          //  let activators: User[] = []


          //   this.form.get("event.assignedToId").setValue( activators.map(a => a.userId).join(","))
          //   this.form.get("event.assignedTo").setValue(activators.map(a => a.dsplNme).join("; "))
          //   this.form.get("event.publishedEmailCC").setValue(activators.map(a => a.dsplNme).join("; "))

          this.form.get("event.publishedEmailCC").setValue(this.mds.pubEmailCcTxt)
          this.form.get("event.completeEmailCC").setValue(this.mds.cmpltdEmailCcTxt)

          // DISCONNECT
          this.form.get("disconnect.isRequireCoordination").setValue(this.mds.discMgmtCd == "Y" ? 1 : 0)
          this.form.get("disconnect.isTotalDisconnect").setValue((this.mds.fullCustDiscCd ? 1 : 0))
          this.form.get("disconnect.reasonForTotalDisconnect").setValue(this.mds.fullCustDiscReasTxt)
          this.form.get("disconnect.shortDescription").setValue(this.mds.shrtDes)

          //console.log(activators)
          //console.log(this.mds.frcdftCd)
          //console.log(this.mds.discMgmtCd)
          if (this.isWoob) {
            this.form.get("woobIpAddress").setValue(this.mds.woobIpAdr);
          }
          else if (!this.isWoob) {
            this.form.get("woobIpAddress").disable()
          }

          if (this.mds.frcdftCd) {
            // IF FT SET requestedActivityDate
            this.form.get("isGenericH1").setValue(this.mds.frcdftCd);
            this.form.get("requestedActivityDate").setValue(this.mds.strtTmst)
          } else {
            if (this.mds.discMgmtCd == "N") {
              this.form.get("requestedActivityDate").setValue(this.mds.strtTmst)
            } else {

              this.form.get("schedule.startDate").setValue(this.mds.strtTmst)
              this.form.get("schedule.endDate").setValue(this.mds.endTmst)
              this.form.get("schedule.eventDuration").setValue(this.mds.eventDrtnInMinQty)
              this.form.get("schedule.extraDuration").setValue(this.mds.extraDrtnTmeAmt)
              let assignedToId = activators.map(a => a.asnToUserId).join(",");
              this.form.get("schedule.assignedToId").setValue(assignedToId);
              this.form.get("schedule.assignedTo").setValue(activators.map(a => a.userDsplNme).join("; "))
              this.form.get("schedule.displayedAssignedTo").setValue(activators.map(a => a.userDsplNme).join("; "))
              if (this.isNtwkIntl) {
                this.form.get("schedule.displayedAssignedTo").setValue(activators.map(a => a.userEmail).join("; "))
              }
              else {
                this.form.get("schedule.displayedAssignedTo").setValue(activators.map(a => a.userDsplNme).join("; "))
              }
              this.form.get("schedule.softAssign").setValue(this.mds.softAssignCd)
              this.form.get("schedule.withConferenceBridge").setValue(this.mds.cnfrcBrdgId)
              this.form.get("schedule.bridgeNumber").setValue(this.mds.cnfrcBrdgNbr)
              this.form.get("schedule.bridgePin").setValue(this.mds.cnfrcPinNbr)
              this.form.get("schedule.onlineMeetingAdr").setValue(this.mds.onlineMeetingAdr)
              this.form.get("schedule.custLtrOptOut").setValue(this.mds.custLtrOptOut ? this.mds.custLtrOptOut : false)
              this.meetingUrlSub.next(this.mds.onlineMeetingAdr);
              assignedToId = assignedToId.toString().split(',').join('|');
              //this.userService.checkForDoubleBooking(this.mds.strtTmst, this.mds.endTmst, assignedToId, this.id).subscribe(res => {
              //  this.dblBookingMessage = res;
              //})

              // Added condition to prevent double booking checking on Network International
              if (this.ntwkIntlUser != null) {
                if (this.form.get("schedule.assignedToId").value != this.ntwkIntlUser.userId) {
                  this.spnr.manageSpinner("show");
                  this.userService.checkForDoubleBooking(this.mds.strtTmst, this.mds.endTmst, assignedToId, this.id).subscribe(res => {
                    this.dblBookingMessage = res;
                  }).add(() => {
                    this.spnr.manageSpinner("hide");
                  });
                }
              }
            }
          }

          // Filter Workflow Status according to MDS Details
          if (this.showMDS) {
            if (this.mdsActivityType.value == 2 || this.mdsActivityType.value == 3) {
              this.workflowStatusListFiltered = this.workflowStatusList.filter(a => !(a.wrkflwStusId == 12 || a.wrkflwStusId == 13))
            } else if (this.mdsActivityType.value == 5) {
              if (this.eventStatus.value != 10 && this.eventStatus.value != 5) {
                this.workflowStatusListFiltered = this.workflowStatusList.filter(a => a.wrkflwStusId != 6)
              } else {
                this.workflowStatusListFiltered = this.workflowStatusList
              }
            }
          } else {
            this.workflowStatusListFiltered = this.workflowStatusList.filter(a => !(a.wrkflwStusId == 12 || a.wrkflwStusId == 13))
          }
          //console.log(`wf fltd 1` + JSON.stringify(this.workflowStatusListFiltered))
          //console.log(`wf 1` + JSON.stringify(this.workflowStatusList))
          // If current Workflow Status is not on the list
          //console.log(this.workflowStatusListFiltered.find(a => a.wrkflwStusId == this.mds.wrkflwStusId))
          //console.log(this.workflowStatusListFiltered.length)
          //console.log(this.mds.wrkflwStusId)
          //if (this.workflowStatusListFiltered.find(a => a.wrkflwStusId == this.mds.wrkflwStusId) == undefined || this.workflowStatusListFiltered == []) {
          //  console.log(workflowStatus)
          //  if (this.isMember && !this.isReviewer && !this.isActivator) {

          //  }
          //  else {
          //    if (workflowStatus.find(a => a.wrkflwStusId == this.mds.wrkflwStusId)) {
          //      this.workflowStatusListFiltered = workflowStatus.filter(a => a.wrkflwStusId == this.mds.wrkflwStusId);
          //    }
          //    else {
          //      this.workflowStatusListFiltered;
          //    }
          //    //this.workflowStatusListFiltered = [workflowStatus.find(a => a.wrkflwStusId == this.mds.wrkflwStusId), ...this.workflowStatusListFiltered]
          //  }
          //}
          //console.log(`wf fltd 2` + JSON.stringify(this.workflowStatusListFiltered))
          //console.log(`wf 2` + JSON.stringify(this.workflowStatusList))
          //console.log(`wfstusid` + JSON.stringify(this.mds.wrkflwStusId))
          this.form.get("preconfigCompleted").setValue(this.mds.preCfgCmpltCd ? "Y" : "N")

          if (this.isMember && !this.isReviewer && !this.isActivator && this.mds.wrkflwStusId == 4) {
            this.form.get("workflowStatus").setValue(this.workflowStatusListFiltered[0].wrkflwStusId)
          }
          else {
            if (this.workflowStatusListFiltered.length > 0 && this.workflowStatusListFiltered.find(a => a.wrkflwStusId == this.mds.wrkflwStusId)) {
              this.form.get("workflowStatus").setValue(this.mds.wrkflwStusId)
            }
            else if (this.workflowStatusListFiltered.length > 0 && !this.workflowStatusListFiltered.find(a => a.wrkflwStusId == this.mds.wrkflwStusId)) {
              this.form.get("workflowStatus").setValue(this.workflowStatusListFiltered[0].wrkflwStusId)
            }
            else {
              this.form.get("workflowStatus").setValue(this.mds.wrkflwStusId)
            }
          }

          if (this.isActivator && this.workflowStatus.value == WORKFLOW_STATUS.Reschedule) {
            let forDisplayOnly: Workflow[] = [
              {
                wrkflwStusId: 5,
                wrkflwStusDes: "Reschedule",
                recStusId: 1,
                creatDt: null,
                creatByUserId: 0,
                modfdDt: null,
                modfdByUserId: 0
              }
            ]
            this.workflowStatusListFiltered = forDisplayOnly;
            this.form.get("mdsFailCode").patchValue(this.mds["failReasId"].toString())
          }

          let activityTypeCompletionList: number[] = []
          activityTypeCompletionList = this.mds.mdsCmpltCd ? [...activityTypeCompletionList, 1] : activityTypeCompletionList
          activityTypeCompletionList = this.mds.ntwkCmpltCd ? [...activityTypeCompletionList, 2] : activityTypeCompletionList
          activityTypeCompletionList = this.mds.vasCmpltCd ? [...activityTypeCompletionList, 3] : activityTypeCompletionList
          //console.log(this.mds.mdsCmpltCd)
          //console.log(this.mds.ntwkCmpltCd)
          //console.log(this.mds.vasCmpltCd)
          //console.log(activityTypeCompletionList)
          this.form.get("activityCompletion").setValue(activityTypeCompletionList)

          this.isCpe = this.cpeDeviceList.length > 0 ? true : false
          this.isMns = this.mnsOrderList.length > 0 ? true : false

          //isDone = true
        }).add(() => {
          this.setDefaultFields();
          this.spnr.manageSpinner("hide");
        });
    }
    else {
      this.spnr.manageSpinner("show");
      this.isPageLoad = false
      let data = zip(
        this.userService.getLoggedInUser(),
        this.mdsNetworkActivityTypeService.get(),
        this.mplsActivityTypeService.getByEventId(this.eventType),
        this.mdsEventService.getVASSubTypes(),
        this.mdsActivityTypeService.get(),
        this.mdsMacActivityService.getForLookup(),//this.mdsMacActivityService.get(),
        this.devManfSrvc.get(),
        this.devModelSrvc.getForLookup(),//this.devModelSrvc.get(),
        this.saSiteSupportService.getForLookup(),//this.saSiteSupportService.getServiceAssuranceSiteSupport(),
        this.serviceTierService.getForLookup(),//this.serviceTierService.getServiceTier(),
        this.mdsServiceTypeService.get(),
        this.mds3rdPartyVendorService.getForLookup(),//this.mds3rdPartyVendorService.get(),
        this.mds3rdPartyServiceService.getForLookup(),//this.mds3rdPartyServiceService.get(),
        this.serviceDelivery.getForControl(),
        this.workflowService.getForEvent(this.eventType, EVENT_STATUS.Visible),
        this.userService.getFinalUserProfile(this.userService.loggedInUser.adid, this.eventType),
        this.systemConfigService.getSysCfgByName("MDSCPEVldtn"),
        this.systemConfigService.getSysCfgByName("MDSCPEPostPblshVldtn"),
        this.systemConfigService.getSysCfgByName("MDSCPESbmtVldStuses"),
        this.systemConfigService.getSysCfgByName("MDSCPEPblshVldStuses"),
        this.mdsEventService.calDtwithBusiDays("2|3|0"),
        this.userService.getUserByADID("NtwkIntl"),
        this.mdsEventService.calDtwithBusiDaysIncludingWeekend("2|3|0")
      )

      data.subscribe(
        res => {
          let user = res[0] as User
          this.form.get("requestor.id").setValue(user.userId)
          this.form.get("requestor.name").setValue(user.fullNme)
          this.form.get("requestor.email").setValue(user.emailAdr)
          this.form.get("requestor.cellphone").setValue(user.cellPhnNbr)
          this.form.get("requestor.phone").setValue(user.phnNbr)

          this.activityTypeList = res[1] as MDSNetworkActivityType[]
          this.networkActivityTypeList = res[2] as KeyValuePair[]
          this.vasTypeList = res[3]
          this.mdsActivityTypeList = (res[4] as MDSActivityType[]).filter(a => ![1, 4, 6].includes(a.mdsActyTypeId)) // Id IN (1,4,6) is not being used anymore
          this.mdsMacActivityList = res[5] as MDSMACActivity[]
          this.deviceManfList = res[6] as DeviceManufacturer[]
          this.deviceModelList = res[7] as DeviceModel[]
          this.saSiteSupportList = res[8] as ServiceAssuranceSiteSupport[]
          this.serviceTierList = res[9] as MDSServiceTier[]
          this.mdsServiceTypeList = res[10] as MdsServiceType[]
          this.mds3rdPartyVendorList = res[11] as MDS3rdPartyVendor[]
          this.mds3rdPartyServiceList = res[12] as MDS3rdPartyService[]
          this.serviceDeliveryList = res[13]
          this.workflowStatusList = (res[14] as Workflow[]).filter(a => !a.wrkflwStusDes.includes("UAT"))
          this.workflowStatusListFiltered = this.workflowStatusList
          var usrPrf = res[15] as UserProfile;
          this.mdsCpeVldtn = res[16][0].prmtrValuTxt
          this.mdsCpePostPublishValidation = res[17][0].prmtrValuTxt as string
          this.mdsCpeSubmitValidationStatuses = (res[18][0].prmtrValuTxt as string).split("/")
          this.mdsCpePublishValidationStatuses = (res[19][0].prmtrValuTxt as string).split("/")
          //console.log(JSON.stringify(res[20]));
          var dtArr = (res[20] as string).split("|");
          var dtArr2 = (res[22] as string).split("|");
          //console.log(JSON.stringify(dtArr));
          this.dtNow = new Date(dtArr[0]);
          this.dt72 = new Date(dtArr[1]);
          this.dt48 = new Date(dtArr[2]);

          // Business days including weekend
          this.dt72WithWeekend = new Date(dtArr2[1]);
          this.dt48WithWeekend = new Date(dtArr2[2]);
          if (usrPrf != null) {
            this.profile = this.helper.getFinalProfileName(usrPrf);
          }

          this.showEditButton = (res[14] as Workflow[]).filter(a => !a.wrkflwStusDes.includes("UAT")).length > 0
          this.ntwkIntlUser = res[21] as User;
        },
        error => {}, 
        () => {
          this.setDefaultFields(); 
        }).add(() => {
          this.spnr.manageSpinner("hide");
        });
    }

    //this.setDefaultFields()

    //console.log(`IS LOCKED: ${this.isLocked}`)
    //console.log(`IS EDIT MODE: ${this.editMode}`)
  }

  setDefaultFields() {
    this.form.disable()
    this.option.mds.userFinderForContactEmailAddresses.isEnabled = false;

    if (this.id > 0) {
      if (this.editMode) {
        // REVERTED DUE TO PROD ISSUE
        if (this.isActivator) {
          this.form.get("comments").enable()
          this.form.get("mdsFailCode").enable()
          this.form.get("preconfigCompleted").enable()
          this.form.get("activityCompletion").enable()
          this.form.get("workflowStatus").enable()
          if (this.form.get("eventStatus").value != EVENT_STATUS.Fulfilling) {
            this.form.get("shipping").enable()
            this.form.get("shipping.shippedDate").disable()
            this.form.get("shipping.shipmentRefNo").disable()
            this.form.get("shipping.shippedDeviceSerialNumber").disable()
            this.form.get("shipping.shipmentCarrierName").disable()
            this.form.get("shipping.isAlreadyShipped").disable()
          } else {
            this.form.get("shipping.shippedDate").enable()
            this.form.get("shipping.shipmentRefNo").enable()
            this.form.get("shipping.shippedDeviceSerialNumber").enable()
            this.form.get("shipping.shipmentCarrierName").enable()
            //this.form.get("shipping.isAlreadyShipped").enable()
          }

          this.option.mds.userFinderForContactEmailAddresses.isEnabled = false;
          this.option.event.userFinderForPublishedEmail.isEnabled = false;
          this.option.event.userFinderForCompleteEmail.isEnabled = false;
          this.userService.isCheckCurrentSlotShown = false;
          this.userService.isSlotPickerShown = false;
          this.option.schedule.checkCurrentSlot.isShown = false;
          //this.option.schedule.launchPicker.isShown = false
          //console.log(this.option.schedule.checkCurrentSlot)
        }
        else if (this.isReviewer) {
          this.form.get("activityType").enable()
          this.form.get("customerSOW").enable()
          this.form.get("mds.customerSOW").enable()
          this.form.get("ipmDescription").enable()
          this.setValidators(this.form, "ipmDescription", [Validators.maxLength(500)])
          this.form.get("shortDescription").enable()
          this.form.get("network").enable()
          this.form.get("mds").enable()
          this.form.get("disconnect").enable()
          this.form.get("woobIpAddress").enable()
          this.form.get("site").enable()
          this.form.get("cpe").enable()
          if (this.form.get("eventStatus").value != EVENT_STATUS.Fulfilling) {
            this.form.get("shipping").enable()
            this.form.get("shipping.shippedDate").disable()
            this.form.get("shipping.shipmentRefNo").disable()
            this.form.get("shipping.shippedDeviceSerialNumber").disable()
            this.form.get("shipping.shipmentCarrierName").disable()
            this.form.get("shipping.isAlreadyShipped").disable()
          } else {
            this.form.get("shipping.shippedDate").enable()
            this.form.get("shipping.shipmentRefNo").enable()
            this.form.get("shipping.shippedDeviceSerialNumber").enable()
            this.form.get("shipping.shipmentCarrierName").enable()
            //this.form.get("shipping.isAlreadyShipped").enable()
          }
          this.form.get("requestor").enable()
          this.form.get("event").enable()
          this.form.get("isGenericH1").enable()
          this.form.get("fastTrackType").enable()
          this.form.get("requestedActivityDate").enable()
          this.form.get("comments").enable()
          this.form.get("isSuppressedEmails").enable()
          this.form.get("schedule").enable()
          this.form.get("schedule.endDate").disable()
          if (!this.isReviewer)
            this.form.get("schedule.displayedAssignedTo").disable()
          this.form.get("workflowStatus").enable()

          this.option.lookup_h6.isEnabled = true;
          this.option.lookup_h1.isEnabled = true;
          this.option.lookup_site.isEnabled = true;
          this.option.requestor.userFinder.isEnabled = true;
          this.userService.isCheckCurrentSlotShown = true;
          this.userService.isSlotPickerShown = this.userService.isMdsReviewer();
          this.option.schedule.checkCurrentSlot.isShown = true;
          //this.option.schedule.launchPicker.isShown = this.userService.isMdsReviewer();
        }
        else {
          console.log(this.eventStatus.value)
          if (this.eventStatus.value == EVENT_STATUS.Visible || this.eventStatus.value == EVENT_STATUS.Rework) {
            this.form.get("activityType").enable()
            this.form.get("customerSOW").enable()
            this.form.get("mds.customerSOW").enable()
            this.form.get("ipmDescription").enable()
            this.setValidators(this.form, "ipmDescription", [Validators.maxLength(500)])
            this.form.get("shortDescription").enable()
            this.form.get("network").enable()
            this.form.get("mds").enable()
            this.form.get("disconnect").enable()
            this.form.get("woobIpAddress").enable()
            this.form.get("site").enable()
            this.form.get("cpe").enable()
            if (this.form.get("eventStatus").value != EVENT_STATUS.Fulfilling) {
              this.form.get("shipping").enable()
              this.form.get("shipping.shippedDate").disable()
              this.form.get("shipping.shipmentRefNo").disable()
              this.form.get("shipping.shippedDeviceSerialNumber").disable()
              this.form.get("shipping.shipmentCarrierName").disable()
              this.form.get("shipping.isAlreadyShipped").disable()
            } else {
              this.form.get("shipping.shippedDate").enable()
              this.form.get("shipping.shipmentRefNo").enable()
              this.form.get("shipping.shippedDeviceSerialNumber").enable()
              this.form.get("shipping.shipmentCarrierName").enable()
              //this.form.get("shipping.isAlreadyShipped").enable()
            }
            this.form.get("requestor").enable()
            this.form.get("event").enable()
            this.form.get("isGenericH1").enable()
            this.form.get("fastTrackType").enable()
            this.form.get("requestedActivityDate").enable()
            this.form.get("comments").enable()
            this.form.get("isSuppressedEmails").enable()
            this.form.get("schedule").enable()
            this.form.get("schedule.endDate").disable()
            if (!this.isReviewer)
              this.form.get("schedule.displayedAssignedTo").disable()
            this.form.get("workflowStatus").enable()

            this.option.lookup_h6.isEnabled = true;
            this.option.lookup_h1.isEnabled = true;
            this.option.lookup_site.isEnabled = true;
            this.option.requestor.userFinder.isEnabled = true;
            this.userService.isCheckCurrentSlotShown = true;
            this.userService.isSlotPickerShown = this.userService.isMdsReviewer();
            this.option.schedule.checkCurrentSlot.isShown = !(this.form.get("event.isEscalation").value);
            //this.option.schedule.launchPicker.isShown = this.userService.isMdsReviewer();
          }
          else {
            this.form.get("comments").enable()
            this.form.get("workflowStatus").enable()

            this.option.mds.userFinderForContactEmailAddresses.isEnabled = false;
            this.option.event.userFinderForPublishedEmail.isEnabled = false;
            this.option.event.userFinderForCompleteEmail.isEnabled = false;
            this.userService.isCheckCurrentSlotShown = false;
            this.userService.isSlotPickerShown = false;
            this.option.schedule.checkCurrentSlot.isShown = false;
          }
        }

        // OLD CODES
        //if (!this.isActivator) {
        //  this.form.get("activityType").enable()
        //  this.form.get("customerSOW").enable()
        //  this.form.get("mds.customerSOW").enable()
        //  this.form.get("ipmDescription").enable()
        //  this.setValidators(this.form, "ipmDescription", [Validators.maxLength(500)])
        //  this.form.get("shortDescription").enable()
        //  this.form.get("network").enable()
        //  this.form.get("mds").enable()
        //  this.form.get("disconnect").enable()
        //  this.form.get("woobIpAddress").enable()
        //  this.form.get("site").enable()
        //  this.form.get("cpe").enable()
        //  if (this.form.get("eventStatus").value != EVENT_STATUS.Fulfilling) {
        //    this.form.get("shipping").enable()
        //    this.form.get("shipping.shippedDate").disable()
        //    this.form.get("shipping.shipmentRefNo").disable()
        //    this.form.get("shipping.shippedDeviceSerialNumber").disable()
        //    this.form.get("shipping.shipmentCarrierName").disable()
        //    this.form.get("shipping.isAlreadyShipped").disable()
        //  } else {
        //    this.form.get("shipping.shippedDate").enable()
        //    this.form.get("shipping.shipmentRefNo").enable()
        //    this.form.get("shipping.shippedDeviceSerialNumber").enable()
        //    this.form.get("shipping.shipmentCarrierName").enable()
        //    //this.form.get("shipping.isAlreadyShipped").enable()
        //  }
        //  this.form.get("requestor").enable()
        //  this.form.get("event").enable()
        //  this.form.get("isGenericH1").enable()
        //  this.form.get("fastTrackType").enable()
        //  this.form.get("requestedActivityDate").enable()
        //  this.form.get("comments").enable()
        //  this.form.get("isSuppressedEmails").enable()
        //  this.form.get("schedule").enable()
        //  this.form.get("schedule.endDate").disable()
        //  if (!this.isReviewer)
        //    this.form.get("schedule.displayedAssignedTo").disable()
        //  this.form.get("workflowStatus").enable()

        //  this.option.lookup_h6.isEnabled = true;
        //  this.option.lookup_h1.isEnabled = true;
        //  this.option.lookup_site.isEnabled = true;
        //  this.option.requestor.userFinder.isEnabled = true;
        //  this.option.schedule.checkCurrentSlot.isShown = true;
        //  this.option.schedule.launchPicker.isShown = this.userService.isMdsReviewer();
        //}
        //else {
        //  this.form.get("comments").enable()
        //  this.form.get("mdsFailCode").enable()
        //  this.form.get("preconfigCompleted").enable()
        //  this.form.get("activityCompletion").enable()
        //  this.form.get("workflowStatus").enable()
        //  if (this.form.get("eventStatus").value != EVENT_STATUS.Fulfilling) {
        //    this.form.get("shipping").enable()
        //    this.form.get("shipping.shippedDate").disable()
        //    this.form.get("shipping.shipmentRefNo").disable()
        //    this.form.get("shipping.shippedDeviceSerialNumber").disable()
        //    this.form.get("shipping.shipmentCarrierName").disable()
        //    this.form.get("shipping.isAlreadyShipped").disable()
        //  } else {
        //    this.form.get("shipping.shippedDate").enable()
        //    this.form.get("shipping.shipmentRefNo").enable()
        //    this.form.get("shipping.shippedDeviceSerialNumber").enable()
        //    this.form.get("shipping.shipmentCarrierName").enable()
        //    //this.form.get("shipping.isAlreadyShipped").enable()
        //  }

        //  this.option.event.userFinderForPublishedEmail.isEnabled = false;
        //  this.option.event.userFinderForCompleteEmail.isEnabled = false;
        //  this.option.schedule.checkCurrentSlot.isShown = false;
        //  this.option.schedule.launchPicker.isShown = false
        //  //console.log(this.option.schedule.checkCurrentSlot)
        //}
      }
    } else {
      this.form.get("activityType").enable()
      this.form.get("customerSOW").enable()
      this.form.get("mds.customerSOW").enable()
      this.form.get("ipmDescription").enable()
      this.setValidators(this.form, "ipmDescription", [Validators.maxLength(500)])
      this.form.get("shortDescription").enable()
      this.form.get("network").enable()
      this.form.get("mds").enable()
      this.form.get("disconnect").enable()
      this.form.get("woobIpAddress").enable()
      this.form.get("site").enable()
      this.form.get("cpe").enable()
      this.form.get("shipping").enable()
      this.form.get("shipping.shippedDate").disable()
      this.form.get("shipping.shipmentRefNo").disable()
      this.form.get("shipping.shippedDeviceSerialNumber").disable()
      this.form.get("shipping.shipmentCarrierName").disable()
      this.form.get("shipping.isAlreadyShipped").disable()
      this.form.get("requestor").enable()
      this.form.get("event").enable()
      this.form.get("isGenericH1").enable()
      this.form.get("fastTrackType").enable()
      this.form.get("requestedActivityDate").enable()
      this.form.get("comments").enable()
      this.form.get("workflowStatus").enable()
      this.form.get("preconfigCompleted").enable()
      this.form.get("isSuppressedEmails").enable()
      this.form.get("schedule").enable()
      this.form.get("schedule.endDate").disable()
      this.form.get("schedule.displayedAssignedTo").disable()
      //this.form.get("activityCompletion").enable()

      this.option.lookup_h6.isEnabled = true;
      this.option.lookup_h1.isEnabled = true;
      this.option.lookup_site.isEnabled = true;
      this.option.requestor.userFinder.isEnabled = true;
      this.userService.isCheckCurrentSlotShown = true;
      this.userService.isSlotPickerShown = this.userService.isMdsReviewer();
      this.option.schedule.checkCurrentSlot.isShown = (this.form.get("event.isEscalation").value && this.isOnlyMember) ? false : true;
      //this.option.schedule.launchPicker.isShown = this.userService.isMdsReviewer();
    }

    //console.log(this.isReviewer)
    //this.option.schedule.softAssign.isShown = true

    this.setValidators(this.form, "activityType", [Validators.required])
    this.setValidators(this.form, "comments", [Validators.maxLength(20000)])
    this.isAllowedForEdit = this.editMode && !this.isActivator && (this.isReviewer || (this.isMember && (this.eventStatus.value == EVENT_STATUS.Visible || this.eventStatus.value == EVENT_STATUS.Rework)))
    this.siteServiceListAllowEdit = this.editMode && (this.isActivator && !(this.eventStatus.value == EVENT_STATUS.Visible || this.eventStatus.value == EVENT_STATUS.Submitted || this.eventStatus.value == EVENT_STATUS.Rework));
  
    if(this.isOnlyMember) {
      this.form.get("schedule.eventDuration").disable()
    }
  }

  getVASCECd() {
    if (this.checkMDSNtwkActy("hasVAS") && this.checkMDSNtwkActy("hasNtwk") && this.showNID) {
      return 5
    } else if (this.checkMDSNtwkActy("hasVAS") && this.showNID) {
      return 4
    } else if (this.checkMDSNtwkActy("hasNtwk") && this.showNID) {
      return 3
    } else if (this.checkMDSNtwkActy("hasVAS") && this.checkMDSNtwkActy("hasNtwk") && !this.showNID) {
      return 2
    } else if (this.checkMDSNtwkActy("hasVAS") && !this.showNID) {
      return 1
    } else {
      return 0
    }

    // Difficult to read
    //return (this.checkMDSNtwkActy("hasVAS") && this.checkMDSNtwkActy("hasNtwk") && this.showNID) ? 5 :
    //  ((this.checkMDSNtwkActy("hasVAS") && this.showNID) ? 4 : ((this.checkMDSNtwkActy("hasNtwk") && this.showNID) ? 3 :
    //    ((this.checkMDSNtwkActy("hasVAS") && this.checkMDSNtwkActy("hasNtwk") && !this.showNID) ? 2 : ((this.checkMDSNtwkActy("hasVAS") && !this.showNID) ? 1 : 0))));
  }

  isSpclPrj() {
    if (!this.checkMDSNtwkActy("MDSOnly")) {
      return this.hasSpclPrj;
    }
    else return false;
  }

  checkMDSNtwkActy(sMDSNtwkActy: string) {
    let hasMDS = this.activityType.value.includes(1);
    let hasNtwk = this.activityType.value.includes(2) || this.activityType.value.includes(4);
    let hasVAS = this.activityType.value.includes(3);

    if (sMDSNtwkActy == "hasMDS")
      return hasMDS;
    else if (sMDSNtwkActy == "hasNtwk")
      return hasNtwk;
    else if (sMDSNtwkActy == "hasVAS")
      return hasVAS;
    else if (sMDSNtwkActy == "MDSOnly")
      return hasMDS && !hasNtwk && !hasVAS;
    else if (sMDSNtwkActy == "NtwkOnly")
      return !hasMDS && hasNtwk && !hasVAS;
    else if (sMDSNtwkActy == "VASOnly")
      return !hasMDS && !hasNtwk && hasVAS;
    else if (sMDSNtwkActy == "MDSNtwk")
      return hasMDS && hasNtwk && !hasVAS;
    else if (sMDSNtwkActy == "MDSVAS")
      return hasMDS && !hasNtwk && hasVAS;
    else if (sMDSNtwkActy == "NtwkVAS")
      return !hasMDS && hasNtwk && hasVAS;
    else if (sMDSNtwkActy == "MDSNtwkVAS")
      return hasMDS && hasNtwk && hasVAS;
    else
      return false;
  }

  // EVENT HANDLING
  submit() {
    this.spinner.show();
    this.isSubmitted = true;

    let isSucceed = false
    //let isformValid = false
    //console.log(`workflow status : ${this.workflowStatus.value}`)

    this.validateFormData().then(res => {
      isSucceed = res && this.form.valid
      //console.log('validateFormData() - res: ' + res)
      //console.log('validateFormData() - form.valid: ' + this.form.valid)
      //console.log(this.form.errors)
      //console.log(this.getErrors(this.form))
      //console.log(`validateFormData() - IS SUCCEED: ${isSucceed}`)
      //console.log(`validateFormData() - Error List: ${JSON.stringify(this.errors)}`)
      if (isSucceed) {
        let mds = this.getFormDetails()
        console.log(mds)

        //if (this.id > 0) {
        //  // Update
        //  this.mdsEventService.update(this.id, mds).subscribe(res => {
        //    this.helper.notifySavedFormMessage(`IPSD Event - ${this.id}`, Global.NOTIFY_TYPE_SUCCESS, false, null)
        //    this.cancel()
        //  }, error => {
        //    this.helper.notifySavedFormMessage(`IPSD Event - ${this.id}`, Global.NOTIFY_TYPE_ERROR, false, error)
        //    this.spinner.hide()
        //  }, () => {

        //  });
        //} else {
        //  this.mdsEventService.create(mds).subscribe(res => {
        //    if (res != null) {
        //      this.helper.notify(`Successfully created IPSD Event - ${res.eventId}.`, Global.NOTIFY_TYPE_SUCCESS);
        //    }
        //    else {
        //      this.helper.notifySavedFormMessage("IPSD Event", Global.NOTIFY_TYPE_SUCCESS, true, null)
        //    }
        //    this.cancel()
        //  }, error => {
        //    this.helper.notifySavedFormMessage("IPSD Event", Global.NOTIFY_TYPE_ERROR, true, error)
        //    this.spinner.hide()
        //  }, () => {

        //  });
        //}

        // COMMENTED FOR A WHILE
        let isConfirmed = true
        //console.log(this.isActivator)
        //console.log(this.devCompletionList)
        //console.log(this.devCompletionList.filter(a => a.cmpltdCd == true && !a.isAlreadyCompleted).length)
        if (this.isActivator && this.devCompletionList.filter(a => a.cmpltdCd == true && !a.isAlreadyCompleted).length > 0) {
          this.spinner.hide();
          $("#activityCompletionDialog").modal("show");

          // Check if at least one Device is to be completed
          //isConfirmed = confirm("You have marked at least one device complete in the Device Completion Table and you are about to save this event.\n\nSaving the event will send completion messages to M5 and the redesign portal, which will trigger billing.\n\nIf all activity for the device(s) selected is not complete, the completion box should be deselected before proceeding.")
        } else {
          this.saveEvent()
        }

        //if (isConfirmed) {
        //  let mds = this.getFormDetails()
        //  console.log(mds)

        //  if (this.id > 0) {
        //    // Update
        //    this.mdsEventService.update(this.id, mds).subscribe(res => {
        //      this.helper.notifySavedFormMessage(`IPSD Event - ${this.id}`, Global.NOTIFY_TYPE_SUCCESS, false, null)
        //      this.cancel()
        //    }, error => {
        //      this.helper.notifySavedFormMessage(`IPSD Event - ${this.id}`, Global.NOTIFY_TYPE_ERROR, false, error)
        //      this.spinner.hide()
        //    }, () => {

        //    });
        //  } else {
        //    this.mdsEventService.create(mds).subscribe(res => {
        //      if (res != null) {
        //        this.helper.notify(`Successfully created IPSD Event - ${res.eventId}.`, Global.NOTIFY_TYPE_SUCCESS);
        //      }
        //      else {
        //        this.helper.notifySavedFormMessage("IPSD Event", Global.NOTIFY_TYPE_SUCCESS, true, null)
        //      }
        //      this.cancel()
        //    }, error => {
        //      this.helper.notifySavedFormMessage("IPSD Event", Global.NOTIFY_TYPE_ERROR, true, error)
        //      this.spinner.hide()
        //    }, () => {

        //    });
        //  }
        //} else {
        //  this.isSubmitted = false
        //  this.spinner.hide()
        //}
      } else {
        this.isSubmitted = false
        this.spinner.hide()
      }
    })
  }

  onContinueClicked() {
    $("#activityCompletionDialog").modal("hide");
    this.saveEvent()
  }

  onStopClicked() {
    $("#activityCompletionDialog").modal("hide");
    console.log(this.isSubmitted)
    this.isSubmitted = false
    console.log(this.isSubmitted)
  }

  saveEvent() {
    this.spinner.show();
    let mds = this.getFormDetails()
    console.log(mds)

    if (this.id > 0) {
      // Update
      this.mdsEventService.update(this.id, mds).subscribe(res => {
        this.helper.notifySavedFormMessage(`IPSD Event - ${this.id}`, Global.NOTIFY_TYPE_SUCCESS, false, null);
        this.spinner.hide();
        // Commented out by Sarah Sandoval [20220201] - Transfer event unlocking on controller for performance issue
        //let isCancel = this.isActivator && this.workflowStatus.value === WORKFLOW_STATUS.InProgress
        //this.cancel(!isCancel);
        
        // Added by Sarah Sandoval [20220201] - Unlocking happens on Controller and will stay on Event form for Activator/InProgress
        const lockEvent = this.isActivator && this.workflowStatus.value === WORKFLOW_STATUS.InProgress
        if (lockEvent) {
          window.location.reload();
        }
        else {
          this.router.navigate(['event/mds']);
        }
      }, error => {
        this.helper.notifySavedFormMessage(`IPSD Event - ${this.id}`, Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
          this.isSubmitted = false
      });
    } else {
      this.mdsEventService.create(mds).subscribe(res => {
        if (res != null) {
          this.helper.notify(`Successfully created IPSD Event - ${res.eventId}.`, Global.NOTIFY_TYPE_SUCCESS);
        }
        else {
          this.helper.notifySavedFormMessage("IPSD Event", Global.NOTIFY_TYPE_SUCCESS, true, null)
        }
        this.cancel()
      }, error => {
        this.helper.notifySavedFormMessage("IPSD Event", Global.NOTIFY_TYPE_ERROR, true, error)
        this.spinner.hide()
      }, () => {

      });
    }
  }

  //  checkDblBooking(event: any = null) {
  //    this.spinner.show();

  //   let publishedEmailCC = this.form.get("event.publishedEmailCC").value;
  //   publishedEmailCC = publishedEmailCC.toString().split(',').join('|');
  // }

  edit() {
    this.spinner.show()
    this.evntLockSrvc.lock(this.id).subscribe(
      res => {
        this.editMode = true;
        this.isActivationDateDisabled = !this.isMember ? false : true;
        this.isCommentDisabled = !this.isMember ? false : true;
        //console.log("123")
        this.setDefaultFields()
        this.option.schedule.checkCurrentSlot.isShown = this.isShowCheckCurrentslot() && (!(this.isOnlyMember && this.form.get("event.isEscalation").value));
      },
      error => {
        this.spinner.hide()
        //this.helper.notify('An error occurred while locking the Event.', Global.NOTIFY_TYPE_ERROR);
      },
      () => {
        this.spinner.hide()
      }
    );
  }

  cancel(isCancel: boolean = true) {
    //this.spinner.show()
    this.evntLockSrvc.unlock(this.id).subscribe(
      res => {
        this.isSubmitted = false
        if(isCancel) {
          this.router.navigate(['event/mds']);
        }else {
          window.location.reload();
        }
      },
      error => {
        this.helper.notify('An error occurred while unlocking the Event.', Global.NOTIFY_TYPE_ERROR);
        this.spinner.hide()
        this.isSubmitted = false
      }
    );
  }
  // EVENT HANDLING

  // MAIN INFO
  onActivityTypeChanged(e) {

    this.activityTypeValSub.next(e.value);

    // #Begin Added for PJ022373 - NtwkIntl type
    if (this.isNtwkIntl) {
      this.activityType.setValue([4]);
      this.activityTypeList.forEach(i => {
        if (i.ntwkActyTypeId != 4) {
          i.disabled = true;
        }
      })

      // Assigned Activator
      if (this.isMember && !this.isReviewer && !this.isActivator) {
        this.form.get("schedule.assignedToId").setValue(this.ntwkIntlUser.userId)
        this.form.get("schedule.assignedTo").setValue(this.ntwkIntlUser.dsplNme)
        this.form.get("schedule.displayedAssignedTo").setValue(this.ntwkIntlUser.emailAdr)
      }

      // Default to Intl
      this.form.get("site.us").setValue("I")
    }
    else {
      this.activityTypeList.forEach(i => { i.disabled = false; })
    }
    // #End Added for PJ022373 - NtwkIntl type

    this.setValidators(this.form, "shortDescription", [Validators.required, Validators.maxLength(2500)])
    if (this.showIPMDescription) {
      this.setValidators(this.form, "ipmDescription", [Validators.maxLength(500)])
    }
    if (this.showNetwork) {
      this.setValidators(this.form, "customerSOW", [])
      this.setValidators(this.form, "mds.customerSOW", [])
      if (this.isCEChng) {
        this.setValidators(this.form, "network.h6", [Validators.required])
      }
      else {
        this.setValidators(this.form, "network.h6", [Validators.required, Validators.pattern(/^\d{9}$/)])
      }
      this.setValidators(this.form, "network.h1", [Validators.required, Validators.pattern(/^\d{9}$/)])
      this.setValidators(this.form, "network.customerName", [Validators.required])
      this.setValidators(this.form, "network.networkActivityType", [Validators.required])
    }

    if (this.showMDS) {
      this.setValidators(this.form, "customerSOW", [Validators.required])
      this.setValidators(this.form, "mds.customerSOW", [Validators.required])
      this.setValidators(this.form, "mds.h1", [Validators.required, Validators.pattern(/^\d{9}$/)])
      this.setValidators(this.form, "mds.odieCustomerName", [Validators.required])
      //this.setValidators(this.form, "mds.teamPDL", [Validators.required])
      this.setValidators(this.form, "mds.mdsActivityType", [Validators.required])
    }
    if (this.isWoob) {
      this.setValidators(this.form, "woobIpAddress", [Validators.required])
    }
    if (this.showDisconnect) {
      this.setValidators(this.form, "customerSOW", [Validators.required])
      this.setValidators(this.form, "mds.customerSOW", [Validators.required])
      this.setValidators(this.form, "shortDescription", [])
      this.setValidators(this.form, "requestedActivityDate", [])
    }

    if (!this.showNetwork) {
      this.form.get("network.networkActivityType").setValue(null)
      this.clearValidators(this.form.get("network") as FormGroup)
    }

    if (!this.showMDS) {
      this.form.get("mds.mdsActivityType").setValue(null)
      this.clearValidators(this.form.get("mds") as FormGroup)
    }

    if (this.dgNetworkTransport) {
      if (this.showVas) {
        this.dgNetworkTransport.instance.columnOption('vasType', 'visible', true);
      }
      else {
        this.dgNetworkTransport.instance.columnOption('vasType', 'visible', false);
      }
    }

    if (this.workflowStatus.value == 7) {
      this.setValidators(this.form, "activityCompletion", [Validators.required])
    } else {
      this.setValidators(this.form, "activityCompletion", [])
    }

    let activityType: number[] = this.activityType.value || []
    this.activityTypeCompletionList = this.activityTypeList.filter(a => activityType.includes(a.ntwkActyTypeId))

    if (this.showNetwork && this.showMDS) {
      //this.form.get("schedule.displayedAssignedTo").setValue('');
      this.clearAssignee();
    }

    if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
      this.setValidators(this.form, "schedule.withConferenceBridge", [Validators.required])
    } else {
      this.setValidators(this.form, "schedule.withConferenceBridge", [])
    }

    console.log(e)
    if ((e.previousValue || []).length > 0) {
      this.contactList = this.contactList.filter(a => a.creatByUserId > 1) || []
      this.contactListErrors = []
    }
  }

  clearMds() {
    this.form.get("mds.mdsActivityType").setValue(null)
    this.clearValidators(this.form.get("mds") as FormGroup)
    //this.clearValidators(this.form.get("disconnect") as FormGroup)
  }

  clearNetwork() {
    this.form.get("network.networkActivityType").setValue(null)
    this.clearValidators(this.form.get("network") as FormGroup)
  }
  // MAIN INFO

  // NETWORK INFORMATION
  // NETWORK DATA
  onH6LookupResult(result: MDSEventH6Lookup) {
    this.h1CustomerName = ""
    this.h6CustomerName = ""
    if (result != null && result != undefined) {
      console.log(result)
      if (result.groupName == "SPRINT") {
        this.form.get("network.h1").setValue(result.h1)
        this.h6CustomerName = result.customerName

        console.log(this.activityType.value)
        if (this.showNetworkOnly) {
          this.h1CustomerName = result.h1CustNme
          this.form.get("network.customerName").setValue(this.h1CustomerName)
          // Added by Sarah Sandoval [20220218]
          // To put H6 Customer Name on MDS Customer Name as a field holder only for Network type only
          this.form.get("mds.odieCustomerName").setValue(this.h6CustomerName)
        } else {
          this.form.get("network.customerName").setValue(this.h6CustomerName)
        }

        if (result.networkCustomer != null) {
          for (let value of result.networkCustomer) {
            if (!this.compareNetworkCustomer(value, this.networkCustomerList))
              this.networkCustomerList.push(value)
          }
        } else {
          this.networkCustomerList = []
        }
        if (result.networkTransport != null) {
          for (let value of result.networkTransport) {

            value.vasType = value.vasTypeDes;

            if (!this.customerNetworkTransport(value, this.networkTransportList))
              this.networkTransportList.push(value)
          }
        } else {
          this.networkTransportList = []
        }
        this.validateNetworkTables();
        //this.networkCustomerList = result.networkCustomer as MDSEventNtwkCust[]
        //this.networkTransportList = result.networkTransport || []
        this.relatedEventsList = (result.relatedCEEvents || []).filter(a => a.rltdCEEventID != this.id) || []
        if (this.prevNTVTableCnt != this.networkTransportList.length) {
          this.clearAssignee();
        }
      }
      //else if(result.networkTransport.length > 0){
      //  this.helper.notify('This Network H6 activity does not belong to Sprint per BPM and cannot be scheduled via IPSD Event.', Global.NOTIFY_TYPE_WARNING);
      //  this.networkCustomerList = [];
      //  this.networkTransportList = [];
      //  this.relatedEventsList = [];
      //}
      else {
        // Update by Sarah Sandoval [20210317] - Change error message for Carrier Ethernet Change
        if (this.isCEChng) {
          this.helper.notify('There is no data found for this CE Service ID in BPM view.', Global.NOTIFY_TYPE_WARNING);
        } else {
          this.helper.notify('There is no data found for this Network H6 in BPM view.', Global.NOTIFY_TYPE_WARNING);
        }
        
        this.networkCustomerList = [];
        this.networkTransportList = [];
        this.relatedEventsList = [];
      }

      if (!this.showMDS) {
        this.cntctList.refreshContact()
      }
    }
  }

  compareNetworkCustomer(newValue: MDSEventNtwkCust, oldValue: MDSEventNtwkCust[]): boolean {
    return oldValue.filter(a =>
      a.assocH6 == newValue.assocH6 &&
      a.instlSitePocNme == newValue.instlSitePocNme &&
      a.roleNme == newValue.roleNme &&
      a.instlSitePocEmail == newValue.instlSitePocEmail &&
      a.instlSitePocPhn == newValue.instlSitePocPhn)
      .length > 0 ? true : false
  }

  customerNetworkTransport(newValue: MDSEventNtwkTrpt, oldValue: MDSEventNtwkTrpt[]): boolean {
    return oldValue.filter(a =>
      a.assocH6 == newValue.assocH6 &&
      a.ddAprvlNbr == newValue.ddAprvlNbr &&
      //a.salsEngrPhn == newValue.salsEngrPhn &&
      //a.salsEngrEmail == newValue.salsEngrEmail &&
      //a.bdwdNme == newValue.bdwdNme &&
      //a.locCity == newValue.locCity &&
      //a.locSttPrvn == newValue.locSttPrvn &&
      //a.locCtry == newValue.locCtry &&
      //a.mdsTrnsprtType == newValue.mdsTrnsprtType &&
      //a.ceSrvcId == newValue.ceSrvcId &&
      //a.lecPrvdrNme == newValue.lecPrvdrNme &&
      //a.lecCntctInfo == newValue.lecCntctInfo &&
      a.mach5OrdrNbr == newValue.mach5OrdrNbr &&
      //a.ipNuaAdr == newValue.ipNuaAdr &&
      a.vasType == newValue.vasTypeDes
      //a.plNbr == newValue.plNbr &&
      //a.scaNbr == newValue.scaNbr &&
      //a.vlanNbr == newValue.vlanNbr &&
      //a.ipVer == newValue.ipVer
    ).length > 0 ? true : false
  }

  
  onNetworkActivityTypeChanged(e) {
    if (e.value.filter(a => [20].includes(a)).length > 0 && e.value.filter(a => [21].includes(a)).length > 0) {
      //e.value = e.value.filter(e => e !== 21)
      this.form.get("network.networkActivityType").setValue(e.value.filter(e => e !== 21))
    } else {
      // Reset Network Info if CE -> CE Change or CE Change -> CE
      if ((this.recentNetworkActivityType.includes(20) && (e.value || []).includes(21))
        || (this.recentNetworkActivityType.includes(21) && (e.value || []).includes(20))) {
        // NETWORK
        this.form.get("network.h6").reset()
        this.form.get("network.h1").reset()
        this.form.get("network.customerName").reset()
        this.form.get("network.isRelatedCompassNCR").setValue(0);
        this.form.get("network.compassNCR").reset()
        this.networkCustomerList = []
        this.networkTransportList = []
      }

      this.setPhysicalAddressValidations()
      this.setCpeDeliveryOptionValidation()
      this.showNIDData();
      let prevValue = (e.previousValue != null) ? e.previousValue.filter(a => [20, 21].includes(a)).length : 0;
      let currentValue = (e.value != null) ? e.value.filter(a => [20, 21].includes(a)).length : 0;
      if ((prevValue != 0 && currentValue == 0) || (prevValue == 0 && currentValue != 0)) {
        this.form.get("schedule.displayedAssignedTo").setValue('');
      }

      if (e.value.filter(a => [20].includes(a)).length > 0) {
        this.networkActivityTypeList.map(x => {
          x['disabled'] = false;
          if (x.value == 21) {
            x['disabled'] = true;
          }
          return x;
        })
      } else if (e.value.filter(a => [21].includes(a)).length > 0) {
        this.networkActivityTypeList.map(x => {
          x['disabled'] = false;
          if (x.value == 20) {
            x['disabled'] = true;
          }
          return x;
        })
      } else {
        this.networkActivityTypeList.map(x => {
          delete x['disabled'];
          return x;
        })
      }

      if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
        this.setValidators(this.form, "schedule.withConferenceBridge", [Validators.required])
      } else {
        this.setValidators(this.form, "schedule.withConferenceBridge", [])
      }

      this.recentNetworkActivityType = (e.value || []).length > 0 ? e.value : this.recentNetworkActivityType
      console.log(this.recentNetworkActivityType)
    }
  }

  onNetworkActivityTypeSelectAll(e) {
    console.log(e)
    console.log(e.component)
  }

  showNIDData() {
    if (this.dgNetworkTransport) {
      if (this.showVas) {
        this.dgNetworkTransport.instance.columnOption('vasType', 'visible', true);
      }
      else {
        this.dgNetworkTransport.instance.columnOption('vasType', 'visible', false);
      }

      if (this.isCEChng) {
        this.h6Label = "H6/CE ServiceID";
        this.dgNetworkTransport.instance.columnOption('ceServiceID', 'visible', true);
        this.dgNetworkTransport.instance.columnOption('nidSerialNbr', 'visible', true);
        this.dgNetworkTransport.instance.columnOption('nidIpAdr', 'visible', true);
        this.dgNetworkTransport.instance.columnOption('nidIpAdr', 'allowEditing', false);
        this.dgNetworkTransport.instance.columnOption('nidHostName', 'visible', true);
        this.setValidators(this.form, "network.h6", [Validators.required])
      }
      else if (!this.isCEChng && this.showNID) {
        this.h6Label = "H6";
        this.dgNetworkTransport.instance.columnOption('ceServiceID', 'visible', true);
        this.dgNetworkTransport.instance.columnOption('nidSerialNbr', 'visible', true);
        this.dgNetworkTransport.instance.columnOption('nidSerialNbr', 'allowEditing', false);
        this.dgNetworkTransport.instance.columnOption('nidIpAdr', 'visible', true);
        this.dgNetworkTransport.instance.columnOption('nidIpAdr', 'allowEditing', false);
        this.dgNetworkTransport.instance.columnOption('nidHostName', 'visible', true);
        this.dgNetworkTransport.instance.columnOption('nidHostName', 'allowEditing', false);
        this.setValidators(this.form, "network.h6", [Validators.required, Validators.pattern(/^\d{9}$/)])
      }
      else {
        this.h6Label = "H6";
        this.dgNetworkTransport.instance.columnOption('ceServiceID', 'visible', false);
        this.dgNetworkTransport.instance.columnOption('nidSerialNbr', 'visible', false);
        this.dgNetworkTransport.instance.columnOption('nidIpAdr', 'visible', false);
        this.dgNetworkTransport.instance.columnOption('nidHostName', 'visible', false);
        this.setValidators(this.form, "network.h6", [Validators.required, Validators.pattern(/^\d{9}$/)])
      }
    }
  }

  onTransportVasEditorPreparing(e) {
    if (e.parentType == "dataRow" && e.dataField == "nidSerialNbr" && this.isCEChng) {
      //console.log(e)
      e.editorOptions.disabled = e.row.cells[9].data.isNidSerialNbrNative
      //console.log(e.row.cells[9].data.isNidSerialNbrNative)
    } else if (e.parentType == "dataRow" && e.dataField == "nidHostName" && this.isCEChng) {
      //console.log(e)
      e.editorOptions.disabled = e.row.cells[11].data.isNidHostNameNative
      //console.log(e.row.cells[11].data.isNidHostNameNative)
    }
  }

  onNetworkCustomerInserting(e) {
    e.eventId = this.id > 0 ? this.id : 0
    e.mdsEventNtwkCustId = isNaN(+e.mdsEventNtwkCustId) ? 0 : e.mdsEventNtwkCustId
    e.assocH6 = e.data.assocH6 || ""
    e.instlSitePocNme = e.data.instlSitePocNme || ""
    e.instlSitePocEmail = e.data.instlSitePocEmail || ""
    e.instlSitePocPhn = e.data.instlSitePocPhn || ""
    e.roleNme = e.data.roleNme || ""
    e.creatDt = e.data.creatDt || new Date()
  }

  onRelatedEventClicked(id) {
    this.router.navigate(['event/mds/' + id]);
  }
  // NETWORK DATA

  // TRANSPORT/VAS
  onViewDesignDetailClicked(e, a) {
    e.preventDefault();

    this.isContentReady = true;
    // this.designDocDetailList = []
    this.spinner.show()
    this.mdsEventService.getDesignDoc(a.assocH6, a.ddAprvlNbr, this.getVASCECd(), false).subscribe(res => {
      console.log(res)
      let id = 0;
      this.designDocDetailList = res.table3.map(x => {
        x['id'] = id;
        id++;

        return x;
      });
      $("#designDocDetail").modal("show");
      this.spinner.hide()
    }, error => {
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })

    //return false;
  }
  // TRANSPORT/VAS
  // NETWORK INFORMATION

  // MDS
  onH1LookupResult(result: OdieRspn[]) {
    this.odieRspnList = result
    if (result.length == 1) {
      this.form.get("mds.odieCustomerName").setValue(result[0].custNme)
    }

    //this.cntctList.refreshContact()
  }

  onMdsActivityType(e) {

    this.mdsActivityTypeSub.next(e);

    this.setValidators(this.form, "customerSOW", [])
    this.setValidators(this.form, "mds.customerSOW", [])
    this.setValidators(this.form, "shortDescription", [])
    this.setValidators(this.form, "mds.mdsMacActivity", [])
    this.setValidators(this.form, "shipping.shipmentCustomerEmail", [])
    this.setValidators(this.form, "requestedActivityDate", [])

    //console.log(0)
    if (this.showIPMDescription) {
      this.setValidators(this.form, "ipmDescription", [Validators.maxLength(500)])
    }
    if (this.showDisconnect) {
      //console.log(1)
      this.setValidators(this.form, "disconnect.isRequireCoordination", [Validators.required])
      this.setValidators(this.form, "disconnect.isTotalDisconnect", [Validators.required])
      this.setValidators(this.form, "customerSOW", [Validators.required])
      this.setValidators(this.form, "mds.customerSOW", [Validators.required])
    } else if (this.showMDS) {
      //console.log(2)
      this.setValidators(this.form, "customerSOW", [Validators.required])
      this.setValidators(this.form, "mds.customerSOW", [Validators.required])
      this.setValidators(this.form, "shortDescription", [Validators.required, Validators.maxLength(2500)])
      this.setValidators(this.form, "mds.mdsMacActivity", [Validators.required, this.isMixed(this.mdsMacActivityList)])

      if (this.showPreStaging) {
        this.setValidators(this.form, "shipping.shipmentCustomerEmail", [Validators.required])
      }

      this.form.get("disconnect.isRequireCoordination").setValue(null)
      this.form.get("disconnect.isTotalDisconnect").setValue(null)
      this.clearValidators(this.form.get("disconnect") as FormGroup)

      this.dropAssignee();
    }

    if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
      this.setValidators(this.form, "schedule.withConferenceBridge", [Validators.required])
    } else {
      this.setValidators(this.form, "schedule.withConferenceBridge", [])
    }

    this.setRedesignValidation()
    this.setPhysicalAddressValidations()
    this.setCpeDeliveryOptionValidation()
    this.clearAssignee()
  }

  onOdieCustomerNameChanged(e) {
    let selected = this.odieRspnList.find(a => a.custNme == e.value) as OdieRspn

    //if (this.mdsActivityType.value == 2 ||this.mdsActivityType.value == 3 ||this.mdsActivityType.value == 5 ) {
    this.customerSOW.setValue(selected.sowsFoldrPathNme)
    this.mdsCustomerSOW.setValue(selected.sowsFoldrPathNme);
    //} else {
    //  this.customerSOW.reset()
    //  this.mdsCustomerSOW.reset()
    //}
    this.teamPDL.setValue(selected.custTeamPdl)
    this.form.get("mds.mnspmId").setValue(selected.mnspmId)
    this.form.get("mds.odieCustId").setValue(selected.odieCustId)
    this.getRedesign()
    this.dropAssignee();

    console.log(1)
    this.cntctList.refreshContact()
  }

  // DISCONNECT
  getOdieData(e) {
    //this.deviceModelList = res[7] as DeviceModel[]
    //this.devModelSrvc.getForLookup()
    this.errors = []
    this.eventDiscoDevGrid.instance.saveEditData();
    if (this.eventDiscoDevList.length == 0 || this.eventDiscoDevList.filter(a =>
      this.helper.isEmpty(a.odieDevNme))
      .length > 0) {
      console.log(1)
      //Select atleast one row for retreiving Disconnect Device data from ODIE
      this.errors.push("Please populate ODIE Device Name for retrieving Disconnect Device data from ODIE");
    } else {
      this.spinner.show();
      console.log(0)
      let data = this.searchService.getOdieDiscoDevData(this.mdsH1.value, this.eventDiscoDevList).pipe(
        concatMap(
          res => zip(
            of(res as EventDiscoDev[]),
            this.devModelSrvc.getForLookup()
          )
        )
      )

      data.subscribe(
        res => {
          this.eventDiscoDevList = res[0];
          this.deviceModelList = res[1] as DeviceModel[]
          this.eventDiscoDevList = this.eventDiscoDevList.map(a => {
            a.readyBegin = a.readyBeginCd == "Y" ? true : false
            a.optInH = a.optInHCd == "Y" ? true : false
            a.optInCkt = a.optInCktCd == "Y" ? true : false
            a.devModelId = a.devModelId == 0 ? null : a.devModelId
            return a
          })
        },
        error => {
          this.spinner.hide();
          this.helper.notify(`An error occurred while retrieving ODIE Disconnect Devices Data.
            ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
        },
        () => {
          this.spinner.hide();
        }
      )


      //this.searchService.getOdieDiscoDevData(this.mdsH1.value, this.eventDiscoDevList).subscribe(
      //  res => {
      //    this.eventDiscoDevList = res;
      //    this.eventDiscoDevList = this.eventDiscoDevList.map(a => {
      //      a.readyBegin = a.readyBeginCd == "Y" ? true : false
      //      a.optInH = a.optInHCd == "Y" ? true : false
      //      a.optInCkt = a.optInCktCd == "Y" ? true : false
      //      a.devModelId = a.devModelId == 0 ? null : a.devModelId
      //      return a
      //    })
      //  },
      //  error => {
      //    this.spinner.hide();
      //    this.helper.notify(`An error occurred while retrieving ODIE Disconnect Devices Data.
      //      ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
      //  },
      //  () => {
      //    this.spinner.hide();
      //  });
    }
  }

  onEventDiscoDevNewRow(e) {
    e.data.readyBegin = false
    e.data.optInH = false
    e.data.optInCkt = false

    this.dropAssignee();
  }

  onIsRequireCoordinationChanged(e) {
    if (e.value == 0) {
      this.setValidators(this.form, "requestedActivityDate", [Validators.required])
      if (this.id > 0) {
        this.form.get("requestedActivityDate").setValue(this.mds.strtTmst)
      }
    } else {
      this.setValidators(this.form, "requestedActivityDate", [])
      if (this.id > 0) {
        this.form.get("schedule.withConferenceBridge").setValue(this.mds.cnfrcBrdgId)
      }
    }

    if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
      this.setValidators(this.form, "schedule.withConferenceBridge", [Validators.required])
    } else {
      this.setValidators(this.form, "schedule.withConferenceBridge", [])
    }
  }

  onTotalDisconnectChanged(e) {
    this.setValidators(this.form, "disconnect.reasonForTotalDisconnect", [])
    this.setValidators(this.form, "disconnect.shortDescription", [])

    if (e.value == 1) {
      this.setValidators(this.form, "disconnect.reasonForTotalDisconnect", [Validators.required])
    } else if (e.value == 0) {
      this.setValidators(this.form, "disconnect.shortDescription", [Validators.required])
    }

    if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
      this.setValidators(this.form, "schedule.withConferenceBridge", [Validators.required])
    } else {
      this.setValidators(this.form, "schedule.withConferenceBridge", [])
    }
  }
  // DISCONNECT

  // NON DISCONNECT
  // REDESIGN DEVICE
  onRedesignRowUpdating(e: any) {
    //console.log(`onRedesignRowUpdating`);
    //console.log("e", JSON.stringify(e));
    //this.isWoob = (e.newData.woobCd == "Y" || this.mdsRedesignDevInfoList.filter(a => a.woobCd == "Y").length > 0) ? true : false
    if (!this.isWoob) {
      this.form.get("woobIpAddress").setValue(null)
      this.clearValidators(this.form.get("woobIpAddress") as FormGroup)
    }
    if (this.showMDS && !this.showDisconnect && this.isWoob) {
      this.form.get("woobIpAddress").value
      this.setValidators(this.form, "woobIpAddress", [Validators.required])
      //this.setRedesignValidation()
      //this.isRedesignHasChanges = true;
    }
    //  this.setRedesignValidation()
    this.isRedesignHasChanges = true;
    this.validateMdsOdie();
    //console.log(JSON.stringify(e.newData));
    //console.log(JSON.stringify(e.oldData));
    if ((!isNullOrUndefined(e.newData)) && (!isNullOrUndefined(e.newData.devModelId))) {
      if ((!isNullOrUndefined(e.oldData)) && (!isNullOrUndefined(e.newData))
        && (!isNullOrUndefined(e.oldData.devModelId)) && (!isNullOrUndefined(e.newData.devModelId))) {
        if (e.oldData.devModelId != e.newData.devModelId) {
          this.clearAssignee();
        }
      }
    }
    if ((!isNullOrUndefined(e.newData)) && (!isNullOrUndefined(e.newData.manfId))) {
      if ((!isNullOrUndefined(e.oldData)) && (!isNullOrUndefined(e.newData))
        && (!isNullOrUndefined(e.oldData.manfId)) && (!isNullOrUndefined(e.newData.manfId))) {
        if (e.oldData.manfId != e.newData.manfId) {
          this.clearAssignee();
        }
      }
    }
    if ((!isNullOrUndefined(e.newData)) && (!isNullOrUndefined(e.newData.frwlProdCd))) {
      if ((!isNullOrUndefined(e.oldData)) && (!isNullOrUndefined(e.newData))
        && (!isNullOrUndefined(e.oldData.frwlProdCd)) && (!isNullOrUndefined(e.newData.frwlProdCd))) {
        if (e.oldData.frwlProdCd != e.newData.frwlProdCd) {
          this.clearAssignee();
        }
      }
    }
    if ((!isNullOrUndefined(e.newData)) && (!isNullOrUndefined(e.newData.scCd))) {
      if ((!isNullOrUndefined(e.oldData)) && (!isNullOrUndefined(e.newData))
        && (!isNullOrUndefined(e.oldData.scCd)) && (!isNullOrUndefined(e.newData.scCd))) {
        if (e.oldData.scCd != e.newData.scCd) {
          this.clearAssignee();
        }
      }
    }
  }

  onWiredTransportRowUpdating(e) {
    if ((!isNullOrUndefined(e.newData)) && (!isNullOrUndefined(e.oldData))
      && (!isNullOrUndefined(e.newData.primBkupCd)) && (!isNullOrUndefined(e.oldData.primBkupCd))) {
      if (e.oldData.primBkupCd != e.newData.primBkupCd) {
        this.clearAssignee();
      }
      //if(e.newData.primBkupCd =="B" && e.oldData.primBkupCd == "P")
      //   {
      //     let mds = new MdsEvent();
      //     mds.eventDrtnInMinQty = parseInt(this.form.get("schedule.eventDuration").value);
      //     mds.eventDrtnInMinQty = mds.eventDrtnInMinQty +30;
      //    this.form.get("schedule.eventDuration").setValue( mds.eventDrtnInMinQty);
      //     let start  = this.form.get("schedule.startDate").value
      //     let end = this.helper.dateAdd(start, 'minute', mds.eventDrtnInMinQty)
      //     this.form.get("schedule.endDate").setValue(end);
      //    // if(typeof(start) === 'object'){
      //    //  start = this.helper.dateObjectToString(start);
      //    //  }
      //    //  if(typeof(end) === 'object'){
      //    //  end = this.helper.dateObjectToString(end);
      //    //  }
      //   }
      // if(e.newData.primBkupCd == "P" && e.oldData.primBkupCd == "B"){
      //       let mds = new MdsEvent();
      //     mds.eventDrtnInMinQty = parseInt(this.form.get("schedule.eventDuration").value);
      //     mds.eventDrtnInMinQty = mds.eventDrtnInMinQty - 30;
      //    this.form.get("schedule.eventDuration").setValue( mds.eventDrtnInMinQty);
      //     let start  = this.form.get("schedule.startDate").value
      //     let end = this.helper.dateAdd(start, 'minute', mds.eventDrtnInMinQty)
      //     this.form.get("schedule.endDate").setValue(end);
      //     }
    }
  }

  onVendorPreparing(e) {
    if (e.parentType === "dataRow" && e.dataField === "devModelId") {
      e.editorOptions.disabled = (typeof e.row.data.manfId !== "number")
    }
  }

  onVendorChange(rowData, value) {
    console.log(rowData)
    let devModel = this.deviceModelList.filter(a => a.manfId == value)
    rowData.manfId = value
    rowData.devModelId = devModel.length > 0 ? devModel[0].devModelId : null
  }

  getDeviceModelByManfId(options) {
    return {
      store: this.deviceModelList,
      filter: options.data ? ["manfId", "=", options.data.manfId] : null
    }
  }

  onRedesignChanged(e) {
    // GET ODIE DEVICE NAMES
    this.getMdsOdieDeviceNameList()

    this.clearAssignee();
    // Added by Sara Sandoval [20210121]
    // To fix PROD Issue IM6023233 raised by Diane Mcmillan (Event 256148 // Redesign R00056944)
    // The issue is when Managed activity removed device but did not remove device on device Completion table
    if (!this.helper.isEmpty(e.key)) {
      this.devCompletionList = this.devCompletionList.filter(i => i.odieDevNme != e.key.odieDevNme);
    }
  }

  compareRedesignDevInfo(newValue: RedesignDevicesInfo, oldValue: MdsEventOdieDevice[]): boolean {
    return oldValue.filter(a =>
      a.rdsnNbr == newValue.redesignNbr &&
      a.rdsnExpDt == newValue.expirationDate &&
      a.odieDevNme == newValue.devNme)
      .length > 0 ? true : false
  }

  getRedesign() {
    let h1 = this.form.get("mds.h1").value
    let customerName = this.form.get("mds.odieCustomerName").value

    if (this.helper.isEmpty(customerName) || this.helper.isEmpty(h1)) {
      this.helper.notify('H1 and/or Customer Name is empty', Global.NOTIFY_TYPE_ERROR);
      return;
    }

    this.spinner.show()
    this.searchService.retrieveRedesign(h1, customerName, this.eventType).subscribe(res => {
      this.odieRedesignDevInfoList = res as RedesignDevicesInfo[]
      if (this.odieRedesignDevInfoList.length == 0) {
        this.helper.notify('There is no approved redesign data associated for this H1, Please try with an different H1 or check Redesign Data', Global.NOTIFY_TYPE_WARNING);
      }
    }, error => {
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  addSelectedOdieRedesignDevInfo() {
    console.log(this.odieRedesignGrid.instance.getSelectedRowsData())
    let odieSelectedDev = this.odieRedesignDevInfoList.filter(i => this.odieRedesignGrid.instance.getSelectedRowKeys().includes(i.redsgnDevId));

    if (odieSelectedDev != null && odieSelectedDev.length == 0) { // If Empty
      this.mdsRedesignDevInfoList = odieSelectedDev.map(data => {
        return new MdsEventOdieDevice({
          eventId: this.id,
          rdsnNbr: data.redesignNbr,
          rdsnExpDt: data.expirationDate,
          odieDevNme: data.devNme,
          redsgnDevId: data.redsgnDevId,
          scCd: data.scCd
        })
      });

      this.odieRedesignPanel.close()
    } else if (odieSelectedDev != null && odieSelectedDev.length > 0) { // If not Empty
      for (let value of odieSelectedDev) {
        if (!this.compareRedesignDevInfo(value, this.mdsRedesignDevInfoList))
          this.mdsRedesignDevInfoList.push(new MdsEventOdieDevice({
            eventId: this.id,
            rdsnNbr: value.redesignNbr,
            rdsnExpDt: value.expirationDate,
            odieDevNme: value.devNme,
            redsgnDevId: value.redsgnDevId,
            scCd: value.scCd
          }))
      }

      this.odieRedesignPanel.close()
    }

    // GET ODIE DEVICE NAMES
    this.getMdsOdieDeviceNameList()
    this.dropAssignee();
  }

  saveRedesignData(e) {
    if (this.mdsRedesignDevInfoList != null && this.mdsRedesignDevInfoList.length > 0) {

      //console.log(this.mdsRedesignDevInfoList)
      //this.devCompletionList = this.mdsRedesignDevInfoList.map(data => {
      //  let redesign = this.odieRedesignDevInfoList.find(a =>
      //    a.redesignNbr == data.rdsnNbr &&
      //    a.expirationDate == data.rdsnExpDt &&
      //    a.devNme == data.odieDevNme)
      //  console.log(redesign)

      //  let temp = redesign != undefined ? redesign.h6CustId : null
      //  let h6 = (temp == null || temp == "") ? this.form.get("site.h6").value : temp
      //  //return new EventDeviceCompletion(0, this.id, data.odieDevNme, h6, false, new Date(), data.redsgnDevId, 0, null);
      //  return new EventDeviceCompletion({
      //    eventId: this.id,
      //    odieDevNme: data.odieDevNme,
      //    h6: h6,
      //    cmpltdCd: false,
      //    redsgnDevId: data.redsgnDevId
      //  });
      //});
      this.validateMdsOdie();

      console.log(this.mdsRedesignDevInfoList)
      console.log(this.odieRedesignDevInfoList)

      this.devCompletionList = this.mdsRedesignDevInfoList.map(a => {
        ////let cpeDevice = this.cpeDeviceList.find(b =>
        //  b.devNme == a.odieDevNme)
        let redesign = this.odieRedesignDevInfoList.filter(b =>
          b.redesignNbr == a.rdsnNbr &&
          b.expirationDate == a.rdsnExpDt &&
          b.devNme == a.odieDevNme) as RedesignDevicesInfo[]

        let redesignH6 = (redesign.length == 0) ? null : redesign[0].h6CustId
        //let selectedH6 = (redesignH6 == null || redesignH6 == "") ? this.form.get("site.h6").value : redesignH6
        // Added by Sarah Sandoval [20210216] - Per old COWS requirement H6 should always have a value on Device Completion table
        // If H6 on site lookup has value, Device Completion table will take that H6 otherwise the Redesign H6
        // Commented selectedH6 code and update here below
        let selectedH6 = this.helper.isEmpty(this.form.get("site.h6").value) ? redesignH6 : this.form.get("site.h6").value

        return new EventDeviceCompletion({
          eventId: this.id,
          odieDevNme: a.odieDevNme,
          h6: selectedH6,
          cmpltdCd: false,
          redsgnDevId: a.redsgnDevId || this.devCompletionList.find(b => b.odieDevNme == a.odieDevNme).redsgnDevId
        });
      })
    }
    else {
      // Just for create for the meantime; will need to change implementation for updating event
      this.devCompletionList = [];
    }
  }

  setRedesignValidation() {
    //console.log(this.isWoob)
    //console.log(this.showMDS && !this.showDisconnect && this.isWoob)
    if (this.showMDS && !this.showDisconnect && this.isWoob && (this.form.get('woobIpAddress').value) == null) {
      this.setValidators(this.form, "woobIpAddress", [Validators.required])
    } else {
      this.setValidators(this.form, "woobIpAddress", [])
    }

    if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
      this.setValidators(this.form, "schedule.withConferenceBridge", [Validators.required])
    } else {
      this.setValidators(this.form, "schedule.withConferenceBridge", [])
    }
  }
  // REDESIGN DEVICE

  // CPE DEVICE
  onSiteLookup(result: MDSEventSiteLookup) {
    this.h6Lookup = result
    console.log(['onSiteLookup result', this.h6Lookup])

    //// Added by Sarah Sandoval [20220120] - To reload Contact List on Site Lookup for Generic H1
    //let odieCustId = this.helper.isEmpty(this.mds) ? this.odieCustId.value : this.mds.odieCustId;
    ////console.log(['empty contactList', this.helper.isEmpty(this.contactList)]
    ////  , ['contactList length', this.contactList.length]
    ////  , ['contactList', this.contactList]
    ////  , ['odieCustId', odieCustId]
    ////  , ['mds', this.mdsH1.value]
    ////  , ['condition', (this.mdsH1.value === '999999999'
    ////    && (this.helper.isEmpty(this.contactList) || this.contactList.length <= 0))])
    //if (this.mdsH1.value === '999999999' && this.contactList.length <= 0) {
    //  this.cntctDtlSrvc.getContactDetails(this.id, this.objType, this.mdsH1.value, 'H1', odieCustId || '', this.form.get("site.h6").value || '')
    //    .toPromise().then(res => {
    //      this.contactList = (res || []) as ContactDetail[];
    //      console.log(['contact list result', this.contactList])
    //      // Construct Email Code Text on Update
    //      this.contactList.forEach(item => {
    //        if (!this.helper.isEmpty(item.emailCd)) {
    //          const statusText: string[] = [];
    //          item.emailCdIds = item.emailCd.split(',').map(Number);
    //          item.emailCdIds.forEach(row => {
    //            statusText.push(EVENT_STATUS_EMAIL_CD[row])
    //          });
    //          item.emailCdTxt = statusText.join(',');
    //        }

    //        if (item.id != 0 && (item.creatByUserId > 1 && item.creatByUserId != this.userService.loggedInUser.userId)) {
    //          item.allowEdit = false;
    //        } else {
    //          item.allowEdit = true;
    //        }
    //      });
    //    });
    //}

    this.cntctList.refreshContact()

    if (this.isSprintCPE && result.cpeDispatchEmail.length > 0) {
      this.form.get("cpe.cpeDispatchEmail").setValue(result.cpeDispatchEmail)
    }
    //if ((this.showNID) && this.mdsRedesignDevInfoList.length === 0) {
    //  this.mdsOdieDeviceNameList = [{ deviceName: "Do Not Send to ODIE", value: "0" }]
    //}
    this.cpeMnsNotFoundMessage = null
    this.cpeMnsFoundMessage = null

    if (result.cpeDevices.length > 0) {
      result.cpeDevices = result.cpeDevices.filter((value, index, self) => self.findIndex(b => b.deviceId == value.deviceId && b.cpeOrdrId == value.cpeOrdrId && b.assocH6 == value.assocH6) === index)
    }
    //  this.cpeDeviceList = result.cpeDevices
    //  this.mnsOrderList = result.mnsOrders

    if (result.cpeDevices.length > 0 || result.mnsOrders.length > 0) {
      this.isCpe = result.cpeDevices.length > 0 ? true : false
      this.isMns = result.mnsOrders.length > 0 ? true : false

      // this.cpeDeviceList = result.cpeDevices
      // this.mnsOrderList = result.mnsOrders
      this.loadCpeData(result)
      // Commented by Sarah Sandoval [20210322] - Removed PageLoad condition for this defect IM6113628
      // The Device MNS Service table didn't reload after doing site lookup on Network event that was updated to MDS+Network
      //if(!this.isPageLoad) {
        this.loadDevMgmt(result)
      //}
     
      if ((!this.isPageLoad && this.id == 0) && (this.showMDS && !this.showDisconnect)) {
        const ccd = this.form.get("site.ccd").value;
        this.form.get("schedule.startDate").setValue(this.helper.dateAdd(this.helper.dateAdd(ccd, "hour", 8), "minute", 0));
        this.form.get("schedule.endDate").setValue(this.helper.dateAdd(this.helper.dateAdd(ccd, "hour", 9), "minute", 0));
        this.form.get("schedule.eventDuration").setValue("60");
        this.clearAssignee()
      }
    } else {
      this.isCpe = result.cpeDevices.length > 0 ? true : false
      this.isMns = result.mnsOrders.length > 0 ? true : false
      // this.cpeDeviceList = result.cpeDevices
      // this.mnsOrderList = result.mnsOrders
      this.loadCpeData(result)
      this.loadDevMgmt(result)
    }

    this.getMdsOdieDeviceNameList()
    console.log(this.wiredTransportList)
    this.wiredTransportList = this.wiredTransportList.map(a => { a.isNew = false; return a; })
    console.log(this.wiredTransportList)
    if (!this.isPageLoad && result.wiredDeviceTransports.length > 0) {
      let wiredDeviceTransports = JSON.parse(JSON.stringify(result.wiredDeviceTransports))
      this.wiredTransportList = this.wiredTransportList.filter(a => a.h6 == this.form.get("site.h6").value) as MDSEventSlnkWiredTrpt[]
      console.log(this.wiredTransportList)
      console.log(result.wiredDeviceTransports)
      this.wiredTransportList = [...this.wiredTransportList, ...wiredDeviceTransports]
      //this.wiredTransportList.concat(result.wiredDeviceTransports)
      console.log(this.wiredTransportList)
      this.wiredTransportList = this.wiredTransportList
        .reduce(
          (items, item) =>
            items.find(x => x.mdsTrnsprtType === item.mdsTrnsprtType &&
              x.h6 === item.h6 &&
              x.bdwdChnlNme === item.bdwdChnlNme &&
              x.fmsNbr === item.fmsNbr &&
              x.ipNuaAdr === item.ipNuaAdr &&
              x.vndrPrvdrTrptCktId === item.vndrPrvdrTrptCktId) ? [...items] : [...items, item], []
      )

      this.wiredTransportList = this.wiredTransportList.map(
        a => {
          a.slnkWiredTrptId = a.slnkWiredTrptId == 0 ? this.uuidv4() : a.slnkWiredTrptId
          //a.slnkWiredTrptIdstring = this.uuidv4()
          a.readyBeginCdBoolean = a.readyBeginCd == "Y" ? true : false
          a.optInCktCdBoolean = a.optInCktCd == "Y" ? true : false
          a.optInHCdBoolean = a.optInHCd == "Y" ? true : false
          a.isFromLookup = true
          a.isNew = false

          if (this.mdsRedesignDevInfoList != null) {
            a.odieDevNme = this.mdsRedesignDevInfoList.length == 1 ? this.mdsRedesignDevInfoList[0].odieDevNme : null
          }

          return a
        }
      )
    }

    console.log(this.wiredTransportList)

    if (!this.isPageLoad && result.portBandwidth.length > 0) {
      let portBandwidth = JSON.parse(JSON.stringify(result.portBandwidth))
      this.portBandwidthList = [...this.portBandwidthList, ...portBandwidth]
      this.portBandwidthList = this.portBandwidthList
        .reduce(
          (items, item) =>
            items.find(x => x.m5OrdrCmpntId === item.m5OrdrCmpntId &&
              x.portBndwd === item.portBndwd &&
              x.nua === item.nua &&
              x.m5OrdrNbr === item.m5OrdrNbr) ? [...items] : [...items, item], []
        )

      // Added by Sarah Sandoval [20210322] - Part of Sabrina's issue - IM6113628
      // Odie Dev Name auto select dev name for single record
      this.portBandwidthList
        .forEach(i => {
          if (this.mdsRedesignDevInfoList != null) {
            i.odieDevNme = this.mdsRedesignDevInfoList.length == 1 ? this.mdsRedesignDevInfoList[0].odieDevNme : null
          }
          i.portBndwdId = i.portBndwdId == 0 ? this.uuidv4() : i.portBndwdId
          //i.portBndwdIdstring = this.uuidv4()
        })
    }

    this.relatedMnsOrderList = result.relatedMnsOrders

    if (result.cpeDevices.length == 0 && result.mnsOrders.length == 0) {
      this.cpeMnsNotFoundMessage = "Sorry, no CPE and MNS data found for that H6 and CCD combination"
      this.helper.notify(this.cpeMnsNotFoundMessage, Global.NOTIFY_TYPE_WARNING);
      this.form.get("site.siteIdAddress").setValue('');
    } else if (result.cpeDevices.length > 0 && result.cpeDevices.filter(a => a.ordrId == null || a.ordrId == -1).length > 0) {
      let deviceIds = result.cpeDevices.filter(a => a.ordrId == null || a.ordrId == -1).map(a => a.deviceId).join(", ")
      this.cpeMnsNotFoundMessage = `There is no associated CPE order in COWS for these devices (${deviceIds}), Please validate before moving forward`
      this.helper.notify(this.cpeMnsNotFoundMessage, Global.NOTIFY_TYPE_WARNING);
    }
    // else if (result.cpeDevices.length > 0 && result.mnsOrders.length == 0) {
    //  this.cpeMnsFoundMessage = "Only CPE Order data found for the H6 and CCD combination !!"
    //  this.helper.notify(this.cpeMnsFoundMessage, Global.NOTIFY_TYPE_WARNING);
    //} else if (result.cpeDevices.length == 0 && result.mnsOrders.length > 0) {
    //  this.cpeMnsFoundMessage = "Only MNS Order data found for the H6 and CCD combination !!"
    //  this.helper.notify(this.cpeMnsFoundMessage, Global.NOTIFY_TYPE_WARNING);
    //} else if (result.cpeDevices.length > 0 && result.mnsOrders.length > 0) {
    //  this.cpeMnsFoundMessage = "CPE and MNS Order data found for the H6 and CCD combination !!"
    //  this.helper.notify(this.cpeMnsFoundMessage, Global.NOTIFY_TYPE_WARNING);
    //}

    //if (this.form.get("cpe.cpeDeliveryOption").value )

    // Added by Sarah Sandoval [20210216] - Per old COWS requirement H6 should always have a value on Device Completion table
    // If H6 on site lookup has value, Device Completion table will take that H6 otherwise the Redesign H6
    //if (!this.helper.isEmpty(this.form.get("site.h6").value)) {
    //  this.devCompletionList.forEach(i => { i.h6 = this.form.get("site.h6").value })
    //}
    this.devCompletionList = this.devCompletionList.map(
      a => {
        // Device Completion ODIE H6 comes from Redesign Device H6, CPE Table Assoc H6, if there's still blank take Site Lookup H6
        var assocH6 = this.cpeDeviceList.find(i => i.odieDevNme == a.odieDevNme) == null ? "" : this.cpeDeviceList.find(i => i.odieDevNme == a.odieDevNme).assocH6;
        if (!this.helper.isEmpty(assocH6)) {
          a.h6 = assocH6
        }
        else if (!this.helper.isEmpty(this.form.get("site.h6").value)) {
          a.h6 = this.form.get("site.h6").value
        }
        return a
      }
    )

    this.isPageLoad = false
  }

  onWiredTransportEditorPreparing(e) {
    let isNew = this.helper.ifEmpty(e.row.data.isNew, false)
    let allowEditing = true
    if (this.helper.ifEmpty(e.row.data.isNew, false)) {
      if (!this.isReviewer) {
        allowEditing = false
      }
    }

    if (e.parentType == "dataRow" && e.dataField == "mdsTrnsprtType") {
      e.editorOptions.disabled = !isNew
    }

    if (e.parentType == "dataRow" && e.dataField == "primBkupCd") {
      e.editorOptions.disabled = (this.isActivator && !isNew)
    }

    if (e.parentType == "dataRow" && e.dataField == "vndrPrvdrTrptCktId") {
      e.editorOptions.disabled = !isNew
    }

    if (e.parentType == "dataRow" && e.dataField == "vndrPrvdrNme") {
      e.editorOptions.disabled = !isNew
    }

    if (e.parentType == "dataRow" && e.dataField == "bdwdChnlNme") {
      e.editorOptions.disabled = !isNew
    }

    if (e.parentType == "dataRow" && e.dataField == "ipNuaAdr") {
      e.editorOptions.disabled = !isNew
    }

    if (e.parentType == "dataRow" && e.dataField == "oldCktId") {
      e.editorOptions.disabled = (!this.isReviewer && !isNew)
    }

    //if (e.parentType == "dataRow" && e.dataField == "vlanNbr") {
    //  e.editorOptions.disabled = !isNew
    //}

    if (e.parentType == "dataRow" && e.dataField == "sprintMngdCd") {
      e.editorOptions.disabled = (!this.isReviewer && !isNew) || (this.isReviewer && !e.row.data.isFromLookup && !isNew)
    }

    if (e.parentType == "dataRow" && e.dataField == "readyBeginCdBoolean") {
      e.editorOptions.disabled = true
    }

    if (e.parentType == "dataRow" && e.dataField == "optInHCdBoolean") {
      e.editorOptions.disabled = true
    }

    if (e.parentType == "dataRow" && e.dataField == "optInCktCdBoolean") {
      e.editorOptions.disabled = true
    }

    if (e.parentType == "dataRow" && e.dataField == "odieDevNme") {
      e.editorOptions.disabled = (!this.isReviewer && !isNew)
    }
  }

  loadCpeData(res: MDSEventSiteLookup) {
    if (res.cpeDevices.length > 0) {
      if (this.cpeDeviceList.length > 0) {
        this.cpeDeviceList = res.cpeDevices.map(
          a => {

            let common = this.cpeDeviceList.find(b => b.deviceId == a.deviceId && b.cpeOrdrId == a.cpeOrdrId && b.assocH6 == a.assocH6)
            a.eventCpeDevId = (common || { eventCpeDevId: a.eventCpeDevId }).eventCpeDevId
            a.odieDevNme = (common || { odieDevNme: "" }).odieDevNme
            if (this.showNetwork) {
              a.odieDevNme = a.odieDevNme == "" ? "0" : a.odieDevNme;
            }
            return a
          }
        )
      } else {
        this.cpeDeviceList = !this.showNetwork ? res.cpeDevices : res.cpeDevices.map(data => {
          data.odieDevNme = data.odieDevNme == "" ? "0" : data.odieDevNme;
          return data;
        })
      }

      if (!this.isPageLoad && this.mdsRedesignDevInfoList.length === 1) {
        this.cpeDeviceList.map(a => {
          a.odieDevNme = this.mdsRedesignDevInfoList[0].odieDevNme
        })
      }

      this.devCompletionList = this.devCompletionList.map(
        a => {
          //let common = res.cpeDevices.find(b => b.odieDevNme == a.odieDevNme)
          //a.odieDevNme = (common || { odieDevNme: "" }).odieDevNme

          if (this.helper.isEmpty(a.h6)) {
            a.h6 = this.form.get("site.h6").value
          }
          return a
        }
      )
    } else {
      this.cpeDeviceList = []
    }

    if (!this.helper.isEmpty(res.siteIdTxt)) {
      //this.siteIdAddress = res.siteId + '_' + res.ctyNme + ',' + res.sttPrvnNme;
      this.form.get("site.siteIdAddress").setValue(res.siteIdTxt + '_' + res.ctyNme + ',' + res.sttPrvnNme);
    }
    else {
      this.form.get("site.siteIdAddress").setValue('');
    }

    if ((this.id == 0 || this.helper.isEmpty(this.form.get("site.country").value)) && (this.showMDS || this.isCEChng)) {
      this.setPhysicalAddressValidations()

      if ((this.form.get("site.country").value == null) || this.form.get("site.country").value.trim().toUpperCase() == "US" || this.form.get("site.country").value.trim().toUpperCase() == "U.S" || this.form.get("site.country").value.trim().toUpperCase() == "USA" ||
        this.form.get("site.country").value.trim().toUpperCase() == "U.S.A" || this.form.get("site.country").value.trim().toUpperCase() == "" || this.form.get("site.country").value.trim().toUpperCase() == "UNITED STATE OF AMERICAN"
        || this.form.get("site.country").value.trim().toUpperCase() == "UNITED STATES" || this.form.get("site.country").value.trim().toUpperCase() == "UNITED STATES OF AMERICA" || this.form.get("site.country").value.trim() == "") {
        if (this.prevUSIntlCd == "I") {
          this.clearAssignee();
        }
        this.form.get("site.us").setValue('D');
        this.prevUSIntlCd = "D";
        this.form.get("site.installSite.phoneCode").setValue("")
        this.form.get("site.installSite.cellphoneCode").setValue("")
      }
      else {
        if (this.prevUSIntlCd == "D") {
          this.clearAssignee();
        }
        this.form.get("site.us").setValue('I');
        this.prevUSIntlCd = "I";
      }
    }
  }

  loadDevMgmt(res: MDSEventSiteLookup) {
    if (res.mnsAsasTypes.length > 0) {
      if (res.mnsOrders.length > 0) {
        let common = res.mnsOrders.filter(a => res.mnsAsasTypes.some(b => b.mnsOrdrNbr == a.mnsOrdrNbr && b.deviceId == a.deviceId && a.mnsSrvcTierId == b.mnsSrvcTierId))
        if (common.length == 0) {
          res.mnsOrders = [...res.mnsOrders, ...res.mnsAsasTypes]
            .filter(
              (set => // store the set and return the actual callback
                o => set.has(o.a) ? false : set.add(o.a)
              )(new Set()) // use an IIFE to create a Set and store it set
            );
        }
      } else {
        res.mnsOrders = res.mnsAsasTypes
      }
    }

    if (res.mnsOrders.length > 0) {
      if (this.mnsOrderList.length > 0) {
        this.mnsOrderList = res.mnsOrders.map(
          a => {
            let common = this.mnsOrderList.find(b => b.deviceId == a.deviceId && b.mnsOrdrId == a.mnsOrdrId && a.mnsSrvcTierId == b.mnsSrvcTierId)
            a.eventDevSrvcMgmtId = (common || { eventDevSrvcMgmtId: a.eventDevSrvcMgmtId }).eventDevSrvcMgmtId
            a.odieDevNme = (common || { odieDevNme: "" }).odieDevNme
            return a;
          }
        )
      }
      this.mnsOrderList = res.mnsOrders
    } else {
      this.mnsOrderList = []
    }

    if (!this.isPageLoad && this.mdsRedesignDevInfoList.length === 1) {
      this.mnsOrderList.map(a => {
        a.odieDevNme = this.mdsRedesignDevInfoList[0].odieDevNme
      })
    }
  }
  onTransportVASTableUpdate(e) {
    e.data.assocH6 = this.helper.removeSpaces(e.data.assocH6);
  }

  onCpeDeviceUpdate(e) {
    this.devCompletionList.map(a => {
      if (a.odieDevNme == e.data.odieDevNme) {
        a.h6 = e.data.assocH6
      }

      return a;
    })

    let row = e.data
    this.mnsOrderList = this.mnsOrderList.map(
      a => {
        if (a.mnsOrdrNbr == row.cpeOrdrNbr && a.deviceId == row.deviceId) {
          a.odieDevNme = row.odieDevNme
        }

        return a
      }
    )
  }

  onCpeDeviceRemoved(e) {
    e.data.recStusId = 2;
    this.deletedCpeDeviceList.push(e.data);
  }

  //onCpeDeviceRemove(e) {
  //  this.devCompletionList.map(a => {
  //    if (a.odieDevNme == e.data.odieDevNme) {
  //      a.recStusId = 2
  //    }

  //    return a;
  //  })
  //}

  loadMnsOrders(obj: MDSEventSiteLookup) {
    this.mnsOrderList = obj.mnsOrders
    if (this.mdsRedesignDevInfoList.length === 1) {

      this.mnsOrderList.map(a => {
        a.odieDevNme = this.mdsRedesignDevInfoList[0].odieDevNme
      })
      //this.mnsOrderList[0].odieDevNme = this.mdsRedesignDevInfoList[0].odieDevNme;
    }
    this.relatedMnsOrderList = obj.relatedMnsOrders
  }

  onViewOrderDetailClicked(e, data) {
    e.preventDefault();
    this.modalCpeDeviceList = [];
    this.modalPendingComponentList = [];
    this.modalInstalledInventoryList = [];

    this.spinner.show();
    this.searchService.viewOrderDetails(data.assocH6, data.ccd, 'N', this.id).subscribe(
      res => {
        console.log(res)
        if (res != null) {
          this.modalCpeDeviceList = res.result1;
          this.modalPendingComponentList = res.result2;
          this.modalInstalledInventoryList = res.result3;
        } else {

        }

        $("#viewCpeOrderDetails").modal("show");
      },
      error => {
        this.spinner.hide()
      },
      () => this.spinner.hide()
    );

    //return false;
  }
  // CPE DEVICE

  // PHYSICAL ADDRESS
  setPhysicalAddressValidations() {
    //console.log(`PHYSICAL ADDRESS REQUIRED: ${this.isPhysicalAddressRequired}`)
    if (this.isPhysicalAddressRequired) {
      this.setValidators(this.form, "site.installSite.poc", [Validators.required])
      this.setValidators(this.form, "site.installSite.phone", [Validators.required])
      this.setValidators(this.form, "site.serviceAssurance.poc", [Validators.required])
      this.setValidators(this.form, "site.serviceAssurance.phone", [Validators.required])
      this.setValidators(this.form, "site.address", [Validators.required])
      this.setValidators(this.form, "site.state", [Validators.required])
      this.setValidators(this.form, "site.country", [Validators.required])
      this.setValidators(this.form, "site.city", [Validators.required])
      this.setValidators(this.form, "site.zip", [Validators.required])
    } else {
      this.clearValidators(this.form.get("site") as FormGroup)
    }
  }
  // PHYSICAL ADDRESS

  // SITE INFO
  getMds3rdPartyVendorByServiceTypeId(options) {
    //console.log(options)
    //console.log(this.mds3rdPartyServiceList)
    return {
      store: this.mds3rdPartyVendorList,
      //filter: options.data ? ["srvcTypeId", "=", options.data.srvcTypeId] : null
    }
  }

  getMds3rdPartyServiceByServiceTypeId(options) {
    return {
      store: this.mds3rdPartyServiceList,
      filter: options.data ? ["srvcTypeId", "=", options.data.srvcTypeId] : null
    }
  }

  onSiteServiceNewRow(e) {
    if (this.mdsRedesignDevInfoList != null) {
      e.data.odieDevNme = this.mdsRedesignDevInfoList.length == 1 ? this.mdsRedesignDevInfoList[0].odieDevNme : null
    }
  }

  onThirdPartyNewRow(e) {
    if (this.mdsRedesignDevInfoList != null) {
      e.data.odieDevNme = this.mdsRedesignDevInfoList.length == 1 ? this.mdsRedesignDevInfoList[0].odieDevNme : null
    }
  }

  onWiredTransportNewRow(e) {
    if (this.mdsRedesignDevInfoList != null) {
      e.data.odieDevNme = this.mdsRedesignDevInfoList.length == 1 ? this.mdsRedesignDevInfoList[0].odieDevNme : null
    }
    e.data.vlanNbr = e.data.vlanNbr != null ? e.data.vlanNbr : ""
    e.data.readyBeginCdBoolean = false
    e.data.optInHCdBoolean = false
    e.data.optInCktCdBoolean = false
    e.data.isFromLookup = false
    e.data.isNew = true
    console.log(e.data)
  }

  onWirelessTransportNewRow(e) {
    if (this.mdsRedesignDevInfoList != null) {
      e.data.odieDevNme = this.mdsRedesignDevInfoList.length == 1 ? this.mdsRedesignDevInfoList[0].odieDevNme : null
    }
  }

  onServiceTypeChanged(rowData: any, value: any): void {
    //rowData.m5OrdrNbr = ""
    if ([3, 5, 7, 8, 9, 10].includes(value as number))
      rowData.mach5SrvcOrdrId = ""
    rowData.srvcTypeId = value
    if ([1, 5, 6, 7, 8, 10].includes(value as number))
      rowData.thrdPartyVndrId = null
    if ([1, 5, 6, 7, 8, 9, 10].includes(value as number))
      rowData.thrdPartyId = ""
    if ([1, 5, 6, 7, 9, 10].includes(value as number))
      rowData.thrdPartySrvcLvlId = null
    if ([].includes(value as number))
      rowData.actvDt = null
    if ([].includes(value as number))
      rowData.cmntTxt = ""
    rowData.odieDevNme = this.mdsRedesignDevInfoList.length > 0 ? this.mdsRedesignDevInfoList[0].odieDevNme : ""

    console.log(`VALUE : ${value}`);

    this.isServiceIdDisabled = [3, 5, 7, 8, 9, 10].includes(value as number) ? true : false
    this.isThirdPartyVendorDisabled = [1, 5, 6, 7, 8, 10].includes(value as number)  ? true : false
    this.isThirdPartyIdDisabled = [1, 5, 6, 7, 8, 9, 10].includes(value as number) ? true : false
    this.isThirdPartyServiceLevelDisabled = [1, 5, 6, 7, 9, 10].includes(value as number) ? true : false
    this.isActivationDateDisabled = [].includes(value as number) || this.isMember ? true : false
    this.isCommentDisabled = [].includes(value as number) || this.isMember? true : false

    if(this.id > 0 && this.isActivator) {
      this.isMach5Disabled = true;
      this.isServiceIdDisabled = true;
      this.isServiceTypeDisabled = true;
      this.isThirdPartyVendorDisabled = true;
      this.isThirdPartyIdDisabled = true;
      this.isThirdPartyServiceLevelDisabled = true;
      this.isActivationDateDisabled = false;
      this.isActivationDateDisabled = false;
      this.isOdieDisabled = true;
    }

  }

  onRelatedComponentsClicked(e) {
    $("#relatedMnsOrder").modal("show");
    return false;
  }
  // SITE INFO

  // CPE DELIVERY OPTION
  onCpeDeliveryOptionChanged(e) {
    let id = e.value
    this.form.get("cpe.cpeDispatchEmail").reset()

    if (this.showCPEOption) {
      this.setValidators(this.form, "cpe.cpeDispatchEmail", [Validators.required])
      this.setValidators(this.form, "cpe.cpeDispatchComment", [Validators.required, Validators.maxLength(1000)])
      if (id == 10) {
        this.systemConfigService.getSysCfgByName("MDSCPEDispatchArrisEmail").subscribe(
          res => {
            this.form.get("cpe.cpeDispatchEmail").setValue(res[0].prmtrValuTxt)
          });
      } else if (id == 12) {
        this.systemConfigService.getSysCfgByName("MDSCPEDispatchCommScopeEmail").subscribe(
          res => {
            this.form.get("cpe.cpeDispatchEmail").setValue(res[0].prmtrValuTxt)
          });
      } else if (id == 2) {
        if (this.form.get("site.h6").value != null && this.form.get("site.ccd").value != null) {
          // if ((!isNullOrUndefined(this.mds.h6)) && (!isNullOrUndefined(this.mds.ccd))) {
          //  if (!this.helper.isEmpty(this.mds.h6) && !this.helper.isEmpty(this.mds.ccd)){
          let date = moment((this.form.get("site.ccd").value)).format("MM/DD/YYYY")
          this.searchService.getSiteLookup((this.form.get("site.h6").value), date, 'N', this.showNID).subscribe(
            res => {
              this.form.get("cpe.cpeDispatchEmail").setValue(res.cpeDispatchEmail)
              this.isPageLoad = false
            }, error => {
              this.spinner.hide()
            }, () => {
              this.spinner.hide();
            });
        }
      }

    } else {
      this.setValidators(this.form, "cpe.cpeDispatchEmail", [])
      this.setValidators(this.form, "cpe.cpeDispatchComment", [])
    }

    const country = this.form.get('site.country').value == null ? "US" : this.form.get('site.country').value;
    const isDefaultCountry = this.defaultCountry.includes(country.toUpperCase());
    const specialDeliveryOption = [3, 4];
    //console.log(`isdefaultctry: ${isDefaultCountry}`)
    if (!isDefaultCountry) {
      if (specialDeliveryOption.includes(this.oldDeliveryOption) && !specialDeliveryOption.includes(id)) {
        // Drop assignee
        this.form.get('schedule.assignedTo').setValue('');
        this.form.get('schedule.displayedAssignedTo').setValue('');
        this.dblBookingMessage = '';
      } else if (!specialDeliveryOption.includes(this.oldDeliveryOption)) {
        // Drop assignee
        this.form.get('schedule.assignedTo').setValue('');
        this.form.get('schedule.displayedAssignedTo').setValue('');
        this.dblBookingMessage = '';
      }
    }
    this.oldDeliveryOption = id;
  }

  onCpeLookup() {
    var URL = "https://cpegjlreports.corp.sprint.com/CPEWeb/Login.aspx?ReturnUrl=/CPEWeb/Zip_Clli_Lookup/view_clli_from_zip_code.aspx";
    if (isDevMode())
      URL = "https://cpegjlreports.test.sprint.com/CPEWeb/Login.aspx?ReturnUrl=/CPEWeb/Zip_Clli_Lookup/view_clli_from_zip_code.aspx";
    var w = window.open(URL, 'CpeLookupUrl', 'dependent=yes,menubar=no,resizable=yes,status=no,toolbar=no,titlebar=no,scrollbars=yes,left=400,top=200,width=600px,height=600px');
    w.focus();
    return false;
  }

  onHistoryLookup() {
    this.helper.gotoEventHistory(this.id);
  }

  setCpeDeliveryOptionValidation() {
    if (this.showNID || (this.showMDS && !this.showDisconnect)) {
      this.setValidators(this.form, "cpe.cpeDeliveryOption", [Validators.required])
    } else {
      this.form.get("cpe.cpeDeliveryOption").setValue(null)
      this.setValidators(this.form, "cpe.cpeDeliveryOption", [])
    }
  }
  // CPE DELIVERY OPTION
  // NON DISCONNECT
  // MDS

  // EVENT
  onIsGenericH1(e) {
    this.clearValidators(this.form.get("event") as FormGroup)

    if (e.value) {
      this.form.get("event.isEscalation").disable() // DISABLE Is Escalation checkbox
      this.form.get("event.isEscalation").setValue(false)
      this.setValidators(this.form, "requestedActivityDate", [Validators.required])
      this.setValidators(this.form, "schedule.startDate", [])
    } else {
      this.form.get("event.isEscalation").enable() // DISABLE Is Escalation checkbox
      if (this.form.get("event.isEscalation").value) {
        this.setValidators(this.form, "event.escalationReason", [Validators.required])
        this.setValidators(this.form, "event.primaryRequestDate", [Validators.required])
        this.setValidators(this.form, "event.secondaryRequestDate", [Validators.required, this.isNotPastOneHour(this.form.get("event.primaryRequestDate") as FormControl)])
        this.setValidators(this.form, "event.escalationBusinessJustification", [Validators.required])
      }
      this.setValidators(this.form, "requestedActivityDate", [])
    }

    if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
      this.setValidators(this.form, "schedule.withConferenceBridge", [Validators.required])
    } else {
      this.setValidators(this.form, "schedule.withConferenceBridge", [])
    }
  }

  onWorkflowStatusChanged(e) {
    if (this.workflowStatus.value == 7) {
      this.setValidators(this.form, "activityCompletion", [Validators.required])
    } else {
      this.setValidators(this.form, "activityCompletion", [])
    }

    if (this.workflowStatus.value == 5) { // Reschedule
      this.setValidators(this.form, "mdsFailCode", [Validators.required])
    } else {
      this.setValidators(this.form, "mdsFailCode", [])
      this.form.get("mdsFailCode").setValue(null)
    }
  }
  // EVENT


  setValidators(form: FormGroup, controlName: string, validators: ValidatorFn[]) {
    form.get(controlName).setValidators(validators)
    form.get(controlName).updateValueAndValidity()
  }

  clearValidators(formGroup: FormGroup | FormArray): void {
    if (!(formGroup.controls == null || formGroup.controls == undefined)) {
      Object.keys(formGroup.controls).forEach(key => {
        const control = formGroup.controls[key] as FormControl | FormGroup | FormArray;
        if (control instanceof FormControl) {
          control.clearValidators();
          control.updateValueAndValidity()
        } else if (control instanceof FormGroup || control instanceof FormArray) {
          this.clearValidators(control);
        }
      });
    } else {
      formGroup.clearValidators();
      formGroup.updateValueAndValidity();
    }
  }

  getErrors(formGroup: FormGroup | FormArray): void {
    if (!(formGroup.controls == null || formGroup.controls == undefined)) {
      Object.keys(formGroup.controls).forEach(key => {
        const control = formGroup.controls[key] as FormControl | FormGroup | FormArray;
        if (control instanceof FormControl) {
          if (!control.valid && control.status != "DISABLED") {
            console.log(`${key}: ${control.valid}`)
          }
        } else if (control instanceof FormGroup || control instanceof FormArray) {
          this.getErrors(control);
        }
      });
    } else {
      console.log("NOT A FORM GROUP")
    }
  }

  isMixed(mdsMacActivityList: MDSMACActivity[]): any {
    return (control: FormControl): { [key: string]: any } => {
      let mdsMacActivity = control.value as number[]
      let genericCount = 0
      let nonGenericCount = 0

      if (mdsMacActivity != null && mdsMacActivityList != undefined) {
        mdsMacActivity.map(a => {
          if (mdsMacActivityList.find(b => b.mdsMacActyId == a).mdsMacActyNme.includes("999999999")) {
            genericCount++
          } else {
            nonGenericCount++
          }
        })
      }

      return (genericCount > 0 && nonGenericCount > 0) ? { mixed: true } : null
    };
  }

  isNotPastOneHour(dateToCompare: FormControl): any {
    return (control: FormControl): { [key: string]: any } => {
      let primaryRequestDate = moment(dateToCompare.value).format('MM/DD/YYYY hh:mm a')
      let secondaryRequestDate = moment(control.value).format('MM/DD/YYYY hh:mm a')
      let diff = moment(secondaryRequestDate).diff(primaryRequestDate, 'minutes');

      return (diff >= 60) ? { notPastOneHour: true } : null
    };
  }

  getFormDetails(): MdsEvent {
    let mds = new MdsEvent();
    // DEFAULTS
    mds.h6Lookup = this.h6Lookup || new MDSEventSiteLookup({})
    mds.cpeOrdrCd = false
    mds.mnsOrdrCd = false

    mds.eventId = this.id
    mds.mdsEventNtwkActyIds = this.form.get("activityType").value;
    mds.eventStusId = this.form.get("eventStatus").value

    if (this.form.get("workflowStatus").value == WORKFLOW_STATUS.Visible || this.form.get("workflowStatus").value == WORKFLOW_STATUS.Submit) {
      mds.eventTitleTxt = this.getItemTitle()
    } else {
      mds.eventTitleTxt = this.form.get("itemTitle").value;
    }
    

    mds.isSuppressedEmails = this.form.get('isSuppressedEmails').value

    if (this.isWoob) {
      mds.woobIpAdr = this.form.get('woobIpAddress').value
    }

    mds.contactDetails = this.contactList;
    mds.contactDetails.forEach(i => { i.emailCd = (i.emailCdIds || []).join(','); i.objTypCd = this.objType });
    console.log(mds.contactDetails)

    // NETWORK ONLY
    if (this.showNetwork) {
      mds.ntwkH6 = this.form.get("network.h6").value
      mds.ntwkH1 = this.form.get("network.h1").value
      mds.ntwkCustNme = this.form.get("network.customerName").value
      // Added by Sarah Sandoval [20220218]
      // To put H6 Customer Name on MDS Customer Name as a field holder only for Network type only
      mds.custNme = this.form.get("mds.odieCustomerName").value
      mds.mplsEventActyTypeIds = this.form.get("network.networkActivityType").value
      mds.rltdCmpsNcrCd = this.form.get("network.isRelatedCompassNCR").value
      if (mds.rltdCmpsNcrCd) {
        mds.rltdCmpsNcrNme = this.form.get("network.compassNCR").value
      }
      else {
        mds.rltdCmpsNcrNme = null;
      }
      mds.custSowLocTxt = this.form.get("customerSOW").value
      mds.ipmDes = this.form.get("ipmDescription").value
      mds.shrtDes = this.form.get("shortDescription").value

      mds.networkCustomer = this.getNetworkCustomer()
      mds.networkTransport = this.getNetworkTransport()
      console.log(mds.networkCustomer)
    }

    // CPE INFORMATION
    if (this.showNID || (this.showMDS && !this.showDisconnect)) {
      mds.h6 = this.form.get("site.h6").value

      const ccd = this.form.get("site.ccd").value;
      if (ccd) {
        mds.ccd = this.helper.dateObjectToString(ccd);
      }

      let siteId = this.form.get("site.siteIdAddress").value;
      if (!this.helper.isEmpty(siteId)) {
        siteId = siteId.toString().split("_")[0];
        mds.siteIdTxt = siteId;
      }
      else {
        mds.siteIdTxt = "";
      }

      mds.cpeDevice = this.getCpeDeviceList() // DONE

      if (this.showMDS && !this.showDisconnect) {
        mds.mdsRedesignDevInfo = this.getMdsRedesignDevInfoList()
        mds.devCompletion = this.getDevCompletionList()
        mds.mnsOrder = this.getMnsOrderList()
        mds.siteService = this.getSiteServiceList() // DONE
        mds.thirdParty = this.getThirdPartyList() // DONE
        mds.wiredTransport = this.getWiredTransportList() // DONE
        mds.wirelessTransport = this.getWirelessTransportList() // DONE
        mds.portBandwidth = this.getPortBandwidthList() // DONE

        console.log(mds.wiredTransport)
      }

      mds.streetAdr = this.form.get("site.address").value
      mds.sttPrvnNme = this.form.get("site.state").value
      mds.flrBldgNme = this.form.get("site.bldg_flr_rm").value
      mds.ctryRgnNme = this.form.get("site.country").value
      mds.ctyNme = this.form.get("site.city").value
      mds.zipCd = this.form.get("site.zip").value

      if ((mds.ctryRgnNme == null) || (mds.ctryRgnNme.trim().toUpperCase() == "US" || mds.ctryRgnNme.trim().toUpperCase() == "U.S" || mds.ctryRgnNme.trim().toUpperCase() == "USA" ||
        mds.ctryRgnNme.trim().toUpperCase() == "U.S.A" || mds.ctryRgnNme.trim().toUpperCase() == "" || mds.ctryRgnNme.trim().toUpperCase() == "UNITED STATE OF AMERICAN"
        || mds.ctryRgnNme.trim().toUpperCase() == "UNITED STATES" || mds.ctryRgnNme.trim().toUpperCase() == "UNITED STATES OF AMERICA" || mds.ctryRgnNme.trim() == "")) {
        mds.usIntlCd = this.prevUSIntlCd = "D";
        this.form.get("site.us").setValue(mds.usIntlCd)
        this.form.get("site.installSite.phoneCode").setValue("")
        this.form.get("site.installSite.cellphoneCode").setValue("")
      }
      else {
        mds.usIntlCd = this.prevUSIntlCd = "I";
        this.form.get("site.us").setValue(mds.usIntlCd)
        //console.log(this.form.get("site.us").value)
        //console.log(`UsorIntl: ${mds.zipCd}`)
      }

      //mds.usIntlCd = this.form.get("site.us").value
      mds.srvcAvlbltyHrs = this.form.get("site.saContactHrs").value
      mds.srvcTmeZnCd = this.form.get("site.saTimezone").value
      mds.instlSitePocNme = this.form.get("site.installSite.poc").value
      mds.instlSitePocIntlPhnCd = this.form.get("site.installSite.phoneCode").value
      mds.instlSitePocPhnNbr = this.form.get("site.installSite.phone").value
      mds.instlSitePocIntlCellPhnCd = this.form.get("site.installSite.cellphoneCode").value
      mds.instlSitePocCellPhnNbr = this.form.get("site.installSite.cellphone").value
      mds.srvcAssrnPocNme = this.form.get("site.serviceAssurance.poc").value
      mds.srvcAssrnPocIntlPhnCd = this.form.get("site.serviceAssurance.phoneCode").value
      mds.srvcAssrnPocPhnNbr = this.form.get("site.serviceAssurance.phone").value
      mds.srvcAssrnPocIntlCellPhnCd = this.form.get("site.serviceAssurance.cellphoneCode").value
      mds.srvcAssrnPocCellPhnNbr = this.form.get("site.serviceAssurance.cellphone").value

      mds.sprintCpeNcrId = this.form.get("cpe.cpeDeliveryOption").value
      //mds.nidSerialNbr = this.form.get("cpe.nidSerialNbr").value
      //mds.nidHostNme = this.form.get("cpe.nidHostName").value
      mds.mdrNumber = this.form.get("cpe.mdrNumber").value
      mds.cpeDspchEmailAdr = this.form.get("cpe.cpeDispatchEmail").value
      mds.cpeDspchCmntTxt = this.form.get("cpe.cpeDispatchComment").value

      //if (this.showNID) {
      //  mds.nidSerialNbr = this.form.get("cpe.nidSerialNbr").value
      //  mds.nidHostNme = this.form.get("cpe.nidHostName").value
      //}
    }

    // MDS ONLY
    if (this.showMDS) {
      mds.h1 = this.form.get("mds.h1").value
      mds.custNme = this.form.get("mds.odieCustomerName").value
      //mds.custAcctTeamPdlNme = this.form.get("mds.teamPDL").value
      mds.cntctEmailList = this.form.get("mds.contactEmails").value
      mds.mdsActyTypeId = this.form.get("mds.mdsActivityType").value
      mds.mnsPmId = this.form.get("mds.mnspmId").value
      mds.odieCustId = this.form.get("mds.odieCustId").value

      if (this.showDisconnect) { // DISCONNECT
        //mds.discMgmtCd = this.form.get("disconnect.isRequireCoordination").value == 1 ? "Y" : "N"
        //mds.fullCustDiscCd = this.form.get("disconnect.isTotalDisconnect").value == 1 ? true : false
        //mds.fullCustDiscReasTxt = mds.fullCustDiscCd ? this.form.get("disconnect.reasonForTotalDisconnect").value : null
        //mds.shrtDes = mds.fullCustDiscCd ? null : this.form.get("disconnect.shortDescription").value

        if (this.form.get("disconnect.isRequireCoordination").value == null) {
          //this.helper.notify('Does this disconnect require handoff? cannot be blank', Global.NOTIFY_TYPE_WARNING);
          //this.spinner.hide()
          //return;
        }
        else {
          mds.discMgmtCd = this.form.get("disconnect.isRequireCoordination").value == 1 ? "Y" : "N"
        }

        if (this.form.get("disconnect.isTotalDisconnect").value == null) {
          //this.helper.notify('Is this a total customer disconnect ? cannot be blank', Global.NOTIFY_TYPE_WARNING);
          //this.spinner.hide()
          //return;
        } else {
          mds.fullCustDiscCd = this.form.get("disconnect.isTotalDisconnect").value == 1 ? true : false
        }

        if (this.form.get("disconnect.isTotalDisconnect").value == 1 && this.helper.isEmpty(this.form.get("disconnect.reasonForTotalDisconnect").value)) {
          //this.helper.notify('Reason for total customer disconnect cannot be blank.', Global.NOTIFY_TYPE_WARNING);
          //this.spinner.hide()
          //return;
        }
        else {
          mds.fullCustDiscReasTxt = mds.fullCustDiscCd ? this.form.get("disconnect.reasonForTotalDisconnect").value : null
        }

        if (this.form.get("disconnect.isTotalDisconnect").value == 0 && this.helper.isEmpty(this.form.get("disconnect.shortDescription").value)) {
          //this.helper.notify('Short Description cannot be blank.', Global.NOTIFY_TYPE_WARNING);
          //this.spinner.hide()
          //return;
        }
        else {
          mds.shrtDes = mds.fullCustDiscCd ? null : this.form.get("disconnect.shortDescription").value
        }

        this.eventDiscoDevListFinal = this.eventDiscoDevList.map(a => {
          a.eventId = this.id > 0 ? this.id : 0
          a.eventDiscoDevId = isNaN(+a.eventDiscoDevId) ? 0 : a.eventDiscoDevId
          a.readyBeginCd = a.readyBegin ? "Y" : "" //"N"
          a.optInHCd = a.optInH ? "Y" : "" //"N"
          a.optInCktCd = a.optInCkt ? "Y" : "" //"N"
          a.creatDt = a.creatDt || new Date()
          return a
        })
        mds.eventDiscoDev = this.eventDiscoDevListFinal
        mds.custSowLocTxt = this.form.get("customerSOW").value
      } else { // MAC
        mds.mdsEventMacActyIds = this.mdsMacActivity.value
        mds.custSowLocTxt = this.form.get("customerSOW").value
        mds.ipmDes = this.form.get("ipmDescription").value
        mds.shrtDes = this.form.get("shortDescription").value

        // PRE STAGING
        if (this.showMDS && this.showPreStaging) {
          mds.shippedDt = this.form.get("shipping.shippedDate").value
          mds.shipTrkRefrNbr = this.form.get("shipping.shipmentRefNo").value
          mds.shipCustEmailAdr = this.form.get("shipping.shipmentCustomerEmail").value
          mds.shipDevSerialNbr = this.form.get("shipping.shippedDeviceSerialNumber").value
          mds.shipCxrNme = this.form.get("shipping.shipmentCarrierName").value
          mds.altShipPocNme = this.form.get("shipping.altShipPocNme").value
          mds.altShipPocPhnNbr = this.form.get("shipping.altShipPocPhnNbr").value
          mds.altShipPocEmail = this.form.get("shipping.altShipPocEmail").value
          mds.altShipStreetAdr = this.form.get("shipping.altShipStreetAdr").value
          mds.altShipSttPrvnNme = this.form.get("shipping.altShipSttPrvnNme").value
          mds.altShipFlrBldgNme = this.form.get("shipping.altShipFlrBldgNme").value
          mds.altShipCtyNme = this.form.get("shipping.altShipCtyNme").value
          mds.altShipCtryRgnNme = this.form.get("shipping.altShipCtryRgnNme").value
          mds.altShipZipCd = this.form.get("shipping.altShipZipCd").value
        }
      }
    }

    mds.reqorUserId = this.form.get("requestor.id").value
    mds.reqorUserCellPhnNbr = this.form.get("requestor.cellphone").value == null ? "" : this.form.get("requestor.cellphone").value

    mds.esclCd = this.form.get("event.isEscalation").value
    if (mds.esclCd) {
      mds.esclReasId = this.form.get("event.escalationReason").value

      let primReqDate = this.form.get("event.primaryRequestDate").value;
      primReqDate = this.helper.dateObjectToString(this.form.get("event.primaryRequestDate").value);
      mds.primReqDt = primReqDate;

      let scndyReqDate = this.form.get("event.secondaryRequestDate").value;
      scndyReqDate = this.helper.dateObjectToString(this.form.get("event.secondaryRequestDate").value);
      mds.scndyReqDt = scndyReqDate;

      //mds.primReqDt = new Date(this.form.get("event.primaryRequestDate").value)
      //mds.scndyReqDt = new Date(this.form.get("event.secondaryRequestDate").value)
      mds.esclBusJustnTxt = this.form.get("event.escalationBusinessJustification").value
      // mds.esclBusJustnTxt = this.form.get("event.escalationPolicy").value
    }
    //  let activators: User[] = []

    //    if (!this.helper.isEmpty(this.form.get("event.publishedEmailCC").value)) {
    //      let adActivators = this.form.get("event.assignedToId").value
    //      if (adActivators != null) {
    //        let activators = adActivators.toString()
    //        if (activators.length > 0) {
    //          if (activators.indexOf(',') != -1) {
    //            mds.pubEmailCcTxt = activators.toString().split(",").map(a => parseInt(a));
    //          } else {
    //            mds.pubEmailCcTxt = [adActivators];
    //          }
    //        }
    //      }
    //    }

    mds.pubEmailCcTxt = this.form.get("event.publishedEmailCC").value
    mds.cmpltdEmailCcTxt = this.form.get("event.completeEmailCC").value
    mds.frcdftCd = this.isGenericH1.value
    mds.failReasId = this.form.get("mdsFailCode").value
    //console.log(`FT check ` + JSON.stringify((this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1)))
    //console.log(`FT check2 ` + JSON.stringify(this.isGenericH1.value))
    //console.log(`FT value ` + JSON.stringify(this.fastTrackType.value))
    if ((this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1) || this.isGenericH1.value) {
      mds.mdsFastTrkTypeId = this.fastTrackType.value == "At Will" ? "A" : "S"
    } else {
      mds.mdsFastTrkTypeId = null
    }
    if ((this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1) || this.isGenericH1.value) {
      mds.frcdftCd = this.isGenericH1.value
      if (this.requestedActivityDate.value == null) {
        //this.helper.notify('Activity Date is required.', Global.NOTIFY_TYPE_WARNING);
        //this.spinner.hide()
        //return;
      } else {
        mds.strtTmst = this.helper.dateObjectToString(this.requestedActivityDate.value);
        mds.endTmst = this.helper.dateObjectToString(this.helper.dateAdd(mds.strtTmst, "hour", 1));
      }
      mds.activators = [5001] // COWS ES Activators
      this.form.get("schedule.startDate").setValue(this.requestedActivityDate.value)
    } else {
      if (this.form.get("schedule.withConferenceBridge").value == null && this.form.get("disconnect.isRequireCoordination").value == 1) {
        mds.cnfrcBrdgId = 1;
      } else {
        mds.cnfrcBrdgId = this.form.get("schedule.withConferenceBridge").value
      }
      mds.cnfrcBrdgNbr = this.form.get("schedule.bridgeNumber").value
      mds.cnfrcPinNbr = this.form.get("schedule.bridgePin").value
      mds.onlineMeetingAdr = this.form.get("schedule.onlineMeetingAdr").value
      mds.custLtrOptOut = this.form.get("schedule.custLtrOptOut").value

      let startDate = this.form.get("schedule.startDate").value;
      startDate = this.helper.dateObjectToString(this.form.get("schedule.startDate").value);
      mds.strtTmst = startDate;

      let endDate = this.form.get("schedule.endDate").value;
      endDate = this.helper.dateObjectToString(this.form.get("schedule.endDate").value);
      mds.endTmst = endDate;
      mds.extraDrtnTmeAmt = this.form.get("schedule.extraDuration").value

      mds.eventDrtnInMinQty = this.form.get("schedule.eventDuration").value
      mds.softAssignCd = this.form.get("schedule.softAssign").value || false
      //mds.activators = (!(mds.esclCd && this.isOnlyMember)) ? this.getSelectedActivators(this.form.get("workflowStatus").value) : []
      mds.activators = (!(mds.esclCd && this.isOnlyMember) || this.isNtwkIntl) ? this.getSelectedActivators(this.form.get("workflowStatus").value) : []


      if(this.isReviewer && mds.esclCd && this.workflowStatus.value == WORKFLOW_STATUS.Submit) {
         mds.isSuppressedEmails = true;
      }
    }

    mds.cmntTxt = (this.form.get("comments").value || "").replace(/\r?\n/g, "<br/>");
    mds.preCfgCmpltCd = this.form.get("preconfigCompleted").value == "Y" ? true : false
    mds.wrkflwStusId = this.form.get("workflowStatus").value
    //mds.isSuppressedEmails = this.form.get("isSuppressedEmails").value || false

    let activityTypeCompletionList: number[] = this.form.get("activityCompletion").value || []
    mds.mdsCmpltCd = activityTypeCompletionList.filter(a => a == 1).length > 0 ? true : false
    mds.ntwkCmpltCd = activityTypeCompletionList.filter(a => a == 2).length > 0 ? true : false
    mds.vasCmpltCd = activityTypeCompletionList.filter(a => a == 3).length > 0 ? true : false
    mds.reviewerUserId = this.isReviewer ? this.userService.loggedInUser.userId : 0
    mds.activatorUserId = this.isActivator ? this.userService.loggedInUser.userId : 0

    return mds
  }

  getItemTitle() {
    let title = ""
    let mdsActyTypeId = this.mdsActivityType.value
    let address: string[] = []
    address.push(this.city.value)
    address.push(this.state.value)
    address.push(this.country.value)

    // Updated by Sarah Sandoval [20210410] - reflecting how OLD COWS build Event Title; Commented old implementation
    // Whenever MDS is selected, ODIE Customer Name is used otherwise it will be Network Customer Name
    let activityTypeId = this.activityType.value != null ? this.activityType.value as any[] : []
    let activityType = activityTypeId
      .sort((a, b) => { return a - b })
      .map(a => {
        return this.activityTypeList.find(b => b.ntwkActyTypeId == a).ntwkActyTypeDes
      }).join("+")

    if (this.showMDS) {
      let customerName = this.odieCustomerName.value
      let cdAddress = address.filter(a => !this.helper.isReallyEmpty(a)).join(",")
      let mdsActyTypeDes = mdsActyTypeId != null ? this.mdsActivityTypeList.find(a => a.mdsActyTypeId == mdsActyTypeId).mdsActyTypeDes : ""
      mdsActyTypeDes = (cdAddress.length > 0) ? `${cdAddress} -- ${mdsActyTypeDes}` : mdsActyTypeDes

      title = `${activityType} - ${customerName} - ${mdsActyTypeDes}`
    }
    else {
      let customerName = this.form.get("network.customerName").value != null ? this.form.get("network.customerName").value : "" 
      title = `${activityType} - ${customerName}`

      console.log(this.showNetworkOnly)
      if (this.showNetworkOnly) {
        // Added condition when H6 Lookup is not clicked [09212021]
        if (!this.helper.isEmpty(this.h1CustomerName) && !this.helper.isEmpty(this.h6CustomerName)) {
          title = `${activityType} - ${this.h1CustomerName} - ${this.h6CustomerName}`
        }
      }
      console.log(title)
    }

    //if (this.showMDS) { // MDS
    //  let customerName = this.odieCustomerName.value

    //  let cdAddress = address.filter(a => !this.helper.isEmpty(a)).join(",")
    //  let mdsActyTypeDes = mdsActyTypeId != null ? this.mdsActivityTypeList.find(a => a.mdsActyTypeId == mdsActyTypeId).mdsActyTypeDes : ""
    //  mdsActyTypeDes = (cdAddress.length > 0) ? `${cdAddress} -- ${mdsActyTypeDes}` : mdsActyTypeDes

    //  title = `MDS - ${customerName} - ${mdsActyTypeDes}`
    //}

    //if (this.showNetwork) { // NETWORK or VAS
    //  let customerName = this.form.get("network.customerName").value != null ? this.form.get("network.customerName").value : ""
    //  let activityTypeId = this.activityType.value != null ? this.activityType.value as any[] : []
    //  let activityType = activityTypeId
    //    .sort((a, b) => { return a - b })
    //    .map(a => {
    //      if (a == 1) {
    //        return "MDS"
    //      } else if (a == 2) {
    //        return "NETWORK"
    //      } else if (a == 4) {
    //        return "NETWORK INTL"
    //      } else {
    //        return "VAS"
    //      }
    //    }).join("+")

    //  title = `${activityType} - ${customerName}`
    //}

    //if (this.showMDS && this.showNetwork) {
    //  let customerName = this.form.get("network.customerName").value != null ? this.form.get("network.customerName").value : ""
    //  let activityTypeId = this.activityType.value != null ? this.activityType.value as any[] : []
    //  let activityType = activityTypeId
    //    .sort((a, b) => { return a - b })
    //    .map(a => {
    //      return this.activityTypeList.find(b => b.ntwkActyTypeId == a).ntwkActyTypeDes
    //      //if (a == 1) {
    //      //  return "MDS"
    //      //} else if (a == 2) {
    //      //  return "NETWORK"
    //      //} else {
    //      //  return "VAS"
    //      //}
    //    }).join("+")

    //  let cdAddress = address.filter(a => !this.helper.isEmpty(a)).join(",")
    //  let mdsActyTypeDes = mdsActyTypeId != null ? this.mdsActivityTypeList.find(a => a.mdsActyTypeId == mdsActyTypeId).mdsActyTypeDes : ""
    //  mdsActyTypeDes = (cdAddress.length > 0) ? `${cdAddress} -- ${mdsActyTypeDes}` : mdsActyTypeDes

    //  title = `${activityType} - ${customerName} - ${mdsActyTypeDes}`
    //}

    return title
  }

  getNetworkCustomer() {
    return this.networkCustomerList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.mdsEventNtwkCustId = isNaN(+a.mdsEventNtwkCustId) ? 0 : a.mdsEventNtwkCustId
      a.assocH6 = a.assocH6 || ""
      a.instlSitePocNme = a.instlSitePocNme || ""
      a.roleNme = a.roleNme || ""
      a.instlSitePocEmail = a.instlSitePocEmail || ""
      a.instlSitePocPhn = a.instlSitePocPhn || ""

      return a
    })
  }

  getNetworkTransport() {
    return this.networkTransportList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.mdsEventNtwkTrptId = isNaN(+a.mdsEventNtwkTrptId) ? 0 : a.mdsEventNtwkTrptId
      a.mdsTrnsprtType = a.mdsTrnsprtType != null ? a.mdsTrnsprtType : ""
      a.ddAprvlNbr = a.ddAprvlNbr || ""
      //a.salsEngrPhn = a.salsEngrPhn || ""
      //a.salsEngrEmail = a.salsEngrEmail || ""
      a.bdwdNme = a.bdwdNme || ""
      a.locCity = a.locCity || ""
      a.locCtry = a.locCtry || ""
      a.locSttPrvn = a.locSttPrvn || ""
      a.creatDt = a.creatDt || new Date()
      a.recStusId = 1

      return a
    })
  }

  getCpeDeviceList() {
    this.deletedCpeDeviceList.forEach(a =>
      this.cpeDeviceList.push(a)
    );

    return this.cpeDeviceList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.eventCpeDevId = isNaN(+a.eventCpeDevId) ? 0 : a.eventCpeDevId
      a.creatDt = a.creatDt || new Date()

      return a
    })
  }

  getMdsRedesignDevInfoList() {
    return this.mdsRedesignDevInfoList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.mdsEventOdieDevId = isNaN(+a.mdsEventOdieDevId) ? 0 : a.mdsEventOdieDevId
      a.creatDt = a.creatDt || new Date()

      return a
    })
  }

  getDevCompletionList() {
    return this.devCompletionList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.eventDevCmpltId = isNaN(+a.eventDevCmpltId) ? 0 : a.eventDevCmpltId
      a.creatDt = a.creatDt || new Date()

      return a
    })
  }

  getMnsOrderList() {
    return this.mnsOrderList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.eventDevSrvcMgmtId = isNaN(+a.eventDevSrvcMgmtId) ? 0 : a.eventDevSrvcMgmtId
      a.creatDt = a.creatDt || new Date()
      a.recStusId = 1

      return a
    })
  }

  getSiteServiceList() {
    return this.siteServiceList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.mdsEventSiteSrvcId = isNaN(+a.mdsEventSiteSrvcId) ? 0 : a.mdsEventSiteSrvcId
      a.odieDevNme = a.odieDevNme != null ? a.odieDevNme : ""
      a.creatDt = a.creatDt || new Date()

      return a
    })
  }

  getThirdPartyList() {
    return this.thirdPartyList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.dslSbicCustTrptId = isNaN(+a.dslSbicCustTrptId) ? 0 : a.dslSbicCustTrptId
      a.odieDevNme = a.odieDevNme != null ? a.odieDevNme : ""
      a.creatDt = a.creatDt || new Date()
      // a.isWirelessCd = a.isWirelessCd? "Y" : "N"
      return a
    })
  }

  getWiredTransportList() {
    return this.wiredTransportList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.slnkWiredTrptId = isNaN(+a.slnkWiredTrptId) ? 0 : a.slnkWiredTrptId
      a.mdsTrnsprtType = a.mdsTrnsprtType || ""
      a.odieDevNme = a.odieDevNme != null ? a.odieDevNme : ""
      a.h6 = a.h6 || ""
      a.creatDt = a.creatDt || new Date()
      a.readyBeginCd = a.readyBeginCdBoolean ? "Y" : "" //"N"
      a.optInCktCd = a.optInCktCdBoolean ? "Y" : "" //"N"
      a.optInHCd = a.optInHCdBoolean ? "Y" : "" //"N"

      return a
    })
  }

  getWirelessTransportList() {
    return this.wirelessTransportList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.mdsEventWrlsTrptId = isNaN(+a.mdsEventWrlsTrptId) ? 0 : a.mdsEventWrlsTrptId
      a.odieDevNme = a.odieDevNme != null ? a.odieDevNme : ""
      a.esnMacId = a.esnMacId || ""
      a.tabSeqNbr = a.tabSeqNbr || 0
      a.creatDt = a.creatDt || new Date()

      return a
    })
  }

  getPortBandwidthList() {
    return this.portBandwidthList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.portBndwdId = isNaN(+a.portBndwdId) ? 0 : a.portBndwdId
      a.odieDevNme = a.odieDevNme != null ? a.odieDevNme : ""
      a.creatDt = a.creatDt || new Date()

      return a
    })
  }

  getEventDiscoDevList() {
    this.eventDiscoDevListFinal = this.eventDiscoDevList.map(a => {
      a.eventId = this.id > 0 ? this.id : 0
      a.eventDiscoDevId = isNaN(+a.eventDiscoDevId) ? 0 : a.eventDiscoDevId
      a.readyBeginCd = a.readyBegin ? "Y" : "" //"N"
      a.optInHCd = a.optInH ? "Y" : "" //"N"
      a.optInCktCd = a.optInCkt ? "Y" : "" //"N"
      a.creatDt = a.creatDt || new Date()
      return a
    })
    this.clearAssignee();
  }

  clearAssignee() {
    this.form.get("schedule.displayedAssignedTo").setValue('');
    this.form.get("schedule.assignedToId").setValue('');
    this.form.get("schedule.assignedTo").setValue('');
    this.dblBookingMessage = '';
  }
  getSelectedActivators(wrkflwStusId: number) {
    //if (wrkflwStusId != WORKFLOW_STATUS.Reject && wrkflwStusId != WORKFLOW_STATUS.Reschedule) {
    if (!this.helper.isEmpty(this.form.get("schedule.assignedToId").value)) {
      let adActivators = this.form.get("schedule.assignedToId").value
      if (adActivators != null) {
        let activators = adActivators.toString()
        if (activators.length > 0) {
          if (activators.indexOf(',') != -1) {
            return activators.toString().split(",").map(a => parseInt(a));
          } else {
            return [adActivators];
          }
        }
      }
    }
    //}

    return [];
  }

  // FORM VALIDATION
  validateFormData() {
    return new Promise(resolve => {
      this.errors = [];
      this.option.validationSummaryOptions.title = "Validation Summary";
      this.option.validationSummaryOptions.message = null;

      if (this.workflowStatus.value != WORKFLOW_STATUS.Retract
        && this.workflowStatus.value != WORKFLOW_STATUS.Reschedule
        && this.workflowStatus.value != WORKFLOW_STATUS.Return
        && this.workflowStatus.value != WORKFLOW_STATUS.Reject
        && this.workflowStatus.value != WORKFLOW_STATUS.Delete) {

        if (this.form.get("activityType").invalid) {
          if (this.form.get("activityType").errors.required) {
            this.errors.push("Activity Type is required");
          }

          this.errors = Array.from(new Set(this.errors))
          resolve(this.errors.length == 0 ? true : false)
        }
        else {
          //if (this.isActivator && this.mdsFailCode.invalid) {
          //  if (this.mdsFailCode.errors.required) {
          //    this.errors.push("MDS Fail Code is required");
          //  }
          //}

          // MDS
          if (this.showMDS) {
            if (this.form.get("customerSOW").invalid) {
              if (this.form.get("customerSOW").errors.required) {
                this.errors.push("Customer SOW is required");
              }
            }
            if (this.form.get("shortDescription").invalid && !this.showDisconnect) {
              if (this.form.get("shortDescription").errors.required) {
                this.errors.push("Short Description is required");
              } else if (this.form.get("shortDescription").errors.maxlength) {
                this.errors.push(" Short Description must have at most 2500 characters");
              }
            }
            if (!this.showDisconnect) {
              if (this.showIPMDescription) {
                if (this.form.get("ipmDescription").invalid) {
                  if (this.form.get("ipmDescription").errors.maxlength) {
                    this.errors.push("IPM Description must have at most 500 characters");
                  }
                }
              }
            }
            if (this.form.get("mds.h1").invalid) {
              if (this.form.get("mds.h1").errors.required) {
                this.errors.push("H1 is required");
              } else if (this.form.get("mds.h1").errors.pattern) {
                this.errors.push("H1 must be 9-digit number");
              } else if (this.form.get("mds.h1").errors.notMatch) {
                this.errors.push("H1 values should all match");
              }
            }
            if ((this.showNetwork) && (this.showMDS)) {
              if (this.form.get("mds.h1").value != this.form.get("network.h1").value)
                this.errors.push("Please make sure both MDS and Network H1 values match up");
            }
            if (this.form.get("mds.odieCustomerName").invalid) {
              if (this.form.get("mds.odieCustomerName").errors.required) {
                this.errors.push("ODIE Customer Name is required");
              }
            }
            //if (this.form.get("mds.teamPDL").invalid) {
            //  if (this.form.get("mds.teamPDL").errors.required) {
            //    this.errors.push("Team PDL is required");
            //  }
            //}
            if (this.form.get("mds.mdsActivityType").invalid) {
              if (this.form.get("mds.mdsActivityType").errors.required) {
                this.errors.push("MDS Activity Type is required");
              }
            } else {
              if (this.form.get("mds.mdsMacActivity").invalid && !this.showDisconnect) {
                if (this.form.get("mds.mdsMacActivity").errors.required) {
                  this.errors.push("MDS MAC Activity is required");
                } else if (this.form.get("mds.mdsMacActivity").errors.mixed) {
                  this.errors.push("MDS MAC Activity should not be a mix of Generic and Non-Generic type");
                }
              }
            }

            if (this.isWoob && this.form.get("woobIpAddress").invalid) {
              if (this.form.get("woobIpAddress").errors.required) {
                this.errors.push("WOOB IP Address is required");
              }
            }

            if ((([WORKFLOW_STATUS.Submit, WORKFLOW_STATUS.Publish].includes(this.form.get("workflowStatus").value))
              && !this.isReviewer && !this.isActivator)
              && (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1)))) {
              if (!this.form.get("event.isEscalation").value) {
                let gblNowDate = moment(this.dtNow).format('MM/DD/YYYY hh:mm a')
                let gblStrtDt = moment(new Date((this.form.get("schedule.startDate").value))).format('MM/DD/YYYY hh:mm a')
                let gblStDtdiff = moment(gblStrtDt).diff(gblNowDate, 'minutes');
                const errorMessage = "Start Date cannot be in the past";
                if (gblStDtdiff < 0 && !this.errors.includes(errorMessage)) {
                  this.errors.push("Start Date cannot be in the past");
                }
              }
            }

            // DISCONNECT
            if (this.showDisconnect) {
              //console.log("ASD")
              if (!this.validateEventDiscoDev()) {
                this.errors = [...this.errors, ...this.eventDiscoDevErrors]
              }
              if (this.form.get("disconnect.isRequireCoordination").invalid) {
                if (this.form.get("disconnect.isRequireCoordination").errors.required) {
                  this.errors.push("Does this disconnect require handoff and coordination with the customer field is required");
                }
              }
              if (this.form.get("customerSOW").invalid) {
                if (this.form.get("customerSOW").errors.required) {
                  this.errors.push("Customer SOW is required");
                }
              }
              if (this.form.get("disconnect.isTotalDisconnect").invalid) {
                if (this.form.get("disconnect.isTotalDisconnect").errors.required) {
                  this.errors.push("Is this a total customer disconnect field is required");
                }
              } else {
                if (this.form.get("disconnect.isTotalDisconnect").value == 0) {
                  if (this.form.get("disconnect.shortDescription").invalid) {
                    if (this.form.get("disconnect.shortDescription").errors.required) {
                      this.errors.push("Disconnect Information Description is required");
                    }
                  }
                } else if (this.form.get("disconnect.isTotalDisconnect").value == 1) {
                  if (this.form.get("disconnect.reasonForTotalDisconnect").invalid) {
                    if (this.form.get("disconnect.reasonForTotalDisconnect").errors.required) {
                      this.errors.push("Reason for total customer disconnect field is required");
                    }
                  }
                }
                if (this.form.get("disconnect.isTotalDisconnect").invalid) {
                  if (this.form.get("disconnect.isTotalDisconnect").errors.required) {
                    this.errors.push("Disconnect Information Reason for total customer disconnect is required field");
                  }
                }
              }

              if (this.form.get("requestedActivityDate").invalid) {
                if (this.form.get("requestedActivityDate").errors.required) {
                  this.errors.push("Requested Activity Date is required");
                }
              } else {
                const hasRequestedDate = (this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))
                const dateSelected = hasRequestedDate ? this.form.get("requestedActivityDate").value : this.form.get("schedule.startDate").value;
                const fieldKey = hasRequestedDate ? "Requested Activity Date" : "Start Date";

                if (this.form.get("workflowStatus").value != WORKFLOW_STATUS.Visible && (this.isReviewer) && this.fastTrackType.value == "At Will") {
                  this.mdsEventService.isSprintHoliday(moment(new Date(dateSelected)).format('MM/DD/YYYY')).subscribe(
                    data => {
                      console.log(`issprinthol ` + data)
                      if (data) {
                        this.errors.push(fieldKey + " cannot be on a T-Mobile Holiday");
                        this.hasDisconnectSprintHolidayError = true;
                      } else {
                        let gblNowDate = moment(this.dtNow).format('MM/DD/YYYY')
                        let gblStrtDt = moment(new Date(dateSelected)).format('MM/DD/YYYY')
                        let gblStDtdiff = moment(gblStrtDt).diff(gblNowDate, 'days');

                        const errorMessage = fieldKey + " cannot be in the past";
                        if (gblStDtdiff < 0 && !this.errors.includes(errorMessage)) { 
                          this.errors.push(errorMessage);
                        }
                      }
                    },
                    error => {
                      console.log(error);
                      //this.spinner.hide();
                    });
                }

              }
            }
            // NON-DISCONNECT
            else {
              if (this.showMDS) {
                //console.log("NonDisc")
                if (!this.validateMdsOdie()) {
                  this.errors = [...this.errors, ...this.mdsOdieErrors]
                }
                if (!this.validateDevCompletion()) {
                  this.errors = [...this.errors, ...this.devCompletionErrors]
                }
                if (!this.validateCpeAndMns()) {
                  this.errors = [...this.errors, ...this.cpeErrors]
                  this.errors = [...this.errors, ...this.mnsErrors]
                }
                if (!this.validateSiteService()) {
                  this.errors = [...this.errors, ...this.siteServiceErrors]
                }
                if (!this.validateThirdParty()) {
                  this.errors = [...this.errors, ...this.thirdPartyErrors]
                }
                if (!this.validateWiredTransport()) {
                  this.errors = [...this.errors, ...this.wiredTransportErrors]
                }
                if (!this.validateWirelessTransport()) {
                  this.errors = [...this.errors, ...this.wirelessTransportErrors]
                }
                if (!this.validatePortBandwidth()) {
                  this.errors = [...this.errors, ...this.portBandwidthErrors]
                }

                //Esc, StartTime validations for MDS
                if ((![WORKFLOW_STATUS.Reject, WORKFLOW_STATUS.Retract, WORKFLOW_STATUS.Return, WORKFLOW_STATUS.Delete].includes(this.form.get("workflowStatus").value))
                  && !this.isReviewer && !this.isActivator) {
                  if (this.form.get("event.isEscalation").value) {
                    let mntNowDate = moment(this.dtNow).format('MM/DD/YYYY hh:mm a')
                    let mntEscSt = moment(new Date((this.form.get("event.primaryRequestDate").value))).format('MM/DD/YYYY hh:mm a')
                    let mntEscDtdiff = moment(mntEscSt).diff(mntNowDate, 'minutes');
                    if (mntEscDtdiff < 0) { this.errors.push("Escalation Primary DateTime cannot be in the past"); }
                  }
                  else {
                    const isRequestedDateVisible = (this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))
                    // Updated by Sarah Sandoval [20220418] - Use only Start Date as field lable in validation
                    //const fieldKey = isRequestedDateVisible ? 'Requested Activity Date' : 'Start Date';
                    const fieldKey = 'Start Date';
                    let mdsStDate = isRequestedDateVisible ? moment(new Date((this.form.get("requestedActivityDate").value))).format('MM/DD/YYYY') : moment(new Date((this.form.get("schedule.startDate").value))).format('MM/DD/YYYY hh:mm a')
                    console.log("isSprintCPE : " + this.isSprintCPE);
                    console.log("mdsStDate : " + mdsStDate);
                    console.log("dt48 : " + this.dt48WithWeekend);
                    console.log("dt72 : " + this.dt72);

                    if (!this.isSprintCPE) {
                      console.log(this.isGenericH1.value);
                      console.log(this.fastTrackType.value);
                      let dontIncludeWeekend = (this.isGenericH1.value && (this.fastTrackType.value == 'At Will'));
                      let dt48hrVal =  dontIncludeWeekend ? this.dt48: this.dt48WithWeekend;
                      let mntDt48StDate = moment(dt48hrVal).format('MM/DD/YYYY hh:mm a')
                      let mnt48Dtdiff = moment(mdsStDate).diff(mntDt48StDate, 'minutes');
                      console.log("mntDt48StDate : " + mntDt48StDate);
                      console.log("mnt48Dtdiff : " + mnt48Dtdiff);
                      if (mnt48Dtdiff < 0 && !this.showPreStaging) { 
                        let message = dontIncludeWeekend ? 'must be greater than 48 hours and cannot be a weekend' : 'must be greater than 48 hours'
                        this.errors.push(`${fieldKey} ${message}`); 
                      }  
                      // else if (this.helper.checkDateIfWeekend(mntDt48StDate)) { 
                      //   this.errors.push(`${ fieldKey } cannot be on a weekend`); 
                      // }
                    }
                    else {
                      let mntDt72StDate = moment(this.dt72).format('MM/DD/YYYY hh:mm a')
                      let mnt72Dtdiff = moment(mdsStDate).diff(mntDt72StDate, 'minutes');
                      console.log("mntDt72StDate : " + mntDt72StDate);
                      console.log("mnt72Dtdiff : " + mnt72Dtdiff);
                      if (mnt72Dtdiff < 0) { this.errors.push("Start Date must be greater than 72 hours and cannot be a weekend"); }
                      // Added by Sarah Sandoval [20220418] - Not allow on weekend
                      else if (this.helper.checkDateIfWeekend(mdsStDate)) { this.errors.push('Start Date cannot be on a weekend'); }
                    }
                  }
                }

                // PHYSICAL ADDRESS
                if (this.isPhysicalAddressRequired) {
                  if (this.form.get("site.installSite.poc").invalid) {
                    if (this.form.get("site.installSite.poc").errors.required) {
                      this.errors.push("Install Site POC is required");
                    }
                  }
                  if (this.form.get("site.installSite.phone").invalid) {
                    if (this.form.get("site.installSite.phone").errors.required) {
                      this.errors.push("Install Site Phone is required");
                    }
                  }
                  if (this.form.get("site.serviceAssurance.poc").invalid) {
                    if (this.form.get("site.serviceAssurance.poc").errors.required) {
                      this.errors.push("Service Assurance POC is required");
                    }
                  }
                  if (this.form.get("site.serviceAssurance.phone").invalid) {
                    if (this.form.get("site.serviceAssurance.phone").errors.required) {
                      this.errors.push("Service Assurance Phone is required");
                    }
                  }
                  if (this.form.get("site.address").invalid) {
                    if (this.form.get("site.address").errors.required) {
                      this.errors.push("Address is required");
                    }
                  }
                  if (this.form.get("site.state").invalid) {
                    if (this.form.get("site.state").errors.required) {
                      this.errors.push("State is required");
                    }
                  }
                  if (this.form.get("site.country").invalid) {
                    if (this.form.get("site.country").errors.required) {
                      this.errors.push("Country is required");
                    }
                  }
                  if (this.form.get("site.city").invalid) {
                    if (this.form.get("site.city").errors.required) {
                      this.errors.push("City is required");
                    }
                  }
                  if (this.form.get("site.zip").invalid) {
                    if (this.form.get("site.zip").errors.required) {
                      this.errors.push("ZIP is required");
                    }
                  }
                }

                // CPE DELIVERY OPTION
                if (this.form.get("cpe.cpeDeliveryOption").invalid) {
                  if (this.form.get("cpe.cpeDeliveryOption").errors.required) {
                    this.errors.push("CPE Delivery Option is required");
                  }
                } else {
                  if (this.form.get("cpe.cpeDispatchEmail").invalid) {
                    if (this.form.get("cpe.cpeDispatchEmail").errors.required) {
                      this.errors.push("CPE Dispatch Email Address is required");
                    }
                  }
                  if (this.form.get("cpe.cpeDispatchComment").invalid) {
                    if (this.form.get("cpe.cpeDispatchComment").errors.required) {
                      this.errors.push("CPE Dispatch Comments is required");
                    } else if (this.form.get("cpe.cpeDispatchComment").errors.maxlength) {
                      this.errors.push("CPE Dispatch Comments must have at most 20,000 characters");
                    }
                  }
                }

                // SHIPPING
                if (this.showPreStaging) {
                  // if (this.form.get("workflowStatus").value == WORKFLOW_STATUS.Submit || this.form.get("workflowStatus").value == WORKFLOW_STATUS.Publish) {
                  if (this.form.get("shipping.shipmentCustomerEmail").invalid) {
                    if (this.form.get("shipping.shipmentCustomerEmail").errors.required) {
                      this.errors.push("Shipment Customer Email is required");
                    }
                  }
                  // }

                  if (this.eventStatus.value == EVENT_STATUS.Fulfilling && this.form.get("workflowStatus").value == WORKFLOW_STATUS.Shipped) {
                    if (this.form.get("shipping.shippedDate").value == null && !this.helper.checkStartTime(0, this.form.get("shipping.shippedDate").value, new Date())) {
                      this.errors.push("Shipped Date is required and should be today or in future");
                    }

                    if (this.helper.isReallyEmpty(this.form.get("shipping.shipmentRefNo").value)) {
                      this.errors.push("Shipping Tracking/Ref No. is required");
                    }

                    if (this.helper.isReallyEmpty(this.form.get("shipping.shippedDeviceSerialNumber").value)) {
                      this.errors.push("Shipped Device Serial Number is required");
                    }

                    if (this.helper.isReallyEmpty(this.form.get("shipping.shipmentCarrierName").value)) {
                      this.errors.push("Shipping Carrier Name is required");
                    }
                  }
                }
              }

              ////Conf Bridge
              //if (this.form.get("schedule.withConferenceBridge").value == null) {
              //  this.errors.push("Please select if a MDS Conf. Bridge is Needed");
              //}
            }

            // IS GENERIC
            if (this.form.get("isGenericH1").value) {
              if (this.form.get("requestedActivityDate").invalid) {
                if (this.form.get("requestedActivityDate").errors.required) {
                  this.errors.push("Requested Activity Date is required");
                }
              }
              //// Validate Activators
              //if ([WORKFLOW_STATUS.Submit, WORKFLOW_STATUS.Publish].includes(this.form.get("workflowStatus").value)
              //  && this.getSelectedActivators(this.form.get("workflowStatus").value).length == 0
              //  && !this.form.get("event.isEscalation").value) {
              //  this.errors.push("Assigned Activator is required");
              //}
            }
            // ESCALATION
            else {
              if (this.form.get("event.isEscalation").value) {
                if (!this.isReviewer && !this.isActivator) {
                  let mntNowDate = moment(this.dtNow).format('MM/DD/YYYY hh:mm a')
                  let mntEscSt = moment(new Date((this.form.get("event.primaryRequestDate").value))).format('MM/DD/YYYY hh:mm a')
                  let mntEscDtdiff = moment(mntEscSt).diff(mntNowDate, 'minutes');
                  if (mntEscDtdiff < 0) { this.errors.push("Escalation Primary DateTime cannot be in the past"); }
                }

                if (this.form.get("event.escalationBusinessJustification").invalid) {
                  if (this.form.get("event.escalationBusinessJustification").errors.required) {
                    this.errors.push("Escalation Business Justification is required");
                  }
                }

                const esclRsn = this.form.get("event.escalationReason").value
                //console.log('escalationReason: ' + esclRsn + ' condition: ' + (this.helper.isEmpty(esclRsn) || esclRsn === 0))
                if (this.helper.isEmpty(esclRsn) || esclRsn === 0) {
                  this.errors.push("Escalation Reason is required");
                }
                //if ((![WORKFLOW_STATUS.Reject, WORKFLOW_STATUS.Retract, WORKFLOW_STATUS.Return, WORKFLOW_STATUS.Delete].includes(this.form.get("workflowStatus").value))
                //  && !this.isReviewer && !this.isActivator) {
                //  if (this.form.get("event.isEscalation").value) {
                //    let mntNowDate = moment(this.dtNow).format('MM/DD/YYYY hh:mm a')
                //    let mntEscSt = moment(new Date((this.form.get("event.primaryRequestDate").value))).format('MM/DD/YYYY hh:mm a')
                //    let mntEscDtdiff = moment(mntEscSt).diff(mntNowDate, 'minutes');
                //    if (mntEscDtdiff < 0) { this.errors.push("Escalation Primary DateTime cannot be in the past"); }
                //  }
                //}
                //else if (this.form.get("workflowStatus").value == WORKFLOW_STATUS.Publish) {
                //  if ((date1 > date2) && this.showDisconnect) {
                //    this.errors.push("Activity Date cannot be in the past.");
                //  }
                //  else if ((date1 > date2) && !this.showDisconnect) {
                //    this.errors.push("Start Date cannot be in the past.");
                //  }
                //}
              }
            }
          }


          // NETWORK
          if (this.showNetwork) {
            if (this.form.get("shortDescription").invalid) {
              if (this.form.get("shortDescription").errors.required) {
                this.errors.push("Description is required");
              } else if (this.form.get("shortDescription").errors.maxlength) {
                this.errors.push(" Short Description must have at most 2500 characters");
              }
            }
            if (this.showIPMDescription) {
              if (this.form.get("ipmDescription").invalid) {
                if (this.form.get("ipmDescription").errors.maxlength) {
                  this.errors.push("IPM Description must have at most 500 characters");
                }
              }
            }
            if (this.form.get("network.h6").invalid) {
              if (this.form.get("network.h6").errors.required) {
                this.errors.push(`${this.h6Label} is required`);
              }
              else if ((!this.isCEChng) && (this.form.get("network.h6").errors.pattern)) {
                this.errors.push(`H6 must be 9-digit number`);
              }
            }
            if (this.form.get("network.h1").invalid) {
              if (this.form.get("network.h1").errors.required) {
                this.errors.push("H1 is required");
              } else if (this.form.get("network.h1").errors.pattern) {
                this.errors.push(`H1 must be 9-digit number`);
              } else if (this.form.get("network.h1").errors.notMatch) {
                this.errors.push(`H1 values should all match`);
              }
            }
            if (this.form.get("network.networkActivityType").invalid) {
              if (this.form.get("network.networkActivityType").errors.required) {
                this.errors.push("Network Activity Type is required");
              }
            }
            if (this.form.get("network.customerName").invalid) {
              if (this.form.get("network.customerName").errors.required) {
                this.errors.push("Network Customer Name is required");
              }
            }
            if (this.form.get("network.isRelatedCompassNCR").invalid) {
              if (this.form.get("network.isRelatedCompassNCR").errors.required) {
                this.errors.push("Related Compass NCR is required");
              }
            }
            if (!this.validateCpeAndMns()) {
              this.errors = [...this.errors, ...this.cpeErrors]
              //  this.errors = [...this.errors, ...this.mnsErrors]
            }
            if (!this.validateNetworkTransport()) {
              this.errors = [...this.errors, ...this.networkTransportErrors]
            }
            if (!this.validateNetworkCustomer()) {
              this.errors = [...this.errors, ...this.networkCustomerErrors]
            }
            //iswoob
            if (this.showMDS && !this.showDisconnect && this.isWoob) {
              if (this.form.get("woobIpAddress").invalid) {
                if (this.form.get("woobIpAddress").errors.required) {
                  this.errors.push("WOOB IP Address is required");
                }
              }
            }


            // NID
            if (this.showNID) {
              // PHYSICAL ADDRESS
              //if (this.form.get("site.installSite.poc").invalid) {
              //  if (this.form.get("site.installSite.poc").errors.required) {
              //    this.errors.push("Install Site POC is required");
              //  }
              //}
              //if (this.form.get("site.installSite.phone").invalid) {
              //  if (this.form.get("site.installSite.phone").errors.required) {
              //    this.errors.push("Install Site Phone is required");
              //  }
              //}
              //if (this.form.get("site.serviceAssurance.poc").invalid) {
              //  if (this.form.get("site.serviceAssurance.poc").errors.required) {
              //    this.errors.push("Service Assurance POC is required");
              //  }
              //}
              //if (this.form.get("site.serviceAssurance.phone").invalid) {
              //  if (this.form.get("site.serviceAssurance.phone").errors.required) {
              //    this.errors.push("Service Assurance Phone is required");
              //  }
              //}
              //if (this.form.get("site.address").invalid) {
              //  if (this.form.get("site.address").errors.required) {
              //    this.errors.push("Address is required");
              //  }
              //}
              //if (this.form.get("site.state").invalid) {
              //  if (this.form.get("site.state").errors.required) {
              //    this.errors.push("State is required");
              //  }
              //}
              //if (this.form.get("site.country").invalid) {
              //  if (this.form.get("site.country").errors.required) {
              //    this.errors.push("Country is required");
              //  }
              //}
              //if (this.form.get("site.city").invalid) {
              //  if (this.form.get("site.city").errors.required) {
              //    this.errors.push("City is required");
              //  }
              //}
              //if (this.form.get("site.zip").invalid) {
              //  if (this.form.get("site.zip").errors.required) {
              //    this.errors.push("ZIP is required");
              //  }
              //}

              // CPE DELIVERY OPTION
              if (this.form.get("cpe.cpeDeliveryOption").invalid) {
                if (this.form.get("cpe.cpeDeliveryOption").errors.required) {
                  this.errors.push("CPE Delivery Option is required");
                }
              } else {
                if (this.form.get("cpe.cpeDispatchEmail").invalid) {
                  if (this.form.get("cpe.cpeDispatchEmail").errors.required) {
                    this.errors.push("CPE Dispatch Email Address is required");
                  }
                }
                if (this.form.get("cpe.cpeDispatchComment").invalid) {
                  if (this.form.get("cpe.cpeDispatchComment").errors.required) {
                    this.errors.push("CPE Dispatch Comments is required");
                  } else if (this.form.get("cpe.cpeDispatchComment").errors.maxlength) {
                    this.errors.push("CPE Dispatch Comments must have at most 20,000 characters");
                  }
                }
              }
            }

            // IS GENERIC
            if (this.form.get("isGenericH1").value) {
              if (this.form.get("requestedActivityDate").invalid) {
                if (this.form.get("requestedActivityDate").errors.required) {
                  this.errors.push("Requested Activity Date is required");
                }
              }
            }
            // ESCALATION
            else {
              if (this.form.get("event.isEscalation").value) {
                if (!this.isReviewer && !this.isActivator) {
                  let mntNowDate = moment(this.dtNow).format('MM/DD/YYYY hh:mm a')
                  let mntEscSt = moment(new Date((this.form.get("event.primaryRequestDate").value))).format('MM/DD/YYYY hh:mm a')
                  let mntEscDtdiff = moment(mntEscSt).diff(mntNowDate, 'minutes');
                  if (mntEscDtdiff < 0) { this.errors.push("Escalation Primary DateTime cannot be in the past"); }
                }
                if (this.form.get("event.escalationBusinessJustification").invalid) {
                  if (this.form.get("event.escalationBusinessJustification").errors.required) {
                    this.errors.push("Escalation Business Justification is required");
                  }
                }
                const esclRsn = this.form.get("event.escalationReason").value
                //console.log('escalationReason: ' + esclRsn + ' condition: ' + (this.helper.isEmpty(esclRsn) || esclRsn === 0))
                if (this.helper.isEmpty(esclRsn) || esclRsn === 0) {
                  this.errors.push("Escalation Reason is required");
                }
              }
              else {// Validate Activators
                if ([WORKFLOW_STATUS.Submit, WORKFLOW_STATUS.Publish].includes(this.form.get("workflowStatus").value)
                  && this.getSelectedActivators(this.form.get("workflowStatus").value).length == 0) {
                  this.errors.push("Assigned Activator is required");
                }
              }
            }
          }

          // Added by Sarah Sandoval [20211004] for Contact List Validation
          console.log('Contact List hasEditData(): ' + this.cntctList.dataGrid.instance.hasEditData())
          if (this.cntctList.dataGrid.instance.hasEditData()) {
            this.errors.push("Changes made on Contact List table is not yet saved.");
          }
          if (!this.validateContactList()) {
            this.errors = [...this.errors, ...this.contactListErrors]
          }

          // Added by Sarah Sandoval [20210224]
          // Added H6/CCD validation whenever CPE/MNS Table have values to prevent exception error on UpdatePMFlagToNRM in Repository
          let h6 = this.form.get('site.h6').value
          let ccd = this.form.get('site.ccd').value

          if ((this.helper.isReallyEmpty(h6) || this.helper.isEmpty(ccd))
            && (this.cpeDeviceList.length > 0 || this.mnsOrderList.length > 0)) {
            var regexH1H6 = /^[0-9]{9}$/;
            if (this.helper.isReallyEmpty(h6) || !regexH1H6.test(h6)) {
              this.errors.push("A 9-digit numeric H6 for lookup is required");
            }
            else if (this.helper.isEmpty(ccd)) {
              this.errors.push("A valid CCD for lookup is required");
            }
          }

          ////Check if Start Time falls on a Sprint Holiday
          //if ((![WORKFLOW_STATUS.Reject, WORKFLOW_STATUS.Retract, WORKFLOW_STATUS.Return, WORKFLOW_STATUS.Delete].includes(this.form.get("workflowStatus").value))) {
          //  this.spinner.show();
          //  let HolStDt = (this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1)) ? this.form.get("requestedActivityDate").value : this.form.get("schedule.startDate").value;
          //  //console.log(this.form.get("requestedActivityDate").value);
          //  //console.log(this.form.get("schedule.startDate").value);
          //  console.log(`HolStDt : ` + HolStDt)
          //  this.mdsEventService.isSprintHoliday(moment(new Date(HolStDt)).format('MM/DD/YYYY')).subscribe(
          //    data => {
          //      console.log(`issprinthol ` + data)
          //      if (data) {
          //        this.errors.push("Start Date cannot be on a Sprint Holiday");
          //      }
          //    },
          //    error => {
          //      console.log(error);
          //      //this.spinner.hide();
          //    });
          //  //this.spinner.hide();
          //  //console.log(`out of issprinthol`)
          //}

          if ((![WORKFLOW_STATUS.Delete, WORKFLOW_STATUS.Retract, WORKFLOW_STATUS.Return, WORKFLOW_STATUS.Reject].includes(this.form.get("workflowStatus").value))
            && !this.checkMDSNtwkActy("MDSOnly") && !this.checkMDSNtwkActy("hasMDS") && !this.isReviewer && !this.isActivator
            // Updated by Sarah Sandoval [20220418] - Changed condition to validate 48 hour on generic events
            //&& (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1)))
            && (!(this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))
            && !this.form.get("event.isEscalation").value
          ) {
            //Network 48 and 72 hours validation
            let sNtwkValMsg = "";
            let dtNtwkStDt;

            // Updated condition by removing hasMDS - In OLD COWS, MDS preceeds Network validation with MDS+Network Events
            //if (((!this.checkMDSNtwkActy("hasMDS")) && (!this.showNID)) || (this.isCEChng && (!this.isSprintCPE))) {
            if (!this.showNID || (this.isCEChng && !this.isSprintCPE)) {
              // Updated by Sarah Sandoval [20220418] - Changed 48 hour condition to not allow on weekend
              sNtwkValMsg = "Start Date must be greater than 48 hours and cannot be a weekend"
              dtNtwkStDt = this.dt48;
            } else if (this.isCEOnly || (this.isCEChng && this.isSprintCPE)) {
              sNtwkValMsg = "Start Date must be greater than 72 hours and cannot be a weekend"
              dtNtwkStDt = this.dt72;
            } else {
              sNtwkValMsg = ""
              dtNtwkStDt = this.dtNow;
            }
            // let sNtwkValMsg = (((!this.checkMDSNtwkActy("hasMDS")) && (!this.showNID)) || (this.isCEChng && (!this.isSprintCPE))) ? "Start Date must be greater than 48 hours"
            //   : (((this.isCEOnly || (this.isCEChng && (this.isSprintCPE)))) ? "Start Date must be greater than 72 hours and cannot be a weekend" : "");
            // let dtNtwkStDt = (((!this.checkMDSNtwkActy("hasMDS")) && (!this.showNID)) || (this.isCEChng && (!this.isSprintCPE))) ? this.dt48WithWeekend
            //   : (((this.isCEOnly || (this.isCEChng && (this.isSprintCPE)))) ? this.dt72 : this.dtNow);
            console.log('sNtwkValMsg :' + sNtwkValMsg);
            console.log('dtNtwkStDt :' + dtNtwkStDt);
            //console.log('StDtTime :' + (this.form.get("schedule.startDate").value));
            let mntDtNtwkStDate = moment(dtNtwkStDt).format('MM/DD/YYYY hh:mm a')
            //let mntStDate = moment(new Date((this.form.get("schedule.startDate").value))).format('MM/DD/YYYY hh:mm a')
            // Added by Sarah Sandoval [20220418] - Validation for non-MDS Generic
            let mntStDate = this.isGenericH1.value ? moment(new Date((this.form.get("requestedActivityDate").value))).format('MM/DD/YYYY') : moment(new Date((this.form.get("schedule.startDate").value))).format('MM/DD/YYYY hh:mm a')
            let mntNtwkDtdiff = moment(mntStDate).diff(mntDtNtwkStDate, 'minutes');
            console.log('mntDtNtwkStDate :' + mntDtNtwkStDate);
            console.log('mntStDate :' + mntStDate);
            console.log('mntNtwkDtdiff :' + mntNtwkDtdiff);
            if (mntNtwkDtdiff < 0) { this.errors.push(sNtwkValMsg); }
            // Added by Sarah Sandoval [20220418] - Not allow on weekend
            else if (this.helper.checkDateIfWeekend(mntStDate)) { this.errors.push('Start Date cannot be on a weekend'); }
          }

          let normalOperationStatus = (![WORKFLOW_STATUS.Delete, WORKFLOW_STATUS.Retract, WORKFLOW_STATUS.Return, WORKFLOW_STATUS.Reject].includes(this.form.get("workflowStatus").value));
          if (normalOperationStatus && this.isMember && this.checkMDSNtwkActy("MDSOnly")) {
            if ((this.isGenericH1.value && !this.isSprintCPE) || (this.showMDS && this.showDisconnect)) {

              const isRequestedDateVisible = this.isRequireCoordination.value != 1;
              // Updated by Sarah Sandoval [20220418] - Use only Start Date as field lable in validation
              //const fieldKey = isRequestedDateVisible ? 'Requested Activity Date' : 'Start Date';
              const fieldKey = 'Start Date';
              let mdsStDate = isRequestedDateVisible ? moment(new Date((this.form.get("requestedActivityDate").value))).format('MM/DD/YYYY') : moment(new Date((this.form.get("schedule.startDate").value))).format('MM/DD/YYYY hh:mm a')
  
              console.log(this.isGenericH1.value);
              console.log(this.fastTrackType.value);

              //let dontIncludeWeekend = (this.isGenericH1.value && (this.fastTrackType.value == 'At Will')) || ((this.showMDS && this.showDisconnect) && isRequestedDateVisible);
              // Updated by Sarah Sandoval [20220418] - Disconnect events not allowed on weekend
              let dontIncludeWeekend = (this.isGenericH1.value && (this.fastTrackType.value == 'At Will')) || (this.showMDS && this.showDisconnect);
              let dt48hrVal =  dontIncludeWeekend ? this.dt48: this.dt48WithWeekend;

              let mntDt48StDate = moment(dt48hrVal).format('MM/DD/YYYY hh:mm a')
              let mntStDate = moment(mdsStDate).format('MM/DD/YYYY') 
              let mnt48Dtdiff = moment(mdsStDate).diff(mntDt48StDate, 'minutes');

              if (mnt48Dtdiff < 0) { 
                let message = dontIncludeWeekend ? 'must be greater than 48 hours and cannot be a weekend' : 'must be greater than 48 hours'
                this.errors.push(`${ fieldKey } ${message}`); 
              } 
              else if (this.helper.checkDateIfWeekend(mntStDate) && dontIncludeWeekend) { 
                this.errors.push(`${ fieldKey } cannot be on a weekend`); 
              }
            } 
          } 


          if (this.form.get('cpe.cpeDeliveryOption').value === '') {
            this.errors.push("CPE Delivery Option is required");
          }
          if (this.form.get("comments").invalid) {
            if (this.form.get("comments").errors.maxlength) {
              this.errors.push("Notes must have at most 20,000 characters");
            }
          }


          // if (!(this.isMember()) && !this.isScheduleDateValid ) {
          //   this.errors.push("Pre-Staging Events needs to be scheduled atleast 10 business days in future");
          // }


          // Reset USIntlCd, Activator and throw error if Previous USIntl doesnt match latest one
          if ((this.showMDS) && ((this.isMember || this.isReviewer))) {
            if ((this.form.get("site.country").value == null) || this.form.get("site.country").value.trim().toUpperCase() == "US" || this.form.get("site.country").value.trim().toUpperCase() == "U.S" || this.form.get("site.country").value.trim().toUpperCase() == "USA" ||
              this.form.get("site.country").value.trim().toUpperCase() == "U.S.A" || this.form.get("site.country").value.trim().toUpperCase() == "" || this.form.get("site.country").value.trim().toUpperCase() == "UNITED STATE OF AMERICAN"
              || this.form.get("site.country").value.trim().toUpperCase() == "UNITED STATES" || this.form.get("site.country").value.trim().toUpperCase() == "UNITED STATES OF AMERICA" || this.form.get("site.country").value.trim() == "") {
              if (this.prevUSIntlCd == "I") {
                this.clearAssignee();
                this.form.get("site.us").setValue('D');
                this.prevUSIntlCd = "D";
                this.form.get("site.installSite.phoneCode").setValue("")
                this.form.get("site.installSite.cellphoneCode").setValue("")
                // Updated by Sarah Sandoval [20210303] - Added condition
                //this.errors.push("Assigned Activator cannot be blank");
                if ([WORKFLOW_STATUS.Submit, WORKFLOW_STATUS.Publish].includes(this.workflowStatus.value)
                  && this.getSelectedActivators(this.workflowStatus.value).length == 0) {
                  this.errors.push("Assigned Activator is required");
                }
              }
            }
            else {
              if (this.prevUSIntlCd == "D") {
                this.clearAssignee();
                this.form.get("site.us").setValue('I');
                this.prevUSIntlCd = "I";
                // Updated by Sarah Sandoval [20210303] - Added condition
                //this.errors.push("Assigned Activator cannot be blank");
                if ([WORKFLOW_STATUS.Submit, WORKFLOW_STATUS.Publish].includes(this.workflowStatus.value)
                  && this.getSelectedActivators(this.workflowStatus.value).length == 0) {
                  this.errors.push("Assigned Activator is required");
                }
              }
            }
          }

          if ((!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1)))
            && ((this.workflowStatus.value == WORKFLOW_STATUS.Submit && !this.form.get("event.isEscalation").value && (this.form.get("schedule.assignedToId").value == null || this.form.get("schedule.assignedToId").value == "" || this.form.get("schedule.displayedAssignedTo").value == ""))
              || (this.workflowStatus.value == WORKFLOW_STATUS.Publish && (this.form.get("schedule.assignedToId").value == null || this.form.get("schedule.assignedToId").value == "" || this.form.get("schedule.displayedAssignedTo").value == "")))) {
            this.errors.push("Assigned Activator cannot be blank");
          }
          if (![WORKFLOW_STATUS.Retract, WORKFLOW_STATUS.Delete].includes(this.form.get("workflowStatus").value)) {
            this.spinner.show();
            let mds = this.getFormDetails()
            let assignedToId = mds.activators.toString().split(',').join('|');
            const mdsActivityType = this.form.get('mds.mdsActivityType').value;
            const workflowStatus = this.form.get('workflowStatus').value;
            let startDate = this.form.get('schedule.startDate').value;
            if (startDate != undefined || startDate != '') {
              startDate = new Date(startDate);
            }
            let fieldKey = (this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1)) ? "Requested Installation Date" : "Start Date";
            let HolStDt = (this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1)) ? this.form.get("requestedActivityDate").value : this.form.get("schedule.startDate").value;
            let data = zip(
              this.userService.checkForDoubleBooking(mds.strtTmst, mds.endTmst, assignedToId, this.id),
              this.mdsEventService.isSprintHoliday(moment(new Date(HolStDt)).format('MM/DD/YYYY')),
              this.mdsEventService.calDtwithBusiDays("11")
            )
            data.toPromise().then(
              res => {
                console.log(res)
                if (!isNullOrUndefined(res[0]) && res[0] != "") {
                  if (this.isMember && !this.isReviewer && !this.isActivator
                    && ([WORKFLOW_STATUS.Submit].includes(this.form.get("workflowStatus").value))
                    && !(this.form.get("event.isEscalation").value)
                    // Added Activity Type checking to not include Network Intl
                    //&& ["1", "2", "3"].includes(this.activityType.value) // this.activityType.value is an array
                    && (this.activityType.value || []).filter(a => [1, 2, 3].includes(a)).length > 0) {
                    this.errors.push(res[0]);
                  }
                }
                if (res[1]) {
                  if (![WORKFLOW_STATUS.Reject, WORKFLOW_STATUS.Return].includes(this.form.get("workflowStatus").value)) {
                    if (!this.hasDisconnectSprintHolidayError) {
                      this.errors.push(fieldKey + " cannot be on a T-Mobile Holiday");
                    }
                  }
                }
                if (res[2]) {
                  if (this.isMember && !this.isReviewer && !this.isActivator && !(this.form.get("event.isEscalation").value) && ((mdsActivityType == 4 || mdsActivityType == 5) && (workflowStatus != 3 && workflowStatus != 11))) {
                    var dtArr = (res[2] as string).split("|");
                    res[2] = new Date(dtArr[0]);
                    console.log("res", res[2]);
                    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
                    const date1 = Date.UTC(res[2].getFullYear(), res[2].getMonth(), res[2].getDate());
                    const date2 = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
                    const dateDiff = Math.floor((date2 - date1) / _MS_PER_DAY);
                    if (dateDiff < 0)
                      this.errors.push("Pre-Staging Events needs to be scheduled atleast 10 business days in future");
                  }
                }
                this.errors = Array.from(new Set(this.errors))
                resolve(this.errors.length == 0 ? true : false)
              },
              error => {
                console.log(error);
                resolve(false)
              });

          }

          //  if(this.isMember && ([WORKFLOW_STATUS.Submit].includes(this.form.get("workflowStatus").value)) && !(this.form.get("event.isEscalation").value)){
          //    console.log("member trying to submit an non escalated event check for double booking");

          //        let mds = this.getFormDetails()
          //        let assignedToId = mds.activators.toString().split(',').join('|');
          // this.userService.checkForDoubleBooking(mds.strtTmst, mds.endTmst,assignedToId, this.id).subscribe(res => {
          // if(res.length > 0) {      
          //   this.errors.push(res);
          // }
          // else if(this.dblBookingMessage.length > 0) {      
          //   this.errors.push(this.dblBookingMessage);
          // }

          // })
          //     this.userService.checkForDoubleBooking(mds.strtTmst, mds.endTmst,assignedToId, this.id).toPromise().then(
          //    data => {
          //   console.log(data)
          //   if (data) {
          //     this.errors.push(data);
          //   }
          //   this.errors = Array.from(new Set(this.errors))
          //   resolve(this.errors.length == 0 ? true : false)
          // },
          // error => {
          //   console.log(error);              
          //   resolve(false)
          // });                       
          //  }

          //if (!(!this.showNetwork && ((this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1) || this.isGenericH1.value))) {
          //  if ((this.workflowStatus.value != WORKFLOW_STATUS.Visible && this.form.get("schedule.assignedTo").value == "" || this.form.get("schedule.assignedTo").value == "")
          //    && (!(this.isOnlyMember && this.form.get("event.isEscalation").value))) {
          //    this.errors.push("Assigned Activator cannot be blank");
          //  }
          //}

          if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
            if(this.form.get("schedule.withConferenceBridge").invalid) {
              if (this.form.get("schedule.withConferenceBridge").errors.required) {
                this.errors.push("Please select if a MDS Conf. Bridge is Needed");
              }
            }
            


            if (this.form.get("schedule.onlineMeetingAdr").invalid) {
              if (this.form.get("schedule.onlineMeetingAdr").errors.required) {
                this.errors.push("Please select a valid meeting url");
              }
            }

            // Added to fix an issue for IM6358973 [20211025]
            if (!this.helper.isEmpty(this.form.get("schedule.bridgeNumber").value)) {
              if (this.form.get("schedule.bridgeNumber").value.trim().length > 50) {
                this.errors.push("Bridge Number exceeds maximum of 50 characters");
              }
            }

            if (!this.helper.isEmpty(this.form.get("schedule.bridgePin").value)) {
              if (this.form.get("schedule.bridgePin").value.trim().length > 20) {
                this.errors.push("Bridge Pin exceeds maximum of 20 characters");
              }
            }

            if(this.withConferenceBridge.value == 4) { // Yes Other
              if(this.helper.isEmpty(this.onlineMeetingAdr.value)) {
                if(this.helper.isEmpty(this.form.get("schedule.bridgeNumber").value)) {
                  this.errors.push("Bridge Number is required");
                }
                if(this.helper.isEmpty(this.form.get("schedule.bridgePin").value)) {
                  this.errors.push("Bridge Pin is required");
                }
              }
            }


          }
        }

        if (this.workflowStatus.value == WORKFLOW_STATUS.Complete) {
          let activityTypeCompletionList: number[] = this.form.get("activityCompletion").value || []
          console.log(`CHOICES: ${this.activityTypeCompletionList.length}`)
          console.log(`CHOSEN: ${activityTypeCompletionList.length}`)
          if (activityTypeCompletionList.length <= 0 || this.activityTypeCompletionList.length != activityTypeCompletionList.length) {
            this.errors.push("Activity Completion is required");
          }
        }
      }
      // For Workflow Status: Retract, Reschedule, Return, Reject and Delete
      else {
        //this.errors = [];
        //this.option.validationSummaryOptions.title = "Validation Summary";
        //this.option.validationSummaryOptions.message = null;

        if (!(this.isGenericH1.value || (this.showMDS && this.showDisconnect && this.isRequireCoordination.value != 1))) {
          if(this.form.get("schedule.onlineMeetingAdr").invalid) {
            if (this.form.get("schedule.onlineMeetingAdr").errors.required) {
              this.errors.push("Please select a valid meeting url");
            }
          }
        }

        // Added to fix an issue for IM6358973 [20211025]
        if (!this.helper.isEmpty(this.form.get("schedule.bridgeNumber").value)) {
          if (this.form.get("schedule.bridgeNumber").value.trim().length > 50) {
            this.errors.push("Bridge Number exceeds maximum of 50 characters");
          }
        }

        if (!this.helper.isEmpty(this.form.get("schedule.bridgePin").value)) {
          if (this.form.get("schedule.bridgePin").value.trim().length > 20) {
            this.errors.push("Bridge Pin exceeds maximum of 20 characters");
          }
        }

        if (this.isActivator && this.workflowStatus.value == WORKFLOW_STATUS.Reschedule && this.form.get("mdsFailCode").invalid) {
          if (this.form.get("mdsFailCode").errors.required) {
            this.errors.push("MDS fail code is required");
          }
        }
        console.log(this.isActivator)
        console.log(this.workflowStatus.value)
        console.log(this.form.get("mdsFailCode"))
        console.log(this.errors)

        resolve(true)
      }
    })
  }

  // TABLE VALIDATION
  validateTables() {
    if (this.showMDS && this.showNetwork) {
      return this.validatedMdsTables() && this.validateNetworkTables() && this.validateContactList()
    }

    if (this.showMDS) {
      return this.validatedMdsTables() && this.validateContactList()
    }

    if (this.showNetwork) {
      return this.validateNetworkTables() && this.validateContactList()
    }

    return false
  }

  validateContactList() {
    let table = this.contactList
    let errors: string[] = []
    this.contactListErrors = []

    if (table.filter(a =>
      this.helper.isEmpty(a.hierId) ||
      this.helper.isEmpty(a.hierLvlCd) ||
      this.helper.isEmpty(a.roleId) ||
      this.helper.isEmpty(a.emailAdr) ||
      this.helper.isEmpty(a.emailCdIds))
      .length > 0) {
      errors.push("Please enter the required fields in the Contact List")
    }

    this.contactListErrors = errors

    return errors.length > 0 ? false : true
  }

  validatedMdsTables() {
    if (this.showDisconnect) {
      //console.log(`DISCONNECT TABLE: ${this.validateEventDiscoDev()}`)
      return this.validateEventDiscoDev()
    } else {
      //console.log(`MDS ODIE TABLE: ${this.validateMdsOdie()}`)
      //console.log(`DEVICE COMPLETION TABLE: ${this.validateDevCompletion()}`)
      //console.log(`CPE AND MNS TABLE: ${this.validateCpeAndMns()}`)
      //console.log(`SITE TABLE: ${this.validateSiteService()}`)
      //console.log(`THIRD PARTY TABLE: ${this.validateWiredTransport()}`)
      //console.log(`WIRED TABLE: ${this.validateWiredTransport()}`)
      //console.log(`WIRELESS TABLE: ${this.validateWirelessTransport()}`)
      //console.log(`PORT TABLE: ${this.validatePortBandwidth()}`)
      return (this.validateMdsOdie() &&
        this.validateDevCompletion() &&
        this.validateCpeAndMns() &&
        this.validateSiteService() &&
        this.validateThirdParty() &&
        this.validateWiredTransport() &&
        this.validateWirelessTransport() &&
        this.validatePortBandwidth())
    }
  }

  validateNetworkTables() {
    console.log(`Network Customer TABLE: ${this.validateNetworkCustomer()}`)
    console.log(`Network Transport TABLE: ${this.validateNetworkTransport()}`)
    return this.validateNetworkTransport() && this.validateNetworkCustomer()
  }

  validateEventDiscoDev() {
    let table = this.eventDiscoDevList.map(data => { 
      data.h5H6 = this.helper.removeSpaces(data.h5H6);
      return data;
    })
    let errors: string[] = []
    this.eventDiscoDevErrors = []

    if (table.length == 0) {
      //console.log("validateEventDiscoDev");
      errors.push("Managed Activity Table is required")
    }
    else {
      console.log(table)
      if (table.filter(a =>
        this.helper.isReallyEmpty(a.odieDevNme) ||
        this.helper.isReallyEmpty(a.h5H6) ||
        this.helper.isEmpty(a.manfId) ||
        this.helper.isEmpty(a.devModelId) ||
        this.helper.isEmpty(a.readyBegin) ||
        this.helper.isEmpty(a.optInH) ||
        this.helper.isEmpty(a.optInCkt))
        .length > 0) {
        errors.push("Please enter the required fields in the Managed Activity Table")
      } else if (table.filter(a => a.h5H6.length != 9).length > 0) {
        
        errors.push("Please enter a valid H6 in the Managed Activity Table")
      }

      //if (table.filter(a => !/^\d{9}$/.test(this.helper.ifEmpty(a.h5H6, ""))).length > 0) {
      //  errors.push("H6 should be 9-digit numbers")
      //}
    }

    this.eventDiscoDevErrors = errors

    return errors.length > 0 ? false : true
  }

  validateMdsOdie() {
    let table = this.mdsRedesignDevInfoList
    let errors: string[] = []
    this.mdsOdieErrors = []

    if (table.length == 0) {
      //console.log("validateMdsOdie");
      errors.push("Managed Activity Table is required")
    } else {
      //if (table.filter(a =>
      //  this.helper.isEmpty(a.rdsnNbr) ||
      //  this.helper.isEmpty(a.odieDevNme) ||
      //  this.helper.isEmpty(a.scCd) ||
      //  this.helper.isEmpty(a.manfId) ||
      //  this.helper.isEmpty(a.devModelId) ||
      //  this.helper.isEmpty(a.frwlProdCd) ||
      //  this.helper.isEmpty(a.phnNbr) ||
      //  this.helper.isEmpty(a.woobCd) ||
      //  this.helper.isEmpty(a.srvcAssrnSiteSuppId))
      //  .length > 0) {
      //  errors.push("Please enter the required fields in the Managed Activity Table")
      //}
      if (table.filter(a => this.helper.isReallyEmpty(a.rdsnNbr)).length > 0) {
        errors.push("Redesign Number is required")
      }
      if (table.filter(a => this.helper.isEmpty(a.rdsnExpDt)).length > 0) {
        errors.push("Redesign Expiration Date is required")
      }
      if (table.filter(a => this.helper.isReallyEmpty(a.odieDevNme)).length > 0) {
        errors.push("ODIE Device Name is required")
      }
      if (table.filter(a => this.helper.isEmpty(a.scCd)).length > 0) {
        errors.push("Simple/Complex Flag is required")
      }
      if (table.filter(a => this.helper.isEmpty(a.manfId)).length > 0) {
        errors.push("Vendor is required")
      }
      if (table.filter(a => this.helper.isEmpty(a.devModelId)).length > 0) {
        errors.push("Model is required")
      }
      if (table.filter(a => this.helper.isEmpty(a.frwlProdCd)).length > 0) {
        errors.push("Firewall/Security product is required")
      }
      if (table.filter(a => !/^\d{0,4}$/.test(this.helper.ifEmpty(a.intlCtryCd, ""))).length > 0) {
        errors.push("Intl Cd is not in correct format")
      }
      if (table.filter(a => this.helper.isReallyEmpty(a.phnNbr)).length > 0) {
        errors.push("Phone Number is required")
      } else {
        if (table.filter(a => !/^\d{5,12}$/.test(this.helper.ifEmpty(a.phnNbr, ""))).length > 0) {
          errors.push("Phone Number should not contain dashes, spaces or other special characters. Correct format should be (NNNNNNNNNN)")
        }
      }
      if (table.filter(a => this.helper.isEmpty(a.woobCd)).length > 0) {
        errors.push("WOOB is required")
      }
      // Commented by Sarah Sandoval [20210728] - Upon request from Bonnie Johnson to remove the said column
      //if (table.filter(a => this.helper.isEmpty(a.srvcAssrnSiteSuppId)).length > 0) {
      //  errors.push("Entity Responsible is required")
      //}
      for (let a of table) {
        let hasError = false
        if (this.deviceManfList.filter(b => b.manfId == a.manfId).length == 0) {
          errors.push("Vendor is required")
          a.manfId = null
          hasError = true
        }
        if (this.deviceModelList.filter(b => b.devModelId == a.devModelId).length == 0) {
          errors.push("Model is required")
          a.devModelId = null
          hasError = true
        }

        if (hasError) {
          break
        }
      }
    }

    this.mdsOdieErrors = errors

    return errors.length > 0 ? false : true
  }

  validateDevCompletion() {
    let table = this.devCompletionList
    let errors: string[] = []
    this.devCompletionErrors = []

    if (table.length == 0) {
      errors.push("Device Completion Table is required")
    } else {
      if (table.filter(a =>
        this.helper.isReallyEmpty(a.h6))
        .length > 0) {
        errors.push("Please enter the required fields in the Device Completion Table")
      }

      if (this.workflowStatus.value == 7 && this.userService.isMdsActivator() && table.filter(a => a.cmpltdCd != true).length > 0) {
        errors.push("Please make sure all devices are complete in Device Completion Table")
      }
    }
    this.devCompletionErrors = errors

    return errors.length > 0 ? false : true
  }

  validateCpeAndMns() {
    let cpe = this.cpeDeviceList
    let mns = this.mnsOrderList
    this.cpeErrors = []
    this.mnsErrors = []

    if ((cpe.length == 0 && mns.length == 0) && (this.isCpe || this.isMns)) {
      this.cpeErrors.push("At least one of CPE Device or Device Management Table is required")
      this.mnsErrors.push("At least one of CPE Device or Device Management Table is required")
    } else {
      if (mns.filter(a =>
        this.helper.isEmpty(a.mnsSrvcTierId) ||
        this.helper.isReallyEmpty(a.odieDevNme))
        .length > 0) {
        this.mnsErrors.push("Please enter the required fields in the Device Management Table")
        //this.mnsErrors = errors
      }

      if (cpe.filter(a =>
        this.helper.isReallyEmpty(a.odieDevNme)).length > 0) {
        this.cpeErrors.push("Please enter the required fields in the CPE Equipment Device Information Table")
        //this.cpeErrors = errors;
      } else {
        let MdsCpeVldtn = this.mdsCpeVldtn
        let MdsCpePostPublishValidation = this.mdsCpePostPublishValidation
        let MdsCpeSubmitValidationStatuses = this.mdsCpeSubmitValidationStatuses
        let MdsCpePublishValidationStatuses = this.mdsCpePublishValidationStatuses

        let MdsCpeSubmitValidationStatusesString = MdsCpeSubmitValidationStatuses.join("/")
        let MdsCpePublishValidationStatusesString = MdsCpePublishValidationStatuses.join("/")

        var workflowStatus = `|${this.workflowStatus.value}|`
        console.log(MdsCpePostPublishValidation)
        if (this.workflowStatus.value == 2 || MdsCpePostPublishValidation.indexOf(workflowStatus) >= 0) {
          let statusList = cpe.map(a => a.rcptStus)

          console.log(statusList)

          // Valid if at least one
          let message = (workflowStatus == '|4|') ? 'Published/Post-Published' : 'Submitted' // 4 : Publish | 2 : Submit
          if (MdsCpeVldtn == "1") {
            //console.log("ASD")
            if (this.workflowStatus.value == 2 && !statusList.some(a => MdsCpeSubmitValidationStatuses.includes(a))) {
              //console.log("QWE")
              this.cpeErrors.push(`* Event cannot be Submitted without atleast one of the cpe devices in one of these valid statuses - ${MdsCpeSubmitValidationStatusesString}`)
            } else if (MdsCpePostPublishValidation.indexOf(workflowStatus) >= 0 && !statusList.some(a => MdsCpePublishValidationStatuses.includes(a))) {
              this.cpeErrors.push(`* Event cannot be ${message} without atleast one of the cpe devices in one of these valid statuses - ${MdsCpePublishValidationStatusesString}`)
            }
          }
          // Valid if ALL
          else {
            //console.log(!statusList.every(a => [...MdsCpeSubmitValidationStatuses, "Cancelled"].includes(a)))
            //console.log(!statusList.every(a => [...MdsCpePublishValidationStatuses, "Cancelled"].includes(a)))
            if (this.workflowStatus.value == 2 && !statusList.every(a => [...MdsCpeSubmitValidationStatuses, "Cancelled"].includes(a))) {
              this.cpeErrors.push(`* Event cannot be Submitted without all the cpe devices in one of these valid statuses - ${MdsCpeSubmitValidationStatusesString}`)
            } else if (MdsCpePostPublishValidation.indexOf(workflowStatus) >= 0 && !statusList.every(a => [...MdsCpePublishValidationStatuses, "Cancelled"].includes(a))) {
              
              this.cpeErrors.push(`* Event cannot be ${message} without all the cpe devices in one of these valid statuses - ${MdsCpePublishValidationStatusesString}`)
            }
          }
        }
      }
    }


    if (this.cpeErrors.length > 0 || this.mnsErrors.length > 0) {
      setTimeout(() => {
        window.location.hash = 'cpeInformation';
      }, 100);
    }

    return this.cpeErrors.length > 0 || this.mnsErrors.length > 0 ? false : true
  }

  validateSiteService() {
    let table = this.siteServiceList
    let errors: string[] = []
    this.siteServiceErrors = []

    if (table.filter(a =>
      this.helper.isEmpty(a.srvcTypeId) ||
      this.helper.isReallyEmpty(a.odieDevNme))
      .length > 0) {
      errors.push("Please enter the required fields in the Service Table")
    }

    if (table.filter(a =>
      (a.srvcTypeId == "1" || a.srvcTypeId == "4" || a.srvcTypeId == "6") &&
      !/[a-zA-Z0-9]{3}$/.test(this.helper.ifEmpty(a.mach5SrvcOrdrId, "")))
      .length > 0) {
      errors.push("Please populate the Mach5 Service ID in the Service Table")
    }

    if (table.filter(a =>
      a.srvcTypeId == "4" &&
      (this.helper.isEmpty(a.thrdPartyVndrId) ||
        this.helper.isEmpty(a.thrdPartyId) ||
        this.helper.isEmpty(a.thrdPartySrvcLvlId)))
      .length > 0) {
      errors.push("3rd Party Vendor, 3rd Party ID and 3rd Party Service Level are required when Service Type is SBIC")
    }

    if (table.filter(a =>
      a.srvcTypeId == "4" &&
      a.thrdPartyVndrId == "2" &&
      !/^[a-zA-Z0-9]{11}$/.test(this.helper.ifEmpty(a.thrdPartyId, "")))
      .length > 0) {
      errors.push("3rd Party ID should be 11 alpha-numeric characters when Service Type is SBIC & 3rd Party Vendor Tolt")
    }

    this.siteServiceErrors = errors

    return errors.length > 0 ? false : true
  }

  validateThirdParty() {
    let table = this.thirdPartyList
    let errors: string[] = []
    this.thirdPartyErrors = []

    if (table.filter(a =>
      this.helper.isReallyEmpty(a.mdsTrnsprtType) ||
      this.helper.isReallyEmpty(a.primBkupCd) ||
      this.helper.isReallyEmpty(a.vndrPrvdrTrptCktId) ||
      this.helper.isReallyEmpty(a.vndrPrvdrNme) ||
      this.helper.isReallyEmpty(a.isWirelessCd) ||
      this.helper.isReallyEmpty(a.bwEsnMeid) ||
      this.helper.isReallyEmpty(a.scaNbr) ||
      this.helper.isReallyEmpty(a.ipAdr) ||
      this.helper.isReallyEmpty(a.subnetMaskAdr) ||
      this.helper.isReallyEmpty(a.nxthopGtwyAdr) ||
      this.helper.isReallyEmpty(a.sprintMngdCd) ||
      this.helper.isReallyEmpty(a.odieDevNme))
      .length > 0) {
      errors.push("Please enter the required fields in the DSL/SBIC Table")
    }

    this.thirdPartyErrors = errors

    return errors.length > 0 ? false : true
  }

  validateWiredTransport() {
    let table = this.wiredTransportList
    let errors: string[] = []
    this.wiredTransportErrors = []

    if (table.filter(a =>
      this.helper.isReallyEmpty(a.mdsTrnsprtType) ||
      this.helper.isReallyEmpty(a.primBkupCd) ||
      this.helper.isReallyEmpty(a.vndrPrvdrTrptCktId) ||
      this.helper.isReallyEmpty(a.vndrPrvdrNme) ||
      this.helper.isReallyEmpty(a.bdwdChnlNme) ||
      //this.helper.isEmpty(a.plNbr) ||
      this.helper.isReallyEmpty(a.ipNuaAdr) ||
      //this.helper.isEmpty(a.multiLinkCktCd) ||
      //this.helper.isEmpty(a.dedAccsCd) ||
      //this.helper.isEmpty(a.fmsNbr) ||
      //this.helper.isEmpty(a.dlciVpiVci) ||
      //this.helper.isEmpty(a.vlanNbr) ||
      //this.helper.isEmpty(a.spaNua) ||
      this.helper.isReallyEmpty(a.sprintMngdCd) ||
      this.helper.isReallyEmpty(a.odieDevNme))
      .length > 0) {
      errors.push("Please enter the required fields in the Sprintlink FR/Wired Device Table")
    }

    this.wiredTransportErrors = errors

    return errors.length > 0 ? false : true
  }

  validateWirelessTransport() {
    let table = this.wirelessTransportList
    let errors: string[] = []
    this.wirelessTransportErrors = []

    if (table.filter(a =>
      this.helper.isReallyEmpty(a.wrlsTypeCd) ||
      this.helper.isReallyEmpty(a.primBkupCd) ||
      this.helper.isReallyEmpty(a.esnMacId) ||
      this.helper.isReallyEmpty(a.odieDevNme))
      .length > 0) {
      errors.push("Please enter the required fields in the Wireless Transport Table")
    } else {
      if (table.filter(a =>
        a.wrlsTypeCd == "4G" && (
          this.helper.isReallyEmpty(a.billAcctNme) ||
          this.helper.isReallyEmpty(a.billAcctNbr) ||
          this.helper.isReallyEmpty(a.acctPin) ||
          this.helper.isReallyEmpty(a.salsCd) ||
          this.helper.isReallyEmpty(a.soc) ||
          this.helper.isReallyEmpty(a.staticIpAdr) ||
          this.helper.isReallyEmpty(a.imei) ||
          this.helper.isReallyEmpty(a.uiccIdNbr) ||
          this.helper.isReallyEmpty(a.pricePln)))
        .length > 0) {
        errors.push("Please enter the required fields in the Wireless Transport Table")
      }
    }

    this.wirelessTransportErrors = errors

    return errors.length > 0 ? false : true
  }

  validatePortBandwidth() {
    let table = this.portBandwidthList
    let errors: string[] = []
    this.portBandwidthErrors = []

    if (table.filter(a =>
      this.helper.isReallyEmpty(a.odieDevNme))
      .length > 0) {
      errors.push("Please enter the required fields in the Port Bandwidth Table")
    }

    this.portBandwidthErrors = errors

    return errors.length > 0 ? false : true
  }

  validateNetworkTransport() {
    let table = this.networkTransportList
    let errors: string[] = []
    this.networkTransportErrors = []

    if (table.length == 0) {
      errors.push("Network Transport/VAS is required")
    }

    table.forEach((row, index) => {
      if (this.helper.isReallyEmpty(row.assocH6)) {
        errors.push("H6 is required")
      } else if (row.assocH6.length != 9) {
        errors.push("H6 must be 9-digit number")
      }
      // if (this.helper.isReallyEmpty(row.locCity)) {
      //   errors.push("Location City is required")
      // }
      // if (this.helper.isReallyEmpty(row.locSttPrvn)) {
      //   errors.push("Location State is required")
      // }
      if (this.helper.isReallyEmpty(row.locCtry)) {
        errors.push("Location Country is required")
      }
      if (this.helper.isReallyEmpty(row.mdsTrnsprtType)) {
        errors.push("Transport Type is required")
      }
      if (this.helper.isReallyEmpty(row.ddAprvlNbr)) {
        errors.push("Design Doc # is required")
      }
      //if (this.helper.isEmpty(row.ipVer)) {
      //  errors.push("IP version is required")
      //}
      if (this.helper.isReallyEmpty(row.mach5OrdrNbr)) {
        errors.push("M5 Order # is required")
      }
      if (this.helper.isReallyEmpty(row.ipNuaAdr)) {
        errors.push("NUA is required")
      }
      if (this.showVas && this.helper.isReallyEmpty(row.vasType)) {
        errors.push("Please select a valid VAS SubType in Network Transport Table")
      }

      if (this.isCEOnly) {
        if (this.helper.isReallyEmpty(row.nidSerialNbr)) {
          errors.push("Please enter a valid NID S/N in Network Transport Table")
        }
        // COMMENT THIS FOR TESTING
        if (this.helper.isReallyEmpty(row.nidIpAdr)) {
          // errors.push("Please enter a valid NID IP in Network Transport Table")
        }
        if (this.helper.isReallyEmpty(row.nidHostName)) {
          // errors.push("Please enter a valid NID Hostname in Network Transport Table")
        }
      }
    })

    this.networkTransportErrors = Array.from(new Set(errors))

    return errors.length > 0 ? false : true
  }

  validateNetworkCustomer() {
    let table = this.networkCustomerList
    let errors: string[] = []
    this.networkCustomerErrors = []

    if (table.length == 0) {
      errors.push("Network Customer is required")
    }

    this.networkCustomerErrors = errors

    return errors.length > 0 ? false : true
  }

  openNewTab(eventId: number) {
    const baseUrl = window.origin;
    if (window.opener && window.opener !== window) {
      // you are in a popup
      // this.router.navigate([`/event/mds/${eventId}`]);
      // window.location = `${baseUrl}/event/mds/${eventId}`;
      location.replace(`${baseUrl}/event/mds/${eventId}`);
      // this.router.navigateByUrl(`${baseUrl}/event/mds/${eventId}`);
    }
    else {
      window.open(`${baseUrl}/event/mds/${eventId}`, '_blank', 'toolbar=no,scrollbars=no,resizable=no,top=100,left=500,width=600,height=400');
    }
  }
  // TABLE VALIDATION

  public isShowCheckCurrentslot(): boolean {
    // if(!this.userService.hasMultipleRoleInGroup("MDS")) {
    return !(this.userService.isMdsActivator() && [WORKFLOW_STATUS.Publish, WORKFLOW_STATUS.InProgress, WORKFLOW_STATUS.Fulfilling, WORKFLOW_STATUS.Shipped].includes(this.form.get("workflowStatus").value))
    // } 

    // return this.option.schedule.checkCurrentSlot.isShown;    
  }

  public openUrl() {
    const url = this.form.get('mds.customerSOW').value;
    window.open(url, 'popUpWindow', 'height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
  }



  dropAssignee() {
    this.form.get("schedule.displayedAssignedTo").setValue('');
    this.form.get("schedule.assignedToId").setValue('');
    this.form.get("schedule.assignedTo").setValue('');
    this.dblBookingMessage = '';
  }

  handleWhiteSpaces(event: any, key: string) {
    let newData = event.currentTarget.value.trim();

    if(newData.length > 9) {
      newData = newData.slice(0,9);
    }
    event.currentTarget.value = newData
    this.form.get(key).setValue(newData);
  }

  selectDesignDocFirstRecord() {

    const dgDesignDocDetailInstance = this.dgDesignDocDetail.instance
    const selection = dgDesignDocDetailInstance.getSelectedRowKeys();

    if (selection.length == 0) {
      dgDesignDocDetailInstance.selectRowsByIndexes([0]);
    }


    if (this.isContentReady) {
      dgDesignDocDetailInstance.refresh();
      this.isContentReady = false;
    }

  }

  onDgNetworkTransportContentReady(e) {
    if (this.checkMDSNtwkActy("hasNtwk") || this.checkMDSNtwkActy("hasVAS")) {
      if (this.prevNTVTableCnt != this.networkTransportList.length) {
        this.clearAssignee();
      }
      this.showNIDData();
    }
  }

  customerSOWChange(e: any) {
    // For validation purposes only
    const value = e.currentTarget.value;
    this.form.get("customerSOW").setValue(value);
  }

  onTransportVASTableInserting(e) {
    e.eventId = this.id > 0 ? this.id : 0
    e.mdsEventNtwkTrptId = isNaN(+e.mdsEventNtwkTrptId) ? 0 : e.mdsEventNtwkTrptId
    e.mdsTrnsprtType = e.mdsTrnsprtType != null ? e.mdsTrnsprtType : ""
    e.ddAprvlNbr = e.ddAprvlNbr || ""
    //a.salsEngrPhn = a.salsEngrPhn || ""
    //a.salsEngrEmail = a.salsEngrEmail || ""
    e.data.assocH6 = this.helper.removeSpaces(e.data.assocH6)
    e.bdwdNme = e.bdwdNme || ""
    e.locCity = e.locCity || ""
    e.locCtry = e.locCtry || ""
    e.locSttPrvn = e.locSttPrvn || ""
    e.creatDt = e.creatDt || new Date()
  }


  // onManageActivityTableNewRow(e) {
  //   if (this.mdsRedesignDevInfoList != null) {
  //     e.data.odieDevNme = this.mdsRedesignDevInfoList.length == 1 ? this.mdsRedesignDevInfoList[0].odieDevNme : null
  //   }
  //   e.data.readyBeginCdBoolean = false
  //   e.data.optInHCdBoolean = false
  //   e.data.optInCktCdBoolean = false
  //   e.data.isFromLookup = false
  //   e.data.isNew = true
  //   console.log(e.data)
  // }

  onManageActivityTableDelete(e) {

    const index = e.row.rowIndex;
    this.eventDiscoDevList.splice(index, 1);

    this.dropAssignee();

  }

  getMdsOdieDeviceNameList() {
    this.mdsOdieDeviceNameList = this.mdsRedesignDevInfoList.map(a => {
      return {
        deviceName: a.odieDevNme,
        value: a.odieDevNme
      }
    }) || []
    this.mdsOdieDeviceNameList = [...this.mdsOdieDeviceNameList, { deviceName: "Do Not Send to ODIE", value: "0" }]
    console.log(this.mdsOdieDeviceNameList)

    // Reset child tables
    if (this.mdsOdieDeviceNameList.length == 1) {
      this.cpeDeviceList.map(a => { a.odieDevNme = "0" })
      this.mnsOrderList.map(a => { a.odieDevNme = "0" })
      this.siteServiceList.map(a => { a.odieDevNme = "0" })
      this.thirdPartyList.map(a => { a.odieDevNme = "0" })
      this.wiredTransportList.map(a => { a.odieDevNme = "0" })
      this.wirelessTransportList.map(a => { a.odieDevNme = "0" })
      this.portBandwidthList.map(a => { a.odieDevNme = "0" })
    } else if (this.mdsOdieDeviceNameList.length == 2) {
      this.cpeDeviceList.map(a => { a.odieDevNme = a.odieDevNme == "0" ? a.odieDevNme : this.mdsOdieDeviceNameList[0].value })
      this.mnsOrderList.map(a => { a.odieDevNme = a.odieDevNme == "0" ? a.odieDevNme : this.mdsOdieDeviceNameList[0].value })
      this.siteServiceList.map(a => { a.odieDevNme = a.odieDevNme == "0" ? a.odieDevNme : this.mdsOdieDeviceNameList[0].value })
      this.thirdPartyList.map(a => { a.odieDevNme = a.odieDevNme == "0" ? a.odieDevNme : this.mdsOdieDeviceNameList[0].value })
      this.wiredTransportList.map(a => { a.odieDevNme = a.odieDevNme == "0" ? a.odieDevNme : this.mdsOdieDeviceNameList[0].value })
      this.wirelessTransportList.map(a => { a.odieDevNme = a.odieDevNme == "0" ? a.odieDevNme : this.mdsOdieDeviceNameList[0].value })
      this.portBandwidthList.map(a => { a.odieDevNme = a.odieDevNme == "0" ? a.odieDevNme : this.mdsOdieDeviceNameList[0].value })
    } else {
      this.cpeDeviceList.map(a => { a.odieDevNme = (this.mdsOdieDeviceNameList.findIndex(b => b.value == a.odieDevNme) == -1) ? "" : a.odieDevNme })
      this.mnsOrderList.map(a => { a.odieDevNme = (this.mdsOdieDeviceNameList.findIndex(b => b.value == a.odieDevNme) == -1) ? "" : a.odieDevNme })
      this.siteServiceList.map(a => { a.odieDevNme = (this.mdsOdieDeviceNameList.findIndex(b => b.value == a.odieDevNme) == -1) ? "" : a.odieDevNme })
      this.thirdPartyList.map(a => { a.odieDevNme = (this.mdsOdieDeviceNameList.findIndex(b => b.value == a.odieDevNme) == -1) ? "" : a.odieDevNme })
      this.wiredTransportList.map(a => { a.odieDevNme = (this.mdsOdieDeviceNameList.findIndex(b => b.value == a.odieDevNme) == -1) ? "" : a.odieDevNme })
      this.wirelessTransportList.map(a => { a.odieDevNme = (this.mdsOdieDeviceNameList.findIndex(b => b.value == a.odieDevNme) == -1) ? "" : a.odieDevNme })
      this.portBandwidthList.map(a => { a.odieDevNme = (this.mdsOdieDeviceNameList.findIndex(b => b.value == a.odieDevNme) == -1) ? "" : a.odieDevNme })
    }
  }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }


  //onDeviceCompletionEditingStart(e) {
  //  console.log(e)
  //  if (e.parentType == "dataRow" && e.dataField == "cmpltdCd") {
  //    e.cancel = !this.isActivator || (this.isActivator && e.row.data.isAlreadyChecked)
  //  }
  //}

  onDeviceCompletionRowUpdating(e) {
    console.log(e)
    //console.log(this.isActivator)
    if (this.isActivator && e.oldData.isAlreadyCompleted) {
      console.log(e.newData.cmpltdCd)
      e.newData.cmpltdCd = true
      e.oldData.cmpltdCd = true
      console.log(e.newData.cmpltdCd)
      this.helper.notify("Completed device cannot be unchecked", Global.NOTIFY_TYPE_WARNING)
    }
  }

  onVendorUpdate(e) {
    e.data.h5H6 = this.helper.removeSpaces(e.data.h5H6);
  }

  onContactDetailSaved(result) {
    console.log(new Date)
    console.log('onContactDetailSaved')
    console.log(result)
    this.contactList = result;
    console.log(new Date)
  }
}
