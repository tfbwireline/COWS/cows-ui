import { Component, OnInit, AfterViewInit } from '@angular/core';
import { EEventType } from "./../../../shared/global";
import { EventViewsService } from "./../../../services/";
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerAsync } from '../../../shared';

@Component({
  selector: 'app-mds',
  templateUrl: './mds.component.html'
})
export class MdsComponent implements OnInit, AfterViewInit {

  loggedInUser = <any>[];
  eventViewsList = <any>[];
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;
  spnr: SpinnerAsync;

  constructor(private eventService: EventViewsService, private spinner: NgxSpinnerService) { 
    this.spnr = new SpinnerAsync(spinner)
  }

  ngOnInit() {
    this.eventType = EEventType.MDS;
    this.eventJobAid = "IPSD Event Job Aid for IPMs/Members";
    this.eventJobAidUrl = "http://webcontoo.corp.sprint.com/webcontoo/llisapi.dll/fetch/2000/1835597/1836298/14404/106652/106324/107866/106767/6074490/0069792_Domestic_and_International_MNS_and_MSS_Events_in_Comprehensive_Order_Workflow_System_COWS___(2).pdf?nodeid=6460971";
  }

  ngAfterViewInit() {
    
  }

}
