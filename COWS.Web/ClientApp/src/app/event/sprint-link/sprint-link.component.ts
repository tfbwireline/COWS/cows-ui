import { Component, OnInit } from '@angular/core';
import { EEventType } from "./../../../shared/global";
import { EventViewsService } from "./../../../services/";

@Component({
  selector: 'app-sprint-link',
  templateUrl: './sprint-link.component.html'
})
export class SprintLinkComponent implements OnInit {

  loggedInUser = <any>[];
  eventViewsList = <any>[];
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;

  constructor(private eventService: EventViewsService) { }

  ngOnInit() {
    this.eventType = EEventType.SprintLink;
    this.eventJobAid = "SprintLink Event Job Aid for IPMs/Members";
    this.eventJobAidUrl = "http://webcontoo.corp.sprint.com/webcontoo/llisapi.dll/fetch/2000/1835597/1836298/14404/106652/106324/107866/106767/6074490/0069796_Sprintlink_Events_in_Comprehensive_Order_Workflow_System_COWS__%282%29.pdf?nodeid=6450694&vernum=-2";
  }
}

