import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, AbstractControl } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from "rxjs";
import { Helper } from "./../../../shared";
import { EVENT_STATUS, KeyValuePair, EEventType, RB_CONDITION, Global, WORKFLOW_STATUS, EEventProfile } from "./../../../shared/global";
import {
  UserService, ActivityLocaleService, EventLockService, SplkEventService,
  VpnPlatformTypeService, VasTypeService, IPVersionService, MultiVrfReqService,
  VendorService, WholesalePartnerService, WorkflowService, EventAsnToUserService
} from "./../../../services/";
import { User, FormControlItem, SplkEventAccessTag, SplkEvent, Workflow, EventAsnToUser, UserProfile } from "./../../../models";
import { CustomValidator } from '../../../shared/validator/custom-validator';

@Component({
  selector: 'app-sprint-link-form',
  templateUrl: './sprint-link-form.component.html'
})
export class SprintLinkFormComponent implements OnInit {
  id: number = 0
  form: FormGroup;
  isSubmitted: boolean = false;
  isRevActTask: boolean = false;
  option: any;
  splkEventTypeId: any;
  splkEvent: SplkEvent = null;
  eventTypeId: number = EEventType.SprintLink;
  relatedCompassList: KeyValuePair[];
  eventStatusList: KeyValuePair[];
  profile: string = '';
  ipVersionList: FormControlItem[];
  isLocked: boolean = false;
  splkActivityTypeList: KeyValuePair[];
  splkEventTypeList: KeyValuePair[];
  showEditButton: boolean = false;
  ActivatorEditButton: boolean = false;
  splkEventAccessTagList: SplkEventAccessTag[] = [];
  workflowStatusList: Workflow[] = [];
  activators: EventAsnToUser[];
  eventNote: string = '';
  lockedByUser: string;
  eventType: number = EEventType.SprintLink
  isActivator: boolean = false;
  isReviewer: boolean = false;
  isMember: boolean = false;
  allowEdit: boolean = true;

  public eventSucssActyListsArray: AbstractControl[];
  public eventSucssActyList: KeyValuePair[];
  public MyeventSucssActy: any[];
  public eventFailActyListsArray: AbstractControl[];
  public eventFailActyList: KeyValuePair[];

  // For quick accessing control
  get h1() { return this.form.get("h1") }
  get publishedEmailCC() { return this.form.get("design.publishedEmailCC") }
  get completeEmailCC() { return this.form.get("design.completeEmailCC") }
  get mdsManaged() { return this.form.get("event.mdsManaged") }
  get comments() { return this.form.get("event.comments") }
  get splkEventType() { return this.form.get("event.splkEventType") }
  get splkActivityType() { return this.form.get("event.splkActivityType") }
  get ipVersion() { return this.form.get("event.ipVersion") }
  get relatedCompassNcr() { return this.form.get("event.relatedCompassNcr") }

  constructor(private avRoute: ActivatedRoute, private router: Router, private helper: Helper,
    private spinner: NgxSpinnerService, private fb: FormBuilder, private userSrvc: UserService,
    private splkEvntSrvc: SplkEventService, private evntLockSrvc: EventLockService,
    private SplkEventService: SplkEventService, private multiVrfSrvc: MultiVrfReqService,
    private ipVersionSrvc: IPVersionService, private vpnPltfrmTypeSrvc: VpnPlatformTypeService,
    private vndrSrvc: VendorService, private workflowSrvc: WorkflowService,
    private evntAsnToUserSrvc: EventAsnToUserService) { }

  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"] || 0;
    //this.showEditButton = this.id > 0 ? true : false;
    //this.ActivatorEditButton = this.id > 0 ? true : false;
    this.form = new FormGroup({
      itemTitle: new FormControl({ value: null, disabled: true }),
      eventDescription: new FormControl(),
      eventID: new FormControl({ value: null, disabled: true }),
      eventStatus: new FormControl({ value: EVENT_STATUS.Visible, disabled: true }),
      m5_no: new FormControl(),
      charsID: new FormControl(),
      h1: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(9)])),
      h6: new FormControl(),
      workflowStatus: new FormControl({ value: 2, disabled: this.showEditButton }),
      customer: new FormGroup({
        name: new FormControl(),
        email: new FormControl(),
        phone: new FormControl(),
        cellphone: new FormControl(),
        contactName: new FormControl(),
        pager: new FormControl(),
        pagerPin: new FormControl()
      }),
      requestor: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        cellphone: new FormControl(),
        email: new FormControl(),
        pager: new FormControl(),
        pagerPin: new FormControl(),
        bridgeNumber: new FormControl(),
        bridgePin: new FormControl(),
        eventComments: new FormControl()
      }),
      sales: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        email: new FormControl()
      }),
      design: new FormGroup({
        publishedEmailCC: new FormControl('', Validators.compose([CustomValidator.emailValidator])),
        completeEmailCC: new FormControl('', Validators.compose([CustomValidator.emailValidator]))

      }),
      event: new FormGroup({
        comments: new FormControl('', Validators.required),
        splkEventType: new FormControl({ value: null, disabled: this.showEditButton }),
        splkActivityType: new FormControl({ value: null, disabled: this.showEditButton }),
        ipVersion: new FormControl({ value: null, disabled: this.showEditButton }),
        mdsManaged: new FormControl({ value: false, disabled: this.showEditButton }),
        isEscalation: new FormControl({ value: false, disabled: this.showEditButton }),
        escalationReason: new FormControl({ value: 1, disabled: this.showEditButton }),
        primaryRequestDate: new FormControl(new Date().setHours(new Date().getHours(), 0, 0, 0)),
        secondaryRequestDate: new FormControl(new Date().setHours(new Date().getHours(), 0, 0, 0)),
        relatedCompassNcr: new FormControl({ value: false, disabled: this.showEditButton }),
        relatedCompassNcrName: new FormControl()
      }),
      schedule: new FormGroup({
        startDate: new FormControl(),
        endDate: new FormControl({ value: null, disabled: true }),
        eventDuration: new FormControl(60),
        extraDuration: new FormControl(0),
        assignedToId: new FormControl(),
        assignedTo: new FormControl(),
        displayedAssignedTo: new FormControl(),
      }),
      reviewer: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        comments: new FormControl()
      }),
      activators: new FormGroup({
        successfulActivities: new FormArray([]),
        selectedSuccessfulActs: new FormControl(),
        failedActivities: new FormArray([]),
        selectedFailedActs: new FormControl(),
        failedCode: new FormControl(),
        preconfigureCompleted: new FormControl(),
        comments: new FormControl()
      }),
    });

    this.option = {
      customer: {
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Customer Name is required" }
          ]
        },
        email: {
          isShown: true
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        contactName: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Contact Name is required" }
          ]
        },
        pager: {
          isShown: true
        },
        pagerPin: {
          isShown: true
        },
      },
      requestor: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["cellphone", "cellPhnNbr"],
            ["email", "emailAdr"],
            ["pager", "pgrNbr"],
            ["pagerPin", "pgrPinNbr"],
            ["bridgeNumber", "cnfrcBrdgNbr"],
            ["bridgePin", "cnfrcPinNbr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for requestor" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Requestor is required" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        email: {
          isShown: true
        },
        pager: {
          isShown: true
        },
        pagerPin: {
          isShown: true
        },
        bridgeNumber: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Conference Bridge Number is required" }
          ]
        },
        bridgePin: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Conference Bridge Pin is required" }
          ]
        }
      },
      sales: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["email", "emailAdr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for sales" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Sales Engineer Name is required" }
          ]
        },
        phone: {
          isShown: true
        },
        email: {
          isShown: true
        }
      },
      design: {
        userFinderForPublishedEmail: {
          isEnabled: true,
          searchQuery: "publishedEmailCC",
          mappings: [
            ["publishedEmailCC", "emailAdr"]
          ]
        },
        userFinderForCompleteEmail: {
          isEnabled: true,
          searchQuery: "completeEmailCC",
          mappings: [
            ["completeEmailCC", "emailAdr"]
          ]
        },
      },
      event: {
        isEscalation: {
          isShown: true
        },
        escalationReason: {
          isShown: true
        },
        primaryRequestDate: {
          isShown: true
        },
        secondaryRequestDate: {
          isShown: true
        },
      },
      schedule: {
        userFinder: {
          isEnabled: false,
          searchQuery: "displayedAssignedTo",
          mappings: [
            ["assignedToId", "userId"],
            ["assignedTo", "dsplNme"],
            ["displayedAssignedTo", "dsplNme"]
          ]
        },
        softAssign: {
          isShown: false
        },
        checkCurrentSlot: {
          isShown: false
        },
        launchPicker: {
          isShown: false
        }
      },
      lookup_m5: {
        isEnabled: true,
        searchQuery: "m5_no",
        mappings: [
          ["h1", "h1"],
          ["h6", "h6"],
          ["customer.name", "custNme"],
          ["customer.email", "custEmailAdr"],
          ["customer.cellphone", "custCntctCellPhnNbr"],
          ["customer.phone", "custCntctPhnNbr"],
          ["customer.contactName", "custCntctNme"],
          ["customer.pager", "custCntctPgrNbr"],
          ["customer.pagerPin", "custCntctPgrPinNbr"]
        ]
      },
      reviewer: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for reviewer" }
          ]
        },
        name: {
          validators: [
            { type: Validators.required, name: "required", message: "Reviewer is required" }
          ]
        }
      },
      activator: {
        failedCode: {
          isShown: false
        }
      }
    }

    this.eventStatusList = this.helper.getEnumKeyValuePair(EVENT_STATUS);
    this.relatedCompassList = this.helper.getEnumKeyValuePair(RB_CONDITION);

    let data = zip(
      this.userSrvc.getLoggedInUser(),
      this.ipVersionSrvc.getForControl(),
      this.SplkEventService.getSplkActyType(),
      this.SplkEventService.getSplkEventType(),
      this.workflowSrvc.getForEvent(this.eventType, this.splkEvent == null ? EVENT_STATUS.Visible : this.splkEvent.eventStusId),
      this.userSrvc.getFinalUserProfile(this.userSrvc.loggedInUser.adid, EEventType.SprintLink)
    )

    data.subscribe(res => {
      let user = res[0] as User;
      this.setFormControlValue("requestor.id", user.userId)
      this.setFormControlValue("requestor.name", user.fullNme);
      this.setFormControlValue("requestor.email", user.emailAdr);
      this.ipVersionList = (res[1] as FormControlItem[]).filter(i => i.text.toLowerCase().includes('sprintlink'));
      this.splkActivityTypeList = res[2] as KeyValuePair[];
      this.splkEventTypeList = res[3] as KeyValuePair[];
      this.workflowStatusList = res[4] as Workflow[];
      var usrPrf = res[5] as UserProfile;
      if (usrPrf != null) {
        this.profile = this.helper.getFinalProfileName(usrPrf);
        this.setFormControlValue("workflowStatus",
          this.profile == EEventProfile.REVIEWER ? WORKFLOW_STATUS.Submit : WORKFLOW_STATUS.Visible);
        this.isMember = this.profile == EEventProfile.MEMBER ? true : false;
        this.isReviewer = this.profile == EEventProfile.REVIEWER ? true : false;
        this.isActivator = this.profile == EEventProfile.ACTIVATOR ? true : false;

        if(this.isMember && !this.isReviewer) {
          this.form.get("schedule.eventDuration").disable();
        }
      }
    }, error => {
      this.spinner.hide();
      }, () => {
        if (this.id > 0) {
          this.splkEvntSrvc.GetSplkEventById(this.id).subscribe(
            res => {
              this.splkEvent = res;
              this.splkEventAccessTagList = this.splkEvent.splkEventAccsTag;
            }, error => {
              this.spinner.hide();
              this.helper.notify('An error occurred while retrieving the Sprint Link Event data.', Global.NOTIFY_TYPE_ERROR);
            }, () => {
              this.setSplkEventFormValue();
              this.disableForm();
              this.setSplkEventFormFields();
              // Check EventLock
              //this.evntLockSrvc.checkLock(this.id).subscribe(
              //  res => {
              //    if (res != null) {
              //      this.eventNote = "This event is currently locked by " + res.lockByFullName;
              //      this.lockedByUser = res.lockByFullName;
              //      this.isLocked = true;
              //    }
              //  },
              //  error => {
              //    this.spinner.hide();
              //    this.helper.notify('An error occurred while retrieving Event Lock status.', Global.NOTIFY_TYPE_ERROR);
              //  },
              //  () => {
              //    this.spinner.hide();
              //  }
              //);
            });
        }
        else {
          this.setSplkEventFormFields();
          this.spinner.hide();
          this.disableForm();
        }
      });   
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  getSelectedActivators() {
    let MyActivators = this.getFormControlValue("schedule.assignedToId")
    if (MyActivators != null) {
      let Activators = MyActivators.toString()
      if (Activators.length > 0) {
        if (Activators.indexOf(',') != -1) {
          return Activators.toString().split(",").map(a => parseInt(a));
        } else {
          return [MyActivators];
        }
      }
    }

    return [];
  }

  setSplkEventFormValue() {
    if (this.id > 0 && this.splkEvent != null) {
      this.setFormControlValue("itemTitle", this.splkEvent.eventTitleTxt);
      this.setFormControlValue("eventDescription", this.splkEvent.eventDes);
      this.setFormControlValue("eventID", this.splkEvent.eventId);
      this.setFormControlValue("eventStatus", this.splkEvent.eventStusId);
      this.setFormControlValue("m5_no", this.splkEvent.ftn);
      this.setFormControlValue("charsID", this.splkEvent.charsId);
      this.setFormControlValue("h1", this.splkEvent.h1);
      this.setFormControlValue("h6", this.splkEvent.h6);

      this.setFormControlValue("customer.name", this.splkEvent.custNme);
      this.setFormControlValue("customer.email", this.splkEvent.custEmailAdr);
      this.setFormControlValue("customer.phone", this.splkEvent.custCntctPhnNbr);
      this.setFormControlValue("customer.cellphone", this.splkEvent.custCntctCellPhnNbr);
      this.setFormControlValue("customer.contactName", this.splkEvent.custCntctNme);
      this.setFormControlValue("customer.pager", this.splkEvent.custCntctPgrNbr);
      this.setFormControlValue("customer.pagerPin", this.splkEvent.custCntctPgrPinNbr);

      this.setFormControlValue("requestor.id", this.splkEvent.reqorUser.userId);
      this.setFormControlValue("requestor.name", this.splkEvent.reqorUser.fullNme);
      this.setFormControlValue("requestor.phone", this.splkEvent.reqorUser.phnNbr);
      this.setFormControlValue("requestor.cellphone", this.splkEvent.reqorUser.cellPhnNbr);
      this.setFormControlValue("requestor.email", this.splkEvent.reqorUser.emailAdr);
      this.setFormControlValue("requestor.pager", this.splkEvent.reqorUser.pgrNbr);
      this.setFormControlValue("requestor.pagerPin", this.splkEvent.reqorUser.pgrPinNbr);
      this.setFormControlValue("requestor.bridgeNumber", this.splkEvent.cnfrcBrdgNbr);
      this.setFormControlValue("requestor.bridgePin", this.splkEvent.cnfrcPinNbr);
      this.setFormControlValue("requestor.eventComments", this.splkEvent.dsgnCmntTxt);

      this.setFormControlValue("sales.id", this.splkEvent.salsUser.userId);
      this.setFormControlValue("sales.name", this.splkEvent.salsUser.fullNme);
      this.setFormControlValue("sales.phone", this.splkEvent.salsUser.phnNbr);
      this.setFormControlValue("sales.email", this.splkEvent.salsUser.emailAdr);

      this.setFormControlValue("design.publishedEmailCC", this.splkEvent.pubEmailCcTxt);
      this.setFormControlValue("design.completeEmailCC", this.splkEvent.cmpltdEmailCcTxt);

      this.setFormControlValue("event.comments", this.splkEvent.dsgnCmntTxt);
      this.setFormControlValue("event.splkEventType", this.splkEvent.splkEventTypeId);
      this.setFormControlValue("event.splkActivityType", this.splkEvent.splkActyTypeId);
      this.setFormControlValue("event.ipVersion", this.splkEvent.ipVerId);
      this.setFormControlValue("event.mdsManaged", this.splkEvent.mdsMngdCd);
      this.setFormControlValue("event.relatedCompassNcr", this.splkEvent.reltdCmpsNcrCd ? 1 : 0);
      this.setFormControlValue("event.relatedCompassNcrName", this.splkEvent.reltdCmpsNcrNme);

      this.setFormControlValue("event.isEscalation", this.splkEvent.esclCd);
      this.setFormControlValue("event.escalationReason", this.splkEvent.esclReasId);
      this.setFormControlValue("event.primaryRequestDate", this.splkEvent.primReqDt);
      this.setFormControlValue("event.secondaryRequestDate", this.splkEvent.scndyReqDt);
      this.setFormControlValue("schedule.startDate", this.splkEvent.strtTmst);
      this.setFormControlValue("schedule.endDate", this.splkEvent.endTmst);
      this.setFormControlValue("schedule.eventDuration", this.splkEvent.eventDrtnInMinQty);
      this.setFormControlValue("schedule.extraDuration", this.splkEvent.extraDrtnTmeAmt);

      if (this.isActivator) {
        if (this.splkEvent.wrkflwStusId == WORKFLOW_STATUS.Publish || this.splkEvent.wrkflwStusId == WORKFLOW_STATUS.InProgress) {
          this.ActivatorEditButton = true;
        } else
          this.ActivatorEditButton = false;
      }
      this.setFormControlValue("reviewer.comments", this.splkEvent.reviewCmntTxt);
      if (this.profile == "Reviewer") {
        this.userSrvc.getLoggedInUser().subscribe(
          res => {
            let user = res as User;
            this.setFormControlValue("reviewer.id", user.userId)
            this.setFormControlValue("reviewer.name", user.fullNme);
          }
        );
      }

      this.evntAsnToUserSrvc.getByEventId(this.splkEvent.eventId).subscribe(
        res => {
          this.activators = res;
          console.log(this.activators);
          this.setFormControlValue("schedule.assignedToId", this.activators.map(a => a.asnToUserId).join(","))
          this.setFormControlValue("schedule.assignedTo", this.activators.map(a => a.userDsplNme).join("; "))
          this.setFormControlValue("schedule.displayedAssignedTo", this.activators.map(a => a.userDsplNme).join("; "))
        },
        error => {

        });
        
        this.workflowSrvc.getForEvent(EEventType.SprintLink, this.splkEvent.eventStusId).subscribe(
          res => {
            //let workflowStatus = this.workflowStatusList;
            this.workflowStatusList = res as Workflow[];
            if (this.splkEvent.wrkflwStusId == WORKFLOW_STATUS.Reschedule) {
              if (this.workflowStatusList.some(a => a.wrkflwStusId == WORKFLOW_STATUS.Reschedule)) {
                this.setFormControlValue("workflowStatus", this.splkEvent.wrkflwStusId);
              } else {
                if (this.workflowStatusList.some(a => a.wrkflwStusId == WORKFLOW_STATUS.Delete)) {
                  this.setFormControlValue("workflowStatus", WORKFLOW_STATUS.Delete);
                }
              }
            } else {
              this.setFormControlValue("workflowStatus", this.splkEvent.wrkflwStusId);
            }
            this.isRevActTask = (this.id > 0 && this.workflowStatusList.length > 0) ? true : false;
          }
      );
    }
  }

  setSplkEventFormFields() {
    if (this.id > 0) {
      let eventStatusId = this.getFormControlValue("eventStatus");
      let wfStatusId = this.getFormControlValue("workflowStatus");
      if (this.profile == EEventProfile.ACTIVATOR) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Published) {
          this.allowEdit = true;
        } else {
          this.allowEdit = false;
        }
        // If not reviewer and EStus not equal to Inprogress or Published
        // Hide Launcher and Slot Picker
        if (!this.showEditButton) {
          if (this.userSrvc.loggedInUser.profiles.includes('CAND Event Reviewer')
            && (this.splkEvent.eventStusId == EVENT_STATUS.InProgress
            || this.splkEvent.eventStusId == EVENT_STATUS.Published)) {
            this.option.schedule.checkCurrentSlot.isShown = true;
            this.option.schedule.launchPicker.isShown = true;
          }
          else {
            this.option.schedule.checkCurrentSlot.isShown = false;
            this.option.schedule.launchPicker.isShown = false;
          }
        }
      }
      else if (this.profile == EEventProfile.REVIEWER) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Completed) {
          this.allowEdit = false;
        } else {
          this.allowEdit = true;
        }
        // Show Launcher and Slot Picker
        if (!this.showEditButton) {
          this.option.schedule.userFinder.isEnabled = true;
          this.option.schedule.checkCurrentSlot.isShown = true;
          this.option.schedule.launchPicker.isShown = true;
        }

      }
      else if (this.profile == EEventProfile.MEMBER) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Completed) {
          this.allowEdit = false;
        }
        else if (eventStatusId == EVENT_STATUS.Published) {
          this.allowEdit = true;
        }
        else {
          if (this.splkEvent != null && this.splkEvent.entityBase != null
            && !this.helper.isEmpty(this.splkEvent.entityBase.createdByUserName)) {
            if (this.splkEvent.entityBase.createdByUserName != this.userSrvc.loggedInUser.adid) {
              if (wfStatusId == WORKFLOW_STATUS.Visible || wfStatusId == WORKFLOW_STATUS.Submit
                || eventStatusId == EVENT_STATUS.Rework) {
                this.allowEdit = true;
              } else {
                this.allowEdit = false;
              }
            } else {
              this.allowEdit = true;
            }
          } else {
            this.allowEdit = true;
          }
        }
        // Show Slot picker, Hide Launcher
        if (!this.showEditButton) {
          this.option.schedule.checkCurrentSlot.isShown = true;
          this.option.schedule.launchPicker.isShown = false;
        }
      }
      else {
        // Not CAND Activator, Revieiwer or Member
        this.allowEdit = false;
        // Hide Launcher and Picker
        this.option.schedule.checkCurrentSlot.isShown = false;
        this.option.schedule.launchPicker.isShown = false;
      }
    }
    else {
      // Allow Edit set to true for New Event so that the buttons are seen
      this.allowEdit = true;
      if (this.profile == EEventProfile.REVIEWER) {
        this.option.schedule.checkCurrentSlot.isShown = true;
        this.option.schedule.launchPicker.isShown = true;
      }
      else if (this.profile == EEventProfile.MEMBER) {
        this.option.schedule.checkCurrentSlot.isShown = true;
      }
    }
  }

  getTitle(): string {
    return this.splkEventTypeList.find(a => a.value == this.splkEventTypeId).description
  }

  getSplkEventFormValue(): SplkEvent {
    let splk = new SplkEvent();
    this.splkEventTypeId = this.splkEventTypeId != null ? this.splkEventTypeId : this.getFormControlValue("event.splkEventType")
    splk.eventTitleTxt = this.getFormControlValue("customer.name") + " " + this.getTitle()
    splk.eventDes = this.getFormControlValue("eventDescription")
    splk.eventStusId = this.getFormControlValue("eventStatus")
    splk.ftn = this.getFormControlValue("m5_no")
    splk.h1 = this.getFormControlValue("h1")
    splk.h6 = this.getFormControlValue("h6")
    splk.charsId = this.getFormControlValue("charsID")
    splk.wrkflwStusId = this.getFormControlValue("workflowStatus");

    splk.custNme = this.getFormControlValue("customer.name")
    splk.custEmailAdr = this.getFormControlValue("customer.email")
    splk.custCntctPhnNbr = this.getFormControlValue("customer.phone")
    splk.custCntctCellPhnNbr = this.getFormControlValue("customer.cellphone")
    splk.custCntctNme = this.getFormControlValue("customer.contactName")
    splk.custCntctPgrNbr = this.getFormControlValue("customer.pager")
    splk.custCntctPgrPinNbr = this.getFormControlValue("customer.pagerPin")
    splk.reqorUserId = this.getFormControlValue("requestor.id")
    splk.cnfrcBrdgNbr = this.getFormControlValue("requestor.bridgeNumber")
    splk.cnfrcPinNbr = this.getFormControlValue("requestor.bridgePin")
    splk.salsUserId = this.getFormControlValue("sales.id")

    splk.pubEmailCcTxt = this.getFormControlValue("design.publishedEmailCC")
    splk.cmpltdEmailCcTxt = this.getFormControlValue("design.completeEmailCC")

    splk.dsgnCmntTxt = this.getFormControlValue("event.comments")
    splk.splkEventTypeId = this.getFormControlValue("event.splkEventType")
    splk.splkActyTypeId = this.getFormControlValue("event.splkActivityType")

    splk.ipVerId = this.getFormControlValue("event.ipVersion")

    splk.reltdCmpsNcrCd = this.getFormControlValue("event.relatedCompassNcr")
    splk.reltdCmpsNcrNme = this.getFormControlValue("event.relatedCompassNcrName")

    splk.splkEventAccsTag = this.splkEventAccessTagList;
    splk.esclCd = this.getFormControlValue("event.isEscalation")
    splk.esclReasId = splk.esclCd ? this.getFormControlValue("event.escalationReason") : null
    splk.primReqDt = splk.esclCd ? new Date(this.getFormControlValue("event.primaryRequestDate")) : null
    splk.scndyReqDt = splk.esclCd ? new Date(this.getFormControlValue("event.secondaryRequestDate")) : null
    splk.mdsMngdCd = this.getFormControlValue("event.mdsManaged")
    splk.strtTmst = this.getFormControlValue("schedule.startDate")
    splk.endTmst = this.getFormControlValue("schedule.endDate")
    splk.eventDrtnInMinQty = this.getFormControlValue("schedule.eventDuration")
    splk.extraDrtnTmeAmt = this.getFormControlValue("schedule.extraDuration")
    splk.activators = this.getSelectedActivators()
    splk.wrkflwStusId = this.getFormControlValue("workflowStatus")
    splk.dsgnCmntTxt = this.getFormControlValue("event.comments");
    if (this.profile == 'Reviewer') splk.reviewCmntTxt = this.getFormControlValue("reviewer.comments");
    splk.profile = this.profile;
    if (this.profile == EEventProfile.ACTIVATOR) {
      splk.eventSucssActyIds = this.getFormControlValue("activators.selectedSuccessfulActs")
      splk.eventFailActyIds = this.getFormControlValue("activators.selectedFailedActs")
      splk.preCfgConfgCode = this.getFormControlValue("activators.preconfigureCompleted")
      splk.reviewCmntTxt = this.getFormControlValue("activators.comments")
    }
    if (this.getFormControlValue("workflowStatus") == WORKFLOW_STATUS.Reject
      || this.getFormControlValue("workflowStatus") == WORKFLOW_STATUS.Reschedule) {
        splk.activators = [];
      }
    
    return splk;
  }

  saveSplkTag(event: any) {
    //console.log(event.data);
    //let tag = new SplkEventAccessTag();
    //tag = event.data;
    //tag.splkEventAccsId = 0;
    //this.splkEventAccessTagList.push(tag);
  }

  test() {
    //var splk = this.getSplkEventFormValue();
    //var successfulActivities = this.getFormControlValue("activators.cblSuccActArray")
    //var failedActivities = this.getFormControlValue("activators.cblFailActArray")
    //var failedCodes = this.getFormControlValue("activators.ddlFailCodes")
    //var preconfigureOptions = this.getFormControlValue("activators.ddlPreConfig")
    //var activatorComments = this.getFormControlValue("activators.comments")

    //console.log(splk);
    //console.log(successfulActivities);
    //console.log(this.profile);
    //console.log(failedActivities);
    //console.log(failedCodes);
    //console.log(preconfigureOptions);
    //console.log(activatorComments);
    //console.log(this.eventTypeId);
    let activators = this.getFormControlValue("schedule.assignedToId")
    console.log(activators);
    console.log(activators.toString().length);
    console.log(activators.toString().indexOf(","));
    //console.log(activators.includes(","));
    if (activators != null) {
      activators = activators.toString()
      if (activators.toString().length > 0) {
        if (activators.indexOf(',') != -1) {
          console.log(activators.toString().split(",").map(a => parseInt(a)));
        } else
          console.log(activators);
      }
    }
  }

  CheckEmailAddressWRegex(emailadr) {
    var filter = /^([a-zA-Z0-9_\.\-&])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,10})+$/;
    if (filter.test(emailadr.trim())) {
      return true;
    }
    else { return false; }
  }

  save() {
    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }
    
    var er = this.getFormControlValue("event.relatedCompassNcrName") != null ? this.getFormControlValue("event.relatedCompassNcrName") : "";
    if (this.getFormControlValue("event.relatedCompassNcr") == 1) {
      if (er == "") {
        this.helper.notify('If Related Compass NCR is Yes, corresponding value needs to be populated', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    var rcmts = this.getFormControlValue("reviewer.comments") != null ? this.getFormControlValue("reviewer.comments") : "";
    var rnme = this.getFormControlValue("reviewer.name") != null ? this.getFormControlValue("reviewer.name") : "";
    if (this.profile=="Reviewer" && this.id > 0) {
      if (this.getFormControlValue("workflowStatus") == WORKFLOW_STATUS.Reject) {
        if (rcmts == "") {
          this.helper.notify('Reviewer Comments should be populated when the event is being rejected', Global.NOTIFY_TYPE_WARNING);
          return;
        }
      }
      if (rnme == "") {
        this.helper.notify('Reviewer cannot be blank', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    var needActivatorComnt = this.getFormControlValue("workflowStatus") != WORKFLOW_STATUS.Reschedule ? true : false;
    var Acmnts = this.getFormControlValue("activators.comments") != null ? this.getFormControlValue("activators.comments") : "";
    if (this.profile == "Activator" && this.id > 0 && needActivatorComnt) {
      if (Acmnts == "") {
        this.helper.notify('Activator Comment cannot be blank', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    if (this.splkEventAccessTagList!= null) {
      if (this.splkEventAccessTagList.length <= 0) {
        this.helper.notify('Access Table cannot be blank', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    var sAsid = this.getFormControlValue("schedule.assignedToId") != null ? this.getFormControlValue("schedule.assignedToId") : "";
    if (this.getFormControlValue("workflowStatus") == WORKFLOW_STATUS.Publish || this.getFormControlValue("workflowStatus") == WORKFLOW_STATUS.Submit) {
      if (sAsid == "") {
        this.helper.notify('Assigned Activator cannot be blank', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    var cem = this.getFormControlValue("customer.email") != null ? this.getFormControlValue("customer.email") : "";
    if (cem != "") {
      if (!this.CheckEmailAddressWRegex(cem)) {
        this.helper.notify('Please enter a valid address in Customer Contact Email', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    var dpem = this.getFormControlValue("design.publishedEmailCC") != null ? this.getFormControlValue("design.publishedEmailCC") : "";
    if (dpem != "") {
      if (!this.CheckEmailAddressWRegex(dpem)) {
        this.helper.notify('Please enter a valid address in Published Email CC', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    var dcem = this.getFormControlValue("design.completeEmailCC") != null ? this.getFormControlValue("design.completeEmailCC") : "";
    if (dcem != "") {
      if (!this.CheckEmailAddressWRegex(dcem)) {
        this.helper.notify('Please enter a valid address in Completed Email CC', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    var sem = this.getFormControlValue("sales.email") != null ? this.getFormControlValue("sales.email") : "";
    if (sem != "") {
      if (!this.CheckEmailAddressWRegex(sem)) {
        this.helper.notify('Please enter a valid address in Sales Engineer Email', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    var rem = this.getFormControlValue("requestor.email") != null ? this.getFormControlValue("requestor.email") : "";
    if (rem != "") {
      if (!this.CheckEmailAddressWRegex(rem)) {
        this.helper.notify('Please enter a valid address in Requestor Contact Email', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }

    this.isSubmitted = true;
    var splk = this.getSplkEventFormValue();
    console.log(splk);
    if (this.id > 0) {
      // Update splk Event
      this.spinner.show();
      this.splkEvntSrvc.update(this.id, splk).subscribe(res => {
        this.helper.notifySavedFormMessage("SPLK Event", Global.NOTIFY_TYPE_SUCCESS, false, null)
      }, error => {
        this.helper.notifySavedFormMessage("SPLK Event", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide();
          this.router.navigate(['/event/sprint-link/']);
      });
    }
    else {
      // Create splk Event
      this.spinner.show();
      this.splkEvntSrvc.create(splk).subscribe(res => {
        this.helper.notifySavedFormMessage("SPLK Event", Global.NOTIFY_TYPE_SUCCESS, true, null);
        this.spinner.hide();
        this.router.navigate(['/event/sprint-link/']);
      }, error => {
        this.helper.notifySavedFormMessage("SPLK Event", Global.NOTIFY_TYPE_ERROR, true, error);
        this.spinner.hide()
      });
    }
  }

  edit() {
    // Lock Event
    this.evntLockSrvc.lock(this.id).subscribe(
      res => {
        this.showEditButton = false;
        this.form.get("itemTitle").enable();
        this.form.get("eventDescription").enable();
        this.form.get("m5_no").enable();
        this.form.get("h1").enable();
        this.form.get("h6").enable();
        this.form.get("charsID").enable();
        this.form.get("workflowStatus").enable();
        this.form.get("customer").enable();
        this.form.get("sales").enable();
        this.form.get("design").enable();
        this.form.get("event").enable();
        this.form.get("requestor").enable();
        this.form.get("schedule").enable();
        this.form.get("reviewer").enable();
        
        if (this.id > 0 && this.profile == EEventProfile.ACTIVATOR) {
          this.form.get("activators.successfulActivities").enable();
          this.form.get("activators.failedActivities").enable();
          this.form.get("activators.preconfigureCompleted").enable();
          this.form.get("activators.comments").enable();
        }
        this.setSplkEventFormFields();
      },
      error => {
        this.helper.notify('An error occurred while locking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  disableForm() {
    this.showEditButton = true;
    this.form.get("itemTitle").disable();
    this.form.get("eventDescription").disable();
    this.form.get("m5_no").disable();
    this.form.get("h1").disable();
    this.form.get("h6").disable();
    this.form.get("charsID").disable();
    this.form.get("workflowStatus").disable();
    this.form.get("customer").disable();
    this.form.get("sales").disable();
    this.form.get("design").disable();
    this.form.get("event").disable();
    this.form.get("requestor").disable();
    this.form.get("schedule").disable();
    this.form.get("reviewer").disable();
    
    if (this.id > 0 && this.profile == EEventProfile.ACTIVATOR) {
      this.form.get("activators.successfulActivities").disable();
      this.form.get("activators.failedActivities").disable();
      this.form.get("activators.preconfigureCompleted").disable();
      this.form.get("activators.comments").disable();
    }
  }

  cancel() {
    if (this.id > 0 && !this.showEditButton) {
      this.unlockEvent();
    }

    this.router.navigate(['/event/sprint-link/']);
  }

  unlockEvent() {
    // Unlock Event
    this.evntLockSrvc.unlock(this.id).subscribe(
      res => { },
      error => {
        this.helper.notify('An error occurred while unlocking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }
}
