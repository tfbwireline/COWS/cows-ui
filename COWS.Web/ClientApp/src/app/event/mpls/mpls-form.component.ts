import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from "rxjs";
import ArrayStore from "devextreme/data/array_store";
import { Helper } from "./../../../shared";
import {
  EVENT_STATUS, KeyValuePair, EEventType, RB_CONDITION, Global, WORKFLOW_STATUS, EEventProfile
} from "./../../../shared/global";
import {
  UserService, ActivityLocaleService, MplsAccessBandwidthService, MplsActivityTypeService,
  MplsEventTypeService, VpnPlatformTypeService, VasTypeService, IPVersionService, MultiVrfReqService,
  MplsMigrationTypeService, MplsEventService, VendorService, WholesalePartnerService,
  WorkflowService, EventAsnToUserService, MnsOfferingRouterService, MnsPerformanceReportingService,
  MnsRoutingTypeService, EventLockService
} from "./../../../services/";
import {
  User, FormControlItem, MplsEventAccessTag, MplsEvent, Workflow, EventAsnToUser, UserProfile
} from "./../../../models";
import { CustomValidator } from '../../../shared/validator/custom-validator';

@Component({
  selector: 'app-mpls-form',
  templateUrl: './mpls-form.component.html'
})

export class MplsFormComponent implements OnInit, OnDestroy {
  // Page properties
  id: number = 0;
  profile: string;
  eventType: number = EEventType.MPLS;
  mplsEvent: MplsEvent;
  allowEdit: boolean = true;
  editMode: boolean = true;
  isLocked: boolean = false;
  lockedByUser: string;
  isRevActTask: boolean = false;
  isActivator: boolean = false;
  isReviewer: boolean = false;
  isMember: boolean = false;

  // For UI Controls
  relatedCompassList: KeyValuePair[];
  eventStatusList: KeyValuePair[];
  activityLocaleList: KeyValuePair[];
  ipVersionList: FormControlItem[];
  mplsAccessBandwidthList: KeyValuePair[];
  mplsActivityTypeList: any;
  mplsEventTypeList: KeyValuePair[];
  mplsMigrationTypeList: KeyValuePair[];
  multiVrfReqList: FormControlItem[];
  vasTypeList: any;
  vpnPlatformTypeList: KeyValuePair[];
  carrierVendorList: any[];
  wholesalePartnerList: KeyValuePair[];
  mnsOfferingRouterList: KeyValuePair[];
  mnsPerformanceReportingList: KeyValuePair[];
  mnsRoutingTypeList: KeyValuePair[];
  mplsEventAccessTagList: MplsEventAccessTag[] = [];
  workflowStatusList: Workflow[];
  activators: EventAsnToUser[];

  // Form Properties
  form: FormGroup;
  option: any;
  isSubmitted: boolean = false;
  get h1() { return this.form.get("h1") }
  get workflowStatus() { return this.form.get("workflowStatus") }
  get publishedEmailCC() { return this.form.get("design.publishedEmailCC") }
  get completeEmailCC() { return this.form.get("design.completeEmailCC") }
  get designDocumentLocation() { return this.form.get("design.designDocumentLocation") }
  get designDocumentApprovalNo() { return this.form.get("design.designDocumentApprovalNo") }
  get comments() { return this.form.get("event.comments") }
  get mplsEventType() { return this.form.get("event.mplsEventType") }
  get vpnPlatformType() { return this.form.get("event.vpnPlatformType") }
  get ipVersion() { return this.form.get("event.ipVersion") }
  get relatedCompassNcr() { return this.form.get("event.relatedCompassNcr") }

  get mdsManaged() { return this.form.get("event.mdsManaged") }
  get multiVrfRequirement() { return this.form.get("event.multiVrfRequirement") }
  get addEndToEndMonitoring() { return this.form.get("event.addEndToEndMonitoring") }
  get isWholesalePartner() { return this.form.get("event.isWholesalePartner") }
  get isCarrierPartner() { return this.form.get("event.isCarrierPartner") }
  get isMigration() { return this.form.get("event.isMigration") }

  constructor(private avRoute: ActivatedRoute, private router: Router, private helper: Helper,
    private spinner: NgxSpinnerService, private userSrvc: UserService, private mplsEvntSrvc: MplsEventService,
    private activityLocaleSrvc: ActivityLocaleService, private mplsAccsBndwdthSrvc: MplsAccessBandwidthService,
    private mplsActvtyTypeSrvc: MplsActivityTypeService, private mplsEventTypeSrvc: MplsEventTypeService,
    private multiVrfSrvc: MultiVrfReqService, private ipVersionSrvc: IPVersionService,
    private vasTypeSrvc: VasTypeService, private vpnPltfrmTypeSrvc: VpnPlatformTypeService,
    private mplsMigrationSrvc: MplsMigrationTypeService, private vndrSrvc: VendorService,
    private wholesalePrtnrSrvc: WholesalePartnerService, private workflowSrvc: WorkflowService,
    private evntAsnToUserSrvc: EventAsnToUserService, private mnsOffrngRoutrSrvc: MnsOfferingRouterService,
    private mnsPrfrmncRprtngSrvc: MnsPerformanceReportingService, private evntLockSrvc: EventLockService,
    private mnsRoutngTypeSrvc: MnsRoutingTypeService
  ) { }

  ngOnDestroy() {
    this.unlockEvent();
    this.helper.form = null;
  }

  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"] || 0;
    //this.editMode = this.id > 0 ? false : true;
    this.form = new FormGroup({
      itemTitle: new FormControl({ value: null, disabled: true }),
      eventDescription: new FormControl(),
      eventID: new FormControl({ value: null, disabled: true }),
      eventStatus: new FormControl({ value: EVENT_STATUS.Visible, disabled: true }),
      m5_no: new FormControl(),
      charsID: new FormControl(),
      h1: new FormControl(null, Validators.compose([Validators.required, CustomValidator.h1Validator])),
      h6: new FormControl(),
      workflowStatus: new FormControl({ value: WORKFLOW_STATUS.Visible }),
      customer: new FormGroup({

        name: new FormControl(),
        email: new FormControl(),
        phone: new FormControl(),
        cellphone: new FormControl(),
        contactName: new FormControl(),
        pager: new FormControl(),
        pagerPin: new FormControl()
      }),
      requestor: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        cellphone: new FormControl(),
        email: new FormControl(),
        pager: new FormControl(),
        pagerPin: new FormControl(),
        bridgeNumber: new FormControl(),
        bridgePin: new FormControl(),
        eventComments: new FormControl()
      }),
      sales: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        email: new FormControl()
      }),
      design: new FormGroup({
        publishedEmailCC: new FormControl('', Validators.compose([CustomValidator.emailValidator])),
        completeEmailCC: new FormControl('', Validators.compose([CustomValidator.emailValidator])),
        designDocumentLocation: new FormControl('', Validators.required),
        designDocumentApprovalNo: new FormControl('', Validators.required),
        isNddUpdated: new FormControl(false)
      }),
      event: new FormGroup({
        comments: new FormControl('', Validators.required),
        mplsEventType: new FormControl('', Validators.required),
        mplsActivityTypes: new FormControl(null, Validators.required),
        vpnPlatformType: new FormControl(null, Validators.required),
        vasTypes: new FormControl(),
        ipVersion: new FormControl('', Validators.required),
        activityLocale: new FormControl(),
        multiVrfRequirement: new FormControl(),
        vrfName: new FormControl(),
        mdsManaged: new FormControl(false),
        addEndToEndMonitoring: new FormControl(false),
        mdsVrfName: new FormControl(),
        mdsIpAddress: new FormControl(),
        mdsDlci: new FormControl(),
        mdsStaticRoute: new FormControl(),
        onNetMonitoring: new FormControl(false),
        isWholesalePartner: new FormControl(false),
        wholesalePartner: new FormControl(),
        nniDesignDocument: new FormControl(),
        isCarrierPartner: new FormControl(false),
        carrierVendorPartner: new FormControl(),
        offNetMonitoring: new FormControl(false),
        isMigration: new FormControl(false),
        migrationType: new FormControl(),
        relatedCompassNcr: new FormControl(false, Validators.required),
        relatedCompassNcrName: new FormControl(),
        isEscalation: new FormControl(false),
        escalationReason: new FormControl(1),
        primaryRequestDate: new FormControl(new Date().setHours(new Date().getHours(), 0, 0, 0)),
        secondaryRequestDate: new FormControl(new Date().setHours(new Date().getHours(), 0, 0, 0))
        //primaryRequestDate: new FormControl({ value: new Date().setHours(new Date().getHours(), 0, 0, 0) }),
        //secondaryRequestDate: new FormControl({ value: new Date().setHours(new Date().getHours(), 0, 0, 0) })
      }),
      schedule: new FormGroup({
        startDate: new FormControl(),
        endDate: new FormControl({ value: null, disabled: true }),
        eventDuration: new FormControl(60),
        extraDuration: new FormControl(0),
        assignedToId: new FormControl(),
        assignedTo: new FormControl(),
        displayedAssignedTo: new FormControl()
      }),
      reviewer: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        comments: new FormControl()
      }),
      activators: new FormGroup({
        successfulActivities: new FormArray([]),
        selectedSuccessfulActs: new FormControl(),
        failedActivities: new FormArray([]),
        selectedFailedActs: new FormControl(),
        failedCode: new FormControl(),
        preconfigureCompleted: new FormControl(),
        comments: new FormControl()
      })
    });
    this.option = {
      customer: {
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Customer Name is required" }
          ]
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        contactName: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Contact Name is required" }
          ]
        },
        pager: {
          isShown: true
        },
        pagerPin: {
          isShown: true
        },
      },
      requestor: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["cellphone", "cellPhnNbr"],
            ["email", "emailAdr"],
            ["pager", "pgrNbr"],
            ["pagerPin", "pgrPinNbr"],
            ["bridgeNumber", "cnfrcBrdgNbr"],
            ["bridgePin", "cnfrcPinNbr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for requestor" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Requestor is required" }
          ]
        },
        phone: {
          isShown: true
        },
        cellphone: {
          isShown: true
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        },
        pager: {
          isShown: true
        },
        pagerPin: {
          isShown: true
        },
        bridgeNumber: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Conference Bridge Number is required" }
          ]
        },
        bridgePin: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Conference Bridge Pin is required" }
          ]
        }
      },
      sales: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["email", "emailAdr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for sales" }
          ]
        },
        name: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "Sales is required" }
          ]
        },
        phone: {
          isShown: true
        },
        email: {
          isShown: true,
          validators: [
            { type: CustomValidator.emailValidator, name: "invalidEmail", message: "Email is invalid" }
          ]
        }
      },
      design: {
        userFinderForPublishedEmail: {
          isEnabled: true,
          searchQuery: "publishedEmailCC",
          mappings: [
            ["publishedEmailCC", "emailAdr"]
          ]
        },
        userFinderForCompleteEmail: {
          isEnabled: true,
          searchQuery: "completeEmailCC",
          mappings: [
            ["completeEmailCC", "emailAdr"]
          ]
        },
      },
      event: {
        isEscalation: {
          isShown: true
        },
        escalationReason: {
          isShown: true
        },
        primaryRequestDate: {
          isShown: true
        },
        secondaryRequestDate: {
          isShown: true
        },
      },
      schedule: {
        userFinder: {
          isEnabled: false,
          searchQuery: "displayedAssignedTo",
          mappings: [
            ["assignedToId", "userId"],
            ["assignedTo", "dsplNme"],
            ["displayedAssignedTo", "dsplNme"]
          ]
        },
        softAssign: {
          isShown: false
        },
        checkCurrentSlot: {
          isShown: false
        },
        launchPicker: {
          isShown: false
        }
      },
      lookup_m5: {
        isEnabled: true,
        searchQuery: "m5_no",
        mappings: [
          ["h1", "h1"],
          ["h6", "h6"],
          ["customer.name", "custNme"],
          ["customer.email", "custEmailAdr"],
          ["customer.cellphone", "custCntctCellPhnNbr"],
          ["customer.phone", "custCntctPhnNbr"],
          ["customer.contactName", "custCntctNme"],
          ["customer.pager", "custCntctPgrNbr"],
          ["customer.pagerPin", "custCntctPgrPinNbr"]
        ]
      },
      reviewer: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for reviewer" }
          ]
        },
        name: {
          validators: [
            { type: Validators.required, name: "required", message: "Reviewer is required" }
          ]
        }
      },
      activator: {
        failedCode: {
          isShown: false
        }
      }
    }

    // Set the helper form to this form
    this.helper.form = this.form;
    this.spinner.show();

    // Initial list data
    this.eventStatusList = this.helper.getEnumKeyValuePair(EVENT_STATUS);
    this.relatedCompassList = this.helper.getEnumKeyValuePair(RB_CONDITION);
    let data = zip(
      this.userSrvc.getLoggedInUser(),
      this.activityLocaleSrvc.getForControl(),
      this.ipVersionSrvc.getForControl(),
      this.mplsAccsBndwdthSrvc.getForControl(),
      this.mplsActvtyTypeSrvc.getByEventId(this.eventType),
      this.mplsEventTypeSrvc.getByEventId(this.eventType),
      this.mplsMigrationSrvc.getForControl(),
      this.multiVrfSrvc.getForControl(),
      this.vasTypeSrvc.getForControl(),
      this.vpnPltfrmTypeSrvc.getByEventId(this.eventType),
      this.vndrSrvc.get(),
      this.wholesalePrtnrSrvc.getForControl(),
      this.mnsOffrngRoutrSrvc.getForControl(),
      this.mnsPrfrmncRprtngSrvc.getForControl(),
      this.mnsRoutngTypeSrvc.getForControl(),
      this.workflowSrvc.getForEvent(this.eventType, this.mplsEvent == null ? EVENT_STATUS.Visible : this.mplsEvent.eventStusId),
      this.userSrvc.getFinalUserProfile(this.userSrvc.loggedInUser.adid, this.eventType)
    )

    data.subscribe(res => {
      let user = res[0] as User;
      this.helper.setFormControlValue("requestor.id", user.userId);
      this.helper.setFormControlValue("requestor.name", user.fullNme);
      this.helper.setFormControlValue("requestor.email", user.emailAdr);
      this.helper.setFormControlValue("requestor.phone", user.phnNbr);
      this.helper.setFormControlValue("requestor.cellphone", user.cellPhnNbr);
      this.helper.setFormControlValue("requestor.pager", user.pgrNbr);
      this.helper.setFormControlValue("requestor.pagerPin", user.pgrPinNbr);
      this.activityLocaleList = res[1] as KeyValuePair[];
      this.ipVersionList = (res[2] as FormControlItem[]).filter(i => i.text.toLowerCase().includes('mpls'));
      this.mplsAccessBandwidthList = res[3] as KeyValuePair[];
      this.mplsActivityTypeList = new ArrayStore({
        data: res[4] as KeyValuePair[],
        key: "value"
      });
      this.mplsEventTypeList = res[5] as KeyValuePair[];
      this.mplsMigrationTypeList = res[6] as KeyValuePair[];
      this.multiVrfReqList = res[7] as FormControlItem[];
      this.vasTypeList = new ArrayStore({
        data: res[8] as KeyValuePair[],
        key: "value"
      });
      this.vpnPlatformTypeList = res[9] as KeyValuePair[];
      this.carrierVendorList = res[10] as any[];
      this.wholesalePartnerList = res[11] as KeyValuePair[];
      this.mnsOfferingRouterList = res[12] as KeyValuePair[];
      this.mnsPerformanceReportingList = res[13] as KeyValuePair[];
      this.mnsRoutingTypeList = res[14] as KeyValuePair[];
      this.workflowStatusList = (res[15] as Workflow[]);
      var usrPrf = res[16] as UserProfile;
      if (usrPrf != null) {
        this.profile = this.helper.getFinalProfileName(usrPrf);
        this.helper.setFormControlValue("workflowStatus",
          this.profile == EEventProfile.REVIEWER ? WORKFLOW_STATUS.Submit : WORKFLOW_STATUS.Visible);
        this.isMember = this.profile == EEventProfile.MEMBER ? true : false;
        this.isReviewer = this.profile == EEventProfile.REVIEWER ? true : false;
        this.isActivator = this.profile == EEventProfile.ACTIVATOR ? true : false;

        if(this.isMember && !this.isReviewer) {
          this.form.get("schedule.eventDuration").disable();
        }
      }
    }, error => {
      this.spinner.hide();
    }, () => {
      // Get MPLS Event Data to view or update
      if (this.id > 0) {
        this.mplsEvntSrvc.getById(this.id).subscribe(
          res => {
            //console.log(res)
            this.mplsEvent = res;
            this.mplsEventAccessTagList = this.mplsEvent.mplsEventAccsTag;
          }, error => {
            this.spinner.hide();
            this.helper.notify('An error occurred while retrieving the MPLS Event data.',
              Global.NOTIFY_TYPE_ERROR);
          }, () => {
            this.setMplsEventFormValue();
            this.disableForm();
            this.setMplsEventFormFields();
            this.spinner.hide();

            // Check EventLock
            //this.evntLockSrvc.checkLock(this.id).subscribe(
            //  res => {
            //    if (res != null) {
            //      this.lockedByUser = res.lockByFullName;
            //      this.isLocked = true;
            //    }
            //  },
            //  error => {
            //    this.spinner.hide();
            //    this.helper.notify('An error occurred while retrieving Event Lock status.',
            //      Global.NOTIFY_TYPE_ERROR);
            //  },
            //  () => {
            //    this.spinner.hide();
            //  }
            //);
          }
        );
      } else {
        this.setMplsEventFormFields();
        this.spinner.hide();
        this.disableForm();
      }
    });
  }

  setMplsEventFormValue() {
    if (this.id > 0 && this.mplsEvent != null) {
      //let title = this.helper.isEmpty(this.mplsEvent.eventDes) ? this.mplsEvent.eventTitleTxt
      //  : this.mplsEvent.eventTitleTxt + " : " + this.mplsEvent.eventDes; 
      this.helper.setFormControlValue("itemTitle", this.mplsEvent.eventTitleTxt);
      this.helper.setFormControlValue("eventDescription", this.mplsEvent.eventDes);
      this.helper.setFormControlValue("eventID", this.mplsEvent.eventId);
      this.helper.setFormControlValue("eventStatus", this.mplsEvent.eventStusId);
      this.helper.setFormControlValue("m5_no", this.mplsEvent.ftn);
      this.helper.setFormControlValue("charsID", this.mplsEvent.charsId);
      this.helper.setFormControlValue("h1", this.mplsEvent.h1);
      this.helper.setFormControlValue("h6", this.mplsEvent.h6);
      this.helper.setFormControlValue("workflowStatus", this.mplsEvent.wrkflwStusId);

      this.helper.setFormControlValue("customer.name", this.mplsEvent.custNme);
      this.helper.setFormControlValue("customer.email", this.mplsEvent.custEmailAdr);
      this.helper.setFormControlValue("customer.phone", this.mplsEvent.custCntctPhnNbr);
      this.helper.setFormControlValue("customer.cellphone", this.mplsEvent.custCntctCellPhnNbr);
      this.helper.setFormControlValue("customer.contactName", this.mplsEvent.custCntctNme);
      this.helper.setFormControlValue("customer.pager", this.mplsEvent.custCntctPgrNbr);
      this.helper.setFormControlValue("customer.pagerPin", this.mplsEvent.custCntctPgrPinNbr);

      this.helper.setFormControlValue("requestor.id", this.mplsEvent.reqorUser.userId);
      this.helper.setFormControlValue("requestor.name", this.mplsEvent.reqorUser.fullNme);
      this.helper.setFormControlValue("requestor.phone", this.mplsEvent.reqorUser.phnNbr);
      this.helper.setFormControlValue("requestor.cellphone", this.mplsEvent.reqorUser.cellPhnNbr);
      this.helper.setFormControlValue("requestor.email", this.mplsEvent.reqorUser.emailAdr);
      this.helper.setFormControlValue("requestor.pager", this.mplsEvent.reqorUser.pgrNbr);
      this.helper.setFormControlValue("requestor.pagerPin", this.mplsEvent.reqorUser.pgrPinNbr);
      this.helper.setFormControlValue("requestor.bridgeNumber", this.mplsEvent.cnfrcBrdgNbr);
      this.helper.setFormControlValue("requestor.bridgePin", this.mplsEvent.cnfrcPinNbr);

      this.helper.setFormControlValue("sales.id", this.mplsEvent.salsUser.userId);
      this.helper.setFormControlValue("sales.name", this.mplsEvent.salsUser.fullNme);
      this.helper.setFormControlValue("sales.phone", this.mplsEvent.salsUser.phnNbr);
      this.helper.setFormControlValue("sales.email", this.mplsEvent.salsUser.emailAdr);

      this.helper.setFormControlValue("design.publishedEmailCC", this.mplsEvent.pubEmailCcTxt);
      this.helper.setFormControlValue("design.completeEmailCC", this.mplsEvent.cmpltdEmailCcTxt);
      this.helper.setFormControlValue("design.designDocumentLocation", this.mplsEvent.docLinkTxt);
      this.helper.setFormControlValue("design.designDocumentApprovalNo", this.mplsEvent.ddAprvlNbr);
      this.helper.setFormControlValue("design.isNddUpdated", this.mplsEvent.nddUpdtdCd);

      this.helper.setFormControlValue("event.comments", this.mplsEvent.desCmntTxt);
      this.helper.setFormControlValue("event.mplsEventType", this.mplsEvent.mplsEventTypeId);
      this.helper.setFormControlValue("event.mplsActivityTypes", this.mplsEvent.mplsEventActyTypeIds);
      this.helper.setFormControlValue("event.vpnPlatformType", this.mplsEvent.vpnPltfrmTypeId);
      this.helper.setFormControlValue("event.vasTypes", this.mplsEvent.mplsEventVasTypeIds);
      this.helper.setFormControlValue("event.ipVersion", this.mplsEvent.ipVerId);
      this.helper.setFormControlValue("event.activityLocale", +this.mplsEvent.actyLocaleId);
      this.helper.setFormControlValue("event.multiVrfRequirement", this.mplsEvent.multiVrfReqId);
      this.helper.setFormControlValue("event.vrfName", this.mplsEvent.vrfNme);
      this.helper.setFormControlValue("event.mdsManaged", this.mplsEvent.mdsMngdCd);
      this.helper.setFormControlValue("event.addEndToEndMonitoring", this.mplsEvent.addE2eMontrgCd);
      this.helper.setFormControlValue("event.mdsVrfName", this.mplsEvent.mdsVrfNme);
      this.helper.setFormControlValue("event.mdsIpAddress", this.mplsEvent.mdsIpAdr);
      this.helper.setFormControlValue("event.mdsDlci", this.mplsEvent.mdsDlci);
      this.helper.setFormControlValue("event.mdsStaticRoute", this.mplsEvent.mdsStcRteDes);
      this.helper.setFormControlValue("event.onNetMonitoring", this.mplsEvent.onNetMontrgCd);
      this.helper.setFormControlValue("event.isWholesalePartner", this.mplsEvent.whlslPtnrCd);
      this.helper.setFormControlValue("event.wholesalePartner", this.mplsEvent.whlslPtnrId);
      this.helper.setFormControlValue("event.nniDesignDocument", this.mplsEvent.nniDsgnDocNme);
      this.helper.setFormControlValue("event.isCarrierPartner", this.mplsEvent.cxrPtnrCd);
      this.helper.setFormControlValue("event.carrierVendorPartner", this.mplsEvent.mplsCxrPtnrId);
      this.helper.setFormControlValue("event.offNetMonitoring", this.mplsEvent.offNetMontrgCd);
      this.helper.setFormControlValue("event.isMigration", this.mplsEvent.mgrtnCd);
      this.helper.setFormControlValue("event.migrationType", this.mplsEvent.mplsMgrtnTypeId);
      this.helper.setFormControlValue("event.relatedCompassNcr", this.mplsEvent.reltdCmpsNcrCd ? 1 : 0);
      this.helper.setFormControlValue("event.relatedCompassNcrName", this.mplsEvent.reltdCmpsNcrNme);
      this.helper.setFormControlValue("event.isEscalation", this.mplsEvent.esclCd);
      this.helper.setFormControlValue("event.escalationReason", this.mplsEvent.esclReasId);
      this.helper.setFormControlValue("event.primaryRequestDate", this.mplsEvent.primReqDt);
      this.helper.setFormControlValue("event.secondaryRequestDate", this.mplsEvent.scndyReqDt);

      this.helper.setFormControlValue("schedule.startDate", this.mplsEvent.strtTmst);
      this.helper.setFormControlValue("schedule.endDate", this.mplsEvent.endTmst);
      this.helper.setFormControlValue("schedule.eventDuration", this.mplsEvent.eventDrtnInMinQty);

      if (this.profile == EEventProfile.REVIEWER) {
        this.helper.setFormControlValue("reviewer.id", this.userSrvc.loggedInUser.userId);
        this.helper.setFormControlValue("reviewer.name", this.userSrvc.loggedInUser.fullName);
      }

      this.evntAsnToUserSrvc.getByEventId(this.mplsEvent.eventId).subscribe(
        res => {
          this.activators = res;
          this.helper.setFormControlValue("schedule.assignedToId", this.activators.map(a => a.asnToUserId).join(","))
          this.helper.setFormControlValue("schedule.assignedTo", this.activators.map(a => a.userDsplNme).join("; "))
          this.helper.setFormControlValue("schedule.displayedAssignedTo", this.activators.map(a => a.userDsplNme).join("; "))
        });

      this.workflowSrvc.getForEvent(this.eventType, this.mplsEvent.eventStusId).subscribe(
        res => {
          this.workflowStatusList = res as Workflow[];
          this.helper.setFormControlValue("workflowStatus", this.mplsEvent.wrkflwStusId);
          this.isRevActTask = (this.id > 0 && this.workflowStatusList.length > 0) ? true : false;
        }
      );
    }
  }

  getMplsEventFormValue(): MplsEvent {
    let mpls = new MplsEvent();

    mpls.eventTitleTxt = this.helper.getFormControlValue("customer.name") + " "
      + this.mplsEventTypeList.find(a => a.value == this.helper.getFormControlValue("event.mplsEventType")).description;
    mpls.eventDes = this.helper.getFormControlValue("eventDescription");
    mpls.eventStusId = this.helper.getFormControlValue("eventStatus");
    mpls.ftn = this.helper.getFormControlValue("m5_no");
    mpls.h1 = this.helper.getFormControlValue("h1");
    mpls.h6 = this.helper.getFormControlValue("h6");
    mpls.charsId = this.helper.getFormControlValue("charsID");
    mpls.wrkflwStusId = this.helper.getFormControlValue("workflowStatus");

    mpls.custNme = this.helper.getFormControlValue("customer.name");
    mpls.custEmailAdr = this.helper.getFormControlValue("customer.email");
    mpls.custCntctPhnNbr = this.helper.getFormControlValue("customer.phone");
    mpls.custCntctCellPhnNbr = this.helper.getFormControlValue("customer.cellphone");
    mpls.custCntctNme = this.helper.getFormControlValue("customer.contactName");
    mpls.custCntctPgrNbr = this.helper.getFormControlValue("customer.pager");
    mpls.custCntctPgrPinNbr = this.helper.getFormControlValue("customer.pagerPin");
    mpls.reqorUserId = this.helper.getFormControlValue("requestor.id");
    mpls.cnfrcBrdgNbr = this.helper.getFormControlValue("requestor.bridgeNumber");
    mpls.cnfrcPinNbr = this.helper.getFormControlValue("requestor.bridgePin");
    mpls.salsUserId = this.helper.getFormControlValue("sales.id");

    mpls.pubEmailCcTxt = this.helper.getFormControlValue("design.publishedEmailCC");
    mpls.cmpltdEmailCcTxt = this.helper.getFormControlValue("design.completeEmailCC");
    mpls.docLinkTxt = this.helper.getFormControlValue("design.designDocumentLocation");
    mpls.ddAprvlNbr = this.helper.getFormControlValue("design.designDocumentApprovalNo");
    mpls.nddUpdtdCd = this.helper.getFormControlValue("design.isNddUpdated");

    mpls.desCmntTxt = this.helper.getFormControlValue("event.comments");
    mpls.mplsEventTypeId = this.helper.getFormControlValue("event.mplsEventType");
    mpls.mplsEventActyTypeIds = this.helper.getFormControlValue("event.mplsActivityTypes");
    mpls.vpnPltfrmTypeId = this.helper.getFormControlValue("event.vpnPlatformType");
    mpls.mplsEventVasTypeIds = this.helper.getFormControlValue("event.vasTypes");
    mpls.ipVerId = this.helper.getFormControlValue("event.ipVersion");
    mpls.actyLocaleId = this.helper.getFormControlValue("event.activityLocale");
    mpls.multiVrfReqId = this.helper.getFormControlValue("event.multiVrfRequirement");
    mpls.vrfNme = this.helper.getFormControlValue("event.vrfName");
    mpls.mdsMngdCd = this.helper.getFormControlValue("event.mdsManaged");
    mpls.addE2eMontrgCd = this.helper.getFormControlValue("event.addEndToEndMonitoring");
    mpls.mdsVrfNme = this.helper.getFormControlValue("event.mdsVrfName");
    mpls.mdsIpAdr = this.helper.getFormControlValue("event.mdsIpAddress");
    mpls.mdsDlci = this.helper.getFormControlValue("event.mdsDlci");
    mpls.mdsStcRteDes = this.helper.getFormControlValue("event.mdsStaticRoute");
    mpls.onNetMontrgCd = this.helper.getFormControlValue("event.onNetMonitoring");
    mpls.whlslPtnrCd = this.helper.getFormControlValue("event.isWholesalePartner");
    mpls.whlslPtnrId = this.helper.getFormControlValue("event.wholesalePartner");
    mpls.nniDsgnDocNme = this.helper.getFormControlValue("event.nniDesignDocument");
    mpls.cxrPtnrCd = this.helper.getFormControlValue("event.isCarrierPartner");
    mpls.mplsCxrPtnrId = this.helper.getFormControlValue("event.carrierVendorPartner");
    mpls.offNetMontrgCd = this.helper.getFormControlValue("event.offNetMonitoring");
    mpls.mgrtnCd = this.helper.getFormControlValue("event.isMigration");
    mpls.mplsMgrtnTypeId = this.helper.getFormControlValue("event.migrationType");
    mpls.reltdCmpsNcrCd = this.helper.getFormControlValue("event.relatedCompassNcr");
    mpls.reltdCmpsNcrNme = this.helper.getFormControlValue("event.relatedCompassNcrName");

    mpls.mplsEventAccsTag = this.mplsEventAccessTagList;
    mpls.esclCd = (mpls.wrkflwStusId == WORKFLOW_STATUS.Reject
      || mpls.wrkflwStusId == WORKFLOW_STATUS.Reschedule) ? false : this.helper.getFormControlValue("event.isEscalation");
    mpls.esclReasId = mpls.esclCd ? this.helper.getFormControlValue("event.escalationReason") : null;
    mpls.primReqDt = mpls.esclCd ? new Date(this.helper.getFormControlValue("event.primaryRequestDate")) : null;
    mpls.scndyReqDt = mpls.esclCd ? new Date(this.helper.getFormControlValue("event.secondaryRequestDate")) : null;

    mpls.strtTmst = this.helper.getFormControlValue("schedule.startDate");
    mpls.endTmst = this.helper.getFormControlValue("schedule.endDate");
    mpls.eventDrtnInMinQty = this.helper.getFormControlValue("schedule.eventDuration");
    mpls.activators = this.getSelectedActivators(mpls.wrkflwStusId);

    if (this.profile == EEventProfile.ACTIVATOR) {
      mpls.eventSucssActyIds = this.helper.getFormControlValue("activators.selectedSuccessfulActs")
      mpls.eventFailActyIds = this.helper.getFormControlValue("activators.selectedFailedActs")
      mpls.preCfgConfgCode = this.helper.getFormControlValue("activators.preconfigureCompleted")
      //mpls.failCode = this.helper.getFormControlValue("activators.failedCode")
      //mpls.failCode = this.helper.isEmpty(mpls.failCode) ? 0 : mpls.failCode
      mpls.activatorComments = this.helper.getFormControlValue("activators.comments")
      mpls.activatorUserId = this.userSrvc.loggedInUser.userId
    }
    else if (this.profile == EEventProfile.REVIEWER) {
      let revId = this.helper.getFormControlValue("reviewer.id")
      mpls.reviewerUserId = this.helper.isEmpty(revId) ? 0 : revId
      mpls.reviewerComments = this.helper.getFormControlValue("reviewer.comments");
    }

    return mpls;
  }

  getSelectedActivators(wrkflwStusId: number) {
    if (wrkflwStusId != WORKFLOW_STATUS.Reject && wrkflwStusId != WORKFLOW_STATUS.Reschedule) {
      if (!this.helper.isEmpty(this.helper.getFormControlValue("schedule.displayedAssignedTo"))) {
        let mplsActivators = this.helper.getFormControlValue("schedule.assignedToId")
        if (mplsActivators != null) {
          let activators = mplsActivators.toString()
          if (activators.length > 0) {
            if (activators.indexOf(',') != -1) {
              return activators.toString().split(",").map(a => parseInt(a));
            } else {
              return [mplsActivators];
            }
          }
        }
      }
    }

    return [];
  }

  saveMplsTag(event: any) {
    // no need this function
    //console.log(event.data);
    //console.log(this.mplsEventAccessTagList);

    // Check if this tag already exist in list
    //let item = event.data as MplsEventAccessTag;
    //console.log(item);
    //console.log(this.mplsEventAccessTagList);
    //console.log(this.mplsEventAccessTagList.includes(item));
  }

  setMplsEventFormFields() {
    if (this.id > 0) {
      let eventStatusId = this.helper.getFormControlValue("eventStatus");
      let wfStatusId = this.helper.getFormControlValue("workflowStatus");
      if (this.profile == EEventProfile.ACTIVATOR) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Published) {
          this.allowEdit = true;
        } else {
          this.allowEdit = false;
        }
        
        // If not reviewer and EStus not equal to Inprogress or Published
        // Hide Launcher and Slot Picker
        if (this.editMode) {
          if (this.userSrvc.loggedInUser.profiles.includes('CAND Event Reviewer')
            && (this.mplsEvent.eventStusId == EVENT_STATUS.InProgress
              || this.mplsEvent.eventStusId == EVENT_STATUS.Published)) {
            this.option.schedule.checkCurrentSlot.isShown = true;
            this.option.schedule.launchPicker.isShown = true;
          }
          else {
            this.option.schedule.checkCurrentSlot.isShown = false;
            this.option.schedule.launchPicker.isShown = false;
          }

          // Set editable fields base on roles
          this.form.get("eventDescription").disable();
        }
      }
      else if (this.profile == EEventProfile.REVIEWER) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Completed) {
          this.allowEdit = false;
        } else {
          this.allowEdit = true;
        }
        // Show Launcher and Slot Picker
        if (this.editMode) {
          this.option.schedule.userFinder.isEnabled = true;
          this.option.schedule.checkCurrentSlot.isShown = true;
          this.option.schedule.launchPicker.isShown = true;
        }
        
      }
      else if (this.profile == EEventProfile.MEMBER) {
        if (eventStatusId == EVENT_STATUS.InProgress || eventStatusId == EVENT_STATUS.Completed) {
          this.allowEdit = false;
        }
        else if (eventStatusId == EVENT_STATUS.Published) {
          this.allowEdit = true;
        }
        else {
          if (this.mplsEvent != null && this.mplsEvent.entityBase != null
            && !this.helper.isEmpty(this.mplsEvent.entityBase.createdByUserName)) {
            if (this.mplsEvent.entityBase.createdByUserName != this.userSrvc.loggedInUser.adid) {
              if (wfStatusId == WORKFLOW_STATUS.Visible || wfStatusId == WORKFLOW_STATUS.Submit
                || eventStatusId == EVENT_STATUS.Rework) {
                this.allowEdit = true;
              } else {
                this.allowEdit = false;
              }
            } else {
              this.allowEdit = true;
            }
          } else {
            this.allowEdit = true;
          }
        }
        // Show Slot picker, Hide Launcher
        if (this.editMode) {
          this.option.schedule.checkCurrentSlot.isShown = true;
          this.option.schedule.launchPicker.isShown = false;
        }
      }
      else {
        // Not CAND Activator, Revieiwer or Member
        this.allowEdit = false;
        // Hide Launcher and Picker
        this.option.schedule.checkCurrentSlot.isShown = false;
        this.option.schedule.launchPicker.isShown = false;
      }
    }
    else {
      // Allow Edit set to true for New Event so that the buttons are seen
      this.allowEdit = true;
      if (this.profile == EEventProfile.REVIEWER) {
        this.option.schedule.checkCurrentSlot.isShown = true;
        this.option.schedule.launchPicker.isShown = true;
        this.option.schedule.userFinder.isEnabled = true;
      }
      else if (this.profile == EEventProfile.MEMBER) {
        this.option.schedule.checkCurrentSlot.isShown = true;
      }
    }
  }

  resetAssignee(event: any) {
    this.helper.setFormControlValue("schedule.assignedToId", "");
    this.helper.setFormControlValue("schedule.assignedTo", "");
    this.helper.setFormControlValue("schedule.displayedAssignedTo", "");
  }

  save() {
    let mpls = this.getMplsEventFormValue();
    console.log(mpls);
    this.isSubmitted = true;

    // No Validation for Rework (Retract, Resched, Return and Reject) statuses and Delete.
    if (mpls.wrkflwStusId != WORKFLOW_STATUS.Retract
      && mpls.wrkflwStusId != WORKFLOW_STATUS.Reschedule
      && mpls.wrkflwStusId != WORKFLOW_STATUS.Return
      && mpls.wrkflwStusId != WORKFLOW_STATUS.Reject
      && mpls.wrkflwStusId != WORKFLOW_STATUS.Delete) {

      // Validate form fields
      if (this.form.invalid) {
        this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate Activators
      if (mpls.activators.length == 0 && !mpls.esclCd
        && (mpls.wrkflwStusId == WORKFLOW_STATUS.Submit || mpls.wrkflwStusId == WORKFLOW_STATUS.Publish)) {
        this.helper.notify('Assigned Activator is required.', Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate StartDate for IsEscalation
      if (!mpls.esclCd) {
        if (mpls.wrkflwStusId == WORKFLOW_STATUS.Submit) {
          if (this.profile == EEventProfile.MEMBER && !this.helper.checkStartTime(48, mpls.strtTmst, mpls.endTmst)) {
            this.helper.notify('Start Date must be greater than 48 hours.', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        }
        else if (mpls.wrkflwStusId == WORKFLOW_STATUS.Publish) {
          if (!this.helper.checkStartTime(0, mpls.strtTmst, mpls.endTmst)) {
            this.helper.notify('Start Date cannot be in the past.', Global.NOTIFY_TYPE_WARNING);
            return;
          }
        }
      }
    }
    
    this.spinner.show();
    if (this.id > 0) {
      // Validate Event Delete Status
      let error = this.helper.validateEventDeleteStatus(mpls.wrkflwStusId, mpls.eventStusId,
        this.mplsEvent.entityBase.createdByUserName, this.userSrvc.loggedInUser.adid);
      if (!this.helper.isEmpty(error)) {
        this.spinner.hide();
        this.helper.notify(error, Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // Validate Reviewer Comments for Rejected Event
      //console.log('revTask: ' + this.isRevActTask + '; profile: ' + this.profile + '; wf: ' + mpls.wrkflwStusId + '; comment: ' + mpls.reviewerComments);
      if (this.isRevActTask && this.profile == EEventProfile.REVIEWER
        && mpls.wrkflwStusId == WORKFLOW_STATUS.Reject && this.helper.isEmpty(mpls.reviewerComments)) {
        this.spinner.hide();
        this.helper.notify("Reviewer Comments is required when the event is being rejected.", Global.NOTIFY_TYPE_WARNING);
        return;
      }

      // TODO: Check Double Booking

      // Update Mpls Event
      this.mplsEvntSrvc.update(this.id, mpls).subscribe(res => {
        this.helper.notifySavedFormMessage("MPLS Event", Global.NOTIFY_TYPE_SUCCESS, false, null);
      }, error => {
        this.helper.notifySavedFormMessage("MPLS Event", Global.NOTIFY_TYPE_ERROR, false, error);
        this.spinner.hide();
      }, () => {
        this.cancel();
        this.spinner.hide();
      });
    }
    else {
      // Create Mpls Event
      this.mplsEvntSrvc.create(mpls).subscribe(res => {
        this.helper.notifySavedFormMessage("MPLS Event", Global.NOTIFY_TYPE_SUCCESS, true, null);
      }, error => {
        this.helper.notifySavedFormMessage("MPLS Event", Global.NOTIFY_TYPE_ERROR, true, error);
        this.spinner.hide();
      }, () => {
        this.cancel();
        this.spinner.hide();
      });
    }
  }

  edit() {
    // Lock Event
    this.evntLockSrvc.lock(this.id).subscribe(
      res => {
        this.editMode = true;
        this.form.enable();
        this.form.get("itemTitle").disable();
        this.form.get("eventID").disable();
        this.form.get("eventStatus").disable();
        this.form.get("schedule.endDate").disable();
        if (this.mplsEvent.wrkflwStusId == WORKFLOW_STATUS.Visible
          || this.mplsEvent.eventStusId == EVENT_STATUS.Rework)
        {
          this.form.get("eventDescription").enable();
          this.form.get("event.comments").enable();
        }
        else if (this.mplsEvent.wrkflwStusId == WORKFLOW_STATUS.Submit)
        {
          this.form.get("eventDescription").disable();
          this.form.get("event.comments").disable();
        }
        this.option.requestor.userFinder.isEnabled = true;
        this.option.sales.userFinder.isEnabled = true;
        this.option.design.userFinderForPublishedEmail.isEnabled = true;
        this.option.design.userFinderForCompleteEmail.isEnabled = true;
        this.option.reviewer.userFinder.isEnabled = true;
        this.option.lookup_m5.isEnabled = true;
        this.setMplsEventFormFields();
      },
      error => {
        this.helper.notify('An error occurred while locking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  disableForm() {
    this.editMode = false;
    this.form.disable();
    this.form.get("schedule.displayedAssignedTo").disable();
    //console.log(this.form.controls.activators.controls.successfulActivities)
    this.option.schedule.checkCurrentSlot.isShown = false;
    this.option.schedule.launchPicker.isShown = false;
    this.option.requestor.userFinder.isEnabled = false;
    this.option.sales.userFinder.isEnabled = false;
    this.option.design.userFinderForPublishedEmail.isEnabled = false;
    this.option.design.userFinderForCompleteEmail.isEnabled = false;
    this.option.reviewer.userFinder.isEnabled = false;
    this.option.lookup_m5.isEnabled = false;
  }

  cancel() {
    if (this.id > 0 && this.editMode) {
      this.unlockEvent();
    } else {
      this.router.navigate(['/event/mpls/']);
    }
  }

  unlockEvent() {
    // Unlock Event
    if (this.id > 0) {
      this.evntLockSrvc.unlock(this.id).subscribe(
        res => {
          this.router.navigate(['/event/mpls/']);
        },
        error => {
          this.helper.notify('An error occurred while unlocking the Event.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }
}
