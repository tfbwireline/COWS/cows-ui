import { Component, OnInit } from '@angular/core';
import { EEventType } from "./../../../shared/global";
import { EventViewsService } from "./../../../services/";

@Component({
  selector: 'app-mpls',
  templateUrl: './mpls.component.html'
})
export class MplsComponent implements OnInit {
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;

  constructor() { }

  ngOnInit() {
    this.eventType = EEventType.MPLS;
    this.eventJobAid = "MPLS Event Job Aid for IPMs/Members";
    this.eventJobAidUrl = "http://webcontoo.corp.sprint.com/webcontoo/llisapi.dll/fetch/2000/1835597/1836298/14404/106652/106324/107866/106767/6074490/0069794_MPLS_and_MPLS_VAS_Events_in_Comprehensive_Order_Workflow_System_COWS_.pdf?nodeid=6449265&vernum=-2";
   }
}
