import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MdsEventService } from "./../../../services/";

@Component({
    selector: 'app-generate-ics',
    templateUrl: './generate-ics.component.html'
  })
export class GenerateICSComponent implements OnInit {

    constructor(private mdsEventService: MdsEventService, private router: Router, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {

        const eventId = this.activatedRoute.snapshot.queryParams["eid"] || 0;
        const mode = this.activatedRoute.snapshot.queryParams["mode"] || 0;
        const apptId = this.activatedRoute.snapshot.queryParams["apptId"] || 0;
        //const userIds = this.activatedRoute.snapshot.queryParams["uid"] || '';
        let fileName = (eventId > 0) ? `${eventId}.ics` : `${apptId}.ics` 

        this.mdsEventService.generateICSFile(eventId, mode, apptId).subscribe(file => {
            saveAs(file, fileName);
            this.router.navigate(['/home']);
        })
    }
}
