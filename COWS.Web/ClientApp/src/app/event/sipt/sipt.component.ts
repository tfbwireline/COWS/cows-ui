import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { EVENT_VIEWS, ENUM_OPTIONS, KeyValuePair, Global, EEventType } from "./../../../shared/global";

import { AdEventService, UserService, EventViewsService } from "./../../../services/";

@Component({
  selector: 'app-sipt',
  templateUrl: './sipt.component.html'
})
export class SiptComponent implements OnInit {
  loggedInUser = <any>[];
  eventViewsList = <any>[];
  eventType: number;
  eventJobAid: string;
  eventJobAidUrl: string;

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private userService: UserService, private eventService: EventViewsService) { }

  ngOnInit() {
    //this.getLoggedInUser();
    this.eventType = EEventType.SIPT;
    this.eventJobAid = "SIPT Event Job Aid for IPMs/Members";
    this.eventJobAidUrl = "https://webcontoo.corp.sprint.com/webcontoo/livelink/fetch/2000/1835597/1836298/14404/106652/106324/107866/106767/6074490/-/9233659_SIP_Trunking_COWS_Event_job_aid_for_IPMs.docx.pdf?nodeid=9233362&vernum=-2";   
  }
}
