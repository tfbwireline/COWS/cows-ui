import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidator } from '../../../shared/validator/custom-validator';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  UserService, SiptEventService, WorkflowService, SearchService,
  EventLockService
} from "./../../../services/";
import { SiptRltdOrdr, Workflow, SiptEvent, UserProfile, SiptEventDoc } from "./../../../models";
import {
  Global, EVENT_STATUS, KeyValuePair, EEventType, WORKFLOW_STATUS,
  EEventProfile
} from "./../../../shared/global";
import { Helper } from "./../../../shared";
import { zip } from "rxjs";
import { saveAs } from 'file-saver';
declare let $: any;

@Component({
  selector: 'app-sipt-form',
  templateUrl: './sipt-form.component.html'
})

export class SiptFormComponent implements OnInit {
  id: number = 0;
  siptEvent: SiptEvent;
  eventType: number = EEventType.SIPT;

  isCompleted: boolean = false;
  isSubmitted: boolean = false;
  isLocked: boolean = false;
  showEditButton: boolean = false;
  isActivator: boolean = false;
  isReviewer: boolean = false;
  isMember: boolean = false;

  siptProdTypeList: KeyValuePair[];
  siptTollTypeList: KeyValuePair[];
  siptActivityTypeList: KeyValuePair[];
  eventStatusList: KeyValuePair[];
  workflowStatusList: Workflow[];
  siptEventDocList: SiptEventDoc[] = [];
  siptRltdOrdr: SiptRltdOrdr[] = [];

  form: FormGroup;
  option: any;
  validationMessage: string;
  errors = [];
  mach5RelatedOrderError = [];
  
  isTnsChecked: boolean;
  filename: string = null;
  MysiptRltdOrdr: any[];
  MysiptActyType: any[];

  siptProdTypeId: any;
  IsSiptActvtyCheck: boolean = true;
  
  MyfocDate: any;
  profile: string = '';

  eventNote: string = '';
  defaultStart: Date = new Date();
  defaultEnd: Date = new Date();
  reqstrCntctTeamPdl: any;
  
  // For quick accessing control for the Validation Message
  get workflowStatus() { return this.form.get("workflowStatus") }
  get h1() { return this.form.get("customer.h1") }
  get h6() { return this.form.get("customer.h6") }
  get siteID() { return this.form.get("customer.siteID") }
  get productType() { return this.form.get("customer.productType") }
  get tollType() { return this.form.get("customer.tollType") }
  get siteContactName() { return this.form.get("customer.siteContactName") }
  get siteContactPhone() { return this.form.get("customer.siteContactPhone") }
  get eventStartDate() { return this.form.get("customer.startDate") }
  get eventEndDate() { return this.form.get("customer.endDate") }

  get siptActvty() { return this.form.get("event.siptActvty") }
  get siptDesignDoc() { return this.form.get("event.siptDesignDoc") }
  get comments() { return this.form.get("event.comments") }

  constructor(private avRoute: ActivatedRoute, private helper: Helper, private router: Router,
    private spinner: NgxSpinnerService, private userSrvc: UserService,
    private searchSrvc: SearchService, private siptService: SiptEventService,
    private workflowSrvc: WorkflowService, private evntLockSrvc: EventLockService) {
    this.download = this.download.bind(this);
  }

  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"] || 0;
    this.showEditButton = this.id > 0 ? true : false;
    // Updated by Sarah Sandoval [20210413] - Fixed past event issue
    this.defaultStart.setHours(this.defaultStart.getHours() + 1, 0, 0);
    this.defaultEnd.setHours(this.defaultStart.getHours() + 1, 0, 0);

    //this.defaultStart = new Date();
    //this.defaultStart.setUTCHours(0, 0, 0);
    //this.defaultEnd = new Date();
    //this.defaultEnd.setUTCHours(9, 0, 0);

    this.form = new FormGroup({
      eventID: new FormControl({ value: null, disabled: true }),
      itemTitle: new FormControl({ value: null, disabled: false }),
      eventStatus: new FormControl({ value: EVENT_STATUS.Visible, disabled: true }),
      workflowStatus: new FormControl({ value: 2, disabled: this.showEditButton }),
      customer: new FormGroup({
        id: new FormControl(),
        h1: new FormControl('', Validators.compose([Validators.required, CustomValidator.h1Validator])),
        h6: new FormControl('', Validators.compose([Validators.required, CustomValidator.h1Validator])),
        siteID: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50), Validators.minLength(3)])),
        charsID: new FormControl(),
        custName: new FormControl(),
        teamPDL: new FormControl(),
        productType: new FormControl({ value: null, disabled: this.showEditButton }, Validators.required),
        tollType: new FormControl({ value: null, disabled: this.showEditButton }),
        reqstrCntctName: new FormControl(),
        reqstrCntctPhne: new FormControl(),
        siptDocLoc: new FormControl(),
        address: new FormControl(),
        stateProvince: new FormControl(),
        bldgFloorRoom: new FormControl(),
        country: new FormControl(),
        city: new FormControl(),
        zip: new FormControl(),
        siteContactName: new FormControl('', Validators.required),
        siteContactPhone: new FormControl('', Validators.required),
        siteContactEmail: new FormControl(),
        siteContactHours: new FormControl(),
        startDate: new FormControl({ value: this.defaultStart, disabled: this.showEditButton }),
        endDate: new FormControl({ value: this.defaultEnd, disabled: this.showEditButton })
      }),
      event: new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        phone: new FormControl(),
        email: new FormControl(),
        siptActvty: new FormControl({ value: null, disabled: this.showEditButton }),
        mappedTg: new FormControl(false),
        pon: new FormControl(),
        foc: new FormControl({ value: null, disabled: this.showEditButton }),
        siptDesignDoc: new FormControl('', Validators.required),
        comments: new FormControl('', Validators.required)
      }),
    });

    this.option = {
      customer: {
        userFinder: {
          isEnabled: true,
          searchQuery: "reqstrCntctName",
          mappings: [
            ["id", "userId"],
            ["reqstrCntctName", "fullNme"],
            ["reqstrCntctPhne", "phnNbr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for customer" }
          ]
        },
        h1: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "H1 Name is required" }
          ]
        },
        h6: {
          isShown: true,
          validators: [
            { type: Validators.required, name: "required", message: "H6 Name is required" }
          ]
        },
        siteID: {
          isShown: true
        },
        charsID: {
          isShown: true
        },
        cutName: {
          isShown: true
        },
        teamPDL: {
          isShown: true
        },
        productType: {
          isShown: true
        },
        tollType: {
          isShown: false
        },
        reqstrCntctName: {
          isShown: true
        },
        reqstrCntctPhne: {
          isShown: true
        },
        siptDocLoc: {
          isShown: true
        },
        address: {
          isShown: true
        },
        stateProvince: {
          isShown: true
        },
        bldgFloorRoom: {
          isShown: true
        },
        country: {
          isShown: true
        },
        city: {
          isShown: true
        },
        zip: {
          isShown: true
        },
        siteContactName: {
          isShown: true
        },
        siteContactPhone: {
          isShown: true
        },
        siteContactEmail: {
          isShown: true
        },
        siteContactHours: {
          isShown: true
        },
        startDate: {
          isShown: true
        },
        endDate: {
          isShown: true
        }
      },
      event: {
        userFinder: {
          isEnabled: true,
          searchQuery: "name",
          mappings: [
            ["id", "userId"],
            ["name", "fullNme"],
            ["phone", "phnNbr"],
            ["email", "emailAdr"]
          ]
        },
        id: {
          validators: [
            { type: Validators.required, name: "required", message: "Please use the user picker for event" }
          ]
        },
        name: {
          isShown: true
        },
        phone: {
          isShown: true
        },
        email: {
          isShown: true
        },
        siptActvty: {
          isShown: true
        },
        pon: {
          isShown: true
        },
        foc: {
          isShown: true
        },
        comments: {
          isShown: true
        }
      },
      validationSummaryOptions: {
        title: "Validation Summary"
      }
    }

    // Get Lookups
    this.eventStatusList = this.helper.getEnumKeyValuePair(EVENT_STATUS);

    let data = zip(
      this.siptService.getSiptActyType(),
      this.siptService.getSiptProdType(),
      this.siptService.getSiptTollType(),
      this.searchSrvc.getSysCfgValue("UCaaSTeamPDL"),
      this.workflowSrvc.getForEvent(this.eventType, this.siptEvent == null ? EVENT_STATUS.Visible : this.siptEvent.eventStusId),
      this.userSrvc.getFinalUserProfile(this.userSrvc.loggedInUser.adid, EEventType.SIPT),
    )

    this.spinner.show();
    data.subscribe(
      res => {
        this.setFormControlValue("customer.id", this.userSrvc.loggedInUser.userId)
        this.setFormControlValue("customer.reqstrCntctName", this.userSrvc.loggedInUser.fullName)
        this.setFormControlValue("customer.reqstrCntctPhne", this.userSrvc.loggedInUser.phnNbr)
        this.siptActivityTypeList = res[0] as KeyValuePair[];
        this.siptProdTypeList = res[1] as KeyValuePair[];
        this.siptTollTypeList = res[2] as KeyValuePair[];
        this.reqstrCntctTeamPdl = res[3] as string;
        var workflows = res[4] as Workflow[];
        this.workflowStatusList = workflows

        var usrPrf = res[5] as UserProfile;
        if (usrPrf != null) {
          this.profile = this.helper.getFinalProfileName(res[5] as UserProfile);
        }

        if (this.id == 0 && this.siptEvent == null) {
          this.setFormControlValue("customer.teamPDL", this.reqstrCntctTeamPdl);
          if (this.profile == EEventProfile.MEMBER)
            this.workflowStatusList = workflows.filter(a => a.wrkflwStusId == WORKFLOW_STATUS.Visible
              || a.wrkflwStusId == WORKFLOW_STATUS.Retract || a.wrkflwStusId == WORKFLOW_STATUS.Submit
              || a.wrkflwStusId == WORKFLOW_STATUS.Delete) // Filter for SIPT Creation; Workflow Status
          if (this.profile == EEventProfile.REVIEWER)
            this.workflowStatusList = workflows.filter(a => a.wrkflwStusId == WORKFLOW_STATUS.Submit
              || a.wrkflwStusId == WORKFLOW_STATUS.Delete) // Filter for SIPT Creation; Workflow Status
          if (this.profile == EEventProfile.ACTIVATOR)
            this.workflowStatusList = workflows.filter(a => a.wrkflwStusId == WORKFLOW_STATUS.Submit
              || a.wrkflwStusId == WORKFLOW_STATUS.Delete) // Filter for SIPT Creation; Workflow Status
        }
      },
      error => {
        this.spinner.hide();
      },
      () => {
        // Get User Access
        this.isActivator = this.userSrvc.isSIPTnUCaaSActivator();
        this.isReviewer = this.userSrvc.isSIPTnUCaaSReviewer();
        this.isMember = this.userSrvc.isSIPTnUCaaSMember();

        if (this.id > 0) {
          this.siptService.getById(this.id).subscribe(
            res1 => {
              this.siptEvent = res1;
              console.log(this.siptEvent)
              this.setSiptEventFormValue();
              this.spinner.hide();
            },
            error => {
              this.spinner.hide();
              this.helper.notify('An error occurred while retrieving SIPT Event data.', Global.NOTIFY_TYPE_ERROR);
            });
        }
        else {
          // Disable Project Manager section for Event Member
          if (this.isMember && !this.isReviewer && !this.isActivator) {
            this.form.get('event.name').disable();
            this.form.get('event.phone').disable();
            this.form.get('event.email').disable();
            this.option.event.userFinder.isEnabled = false;
          }

          this.spinner.hide();
        }
      });
  }

  setSiptEventFormValue() {
    if (this.id > 0 && this.siptEvent != null) {
      this.setFormControlValue("eventID", this.siptEvent.eventId);
      this.setFormControlValue("itemTitle", this.siptEvent.eventTitleTxt);
      this.setFormControlValue("eventStatus", this.siptEvent.eventStusId);
      this.setFormControlValue("customer.id", this.siptEvent.reqorUserId)
      this.setFormControlValue("customer.reqstrCntctName", this.siptEvent.reqorUserName)
      this.setFormControlValue("customer.reqstrCntctPhne", this.siptEvent.reqorUserPhone)
      this.setFormControlValue("customer.h1", this.siptEvent.h1);
      this.setFormControlValue("customer.h6", this.siptEvent.h6);
      this.setFormControlValue("customer.siteID", this.siptEvent.siteId);
      this.setFormControlValue("customer.charsID", this.siptEvent.charsId);
      this.setFormControlValue("customer.custName", this.siptEvent.custNme);
      this.setFormControlValue("customer.teamPDL", this.siptEvent.teamPdlNme);
      this.setFormControlValue("customer.productType", this.siptEvent.siptProdTypeId);
      this.tollType.setValue(this.siptEvent.siptEventTollTypeIds);
      this.setFormControlValue("customer.siptDocLoc", this.siptEvent.siptDocLocTxt);

      this.siptRltdOrdr = this.siptEvent.siptReltdOrdr;

      this.setFormControlValue("customer.address", this.siptEvent.streetAdr);
      this.setFormControlValue("customer.stateProvince", this.siptEvent.sttPrvnNme);
      this.setFormControlValue("customer.bldgFloorRoom", this.siptEvent.flrBldgNme);
      this.setFormControlValue("customer.country", this.siptEvent.ctryRgnNme);
      this.setFormControlValue("customer.city", this.siptEvent.ctyNme);
      this.setFormControlValue("customer.zip", this.siptEvent.zipCd);
      this.setFormControlValue("customer.siteContactName", this.siptEvent.siteCntctNme);
      this.setFormControlValue("customer.siteContactPhone", this.siptEvent.siteCntctPhnNbr);
      this.setFormControlValue("customer.siteContactEmail", this.siptEvent.siteCntctEmailAdr);
      this.setFormControlValue("customer.siteContactHours", this.siptEvent.siteCntctHrNme);
      this.setFormControlValue("customer.startDate", this.siptEvent.custReqStDt);
      this.setFormControlValue("customer.endDate", this.siptEvent.custReqEndDt);

      this.siptActvty.setValue(this.siptEvent.siptEventActyIds);
      this.setFormControlValue("event.mappedTg", this.siptEvent.tnsTgCd);
      this.setFormControlValue("event.pon", this.siptEvent.prjId);
      this.setFormControlValue("event.foc", this.siptEvent.focDt);
      this.setFormControlValue("event.siptDesignDoc", this.siptEvent.siptDesgnDoc);
      this.setFormControlValue("event.comments", this.siptEvent.wrkDes);
      this.siptEventDocList = this.siptEvent.siptEventDoc;
      this.setFormControlValue("workflowStatus", this.siptEvent.wrkflwStusId);

      this.setFormControlValue("event.id", this.siptEvent.pmId)
      this.setFormControlValue("event.name", this.siptEvent.pmName)
      this.setFormControlValue("event.email", this.siptEvent.pmEmail)
      this.setFormControlValue("event.phone", this.siptEvent.pmPhone)

      this.isCompleted = this.form.get("eventStatus").value === EVENT_STATUS.Completed;
      if (this.isCompleted) {
        this.disableForm();
      }

      if (this.siptEvent.isLocked) {
        this.eventNote = "This event is currently locked by " + this.siptEvent.lockedBy;
        this.disableForm();
      }

      this.workflowSrvc.getForEvent(EEventType.SIPT, this.siptEvent.eventStusId).subscribe(
        res => {
          this.workflowStatusList = res as Workflow[];
          this.showEditButton = this.workflowStatusList.length > 0;
          if (this.workflowStatusList.findIndex(i => i.wrkflwStusId == this.siptEvent.wrkflwStusId) == -1) {
            this.workflowStatusList.push(new Workflow({
              wrkflwStusId: this.siptEvent.wrkflwStusId,
              wrkflwStusDes: WORKFLOW_STATUS[this.siptEvent.wrkflwStusId]
            }));
          }
          this.setFormControlValue("workflowStatus", this.siptEvent.wrkflwStusId);
        } 
      );
    }
  }

  edit() {
    // Lock Event
    this.evntLockSrvc.lock(this.id).subscribe(
      res => {
        this.showEditButton = false;
        this.form.get("itemTitle").enable();
        this.form.get("workflowStatus").enable();
        this.form.get("customer").enable();
        this.form.get("event").enable();

        // Disable Project Manager section for Event Member
        if (this.isMember && !this.isReviewer && !this.isActivator) {
          this.form.get('event.name').disable();
          this.option.event.userFinder.isEnabled = false;
        }

        // Always disabled since values here are dependent on PM Picker
        this.form.get('event.phone').disable();
        this.form.get('event.email').disable();
      },
      error => {
        this.helper.notify('An error occurred while locking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  disableForm() {
    this.showEditButton = true;
    this.isLocked = true;
    this.form.get("itemTitle").disable();
    this.form.get("workflowStatus").disable();
    this.form.get("customer").disable();
    this.form.get("event").disable();
  }

  cancel() {
    if (this.id > 0 && !this.showEditButton) {
      this.unlockEvent();
    }

    this.router.navigate(['/event/sipt/']);
  }

  unlockEvent() {
    // Unlock Event
    this.evntLockSrvc.unlock(this.id).subscribe(
      res => { },
      error => {
        this.helper.notify('An error occurred while unlocking the Event.', Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  // For MACH Order
  onRelatedOrderRowInserting(e: any) {
    // Added to prevent error on Template Id
    e.data.siptReltdOrdrId = this.getRelatedOrderId();
  }

  getRelatedOrderId(): number {
    if (this.siptRltdOrdr != null && this.siptRltdOrdr.length > 0) {
      let maxId = this.siptRltdOrdr.reduce((max, ordr) => ordr.siptReltdOrdrId > max ? ordr.siptReltdOrdrId : max, this.siptRltdOrdr[0].siptReltdOrdrId);
      return maxId + 1;
    } else {
      return 1;
    }
  }

  // For H6 Lookup
  H6lookup() {
    let h6 = !this.helper.isEmpty(this.h6.value) ? this.h6.value : this.siptEvent.h6;
    if (h6 != null) {
      // Both seems to be not working
      this.spinner.show()
      this.searchSrvc.getH6Lookup(h6, EEventType.SIPT, 0, false).subscribe(res => {
        let resultH6 = res as SiptEvent
        if (resultH6 != null && resultH6.h6 != null) {
          this.setFormControlValue("customer.charsID", resultH6.charsId)
          this.setFormControlValue("customer.siteID", resultH6.siteId)
          this.setFormControlValue("customer.h1", resultH6.h1)
          this.setFormControlValue("customer.h6", resultH6.h6)
          this.setFormControlValue("customer.custName", resultH6.custNme)
          this.setFormControlValue("customer.siteContactEmail", resultH6.siteCntctEmailAdr)
          this.setFormControlValue("customer.siteContactPhone", resultH6.siteCntctPhnNbr)
          this.setFormControlValue("customer.siteContactName", resultH6.siteCntctNme)
          this.setFormControlValue("customer.bldgFloorRoom", resultH6.flrBldgNme)
          this.setFormControlValue("customer.address", resultH6.streetAdr)
          this.setFormControlValue("customer.zip", resultH6.zipCd)
          this.setFormControlValue("customer.stateProvince", resultH6.sttPrvnNme)
          this.setFormControlValue("customer.city", resultH6.ctyNme)
          this.setFormControlValue("customer.country", resultH6.ctryRgnNme)
        }
        else {
          this.helper.notify('H6 Not Found ' + h6, Global.NOTIFY_TYPE_WARNING);
        }
      }, error => {
        this.spinner.hide()
          this.helper.notify('H6 Not Found ' + h6, Global.NOTIFY_TYPE_WARNING);
      }, () => this.spinner.hide());
    }
  }

  // For SIPT Documents
  download(e) {
    // this.spinner.show();
    let doc = e.row.data as SiptEventDoc;
    if (doc != null) {
      this.siptService.download(doc).subscribe(
        res => {
          if (res != null) {
            saveAs(res, doc.fileNme);
          }
        },
        error => {
          this.helper.notify("File not found", Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }

  formatFileSize(bytes: number = 0): string {
    return this.helper.getFileSizeFormat(bytes);
  }

  onFileLoad(result) {
    //console.log(result)
    if (result != null) {
      let file = result.file as File;
      //console.log(file);
      let allowedExtensions = /(\.xls|\.xlsx|\.csv)$/i;
      let fileSizeLimit = 10 * 1024 * 1024;
      //console.log(allowedExtensions.exec(file.name));
      if (!allowedExtensions.exec(file.name)) {
        this.helper.notify('File type is not allowed. Allowed file types are as follows: xls, xlsx, csv.', Global.NOTIFY_TYPE_WARNING);
        return;
      }
      else if (file.size > fileSizeLimit) {
        this.helper.notify('File size limit of 10MB for the following file types: .xls/.xlsx/.csv  ', Global.NOTIFY_TYPE_WARNING);
        return;
      }

      let doc = new SiptEventDoc();
      doc.siptEventDocId = this.getSiptEventDocId();
      doc.fileNme = file.name;
      doc.base64string = result.base64;
      doc.fileSizeQty = file.size;
      doc.creatByAdId = this.userSrvc.loggedInUser.adid;
      doc.creatDt = new Date();
      this.siptEventDocList.push(doc);
    }
  }

  getSiptEventDocId(): number {
    if (this.siptEventDocList != null && this.siptEventDocList.length > 0) {
      let maxId = this.siptEventDocList.reduce((max, doc) => doc.siptEventDocId > max ? doc.siptEventDocId : max, this.siptEventDocList[0].siptEventDocId);
      return maxId + 1;
    } else {
      return 1;
    }
  }

  getSiptEventFormValue() {
    let custName = this.getFormControlValue("customer.custName");
    const name = this.helper.isEmpty(custName) ? this.getTitle() : custName;
    const product = this.siptProdTypeList.find(x => x.value === this.productType.value);
    let productDescription = this.helper.isEmpty(product) ? '' : product.description;
    this.siptEvent.eventTitleTxt = `${name} - ${productDescription}`
    this.siptEvent.eventId = this.id;
    this.siptEvent.eventStusId = this.getFormControlValue("eventStatus");

    this.siptEvent.h1 = this.h1.value;
    this.siptEvent.h6 = this.h6.value;
    this.siptEvent.siteId = this.siteID.value;
    this.siptEvent.custNme = custName;
    this.siptEvent.charsId = this.getFormControlValue("customer.charsID");
    this.siptEvent.teamPdlNme = this.getFormControlValue("customer.teamPDL");
    this.siptEvent.siptProdTypeId = this.productType.value;
    this.siptEvent.siptEventTollTypeIds = this.tollType.value;
    this.siptEvent.reqorUserId = this.getFormControlValue("customer.id");
    this.siptEvent.siptDocLocTxt = this.getFormControlValue("customer.siptDocLoc");

    this.siptEvent.siptReltdOrdr = this.siptRltdOrdr;
    this.siptEvent.streetAdr = this.getFormControlValue("customer.address");
    this.siptEvent.flrBldgNme = this.getFormControlValue("customer.bldgFloorRoom");
    this.siptEvent.ctyNme = this.getFormControlValue("customer.city");
    this.siptEvent.sttPrvnNme = this.getFormControlValue("customer.stateProvince");
    this.siptEvent.ctryRgnNme = this.getFormControlValue("customer.country");
    this.siptEvent.zipCd = this.getFormControlValue("customer.zip");

    this.siptEvent.siteCntctNme = this.getFormControlValue("customer.siteContactName");
    this.siptEvent.siteCntctPhnNbr = this.getFormControlValue("customer.siteContactPhone");
    this.siptEvent.siteCntctEmailAdr = this.getFormControlValue("customer.siteContactEmail");
    this.siptEvent.siteCntctHrNme = this.getFormControlValue("customer.siteContactHours");
    let custReqStDt = !this.helper.isEmpty(this.eventStartDate.value) ? this.eventStartDate.value : this.defaultStart;
    let custReqEndDt = !this.helper.isEmpty(this.eventEndDate.value) ? this.eventEndDate.value : this.defaultEnd;
    this.siptEvent.custReqStDt = this.helper.dateObjectToString(custReqStDt);
    this.siptEvent.custReqEndDt = this.helper.dateObjectToString(custReqEndDt);

    this.siptEvent.siptEventActyIds = this.siptActvty.value;
    this.siptEvent.tnsTgCd = (this.siptEvent.siptEventActyIds.includes(4) || this.siptEvent.siptEventActyIds.includes(5))
      ? this.getFormControlValue("event.mappedTg") : false;
    this.siptEvent.prjId = this.getFormControlValue("event.pon");
    let focDate = this.getFormControlValue("event.foc");
    this.siptEvent.focDt = this.helper.isEmpty(focDate) ? null : this.helper.dateObjectToString(focDate);
    this.siptEvent.siptDesgnDoc = this.siptDesignDoc.value;
    this.siptEvent.wrkDes = (this.comments.value || "").replace(/\r?\n/g, "<br/>");

    this.siptEvent.siptEventDoc = this.siptEventDocList;
    this.siptEvent.pmId = this.getFormControlValue("event.id");
    this.siptEvent.pmName = this.getFormControlValue("event.name");
    this.siptEvent.pmPhone = this.getFormControlValue("event.phone");
    this.siptEvent.pmEmail = this.getFormControlValue("event.email");
    this.siptEvent.wrkflwStusId = this.getFormControlValue("workflowStatus");

    let ctryRgn = this.helper.isEmpty(this.siptEvent.ctryRgnNme) ? null : this.siptEvent.ctryRgnNme.trim().toUpperCase();
    if (this.helper.isEmpty(ctryRgn) || (ctryRgn == "US" || ctryRgn == "U.S" || ctryRgn == "USA" || ctryRgn == "U.S.A"
      || this.helper.isEmpty(ctryRgn) || ctryRgn == "UNITED STATE OF AMERICAN"
      || ctryRgn == "UNITED STATES" || ctryRgn == "UNITED STATES OF AMERICA")) {
      this.siptEvent.usIntlCd = "D";
    }
    else {
      this.siptEvent.usIntlCd = "I";
    }

    this.siptEvent.ntwkEngrId = this.siptEvent.ntwkEngrId;
    this.siptEvent.ntwkTechEngrId = this.siptEvent.ntwkTechEngrId;
    this.siptEvent.siptReltdOrdrIds = null;
    this.siptEvent.siptReltdOrdrNos = this.siptEvent.siptReltdOrdrNos;
  }

  save() {
    if (this.validateFormData()) {
      return; // Has error
    }

    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }
    
    if (this.form.valid) {
      this.isSubmitted = true;
      this.spinner.show();

      if (this.siptEvent == null) {
        this.siptEvent = new SiptEvent();
      }

      this.getSiptEventFormValue();

      if (this.id > 0) {
        // Update
        this.siptService.update(this.id, this.siptEvent).subscribe(res => {
          this.helper.notifySavedFormMessage(`SIPT Event - ${res.eventId}.`, Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.spinner.hide();
          this.cancel();
        }, error => {
          this.helper.notifySavedFormMessage(`SIPT Event - ${this.id}.`, Global.NOTIFY_TYPE_ERROR, false, error);
          this.spinner.hide();
        });
      }
      else {
        // Create
        this.siptService.create(this.siptEvent).subscribe(res => {
          if (res != null) {
            this.helper.notify(`Successfully created SIPT Event - ${res.eventId}.`, Global.NOTIFY_TYPE_SUCCESS);
          }
          else {
            this.helper.notifySavedFormMessage("SIPT Event", Global.NOTIFY_TYPE_SUCCESS, true, null)
          }
          this.spinner.hide();
          this.cancel();
        }, error => {
          this.helper.notifySavedFormMessage("SIPT Event", Global.NOTIFY_TYPE_ERROR, true, error);
          this.spinner.hide()
        });
      }
    }
  }

  validateFormData(): boolean {
    this.option.validationSummaryOptions.title = "Validation Summary";
    this.option.validationSummaryOptions.message = null;
    let errors = [];

    // Customer Info Section Validation
    var regexH1H6 = /^[0-9]{9}$/;
    if (this.helper.isEmpty(this.h1.value) || this.h1.value.trim() == "") {
      errors.push('H1 is required');
    }
    else if (!regexH1H6.test(this.h1.value)) {
      errors.push('H1 must be a 9 digit number');
    }

    if (this.helper.isEmpty(this.h6.value) || this.h6.value.trim() == "") {
      errors.push('H6 is required');
    }
    else if (!regexH1H6.test(this.h6.value.trim())) {
      errors.push('H6 must be a 9 digit number');
    }

    if (this.helper.isEmpty(this.siteID.value) || this.siteID.value.trim() == "") {
      errors.push('Site ID is required');
    }

    if (this.helper.isEmpty(this.productType.value)) {
      errors.push('SIPT Product Type is required');
    }

    if (this.helper.isEmpty(this.tollType.value) && this.productType.value == 9) {
      errors.push("Atleast one Toll Type needs to be selected.")
    }

    // Mach5 Related Order Validation
    let table = this.siptRltdOrdr;
    this.mach5RelatedOrderError = [];
    if (table.length <= 0) {
      errors.push("Mach5 Related Order Number Table is required and cannot be blank")
      this.mach5RelatedOrderError.push("Mach5 Related Order Number Table is required and cannot be blank");
    } else {
      if (table.filter(a => this.helper.isEmpty(a.m5OrdrNbr)).length > 0) {
        errors.push("Mach5 Related Order Number is required");
        this.mach5RelatedOrderError.push("Mach5 Related Order Number is required");
      }
    }

    if (this.helper.isEmpty(this.siteContactName.value) || this.siteContactName.value.trim() == "") {
      errors.push('Site Contact Name is required');
    }

    if (this.helper.isEmpty(this.siteContactPhone.value) || this.siteContactPhone.value.trim() == "") {
      errors.push('Site Contact Phone is required');
    }

    // Event Section Validation
    if (this.helper.isEmpty(this.siptActvty.value)) {
      errors.push('SIPT Activity Type is required');
    }
    else {
      if (this.siptActvty.value.length <= 0) {
        errors.push('SIPT Activity Type is required');
      }
    }

    if (this.helper.isEmpty(this.siptDesignDoc.value) || this.siptDesignDoc.value.trim() == "") {
      errors.push('SIPT Design Document is required');
    }

    if (this.helper.isEmpty(this.comments.value) || this.comments.value.trim() == "") {
      errors.push('Description and Comments is required');
    }

    var startDate = this.eventStartDate.value;
    var endDate = this.eventEndDate.value;
    if (this.helper.isEmpty(startDate) || this.helper.isEmpty(endDate)) {
      if (this.helper.isEmpty(startDate)) {
        errors.push('Customer Request Start Date is required');
      }
      else {
        if (this.helper.isEmpty(endDate)) {
          this.setFormControlValue("customer.endDate", this.helper.dateAdd(this.getFormControlValue("customer.startDate").setHours(17, 0, 0, 0), "day", 0));
        }
      }

      if (this.helper.isEmpty(endDate)) {
        errors.push('Customer Request End Date is required');
      }
    }
    else {
      // Added by Sarah Sandoval [20210305] - Added condition for past date but only for Visible/Submit Status as per Jagan
      let oldStartDate = this.id > 0 ? this.siptEvent.custReqStDt : null;
      let oldEndDate = this.id > 0 ? this.siptEvent.custReqEndDt : null;
      let dtNow = new Date();

      const errorStrtMessage = 'Customer Request Start Date cannot be in the past';
      if ((this.id == 0 || (this.id > 0 && oldStartDate != startDate))
        && (this.workflowStatus.value == WORKFLOW_STATUS.Visible || this.workflowStatus.value == WORKFLOW_STATUS.Submit)
        && startDate < dtNow && !errors.includes(errorStrtMessage)) {
        errors.push(errorStrtMessage);
      }

      const errorEndMessage = 'Customer Request End Date cannot be in the past';
      if ((this.id == 0 || (this.id > 0 && oldEndDate != endDate))
        && (this.workflowStatus.value == WORKFLOW_STATUS.Visible || this.workflowStatus.value == WORKFLOW_STATUS.Submit)
        && endDate < dtNow && !errors.includes(errorEndMessage)) {
        errors.push(errorEndMessage);
      }

      if (startDate > endDate) {
        errors.push('Customer Request End Date cannot be less than Customer Request Start Date');
      }
    }

    this.errors = errors;
    return this.errors.length > 0;
  }

  validateMach5RelatedOrder() {
    let table = this.siptRltdOrdr;
    let errors: string[] = []
    this.mach5RelatedOrderError = [];

    if (table.length == 0) {
      errors.push("Mach5 Related Order Number Table is required and cannot be blank")
    } else {
      if (table.filter(a => this.helper.isEmpty(a.m5OrdrNbr)).length > 0) {
        errors.push("Mach5 Related Order Number is required")
      }
    }
  }

  setDate() {
    if (this.id == 0 && this.siptEvent == null) {
      this.setFormControlValue("customer.startDate", this.defaultStart);
      this.setFormControlValue("customer.endDate", this.defaultEnd);
    }
  }

  setEndDate() {
    let endDate = new Date(this.eventStartDate.value);
    endDate.setHours(endDate.getHours() + 1, 0, 0)
    this.eventEndDate.setValue(endDate)
  }

  setFOCDate() {
    let end = this.helper.dateAdd(this.getFormControlValue("event.foc"), "day", null)
    this.setFormControlValue("event.foc", end)
  }

  onHistoryLookup() {
    this.helper.gotoEventHistory(this.id);
  }

  getTitle(): string {
    return this.siptProdTypeList.find(a => a.value == this.siptProdTypeId).description
  }
  
  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }
}
