import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from "rxjs";
import { CustomerUserProfile } from "../../../models/index";
import { TelcoService, CustomerUserProfileService, UserService, ManageUsersService } from '../../../services/index';
import { MaskUtil } from "../../../shared/mask/mask.util";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";
import { isEmpty } from 'rxjs/operators';
import { h5Services, mns, isip,status } from './h5.Services';
//import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'h5-customer-assgnmnt',
  templateUrl: './h5-customer-assgnmnt.component.html',
  providers: [h5Services]
})
export class CustomerUserProfileComponent implements OnInit {
  public items: Observable<CustomerUserProfile[]>;
  //usersForm: FormGroup;
  searchUsers = <any>[];
  //searchTerm: FormControl = new FormControl();
  public H5Customer = new CustomerUserProfile();
  //public h5Customer = new CustomerUserProfile();
  public selectedItemKeys: any[] = [];
  public custID: any;
  public H5custID: any;
  mns: mns[];
  isip: isip[];
  status: status[];
  //public phoneMask = MaskUtil.PHONE_MASK_GENERATOR;
  //phonePattern: any = /^\+\s*1\s*\(\s*[02-9]\d{2}\)\s*\d{3}\s*-\s*\d{4}$/;
  //phoneRules: any = {
  //  X: /[02-9]/
  //}
  constructor(
    private userService: UserService,
    private serviceH5: CustomerUserProfileService,
    private router: Router,
    private mnsService: h5Services,
    //private manageUserService: ManageUsersService,
    //private fb: FormBuilder,
    public helper: Helper, private spinner: NgxSpinnerService) { }

  public ngOnInit() {
    this.getH5User();
    this.getUsers();
    this.mns = this.mnsService.getMns();
    this.isip = this.mnsService.getIsips();
    this.status = this.mnsService.getStatus();
  }
  ClearForm() {
    //this.searchTerm = null;
    //this.userAdid = null;
    //this.selectedAssignmentKeys = '';
  }
  getUsers() {
    this.spinner.show();
    this.userService.getUsers().subscribe(
      res => {
        //console.log(res);
        this.searchUsers = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  
  public getH5User() {
    this.spinner.show();
    this.serviceH5.getCustomerUserProfile().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.custID = 0;
    this.H5custID = 0;
    this.H5Customer = new CustomerUserProfile();
    this.serviceH5.getCustomerUserProfile().subscribe(
      res => {
        this.items = res;
      },
      error => {
        this.spinner.hide();
      });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'H5Customer_' + this.helper.formatDate(new Date());
  }

  loadEditForm(event: any) {
    this.custID = event.data.custID;
    this.H5custID = event.data.usrH5Id;
  }
  customizeExcelCell(options) {
    if (options.gridCell.rowType === 'data') {
    //  if (options.gridCell.data.OrderDate < new Date(2014, 2, 3)) {
    //    options.font.color = '#AAAAAA';
    //  }
    //  if (options.gridCell.data.SaleAmount > 15000) {
    //    if (options.gridCell.column.dataField === 'Employee') {
    //      options.font.bold = true;
    //    }
    //    if (options.gridCell.column.dataField === 'SaleAmount') {
    //      options.backgroundColor = '#FFBB00';
    //      options.font.color = '#000000';
    //    }
    //  }
    }
  }
  saveForm(event: any) {
    this.spinner.show();
    //console.log(event.Data);
    //this.custID = event.data.custID;
    //this.H5custID = event.data.usrH5Id;
    //console.log(this.custID);
    //console.log(event.data.custID);

    // Is Update else New
    if(event.data === undefined) {
      this.custID = event.newData.custID === undefined ? event.oldData.custID : event.newData.custID;
    } else {
      this.custID = event.data.custID;
    }
   

    // Temp Useless code
    // this.custID = (this.custID==0)
    //   ? event.data.custID : this.custID;
    // this.custID = this.helper.isEmpty(this.custID)
    //   ? event.data.custID : this.custID;
    //////////////////////////////////

    this.H5custID = this.helper.isEmpty(this.H5custID)
      ? 0 : this.H5custID;
    //console.log(this.custID);
    //console.log(this.H5custID);
    //// Issue could be it look for Active H5 User but while updating it may look for InActive User
    this.serviceH5.CheckH5ById(this.custID).subscribe(
      data => {

        let bExistH5Customer = data;
        //console.log(bExistH5Customer);

        if (bExistH5Customer==true) {
          if (this.H5custID>0) {
            this.H5Customer = event.oldData;
            //console.log(this.H5Customer);
            //console.log(event.oldData);
            //console.log(event.newData);
            this.H5Customer.usrH5Id = this.helper.isEmpty(event.newData.usrH5Id)
              ? event.oldData.usrH5Id : event.newData.usrH5Id;

            this.H5Customer.custID = this.helper.isEmpty(event.newData.custID)
              ? event.oldData.custID : event.newData.custID;
            this.H5Customer.userId = this.helper.isEmpty(event.newData.userId)
              ? event.oldData.userId : event.newData.userId;
            this.H5Customer.usrNme = this.helper.isEmpty(event.newData.usrNme)
              ? event.oldData.usrNme : event.newData.usrNme;

            this.H5Customer.mns = this.helper.isEmpty(event.newData.mns)
              ? event.oldData.mns : event.newData.mns;
            this.H5Customer.mnsCd = this.helper.isEmpty(event.newData.mnsCd)
              ? event.oldData.mnsCd : event.newData.mnsCd;

            this.H5Customer.isip = this.helper.isEmpty(event.newData.isip)
              ? event.oldData.isip : event.newData.isip;
            this.H5Customer.isipCd = this.helper.isEmpty(event.newData.isipCd)
              ? event.oldData.isipCd : event.newData.isipCd;

            this.H5Customer.status = this.helper.isEmpty(event.newData.status)
              ? event.oldData.status : event.newData.status;
            this.H5Customer.statusId = this.helper.isEmpty(event.newData.statusId)
              ? event.oldData.statusId : event.newData.statusId;

            //console.log(this.H5custID);
            //console.log(this.H5Customer);
            this.serviceH5.updateCustomerUserProfile(this.H5custID, this.H5Customer).subscribe(
              data => {
                this.helper.notify(`Successfully updated ${this.H5Customer.custNme}.`,
                  Global.NOTIFY_TYPE_SUCCESS);
                this.clear();
                setTimeout(() => {
                  this.spinner.hide();
                }, 0);
              },
              error => {
                if (!this.helper.isEmpty(error.Message) && error.Message.indexOf('duplicate') !== -1) {
                  this.helper.notify(`${this.H5Customer.custNme} already exists.`, Global.NOTIFY_TYPE_ERROR);
                }
                else if (!this.helper.isEmpty(error) && error.indexOf('duplicate') !== -1) {
                  this.helper.notify(`${this.H5Customer.custNme} already exists.`, Global.NOTIFY_TYPE_ERROR);
                }
                else {
                  this.helper.notify(`Failed to update ${this.H5Customer.custNme}. ` + error,
                    Global.NOTIFY_TYPE_ERROR);
                }
                this.clear();
                this.spinner.hide();
              });



          }
          else {
            this.H5Customer = event.data;
            //this.telco.recStusId = this.telco.recStatus == true ? 1 : 0;
            //this.H5Customer.recStusId =  Global.REC_STATUS_ACTIVE;
            this.serviceH5.createCustomerUserProfile(this.H5Customer).subscribe(
              data => {
                this.helper.notify(`Successfully created ${this.H5Customer.custID}.`,
                  Global.NOTIFY_TYPE_SUCCESS);
                this.clear();
                setTimeout(() => {
                  this.spinner.hide();
                }, 0);
              },
              error => {
                if (!this.helper.isEmpty(error.Message) && error.Message.indexOf('duplicate') !== -1) {
                  this.helper.notify(`${this.H5Customer.custID} already exists.`, Global.NOTIFY_TYPE_ERROR);
                }
                else if (!this.helper.isEmpty(error) && error.indexOf('duplicate') !== -1) {
                  this.helper.notify(`${this.H5Customer.custID} already exists.`, Global.NOTIFY_TYPE_ERROR);
                }
                else {
                  this.helper.notify(`Failed to create ${this.H5Customer.custID}. ` + error, Global.NOTIFY_TYPE_ERROR);
                }
                this.clear();
                this.spinner.hide();
              });
          }
        } else {
          // this.H5Customer = event.newData;
          this.helper.notify(`The H6 ID entered was not found as valid.  H6 Id must have H6 Folder.`,
            Global.NOTIFY_TYPE_ERROR);
          this.clear();
          this.spinner.hide();

        }
      },
      error => {
            this.H5Customer = event.data;
            this.helper.notify(`Not a valid Customer ID Not a valid Customer ID Fail to Create User Assignment Details  ${this.H5Customer.custID}. ` + error,
              Global.NOTIFY_TYPE_ERROR);

            this.clear();
            this.spinner.hide();
      });
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  //deleteRows() {
  //  this.spinner.show();
  //  this.selectedItemKeys.forEach((key) => {
  //    this.service.deleteTelco(key).subscribe(
  //      res => {
  //        this.helper.notify(`Successfully Deactivated Selected Telco('s)`,
  //          Global.NOTIFY_TYPE_SUCCESS);
  //        this.clear();
  //      },
  //      error => {
  //        this.spinner.hide();
  //      });
  //  });
  //  this.spinner.hide();
  //}
}
