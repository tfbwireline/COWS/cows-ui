import { Injectable } from '@angular/core';

export class mns {
  ID: number;
  Name: string;
}
export class isip {
  ID: number;
  Name: string;
}
export class status {
  ID: number;
  Name: string;
}
let mnss: mns[] = [{
  "ID": 0,
  "Name": "No"
}, {
  "ID": 1,
  "Name": "Yes"
  }];

let isips: isip[] = [{
  "ID": 0,
  "Name": "No"
}, {
  "ID": 1,
  "Name": "Yes"
  }];

let statuss: status[] = [{
  "ID": 1,
  "Name": "Enabled"
}, {
  "ID": 0,
  "Name": "Disabled"
}];


@Injectable()
export class h5Services {
  getIsips() {
    return isips;
  }
  getStatus() {
    return statuss;
  }
  getMns() {
    return mnss;
  }
}
