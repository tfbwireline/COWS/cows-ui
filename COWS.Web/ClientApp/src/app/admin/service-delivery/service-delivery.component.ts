import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { ServiceDelivery } from "../../../models/index";
import { ServiceDeliveryService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-service-delivery',
  templateUrl: './service-delivery.component.html'
})

export class ServiceDeliveryComponent implements OnInit {
  items: Observable<ServiceDelivery[]>;
  serviceDelivery = new ServiceDelivery();
  selectedItemKeys: any[] = [];
  serviceDeliveryId: any;

  constructor(private service: ServiceDeliveryService, private router: Router,
    public helper: Helper, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getServiceDeliveries();
  }

  getServiceDeliveries() {
    this.spinner.show();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.serviceDeliveryId = 0;
    this.serviceDelivery = new ServiceDelivery();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.serviceDeliveryId = event.data.sprintCpeNcrId;
  }

  saveForm(event: any) {
    this.spinner.show();

    // For Edit
    if (this.serviceDeliveryId > 0) {
      this.serviceDelivery = event.oldData;
      this.serviceDelivery.sprintCpeNcrDes = this.helper.isEmpty(event.newData.sprintCpeNcrDes)
                                            ? event.oldData.sprintCpeNcrDes : event.newData.sprintCpeNcrDes;
      this.service.update(this.serviceDeliveryId, this.serviceDelivery).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.serviceDelivery.sprintCpeNcrDes,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.serviceDelivery.sprintCpeNcrDes,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.serviceDelivery = event.data;
      this.service.create(this.serviceDelivery).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.serviceDelivery.sprintCpeNcrDes,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.serviceDelivery.sprintCpeNcrDes,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
