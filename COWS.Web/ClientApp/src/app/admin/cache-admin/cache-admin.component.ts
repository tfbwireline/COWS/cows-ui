import { Component, OnInit, ViewChild } from '@angular/core';
import { DxSelectBoxModule, DxSelectBoxComponent } from 'devextreme-angular';
import { CacheAdminService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-cache-admin',
  templateUrl: './cache-admin.component.html'
})

export class CacheAdminComponent implements OnInit {
  name: string;
  validationMessage: string;
  cacheObjList: any[] = [];
  @ViewChild('form', { static: true }) public form: any;
  @ViewChild('cacheObj', { static: true }) public cacheObj: DxSelectBoxComponent;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private cacheAdminService: CacheAdminService) { }

  ngOnInit() {
    this.name = this.form.name;
    this.cacheAdminService.get().subscribe(res => {
      this.cacheObjList = res;
    });
  }

  onValueChanged(e: any) {
    if (!this.helper.isEmpty(e.value)) {
      this.form.name = e.value;
    }
  }

  reset() {
    this.cacheObj.instance.reset();
    this.form.name = null;
  }

  onClearCache() {
    this.name = this.form.name;
    if (this.name.length > 0) {
      this.cacheAdminService.removeCacheObject(this.name).subscribe(
        res => {
          if (res) {
            this.validationMessage = "Successfully removed " + this.name + " object from Cache.";
            this.helper.notify(this.validationMessage, Global.NOTIFY_TYPE_SUCCESS);
          }
          else {
            this.validationMessage = "No such Cache object found.";
            this.helper.notify(this.validationMessage, Global.NOTIFY_TYPE_WARNING);
          }
        },
        error => {
          this.spinner.hide();
          this.helper.notify('Failed to delete Cache Object', Global.NOTIFY_TYPE_ERROR);
        }, () => {
          this.reset();
          this.spinner.hide()
        });
    }
    else {
      this.validationMessage = "Please enter a valid value";
      this.helper.notify(this.validationMessage, Global.NOTIFY_TYPE_WARNING);
    }
  }
}
