import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { WorldHoliday } from "../../../models/index";
import { WorldHolidayService, CountryService, CityService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-world-holiday',
  templateUrl: './world-holiday.component.html'
})

export class WorldHolidayComponent implements OnInit {
  items: Observable<WorldHoliday[]>;
  worldHoliday = new WorldHoliday();
  countryList: Observable<any[]>;
  cityList: Observable<any[]>;
  selectedItemKeys: any[] = [];
  worldHolidayId: any;
  includeAll: boolean;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: WorldHolidayService, private countrySrvc: CountryService,
    private citySrvc: CityService) { }

  ngOnInit() {
    this.includeAll = false;
    this.getCountries();
    //this.getCities();
    this.getWorldHolidays();
  }

  valueChangedHandler(event: any) {
    this.includeAll = event.value;
    this.getWorldHolidays();
  }

  getWorldHolidays() {
    this.spinner.show();
    this.service.get(this.includeAll).subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  getCountries() {
    this.countrySrvc.get().subscribe(
      res => {
        this.countryList = res;
      });
  }

  //getCities() {
  //  this.citySrvc.get().subscribe(
  //    res => {
  //      this.cityList = res;
  //    });
  //}

  clear() {
    this.worldHolidayId = 0;
    this.worldHoliday = new WorldHoliday();
    this.service.get(this.includeAll).subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.worldHolidayId = event.key;
  }

  saveForm(event: any) {
    this.spinner.show();
    
    // For Edit
    if (this.worldHolidayId > 0) {
      this.worldHoliday = event.oldData;
      console.log(event)
      this.worldHoliday.wrldHldyDes = this.helper.isEmpty(event.newData.wrldHldyDes)
        ? event.oldData.wrldHldyDes : event.newData.wrldHldyDes;
      this.worldHoliday.wrldHldyDt = this.helper.isEmpty(event.newData.wrldHldyDt)
        ? event.oldData.wrldHldyDt : event.newData.wrldHldyDt;
      this.worldHoliday.ctryCd = this.helper.isEmpty(event.newData.ctryCd)
        ? event.oldData.ctryCd : event.newData.ctryCd;
      this.service.update(this.worldHolidayId, this.worldHoliday).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.worldHoliday.wrldHldyDes,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.worldHoliday.wrldHldyDes,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.worldHoliday = event.data;
      this.service.create(this.worldHoliday).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.worldHoliday.wrldHldyDes,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.worldHoliday.wrldHldyDes,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
