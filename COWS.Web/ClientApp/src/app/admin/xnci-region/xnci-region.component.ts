import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from "rxjs";
import { Country, Region } from "../../../models/index";
import { CountryService, RegionService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";
import DataSource from 'devextreme/data/data_source';
import ArrayStore from 'devextreme/data/array_store';

@Component({
  selector: 'app-xnci-region',
  templateUrl: './xnci-region.component.html'
})
export class XnciRegionComponent implements OnInit {
  public regions: Observable<Region[]>;
  public countries: Observable<Country[]>;
  public regionalCountries: Observable<Country[]>;
  public selectedCountryKeys: any[] = [];
  public selectedRegionalCountryKeys: any[] = [];
  public selectedRegionId: number;
  public successKeys: any[] = [];
  public errorKeys: any[] = [];

  constructor(private service: CountryService, private router: Router, public helper: Helper,
    private spinner: NgxSpinnerService, private regionService: RegionService) { }

  public ngOnInit() {
    this.getRegions();
  }

  public getRegions() {
    this.spinner.show();
    this.regionService.get().subscribe(
      res => {
        this.regions = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  selectedRegionChangedHandler(data: any) {
    this.clear();
    this.selectedRegionId = data.selectedRowKeys[0];
    this.getCountries();
  }

  clear() {
    this.selectedCountryKeys = [];
    this.selectedRegionalCountryKeys = [];
    this.successKeys = [];
    this.errorKeys = [];
  }

  public getCountries() {
    this.spinner.show();
    this.service.get().subscribe(
      res => {
        // Get All Country List with Region
        this.countries = res.filter(resp => !this.helper.isEmpty(resp.rgnId) && resp.rgnId != this.selectedRegionId);
        // Get Countries by Region
        this.regionalCountries = res.filter(resp => resp.rgnId == this.selectedRegionId);
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  selectedCountryChangedHandler(data: any) {
    this.selectedCountryKeys = data.selectedRowKeys;
  }

  selectedCountryRegionChangedHandler(data: any) {
    this.selectedRegionalCountryKeys = data.selectedRowKeys;
  }

  addToRegion() {
    this.spinner.show();
    if (this.selectedCountryKeys != null && this.selectedCountryKeys.length > 0) {
      var counter = this.selectedCountryKeys.length;

      this.selectedCountryKeys.forEach(item => {
        this.service.updateCountryRegion(item, this.selectedRegionId).subscribe(
          res => {
            this.successKeys.push(item);
            counter -= 1;
            if (counter === 0)
              this.end();
          },
          error => {
            this.errorKeys.push(item);
            counter -= 1;
            if (counter === 0)
              this.end();
          });
      });
    }
  }

  removeFromRegion() {
    this.spinner.show();
    if (this.selectedRegionalCountryKeys != null && this.selectedRegionalCountryKeys.length > 0) {
      var counter = this.selectedRegionalCountryKeys.length;

      this.selectedRegionalCountryKeys.forEach(item => {
        this.service.updateCountryRegion(item, 0).subscribe(
          res => {
            this.successKeys.push(item);
            counter -= 1;
            if (counter === 0)
              this.end();
          },
          error => {
            this.errorKeys.push(item);
            counter -= 1;
            if (counter === 0)
              this.end();
          });
      });
    }
  }

  end() {
    if (this.errorKeys != null && this.errorKeys.length > 0) {
      this.helper.notify(`Failed in updating region/s: ${this.errorKeys.join(',')}.`, Global.NOTIFY_TYPE_ERROR);
    } else {
      this.helper.notify(`Successfully updated region/s: ${this.successKeys.join(',')}.`, Global.NOTIFY_TYPE_SUCCESS);
    }
    this.getCountries();
    this.clear();
    this.spinner.hide();
  }
}
