import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { UnlockOrder } from "../../../models/index";
import { getUnlockItemsViewService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DxRadioGroupModule } from "devextreme-angular"
import { Observable } from 'rxjs/internal/Observable';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";
//import { UnlockOrder } from '../../../models';

@Component({
  selector: 'app-unlock-items',
  templateUrl: './unlock-items.component.html'
})
export class UnlockItemsComponent implements OnInit {

  public unlockOrders$: any[] = null;
  public unlockEvents$: any[] = null;
  public unlockRedesigns$: any[] = null;
  public unlockCPTs$: any[] = null;
  //public ordr: = new Observable<UnlockOrder[]>;
  //public ordr: Observable<UnlockOrder[]>;
  public ordr = new UnlockOrder();
  public refId: string = "C100515141325";
  public flag: number = 3;
  public isSubmitted = false;
  //public priorities: string[];
  public radioGroupItems = [
    { text: "Order",    color: "0" },
    { text: "Event",    color: "1" },
    { text: "Redesign", color: "2" },
    { text: "CPT",      color: "3" }
  ];
  public selectedItems = false;
  public displaySuccessMessage = false;
  public displayErrorMessage = false;
  public displayNotfound = false;

  public txtEmpty = true;
  public name: string;
  public ftn: string;
  public ordrId: string;
  public userId: string;
  public itemType: string;
  public selectedOption: number= 0;
  @ViewChild('form', { static: true }) public form: any;
  //buttonOptions: any = {
  //  text: "Unlock",
  //  type: "success",
  //  useSubmitBehavior: true
  //}
  constructor(private service: getUnlockItemsViewService, private router: Router, public helper: Helper,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    //this.getUnlockItems();
    //this.priorities = [
    //  "Order",
    //  "Event",
    //  "Redesign",
    //  "CPT"
    //];
  }
  clear() {
    this.displaySuccessMessage = false;
    this.displayErrorMessage = false;
    this.selectedItems = false;
    this.displayNotfound = false;
  }
  onSubmit($event) {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    this.spinner.show();
    //console.log("Hello Monir");
    this.ordr = new UnlockOrder();
    this.clear();
    if (this.selectedOption == 0) {    //Ordr
      if (this.unlockOrders$!=null) {
        //console.log(this.ordr);
        //console.log(this.unlockItems$);
        //this.ftn = "FTN: " +this.unlockItems$[0].ftn;
        //console.log(this.unlockItems$[0].ordrId);
        //console.log(this.ftn);
        //console.log(this.unlockItems$[0].ftn);
        //console.log(this.unlockItems$[0].lockByUserId);
        //console.log(this.unlockItems$[0].lockedBy);
        //console.log(this.unlockItems$[0].ordrId);
        //console.log(this.unlockItems$[0].productType);

        this.ftn = "FTN: " + this.unlockOrders$[0].ftn;
        this.ordrId = this.unlockOrders$[0].ordrId;
        this.userId = this.unlockOrders$[0].lockByUserId;
        this.itemType = "0";
        this.ordr.ordrId = this.unlockOrders$[0].ordrId
        this.ordr.LockByUserId = this.unlockOrders$[0].lockByUserId;
        this.ordr.productType = "0";
        //console.log(this.ordr.ordrId);
      }
    } else if (this.selectedOption == 1) {  //Event
      if (this.unlockEvents$ != null) {
        //console.log(this.unlockItems$);
        //this.ftn = "Event Id: " +this.unlockItems$[0].eventId;
        //console.log(this.ftn);
        //console.log(this.unlockItems$[0].lockedBy);
        //console.log(this.unlockItems$[0].eventType);

        this.ftn = "Event Id: " + this.unlockEvents$[0].eventId;
        this.ordrId = this.unlockEvents$[0].eventId;
        this.userId = this.unlockEvents$[0].lockByUserId;
        this.itemType = "1";
        this.ordr.ordrId = this.unlockEvents$[0].eventId
        this.ordr.LockByUserId = this.unlockEvents$[0].lockByUserId;
        this.ordr.productType = "1";
        //console.log(this.ordr.ordrId);
      }
    } else if (this.selectedOption == 2) { //Redesign
      if (this.unlockRedesigns$ != null) {
        //console.log(this.unlockItems$);
        //this.ftn = "Redesign Number: " + this.unlockItems$[0].redsgnNumber;
        //console.log(this.ftn);
        //console.log(this.unlockItems$[0].lockedBy);
        //console.log(this.unlockItems$[0].redsgnType);
        //console.log(this.unlockItems$[0].redsgnId);

        this.ftn = "Redesign Number: " + this.unlockRedesigns$[0].redsgnNumber;
        this.ordrId = this.unlockRedesigns$[0].redsgnId;
        this.userId = this.unlockRedesigns$[0].lockByUserId;
        this.itemType = "2";
        this.ordr.ordrId = this.unlockRedesigns$[0].redsgnId
        this.ordr.LockByUserId = this.unlockRedesigns$[0].lockByUserId;
        this.ordr.productType = "2";
        //console.log(this.ordr.ordrId);
      }
    } else {  //3= CPT
      //console.log(this.unlockItems$);
      //this.ftn = "CPT Customer Number: " + this.unlockItems$[0].cptCustNumber;
      //console.log(this.ftn);
      //console.log(this.unlockItems$[0].lockedBy);
      //console.log(this.unlockItems$[0].cptId);
      //console.log(this.unlockItems$[0].custName);

      this.ftn = "CPT Customer Number: " + this.unlockCPTs$[0].cptCustNumber;
      this.ordrId = this.unlockCPTs$[0].cptId;
      this.userId = this.unlockCPTs$[0].lockByUserId;
      this.itemType = "3";

      this.ordr.ordrId = this.unlockCPTs$[0].cptId
      this.ordr.LockByUserId = this.unlockCPTs$[0].lockByUserId;
      this.ordr.productType = "3";
      //console.log(this.ordr.ordrId);
    }
    //console.log(this.ordr)
    this.service.unlockItems(this.ordr).subscribe(
      data => {
        this.helper.notify(`Successfully Unlocked ${this.ftn}.`,
          Global.NOTIFY_TYPE_SUCCESS);
        
        this.clear();
        this.displaySuccessMessage = true;

        setTimeout(() => {
          this.spinner.hide();
        }, 0);
      },
      error => {
        if (!this.helper.isEmpty(error.Message) && error.Message.indexOf('duplicate') !== -1) {
          this.helper.notify(`${this.ftn} already exists.`, Global.NOTIFY_TYPE_ERROR);
        }
        else if (!this.helper.isEmpty(error) && error.indexOf('duplicate') !== -1) {
          this.helper.notify(`${this.ftn} already exists.`, Global.NOTIFY_TYPE_ERROR);
        }
        else {
          this.helper.notify(`Failed to create ${this.ftn}. ` + error, Global.NOTIFY_TYPE_ERROR);
        }

        this.clear();
        this.displayErrorMessage = true;
        this.spinner.hide();
      });
    this.spinner.hide();
  }
  //event handler for the radio button's change event
  onValueChanged($event) {
    //update the ui
    this.selectedOption = Number($event.value);
    //console.log(this.selectedOption); 
  }
  signalSelected(nameVal: string) {
    nameVal = this.form.name
    if (nameVal == "") {
      this.txtEmpty = false;
      this.clear();
      this.selectedItems = false;
      
    }
    //console.log(nameVal); // it is the value
    //console.log(this.selectedItems); // it is the value
  }
  onKey(event: any) { // without type info
    if (event.target.value = "") {
      //console.log(event.target.value);
      this.clear();
      this.selectedItems = false;
      //console.log(this.selectedItems);
    }
  }
  onSubmitSearch() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    this.spinner.show();
    this.refId = this.form.name;
    this.flag = this.selectedOption;
    //console.log(this.refId);
    //console.log(this.flag);
    this.displayNotfound = false;
    this.service.getlockItems(this.refId, this.flag).subscribe(
      res => {
        if (this.selectedOption == 0) {    //Ordr
          this.unlockOrders$ = res;
          for (let resultItem of this.unlockOrders$) {
            this.clear();
            this.selectedItems = true;
            this.unlockRedesigns$ = null;
            this.unlockEvents$ = null;
            this.unlockCPTs$ = null;
          }
        } else if (this.selectedOption == 1) {  //Event
          this.unlockEvents$ = res;
          for (let resultItem of this.unlockEvents$) {
            this.clear();
            this.selectedItems = true;
            this.unlockOrders$ = null;
            this.unlockRedesigns$ = null;
            this.unlockCPTs$ = null;
          }
        } else if (this.selectedOption == 2) { //Redesign
          this.unlockRedesigns$ = res;
          for (let resultItem of this.unlockRedesigns$) {
            this.clear();
            this.selectedItems = true;
            this.unlockOrders$ = null;
            this.unlockEvents$ = null;
            this.unlockCPTs$ = null;
          }
        } else {  //3= CPT
          this.unlockCPTs$ = res;
          for (let resultItem of this.unlockCPTs$) {
            this.clear();
            this.selectedItems = true;
            this.unlockOrders$ = null;
            this.unlockEvents$ = null;
            this.unlockRedesigns$ = null;
          }
        }

        //this.selectedItems = true;
        //console.log(res);
        //console.log(this.ordr);
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        this.clear();
        this.displayNotfound = true;
      });
  }
  //public getUnlockItems() {
  //  this.spinner.show();
  //  this.service.getlockItems(this.refId, this.flag).subscribe(
  //    res => {
  //      this.unlockItems$ = res;
  //      for (let resultItem of this.unlockItems$) {
  //        this.selectedItems = true;
  //      }
  //      //this.selectedItems = true;
  //      console.log("unlockItems$" + res);
  //      this.spinner.hide();
  //    },
  //    error => {
  //      this.spinner.hide();
  //    });
  //}

}
