import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { SprintHoliday } from "../../../models/index";
import { SprintHolidayService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-sprint-holiday',
  templateUrl: './sprint-holiday.component.html'
})

export class SprintHolidayComponent implements OnInit {
  items: Observable<SprintHoliday[]>;
  holidays: SprintHoliday[];
  sprintHoliday = new SprintHoliday();
  selectedItemKeys: any[] = [];
  sprintHolidayId: any;
  includeAll: boolean;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: SprintHolidayService) { }

  ngOnInit() {
    this.includeAll = false;
    this.getSprintHolidays();
  }

  valueChangedHandler(event: any) {
    this.includeAll = event.value;
    this.getSprintHolidays();
  }

  getSprintHolidays() {
    this.spinner.show();
    this.service.get(this.includeAll).subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.sprintHolidayId = 0;
    this.sprintHoliday = new SprintHoliday();
    this.service.get(this.includeAll).subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.sprintHolidayId = event.data.sprintHldyId;
  }

  saveForm(event: any) {
    this.spinner.show();

    // For Edit
    if (this.sprintHolidayId > 0) {
      this.sprintHoliday = event.oldData;
      this.sprintHoliday.sprintHldyDes = this.helper.isEmpty(event.newData.sprintHldyDes)
        ? event.oldData.sprintHldyDes : event.newData.sprintHldyDes;
      this.sprintHoliday.sprintHldyDt = this.helper.isEmpty(event.newData.sprintHldyDt)
        ? event.oldData.sprintHldyDt : event.newData.sprintHldyDt;
      this.service.update(this.sprintHolidayId, this.sprintHoliday).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.sprintHoliday.sprintHldyDes,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.sprintHoliday.sprintHldyDes,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.sprintHoliday = event.data;
      this.service.create(this.sprintHoliday).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.sprintHoliday.sprintHldyDes,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.sprintHoliday.sprintHldyDes,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
