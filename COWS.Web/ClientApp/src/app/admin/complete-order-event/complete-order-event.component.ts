import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { CompleteOrderService,UserService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { DxRadioGroupModule } from "devextreme-angular"
import { Observable } from 'rxjs/internal/Observable';
import { Helper } from './../../../shared';
import { Global ,ELkSysCfgMenus} from "../../../shared/global";
import { CompleteOrderEvent } from "../../../models";

 

@Component({
  selector: 'app-complete-order-event',
  templateUrl: './complete-order-event.component.html'
})
export class CompleteOrderEventComponent implements OnInit{
items: Observable<CompleteOrderEvent[]>;
 public radioGroupItems = [
    { text: "Order",    color: "0" },
    { text: "Event",    color: "1" }
  ];
public completeOrderEvent = new CompleteOrderEvent();
public selectedItems = false;
  public displaySuccessOrderMessage = false;
  public displaySuccessEventMessage = false;
  public displayErrorMessage = false;
  public displayNotfound = false;
 public selectedOption: number= 0;
 public refId: string;
validationMessage: string;
validMenu: string;
  public txtEmpty = true;
  public name: string;
 @ViewChild('form', { static: true }) public form: any;
  constructor(private service: CompleteOrderService,private userService: UserService, private router: Router, public helper: Helper,private spinner: NgxSpinnerService) { }

 

  ngOnInit() {
    this.userService.GetAdminExcPerm(ELkSysCfgMenus.CompleteOrderEvent).subscribe(hasAccess => {
      if(!hasAccess) {
        this.router.navigate(['/sorry']);
      }
    })
this.init();
  }
init() {
    
this.refId = this.form.name;
this.completeOrderEvent.msgType = this.selectedOption;
    this.completeOrderEvent.ftn = this.refId;
   // this.service.M5CmpltMsg(this.completeOrder).subscribe(res => {
    //  this.items = res ;
   // }, error => {
    //  this.spinner.hide();
   // }, () => this.spinner.hide());
  }
 clear() {
    this.displaySuccessOrderMessage = false;
    this.displaySuccessEventMessage = false;
    this.displayErrorMessage = false;
    this.selectedItems = false;
    this.displayNotfound = false;
  }
onValueChanged($event) {
    //update the ui
    this.selectedOption = Number($event.value);
    //console.log(this.selectedOption); 
  }
onKey(event: any) { // without type info
    if (event.target.value = "") {
      //console.log(event.target.value);
      this.clear();
      this.selectedItems = false;
      //console.log(this.selectedItems);
    }
}

 reset() {
    this.selectedItems = false;
    this.refId = null;
  }

onSubmitSearch() {
console.log("displaySuccessEventMessage", this.displaySuccessEventMessage);
console.log("displaySuccessOrderMessage", this.displaySuccessOrderMessage);
    this.displaySuccessOrderMessage = false;
    this.displaySuccessEventMessage = false;
    this.displayErrorMessage = false;
    this.displayNotfound = false;
    this.refId = this.form.name;
    this.completeOrderEvent.msgType = this.selectedOption;
    this.completeOrderEvent.ftn = this.refId;
    this.service.M5CmpltMsg(this.completeOrderEvent).subscribe(
      res => {
        if (res == 1) {
          this.displaySuccessOrderMessage = true;  
         this.validationMessage ="Complete message for MNS and/or MSS was sent back to M5"
          }
         if(res == 2)  { 
         this.validationMessage ="Complete message for Order was sent back to M5"
         this.displaySuccessEventMessage = true;
         
               }
        else if (res !=1 && res!=2) {
           this.validationMessage ="Error occurred. Message not sent to M5."
         this.displayErrorMessage = true;
            
                }
         },
      error => {
        this.spinner.hide();
         }, () => {
          this.reset();
          this.spinner.hide()
      });
  }  
}
