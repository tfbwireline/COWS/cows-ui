import { Component, Inject, OnInit } from '@angular/core';
import { PeopleSoftInterfaceViewService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-people-soft-interface-view',
  templateUrl: './people-soft-interface-view.component.html'
})
export class PeopleSoftInterfaceViewComponent implements OnInit {

  public psftInterfaceView$: any[];

  constructor(private psftService: PeopleSoftInterfaceViewService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getSCMInterfaceView();
  }

  public getSCMInterfaceView() {
    this.spinner.show();
    this.psftService.getSCMInterfaceView().subscribe(
      res => {
        this.psftInterfaceView$ = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

}
