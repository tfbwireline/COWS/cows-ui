import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { AccessCitySite, Vendor } from "../../../models/index";
import { AccessCitySiteService, CountryService, VendorService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-vendor-management',
  templateUrl: './vendor-management.component.html'
})

export class VendorManagementComponent implements OnInit {
  // Vendor Properties
  vendorItems: Vendor[] = [];
  vendor = new Vendor();
  selectedVendorItemKeys: any[] = [];
  vendorCode: any;
  countryList: Observable<any[]>;
  allowEditVendorCode: boolean = false;

  // Access City Site Properties
  items: Observable<AccessCitySite[]>;
  accessCitySite = new AccessCitySite();
  selectedItemKeys: any[] = [];
  accessCitySiteId: any;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: AccessCitySiteService, private vendorService: VendorService,
    private countrySrvc: CountryService) { }

  ngOnInit() {
    this.getVendors();
    this.getCountries();
    this.getAccessCitySites();
  }

  //** Vendor Functions **//

  getVendors() {
    this.spinner.show();
    this.vendorService.get().subscribe(
      res => {
        this.vendorItems = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  getCountries() {
    this.countrySrvc.get().subscribe(
      res => {
        this.countryList = res;
      });
  }

  allowUpdating(e) {
    if (e.row.rowType === "data") { return (e.row.data.vndrCd.length < 6); }
    else { false; }
  }

  onVendorToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'gridNote'
    });
  }

  onVendorNewRow(event) {
    event.data.recStatus = true;
    this.allowEditVendorCode = true;
  }

  clearVendorForm() {
    this.vendorCode = "";
    this.vendor = new Vendor();
    this.vendorService.get().subscribe(
      res => {
        this.vendorItems = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  loadVendorEditForm(event: any) {
    this.vendorCode = event.key;
    this.allowEditVendorCode = false;
  }

  saveVendorForm(event: any) {
    this.spinner.show();

    // For Insert
    if (this.helper.isEmpty(this.vendorCode)) {
      this.vendor = event.data;
      this.vendor.recStusId = this.vendor.recStatus == true ? Global.REC_STATUS_ACTIVE : Global.REC_STATUS_INACTIVE;
      this.vendorService.create(this.vendor).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.vendor.vndrCd, Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clearVendorForm();
        },
        error => {
          this.helper.notifySavedFormMessage(this.vendor.vndrCd, Global.NOTIFY_TYPE_ERROR, true, error);
          this.clearVendorForm();
        });
    }
    // For Update
    else {
      this.vendor = event.newData;
      this.vendor.vndrCd = this.helper.isEmpty(event.newData.vndrCd)
        ? event.oldData.vndrCd : event.newData.vndrCd;
      this.vendor.vndrNme = this.helper.isEmpty(event.newData.vndrNme)
        ? event.oldData.vndrNme : event.newData.vndrNme;
      this.vendor.ctryCd = this.helper.isEmpty(event.newData.ctryCd)
        ? event.oldData.ctryCd : event.newData.ctryCd;
      this.vendor.recStatus = this.helper.isEmpty(event.newData.recStatus)
        ? event.oldData.recStatus : event.newData.recStatus;
      console.log(event)
      console.log(this.vendor.recStatus)
      this.vendor.recStusId = this.vendor.recStatus == true ? Global.REC_STATUS_ACTIVE : Global.REC_STATUS_INACTIVE;
      this.vendorService.update(this.vendorCode, this.vendor).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.vendor.vndrCd, Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clearVendorForm();
        },
        error => {
          this.helper.notifySavedFormMessage(this.vendor.vndrCd, Global.NOTIFY_TYPE_ERROR, false, error);
          this.clearVendorForm();
        });
    }
  }

  vendorSelectionChangedHandler(event) {
    this.selectedVendorItemKeys = event.selectedRowKeys;
  }

  deleteVendorRows() {
    this.spinner.show();
    var hasError = false;
    var errorVendorKeys: any[] = [];
    this.selectedVendorItemKeys.forEach((key) => {
      this.vendorService.delete(key).subscribe(
        res => {
          this.clearVendorForm();
        },
        error => {
          errorVendorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorVendorKeys.length == this.selectedVendorItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedVendorItemKeys.length.toString(), false);
    }
  }


  //** Access City Site Functions **//

  getAccessCitySites() {
    this.spinner.show();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.accessCitySiteId = 0;
    this.accessCitySite = new AccessCitySite();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.accessCitySiteId = event.data.accsCtySiteId;
  }

  saveForm(event: any) {
    this.spinner.show();

    // For Edit
    if (this.accessCitySiteId > 0) {
      this.accessCitySite = event.oldData;
      this.accessCitySite.accsCtyNmeSiteCd = this.helper.isEmpty(event.newData.accsCtyNmeSiteCd)
        ? event.oldData.accsCtyNmeSiteCd : event.newData.accsCtyNmeSiteCd;
      this.service.update(this.accessCitySiteId, this.accessCitySite).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.accessCitySite.accsCtyNmeSiteCd,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.accessCitySite.accsCtyNmeSiteCd,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.accessCitySite = event.data;
      this.service.create(this.accessCitySite).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.accessCitySite.accsCtyNmeSiteCd,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.accessCitySite.accsCtyNmeSiteCd,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(event: any) {
    this.selectedItemKeys = event.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
