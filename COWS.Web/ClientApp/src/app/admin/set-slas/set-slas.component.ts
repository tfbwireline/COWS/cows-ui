import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { User, XnciMsSla, PlatForm } from "../../../models/index";
import { UserService,SetSlasService,XnciMsService,PlatformService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-set-sla',
  templateUrl: './set-slas.component.html'
})
export class XnciMsSlaComponent implements OnInit {
  items: Observable<XnciMsSla[]>;
  SetSlas = new XnciMsSla();
  ms: Observable<any[]>;
  userData = new User();
  pltfrm: Array<PlatForm[]>;
  selectedItemKeys: any[] = [];
  msSlaId: any;

  constructor(private userService: UserService, private slaService: SetSlasService,
    private msService: XnciMsService, private pltFormService: PlatformService, private router: Router,
    public helper: Helper, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getSlaSrvc();
    this.getMs();
    this.getPlatForm();
    this.getUser();
  }

  searchUser(userId: number) {
    this.spinner.show();
    this.userService.getByUserID(userId).toPromise().then(
      res => {
        this.userData = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  getUser() {
    this.spinner.show();
    this.userService.getUsers().subscribe(
        res => {
          this.userData = res;
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
        });
  }

  getSlaSrvc() {
    this.spinner.show();
    this.slaService.getSetSlas().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  getPlatForm() {
    this.spinner.show();
    this.pltFormService.get().subscribe(
      res => {
        this.pltfrm = res;
        
        var a = new Array(
          { pltfrmCd: "IP", pltfrmNme: "IPL", creatDt: null }
        );
        this.pltfrm=this.pltfrm.concat(a);
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  getMs() {
    this.spinner.show();
    this.msService.getXnciMs().subscribe(
      res => {
        this.ms = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }
 
  clear() {
    this.msSlaId = 0;
    this.SetSlas = new XnciMsSla();
    this.slaService.getSetSlas().subscribe(
      res => {
        this.items = res;
      },
      error => {
        this.spinner.hide();
      });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.msSlaId = event.data.msSlaId;
  }

  saveForm(event: any) {
    this.spinner.show();
    if (this.msSlaId > 0) {
      this.SetSlas = event.oldData;
      this.SetSlas.slaInDayQty = this.helper.isEmpty(event.newData.slaInDayQty)
        ? event.oldData.slaInDayQty : event.newData.slaInDayQty;
      this.SetSlas.msId = event.oldData.msId;
      this.SetSlas.toMsId = event.oldData.toMsId;
      this.SetSlas.ordrTypeId = event.oldData.ordrTypeId;
      this.SetSlas.creatByUserId = event.oldData.creatByUserId;
      this.SetSlas.creatDt = event.oldData.creatDt;
      this.slaService.updateSetSlas(this.msSlaId, this.SetSlas).subscribe(
        data => {
          this.helper.notify(`Successfully updated SLA ${this.SetSlas.msSlaId}.`,
            Global.NOTIFY_TYPE_SUCCESS);
          this.clear();
          setTimeout(() => {
            this.spinner.hide();
          }, 0);
        },
        error => {
          if (!this.helper.isEmpty(error.Message) && error.Message.indexOf('duplicate') !== -1) {
            this.helper.notify(`${this.SetSlas.msSlaId} SLA  already exists.`, Global.NOTIFY_TYPE_ERROR);
          }
          else if (!this.helper.isEmpty(error) && error.indexOf('duplicate') !== -1) {
            this.helper.notify(`${this.SetSlas.msSlaId} SLA already exists.`, Global.NOTIFY_TYPE_ERROR);
          }
          else {
            this.helper.notify(`Failed to update SLA  ${this.SetSlas.msSlaId}. ` + error,
              Global.NOTIFY_TYPE_ERROR);
          }
          this.clear();
          this.spinner.hide();
        });
    }
    else {
      this.SetSlas = event.data;
      this.slaService.createSetSlas(this.SetSlas).subscribe(
        data => {
          this.helper.notify(`Successfully created SLA ${this.SetSlas.msSlaId}.`,
            Global.NOTIFY_TYPE_SUCCESS);
          this.clear();
          setTimeout(() => {
            this.spinner.hide();
          }, 0);
        },
        error => {
          if (!this.helper.isEmpty(error.Message) && error.Message.indexOf('duplicate') !== -1) {
            this.helper.notify(`${this.SetSlas.msSlaId} already exists SLA .`, Global.NOTIFY_TYPE_ERROR);
          }
          else if (!this.helper.isEmpty(error) && error.indexOf('duplicate') !== -1) {
            this.helper.notify(`${this.SetSlas.msSlaId} already exists SLA .`, Global.NOTIFY_TYPE_ERROR);
          }
          else {
            this.helper.notify(`Failed to create SLA ${this.SetSlas.msSlaId}. ` + error, Global.NOTIFY_TYPE_ERROR);
          }
          this.clear();
          this.spinner.hide();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    this.selectedItemKeys.forEach((key) => {
      this.slaService.deleteSetSlas(key).subscribe(
        res => {
          this.helper.notify(`Successfully Updated Selected SLA('s) to 0`,
            Global.NOTIFY_TYPE_SUCCESS);
          this.clear();
        },
        error => {
          this.spinner.hide();
        });
    });
    this.spinner.hide();
  }
}
