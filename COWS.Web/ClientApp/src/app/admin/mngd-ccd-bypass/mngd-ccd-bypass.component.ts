import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { User,CCDBypassUser } from "../../../models/index";
import { CCDBypassService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DxRadioGroupModule } from "devextreme-angular"
import { Observable } from 'rxjs/internal/Observable';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-mngd-ccd-bypass',
  templateUrl: './mngd-ccd-bypass.component.html'
})
export class CCDBypassComponent implements OnInit {
  ccdByPassUser$: any[];
  items: Observable<CCDBypassUser[]>;
  user: Observable<User[]>;
  ccdByPass = new CCDBypassUser();
  adId: string = "";
  ConfigID: number = 0;
  isSubmitted = false;
  ccdUsers = true;
  selectedItems = false;
  displayNotfound = false;
  selectedItemKeys: any[] = [];
  selectedItemKeysCCD: any[] = [];
  validationMessage: string;
  @ViewChild('form', {static : true}) public form: any;
 
  constructor(private service: CCDBypassService, private router: Router, public helper: Helper,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getCurrentUsers();
  }

  getCurrentUsers() {
    this.spinner.show();
    this.service.getBypassUserList().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.validationMessage = "";
    this.selectedItems = false;
    this.ConfigID = 0;
    this.ccdByPass = new CCDBypassUser();
    this.service.getBypassUserList().subscribe(
      res => {
        this.items = res;
      },
      error => {
        this.spinner.hide();
      });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  selectionChangedHandlerCCD(data: any) {
    this.selectedItemKeysCCD = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    this.selectedItemKeysCCD.forEach((key) => {
      this.service.deleteCCDBypass(key.configID).subscribe(
        res => {
          this.helper.notify(`Selected Users successfully removed (It will take a few minutes for the updated permissions to take effect)`,
            Global.NOTIFY_TYPE_SUCCESS);
          this.clear();
        },
        error => {
          this.spinner.hide();
        });
    });
    this.spinner.hide();
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    this.spinner.show();
    this.ccdByPass = new CCDBypassUser();
    this.clear();
    this.selectedItemKeys.forEach((key) => {
      this.ccdByPass.configID = 0;
      this.ccdByPass.userADID = key.userAdid;
      this.ccdByPass.userName = key.fullNme;

      this.service.create(this.ccdByPass).subscribe(
        data => {
          this.helper.notify(`User successfully added (It will take a few minutes for the new permissions to take effect) :  ${key.fullNme}.`,
            Global.NOTIFY_TYPE_SUCCESS);
          this.clear();
          // this.onSubmitSearch();
          setTimeout(() => {
            this.spinner.hide();
          }, 0);
        },
        error => {
          if (!this.helper.isEmpty(error.Message) && error.Message.indexOf('duplicate') !== -1) {
            this.helper.notify(`${this.adId} already exists.`, Global.NOTIFY_TYPE_ERROR);
          }
          else if (!this.helper.isEmpty(error) && error.indexOf('duplicate') !== -1) {
            this.helper.notify(`${this.adId} already exists.`, Global.NOTIFY_TYPE_ERROR);
          }
          else {
            this.helper.notify(`Failed to add to CCD Bypass List ${this.adId}. ` + error, Global.NOTIFY_TYPE_ERROR);
          }

          this.clear();
          this.spinner.hide();
        });
    });

    this.spinner.hide();
  }
  
  signalSelected(nameVal: string) {
    nameVal = this.form.name
    if (nameVal == "") {
      //this.txtEmpty = false;
      //this.clear();
      //this.selectedItems = false;
    }
  }
  
  onKey(event: any) { // without type info
    if (event.target.value = "") {
      this.clear();
      this.selectedItems = false;
    }
  }

  onSubmitSearch() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    this.adId = this.form.name;
    if (this.form.name == null || this.form.name == undefined) {
      this.adId = "";
      this.validationMessage = "Please enter Name or ADID to search for.";
      this.displayNotfound = true;
      return;
    }

    if (this.adId.length < 1 ) {
      this.clear();
      this.validationMessage = "Please enter Name or ADID to search for.";
      this.displayNotfound = true;
      return;
    }

    this.spinner.show();
    this.clear();
    this.ccdByPassUser$ = null;
    this.displayNotfound = false;
    this.service.findUser(this.adId).subscribe(
      res => {
        if(res.length > 0) {
          // Checking if searched string is already exist or will do Express Create
          if(res[0].userAdid == 'exist') {
            this.validationMessage = "No active users in your group found.";
            this.displayNotfound = true;
            this.spinner.hide();
            return;
          } else if(res[0].userAdid== 'expressCreate') {
            this.validationMessage = '';
            this.helper.notify(`User successfully added (It will take a few minutes for the new permissions to take effect) :  ${res[0].fullNme}.`,
              Global.NOTIFY_TYPE_SUCCESS);
            this.clear();
            this.spinner.hide();
            return;
          }
        }

        this.ccdByPassUser$ = res;
        this.user = res;
        for (let resultItem of this.ccdByPassUser$) {

          this.clear();
          this.selectedItems = true;
        }
        if (this.ccdByPassUser$.length < 1) {
          this.validationMessage = "No active users in your group found.";
          this.displayNotfound = true;
        }
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        this.clear();
        this.displayNotfound = true;
      });
  }
}
