import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { SetPreference } from "../../../models/index";
import { SetPreferenceService } from '../../../services/index';
import { MaskUtil } from "../../../shared/mask/mask.util";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-SetPreference',
  templateUrl: './preferences.component.html'
})

export class SetPreferenceComponent implements OnInit {
  items: Observable<SetPreference[]>;
  setPreference = new SetPreference();
  eventTypes: Observable<any[]>;
  selectedItemKeys: any[] = [];
  cnfrcBrdgId: any;
  phoneMask = MaskUtil.PHONE_MASK_GENERATOR;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: SetPreferenceService) { }

  ngOnInit() {
    this.getSetPreference();
  }

  openLink(url) {
    window.open(url);
  }

  getSetPreference() {
    this.spinner.show();
    this.service.getSetPreference().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }
  
  clear() {
    this.cnfrcBrdgId = 0;
    this.setPreference = new SetPreference();
    this.service.getSetPreference().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.cnfrcBrdgId = event.data.cnfrcBrdgId;
  }

  saveForm(event: any) {
    this.spinner.show();
    if (this.cnfrcBrdgId > 0) {
      this.setPreference = event.oldData;
      this.setPreference.cnfrcBrdgNbr = event.newData.cnfrcBrdgNbr == undefined
        ? event.oldData.cnfrcBrdgNbr : event.newData.cnfrcBrdgNbr;
      this.setPreference.cnfrcPinNbr = event.newData.cnfrcPinNbr == undefined
        ? event.oldData.cnfrcPinNbr : event.newData.cnfrcPinNbr;
      this.setPreference.soi = event.newData.soi == undefined
        ? event.oldData.soi : event.newData.soi;
      this.setPreference.onlineMeetingAdr = event.newData.onlineMeetingAdr == undefined
        ? event.oldData.onlineMeetingAdr : event.newData.onlineMeetingAdr;
      this.service.updateSetPreference(this.cnfrcBrdgId, this.setPreference).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.setPreference.cnfrcBrdgNbr,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          console.log(error)
          this.helper.notifySavedFormMessage(this.setPreference.cnfrcBrdgNbr,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    else {
      this.setPreference = event.data;
      this.service.createSetPreference(this.setPreference).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.setPreference.cnfrcBrdgNbr,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          console.log(error)
          this.helper.notifySavedFormMessage(this.setPreference.cnfrcBrdgNbr,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.deleteSetPreference(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });

    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
