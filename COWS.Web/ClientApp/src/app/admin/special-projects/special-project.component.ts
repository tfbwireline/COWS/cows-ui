import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { SpecialProject } from "../../../models/index";
import { SpecialProjectService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-special-project',
  templateUrl: './special-project.component.html'
})

export class SpecialProjectComponent implements OnInit {
  items: Observable<SpecialProject[]>;
  specialProject = new SpecialProject();
  selectedItemKeys: any[] = [];
  specialProjectId: any;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: SpecialProjectService) { }

  ngOnInit() {
    this.getSpecialProjects();
  }

  getSpecialProjects() {
    this.spinner.show();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.specialProjectId = 0;
    this.specialProject = new SpecialProject();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.specialProjectId = event.data.spclProjId;
  }

  saveForm(event: any) {
    this.spinner.show();

    // For Edit
    if (this.specialProjectId > 0) {
      this.specialProject = event.oldData;
      this.specialProject.spclProjNme = this.helper.isEmpty(event.newData.spclProjNme)
        ? event.oldData.spclProjNme : event.newData.spclProjNme;
      this.service.update(this.specialProjectId, this.specialProject).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.specialProject.spclProjNme,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.specialProject.spclProjNme,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.specialProject = event.data;
      this.service.create(this.specialProject).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.specialProject.spclProjNme,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.specialProject.spclProjNme,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
