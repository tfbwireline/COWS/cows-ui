import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { DedicatedCustomer } from "../../../models/index";
import { DedicatedCustomerService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-dedicated-customer',
  templateUrl: './dedicated-customer.component.html'
})

export class DedicatedCustomerComponent implements OnInit {
  items: Observable<DedicatedCustomer[]>;
  customer = new DedicatedCustomer();
  selectedItemKeys: any[] = [];
  customerId: number = 0;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: DedicatedCustomerService) { }

  ngOnInit() {
    this.getDedicatedCustomers();
  }

  getDedicatedCustomers() {
    this.spinner.show();
    this.service.getDedicatedCustomers().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.customerId = 0;
    this.customer = new DedicatedCustomer();
    this.service.getDedicatedCustomers().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.customerId = event.data.custId;
  }

  saveForm(event: any) {
    this.spinner.show();

    // For Edit
    if (this.customerId > 0) {
      this.customer = event.oldData;
      this.customer.custNme = this.helper.isEmpty(event.newData.custNme)
                                ? event.oldData.custNme : event.newData.custNme;
      this.service.updateDedicatedCustomer(this.customerId, this.customer).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.customer.custNme, Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.customer.custNme, Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.customer = event.data;
      this.service.createDedicatedCustomer(this.customer).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.customer.custNme, Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.customer.custNme, Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.deleteDedicatedCustomer(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
