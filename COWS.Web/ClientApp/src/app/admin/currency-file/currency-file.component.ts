import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { Helper } from './../../../shared';
import { Global, ELkSysCfgMenus } from "./../../../shared/global";
import { CurrencyFileService, UserService } from '../../../services/index';
import { CurrencyFile } from "../../../models/index";
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-currency-file',
  templateUrl: './currency-file.component.html'
})
export class CurrencyFileComponent implements OnInit {
  items: CurrencyFile[]
  form: FormGroup
  filename: string = null
  file: File = null
  base64: any = null

  constructor(private spinner: NgxSpinnerService, private helper: Helper, private router: Router,
    private currencyFileService: CurrencyFileService, private userService: UserService) { }

  ngOnInit() {

    this.userService.GetAdminExcPerm(ELkSysCfgMenus.CurrencyFileUpload).subscribe(hasAccess => {
      if(!hasAccess) {
        this.router.navigate(['/sorry']);
      }
    })

    this.init();

    this.form = new FormGroup({
      file: new FormControl()
    });
  }

  init() {
    this.spinner.show();
    this.currencyFileService.get().subscribe(res => {
      this.items = res as CurrencyFile[]
    }, error => {
      this.spinner.hide();
    }, () => this.spinner.hide());
  }

  download(id, filename) {
    let data = zip(
      this.currencyFileService.getById(id),
      this.currencyFileService.download(id)
    )

    this.spinner.show();
    data.subscribe(res => {
      let currencyFile = res[0] as CurrencyFile

      if (currencyFile != null) {
        saveAs(res[1], currencyFile.fileNme)
      }
    }, error => {
      this.helper.notify("File not found", Global.NOTIFY_TYPE_ERROR);
      this.spinner.hide();
    }, () => this.spinner.hide());
  }

  upload(files) {
    if (files && files.length) {
      let formData = new FormData();
      formData.append('uploadFile', files[0], files[0].name);

      this.spinner.show()
      this.currencyFileService.create(formData).subscribe(res => {

      }, error => {
        this.reset();
        this.spinner.hide()
      }, () => {
        this.currencyFileService.get().subscribe(res2 => {
          this.items = res2 as CurrencyFile[]
          this.helper.notifySavedFormMessage(files[0].name, Global.NOTIFY_TYPE_SUCCESS, true, null);
        }, error => {
          this.spinner.hide();
        }, () => {
          this.reset();
          this.spinner.hide()
        });
      });
    }
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  reset() {
    this.setFormControlValue("file", null);
  }

  onFileLoad(result) {
    this.file = result.file as File
    //let byteArray = result.byteArray as Uint8Array
    this.base64 = result.base64

    this.filename = this.file.name    
  }

  uploadFile() {
    let model = {     
      base64string: this.base64,
      fileNme: this.file.name
    }

    this.spinner.show()
    this.currencyFileService.create(model).subscribe(res => {
      this.filename = "";
      console.log(res)
    }, error => {
      this.reset();
      this.spinner.hide()
    }, () => {
      this.currencyFileService.get().subscribe(res2 => {
        this.items = res2 as CurrencyFile[]
        this.helper.notifySavedFormMessage(this.file.name, Global.NOTIFY_TYPE_SUCCESS, true, null);
      }, error => {
        this.spinner.hide();
      }, () => {
        this.reset();
        this.spinner.hide()
      });
    });
  }
}
