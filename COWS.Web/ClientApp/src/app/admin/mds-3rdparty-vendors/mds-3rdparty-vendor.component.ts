import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { MDS3rdPartyVendor } from "../../../models/index";
import { MDS3rdPartyVendorService, MDSServiceTypeService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-mds-3rdparty-vendor',
  templateUrl: './mds-3rdparty-vendor.component.html'
})

export class MDS3rdPartyVendorComponent implements OnInit {
  items: Observable<MDS3rdPartyVendor[]>;
  mdsServiceTypes: Observable<any[]>;
  mds3rdPartyVendor = new MDS3rdPartyVendor();
  selectedItemKeys: any[] = [];
  mds3rdPartyVendorId: any;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: MDS3rdPartyVendorService, private mdsService: MDSServiceTypeService) { }

  ngOnInit() {
    this.getMDS3rdPartyVendors();
    this.getMDSServiceTypes();
  }

  getMDS3rdPartyVendors() {
    this.spinner.show();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  getMDSServiceTypes() {
    this.spinner.show();
    this.mdsService.get().subscribe(
      res => {
        this.mdsServiceTypes = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.mds3rdPartyVendorId = 0;
    this.mds3rdPartyVendor = new MDS3rdPartyVendor();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.mds3rdPartyVendorId = event.key;
  }

  saveForm(event: any) {
    this.spinner.show();

    // For Edit
    if (this.mds3rdPartyVendorId > 0) {
      this.mds3rdPartyVendor = event.oldData;
      this.mds3rdPartyVendor.thrdPartyVndrDes = this.helper.isEmpty(event.newData.thrdPartyVndrDes)
        ? event.oldData.thrdPartyVndrDes : event.newData.thrdPartyVndrDes;
      this.mds3rdPartyVendor.srvcTypeId = this.helper.isEmpty(event.newData.srvcTypeId)
        ? event.oldData.srvcTypeId : event.newData.srvcTypeId;
      this.service.update(this.mds3rdPartyVendorId, this.mds3rdPartyVendor).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.mds3rdPartyVendor.thrdPartyVndrDes,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.mds3rdPartyVendor.thrdPartyVndrDes,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.mds3rdPartyVendor = event.data;
      this.service.create(this.mds3rdPartyVendor).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.mds3rdPartyVendor.thrdPartyVndrDes,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.mds3rdPartyVendor.thrdPartyVndrDes,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
