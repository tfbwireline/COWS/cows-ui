import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { EventType } from "../../../models/index";
import { EventTypeService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-event-type',
  templateUrl: './event-type.component.html'
})
export class EventTypeComponent implements OnInit {
  items: Observable<EventType[]>;
  eventType = new EventType();
  selectedItemKeys: any[] = [];
  eventTypeId: any;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: EventTypeService) { }

  ngOnInit() {
    this.getEventTypes();
  }

  getEventTypes() {
    this.spinner.show();
    this.service.getEventTypes().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.eventTypeId = 0;
    this.eventType = new EventType();
    this.service.getEventTypes().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.eventTypeId = event.data.eventTypeId;
  }

  saveForm(event: any) {
    this.spinner.show();

    // For Edit
    if (this.eventTypeId > 0) {
      this.eventType = event.oldData;
      this.eventType.eventTypeNme = this.helper.isEmpty(event.newData.eventTypeNme)
                                        ? event.oldData.eventTypeNme : event.newData.eventTypeNme;
      this.service.updateEventType(this.eventTypeId, this.eventType).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.eventType.eventTypeNme,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.eventType.eventTypeNme,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.eventType = event.data;
      this.service.createEventType(this.eventType).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.eventType.eventTypeNme,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.eventType.eventTypeNme,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.deleteEventType(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
