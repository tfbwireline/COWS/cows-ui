import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { Observable, zip } from "rxjs";
import { MDSMACActivity } from "../../../models/index";
import { MDSMACActivityService, SearchService, SystemConfigService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";
declare let $: any;

@Component({
  selector: 'app-mds-mac-activity',
  templateUrl: './mds-mac-activity.component.html'
})

export class MDSMACActivityComponent implements OnInit, OnDestroy {
  form: FormGroup
  items: Observable<MDSMACActivity[]>;
  activityId: number = 0;
  activity = new MDSMACActivity();
  selectedItemKeys: any[] = [];
  mdsNtwrkActyTpeList: any[] = [];

  get mdsMacName() { return this.form.get("mdsMacName") }
  get duration() { return this.form.get("duration") }

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: MDSMACActivityService, private searchSrvc: SearchService,
    private sysCfgSrvc: SystemConfigService) { }

  ngOnInit() {
    this.getMDSActivities();
    this.form = new FormGroup({
      scMultiplier: new FormControl(),
      mdsNtwrkActyType: new FormControl(),
      baseDuration: new FormControl(),
      trnprtDuration: new FormControl()
    });
    this.helper.form = this.form;
  }

  ngOnDestroy() {
    this.helper.form = null;
  }

  getMDSActivities() {
    this.spinner.show();
    let data = zip(
      this.service.get(),
      this.sysCfgSrvc.getSysCfgByName('MDSSlotDuration'),
      this.searchSrvc.getSysCfgValue('SCMultiplier')
    );

    data.toPromise().then(
      res => {
        this.items = res[0];
        this.mdsNtwrkActyTpeList = res[1];
        this.helper.setFormControlValue("scMultiplier", +res[2]);
      }
    ).then(() => { this.spinner.hide(); });
  }

  clear() {
    this.activityId = 0;
    this.activity = new MDSMACActivity();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    //}, {
    //  location: 'after',
    //  widget: 'dxButton',
    //  options: {
    //    icon: 'add',
    //    onClick: this.onRowClick.bind(this)
    //  }
    });
  }

  loadEditForm(event: any) {
    this.activityId = event.data.mdsMacActyId;
  }

  updateMultiplier() {
    var multiplier = this.form.get("scMultiplier").value;
    if (this.helper.isEmpty(multiplier)) {
      this.helper.notify('Simple/Complex Redesign Multiplier is required.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.service.updateSysCfgValue("SCMultiplier", multiplier).subscribe(
      res => {
        this.helper.notify("Successfully updated Simple/Complex Redesign Multiplier.", Global.NOTIFY_TYPE_SUCCESS);
      },
      error => {
        this.helper.notify("Failed to update Simple/Complex Redesign Multiplier.", Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  onMDSNtwrkActyLookupChanged(e) {
    this.form.get('baseDuration').reset();
    this.form.get('trnprtDuration').reset();
    var cfgId = e.selectedItem.cfgId;
    var cfg = this.mdsNtwrkActyTpeList.find(i => i.cfgId == cfgId);
    var val = cfg.prmtrValuTxt.split(",");
    if (!this.helper.isEmpty(val)) {
      this.form.get('baseDuration').setValue(val[0]);
      this.form.get('trnprtDuration').setValue(val[1]);
    }
  }

  updateDuration() {
    var baseDuration = this.form.get("baseDuration").value;
    var trnprtDuration = this.form.get("trnprtDuration").value;
    var cfgId = this.form.get('mdsNtwrkActyType').value;

    if (this.helper.isEmpty(baseDuration)) {
      this.helper.notify('Base Dusration is required.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    if (this.helper.isEmpty(trnprtDuration)) {
      this.helper.notify('Transport Dusration is required.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    if (this.helper.isEmpty(cfgId)) {
      this.helper.notify('MDS Network Activity Type is required.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    var cfg = this.mdsNtwrkActyTpeList.find(i => i.cfgId == cfgId);
    var name = cfg.prmtrNme;
    var val = baseDuration + ',' + trnprtDuration;

    this.sysCfgSrvc.updateSysCfgValue(name, val).toPromise().then(
      res => {
        this.helper.notify(`Successfully updated ${name}.`, Global.NOTIFY_TYPE_SUCCESS);
      },
      error => {
        this.helper.notify(`Failed to update ${name}.`, Global.NOTIFY_TYPE_ERROR);
      }
    ).then(() => {
      this.sysCfgSrvc.getSysCfgByName('MDSSlotDuration').subscribe(
        res => {
          this.mdsNtwrkActyTpeList = res;
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
        }
      );
    });
  }

  saveForm(event) {
    this.spinner.show();

    // For Edit
    if (this.activityId > 0) {
      this.activity = event.oldData;
      this.activity.mdsMacActyNme = this.helper.isEmpty(event.newData.mdsMacActyNme)
        ? event.oldData.mdsMacActyNme : event.newData.mdsMacActyNme;
      this.activity.minDrtnTmeReqrAmt = this.helper.isEmpty(event.newData.minDrtnTmeReqrAmt)
        ? event.oldData.minDrtnTmeReqrAmt : event.newData.minDrtnTmeReqrAmt;
      this.service.update(this.activityId, this.activity).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.activity.mdsMacActyNme,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.activity.mdsMacActyNme,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    // For Insert
    else {
      this.activity = event.data;
      this.service.create(this.activity).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.activity.mdsMacActyNme,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.activity.mdsMacActyNme,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
