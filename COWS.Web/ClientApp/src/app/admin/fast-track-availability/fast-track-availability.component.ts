import { Component, OnInit } from '@angular/core';
import { TimeSlotOccurence } from "../../../models/index";
import { UserService, TimeSlotService, ModalService } from '../../../services/index';
import { Observable } from "rxjs";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from '../../../shared/global';

@Component({
  selector: 'app-fast-track-availability',
  templateUrl: './fast-track-availability.component.html'
})
export class FastTrackAvailabilityComponent implements OnInit {
  timeSlots: Observable<TimeSlotOccurence[]>;
  events: Observable<any[]>;
  eventDay: string;
  eventTimeSlot: string;
  updResultset: string = '';
  columnResizingMode: string = 'nextColumn';;

  constructor(private userService: UserService, private service: TimeSlotService, private router: Router,
    public helper: Helper, private spinner: NgxSpinnerService, private modalService: ModalService) { }

  ngOnInit() {
    this.getTimeSlotOccurrences();
  }

  public getTimeSlotOccurrences() {
    this.userService.getLoggedInUser().toPromise().then(
      res => {
        this.spinner.show();
        let adid = res.userAdid;
        this.service.checkUserAccessToAfterHrsSlots(adid).toPromise().then(
          res => {
            let bShowAfterHrsSlots = res;
            this.service.getTimeSlotOccurances(5, bShowAfterHrsSlots).subscribe(
              res => {
                this.timeSlots = res;
                this.spinner.hide();
              },
              error => {
                this.spinner.hide();
              });
          });
      });
  }

  public getFTEventsForTimeSlot(data: any, rsrcAvlbltyID: number) {
    this.eventDay = data.data.day;
    this.eventTimeSlot = data.data.displayTimeSlot;

    this.spinner.show();
    this.service.getFTEventsForTS(rsrcAvlbltyID).subscribe(
      res => {
        this.events = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  public updateFTCount(rsrcAvlbltyID: number, input: string) {
    if (!this.updResultset.includes(rsrcAvlbltyID + '|'))
      this.updResultset = this.updResultset + ',' + rsrcAvlbltyID + '|' + input;
  }

  public updateFTEvents() {
    this.spinner.show();
    this.service.updateResourceAvailability(this.updResultset).subscribe(
      res => {
        this.helper.notify(`Successfully updated Allowed FT Events`,
          Global.NOTIFY_TYPE_SUCCESS);
        this.spinner.hide();
        this.clear();
      },
      error => {
        this.spinner.hide();
      });
  }

  public clear() {
    this.updResultset = '';
    this.events = null;
    this.eventDay = null;
    this.eventTimeSlot = null;
    this.getTimeSlotOccurrences();
  }
}
