import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { Telco } from "../../../models/index";
import { TelcoService } from '../../../services/index';
import { MaskUtil } from "../../../shared/mask/mask.util";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-telco',
  templateUrl: './telco.component.html'
})

export class TelcoComponent implements OnInit {
  items: Observable<Telco[]>;
  telco = new Telco();
  selectedItemKeys: any[] = [];
  telcoId: any;
  phoneMask = MaskUtil.PHONE_MASK_GENERATOR;
  phonePattern: any = /^\+\s*1\s*\(\s*[02-9]\d{2}\)\s*\d{3}\s*-\s*\d{4}$/;
  phoneRules: any = { "X": /[02-9]/ };

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: TelcoService) { }

  ngOnInit() {
    this.getTelco();
  }

  getTelco() {
    this.spinner.show();
    this.service.getTelco().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.telcoId = 0;
    this.telco = new Telco();
    this.service.getTelco().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.telcoId = event.data.telcoId;
  }

  saveForm(event: any) {
    this.spinner.show();
    if (this.telcoId > 0) {
      this.telco = event.oldData;
      this.telco.telcoNme = this.helper.isEmpty(event.newData.telcoNme)
        ? event.oldData.telcoNme : event.newData.telcoNme;
      this.telco.telcoCntctPhnNbr = this.helper.isEmpty(event.newData.telcoCntctPhnNbr)
        ? event.oldData.telcoCntctPhnNbr : event.newData.telcoCntctPhnNbr;
      this.service.updateTelco(this.telcoId, this.telco).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.telco.telcoNme,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.telco.telcoNme,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    else {
      this.telco = event.data;
      this.service.createTelco(this.telco).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.telco.telcoNme,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.telco.telcoNme,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.deleteTelco(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });

    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
