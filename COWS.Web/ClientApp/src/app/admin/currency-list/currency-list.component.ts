import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { CurrencyList } from "../../../models/index";
import { CurrencyListService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-currency-list',
  templateUrl: './currency-list.component.html'
})
export class CurrencyListComponent implements OnInit {
  public items: Observable<CurrencyList[]>;

  constructor(private service: CurrencyListService, private spinner: NgxSpinnerService) { }

  public ngOnInit() {
    this.getCurrencyList();
  }

  public getCurrencyList() {
    this.spinner.show();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }
}
