import { formatDate } from "@angular/common";
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { Helper } from './../../../shared';
import { Global } from "./../../../shared/global";
import { EventIntervalService, SprintHolidayService } from '../../../services/index';
import { SprintHoliday, EventInterval } from "../../../models/index";
declare let $: any;

@Component({
  selector: 'app-event-interval',
  templateUrl: './event-interval.component.html'
})
export class EventIntervalComponent implements OnInit {
  form: FormGroup
  items: EventInterval[]
  sprintHolidays: SprintHoliday[]
  cpeImplementationDate: string = null
  nonCpeImplementationDate: string = null

  constructor(private spinner: NgxSpinnerService, private helper: Helper,
    private eventIntervalService: EventIntervalService,
    private sprintHolidayService: SprintHolidayService) { }

  ngOnInit() {
    this.form = new FormGroup({
      eventTypeId: new FormControl(),
      eventTypeName: new FormControl({ value: '', disabled: true }),
      cpeInterval: new FormControl(),
      nonCpeInterval: new FormControl(),
      testDate: new FormControl()
    });

    let data = zip(
      this.eventIntervalService.get(),
      this.sprintHolidayService.get()
    )

    this.spinner.show()
    data.subscribe(res => {
      this.items = res[0] as EventInterval[]
      this.sprintHolidays = res[1] as SprintHoliday[]
    },
      error => { },
      () => this.spinner.hide());

    this.form.valueChanges
      .pipe(debounceTime(1000))
      .subscribe(res => {
        this.calculate()
      });
  }

  onRowClick(event) {
    console.log(event)
    this.setFormControlValue("eventTypeId", event.data.eventTypeId);
    this.setFormControlValue("eventTypeName", event.data.eventTypeName);
    this.setFormControlValue("cpeInterval", event.data.cpeNtvlInDayQty);
    this.setFormControlValue("nonCpeInterval", event.data.nonCpeNtvlInDayQty);
    this.setFormControlValue("testDate", formatDate(new Date(), 'MM/dd/yyyy', 'en'));

    $("#event-interval").modal("show");
  }

  getImplementationDate(date, interval: number): string {
    let addedDays = 0
    let currentDate = new Date(date)

    while (addedDays < interval) {
      currentDate.setDate(currentDate.getDate() + 1)

      let currentDate_string = formatDate(currentDate, 'MM/dd/yyyy', 'en')

      while (currentDate.getDay() == 6
        || currentDate.getDay() == 0
        || this.sprintHolidays.findIndex(a => formatDate(new Date(a.sprintHldyDt), 'MM/dd/yyyy', 'en') == currentDate_string) >= 0) {
        currentDate.setDate(currentDate.getDate() + 1)
        currentDate_string = formatDate(currentDate, 'MM/dd/yyyy', 'en')
      }

      addedDays++
    }

    return formatDate(currentDate, 'MM/dd/yyyy', 'en');
  }

  calculate() {
    let testDate = this.getFormControlValue("testDate")
    this.cpeImplementationDate = this.getImplementationDate(testDate, this.getFormControlValue("cpeInterval"));
    this.nonCpeImplementationDate = this.getImplementationDate(testDate, this.getFormControlValue("nonCpeInterval"));
  }

  onSubmit() {
    let model = new EventInterval()

    model.eventTypeId = this.getFormControlValue("eventTypeId")
    model.eventTypeNme = this.getFormControlValue("eventTypeName")
    model.cpeNtvlInDayQty = this.getFormControlValue("cpeInterval")
    model.nonCpeNtvlInDayQty = this.getFormControlValue("nonCpeInterval")

    this.spinner.show();
    this.eventIntervalService.update(model.eventTypeId, model).subscribe(res => {
      $("#event-interval").modal("hide");
      // Success message
      this.helper.notify(`Successfully updated ${model.eventTypeNme}.`, Global.NOTIFY_TYPE_SUCCESS);

      // Resets form
      this.reset()

      // Fetch new set of list
      this.eventIntervalService.get().subscribe(res => {
        this.items = res as EventInterval[]
      },
        error => this.spinner.hide(),
        () => this.spinner.hide());
    },
      error => this.spinner.hide(),
      () => this.spinner.hide());
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  reset() {
    this.setFormControlValue("eventTypeId", 0);
    this.setFormControlValue("eventTypeName", null);
    this.setFormControlValue("cpeInterval", null);
    this.setFormControlValue("nonCpeInterval", null);
    this.cpeImplementationDate = null
    this.nonCpeImplementationDate = null

    $("#event-interval").modal("hide");
  }
}
