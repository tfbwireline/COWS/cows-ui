import { Component, OnInit } from '@angular/core';
import { Qualification } from "../../../models/index";
import { QualificationService } from '../../../services/index';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';

@Component({
  selector: 'app-qualification-view',
  templateUrl: './qualification-view.component.html'
})
export class QualificationViewComponent implements OnInit {
  public id: any;
  public qualification: Qualification;
  public assignTo: string = "";
  public eventTypes: string[] = [];
  public activityTypes: string[] = [];
  public customers: string[] = [];
  public specialProjects: string[] = [];
  public enhancedServices: string[] = [];
  public vendorModels: string[] = [];
  public ipVersions: string[] = [];

  constructor(private avRoute: ActivatedRoute, private router: Router, public helper: Helper,
    private spinner: NgxSpinnerService, private service: QualificationService) { }

  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"];
    if (this.id > 0) {
      this.getData();
    }
  }

  public getData() {
    this.spinner.show();

    this.service.getById(this.id).subscribe(
      res => {
        this.qualification = res;
        this.assignTo = this.qualification.assignToUser;
        this.eventTypes = this.helper.isEmpty(this.qualification.eventTypeNames)
          ? null : this.qualification.eventTypeNames.split(',');
        this.activityTypes = this.helper.isEmpty(this.qualification.networkActivityNames)
          ? null : this.qualification.networkActivityNames.split(',');
        this.customers = this.helper.isEmpty(this.qualification.customerNames)
          ? null : this.qualification.customerNames.split(',');
        this.specialProjects = this.helper.isEmpty(this.qualification.specialProjectNames)
          ? null : this.qualification.specialProjectNames.split(',');
        this.enhancedServices = this.helper.isEmpty(this.qualification.enhanceServiceNames)
          ? null : this.qualification.enhanceServiceNames.split(',');
        this.vendorModels = this.helper.isEmpty(this.qualification.vendorModelNames)
          ? null : this.qualification.vendorModelNames.split(',');
        this.ipVersions = this.helper.isEmpty(this.qualification.ipVersionNames)
          ? null : this.qualification.ipVersionNames.split(',');

        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }
}
