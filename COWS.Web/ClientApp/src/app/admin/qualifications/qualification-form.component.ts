import { Component, OnInit } from '@angular/core';
import { zip } from "rxjs";
import { FormControlItem, Qualification, User } from "../../../models/index";
import {
  EventTypeService, DedicatedCustomerService, MplsActivityTypeService, EnhncSrvcService,
  DeviceModelService, IPVersionService, MDSNetworkActivityTypeService, QualificationService, UserService
} from '../../../services/index';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-qualification-form',
  templateUrl: './qualification-form.component.html'
})
export class QualificationFormComponent implements OnInit {
  id: any;
  form: FormGroup;
  isSubmitted: boolean = false;
  isActivator: boolean = false;
  isAdmin: boolean = false;
  qualification: Qualification;
  eventTypes: FormControlItem[] = [];
  dedicatedCustomers: FormControlItem[] = [];
  mplsActivityTypes: FormControlItem[] = [];
  enhancedServices: FormControlItem[] = [];
  vendorModels: FormControlItem[] = [];
  ipVersions: FormControlItem[] = [];
  mdsActivityTypes: FormControlItem[] = [];
  users: User[] = [];

  get assignedTo() { return this.form.get("assignedTo") }

  constructor(private avRoute: ActivatedRoute, private router: Router, public helper: Helper,
    private spinner: NgxSpinnerService, private fb: FormBuilder, private userSrvc: UserService,
    private qftnSrvc: QualificationService, private evntSrvc: EventTypeService,
    private custSrvc: DedicatedCustomerService, private mplsActySrvc: MplsActivityTypeService,
    private enhSrvc: EnhncSrvcService, private ipSrvc: IPVersionService,
    private devModelSrvc: DeviceModelService, private mdsSrvc: MDSNetworkActivityTypeService
  ) { }

  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"];
    this.form = this.fb.group({
      eventTypes: this.fb.array([]),
      eventTypesSelectAll: this.fb.control(false),
      mdsActivityTypes: this.fb.array([]),
      dedicatedCustomers: this.fb.array([]),
      dedicatedCustomersSelectAll: this.fb.control(false),
      mplsActivityTypes: this.fb.array([]),
      mplsActivityTypesSelectAll: this.fb.control(false),
      enhancedServices: this.fb.array([]),
      enhancedServicesSelectAll: this.fb.control(false),
      vendorModels: this.fb.array([]),
      vendorModelsSelectAll: this.fb.control(false),
      ipVersions: this.fb.array([]),
      ipVersionsSelectAll: this.fb.control(false),
      assignedTo: this.fb.control(null, Validators.required)
    });

    this.getInitialData();
  }

  getInitialData() {
    this.spinner.show();

    let data = zip(
      this.evntSrvc.getForControl(),
      this.mdsSrvc.getForControl(),
      this.custSrvc.getForControl(),
      //this.projSrvc.getForControl(),
      this.mplsActySrvc.getMplsActivityTypeForMDS(),
      this.enhSrvc.getForControl(),
      this.devModelSrvc.getForControl(),
      this.ipSrvc.getForControl(),
      this.userSrvc.getUsersByProfileName('MDSEActivator')
    );

    let dataPromise = data.toPromise();
    let qualificationPromise = this.id > 0 ? this.qftnSrvc.getById(this.id).toPromise()
      : new Promise((resolve) => resolve(new Qualification()));

    Promise
      .all([dataPromise, qualificationPromise])
      .then(res => {
        this.eventTypes = res[0][0] as FormControlItem[];
        this.mdsActivityTypes = res[0][1] as FormControlItem[];
        this.dedicatedCustomers = res[0][2] as FormControlItem[];
        this.mplsActivityTypes = res[0][3] as FormControlItem[];
        this.enhancedServices = res[0][4] as FormControlItem[];
        this.vendorModels = res[0][5] as FormControlItem[];
        this.vendorModels.sort((t1, t2) => {
          if (t1.text > t2.text) { return 1; }
          if (t1.text < t2.text) { return -1; }
          return 0;
        });
        this.ipVersions = res[0][6] as FormControlItem[];
        this.users = res[0][7] as User[];

        this.qualification = res[1] as Qualification;
        console.log(this.qualification)

        // Updated by Sarah Sandoval [20210419] - Changed string id to number just to set the correct check box on UI
        // Event Types
        if (!this.helper.isEmpty(this.qualification) && !this.helper.isEmpty(this.qualification.eventTypeIDs)) {
          let ids = this.qualification.eventTypeIDs.split(',').map(i => +i);
          this.eventTypes.map((item) => {
            item.selected = ids.includes(+item.value);
          });
        }
        var ctrl = this.eventTypes.map(item => new FormControl(item.selected));
        this.form.setControl('eventTypes', this.fb.array(ctrl));

        // MDS Network Activity Types
        if (!this.helper.isEmpty(this.qualification) && !this.helper.isEmpty(this.qualification.networkActivityIDs)) {
          let ids = this.qualification.networkActivityIDs.split(',').map(i => +i);
          this.mdsActivityTypes.map((item) => {
            item.selected = ids.includes(+item.value);
          });
        }
        var ctrlMat = this.mdsActivityTypes.map(item => new FormControl(item.selected));
        this.form.setControl('mdsActivityTypes', this.fb.array(ctrlMat));

        // Dedicated Customers
        if (!this.helper.isEmpty(this.qualification) && !this.helper.isEmpty(this.qualification.customerIDs)) {
          let ids = this.qualification.customerIDs.split(',').map(i => +i);
          this.dedicatedCustomers.map((item) => {
            item.selected = ids.includes(+item.value);
          });
        }
        var ctrlDC = this.dedicatedCustomers.map(item => new FormControl(item.selected));
        this.form.setControl('dedicatedCustomers', this.fb.array(ctrlDC));

        // MPLS Activity Types for MDS
        if (!this.helper.isEmpty(this.qualification) && !this.helper.isEmpty(this.qualification.mplsActyTypeIDs)) {
          let ids = this.qualification.mplsActyTypeIDs.split(',').map(i => +i);
          this.mplsActivityTypes.map((item) => {
            item.selected = ids.includes(+item.value);
          });
        }
        var ctrlMAT = this.mplsActivityTypes.map(item => new FormControl(item.selected));
        this.form.setControl('mplsActivityTypes', this.fb.array(ctrlMAT));

        // Enhanced Services
        if (!this.helper.isEmpty(this.qualification) && !this.helper.isEmpty(this.qualification.enhanceServiceIDs)) {
          let ids = this.qualification.enhanceServiceIDs.split(',').map(i => +i);
          this.enhancedServices.map((item) => {
            item.selected = ids.includes(+item.value);
          });
        }
        var ctrlES = this.enhancedServices.map(item => new FormControl(item.selected));
        this.form.setControl('enhancedServices', this.fb.array(ctrlES));

        // Vendor Models
        if (!this.helper.isEmpty(this.qualification) && !this.helper.isEmpty(this.qualification.vendorModelIDs)) {
          let ids = this.qualification.vendorModelIDs.split(',').map(i => +i);
          this.vendorModels.map((item) => {
            item.selected = ids.includes(+item.value);
          });
        }
        var ctrlVM = this.vendorModels.map(item => new FormControl(item.selected));
        this.form.setControl('vendorModels', this.fb.array(ctrlVM));

        // IP Version
        if (!this.helper.isEmpty(this.qualification) && !this.helper.isEmpty(this.qualification.ipVersionIDs)) {
          let ids = this.qualification.ipVersionIDs.split(',').map(i => +i);
          this.ipVersions.map((item) => {
            item.selected = ids.includes(+item.value);
          });
        }
        var ctrlIP = this.ipVersions.map(item => new FormControl(item.selected));
        this.form.setControl('ipVersions', this.fb.array(ctrlIP));

        if (!this.helper.isEmpty(this.qualification) && !this.helper.isEmpty(this.qualification.assignToUserID)) {
          if (this.users.findIndex(i => i.userId == this.qualification.assignToUserID) == -1) {
            this.userSrvc.getByUserID(this.qualification.assignToUserID)
              .toPromise().then(res => {
                this.users.push(res);
              }).then(() => {
                this.form.get('assignedTo').setValue(this.qualification.assignToUserID);
              });
          }
          else {
            this.form.get('assignedTo').setValue(this.qualification.assignToUserID);
          }
        }

        this.onChanges();
        let eActivator: number = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('MDS Event Activator'));
        let admin: number = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('MDS Admin'));
        this.isActivator = (admin == -1 && eActivator > -1);
        this.isAdmin = admin > -1;

        if (this.isActivator) {
          if (this.id > 0) {
            this.users = this.users.filter(i => i.userId == this.qualification.assignToUserID);
          }
          else {
            this.users = this.users.filter(i => i.userId == this.userSrvc.loggedInUser.userId);
          }
        }

        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          this.helper.notify('Failed in getting qualification data.', Global.NOTIFY_TYPE_ERROR);
        }
      );
  }

  onChanges(): void {
    // Subscribe to changes on the selectAll event type checkbox
    this.form.get('eventTypesSelectAll').valueChanges.subscribe(bool => {
      this.form
        .get('eventTypes')
        .patchValue(Array(this.eventTypes.length).fill(bool), { emitEvent: false });
    });
    this.form.get('eventTypes').valueChanges.subscribe(val => {
      const allSelected = val.every(bool => bool);
      //console.log('allSelected: ' + allSelected);
      //console.log('selectall: ' + this.form.get('eventTypesSelectAll').value);
      if (this.form.get('eventTypesSelectAll').value !== allSelected) {
        this.form.get('eventTypesSelectAll').patchValue(allSelected, { emitEvent: false });
      }
    });

    // Subscribe to changes on the selectAll dedicated customer checkbox
    this.form.get('dedicatedCustomersSelectAll').valueChanges.subscribe(bool => {
      this.form
        .get('dedicatedCustomers')
        .patchValue(Array(this.dedicatedCustomers.length).fill(bool), { emitEvent: false });
    });
    this.form.get('dedicatedCustomers').valueChanges.subscribe(val => {
      const allSelected = val.every(bool => bool);
      if (this.form.get('dedicatedCustomersSelectAll').value !== allSelected) {
        this.form.get('dedicatedCustomersSelectAll').patchValue(allSelected, { emitEvent: false });
      }
    });

    // Subscribe to changes on the selectAll MPLS Activity Types checkbox
    this.form.get('mplsActivityTypesSelectAll').valueChanges.subscribe(bool => {
      this.form
        .get('mplsActivityTypes')
        .patchValue(Array(this.mplsActivityTypes.length).fill(bool), { emitEvent: false });
    });
    this.form.get('mplsActivityTypes').valueChanges.subscribe(val => {
      const allSelected = val.every(bool => bool);
      if (this.form.get('mplsActivityTypesSelectAll').value !== allSelected) {
        this.form.get('mplsActivityTypesSelectAll').patchValue(allSelected, { emitEvent: false });
      }
    });

    // Subscribe to changes on the selectAll enhanced service checkbox
    this.form.get('enhancedServicesSelectAll').valueChanges.subscribe(bool => {
      this.form
        .get('enhancedServices')
        .patchValue(Array(this.enhancedServices.length).fill(bool), { emitEvent: false });
    });
    this.form.get('enhancedServices').valueChanges.subscribe(val => {
      const allSelected = val.every(bool => bool);
      if (this.form.get('enhancedServicesSelectAll').value !== allSelected) {
        this.form.get('enhancedServicesSelectAll').patchValue(allSelected, { emitEvent: false });
      }
    });

    // Subscribe to changes on the selectAll vendor model checkbox
    this.form.get('vendorModelsSelectAll').valueChanges.subscribe(bool => {
      this.form
        .get('vendorModels')
        .patchValue(Array(this.vendorModels.length).fill(bool), { emitEvent: false });
    });
    this.form.get('vendorModels').valueChanges.subscribe(val => {
      const allSelected = val.every(bool => bool);
      if (this.form.get('vendorModelsSelectAll').value !== allSelected) {
        this.form.get('vendorModelsSelectAll').patchValue(allSelected, { emitEvent: false });
      }
    });

    // Subscribe to changes on the selectAll ip version checkbox
    this.form.get('ipVersionsSelectAll').valueChanges.subscribe(bool => {
      this.form
        .get('ipVersions')
        .patchValue(Array(this.ipVersions.length).fill(bool), { emitEvent: false });
    });
    this.form.get('ipVersions').valueChanges.subscribe(val => {
      const allSelected = val.every(bool => bool);
      if (this.form.get('ipVersionsSelectAll').value !== allSelected) {
        this.form.get('ipVersionsSelectAll').patchValue(allSelected, { emitEvent: false });
      }
    });
  }

  submit() {
    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.isSubmitted = true;
    this.spinner.show();

    var selectedEventTypes = this.form.value.eventTypes
      .map((checked, index) => checked ? this.eventTypes[index].value : null)
      .filter(value => value !== null);

    var selectedMdsActivityTypes = this.form.value.mdsActivityTypes
      .map((checked, index) => checked ? this.mdsActivityTypes[index].value : null)
      .filter(value => value !== null);

    var selectedDedicatedCustomers = this.form.value.dedicatedCustomers
      .map((checked, index) => checked ? this.dedicatedCustomers[index].value : null)
      .filter(value => value !== null);

    var selectedMplsActyTypes = this.form.value.mplsActivityTypes
      .map((checked, index) => checked ? this.mplsActivityTypes[index].value : null)
      .filter(value => value !== null);

    var selectedEnhancedServices = this.form.value.enhancedServices
      .map((checked, index) => checked ? this.enhancedServices[index].value : null)
      .filter(value => value !== null);

    var selectedVendorModels = this.form.value.vendorModels
      .map((checked, index) => checked ? this.vendorModels[index].value : null)
      .filter(value => value !== null);

    var selectedIPVersions = this.form.value.ipVersions
      .map((checked, index) => checked ? this.ipVersions[index].value : null)
      .filter(value => value !== null);

    let prevAssigned: number = (this.id > 0) ? this.qualification.assignToUserID : 0;
    let isNewAssigned: boolean = (this.id > 0) ? prevAssigned != this.form.value.assignedTo : false;
    let isDuplicate: boolean = false;

    this.qualification = new Qualification();
    this.qualification.assignToUserID = this.form.value.assignedTo;
    this.qualification.eventTypeIDs = selectedEventTypes.toString();
    this.qualification.networkActivityIDs = selectedMdsActivityTypes.toString();
    this.qualification.customerIDs = selectedDedicatedCustomers.toString();
    this.qualification.mplsActyTypeIDs = selectedMplsActyTypes.toString();
    this.qualification.enhanceServiceIDs = selectedEnhancedServices.toString();
    this.qualification.vendorModelIDs = selectedVendorModels.toString();
    this.qualification.ipVersionIDs = selectedIPVersions.toString();
    this.qualification.createdDate = new Date();
    this.qualification.modifiedDate = new Date();

    //console.log(this.qualification);

    this.qftnSrvc.checkDuplicate(this.qualification.assignToUserID).toPromise().then(
      data => {
        if (data && isNewAssigned) {
          isDuplicate = true;
        }
      },
      error => {
        let msg = this.id > 0 ? 'Failed to update Qualification.' : 'Failed to create Qualification.';
        this.helper.notify(msg, Global.NOTIFY_TYPE_ERROR);
      }).then(() => {
        if (isDuplicate) {
          this.spinner.hide();
          this.helper.notify(`There is already a Qualification assigned to this user. Please select a different activator.`, Global.NOTIFY_TYPE_ERROR);
          return;
        }

        if (this.id > 0) {
          // For Update Qualification
          this.qftnSrvc.update(this.id, this.qualification).toPromise().then(
            data => {
              this.helper.notify(`Successfully updated Qualification.`, Global.NOTIFY_TYPE_SUCCESS);
            },
            error => {
              this.helper.notify(`Failed to update Qualification.`, Global.NOTIFY_TYPE_ERROR);
            }).then(() => {
              this.spinner.hide();
              this.router.navigate(['/admin/qualifications/']);
            });
        }
        else {
          // For Create Qualification
          this.qftnSrvc.create(this.qualification).toPromise().then(
            data => {
              this.helper.notify(`Successfully created Qualification.`, Global.NOTIFY_TYPE_SUCCESS);
            },
            error => {
              this.helper.notify(`Failed to create Qualification.`, Global.NOTIFY_TYPE_ERROR);
            }).then(() => {
              this.spinner.hide();
              this.router.navigate(['/admin/qualifications/']);
            });
        }
      });
  }
}
