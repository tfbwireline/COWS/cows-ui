import { Component, OnInit, ViewChild } from '@angular/core';
import { QualificationService, UserService } from '../../../services/index';
import { Qualification } from '../../../models/index';
import { Observable } from "rxjs";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { FormGroup, FormControl } from "@angular/forms";
import { DxDataGridComponent } from "devextreme-angular";

@Component({
  selector: 'app-qualification',
  templateUrl: './qualification.component.html',
  styleUrls: ['./qualification.component.css']
})
export class QualificationComponent implements OnInit {
  @ViewChild(DxDataGridComponent, {static : true}) dataGrid: DxDataGridComponent
  items: Observable<Qualification[]>;
  isActivator: boolean = false;
  isAdmin: boolean = false;
  searchFilter: any;
  selectedItemKeys: any[] = [];
  errorKeys: any[] = [];
  form: FormGroup;

  constructor(private service: QualificationService, private spinner: NgxSpinnerService,
    public helper: Helper, private router: Router, private userSrvc: UserService) { }

  ngOnInit() {
    this.getQualifications();
    this.setForm();
    this.searchFilter = [{
      text: "Assigned User",
      value: "assignToUser"
    }, {
      text: "Dedicated Customer",
      value: "customerNames"
    }, {
      text: "Device",
      value: "deviceNames"
    }, {
      text: "Enhanced Service",
      value: "enhanceServiceNames"
    }, {
      text: "Event Type",
      value: "eventTypeNames"
    }, {
      text: "IP Version",
      value: "iPVersionNames"
    }, {
      text: "Special Project",
      value: "specialProjectNames"
    }, {
      text: "Vendor Model",
      value: "vendorModelNames"
    }];
  }

  setForm() {
    this.form = new FormGroup({
      'searchCriteria': new FormControl(''),
      'searchText': new FormControl('')
    });
  }

  getQualifications() {
    this.spinner.show();
    this.service.get().toPromise().then(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }).then(() => {
        this.spinner.hide();
        let eActivator: number = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('MDS Event Activator'));
        let admin: number = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('MDS Admin'));
        this.isActivator = (admin == -1 && eActivator > -1);
        this.isAdmin = admin > -1;
      });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
        location: 'before',
        template: 'searchTemplate'
      },
      {
        location: 'after',
        template: 'exportTemplate'
      });
  }

  onInitNewRow(e) {
    this.router.navigate(['/admin/qualifications/add']);
  }

  onViewClick(e) {
    this.goToFormPage(true, e.row.key);
  }

  onEditingStart(e: any) {
    e.cancel = true;
    this.goToFormPage(false, e.key);
  }

  goToFormPage(isView: boolean, key: number) {
    if (isView) {
      this.router.navigate(['/admin/qualifications/view/' + key]);
    }
    else {
      this.router.navigate(['/admin/qualifications/' + key]);
    }
  }

  onExporting(e) {
    e.fileName = 'Qualifications_' + this.helper.formatDate(new Date());
    e.component.beginUpdate();
    e.component.columnOption("assignToUserAdId", "visible", true);
    e.component.columnOption("assignToUser", "visible", true);
    e.component.columnOption("customerNames", "visible", true);
    e.component.columnOption("deviceNames", "visible", true);
    e.component.columnOption("enhanceServiceNames", "visible", true);
    e.component.columnOption("eventTypeNames", "visible", true);
    e.component.columnOption("networkActivityNames", "visible", true);
    e.component.columnOption("iPVersionNames", "visible", true);
    e.component.columnOption("specialProjectNames", "visible", true);
    e.component.columnOption("vendorModelNames", "visible", true);
    e.component.columnOption("createdByAdId", "visible", true);
    e.component.columnOption("createdByUser", "visible", true);
    e.component.columnOption("createdDate", "visible", true);
    e.component.columnOption("modifiedByAdId", "visible", true);
    e.component.columnOption("modifiedByUser", "visible", true);
    e.component.columnOption("modifiedDate", "visible", true);
  }

  onExported(e) {
    e.component.columnOption("assignToUserAdId", "visible", false);
    e.component.columnOption("assignToUser", "visible", false);
    e.component.columnOption("customerNames", "visible", false);
    e.component.columnOption("deviceNames", "visible", false);
    e.component.columnOption("enhanceServiceNames", "visible", false);
    e.component.columnOption("eventTypeNames", "visible", false);
    e.component.columnOption("networkActivityNames", "visible", false);
    e.component.columnOption("iPVersionNames", "visible", false);
    e.component.columnOption("specialProjectNames", "visible", false);
    e.component.columnOption("vendorModelNames", "visible", false);
    e.component.columnOption("createdByAdId", "visible", false);
    e.component.columnOption("createdByUser", "visible", false);
    e.component.columnOption("createdDate", "visible", false);
    e.component.columnOption("modifiedByAdId", "visible", false);
    e.component.columnOption("modifiedByUser", "visible", false);
    e.component.columnOption("modifiedDate", "visible", false);
    e.component.endUpdate();
  }

  search() {
    if (this.helper.isEmpty(this.form.value.searchCriteria)) {
      this.dataGrid.instance.searchByText(this.form.value.searchText)
    }
    else {
      //console.log(this.dataGrid.instance);
      //console.log(this.form.value.searchCriteria);
      this.dataGrid.instance.filter([this.form.value.searchCriteria, "contains", this.form.value.searchText]);
    }
  }

  clear() {
    if (this.helper.isEmpty(this.form.value.searchCriteria)) {
      this.dataGrid.instance.searchByText(this.form.value.searchText)
    }
    else {
      this.dataGrid.instance.clearFilter();
    }

    this.form.reset({
      searchCriteria: '',
      searchText: ''
    });
  }


  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => { },
        error => {
          this.errorKeys.push(key);
          hasError = true;
        },
        () => {
          this.getQualifications();
          this.spinner.hide();
          if (this.errorKeys.length == this.selectedItemKeys.length) {
            this.helper.notifyRemoveFormMessage(hasError, "", true);
          } else {
            this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
          }
        });
    });
  }
}
