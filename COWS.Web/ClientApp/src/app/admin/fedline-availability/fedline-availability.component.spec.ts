import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FedlineAvailabilityComponent } from './fedline-availability.component';

describe('FedlineAvailabilityComponent', () => {
  let component: FedlineAvailabilityComponent;
  let fixture: ComponentFixture<FedlineAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FedlineAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FedlineAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
