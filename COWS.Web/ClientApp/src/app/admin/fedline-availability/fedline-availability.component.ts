import { Component, OnInit } from '@angular/core';
import { TimeSlotOccurence, FedlineManageUserEvent } from "../../../models/index";
import { TimeSlotService, UserService } from '../../../services/index';
import { Observable } from "rxjs";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from '../../../shared/global';
import { exportDataGrid } from 'devextreme/excel_exporter';
import * as moment from 'moment';

@Component({
  selector: 'app-fedline-availability',
  templateUrl: './fedline-availability.component.html',
  styleUrls: ['./fedline-availability.component.css']
})
export class FedlineAvailabilityComponent implements OnInit {

  public timeSlots: Observable<TimeSlotOccurence[]>;
  public invalidTSEvents: Observable<FedlineManageUserEvent[]>;
  public disconnectEvents: Observable<FedlineManageUserEvent[]>;
  public events: Observable<any[]>;
  public eventDay: string;
  public eventTimeSlot: string;
  public updResultset: string = '';
  public csgLevelUser: boolean;
  public columnResizingMode: string = 'nextColumn';
  public showEditButton: boolean = false;

  constructor(private service: TimeSlotService, private router: Router,
    public helper: Helper, private spinner: NgxSpinnerService, public userService: UserService) { }

  ngOnInit() {
    this.getTimeSlotOccurrences();
    this.getInvalidTSEvents();
    this.getDisconnectEvents();
    this.init();
  }
  init()
  {
 this.userService.getUserProfilesByADID(this.userService.loggedInUser.adid).subscribe(
      res => {
        console.log(res);
             for(let i=0; i<res.length; i++){
        if(res[i].usrPrfDes.includes("Fedline MNS Activator")){
              this.showEditButton = true;
            }
          else
            this.showEditButton = false;
          }
      },
      error => {
        this.spinner.hide();
      });
  }


  public getTimeSlotOccurrences() {
    this.spinner.show();
    this.userService.isCSGUser(localStorage.getItem('userADID'), 2).toPromise().then(
      res => {
        this.csgLevelUser = res;
        if (this.csgLevelUser) {
          this.service.getTimeSlotOccurances(9, false).subscribe(
            res => {

              let day = '';
              this.timeSlots = res.map(record => {
                if(day != record.day) {
                  day = record.day;
                  record.dayDisp = record.day;
                } else {
                  record.dayDisp = '';
                }
                if(record.dayDisp !== '') {
                  record.dayDisp = moment(record.dayDisp).format('MM/DD/YYYY');
                }
               
                return record;
              });
              setTimeout(() => {
                this.spinner.hide();
              }, 0);
            },
            error => {
              this.spinner.hide();
            });
        }
        else {
          this.spinner.hide();
          this.router.navigate(['/csg-error-page']);
        }
      },
      error => {
        this.spinner.hide();
      });    
  }

  public getInvalidTSEvents() {
    this.service.getInvalidTSEvents().subscribe(
      res => {
        this.spinner.show();
        this.invalidTSEvents = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  public getDisconnectEvents() {
    this.service.getDisconnectEvents().subscribe(
      res => {
        this.spinner.show();
        this.disconnectEvents = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }
  public getFedlineEventsForTimeSlot(data: any, rsrcAvlbltyID: number) {
    //alert('rsrcAvlbltyID ' + rsrcAvlbltyID + 'Date: ' + data.data.day +' '+ data.data.displayTimeSlot);
    this.eventDay = data.data.day;
    this.eventTimeSlot = data.data.displayTimeSlot;

    this.spinner.show();
    this.service.getFedEventsForTS(rsrcAvlbltyID).subscribe(
      res => {
        this.events = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  public updateFedlineCount(rsrcAvlbltyID: number, input: string) {
    //alert('rsrcAvlbltyID: ' + rsrcAvlbltyID + ' ;input: ' + input);
    if (!this.updResultset.includes(rsrcAvlbltyID + '|'))
      this.updResultset = this.updResultset + ',' + rsrcAvlbltyID + '|' + input;
  }

  public updateFedlineEvents() {
    this.spinner.show();
    this.service.updateResourceAvailability(this.updResultset).subscribe(
      res => {
        //this.events = res;
        this.helper.notify(`Successfully updated Allowed Fedline Events`,
          Global.NOTIFY_TYPE_SUCCESS);
        this.spinner.hide();
        this.clear();
      },
      error => {
        this.spinner.hide();
      });
  }

  highlightOverbooked(info) {
    if (info.rowType === 'data') {
      if (info.data.isOverbooked === 'True')
        info.rowElement.classList.add('overbooked');
    }
  }

  public clear() {
    this.updResultset = '';
    this.events = null;
    this.eventDay = null;
    this.eventTimeSlot = null;
    this.getTimeSlotOccurrences();
  }

  selectionChangedHandler(id) {
    this.router.navigate(['event/fedline/' + id]);
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.component.beginUpdate();

    e.component.columnOption('dayDisp', 'text-align', 'left');
    e.component.columnOption('isOverbooked', 'visible', true);
    e.fileName = 'Resource_Count_' + this.helper.formatDate(new Date());
    
  }

  onExported(e) {
    e.component.columnOption("isOverbooked", "visible", false);  
    e.component.endUpdate(); 
  }

  onContentReady(e) {  
    e.element.find(".dx-datagrid-text-content").removeClass("dx-text-content-alignment-left");  
  }  
}

