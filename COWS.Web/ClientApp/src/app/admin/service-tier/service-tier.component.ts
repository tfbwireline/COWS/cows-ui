import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { MDSServiceTier } from "../../../models/index";
import { ServiceTierService, EventTypeService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-service-tier',
  templateUrl: './service-tier.component.html'
})

export class ServiceTierComponent implements OnInit {
  items: Observable<MDSServiceTier[]>;
  serviceTier = new MDSServiceTier();
  eventTypes: Observable<any[]>;
  selectedItemKeys: any[] = [];
  serviceTierId: any;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: ServiceTierService, private eventTypesservice: EventTypeService) { }

  ngOnInit() {
    this.getServiceTier();
    this.getEventTypes();
  }

  getServiceTier() {
    this.spinner.show();
    this.service.getServiceTier().subscribe(
      res => {
        console.log(res);
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }
  
  getEventTypes() {
    this.eventTypesservice.getEventTypes().subscribe(
      res => {
        this.eventTypes = res.filter(i => i.recStusId == Global.REC_STATUS_ACTIVE);
      });
  }
  
  clear() {
    this.serviceTierId = 0;
    this.serviceTier = new MDSServiceTier();
    this.service.getServiceTier().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.serviceTierId = event.data.mdsSrvcTierId;
  }

  saveForm(event: any) {
    this.spinner.show();
    if (!this.helper.isEmpty(event.key)) {
      this.serviceTier = event.oldData;
      this.serviceTier.mdsSrvcTierDes = this.helper.isEmpty(event.newData.mdsSrvcTierDes)
        ? event.oldData.mdsSrvcTierDes : event.newData.mdsSrvcTierDes;
      this.serviceTier.eventTypeId = this.helper.isEmpty(event.newData.eventTypeId)
        ? event.oldData.eventTypeId : event.newData.eventTypeId;
      this.service.updateServiceTier(this.serviceTierId, this.serviceTier).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.serviceTier.mdsSrvcTierDes,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.serviceTier.mdsSrvcTierDes,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    else {
      this.serviceTier = event.data;
      this.service.createServiceTier(this.serviceTier).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.serviceTier.mdsSrvcTierDes,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.serviceTier.mdsSrvcTierDes,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.deleteServiceTier(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });

    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
