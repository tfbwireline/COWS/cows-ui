import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { User } from "../../../models/index";
import { UserService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {
  //public users: Observable<User[]>;
  public users$;

  public constructor(private userService: UserService, private router: Router, private spinner: NgxSpinnerService) { }

  public ngOnInit() {
    this.getUsers();
  }


  public getUsers() {
    this.spinner.show();
    this.userService.getUsers().subscribe(
      res => {
        //this.users = res;
        this.users$ = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }
}
