import { Component, OnInit } from '@angular/core';
import { UserService, UserProfileService, ManageUsersService, ProductTypeService, PlatformService, CountryService, OrderActionService } from '../../../services/index';
import { User, UserProfile, WfmUserAssignments, ManageUsers } from "../../../models/index";
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subject } from 'rxjs';
import { debounceTime, switchMap, tap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import notify from 'devextreme/ui/notify';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {
  loggedInUser : any;
  usersForm: FormGroup;
  searchTerm: FormControl = new FormControl();
  searchManager: FormControl = new FormControl();
  userAdid: FormControl = new FormControl();
  searchUsers : Array<any> = [];
  //searchManagersList = <any>[];
  userProfiles : any;

  displayedColumns = [];
  userData = new User();
  //managerUserData = new User();
  orderActions : Array<any> = [];
  //productTypes = <any>[];
  managerList : Array<any> = [];
  lblAssignment: string;
  selectedAssignmentKeys: string = '';
  selectedOrderActionKeys: string = '';
  selectedProdPlatCombKeys: string = '';
  selectedOriginatingCountryKeys: string = '';
  selectedProfileID: number = 0;
  wfmUserAssignments = new WfmUserAssignments();

  //original values on page load:
  status: boolean = false;
  oldStatus: string = "0";
  newStatus: string = "";
  oldManagerAdid: string;
  newManagerAdid: string = "";
  oldAutoAssignment: string = "0";
  newAutoAssignment: string = "";
  displayAutoAssignment: boolean = false;

  public assignmentTypeOptions = [
    { "id": 0, "name": "Manual Assignments Only" },
    { "id": 1, "name": "Auto Assignments" }
  ]

  columnNames = [{
    id: "usrPrfDes",
    value: "Profile Name"

  }, {
    id: "recStusId",
    value: "Access"
  }];

  private viewType : string = '';
  private originatingCountry: number = 0;
  private userList: Array<User> = [];

  selectedUserProfile : Array<any> = [];
  wfmUserAssignmentsList : Array<any> = [];
  wfmUserAssignmentsListSub : Subject<any> = new Subject();
  prodPlatCombinedList : Array<any> = [];
  prodPlatCombinedListSub : Subject<any> = new Subject();
  originatingCountryList : Array<any> = [];
  originatingCountryListSub : Subject<any> = new Subject();
  orderActionList : Array<any> = [];
  orderActionListSub : Subject<any> = new Subject();

  isCANDAdhocRemove : boolean = false;
  candAdhocProfile : any = {
    poolCd: null,
    recStusId: 0,
    userId: 0,
    usrPrfDes: "CAND Adhoc Reports User",
    usrPrfId: 29
  };
  public csgLevel: string = '';

  isPooled: boolean = false;
  poolCodeProfiles = [
    "MDS Event Reviewer",
    "MDS Event Activator",
    "MDS CPT Manager",
    "MDS CPT GateKeeper",
    "MDS CPT CRM"
  ]

  constructor(private userService: UserService, private orderActionService: OrderActionService, private userProfileService: UserProfileService, private manageUserService: ManageUsersService, private productTypeService: ProductTypeService, private platformService: PlatformService, private countryService: CountryService, public helper: Helper, private spinner: NgxSpinnerService, private fb: FormBuilder) { }

  private resetValues(includeList: boolean = false) {
    // Reset checked values;
    this.selectedAssignmentKeys = '';
    this.selectedOrderActionKeys = '';
    this.selectedProdPlatCombKeys = '';
    this.selectedOriginatingCountryKeys = '';

    if(includeList) {
      this.wfmUserAssignmentsListSub.next([]);
      this.prodPlatCombinedListSub.next([]);
      this.originatingCountryListSub.next([]);
      this.originatingCountryListSub.next([]);
      this.orderActionListSub.next([]);
    }
  }

  public susbscriptions() {
    this.wfmUserAssignmentsListSub.subscribe(data => {
      this.wfmUserAssignmentsList = data;
    })

    this.prodPlatCombinedListSub.subscribe(data => {
      this.prodPlatCombinedList = data;
    })

    this.originatingCountryListSub.subscribe(data => {
      this.originatingCountryList = data;
    })

    this.orderActionListSub.subscribe(data => {
      this.orderActionList = data;
    })
  }

  public loadDefault() {

  }

  private assignmentInsertValid(): boolean {
    if (this.viewType === 'AMNCI' || this.viewType === 'ANCI' || this.viewType === 'ENCI') {
      return this.selectedProdPlatCombKeys !== '' && this.selectedOriginatingCountryKeys != '';
    } else if(this.viewType === 'GOM') {
      return this.selectedOrderActionKeys !== '' && this.selectedProdPlatCombKeys != '';
    }
  }
  get isCountryColumnVisible() {
    if (this.viewType === 'AMNCI' || this.viewType === 'ANCI' || this.viewType === 'ENCI') {
      return true;
    }
    else
      return false;
  }
  
  private getUser(key: string) {
      setTimeout(() => {
        const users : Array<User> = Object.assign([], this.userList);
        const newSearchUsers =  users.filter(x => x.fullNme.toLowerCase().includes(key) || x.emailAdr.toLowerCase().includes(key) || x.userAdid.toLowerCase().includes(key));
        var filteredUSer = [];
        if(newSearchUsers.filter(x => x.userAdid.toLowerCase()  == key).length > 0) {
          filteredUSer = newSearchUsers.filter(x => x.userAdid.toLowerCase()  == key);
        } else if(newSearchUsers.filter(x => x.fullNme.toLowerCase() == key).length > 0)
        {
          filteredUSer = newSearchUsers.filter(x => x.fullNme.toLowerCase()  == key);
        }
        
        this.searchUsers =  (filteredUSer.length > 0) ?  Object.assign([], filteredUSer) : Object.assign([], newSearchUsers);
      }, 500);
  }

  ngOnInit() {

    // Create formGroup for search
    this.usersForm = this.fb.group({
      userInput: null
    });

    this.searchTerm.valueChanges.subscribe(key => {
      
        if (key != '') {
          if(key.includes('|')) {
            key = key.split('|')[1].trim();
          }

         this.getUser(key.toLowerCase());
        }
    });

    this.userService.getLoggedInUser().subscribe(data => {
        this.loggedInUser = data;
    })

    // Get all users, used as managerList
    this.userService.getAllUsers().subscribe(data => {
        this.userList = data;
        this.managerList = data;
    })

    this.susbscriptions();
    this.displayedColumns = this.columnNames.map(x => x.id);

    this.getOrderActionList(false);
  }


  getUserByNameOrADID(term: string) {
    this.userService.searchUsers(term).subscribe(
      data => {
        this.searchUsers = data as any[];
            console.log('GET USERS 1');
      })
  }

  public cancel() {
    this.userData = null;
    this.searchTerm.patchValue('');
    this.selectedUserProfile = [];
  }

  candCondition(res : any[]) {

    // 29 CAND Adhoc Reports User
    // 31 CAND Canned Reports User
    // 36 CAND OnDemand Reports User

    const candProfiles = res.filter(x => x.usrPrfDes.includes('CAND'));
    if(candProfiles.length > 0) {
      if(candProfiles.find(x => (x.usrPrfId === 31 || x.usrPrfId === 36) && x.recStusId === 1)) {
        const index = res.findIndex(x => x.usrPrfId === 29);
        if(index !== -1) {
          res.splice(index, 1);
          this.selectedUserProfile = res;
          this.isCANDAdhocRemove = true;
        }
      } else {
        if(this.isCANDAdhocRemove) {
          const index = res.findIndex(x => x.usrPrfId === 30) > 0 ? res.findIndex(x => x.usrPrfId === 30) - 1 : 0;
          this.selectedUserProfile.splice(index, 0, this.candAdhocProfile);
          this.isCANDAdhocRemove = false;
        }
      }
    } 
  }
  
  searchUser(userId: number) {
    this.spinner.show();
    this.userService.getUserWithCsgLvlByUserID(userId).toPromise().then(
      res => {
        this.userData = res;
        if(res) {
          this.csgLevel = res.csgLvlCd;
        }
        //this.oldStatus = String(this.userData.recStusId);
        this.status = this.userData.recStusId ? true : false
        this.oldManagerAdid = this.userData.mgrAdid;

        this.userProfileService.getUserProfileByUserId(this.userData.userId, this.loggedInUser.userId).toPromise().then(
          res => {
            this.selectedUserProfile = res;
            this.candAdhocProfile.userId = userId;
            this.candCondition(res);
            
            // Load User Assignments
            //this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
    
            if (this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner') && (this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner').recStusId == 1 || this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner').recStusId == 2)) {
              this.displayAutoAssignment = true;
              this.selectedProfileID = this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner').usrPrfId;
              this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
              this.getOriginatingCountry(1);
              this.getProductPlatformCombinations();
              this.viewType = 'AMNCI';
              this.originatingCountry = 1;
            }
            else if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner') && (this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner').recStusId == 1 || this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner').recStusId == 2)) {
              this.displayAutoAssignment = true;
              this.selectedProfileID = this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner').usrPrfId;
              this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
              this.getOriginatingCountry(2);
              this.getProductPlatformCombinations();
              this.viewType = 'ENCI';
              this.originatingCountry = 2;
            }
            else if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner') && (this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner').recStusId == 1 || this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner').recStusId == 2)) {
              this.displayAutoAssignment = true;
              this.selectedProfileID = this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner').usrPrfId;
              this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
              this.getOriginatingCountry(3);
              this.getProductPlatformCombinations();
              this.viewType = 'ANCI';
              this.originatingCountry = 3;
            }
            else if (this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner') && (this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner').recStusId == 1 || this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner').recStusId == 2)) {
              this.selectedProfileID = this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner').usrPrfId;
              this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
              this.getOrderActionList(true);
              this.getProductPlatformCombinations();
              this.originatingCountryList = [];
              this.viewType = 'GOM';
            }

            if (this.selectedUserProfile.filter(i => this.poolCodeProfiles.includes(i.usrPrfDes)).length > 0) {
              this.isPooled = true;
            }

            this.selectedUserProfile = this.helper.ArrayValueStringReplace(res, ['usrPrfDes']);
            this.spinner.hide();
          },
          error => {
            this.selectedUserProfile = [];
            this.spinner.hide();
            this.helper.notify(`No data found for this User `, Global.NOTIFY_TYPE_ERROR);
          });

      }
    )
  }

  AddUser(userAdid: string) {
    this.spinner.show();
    this.userService.InsertUser(userAdid).toPromise().then(
      res => {
        this.userProfiles = res;
        this.helper.notify(`Successfully Created New User Profile for ${userAdid}.`,
          Global.NOTIFY_TYPE_SUCCESS);
        this.resetValues(true);
        setTimeout(() => {
          this.spinner.hide();
        }, 0);
        this.searchUser(this.userProfiles.userId);
        this.getUserByNameOrADID(this.userProfiles.userAdid);
      },
      error => {
        //this.helper.notify(`Failed to Create New User for ${userAdid}. ` + error.message, Global.NOTIFY_TYPE_ERROR);
        //this.helper.notify(error.message, Global.NOTIFY_TYPE_ERROR);
        //this.clear();
        this.spinner.hide();
      });
  }

  updateUserProfile(selectedUserProfile: any[]) {
    this.spinner.show();

    const profiles = selectedUserProfile.filter( x=> x.recStusId == 1 || x.recStusId == 2);
    
    this.userProfileService.updateUserProfile(profiles).subscribe(
      res => {
        console.log('updateUserProfile successful');
        this.updateUserPermissions();
        this.helper.notify(`Successfully saved User Profile for ${this.searchUsers[0].fullNme}.`,
          Global.NOTIFY_TYPE_SUCCESS);
        setTimeout(() => {
          this.spinner.hide();
        }, 0);
      },
      error => {
        this.helper.notify(`Failed to save User Profile for ${this.searchUsers[0].fullNme}. `, Global.NOTIFY_TYPE_ERROR);
        this.spinner.hide();
      },
      () =>  {
        this.spinner.hide();
      }
    );
  }

  onClick(e, data) {
    this.selectedUserProfile[data.row.rowIndex].recStusId = e ? 1 : 0;

    var profile = this.selectedUserProfile[data.row.rowIndex].usrPrfDes;

    if (this.selectedUserProfile[data.row.rowIndex].recStusId == 1) {
      if (profile.includes("NCI WFM Assigner")) {
        this.selectedProfileID = this.selectedUserProfile[data.row.rowIndex].usrPrfId;
        this.displayAutoAssignment = true;
        this.selectedOrderActionKeys = this.orderActions.find(a => a.ordrActnDes == 'Submit').ordrActnId;
        this.lblAssignment = 'NCI Assignments';
        this.orderActionList = [];
        if (profile.includes("AMNCI WFM Assigner")) {
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner').recStusId = 0;
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner').recStusId = 0;
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner').recStusId = 0;
          this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
          this.getOriginatingCountry(1);
          this.getProductPlatformCombinations();
          this.viewType = 'AMNCI';
          this.originatingCountry = 1;
        }
        else if (profile.includes("ENCI WFM Assigner")) {
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner').recStusId = 0;
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner').recStusId = 0;
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner').recStusId = 0;
          this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
          this.getOriginatingCountry(2);
          this.getProductPlatformCombinations();
          this.viewType = 'ENCI';
          this.originatingCountry = 2;
        }
        else if (profile.includes("ANCI WFM Assigner")) {
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner').recStusId = 0;
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner').recStusId = 0;
          if (this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner'))
            this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner').recStusId = 0;
          this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
          this.getOriginatingCountry(3);
          this.getProductPlatformCombinations();
          this.viewType = 'ANCI';
          this.originatingCountry = 3;
        }
      }
      else if (profile.includes("GOM WFM Assigner")) {
        this.selectedProfileID = this.selectedUserProfile[data.row.rowIndex].usrPrfId;
        if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner'))
          this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner').recStusId = 0;
        if (this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner'))
          this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner').recStusId = 0;
        if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner'))
          this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner').recStusId = 0;
        this.lblAssignment = 'GOM Assignments';
        this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
        this.getOrderActionList(true);
        this.getProductPlatformCombinations();
        this.originatingCountryList = [];
      }
    }
    else {
      if (profile.includes("NCI WFM Assigner") || profile.includes("GOM WFM Assigner")) {
        if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner'))
          this.selectedUserProfile.find(a => a.usrPrfDes === 'ENCI WFM Assigner').recStusId = 0;
        if (this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner'))
          this.selectedUserProfile.find(a => a.usrPrfDes === 'AMNCI WFM Assigner').recStusId = 0;
        if (this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner'))
          this.selectedUserProfile.find(a => a.usrPrfDes === 'ANCI WFM Assigner').recStusId = 0;
        if (this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner'))
          this.selectedUserProfile.find(a => a.usrPrfDes === 'GOM WFM Assigner').recStusId = 0;

        this.wfmUserAssignmentsListSub.next([]);
        this.orderActionListSub.next([]);
        this.prodPlatCombinedListSub.next([]);
        this.originatingCountryListSub.next([]);
        this.lblAssignment = null;
      }
    }

    var profileId = this.selectedUserProfile[data.row.rowIndex].usrPrfId;
    if(profileId === 31 || profileId === 36) {
      this.candCondition(this.selectedUserProfile);
    }
  }

  onPoolCdClick(e, data) {
    this.selectedUserProfile[data.row.rowIndex].poolCd = e ? 1 : 0;
  }

  ClearForm() {
    this.searchTerm.patchValue(null);
    //this.userAdid = null;
    this.selectedAssignmentKeys = '';
  }

  getWFMUserAssignments(userId: number, profId: number) {
    this.spinner.show();
    this.manageUserService.getWFMUserAssignments(userId, profId).subscribe(
      data => {
        this.wfmUserAssignmentsList = data;
        if (this.wfmUserAssignmentsList.length > 0) {
          this.oldAutoAssignment = this.wfmUserAssignmentsList[0].wfmAsmtLvlId;
          console.log('oldAutoAssignment: ' + this.oldAutoAssignment)
        }
        this.spinner.hide();
      })
  }

  getOrderActionList(display: boolean) {
    this.orderActionList = [];
    this.spinner.show();
    this.orderActionService.get().subscribe(
      data => {
        if (display) {
          this.orderActionListSub.next(data);
        } else {
          this.orderActions = data 
        }
        this.spinner.hide();
      })
  }

  getProductPlatformCombinations() {
    this.prodPlatCombinedListSub.next([]);
    this.spinner.show();
    this.productTypeService.getProductPlatformCombinations().subscribe(
      data => {
        this.prodPlatCombinedListSub.next(data);
        this.spinner.hide();
      })
  }

  getOriginatingCountry(regionId: number) {
    this.originatingCountryListSub.next([]);
    this.spinner.show();
    this.countryService.getCountriesByRegion(regionId).subscribe(
      data => {
        this.originatingCountryListSub.next(data);
        this.spinner.hide();
      })
  }


  wfmAssignmentsSelectionChangedHandler(data: any) {
    if(data['selectedRowsData'].length > 0) {
      this.selectedAssignmentKeys = data.selectedRowKeys + ',';
    } else {
      this.selectedAssignmentKeys = '';
    }
    
  }

  deleteAssignments() {
    this.spinner.show();
    this.manageUserService.updateWFMUserAssignments(this.selectedAssignmentKeys).toPromise().then(
      res => {
        //this.events = res;
        this.helper.notify(`Successfully deleted WFM User Assignments`,
          Global.NOTIFY_TYPE_SUCCESS);
        this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);


        if (this.viewType === 'AMNCI' || this.viewType === 'ANCI' || this.viewType === 'ENCI') {
          this.getOriginatingCountry(this.originatingCountry);
        } else {
          this.getOrderActionList(true);
        }
        this.getProductPlatformCombinations();

        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  orderActionSelectionChangedHandler(data: any) {
    this.selectedOrderActionKeys = data.selectedRowKeys + ',';
  }

  prodPlatCombSelectionChangedHandler(data: any) {
    this.selectedProdPlatCombKeys = data.selectedRowKeys + ',';
  }

  originatingCountrySelectionChangedHandler(data: any) {
    this.selectedOriginatingCountryKeys = data.selectedRowKeys + ',';
  }

  managerSelectionChange(value) {
    if (value.value == this.oldManagerAdid) {
      console.log('no change: ', value.value);
    }
    else {
      this.newManagerAdid = value.value;
      console.log('selectChange: ', value.value);
    }
  }

  //statusSelectionChange(value) {
  //  console.log('statusSelectionChange: ', this.oldStatus);
  //  if (value == this.oldStatus) {
  //    this.newStatus = value;
  //    console.log('no change: ', value);
  //  }
  //  else {
  //    this.newStatus = value;
  //    console.log('selectChange: ', value);
  //  }
  //}

  assignmentSelectionChange(value) {
    console.log('assignmentSelectionChange: ', value.value);
    this.newAutoAssignment = value.value;
  }

  insertWFMAssignment() {
    this.wfmUserAssignments.userId = this.userData.userId;
    if (this.viewType === 'AMNCI')
      this.wfmUserAssignments.grpId = 5;
    else if (this.viewType === 'ANCI')
      this.wfmUserAssignments.grpId = 6;
    else if (this.viewType === 'ENCI')
      this.wfmUserAssignments.grpId = 7;
    else
      this.wfmUserAssignments.grpId = 1;

    this.wfmUserAssignments.roleList = '50';

    //if (this.selectedOrderActionKeys != '') {
    //    console.log("Tony 1-A: ", this.selectedOrderActionKeys)
    //    this.wfmUserAssignments.orderActionList = this.selectedOrderActionKeys;
    //}
    //else {
    //    console.log("Tony 1-B: ", this.orderActions)
    //    this.wfmUserAssignments.orderActionList = this.orderActions;
    //}
    if (this.viewType === 'AMNCI' || this.viewType === 'ANCI' || this.viewType === 'ENCI') {
      this.wfmUserAssignments.orderActionList =  this.selectedOrderActionKeys === "" ? '2' : this.selectedOrderActionKeys;
    } else {
      this.wfmUserAssignments.orderActionList = this.selectedOrderActionKeys
    }
    
    this.wfmUserAssignments.prodPlatList = this.selectedProdPlatCombKeys;
    this.wfmUserAssignments.wfmAsmtLvlId = Number(this.newAutoAssignment);
    this.wfmUserAssignments.countryOrigList = this.selectedOriginatingCountryKeys;
    this.wfmUserAssignments.usrPrfId = this.selectedProfileID;

    this.manageUserService.insertWFMAssignment(this.wfmUserAssignments).toPromise().then(
      data => {
        this.getWFMUserAssignments(this.userData.userId, this.selectedProfileID);
      })
  }

  updateUserPermissions() {
    this.spinner.show();
    console.log(this.userData.recStusId)
    console.log(this.status)

    this.manageUserService.updateUserPermissions(this.userData.userId, this.status ? "1" : "0", this.newManagerAdid, this.newAutoAssignment).subscribe(
      res =>  {
        if(this.assignmentInsertValid()) {
          this.insertWFMAssignment();
          if (this.viewType === 'AMNCI' || this.viewType === 'ANCI' || this.viewType === 'ENCI') {
            this.getOriginatingCountry(this.originatingCountry);
          } else {
            this.getOrderActionList(true);
          }
          this.getProductPlatformCombinations();
        }
        this.resetValues();
        this.spinner.hide();
      },
      error => {
        this.helper.notify(`Failed to save User Permissions for ${this.searchUsers[0].fullNme}. `, Global.NOTIFY_TYPE_ERROR);
        this.spinner.hide();
      },
      () =>  {
        this.spinner.hide();
      }
    );
  }

  //highlightOverbooked(info) {
  //  if (info.rowType === 'data') {
  //    if (info.data.recStusId === 2)
  //      info.rowElement.classList.add('overbooked');
  //  }
  //}
}
