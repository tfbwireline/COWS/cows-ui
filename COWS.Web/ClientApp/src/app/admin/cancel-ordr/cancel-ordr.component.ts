import { Component, OnInit, ViewChild } from '@angular/core';
import { CancelOrder } from "../../../models/index";
import { getCancelOrdrViewService, NavLinkService } from '../../../services/index';
import { Router, CanActivate, ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs/internal/Observable';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-unlock-items',
  templateUrl: './cancel-ordr.component.html'
})

export class CancelOrdrComponent implements OnInit, CanActivate {
  unlockItems$: any[];
  items: Observable<CancelOrder[]>;
  ordr = new CancelOrder();
  ftn: string = "";
  h5_H6: string = "";
  isSubmitted = false;
  selectedItems = false;
  displaySuccessMessage = false;
  displayErrorMessage = false;
  displayNotfound = false;
  selectedItemKeys: any[] = [];
  txtEmpty = true;
  name: string;
  validationMessage: string;
  ordrId: string;
  userId: string;
  itemType: string;
  selectedOption: number = 0;
  validMenu: string;
  
  @ViewChild('form', {static : true}) public form: any;
  
  constructor(private service: getCancelOrdrViewService, private router: Router, public helper: Helper,
    private spinner: NgxSpinnerService, private navLinkService: NavLinkService) { }

  ngOnInit() {
   
  }

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    let validUser = localStorage.getItem('validUser');
    let url = state.url.split('/');
    this.navLinkService.validateMenu(url).subscribe(res => {
      this.validMenu = res;
      // Inactive users will be redirected to unauthorized page.
      // Should only happen if the user tries to force their way
      if (validUser == "true" && this.validMenu =="authorized") {
        return true
      }
      
      else {
        console.log("cancelorder");
        //this.helper.notify("")
        this.router.navigate(['/authorize'], { queryParams: { returnUrl: state.url } })
        return false
      }
    });
    return true;

  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(childRoute, state);
  }

  clear() {
    this.validationMessage = "";
    this.displaySuccessMessage = false;
    this.displayErrorMessage = false;
    this.selectedItems = false;
    this.displayNotfound = false;
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }
  onSubmit($event) {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    this.spinner.show();
    this.ordr = new CancelOrder();
    this.clear();
    if (this.selectedOption == 0) {    //if Rowsare checked in grid
      if(this.unlockItems$!=null) {
        this.ftn = "FTN: " + this.unlockItems$[0].ftn;
      }
    }

    this.selectedItemKeys.forEach((key) => {
      this.ordr.ftn = key.ftn
      this.ordr.userId = key.userId;
      this.ordr.productType = key.productType;
      this.ordr.ordrType = key.ordrType;
      this.ordr.custName = key.custName;
      this.ordr.notes = key.notes;
      this.ordr.ccd = key.ccd;
      this.ordr.h5_H6 = key.h5_h6;

      this.service.cancelOrdr(this.ordr).subscribe(
        data => {
          this.helper.notify(`Successfully Cancelled ${this.ftn}.`,
            Global.NOTIFY_TYPE_SUCCESS);
        
          this.clear();
          this.displaySuccessMessage = true;

          setTimeout(() => {
            this.spinner.hide();
          }, 0);
        },
        error => {
          if (!this.helper.isEmpty(error.Message) && error.Message.indexOf('duplicate') !== -1) {
            this.helper.notify(`${this.ftn} already exists.`, Global.NOTIFY_TYPE_ERROR);
          }
          else if (!this.helper.isEmpty(error) && error.indexOf('duplicate') !== -1) {
            this.helper.notify(`${this.ftn} already exists.`, Global.NOTIFY_TYPE_ERROR);
          }
          else {
            this.helper.notify(`Failed to Cancell ${this.ftn}. ` + error, Global.NOTIFY_TYPE_ERROR);
          }

          this.clear();
          this.displayErrorMessage = true;
          this.spinner.hide();
          });
    });

    this.spinner.hide();
  }

  //event handler for the radio button's change event
  onValueChanged($event) {
    //update the ui
    this.selectedOption = Number($event.value);
    //console.log(this.selectedOption); 
  }

  signalSelected(nameVal: string) {
    nameVal = this.form.name
    if (nameVal == "") {
      //this.txtEmpty = false;
      //this.clear();
      //this.selectedItems = false;
    }
  }

  signalSelectedH5(nameVal: string) {
    nameVal = this.form.h5
    if (nameVal == "") {
      //this.txtEmpty = false;
      //this.clear();
      //this.selectedItems = false;
    }
  }

  onKey(event: any) { // without type info
    if (event.target.value = "") {
      this.clear();
      this.selectedItems = false;
    }
  }

  onSubmitSearch() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    this.ftn = this.form.name;
    this.h5_H6 = this.form.h5;
    if (this.form.name == null || this.form.name == undefined ) {
      this.ftn = "";
    }
    if (this.form.h5 == null || this.form.h5 == undefined) {
      this.h5_H6 = "";
    }
    if (this.ftn.length < 1 && this.h5_H6.length < 1) {
      this.clear();
      this.validationMessage = "Please enter either M5 # or H6 value.";
      this.displayNotfound = true;
      return;
    }
    if (this.h5_H6.length > 0) {
      if (this.h5_H6.length < 9 || !this.isNumber(this.h5_H6.length)) {
        this.clear();
        this.validationMessage = "H6 value should be exactly 9 digits.";
        this.displayNotfound = true;
        return;
      }
    } 
    this.spinner.show();
    this.clear();
    this.unlockItems$ = null;
    this.displayNotfound = false;
    this.h5_H6 = this.h5_H6 == "" ? "0" : this.h5_H6;
    this.ftn = this.ftn == "" ? "" : this.ftn;
    this.service.getCancelOrdr(this.ftn, this.h5_H6).subscribe(
      res => {
        this.unlockItems$ = res;
        this.items = res;
        for (let resultItem of this.unlockItems$) {
          
          this.clear();
          this.selectedItems = true;

        }
        if (this.unlockItems$.length < 1) {
          this.validationMessage = "There are no records for this M5 #_H6 which are active and need to be cancelled.";
          this.displayNotfound = true;
        }
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        this.clear();
        this.displayNotfound = true;
      });
  }

  isNumber(value: string | number): boolean {
    return !isNaN(Number(value.toString()));
  }
}
