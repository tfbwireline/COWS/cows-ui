import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { EnhncSrvc } from "../../../models/index";
import { EnhncSrvcService, EventTypeService } from '../../../services/index';
import { MaskUtil } from "../../../shared/mask/mask.util";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-enhanced-services',
  templateUrl: './enhanced-services.component.html'
})

export class EnhncSrvcComponent implements OnInit {
  items: Observable<EnhncSrvc[]>;
  enhncSrvc = new EnhncSrvc();
  eventTypes: Observable<any[]>;
  selectedItemKeys: any[] = [];
  enhncSrvcId: any;
  phoneMask = MaskUtil.PHONE_MASK_GENERATOR;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: EnhncSrvcService, private eventTypesservice: EventTypeService) { }

  ngOnInit() {
    this.getEnhncSrvc();
    this.getEventTypes();
  }

  getEnhncSrvc() {
    this.spinner.show();
    this.service.getEnhncSrvc().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }
  
  getEventTypes() {
    this.eventTypesservice.getEventTypes().subscribe(
      res => {
        this.eventTypes = res.filter(i => i.recStusId == Global.REC_STATUS_ACTIVE);
      });
  }
  
  clear() {
    this.enhncSrvcId = 0;
    this.enhncSrvc = new EnhncSrvc();
    this.service.getEnhncSrvc().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.enhncSrvcId = event.data.enhncSrvcId;
  }

  saveForm(event: any) {
    this.spinner.show();

    if (this.enhncSrvcId > 0) {
      // Update Enhance Servie
      this.enhncSrvc = event.oldData;
      this.enhncSrvc.enhncSrvcNme = this.helper.isEmpty(event.newData.enhncSrvcNme)
        ? event.oldData.enhncSrvcNme : event.newData.enhncSrvcNme;
      this.enhncSrvc.eventTypeId = this.helper.isEmpty(event.newData.eventTypeId)
        ? event.oldData.eventTypeId : event.newData.eventTypeId;
      this.service.updateEnhncSrvc(this.enhncSrvcId, this.enhncSrvc).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.enhncSrvc.enhncSrvcNme,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.enhncSrvc.enhncSrvcNme,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    else {
      // Create Enhance Service
      this.enhncSrvc = event.data;
      this.service.createEnhncSrvc(this.enhncSrvc).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.enhncSrvc.enhncSrvcNme,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.enhncSrvc.enhncSrvcNme,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.deleteEnhncSrvc(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });

    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
