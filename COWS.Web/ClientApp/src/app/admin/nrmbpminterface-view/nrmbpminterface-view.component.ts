import { Component, Inject, OnInit } from '@angular/core';
import { NRMBPMInterfaceViewService } from '../../../services/index';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-nrmbpminterface-view',
  templateUrl: './nrmbpminterface-view.component.html'
})
export class NRMBPMInterfaceViewComponent implements OnInit {

  public nrmbpmInterfaceView$: any[];

  constructor(private nrmbpmService: NRMBPMInterfaceViewService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getNRMBPMInterfaceView();
  }

  public getNRMBPMInterfaceView() {
    this.spinner.show();
    this.nrmbpmService.getNRMBPMInterfaceView().subscribe(
      res => {
        this.nrmbpmInterfaceView$ = res;
        //console.log("nrmbpmInterfaceView$" + res);
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

}
