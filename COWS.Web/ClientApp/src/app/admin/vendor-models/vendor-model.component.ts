import { Component, OnInit } from '@angular/core';
import { Observable, of, zip } from "rxjs";
import { DeviceModel, DeviceManufacturer } from "../../../models/index";
import { DeviceModelService, DeviceManufacturerService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";
import $ from "jquery";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'app-vendor-model',
  templateUrl: './vendor-model.component.html'
})
export class VendorModelComponent implements OnInit {
  form: FormGroup;

  // Device/Vendor Model Properties
  items: Observable<DeviceModel[]>;
  vendorModel = new DeviceModel();
  selectedItemKeys: any[] = [];
  vendorModelId: number = 0;

  // Device/Vendor Manufacturer Properties
  manufacturerList: Observable<any[]>;
  selectedManufacturer = new DeviceManufacturer();
  rememberManfSelection: boolean = false;

  // UI Fields
  get manfList() { return this.form.get("manfList") }
  get txtDevModel() { return this.form.get("txtDevModel") }
  get txtDuration() { return this.form.get("txtDuration") }
  get manufacturerName() { return this.form.get("manufacturerName") }
  get chkStatus() { return this.form.get("chkStatus") }
  get chkRemember() { return this.form.get("chkRemember") }

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: DeviceModelService, private manufacturerService: DeviceManufacturerService) { 
      this.loadEditForm = this.loadEditForm.bind(this);
    }

  ngOnInit() {
    this.getManufacturer();
    this.getVendorModels();
    this.setForm();
  }

  setForm() {
    this.form = new FormGroup({
      'manfList': new FormControl('', [Validators.required]),
      'txtDevModel': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'txtDuration': new FormControl('', [Validators.required]),
      'manufacturerName' : new FormControl(''),
      'chkRemember': new FormControl(''),
      'chkStatus': new FormControl('')
    });
  }

  clearForm() {
    this.form.reset();
    this.vendorModelId = 0;
    this.vendorModel = new DeviceModel();
    this.vendorModel.manfId = (this.rememberManfSelection) ? this.selectedManufacturer.manfId : 0;
    this.chkRemember.setValue(this.rememberManfSelection);
    this.manfList.setValue(this.vendorModel.manfId);

    if ($('#manfDiv').is(':visible')) {
      this.alter();
    }

    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  alter() {
    this.spinner.hide();
    $('#manfDiv').toggle();
    $('#manfAlterButton').toggle();
    $('#manfCloseButton').toggle();
    this.manufacturerName.setValue('');
  }

  //** Device/Vendor Manufacturer Functions **//
  getManufacturer() {
    this.manufacturerService.get().subscribe(
      res => {
        this.manufacturerList = res;
      });
  }

  onManufacturerChanged(event: any) {
    this.selectedManufacturer = event.selectedItem;
  }

  valueChangedHandler(event: any) {
    this.rememberManfSelection = event.value;
  }

  addManufacturer() {
    var manFName = this.manufacturerName.value;
    if (this.helper.isEmpty(manFName)) {
      this.helper.notify('Manufacturer Name is required', Global.NOTIFY_TYPE_WARNING);
    } else {
      this.spinner.show();
      var manf = new DeviceManufacturer();
      manf.manfNme = manFName;
      this.manufacturerService.create(manf).subscribe(
        res => {
          this.helper.notifySavedFormMessage(manFName, Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.getManufacturer();
          this.alter();
        },
        error => {
          this.helper.notifySavedFormMessage(manFName, Global.NOTIFY_TYPE_ERROR, true, error);
          this.alter();
        });
    }
  }

  removeManufacturer() {
    var manFName = this.manufacturerName.value;
    if (this.helper.isEmpty(manFName)) {
      this.helper.notify('Manufacturer Name is required', Global.NOTIFY_TYPE_WARNING);
    } else {
      this.spinner.show();
      this.manufacturerService.getByName(manFName).subscribe(
        res => {
          var manf = res;
          if (this.helper.isEmpty(manf)) {
            this.helper.notify(`${manFName} is not found.`, Global.NOTIFY_TYPE_WARNING);
            this.alter();
          }
          else {
            if (!manf.recStatus) {
              this.helper.notify(`${manFName} is already inactive.`, Global.NOTIFY_TYPE_WARNING);
              this.alter();
            }
            else {
              this.manufacturerService.delete(manf.manfId).subscribe(
                res1 => {
                  this.helper.notify(`Successfully removed ${manFName}.`, Global.NOTIFY_TYPE_SUCCESS);
                  this.getManufacturer();
                  this.alter();
                },
                error => {
                  this.helper.notify(`Failed in removing ${manFName}.`, Global.NOTIFY_TYPE_ERROR);
                  this.alter();
                });
            }
          }
        },
        error => {
          this.helper.notify(`Failed in removing ${manFName}.`, Global.NOTIFY_TYPE_ERROR);
          this.alter();
        });
    }
  }

  //** Device/Vendor Model Functions **//
  getVendorModels() {
    this.spinner.show();
    this.service.get().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  onEditingStart(e) {
    if (e.column.command == 'edit') {
      // Clear Remembering Manufacturer
      this.rememberManfSelection = false;
      this.selectedManufacturer = null;

      // Set values for Edit Form
      this.vendorModelId = e.row.key;
      this.vendorModel = e.row.data;
      this.form.patchValue({
        manfList: this.vendorModel.manfId,
        txtDevModel: this.vendorModel.devModelNme,
        txtDuration: this.vendorModel.minDrtnTmeReqrAmt,
        chkStatus: this.vendorModel.recStatus,
        chkRemember: this.rememberManfSelection,
        manufacturerName: ''
      });
    }
  }

  loadEditForm(e) {
    this.rememberManfSelection = false;
    this.selectedManufacturer = null;

    // Set values for Edit Form
    this.vendorModelId = e.row.key;
    this.vendorModel = e.row.data;
    this.form.patchValue({
      manfList: this.vendorModel.manfId,
      txtDevModel: this.vendorModel.devModelNme,
      txtDuration: this.vendorModel.minDrtnTmeReqrAmt,
      chkStatus: this.vendorModel.recStatus,
      chkRemember: this.rememberManfSelection,
      manufacturerName: ''
    });

    e.event.preventDefault();
    // return false;
  }

  onSubmit() {
    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.spinner.show();
    this.vendorModel.manfId = this.manfList.value; //this.form.value.manfList;
    this.vendorModel.devModelNme = this.txtDevModel.value; // this.form.value.txtDevModel;
    this.vendorModel.minDrtnTmeReqrAmt = this.txtDuration.value; // this.form.value.txtDuration;
    this.vendorModel.recStatus = this.chkStatus.value;
    this.vendorModel.recStusId = this.vendorModel.recStatus ? 1 : 0;
    console.log(this.vendorModel)

    // For Edit
    if (this.vendorModelId > 0) {
      let data = this.service.getById(this.vendorModelId).pipe(
        concatMap(res => {
          let model = res as DeviceModel
          console.log(model)

          // If user wants to deactivate Used Model
          if (this.vendorModel.recStusId == 0 && model.isAlreadyInUse) {
            return zip(
              of(model)
            )
          } else {
            return zip(
              of(model),
              this.service.update(this.vendorModelId, this.vendorModel)
            )
          }
        })
      )

      data.subscribe(
        res => {
          // Message for deactivating Used Model
          if (this.vendorModel.recStusId == 0 && res[0].isAlreadyInUse) {
            this.helper.notify(`'${res[0].devModelNme}' has an active record on Events/Qualifications and might cause issue/s when deactivated.`, Global.NOTIFY_TYPE_WARNING)
          } else {
            this.helper.notifySavedFormMessage(this.vendorModel.devModelNme, Global.NOTIFY_TYPE_SUCCESS, false, null);
          }
        },
        error => {
          console.log(error)
          this.helper.notifySavedFormMessage(this.vendorModel.devModelNme, Global.NOTIFY_TYPE_ERROR, false, error);
          this.spinner.hide()
        },
        () => {
          this.clearForm();
        });
    }
    // For Insert
    else {
      this.service.create(this.vendorModel).subscribe(
        res => {
          this.helper.notifySavedFormMessage(this.vendorModel.devModelNme,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clearForm();
        },
        error => {
          this.helper.notifySavedFormMessage(this.vendorModel.devModelNme,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clearForm();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.delete(key).subscribe(
        res => {
          this.clearForm();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });
    
    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
