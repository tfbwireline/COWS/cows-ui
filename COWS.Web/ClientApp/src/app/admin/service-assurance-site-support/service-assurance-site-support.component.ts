import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { ServiceAssuranceSiteSupport } from "../../../models/index";
import { ServiceAssuranceSiteSupportService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";

@Component({
  selector: 'app-service-assurance-site-support',
  templateUrl: './service-assurance-site-support.component.html'
})

export class ServiceAssuranceSiteSupportComponent implements OnInit {
  items: Observable<ServiceAssuranceSiteSupport[]>;
  SrvcAssrnSiteSupp = new ServiceAssuranceSiteSupport();
  selectedItemKeys: any[] = [];
  SrvcAssrnSiteSuppId: any;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private service: ServiceAssuranceSiteSupportService) { }

  ngOnInit() {
    this.getServiceAssuranceSiteSupport();
  }

  getServiceAssuranceSiteSupport() {
    this.spinner.show();
    this.service.getServiceAssuranceSiteSupport().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  clear() {
    this.SrvcAssrnSiteSuppId = 0;
    this.SrvcAssrnSiteSupp = new ServiceAssuranceSiteSupport();
    this.service.getServiceAssuranceSiteSupport().subscribe(
      res => {
        this.items = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  loadEditForm(event: any) {
    this.SrvcAssrnSiteSuppId = event.data.srvcAssrnSiteSuppId;
  }

  saveForm(event: any) {
    this.spinner.show();
    if (this.SrvcAssrnSiteSuppId > 0) {
      this.SrvcAssrnSiteSupp = event.oldData;
      this.SrvcAssrnSiteSupp.srvcAssrnSiteSuppDes = this.helper.isEmpty(event.newData.srvcAssrnSiteSuppDes)
        ? event.oldData.srvcAssrnSiteSuppDes : event.newData.srvcAssrnSiteSuppDes;
      this.service.updateServiceAssuranceSiteSupport(this.SrvcAssrnSiteSuppId, this.SrvcAssrnSiteSupp).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.SrvcAssrnSiteSupp.srvcAssrnSiteSuppDes,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.SrvcAssrnSiteSupp.srvcAssrnSiteSuppDes,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clear();
        });
    }
    else {
      this.SrvcAssrnSiteSupp = event.data;
      this.service.createServiceAssuranceSiteSupport(this.SrvcAssrnSiteSupp).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.SrvcAssrnSiteSupp.srvcAssrnSiteSuppDes,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clear();
        },
        error => {
          this.helper.notifySavedFormMessage(this.SrvcAssrnSiteSupp.srvcAssrnSiteSuppDes,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clear();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.service.deleteServiceAssuranceSiteSupport(key).subscribe(
        res => {
          this.clear();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });

    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyRemoveFormMessage(hasError, "", true);
    } else {
      this.helper.notifyRemoveFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}
