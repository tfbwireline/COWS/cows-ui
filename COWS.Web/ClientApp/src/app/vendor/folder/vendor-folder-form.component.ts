import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  CountryService, StateService, VendorService,
  VendorFolderService, UserService
} from '../../../services/index';
import {
  Vendor, VendorFolder, VendorFolderContact,
  VendorFolderTemplate, Country, State, DocEntity
} from '../../../models/index';
import { Global } from '../../../shared/global';
import { Helper } from "../../../shared/index";
import { zip } from "rxjs";
declare let $: any;

@Component({
  selector: 'app-vendor-folder-form',
  templateUrl: './vendor-folder-form.component.html'
})

export class VendorFolderFormComponent implements OnInit {
  id: number = 0;
  isNci: boolean = false;
  isReviewer: boolean = false;
  isSubmitted: boolean = false;
  form: FormGroup;
  option: any;
  countryList: Country[] = [];
  stateList: State[] = [];
  vendorList: Vendor[] = [];
  vendorFolder: VendorFolder;
  vndrContactList: VendorFolderContact[] = [];
  selectedContactKeys: number[] = [];
  docList: DocEntity[] = [];

  get vendorName() { return this.form.get("vendorName") }
  get country() { return this.form.get("country") }

  constructor(private avRoute: ActivatedRoute, private router: Router, private helper: Helper,
    private spinner: NgxSpinnerService, private usrSrvc: UserService,
    private countrySrvc: CountryService, private stateSrvc: StateService,
    private vndrSrvc: VendorService, private vndrFldrSrvc: VendorFolderService) { }

  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"] || 0;
    this.form = new FormGroup({
      vendorName: new FormControl('', Validators.required),
      stAddress1: new FormControl(''),
      stAddress2: new FormControl(''),
      bldg: new FormControl(''),
      flr: new FormControl(''),
      rm: new FormControl(''),
      city: new FormControl(''),
      state: new FormControl(''),
      province: new FormControl(''),
      zipCd: new FormControl(''),
      country: new FormControl('', Validators.required),
      pdl: new FormControl('')
    });
    this.option = {
      inputFileControl: {
        inputFileField: {
          label: "Upload Template(s) to Vendor Folder",
          isShown: true
        }
      },
      grid: {
        column: {
          fileName: {
            caption: "Template"
          },
          fileSize: {
            byte: true
          }
        }
      }
    }
    this.init();
  }

  init() {
    let data = zip(
      this.usrSrvc.userActiveWithProfileName(this.usrSrvc.loggedInUser.userId, "NCI"),
      this.countrySrvc.get(),
      this.stateSrvc.get(),
      this.vndrSrvc.get()
    )

    data.toPromise().then(
      res => {
        this.isNci = res[0] ? true : false;
        this.countryList = res[1];
        this.stateList = res[2];
        this.vendorList = res[3];

        if (!this.isNci) {
          this.router.navigate(['/sorry']);
          return;
        }
        else {
          // Added condition by Sarah Sandoval [20200701]
          // As per Anusha/Pramod - Order Reviewer should not be able to Create/Update
          let admin: number = this.usrSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Admin'));
          let reviewer: number = this.usrSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order Reviewer'));
          let updater = this.usrSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order Updater'));
          let rts = this.usrSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order RTS'));
          this.isReviewer = (admin == -1 && reviewer > -1 && updater == -1 && rts == -1);

          if (this.isReviewer) {
            this.form.disable();
          }
        }
      });

    if (this.id > 0) {
      this.spinner.show();
      this.vndrFldrSrvc.getById(this.id).subscribe(
        res => {
          this.vendorFolder = res;
          this.setFormData();
          this.spinner.hide();
        }, error => {
          this.spinner.hide();
          this.helper.notify('Failed in getting the Vendor Folder data.', Global.NOTIFY_TYPE_ERROR);
        });
    }
  }

  setFormData() {
    this.form.get('vendorName').setValue(this.vendorFolder.vndrCd);
    this.form.get('stAddress1').setValue(this.vendorFolder.streetAdr1);
    this.form.get('stAddress2').setValue(this.vendorFolder.streetAdr2);
    this.form.get('bldg').setValue(this.vendorFolder.bldgNme);
    this.form.get('flr').setValue(this.vendorFolder.flrId);
    this.form.get('rm').setValue(this.vendorFolder.rmNbr);
    this.form.get('city').setValue(this.vendorFolder.ctyNme);
    this.form.get('state').setValue(this.vendorFolder.sttCd);
    this.form.get('province').setValue(this.vendorFolder.prvnNme);
    this.form.get('zipCd').setValue(this.vendorFolder.zipPstlCd);
    this.form.get('country').setValue(this.vendorFolder.ctryCd);
    this.form.get('pdl').setValue(this.vendorFolder.vndrEmailListTxt);

    this.vndrContactList = this.vendorFolder.vndrFoldrCntct;
    if (this.vendorFolder.vndrTmplt != null && this.vendorFolder.vndrTmplt.length > 0) {
      this.docList = this.vendorFolder.vndrTmplt.map(item => {
        return new DocEntity({
          docId: item.vndrTmpltId,
          id: item.vndrFoldrId,
          fileNme: item.fileNme,
          fileCntnt: item.fileCntnt,
          fileSizeQty: item.fileSizeQty,
          creatDt: item.creatDt,
          creatByUserId: item.creatByUserId,
          creatByUserAdId: item.creatByUserAdId,
          base64string: item.base64string
        })
      })
    }

    this.vendorName.disable();
    this.country.disable();
  }

  onContactToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  onContactRowInserting(e: any) {
    // Added to prevent error on Contact Id
    e.data.cntctId = this.getVendorContactId();
  }

  getVendorContactId(): number {
    if (this.vndrContactList != null && this.vndrContactList.length > 0) {
      let maxId = this.vndrContactList.reduce((max, cntct) => cntct.cntctId > max ? cntct.cntctId : max, this.vndrContactList[0].cntctId);
      return maxId + 1;
    } else {
      return 1;
    }
  }

  contactSelectionChangedHandler(data: any) {
    this.selectedContactKeys = data.selectedRowKeys;
  }

  deleteContactRows() {
    //console.log(this.selectedContactKeys)
    this.vndrContactList = this.vndrContactList.filter(i => !this.selectedContactKeys.includes(i.cntctId));
  }

  cancel() {
    this.router.navigate(['vendor/vendor-folder']);
  }

  save() {
    if (this.helper.isEmpty(this.vendorName.value) || this.helper.isEmpty(this.country.value)) {
      this.helper.notify('Vendor and/or Country is required.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.spinner.show();
    this.isSubmitted = true;

    if (this.id == 0) {
      this.vendorFolder = new VendorFolder();
    }

    this.vendorFolder.vndrCd = this.vendorName.value;
    this.vendorFolder.vndrNme = this.vendorList.filter(i => i.vndrCd === this.vendorName.value)[0].vndrNme;
    this.vendorFolder.streetAdr1 = this.form.get("stAddress1").value;
    this.vendorFolder.streetAdr2 = this.form.get("stAddress2").value;
    this.vendorFolder.bldgNme = this.form.get("bldg").value;
    this.vendorFolder.flrId = this.form.get("flr").value;
    this.vendorFolder.rmNbr = this.form.get("rm").value;
    this.vendorFolder.ctyNme = this.form.get("city").value;
    this.vendorFolder.sttCd = this.form.get("state").value;
    this.vendorFolder.prvnNme = this.form.get("province").value;
    this.vendorFolder.zipPstlCd = this.form.get("zipCd").value;
    this.vendorFolder.ctryCd = this.country.value;
    this.vendorFolder.ctryNme = this.countryList.filter(i => i.ctryCd === this.country.value)[0].ctryNme;
    this.vendorFolder.vndrEmailListTxt = this.form.get("pdl").value;

    this.vendorFolder.vndrFoldrCntct = this.vndrContactList;
    this.vendorFolder.vndrTmplt = [];
    if (this.docList != null && this.docList.length > 0) {
      this.docList.forEach(i => {
        let tmplt = new VendorFolderTemplate();
        tmplt.vndrTmpltId = i.docId;
        tmplt.vndrFoldrId = i.id;
        tmplt.fileNme = i.fileNme;
        // Commented by Sarah Sandoval [20220113] - To prevent UI error for maximum entity when uploading > 20 docs
        //tmplt.fileCntnt = i.fileCntnt;
        tmplt.fileSizeQty = i.fileSizeQty;
        tmplt.creatByUserId = i.creatByUserId;
        tmplt.creatDt = i.creatDt;
        tmplt.base64string = i.base64string;
        this.vendorFolder.vndrTmplt.push(tmplt);
      });
    }

    //console.log(this.vendorFolder)
    if (this.id > 0) {
      // Update
      this.vndrFldrSrvc.update(this.id, this.vendorFolder).toPromise().then(
        res => {
          this.helper.notify('Successfully updated Vendor Folder.', Global.NOTIFY_TYPE_SUCCESS);
          this.router.navigate(['vendor/vendor-folder']);
        },
        error => {
          this.helper.notify(`An error occurred while updating Vendor Folder data. ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
        }
      ).then(() => { this.spinner.hide() });
    }
    else {
      // Create
      this.vndrFldrSrvc.create(this.vendorFolder).toPromise().then(
        res => {
          this.helper.notify('Successfully created Vendor Folder.', Global.NOTIFY_TYPE_SUCCESS);
          this.router.navigate(['vendor/vendor-folder']);
        },
        error => {
          this.helper.notify(`An error occurred while creating Vendor Folder data. ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
        }
      ).then(() => { this.spinner.hide(); });
    }
  }
}
