import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService, CountryService, VendorFolderService } from '../../../services';
import { Global } from '../../../shared/global';
import { Helper, SpinnerAsync } from "../../../shared";
import { VendorFolder, Country } from '../../../models/index';
import { zip } from 'rxjs';

@Component({
  selector: 'app-vendor-folder',
  templateUrl: './vendor-folder.component.html'
})

export class VendorFolderComponent implements OnInit {
  isNci: boolean = false;
  isReviewer: boolean = false;
  form: FormGroup;
  countryList: Country[] = [];
  vendorFolderList: VendorFolder[] = [];
  spnr: SpinnerAsync;

  constructor(private router: Router, private spinner: NgxSpinnerService,
    private helper: Helper, private userSrvc: UserService,
    private countrySrvc: CountryService, private vndrFldrSrvc: VendorFolderService) { 
      this.spnr = new SpinnerAsync(spinner);
    }

  ngOnInit() {
    this.form = new FormGroup({
      vendorName: new FormControl(''),
      country: new FormControl('')
    });

    this.init();
  }

  init() {
    this.spnr.manageSpinner('show');
    let data = zip(
      this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "NCI"),
      this.countrySrvc.get()
    )

    data.subscribe(res => {
      this.isNci = res[0] ? true : false;
      this.countryList = res[1];

      // Added by Sarah Sandoval [20200701]
      // As per Anusha/Pramod - Order Reviewer should not be able to Create/Update
      let admin: number = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Admin'));
      let reviewer: number = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order Reviewer'));
      let updater = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order Updater'));
      let rts = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order RTS'));
      this.isReviewer = (admin == -1 && reviewer > -1 && updater == -1 && rts == -1);

      if (!this.isNci) {
        this.router.navigate(['/sorry']);
      }
    }, error => {
        this.helper.notify('An error occurred while getting user access.', Global.NOTIFY_TYPE_ERROR);
      }
    ).add(() => {
      this.spnr.manageSpinner('hide');
    });
  }

  reset() {
    this.vendorFolderList = [];
  }

  search() {
    let vndrNme = this.form.get('vendorName').value;
    let ctryCd = this.form.get('country').value;
    if (this.helper.isEmpty(vndrNme) && this.helper.isEmpty(ctryCd)) {
      this.helper.notify('Please enter search criteria', Global.NOTIFY_TYPE_WARNING);
      return;
    }
    this.spinner.show();
    this.vndrFldrSrvc.getVendorFolder(vndrNme, ctryCd).subscribe(
        res => {
          this.vendorFolderList = res;
          this.spinner.hide();
        },
        error => {
          let msg = error != null ? ' ' + error.message : '';
          this.helper.notify('Search failed.' + msg, Global.NOTIFY_TYPE_ERROR);
          this.spinner.hide();
        }
      );
  }

  onSearchSelectionChanged(e) {
    // Route to Vendor Folder Form for Update
    this.router.navigate(['vendor/vendor-folder/' + e]);
  }
}
