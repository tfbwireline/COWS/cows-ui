// Core
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Libraries
import { AppMaterialModule } from '../app-material.module';
import { AppDxtremeModule } from '../app-dxtreme.module'

// Vendor Section
import { VendorRoutes } from './vendor.routes';

import {
  VendorFolderService, VendorTemplateService, CountryService, StateService,
  VendorOrderService, VendorEmailTypeService, VendorFormService, VendorOrderEmailAttachmentService, ProfileHierarchyService
} from '../../services/index';

import { VendorFolderComponent } from './folder/vendor-folder.component';
import { VendorFolderFormComponent } from './folder/vendor-folder-form.component';
import { VendorFolderTemplateComponent } from './generic-template/vendor-folder-template.component';
import { VendorOrderFormComponent } from './order/vendor-order-form.component';
import { SharedModule } from '../shared/shared-module';


@NgModule({
  declarations: [
    VendorFolderComponent,
    VendorFolderFormComponent,
    VendorFolderTemplateComponent,
    VendorOrderFormComponent
  ],
  imports: [
    CommonModule, 
    VendorRoutes,
    AppMaterialModule,
    AppDxtremeModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    VendorFolderService,
    VendorTemplateService,
    StateService,
    CountryService,
    VendorOrderService,
    VendorEmailTypeService,
    VendorFormService,
    VendorOrderEmailAttachmentService,
    ProfileHierarchyService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VendorModule { }
