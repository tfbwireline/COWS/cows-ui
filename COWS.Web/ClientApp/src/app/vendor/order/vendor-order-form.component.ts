import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CountryService, VendorFolderService, H5FolderService, VendorOrderService, VendorEmailTypeService, VendorOrderEmailService, VendorOrderMsService, VendorFormService, OrderService, UserService, VendorOrderEmailAttachmentService, VendorTemplateService, ProfileHierarchyService,OrderNoteService } from '../../../services';
import { Country, VendorFolder, VendorFolderContact, H5Folder, H5Doc, VendorFolderTemplate, VendorOrder, VendorEmailType, VendorOrderEmail, VendorOrderMs, VendorOrderForm, VendorOrderEmailAttachment, ProfileHierarchy } from '../../../models';
import { zip, of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { VendorOrderFormService } from '../../../services/vendor-order-form.service';
import { Helper } from '../../../shared';
import { MatExpansionPanel } from '@angular/material';
import { DxDataGridComponent } from 'devextreme-angular';
import { Global, EmailStus } from '../../../shared/global';
import { concatMap } from 'rxjs/operators';
import { FileSizePipe } from '../../../shared/pipe/file-size.pipe';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'vendor-order-form',
  templateUrl: './vendor-order-form.component.html'
})
export class VendorOrderFormComponent implements OnInit {
  @ViewChild('searchVendorFolderPanel', { static: false }) searchVendorFolderPanel: MatExpansionPanel;
  @ViewChild('searchVendorOrderPanel', { static: false }) searchVendorOrderPanel: MatExpansionPanel;
  @ViewChild('searchH5FolderPanel', { static: false }) searchH5FolderPanel: MatExpansionPanel;
  @ViewChild('vendorFolderTemplateGrid', { static: false }) vendorFolderTemplateGrid: DxDataGridComponent;

  get isCreate() {
    return this.activatedRoute.snapshot.data.title == "Create" ? true : false
  }
  get showAddOffnetButton() {
    return this.isDynamic
  }

  // DROPDOWN|CHECKBOX|RADIO
  countryList: Country[] = []
  isTerminatingList: any[] = ["Originating", "Terminating"]
  vendorEmailTypeList: VendorEmailType[] = []

  // GRID
  vendorFolderList: VendorFolder[] = []
  vendorOrderList: VendorOrder[] = []
  vendorFolderContactList: VendorFolderContact[] = []
  h5FolderList: H5Folder[] = []
  h5DocList: H5Doc[] = []
  vendorFolderTemplateList: VendorFolderTemplate[] = []
  vendorOrderFormList: VendorOrderForm[] = []
  vendorOrderEmailList: VendorOrderEmail[] = []
  vendorOrderEmailAttachmentList: VendorOrderEmailAttachment[] = []
  noteList = []
  orderResult: any;

  orderId: number = null
  vendor: string = null
  wg: number = 0
  orderData: any = null
  orderLink: any = null
  vendorOrderId: any = 0
  vendorOrderStr: string = '';
  vendorOrder: VendorOrder = null
  vendorFolder: VendorFolder = null
  h5Folder: H5Folder = null
  filename: string = ""
  attachmentFilename: string = ""
  vendorOrderForm: VendorOrderForm = null
  vendorOrderEmail: VendorOrderEmail = null
  vendorOrderEmailAttachment: VendorOrderEmailAttachment = null
  isDynamic: boolean = false

  isPerformingClone: boolean = false;

  isReviewer: boolean = false;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService, private helper: Helper,
    public vendorForm: VendorOrderFormService, private countryService: CountryService, private vendorFolderService: VendorFolderService, private h5FolderService: H5FolderService,
    private vendorOrderService: VendorOrderService, private vendorEmailTypeService: VendorEmailTypeService, private vendorOrderEmailService: VendorOrderEmailService,
    private vendorOrderMsService: VendorOrderMsService, private vendorFormService: VendorFormService, private orderService: OrderService, private userService: UserService,
    private fileSizePipe: FileSizePipe, private vendorOrderEmailAttachmentService: VendorOrderEmailAttachmentService, private vendorTemplateService: VendorTemplateService,
    private profileHierarchyService: ProfileHierarchyService, private orderNoteService: OrderNoteService,private sanitizer: DomSanitizer
  ) {
    this.onDownloadClicked = this.onDownloadClicked.bind(this);
    this.onEmailUpdating = this.onEmailUpdating.bind(this);
    this.isEditAllowed = this.isEditAllowed.bind(this);
    this.onViewEmailClicked = this.onViewEmailClicked.bind(this);
    this.onAttachmentDownloadClicked = this.onAttachmentDownloadClicked.bind(this);
    this.onTemplateDownloadClicked = this.onTemplateDownloadClicked.bind(this);
    this.vendorForm.clear();
    this.vendorForm.form.enable();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.vendorOrderId = this.activatedRoute.snapshot.params['id'] || 0
      this.orderId = this.activatedRoute.snapshot.queryParams['orderId'] || null
      this.vendor = this.activatedRoute.snapshot.queryParams['vendor'] || null
      this.wg = this.activatedRoute.snapshot.queryParams['wg'] || 0

      this.getInitialData()
    })
  }

  getInitialData() {
    if (this.vendorOrderId == 0) {
      let data = zip(
        this.countryService.get(),
        //this.orderNoteService.getByOrderId(this.orderId),
      )

      this.spinner.show()
      data.subscribe(
        res => {
          this.countryList = res[0] as Country[]
          this.countryList.unshift(new Country({ ctryNme: '- Please Select -', ctryCd: '' }))
          this.vendorForm.setValue("general.isTerminating", "Terminating")
         // this.noteList = res[1] as any[]
         
          //console.log(res)
        },
        error => {
          console.log(error)
          this.spinner.hide()
        },
        () => {
          // Added by Sarah Sandoval[20200701]
          // As per Anusha/Pramod - Order Reviewer should not be able to Create/Update
          let admin: number = this.userService.loggedInUser.profiles.findIndex(i => i.includes('NCI Admin'));
          let reviewer: number = this.userService.loggedInUser.profiles.findIndex(i => i.includes('NCI Order Reviewer'));
          let updater = this.userService.loggedInUser.profiles.findIndex(i => i.includes('NCI Order Updater'));
          let rts = this.userService.loggedInUser.profiles.findIndex(i => i.includes('NCI Order RTS'));
          this.isReviewer = (admin == -1 && reviewer > -1 && updater == -1 && rts == -1);

          // KM967761 - 08/12/2020 - Pre-populate vendor name from querystring "vendor"
          this.vendorForm.form.get("vendorFolder.vendorName").setValue(this.vendor)

          this.vendorForm.form.enable();
          this.spinner.hide()
        })
    }
    else {
      this.spinner.show()
      // Format vendor order string
      const rawRendorOrderStr = this.vendorOrderId.toString();
      const charRepeat = 10 - rawRendorOrderStr.length;
      this.vendorOrderStr = `VON${'0'.repeat(charRepeat)}${rawRendorOrderStr}`;

      let data = zip(
        this.vendorOrderService.get2(this.vendorOrderId),
        this.countryService.get(),
        this.vendorEmailTypeService.get(),
        this.orderNoteService.getByOrderId2(this.vendorOrderId),
      )
      data.subscribe(res => {
        console.log(res)
        this.vendorOrder = res[0].vendorOrder
        this.h5Folder = res[0].vendorOrder.h5Foldr
        this.vendorFolder = res[0].vendorOrder.vndrFoldr
        this.vendorFolderContactList = res[0].vendorOrder.vndrFoldr.vndrFoldrCntct as VendorFolderContact[]
        this.vendorFolderTemplateList = res[0].vendorOrder.vndrFoldr.vndrTmplt as VendorFolderTemplate[]
        this.vendorOrderFormList = (res[0].vendorOrder.vndrOrdrForm as VendorOrderForm[]).map(data => {
          data.fileSizeQtyStr = this.fileSizePipe.transform(data.fileSizeQty);
          return data;
        })
        this.noteList = (res[3]).map(
            a => {
              a.nteTxt = this.sanitizer.bypassSecurityTrustHtml(a.nteTxt)
              return a
            }
          )
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res[0].vendorOrder.vndrOrdrEmail as VendorOrderEmail[]);
        this.countryList = res[1] as Country[]
        this.countryList.unshift(new Country({ ctryNme: '- Please Select -', ctryCd: '' }))
        this.vendorEmailTypeList = res[2] as VendorEmailType[]

        this.profileHierarchyService.profileHierarchy = res[0].profileHierarchy as ProfileHierarchy[]
        this.isDynamic = res[0].isBasic

        this.vendorFolder.vndrNme = this.vendorOrder.vndrNme
        this.vendorForm.setValue("general.vendorName", `${this.vendorOrder.vndrNme} [${this.vendorFolder.vndrCd}]`)
        this.vendorForm.setValue("general.emailPdl", this.vendorFolder.vndrEmailListTxt)
        this.vendorForm.setValue("general.streetAddress1", this.vendorFolder.streetAdr1)
        this.vendorForm.setValue("general.streetAddress2", this.vendorFolder.streetAdr2)
        this.vendorForm.setValue("general.building", this.vendorFolder.bldgNme)
        this.vendorForm.setValue("general.floor", this.vendorFolder.flrId)
        this.vendorForm.setValue("general.room", this.vendorFolder.rmNbr)
        this.vendorForm.setValue("general.city", this.vendorFolder.ctyNme)
        this.vendorForm.setValue("general.state", this.vendorFolder.sttNme)
        this.vendorForm.setValue("general.country", this.vendorFolder.ctryNme)
        this.vendorForm.setValue("general.zip", this.vendorFolder.zipPstlCd)
        this.vendorForm.setValue("general.isTerminating", this.vendorOrder.trmtgCd ? "Terminating" : "Originating")

        this.searchVendorOrderPanel.close()

        if (this.h5Folder) {
          this.vendorForm.setValue("h5Folder.customerId", this.h5Folder.custId)
          this.vendorForm.setValue("h5Folder.customerName", this.h5Folder.custNme)
          this.vendorForm.setValue("h5Folder.city", this.h5Folder.custCtyNme)
          this.vendorForm.setValue("h5Folder.country", this.h5Folder.ctryCd)
          this.h5DocList = this.h5Folder.h5Docs as H5Doc[]
        }

        if (this.vendorOrder.ordrId) {
          this.getOrderUrl(res[0].workgroup as any[])
        }

        this.spinner.hide()
      }, error => {
        console.log(error)
        this.spinner.hide()
      })
    }
  }

  isDeleteAllowed(e) {
    return e.row.data.emailStatusDesc.includes("Draft")
  }

  isEditAllowed(e) {
    return !e.row.isEditing && e.row.data.emailStatusDesc.includes("Sent") && e.row.data.ackByVendorDate == null
  }
  // isEditAllowed(e) {
  //   return e.row.data.emailStatusDesc.includes("Sent") && e.row.data.ackByVendorDate == null
  // }

  showSaveColumn() {
    return this.vendorOrderEmailList.find(x => x.ableToAddAckDate === true);
  }

  onEditingStart(e) {
    if(!e.data.ableToAddAckDate) {
      e.cancel = true;   
    }
  }

  setEmailForm(obj: VendorOrderEmail) {
    this.vendorOrderEmail = obj
    this.vendorForm.setValue("emailUpdate.emailType", this.vendorOrderEmail.vndrEmailTypeId)
    this.vendorForm.setValue("emailUpdate.to", this.vendorOrderEmail.toEmailAdr)
    this.vendorForm.setValue("emailUpdate.cc", this.vendorOrderEmail.ccEmailAdr)
    this.vendorForm.setValue("emailUpdate.subject", this.vendorOrderEmail.emailSubjTxt)
    this.vendorForm.setValue("emailUpdate.body", this.vendorOrderEmail.emailBody)
    this.vendorOrderEmailAttachmentList = (this.vendorOrderEmail.vndrOrdrEmailAtchmt as VendorOrderEmailAttachment[]).map(data => {
      data.fileSizeQtyStr = this.fileSizePipe.transform(data.fileSizeQty);
      return data;
    })
    //console.log(obj)
  }

  // EVENT HANDLING
  searchVendorFolder(e) {
    let vendorName = this.helper.ifEmpty(this.vendorForm.getValue("vendorFolder.vendorName"), '')
    let country = this.helper.ifEmpty(this.vendorForm.getValue("vendorFolder.country"), '')

    this.spinner.show()
    this.vendorFolderService.getVendorFolder(vendorName, country).subscribe(res => {
      //console.log(res)
      this.vendorFolderList = res as VendorFolder[]
    }, error => {
      console.log(error)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  searchVendorOrder(e) {
    let vendorOrderNo: string = this.helper.ifEmpty(this.vendorForm.getValue("vendorOrder.vendorOrderNo"), '')
    let customerId = this.helper.ifEmpty(this.vendorForm.getValue("vendorOrder.customerId"), 0)
    let ctn: string = this.helper.ifEmpty(this.vendorForm.getValue("vendorOrder.ctn"), '')
    let isTerminating = this.vendorForm.getValue("general.isTerminating") == "Terminating" ? true : false

    this.spinner.show()
    this.vendorOrderService.getVendorOrderTerminating(vendorOrderNo, customerId, ctn, isTerminating).subscribe(res => {
      //console.log(res)
      this.vendorOrderList = res //as VendorOrder[]
    }, error => {
      console.log(error)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  onVendorFolderRowClick(e) {
    let data = e.data

    this.spinner.show()
    let sub = zip(
      this.vendorFolderService.getById(data.vndrFoldrId),
      this.vendorFolderService.getContactsByFolderId(data.vndrFoldrId),
      this.vendorFolderService.getTemplatesByFolderId(data.vndrFoldrId),
    )

    sub.subscribe(res => {
      //console.log(res)
      this.vendorFolder = res[0] as VendorFolder
      this.vendorFolderContactList = res[1] as VendorFolderContact[]
      this.vendorFolderTemplateList = res[2] as VendorFolderTemplate[]

      this.vendorFolder.vndrNme = data.vndrNme
      this.vendorForm.setValue("general.vendorName", `${data.vndrNme} [${data.vndrCd}]`)
      this.vendorForm.setValue("general.emailPdl", this.vendorFolder.vndrEmailListTxt)
      this.vendorForm.setValue("general.streetAddress1", this.vendorFolder.streetAdr1)
      this.vendorForm.setValue("general.streetAddress2", this.vendorFolder.streetAdr2)
      this.vendorForm.setValue("general.building", this.vendorFolder.bldgNme)
      this.vendorForm.setValue("general.floor", this.vendorFolder.flrId)
      this.vendorForm.setValue("general.room", this.vendorFolder.rmNbr)
      this.vendorForm.setValue("general.city", this.vendorFolder.ctyNme)
      this.vendorForm.setValue("general.state", this.vendorFolder.sttNme)
      this.vendorForm.setValue("general.country", this.vendorFolder.ctryNme)
      this.vendorForm.setValue("general.zip", this.vendorFolder.zipPstlCd)

      this.searchVendorFolderPanel.close()

      console.log(`WG`, this.wg)
      // Create if from order page
      if (this.orderId) {
        let sub2 = zip(
          this.h5FolderService.getByOrderId(this.orderId),
          this.orderService.getWgData(this.orderId, 0),
          this.profileHierarchyService.get()
        )

        sub2.subscribe(res => {
          this.h5Folder = res[0] as H5Folder

          this.vendorForm.setValue("h5Folder.customerId", this.h5Folder.custId)
          this.vendorForm.setValue("h5Folder.customerName", this.h5Folder.custNme)
          this.vendorForm.setValue("h5Folder.city", this.h5Folder.custCtyNme)
          this.vendorForm.setValue("h5Folder.country", this.h5Folder.ctryCd)
          this.h5DocList = this.h5Folder.h5Docs as H5Doc[]
          let wgData = (res[1] as any[])
          this.profileHierarchyService.profileHierarchy = res[2] as ProfileHierarchy[]

          console.log(`wgData`, wgData)
          this.getOrderUrl(wgData as any[])
          this.spinner.hide()
        }, error => {
          console.log(error)
          this.spinner.hide()
        })
        //let vendorOrder = new VendorOrder({
        //  vndrOrdrId: 0,
        //  vndrFoldrId: this.vendorFolder.vndrFoldrId,
        //  trmtgCd: this.vendorForm.getValue("general.isTerminating") == "Terminating" ? true : false,
        //  ordrId: this.orderId
        //})

        //this.createVendorOrder({ vendorOrder: vendorOrder, callback: this.spinner.hide() })
      } else {
        this.spinner.hide()
      }
    }, error => {
      console.log(error)
      this.spinner.hide()
    })
  }

  onVendorOrderRowClick(e) {
    //console.log(e)
    let data = e.data

    this.router.navigate([`vendor/vendor-order/${data.vndrOrdrId}`]);
  }

  onTemplateDownloadClicked(e) {
    e.event.preventDefault()
    let vendorFolderTemplate = e.row.data as VendorFolderTemplate

    this.spinner.show()
    this.vendorTemplateService.download(e.row.key).subscribe(res => {
      saveAs(res, vendorFolderTemplate.fileNme)
    }, error => {
      console.log(error)
      this.helper.notify("Vendor Order Form could not be dowloaded. Please contact your system administrator", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  searchH5Folder(e) {
    let customerId = this.vendorForm.getValue("h5FolderSearch.customerId") || 0
    let customerName = this.vendorForm.getValue("h5FolderSearch.customerName") || ''
    let city = this.vendorForm.getValue("h5FolderSearch.city") || ''
    let country = this.vendorForm.getValue("h5FolderSearch.country") || ''

    if (customerId == 0 && customerName == '' && city == '' && country == '') {
      this.helper.notify("Please enter search criteria first", Global.NOTIFY_TYPE_INFO)
    } else {
      this.spinner.show()
      this.h5FolderService.search(customerId, customerName, '', city, country).subscribe(res => {
        //console.log(res)
        this.h5FolderList = res as H5Folder[]
      }, error => {
        console.log(error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide()
      })
    }
  }

  onH5FolderRowClick(e) {
    //console.log(e)
    this.h5Folder = e.data as H5Folder

    this.vendorForm.setValue("h5Folder.customerId", this.h5Folder.custId)
    this.vendorForm.setValue("h5Folder.customerName", this.h5Folder.custNme)
    this.vendorForm.setValue("h5Folder.city", this.h5Folder.custCtyNme)
    this.vendorForm.setValue("h5Folder.country", this.h5Folder.ctryCd)
    this.h5DocList = this.h5Folder.h5Docs as H5Doc[]

    this.searchH5FolderPanel.close()
  }

  onFileLoad(result) {
    let file = result.file as File
    //let byteArray = result.byteArray as Uint8Array
    if (file.name.length > 100) {
      this.helper.notify('File name only allows 100 characters only', Global.NOTIFY_TYPE_WARNING);
      this.filename = null
      this.vendorOrderForm = null
      return;
    }

    let base64 = result.base64

    this.filename = file.name

    this.vendorOrderForm = new VendorOrderForm({
      base64string: base64,
      fileNme: file.name,
      fileSizeQty: file.size
    })
  }

  uploadVendorForm(e) {
    if (this.vendorOrderForm != null) {
      this.spinner.show()
      this.vendorOrderForm.vndrOrdrId = this.vendorOrderId
      this.vendorFormService.create(this.vendorOrderForm).subscribe(res => {
        this.vendorOrderFormList = (res as VendorOrderForm[]).map(data => {
          data.fileSizeQtyStr = this.fileSizePipe.transform(data.fileSizeQty);
          return data;
        })
        this.helper.notify("Vendor Form has been uploaded", Global.NOTIFY_TYPE_SUCCESS)
      }, error => {
        this.spinner.hide()
        this.helper.notify("Vendor Form could not be uploaded", Global.NOTIFY_TYPE_ERROR)
      }, () => {
        this.spinner.hide()
      });
    } else {
      this.helper.notify("Please select a file", Global.NOTIFY_TYPE_WARNING)
    }
  }

  onDownloadClicked(e) {
    e.event.preventDefault()
    let vendorOrderForm = e.row.data as VendorOrderForm

    this.spinner.show()
    this.vendorFormService.download(e.row.key).subscribe(res => {
      saveAs(res, vendorOrderForm.fileNme)
    }, error => {
      console.log(error)
      this.helper.notify("Vendor Order Form could not be dowloaded. Please contact your system administrator", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  onVendorFormRemoving(e) {
    this.spinner.show()
    this.vendorFormService.delete(e.key).subscribe(res => {
      this.helper.notify("Vendor Order Form deleted", Global.NOTIFY_TYPE_SUCCESS)
    }, error => {
      console.log(error)
      e.cancel = true
      this.helper.notify("Vendor Order Form deletion failed. Please contact your system administrator", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  updateVendorAckDate(data) {

    let vendorOrderMs = new VendorOrderMs({
      vndrOrdrId: data.vndrOrdrId,
      verId: 2,
      vndrOrdrEmailId: data.vndrOrdrEmailId,
      sentToVndrDt: data.sentToVendorDate,
      ackByVndrDt: data.ackByVendorDate
    })

    if(vendorOrderMs.ackByVndrDt != undefined && vendorOrderMs.ackByVndrDt != null) {
      this.spinner.show()
      this.vendorOrderEmailService.updateEmailAckDate(data.vndrOrdrEmailId, vendorOrderMs).subscribe(res => {
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res as VendorOrderEmail[]);
        this.helper.notifySavedFormMessage("Vendor Order Email Acknowledge Date", Global.NOTIFY_TYPE_SUCCESS, false, null)
      }, error => {
        console.log(error)
        this.helper.notifySavedFormMessage("Vendor Order Email Acknowledge Date", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide()
      })
    }
  }

  onEmailUpdating(e) {
    //e.cancel = true
    //console.log(e)
    let data = e.oldData as VendorOrderEmail


    let vendorOrderMs = new VendorOrderMs({
      vndrOrdrId: data.vndrOrdrId,
      verId: 2,
      vndrOrdrEmailId: data.vndrOrdrEmailId,
      sentToVndrDt: data.sentToVendorDate,
      ackByVndrDt: e.newData.ackByVendorDate
    })

    if(vendorOrderMs.ackByVndrDt != undefined && vendorOrderMs.ackByVndrDt != null) {
      this.spinner.show()
      this.vendorOrderEmailService.updateEmailAckDate(data.vndrOrdrEmailId, vendorOrderMs).subscribe(res => {
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res as VendorOrderEmail[]);
        this.helper.notifySavedFormMessage("Vendor Order Email Acknowledge Date", Global.NOTIFY_TYPE_SUCCESS, false, null)
        console.log(`WG`, this.wg)
        this.orderService.getWgData(this.vendorOrder.ordrId, 0).subscribe(res => {
          console.log(`wgData`, res)
          this.getOrderUrl(res as any[])
        }, error => {
          this.helper.notify("FTN link Could not be found", Global.NOTIFY_TYPE_ERROR)
          this.spinner.hide()
        }, () => {
          this.spinner.hide()
        })
      }, error => {
        console.log(error)
        this.helper.notifySavedFormMessage("Vendor Order Email Acknowledge Date", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        //this.spinner.hide()
        e.cancel = false
      })
    } else {
      data.vndrOrdrMs.push(vendorOrderMs)
      this.spinner.show()
      this.vendorOrderEmailService.update(data.vndrOrdrEmailId, data).subscribe(res => {
        
        //console.log(res)
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res as VendorOrderEmail[]);
        this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_SUCCESS, false, null)
      }, error => {
        console.log(error)
        this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide()
        e.cancel = false
      })
    }

  }
  onNoteRemoving(e){
  this.spinner.show()
    this.orderNoteService.delete(e.key).subscribe(res => {
      this.helper.notify("Vendor Order Note deleted", Global.NOTIFY_TYPE_SUCCESS)
    }, error => {
      console.log(error)
      e.cancel = true
      this.helper.notify("Vendor Order Note deletion failed", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  onEmailRemoving(e) {
    this.spinner.show()
    this.vendorOrderEmailService.delete(e.key).subscribe(res => {
      this.helper.notify("Vendor Order Email deleted", Global.NOTIFY_TYPE_SUCCESS)
    }, error => {
      console.log(error)
      e.cancel = true
      this.helper.notify("Vendor Order Email deletion failed. Please contact your system administrator", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  onViewEmailClicked(e) {
    e.event.preventDefault();

    const data = e.row.data;
    //if(data.emailStusId === EmailStus.SentUnenc || data.emailStusId === EmailStus.SentEnc) {
    //  this.isPerformingClone = true;
    //} else {
    //  this.isPerformingClone = false;
    //}
  
    this.setEmailForm(data as VendorOrderEmail)
  }

  get commentCharRemaining(): number {
    const comment = this.vendorForm.getValue('general.comments');
    return (comment) ? 250 - comment.length : 250;
  }

  submit() {
    let isTerminating = this.vendorForm.getValue("general.isTerminating") == "Terminating" ? true : false
    let comments = this.vendorForm.getValue("general.comments")
    let h5Folder = this.h5Folder

    //let vendorFolderTemplate = this.vendorFolderTemplateGrid.instance.getSelectedRowsData()
    if (h5Folder == null) { 
      this.helper.notify("Please select H6 Folder for Vendor Order", Global.NOTIFY_TYPE_WARNING)
    } else {
      //Proceed
      h5Folder.h5Docs = [] // To drop H5 Docs before passing it to back end. Reason is, if it's too big, it will throw error and besides, we really don't need it since it is already tied to H5 Folder
      if (this.isCreate) {
        let vendorOrder = new VendorOrder({
          vndrOrdrId: 0,
          ordrId: this.orderId > 0 ? this.orderId : null,
          vndrFoldrId: this.vendorFolder.vndrFoldrId,//
          trmtgCd: isTerminating,//
          h5Foldr: h5Folder
        })

        this.createVendorOrder({ vendorOrder: vendorOrder })
      } else {
        let vendorOrder = new VendorOrder({
          ordrId: this.vendorOrder.ordrId,
          vndrOrdrId: this.vendorOrderId,
          vndrFoldrId: this.vendorFolder.vndrFoldrId,
          trmtgCd: isTerminating,
          h5Foldr: h5Folder
        })
        
        let orderNotes = {
        ordrId: this.vendorOrderId,
        nteTypeId: 11,
        nteTxt: this.vendorForm.getValue("general.comments"),     
        }
         if(orderNotes.nteTxt!= null && orderNotes.nteTxt!= ""){
         let data = this.orderNoteService.create(orderNotes).pipe(
        concatMap(res => this.orderNoteService.getByOrderId(this.vendorOrderId))
      )

      this.spinner.show()
      data.subscribe(res => {
        this.noteList = res
      })
      }
        this.spinner.show()
        this.vendorOrderService.update(this.vendorOrderId, vendorOrder).subscribe(res => {
          //console.log(`/vendor/order-edit/${res.vndrOrdrId}`)
          this.helper.notifySavedFormMessage("Vendor Order", Global.NOTIFY_TYPE_SUCCESS, false, null)
          this.getInitialData()
        }, error => {
          console.log(error)
          this.helper.notifySavedFormMessage("Vendor Order", Global.NOTIFY_TYPE_ERROR, false, error)
          this.spinner.hide()
        }, () => {
          //this.spinner.hide()
        })
      }
    }
   this.reset();
  }
reset() {
   this.vendorForm.setValue("general.comments", null);
  }
//getRowNumber(grid, data) {
//    let missedRowsNumber = grid.instance.getController('data').virtualItemsCount().begin;
//    return data.row.rowIndex + missedRowsNumber;
//  }
  createVendorOrder({ vendorOrder = null, callback = null }) {
    this.spinner.show()
    this.vendorOrderService.create(vendorOrder).subscribe(res => {
      console.log(res)
      if (!res.isExisting) {
        this.helper.notifySavedFormMessage("Vendor Order", Global.NOTIFY_TYPE_SUCCESS, true, null)
      }
      this.router.navigate([`/vendor/vendor-order/${res.vendorOrderId}`], { queryParams: { wg: this.wg }});
    }, error => {
      console.log(error)
        this.helper.notifySavedFormMessage("Vendor Order", Global.NOTIFY_TYPE_ERROR, true, error)
        this.spinner.hide()
      //if (callback != null) {
      //  console.log(typeof callback)
      //  callback()
      //}
    }, () => {
      //if (callback != null) {
      //  callback()
      //}
    })
  }

  createEmail() {
    let emailType = this.vendorForm.getValue("email.emailType")

    if (emailType == null) {
      this.helper.notify("Please select Email Type", Global.NOTIFY_TYPE_WARNING)
    } else {
      let verNo = this.vendorOrderEmailList.length > 0 ? Math.max(...(this.vendorOrderEmailList.map(a => a.verNbr))) : 0
      let vendorOrderEmail = new VendorOrderEmail({
        vndrOrdrId: this.vendorOrderId,
        verNbr: ++verNo,
        vndrEmailTypeId: emailType
      })

      this.spinner.show();
      let languageID = 0;
      //this.vendorOrderEmailService.create(vendorOrderEmail).subscribe(res => {
      this.vendorOrderEmailService.addVendorOrderEmail(this.vendorOrderId, emailType, languageID).subscribe(res => {
        this.setEmailForm(res as VendorOrderEmail);
        // this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_SUCCESS, true, null);
        //let [vendorOrderEmail] = this.vendorOrderEmailList.slice(-1);        
        //this.vendorFolderService.getVendorOrderEmails(this.vendorOrderId).subscribe(res => {
        //  this.vendorOrderEmailList = res.voe as VendorOrderEmail[];
        //  this.vendorOrderEmailAttachmentList = res.voea as VendorOrderEmailAttachment[];
        //});
        this.vendorOrderEmailService.getVendorOrderEmails(this.vendorOrderId).subscribe(res => {
          this.vendorOrderEmailList = this.processVendorOrderEmailList(res as VendorOrderEmail[]);
          //this.vendorOrderEmailAttachmentList = res. as VendorOrderEmailAttachment[];
        }, error => {
            console.log(error)
            this.helper.notify("Vendor Order Emails Could Not Be Loaded", Global.NOTIFY_TYPE_ERROR)
          this.spinner.hide()
        }, () => {
          this.spinner.hide()
        })
      }, error => {
        console.log(error)
        this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_ERROR, true, error)
        this.spinner.hide()
      }, () => {
        //this.spinner.hide()
      })
    }
  }

  sendEmail() {
    this.vendorOrderEmail.vndrEmailTypeId = this.vendorForm.getValue("emailUpdate.emailType")
    this.vendorOrderEmail.toEmailAdr = this.vendorForm.getValue("emailUpdate.to")
    this.vendorOrderEmail.ccEmailAdr = this.vendorForm.getValue("emailUpdate.cc")
    this.vendorOrderEmail.emailSubjTxt = this.vendorForm.getValue("emailUpdate.subject")
    this.vendorOrderEmail.emailBody = this.vendorForm.getValue("emailUpdate.body")

    this.spinner.show()
    this.vendorOrderEmailService.sendEmail(this.vendorOrderEmail.vndrOrdrEmailId, this.vendorOrderEmail).subscribe(res => {
      console.log(res)
      this.setEmailForm(res as VendorOrderEmail)
      this.vendorOrderEmailService.getVendorOrderEmails(this.vendorOrderId).subscribe(res => {
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res as VendorOrderEmail[]);
        this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_SUCCESS, false, null)
      }, error => {
        console.log(error)
        //this.helper.notify("Vendor Order Email", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide()
      })
    }, error => {
      console.log(error)
      this.helper.notify("Cannot send Vendor Order Email", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      //this.spinner.hide()
    })
  }

  draftEmail() {
    this.vendorOrderEmail.vndrEmailTypeId = this.vendorForm.getValue("emailUpdate.emailType")
    this.vendorOrderEmail.toEmailAdr = this.vendorForm.getValue("emailUpdate.to")
    this.vendorOrderEmail.ccEmailAdr = this.vendorForm.getValue("emailUpdate.cc")
    this.vendorOrderEmail.emailSubjTxt = this.vendorForm.getValue("emailUpdate.subject")
    this.vendorOrderEmail.emailBody = this.vendorForm.getValue("emailUpdate.body")

    this.spinner.show()
    this.vendorOrderEmailService.update(this.vendorOrderEmail.vndrOrdrEmailId, this.vendorOrderEmail).subscribe(res => {
      console.log(res)
      this.setEmailForm(res as VendorOrderEmail)
      this.vendorOrderEmailService.getVendorOrderEmails(this.vendorOrderId).subscribe(res => {
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res as VendorOrderEmail[]);
        this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_SUCCESS, false, null)
      }, error => {
        console.log(error)
        //this.helper.notify("Vendor Order Email", Global.NOTIFY_TYPE_ERROR, false, error)
        this.spinner.hide()
      }, () => {
        this.spinner.hide()
      })
    }, error => {
      console.log(error)
      this.helper.notifySavedFormMessage("Vendor Order Email", Global.NOTIFY_TYPE_ERROR, false, error)
      this.spinner.hide()
    }, () => {
      //this.spinner.hide()
    })
  }

  onAttachmentFileLoad(result) {
    let file = result.file as File
    //let byteArray = result.byteArray as Uint8Array
    let base64 = result.base64

    this.attachmentFilename = file.name

    this.vendorOrderEmailAttachment = new VendorOrderEmailAttachment({
      base64string: base64,
      fileNme: file.name,
      fileSizeQty: file.size
    })
  }

  uploadAttachment(e) {
    //console.log(e)
    if (this.vendorOrderEmailAttachment != null) {
      this.spinner.show()
      this.vendorOrderEmailAttachment.vndrOrdrEmailId = this.vendorOrderEmail.vndrOrdrEmailId
      
      // Validate file size
      if (this.vendorOrderEmailAttachment.fileSizeQty) {
        if (this.vendorOrderEmailAttachment.fileSizeQty > 1024 * 1024) {
          if ((Math.round(this.vendorOrderEmailAttachment.fileSizeQty * 100 / (1024 * 1024)) / 100) > 10) {
            this.spinner.hide()
            this.helper.notify("Email Attachment could not be uploaded. File must be under 10 MB in size.", Global.NOTIFY_TYPE_ERROR);
            return;
          }
        }
      }
      
      // Validate file name length
      if (this.vendorOrderEmailAttachment.fileNme) {
        if (this.vendorOrderEmailAttachment.fileNme.length > 100) {
          this.spinner.hide()
          this.helper.notify("Email Attachment could not be uploaded. File names must be under 100 characters in length.", Global.NOTIFY_TYPE_ERROR);
          return;
        }
      }
            
      this.vendorOrderEmailAttachmentService.create(this.vendorOrderEmailAttachment).pipe(
        concatMap(
          res => {
            return zip(
              of(res),
              this.vendorOrderService.getEmailsByVendorOrderId(this.vendorOrderId)
            )
          }
        )
      ).subscribe(res => {

        //let vndrOrdrEmailId = 0;

        //this.vendorOrderEmailAttachmentList = (res[0] as VendorOrderEmailAttachment[]).map(data => {
        //  // Since the record is fetching the attachments of the newly cloned email details set the selected vendorEmailId here
        //  if(this.isPerformingClone) {
        //    vndrOrdrEmailId = data.vndrOrdrEmailId;
        //    // this.vendorOrderEmail.vndrOrdrEmailId = data.vndrOrdrEmailId;
        //    // this.vendorOrderEmailAttachment.vndrOrdrEmailId = data.vndrOrdrEmailId;
        //  }         

        //  data.fileSizeQtyStr = this.fileSizePipe.transform(data.fileSizeQty);
        //  return data;
        //})
        //this.vendorOrderEmailList = this.processVendorOrderEmailList(res[1] as VendorOrderEmail[]);

        //// Update vndrOrdrEmailAtchmt
        //this.vendorOrderEmail.vndrOrdrEmailAtchmt = this.vendorOrderEmailAttachmentList;
        //// Set the newly created clone of the email.
        //if(this.isPerformingClone) {
        //  const data = res[1].filter(x => x.vndrOrdrEmailId === vndrOrdrEmailId)
        //  this.setEmailForm(data[0] as VendorOrderEmail)
        //}

        console.log(res)
        this.vendorOrderEmailList = this.processVendorOrderEmailList(res[1] as VendorOrderEmail[]);

        // Update vndrOrdrEmailAtchmt
        this.vendorOrderEmail = this.vendorOrderEmailList.find(a => a.vndrOrdrEmailId == this.vendorOrderEmail.vndrOrdrEmailId)
        console.log(this.vendorOrderEmail)

        this.vendorOrderEmailAttachmentList = (this.vendorOrderEmail.vndrOrdrEmailAtchmt as VendorOrderEmailAttachment[]).map(data => {
          data.fileSizeQtyStr = this.fileSizePipe.transform(data.fileSizeQty);
          return data;
        })

        this.attachmentFilename = ""
        this.helper.notify("Email Attachment has been uploaded", Global.NOTIFY_TYPE_SUCCESS)
        this.spinner.hide()
      }, error => {
          this.spinner.hide()
          let msg = error.message || "Email Attachment could not be uploaded"
          this.helper.notify(msg, Global.NOTIFY_TYPE_ERROR)
      }, () => {
        //this.spinner.hide()
      });
    } else {
      this.helper.notify("Please select a file", Global.NOTIFY_TYPE_WARNING)
    }
  }

  onAttachmentDownloadClicked(e) {
    e.event.preventDefault();
    let vendorOrderEmailAttachment = e.row.data as VendorOrderEmailAttachment

    this.spinner.show()
    this.vendorOrderEmailAttachmentService.download(e.row.key).subscribe(res => {
      saveAs(res, vendorOrderEmailAttachment.fileNme)
    }, error => {
      console.log(error)
      this.helper.notify("Email Attachment could not be dowloaded. Please contact your system administrator", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      this.spinner.hide()
    })
  }

  onAttachmentRemoving(e) {
    this.spinner.show()
    this.vendorOrderEmailAttachmentService.delete(e.key, this.vendorOrderEmail).pipe(
      concatMap(
        res => {
          return zip(
            of(res),
            this.vendorOrderService.getEmailsByVendorOrderId(this.vendorOrderId)
          )
        }
      )
    ).subscribe(res => {
      // delete returns the vendorOrderId
      //if(this.isPerformingClone && res[0] > 0) {
      //  this.vendorOrderEmail = this.vendorOrderEmailList.find(x => x.vndrOrdrEmailId == this.vendorOrderEmail.vndrOrdrEmailId);
      //  this.vendorOrderEmail.vndrOrdrEmailId = res[0];
      //  this.vendorOrderEmailAttachment.vndrOrdrEmailId = res[0];
      //  this.vendorOrderEmail.vndrOrdrEmailAtchmt = res[1]; 
      //}   

      this.vendorOrderEmailList = this.processVendorOrderEmailList(res[1] as VendorOrderEmail[]);

      // Update vndrOrdrEmailAtchmt
      this.vendorOrderEmail = this.vendorOrderEmailList.find(a => a.vndrOrdrEmailId == this.vendorOrderEmail.vndrOrdrEmailId)

      this.vendorOrderEmailAttachmentList = (this.vendorOrderEmail.vndrOrdrEmailAtchmt as VendorOrderEmailAttachment[]).map(data => {
        data.fileSizeQtyStr = this.fileSizePipe.transform(data.fileSizeQty);
        return data;
      })
  
      this.helper.notify("Email Attachment deleted", Global.NOTIFY_TYPE_SUCCESS)
      this.spinner.hide()
    }, error => {
      console.log(error)
      e.cancel = true
      this.helper.notify("Email Attachment deletion failed. Please contact your system administrator", Global.NOTIFY_TYPE_ERROR)
      this.spinner.hide()
    }, () => {
      //this.spinner.hide()
    });
  }

  addOffnetForm(e) {
    this.spinner.show()
    this.vendorOrderEmailAttachmentService.uploadOffnetForm(this.vendorOrderEmail.vndrOrdrEmailId).pipe(
      concatMap(
        res => {
          return zip(
            of(res),
            this.vendorOrderService.getEmailsByVendorOrderId(this.vendorOrderId)
          )
        }
      )
    ).subscribe(res => {
      this.vendorOrderEmailList = this.processVendorOrderEmailList(res[1] as VendorOrderEmail[]);

      // Update vndrOrdrEmailAtchmt
      this.vendorOrderEmail = this.vendorOrderEmailList.find(a => a.vndrOrdrEmailId == this.vendorOrderEmail.vndrOrdrEmailId)
      console.log(this.vendorOrderEmail)

      this.vendorOrderEmailAttachmentList = (this.vendorOrderEmail.vndrOrdrEmailAtchmt as VendorOrderEmailAttachment[]).map(data => {
        data.fileSizeQtyStr = this.fileSizePipe.transform(data.fileSizeQty);
        return data;
      })

      this.attachmentFilename = ""
      this.helper.notify("Email Attachment has been uploaded", Global.NOTIFY_TYPE_SUCCESS)
      this.spinner.hide()
    }, error => {
      this.spinner.hide()
      this.helper.notify("Email Attachment could not be uploaded", Global.NOTIFY_TYPE_ERROR)
    }, () => {
      //this.spinner.hide()
    });
  }
  getVendorNote(id){
   }
  getOrderUrl(wgData) {
    //if (orderDetails.orderCatId == 4) {
    //  //let param = {
    //  //  orderId: orderDetails.orderId,
    //  //  wg: parentProfile,
    //  //  orderCategoryId: this.orderData.catId as number,
    //  //  taskId: this.orderData.taskId as number,
    //  //  purchaseOrderNo: this.orderData.prchOrdrNbr as number
    //  //}
    //  //this.orderLink = this.orderService.getOrderUrl(param)
    //} else {
    console.log(wgData)
      if (wgData) {
        this.orderData = null
        console.log(wgData)
        //console.log(wgData.map(a => { a.usrPrfId }))

        for (let profileId of this.userService.loggedInUser.profileIds) {
          let profile = this.wg > 0 ? this.wg : 2

          let parentProfile = new ProfileHierarchy({ prntPrfId: profile })
          //if (profile == 2) {
          //  parentProfile = this.profileHierarchyService.profileHierarchy.find(a => a.chldPrfId == profileId && a.prntPrfId == profile) || new ProfileHierarchy({ prntPrfId: 0 })
          //  //console.log(parentProfile)
          //} else if (profile > 0) {
          //  parentProfile = new ProfileHierarchy({ prntPrfId: profile })
          //  //console.log(parentProfile)
          //}
          //console.log(this.profileHierarchyService.profileHierarchy)
          //console.log(profileId)
          //console.log(profile)
          //console.log(parentProfile)

          if (Array.isArray(wgData)) {
            console.log(wgData.findIndex(a => a.usrPrfId == parentProfile.prntPrfId))
            this.orderData = wgData[0]
          } else {
            console.log(wgData.usrPrfId == parentProfile.prntPrfId)
            //if (wgData.usrPrfId == parentProfile.prntPrfId) {
              this.orderData = wgData
            //} else {
            //  this.orderData = null
            //}
          }

          console.log(this.orderData)

          if (this.orderData) {
            let param = {
              orderId: this.orderData.ordrId,
              wg: parentProfile.prntPrfId,
              orderCategoryId: this.orderData.catId as number,
              taskId: this.orderData.taskId as number,
              purchaseOrderNo: this.orderData.prchOrdrNbr as number
            }
            this.orderLink = this.orderService.getOrderUrl(param)

            console.log(this.orderLink)
            break;
          }
        }
      }
    //}
  }

  processVendorOrderEmailList(data: Array<any>) {
    return data.map(record => {
      const status = record.emailStatusDesc;
      if(status === 'Draft') {
        record.icon = '';
      } else if(status.includes('UnEnc')) {
        record.icon = 'fa-unlock';
        record.emailStatusDesc = status.includes('UnSent') ? 'Unsent' : 'Sent';
      } else if(status.includes('Enc')) {
        record.icon = 'fa-lock';
        record.emailStatusDesc = status.includes('UnSent') ? 'Unsent' : 'Sent';
      }
      record.ableToAddAckDate = (record.sentToVendorDate != null && record.ackByVendorDate == null);

      return record;
    })
  }
}
