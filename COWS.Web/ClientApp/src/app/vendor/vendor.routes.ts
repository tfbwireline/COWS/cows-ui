import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorFolderComponent } from './folder/vendor-folder.component';
import { VendorFolderFormComponent } from './folder/vendor-folder-form.component';
import { VendorFolderTemplateComponent } from './generic-template/vendor-folder-template.component';
import { VendorOrderFormComponent } from './order/vendor-order-form.component';


const routes: Routes = [
  //{ path: '', component: VendorFolderComponent },
  {
    path: "vendor-folder",
    data: { title: "Vendor Folder" },
    children: [
      { path: '', component: VendorFolderComponent, data: { title: "Vendor Folder" } },
      { path: 'add', component: VendorFolderFormComponent, data: { title: "Create" } },
      { path: ':id', component: VendorFolderFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: "vendor-order",
    data: { title: "Vendor Order" },
    children: [
      { path: '', component: VendorOrderFormComponent, data: { title: "Vendor Order" } },
      { path: 'add', component: VendorOrderFormComponent, data: { title: "Create" } },
      { path: ':id', component: VendorOrderFormComponent, data: { title: "Update" } }
    ]
  },
  {
    path: "vendor-folder-template",
    data: { title: "Vendor Template" },
    component: VendorFolderTemplateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRoutes { }
