import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService, VendorTemplateService } from '../../../services';
import { Global } from '../../../shared/global';
import { Helper, SpinnerAsync } from "../../../shared";
import { VendorFolderTemplate, DocEntity } from '../../../models/index';
import { zip } from 'rxjs';
declare let $: any;

@Component({
  selector: 'app-vendor-folder-template',
  templateUrl: './vendor-folder-template.component.html'
})

export class VendorFolderTemplateComponent implements OnInit {
  option: any;
  isNci: boolean = false;
  docList: DocEntity[] = [];
  spnr: SpinnerAsync;

  constructor(private router: Router, private spinner: NgxSpinnerService, private helper: Helper,
    private userSrvc: UserService, private vndrTmpltSrvc: VendorTemplateService) { 
      this.spnr = new SpinnerAsync(spinner);
    }

  ngOnInit() {
    this.init();
    this.option = {
      inputFileControl: {
        inputFileField: {
          label: "Upload Template(s) to Vendor Folder",
          isShown: true
        }
      },
      grid: {
        column: {
          fileName: {
            caption: "Template"
          },
          fileSize: {
            byte: true
          }
        }
      }
    }
  }

  init() {
    this.spnr.manageSpinner('show');
    let data = zip(
      this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "NCI"),
      this.vndrTmpltSrvc.get()
    )

    data.subscribe(res => {
      this.isNci = res[0] ? true : false;
      let tmplts = res[1] as VendorFolderTemplate[];
      if (tmplts != null && tmplts.length > 0) {
        this.docList = tmplts.map(item => {
          return new DocEntity({
            docId: item.vndrTmpltId,
            id: item.vndrFoldrId,
            fileNme: item.fileNme,
            fileCntnt: item.fileCntnt,
            fileSizeQty: item.fileSizeQty,
            creatDt: item.creatDt,
            creatByUserId: item.creatByUserId,
            creatByUserAdId: item.creatByUserAdId,
            base64string: item.base64string
          })
        })
      }

      if (!this.isNci) {
        this.router.navigate(['/sorry']);
        return;
      }
    }, error => {
      this.helper.notify('An error occurred while getting user access.', Global.NOTIFY_TYPE_ERROR);
    }).add(() => {
      this.spnr.manageSpinner('hide');
    });
  }

  onFileUploadResult(res) {
    let doc = res as DocEntity;
    if (doc != null) {
      this.spinner.show();
      let tmplt = new VendorFolderTemplate();
      tmplt.vndrTmpltId = doc.docId;
      tmplt.vndrFoldrId = doc.id;
      tmplt.fileNme = doc.fileNme;
      tmplt.fileCntnt = doc.fileCntnt;
      tmplt.fileSizeQty = doc.fileSizeQty;
      tmplt.creatByUserId = doc.creatByUserId;
      tmplt.creatDt = doc.creatDt;
      tmplt.base64string = doc.base64string;

      this.vndrTmpltSrvc.create(doc).subscribe(
        res => {
          this.init();
          this.helper.notify('Successfully created generic vendor folder template.', Global.NOTIFY_TYPE_SUCCESS);
          this.spinner.hide();
        }, error => {
          this.spinner.hide();
          this.helper.notify('Failed in creating generic vendor folder template.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }

  onFileRemovedResult(res) {
    if (res > 0) {
      this.spinner.show();
      this.vndrTmpltSrvc.delete(res).subscribe(
        res => {
          this.init();
          this.helper.notify('Successfully deleted generic vendor folder template.', Global.NOTIFY_TYPE_SUCCESS);
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          this.helper.notify('Failed to delete generic vendor folder template.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }
}
