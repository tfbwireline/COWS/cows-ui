import { Component, ViewChild, HostListener } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { zip, Observable, fromEvent } from "rxjs";
import { SideNavComponent } from './side-nav/side-nav.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { NavLinksComponent } from './nav-links/nav-links.component';
import { User, UserProfile } from "./../models/";
import { UserService, MenuService } from "./../services/";
import './content/app.less';
import './content/modal.less';
import { startWith, debounceTime, map } from "rxjs/operators";
//import 'rxjs/add/observable/fromEvent';
import { Router, ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET } from "@angular/router";
import { Title, Meta } from '@angular/platform-browser';
import { filter } from 'rxjs/operators';
import { RoutingStateService } from '../shared';
import { NavHeaderComponent } from './nav-header/nav-header.component';
import { Subject } from 'rxjs';
import { VersionCheckService } from './service/version-check.service';

interface IBreadcrumb {
  label: string;
  params: Params;
  url: string;
  isParent: boolean;
}
declare let $ : any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // CHILDREN
  //@ViewChild(SideNavComponent) sideNav: SideNavComponent;
  //@ViewChild(NavMenuComponent) navMenu: NavMenuComponent;
  //@ViewChild(NavLinksComponent) navLink: NavLinksComponent;
  @ViewChild(NavHeaderComponent, { static: false }) navHeader: NavHeaderComponent;

  // Classes
  user: User;
  userProfile: string[]
  urlAddress: string = "";
  public breadcrumbs: IBreadcrumb[];
  title: string[] = ['Default Title'];
  menuCount: string;
  menuCountInt: number;
  browsertitle = "COWS Rewrite"
  //isSideNav: boolean = true; // Whether the default is side or top
  //isContentCollapsed: boolean;
  //isAuthorize: boolean = false;
  //forcedHorizontal: boolean = false; // If and only if viewport is less than 768px
  
  userActivity;
  userInactive: Subject<any> = new Subject();
  isInActive: boolean = false;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService,
    public userService: UserService, public menuService: MenuService,private titleService: Title, private meta: Meta, private routingStateService: RoutingStateService
    , private versionCheck: VersionCheckService) {
    //this.breadcrumbs = [];
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.menuService.getTitle(router.routerState, router.routerState.root, 1);
        //console.log(this.menuService.breadcrumbs)
      }
    });

    this.routingStateService.loadRouting()
    //this.getLoggedInUser();
    //this.userService.getLoggedInUser2();
    //console.log("POI")
    //this.userService.getLoggedInUser2().subscribe(res => {
    //  console.log("LKJ")
    //  console.log(res)
    //});

    this.menuCount = localStorage.getItem('menuCount');
    this.menuCountInt = this.menuCount == null ? 0 : Number(this.menuCount);
    
    this.setTimeout();
    this.userInactive.subscribe(status =>  {
      this.isInActive = status;
      if(status) {
        // this.router.navigate(['/session-timeout']);
      }
    });
  }

  @HostListener('window:mousemove') refreshUserState() {
    clearTimeout(this.userActivity);
    this.setTimeout();
  }

  setTimeout() {
    this.userActivity = setTimeout(() => this.userInactive.next(true), 3600000);
  }

  setUserToActive(event : any) {
    this.userInactive.next(false);
  }

  returnHome() {
    // Workaround to force reload Home Component ngOnInit
    this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() => {
      this.userInactive.next(false);
      this.router.navigate(['/home']);
    });
  }

  ngOnInit() {    
    const appTitle = this.titleService.getTitle();
    //console.log(appTitle);
    //this.titleService.setTitle(appTitle);

    //this.versionCheck.initVersionCheck('../../assets/version.json');
   
    this.router
      .events.pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => {
          let child = this.activatedRoute.firstChild;          
          if (child.snapshot.data['title']) {
            return child.snapshot.data['title'];
          }
          return appTitle;
        })
      ).subscribe((ttl: string) => {
        this.titleService.setTitle("COWS " +ttl);
      });

    //let resizeObservable$ = fromEvent(window, 'resize').pipe()
    //resizeObservable$.subscribe(evt => {
    //  //this.refresh()
    //  this.menuService.refresh()
    //})

    $(document).ready(function () {
      $(document).click(function (event) {
        var $button = $(event.target);
        $(".collapse").each(function() {
          var $collapsible = $(this)
          var isOpened = $collapsible.hasClass("show")
          if (isOpened && $button.attr("href") != "#" + $collapsible.attr("id")) {
            $collapsible.collapse("hide");
          }
        });
      });
    });
  }

  //refresh() {
  //  this.forcedHorizontal = window.innerWidth < 768 ? true : false;

  //  if (this.forcedHorizontal) {
  //    this.isContentCollapsed = this.forcedHorizontal
  //    this.sideNav.isSideNavCollapsed = !this.forcedHorizontal
  //  } else {
  //    this.isContentCollapsed = this.isSideNav ? false : true;
  //    this.sideNav.isSideNavCollapsed = this.isSideNav ? true : false;
  //  }

  //  this.sideNav.navLink.init();
  //  this.navMenu.navLink.init();
  //}
  
  getTitle(state, parent, i) {
    
    var data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.title) {
      if (parent.snapshot._lastPathIndex == 0)
        this.urlAddress = "";

      if (parent.snapshot.routeConfig.path != "") {
        console.log(parent.snapshot._lastPathIndex)
        console.log(parent.snapshot.url)
        //get the route's URL segment
        let routeURL: string = parent.snapshot.url.map(segment => segment.path).join("/");
        console.log(routeURL)

        //append route URL to URL
        this.urlAddress += `/${routeURL}`;

        //add breadcrumb
        let breadcrumb: IBreadcrumb = {
          label: parent.snapshot.data.title,
          params: parent.snapshot.params,
          url: this.urlAddress,
          //isParent: parent.snapshot._lastPathIndex == 0 || parent.snapshot.data.title == "Home" ? true : false
          isParent: true
        };
        data.push(breadcrumb);
      }
    }

    if (state && parent) {
      for (let val of this.getTitle(state, state.firstChild(parent), i)) {
        if (!data.includes(val)) {
          data.push(val);
        }
      }
    }
    return data
  }

  //getLoggedInUser() {
  //  // Check if user is logged in

  //  this.spinner.show();

  //  this.userService.getLoggedInUser().subscribe(
  //    res => {

  //      if (res != null) {
  //        let adid = res.userAdid;

  //        localStorage.setItem('userADID', adid);
  //        localStorage.setItem('userID', res.userId);
  //        //localStorage.setItem('validUser', 'true');
  //        this.setAuthorization(true);

  //        let data = zip(
  //          this.userService.getUserByADID(adid),
  //          this.userService.getUserProfilesByADID(adid)
  //        );

  //        data.subscribe(res2 => {
  //          let user = res2[0] as User
  //          let userProfile = res2[1] as UserProfile[]

  //          if (user != null) {
  //            this.user = user
  //            this.userProfile = userProfile.map(a => a.usrPrfDes);

  //            localStorage.setItem('userFullName', this.user.fullNme);
  //            //localStorage.setItem('userProfile', userProfile);

  //            if (this.user.menuPref == 'V') {
  //              this.isSideNav = true
  //            } else {
  //              this.isSideNav = false
  //            }
  //            this.refresh();
  //          } else {
  //            this.setAuthorization(false);
  //            this.router.navigate(['/authorize']);
  //          }
  //        });

  //        this.spinner.hide();
  //      }
  //      else {
  //        //localStorage.setItem('validUser', 'false');
  //        this.setAuthorization(false);
  //        this.router.navigate(['/authorize']);
  //      }
  //    },
  //    error => {
  //      setTimeout(() => { this.spinner.hide(); }, 0);
  //    });
  //}

  //setAuthorization(isAuthorize: boolean) {
  //  this.isAuthorize = isAuthorize;
  //  localStorage.setItem('validUser', (isAuthorize ? "true" : "false"));
  //}
  //get isAdmin() {
  //  return this.logged
  //}


  collapseSideNav() {
    this.menuService.isContentCollapsed = !this.menuService.isContentCollapsed
    this.menuService.isSideNav = !this.menuService.isSideNav
  }
}
