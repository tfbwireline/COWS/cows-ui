import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-cancel',
  templateUrl: './cancel.component.html',
  //styleUrls: ['./cancel.component.css']
})
export class CancelComponent implements OnInit {
  url: string;
  message: string;
  
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams
      .filter(params => params.returnUrl)
      .subscribe(params => {
        console.log(params); // {order: "popular"}

        this.url = params.returnUrl;
        console.log(this.url); // popular
      });

    if (this.url.includes("cancel")) {
      this.message = 'You are not an authorized user to initiate cancels';
    }
    else if (this.url.includes("fedline")) {
      this.message = 'CSG level 2 is required to view Fedline events';
    }
  }

}
