import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Global } from "./../../../shared/global";
import { EventHistoryService } from "./../../../services";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lookup-event-history',
  templateUrl: './lookup-event-history.component.html'
})

export class LookupEventHistoryComponent implements OnInit {
  form: FormGroup;
  eventHistoryList: any[] = [];
  get eventId() { return this.form.get("eventId") }

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private avRoute: ActivatedRoute,
    private evntHistSrvc: EventHistoryService) { }

  ngOnInit() {
    let id = this.avRoute.snapshot.params["id"] || 0;
    this.form = new FormGroup({
      eventId: new FormControl(null),
    });

    if (id > 0) {
      this.form.get('eventId').setValue(id);
      this.lookup(id);
    }
  }


  lookup(id: any) {
    console.log('id: ' + id)
    //let eventId = this.avRoute.snapshot.params["id"] || this.form.get('eventId').value;
    if (this.helper.isEmpty(id)) {
      this.helper.notify('Event ID is required', Global.NOTIFY_TYPE_WARNING);
    }
    else {
      if (id > 0) {
        this.spinner.show();
        //this.form.get('eventId').setValue(eventId);
        this.evntHistSrvc.getByEventId(id).toPromise().then(
          data => {
            this.eventHistoryList = data;
          }
        ).then(() => { this.spinner.hide() });
      }
      else {
        this.helper.notify('Event ID should be numeric', Global.NOTIFY_TYPE_WARNING);
      }
    }
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'EventHistory_' + this.form.get("eventId").value + '_' + this.helper.formatDate(new Date());
  }
}
