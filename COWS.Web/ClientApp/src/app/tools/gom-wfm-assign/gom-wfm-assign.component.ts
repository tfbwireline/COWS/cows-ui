import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, Subscribable, Observable } from "rxjs";
import { Helper } from "./../../../shared";
import { KeyValuePair, EEventType, Global } from "./../../../shared/global";
import { EventTypeService, MDSNetworkActivityTypeService, UserService, MplsEventTypeService, IPVersionService, WFMOrderService, UserProfileService } from "./../../../services/";
import { FormControlItem } from "./../../../models";


@Component({
  selector: 'app-gom-wfm-assign',
  templateUrl: './gom-wfm-assign.component.html'
})
export class GomWfmAssignComponent implements OnInit {

  form: FormGroup;
  userId: number;
  userProfileId: number;
  wfmLevel: number;
  userAdid: string;
  fullName: string;
  gomAssigner: boolean = false;
  gomUser: boolean = false;
  loggedInUserProfilesList: any[] = [];
  wfmProfileList: any[] = [];
  wfmOrdersList: any[] = [];
  userAssignList: any[] = [];
  assignedUserList: any[] = [];
  wfmOrdersList_1: any[] = [];
  userAssignList_1: any[] = [];
  assignedUserList_1: any[] = [];
  chkWorkGroupList: string[]=[]; 
  selectedWorkGroup: string="";
  subStatusList: string[]=[];
  selectedSubStatus: string="";
  selectedUserToAssign: string="";
  selectedWFMOrders: any[] = [];
  lblError: string = "";
  locked: boolean = false;

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private wfmOrderService: WFMOrderService, private userProfileService: UserProfileService) {
    this.chkWorkGroupList = [
      "Not in Intl CPE Workgroup",
      "Intl CPE Workgroup",
      "Show everything"
    ];
    this.selectedWorkGroup = this.chkWorkGroupList[1];

    this.subStatusList = [
      "--",
      "PreSubmit",
      "Submit"
    ];    
  }

  ngOnInit() {

    this.form = new FormGroup({
      userToAssign: new FormControl(),
      m5Filter: new FormControl(),
      assignedUser: new FormControl(),
      subStatus: new FormControl(),

    });

    this.userId = Number(localStorage.getItem('userID'));

    this.helper.form = this.form;

    let data = zip(
      this.userProfileService.getMapUserProfilesByUserId(this.userId),
      this.wfmOrderService.GetWFMprofile(this.userId, 'GOM'),
      this.wfmOrderService.GetWFMData('', '', 1, 'GOM'),
      this.wfmOrderService.GetNCIUser(126)
    );

    data.subscribe(res => {
      this.loggedInUserProfilesList = res[0];
      this.wfmProfileList = res[1];
      this.wfmOrdersList_1 = res[2].table;
      this.assignedUserList_1 = res[2].table1;
      this.userAssignList_1 = res[3];

      this.gomAssigner = this.loggedInUserProfilesList.some(a => a.usrPrfDes == 'GOM WFM Assigner');

      if (this.wfmProfileList.length > 0) {
        this.userProfileId = this.wfmProfileList[0].usrPrfId;
        this.wfmLevel = this.wfmProfileList[0].wfmAsmtLvlId;
        this.userAdid = this.wfmProfileList[0].userAdid;
        this.fullName = this.wfmProfileList[0].dsplNme;

        if (this.userProfileId == 114) {
          this.gomUser = true;
        }
      }

      if (this.gomUser != true && this.gomAssigner != true) {
        this.lblError = "Sorry, you are not in Intl CPE profile. No orders will be displayed.";
        this.wfmOrdersList = [];
        this.assignedUserList = [];
        this.userAssignList = [];
        this.subStatusList = [];
        this.locked = true;
      }
      else if ((this.wfmProfileList.length > 0 && this.gomUser) || this.gomAssigner) {
        if (!this.gomAssigner) {
          this.userAssignList = this.userAssignList_1.filter(a => a.userId = this.userId);
        }
        this.userAssignList = this.userAssignList_1;
        this.wfmOrdersList = this.wfmOrdersList_1;
        this.assignedUserList = this.assignedUserList_1;
        this.selectedSubStatus = this.subStatusList[0];
      }
    });
  }

  onChkWorkGroupChanged($event) {
    if (this.gomUser != true && this.gomAssigner != true) {
      this.lblError = "Sorry, you are not in Intl CPE profile. No orders will be displayed.";
      this.wfmOrdersList = [];
      this.assignedUserList = [];
      this.userAssignList = [];
      this.subStatusList = [];
      this.locked = true;
    }
    else {
      this.selectedWorkGroup = $event.value;
      this.clearForm();
      this.loadGrid();
    }
  }

  public onUserToAssignChanged(event: any) {
    this.selectedUserToAssign = event.selectedItem.dsplNme;
  }

  btngrvAssign_Click() {
    if (this.selectedUserToAssign == "") {
      this.helper.notify(`User to Assign: box must be populated to use the Assign button.`, Global.NOTIFY_TYPE_WARNING);
    }
    else if (this.selectedUserToAssign != this.fullName && this.gomAssigner == false) {
      this.helper.notify(`Your profile only allows you to assign orders to yourself.`, Global.NOTIFY_TYPE_WARNING);
    }
    else if (this.selectedWFMOrders.length == 0) {
      this.helper.notify(`Please select atleast one item from the list to perform the assign/unassign operation.`, Global.NOTIFY_TYPE_WARNING);
    }
    else {
      let orderModel = {
        UserId: this.userId,
        UserToAssign: this.selectedUserToAssign,
        WFMOrders: this.selectedWFMOrders,
        Profile: 'GOM'
      }
      this.wfmOrderService.assignClick(orderModel).subscribe(data => {
        if (data != null) {
          this.wfmOrdersList = data.table;
          this.assignedUserList = data.table1;
          this.helper.notifySavedFormMessage("", Global.NOTIFY_TYPE_SUCCESS, false, null);
        }
      });
    }
  }

  btngrvUnAssign_Click() {
    if (this.gomAssigner == false) {
      this.helper.notify(`You do not have Unassign authority on your profile. Your authority only allows you to assign orders to yourself.`, Global.NOTIFY_TYPE_WARNING);
    }
    else if (this.selectedWFMOrders.length == 0) {
      this.helper.notify(`Please select atleast one item from the list to perform the assign/unassign operation.`, Global.NOTIFY_TYPE_WARNING);
    }
    else {
      let orderModel = {
        UserId: this.userId,
        UserToAssign: this.selectedUserToAssign,
        WFMOrders: this.selectedWFMOrders,
        Profile: 'GOM'
      }
      this.wfmOrderService.unAssignClick(orderModel).subscribe(data => {
        if (data != null) {
          this.wfmOrdersList = data.table;
          this.assignedUserList = data.table1;
          this.helper.notifySavedFormMessage("", Global.NOTIFY_TYPE_SUCCESS, false, null);
        }
      });
    }
  }

  onAssignedUserChanged(event: any) {
    this.loadGrid();
  }

  onSubStatusChanged(event: any) {

  }

  selectionChangedHandler(data: any) {
    this.selectedWFMOrders = data.selectedRowsData;
  }

  btngrvSearch_Click() {
    this.loadGrid();
  }

  btngrvClear_Click() {
    this.clearForm();
    this.loadGrid();
  }

  clearForm() {
    this.helper.setFormControlValue('userToAssign', '');
    this.helper.setFormControlValue('m5Filter', '');
    this.helper.setFormControlValue('assignedUser', '');
    //this.helper.setFormControlValue('subStatus', '');
  }

  loadGrid() {
    let sFilter: string = "";
    if (!this.helper.isEmpty(this.helper.getFormControlValue('m5Filter'))) {
      sFilter = " WHERE FTN LIKE '%" + this.helper.getFormControlValue('m5Filter') + "%'";
    }

    if (!this.helper.isEmpty(this.helper.getFormControlValue('assignedUser'))) {
      if (this.helper.getFormControlValue('assignedUser') != "--" && sFilter != "") {
        sFilter += " AND ASSIGNED_USER = '" + this.helper.getFormControlValue('assignedUser') + "'";
      }
      else if (this.helper.getFormControlValue('assignedUser') != "--") {
        sFilter += "WHERE ASSIGNED_USER = '" + this.helper.getFormControlValue('assignedUser') + "'";
      }
    }

    //if (!this.helper.isEmpty(this.helper.getFormControlValue('subStatus'))) {
    //  if (this.helper.getFormControlValue('subStatus') != "--" && sFilter != "") {
    //    sFilter += " AND SUB_STATUS = '" + this.helper.getFormControlValue('subStatus') + "'";
    //  }
    //  else if (this.helper.getFormControlValue('subStatus') != "--") {
    //    sFilter += "WHERE SUB_STATUS = '" + this.helper.getFormControlValue('subStatus') + "'";
    //  }
    //}

    let view: number = 1;
    if (this.selectedWorkGroup == "Not in Intl CPE Workgroup")
      view = 0;
    else if (this.selectedWorkGroup == "Intl CPE Workgroup")
      view = 1;
    else
      view = 2;

    this.wfmOrderService.GetWFMData('', sFilter, view, 'GOM').subscribe(data => {
      if (data != null) {
        this.wfmOrdersList = data.table;
        this.assignedUserList = data.table1;
      }
    });
  }
}
