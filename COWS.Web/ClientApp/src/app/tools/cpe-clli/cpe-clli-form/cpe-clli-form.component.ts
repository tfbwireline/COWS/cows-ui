import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { NgxSpinnerService } from 'ngx-spinner';

import { StateService } from '../../../../services/state.service';
import { CpeClliService } from '../../../../services/cpe-clli.service';
import { Helper } from '../../../../shared';
import { Global } from "../../../../shared/global";
import { CpeClliFormModel } from './cpe-clli-form.model';

@Component({
  selector: 'app-cpe-clli-form',
  templateUrl: './cpe-clli-form.component.html',
})
export class CpeClliFormComponent implements OnInit, OnDestroy {

  public pageForm: FormGroup;
  public CPE_CLLI_ID = 0;
  public requestDetails: CpeClliFormModel;

  public IsModify = false;
  public statesList = [];
  public rbtIsSprintOwned = [{ text : 'Yes', value : true }, { text : 'No', value : false }];

  get clliId() { return this.pageForm.get("clliId") }

  public onDestroy = new Subject<void>();

  constructor(public helper: Helper, private activeRoute: ActivatedRoute, private router: Router, private fb: FormBuilder, private spinner: NgxSpinnerService, private stateService : StateService, private cpeClliService: CpeClliService) { }

  ngOnInit() {

    this.stateService.get().pipe(takeUntil(this.onDestroy)).subscribe(data => {
      this.statesList = data;
    })
    
    this.pageForm = this.createPageForm();

    this.CPE_CLLI_ID = this.activeRoute.snapshot.paramMap.get('id') === null ? 0 : parseInt(this.activeRoute.snapshot.paramMap.get('id'));
    this.IsModify = this.CPE_CLLI_ID !== 0;

    if(this.IsModify) {
      this.cpeClliService.getById(this.CPE_CLLI_ID).pipe(take(1)).subscribe(data => {
        this.requestDetails = Object.assign({}, data);
        this.mapDetailsToForm(data);
      });
    }
  }


  public onSubmit() {
    const formData = this.pageForm.value;

    const clliId = this.pageForm.controls['clliId'].value;
    this.cpeClliService.checkClliIdIfExist(this.CPE_CLLI_ID, clliId).pipe(take(1)).subscribe(isExist => {
      if(isExist) {
        this.helper.notify(`CLLI ID already exists `, Global.NOTIFY_TYPE_ERROR);
      }
      else {
        formData['recStusId'] = formData['recStusId'] ? '0' : '1';
        formData['cpeClliStusId'] = (formData['psoftUpdDt'] !== null && formData['sstatUpdDt'] != null) ? '2' : '0';
        if(!this.isValidToSubmit(formData) || !this.pageForm.valid) {
          return;
        }
        
        formData['clliId'] = clliId.toUpperCase();
        if(this.IsModify) {
          this.cpeClliService.update(formData).pipe(take(1)).subscribe(data => {
            this.helper.notify(`Successfully updated CPE CLLI Request`, Global.NOTIFY_TYPE_SUCCESS);
            this.router.navigateByUrl('/tools/cpe-clli');
          });
        } else {
          this.cpeClliService.create(formData).subscribe(data => {
            this.helper.notify(` Successfully created CPE CLLI Request`, Global.NOTIFY_TYPE_SUCCESS);
            this.router.navigateByUrl('/tools/cpe-clli');
          });
        }
      }
    })
  }

  public onStateChange(event: any) { 
    const cityNme = this.pageForm.controls['cityNme'].value;
    const stateCd = event['selectedItem']['usStateId'];

    this.formatClliId(cityNme, stateCd);
  }

  public onCityNameBlur(event: any) {
    const cityNme = event.currentTarget.value;
    const stateCd = this.pageForm.controls['sttId'].value;

    console.log(cityNme);

    this.formatClliId(cityNme, stateCd);
  }

  private formatClliId(cityNme: string, stateCd: string) {
    if(cityNme != ""  && cityNme.length >= 4 && stateCd != '') {
      this.spinner.show();
      this.cpeClliService.getClliID(cityNme.replace(/\s/g, ''), stateCd).pipe(take(1)).subscribe(val => {
        this.pageForm.controls['clliId'].patchValue(val);
        this.spinner.hide();
      });
    }
  }

  private isValidToSubmit(data: any): boolean {

    const oldData = Object.assign({}, this.requestDetails);
    const newData = data;

    if(newData['cpeClliId'] === 0) {
      if(newData['cpeClliStusId'] === "2" && newData['recStusId'] === "0") {
        this.helper.notify(`Please make the request Active first before completion`, Global.NOTIFY_TYPE_ERROR);
        return false;
      }
    } else {
      if(newData['recStusId'] != "1" && newData['cpeClliStusId'] == "2" && oldData['cpeClliStusId'] == "0") {
        this.helper.notify(`Please make the request Active first before completion`, Global.NOTIFY_TYPE_ERROR);
        return false;
      }
    }

    return true;
  }

  private createPageForm(data: any = null) {

    return this.fb.group({
         cpeClliId: new FormControl(0),
         clliId: new FormControl('', [Validators.required, Validators.minLength(11)]), // CLLI ID:
         siteNme: new FormControl(''), // Site Name:
         addrTxt: new FormControl('', [Validators.required]), // Address
         sttId: new FormControl('', [Validators.required]), // State - dropdown
         cityNme: new FormControl('', [Validators.required, Validators.minLength(4)]), // City:
         zipCd: new FormControl('', [Validators.required]), // Zip Code:
         countyNme: new FormControl('', [Validators.required]), // County:
         cmntyNme: new FormControl(''), // Legacy Community Name:
         flr: new FormControl(''), // Floor:
         room: new FormControl(''), // Room:
         lttdeCoord: new FormControl(''), // Latitude Coordinate:
         lngdeCoord: new FormControl(''), // Longitude Coordinate:
         sprntOwndCd: new FormControl(true), // Sprint Owned:
         sitePurpose: new FormControl(''), // Site Purpose:
         siteOwnr: new FormControl(''), // Site Owner:
         sitePhn: new FormControl(''), // Site Phone:
         cmntTxt: new FormControl(''), // Comments:
         emailCc: new FormControl(''), // CC:
         psoftUpdDt: new FormControl(null), // PeopleSoft Update:
         sstatUpdDt: new FormControl(null), // SSTAT Update:
         psoftSendCd: new FormControl(true), // Send email to PeopleSoft
         recStusId: new FormControl(false), // Make inactive
         cpeClliStusId: new FormControl('') 
     })
 }

  private mapDetailsToForm(data : any) {
  const detail = {};
  for(let key in data) {

    let val =  data[key];
    if(key === 'recStusId') {
       val = (val === 0) ? true : false;
    } 
    detail[key] = val;
    this.pageForm.controls[key].patchValue(val);
  }
}

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}
