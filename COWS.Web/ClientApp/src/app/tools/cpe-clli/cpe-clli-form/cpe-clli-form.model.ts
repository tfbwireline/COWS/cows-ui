export interface CpeClliFormModel {
    cpeClliId  : number;
    clliId : string;
    siteNme : string;
    addrTxt : string;
    sttId : string;
    cityNme : string;
    zipCd : string;
    countyNme : string;
    cmntyNme : string;
    flr : string;
    room : string;
    lttdeCoord : string;
    lngdeCoord : string;
    siteOwnr : string;
    sprntOwndCd : boolean;
    sitePurpose : string;
    sitePhn : string;
    cmntTxt : string;
    emailCc : string;
    psoftSendCd : boolean;
    recStusId : boolean;
    psoftUpdDt: Date;
    sstatUpdDt: Date;
    cpeClliStusId : string;
}