import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { CpeClliService } from '../../../services/cpe-clli.service';
import { Helper } from '../../../shared';

@Component({
  selector: 'app-cpe-clli',
  templateUrl: './cpe-clli.component.html',
  styleUrls: ['./cpe-clli.component.css']
})

export class CpeClliComponent implements OnInit, OnDestroy {

  public statusList = [
    { text: 'All', value : '' },
    { text: 'All Pending', value : '0' },
    { text: 'All Completed', value: '2' }
  ]

  public filterList = [
    { text: 'CLLI ID', value : 'clliId' },
    { text: 'Zip Code', value : 'zipCd' },
  ]

  public filterForm: FormGroup;
  public gridData = [];
  public gridDataSub: Subject<any> = new Subject();
  public excelFileName = ''

  public onDestroy = new Subject<void>();

  constructor(private fb: FormBuilder, public router: Router, public helper: Helper,
    private spinner: NgxSpinnerService, private cpeClliService: CpeClliService) { }

  ngOnInit() {
    
    const currentDate = new Date().toLocaleDateString();
    this.excelFileName = `CPE CLLI ${currentDate}`;

    this.gridDataSub.subscribe(data => {
      this.gridData = data;
      this.spinner.hide();
    })

    this.filterForm = this.fb.group({
      status: new FormControl('',[]),
      filter:  new FormControl('clliId',[]),
      filterValue: new FormControl('',[]),
    })

    this.spinner.show()
    this.cpeClliService.get(this.filterForm.value).subscribe(data => {
      this.gridDataSub.next(data);
    });

  }

  public viewDetails(row: any) {
    const url = `tools/cpe-clli/${row['data']['id']}`;
    this.router.navigateByUrl(url);
  }

  selectionChangedHandler(id: any) {
    this.router.navigate(['tools/cpe-clli/' + id]);
  }

  public onStatusChange(event: any) {

    const status = event['selectedItem']['value'];
    const formData = this.filterForm.value;

    formData['status'] = status;
    this.spinner.show()
    this.cpeClliService.get(formData).subscribe(data => {
      this.gridDataSub.next(data);
    })
  }

  public filterSearch() {
    const formData = this.filterForm.value;
    this.spinner.show()
    this.cpeClliService.get(formData).subscribe(data => {
      this.gridDataSub.next(data);
    })
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'CPECLLI_' + this.helper.formatDate(new Date());
  }

  public ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}

