import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { DxDataGridComponent } from "devextreme-angular";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Global, KeyValuePair, ELkSysCfgMenus } from "./../../../shared/global";
import { UserService, CcdService } from '../../../services/index';
import { CustomValidator } from '../../../shared/validator/custom-validator';
import { zip } from 'rxjs';
import { Ccd } from '../../../models';
declare let $: any;

@Component({
  selector: 'app-ccd-change',
  templateUrl: './ccd-change.component.html'
})

export class CcdChangeComponent implements OnInit {
  @ViewChild('gridOrder', { static: false }) gridOrder: DxDataGridComponent;
  form: FormGroup;
  isSubmitted: boolean = false;
  ccdReasonList: KeyValuePair[] = [];
  orderList: any[] = [];
  selectedOrders: any[] = [];

  get m5ctn() { return this.form.get("m5ctn") }
  get h5() { return this.form.get("h5") }
  get ccdDate() { return this.form.get("ccdDate") }
  get ccdReason() { return this.form.get("ccdReason") }
  get notes() { return this.form.get("notes") }

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private ccdSrvc: CcdService, private userSrvc: UserService) { }

  ngOnInit() {

    this.userSrvc.GetAdminExcPerm(ELkSysCfgMenus.ManageCCDBypass).subscribe(hasAccess => {
      if(!hasAccess) {
        this.router.navigate(['/sorry']);
      }
    })

    this.form = new FormGroup({
      m5ctn: new FormControl(null),
      h5: new FormControl(null, Validators.compose([CustomValidator.h1Validator])),
      ccdDate: new FormControl(null),
      ccdReason: new FormControl(null),
      notes: new FormControl(null),
    });

    this.ccdSrvc.getCcdReason().subscribe(
      res => {
        this.ccdReasonList = res;
      },
      error => {
        console.log(error)
      }
    );
  }

  reset() {
    this.isSubmitted = false;
    this.orderList = [];
  }

  search() {
    if (this.helper.isEmpty(this.m5ctn.value) && this.helper.isEmpty(this.h5.value)) {
      this.helper.notify('Please enter either M5 #/CTN or H6 value', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    let m5 = this.helper.isEmpty(this.m5ctn.value) ? '' : this.m5ctn.value;
    let h5 = this.helper.isEmpty(this.h5.value) ? 0 : this.h5.value;

    this.isSubmitted = true;
    this.ccdSrvc.searchCcd(m5, h5).subscribe(
      res => {
        //console.log(res)
        this.orderList = res;
      },
      error => {
        let msg = this.helper.isEmpty(error) ? ''
          : this.helper.isEmpty(error.message) ? '' : error.message;
        this.helper.notify('Failed to load Order details. ' + msg, Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  onOrderSelectionChanged(e) {
    this.selectedOrders = e.selectedRowKeys;
  }

  changeCcd() {
    if (this.helper.isEmpty(this.selectedOrders)) {
      this.helper.notify('No orders selected', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    let orderIds = this.selectedOrders.join();
    let ccdDate: Date = new Date(Date.parse(this.ccdDate.value));
    let ccdReason = this.helper.isEmpty(this.ccdReason.value) ? null : this.ccdReason.value.join();
    let notes = this.notes.value;

    if (this.helper.isEmpty(ccdDate)) {
      this.helper.notify('New CCD Date is required', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    if (new Date(ccdDate).setHours(new Date().getHours(), 0, 0, 0) < new Date().setHours(new Date().getHours(), 0, 0, 0)) {
      this.helper.notify('CCD may not be in the past', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    if (this.helper.isEmpty(this.ccdReason.value)) {
      this.helper.notify('At least one Reason must be checked', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    let ccd = new Ccd();
    ccd.orderIds = orderIds;
    ccd.ccdReasons = ccdReason;
    ccd.notes = notes;
    ccd.ccdDate = ccdDate;

    this.ccdSrvc.changeCcd(ccd).subscribe(
      res => {
        //console.log(res)
        this.helper.notify('Successfully changed CCD. Please look above to see the updated CCD.', Global.NOTIFY_TYPE_SUCCESS);
        this.reload();
      },
      error => {
        let msg = this.helper.isEmpty(error) ? ''
          : this.helper.isEmpty(error.message) ? '' : error.message;
        this.helper.notify('Failed to change CCD. ' + msg, Global.NOTIFY_TYPE_ERROR);
      }
    );
  }

  reload() {
    this.search();
    this.selectedOrders = [];
    if (this.gridOrder != null || this.gridOrder != undefined) {
      this.gridOrder.instance.clearSelection();
    }
    this.ccdDate.reset();
    this.ccdReason.reset();
    this.notes.reset();
  }
}
