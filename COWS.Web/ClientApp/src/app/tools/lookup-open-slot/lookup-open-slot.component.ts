import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from "rxjs";
import { Helper } from "./../../../shared";
import { KeyValuePair, EEventType, Global } from "./../../../shared/global";
import { EventTypeService, MDSNetworkActivityTypeService, UserService } from "./../../../services/";
import { FormControlItem } from "./../../../models";
import * as moment from 'moment';

@Component({
  selector: 'app-lookup-open-slot',
  templateUrl: './lookup-open-slot.component.html'
})

export class LookupOpenSlotComponent implements OnInit {
  isSubmitted: boolean = false;
  form: FormGroup;
  //prodTypeList: any[] = [];
  mdsActvtyTypeList: any[] = [];
  //ipVersionList: any[] = [];
  //mplsEventTypeList: any[] = [];
  openSlotList: any[] = [];

  sortIndex: any[] = []; 

  //sortIndex: any[] =[
  //"12:00 AM",
  //"1:00 AM",
  //"2:00 AM",
  //"3:00 AM",
  //"4:00 AM",
  //"5:00 AM",
  //"6:00 AM",
  //"7:00 AM",
  //"8:00 AM",
  //"9:00 AM",
  //"10:00 AM",
  //"11:00 AM",
  //"12:00 PM",
  //"1:00 PM",
  //"2:00 PM",
  //"3:00 PM",
  //"4:00 PM",
  //"5:00 PM",
  //"6:00 PM",
  //"7:00 PM",
  //"8:00 PM",
  //"9:00 PM",
  //"10:00 PM",
  //"11:00 PM"];

  //get productType() { return this.form.get("productType") }
  get slotDate() { return this.form.get("slotDate") }

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private evntTypeSrvc: EventTypeService,
    private userService: UserService, private mdsNtwrkActvtyTypeSrvc: MDSNetworkActivityTypeService) { }

  ngOnInit() {
    this.form = new FormGroup({
      //productType: new FormControl(null, Validators.required),
      mdsActvtyType: new FormControl(null),
      //mplsEventType: new FormControl(null),
      //ipVersion: new FormControl(null),
      slotDate: new FormControl(null, Validators.required),
    });

    let data = zip(
      this.mdsNtwrkActvtyTypeSrvc.getForControl(),
      //this.ipVersionSrvc.getForControl(),
      //this.mplsEvntTypeSrvc.getByEventId(EEventType.MPLS),
      this.evntTypeSrvc.getForControl()
    );

    data.toPromise().then(res => {
      this.mdsActvtyTypeList = res[0];
      //this.ipVersionList = (res[1] as FormControlItem[]).filter(i => i.text.toLowerCase().includes('mpls'));
      //this.mplsEventTypeList = res[2] as KeyValuePair[];
      //this.prodTypeList = (res[3] as FormControlItem[]).filter(i => +i.value < 6);
    });

    
  }

  onProductTypeChange(e) {
    this.form.get('mdsActvtyType').reset();
    //this.form.get('mplsEventType').reset();
    //this.form.get('ipVersion').reset();
    this.form.get('slotDate').reset();
    this.isSubmitted = false;
    this.openSlotList = [];
  }

  reset() {
    this.isSubmitted = false;
    this.openSlotList = [];
  }

  lookup() {
    let slotDate = this.form.get("slotDate").value as Date || null;
    let actvtyType = this.form.get("mdsActvtyType").value || null;
    if (!this.helper.isEmpty(slotDate) && !this.helper.isEmpty(actvtyType)) {
      this.spinner.show();
      //let start = new Date(Date.parse(this.form.get("slotDate").value));
      let start = moment(slotDate).format("MM/DD/YYYY")
      //let mplsEvntType = this.form.get("mplsEventType").value;
      //let ipVerId = this.form.get("ipVersion").value;

      let params = [{
        stTime: start,
        stHr: "0",
        stMin: "0",
        product: "5",
        //submitType: submitType,
        //eventTypeId: this.eventType,
        //mplsEventType: this.helper.isEmpty(mplsEvntType) ? "0" : mplsEvntType,
        //ipVersionId: this.helper.isEmpty(ipVerId) ? 0 : ipVerId,
        mDSNtwkActy: actvtyType
      }]

      console.log(params)

      this.userService.getOpenSlots(params[0]).toPromise().then(
        data => {
          this.isSubmitted = true;
          this.openSlotList = data;
        }).then(() => { this.spinner.hide(); });
    }
  }

  customSortingFunction(rowData) {
    this.sortIndex = [
      "12:00 AM",
      "12:15 AM",
      "12:30 AM",
      "12:45 AM",
      "1:00 AM",
      "1:15 AM",
      "1:30 AM",
      "1:45 AM",
      "2:00 AM",
      "2:15 AM",
      "2:30 AM",
      "2:45 AM",
      "3:00 AM",
      "3:15 AM",
      "3:30 AM",
      "3:45 AM",
      "4:00 AM",
      "4:15 AM",
      "4:30 AM",
      "4:45 AM",
      "5:00 AM",
      "5:15 AM",
      "5:30 AM",
      "5:45 AM",
      "6:00 AM",
      "6:15 AM",
      "6:30 AM",
      "6:45 AM",
      "7:00 AM",
      "7:15 AM",
      "7:30 AM",
      "7:45 AM",
      "8:00 AM",
      "8:15 AM",
      "8:30 AM",
      "8:45 AM",
      "9:00 AM",
      "9:15 AM",
      "9:30 AM",
      "9:45 AM",
      "10:00 AM",
      "10:15 AM",
      "10:30 AM",
      "10:45 AM",
      "11:00 AM",
      "11:15 AM",
      "11:30 AM",
      "11:45 AM",
      "12:00 PM",
      "12:15 PM",
      "12:30 PM",
      "12:45 PM",
      "1:00 PM",
      "1:15 PM",
      "1:30 PM",
      "1:45 PM",
      "2:00 PM",
      "2:15 PM",
      "2:30 PM",
      "2:45 PM",
      "3:00 PM",
      "3:15 PM",
      "3:30 PM",
      "3:45 PM",
      "4:00 PM",
      "4:15 PM",
      "4:30 PM",
      "4:45 PM",
      "5:00 PM",
      "5:15 PM",
      "5:30 PM",
      "5:45 PM",
      "6:00 PM",
      "6:15 PM",
      "6:30 PM",
      "6:45 PM",
      "7:00 PM",
      "7:15 PM",
      "7:30 PM",
      "7:45 PM",
      "8:00 PM",
      "8:15 PM",
      "8:30 PM",
      "8:45 PM",
      "9:00 PM",
      "9:15 PM",
      "9:30 PM",
      "9:45 PM",
      "10:00 PM",
      "10:15 PM",
      "10:30 PM",
      "10:45 PM",
      "11:00 PM",
      "11:15 PM",
      "11:30 PM",
      "11:45 PM"];
    let index = rowData.timeSlot.split(" - ")[0];
    return this.sortIndex.indexOf(index);  
  }

}
