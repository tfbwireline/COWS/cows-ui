import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
import { zip } from "rxjs";
import { Helper } from "./../../../shared";
import { Global } from "./../../../shared/global";
import { WFMOrderService, UserProfileService } from "./../../../services/";
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-xnci-wfm-assign',
  templateUrl: './xnci-wfm-assign.component.html'
})

export class XnciWfmAssignComponent implements OnInit {
  form: FormGroup;
  userId: number;
  userProfileId: number = 0;
  wfmLevel: number;
  userAdid: string;
  fullName: string;
  nciAssigner: boolean = false;
  nciUser: boolean = false;
  loggedInUserProfilesList: any[] = [];
  wfmProfileList: any[] = [];
  wfmOrdersList: any[] = [];
  userAssignList: any[] = [];
  assignedUserList: any[] = [];
  wfmOrdersList_1: any[] = [];
  userAssignList_1: any[] = [];
  assignedUserList_1: any[] = [];
  chkWorkGroupList: string[] = [];
  selectedWorkGroup: string = "";
  subStatusList: string[] = [];
  selectedSubStatus: string = "";
  selectedUserToAssign: string = "";
  selectedWFMOrders: any[] = [];
  lblError: string = "";
  locked: boolean = false;
  searchCriteria: string = "";

  @ViewChild('grid', { static: true }) dataGrid: DxDataGridComponent;

  constructor(private helper: Helper, private wfmOrderService: WFMOrderService, private userProfileService: UserProfileService) { }

  ngOnInit() {
    this.userId = Number(localStorage.getItem('userID'));
    this.form = new FormGroup({
      userToAssign: new FormControl(),
      m5Filter: new FormControl(),
      assignedUser: new FormControl()

    });

    let data = zip(
      this.userProfileService.getMapUserProfilesByUserId(this.userId),
      this.wfmOrderService.GetWFMprofile(this.userId, 'NCI')
    );

    data.subscribe(res => {
      this.loggedInUserProfilesList = res[0].filter(a => a.usrPrfDes == 'AMNCI WFM Assigner' || a.usrPrfDes == 'ANCI WFM Assigner' || a.usrPrfDes == 'ENCI WFM Assigner');
      this.wfmProfileList = res[1];

      this.nciAssigner = this.loggedInUserProfilesList.some(a => a.usrPrfDes == 'AMNCI WFM Assigner' || a.usrPrfDes == 'ANCI WFM Assigner' || a.usrPrfDes == 'ENCI WFM Assigner');
      //console.log(JSON.stringify(this.loggedInUserProfilesList));
      //console.log(JSON.stringify(this.wfmProfileList));
      //console.log(JSON.stringify(this.wfmOrdersList_1));
      //console.log(JSON.stringify(this.assignedUserList_1));
      if (this.wfmProfileList.length > 0) {
        this.wfmLevel = this.wfmProfileList[0].wfmAsmtLvlId;
        this.userAdid = this.wfmProfileList[0].userAdid;
        this.fullName = this.wfmProfileList[0].dsplNme;

        if (this.loggedInUserProfilesList.length > 0) {
          this.userProfileId = this.loggedInUserProfilesList[0].usrPrfId;
        }

        if (this.userProfileId == 14 || 28 || 112) {
          this.nciUser = true;
          if (this.userProfileId == 14)
            this.searchCriteria = "REGION = 'AMNCI' |REGION = 'AMNCI' ";
          else if (this.userProfileId == 28)
            this.searchCriteria = "REGION = 'ANCI' |REGION = 'ANCI' ";
          else if (this.userProfileId == 112)
            this.searchCriteria = "REGION = 'ENCI' |REGION = 'ENCI' ";
        }

        this.wfmOrderService.GetNCIUser(this.userProfileId).subscribe(data => {
          this.userAssignList_1 = data;
          if (this.nciUser != true && this.nciAssigner != true) {
            this.lblError = "Sorry, you are not in a xNCI workgroup.  No orders will be displayed.";
            this.wfmOrdersList = [];
            this.assignedUserList = [];
            this.userAssignList = [];
            this.subStatusList = [];
            this.locked = true;
          }
          else if ((this.wfmProfileList.length > 0 && this.nciUser) || this.nciAssigner) {
            if (!this.nciAssigner) {
              this.userAssignList = this.userAssignList_1.filter(a => a.userId = this.userId);
            }
            this.userAssignList = this.userAssignList_1;
            this.wfmOrderService.GetWFMData('ASSIGNED_USER asc', this.searchCriteria, 0, 'NCI').subscribe(res => {
              this.wfmOrdersList = res.table;
              this.assignedUserList = res.table1;
            })
          }
        });
      }
    });
  }

  onUserToAssignChanged(event: any) {
    if (!this.helper.isEmpty(event.selectedItem)) {
      this.selectedUserToAssign = event.selectedItem.dsplNme;
    }
  }

  btngrvAssign_Click() {
    if (this.helper.isEmpty(this.selectedUserToAssign)) {
      this.helper.notify(`User to Assign: box must be populated to use the Assign button.`, Global.NOTIFY_TYPE_WARNING);
    }
    else if (this.selectedUserToAssign != this.fullName && this.nciAssigner == false) {
      this.helper.notify(`Your profile only allows you to assign orders to yourself.`, Global.NOTIFY_TYPE_WARNING);
    }
    else if (this.selectedWFMOrders.length == 0) {
      this.helper.notify(`Please select atleast one item from the list to perform the assign/unassign operation.`, Global.NOTIFY_TYPE_WARNING);
    }
    else {
      let orderModel = {
        UserId: this.userId,
        UserToAssign: this.selectedUserToAssign,
        WFMOrders: this.selectedWFMOrders,
        Profile: 'NCI',
        SearchCriteria: this.searchCriteria
      }

      this.wfmOrderService.assignClick(orderModel).subscribe(data => {
        if (data != null) {
          this.wfmOrdersList = data.table;
          this.assignedUserList = data.table1;
          this.form.get('userToAssign').reset();
          this.helper.notify("Successfully assign user.", Global.NOTIFY_TYPE_SUCCESS);
        }
      }, error => {
          this.helper.notify("Failed to assign user.", Global.NOTIFY_TYPE_ERROR);
        });
    }
  }

  btngrvUnAssign_Click() {
    if (this.nciAssigner == false) {
      this.helper.notify(`You do not have Unassign authority on your profile. Your authority only allows you to assign orders to yourself.`, Global.NOTIFY_TYPE_WARNING);
    }
    else if (this.selectedWFMOrders.length == 0) {
      this.helper.notify(`Please select atleast one item from the list to perform the assign/unassign operation.`, Global.NOTIFY_TYPE_WARNING);
    }
    else {
      let orderModel = {
        UserId: this.userId,
        UserToAssign: '',
        WFMOrders: this.selectedWFMOrders,
        Profile: 'NCI',
        SearchCriteria: this.searchCriteria
      }

      this.wfmOrderService.unAssignClick(orderModel).subscribe(data => {
        if (data != null) {
          this.wfmOrdersList = data.table;
          this.assignedUserList = data.table1;
          this.form.get('userToAssign').reset();
          this.helper.notify("Successfully unassign user.", Global.NOTIFY_TYPE_SUCCESS);
        }
      }, error => {
          this.helper.notify("Failed to unassign user.", Global.NOTIFY_TYPE_ERROR);
        });
    }
  }

  btngrvTransfer_Click() {
    if (this.selectedWFMOrders.length == 0) {
      this.helper.notify(`Please select atleast one item from the list to perform the assign/unassign operation.`, Global.NOTIFY_TYPE_WARNING);
    }
    else {
      let orderModel = {
        UserId: this.userId,
        UserToAssign: this.selectedUserToAssign,
        WFMOrders: this.selectedWFMOrders,
        NCIAssigner: this.nciAssigner,
        Profile: 'NCI',
        SearchCriteria: this.searchCriteria
      }

      this.wfmOrderService.transferClick(orderModel).subscribe(data => {
        if (data != null) {
          this.wfmOrdersList = data.table;
          this.assignedUserList = data.table1;
          this.form.get('userToAssign').reset();
          this.helper.notify("Successfully transfer order.", Global.NOTIFY_TYPE_SUCCESS);
        }
      }, error => {
          this.helper.notify("Failed to transfer order.", Global.NOTIFY_TYPE_ERROR);
        });
    }
  }

  btngrvSearch_Click() {
    this.loadGrid(1);
  }

  btngrvClear_Click() {
    this.form.get('userToAssign').reset();
    this.form.get('m5Filter').reset();
    this.form.get('assignedUser').reset();
    this.loadGrid(0);
  }

  loadGrid(searchType: number) {
    let sFilter: string = '';
    if (searchType == 0) {
      if (this.userProfileId == 14 || 28 || 112) {
        this.nciUser = true;
        if (this.userProfileId == 14)
          this.searchCriteria = "REGION = 'AMNCI' |REGION = 'AMNCI' ";
        else if (this.userProfileId == 28)
          this.searchCriteria = "REGION = 'ANCI' |REGION = 'ANCI' ";
        else if (this.userProfileId == 112)
          this.searchCriteria = "REGION = 'ENCI' |REGION = 'ENCI' ";
      }
      else {
        this.searchCriteria = "";
      }
      sFilter = this.searchCriteria;
    }
    else {
      if (!this.helper.isEmpty(this.form.get('m5Filter').value)) {
        sFilter = " FTN LIKE '%" + this.form.get('m5Filter').value.trim() + "%'";
      }

      if (!this.helper.isEmpty(this.form.get('assignedUser').value)) {
        if (this.form.get('assignedUser').value != "--" && sFilter != "") {
          sFilter += " AND ASSIGNED_USER = '" + this.form.get('assignedUser').value + "'";
        }
        else if (this.form.get('assignedUser').value != "--") {
          sFilter += " ASSIGNED_USER = '" + this.form.get('assignedUser').value + "'";
        }
      }

      if (this.userProfileId == 14 || 28 || 112) {
        this.nciUser = true;
        if (this.userProfileId == 14)
          sFilter += "AND REGION = 'AMNCI' |REGION = 'AMNCI' ";
        else if (this.userProfileId == 28)
          sFilter += "AND REGION = 'ANCI' |REGION = 'ANCI' ";
        else if (this.userProfileId == 112)
          sFilter += "AND REGION = 'ENCI' |REGION = 'ENCI' ";
      }

      if (this.helper.isEmpty(this.form.get('m5Filter').value) && this.helper.isEmpty(this.form.get('assignedUser').value)) {
        sFilter = this.searchCriteria;  
      }
    }

    this.searchCriteria = sFilter;
    this.wfmOrderService.GetWFMData('', this.searchCriteria, 0, 'NCI').subscribe(data => {
      if (data != null) {
        this.wfmOrdersList = data.table;
        this.assignedUserList = data.table1;
      }
    });
  }

  selectionChangedHandler(data: any) {
    this.selectedWFMOrders = data.selectedRowsData;
  }

  onCellPrepared(e) {
    if (e.rowType == 'header') {
      e.cellElement.style.userSelect = 'all';
    }
  }

  contentReadyHandler(e) {
    if (!this.helper.isEmpty(this.dataGrid.searchPanel.text.trim())) {
      this.dataGrid.instance.searchByText(this.dataGrid.searchPanel.text.trim())
    }
  }
}
