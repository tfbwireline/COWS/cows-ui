import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { UserService } from '../../../services';
import { Helper } from '../../../shared';
import { Global, OrderStatus } from "../../../shared/global";
import { NgxSpinnerService } from 'ngx-spinner';
import { takeUntil } from 'rxjs/operators';
import { AdditionalCustomerChargeService } from '../../../services/additional-customer-charge.service';
import { SearchedFTNResultModel } from '../../../models/additional-charge.model'
import { zip } from 'rxjs';

@Component({
    selector: 'app-additional-customer-charge',
    templateUrl: './additional-customer-charge.component.html',
})

export class AdditionalCustomerChargeComponent implements OnInit, OnDestroy {

    public title: string;
    public orderId: number = 0;
    public orderIdSub: Subject<number> = new Subject();
    public viewType: string = '';

    public searchedRecord: any = [];
    public searchedRecordSub: Subject<any> = new Subject();
    public pageForm: FormGroup;
    
    public isACOVisible : boolean = false;
    public isACTVisible : boolean = false;
    public parentPage : string = 'ACC';
    private onDestory = new Subject<void>();

    get ftn() { return this.pageForm.get("ftn") }

    constructor(private router: Router, private fb: FormBuilder, private userSrvc: UserService, 
        private spinner: NgxSpinnerService, private helper: Helper, 
        private additionalCustomerChargeService: AdditionalCustomerChargeService ) {}

    ngOnInit() {

        const isVendor = window.location.href.includes('vendor');
        this.title = isVendor ? 'Additional Vendor Charge' : 'Additional Customer Charge'
        this.pageForm = this.fb.group({
            ftn: new FormControl('',[Validators.required])
        })

        this.subscriptions();
        let user = zip(
            this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "NCI"),
            this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "SUPPORT")
        )

        user.pipe(takeUntil(this.onDestory)).subscribe(data => {
            if(data[0]) {
                this.viewType = 'NCI';
            }
            else if(data[1]) {
                this.viewType = 'SaleSupport';
            } else {
                this.router.navigateByUrl('/sorry');
            }
        })     
    }

    public search() {

        this.orderId = 0;
        this.searchedRecordSub.next([]);
        const ftn = this.pageForm.controls['ftn'].value;

        if(ftn === '') {
            return;
        }

        this.spinner.show();
        this.additionalCustomerChargeService.search(ftn).pipe(takeUntil(this.onDestory)).subscribe((data : Array<SearchedFTNResultModel>) => {
            this.spinner.hide();
            if(data.length > 0) {
                for(let i = 0; i < data.length; i++) {
                    const isLocked = (data[i].ol != null && data[i].ol !=  '') ? true : false;
                    data[i].isLocked = isLocked;
                    data[i].ftn = isLocked ? `${data[i].ftn} - locked` : data[i].ftn;
                }
                this.searchedRecordSub.next(data);
            } else {
                this.helper.notify('No orders found for FTN.', Global.NOTIFY_TYPE_ERROR)
            }
        })
    }

    public getAdditionalCostRecord(event: any) {
        const data :  SearchedFTNResultModel = event['data'];
    
        this.orderIdSub.next(data.ordrId);
        if(data.ordrType.toUpperCase() === 'BILLING ONLY') {
            this.helper.notify('Cannot enter additional charges on Billing orders.', Global.NOTIFY_TYPE_ERROR);
            return;
        }
        if(data.prodType === 'IPL E2E' &&  this.viewType === 'NCI') {
            this.isACOVisible = true;
        } else {
            this.isACOVisible = false;
        }
        this.isACTVisible = true;
        
    }

    private subscriptions() {

        this.orderIdSub.subscribe(data => {
            this.orderId = data;
        });

        this.searchedRecordSub.subscribe(data => {
            this.searchedRecord = data;
        });
    }

    ngOnDestroy() {
        this.onDestory.next();
        this.onDestory.complete();
    }
}
