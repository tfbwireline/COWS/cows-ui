import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from 'rxjs';
import 'rxjs/add/operator/map'
import {
  AdvancedSearchEvent, AdvancedSearchOrder, AdvancedSearchRedesign, EventType,
  MDSMACActivity, MDSNetworkActivityType, SearchCode
} from '../../models';
import {
  EventStatusService, EventTypeService, FedlineOrderTypeService, GroupService, MDSActivityTypeService,
  MDSFastTrackTypeService, MDSMACActivityService, OrderSubTypeService, OrderTypeService,
  PlatformService, ProductTypeService, RedesignCategoryService, RedesignTypeService, RegionService,
  SearchService, StatusService, MenuService, UserService, MDSNetworkActivityTypeService, OrderService, OrderLockService, EventLockService, RedesignRecLockService
} from '../../services';
import { Helper } from '../../shared';
import { SearchTypes } from '../search/search.criteria';

interface IBreadcrumb {
  label: string;
  params: Params;
  url: string;
  isLink: boolean;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  @ViewChild('eventTemplate', { static: true })
  eventTemplate: TemplateRef<any> | null = null;
  @ViewChild('orderTemplate', { static: true })
  orderTemplate: TemplateRef<any> | null = null;
  @ViewChild('redesignTemplate', { static: true })
  redesignTemplate: TemplateRef<any> | null = null;

  searchCode = new SearchCode();

  eventList = null;
  orderList = null;
  redesignList = null;
  public msg: string;
  eventListLen = 0;
  orderListLen = 0;
  redesignListLen = 0;
  form: FormGroup
  types: SearchTypes[] = []
  status: SearchTypes[] = []
  template: TemplateRef<any> | null = null;
  temp: IBreadcrumb[]

  eventTypes: EventType[]
  eventStatus: any[]
  mdsFastTrackTypes: any[]
  frbConfigurationTypes: SearchTypes[]
  frbActivityTypes: any[]
  mdsActivityTypes: any[]
  mdsMacActivity: MDSMACActivity[]
  mdsNtwrkActivity: MDSNetworkActivityType[]

  productTypes: any[]
  orderSubTypes: any[]
  orderTypes: any[]
  groups: any[]
  platforms: any[]
  regions: any[]

  redesignCategories: any[]
  redesignTypes: any[]
  redesignStatus: any[]

  orderLink: any;
  isTypeOptionChange: boolean = false;

  get type() { return this.getFormControlValue("criteria.type"); }

  constructor(
    private router: Router, private helper: Helper, private spinner: NgxSpinnerService,
    public menuService: MenuService, private searchService: SearchService, private actRoute: ActivatedRoute,
    private eventTypeService: EventTypeService, private eventStatusService: EventStatusService,
    private mdsFastTrackTypeService: MDSFastTrackTypeService,
    private fedlineOrderTypeService: FedlineOrderTypeService,
    private mdsActivityTypeService: MDSActivityTypeService,
    private mdsMacActivityService: MDSMACActivityService,
    private mdsNtwrkActySrvc: MDSNetworkActivityTypeService,
    private productTypeService: ProductTypeService, private orderSubTypeService: OrderSubTypeService,
    private orderTypeService: OrderTypeService, private groupService: GroupService,
    private platformService: PlatformService, private regionService: RegionService,
    private redesignCategoryService: RedesignCategoryService,
    private redesignTypeService: RedesignTypeService,
    private statusService: StatusService, public userService: UserService,
    private orderService: OrderService, private orderLockSrvc: OrderLockService,
    private eventLockSrvc: EventLockService, private redesignLockSrvc: RedesignRecLockService
  ) {
    this.temp = this.menuService.breadcrumbs;
  }

  ngOnInit() {
    this.spinner.show();
    this.form = new FormGroup({
      criteria: new FormGroup({
        type: new FormControl(),
        status: new FormControl()
      }),
      field: new FormGroup({ })
    });

    this.loadDropDownData().then(
      () => {
        if (this.userService.loggedInUser.profiles == null
          || this.userService.loggedInUser.profiles.length <= 0) {
          this.router.navigate(['/authorize']);
        }
        else if (this.userService.searchCode.isCandUser) {
          this.router.navigate(['/order/cpe-receipts']);
        }
        this.reset();
      }).then(() => { this.spinner.hide(); });
  }

  loadDropDownData() {
    return new Promise(resolve => {
      this.status = [
        new SearchTypes("All", null, []),
        new SearchTypes("Pending", 0, []),
        new SearchTypes("Completed", 1, [])
      ]

      let data = zip(
        // Event Dropdowns
        this.eventTypeService.getEventTypes(),
        this.eventStatusService.get(),
        this.mdsFastTrackTypeService.get(),
        this.fedlineOrderTypeService.get(),
        this.mdsActivityTypeService.get(),
        this.mdsMacActivityService.get(),
        this.mdsNtwrkActySrvc.get(),

        // Order Dropdowns
        this.productTypeService.get(),
        this.orderSubTypeService.get(),
        this.orderTypeService.get(),
        this.groupService.get(),
        this.platformService.get(),
        this.regionService.get(),

        // Redesign Dropdows
        this.redesignCategoryService.get(),
        this.redesignTypeService.get(),
        this.statusService.get()
      );

      data.toPromise().then(res => {
        // Event Dropdowns
        this.eventTypes = res[0]
        this.eventStatus = res[1]
        this.mdsFastTrackTypes = res[2]
        this.frbConfigurationTypes = [
          new SearchTypes("1 - Stand Alone", 1, []),
          new SearchTypes("2 - Single Interface", 2, []),
          new SearchTypes("3 - Dual Interface", 2, [])
        ]
        let frb: SearchTypes[] = []
        for (let val of this.helper.getDistinct(res[3].map(a => a.prntOrdrTypeDes))) {
          frb.push(new SearchTypes(val, val, null))
        }
        this.frbActivityTypes = frb
        this.mdsActivityTypes = res[4]
        this.mdsMacActivity = res[5]
        this.mdsNtwrkActivity = res[6]

        // Order Dropdowns
        this.productTypes = res[7]
        this.orderSubTypes = res[8]
        this.orderTypes = res[9]
        this.groups = res[10]
        this.platforms = res[11]
        this.regions = res[12]

        // Redesign Dropwdowns
        this.redesignCategories = res[13]
        let rdt: SearchTypes[] = []
        for (let val of this.helper.getDistinct(res[14].map(a => a.redsgnTypeNme))) {
          rdt.push(new SearchTypes(val, val, null))
        }
        this.redesignTypes = rdt
        this.redesignStatus = res[15].filter(a => a.stusTypeId == "REDSGN")

        resolve(true);
      });
    });
  }

  setForm() {
    this.template = null;

    let type = this.getFormControlValue("criteria.type");
    let status = this.getFormControlValue("criteria.status");

    if (this.form.contains("field")) {
      this.form.removeControl("field")
    }

    // Updated by Sarah Sandoval [20200511] - Load all dropdown on init
    this.eventList = null
    this.orderList = null
    this.redesignList = null

    //console.log('setForm: Type = ' + type + '; Status = ' + status)
    if (type == 1) {
      this.form.get('criteria.status').setValue(null);
      this.form.addControl("field", new FormGroup({
        m5_no: new FormControl(),
        assignedName: new FormControl(),
        requestorName: new FormControl(),
        eventId: new FormControl(),
        eventType: new FormControl(),
        eventStatus: new FormControl(),
        //fmsCircuit: new FormControl(),
        nua: new FormControl(),
        h5_h6: new FormControl(),
        ftActivityType: new FormControl(),
        customer: new FormControl(),
        deviceName: new FormControl(),
        deviceSerialNo: new FormControl(),
        frbRequestId: new FormControl(),
        frbOrgId: new FormControl(),
        frbConfigurationType: new FormControl(),
        apptStartDate: new FormControl(),
        apptEndDate: new FormControl(),
        frbActivityType: new FormControl(),
        redesignNo: new FormControl(),
        mdsActivityType: new FormControl(),
        mdsMacType: new FormControl(),
        mdsNtwrkActyType: new FormControl()
      }));
      this.template = this.eventTemplate;
    }
    else if (type == 2) {
      this.form.addControl("field", new FormGroup({
        m5_no: new FormControl(),
        isRelatedFtn: new FormControl(),
        name: new FormControl(),
        ccd: new FormControl(),
        privateLine: new FormControl(),
        h1: new FormControl(),
        vendorName: new FormControl(),
        prsQuote: new FormControl(),
        vendorCircuit: new FormControl(),
        productType: new FormControl(),
        soi: new FormControl(),
        h5_h6: new FormControl(),
        orderSubType: new FormControl(),
        orderType: new FormControl(),
        workGroup: new FormControl(),
        nua: new FormControl(),
        platformType: new FormControl(),
        region: new FormControl(),
        parentM5_no: new FormControl(),
        purchaseOrderNumber: new FormControl(),
        atlasWorkOrderNumber: new FormControl(),
        peopleSoftRequisitionNumber: new FormControl(),
        preQualId: new FormControl(),
        deviceId: new FormControl(),
        siteId: new FormControl()
      }));
      this.template = this.orderTemplate;
    }
    else {
      this.form.addControl("field", new FormGroup({
        redesignNo: new FormControl(),
        customer: new FormControl(),
        h1: new FormControl(),
        redesignCategory: new FormControl(),
        redesignType: new FormControl(),
        redesignStatus: new FormControl(),
        odieDeviceName: new FormControl(),
        nteAssigned: new FormControl(),
        pmAssigned: new FormControl(),
        eventId: new FormControl(),
        mssSeAssigned: new FormControl(),
        neAssigned: new FormControl(),
        submittedDate: new FormControl(),
        slaDueDate: new FormControl(),
        expirationDate: new FormControl()
      }));
      this.template = this.redesignTemplate;
    }
  }

  onSubmit() {
    //console.log(this.form)
    //console.log('criteria.type' + this.form.get('criteria.type').value)
    if (this.form.get('criteria.type').value == 1) {
      let event = new AdvancedSearchEvent()
      event.type = this.form.value.criteria.type
      event.status = this.form.value.criteria.status
      event.m5_no = this.form.value.field.m5_no
      event.assignedName = this.form.value.field.assignedName
      event.requestorName = this.form.value.field.requestorName
      event.eventId = this.form.value.field.eventId
      event.eventType = this.form.value.field.eventType
      event.eventStatus = this.form.value.field.eventStatus
      //event.fmsCircuit = this.form.value.field.fmsCircuit
      event.nua = this.form.value.field.nua
      event.h5_h6 = this.form.value.field.h5_h6
      event.ftActivityType = this.form.value.field.ftActivityType
      event.customer = this.form.value.field.customer
      event.deviceName = this.form.value.field.deviceName
      event.deviceSerialNo = this.form.value.field.deviceSerialNo
      event.frbRequestId = this.form.value.field.frbRequestId
      event.frbOrgId = this.form.value.field.frbOrgId
      event.frbConfigurationType = this.form.value.field.frbConfigurationType
      event.apptStartDate = this.form.value.field.apptStartDate
      event.apptEndDate = this.form.value.field.apptEndDate
      event.frbActivityType = this.form.value.field.frbActivityType
      event.redesignNo = this.form.value.field.redesignNo
      event.mdsActivityType = this.form.value.field.mdsActivityType
      event.mdsMacType = this.form.value.field.mdsMacType
      let ntwrkActyTypes = this.form.value.field.mdsNtwrkActyType
      event.mdsNtwkActyType = this.helper.isEmpty(ntwrkActyTypes) ? null : ntwrkActyTypes.join()

      let param = this.getEventParam(event)

      if (this.menuService.breadcrumbs.length > 1) {
        this.menuService.breadcrumbs.pop()
      }

      this.menuService.breadcrumbs.push({
        label: `Event - ${param}`,
        params: {},
        url: "",
        isLink: false
      })

      this.spinner.show();
      //console.log(event)
      this.searchService.getEvent(event).toPromise().then(
        res => {
          this.eventList = res
          this.eventListLen = res.length;
        },
        error => { }).then(
          () => {
            this.spinner.hide();
            if (this.eventList.length == 0) {
              this.eventListLen = this.eventList.length;
              this.msg = "No Records found for your Search Criteria, Please refine your Search";
            }
          }
        );
    }
    else if (this.form.get('criteria.type').value == 2) {
      let order = new AdvancedSearchOrder()
      order.type = this.form.value.criteria.type
      order.status = this.form.value.criteria.status == 0 ? null : this.form.value.criteria.status
      order.m5_no = this.form.value.field.m5_no
      order.isRelatedFtn = this.form.value.field.isRelatedFtn
      order.name = this.form.value.field.name
      order.ccd = this.form.value.field.ccd
      order.privateLine = this.form.value.field.privateLine
      order.h1 = this.form.value.field.h1
      order.vendorName = this.form.value.field.vendorName
      order.prsQuote = this.form.value.field.prsQuote
      order.vendorCircuit = this.form.value.field.vendorCircuit
      order.productType = this.form.value.field.productType
      order.soi = this.form.value.field.soi
      order.h5_h6 = this.form.value.field.h5_h6
      order.orderSubType = this.form.value.field.orderSubType
      order.orderType = this.form.value.field.orderType
      order.workGroup = this.form.value.field.workGroup
      order.nua = this.form.value.field.nua
      order.platformType = this.form.value.field.platformType
      order.region = this.form.value.field.region
      order.parentM5_no = this.form.value.field.parentM5_no
      order.purchaseOrderNumber = this.form.value.field.purchaseOrderNumber
      order.atlasWorkOrderNumber = this.form.value.field.atlasWorkOrderNumber
      order.preQualId = this.form.value.field.preQualId
      order.deviceId = this.form.value.field.deviceId
      order.siteId = this.form.value.field.siteId
      order.peopleSoftRequisitionNumber = this.form.value.field.peopleSoftRequisitionNumber;

      let param = this.getOrderParam(order)

      if (this.menuService.breadcrumbs.length > 1) {
        this.menuService.breadcrumbs.pop()
      }

      this.menuService.breadcrumbs.push({
        label: `Order - ${param}`,
        params: {},
        url: "",
        isLink: false
      })

      this.spinner.show();
      this.searchService.getOrder(order).toPromise().then(
        res => {
          this.orderList = res
          this.orderListLen = res.length;
          //console.log(res)
        },
        error => { }).then(() => {
          this.spinner.hide();
          if (this.orderList.length == 0) {
            this.orderListLen = this.orderList.length;
            this.msg = "No Records found for your Search Criteria, Please refine your Search";
          }
        });
    }
    else if (this.form.get('criteria.type').value == 3) {
      let redesign = new AdvancedSearchRedesign()
      redesign.type = this.form.value.criteria.type
      redesign.status = this.form.value.criteria.status
      redesign.redesignNo = this.form.value.field.redesignNo
      redesign.customer = this.form.value.field.customer
      redesign.h1 = this.form.value.field.h1
      redesign.redesignCategory = this.form.value.field.redesignCategory
      redesign.redesignType = this.form.value.field.redesignType
      redesign.redesignStatus = this.form.value.field.redesignStatus
      redesign.odieDeviceName = this.form.value.field.odieDeviceName
      redesign.nteAssigned = this.form.value.field.nteAssigned
      redesign.pmAssigned = this.form.value.field.pmAssigned
      redesign.eventId = this.form.value.field.eventId
      redesign.mssSeAssigned = this.form.value.field.mssSeAssigned
      redesign.neAssigned = this.form.value.field.neAssigned
      redesign.submittedDate = this.form.value.field.submittedDate
      redesign.slaDueDate = this.form.value.field.slaDueDate
      redesign.expirationDate = this.form.value.field.expirationDate

      let param = ""

      if (this.menuService.breadcrumbs.length > 1) {
        this.menuService.breadcrumbs.pop()
      }

      this.menuService.breadcrumbs.push({
        label: `Redesign - ${param}`,
        params: {},
        url: "",
        isLink: false
      })

      this.spinner.show();
      this.searchService.getRedesign(redesign).toPromise().then(
        res => {
          this.redesignList = res
          this.redesignListLen = res.length;
          //console.log(res)
        },
        error => { }).then(() => {
          this.spinner.hide();
          if (this.redesignList.length == 0) {
            this.redesignListLen = this.redesignList.length;
            this.msg = "No Records found for your Search Criteria, Please refine your Search";
          }
        });
    }
  }

  setType(value: number) {
    this.setFormControlValue("criteria.type", value);
    this.setForm();
  }

  setStatus(value: number) {
    this.setFormControlValue("criteria.status", value);
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  reset() {
    this.setStatus(0);
    this.setType(this.userService.searchCode.isOrderUser ? 2 : 1);
  }

  getEventParam(obj: AdvancedSearchEvent) {
    if (!this.helper.isEmpty(obj.m5_no))
      return obj.m5_no;
    else if (!this.helper.isEmpty(obj.assignedName))
      return obj.assignedName;
    else if (!this.helper.isEmpty(obj.requestorName))
      return obj.requestorName;
    else if (!this.helper.isEmpty(obj.eventId))
      return obj.eventId;
    else if (!this.helper.isEmpty(obj.eventType))
      return obj.eventType;
    else if (!this.helper.isEmpty(obj.eventStatus))
      return obj.eventStatus;
    // else if (!this.helper.isEmpty(obj.fmsCircuit))
    //   return obj.fmsCircuit;
    else if (!this.helper.isEmpty(obj.nua))
      return obj.nua;
    else if (!this.helper.isEmpty(obj.h5_h6))
      return obj.h5_h6;
    else if (!this.helper.isEmpty(obj.ftActivityType))
      return obj.ftActivityType;
    else if (!this.helper.isEmpty(obj.customer))
      return obj.customer;
    else if (!this.helper.isEmpty(obj.deviceName))
      return obj.deviceName;
    else if (!this.helper.isEmpty(obj.deviceSerialNo))
      return obj.deviceSerialNo;
    else if (!this.helper.isEmpty(obj.frbRequestId))
      return obj.frbRequestId;
    else if (!this.helper.isEmpty(obj.frbOrgId))
      return obj.frbOrgId;
    else if (!this.helper.isEmpty(obj.frbConfigurationType))
      return obj.frbConfigurationType;
    else if (!this.helper.isEmpty(obj.apptStartDate))
      return obj.apptStartDate;
    else if (!this.helper.isEmpty(obj.apptEndDate))
      return obj.apptEndDate;
    else if (!this.helper.isEmpty(obj.frbActivityType))
      return obj.frbActivityType;
    else if (!this.helper.isEmpty(obj.redesignNo))
      return obj.redesignNo;
    else if (!this.helper.isEmpty(obj.mdsActivityType))
      return obj.mdsActivityType;
    else if (!this.helper.isEmpty(obj.mdsMacType))
      return obj.mdsMacType;
    else if (!this.helper.isEmpty(obj.mdsNtwkActyType))
      return obj.mdsNtwkActyType;
    else
      return "";
  }

  selectionChangedEventHandler(event) {
    let param = {
      eventId: event[0].eventID,
      eventType: event[0].eventType
    }

    let eventUrl = this.searchService.getEventUrl(param)

    if (!this.helper.isEmpty(eventUrl)) {
      this.router.navigate([eventUrl.link]);
    }
  }

  goToEvent(event) {
    let param = {
      eventId: event.eventID,
      eventType: event.eventType
    }

    let eventUrl = this.searchService.getEventUrl(param)

    if (!this.helper.isEmpty(eventUrl)) {
      this.router.navigate([eventUrl.link]);
    }
  }

  onRowEventPrepared(e) {
    if (e.rowType == 'data') {
      this.eventLockSrvc.checkLock(e.key.eventID).toPromise().then(res => {
        if (res != null) {
          e.rowElement.className = e.rowElement.className + ' checked-in-row';
          e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
          e.rowElement.title = 'This event is locked by ' + res.lockByFullName;
        }
      });
    }
  }

  onToolbarEventPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExportingEvent(e) {
    e.fileName = 'Event_' + this.helper.formatDate(new Date());
  }

  getOrderParam(obj) {
    if (!this.helper.isEmpty(obj.m5_no))
      return obj.m5_no;
    else if (!this.helper.isEmpty(obj.name))
      return obj.name;
    else if (!this.helper.isEmpty(obj.ccd))
      return obj.ccd;
    else if (!this.helper.isEmpty(obj.privateLine))
      return obj.privateLine;
    else if (!this.helper.isEmpty(obj.h1))
      return obj.h1;
    else if (!this.helper.isEmpty(obj.vendorName))
      return obj.vendorName;
    else if (!this.helper.isEmpty(obj.prsQuote))
      return obj.prsQuote;
    else if (!this.helper.isEmpty(obj.vendorCircuit))
      return obj.vendorCircuit;
    else if (!this.helper.isEmpty(obj.productType))
      return obj.productType;
    else if (!this.helper.isEmpty(obj.soi))
      return obj.soi;
    else if (!this.helper.isEmpty(obj.h5_h6))
      return obj.h5_h6;
    else if (!this.helper.isEmpty(obj.orderSubType))
      return obj.orderSubType;
    else if (!this.helper.isEmpty(obj.orderType))
      return obj.orderType;
    else if (!this.helper.isEmpty(obj.workGroup))
      return obj.workGroup;
    else if (!this.helper.isEmpty(obj.nua))
      return obj.nua;
    else if (!this.helper.isEmpty(obj.platformType))
      return obj.platformType;
    else if (!this.helper.isEmpty(obj.region))
      return obj.region;
    else if (!this.helper.isEmpty(obj.parentM5_no))
      return obj.parentM5_no;
    else if (!this.helper.isEmpty(obj.purchaseOrderNumber))
      return obj.purchaseOrderNumber;
    else if (!this.helper.isEmpty(obj.atlasWorkOrderNumber))
      return obj.atlasWorkOrderNumber;
    else if (!this.helper.isEmpty(obj.preQualId))
      return obj.preQualId;
    else if (!this.helper.isEmpty(obj.deviceId))
      return obj.deviceId;
    else if (!this.helper.isEmpty(obj.siteId))
      return obj.siteId;
    else
      return "";
  }

  selectionChangedOrderHandler(M5) {
    this.spinner.show();
    let param = {
      orderId: M5[0].ordR_ID,
      wg: M5[0].wgid,
      wgName: M5[0].workGroup,
      orderCategoryId: M5[0].caT_ID,
      taskId: M5[0].tasK_ID,
      purchaseOrderNo: M5[0].prcH_ORDR_NBR
    }

    // 
    let orderUrl = this.searchService.getOrderUrl(param)

    if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
      if (this.helper.isEmpty(orderUrl.param)) {
        this.router.navigate([orderUrl.link]);
      }
      else {
        this.router.navigate([orderUrl.link], orderUrl.param);
      }
    }
    else {
      this.spinner.show();
    }
  }

  goToOrder(M5) {
    //console.log(M5)
    this.spinner.show();
    let orderId = M5.ordR_ID;
    let wgId = M5.wgid;
    let isSystemWgId = false;

    let param = {
      orderId: orderId,
      wg: M5.wgid,
      wgName: M5.workGroup,
      orderCategoryId: M5.caT_ID,
      taskId: M5.tasK_ID,
      purchaseOrderNo: M5.prcH_ORDR_NBR,
      systemOriginalWorkgroup: 98
    }

    if (wgId == 0 || wgId == 98) {
      isSystemWgId = true;
      this.orderService.GetOrderPPRT(orderId).toPromise().then(data => {
        param.systemOriginalWorkgroup = param.wg = this.searchService.determineWGIDForSystemOrCompletedOrders(data, this.userService.loggedInUser.profiles);
        let orderUrl = this.searchService.getOrderUrl(param)

        if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
          if (this.helper.isEmpty(orderUrl.param)) {
            this.router.navigate([orderUrl.link]);
          }
          else {
            this.router.navigate([orderUrl.link], orderUrl.param);
          }
        }
        else {
          this.spinner.show();
        }
      })
    } else {
      sessionStorage.setItem('deviceId', M5.deviceID);
      let orderUrl = this.searchService.getOrderUrl(param)

      if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
        if (this.helper.isEmpty(orderUrl.param)) {
          this.router.navigate([orderUrl.link]);
        }
        else {
          this.router.navigate([orderUrl.link], orderUrl.param);
        }
      }
      else {
        this.spinner.show();
      }
    }
  }
  //goToOrder(M5) {
  //  //console.log(M5)
  //  this.spinner.show();
  //  let param = {
  //    orderId: M5.ordR_ID,
  //    wg: M5.wgid,
  //    wgName: M5.workGroup,
  //    orderCategoryId: M5.caT_ID,
  //    taskId: M5.tasK_ID,
  //    purchaseOrderNo: M5.prcH_ORDR_NBR
  //  }
  //  let orderUrl = this.searchService.getOrderUrl(param)

  //  if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
  //    if (this.helper.isEmpty(orderUrl.param)) {
  //      this.router.navigate([orderUrl.link]);
  //    }
  //    else {
  //      this.router.navigate([orderUrl.link], orderUrl.param);
  //    }
  //  }
  //  else {
  //    this.spinner.show();
  //  }
  //}

  onRowOrderPrepared(e) {
    if (e.rowType == 'data') {
      this.orderLockSrvc.checkLock(e.key.ordR_ID).toPromise().then(res => {
        if (res != null) {
          e.rowElement.className = e.rowElement.className + ' checked-in-row';
          e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
          e.rowElement.title = 'This order is locked by ' + res.lockByFullName;
        }
      });
    }
  }

  onToolbarOrderPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExportingOrder(e) {
    e.fileName = 'Order_' + this.helper.formatDate(new Date());
  }

  selectionChangedRedesignHandler(Redesign) {
    this.router.navigate(['redesign/update-redesign/' + Redesign[0].redesignID]);
  }

  goToRedesign(Redesign) {
    //console.log(Redesign)
    this.router.navigate(['redesign/update-redesign/' + Redesign.redesignID]);
  }

  onToolbarRedesignPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExportingRedesign(e) {
    e.fileName = 'Redesign_' + this.helper.formatDate(new Date());
  }

  onRowRedesignPrepared(e) {
    if (e.rowType == 'data') {
      this.redesignLockSrvc.checkLock(e.key.redesignID).toPromise().then(res => {
        if (res != null) {
          e.rowElement.className = e.rowElement.className + ' checked-in-row';
          e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
          e.rowElement.title = 'This redesign is locked by ' + res.lockByFullName;
        }
      });
    }
  }
}
