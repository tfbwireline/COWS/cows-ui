import { Routes, RouterModule } from "@angular/router";
import {
  HomeComponent,
  UserComponent,
  NRMBPMInterfaceViewComponent,
  PeopleSoftInterfaceViewComponent,
  DedicatedCustomerComponent,
  EventTypeComponent,
  MDSMACActivityComponent,
  ServiceDeliveryComponent,
  ServiceAssuranceSiteSupportComponent,
  AdvancedSearchComponent,
  SprintHolidayComponent,
  SpecialProjectComponent,
  TelcoComponent,
  MDS3rdPartyServiceComponent,
  MDS3rdPartyVendorComponent,
  ServiceTierComponent,
  EnhncSrvcComponent,
  XnciMsSlaComponent,
  CurrencyListComponent,
  UnlockItemsComponent,
  WorldHolidayComponent,
  CancelOrdrComponent,
  SetPreferenceComponent,
  FastTrackAvailabilityComponent,
  VendorManagementComponent,
  EventIntervalComponent,
  VendorModelComponent,
  XnciRegionComponent,
  CCDBypassComponent,
  CcdChangeComponent,
  CurrencyFileComponent,
  CustomerUserProfileComponent,
  UcaasComponent,
  QualificationComponent,
  QualificationFormComponent,
  QualificationViewComponent,
  FedlineAvailabilityComponent,
  AccessDeliveryComponent,
  AccessDeliveryFormComponent,
  SiptComponent,
  SiptFormComponent,
  ManageUsersComponent,
  FedlineComponent,
  MdsComponent,
  MplsComponent,
  MplsFormComponent,
  NgvnComponent,
  SprintLinkComponent,
  SprintLinkFormComponent,
  CreateViewComponent,
  NgvnFormComponent,
  MdsFormComponent,
  FedlineFormComponent,
  UcaasFormComponent,
  WfCalendarComponent,
  SprintLinkCalendarComponent,
  MssViewEngagementsComponent,
  NccoOrderFormComponent,
  NccoOrderComponent,
  MssProductTypesComponent,
  ViewRedesignComponent,
  NewRedesignComponent,
  CustomerBypassComponent,
  NgvnCalendarComponent,
  MssNewEngagementComponent,
  SiptCalendarComponent,
  UCaaSCalendarComponent,
  MplsVasCalendarComponent,
  MplsCalendarComponent,
  MdsftCalendarComponent,
  MdsCalendarComponent,
  NetworkIntlCalendarComponent,
  ADBroadbandCalendarComponent,
  ADNarrowbandCalendarComponent,
  ADGovernmentCalendarComponent,
  ADInternationalCalendarComponent,
  ADTMTCalendarComponent,
  ESCalendarComponent,
  CpeClliComponent,
  CpeClliFormComponent,
  CptAssignmentComponent,
  CptFormComponent,
  CptReportComponent,
  CptReturnComponent,
  CptSearchComponent,
  CptViewComponent,
  FedlineCalendarComponent,
  ADBroadbandListViewComponent,
  ADGovernmentListViewComponent,
  ADInternationalListViewComponent,
  ADNarrowbandListViewComponent,
  ADTMTListViewComponent,
  ESListViewComponent,
  FedlineListViewComponent,
  MdsListViewComponent,
  MdsftListViewComponent,
  NetworkIntlListViewComponent,
  MplsListViewComponent,
  MplsVasListViewComponent,
  NgvnListViewComponent,
  SiptListViewComponent,
  SprintLinkListViewComponent,
  UCaaSListViewComponent,
  WfListViewComponent,
  LookupOpenSlotComponent,
  LookupEventHistoryComponent,
  MssEmailTeamComponent,
  BillingDispatchComponent,
  H5FolderComponent,
  AdditionalCustomerChargeComponent,
  GenerateICSComponent,
  CompleteOrderEventComponent,
  CacheAdminComponent
} from ".";
import { AuthGuard } from "../shared/auth.guard";
import { AdminAuthGuard } from "../shared/admin-auth.guard";
import { AuthorizeComponent } from "./authorize/authorize.component";
import { CancelComponent } from "./cancel/cancel.component";
import { SorryComponent } from "./sorry/sorry.component";
import { EventAuthGuard } from "../shared/event-auth.guard";
import { EEventType } from "../shared/global";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { BillingDispatchDetailComponent } from "./billing-dispatch/billing-dispatch-detail/billing-dispatch-detail.component";
import { GomWfmAssignComponent } from "./tools/gom-wfm-assign/gom-wfm-assign.component";
import { XnciWfmAssignComponent } from "./tools/xnci-wfm-assign/xnci-wfm-assign.component";
import { UserResolver } from "../shared/user-resolver";
import { SearchResultComponent } from "./search/search-result.component";
import { CsgErrorPageComponent } from "./csg-error-page/csg-error-page.component";
import { DocDownloadComponent } from "./shared";
import { AdminSupportComponent } from "./support/admin-support.component";

export const routes: Routes = [
  // this route will be taken for both localhost:port/ or localhost:port/Home
  {
    path: "home",
    component: HomeComponent,
    data: { title: "Home" },
    canActivate: [AuthGuard],
    resolve: {
      userSearchCode: UserResolver
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "authorize",
    component: AuthorizeComponent,
    data: { title: "Authorize" }
  },
  {
    path: "cancel",
    component: CancelComponent,
    data: { title: "Not Authorized User" }
  },
  {
    path: "billing-dispatch",
    data: { title: "Billing Dispatch" },
    children: [
      {
        path: "",
        component: BillingDispatchComponent
      },
      {
        path: ":id",
        component: BillingDispatchDetailComponent
      }
    ]
  },
  {
    path: "SDE",
    data: { title: "MSS New" },
    children: [
      {
        path: "SDENew.aspx",
        component: MssNewEngagementComponent
      }
    ]
  },
  {
    path: "admin",
    canActivate: [AuthGuard],
    //canActivateChild: [AdminAuthGuard],
    data: { title: "Admin" },
    // All sub modules for Admin will be treated as children
    children: [
      // just to have a landing page when we clicked at "ADMIN" from navbar
      //{
      //  path: "",
      //  component: AdminComponent,
      //},
      //{
      //  path: "users",
      //  data: { title: "Users" },
      //  children: [{ path: "", component: UserComponent }]
      //},
      {
        path: "manage-users",
        data: { title: "Manage Users" },
        children: [{ path: "", component: ManageUsersComponent }]
      },
      {
        path: "preferences",
        data: { title: "Set Preferences" },
        children: [{ path: "", component: SetPreferenceComponent }]
      },
      {
        path: "nrmbpminterface-view",
        data: { title: "NRMBPM Interface" },
        children: [{ path: "", component: NRMBPMInterfaceViewComponent }]
      },
      {
        path: "people-soft-interface-view",
        data: { title: "PeopleSoft Interface" },
        children: [{ path: "", component: PeopleSoftInterfaceViewComponent }]
      },
      {
        path: "dedicated-customers",
        canActivate: [AdminAuthGuard],
        data: { title: "Dedicated Customers" },
        children: [{ path: "", component: DedicatedCustomerComponent }]
      },
      {
        path: "service-assurance-site-support",
        canActivate: [AdminAuthGuard],
        data: { title: "Service Assurance Site Support" },
        children: [
          { path: "", component: ServiceAssuranceSiteSupportComponent }
        ]
      },
      {
        path: "event-types",
        canActivate: [AdminAuthGuard],
        data: { title: "Event Types" },
        children: [{ path: "", component: EventTypeComponent }]
      },
      {
        path: "mds-mac-activities",
        canActivate: [AdminAuthGuard],
        data: { title: "MDS MAC Durations" },
        children: [{ path: "", component: MDSMACActivityComponent }]
      },
      {
        path: "service-delivery",
        canActivate: [AdminAuthGuard],
        data: { title: "Service Delivery Site Support" },
        children: [{ path: "", component: ServiceDeliveryComponent }]
      },
      {
        path: "special-projects",
        data: { title: "Special Projects" },
        children: [{ path: "", component: SpecialProjectComponent }]
      },
      {
        path: "sprint-holidays",
        canActivate: [AdminAuthGuard],
        data: { title: "T-Mobile Holidays" },
        children: [{ path: "", component: SprintHolidayComponent }]
      },
      {
        path: "telco",
        canActivate: [AdminAuthGuard],
        data: { title: "Telcos" },
        children: [{ path: "", component: TelcoComponent }]
      },
      {
        path: "service-tier",
        canActivate: [AdminAuthGuard],
        data: { title: "Service Tier" },
        children: [{ path: "", component: ServiceTierComponent }]
      },
      {
        path: "mds-3rdparty-vendors",
        canActivate: [AdminAuthGuard],
        data: { title: "MDS Third Party Vendors" },
        children: [{ path: "", component: MDS3rdPartyVendorComponent }]
      },
      {
        path: "mds-3rdparty-service-levels",
        canActivate: [AdminAuthGuard],
        data: { title: "MDS Third Party Service Levels" },
        children: [{ path: "", component: MDS3rdPartyServiceComponent }]
      },
      {
        path: "enhanced-services",
        canActivate: [AdminAuthGuard],
        data: { title: "Enhanced Services" },
        children: [{ path: "", component: EnhncSrvcComponent }]
      },
      {
        path: "set-slas",
        data: { title: "Set SLAs" },
        children: [{ path: "", component: XnciMsSlaComponent }]
      },
      {
        path: "currency-list",
        data: { title: "Currency List" },
        children: [{ path: "", component: CurrencyListComponent }]
      },
      {
        path: "unlock-items",
        data: { title: "Unlock items" },
        children: [{ path: "", component: UnlockItemsComponent }]
      },
      {
        path: "world-holidays",
        data: { title: "World Holidays" },
        children: [{ path: "", component: WorldHolidayComponent }]
      },
      {
        canActivate: [AuthGuard],
        canActivateChild: [AdminAuthGuard],
        path: "cancel-ordr",
        data: { title: "Cancel Order" },
        children: [{ path: "", component: CancelOrdrComponent }]
      },
      {
        path: "fast-track-availability",
        data: { title: "Fast Track Availability" },
        children: [{ path: "", component: FastTrackAvailabilityComponent }]
      },
      {
        path: "vendor-management",
        data: { title: "Vendor Management" },
        children: [{ path: "", component: VendorManagementComponent }]
      },
      {
        path: "event-interval",
        canActivate: [AdminAuthGuard],
        data: { title: "Event Intervals" },
        children: [{ path: "", component: EventIntervalComponent }]
      },
      {
        path: "vendor-model",
        data: { title: "Vendor Models" },
        children: [{ path: "", component: VendorModelComponent }]
      },
      {
        path: "xnci-region",
        data: { title: "xNCI Region Setup" },
        children: [{ path: "", component: XnciRegionComponent }]
      },
      {
        path: "fedline-availability",
        data: { title: "Fedline Availability" },
        children: [{ path: "", component: FedlineAvailabilityComponent }]
      },
      {
        path: "currency-file",
        data: { title: "Currency File" },
        children: [{ path: "", component: CurrencyFileComponent }]
      },
      {
        path: "mngd-ccd-bypass",
        data: { title: "Manage CCD Bypass Users" },
        children: [{ path: "", component: CCDBypassComponent }]
      },
      {
        path: "h5-customer-assgnmnt",
        data: { title: "H6 Customer to User Assignment" },
        children: [{ path: "", component: CustomerUserProfileComponent }]
      },
     {
        path: "cache-admin",
        data: { title: "Cache Admin" },
        children: [{ path: "", component: CacheAdminComponent }]
      },
      {
        path: "prod-support",
        component: AdminSupportComponent,
        data: { title: "Prod Support" }
      },
    // {
    //   path: "complete-order-event",
    //    data: { title: "complete order/event" },
    //    children: [{ path: "", component: completeordereventcomponent }]
    //  },
      {
        path: "qualifications",
        data: { title: "Set Qualifications" },
        children: [
          { path: "", component: QualificationComponent },
          {
            path: ":id",
            component: QualificationFormComponent,
            data: { title: "Update" }
          },
          {
            path: "add",
            component: QualificationFormComponent,
            data: { title: "Create" }
          },
          {
            path: "view/:id",
            component: QualificationViewComponent,
            data: { title: "View" }
          }
        ]
      },
      {
        path: "complete-order-event",
        data: { title: "Complete Order/Event" },
        children: [{ path: "", component: CompleteOrderEventComponent }]
      }
    ]
  },
  { 
    path: "event/GenerateICS", 
    component: GenerateICSComponent,
    data: { title: "Generate ICS" }
   },
  {
    path: "event",
    canActivate: [AuthGuard],
    //canActivateChild: [EventAuthGuard],
    data: { title: "Events" },
    children: [
      //{
      //  path: "",
      //  component: AdminComponent,
      //},
      {
        path: "access-delivery",
        //canActivate: [EventAuthGuard],
        data: { title: "Access Delivery", eventType: EEventType.AD },
        children: [
          { path: "", component: AccessDeliveryComponent },
          {
            path: "add",
            component: AccessDeliveryFormComponent,
            data: { title: "Create" }
          },
          {
            path: ":id",
            component: AccessDeliveryFormComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "sipt",
        //canActivate: [EventAuthGuard],
        data: { title: "SIPT", eventType: EEventType.SIPT },
        children: [
          { path: "", component: SiptComponent },
          {
            path: "add",
            component: SiptFormComponent,
            data: { title: "Create" }
          },
          {
            path: ":id",
            component: SiptFormComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "ucaas",
        //canActivate: [EventAuthGuard],
        data: { title: "UCaaS", eventType: EEventType.UCaaS },
        children: [
          { path: "", component: UcaasComponent },
          {
            path: "add",
            component: UcaasFormComponent,
            data: { title: "Create" }
          },
          {
            path: ":id",
            component: UcaasFormComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "fedline",
        data: { title: "Fedline" },
        children: [
          { path: "", component: FedlineComponent },
          {
            path: "add",
            component: FedlineFormComponent,
            data: { title: "Create" }
          },
          {
            path: ":id",
            component: FedlineFormComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "mds",
        //canActivate: [EventAuthGuard],
        data: { title: "IPSD", eventType: EEventType.MDS },
        children: [
          { path: "", component: MdsComponent },
          {
            path: "add",
            component: MdsFormComponent,
            data: { title: "Create" }
          },
          {
            path: ":id",
            component: MdsFormComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "mpls",
        //canActivate: [EventAuthGuard],
        data: { title: "MPLS", eventType: EEventType.MPLS },
        children: [
          { path: "", component: MplsComponent },
          {
            path: "add",
            component: MplsFormComponent,
            data: { title: "Create" }
          },
          {
            path: ":id",
            component: MplsFormComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "ngvn",
        //canActivate: [EventAuthGuard],
        data: { title: "NGVN", eventType: EEventType.NGVN },
        children: [
          { path: "", component: NgvnComponent },
          {
            path: "add",
            component: NgvnFormComponent,
            data: { title: "Create" }
          },
          {
            path: ":id",
            component: NgvnFormComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "sprint-link",
        //canActivate: [EventAuthGuard],
        data: { title: "SprintLink", eventType: EEventType.SprintLink },
        children: [
          { path: "", component: SprintLinkComponent },
          {
            path: "add",
            component: SprintLinkFormComponent,
            data: { title: "Create" }
          },
          {
            path: ":id",
            component: SprintLinkFormComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "create-view",
        data: { title: "Create View" },
        children: [
          { path: "", component: CreateViewComponent },
          {
            path: ":eventTypeId/:selectedViewId",
            component: CreateViewComponent,
            data: { title: "Update" }
          }
        ]
      }
    ]
  },
  {
    path: "order",
    data: { title: "Orders" },
    loadChildren: () =>
      import("./order/order.module").then(mod => mod.OrderModule)
  },
  {
    path: "reports",
    data: { title: "Reports" },
    loadChildren: () =>
      import("./reports/reports.module").then(rm => rm.ReportsModule)
  },
  {
    path: "calendars",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: { title: "Calendars" },
    children: [
      //{
      //  path: "",
      //  component: AdminComponent,
      //},
      {
        path: "wf-calendar",
        data: { title: "WF Calendar" },
        children: [
          { path: "", component: WfCalendarComponent },
          {
            path: "list",
            component: WfListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "sprint-link-calendar",
        data: { title: "SprintLink Calendar" },
        children: [
          { path: "", component: SprintLinkCalendarComponent },
          {
            path: "list",
            component: SprintLinkListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "ngvn-calendar",
        data: { title: "NGVN Calendar" },
        children: [
          { path: "", component: NgvnCalendarComponent },
          {
            path: "list",
            component: NgvnListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "sipt-calendar",
        data: { title: "SIPT Calendar" },
        children: [
          { path: "", component: SiptCalendarComponent },
          {
            path: "list",
            component: SiptListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "ucaas-calendar",
        data: { title: "UCaaS Calendar" },
        children: [
          { path: "", component: UCaaSCalendarComponent },
          {
            path: "list",
            component: UCaaSListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "mpls-vas-calendar",
        data: { title: "MPLS VAS Calendar" },
        children: [
          { path: "", component: MplsVasCalendarComponent },
          {
            path: "list",
            component: MplsVasListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "mpls-calendar",
        data: { title: "MPLS Calendar" },
        children: [
          { path: "", component: MplsCalendarComponent },
          {
            path: "list",
            component: MplsListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "mds-ft-calendar",
        data: { title: "IPSD FT Calendar" },
        children: [
          { path: "", component: MdsftCalendarComponent },
          {
            path: "list",
            component: MdsftListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "mds-calendar",
        data: { title: "IPSD Calendar" },
        children: [
          { path: "", component: MdsCalendarComponent },
          {
            path: "list",
            component: MdsListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "network-intl-calendar",
        data: { title: "Network Intl Calendar" },
        children: [
          { path: "", component: NetworkIntlCalendarComponent },
          {
            path: "list",
            component: NetworkIntlListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "ad-broadband-calendar",
        data: { title: "AD Broadband Calendar" },
        children: [
          { path: "", component: ADBroadbandCalendarComponent },
          {
            path: "list",
            component: ADBroadbandListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "ad-narrowband-calendar",
        data: { title: "AD Narrowband Calendar" },
        children: [
          { path: "", component: ADNarrowbandCalendarComponent },
          {
            path: "list",
            component: ADNarrowbandListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "ad-government-calendar",
        data: { title: "AD Government Calendar" },
        children: [
          { path: "", component: ADGovernmentCalendarComponent },
          {
            path: "list",
            component: ADGovernmentListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "ad-international-calendar",
        data: { title: "AD International Calendar" },
        children: [
          { path: "", component: ADInternationalCalendarComponent },
          {
            path: "list",
            component: ADInternationalListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "ad-tmt-calendar",
        data: { title: "AD TMT Calendar" },
        children: [
          { path: "", component: ADTMTCalendarComponent },
          {
            path: "list",
            component: ADTMTListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "es-calendar",
        data: { title: "ES Calendar" },
        children: [
          { path: "", component: ESCalendarComponent },
          {
            path: "list",
            component: ESListViewComponent,
            data: { title: "List View" }
          }
        ]
      },
      {
        path: "fedline-calendar",
        data: { title: "Fedline Calendar" },
        children: [
          { path: "", component: FedlineCalendarComponent },
          {
            path: "list",
            component: FedlineListViewComponent,
            data: { title: "List View" }
          }
        ]
      }
    ]
  },
  {
    path: "mss",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: { title: "MSS" },
    children: [
      {
        path: "mss-new-engagement",
        data: { title: "New Engagement" },
        children: [
          { path: "", component: MssNewEngagementComponent },
          {
            path: ":id",
            component: MssNewEngagementComponent,
            data: { title: "Update" }
          }
        ]
      },
      {
        path: "mss-view-engagements",
        data: { title: "View Engagements" },
        children: [{ path: "", component: MssViewEngagementsComponent }]
      },
      {
        path: "mss-email-team",
        component: MssEmailTeamComponent,
      },
      {
        path: "mss-product-types",
        data: { title: "MSS Product Types" },
        children: [{ path: "", component: MssProductTypesComponent }]
      }
    ]
  },
  // {
  //   path: "ncco/ncco-order",
  //   component: NccoOrderViewComponent,
  //   data: { title: "NCCO Order" },
  // },
  {
   path: "ncco",
  //  canActivate: [AuthGuard],
  //  canActivateChild: [AuthGuard],
   data: { title: "NCCO" },
   children: [
     {
       path: "ncco-order",
       data: { title: "NCCO Order" },
       children: [
         { path: "", component: NccoOrderComponent },
         {
           path: "add",
           component: NccoOrderFormComponent,
           data: { title: "Add" }
         },
         {
           path: "copy",
           component: NccoOrderFormComponent,
           data: { title: "Copy" }
         },
         {
           path: "cancel",
           component: NccoOrderFormComponent,
           data: { title: "Cancel" }
         },
         {
           path: ":id/:actionId",
           component: NccoOrderFormComponent,
           data: { title: "View/Cancel/Copy" }
         }
       ]
     },
     {
       path: "ncco-view-order",
       data: { title: "View" },
       children: [
         {
           path: "add",
           component: NccoOrderFormComponent,
           data: { title: "Add" }
         },
         {
           path: ":id",
           component: NccoOrderFormComponent,
           data: { title: "Cancel/Copy" }
         }
       ]
     }
   ]
  },
  {
    path: "h5-folder",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: H5FolderComponent,
    data: { title: "H6 Folder" },
  },
  {
    path: "vendor",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: { title: "Vendor" },
    loadChildren: () =>
      import("./vendor/vendor-module").then(rm => rm.VendorModule)
  },
  {
    path: "tools",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: { title: "Tools" },
    children: [
      {
        path: "cpe-clli",
        data: { title: "CPE CLLI" },
        children: [
          { path: "", component: CpeClliComponent, },
          { path: 'add', component: CpeClliFormComponent, data: { title: "" } },
          { path: ':id', component: CpeClliFormComponent, data: { title: "Update" } }
        ]
      },
      {
        path: "open-slots",
        data: { title: "Open Slots Lookup" },
        children: [{ path: "", component: LookupOpenSlotComponent }]
      },
      {
        path: "ccd-change",
        data: { title: "CCD Change" },
        children: [{ path: "", component: CcdChangeComponent }]
      },
      {
        path: "gom-wfm-assign",
        data: { title: "Intl CPE WFM Assign" },
        children: [{ path: "", component: GomWfmAssignComponent }]
      },
      {
        path: "xnci-wfm-assign",
        data: { title: "xNCI WFM Assign" },
        children: [{ path: "", component: XnciWfmAssignComponent }]
      },
      {
        path: "additional-customer-charge",
        data: { title: "Additional Customer Charge" },
        children: [{ path: "", component: AdditionalCustomerChargeComponent }]
      },
      {
        path: "additional-vendor-charge",
        data: { title: "Additional Vendor Charge" },
        children: [{ path: "", component: AdditionalCustomerChargeComponent }]
      },
      {
        path: "event-history",
        data: { title: "Event History Lookup" },
        //children: [{ path: "", component: LookupEventHistoryComponent }]
        children: [
          { path: "", component: LookupEventHistoryComponent },
          { path: ":id", component: LookupEventHistoryComponent }
        ]
      }
    ]
  },
  {
    path: "cpt",
    canActivate: [AuthGuard],
    //canActivateChild: [AuthGuard],
    data: { title: "CPT" },
    children: [
      {
        path: "cpt-assignment",
        data: { title: "Assignment" },
        component: CptAssignmentComponent
      },
      {
        path: "cpt-form",
        data: { title: "New Customer Request" },
        component: CptFormComponent
      },
      {
        path: "cpt-form/:id",
        data: { title: "Update Customer Request" },
        component: CptFormComponent
      },
      {
        path: "cpt-report",
        data: { title: "Report" },
        component: CptReportComponent
      },
      {
        path: "cpt-return",
        data: { title: "Returned to SDE" },
        component: CptReturnComponent
      },
      {
        path: "cpt-search",
        data: { title: "Search" },
        component: CptSearchComponent
      },
      {
        path: "cpt-view",
        data: { title: "Update Customer" },
        component: CptViewComponent
      }
    ]
  },
  {
    path: "redesign",
    data: { title: "Redesign" },
    children: [
      {
        path: "",
        component: ViewRedesignComponent,
      },
      {
        path: "new-redesign",
        component: NewRedesignComponent,
        data: { title: "Create Redesign" }
      },
      {
        //path: "redesign/new-redesign/:id/:statusId",
        path: "update-redesign/:id",
        component: NewRedesignComponent,
        data: { title: "Update Redesign" }
      }
    ]
  },
  {
    path: "redesign/customer-bypass",
    component: CustomerBypassComponent,
    data: { title: "Customer Bypass" }
  },
  {
    path: "search",
    data: { title: "Advanced Search" },
    component: AdvancedSearchComponent
  },
  {
    path: "search-result",
    data: { title: "Search Result" },
    component: SearchResultComponent
  },
  {
    path: "doc-download",
    children: [
      { path: "", component: DocDownloadComponent },
      {
        path: ":id",
        component: DocDownloadComponent
      }
    ]
  },
  {
    path: "ip-management",
    data: { title: "IP Management" },
    loadChildren: () =>
      import("./ip-management/ip-management.module").then(mod => mod.IPManagementModule)
  },
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "sorry", component: SorryComponent, data: { title: "Authorize" } },
  { path: "csg-error-page", component: CsgErrorPageComponent, data: { title: "CSG Level 2" } },
  {
    path: "page-not-found",
    component: PageNotFoundComponent,
    data: { title: "404" }
  },
  { path: "**", redirectTo: "page-not-found" }
];
