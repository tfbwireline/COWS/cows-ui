import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidator } from '../../shared/validator/custom-validator';
import { NgxSpinnerService } from 'ngx-spinner';
import { CountryService, H5FolderService, UserService, SearchService } from '../../services';
import { H5Folder, Country, H5Doc } from '../../models';
import { Global } from '../../shared/global';
import { Helper } from "../../shared";
import { zip } from "rxjs";
import { DxDataGridComponent } from "devextreme-angular";
import { DocEntity } from '../../models/index';
import { FileSizePipe } from '../../shared/pipe/file-size.pipe';
declare let $: any;

@Component({
  selector: 'app-h5-folder',
  templateUrl: './h5-folder.component.html'
})

export class H5FolderComponent implements OnInit, AfterViewInit {
  @ViewChild('gridH5Folder', { static: false }) dataGrid: DxDataGridComponent;
  @ViewChild('modal', { static: false }) private modal: ElementRef;
  id: number = 0;
  isReviewer: boolean = false;
  isSubmitted: boolean = false;
  isSearchSubmitted: boolean = false;
  isView: boolean = false;
  form: FormGroup;
  option: any;
  cmd: string = '';
  countryList: Country[] = [];
  h5FolderData: H5Folder;
  h5FolderList: H5Folder[] = [];
  orderList: any[] = [];
  docList: DocEntity[] = [];
  selectedH5Folder: any[] = [];

  get searchCustId() { return this.form.get("search.customerId") }
  get customerId() { return this.form.get("add.customerId") }
  get customerName() { return this.form.get("add.customerName") }
  get city() { return this.form.get("add.city") }
  get country() { return this.form.get("add.country") }

  constructor(private spinner: NgxSpinnerService, private activatedRoute: ActivatedRoute,
    private helper: Helper, private countrySrvc: CountryService, private h5Srvc: H5FolderService, private searchService: SearchService, private fileSizePipe: FileSizePipe,
    private userSrvc: UserService,  private router: Router) { }


  ngAfterViewInit() {
    // Clear modal when esc or backdrop is clicked
    $('#folderDetails').on('hidden.bs.modal', () => {
      this.clear();
    });
  }

  ngOnInit() {
    this.cmd = this.activatedRoute.snapshot.queryParams["cmd"] || '';
    this.id = this.activatedRoute.snapshot.queryParams["h5FoldrId"] || 0;

    this.form = new FormGroup({
      search: new FormGroup({
        customerId: new FormControl(''),
        customerName: new FormControl(''),
        m5Ctn: new FormControl(''),
        city: new FormControl(''),
        country: new FormControl('')
      }),
      add: new FormGroup({
        customerId: new FormControl(null, Validators.compose([Validators.required, CustomValidator.h1Validator])),
        customerName: new FormControl(null, Validators.required),
        city: new FormControl(null, Validators.required),
        country: new FormControl(null, Validators.required),
      })
    });
    this.option = {
      inputFileControl: {
        inputFileField: {
          label: "H6 Document",
          isShown: true
        }
      },
      grid: {
        column: {
          fileName: {
            caption: "H6 Document"
          },
          fileSize: {
            byte: true
          },
          comment: {
            isShown: true
          }
        }
      }
    }
    this.getUserAccess().then(() => {
      this.init();
    });
  }

  getUserAccess() {
    return new Promise(resolve => {
      let admin: number = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Admin'));
      let reviewer: number = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order Reviewer'));
      let updater = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order Updater'));
      let rts = this.userSrvc.loggedInUser.profiles.findIndex(i => i.includes('NCI Order RTS'));
      this.isReviewer = (admin == -1 && reviewer > -1 && updater == -1 && rts == -1);
      resolve(true);
    });
  }

  init() {
    this.countrySrvc.get().subscribe(
      res => {
        this.countryList = res;
      })

    if (this.cmd == 'add') {
      if (this.isReviewer) {
        this.helper.notify('Order Reviewer profile has read only access for H6 folder.', Global.NOTIFY_TYPE_WARNING);
      }
      else {
        this.addFolder();
      }
    }
    else if (this.id > 0) {
      let data = zip(
        this.h5Srvc.getById(this.id),
        this.h5Srvc.getOrderByH5FolderId(this.id)
      )

      data.toPromise().then(
        res => {
          console.log(res)
          this.h5FolderData = res[0] as H5Folder;
          this.orderList = res[1];
          // Set H5 Folder Form details
          this.customerId.setValue(this.h5FolderData.custId);
          this.customerName.setValue(this.h5FolderData.custNme);
          this.city.setValue(this.h5FolderData.custCtyNme);
          this.country.setValue(this.h5FolderData.ctryCd);
          if (this.h5FolderData.h5Docs != null && this.h5FolderData.h5Docs.length > 0) {
            this.docList = this.h5FolderData.h5Docs.map(item => {
              return new DocEntity({
                docId: item.h5DocId,
                id: item.h5FoldrId,
                fileNme: item.fileNme,
                fileCntnt: item.fileCntnt,
                fileSizeQty: item.fileSizeQty,
                fileSizeQtyStr: this.fileSizePipe.transform(item.fileSizeQty),
                cmntTxt: item.cmntTxt,
                creatDt: item.creatDt,
                creatByUserId: item.creatByUserId,
                creatByUserAdId: item.creatByUserAdid,
                base64string: item.base64string
              })
            })
          }
        },
        error => {
          //console.log(error);
          this.helper.notify('Failed to load H6 Folder details.', Global.NOTIFY_TYPE_ERROR);
        }
      );

      $("#folderDetails").modal("show");
      this.isView = true;
      this.form.get('add').disable();
    }
  }

  addFolder() {
    this.form.get('add').clearValidators();
    this.form.get('add').reset();
    $("#folderDetails").modal("show");
  }

  reset() {
    this.h5FolderList = [];
    this.isSearchSubmitted = false;
  }

  search() {
    this.spinner.show();
    this.isSearchSubmitted = true;
    let custId = this.form.get('search.customerId').value;
    custId = (this.helper.isEmpty(custId)) ? 0 : custId
    this.h5Srvc.search(custId, this.form.get('search.customerName').value,
      this.form.get('search.m5Ctn').value, this.form.get('search.city').value,
      this.form.get('search.country').value).subscribe(
        res => {
          this.h5FolderList = res;
          this.spinner.hide();
        },
        error => {
          //console.log(error)
          this.helper.notify('Search failed.', Global.NOTIFY_TYPE_ERROR);
          this.spinner.hide();
        }
      );
  }

  onSearchToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'removeTemplate'
    });
  }

  onSearchSelectionChanged(e) {
    this.selectedH5Folder = e.selectedRowKeys;
    //console.log(e)
  }

  onSearchRowRemoving(e) {
    this.spinner.show();
    this.h5Srvc.delete(e.key).subscribe(
      res => {
        this.helper.notify('Successfully deleted H6 Folder.', Global.NOTIFY_TYPE_SUCCESS);
        this.spinner.hide();
      },
      error => {
        //console.log(error)
        this.helper.notify('Failed in deleting H6 Folder.', Global.NOTIFY_TYPE_ERROR);
        this.spinner.hide();
      }
    );
  }

  onSearchEditClick(e) {
    this.id = e.key;
    this.getH5FolderDetails(false);
  }

  onSearchViewClick(e) {
    this.id = e.key;
    this.getH5FolderDetails(true);
  }

  getH5FolderDetails(isView: boolean) {
    // Call API for view/update
    if (this.id > 0) {
      let data = zip(
        this.h5Srvc.getById(this.id),
        this.h5Srvc.getOrderByH5FolderId(this.id)
      )

      data.toPromise().then(
        res => {
          this.h5FolderData = res[0] as H5Folder;
          this.orderList = res[1];
          // Set H5 Folder Form details
          this.customerId.setValue(this.h5FolderData.custId);
          this.customerName.setValue(this.h5FolderData.custNme);
          this.city.setValue(this.h5FolderData.custCtyNme);
          this.country.setValue(this.h5FolderData.ctryCd);
          if (this.h5FolderData.h5Docs != null && this.h5FolderData.h5Docs.length > 0) {
            this.docList = this.h5FolderData.h5Docs.map(item => {
              return new DocEntity({
                docId: item.h5DocId,
                id: item.h5FoldrId,
                fileNme: item.fileNme,
                fileCntnt: item.fileCntnt,
                fileSizeQty: item.fileSizeQty,
                fileSizeQtyStr: this.fileSizePipe.transform(item.fileSizeQty),
                cmntTxt: item.cmntTxt,
                creatDt: item.creatDt,
                creatByUserId: item.creatByUserId,
                creatByUserAdId: item.creatByUserAdid,
                base64string: item.base64string
              })
            })
          }
        },
        error => {
          //console.log(error);
          this.helper.notify('Failed to load H6 Folder details.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }

    $("#folderDetails").modal("show");
    this.dataGrid.instance.selectRows([this.id], true);

    if (this.isReviewer) {
      // View
      this.isView = true;
      this.form.get('add').disable();
    }
    else {
      if (isView) {
        // View
        this.isView = true;
        this.form.get('add').disable();
      }
      else {
        // Edit
        this.form.get('add').enable();
        this.customerId.disable();
      }
    }
  }

  deleteH5Folders() {
    //console.log(this.selectedH5Folder)
    if (this.isReviewer) {
      this.helper.notify('Order Reviewer profile has read only access for H6 folder.', Global.NOTIFY_TYPE_WARNING);
    }
    else {
      this.spinner.show();
      var hasError = false;
      var errorKeys: any[] = [];
      var errorMsg: string[] = [];

      //for (let i: number = 0; i < this.selectedH5Folder.length; i++) {
      //  this.h5Srvc.delete(this.selectedH5Folder[i]).toPromise().then(
      //    res => {
      //      this.clear();
      //    },
      //    error => {
      //      if (error.indexOf('FK05_ORDR') !== -1) {
      //        errorMsg.push('You are trying to remove a folder that is being used by one or more orders. The operation was cancelled.');
      //      }
      //      else if (error.indexOf('FK01_H5_DOC') !== -1) {
      //        errorMsg.push('You are trying to remove a folder that contains documents. Please, delete the documents first.');
      //      }
      //      errorKeys.push(this.selectedH5Folder[i]);
      //      hasError = true;
      //      this.spinner.hide();
      //    });

      //  if (i == this.selectedH5Folder.length - 1) {
      //    if (hasError && errorKeys.length > 0) {
      //      errorMsg.unshift('Failed to deleted record/s.');
      //      this.helper.notify(errorMsg.join(' '), Global.NOTIFY_TYPE_ERROR);
      //    }
      //    else {
      //      this.helper.notify('Successfully deleted record/s.', Global.NOTIFY_TYPE_SUCCESS);
      //    }

      //    this.search();
      //  }
      //}

      this.selectedH5Folder.forEach((key) => {
        this.h5Srvc.delete(key).toPromise().then(
          res => {
            this.clear();
          },
          error => {
            if (error.indexOf('FK05_ORDR') !== -1) {
              errorMsg.push('You are trying to remove a folder that is being used by one or more orders. The operation was cancelled.');
            }
            else if (error.indexOf('FK01_H5_DOC') !== -1) {
              errorMsg.push('You are trying to remove a folder that contains documents. Please, delete the documents first.');
            }
            errorKeys.push(key);
            hasError = true;
            this.spinner.hide();
          });
      });

      if (errorKeys.length == this.selectedH5Folder.length) {
        this.helper.notifyRemoveFormMessage(hasError, "", true);
      }
      else {
        this.helper.notifyRemoveFormMessage(hasError, this.selectedH5Folder.length.toString(), false);
      }

      this.search();
    }
  }

  clear() {
    this.id = 0;
    this.isView = false;
    this.isSubmitted = false;
    this.h5FolderData = null;
    this.docList = [];
    this.form.get('add').reset();
    this.form.get('add').clearValidators();
    this.form.get('add').enable();
    if (this.dataGrid != null || this.dataGrid != undefined) {
      this.dataGrid.instance.clearSelection();
    }
    this.spinner.hide();
    $("#folderDetails").modal("hide");
  }

  cancel() {
    this.clear();
  }

  save() {
    this.isSubmitted = true;
    let h5 = new H5Folder();
    h5.h5FoldrId = this.id;
    h5.custId = this.customerId.value;
    h5.custNme = this.customerName.value;
    h5.custCtyNme = this.city.value;
    h5.ctryCd = this.country.value;
    h5.h5Docs = [];

    if (this.docList != null && this.docList.length > 0) {
      this.docList.forEach(i => {
        let doc = new H5Doc();
        doc.h5DocId = i.docId;
        doc.h5FoldrId = i.id;
        doc.fileNme = i.fileNme;
        // Commented by Sarah Sandoval [20220113] - To prevent UI error for maximum entity when uploading > 20 docs
        //doc.fileCntnt = i.fileCntnt;
        doc.fileSizeQty = i.fileSizeQty;
        doc.cmntTxt = i.cmntTxt;
        doc.creatByUserId = i.creatByUserId;
        doc.creatDt = i.creatDt;
        doc.base64string = i.base64string;
        h5.h5Docs.push(doc);
      });
    }

    //console.log(h5)
    if (this.helper.isEmpty(h5.custId) || h5.custId <= 0 || this.helper.isEmpty(h5.custNme)
      || this.helper.isEmpty(h5.custCtyNme) || this.helper.isEmpty(h5.ctryCd)) {
      this.helper.notify('Enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.spinner.show();
    if (this.id > 0) {
      this.h5Srvc.update(this.id, h5).subscribe(
        res => {
          this.helper.notify('Successfully updated H6 Folder.', Global.NOTIFY_TYPE_SUCCESS);
          this.clear();
        },
        error => {
          this.spinner.hide();
          let msg = 'Failed to update H6 Folder. ';
          if (!this.helper.isEmpty(error)) {
            msg = msg + error.message;
          }

          this.helper.notify(msg, Global.NOTIFY_TYPE_ERROR);
        }
      );""
    }
    else {
      this.h5Srvc.create(h5).subscribe(
        res => {
          this.clear();
          this.helper.notify('Successfully created H6 Folder.', Global.NOTIFY_TYPE_SUCCESS);
          this.h5FolderData = res;
          this.searchCustId.setValue(this.h5FolderData.custId);
          this.search();
        },
        error => {
          this.spinner.hide();
          let msg = 'Failed to create H6 Folder. ';
          if (!this.helper.isEmpty(error)) {
            msg = msg + error.message;
          }

          this.helper.notify(msg, Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }
  selectionChangedHandler(data: any) {
    this.modal.nativeElement.click()

    const orderId = data["ordR_ID"];
    const orderCatId = data["ordR_CAT_ID"];
    const taskId = data['tasK_ID'];
    const wg = data['prF_ID'];


    if(orderCatId == 3) {
      this.router.navigate(['vendor/vendor-order/' + orderId]);
    } 
    else if(orderCatId == 4) {
      this.router.navigate(['ncco/ncco-order/' + orderId + '/' + 3]);
    }
    else if(orderCatId == 2 || orderCatId == 6) {
      let orderUrl = this.searchService.getOrderUrl(
        { 
          orderId: orderId, 
          wg: wg,
          orderCategoryId: orderCatId,
          taskId: taskId
        })
      if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
        if (this.helper.isEmpty(orderUrl.param)) {
          this.router.navigate([orderUrl.link]);
        }
        else {
          this.router.navigate([orderUrl.link], orderUrl.param);
        }
      }
      //this.router.navigate([orderUrl.link], orderUrl.param);
    }

    // const type = data["fsA_PROD_TYPE_DES"];
    // if(type === "VENDOR") {
    //   this.router.navigate(['vendor/vendor-order/' + orderId]);
    // } else {
    //   this.router.navigate(['ncco/ncco-order/' + orderId + '/' + 3]);
    // }
  }
}
