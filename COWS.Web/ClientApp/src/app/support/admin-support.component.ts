import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from '../../shared';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminSupportService, UserService } from '../../services';
import { Global } from '../../shared/global';

@Component({
  selector: 'app-admin-support',
  templateUrl: './admin-support.component.html'
})
export class AdminSupportComponent implements OnInit {
  form: FormGroup
  get showEvent() { return this.form.get("type").value == "Event" ? true : false }
  get showOrder() { return this.form.get("type").value == "Order" ? true : false }
  get showCPT() { return this.form.get("type").value == "CPT" ? true : false }
  get showRedesign() { return this.form.get("type").value == "Redesign" ? true : false }
  get showBPM() { return this.form.get("type").value == "BPM" ? true : false }
  get showDeleteOrder() { return this.form.get("type").value == "Delete COWS Order" ? true : false }
  get showExtractOrder() { return this.form.get("type").value == "Extract Mach5 Order" ? true : false }

  tables: any[] = []
  types: any[] = ["Event", "Order", "CPT", "Redesign", "BPM", "Delete COWS Order", "Extract Mach5 Order"]
  orderTransactions: any[] = ["Order", "Transaction"]
  //devAdids: string = "km967761,kh946640,vn370313,qi931353,tw299608,he240924,na157643,jrg7298,dlp0278,pferna01,vsrini01,dws1835,dsj4208,dx160495"

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService, private helper: Helper,
    private adminSupportService: AdminSupportService, public userService: UserService
  ) {
    //if (!this.devAdids.includes(this.userService.loggedInUser.adid)) {
    //  this.router.navigate(['home'])
    //}
  }

  ngOnInit() {
    this.form = new FormGroup({
      type: new FormControl(),
      eventId: new FormControl(),
      ftn: new FormControl(),
      cptNo: new FormControl(),
      redesign: new FormControl(),
      h6: new FormControl(),
      orderId: new FormControl(),
      orderTransaction: new FormControl(),
      m5OrderId: new FormControl(),
      relatedM5OrderId: new FormControl(),
      orderType: new FormControl(),
      deviceId: new FormControl(),
    })
  }

  searchEvent() {
    this.tables = []
    let eventId = this.form.get("eventId").value
    if (isNaN(+eventId)) {
      this.helper.notify(`${eventId} is not a valid Event ID`, Global.NOTIFY_TYPE_ERROR)
    } else {
      this.spinner.show()
      this.adminSupportService.getEventByEventId(eventId).subscribe(res => {
        console.log(res)
        let keys = Object.keys(res)

        keys.forEach(a => {
          this.tables.push(res[a])

          return a
        })
        console.log(this.tables)
        this.spinner.hide()
      }, error => {
        console.log(error)
        this.spinner.hide()
      })
    }
  }

  searchOrder() {
    this.tables = []
    let ftn = (this.form.get("ftn").value || "").trim()
    if (ftn.length == 0) {
      this.helper.notify(`${ftn} is not a valid Order No`, Global.NOTIFY_TYPE_ERROR)
    } else {
      this.spinner.show()
      this.adminSupportService.getOrderByFtn(ftn).subscribe(res => {
        let keys = Object.keys(res)

        keys.forEach(a => {
          this.tables.push(res[a])

          return a
        })
        console.log(this.tables)
        this.spinner.hide()
      }, error => {
        console.log(error)
        this.spinner.hide()
      })
    }
  }

  searchCPT() {
    this.tables = []
    let cptNo = (this.form.get("cptNo").value || "").trim()
    if (cptNo.length == 0) {
      this.helper.notify(`${cptNo} is not a valid CPT ID/Number`, Global.NOTIFY_TYPE_ERROR)
    } else {
      this.spinner.show()
      this.adminSupportService.getCPTByCPTNo(cptNo).subscribe(res => {
        let keys = Object.keys(res)

        keys.forEach(a => {
          this.tables.push(res[a])

          return a
        })
        console.log(this.tables)
        this.spinner.hide()
      }, error => {
        console.log(error)
        this.spinner.hide()
      })
    }
  }

  searchRedesign() {
    this.tables = []
    let redesign = (this.form.get("redesign").value || "").trim()
    if (redesign.length == 0) {
      this.helper.notify(`${redesign} is not a valid Redesign ID/Number`, Global.NOTIFY_TYPE_ERROR)
    } else {
      this.spinner.show()
      this.adminSupportService.getRedesign(redesign).subscribe(res => {
        let keys = Object.keys(res)

        keys.forEach(a => {
          this.tables.push(res[a])

          return a
        })
        console.log(this.tables)
        this.spinner.hide()
      }, error => {
        console.log(error)
        this.spinner.hide()
      })
    }
  }

  searchBPM() {
    this.tables = []
    let h6 = (this.form.get("h6").value || "").trim()
    if (h6.length == 0) {
      this.helper.notify(`${h6} is not a valid H6`, Global.NOTIFY_TYPE_ERROR)
    } else {
      this.spinner.show()
      this.adminSupportService.getBPM(h6).subscribe(res => {
        let keys = Object.keys(res)

        keys.forEach(a => {
          this.tables.push(res[a])

          return a
        })
        console.log(this.tables)
        this.spinner.hide()
      }, error => {
        console.log(error)
        this.spinner.hide()
      })
    }
  }

  deleteOrder() {
    this.tables = []
    let orderId = this.form.get("orderId").value
    if (isNaN(+orderId)) {
      this.helper.notify(`${orderId} is not a valid COWS Order ID`, Global.NOTIFY_TYPE_ERROR)
    } else {
      this.spinner.show()
      this.adminSupportService.deleteOrder(orderId).subscribe(res => {
        console.log(res)
        this.helper.notify(`ProdSupport Deleted Order - ${orderId}`, Global.NOTIFY_TYPE_SUCCESS)
        this.spinner.hide()
      }, error => {
        console.log(error)
        this.spinner.hide()
      })
    }
  }

  extractOrder() {
    this.tables = []
    let param = {
      isTransaction: this.form.get("orderTransaction").value == "Transaction" ? true : false,
      m5OrderId: this.form.get("m5OrderId").value || 0,
      relatedM5OrderId: this.form.get("relatedM5OrderId").value || 0,
      orderType: this.form.get("orderType").value || "",
      deviceId: this.form.get("deviceId").value || ""
    }
    if (isNaN(+param.m5OrderId)) {
      this.helper.notify(`${param.m5OrderId} is not a valid Mach5 Order ID`, Global.NOTIFY_TYPE_ERROR)
    } else if (param.orderType.trim().length == 0) {
      this.helper.notify(`${param.orderType} is not a valid Order Type`, Global.NOTIFY_TYPE_ERROR)
    } else {
      this.spinner.show()
      this.adminSupportService.extractOrder(param).subscribe(res => {
        console.log(res)
        if (res.length == 2) {
          this.helper.notify(`Error while extracting Order - ${param.m5OrderId}, Results : ${res}`, Global.NOTIFY_TYPE_ERROR)
        } else {
          this.helper.notify(`ProdSupport Extracted Order - ${param.m5OrderId}, Results : ${res}`, Global.NOTIFY_TYPE_SUCCESS)
        }
        this.spinner.hide()
      }, error => {
        console.log(error)
        this.spinner.hide()
      })
    }
  }

  onTypeChanged(e) {
    const previousValue = e.previousValue;
    const newValue = e.value;
    // Event handling commands go here

    console.log(e)
    this.tables = []
    if (e.value == "Extract Mach5 Order") {
      this.form.get("orderTransaction").setValue("Order")
    }
  }
}
