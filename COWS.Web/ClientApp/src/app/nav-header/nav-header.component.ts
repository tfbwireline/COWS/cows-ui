import { Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { UserService, MenuService } from './../../services';
import { Router } from '@angular/router';
import { SearchComponent } from '../search/search.component';

@Component({
  selector: 'app-nav-header',
  templateUrl: './nav-header.component.html'
})
export class NavHeaderComponent implements OnInit {
  @ViewChild(SearchComponent, { static: false }) qSearch: SearchComponent;
  @Input() isInActive: boolean;
  @Output() sessionReloaded: EventEmitter<boolean> = new EventEmitter();
  oldSiteURL: string;

  constructor(private router: Router, public menuService: MenuService, public userService: UserService) { }

  ngOnInit() {
    //console.log(`oldSiteURL: ${sessionStorage.getItem('oldSiteURL')}`)
    if ((sessionStorage.getItem('oldSiteURL') != null) && sessionStorage.getItem('oldSiteURL').length > 0)
      this.oldSiteURL = sessionStorage.getItem('oldSiteURL');
    else {
      this.menuService.getOldSiteURL().toPromise().then(
        res => {
          this.oldSiteURL = res;
          //console.log(`oldSiteURL: ${this.oldSiteURL}`)
          sessionStorage.setItem('oldSiteURL', this.oldSiteURL)
        });
    }
  }

  refreshToHome() {
    window.location.reload(true);
  }

  returnHome() {
    const pageBodyElement = document.getElementById("pageBody");
    pageBodyElement.className = "";

    if (this.qSearch !== undefined) {
      this.qSearch.resetQuickSearch();
    }

    // Workaround to force reload Home Component ngOnInit
    // this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() => {
    this.router.navigateByUrl('/home').then(() => {
      this.sessionReloaded.emit(true);
      //this.router.navigate(['/home']);
    }).then(() => window.location.reload(true));
  }

  collapseSideNav() {
    this.menuService.isContentCollapsed = !this.menuService.isContentCollapsed
    this.menuService.isSideNav = !this.menuService.isSideNav

    //if (!this.menuService.isContentCollapsed) {
    //  this.menuService.sideNav()
    //} else {
    //  this.menuService.isContentCollapsed = true
    //  this.menuService.isSideNav = false
    //}

    //this.parent.isContentCollapsed = !this.parent.isContentCollapsed;
    //this.parent.sideNav.isSideNavCollapsed = !this.parent.sideNav.isSideNavCollapsed;
  }

  toggleSideNav(value: boolean) {
    if (value) {
      this.menuService.sideNav()
    } else {
      this.menuService.navMenu()
    }

    if (this.userService.loggedInUser != null || this.userService.loggedInUser != undefined) {
      this.userService.loggedInUser.user.menuPref = this.menuService.isSideNav ? 'V' : 'H';
      //console.log(this.userService.loggedInUser)
      this.userService.setAsBlindRequest();
      this.userService.updateUser(this.userService.loggedInUser.userId, this.userService.loggedInUser.user).subscribe(res => {
      });
    }

    //this.parent.isSideNav = value;

    //if (this.parent.user != null || this.parent.user != undefined) {
    //  this.parent.user.menuPref = this.parent.isSideNav ? 'V' : 'H';

    //  this.userService.setAsBlindRequest();
    //  this.userService.updateUser(this.parent.user.userId, this.parent.user).subscribe(res => {
    //    //console.log(res)
    //  });
    //}

    //this.parent.refresh();
  }
}
