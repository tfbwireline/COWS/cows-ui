export const SearchTypeCriteria: string[] = [
  "Event",
  "Order",
  "Redesign"
]

export const EventSearchCriteria: string[] = [
  "Event ID",
  "M5 #",
  "AD Type",
  "Customer Name",
  "Assigned Activator",
  "SOWS EventID",
  "Requestor Name",
  "Event Type",
  "NUA",
  "FRB ID"
]

export const OrderSearchCriteria: string[] = [
  "M5 #",
  "H1 Cust Name",
  "H5 Cust Name",
  "Order Type",
  "Order Sub Type",
  "CCD"
]

export const RedesignSearchCriteria: string[] = [
  "Redesign Number",
  "Customer Name",
  "NTE Name",
  "ODIE Device",
  "PM Name"
]
