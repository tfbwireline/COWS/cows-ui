import { Component, OnInit } from '@angular/core';
import { BillingDispatchService, BillingDispatchParameterService } from '../../services';
import { NgxSpinnerService } from 'ngx-spinner';
import { BillingDispatch } from '../../models'
import { Router } from '@angular/router';
import { Helper } from '../../shared';

@Component({
  selector: 'app-billing-dispatch',
  templateUrl: './billing-dispatch.component.html'
})
export class BillingDispatchComponent implements OnInit {
  items: BillingDispatch[]
  historyMode: boolean

  constructor(private helper: Helper, private dispatchService: BillingDispatchService,
    private spinner: NgxSpinnerService, private router: Router,
    private paramService: BillingDispatchParameterService) { }

  ngOnInit() {
    this.getBillingDispatchData()

    this.historyMode = this.paramService.historyModeOn
  }

  onToolbarPreparing(e) {
    //var toolbarItems = e.toolbarOptions.items;
    //// Modifies an existing item
    //toolbarItems.forEach(function (item) {
    //  if (item.name === "exportButton") {
    //    item.width = 100;
    //  }
    //})
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
      e.fileName = "BillingDispatchExport_" + this.helper.formatDate(new Date())
  }

  cbChanged(e: any) {
    //console.log('value changed to: ' + this.historyMode )
    this.paramService.historyModeOn = e.value
    this.getBillingDispatchData()
  }

  private getBillingDispatchData() {
    this.spinner.show()

    this.dispatchService.get(this.paramService.historyModeOn).subscribe(
      res => {
        this.items = res
      },
      error => {
        //show error
        console.log(error.error)
      }
    )
    //console.log(JSON.stringify(this.items))
    this.spinner.hide()
  }

  selectionChangedHandler(id) {
    if (id > 0) {
      this.router.navigate(['billing-dispatch/' + id])
    }
  }
}
