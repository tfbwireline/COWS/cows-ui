import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { FormGroup, FormControl, RequiredValidator, Validators, ValidatorFn } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { BillingDispatchService, UserService, BillingDispatchParameterService } from "../../../services";
import { BillingDispatch } from "../../..//models";
import { Helper } from "../../../shared";
//import { analyzeAndValidateNgModules } from "@angular/compiler";
import { Global } from "../../../shared/global";


@Component({
  selector: "app-billing-dispatch-detail",
  templateUrl: "./billing-dispatch-detail.component.html"
})
export class BillingDispatchDetailComponent implements OnInit, OnDestroy {
  form: FormGroup;

  id: number = 0;
  model: BillingDispatch;
  loggedInUser = <any>[];
  selectedUserProfile = <any>[];
  commentReq: boolean = false

  //isSubmitted: boolean = false;
  canSubmit: boolean = false;

  historyModeOn: boolean = false

  constructor(
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private service: BillingDispatchService,
    private helper: Helper,
    private userService: UserService,
    private paramService: BillingDispatchParameterService
  ) {
    // console.log('constructor: ' + this.router.getCurrentNavigation().extras.state.mode);
  }

  ngOnDestroy() {
    this.helper.form = null;
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params["id"] || 0;

    this.historyModeOn = this.paramService.historyModeOn

    this.getLoggedInUser();

    this.form = new FormGroup({
      id: new FormControl({ value: null, disabled: true }),
      customerId: new FormControl({ value: null, disabled: true }),
      ticketLocation: new FormControl({ value: null, disabled: true }),
      dispositionCode: new FormControl({ value: null, disabled: true }),
      ticketCategoryName: new FormControl({ value: null, disabled: true }),
      ticketSubCategoryName: new FormControl({ value: null, disabled: true }),
      actionTaken: new FormControl({ value: null, disabled: true }),
      closeDate: new FormControl({ value: null, disabled: true }),
      ticketNumber: new FormControl({ value: null, disabled: true }),
      acceptRejectCode: new FormControl({ value: null, disabled: false, validators: [{ type: Validators.required, name: "required", message: "Accept or Reject selection is required" }] }),
      comment: new FormControl({ value: null, disabled: false }),
      //statusId: new FormControl({ value: null, disabled: true }),
      statusDescription: new FormControl({ value: null, disabled: true }),
      createdByUserId: new FormControl({ value: null, disabled: true }),
      createdBy: new FormControl({ value: null, disabled: true }),
      modifiedByUserId: new FormControl({ value: null, disabkled: true }),
      modifiedBy: new FormControl({ value: null, disabled: true }),
      createdDate: new FormControl({ value: null, disabled: true }),
      modifiedDate: new FormControl({ value: null, disabled: true })
    });

    this.setValidators(this.form, "acceptRejectCode", [Validators.required]);

    this.helper.form = this.form;

    this.init();
  }

  setValidators(form: FormGroup, controlName: string, validators: ValidatorFn[]) {
    form.get(controlName).setValidators(validators)
    form.get(controlName).updateValueAndValidity()
  }

  clearValidators(form: FormGroup, controlName: string){
    form.get(controlName).clearValidators()
    form.get(controlName).updateValueAndValidity()
  }

  getLoggedInUser() {
    this.userService.getLoggedInUser().subscribe(data => {
      this.loggedInUser = data;
      //console.log(JSON.stringify(this.loggedInUser))
      
      //get user profile by ad id

      this.userService.getUserProfilesByADID(this.loggedInUser.userAdid).toPromise().then(res =>{
        if (res.find(a => a.usrPrfDes === 'Billable Dispatch Admin')){
          this.canSubmit = true
        }
      })

    }),
    (error => {
      console.log(error.error)
    }),
    () => {
     this.spinner.hide()
    }
  }

  init() {
    //let id = this.id

    this.spinner.show();

    this.getDetail(this.id);

    this.spinner.hide();
  }

  getDetail(detailId: number) {
    //this.spinner.show();

    this.service.getById(detailId, this.paramService.historyModeOn).subscribe(
      res => {
        //console.log('data: ' + JSON.stringify(res))
        this.model = res;
        //console.log('model: ' + JSON.stringify(this.model))

        this.helper.setFormControlValue("id", this.id);

        this.helper.setFormControlValue("customerId", this.model.customerId);
        this.helper.setFormControlValue(
          "ticketLocation",
          this.model.ticketLocation
        );
        this.helper.setFormControlValue(
          "dispositionCode",
          this.model.dispositionCode
        );
        this.helper.setFormControlValue(
          "ticketCategoryName",
          this.model.ticketCategoryName
        );
        this.helper.setFormControlValue(
          "ticketSubCategoryName",
          this.model.ticketSubCategoryName
        );
        this.helper.setFormControlValue("actionTaken", this.model.actionTaken);
        this.helper.setFormControlValue("closeDate", this.model.closeDate );
        this.helper.setFormControlValue(
          "ticketNumber",
          this.model.ticketNumber
        );
        this.helper.setFormControlValue(
          "acceptRejectCode",
          this.model.acceptRejectCode
        );
        this.helper.setFormControlValue("comment", this.model.comment);
        if (this.model.acceptRejectCode.trim().length > 0) {
          this.form.get('acceptRejectCode').disable();
          this.form.get('comment').disable();
        }
        //this.helper.setFormControlValue("statusId", this.model.statusId);
        this.helper.setFormControlValue(
          "statusDescription",
          this.model.statusDescription
        );
        this.helper.setFormControlValue(
          "createdByUserId",
          this.model.createdByUserID
        );
        this.helper.setFormControlValue("createdBy", this.model.createdBy);
        this.helper.setFormControlValue(
          "modifiedByUserId",
          this.model.modifiedByUserID
        );
        this.helper.setFormControlValue("modifiedBy", this.model.modifiedBy);
        this.helper.setFormControlValue("createdDate", this.model.createdDate);
        this.helper.setFormControlValue(
          "modifiedDate",
          this.model.modifiedDate
        );
      },
      error => {
        //show error
        console.log(error.error);
      }
    );
    //console.log(JSON.stringify(this.model));
    this.spinner.hide();
  }

  cancel() {
    //cancel and return to previous page
    this.router.navigate(["billing-dispatch"]);
  }

  save() {
    //save changes and return to previous page
    //console.log(JSON.stringify(this.getFormDetails()))
    
    
    this.spinner.show()

    this.service.post(this.getFormDetails()).subscribe(
      res => {
        // console.log(JSON.stringify(res))

        this.helper.notifySavedFormMessage(
          "Billing Dispatch Saved",
          Global.NOTIFY_TYPE_SUCCESS,
          true,
          null
        );
        this.cancel();
      },
      error => {
        this.helper.notifySavedFormMessage(
          "Billing Dispatch",
          Global.NOTIFY_TYPE_ERROR,
          true,
          error
        );
        this.spinner.hide();
      },
      () => this.spinner.hide()
    ); 
    
  }

  getFormDetails() {
    const bd: BillingDispatch = <BillingDispatch>{
      id: this.id,
      customerId: this.helper.getFormControlValue("customerId"),
      ticketLocation: this.helper.getFormControlValue("ticketLocation"),
      dispositionCode: this.helper.getFormControlValue("dispositionCode"),
      ticketCategoryName: this.helper.getFormControlValue("ticketCategoryName"),
      ticketSubCategoryName: this.helper.getFormControlValue(
        "ticketSubCategoryName"
      ),
      actionTaken: this.helper.getFormControlValue("actionTaken"),
      closeDate: this.helper.getFormControlValue("closeDate"),
      ticketNumber: this.helper.getFormControlValue("ticketNumber"),
      acceptRejectCode: this.helper.getFormControlValue("acceptRejectCode"),
      comment: this.helper.getFormControlValue("comment"),
      //statusId: this.helper.getFormControlValue("statusId"),
      statusDescription: this.helper.getFormControlValue("statusDescription"),
      modifiedByUserID: this.loggedInUser.userId
    };

    return bd;
  }

  getAcceptReject() : string{
    let actionMsg: string = ""

    var selectedAction = this.helper.getFormControlValue("acceptRejectCode")

    if (selectedAction == "A") {
      actionMsg = "Accept"
    }else if (selectedAction == "R") {
      actionMsg = "Reject"
    }

    return actionMsg
  }

  changeCommentValidation(e: any) {
    //make comment not-required
    this.clearValidators(this.form, "comment");
    this.commentReq = false
    
    var optionSelected = e.value

    // console.log('option selected: ' + optionSelected)

    if (optionSelected === 'R') {
      //make comment required
      this.setValidators(this.form, "comment", [Validators.required]);
      this.commentReq = true
    } 

  }
}
