import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { HomeComponent, DashboardComponent } from './';
import { ReactiveFormsModule } from '@angular/forms';
import { DevExtremeModule } from 'devextreme-angular';
import { ReportsService } from '../../services';
import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';
import { CannedDailyComponent } from './canned-daily/canned-daily.component';
import { CannedWeeklyComponent } from './canned-weekly/canned-weekly.component';
import { CannedMonthlyComponent } from './canned-monthly/canned-monthly.component';


@NgModule({
  declarations: [HomeComponent, DashboardComponent, CannedDailyComponent, CannedWeeklyComponent, CannedMonthlyComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    ReactiveFormsModule,
    DevExtremeModule,
    NgxSpinnerModule
  ],
  providers: [
    ReportsService,
    NgxSpinnerService
  ]
})
export class ReportsModule { }
