import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../services';
import { DashboardData, OrderData } from '../../../models';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  //dashboardData: DashboardData
  //datastring: string
  newOrders: OrderData[] = [];
  installOrders: OrderData[] = [];

  constructor(private service: ReportsService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getDashboardReportData()
  }

  getDashboardReportData() {
    this.spinner.show()

    this.service.getDashboard().subscribe(res => {
      //this.dashboardData = res
      //this.datastring = JSON.stringify(res)
      if (res != null) {
        this.newOrders = res.newOrders
        this.installOrders = res.installOrders
      }
    },
    error => {
      console.log(error.error)
    })

    this.spinner.hide()
  }

  customizeTooltip = (args: any) => {
    return {
      text: Math.abs(args.valueText)
    }
  }
}
