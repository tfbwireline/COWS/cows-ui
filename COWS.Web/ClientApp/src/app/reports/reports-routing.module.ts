import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  HomeComponent,
  DashboardComponent,
  CannedDailyComponent,
  CannedWeeklyComponent,
  CannedMonthlyComponent
} from "./index";

const routes: Routes = [
  //{
  //  path: "",
  //  component: HomeComponent
  //},
  {
    path: "dashboard",
    component: DashboardComponent
  },
  {
    path: "canned-daily",
    component: CannedDailyComponent
  },
  {
    path: "canned-weekly",
    component: CannedWeeklyComponent
  },
  {
    path: "canned-monthly",
    component: CannedMonthlyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule {}
