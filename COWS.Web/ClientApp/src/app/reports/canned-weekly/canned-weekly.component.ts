import { Component, OnInit } from "@angular/core";
import { ReportsService } from "../../../services";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { CannedReport, File } from "../../../models";
import { Helper } from "../../../shared";

@Component({
  selector: "app-canned-weekly",
  templateUrl: "./canned-weekly.component.html",
  styleUrls: ["./canned-weekly.component.css"]
})
export class CannedWeeklyComponent implements OnInit {
  items: CannedReport[];
  startDate: Date = this.mondayOfDate(new Date)

  constructor(
    private reportsService: ReportsService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private helper: Helper
  ) {}

  ngOnInit() {
    this.getCannedReport();
  }

  private getCannedReport() {
    this.spinner.show();

    this.reportsService
      .getCannedWeekly(
        new Date(this.mondayOfDate(this.startDate)).toLocaleDateString(),
        new Date(this.mondayOfDate(this.startDate).setDate(6) ).toLocaleDateString()
      )
      .subscribe(res => {
        this.items = res as CannedReport[];
        this.validateItems();
        // console.log('res ==> ' + JSON.stringify(res));
      }, err => {
        this.spinner.hide();
      }, () => {
        this.spinner.hide();
      });
  }

  mondayOfDate(selectedDate: Date){
    var day = selectedDate.getDay(),
      diff = selectedDate.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    return new Date(selectedDate.setDate(diff));
  }

  download(file: any) {
    this.reportsService.downloadCannedReportFile(file).subscribe(data => {
      saveAs(data, file.name);
    })
  }

  onExporting(e) {
    e.fileName = "CannedReportExport_" + this.helper.formatDate(new Date());
  }

  onRowUpdated(e) {
    this.reloadReports(e);
  }

  validateItems() {
    this.items.forEach(item => {
      if (item.file == null) {
        item.file = new ReportFile();
      }
    });
  }

  private reloadReports(e) {
    this.spinner.show();

    this.reportsService.reloadReports(e.data.report.id, "W", new Date(e.data.file.dateCreated).toLocaleDateString(), new Date(e.data.file.dateCreated).toLocaleDateString())
      .subscribe(
        res => {
          if (res != null) {
            let report = res as File;
            this.items.find(a => a.report.id == e.data.report.id).file.name = report.name;
            this.items.find(a => a.report.id == e.data.report.id).file.fullPath = report.fullPath;
            this.items.find(a => a.report.id == e.data.report.id).file.size = report.size;
            this.items.find(a => a.report.id == e.data.report.id).file.dateCreated = report.dateCreated;
            this.validateItems();
          }
          else {
            this.getCannedReport();
          }
        }
      )
    this.spinner.hide();
  }
}

class ReportFile {
  name: string = '';
  size: number = 0;
  dateCreated: Date = new Date();
  namePattern: string = '';
  fullPath: string = '';
}
