import { NgModule } from '@angular/core';
import {
  MatFormFieldModule,
  MatInputModule,
  MatTableModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatExpansionModule,
  MatDialogModule,
  MatButtonModule
} from "@angular/material";


@NgModule({
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatExpansionModule,
    MatDialogModule,
    MatButtonModule
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatExpansionModule,
    MatDialogModule,
    MatButtonModule
  ]
})
export class AppMaterialModule { }
