import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sorry',
  templateUrl: './sorry.component.html'
})
export class SorryComponent implements OnInit {
  message: string;
  constructor() { }

  ngOnInit() {
    this.message = 'We are sorry, you do not have access for this page. Please contact your supervisor or application admin.';
  }
}
