import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authorize',
  templateUrl: './authorize.component.html',
})
export class AuthorizeComponent implements OnInit {
  message: string;
  constructor() { }

  ngOnInit() {
    this.message = 'You are not a Valid COWS User. Please contact your admin or open a Service Desk ticket to request access.';
  }

}
