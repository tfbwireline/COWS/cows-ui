import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { MssService } from '../../../services/mss.service';
import { ProductTypes } from '../../../models/product-types.model';
import { Global } from "./../../../shared/global";

@Component({
  selector: 'app-mss-product-types',
  templateUrl: './mss-product-types.component.html'
})

export class MssProductTypesComponent implements OnInit {

  form: FormGroup;
  productTypeList: any;
  mssModel = new ProductTypes();
  selectedItemKeys: any[] = [];
  id: number = 0;
  isOnEdit: boolean = false;

  // UI Fields
  get sdePrdctTypeNme() { return this.form.get("name") }
  get sdePrdctTypeDesc() { return this.form.get("description") }
  get ordrBySeqNbr() { return this.form.get("order") }
  get recStusId() { return this.form.get("active") }
  get outsrcdCd() { return this.form.get("outsourced") }

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private mssService: MssService) {
    this.loadEditForm = this.loadEditForm.bind(this);
  }

  public ngOnInit() {
    this.getMssProductTypes();
    this.setForm();
  }

  public setForm() {
    this.form = new FormGroup({
      'name': new FormControl('', [Validators.required]),
      'description': new FormControl('', [Validators.required]),
      'order': new FormControl('', [Validators.required]),
      'active': new FormControl(),
      'outsourced': new FormControl()
    });
  }

  public clearForm() {
    this.isOnEdit = false;
    this.form.reset();
    this.id = 0;
    this.mssModel = new ProductTypes();

    this.mssService.getSDEProductTypesForAdmin().subscribe(
      res => {
        this.productTypeList = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  public getMssProductTypes() {
    this.spinner.show();
    this.mssService.getSDEProductTypesForAdmin().subscribe(
      res => {
        this.productTypeList = res;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  onEditingStart(e) {
    if (e.column.command == 'edit') {
      this.isOnEdit = true;

      // Set values for Edit Form
      this.id = e.row.key;
      this.mssModel = e.row.data;
      this.form.patchValue({
        name: this.mssModel.sdePrdctTypeNme,
        description: this.mssModel.sdePrdctTypeDesc,
        order: this.mssModel.ordrBySeqNbr,
        active: this.mssModel.recStusId ? 1 : 0,
        outsourced: this.mssModel.outsrcdCd
      });
    }
  }

  loadEditForm(e) {
    this.isOnEdit = true;

    // Set values for Edit Form
    this.id = e.row.key;
    this.mssModel = e.row.data;
    this.form.patchValue({
      name: this.mssModel.sdePrdctTypeNme,
      description: this.mssModel.sdePrdctTypeDesc,
      order: this.mssModel.ordrBySeqNbr,
      active: this.mssModel.recStusId ? 1 : 0,
      outsourced: this.mssModel.outsrcdCd
    });

    e.event.preventDefault();
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    },
      {
        location: 'before',
        template: 'removeTemplate'
      });
  }

  onExporting(e) {
    e.fileName = 'MSS_' + this.helper.formatDate(new Date());
  }

  onSubmit() {
    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }

    this.spinner.show();
    this.mssModel.sdePrdctTypeNme = this.sdePrdctTypeNme.value;
    this.mssModel.sdePrdctTypeDesc = this.sdePrdctTypeDesc.value;
    this.mssModel.ordrBySeqNbr = this.ordrBySeqNbr.value;
    this.mssModel.recStusId = this.recStusId.value ? 1 : 0;
    this.mssModel.outsrcdCd = this.outsrcdCd.value ? true : false;

    // For Edit
    if (this.id > 0) {
      this.mssService.updateProductType(this.id, this.mssModel).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.mssModel.sdePrdctTypeNme,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
          this.clearForm();
        },
        error => {
          this.helper.notifySavedFormMessage(this.mssModel.sdePrdctTypeNme,
            Global.NOTIFY_TYPE_ERROR, false, error);
          this.clearForm();
        });
    }
    // For Insert
    else {
      this.mssModel.recStusId = 1; // Default value
      this.mssService.insertProductType(this.mssModel).subscribe(
        data => {
          this.helper.notifySavedFormMessage(this.mssModel.sdePrdctTypeNme,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
          this.clearForm();
        },
        error => {
          this.helper.notifySavedFormMessage(this.mssModel.sdePrdctTypeNme,
            Global.NOTIFY_TYPE_ERROR, true, error);
          this.clearForm();
        });
    }
  }

  selectionChangedHandler(data: any) {
    this.selectedItemKeys = data.selectedRowKeys;
  }

  deleteRows() {
    this.spinner.show();
    var hasError = false;
    var errorKeys: any[] = [];
    this.selectedItemKeys.forEach((key) => {
      this.mssService.deleteProductType(key).subscribe(
        res => {
          this.clearForm();
        },
        error => {
          errorKeys.push(key);
          hasError = true;
          this.spinner.hide();
        });
    });

    if (errorKeys.length == this.selectedItemKeys.length) {
      this.helper.notifyDeactivateFormMessage(hasError, "", true);
    } else {
      this.helper.notifyDeactivateFormMessage(hasError, this.selectedItemKeys.length.toString(), false);
    }
  }
}

