import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MssEmailTeamComponent } from './mss-email-team.component';

describe('MssEmailTeamComponent', () => {
  let component: MssEmailTeamComponent;
  let fixture: ComponentFixture<MssEmailTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MssEmailTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MssEmailTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
