import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mss-email-team',
  templateUrl: './mss-email-team.component.html'
})
export class MssEmailTeamComponent implements OnInit, AfterViewInit{
  

  constructor(private router: Router) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    window.location.href = "mailto:MSS-SSE.List@t-mobile.com";
    this.router.navigateByUrl('/home');
  }
}
