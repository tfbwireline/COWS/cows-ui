import { Component, OnInit } from '@angular/core';
import { MssService } from '../../../services/mss.service';
import { Router } from '@angular/router';
import { Helper } from '../../../shared';

@Component({
  selector: 'app-mss-view-engagements',
  templateUrl: './mss-view-engagements.component.html'
})

export class MssViewEngagementsComponent implements OnInit {
  selectedUserId: number = 0
  selectedStatusId: number = 0
  viewEngagementsList: any
  availableUsers: any
  viewOptions: any

  constructor(private mssService: MssService, private router: Router, private helper: Helper) { }

  ngOnInit() {
    this.mssService.getAvailableUsers().subscribe((res: any) => {
      this.availableUsers = res.sort((a, b) => a.fullNme.localeCompare(b.fullNme));
    });
    this.mssService.getViewOptions().subscribe((res: any) => {
      this.viewOptions = res.sort((a, b) => a.stusDes.localeCompare(b.stusDes));
    });
    this.bindData();
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'MSS_' + this.helper.formatDate(new Date());
  }

  selectionChangedHandler(ID) {
    this.router.navigate(['mss/mss-new-engagement/' + ID]);
  }

  getUser(e) {
    this.selectedUserId = e;
  }

  getStatus(e) {
    this.selectedStatusId = e;
  }

  bindData() {
    this.mssService.getSdeView(this.selectedStatusId, this.selectedUserId).subscribe((res: any) => {
      this.viewEngagementsList = res
      //console.log(res);
    });
  }

  btnViewSearch() {
    this.bindData();
  }
}




