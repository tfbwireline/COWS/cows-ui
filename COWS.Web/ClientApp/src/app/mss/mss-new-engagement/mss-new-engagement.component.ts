import { Component, OnInit } from '@angular/core';
import { KeyValuePair, RB_CONDITION, Global, ESDEStatus, ERecStatus } from '../../../shared/global';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, of, Subject } from "rxjs";
import { Helper } from "./../../../shared";
import { CustomValidator } from '../../../shared/validator/custom-validator';
import { UserService } from "./../../../services/";
import { MssService } from '../../../services/mss.service';
import { SDEOpportunityDoc } from '../../../models/sde-opportunity-doc.model';
import { SDEOpportunityProduct } from '../../../models/sde-opportunity-product.model';
import { User } from "./../../../models";
import { SdeOpportunity } from '../../../models/sde-opportunity.model';
import { FileSizePipe } from '../../../shared/pipe/file-size.pipe';

@Component({
  selector: 'app-mss-new-engagement',
  templateUrl: './mss-new-engagement.component.html',
  styleUrls: ['./mss-new-engagement.component.css']
})
export class MssNewEngagementComponent implements OnInit {
  get allowEditting() {
    return this.isMDSAdmin || this.isMDSSSE;
  }

  isMDSAdmin: boolean = false;
  isMDSSSE: boolean = false;
  notificationMessage: string = '';
  isAbleToEdit: boolean = true;
  showSaveBtn: boolean = false;

  isAbleToCreate: boolean = false;
  isAbleToAssign: boolean = false;

  addBtnDisabled: boolean = false;
  isAbleToAdd: boolean = false;
  fileName: string = null;

  statusId: ESDEStatus;
  OpportunityID: number = 0;
  selectedUserId: number = 0;
  selectedStatusId: number = 0
  profile: string;
  allowEdit: boolean = true;
  editMode: boolean = false;
  regionList: any;
  cfgKeyName: string = "SDE Region";
  //typeOfSaleList: KeyValuePair[];
  rfpList: KeyValuePair[];
  wirelineCustList: KeyValuePair[];
  managedCustList: KeyValuePair[];
  shareFileList: any
  notesHistoryList: any
  sdeOpportunityList: SdeOpportunity
  form: FormGroup
  option: any
  typeOfSale: any = "U";
  rfp: boolean = false;
  wirelinecustomer: boolean = false;
  managedservices: boolean = false;
  userId: any;
  filelist: Array<SDEOpportunityDoc> = [];
  fileListSub: Subject<any> = new Subject();
  fileDetails: SDEOpportunityDoc = new SDEOpportunityDoc();
  file: any;
  statusList: any;
  assignedList: any;
  //availableUsers: any
  //viewOptions: any
  typeOfSaleList = [
    { description: "Unknown", value: "U" },
    { description: "Purchase", value: "P" },
    { description: "Lease", value: "L" },

  ];
  isSubmitted: boolean = false;

  sdeProdsList: Array<SDEOpportunityProduct> = [];

  allSdeProductType = [];
  fileIndex: number = 0;

  get requestor() { return this.form.get("accountteam.requestor"); }
  get email() { return this.form.get("accountteam.email"); }
  get info() { return this.form.get("accountteam.info"); }

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private fb: FormBuilder,
    private activatedRoute: ActivatedRoute, private userService: UserService, private router: Router, private mssService: MssService,
    private fileSizePipe: FileSizePipe) {
    this.viewFile = this.viewFile.bind(this);
    this.deleteFile = this.deleteFile.bind(this);
  }

  ngOnDestroy() {
    this.helper.form = null;
  }

  ngOnInit() {
    this.fileListSub.subscribe(data => {
      this.filelist = data;
    })

    this.OpportunityID = this.activatedRoute.snapshot.params["id"] || 0;

    this.editMode = this.OpportunityID > 0 ? true : false;
    this.form = this.fb.group({
      accountteam: new FormGroup({
        requestor: new FormControl({ value: '', disabled: true }, Validators.required),
        email: new FormControl({ value: '', disabled: true }, Validators.required),
        info: new FormControl({ value: '', disabled: true }, Validators.required)
      }),
      customerinfo: new FormGroup({
        companyname: new FormControl({ value: '', disabled: false }, [Validators.required]),
        address: new FormControl(''),
        region: new FormControl(''),
        state: new FormControl(''),
        city: new FormControl(''),
        contactname: new FormControl({ value: '', disabled: false }, [Validators.required]),
        contactemail: new FormControl({ value: '', disabled: false }, [Validators.required, Validators.compose([CustomValidator.emailValidator])]),
        contactphone: new FormControl({ value: '', disabled: false }, [Validators.required])
      }),
      saleinfo: new FormGroup({
        typeOfSale: new FormControl('U'),
        rfp: new FormControl(0),
        wirelinecustomer: new FormControl(0),
        managedservices: new FormControl(0)
      }),
      prodinfo: new FormGroup({
        products: this.fb.array([]),
        quantity: new FormControl(),
        // sharefile: new FormControl(),
        comments: new FormControl({ value: null, disabled: false }, [Validators.required]),
        ccemail: new FormControl(),
        fileupload: new FormControl({ value: null, disabled: false }),
      }),
      statusID: new FormControl(0),
      assignedToId: new FormControl(0),

    });
    // Set the helper form to this form
    this.helper.form = this.form;
    this.spinner.show();
    // Initial list data

    this.rfpList = this.helper.getEnumKeyValuePair(RB_CONDITION);
    this.wirelineCustList = this.helper.getEnumKeyValuePair(RB_CONDITION);
    this.managedCustList = this.helper.getEnumKeyValuePair(RB_CONDITION);

    let data = zip(
      this.userService.getLoggedInUser(),
      //this.userService.getFinalUserProfile(this.userService.loggedInUser.adid, this.OpportunityID),
      //this.mssService.getByID(this.OpportunityID),
      this.mssService.getSDEProds(this.OpportunityID),
      this.mssService.getSDEDocs(this.OpportunityID),
      this.mssService.getSDENotes(this.OpportunityID),
      this.mssService.getAllSdeProductType(),
      this.mssService.getCowsAppCfgValue(this.cfgKeyName),
      this.userService.getSDEViewUsers(),
      this.userService.userActiveWithProfileName(this.userService.loggedInUser.userId, "MDS Admin"), // 128
      this.userService.userActiveWithProfileName(this.userService.loggedInUser.userId, "MDS System Security Engineer"), // 139
      //this.mssService.getSDEProductTypes(),
      //this.mssService.getAvailableUsers(),
      //this.mssService.getViewOptions(),
    )
    data.subscribe(res => {
      let user = res[0] as User;
      if (this.OpportunityID <= 0) {
        this.helper.setFormControlValue("accountteam.requestor", user.dsplNme);
        this.helper.setFormControlValue("accountteam.email", user.emailAdr);
        this.helper.setFormControlValue("accountteam.info", `${user.phnNbr} ${user.ctyNme} ${user.sttCd}`);
      }
      //this.profile = this.helper.getFinalProfileName(res[1] as UserProfile);
      //this.sdeOpportunityList = res[2] as SdeOpportunity;
      //this.shareFileList = res[3] as SDEOpportunityDoc;
      this.sdeProdsList = res[1];
      this.filelist = res[2].map((data: SDEOpportunityDoc) => {
        const i = this.fileIndex++;
        data.index = i;
        data.fileSizeQuantityDisp = this.fileSizePipe.transform(data.fileSizeQuantity);
        return data;
      });
      this.notesHistoryList = res[3];
      this.allSdeProductType = res[4];
      this.addProductFormControls(this.allSdeProductType);
      //this.shareFileList = res[3];
      // List<string> regions = _db.GetCowsAppCfgValue("SDE Region").Split(',').Select(i => i.Trim()).ToList();
      //var obj = JSON.parse(res[2].toString())

      var array = res[5].toString().replace("{ CfgKeyValuTxt = ", "").replace("}", "").split(",").map(i => i.trim());
      var rlarray = [];
      for (var i = 0; i < array.length; i++) {
        rlarray.push({ data: array[i] });
      }
      // this.regionList = [{ data: "Asia" }, { data: "Africa" },{ data: "Australia" }, { data: "Europe" }, { data: "Middle East" }, { data: "Latin America" }
      // , { data: "North America" }, { data: "South America" }];//res[3].toString().split(",").map(i => i.trim());
      this.regionList = rlarray;
      this.assignedList = res[6];

      this.isMDSAdmin = res[7];
      this.isMDSSSE = res[8];
    }, error => {
      this.spinner.hide();
    }, () => {

      if (this.OpportunityID > 0) {
        this.form.disable();
        this.mssService.getByID(this.OpportunityID).subscribe(
          res => {
            console.log(res)
            this.requestor.setValue(res.createdByDisplayName);
            this.email.setValue(res.createdByEmail);
            this.info.setValue(res.requestorLocation);
            this.statusId = res.statusID;
            this.sdeOpportunityList = res;
            this.setProductFormControlValues(this.sdeProdsList);
            this.mssService.getWorkflowStatus(this.statusId).subscribe(data => {
              this.statusList = data;
            });

            if (this.statusId === ESDEStatus.Close) {
              this.notificationMessage = "This Engagement Request is closed. Updating the information below is not allowed.";
              this.isAbleToEdit = false;
            } else if (!this.isMDSSSE && (this.statusId === ESDEStatus.Assigned || this.statusId === ESDEStatus.Win || this.statusId === ESDEStatus.Ongoing || this.statusId === ESDEStatus.Lost)) {
              this.notificationMessage = "User needs to be part of System Security Engineer(SSE) to be working on this engagement request.";
              this.isAbleToEdit = false;
            } else if ((this.isMDSAdmin || this.isMDSSSE) && this.statusId !== ESDEStatus.Draft) { // Set form to enable
              this.isAbleToEdit = true;
            }

            this.prodInfoControls.get('comments').clearValidators();
            this.prodInfoControls.get('comments').updateValueAndValidity();
          }, error => {
            this.spinner.hide();
            this.helper.notify('An error occurred while retrieving the MSS data.',
              Global.NOTIFY_TYPE_ERROR);
          }, () => {
            this.setMssEngagementFormValue();
            this.spinner.hide();
          }
        );
      } else {
        this.showSaveBtn = true;
      }
      // this.setMssEngagementFormFields();
      this.spinner.hide();
    });
  }

  ddlStatusChange(event: any) {
    const value = event.selectedItem.stusId;
    if (value === ESDEStatus.Draft || value === ESDEStatus.Unassigned || value === ESDEStatus.Close) {
      this.form.get('assignedToId').disable();
    } else {
      this.form.get('assignedToId').enable();
    }
  }

  get customerInfoControls() {
    return this.form.controls.customerinfo;
  }

  get prodInfoControls() {
    return this.form.controls.prodinfo;
  }

  editRecord() {
    this.showSaveBtn = true;
    this.form.enable();

    // If user is MDS Admin only
    if (this.isMDSAdmin && !this.isMDSSSE && this.statusId !== ESDEStatus.Unassigned) {
      this.form.get("statusID").disable();
    }
  }

  setMssEngagementFormValue() {
    if (this.OpportunityID > 0 && this.sdeOpportunityList != null) {
      
      this.helper.setFormControlValue("customerinfo.companyname", this.sdeOpportunityList.companyName)
      this.helper.setFormControlValue("customerinfo.address", this.sdeOpportunityList.address)
      this.helper.setFormControlValue("customerinfo.region", this.sdeOpportunityList.region)
      this.helper.setFormControlValue("customerinfo.state", this.sdeOpportunityList.state)
      this.helper.setFormControlValue("customerinfo.city", this.sdeOpportunityList.city)
      this.helper.setFormControlValue("customerinfo.contactname", this.sdeOpportunityList.contactName)
      this.helper.setFormControlValue("customerinfo.contactemail", this.sdeOpportunityList.contactEmail)
      this.helper.setFormControlValue("customerinfo.contactphone", this.sdeOpportunityList.contactPhone)

      this.helper.setFormControlValue("saleinfo.typeOfSale", this.sdeOpportunityList.saleType)
      this.helper.setFormControlValue("saleinfo.rfp", this.sdeOpportunityList.isRFP ? 1 : 0);
      this.helper.setFormControlValue("saleinfo.wirelinecustomer", this.sdeOpportunityList.isWirelineCustomer ? 1 : 0);
      this.helper.setFormControlValue("saleinfo.managedservices", this.sdeOpportunityList.isManagedServicesCustomer ? 1 : 0);

      this.helper.setFormControlValue("prodinfo.ccemail", this.sdeOpportunityList.ccEmail)
      this.helper.setFormControlValue("statusID", this.sdeOpportunityList.statusID);
      this.helper.setFormControlValue("assignedToId", this.sdeOpportunityList.assignedToId);
    }
  }

  getMssEngagementFormValues(): SdeOpportunity {
    let mss = new SdeOpportunity();

    mss.companyName = this.helper.getFormControlValue("customerinfo.companyname")
    mss.address = this.helper.getFormControlValue("customerinfo.address")
    mss.region = this.helper.getFormControlValue("customerinfo.region")
    mss.state = this.helper.getFormControlValue("customerinfo.state")
    mss.city = this.helper.getFormControlValue("customerinfo.city")
    mss.contactName = this.helper.getFormControlValue("customerinfo.contactname")
    mss.contactEmail = this.helper.getFormControlValue("customerinfo.contactemail")
    mss.contactPhone = this.helper.getFormControlValue("customerinfo.contactphone")

    mss.saleType = this.helper.getFormControlValue("saleinfo.typeOfSale")
    const selectedSaleType = Object.assign([], this.typeOfSaleList.filter(x => x.value === mss.saleType));
    mss.saleTypeDesc = selectedSaleType[0].description;
    mss.isRFP = this.helper.getFormControlValue("saleinfo.rfp") === 0 ? false : this.helper.getFormControlValue("saleinfo.rfp")
    mss.isWirelineCustomer = this.helper.getFormControlValue("saleinfo.wirelinecustomer") === 0 ? false : this.helper.getFormControlValue("saleinfo.wirelinecustomer")
    mss.isManagedServicesCustomer = this.helper.getFormControlValue("saleinfo.managedservices") === 0 ? false : this.helper.getFormControlValue("saleinfo.managedservices")

    //Product Information
    const allSdeProds = this.prodInfoControls.get('products').value;
    const sdeProds = allSdeProds.filter(x => x.qty != 0);
    mss.sdeProducts = sdeProds;

    mss.sdeDocs = this.filelist;
    mss.comments = this.helper.getFormControlValue("prodinfo.comments");
    mss.ccEmail = this.helper.getFormControlValue("prodinfo.ccemail")

    mss.sdeOpportunityId = this.OpportunityID
    if (this.OpportunityID > 0) {
      mss.statusID = this.helper.getFormControlValue("statusID");
      mss.assignedToId = this.helper.getFormControlValue("assignedToId");
    }

    return mss;
  }

  submit() {
    var mss = this.getMssEngagementFormValues();
    this.isSubmitted = true;

    //console.log("check", mss, this.OpportunityID);
    if (this.form.invalid) {
      this.helper.notify('Please enter required fields.', Global.NOTIFY_TYPE_WARNING);
      return;
    }
    this.spinner.show();
    if (this.OpportunityID > 0) {
      // Update
      this.spinner.show();
      this.mssService.update(mss).subscribe(res => {
        this.helper.notifySavedFormMessage("MSS", Global.NOTIFY_TYPE_SUCCESS, false, null);
        this.cancel();
      }, error => {
        this.helper.notifySavedFormMessage("MSS", Global.NOTIFY_TYPE_ERROR, false, error);
        this.spinner.hide();
      }, () => {
        this.spinner.hide();
      });
    } else {
      //Create
      this.spinner.show();
      mss.sdeDocs = this.filelist;
      this.mssService.insert(mss).subscribe(res => {
        this.helper.notifySavedFormMessage("MSS", Global.NOTIFY_TYPE_SUCCESS, true, null);
        this.cancel();
      }, error => {
        this.helper.notifySavedFormMessage("MSS", Global.NOTIFY_TYPE_ERROR, true, error);
        this.spinner.hide();
      }, () => {
        this.spinner.hide();
      });
    }
  }
  cancel() {
    this.router.navigate(['mss/mss-view-engagements']);
  }
  disableForm() {
    this.editMode = false;
    this.form.disable();
  }

  getAllSdeProductType() {
    this.mssService.getAllSdeProductType().subscribe(
      (data: Array<SDEOpportunityProduct>) => {
        this.allSdeProductType = data;
      });
  }

  private addProductFormControls(data: Array<any>) {

    const productForm = this.prodInfoControls.get('products') as FormArray;
    for (let i = 0; i < data.length; i++) {
      productForm.push(
        this.fb.group({
          productTypeId: new FormControl(data[i].sdePrdctTypeId),
          productType: new FormControl(data[i].sdePrdctTypeNme),
          productTypeDesc: new FormControl(data[i].sdePrdctTypeDesc),
          qty: new FormControl(0)
        })
      )
    }
  }

  private setProductFormControlValues(data: Array<any>) {
    const productForm = this.prodInfoControls.get('products') as FormArray;
    const productFormValue = productForm.value;

    for (let i = 0; i < data.length; i++) {
      const index = productFormValue.findIndex(x => x.productTypeId === data[i].productTypeID);
      if (index >= 0) {
        productForm.controls[index].get('qty').patchValue(data[i].qty);
      }
    }
  }

  onFileChange(result) {

    let file = result.file as File

    this.addBtnDisabled = false;
    this.fileName = file.name;

    if (!this.helper.isValidFileType(file.name)) {
      this.helper.notify('File type is not allowed. Allowed file types are as follows.', Global.NOTIFY_TYPE_ERROR)
      this.addBtnDisabled = true;
      return;
    }
    if (!this.helper.isFileSizeValid(file.size)) {
      this.helper.notify('File size limit of 10MB has been exceeded.', Global.NOTIFY_TYPE_ERROR)
      this.addBtnDisabled = true;
      return;
    }

    this.isAbleToAdd = true;
    let newFile = new SDEOpportunityDoc();
    newFile.file = file;
    newFile.base64string = result.base64;
    newFile.fileName = file.name;
    newFile.fileSizeQuantity = file.size;
    newFile.fileSizeQuantityDisp = this.fileSizePipe.transform(file.size);
    newFile.createdDateTime = new Date();
    newFile.recStusId = ERecStatus.Active;

    this.fileDetails = Object.assign({}, newFile);

    this.helper.setFormControlValue("prodinfo.fileupload", file);


  }

  onFileAdd() {

    if (!this.isAbleToAdd) {
      return;
    }

    this.fileName = "";

    const i = this.fileIndex++;
    const newFile = Object.assign({}, this.fileDetails);
    newFile.index = i;

    this.filelist.push(newFile);
    this.isAbleToAdd = false;

  }

  public viewFile(e: any) {
    e.event.preventDefault();

    this.spinner.show();
    const data: SDEOpportunityDoc = e.row.data;

    if (this.editMode) {
      this.mssService.downloadSDEDocsById(data.sdeDocID).subscribe(res => {

        if (res != null) {
          saveAs(res, data.fileName)
        }
      }, error => {
        this.helper.notify("File not found", Global.NOTIFY_TYPE_ERROR);
        this.spinner.hide();
      }, () => this.spinner.hide());
    } else {
      saveAs(data.file, data.fileName)
      this.spinner.hide();
    }
  }

  public deleteFile(e: any) {
    e.event.preventDefault();
    const data: SDEOpportunityDoc = e.row.data;

    const index = this.filelist.findIndex(x => x.index === data.index);
    if (index >= 0) {
      const newFileList = this.filelist.filter(x => x.index !== data.index);
      this.fileListSub.next(newFileList);
    }
  }

}
