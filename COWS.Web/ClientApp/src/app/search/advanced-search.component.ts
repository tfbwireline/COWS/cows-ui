import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { zip } from "rxjs";
import { SearchTypes } from "./search.criteria";
import { EventType, MDSMACActivity, AdvancedSearchEvent, AdvancedSearchOrder, AdvancedSearchRedesign } from "./../../models/";
import { EventTypeService, MDSMACActivityService, MDSActivityTypeService, EventStatusService, MDSFastTrackTypeService, ProductTypeService, OrderTypeService, OrderSubTypeService, GroupService, PlatformService, RegionService, RedesignCategoryService, RedesignTypeService, StatusService, FedlineOrderTypeService, SearchService } from "./../../services/";
import { Helper } from "./../../shared/";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
import { Global } from '../../shared/global';

@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html'
})
export class AdvancedSearchComponent implements OnInit {
  @ViewChild('eventTemplate', { static: true })
  eventTemplate: TemplateRef<any> | null = null;
  @ViewChild('orderTemplate', { static: true })
  orderTemplate: TemplateRef<any> | null = null;
  @ViewChild('redesignTemplate', { static: true })
  redesignTemplate: TemplateRef<any> | null = null;

  eventList = null;
  orderList = null;
  redesignList = null;
  public msg: string;
  eventListLen = 0;
  orderListLen = 0;
  redesignListLen = 0;
  form: FormGroup
  types: SearchTypes[]
  status: SearchTypes[]
  template: TemplateRef<any> | null = null;

  eventTypes: EventType[]
  eventStatus: any[]
  mdsFastTrackTypes: any[]
  frbConfigurationTypes: SearchTypes[]
  frbActivityTypes: any[]
  mdsActivityTypes: any[]
  mdsMacActivity: MDSMACActivity[]

  productTypes: any[]
  orderSubTypes: any[]
  orderTypes: any[]
  groups: any[]
  platforms: any[]
  regions: any[]

  redesignCategories: any[]
  redesignTypes: any[]
  redesignStatus: any[]


  constructor(
    private helper: Helper, private searchService: SearchService, private spinner: NgxSpinnerService,
    private eventTypeService: EventTypeService, private eventStatusService: EventStatusService, private mdsFastTrackTypeService: MDSFastTrackTypeService, private fedlineOrderTypeService: FedlineOrderTypeService, private mdsActivityTypeService: MDSActivityTypeService, private mdsMacActivityService: MDSMACActivityService,
    private productTypeService: ProductTypeService, private orderSubTypeService: OrderSubTypeService, private orderTypeService: OrderTypeService, private groupService: GroupService, private platformService: PlatformService, private regionService: RegionService,
    private redesignCategoryService: RedesignCategoryService, private redesignTypeService: RedesignTypeService, private statusService: StatusService, private router: Router
  ) { }

  ngOnInit() {

    this.init();

    this.form = new FormGroup({
      criteria: new FormGroup({
        type: new FormControl(),
        status: new FormControl()
      })
    });

    this.reset();
  }

  init() {
    this.types = [
      new SearchTypes("Event", 1, []),
      new SearchTypes("Order", 2, []),
      new SearchTypes("Redesign", 3, [])
    ]

    this.status = [
      new SearchTypes("All", null, []),
      new SearchTypes("Pending", 0, []),
      new SearchTypes("Completed", 1, [])
    ]
  }

  initDropdown(type: number) {
    this.eventList = null
    this.orderList = null
    this.redesignList = null

    if (type == 1) {
      let data = zip(
        this.eventTypeService.getEventTypes(),
        this.eventStatusService.get(),
        this.mdsFastTrackTypeService.get(),
        this.fedlineOrderTypeService.get(),
        this.mdsActivityTypeService.get(),
        this.mdsMacActivityService.get()
      );
      data.subscribe(res => {
        this.eventTypes = res[0]
        this.eventStatus = res[1]
        this.mdsFastTrackTypes = res[2]
        this.frbConfigurationTypes = [
          new SearchTypes("1 - Stand Alone", 1, []),
          new SearchTypes("2 - Single Interface", 2, []),
          new SearchTypes("3 - Dual Interface", 2, [])
        ]
        let frb: SearchTypes[] = []
        for (let val of this.helper.getDistinct(res[3].map(a => a.prntOrdrTypeDes))) {
          frb.push(new SearchTypes(val, val, null))
        }
        this.frbActivityTypes = frb
        this.mdsActivityTypes = res[4]
        this.mdsMacActivity = res[5]
      });
    } else if (type == 2) {
      let data = zip(
        this.productTypeService.get(),
        this.orderSubTypeService.get(),
        this.orderTypeService.get(),
        this.groupService.get(),
        this.platformService.get(),
        this.regionService.get()
      );

      data.subscribe(res => {
        this.productTypes = res[0]
        this.orderSubTypes = res[1]
        this.orderTypes = res[2]
        this.groups = res[3]
        this.platforms = res[4]
        this.regions = res[5]
      });
    } else if (type == 3) {
      let data = zip(
        this.redesignCategoryService.get(),
        this.redesignTypeService.get(),
        this.statusService.get()
      );

      data.subscribe(res => {
        this.redesignCategories = res[0]
        let rdt: SearchTypes[] = []
        for (let val of this.helper.getDistinct(res[1].map(a => a.redsgnTypeNme))) {
          rdt.push(new SearchTypes(val, val, null))
        }
        this.redesignTypes = rdt
        this.redesignStatus = res[2].filter(a => a.stusTypeId == "REDSGN")
      });
    }
  }

  setForm() {
    this.template = null;

    let type = this.getFormControlValue("criteria.type");
    let status = this.getFormControlValue("criteria.status");

    if (this.form.contains("field")) {
      this.form.removeControl("field")
    }

    this.initDropdown(type);


    if (type == 1) {
      this.form.addControl("field", new FormGroup({
        m5_no: new FormControl(),
        assignedName: new FormControl(),
        requestorName: new FormControl(),
        eventId: new FormControl(),
        eventType: new FormControl(),
        eventStatus: new FormControl(),
        // fmsCircuit: new FormControl(),
        nua: new FormControl(),
        h5_h6: new FormControl(),
        ftActivityType: new FormControl(),
        customer: new FormControl(),
        deviceName: new FormControl(),
        deviceSerialNo: new FormControl(),
        frbRequestId: new FormControl(),
        frbOrgId: new FormControl(),
        frbConfigurationType: new FormControl(),
        apptStartDate: new FormControl(),
        apptEndDate: new FormControl(),
        frbActivityType: new FormControl(),
        redesignNo: new FormControl(),
        mdsActivityType: new FormControl(),
        mdsMacType: new FormControl()
      }));

      this.template = this.eventTemplate;
    } else if (type == 2) {
      this.form.addControl("field", new FormGroup({
        m5_no: new FormControl(),
        isRelatedFtn: new FormControl(),
        name: new FormControl(),
        ccd: new FormControl(),
        privateLine: new FormControl(),
        h1: new FormControl(),
        vendorName: new FormControl(),
        prsQuote: new FormControl(),
        vendorCircuit: new FormControl(),
        productType: new FormControl(),
        soi: new FormControl(),
        h5_h6: new FormControl(),
        orderSubType: new FormControl(),
        orderType: new FormControl(),
        workGroup: new FormControl(),
        nua: new FormControl(),
        platformType: new FormControl(),
        region: new FormControl(),
        parentM5_no: new FormControl(),
        purchaseOrderNumber: new FormControl(),
        atlasWorkOrderNumber: new FormControl(),
        peopleSoftRequisitionNumber: new FormControl(),
        preQualId: new FormControl(),
        deviceId: new FormControl()
      }));
      this.template = this.orderTemplate;
    } else {
      this.form.addControl("field", new FormGroup({
        redesignNo: new FormControl(),
        customer: new FormControl(),
        h1: new FormControl(),
        redesignCategory: new FormControl(),
        redesignType: new FormControl(),
        redesignStatus: new FormControl(),
        odieDeviceName: new FormControl(),
        nteAssigned: new FormControl(),
        pmAssigned: new FormControl(),
        eventId: new FormControl(),
        mssSeAssigned: new FormControl(),
        neAssigned: new FormControl(),
        submittedDate: new FormControl(),
        slaDueDate: new FormControl(),
        expirationDate: new FormControl()
      }));
      this.template = this.redesignTemplate;
    }
  }

  onSubmit() {
    if (this.form.value.criteria.type == 1) {
      let event = new AdvancedSearchEvent()
      console.log("event")
      console.log(event);
      event.type = this.form.value.criteria.type
      event.status = this.form.value.criteria.status
      event.m5_no = this.form.value.field.m5_no
      event.assignedName = this.form.value.field.assignedName
      event.requestorName = this.form.value.field.requestorName
      event.eventId = this.form.value.field.eventId
      event.eventType = this.form.value.field.eventType
      event.eventStatus = this.form.value.field.eventStatus
      // event.fmsCircuit = this.form.value.field.fmsCircuit
      event.nua = this.form.value.field.nua
      event.h5_h6 = this.form.value.field.h5_h6
      event.ftActivityType = this.form.value.field.ftActivityType
      event.customer = this.form.value.field.customer
      event.deviceName = this.form.value.field.deviceName
      event.deviceSerialNo = this.form.value.field.deviceSerialNo
      event.frbRequestId = this.form.value.field.frbRequestId
      event.frbOrgId = this.form.value.field.frbOrgId
      event.frbConfigurationType = this.form.value.field.frbConfigurationType
      event.apptStartDate = this.form.value.field.apptStartDate
      event.apptEndDate = this.form.value.field.apptEndDate
      event.frbActivityType = this.form.value.field.frbActivityType
      event.redesignNo = this.form.value.field.redesignNo
      event.mdsActivityType = this.form.value.field.mdsActivityType
      event.mdsMacType = this.form.value.field.mdsMacType

      this.spinner.show();
      this.searchService.getEvent(event).subscribe(res => {        
        this.eventList = res
        this.eventListLen = res.length;               
      },
        error => { },
        () => {
          this.spinner.hide();
          //this.helper.notify('No Records found for your Search Criteria, Please refine your Search', Global.NOTIFY_TYPE_INFO);
          if (this.eventList.length == 0) {
            this.eventListLen = this.eventList.length;  
            this.msg = "No Records found for your Search Criteria, Please refine your Search";
          }
        })
    } else if (this.form.value.criteria.type == 2) {
      let order = new AdvancedSearchOrder()

      order.type = this.form.value.criteria.type
      order.status = this.form.value.criteria.status
      order.m5_no = this.form.value.field.m5_no
      order.isRelatedFtn = this.form.value.field.isRelatedFtn
      order.name = this.form.value.field.name
      order.ccd = this.form.value.field.ccd
      order.privateLine = this.form.value.field.privateLine
      order.h1 = this.form.value.field.h1
      order.vendorName = this.form.value.field.vendorName
      order.prsQuote = this.form.value.field.prsQuote
      order.vendorCircuit = this.form.value.field.vendorCircuit
      order.productType = this.form.value.field.productType
      order.soi = this.form.value.field.soi
      order.h5_h6 = this.form.value.field.h5_h6
      order.orderSubType = this.form.value.field.orderSubType
      order.orderType = this.form.value.field.orderType
      order.workGroup = this.form.value.field.workGroup
      order.nua = this.form.value.field.nua
      order.platformType = this.form.value.field.platformType
      order.region = this.form.value.field.region
      order.parentM5_no = this.form.value.field.parentM5_no
      order.purchaseOrderNumber = this.form.value.field.purchaseOrderNumber
      order.atlasWorkOrderNumber = this.form.value.field.atlasWorkOrderNumber
      order.preQualId = this.form.value.field.preQualId
      order.deviceId = this.form.value.field.deviceId

      this.spinner.show();
      this.searchService.getOrder(order).subscribe(res => {
        this.orderList = res
        this.orderListLen = res.length; 
        console.log(res)
      },
        error => { },
        () => {
          this.spinner.hide();
          if (this.orderList.length == 0) {
            this.orderListLen = this.orderList.length;
            this.msg = "No Records found for your Search Criteria, Please refine your Search";
          }
        })
    } else if (this.form.value.criteria.type == 3) {
      let redesign = new AdvancedSearchRedesign()

      redesign.type = this.form.value.criteria.type
      redesign.status = this.form.value.criteria.status
      redesign.redesignNo = this.form.value.field.redesignNo
      redesign.customer = this.form.value.field.customer
      redesign.h1 = this.form.value.field.h1
      redesign.redesignCategory = this.form.value.field.redesignCategory
      redesign.redesignType = this.form.value.field.redesignType
      redesign.redesignStatus = this.form.value.field.redesignStatus
      redesign.odieDeviceName = this.form.value.field.odieDeviceName
      redesign.nteAssigned = this.form.value.field.nteAssigned
      redesign.pmAssigned = this.form.value.field.pmAssigned
      redesign.eventId = this.form.value.field.eventId
      redesign.mssSeAssigned = this.form.value.field.mssSeAssigned
      redesign.neAssigned = this.form.value.field.neAssigned
      redesign.submittedDate = this.form.value.field.submittedDate
      redesign.slaDueDate = this.form.value.field.slaDueDate
      redesign.expirationDate = this.form.value.field.expirationDate

      this.spinner.show();
      this.searchService.getRedesign(redesign).subscribe(res => {
        this.redesignList = res
        this.redesignListLen = res.length;
        //console.log(res)
      },
        error => { },
        () => {
          this.spinner.hide();
          if (this.redesignList.length == 0) {
            this.redesignListLen = this.redesignList.length;
            this.msg = "No Records found for your Search Criteria, Please refine your Search";
          }})
    }
  }

  setType(value: number) {
    this.setFormControlValue("criteria.type", value);
    this.setForm();
  }

  setStatus(value: number) {
    this.setFormControlValue("criteria.status", value);
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  reset() {
    this.setStatus(null);
    this.setType(1);
    console.log("hello");
  }
  selectionChangedHandler(ID) {

    console.log(ID);
    console.log(ID[0].eventID);
    console.log(ID[0].eventType);


    if (ID[0].eventType == "AD") {
      this.router.navigate(['event/access-delivery/' + ID[0].eventID]);
    }
    else if (ID[0].eventType == "Fedline") {
      this.router.navigate(['event/fedline/' + ID[0].eventID]);
    }
    else if (ID[0].eventType == "MDS") {
      this.router.navigate(['event/mds/' + ID[0].eventID]);
    }
    else if (ID[0].eventType == "MPLS") {
      this.router.navigate(['event/mpls/' + ID[0].eventID]);
    }
    else if (ID[0].eventType == "NGVN") {
      this.router.navigate(['event/ngvn/' + ID[0].eventID]);
    }
    else if (ID[0].eventType == "SIPT") {
      this.router.navigate(['event/sipt/' + ID[0].eventID]);
    }
    else if (ID[0].eventType == "SprintLink") {
      this.router.navigate(['event/sprint-link/' + ID[0].eventID]);
    }
    else if (ID[0].eventType == "UCaaS") {
      this.router.navigate(['event/ucaas/' + ID[0].eventID]);
    }

  }
  //selectionChangedHandlerOrder(M5)
  //{

  //}
  selectionChangedOrderHandler(M5) {
    console.log(M5);

    if (M5) {
      this.router.navigate(['ncco/ncco-order/' + M5[0].ordR_ID + '/3']);
    }

  }

}
