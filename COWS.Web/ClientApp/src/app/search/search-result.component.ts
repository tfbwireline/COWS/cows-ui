import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Helper } from "./../../shared";
import { SearchService, OrderLockService, EventLockService, UserService, OrderService } from '../../services';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdvancedSearchEvent, AdvancedSearchOrder } from '../../models';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html'
})

export class SearchResultComponent implements OnInit, OnDestroy {
  eventList: any = [];
  orderList: any = [];
  navigationSubscription;

  constructor(private helper: Helper, private router: Router, private spinner: NgxSpinnerService,
    private avRoute: ActivatedRoute, private searchSrvc: SearchService, private userService: UserService, private orderService: OrderService,
    private orderLockSrvc: OrderLockService, private eventLockSrvc: EventLockService) {
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        let avRoute = this.avRoute.snapshot;
        if (!this.helper.isEmpty(avRoute) && !this.helper.isEmpty(avRoute.queryParams)) {
          this.search(avRoute.queryParams.type, avRoute.queryParams.column, avRoute.queryParams.text);
        }
      }
    });
  }

  ngOnDestroy(): void {
    // Destroy navigationSubscription to avoid memory leaks
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  ngOnInit() { }

  search(type: any, col: any, text: any) {
    this.eventList = [];
    this.orderList = [];
    if (type == 1) {
      this.spinner.show();
      let event = new AdvancedSearchEvent();
      if (col == 1) {
        event.eventId = text;
      }
      else if (col == 2) {
        event.m5_no = text;
      }
      else if (col == 3) {
        event.adType = text;
      }
      else if (col == 4) {
        event.customer = text;
      }
      else if (col == 5) {
        event.assignedName = text;
      }
      else if (col == 6) {
        event.sowsId = text;
      }
      else if (col == 7) {
        event.requestorName = text;
      }
      else if (col == 8) {
        event.eventType = text;
      }
      else if (col == 9) {
        event.nua = text;
      }
      else if (col == 10) {
        event.frbRequestId = text;
      }

      this.searchSrvc.getEvent(event).toPromise().then(
        res => {
          this.eventList = res;
          let len = res != null ? res.length : 0;
          if (len == 1) {
            let param = {
              eventId: res[0].eventID,
              eventType: res[0].eventType
            }

            let eventUrl = this.searchSrvc.getEventUrl(param)
            if (!this.helper.isEmpty(eventUrl)) {
              this.spinner.hide();
              this.router.navigate([eventUrl.link]);
            }
          }
        },
        error => { }
      ).then(() => { this.spinner.hide(); });
    }
    else if (type == 2) {
      this.spinner.show();
      let order = new AdvancedSearchOrder()
      if (col == 1) {
        order.m5_no = text;
      }
      else if (col == 2) {
        order.h1 = text;
      }
      else if (col == 3) {
        order.h5_h6 = text;
      }
      else if (col == 4) {
        order.orderType = text;
      }
      else if (col == 5) {
        order.orderSubType = text;
      }
      else if (col == 6) {
        order.ccd = text;
      }
      
      this.searchSrvc.getOrder(order).toPromise().then(
        res => {
          this.orderList = res
          let len = res != null ? res.length : 0;
          if (len == 1) {
            let param = {
              orderId: res[0].ordR_ID,
              wg: res[0].wgid,
              wgName: res[0].workGroup,
              orderCategoryId: res[0].caT_ID,
              taskId: res[0].tasK_ID,
              purchaseOrderNo: res[0].prcH_ORDR_NBR
            }
            let orderUrl = this.searchSrvc.getOrderUrl(param)

            orderUrl = null // Intentioally added this because the user should not be redirected.
            if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
              if (this.helper.isEmpty(orderUrl.param)) {
                this.spinner.hide();
                this.router.navigate([orderUrl.link]);
              }
              else {
                this.spinner.hide();
                this.router.navigate([orderUrl.link], orderUrl.param);
              }
            }
          }
        },
        error => { }
      ).then(() => { this.spinner.hide(); });
    }
    else if (type == 3) {
      this.router.navigate(['redesign/'], { queryParams: { column: col, text: text } });
    }
  }

  selectionChangedEventHandler(event) {
    let param = {
      eventId: event[0].eventID,
      eventType: event[0].eventType
    }

    let eventUrl = this.searchSrvc.getEventUrl(param)

    if (!this.helper.isEmpty(eventUrl)) {
      this.router.navigate([eventUrl.link]);
    }
  }

  goToEvent(event) {
    let param = {
      eventId: event.eventID,
      eventType: event.eventType
    }

    let eventUrl = this.searchSrvc.getEventUrl(param);

    if (!this.helper.isEmpty(eventUrl)) {
      this.router.navigate([eventUrl.link]);
    }
  }

  onToolbarEventPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExportingEvent(e) {
    e.fileName = 'Event_' + this.helper.formatDate(new Date());
  }

  onRowEventPrepared(e) {
    if (e.rowType == 'data') {
      this.eventLockSrvc.checkLock(e.key.eventID).toPromise().then(res => {
        if (res != null) {
          e.rowElement.className = e.rowElement.className + ' checked-in-row';
          e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
          e.rowElement.title = 'This event is locked by ' + res.lockByFullName;
        }
      });
    }
  }

  selectionChangedOrderHandler(M5) {
    //console.log(M5)
    this.spinner.show();
    let orderId = M5[0].ordR_ID;
    let wgId = M5[0].wgid;
    let isSystemWgId = false;

    let param = {
      orderId: orderId,
      wg: M5[0].wgid,
      wgName: M5[0].workGroup,
      orderCategoryId: M5[0].caT_ID,
      taskId: M5[0].tasK_ID,
      purchaseOrderNo: M5[0].prcH_ORDR_NBR,
      systemOriginalWorkgroup: 98
    }

    if (wgId == 0 || wgId == 98) {
      isSystemWgId = true;
      this.orderService.GetOrderPPRT(orderId).toPromise().then(data => {
        param.systemOriginalWorkgroup = this.searchSrvc.determineWGIDForSystemOrCompletedOrders(data, this.userService.loggedInUser.profiles);
        let orderUrl = this.searchSrvc.getOrderUrl(param)

        if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
          if (this.helper.isEmpty(orderUrl.param)) {
            this.router.navigate([orderUrl.link]);
          }
          else {
            this.router.navigate([orderUrl.link], orderUrl.param);
          }
        }
        else {
          this.spinner.show();
        }
      })
    } else {
      let orderUrl = this.searchSrvc.getOrderUrl(param)

      if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
        if (this.helper.isEmpty(orderUrl.param)) {
          this.router.navigate([orderUrl.link]);
        }
        else {
          this.router.navigate([orderUrl.link], orderUrl.param);
        }
      }
      else {
        this.spinner.show();
      }
    }
  }

  goToOrder(M5) {
    //console.log(M5)
    this.spinner.show();
    let orderId = M5.ordR_ID;
    let wgId = M5.wgid;
    let isSystemWgId = false;

    let param = {
      orderId: orderId,
      wg: M5.wgid,
      wgName: M5.workGroup,
      orderCategoryId: M5.caT_ID,
      taskId: M5.tasK_ID,
      purchaseOrderNo: M5.prcH_ORDR_NBR,
      systemOriginalWorkgroup: 98
    }

    console.log(M5)
    console.log(orderId)
    console.log(wgId)
    console.log(param)

    if (wgId == 0 || wgId == 98) {
      isSystemWgId = true;
      this.orderService.GetOrderPPRT(orderId).toPromise().then(data => {
        param.systemOriginalWorkgroup = param.wg = this.searchSrvc.determineWGIDForSystemOrCompletedOrders(data, this.userService.loggedInUser.profiles);
        let orderUrl = this.searchSrvc.getOrderUrl(param)

        if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
          if (this.helper.isEmpty(orderUrl.param)) {
            this.router.navigate([orderUrl.link]);
          }
          else {
            this.router.navigate([orderUrl.link], orderUrl.param);
          }
        }
        else {
          this.spinner.show();
        }
      })
    } else {
      sessionStorage.setItem('deviceId', M5.deviceID);
      let orderUrl = this.searchSrvc.getOrderUrl(param)
      console.log(orderUrl)

      if (!this.helper.isEmpty(orderUrl) && !this.helper.isEmpty(orderUrl.link)) {
        if (this.helper.isEmpty(orderUrl.param)) {
          this.router.navigate([orderUrl.link]);
        }
        else {
          this.router.navigate([orderUrl.link], orderUrl.param);
        }
      }
      else {
        this.spinner.show();
      }
    }
  }

  onToolbarOrderPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExportingOrder(e) {
    e.fileName = 'Order_' + this.helper.formatDate(new Date());
  }

  onRowOrderPrepared(e) {
    if (e.rowType == 'data') {
      this.orderLockSrvc.checkLock(e.key.ordR_ID).toPromise().then(res => {
        if (res != null) {
          e.rowElement.className = e.rowElement.className + ' checked-in-row';
          e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
          e.rowElement.title = 'This order is locked by ' + res.lockByFullName;
        }
      });
    }
  }
}
