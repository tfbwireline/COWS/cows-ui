import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from "@angular/forms";
import { KeyValuePair } from "./../../shared/global";
import { SearchTypes } from "./search.criteria";
import { Helper } from "./../../shared";
import { UserService, OrderSubTypeService } from '../../services';
import { SearchCode } from '../../models/index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})

export class SearchComponent implements OnInit {
  form: FormGroup
  types: SearchTypes[] = []
  columns: KeyValuePair[]
  criteria: FormGroup
  srchCd = new SearchCode();
  subscription: Subscription

  orderSubTypes = [];

  constructor(private router: Router, private helper: Helper, public usrSrvc: UserService, private orderSubTypeService: OrderSubTypeService) { }

  ngOnInit() {
    this.form = new FormGroup({
      type: new FormControl(this.usrSrvc.searchCode.isOrderUser ? 2 : 1),
      column: new FormControl(),
      query: new FormControl()
    });
    
    this.setType(this.usrSrvc.searchCode.isOrderUser ? 2 : 1);
  }

  resetQuickSearch() {
    this.form.get('query').reset();
    this.form.get('type').setValue(this.usrSrvc.searchCode.isOrderUser);
    this.setType(this.usrSrvc.searchCode.isOrderUser ? 2 : 1);
  }

  setType(value: number) {
    this.columns = this.getColumns(value);
    if (this.columns != null) {
      this.form.get('type').setValue(value);
      this.form.get('column').setValue(1);
    }
  }

  getColumns(value: number) {
    let type = this.usrSrvc.searchCode.types.find(a => a.value == value);

    if(value === 2) { // Remove Order Sub Type since its not working on the old site
      type.columns = type.columns.filter(x => x.value != 5);
    }

    if (type != null)
      return this.usrSrvc.searchCode.types.find(a => a.value == value).columns;

    return null;
  }

  search() {
    if (this.helper.isEmpty(this.form.value.column)
      || this.helper.isEmpty(this.form.value.query))
      return;

    let params = {
      type: this.form.value.type,
      column: this.form.value.column,
      text: this.form.value.query
    }

    this.router.navigate(['search-result/'], { queryParams: params });
  }

  advancedSearchNewTab() {
    const baseUrl = window.origin;
    window.open(baseUrl+'/home', '_blank');
  }
}
