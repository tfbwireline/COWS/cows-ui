import { Component, OnInit } from '@angular/core';
import { IpType } from '../../../shared/global';

@Component({
    selector: 'app-carrier-ethernet',
    templateUrl: './carrier-ethernet.component.html'
})

export class CarrierEthernetComponent implements OnInit {

    public type : IpType;

    constructor() {
        this.type = IpType.CarrierEthernet;
    }

    ngOnInit() {

    }
}