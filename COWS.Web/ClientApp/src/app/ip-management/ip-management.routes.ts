import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  CarrierEthernetComponent, SipNatComponent
} from './';

const routes: Routes = [
    { 
      path: 'carrier-ethernet', 
      component: CarrierEthernetComponent, 
      data: { title: "Carrier Ethernet" } 
    },
    { 
      path: 'sip-nat', 
      component: SipNatComponent, 
      data: { title: "SIP / NAT" } 
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IPManagementRoutes { }
