import { CommonModule, DatePipe } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatExpansionModule } from '@angular/material';
import { DevExtremeModule, DxCheckBoxModule, DxDataGridModule, DxDateBoxModule, DxSelectBoxModule, DxSpeedDialActionModule } from 'devextreme-angular';
import {
    IPManagementService
} from '../../services';

import {
    CarrierEthernetComponent,
    SipNatComponent,
} from './index';

import {
    IPManagementTemplateComponent
} from '../custom'

import { IPManagementRoutes } from './ip-management.routes';
import { SharedModule } from '../shared/shared-module';

@NgModule({
  imports: [
    CommonModule, ReactiveFormsModule, SharedModule,
    //Routes
    IPManagementRoutes,
    // DevExtreme Modules 
    DxDataGridModule, DxSelectBoxModule, DxCheckBoxModule, DxDateBoxModule, DevExtremeModule, DxSpeedDialActionModule,
    // Materialize Modules
    MatExpansionModule, MatDialogModule
  ],
  providers: [
    IPManagementService
  ],
  declarations: [
    CarrierEthernetComponent,
    SipNatComponent,
    IPManagementTemplateComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IPManagementModule { }
