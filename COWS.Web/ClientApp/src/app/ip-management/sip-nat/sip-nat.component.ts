import { Component, OnInit } from '@angular/core';
import { IpType } from '../../../shared/global';

@Component({
    selector: 'app-sip-nat',
    templateUrl: './sip-nat.component.html'
})

export class SipNatComponent implements OnInit {

    public type : IpType;

    constructor() {
        this.type = IpType.SipNat;
    }

    ngOnInit() {

    }
}