import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SearchTypes } from '../../search/search.criteria';
import { Helper } from '../../../shared';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrderTypeService, ProductTypeService, CountryService, VendorService } from '../../../services';
import { zip } from 'rxjs';

@Component({
  selector: 'app-ncco-order-details',
  templateUrl: './ncco-order-details.component.html'
})
export class NccoOrderDetailsComponent implements OnInit {
  @Input("parent") parent: any;
  @Input("group") form: FormGroup;
  @Input() showEditButton: boolean

  isSubmitted: boolean = false;
  productTypes: any[];
  orderTypes: any[];
  countries: any[];
  vendors: any[];
  lassies: SearchTypes[]

  customH5Message = {
    maxlength: 'H6 Account Number should be 9 digit long only',
    minlength: 'H6 Account Number should be 9 digit long only',
  }

  get comment() { return this.form.get("comment") }

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private OrderTypeService: OrderTypeService,
    private ProductTypeService: ProductTypeService,
    private CountryService: CountryService,
    private VendorService: VendorService) { }

  ngOnInit() {
    //this.isSubmitted = this.parent.isSubmitted;
    this.LoadData();
    console.log(this.form)
  }

  LoadData() {
    let data = zip(
      this.ProductTypeService.getProductTypeByOrdrCatId(4),
      this.OrderTypeService.get(),
      this.CountryService.get(),
      this.VendorService.get()
    )

    this.spinner.show();
    data.subscribe(res => {
      this.productTypes = res[0]
      this.orderTypes = res[1]
      this.countries = res[2]
      this.vendors = res[3]

      this.lassies = [
        new SearchTypes("Yes", 1, []),
        new SearchTypes("No", 2, [])
      ]

    }, error => {
      this.spinner.hide();
    }, () => {
      this.spinner.hide();
    });
  }

  get commentCharRemaining(): number {
    const comment = this.form.get('comment').value;
    return (comment) ? 500 - comment.length : 500;
  }
}
