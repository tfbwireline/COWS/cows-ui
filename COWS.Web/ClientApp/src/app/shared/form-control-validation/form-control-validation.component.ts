import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormControlValidationService } from '../../../services/form-control-validation.service';

@Component({
  selector: 'app-form-control-validation',
  templateUrl: './form-control-validation.component.html',
  styleUrls: ['./form-control-validation.component.scss'],
})
export class FormControlValidationComponent implements OnInit {
  @Input() control: FormControl;
  @Input() sourceName: string;
  @Input() sourceType: string;
  @Input() customMessage: string;
  @Input() customMessageObj: Object;
  constructor() {}

  ngOnInit() {}

  get errorMessage(): string {
    if (this.control !== undefined) {
      for (const propertyName in this.control.errors) {
        if (this.control.errors.hasOwnProperty(propertyName) && (this.control.dirty || this.control.touched)) {
          return FormControlValidationService.getValidatorErrorMessage(
            propertyName,
            this.control.errors[propertyName],
            this.sourceName,
            this.customMessage,
            this.customMessageObj,
          );
        }
      }
    }
    return '';
  }
}