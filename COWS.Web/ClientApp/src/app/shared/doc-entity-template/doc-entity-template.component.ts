import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService, DocEntityService } from '../../../services/index';
import { DocEntity } from '../../../models/index';
import { Global } from '../../../shared/global';
import { Helper } from "../../../shared/index";
import { saveAs } from 'file-saver';
import { FileSizePipe } from '../../../shared/pipe/file-size.pipe';

@Component({
  selector: 'app-doc-entity-template',
  templateUrl: './doc-entity-template.component.html'
})

  /**
   * Custom control with file uploader and document grid
   * Option includes Document for File upload label field and Grid for Comment column visibility
   * Modules that implemented this control are as follows: Vendor Folder, Vendor Generic Templates
   *
   **/

export class DocEntityTemplateComponent implements OnInit {
  @Input() option: any;
  @Input() docList: DocEntity[] = [];
  @Input() disabled: boolean = false;
  @Output() uploadResult = new EventEmitter<DocEntity>()
  @Output() removedResult = new EventEmitter<number>()
  filename: string = null;
  isCommentShown: boolean = false;

  constructor(private helper: Helper, private usrSrvc: UserService, private docSrvc: DocEntityService, private fileSizePipe: FileSizePipe) { }

  ngOnInit() {
    this.isCommentShown = (!this.helper.isEmpty(this.option) && !this.helper.isEmpty(this.option.grid)
      && !this.helper.isEmpty(this.option.grid.column) && !this.helper.isEmpty(this.option.grid.column.comment)
      && !this.helper.isEmpty(this.option.grid.column.comment.isShown))
      ? this.option.grid.column.comment.isShown : false;

    this.option.inputFileControl.isEnabled = (!this.helper.isEmpty(this.option)
      && !this.helper.isEmpty(this.option.inputFileControl)
      && !this.helper.isEmpty(this.option.inputFileControl.isEnabled))
      ? this.option.inputFileControl.isEnabled : true;
  }

  onFileLoad(result) {
    if (result != null) {
      let file = result.file as File;
      if (!this.helper.isEmpty(this.option.fileSizeValidation)
        && this.option.fileSizeValidation
        && !this.helper.isFileSizeValid(file.size)) {
        this.helper.notify('File size limit of 10MB has been exceeded.', Global.NOTIFY_TYPE_ERROR)
        return;
      }

      let doc = new DocEntity({
        docId: this.getDocId(),
        fileNme: file.name,
        fileSizeQty: file.size,
        fileSizeQtyStr: this.fileSizePipe.transform(file.size),
        base64string: result.base64,
        creatByUserAdId: this.usrSrvc.loggedInUser.adid,
        creatDt: new Date()
      });
      this.docList.push(doc);
      this.uploadResult.emit(doc as DocEntity);
    }
  }

  getDocId(): number {
    if (this.docList != null && this.docList.length > 0) {
      let maxId = this.docList.reduce((max, doc) => doc.docId > max ? doc.docId : max, this.docList[0].docId);
      return maxId + 1;
    } else {
      return 1;
    }
  }

  download(e) {
    let doc = e.row.data as DocEntity;
    if (doc != null) {
      this.docSrvc.download(doc).subscribe(
        res => {
          saveAs(res, doc.fileNme);
        },
        error => {
          this.helper.notify("File not found", Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }

  onRowRemoved(e: any) {
    this.removedResult.emit(e.key);
  }

  formatFileSize(bytes: number = 0): string {
    return this.helper.getFileSizeFormat(bytes);
  }
}
