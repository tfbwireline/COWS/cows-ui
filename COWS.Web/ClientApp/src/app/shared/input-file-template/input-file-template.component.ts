import { Component, OnInit, HostListener, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-input-file-template',
  templateUrl: './input-file-template.component.html',
})
export class InputFileTemplateComponent implements OnInit {
  @Output() result = new EventEmitter<any>()

  private file: File | null = null;

  @HostListener('change', ['$event.target.files']) emitFiles(event: FileList) {

    if(event.item(0) === null) {
      return;
    }

    const file = event && event.item(0);
    this.file = file;

    let fileReader = new FileReader();

    fileReader.onloadend = (e) => {
      //console.log(fileReader.result)
      let res = {
        //byteArray: new Uint8Array(fileReader.result as ArrayBuffer),
        base64: btoa(fileReader.result as string),
        file: this.file
      }
      //console.log(res)
      this.result.emit(res)
    }

    //fileReader.readAsArrayBuffer(this.file);
    fileReader.readAsBinaryString(this.file)
  }

  constructor() {
  }

  ngOnInit() {
  }
  
}
