import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DocEntityService, SiptEventService } from '../../../services/index';
import { Global } from '../../../shared/global';
import { Helper } from '../../../shared/index';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-doc-download',
  templateUrl: './doc-download.component.html'
})
export class DocDownloadComponent implements OnInit {
  message: string;
  id: number = 0;
  constructor(private avRoute: ActivatedRoute, private helper: Helper,
    private docSrvc: DocEntityService, private siptSrvc: SiptEventService) { }

  ngOnInit() {
    this.id = this.avRoute.snapshot.params["id"] || 0;

    if (this.id > 0) {
      this.getDocFile();
    }
    else {
      this.message = 'Sorry, File no longer available.';
    }
  }

  getDocFile() {
    this.siptSrvc.getSiptDocumentById(this.id).toPromise().then(
      doc => {
        if (doc != null) {
          this.docSrvc.download(doc).subscribe(
            res => {
              saveAs(res, doc.fileNme);
            },
            error => {
              this.helper.notify("File not found", Global.NOTIFY_TYPE_ERROR);
            }
          );
        }
      }
    );
  }
}
