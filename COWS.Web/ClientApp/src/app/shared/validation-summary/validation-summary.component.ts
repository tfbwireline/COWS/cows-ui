import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
declare let $: any;

@Component({
  selector: 'app-validation-summary',
  templateUrl: './validation-summary.component.html'
})

export class ValidationSummaryComponent implements OnInit, OnChanges {
  @Input() option: any;
  @Input() errors: string[] = [];

  constructor() { }

  ngOnInit() { }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    for (let propName in changes) {
      let changedProp = changes[propName];
      let errors = JSON.stringify(changedProp.currentValue);
      if (changedProp.isFirstChange()) {
        $("#validation-summary").modal("hide");
      } else {
        if (errors != null && errors != '[]' && errors.length > 0) {
          $("#validation-summary").modal("show");
        } else {
          $("#validation-summary").modal("hide");
        }
      }
    }
  }
}
