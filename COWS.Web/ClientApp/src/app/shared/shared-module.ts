// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumbersOnlyDirective, AlphaNumericOnlyDirective } from '../../directives';

// Libraries
import { AppMaterialModule } from '../app-material.module';
import { AppDxtremeModule } from '../app-dxtreme.module'

import {
  InputFileTemplateComponent, FormControlValidationComponent, DocDownloadComponent,
  DocEntityTemplateComponent, NccoOrderDetailsComponent, AdditionalCostsFormComponent,
  ValidationSummaryComponent

} from './index';

@NgModule({
  declarations: [
    InputFileTemplateComponent,
    FormControlValidationComponent,
    NccoOrderDetailsComponent,
    DocDownloadComponent,
    DocEntityTemplateComponent,
    NumbersOnlyDirective,
    AlphaNumericOnlyDirective,
    ValidationSummaryComponent,
    // AdditionalCostsFormComponent
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    AppDxtremeModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    InputFileTemplateComponent,
    FormControlValidationComponent,
    NccoOrderDetailsComponent,
    DocDownloadComponent,
    DocEntityTemplateComponent,
    NumbersOnlyDirective,
    AlphaNumericOnlyDirective,
    ValidationSummaryComponent,
    // AdditionalCostsFormComponent
  ],
})
export class SharedModule { }
