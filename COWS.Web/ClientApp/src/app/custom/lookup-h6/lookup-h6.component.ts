import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Global, EEventType } from "./../../../shared/global";
import { SearchService } from "./../../../services/";
import { AdEvent, MDSEventNtwkCust } from "./../../../models/";
import { MdsFormComponent } from '../../event';

@Component({
  selector: 'app-lookup-h6',
  templateUrl: './lookup-h6.component.html'
})
export class LookupH6Component implements OnInit {
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input("parent") parent: any;
  @Input() option: any
  @Input() eventType: number // 5 = MDS
  @Output() result = new EventEmitter<MDSEventNtwkCust>()

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private searchService: SearchService) { }

  ngOnInit() {
  }

  lookup() {
    let parent = this.parent as MdsFormComponent;
    let q = this.getFormControlValue(this.option.searchQuery) || null
    
    if (!this.helper.isEmpty(q)) {
      // Proceed to lookup
      if (parent.networkActivityType.value == null) {
        this.helper.notify('Please select a Network Activity Type before performing a H6 lookup', Global.NOTIFY_TYPE_WARNING);
        return;
      }
      this.spinner.show()
      let isCEChange = parent.networkActivityType.value.filter(a => [21].includes(a)).length > 0 as boolean;
      this.searchService.getH6Lookup(q, this.eventType, parent.getVASCECd(), isCEChange).subscribe(res => {
        if (this.eventType == EEventType.MDS) {
          this.result.emit(res)
        } else {
          let adEvent = res as AdEvent
          for (let field of this.option.mappings) {
            this.setFormControlValue(field[0], adEvent[field[1]])
          }
        }
      }, error => {
          this.spinner.hide()
          if (this.eventType == EEventType.MDS) {
            this.result.emit(null)
            this.helper.notify(error.message, Global.NOTIFY_TYPE_ERROR);
          }
      }, () => this.spinner.hide());
    }
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }
}
