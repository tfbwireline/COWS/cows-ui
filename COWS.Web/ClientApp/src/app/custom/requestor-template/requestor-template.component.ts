import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { UserFinderComponent } from "./../user-finder/user-finder.component";

@Component({
  selector: 'app-requestor-template',
  templateUrl: './requestor-template.component.html'
})
export class RequestorTemplateComponent implements OnInit {
  @ViewChild("app-user-finder", {static : true}) userFinder: UserFinderComponent
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input() option: any
  @Input() isSubmitted: boolean

  get id() { return this.form.get('id'); }
  get name() { return this.form.get('name'); }
  get email() { return this.form.get('email'); }
  get phone() { return this.form.get('phone'); }
  get cellphone() { return this.form.get('cellphone'); }
  get bridgeNumber() { return this.form.get('bridgeNumber'); }
  get bridgePin() { return this.form.get('bridgePin'); }

  constructor() { }

  ngOnInit() {
    this.setValidators(this.id, this.option.id)
    this.setValidators(this.name, this.option.name)
    this.setValidators(this.email, this.option.email)
    this.setValidators(this.bridgeNumber, this.option.bridgeNumber)
    this.setValidators(this.bridgePin, this.option.bridgePin)
  }

  setValidators(control: AbstractControl, option: any) {
    if (option != null && option != undefined) {
      let validators = option.validators
      if (validators != null && validators != undefined) {
        control.setValidators(Validators.compose(validators.map(a => a.type) as ValidatorFn[]))
        control.updateValueAndValidity()
      }
    }
  }

  isRequired(name) {
    if (this.option[name].validators != undefined) {
      return this.option[name].validators.find(a => a.name == "required") != undefined
    }

    return false;
  }
}
