import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { DxDataGridComponent } from "devextreme-angular";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Global } from "./../../../shared/global";
import { User } from "./../../../models";
import { UserService } from "./../../../services";

declare let $: any;

@Component({
  selector: 'app-user-finder',
  templateUrl: './user-finder.component.html'
})
export class UserFinderComponent implements OnInit {
  @Input() id: string
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input() option: any
  @Output() searchOccured: EventEmitter<any> = new EventEmitter();

  users: User[]
  selectedItemKeys: any[] = [];
  isQuick: boolean = false;
  @ViewChild('grid', { static: false }) dataGrid: DxDataGridComponent;

  constructor(private spinner: NgxSpinnerService, private helper: Helper, private userService: UserService) { }

  ngOnInit() { }

  search(isQuick: boolean) {
    // Updated by Sarah Sandoval [20200616] - Modified User search for multiple users
    // The option should have isMultiple set to true otherwise it will function as a single search
    let q = "";
    this.isQuick = isQuick;
    if (isQuick) {
      if (this.getFormControlValue(this.option.searchQuery) != null) {
        q = this.getFormControlValue(this.option.searchQuery)

        this.spinner.show()
        this.userService.getByNameOrADID(q, !isQuick).subscribe(res => {
          //console.log(res)
          if (res != null && res.length > 0) {
            this.setRequestorDetails(res[0])
          } else {
            this.helper.notify("No User Found", Global.NOTIFY_TYPE_ERROR)
          }
        }, error => {
          this.spinner.hide()
        }, () => this.spinner.hide());
      }
      else {
        this.helper.notify("Please enter user email or ADID", Global.NOTIFY_TYPE_WARNING)
      }
    }
    else {
      this.users = []; // Clear Userlist
      $("#searchText")[0].value = null; // Clear search text
      $(`#user-picker-${this.id}`).modal("show");
    }
  }

  advanceSearch(val) {
    this.userService.getByNameOrADID(val, false).subscribe(res => {
      if (res != null && res.length > 0) {
        // Show modal
        this.users = res as User[]
      } else {
        this.helper.notify("No User Found", Global.NOTIFY_TYPE_ERROR)
      }
    });
  }

  onRowClick(event) {
    let user = this.users.find(a => a.userId == event.data.userId)
    this.setRequestorDetails(user)

    $(`#user-picker-${this.id}`).modal("hide");
    //if (this.isMultiple && !this.isQuick) {
    //  this.selectedItemKeys.push(event.data.userId);
    //  this.dataGrid.instance.selectRows(this.selectedItemKeys, true);
    //}
    //else {
    //  let user = this.users.find(a => a.userId == event.data.userId)
    //  this.setRequestorDetails(user)

    //  $(`#user-picker-${this.id}`).modal("hide");
    //}
  }

  onToolbarPreparing(e) {
    //e.toolbarOptions.items.unshift({
    //  location: 'before',
    //  template: 'addTemplate'
    //});
  }

  setRequestorDetails(user: User) {
    // Updated by Sarah Sandoval [20200616]
    // Added condition to cater multiple user
    let isMultiple = this.option.isMultiple != null ? this.option.isMultiple : false;
    for (let field of this.option.mappings) {
      if (isMultiple && !this.isQuick) {
        let newUser = user[field[1]];
        if (!this.helper.isEmpty(this.getFormControlValue(field[0]))) {
          newUser = this.getFormControlValue(field[0]) + ', ' + user[field[1]];
        }

        this.setFormControlValue(field[0], newUser);
      }
      else {
        this.setFormControlValue(field[0], user[field[1]]);
      }
    }
    this.searchOccured.emit(true);
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  reset() {
    for (let field of this.option.mappings) {
      this.setFormControlValue(field[0], null)
    }
  }

  add() {
    if (this.selectedItemKeys != null && this.selectedItemKeys.length > 0) {
      for (let i: number = 0; i < this.selectedItemKeys.length; i++) {
        let user = this.users.find(a => a.userId == this.selectedItemKeys[i]);
        if (!this.helper.isEmpty(user))
          this.setRequestorDetails(user)
      }
    }

    $(`#user-picker-${this.id}`).modal("hide");
  }
}
