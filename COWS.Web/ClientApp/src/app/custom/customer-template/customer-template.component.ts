import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

@Component({
  selector: 'app-customer-template',
  templateUrl: './customer-template.component.html',
})
export class CustomerTemplateComponent implements OnInit {
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input() option: any
  @Input() isSubmitted: boolean

  get name() { return this.form.get('name'); }
  get email() { return this.form.get('email'); }
  get phone() { return this.form.get('phone'); }
  get cellphone() { return this.form.get('cellphone'); }
  get contactName() { return this.form.get('contactName'); }
  get pager() { return this.form.get('pager'); }
  get pagerPin() { return this.form.get('pagerPin'); }

  constructor() { }

  ngOnInit() {
    this.setValidators(this.name, this.option.name.validators)
    this.setValidators(this.email, this.option.email.validators)
    this.setValidators(this.contactName, this.option.contactName.validators)
  }

  setValidators(control: AbstractControl, validators: any) {
    if (validators != null && validators != undefined) {
      control.setValidators(Validators.compose(validators.map(a => a.type) as ValidatorFn[]))
      control.updateValueAndValidity()
    }
  }

  isRequired(name) {
    if (this.option[name].validators != undefined) {
      return this.option[name].validators.find(a => a.name == "required") != undefined
    }

    return false;
  }
}

