import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { ContactDetail } from '../../../models';
import { ContactDetailService, SearchService, UserService } from '../../../services';
import { Helper } from '../../../shared';
import { ContactDetailObjType, CPT_STATUS_EMAIL_CD, EVENT_STATUS_EMAIL_CD, HierLevel, KeyValuePair, REDESIGN_STATUS_EMAIL_CD } from '../../../shared/global';
import { zip } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html'
})
export class ContactListComponent implements OnInit {
  @ViewChild('grid', { static: true }) dataGrid: DxDataGridComponent;
  @Output() result = new EventEmitter<ContactDetail[]>()
  @Input('contactList') contactList: ContactDetail[] = [];
  @Input("parent") parent: any;
  @Input() objId: number;   // Sample Values: EventID, RedesignID, CPTID
  @Input() objType: string; // Sample Values: E (Event), R (Redesign), C (CPT)
  @Input() disabled: boolean = true;
  hierId: string = '';  // Sample Values: 9 digit number value of H1/H2/H4/H6/OD
  hierLvl: string = ''; // Sample Values: H1, H2, H4, H6, OD
  odieCustId: string; 
  statusList: KeyValuePair[] = [];
  roleList: KeyValuePair[] = [];
  hierLvlList: KeyValuePair[] = [];
  hierIdPattern: any = /^\d{9}$/i;
  mdsHierId: string;
  mdsHierLvl: string;
  networkHierId: string
  networkHierLvl: string;
  isRedesign: boolean = false;
  isNetworkOnly: boolean = false;

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private srvc: ContactDetailService, private searchSrvc: SearchService, public userService: UserService) {
    this.isEditAllowed = this.isEditAllowed.bind(this);
    this.isDeleteAllowed = this.isDeleteAllowed.bind(this);
    }

  // For Creating new CPT/IPSD/Redesign form Refresh button will load Contact Details info
  // For Updating CPT/IPSD/Redesign, call the ContactDetailService.getContactDetails on respective form loaddata event and pass the contactList
  ngOnInit() {
    this.objId = this.helper.isEmpty(this.objId) ? 0 : this.objId;
    this.onLoad();
  }

  onLoad() {
    // Load Status Workflow and HierLvl
    // Add HierLvl value if necessary: CPT and Redesign always uses H1
    if (!this.helper.isEmpty(this.objType)) {
      if (this.objType === ContactDetailObjType.CPT) {
        this.statusList = this.helper.getEnumKeyValuePair(CPT_STATUS_EMAIL_CD);
        this.hierLvl = HierLevel.H1;
      }
      else if (this.objType === ContactDetailObjType.Event) {
        this.statusList = this.helper.getEnumKeyValuePair(EVENT_STATUS_EMAIL_CD);
        // Shelly requested to remove Visible, Submitted, Pending, InPorgress, Fullfilling, OnHold through an ALM ticket (1924733)
        this.statusList = this.statusList.filter(i => [2, 4, 10, 3, 6].includes(i.value));
        this.hierLvl = HierLevel.H1;
      }
      else if (this.objType === ContactDetailObjType.Redesign) {
        this.statusList = this.helper.getEnumKeyValuePair(REDESIGN_STATUS_EMAIL_CD);
        this.hierLvl = HierLevel.H1;
        this.isRedesign = true;
      }

      //if (this.statusList.length > 0) {
      //  this.statusList.unshift(new KeyValuePair("No Email", 0));
      //}
    }

    // Load Roles
    //this.roleList = this.helper.getEnumKeyValuePair(ERole);
    let cntctDtlRoles = ["IPMP", "IPMS", "CSM", "IPMD", "IS", "ISS", "SE", "MNS-PM", "NTE", "SDE"];
    //console.log(cntctDtlRoles.join(','))
    this.searchSrvc.getRolesByRoleCodes(cntctDtlRoles.join(',')).subscribe(res => {
      this.roleList = res;
      //console.log(this.roleList)
    });

    // Load Hier Level List
    this.hierLvlList = this.helper.getEnumKeyValuePair(HierLevel);
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'refreshTemplate'
    });
  }

  onInitContactDetailNewRow(e) {
    //console.log(e)
    e.data.objId = this.objId;
    e.data.objTypCd = this.objType;
    e.data.autoRfrshCd = false;
    e.data.suprsEmail = false;
    this.dataGrid.instance.columnOption('hierId', 'allowEditing', true);
    this.dataGrid.instance.columnOption('hierLvlCd', 'allowEditing', true);
    this.dataGrid.instance.columnOption('roleNme', 'allowEditing', true);
    if (!this.helper.isEmpty(e.data.emailCdIds) && e.data.emailCdIds.includes(0)) {
      this.statusList.forEach(i => i.disabled = (i.value !== 0));
    }
    else {
      this.statusList.forEach(i => i.disabled = false);
    }
  }

  onContactDetailEditingStart(e) {
    let isSuppressEmailEnabled = this.objType == 'E' ? this.parent.isSuppressEmailEnabled : true
    this.dataGrid.instance.columnOption('hierId', 'allowEditing', false);
    this.dataGrid.instance.columnOption('hierLvlCd', 'allowEditing', false);
    this.dataGrid.instance.columnOption('roleNme', 'allowEditing', false);
    this.dataGrid.instance.columnOption('suprsEmail', 'allowEditing', isSuppressEmailEnabled);
    if (!this.helper.isEmpty(e.data.emailCdIds) && e.data.emailCdIds.includes(0)) {
      this.statusList.forEach(i => i.disabled = (i.value !== 0));
    }
    else {
      this.statusList.forEach(i => i.disabled = false);
    }
  }

  onContactDetailSaved(e) {
    console.log(e)
    e.data.creatByUserId = e.data.creatByUserId == 1 ? e.data.creatByUserId : this.userService.loggedInUser.userId
    e.data.emailCdIds = e.data.emailCdIds.includes(0) ? [0] : e.data.emailCdIds;
    e.data.emailCdTxt = this.getStatusText(e.data.emailCdIds || []);
    //e.data.roleId = e.data.roleId;
    e.data.emailCd = e.data.emailCdIds.join(',');
    if (this.helper.isEmpty(e.data.roleId)) {
      e.data.roleId = e.data.roleNme;
      e.data.roleNme = this.roleList.filter(i => i.value === e.data.roleId)[0].description;
    }
    if(e.data['id'] == undefined) {
      e.data.allowEdit = true;
    }
    
    this.result.emit(this.contactList)
  }

  onStatusOpened(e) {
    //console.log(e)
    e.component.content().querySelector('.dx-list-select-all').style.display = 'none';
  }

  onStatusChanged(val, rowItem) {
    //console.log('onStatusChanged')
    //console.log(val)
    rowItem.setValue(val);
    //if (val.includes(0)) {
    //  this.statusList.forEach(i => i.disabled = (i.value !== 0));
    //}
    //else {
    //  this.statusList.forEach(i => i.disabled = false);
    //}
  }

  refreshContact() {
    this.hierLvl = 'H1'
    this.odieCustId = ''
    this.isNetworkOnly = false
    console.log([`parent.odieCustId`, this.parent.odieCustId])
    if (this.objType === ContactDetailObjType.CPT) {
      this.hierId = this.parent.h1.value;
      this.getContactList()
    }
    else if (this.objType === ContactDetailObjType.Redesign) {
      this.hierId = this.parent.h1.value;
      this.odieCustId = this.parent.odieCustId.value
      this.getContactList()
    }
    else if (this.objType === ContactDetailObjType.Event) {
      if (this.parent.showMDS) {
        if (this.parent.mdsH1.value != '999999999') {
          this.mdsHierId = this.parent.mdsH1.value
          this.odieCustId = this.parent.odieCustId.value
        } else {
          this.mdsHierId = this.parent.mdsH6.value
          this.hierLvl = 'H6'
        }
      }

      if (this.parent.showNetwork) {
        this.isNetworkOnly = this.parent.isNetworkOnly
        this.networkHierId = this.parent.networkH1.value
      }

      console.log(['isNetworkOnly', this.isNetworkOnly])
      this.hierId = this.mdsHierId || this.networkHierId || ''

      if (this.hierId != '' && this.hierLvl != '') {
        this.getContactList()
      }
    }
  }

  getContactList() {
    console.log(['objId', this.objId])
    console.log(['objType', this.objType])
    console.log(['hierId', this.hierId])
    console.log(['hierLvl', this.hierLvl])
    console.log(['hierLvl', this.hierLvl])
    console.log(['odieCustId', this.odieCustId])
    console.log(['isNetworkOnly', this.isNetworkOnly])
    let data = zip(
      this.srvc.getContactDetails(this.objId, this.objType, this.hierId, this.hierLvl, this.odieCustId, this.isNetworkOnly)
    )

    this.spinner.show();
    data.toPromise().then(
      res => {
        console.log(['upstream', res])
        let contactList: ContactDetail[] = (res[0] as ContactDetail[]) || [];
        contactList.forEach(item => {
          if (!this.helper.isEmpty(item.emailCd)) {
            item.emailCdIds = item.emailCd.split(',').map(Number)
            item.emailCdTxt = this.getStatusText(item.emailCdIds || []);
          }

          if (item.id != 0 && (item.creatByUserId > 1 && item.creatByUserId != this.userService.loggedInUser.userId)) {
            item.allowEdit = false;
          } else {
            item.allowEdit = true;
          }
        });

        console.log(['contactList', contactList])

        //if (this.objId == 0 && this.contactList.length == 0) {
          this.contactList = contactList
        //} else {
        //  if (this.objType == 'R') {
        //    console.log(['this.contactList', this.contactList])
        //    this.contactList = this.contactList.filter(a => {
        //      return contactList.filter(b => a.creatByUserId > 1
        //        && b.hierId == a.hierId
        //        && b.hierLvlCd == a.hierLvlCd
        //        && b.roleId == a.roleId).length == 0
        //    })
        //    console.log(['this.contactList', this.contactList])
        //  }

        //  // Refresh existing records if AutoRefresh is enabled
        //  this.contactList.forEach(a => {
        //    let row = contactList.find(b =>
        //      ((b.id != 0 && b.id == a.id)
        //        || (b.hierId == a.hierId && b.hierLvlCd == a.hierLvlCd && b.roleNme == a.roleNme && b.emailAdr == a.emailAdr))
        //      && a.autoRfrshCd)
        //    if (row) {
        //      a.phnNbr = row.phnNbr
        //    }
        //  })

        //  // Append new records from upstream
        //  for (let value of contactList) {
        //    if (!this.compareContactList(value, this.contactList))
        //      this.contactList.push(value)
        //  }
        //}

        console.log(['finalContactList', this.contactList])

        this.result.emit(this.contactList)
      }
    ).then(() => { this.spinner.hide(); });
  }

  getStatusText(selectedStatus: number[]) {
    //const selectedStatus: string[] = e.data.emailCd || [];
    const selectedStatusText: string[] = [];
    if (selectedStatus.length > 0) {
      if (selectedStatus.includes(0)) {
        selectedStatusText.push("No Email")
      }
      else {
        if (this.objType === ContactDetailObjType.CPT) {
          selectedStatus.forEach(item => {
            selectedStatusText.push(CPT_STATUS_EMAIL_CD[item])
          });
        } else if (this.objType === ContactDetailObjType.Redesign) {
          selectedStatus.forEach(item => {
            selectedStatusText.push(REDESIGN_STATUS_EMAIL_CD[item])
          });
        } else if (this.objType === ContactDetailObjType.Event) {
          selectedStatus.forEach(item => {
            selectedStatusText.push(EVENT_STATUS_EMAIL_CD[item])
          });
        }
      }
    }

    return selectedStatusText.join(',');
  }

  compareContactList(newValue: ContactDetail, oldValue: ContactDetail[]): boolean {
    return oldValue.filter(a =>
      a.hierId == newValue.hierId &&
      a.hierLvlCd == newValue.hierLvlCd &&
      a.roleNme == newValue.roleNme &&
      a.emailAdr == newValue.emailAdr)
      .length > 0 ? true : false
  }


  isEditAllowed(e) {
    return e.row.data.allowEdit;
  }


  isDeleteAllowed(e) {
    return e.row.data.creatByUserId > 1;
  }
}
