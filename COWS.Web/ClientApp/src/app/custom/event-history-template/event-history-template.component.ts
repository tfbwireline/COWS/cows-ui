import { Component, OnInit, Input } from '@angular/core';
import { EventHistoryService } from '../../../services';
import { Helper } from '../../../shared/index';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-event-history-template',
  templateUrl: './event-history-template.component.html'
})
export class EventHistoryTemplateComponent implements OnInit {
  @Input("id") id: number // FormGroup passed from parent
  @Input("accordion") accordion: boolean
  @Input("expanded") expanded: boolean = true
  eventHistoryList: any

  constructor(private helper: Helper, private eventHistoryService: EventHistoryService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.eventHistoryService.getByEventId(this.id).subscribe(res => {
      this.eventHistoryList = res

      this.eventHistoryList = this.eventHistoryList.map(a => {
        a.cmntTxt = this.sanitizer.bypassSecurityTrustHtml(a.cmntTxt)

        return a
      })
    });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = 'EventHistory_' + this.helper.formatDate(new Date());
  }
}
