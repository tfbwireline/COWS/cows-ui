import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { ActivatorsService } from '../../../services';
import { FormControlItem } from '../../../models';
import { zip } from 'rxjs';
import { KeyValuePair, RB_CONDITION } from '../../../shared/global';
import { Helper } from "./../../../shared";

@Component({
  selector: 'app-activators-template',
  templateUrl: './activators-template.component.html'
})

export class ActivatorsTemplateComponent implements OnInit {
  @Input("group") form: FormGroup; // FormGroup passed from parent
  @Input() option: any
  @Input() isSubmitted: boolean;
  @Input("eventTypeId") eventTypeId: number;

  activatorActivityList = {
    successfulActivities: [] as FormControlItem[],
    failedActivities: [] as FormControlItem[]
  }

  failedCodesList: FormControlItem[];
  preconfigureList: KeyValuePair[];

  constructor(private activatorsService: ActivatorsService, private helper: Helper) { }

  ngOnInit() {
    let data = zip(
      this.activatorsService.GetAllSuccessActivities(this.eventTypeId),
      this.activatorsService.GetAllFailActivities(this.eventTypeId),
      this.activatorsService.GetAllFailCodes()
    );

    this.preconfigureList = this.helper.getEnumKeyValuePair(RB_CONDITION);
    if (data != null) {
      data.subscribe(res => {
        this.patchResult("successfulActivities", res[0] as FormControlItem[])
        this.patchResult("failedActivities", res[1] as FormControlItem[])
        this.failedCodesList = res[2] as FormControlItem[]
      })
    }
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value
    }
    return value;
  }

  patchResult(list, result: FormControlItem[]) {
    this.activatorActivityList[list] = result

    this.form.controls[list] = new FormArray([])
    for (let activity of this.activatorActivityList[list]) {
      this.addActivities(list)
    }
  }

  addActivities(activity) {
    const check = this.form.controls[activity] as FormArray;
    check.push(new FormControl(false));
  }

  onSuccessfulValueChanged() {
    let controls = this.form.get('successfulActivities') as FormArray

    var selectedSuccessfulActs = (controls.value as boolean[])
      .map((value, index) => value ? parseInt(this.activatorActivityList.successfulActivities[index].value) : null)
      .filter(value => value !== null);

    this.setFormControlValue("selectedSuccessfulActs", selectedSuccessfulActs)
  }

  onFailedValueChanged() {
    let controls = this.form.get('failedActivities') as FormArray

    var selectedFailedActs = (controls.value as boolean[])
      .map((value, index) => value ? parseInt(this.activatorActivityList.failedActivities[index].value) : null)
      .filter(value => value !== null);

    this.setFormControlValue("selectedFailedActs", selectedFailedActs)
  }
}
