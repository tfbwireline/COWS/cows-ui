import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { OrderNoteService } from '../../../services';

@Component({
  selector: 'app-order-note-template',
  templateUrl: './order-note-template.component.html'
})
export class OrderNoteTemplateComponent implements OnInit {
  @Input("orderId") orderId: number // FormGroup passed from parent
  @Input("noteTypeId") noteTypeId: number // FormGroup passed from parent
  @Input("sortExpression") sortExpression: string // FormGroup passed from parent
  @Input("PrntPrfID") PrntPrfID: number // FormGroup passed from parent
  @Input("userID") userID: number // FormGroup passed from parent
  @Input("accordion") accordion: boolean
  orderNoteList: any

  constructor(private orderNoteService: OrderNoteService) { }

  ngOnInit() {
    //console.log(this.id)

    this.orderNoteService.FindForGivenOrderAndNoteType(this.orderId, this.noteTypeId, this.sortExpression, this.PrntPrfID, this.userID).subscribe(res => {
      //console.log(res)
      console.log(res);
      this.orderNoteList = res
    });
  }
}
