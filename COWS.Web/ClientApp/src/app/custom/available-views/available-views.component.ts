import { Component, OnInit, Input, ViewChild, NgModule } from '@angular/core';
import { DxDataGridComponent,DxDataGridModule } from "devextreme-angular";
import { EventViewsService, EventLockService, UserService } from '../../../services';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper, SpinnerAsync } from '../../../shared';
import { Router } from '@angular/router';
import { Global, EEventType } from '../../../shared/global';

@Component({
  selector: 'app-available-views',
  templateUrl: './available-views.component.html'
})
 @NgModule({
    imports: [
      DxDataGridModule
    ],
})
export class AvailableViewsComponent implements OnInit {
  @Input() eventType: number;
  @Input() eventJobAid: string;
  @Input() eventJobAidUrl: string;
  userId: number;
  availableViewsList: any;
  selectedViewId: any;
  eventViewDetailsList = <any>[];
  availableDefaultViewID: number;
  viewOptions: any;
  @ViewChild('grid', { static: true }) dataGrid: DxDataGridComponent;

  isActivator: boolean = false;
  isReviewer: boolean = false;
  isMember: boolean = false;

  spnr: SpinnerAsync;


  get displayNew() {
    return this.isMember || this.isReviewer;
  }

  get viewOptionFlg() {
    return (!((this.eventType == EEventType.AD) || (this.eventType == EEventType.MPLS) || (this.eventType == EEventType.NGVN) || (this.eventType == EEventType.SprintLink)))
  }

  constructor(private helper: Helper, private router: Router, private spinner: NgxSpinnerService,
    private eventService: EventViewsService, private userService: UserService,
    private eventLockSrvc: EventLockService) { 
      this.spnr = new SpinnerAsync(spinner);
    }

  ngOnInit() {
    this.userId = Number(localStorage.getItem('userID'));
    this.pageLoad();

    // Set Profiles
    if (this.eventType == EEventType.MDS) {
      this.isActivator = this.userService.isMdsActivator();
      this.isReviewer = this.isActivator ? false : this.userService.isMdsReviewer();
      this.isMember = this.isActivator ? false : this.userService.isMdsMember();
    }
    else if (this.eventType == EEventType.SIPT || this.eventType == EEventType.UCaaS) {
      this.isActivator = this.userService.isSIPTnUCaaSActivator();
      this.isReviewer = this.isActivator ? false : this.userService.isSIPTnUCaaSReviewer();
      this.isMember = this.isActivator ? false : this.userService.isSIPTnUCaaSMember();
    }
    else if (this.eventType == EEventType.AD || this.eventType == EEventType.MPLS
      || this.eventType == EEventType.NGVN || this.eventType == EEventType.SprintLink) {
      this.isActivator = false;
      this.isReviewer = false;
      this.isMember = false;
    }
  }
  
  pageLoad(): void {
    this.viewOptions = [
      { "id": 1, "name": "Create View" },
      { "id": 2, "name": "Modify View" },
      { "id": 3, "name": "Delete View" }
    ]

    this.getEventViews(this.userId, this.eventType);
  }

  contentReadyHandler(e) {
    // For Search Panel
    if (!this.helper.isEmpty(this.dataGrid.searchPanel.text.trim())) {
      this.dataGrid.instance.searchByText(this.dataGrid.searchPanel.text.trim())
    }
    
    // Grid Column Options
    this.dataGrid.instance.columnOption('eventID', 'cellTemplate', 'eventIdTemplate');
    this.dataGrid.instance.columnOption('eventID', 'allowResizing', false);
    this.dataGrid.instance.columnOption('eventID', 'allowReordering', false);
    this.dataGrid.instance.columnOption('isLocked', 'visible', false);
    this.dataGrid.instance.columnOption('lockedBy', 'visible', false);

    if (this.eventType == EEventType.AD) {
      this.dataGrid.instance.columnOption('ad Type', 'caption', 'AD Type');
    }
    else if (this.eventType == EEventType.UCaaS) {
      this.dataGrid.instance.columnOption('cpE/NCR Required', 'caption', 'CPE/NCR Required');
      this.dataGrid.instance.columnOption('mns Team PDL', 'caption', 'MNS Team PDL');
      this.dataGrid.instance.columnOption('uCaaS PM', 'caption', 'UCaaS PM');
      this.dataGrid.instance.columnOption('start Time', 'dataType', 'datetime');
      this.dataGrid.instance.columnOption('start Time', 'format', 'MM/dd/yyyy hh:mm:ss a');
    }
    else if (this.eventType == EEventType.Fedline) {
      this.dataGrid.instance.columnOption('frb Order Request ID', 'caption', 'FRB Order Request ID');
      this.dataGrid.instance.columnOption('mds Activity  Type', 'caption', 'MDS Activity Type');
      this.dataGrid.instance.columnOption('mns Activator', 'caption', 'MNS Activator');
      this.dataGrid.instance.columnOption('fedline Order Submission Date', 'dataType', 'datetime');
      this.dataGrid.instance.columnOption('fedline Order Submission Date', 'format', 'MM/dd/yyyy hh:mm:ss a');
      this.dataGrid.instance.columnOption('start Date and Time', 'dataType', 'datetime');
      this.dataGrid.instance.columnOption('start Date and Time', 'format', 'MM/dd/yyyy hh:mm:ss a');
    }
    else if (this.eventType == EEventType.MDS) {
      this.dataGrid.instance.columnOption('start Time', 'dataType', 'datetime');
      this.dataGrid.instance.columnOption('start Time', 'format', 'yyyy-MM-dd hh:mm:ss');
      this.dataGrid.instance.columnOption('end Time', 'dataType', 'datetime');
      this.dataGrid.instance.columnOption('end Time', 'format', 'yyyy-MM-dd hh:mm:ss');
      this.dataGrid.instance.columnOption('ipm Description', 'caption', 'IPM Description');
      this.dataGrid.instance.columnOption('cpE/NCR Required', 'caption', 'CPE/NCR Required');
      this.dataGrid.instance.columnOption('mns Team PDL', 'caption', 'MNS Team PDL');
    }
    else if (this.eventType == EEventType.SIPT) {
      this.dataGrid.instance.columnOption('chars ID', 'caption', 'CHARS ID');
      this.dataGrid.instance.columnOption('customer Request Start Date', 'dataType', 'datetime');
      this.dataGrid.instance.columnOption('customer Request Start Date', 'format', 'MM/dd/yyyy hh:mm:ss a');
      this.dataGrid.instance.columnOption('customer Request End Date', 'dataType', 'datetime');
      this.dataGrid.instance.columnOption('customer Request End Date', 'format', 'MM/dd/yyyy hh:mm:ss a');
    }
  }

  onCellPrepared(e) {
    if (e.rowType == 'data') {            
      if (e.column.dataField.valueOf() == "is Escalation"
        || e.column.dataField.valueOf() == "network Intl") 
        e.cellElement.style.textAlign = 'center';
      else
       e.cellElement.style.textAlign = 'left';
    }
    if (e.rowType == 'header') {
      if (e.column.dataField.valueOf() == "is Escalation"
        || e.column.dataField.valueOf() == "network Intl")
        e.cellElement.style.textAlign = 'center';
      else
        e.cellElement.style.textAlign = 'left';
    }
  }

  onRowPrepared(e) {
    if (e.rowType == 'data' && !this.helper.isEmpty(e.key)
      && !this.helper.isEmpty(e.data.isLocked)) {
      e.rowElement.className = e.rowElement.className + ' checked-in-row';
      e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
      e.rowElement.title = 'This event is locked out by ' + e.data.lockedBy;
    }
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = EEventType[this.eventType] + '_' + this.helper.formatDate(new Date());
  }

  selectionChangedHandler(id) {
    if (this.eventType == 1)
      this.router.navigate(['event/access-delivery/' + id]);
    else if (this.eventType == 2)
      this.router.navigate(['event/ngvn/' + id]);
    else if (this.eventType == 3)
      this.router.navigate(['event/mpls/' + id]);
    else if (this.eventType == 4)
      this.router.navigate(['event/sprint-link/' + id]);
    else if (this.eventType == 5)
      this.router.navigate(['event/mds/' + id]);
    else if (this.eventType == 9)
      this.router.navigate(['event/fedline/' + id]);
    else if (this.eventType == 10)
      this.router.navigate(['event/sipt/' + id]);
    else if (this.eventType == 19)
      this.router.navigate(['event/ucaas/' + id]);
  }

  getEventViews(userId: number, siteCntnt: number) {
    this.eventService.getEventViews(userId, siteCntnt).subscribe(
      data => {
        this.availableViewsList = data;
        if (!this.helper.isEmpty(localStorage.getItem('selectedEventViewId')) && this.availableViewsList.find(a => a.dspL_VW_ID == localStorage.getItem('selectedEventViewId'))) {
          this.availableDefaultViewID = Number(localStorage.getItem('selectedEventViewId'));
        }
        else {
          this.availableDefaultViewID = this.availableViewsList[0].dspL_VW_ID;
          localStorage.setItem('selectedEventViewId', this.availableViewsList[0].dspL_VW_ID);
        }
        //this.getViewData(this.availableDefaultViewID);
      },
      error => {
        console.log(error);
      });
  }

  getViewData(value) {
    this.spnr.manageSpinner('show');
    this.selectedViewId = value;
    localStorage.setItem('selectedEventViewId', value);
    if (this.availableViewsList.find(a => a.dspL_VW_ID == value).pblC_VW_CD == 'Y') {
      this.viewOptions.splice(1, 2);
    }
    else {
      this.viewOptions = [
        { "id": 1, "name": "Create View" },
        { "id": 2, "name": "Modify View" },
        { "id": 3, "name": "Delete View" }
      ]
    }

    this.eventService.getEventViewDetails(value, this.eventType, this.userId, "0").subscribe(
      data => {
        this.eventViewDetailsList = data;

        // [89] - All Pending; [91] - All Items; [92] - All Rework;
        // [93] - All Published; [94] - All Completed; [95] - All Visible
        if (this.selectedViewId == 89 || this.selectedViewId == 91 || this.selectedViewId == 92
          || this.selectedViewId == 93 || this.selectedViewId == 94 || this.selectedViewId == 95) {
          this.eventViewDetailsList.forEach(item => {
            var custStartDate = new Date(item["customer Request Start Date"])
            var custEndDate = new Date(item["customer Request End Date"])
            item["customer Request Start Date"] = this.helper.formatDate(custStartDate);
            item["customer Request End Date"] = this.helper.formatDate(custEndDate);
          })
        }
      },
      error => {
        console.log(error);
      }).add(() => {
        this.spnr.manageSpinner('hide');
      });
  }

  createView(value) {
    if (value == 1) {
      this.router.navigate(['event/create-view/' + this.eventType + '/' + 0]);
    }
    else if (value == 2) {
      this.router.navigate(['event/create-view/' + this.eventType + '/' + this.selectedViewId]);
    }
    else if (value == 3) {
      if (confirm("Are you sure you want to delete the selected view?")) {
        this.spnr.manageSpinner('show');
        this.eventService.DeleteView(this.selectedViewId).toPromise().then(
          data => {
            if (data) {
              this.helper.notify(`Successfully Deleted Event View`, Global.NOTIFY_TYPE_SUCCESS);
              this.pageLoad();
              this.selectionChangedHandler('');
            }
            else {
              this.helper.notify(`Failed to Delete Event View. `, Global.NOTIFY_TYPE_ERROR);
            }
          },
          error => {
            this.helper.notify(`Failed to Delete Event View. ` + error.message, Global.NOTIFY_TYPE_ERROR);
          }).then(() => {
            this.spnr.manageSpinner('hide');
          });
      }
    }
  }
}
