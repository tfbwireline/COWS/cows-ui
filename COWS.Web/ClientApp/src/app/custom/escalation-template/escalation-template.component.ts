import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, ValidatorFn, FormArray, FormControl } from "@angular/forms";
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from 'rxjs';
import { EscalationReason } from "./../../../models/";
import { EscalationReasonService, UserService, SystemConfigService } from "./../../../services/";
import { Helper } from "./../../../shared";
import { KeyValuePair, EEventType, EVENT_STATUS } from "./../../../shared/global";
import { MdsFormComponent } from '../../event';
import * as moment from 'moment';

@Component({
  selector: 'app-escalation-template',
  templateUrl: './escalation-template.component.html'
})

export class EscalationTemplateComponent implements OnInit {
  @Input("parent") parent: any; // Parent (Used for MDS)
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input() eventType: number;
  @Input() option: any;
  @Output() result = new EventEmitter<any>()
  @Input() isMDS: boolean;

  get escalationReason() { return this.form.get('escalationReason'); }
  get primaryRequestDate() { return this.form.get('primaryRequestDate'); }
  get secondaryRequestDate() { return this.form.get('secondaryRequestDate'); }
  get escalationBusinessJustification() { return this.form.get('escalationBusinessJustification'); }
  get escalationPolicy() { return this.form.get('escalationPolicy'); }

  get isNtwkIntl() { return this.parent.activityType.value != null ? this.parent.activityType.value.filter(a => [4].includes(a)).length > 0 : false }

  escalationReasonList: EscalationReason[];
  escalationBusJustificationLength: number = 0;
  useConfBridgeList: KeyValuePair[];
  bridgeUrl: string;

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    private escalationReasonService: EscalationReasonService, private systemConfigService: SystemConfigService,
    private userSrvc: UserService
  ) { }

  ngOnInit() {
    this.useConfBridgeList = [
      new KeyValuePair("N/A", 0),
      new KeyValuePair("No", 1),
      new KeyValuePair("Yes, Submitter", 2),
      new KeyValuePair("Yes, MNS", 3),
      new KeyValuePair("Yes, Other", 4)
    ]

    let data = zip(
      this.escalationReasonService.get()
    )

    //this.spinner.show();
    data.subscribe(res => {
      this.escalationReasonList = (res[0] as EscalationReason[]).filter(a => a.recStusId)
      if(this.isMDS) {
        let escReason = new EscalationReason();
        this.escalationReasonList.unshift(escReason);
      }
    }, error => {
      //this.spinner.hide();
    }, () => {
      //this.spinner.hide(); 
    });
  }

  onIsEscalationChanged(e) {
    //console.log(this.eventType);  
    if (e.value == false) {
      if (this.eventType == EEventType.MDS) {
        let parent = this.parent as MdsFormComponent
        //console.log(`isShown: ` + parent.option.schedule.checkCurrentSlot.isShown);
        parent.option.schedule.checkCurrentSlot.isShown = true;
      }
      let primaryRequestDate = new Date().setHours(new Date().getHours(), 0, 0, 0)
      this.setFormControlValue("escalationReason", 1)
      this.setFormControlValue("primaryRequestDate", primaryRequestDate)
      this.setFormControlValue("secondaryRequestDate", this.helper.dateAdd(primaryRequestDate, 'hour', 1))
      this.clearValidators(this.form)
    }
    else {
      this.setFormControlValue("escalationReason", 0);
      let data1 = zip(
        this.systemConfigService.getSysCfgByName("MDSESCPolicyURL")
      )

      //this.spinner.show();
      data1.subscribe(res => {
        // this.form.get('escalationPolicy').setValue(res[0][0].prmtrValuTxt);
        this.setFormControlValue("escalationPolicy", res[0][0].prmtrValuTxt)
      }, error => {
        //this.spinner.hide();
      }, () => {
        //this.spinner.hide(); 
      });

      // Updated by Sarah Sandoval [20220218]
      // Hiding of Check Cusrrent Slot and reset assigned should happen for all types for member only
      //if (this.eventType == EEventType.MDS) {
        let parent = this.parent as MdsFormComponent
        //console.log(`isShown: ` + parent.option.schedule.checkCurrentSlot.isShown);
        if (parent.isOnlyMember) {
          parent.option.schedule.checkCurrentSlot.isShown = false;
          //parent.userService.isSlotPickerShown = false;
          //if (!this.isNtwkIntl) {
            //console.log('escalation - isNtwkIntl: ' + this.isNtwkIntl)
            if (parent.eventStatus.value !== EVENT_STATUS.Published)
              parent.dropAssignee();
          //}
        }
      //}

      let validators = this.option
      this.setValidators(this.form, "escalationReason", this.getValidators(validators.escalationReason))
      this.setValidators(this.form, "primaryRequestDate", this.getValidators(validators.primaryRequestDate))
      this.setValidators(this.form, "secondaryRequestDate", this.getValidators(validators.secondaryRequestDate))
      this.setValidators(this.form, "secondaryRequestDate", [...this.getValidators(validators.secondaryRequestDate), this.isNotPastOneHour(this.form.get("primaryRequestDate") as FormControl)])
      this.setValidators(this.form, "escalationBusinessJustification", this.getValidators(validators.escalationBusinessJustification))
    }
  }

  isNotPastOneHour(dateToCompare: FormControl): any {
    return (control: FormControl): { [key: string]: any } => {
      let primaryRequestDate = moment(dateToCompare.value).format('MM/DD/YYYY hh:mm a')
      let secondaryRequestDate = moment(control.value).format('MM/DD/YYYY hh:mm a')
      let diff = moment(secondaryRequestDate).diff(primaryRequestDate, 'minutes');

      //console.log(`PRD: ${primaryRequestDate}`)
      //console.log(`SRD: ${secondaryRequestDate}`)
      //console.log(`DIFF: ${diff}`)
      return (diff < 60) ? { notPastOneHour: true } : null
    };
  }

  onPrimaryRequestDate(e) {
    let primaryRequestDate = this.form.get("primaryRequestDate").value
    this.setFormControlValue("secondaryRequestDate", this.helper.dateAdd(primaryRequestDate, 'hour', 1))
    //console.log(this.form)
  }

  openEscalationPolicy() {
    this.spinner.show()
    this.systemConfigService.getSysCfgByName("MDSESCPolicyURL").subscribe(res => {
      window.open(res[0].prmtrValuTxt, 'popUpWindow', 'height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes')
      this.spinner.hide()
    }, error => {
      this.spinner.hide()
    })
    //window.open("https://doc-share.corp.sprint.com/livelink/llisapi.dll/fetch/2000/96754/96653/6741118/18766013/20549430/40919586/40929707/42821262/84192381/MDS_Implementation_Escalation_Guidelines.pdf?nodeid=50148599&vernum=-2", "_blank", "toolbar=0,location=0,menubar=0")
  }

  onEscalationBusJustificationChange(event) {
    this.escalationBusJustificationLength = this.helper.isEmpty(this.escalationBusinessJustification.value)
      ? 0 : this.escalationBusinessJustification.value.length;
  }

  onConfBridgeChanged(e) {
    if (e.value > 1) {
      let userId = 0;

      if (e.value == 2) {
        userId = this.helper.getFormControlValue("requestor.id");
      }
      else if (e.value == 3) {
        userId = this.helper.getFormControlValue("schedule.assignedToId");
      }

      userId = this.helper.isEmpty(userId) ? 0 : userId;

      this.userSrvc.GetConferenceBridgeInfo(userId).toPromise().then(
        res => {
          if (res != null && res != 'null' && res.length > 0) {
            this.bridgeUrl = res[0].onlineMeetingAdr;
            this.result.emit(res);
          }
        }
      );
    }
  }

  openBridgeUrl() {
    if (!this.helper.isEmpty(this.bridgeUrl)) {
      window.open(this.bridgeUrl, "_blank", "toolbar=0,location=0,menubar=0")
    }
    return false;
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  isRequired(name) {
    if (this.option[name].validators != undefined) {
      return this.option[name].validators.find(a => a.name == "required") != undefined
    }

    return false;
  }

  setValidators(form: FormGroup, controlName: string, validators: ValidatorFn[]) {
    form.get(controlName).setValidators(validators)
    form.get(controlName).updateValueAndValidity()
  }

  getValidators(option: any): ValidatorFn[] {
    if (option != null && option != undefined) {
      let validators = option.validators
      if (validators != null && validators != undefined) {
        return validators.map(a => a.type) as ValidatorFn[]
      }
    }

    return []
  }

  clearValidators(formGroup: FormGroup | FormArray): void {
    if (!(formGroup.controls == null || formGroup.controls == undefined)) {
      Object.keys(formGroup.controls).forEach(key => {
        const control = formGroup.controls[key] as FormControl | FormGroup | FormArray;
        if (control instanceof FormControl) {
          control.clearValidators();
          control.updateValueAndValidity()
        } else if (control instanceof FormGroup || control instanceof FormArray) {
          this.clearValidators(control);
        }
      });
    } else {
      formGroup.clearValidators();
      formGroup.updateValueAndValidity();
    }
  }
}
