import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms'
import { NgxSpinnerService } from 'ngx-spinner';
import { zip, Subscribable, Observable } from "rxjs";
import { Helper } from "./../../../shared";
import { KeyValuePair, EEventType, EEnhanceService, Global, WORKFLOW_STATUS } from "./../../../shared/global";
import { EnhncSrvcService, UserService } from "./../../../services/";
import { User, MDSNetworkActivityType, MdsEventOdieDevice, MDSMACActivity } from "./../../../models";
import * as moment from 'moment';
import { MdsFormComponent } from '../../event';
import { forEachChild, transpileModule } from 'typescript';
//import 'moment-timezone';

declare let $: any;

@Component({
  selector: 'app-schedule-template',
  templateUrl: './schedule-template.component.html'
})
export class ScheduleTemplateComponent implements OnInit {
  @Input("parent") parent: any; // Parent (Used for MDS)
  @Input("group") form: FormGroup; // FormGroup passed from parent
  @Input() option: any;
  @Input() eventType: number;
  @Input() eventId: number;
  @Input() isReviewer: boolean = false;
  @Input() mplsAccessTag: any;
  @Input() slnkAccessTag: any;
  @Input() circuitInfo: any;
  @Input() dblBookingMessage: string;
  @Input() isNtwkIntl: boolean = false;
  @Input() meetingUrl: string = ""
  @Input() hasNetworkType: boolean = false;

  //MDS
  //@Input() mdsRedesignDevInfo: any;
  //@Input() devCompletion: any;
  //@Input() wiredTransport: any;
  //@Input() thirdParty: any;
  //@Input() wirelessTransport: any;
  //@Input() networkTransport: any;
  //@Input() eventDiscoDev: any;

  activator: FormGroup
  extraDurationList: KeyValuePair[]
  conferenceBridgeList: KeyValuePair[]
  duration = 0;

  cnfrcBrdgNbrList = [];
  cnfrcPinNbrList = [];
  onlineMeetingAdrList = [];
  configList = []

  showMeetingAdrTextBox = false;
  isPreLoad = true;


  qualifiedList = {
    schedule: [] as User[],
    skilled: [] as User[],
    product: [] as User[]
  }
  scheduleQualifiedList: User[]
  skillQualifiedList: User[]
  productQualifiedList: User[]

  slots: FormGroup
  dtSlots = <any>[]
  lblTimeZone: string;

  currentStartTime: any;

  get startDate() { return this.form.get("schedule.startDate"); }
  get onlineMtngAdr() { return this.form.get("schedule.onlineMeetingAdr"); }

  constructor(private helper: Helper, private spinner: NgxSpinnerService,
    public userService: UserService, private enhncSrvc: EnhncSrvcService) { }

  ngOnInit() {
    //this.option.schedule.launchPicker.isShown = this.isReviewer
    //console.log(this.option.schedule.checkCurrentSlot)
    //console.log(this.parent.option.schedule.checkCurrentSlot)
    this.activator = new FormGroup({
      schedule: new FormArray([]),
      skilled: new FormArray([]),
      product: new FormArray([])
    })

    //let start = this.helper.dateAdd(new Date().setHours(new Date().getHours(), 0, 0, 0), "day", 2)
    //console.log('start1: ' + start)
    let start = this.helper.dateAdd(new Date().setUTCHours(0, 0, 0), "day", 2)
    //console.log('start2: ' + start)
    //this.setFormControlValue("schedule.startDate", this.getStartDate(start))
    this.setFormControlValue("schedule.startDate", start)
    this.setEndDate()

    this.extraDurationList = [
      new KeyValuePair("", 0),
      new KeyValuePair("30", 30),
      new KeyValuePair("60", 60),
      new KeyValuePair("90", 90),
      new KeyValuePair("120", 120),
      new KeyValuePair("150", 150),
      new KeyValuePair("180", 180),
      new KeyValuePair("210", 210),
      new KeyValuePair("240", 240)
    ]
    this.conferenceBridgeList = [
      new KeyValuePair("", null),
      new KeyValuePair("No", 1),
      new KeyValuePair("Yes, Submitter", 2),
      new KeyValuePair("Yes, Assigned SE", 3),
      new KeyValuePair("Yes, Other", 4)
    ]

    if (this.eventType == EEventType.UCaaS) {
      this.extraDurationList = [
        new KeyValuePair("", 0),
        //new KeyValuePair("30", 30),
        new KeyValuePair("60", 60),
        //new KeyValuePair("90", 90),
        new KeyValuePair("120", 120),
        //new KeyValuePair("150", 150),
        new KeyValuePair("180", 180),
        //new KeyValuePair("210", 210),
        new KeyValuePair("240", 240)
      ]
    }

    //this.form.get("schedule.startDate").setValidators([this.isWeekend])
    this.form.get("schedule.startDate").updateValueAndValidity()

    if(this.eventId == 0) {
      this.isPreLoad = false;
    }
  }

  //getStartDate(startDate: Date) {
  //  if (this.checkIfWeekend(startDate)) {
  //    return this.getStartDate(this.helper.dateAdd(startDate, "day", 1))
  //  } else {
  //    return startDate
  //  }
  //}

  onOpened(e) {
    let instance = e.component._strategy._timeView._minuteBox;
    let stepCount = 5;
    let secPerMin = 60;

    instance.option("step", stepCount);

    let minCurrentValue = instance.option("value");
    let oldValueChange = instance.option("onValueChanged");

    instance.option("onValueChanged", function (ev) {
      if (ev.value === secPerMin) {
        ev.value = (minCurrentValue + stepCount) % secPerMin;
      }
      if (ev.value === -1) {
        ev.value = (minCurrentValue - stepCount) + secPerMin;
      }
      minCurrentValue = ev.value;
      oldValueChange(ev);
    });
  }

  setEndDate(dropAssignee: boolean = true) {
    let duration = parseInt(this.getFormControlValue("schedule.eventDuration"));
    // let extraDuration = parseInt(this.getFormControlValue("schedule.extraDuration"));
    let end = this.helper.dateAdd(this.getFormControlValue("schedule.startDate"), "minute", duration)
    this.setFormControlValue("schedule.endDate", end)

    // Computes Event Duration
    if (this.isNtwkIntl) {
      dropAssignee = false;
      this.computeEventDurationForNtwkIntl();
    }

    if (dropAssignee) {
      this.clearActivators();
    }
  }

  clearActivators() {
    this.setFormControlValue("schedule.displayedAssignedTo", '');
    this.setFormControlValue("schedule.assignedToId", '');
    this.setFormControlValue("schedule.assignedTo", '');
    this.dblBookingMessage = '';
  }

  // (onOpened)="setMinuteVal($event)"
  //setMinuteVal(e) {
  //  e.component._strategy._timeView._minuteBox.option('step', 15);
  //}

  checkCurrentSlot() {
    this.clearActivators();
    // Check if required fields are supplied
    let data: Observable<any> = null;
    // Updated to fix timezone issue (check param assignment below)
    //let start = new Date(Date.parse(this.getFormControlValue("schedule.startDate")));
    let start = new Date(this.getFormControlValue("schedule.startDate"));
    let startString = this.helper.dateObjectToString(this.getFormControlValue("schedule.startDate"));
    //console.log('start: ' + start)
    //console.log('startString: ' + startString)
    let duration = this.getFormControlValue("schedule.extraDuration");

    if (this.eventType == EEventType.NGVN) {
      let someDate = new Date(Date.now() + 7);

      // Check if required fields are supplied
      if (start > someDate) {
        let params = [{
          stTime: start,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: "NGVN STANDARD",
          extraDuration: duration,
          eventId: this.eventId.toString()
        }]

        console.log(params);
        this.userService.checkCurrentSlots(params[0]).toPromise().then(
          data => {
            this.dtSlots = data;
            if (this.dtSlots.length <= 0) {
              this.lblTimeZone = "There is no available slot within 10 days of this Start Time. Please Choose another Start Time and try again.";
            }
            else {
              this.lblTimeZone = "All Times in Central TimeZone";

            }
          })
        $("#checkCurrentSlot").modal("show");

      } else {
        this.helper.notify('Please enter required fields', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }
    else if (this.eventType == EEventType.AD) {
      let adType = this.getFormControlValue("adType");

      if (this.helper.isEmpty(adType) || (this.circuitInfo == [] || this.circuitInfo.length <= 0)) {
        this.helper.notify('Please enter required fields: AD Type and Circuit Information.', Global.NOTIFY_TYPE_WARNING);
        return;
      }
      else {
        this.enhncSrvc.getEnhncSrvcById(adType).subscribe(res => {
          let adTypeNme = res.enhncSrvcNme;
          let params = [{
            stTime: start,
            stHr: start.getHours(),
            stMin: start.getMinutes(),
            product: this.eventType.toString(),
            submitType: adTypeNme,
            extraDuration: duration,
            eventId: this.eventId.toString()
          }]

          this.userService.checkCurrentSlots(params[0]).toPromise().then(
            data => {
              this.dtSlots = data;
              if (this.dtSlots.length <= 0) {
                this.lblTimeZone = "There is no available slot within 10 days of this Start Time. Please Choose another Start Time and try again.";
              }
              else {
                this.lblTimeZone = "All Times in Central TimeZone";

              }
            })
          $("#checkCurrentSlot").modal("show");
        });
      }
    }
    else if (this.eventType == EEventType.SprintLink) {
      let splkEventType = this.getFormControlValue("event.splkEventType");
      let splkActivityType = this.getFormControlValue("event.splkActivityType");
      let ipVersion = this.getFormControlValue("event.ipVersion");

      if (this.helper.isEmpty(splkEventType) || this.helper.isEmpty(splkActivityType)
        || this.helper.isEmpty(ipVersion)
        || (this.slnkAccessTag == [] || this.slnkAccessTag.length <= 0)) {
        this.helper.notify('Please enter required fields: SprintLink Event Type, SprintLink Activity Type, IP Version and Access Tag.', Global.NOTIFY_TYPE_WARNING);
        return;
      }
      else {
        let params = [{
          stTime: start,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: "SLNK STANDARD",
          extraDuration: duration,
          eventId: this.eventId.toString(),
          ipVersionId: ipVersion,
          slnkEventType: splkEventType,
          slnkActyType: splkActivityType,
          noOfAccess: (this.slnkAccessTag != null && this.slnkAccessTag.length > 0)
            ? this.slnkAccessTag.length : "0"
        }]

        this.userService.checkCurrentSlots(params[0]).toPromise().then(
          data => {
            this.dtSlots = data;
            if (this.dtSlots.length <= 0) {
              this.lblTimeZone = "There is no available slot within 10 days of this Start Time. Please Choose another Start Time and try again.";
            }
            else {
              this.lblTimeZone = "All Times in Central TimeZone";

            }
          })
        $("#checkCurrentSlot").modal("show");
      }
    }
    else if (this.eventType == EEventType.MPLS) {
      let mplsEventType = this.getFormControlValue("event.mplsEventType");
      let submitType = mplsEventType == 1 ? "STANDARD" : "VAS";
      let ipVersionId = this.getFormControlValue("event.ipVersion");
      let vpnPlatformType = this.getFormControlValue("event.vpnPlatformType");
      let activityLocale = this.getFormControlValue("event.activityLocale");
      let multiVrfReq = this.getFormControlValue("event.multiVrfRequirement");
      let mdsManaged = this.getFormControlValue("event.mdsManaged");
      let isWholesalePartner = this.getFormControlValue("event.isWholesalePartner");
      let mplsActivityTypes = this.getFormControlValue("event.mplsActivityTypes");
      let vasTypes = this.getFormControlValue("event.vasTypes");

      // Check if required fields are supplied
      if (this.helper.isEmpty(mplsEventType)
        || this.helper.isEmpty(ipVersionId)
        || this.helper.isEmpty(vpnPlatformType)
        || (mplsActivityTypes == [] || mplsActivityTypes.length <= 0)
        || (this.mplsAccessTag == [] || this.mplsAccessTag.length <= 0)) {
        this.helper.notify('Please enter required fields: MPLS Event Type, MPLS Activity Type, VPN Platform Type, IP Version and Access Tag Table.',
          Global.NOTIFY_TYPE_WARNING);
        return;
      }
      else {
        if (!this.helper.isEmpty(vasTypes)) {
          vasTypes = vasTypes.map(String);
        }

        let params = [{
          stTime: start,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: submitType,
          eventTypeId: this.eventType,
          mplsEventType: mplsEventType,
          ipVersionId: ipVersionId,
          extraDuration: duration,
          eventId: this.eventId.toString(),
          mplsActivityTypes: mplsActivityTypes.map(String),
          vpnPlatformType: vpnPlatformType,
          mplsVasTypes: vasTypes,
          mplsActyLocale: activityLocale,
          multiVRF: multiVrfReq,
          isWhlSalePrtnr: isWholesalePartner,
          mdsMnged: mdsManaged,
          noOfAccess: (this.mplsAccessTag != null && this.mplsAccessTag.length > 0)
            ? this.mplsAccessTag.length : "0"
        }]

        this.userService.checkCurrentSlots(params[0]).toPromise().then(
          data => {
            this.dtSlots = data;
            if (this.dtSlots.length <= 0) {
              this.lblTimeZone = "There is no available slot within 10 days of this Start Time. Please Choose another Start Time and try again.";
            }
            else {
              this.lblTimeZone = "All Times in Central TimeZone";

            }
          })
        $("#checkCurrentSlot").modal("show");
      }
    }
    else if (this.eventType == EEventType.MDS) {
      let isEscalated: false;
      let IsSprintCPETabCnt: number = 0;
      let InCmplDevItems: MdsEventOdieDevice[] = [];
      let mdsRedesignDevInfo: any;
      let devCompletion: any;
      let wiredTransport: any;
      let thirdParty: any;
      let wirelessTransport: any;
      let networkTransport: any;
      let eventDiscoDev: any;
      let parent = this.parent as MdsFormComponent

      if ((!parent.showMDS) || (parent.showMDS && this.validateMds())) {
        mdsRedesignDevInfo = parent.getMdsRedesignDevInfoList();
        devCompletion = parent.getDevCompletionList();
        wiredTransport = parent.getWiredTransportList();
        thirdParty = parent.getThirdPartyList();
        wirelessTransport = parent.getWirelessTransportList();
        networkTransport = parent.getNetworkTransport();
        parent.getEventDiscoDevList();
        eventDiscoDev = parent.eventDiscoDevListFinal;

        console.log(`mdsRedesignDevInfoList`, parent.mdsRedesignDevInfoList);
        console.log(`devCompletionList`, parent.devCompletionList);

        const a = parent.mdsRedesignDevInfoList;
        const b = parent.devCompletionList.filter(a => a.cmpltdCd == false);
        //console.log(JSON.stringify(mdsRedesignDevInfo));
        //console.log(JSON.stringify(parent.mdsRedesignDevInfoList));
        //console.log(JSON.stringify(parent.devCompletionList));
        //console.log(JSON.stringify(a));
        //console.log(JSON.stringify(b));
        //InCmplDevItems = a.filter(({ odieDevNme: idv }) => b.every(({ odieDevNme: idc }) => idv == idc));
        a.forEach(function (e) {
          //console.log(JSON.stringify(e));
          if (b.some(a2 => a2.odieDevNme == e.odieDevNme)) {
            //console.log(e.odieDevNme);
            var temp = Object.assign({}, e);
            InCmplDevItems.push(temp);
          }
        });
        //console.log(JSON.stringify(InCmplDevItems));
        //const newArr = a.concat(r)//.map((v) => v.position ? v : { ...v, position: null });
        let isUSInternational = "";
        let H1 = (parent.showMDS) ? this.getFormControlValue("mds.h1") : this.getFormControlValue("network.h1");
        let country = this.getFormControlValue("site.country");
        if (this.isNtwkIntl) {
          isUSInternational = "I";
          this.form.get("site.us").setValue(isUSInternational)
        }
        else {
          if (country == null || (country.toUpperCase() == "US" || country.toUpperCase() == "U.S"
            || country.toUpperCase() == "USA" || country.toUpperCase() == "U.S.A"
            || country.toUpperCase() == "" || country.toUpperCase() == "UNITED STATE OF AMERICAN"
            || country.toUpperCase() == "UNITED STATES"
            || country.toUpperCase() == "UNITED STATES OF AMERICA" || country == "")) {
            isUSInternational = "D";
            this.form.get("site.us").setValue(isUSInternational)
            //console.log(this.form.get("site.us").value)
            //console.log(`UsorIntl: ` + isUSInternational)
            this.form.get("site.installSite.phoneCode").setValue("")
            this.form.get("site.installSite.cellphoneCode").setValue("")
          }
          else {
            isUSInternational = "I";
            this.form.get("site.us").setValue(isUSInternational)
            //console.log(this.form.get("site.us").value)
            //console.log(`UsorIntl: ` + isUSInternational)
          }
          //let isUSInternational = this.getFormControlValue("site.us");
        }
        
        let mdsNtwk: number[] = parent.activityType.value || [];
        let CustomeName = !mdsNtwk.includes(1) ? this.getFormControlValue("network.customerName") : this.getFormControlValue("mds.odieCustomerName");

        if (this.form.get("event.isEscalation").value)
          this.setFormControlValue("schedule.eventDuration", "0")

        let mngDevTblCnt = parent.mdsActivityType.value == 3 ? eventDiscoDev.length : InCmplDevItems.length;
        //console.log(mngDevTblCnt);
        let mACActivityType = parent.mdsActivityType.value != null ? parent.mdsActivityTypeList.find(a => a.mdsActyTypeId == parent.mdsActivityType.value).mdsActyTypeDes : "";
        let cpeDeliveryOption = this.getFormControlValue("cpe.cpeDeliveryOption");
        //console.log(isUSInternational);
        //console.log(cpeDeliveryOption);
        if (isUSInternational == "I") {
          if (cpeDeliveryOption != null) {
            if (!((cpeDeliveryOption == 4) || (cpeDeliveryOption == 3))) {
              IsSprintCPETabCnt = 1;
            }
            else {
              IsSprintCPETabCnt = 0;
            }
          }
        }

        let mdsOld = false;

        let mDSMACAct: any;
        let MdsMacActivityTypeIds: any[] = []
        // Added additional condition = (parent.activityType.value == 1 || parent.activityType.value == 3)
        if ((parent.activityType.value.includes(1) || parent.activityType.value.includes(3)) && (parent.mdsActivityType.value == 2 || parent.mdsActivityType.value == 5)) {
          let lmm: any[] = [];
          let selectedMacActivities = parent.mdsMacActivity.value as [];

          selectedMacActivities.forEach(a => parent.mdsMacActivityList.forEach(b => {
            if (a === b.mdsMacActyId) {
              lmm.push({
                mdsMacActyId: b.mdsMacActyId,
                MdsMacActy: {
                  mdsMacActyId: b.mdsMacActyId,
                  mdsMacActyNme: b.mdsMacActyNme
                }
              });
            }
          }));
          mDSMACAct = lmm;
          MdsMacActivityTypeIds = selectedMacActivities
        }

        let IsFirewallSecurity = this.CheckFirewallSecurityProduct(parent.mdsRedesignDevInfoList);

        let AnyVirtualConn = false;
        let VirtualTable = null;

        let IsDSLSBICCustTrptReq = thirdParty == null ? false : thirdParty.length > 0;
        let MDSDSLSBICCustTable = thirdParty;

        let IsSPRFWiredDevTrptReq = wiredTransport == null ? false : wiredTransport.length > 0;
        let MDSSPRFWiredTable = wiredTransport;

        let IsWirelessDeviceTransportRequired = wirelessTransport == null ? false : wirelessTransport.length > 0;
        let MDSWirelessTable = wirelessTransport;

        let MDSNtwkActy = mdsNtwk;
        let MDSNtwkTrpt = (!mdsNtwk.includes(1) ? networkTransport : null);
        //let SCFlag = mdsRedesignDevInfo.find(a => a.scCd == "C").scCd.length > 0;

        // Hnadling null/undefined value
        let SCFlag = mdsRedesignDevInfo.some(a => a.scCd == "C");
        //console.log(mdsRedesignDevInfo.some(a => a.scCd == "C"));
        //console.log(JSON.stringify(mdsRedesignDevInfo));
        let params = [{
          odieDevList: InCmplDevItems,
          h1: H1 != null ? H1 : "",
          customerName: CustomeName,
          extraDuration: duration,
          isUSInternational: isUSInternational != null ? isUSInternational : "",
          mngDevTbl: InCmplDevItems,
          mngDevTblCnt: mngDevTblCnt,
          macActivityType: mACActivityType,
          isSprintCPETabCnt: IsSprintCPETabCnt,
          mdsOld: mdsOld,
          mdsMacAct: mDSMACAct,
          isFirewallSecurity: IsFirewallSecurity,
          anyVirtualConn: AnyVirtualConn,
          virtualTable: VirtualTable,
          isDSLSBICCustTrptReq: IsDSLSBICCustTrptReq,
          mdsDSLSBICCustTable: MDSDSLSBICCustTable,
          isSPRFWiredDevTrptReq: IsSPRFWiredDevTrptReq,
          mdsSPRFWiredTable: MDSSPRFWiredTable,
          isWirelessDeviceTransportRequired: IsWirelessDeviceTransportRequired,
          mdsWirelessTable: MDSWirelessTable,
          // Updated to fix timezone issue (check declaration above as well)
          //stTime: start,
          //stHr: start.getHours(),
          //stMin: start.getMinutes(),
          stTime: startString,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: "MDS",
          isMDSFT: 0,//(!hdMDSType.Value.Equals("FT")) ? "0" : "1";       
          eventId: this.eventId.toString(),
          mdsNtwkActy: MDSNtwkActy,
          mdsNtwkTrpt: MDSNtwkTrpt,
          scFlag: SCFlag,
          vASCECd: this.parent.getVASCECd(),
          isSpclPrj: this.parent.isSpclPrj(),
          mplsActivityTypes: this.helper.isEmpty(this.parent.networkActivityType) ? null : this.parent.networkActivityType.value
        }]

        console.log(params)
        this.spinner.show()
        this.userService.checkCurrentSlots(params[0]).subscribe(res => {
          this.dtSlots = res
          if (this.dtSlots.length <= 0) {
            this.lblTimeZone = "There is no available slot within 10 days of this Start Time. Please Choose another Start Time and try again.";
          }
          else {
            this.lblTimeZone = "All Times in Central TimeZone";
          }
          $("#checkCurrentSlot").modal("show");
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
        })
        //this.userService.checkCurrentSlots(params[0]).toPromise().then(
        //  data => {
        //    this.dtSlots = data;
        //    if (this.dtSlots.length <= 0) {
        //      this.lblTimeZone = "There is no available slot within 10 days of this Start Time. Please Choose another Start Time and try again.";
        //    }
        //    else {
        //      this.lblTimeZone = "All Times in Central TimeZone";

        //    }
        //  })
        //$("#checkCurrentSlot").modal("show");
      }
      //else {
      //  this.parent.errors = []
      //  this.parent.errors = [...this.parent.errors, ...this.parent.mdsOdieErrors]
      //  this.parent.errors = [...this.parent.errors, ...this.parent.devCompletionErrors]
      //}
    }
  }

  CheckFirewallSecurityProduct(mdsRedesignDevInfo: MdsEventOdieDevice[]) {
    let _RetVal: string = "False";

    let FrwlSctyCD: number = 0;

    if ((mdsRedesignDevInfo != null) && (mdsRedesignDevInfo.length > 0)) {
      FrwlSctyCD = mdsRedesignDevInfo.filter(a => a.frwlProdCd == "Y").length;
    }
    if (FrwlSctyCD > 0)
      _RetVal = "True";

    return _RetVal;
  }

  onValueChanged(slot) {
    var a1 = new Array();
    a1 = slot.sloT_ID.split("|");
    this.userService.checkForDoubleBooking(a1[0], a1[1], a1[2], this.eventId).subscribe(res => {
      this.dblBookingMessage = res;
      this.setFormControlValue("schedule.startDate", a1[0])
      this.setFormControlValue("schedule.endDate", a1[1])
      this.setFormControlValue("schedule.assignedToId", a1[2])
      this.setFormControlValue("schedule.eventDuration", a1[3])
      this.setFormControlValue("schedule.assignedTo", slot.sloT_USER)
      this.setFormControlValue("schedule.displayedAssignedTo", slot.sloT_USER)

      const isBridgeWillDrop = this.form.get("schedule.withConferenceBridge").value == 4 ? false : true;
      this.onWithConferenceBridgeChanged({ value: this.form.get("schedule.withConferenceBridge").value }, isBridgeWillDrop, true);

      $("#checkCurrentSlot").modal("hide");
    }, error => {
      $("#checkCurrentSlot").modal("hide");
    })
  }

  checkDblBooking(event: any = null) {
    this.spinner.show();
    let start = this.form.get("schedule.startDate").value;
    let extraDuration = this.form.get("schedule.extraDuration").value;
    let end :any 
    
    // In selecting an activator in Slot picker. The event durations is updated(Defualt duration + Additional Duration)
    // Else is not. So we need to manually add it to get the correct end time.
    if(event) {
      end  = this.helper.dateAdd(this.getFormControlValue("schedule.endDate"), "minute", ((extraDuration == "") ? "0" : extraDuration));
    } else {
       end = this.helper.dateAdd(this.getFormControlValue("schedule.startDate"), "minute", this.getFormControlValue("schedule.eventDuration") );
    }
    
   
    //let end =  this.form.get("schedule.endDate").value;
    let assignedToId = this.form.get("schedule.assignedToId").value;
    assignedToId = assignedToId.toString().split(',').join('|');

    if (typeof (start) === 'object') {
      start = this.helper.dateObjectToString(start);
    }

    if (typeof (end) === 'object') {
      end = this.helper.dateObjectToString(end);
    }

    this.userService.checkForDoubleBooking(start, end, assignedToId, this.eventId).subscribe(res => {
      this.dblBookingMessage = res;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    })

    // Added isEmpty to condition to prevent null error
    if (!this.helper.isEmpty(this.form.get("schedule.withConferenceBridge")) &&
      this.form.get("schedule.withConferenceBridge").value == 1) {
      this.form.get("schedule.bridgeNumber").reset()
      this.form.get("schedule.bridgePin").reset()
    }

    const isBridgeWillDrop = this.form.get("schedule.withConferenceBridge").value == 4 ? false : true;
    this.onWithConferenceBridgeChanged({ value: this.form.get("schedule.withConferenceBridge").value }, isBridgeWillDrop, true);
  }

  launchPicker() {
    this.clearActivators();
    let params = []
    let data: Subscribable<any> = null
    // Updated to fix timezone issue (check param assignment below)
    //let start = new Date(Date.parse(this.getFormControlValue("schedule.startDate")));
    let start = new Date(this.getFormControlValue("schedule.startDate"));
    let startString = this.helper.dateObjectToString(this.getFormControlValue("schedule.startDate"));
    let end = new Date(Date.parse(this.getFormControlValue("schedule.endDate")))
    let duration = this.getFormControlValue("schedule.extraDuration");
    // Check what Event Type

    if (this.eventType == EEventType.NGVN) {
      let someDate = new Date(Date.now() + 7);

      // Check if required fields are supplied
      if (start > someDate) {
        params = [{
          stTime: start,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: "NGVN STANDARD",
          extraDuration: duration,
          eventId: this.eventId.toString()
        }]

        console.log(params)
        this.spinner.show()
        this.userService.getLaunchPicker(params[0]).subscribe(data => {
          console.log(data)
          this.patchResult("schedule", data["schedule"] as User[])
          this.patchResult("skilled", data["skilled"] as User[])
          this.patchResult("product", data["product"] as User[])
          this.duration = data["table1"][0].duration

          $("#schedule").modal("show");
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
        })

      } else {
        this.helper.notify('Please enter required fields', Global.NOTIFY_TYPE_WARNING);
        return;
      }
    }
    else if (this.eventType == EEventType.AD) {
      let adType = this.getFormControlValue("adType");

      if (this.helper.isEmpty(adType) || (this.circuitInfo == [] || this.circuitInfo.length <= 0)) {
        this.helper.notify('Please enter required fields: AD Type and Circuit Information.', Global.NOTIFY_TYPE_WARNING);
        return;
      }
      else {
        this.enhncSrvc.getEnhncSrvcById(adType).subscribe(res => {
          let adTypeNme = res.enhncSrvcNme;
          //params = [{
          //  stTime: start,
          //  stHr: start.getHours(),
          //  stMin: start.getMinutes(),
          //  product: this.eventType.toString(),
          //  submitType: adTypeNme,
          //  extraDuration: duration,
          //  eventId: this.eventId.toString()
          //}]
        });
        console.log(this.parent.adTypeList.find(a => a.enhncSrvcId == adType).enhncSrvcNme)
        params = [{
          stTime: start,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: this.parent.adTypeList.find(a => a.enhncSrvcId == adType).enhncSrvcNme,
          extraDuration: duration,
          eventId: this.eventId.toString()
        }]

        console.log(params)
        this.spinner.show()
        this.userService.getLaunchPicker(params[0]).subscribe(data => {
          console.log(data)
          this.patchResult("schedule", data["schedule"] as User[])
          this.patchResult("skilled", data["skilled"] as User[])
          this.patchResult("product", data["product"] as User[])
          this.duration = data["table1"][0].duration

          $("#schedule").modal("show");
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
        })
      }
    }
    else if (this.eventType == EEventType.SprintLink) {
      let splkEventType = this.getFormControlValue("event.splkEventType");
      let splkActivityType = this.getFormControlValue("event.splkActivityType");
      let ipVersion = this.getFormControlValue("event.ipVersion");

      if (this.helper.isEmpty(splkEventType) || this.helper.isEmpty(splkActivityType)
        || this.helper.isEmpty(ipVersion)
        || (this.slnkAccessTag == [] || this.slnkAccessTag.length <= 0)) {
        this.helper.notify('Please enter required fields: SprintLink Event Type, SprintLink Activity Type, IP Version and Access Tag.', Global.NOTIFY_TYPE_WARNING);
        return;
      }
      else {
        params = [{
          stTime: start,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: "SLNK STANDARD",
          extraDuration: duration,
          eventId: this.eventId.toString(),
          ipVersionId: ipVersion,
          slnkEventType: splkEventType,
          slnkActyType: splkActivityType,
          noOfAccess: (this.slnkAccessTag != null && this.slnkAccessTag.length > 0)
            ? this.slnkAccessTag.length : "0"
        }]

        console.log(params)
        this.spinner.show()
        this.userService.getLaunchPicker(params[0]).subscribe(data => {
          console.log(data)
          this.patchResult("schedule", data["schedule"] as User[])
          this.patchResult("skilled", data["skilled"] as User[])
          this.patchResult("product", data["product"] as User[])
          this.duration = data["table1"][0].duration

          $("#schedule").modal("show");
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
        })
      }
    }
    else if (this.eventType == EEventType.MPLS) {
      let mplsEventType = this.getFormControlValue("event.mplsEventType");
      let submitType = mplsEventType == 1 ? "STANDARD" : "VAS";
      let ipVersionId = this.getFormControlValue("event.ipVersion");
      let vpnPlatformType = this.getFormControlValue("event.vpnPlatformType");
      let activityLocale = this.getFormControlValue("event.activityLocale");
      let multiVrfReq = this.getFormControlValue("event.multiVrfRequirement");
      let mdsManaged = this.getFormControlValue("event.mdsManaged");
      let isWholesalePartner = this.getFormControlValue("event.isWholesalePartner");
      let mplsActivityTypes = this.getFormControlValue("event.mplsActivityTypes");
      let vasTypes = this.getFormControlValue("event.vasTypes");

      // Check if required fields are supplied
      if (this.helper.isEmpty(mplsEventType)
        || this.helper.isEmpty(ipVersionId)
        || this.helper.isEmpty(vpnPlatformType)
        || (mplsActivityTypes == [] || mplsActivityTypes.length <= 0)
        || (this.mplsAccessTag == [] || this.mplsAccessTag.length <= 0)) {
        this.helper.notify('Please enter required fields: MPLS Event Type, MPLS Activity Type, VPN Platform Type, IP Version and Access Tag Table.',
          Global.NOTIFY_TYPE_WARNING);
        return;
      }
      else {
        if (!this.helper.isEmpty(vasTypes)) {
          vasTypes = vasTypes.map(String);
        }

        params = [{
          stTime: start,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: submitType,
          eventTypeId: this.eventType,
          mplsEventType: mplsEventType,
          ipVersionId: ipVersionId,
          extraDuration: duration,
          eventId: this.eventId.toString(),
          mplsActivityTypes: mplsActivityTypes.map(String),
          vpnPlatformType: vpnPlatformType,
          mplsVasTypes: vasTypes,
          mplsActyLocale: activityLocale,
          multiVRF: multiVrfReq,
          isWhlSalePrtnr: isWholesalePartner,
          mdsMnged: mdsManaged,
          noOfAccess: (this.mplsAccessTag != null && this.mplsAccessTag.length > 0)
            ? this.mplsAccessTag.length : "0"
        }]

        console.log(params)
        this.spinner.show()
        this.userService.getLaunchPicker(params[0]).subscribe(data => {
          console.log(data)
          this.patchResult("schedule", data["schedule"] as User[])
          this.patchResult("skilled", data["skilled"] as User[])
          this.patchResult("product", data["product"] as User[])
          this.duration = data["table1"][0].duration

          $("#schedule").modal("show");
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
        })
      }
    }
    else if (this.eventType == EEventType.MDS) {
      let isEscalated: false;
      let IsSprintCPETabCnt: number = 0;
      let InCmplDevItems: MdsEventOdieDevice[] = [];
      let mdsRedesignDevInfo: any;
      let devCompletion: any;
      let wiredTransport: any;
      let thirdParty: any;
      let wirelessTransport: any;
      let networkTransport: any;
      let eventDiscoDev: any;
      let parent = this.parent as MdsFormComponent

      if ((!parent.showMDS) || (parent.showMDS && this.validateMds())) {
        mdsRedesignDevInfo = parent.getMdsRedesignDevInfoList();
        devCompletion = parent.getDevCompletionList();
        wiredTransport = parent.getWiredTransportList();
        thirdParty = parent.getThirdPartyList();
        wirelessTransport = parent.getWirelessTransportList();
        networkTransport = parent.getNetworkTransport();
        parent.getEventDiscoDevList();
        eventDiscoDev = parent.eventDiscoDevListFinal;

        //console.log(JSON.stringify(parent.mdsActivityType.value));
        //console.log(JSON.stringify(eventDiscoDev));

        const a = parent.mdsRedesignDevInfoList;
        const b = parent.devCompletionList.filter(a => a.cmpltdCd == false);

        //console.log(JSON.stringify(parent.mdsRedesignDevInfoList));
        //console.log(JSON.stringify(parent.devCompletionList));
        //console.log(JSON.stringify(a));
        //console.log(JSON.stringify(b));
        //InCmplDevItems = a.filter(({ odieDevNme: idv }) => b.every(({ odieDevNme: idc }) => idv == idc));
        //console.log(JSON.stringify(InCmplDevItems));
        a.forEach(function (e) {
          //console.log(JSON.stringify(e));
          if (b.some(a2 => a2.odieDevNme == e.odieDevNme)) {
            //console.log(e.odieDevNme);
            var temp = Object.assign({}, e);
            InCmplDevItems.push(temp);
          }
        });
        let isUSInternational = "";
        let H1 = (parent.showMDS) ? this.getFormControlValue("mds.h1") : this.getFormControlValue("network.h1");
        let country = this.getFormControlValue("site.country");
        if (this.isNtwkIntl) {
          isUSInternational = "I";
          this.form.get("site.us").setValue(isUSInternational)
        }
        else {
          if (country == null || (country.toUpperCase() == "US" || country.toUpperCase() == "U.S"
            || country.toUpperCase() == "USA" || country.toUpperCase() == "U.S.A"
            || country.toUpperCase() == "" || country.toUpperCase() == "UNITED STATE OF AMERICAN"
            || country.toUpperCase() == "UNITED STATES"
            || country.toUpperCase() == "UNITED STATES OF AMERICA" || country == "")) {
            isUSInternational = "D";
            this.form.get("site.us").setValue(isUSInternational)
            //console.log(this.form.get("site.us").value)
            //console.log(`UsorIntl: ` + isUSInternational)
            this.form.get("site.installSite.phoneCode").setValue("")
            this.form.get("site.installSite.cellphoneCode").setValue("")
          }
          else {
            isUSInternational = "I";
            this.form.get("site.us").setValue(isUSInternational)
            //console.log(this.form.get("site.us").value)
            //console.log(`UsorIntl: ` + isUSInternational)
          }
        }

        let mdsNtwk: number[] = parent.activityType.value || [];
        let CustomeName = !mdsNtwk.includes(1) ? this.getFormControlValue("network.customerName") : this.getFormControlValue("mds.odieCustomerName");

        //if (isEscalated)
        //  txtEventDuration.Text = "0";
        if (this.form.get("event.isEscalation").value)
          this.setFormControlValue("schedule.eventDuration", "0")

        let mngDevTblCnt = parent.mdsActivityType.value == 3 ? eventDiscoDev.length : InCmplDevItems.length;
        //console.log(mngDevTblCnt);
        let mACActivityType = parent.mdsActivityType.value != null ? parent.mdsActivityTypeList.find(a => a.mdsActyTypeId == parent.mdsActivityType.value).mdsActyTypeDes : "";
        let cpeDeliveryOption = this.getFormControlValue("cpe.cpeDeliveryOption");

        if (isUSInternational == "I") {
          if (cpeDeliveryOption != null) {
            if (!((cpeDeliveryOption == 4) || (cpeDeliveryOption == 3))) {
              IsSprintCPETabCnt = 1;
            }
            else {
              IsSprintCPETabCnt = 0;
            }
          }
        }

        let mdsOld = false;

        let mDSMACAct: any;
        let MdsMacActivityTypeIds: any[] = []
        // Added additional condition = (parent.activityType.value == 1 || parent.activityType.value == 3)
        if ((parent.activityType.value.includes(1) || parent.activityType.value.includes(3)) && (parent.mdsActivityType.value == 2 || parent.mdsActivityType.value == 5)) {
          let lmm: any[] = [];
          let selectedMacActivities = parent.mdsMacActivity.value as [];

          selectedMacActivities.forEach(a => parent.mdsMacActivityList.forEach(b => {
            if (a === b.mdsMacActyId) {
              lmm.push({
                mdsMacActyId: b.mdsMacActyId,
                MdsMacActy: {
                  mdsMacActyId: b.mdsMacActyId,
                  mdsMacActyNme: b.mdsMacActyNme
                }
              });
            }
          }));
          mDSMACAct = lmm;
          MdsMacActivityTypeIds = selectedMacActivities
        }

        let IsFirewallSecurity = this.CheckFirewallSecurityProduct(parent.mdsRedesignDevInfoList);

        let AnyVirtualConn = false;
        let VirtualTable = null;

        let IsDSLSBICCustTrptReq = thirdParty == null ? false : thirdParty.length > 0;
        let MDSDSLSBICCustTable = thirdParty;

        let IsSPRFWiredDevTrptReq = wiredTransport == null ? false : wiredTransport.length > 0;
        let MDSSPRFWiredTable = wiredTransport;

        let IsWirelessDeviceTransportRequired = wirelessTransport == null ? false : wirelessTransport.length > 0;
        let MDSWirelessTable = wirelessTransport;

        let MDSNtwkActy = mdsNtwk;
        let MDSNtwkTrpt = (!mdsNtwk.includes(1) ? networkTransport : null);
        //let SCFlag = mdsRedesignDevInfo.find(a => a.scCd == "C").scCd.length > 0;

        // Hnadling null/undefined value
        let SCFlag = mdsRedesignDevInfo.some(a => a.scCd == "C");

        params = [{
          odieDevList: InCmplDevItems,
          h1: H1 != null ? H1 : "",
          customerName: CustomeName,
          extraDuration: duration,
          isUSInternational: isUSInternational != null ? isUSInternational : "",
          mngDevTbl: InCmplDevItems,
          mngDevTblCnt: mngDevTblCnt,
          macActivityType: mACActivityType,
          isSprintCPETabCnt: IsSprintCPETabCnt,
          mdsOld: mdsOld,
          mdsMacAct: mDSMACAct,
          isFirewallSecurity: IsFirewallSecurity,
          anyVirtualConn: AnyVirtualConn,
          virtualTable: VirtualTable,
          isDSLSBICCustTrptReq: IsDSLSBICCustTrptReq,
          mdsDSLSBICCustTable: MDSDSLSBICCustTable,
          isSPRFWiredDevTrptReq: IsSPRFWiredDevTrptReq,
          mdsSPRFWiredTable: MDSSPRFWiredTable,
          isWirelessDeviceTransportRequired: IsWirelessDeviceTransportRequired,
          mdsWirelessTable: MDSWirelessTable,
          // Updated to fix timezone issue (check declaration above as well)
          //stTime: start,
          //stHr: start.getHours(),
          //stMin: start.getMinutes(),
          stTime: startString,
          stHr: start.getHours(),
          stMin: start.getMinutes(),
          product: this.eventType.toString(),
          submitType: "MDS",
          isMDSFT: 0,//(!hdMDSType.Value.Equals("FT")) ? "0" : "1";       
          eventId: this.eventId.toString(),
          mdsNtwkActy: MDSNtwkActy,
          mdsNtwkTrpt: MDSNtwkTrpt,
          scFlag: SCFlag,
          vASCECd: this.parent.getVASCECd(),
          isSpclPrj: this.parent.isSpclPrj(),
          mplsActivityTypes: this.helper.isEmpty(this.parent.networkActivityType) ? null : this.parent.networkActivityType.value
        }]


        console.log(params)
        this.spinner.show()
        this.userService.getLaunchPicker(params[0]).subscribe(data => {
          console.log(data)
          this.patchResult("schedule", data["schedule"] as User[])
          this.patchResult("skilled", data["skilled"] as User[])
          this.patchResult("product", data["product"] as User[])
          this.duration = data["table1"][0].duration

          $("#schedule").modal("show");
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
        })
      }
      //else {
      //  this.parent.errors = []
      //  this.parent.errors = [...this.parent.errors, ...this.parent.mdsOdieErrors]
      //  this.parent.errors = [...this.parent.errors, ...this.parent.devCompletionErrors]
      //}
    }
  }

  patchResult(list, result) {
    this.qualifiedList[list] = []
    for (let res of result) {
      (this.qualifiedList[list] as User[]).push(new User(res.useR_ID, res.useR_ADID, res.fulL_NME, res.dspL_NME))
    }

    console.log(this.qualifiedList)

    //this.qualifiedList[list] = result

    this.activator.controls[list] = new FormArray([])
    for (let user of this.qualifiedList[list]) {
      this.addAssignedTo(list)
    }
  }

  addAssignedTo(qualifications) {
    const check = this.activator.controls[qualifications] as FormArray;
    check.push(new FormControl(false));
  }

  setActivator() {
    let activators: User[] = []

    // Iterate between 'schedule', 'skilled', and 'product'
    Object.keys(this.activator.controls).forEach(key => {
      let controls = this.activator.get(key) as FormArray

      // Get index of all TRUE
      const indices = (controls.value as boolean[]).reduce((out, value, index) =>
        value ? out.concat(index) : out, []
      )

      // Pushes distinct User for each 'indices'
      indices.filter(index =>
        activators.findIndex(a =>
          a.userId == this.qualifiedList[key][index].userId) < 0
      ).map(index =>
        activators.push(this.qualifiedList[key][index])
      )
    });

    this.setFormControlValue("schedule.eventDuration", this.duration)
    this.setFormControlValue("schedule.endDate", this.helper.dateAdd(this.form.get("schedule.startDate").value, "minute", this.duration))
    this.setFormControlValue("schedule.assignedToId", activators.map(a => a.userId).join(","))
    this.setFormControlValue("schedule.assignedTo", activators.map(a => a.dsplNme).join("; "))
    this.setFormControlValue("schedule.displayedAssignedTo", activators.map(a => a.dsplNme).join("; "))

    const isBridgeWillDrop = this.form.get("schedule.withConferenceBridge").value == 4 ? false : true;
    this.onWithConferenceBridgeChanged({ value: this.form.get("schedule.withConferenceBridge").value }, isBridgeWillDrop , true);

    this.checkDblBooking();
  }

  // Event triggered is on changing the dropdown
  onWithConferenceBridgeChanged(e, eventTriggered: boolean = true, manuallySet: boolean = false) {
    this.meetingUrl = ""
    this.form.get('schedule.onlineMeetingAdr').clearValidators()
    this.form.get('schedule.onlineMeetingAdr').updateValueAndValidity()

    let assigned = this.form.get('schedule.assignedToId').value;


    // For No and Yes, Other options
    if ((e.value == 1 || e.value == 4) && eventTriggered) {
      this.form.get("schedule.bridgeNumber").setValue('');
      this.form.get("schedule.bridgePin").setValue('');

      if(e.value == 1) {
        this.form.get('schedule.onlineMeetingAdr').setValue('');

        this.onlineMeetingAdrList = [];
        this.showMeetingAdrTextBox = false;
      } else {
        this.showMeetingAdrTextBox = true;
      }

      this.meetingUrl = '';
      this.isPreLoad = false;
    }
    // For Yes, Submitter option
    else if (e.value == 2) {
      // Added by Sarah Sandoval [20220315] = Clear conf bridge details if option change
      if (!this.helper.isEmpty(e.previousValue) && e.value != e.previousValue) {
        this.form.get("schedule.bridgeNumber").setValue('');
        this.form.get("schedule.bridgePin").setValue('');
        this.form.get('schedule.onlineMeetingAdr').setValue('');
      }
      this.userService.getConfBridgeByUserId(this.form.get("requestor.id").value).subscribe(res => {
        //console.log(['Yes, Submitter Option', res])
        if (res != null) {
          this.getConfigValue(res, manuallySet);
          this.isPreLoad = false;
        }
      })
    }
    // For Yes, MNS option
    else if (e.value == 3 && this.form.get("schedule.assignedToId").value != null) {
      // Updated by Sraah Sandoval [20220314] - Removed condition above to clear every change since that will not work on Yes, Submitter option (INC39324837)
      // Put the condition on this option only since the original issue happens only on Yes MNS option (IM6496493)
      // Clear fields for every Activator change through Check Current Slot/Launch Picker Button

      let assigned = this.getSelectedActivators(this.form.get("workflowStatus").value);
      let noAction = assigned.length > 1;
      assigned = assigned.shift();
      assigned = assigned == undefined ? 0 : assigned as number
      if(assigned == 1) {
        this.form.get("schedule.bridgeNumber").setValue('');
        this.form.get("schedule.bridgePin").setValue('');
        this.form.get('schedule.onlineMeetingAdr').setValue('');
      }
      if (assigned > 0) {
        this.userService.getConfBridgeByUserId(assigned).subscribe(res => {
          //console.log(['Yes, MNS Option', res])
          if (res != null) {
            this.getConfigValue(res, noAction);
            this.isPreLoad = false;
          }
        })
      }
      else {
        this.form.get('schedule.onlineMeetingAdr').setValue('');
        this.onlineMeetingAdrList = [];
        this.meetingUrl = '';
      }
    }
  }

  setConfigValue(e: any) {
    let data = this.configList.filter(x => x['onlineMeetingAdr'] == e.value);
    if(data.length > 0) {
      data = data[0];
      this.form.get("schedule.bridgeNumber").setValue(data['cnfrcBrdgNbr'])
      this.form.get("schedule.bridgePin").setValue(data['cnfrcPinNbr'])
      this.form.get("schedule.onlineMeetingAdr").setValue(data['onlineMeetingAdr'])
      this.meetingUrl = data['onlineMeetingAdr'];
    }
  }

  getConfigValue(res: any, noAction: boolean = false) {
    if(res.length > 1) {
      this.form.get('schedule.onlineMeetingAdr').setValidators([Validators.required])
      this.form.get('schedule.onlineMeetingAdr').updateValueAndValidity()
    }

    if(noAction && !this.isPreLoad) { // Avoid dropping Bridge and PIN
      return;
    }

    this.configList = res;
    this.onlineMeetingAdrList = this.getMeetingUrlsForDropdown(res);

    const cnfrcBrdgNbr = this.form.get("schedule.bridgeNumber")
    const cnfrcPinNbr = this.form.get("schedule.bridgePin")
    const onlineMeetingAdr = this.form.get("schedule.onlineMeetingAdr")

    //if (res.length == 1 && !this.isPreLoad) {
    if (res.length == 1) {
      // Added by Sarah Sandoval [20220330] - To auto select value with 1 conf bridge details
      if (!this.isPreLoad || this.helper.isEmpty(onlineMeetingAdr.value)) {
        const data = res[0];
        cnfrcBrdgNbr.setValue(data['cnfrcBrdgNbr'])
        cnfrcPinNbr.setValue(data['cnfrcPinNbr'])
        onlineMeetingAdr.setValue(data['onlineMeetingAdr'])
        this.meetingUrl = data['onlineMeetingAdr']
      }
    }
    else {
      if(!this.isPreLoad) {
        this.form.get('schedule.onlineMeetingAdr').setValue('');
      }
      let data = this.configList.filter(x => x['onlineMeetingAdr'] == onlineMeetingAdr.value);
      if (data.length > 0) {
        data = data[0];
        this.meetingUrl = data['onlineMeetingAdr']
      }
    }
  }

  getMeetingUrlsForDropdown(list: any) {
    return list.filter(x => x.onlineMeetingAdr != null).map(data => {
      return {description: data['onlineMeetingAdr'], value: data['onlineMeetingAdr'] };
    })
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  //isWeekend(control: FormControl): { [key: string]: any } {
  //  let startDate = control.value
  //  let momentDate = moment(startDate)

  //  if (momentDate.day() == 0 || momentDate.day() == 6) {
  //    return { weekend: true }
  //  } else {
  //    return null
  //  }
  //}

  //checkIfWeekend(startDate: any) {
  //  let momentDate = moment(startDate)

  //  if (momentDate.day() == 0 || momentDate.day() == 6) {
  //    return true
  //  } else {
  //    return false
  //  }
  //}

  getSelectedActivators(wrkflwStusId: number) {
    // Removed condition by Sarah Sandoval [20220329] - Used only on MNS Option
    // It should return the assigned activator so that the conf bridge details will load (INC70022224)
    //if (wrkflwStusId != WORKFLOW_STATUS.Reject && wrkflwStusId != WORKFLOW_STATUS.Reschedule) {
      if (!this.helper.isEmpty(this.form.get("schedule.displayedAssignedTo").value)) {
        let adActivators = this.form.get("schedule.assignedToId").value
        if (adActivators != null) {
          let activators = adActivators.toString()
          if (activators.length > 0) {
            if (activators.indexOf(',') != -1) {
              return activators.toString().split(",").map(a => parseInt(a));
            } else {
              return [adActivators];
            }
          }
        }
      }
    //}

    return [];
  }

  validateMds() {
    this.parent.errors = [];
    this.parent.option.validationSummaryOptions.title = "Validation Summary";
    this.parent.option.validationSummaryOptions.message = null;

    if (this.parent.form.get("activityType").invalid) {
      if (this.parent.form.get("activityType").errors.required) {
        this.parent.errors.push("Activity Type is required");
      }
      this.parent.errors.push("Activity Type is required");
    } else {
      // MDS
      if (this.parent.showMDS) {
        if (this.parent.form.get("customerSOW").invalid && !this.parent.showDisconnect) {
          if (this.parent.form.get("customerSOW").errors.required) {
            this.parent.errors.push("Customer SOW is required");
          }
        }
        if (this.parent.form.get("shortDescription").invalid && !this.parent.showDisconnect) {
          if (this.parent.form.get("shortDescription").errors.required) {
            this.parent.errors.push("Short description is required");
          }
        }
        if (this.parent.form.get("mds.h1").invalid) {
          if (this.parent.form.get("mds.h1").errors.required) {
            this.parent.errors.push("H1 is required");
          } else if (this.parent.form.get("mds.h1").errors.pattern) {
            this.parent.errors.push("H1 must be 9-digit number");
          } else if (this.parent.form.get("mds.h1").errors.notMatch) {
            this.parent.errors.push("H1 values should all match");
          }
        }
        if (this.parent.form.get("mds.odieCustomerName").invalid) {
          if (this.parent.form.get("mds.odieCustomerName").errors.required) {
            this.parent.errors.push("ODIE Customer Name is required");
          }
        }
        if (this.parent.form.get("mds.teamPDL").invalid) {
          if (this.parent.form.get("mds.teamPDL").errors.required) {
            this.parent.errors.push("Team PDL is required");
          }
        }
        if (this.parent.form.get("mds.mdsActivityType").invalid) {
          if (this.parent.form.get("mds.mdsActivityType").errors.required) {
            this.parent.errors.push("MDS Activity Type is required");
          }
        } else {
          if (this.parent.form.get("mds.mdsMacActivity").invalid && !this.parent.showDisconnect) {
            if (this.parent.form.get("mds.mdsMacActivity").errors.required) {
              this.parent.errors.push("MDS MAC Activity is required");
            } else if (this.parent.form.get("mds.mdsMacActivity").errors.mixed) {
              this.parent.errors.push("MDS MAC Activity should not be a mix of Generic and Non-Generic type");
            }
          }
        }

        // DISCONNECT
        if (this.parent.showDisconnect) {
          if (!this.parent.validateEventDiscoDev()) {
            this.parent.errors = [...this.parent.errors, ...this.parent.eventDiscoDevErrors]
          }
        }
        // NON-DISCONNECT
        else {
          if (this.parent.showMDS) {
            //console.log("NonDisc")
            if (!this.parent.validateMdsOdie()) {
              this.parent.errors = [...this.parent.errors, ...this.parent.mdsOdieErrors]
            }
            if (!this.parent.validateDevCompletion()) {
              this.parent.errors = [...this.parent.errors, ...this.parent.devCompletionErrors]
            }
            //if (!this.parent.validateDevCompletion()) {
            //  this.parent.errors = [...this.parent.errors, ...this.parent.devCompletionErrors]
            //}
          }
        }
      }

      // NETWORK
      if (this.parent.showNetwork) {
        if (this.parent.form.get("shortDescription").invalid) {
          if (this.parent.form.get("shortDescription").errors.required) {
            this.parent.errors.push("Description is required");
          }
        }
        if (this.parent.form.get("network.h6").invalid) {
          if (this.parent.form.get("network.h6").errors.required) {
            this.parent.errors.push(`${this.parent.h6Label} is required`);
          } else if (this.parent.form.get("network.h6").errors.pattern) {
            this.parent.errors.push(`H6 must be 9-digit number`);
          }
        }
        if (this.parent.form.get("network.h1").invalid) {
          if (this.parent.form.get("network.h1").errors.required) {
            this.parent.errors.push("H1 is required");
          } else if (this.parent.form.get("network.h1").errors.pattern) {
            this.parent.errors.push(`H1 must be 9-digit number`);
          } else if (this.parent.form.get("network.h1").errors.notMatch) {
            this.parent.errors.push(`H1 values should all match`);
          }
        }
        if (this.parent.form.get("network.networkActivityType").invalid) {
          if (this.parent.form.get("network.networkActivityType").errors.required) {
            this.parent.errors.push("Network Activity Type is required");
          }
        }
        if (this.parent.form.get("network.isRelatedCompassNCR").invalid) {
          if (this.parent.form.get("network.isRelatedCompassNCR").errors.required) {
            this.parent.errors.push("Related Compass NCR is required");
          }
        }

        if (!this.parent.validateNetworkTransport()) {
          this.parent.errors = [...this.parent.errors, ...this.parent.networkTransportErrors]
        }
        if (!this.parent.validateNetworkCustomer()) {
          this.parent.errors = [...this.parent.errors, ...this.parent.networkCustomerErrors]
        }
      }
    }

    this.parent.errors = Array.from(new Set(this.parent.errors))
    //if (this.parent.errors.length > 0) {
    //  $("#validation-summary").modal("show");
    //}
    return this.parent.errors.length == 0 ? true : false
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }

    return true;
  }

  computeEventDurationForNtwkIntl() {
    let data: Observable<any> = null;
    let start = new Date(this.getFormControlValue("schedule.startDate"));
    let startString = this.helper.dateObjectToString(this.getFormControlValue("schedule.startDate"));
    let duration = this.getFormControlValue("schedule.extraDuration");

    let isEscalated: false;
    let IsSprintCPETabCnt: number = 0;
    let InCmplDevItems: MdsEventOdieDevice[] = [];
    let mdsRedesignDevInfo: any;
    let devCompletion: any;
    let wiredTransport: any;
    let thirdParty: any;
    let wirelessTransport: any;
    let networkTransport: any;
    let eventDiscoDev: any;
    let parent = this.parent as MdsFormComponent

    if ((!parent.showMDS) || (parent.showMDS && this.validateMds())) {
      mdsRedesignDevInfo = parent.getMdsRedesignDevInfoList();
      devCompletion = parent.getDevCompletionList();
      wiredTransport = parent.getWiredTransportList();
      thirdParty = parent.getThirdPartyList();
      wirelessTransport = parent.getWirelessTransportList();
      networkTransport = parent.getNetworkTransport();
      parent.getEventDiscoDevList();
      eventDiscoDev = parent.eventDiscoDevListFinal;

      const a = parent.mdsRedesignDevInfoList;
      const b = parent.devCompletionList.filter(a => a.cmpltdCd == false);
      a.forEach(function (e) {
        if (b.some(a2 => a2.odieDevNme == e.odieDevNme)) {
          var temp = Object.assign({}, e);
          InCmplDevItems.push(temp);
        }
      });
      let isUSInternational = "";
      let H1 = (parent.showMDS) ? this.getFormControlValue("mds.h1") : this.getFormControlValue("network.h1");
      let country = this.getFormControlValue("site.country");
      if (this.isNtwkIntl) {
        isUSInternational = "I";
        this.form.get("site.us").setValue(isUSInternational)
      }
      else {
        if (country == null || (country.toUpperCase() == "US" || country.toUpperCase() == "U.S"
          || country.toUpperCase() == "USA" || country.toUpperCase() == "U.S.A"
          || country.toUpperCase() == "" || country.toUpperCase() == "UNITED STATE OF AMERICAN"
          || country.toUpperCase() == "UNITED STATES"
          || country.toUpperCase() == "UNITED STATES OF AMERICA" || country == "")) {
          isUSInternational = "D";
          this.form.get("site.us").setValue(isUSInternational)
          this.form.get("site.installSite.phoneCode").setValue("")
          this.form.get("site.installSite.cellphoneCode").setValue("")
        }
        else {
          isUSInternational = "I";
          this.form.get("site.us").setValue(isUSInternational)
        }
      }

      let mdsNtwk: number[] = parent.activityType.value || [];
      let CustomeName = !mdsNtwk.includes(1) ? this.getFormControlValue("network.customerName") : this.getFormControlValue("mds.odieCustomerName");

      if (this.form.get("event.isEscalation").value)
        this.setFormControlValue("schedule.eventDuration", "0")

      let mngDevTblCnt = parent.mdsActivityType.value == 3 ? eventDiscoDev.length : InCmplDevItems.length;
      let mACActivityType = parent.mdsActivityType.value != null ? parent.mdsActivityTypeList.find(a => a.mdsActyTypeId == parent.mdsActivityType.value).mdsActyTypeDes : "";
      let cpeDeliveryOption = this.getFormControlValue("cpe.cpeDeliveryOption");
      if (isUSInternational == "I") {
        if (cpeDeliveryOption != null) {
          if (!(cpeDeliveryOption == 4 || cpeDeliveryOption == 3)) {
            IsSprintCPETabCnt = 1;
          }
          else {
            IsSprintCPETabCnt = 0;
          }
        }
      }

      let mdsOld = false;
      let mDSMACAct: any;
      let MdsMacActivityTypeIds: any[] = []
      if ((parent.activityType.value.includes(1) || parent.activityType.value.includes(3)) && (parent.mdsActivityType.value == 2 || parent.mdsActivityType.value == 5)) {
        let lmm: any[] = [];
        let selectedMacActivities = parent.mdsMacActivity.value as [];
        selectedMacActivities.forEach(a => parent.mdsMacActivityList.forEach(b => {
          if (a === b.mdsMacActyId) {
            lmm.push({
              mdsMacActyId: b.mdsMacActyId,
              MdsMacActy: {
                mdsMacActyId: b.mdsMacActyId,
                mdsMacActyNme: b.mdsMacActyNme
              }
            });
          }
        }));
        mDSMACAct = lmm;
        MdsMacActivityTypeIds = selectedMacActivities
      }

      let IsFirewallSecurity = this.CheckFirewallSecurityProduct(parent.mdsRedesignDevInfoList);

      let AnyVirtualConn = false;
      let VirtualTable = null;

      let IsDSLSBICCustTrptReq = thirdParty == null ? false : thirdParty.length > 0;
      let MDSDSLSBICCustTable = thirdParty;

      let IsSPRFWiredDevTrptReq = wiredTransport == null ? false : wiredTransport.length > 0;
      let MDSSPRFWiredTable = wiredTransport;

      let IsWirelessDeviceTransportRequired = wirelessTransport == null ? false : wirelessTransport.length > 0;
      let MDSWirelessTable = wirelessTransport;

      let MDSNtwkActy = mdsNtwk;
      let MDSNtwkTrpt = (!mdsNtwk.includes(1) ? networkTransport : null);

      // Hnadling null/undefined value
      let SCFlag = mdsRedesignDevInfo.some(a => a.scCd == "C");
      let params = [{
        odieDevList: InCmplDevItems,
        h1: H1 != null ? H1 : "",
        customerName: CustomeName,
        extraDuration: duration,
        isUSInternational: isUSInternational != null ? isUSInternational : "",
        mngDevTbl: InCmplDevItems,
        mngDevTblCnt: mngDevTblCnt,
        macActivityType: mACActivityType,
        isSprintCPETabCnt: IsSprintCPETabCnt,
        mdsOld: mdsOld,
        mdsMacAct: mDSMACAct,
        isFirewallSecurity: IsFirewallSecurity,
        anyVirtualConn: AnyVirtualConn,
        virtualTable: VirtualTable,
        isDSLSBICCustTrptReq: IsDSLSBICCustTrptReq,
        mdsDSLSBICCustTable: MDSDSLSBICCustTable,
        isSPRFWiredDevTrptReq: IsSPRFWiredDevTrptReq,
        mdsSPRFWiredTable: MDSSPRFWiredTable,
        isWirelessDeviceTransportRequired: IsWirelessDeviceTransportRequired,
        mdsWirelessTable: MDSWirelessTable,
        stTime: startString,
        stHr: start.getHours(),
        stMin: start.getMinutes(),
        product: this.eventType.toString(),
        submitType: "MDS",
        isMDSFT: 0,
        eventId: this.eventId.toString(),
        mdsNtwkActy: MDSNtwkActy,
        mdsNtwkTrpt: MDSNtwkTrpt,
        scFlag: SCFlag,
        vASCECd: this.parent.getVASCECd(),
        isSpclPrj: this.parent.isSpclPrj()
      }]

      console.log(params)
      this.spinner.show()
      this.userService.getNtwkIntlDuration(params[0]).subscribe(res => {
        console.log(res)
        this.setFormControlValue("schedule.eventDuration", res);
        let duration = parseInt(res);
        let end = this.helper.dateAdd(this.getFormControlValue("schedule.startDate"), "minute", duration);
        this.setFormControlValue("schedule.endDate", end);

        // Assigned Activator
        if (this.eventId == 0 || this.parent.editMode) {
          this.form.get("schedule.assignedToId").setValue(this.parent.ntwkIntlUser.userId)
          this.form.get("schedule.assignedTo").setValue(this.parent.ntwkIntlUser.dsplNme)
          this.form.get("schedule.displayedAssignedTo").setValue(this.parent.ntwkIntlUser.emailAdr)
        }

        this.spinner.hide();
      }, error => {
        this.spinner.hide()
      })
    }
  }
}

