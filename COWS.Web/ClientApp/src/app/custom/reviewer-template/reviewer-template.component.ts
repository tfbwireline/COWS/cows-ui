import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { UserFinderComponent } from "./../user-finder/user-finder.component";

@Component({
  selector: 'app-reviewer-template',
  templateUrl: './reviewer-template.component.html'
})
export class ReviewerTemplateComponent implements OnInit {
  @ViewChild("app-user-finder", { static: true }) userFinder: UserFinderComponent;
  @Input("group") form: FormGroup; // FormGroup passed from parent
  @Input() option: any;
  @Input() isSubmitted: boolean;

  get id() { return this.form.get('id'); }
  get name() { return this.form.get('name'); }
  get comments() { return this.form.get('comments'); }

  constructor() { }

  ngOnInit() {
    this.setValidators(this.id, this.option.id)
    this.setValidators(this.name, this.option.name)
  }

  setValidators(control: AbstractControl, option: any) {
    if (option != null && option != undefined) {
      let validators = option.validators
      if (validators != null && validators != undefined) {
        control.setValidators(Validators.compose(validators.map(a => a.type) as ValidatorFn[]))
        control.updateValueAndValidity()
      }
    }
  }

  isRequired(name) {
    if (this.option[name].validators != undefined) {
      return this.option[name].validators.find(a => a.name == "required") != undefined
    }

    return false;
  }
}
