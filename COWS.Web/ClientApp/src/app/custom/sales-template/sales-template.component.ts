import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

@Component({
  selector: 'app-sales-template',
  templateUrl: './sales-template.component.html'
})
export class SalesTemplateComponent implements OnInit {
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input() option: any
  @Input() isSubmitted: boolean

  get id() { return this.form.get("id") }
  get name() { return this.form.get("name") }
  get email() { return this.form.get("email") }

  constructor() { }

  ngOnInit() {
    this.setValidators(this.id, this.option.id.validators)
    this.setValidators(this.name, this.option.name.validators)
    this.setValidators(this.email, this.option.email.validators)
  }

  setValidators(control: AbstractControl, validators: any) {
    if (validators != null && validators != undefined) {
      control.setValidators(Validators.compose(validators.map(a => a.type) as ValidatorFn[]))
      control.updateValueAndValidity()
    }
  }

  isRequired(name) {
    if (this.option[name].validators != undefined) {
      return this.option[name].validators.find(a => a.name == "required") != undefined
    }

    return false;
  }
}
