import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sample-event',
  templateUrl: './sample-event.component.html'
})
export class SampleEventComponent implements OnInit {

  master = 'SampleEvent' //This is only needed if you need to show this one the child template
  
  requestor = null //assign it to the requestor contact when loading existing event

  constructor() { }

  ngOnInit() {
  }

}
