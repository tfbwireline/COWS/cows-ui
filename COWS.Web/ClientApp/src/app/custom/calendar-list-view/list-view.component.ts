import { Component, OnInit, Input } from '@angular/core';
import { EventViewsService } from '../../../../src/services';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from '../../../shared';
import { EEventType } from '../../../shared/global';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
})

export class ListViewComponent implements OnInit {
  @Input() apptTypeId: number;
  calendarViewDetailsList = <any>[];
  eventTypeId: number = 0;

  constructor(private helper: Helper, private router: Router,
    private eventService: EventViewsService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
   this.eventService.getCalendarViewDetails(101, this.apptTypeId).subscribe(
    data => {
       this.calendarViewDetailsList = data;
       if (this.calendarViewDetailsList != null && this.calendarViewDetailsList.length > 0) {
         this.eventTypeId = this.calendarViewDetailsList[0].evenT_TYPE_ID;
       }
     }, error => {
       console.log(error)
       this.spinner.hide();
     }, () => {
       this.spinner.hide()
     });
  }

  selectionChangedHandler(id) {
    if (this.eventTypeId == 1)
      this.router.navigate(['event/access-delivery/' + id]);
    else if (this.eventTypeId == 2)
      this.router.navigate(['event/ngvn/' + id]);
    else if (this.eventTypeId == 3)
      this.router.navigate(['event/mpls/' + id]);
    else if (this.eventTypeId == 4)
      this.router.navigate(['event/sprint-link/' + id]);
    else if (this.eventTypeId == 5)
      this.router.navigate(['event/mds/' + id]);
    else if (this.eventTypeId == 9)
      this.router.navigate(['event/fedline/' + id]);
    else if (this.eventTypeId == 10)
      this.router.navigate(['event/sipt/' + id]);
    else if (this.eventTypeId == 19)
      this.router.navigate(['event/ucaas/' + id]);
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    if (this.eventTypeId > 0) {
      e.fileName = EEventType[this.eventTypeId] + '_' + this.helper.formatDate(new Date());
    }
    else {
      e.fileName = 'CalendarEvents_' + this.helper.formatDate(new Date());
    }
  }
}
