import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, AbstractControl, Validators, ValidatorFn } from "@angular/forms";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Global, EEventType } from "./../../../shared/global";
import { SearchService } from "./../../../services/";
import * as moment from 'moment';
import { MdsFormComponent } from '../../event';

@Component({
  selector: 'app-lookup-site',
  templateUrl: './lookup-site.component.html'
})
export class LookupSiteComponent implements OnInit {
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input() parent: any;
  @Input() option: any;
  @Input() eventType: number;
  @Input() isSubmitted: boolean;
  @Input() isSiteNoteVisible: boolean = false;
  //@Input() prevUSIntlCd: string = "";
  @Output() result = new EventEmitter<any>();
  //@Output("setPrevUSIntlcd") setPrevUSIntlcd = new EventEmitter<any>();

  //showCpeOrderError: boolean = false;
  //siteIdAddress: string = "";
  errorMessage: string = "";

  get h6() { return this.form.get('h6'); }
  get ccd() { return this.form.get('ccd'); }
  get siteIdAddress() { return this.form.get('siteIdAddress'); }
  get showSiteId() { 
    if((this.h6.value !== '' && this.h6.value !== null) && (this.ccd.value !== '' && this.ccd.value !== null))
    {
      const siteId = this.form.get('siteIdAddress').value;
      return (siteId !== '' && siteId !== null);
    }
    return false;
  }

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private searchService: SearchService) { }

  ngOnInit() {
    //this.setValidators(this.h6, this.option.h6.validators)
    //this.setValidators(this.ccd, this.option.ccd.validators)
  }

  lookup() {
    this.isSubmitted = true;
    let h6 = this.form.get("h6").value.trim() || null;
    let ccd = this.form.get("ccd").value as Date || null;

    var regexH1H6 = /^[0-9]{9}$/;
    if (this.helper.isEmpty(h6) || !regexH1H6.test(h6)) {
      this.helper.notify('Please enter a 9-digit numeric h6 for lookup', Global.NOTIFY_TYPE_ERROR)
    }
    else if (this.helper.isEmpty(ccd)) {
      this.helper.notify('Please enter a valid ccd for lookup', Global.NOTIFY_TYPE_ERROR)
    }
    else {
      // Proceed to lookup
      this.spinner.show();
      let date = moment(ccd).format("MM/DD/YYYY")
      let code = this.eventType == EEventType.UCaaS ? "Y" : "N";

      let isCarrierEthernet = false;
      if (this.eventType == EEventType.MDS) {
        isCarrierEthernet = (this.parent as MdsFormComponent).showNID
      }

      this.searchService.getSiteLookup(h6, date, code, isCarrierEthernet).subscribe(
        res => {
          //this.siteIdAddress = ""
          this.errorMessage = ""

          if (code == "Y") {
            if (!this.helper.isEmpty(res.siteId)) {
              //this.siteIdAddress = res.siteId + '_' + res.ctyNme + ',' + res.sttPrvnNme;
              this.form.get("siteIdAddress").setValue(res.siteId + '_' + res.ctyNme + ',' + res.sttPrvnNme);
            }
            else {
              this.form.get("siteIdAddress").setValue('');
            }
            //console.log(res)
            //this.showCpeOrderError = res.event != null && res.event.eventCpeDev.length > 0 ? false : true;
            //this.errorMessage = "Sorry no CPE Order data found for that H6 and CCD combination"
            if (res.event.eventCpeDev.length == 0) {
              this.errorMessage = "Sorry, no CPE Order data found for that H6 and CCD combination";
              this.form.get("siteIdAddress").setValue('');
            }
          } else {
            //if (!this.helper.isEmpty(res.siteIdTxt)) {
            //  //this.siteIdAddress = res.siteId + '_' + res.ctyNme + ',' + res.sttPrvnNme;
            //  this.form.get("siteIdAddress").setValue(res.siteIdTxt + '_' + res.ctyNme + ',' + res.sttPrvnNme);
            //}
            //else {
            //  this.form.get("siteIdAddress").setValue('');
            //}

            //if (res.cpeDevices.length == 0 && res.mnsOrders.length == 0) {
            //  this.errorMessage = "Sorry, no CPE and MNS data found for that H6 and CCD combination"
            //  this.form.get("siteIdAddress").setValue('');
            //} else if (res.cpeDevices.length > 0 && res.cpeDevices.filter(a => a.ordrId == null || a.ordrId == -1).length > 0) {
            //  let deviceIds = res.cpeDevices.filter(a => a.ordrId == null || a.ordrId == -1).map(a => a.deviceId).join(", ")
            //  this.errorMessage = `There is no associated CPE order in COWS for these devices (${deviceIds}), Please validate before moving forward`
            //} else if (res.cpeDevices.length > 0 && res.mnsOrders.length == 0) {
            //  this.errorMessage = "Only CPE Order data found for the H6 and CCD combination !!"
            //} else if (res.cpeDevices.length == 0 && res.mnsOrders.length > 0) {
            //  this.errorMessage = "Only MNS Order data found for the H6 and CCD combination !!"
            //} else if (res.cpeDevices.length > 0 && res.mnsOrders.length > 0) {
            //  this.errorMessage = "CPE and MNS Order data found for the H6 and CCD combination !!"
            //}
          }

          this.result.emit(res);
          for (let field of this.option.mappings) {
            this.setFormControlValue(field[0], res[field[1]]);
          }
        },
        error => {
          this.spinner.hide();
          this.helper.notify("Error in retrieving site data by H6 and CCD.", Global.NOTIFY_TYPE_ERROR);
        }, () => this.spinner.hide()
      );
    }

    //if (!this.helper.isEmpty(h6) && !this.helper.isEmpty(ccd)) {
    //  // Proceed to lookup
    //  this.spinner.show();
    //  let date = moment(ccd).format("MM/DD/YYYY")
    //  let code = this.eventType == EEventType.UCaaS ? "Y" : "N";

    //  let isCarrierEthernet = false;
    //  if (this.eventType == EEventType.MDS) {
    //    isCarrierEthernet = (this.parent as MdsFormComponent).showNID
    //  }

    //  this.searchService.getSiteLookup(h6, date, code, isCarrierEthernet).subscribe(
    //    res => {
    //      //this.siteIdAddress = ""
    //      this.errorMessage = ""

    //      if (code == "Y") {
    //        if (!this.helper.isEmpty(res.siteId)) {
    //          //this.siteIdAddress = res.siteId + '_' + res.ctyNme + ',' + res.sttPrvnNme;
    //          this.form.get("siteIdAddress").setValue(res.siteId + '_' + res.ctyNme + ',' + res.sttPrvnNme);
    //        }
    //        else {
    //          this.form.get("siteIdAddress").setValue('');
    //        }
    //        //console.log(res)
    //        //this.showCpeOrderError = res.event != null && res.event.eventCpeDev.length > 0 ? false : true;
    //        //this.errorMessage = "Sorry no CPE Order data found for that H6 and CCD combination"
    //        if (res.event.eventCpeDev.length == 0) {
    //          this.errorMessage = "Sorry, no CPE Order data found for that H6 and CCD combination";
    //          this.form.get("siteIdAddress").setValue('');
    //        }
    //      } else {
    //        //if (!this.helper.isEmpty(res.siteIdTxt)) {
    //        //  //this.siteIdAddress = res.siteId + '_' + res.ctyNme + ',' + res.sttPrvnNme;
    //        //  this.form.get("siteIdAddress").setValue(res.siteIdTxt + '_' + res.ctyNme + ',' + res.sttPrvnNme);
    //        //}
    //        //else {
    //        //  this.form.get("siteIdAddress").setValue('');
    //        //}

    //        //if (res.cpeDevices.length == 0 && res.mnsOrders.length == 0) {
    //        //  this.errorMessage = "Sorry, no CPE and MNS data found for that H6 and CCD combination"
    //        //  this.form.get("siteIdAddress").setValue('');
    //        //} else if (res.cpeDevices.length > 0 && res.cpeDevices.filter(a => a.ordrId == null || a.ordrId == -1).length > 0) {
    //        //  let deviceIds = res.cpeDevices.filter(a => a.ordrId == null || a.ordrId == -1).map(a => a.deviceId).join(", ")
    //        //  this.errorMessage = `There is no associated CPE order in COWS for these devices (${deviceIds}), Please validate before moving forward`
    //        //} else if (res.cpeDevices.length > 0 && res.mnsOrders.length == 0) {
    //        //  this.errorMessage = "Only CPE Order data found for the H6 and CCD combination !!"
    //        //} else if (res.cpeDevices.length == 0 && res.mnsOrders.length > 0) {
    //        //  this.errorMessage = "Only MNS Order data found for the H6 and CCD combination !!"
    //        //} else if (res.cpeDevices.length > 0 && res.mnsOrders.length > 0) {
    //        //  this.errorMessage = "CPE and MNS Order data found for the H6 and CCD combination !!"
    //        //}
    //      }

    //      this.result.emit(res);
    //      for (let field of this.option.mappings) {
    //        this.setFormControlValue(field[0], res[field[1]]);
    //      }
    //    },
    //    error => {
    //      this.spinner.hide();
    //      this.helper.notify("Error in retrieving site data by H6 and CCD.", Global.NOTIFY_TYPE_ERROR);
    //    }, () => this.spinner.hide()
    //  );
    //}
    //// Added by Sarah Sandoval [20210224] - Mimic OLD COWS
    //// Added validation when Site Lookup is clicked
    //else {
    //  var regexH1H6 = /^[0-9]{9}$/;
    //  if (this.helper.isEmpty(h6) || !regexH1H6.test(h6)) {
    //    this.helper.notify('Please enter a 9-digit numeric h6 for lookup', Global.NOTIFY_TYPE_ERROR)
    //  }
    //  else if (this.helper.isEmpty(ccd)) {
    //    this.helper.notify('Please enter a valid ccd for lookup', Global.NOTIFY_TYPE_ERROR)
    //  }
    //}
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }

  setValidators(control: AbstractControl, validators: any) {
    if (validators != null && validators != undefined) {
      control.setValidators(Validators.compose(validators.map(a => a.type) as ValidatorFn[]))
      control.updateValueAndValidity()
    }
  }

  isRequired(name) {
    if (this.option[name].validators != undefined) {
      return this.option[name].validators.find(a => a.name == "required") != undefined
    }

    return false;
  }

  handleWhitespaces(event: any) {
    const newData = event.currentTarget.value.trim();
    event.currentTarget.value = newData
    this.form.get('h6').setValue(newData);
  }

}
