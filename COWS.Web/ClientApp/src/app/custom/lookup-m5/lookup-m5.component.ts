import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Global } from "./../../../shared/global";
import { SearchService } from "./../../../services/";
import { AdEvent } from "./../../../models/";

@Component({
  selector: 'app-lookup-m5',
  templateUrl: './lookup-m5.component.html'
})
export class LookupM5Component implements OnInit {
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input() option: any
  @Input() eventType: number // 5 = MDS

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private searchService: SearchService) { }

  ngOnInit() {
  }

  lookup() {
    let q = this.getFormControlValue(this.option.searchQuery) || null
    
    if (!this.helper.isEmpty(q)) {
      // Proceed to lookup

      this.spinner.show()
      this.searchService.getM5Lookup(q, this.eventType).subscribe(res => {
        let adEvent = res as AdEvent
        for (let field of this.option.mappings) {
          this.setFormControlValue(field[0], adEvent[field[1]])
        }
      }, error => {
        this.spinner.hide()
      }, () => this.spinner.hide());
    }
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }
}
