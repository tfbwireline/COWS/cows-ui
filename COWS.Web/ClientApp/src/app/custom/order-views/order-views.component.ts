import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DxDataGridComponent } from "devextreme-angular";
import { WorkGroupService } from '../../../services';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from '../../../shared';
import { Router } from '@angular/router';
import { WorkGroup } from '../../../shared/global';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-order-views',
  templateUrl: './order-views.component.html'
})
export class OrderViewsComponent implements OnInit {
  @Input() workGroup: number;
  userId: number;
  profile: string;
  availableViewsList: any;
  displayViewsList: any[] = [];
  displayViewsListByView: any[] = [];
  displayColumnsOnly: any[] = [];
  selectedViewId: any;
  eventViewDetailsList = <any>[];
  availableDefaultViewID: number;
  viewOptions: any;
  showViewOptions: boolean = false;
  //sortIndex: any[] = []; 

  dispColumns = [];
  columns = {
    ftn: { field: 'ftn', caption: 'M5 #', visible: true },
    proD_TYPE: { field: 'proD_TYPE' , caption: 'Product Type', visible: true },
    ordR_TYPE: { field: 'ordR_TYPE', caption: 'Order Type', visible: true },
    ordR_SUB_TYPE: { field: 'ordR_SUB_TYPE' , caption: 'Order Subtype', visible: true },
    cusT_CMMT_DT: { field: 'cusT_CMMT_DT', caption: 'CCD', visible: true },
    ordR_SBMT_DT: { field: 'ordR_SBMT_DT', caption: 'Submit Date', visible: true },
    cusT_NME: { field: 'cusT_NME' , caption: 'Customer Name', visible: true },
    asN_USER: { field: 'asN_USER' , caption: 'Assigned User', visible: true },
    //tsuP_PRS_QOT_NBR: { field: 'tsuP_PRS_QOT_NBR' , caption: 'PRS Quote ID', visible: true },
    ordR_STUS: { field: 'ordR_STUS' , caption: 'Order Status', visible: true },
    vndR_NME: { field: 'vndR_NME' , caption: 'Vendor Name', visible: true },
    //fsA_EXP_TYPE_CD: { field: 'fsA_EXP_TYPE_CD' , caption: 'Expedite', visible: false },
    //instL_ESCL_CD: { field: 'instL_ESCL_CD' , caption: 'Escalate', visible: true },
    ttrpT_SPD_OF_SRVC_BDWD_DES: { field: 'ttrpT_SPD_OF_SRVC_BDWD_DES' , caption: 'Port Speed', visible: true },
    accessBandwidth: { field: 'accessBandwidth', caption: 'Access Bandwidth', visible: true }, 
    ctY_NME: { field: 'ctY_NME' , caption: 'City', visible: true },
    ctrY_NME: { field: 'ctrY_NME' , caption: 'Country', visible: true },
    rtS_FLG: { field: 'rtS_FLG' , caption: 'RTS Flag', visible: true },
    rtS_DT: { field: 'rtS_DT' , caption: 'RTS Date', visible: true },
    xncI_UPDT_DT: { field: 'xncI_UPDT_DT', caption: 'Last xNCI Update', visible: true },
    //adr: { field: 'adr' , caption: 'Address', visible: true },
    trgT_DLVRY_DT: { field: 'trgT_DLVRY_DT', caption: 'Target Delivery Date', visible: true },
    cusT_WANT_DT: { field: 'cusT_WANT_DT', caption: 'CWD', visible: true },
    cpE_ORDR_STUS: { field: 'cpE_ORDR_STUS' , caption: 'CPE Order Status', visible: true },
    h5_H6_CUST_NME: { field: 'h5_H6_CUST_NME' , caption: 'H6 Customer Name', visible: true },
    jprdY_CD: { field: 'jprdY_CD' , caption: 'Jeopardy Code', visible: true },
    sitE_ID: { field: 'sitE_ID' , caption: 'Site ID', visible: true },
    tasK_ID: { field: 'tasK_ID' , caption: 'Task ID', visible: false },
    prcH_ORDR_NBR: { field: 'prcH_ORDR_NBR' , caption: 'PO #', visible: true },
    plsfT_RQSTN_NBR: { field: 'plsfT_RQSTN_NBR' , caption: 'Requisition Number', visible: true },
    soI_CD: { field: 'soI_CD' , caption: 'SOI Code', visible: true },
    ordR_ACTN_DES: { field: 'ordR_ACTN_DES' , caption: 'Action Type', visible: true },
    h5_H6_CUST_ID: { field: 'h5_H6_CUST_ID' , caption: 'H6 CUST_ID', visible: true },
    pltfrM_NME: { field: 'pltfrM_NME' , caption: 'Platform', visible: false },
    ordR_RCVD_DT: { field: 'ordR_RCVD_DT' , caption: 'Order Received Date', visible: true },
    dmstC_CD: { field: 'dmstC_CD' , caption: 'INTL/DOM', visible: false },
    srvC_SUB_TYPE: { field: 'srvC_SUB_TYPE' , caption: 'Service Subtype', visible: true },
    chngD_CCD: { field: 'chngD_CCD' , caption: 'Changed CCD', visible: true },
    taC_Primary: { field: 'taC_Primary' , caption: 'Account Team IPM', visible: true },
    ziP_CD: { field: 'ziP_CD' , caption: 'Postal Code', visible: true },
    cpE_EVENT_ID: { field: 'cpE_EVENT_ID' , caption: 'Event ID', visible: true },
    evenT_START_TIME: { field: 'evenT_START_TIME' , caption: 'Event Start Time', visible: true },
    evenT_STUS_DES: { field: 'evenT_STUS_DES' , caption: 'Event Status', visible: true },
    hold: { field: 'hold' , caption: '', visible: false },
    ordR_ID: { field: 'ordR_ID' , caption: '', visible: false },
    slA_VLTD_CD: { field: 'slA_VLTD_CD' , caption: '', visible: false },
    rtssla: { field: 'rtssla' , caption: '', visible: false },
    // fsA_EXP_TYPE_CD: { field: 'fsA_EXP_TYPE_CD' , caption: '', visible: false },
    // pltfrM_NME: { field: 'pltfrM_NME' , caption: '', visible: false },
    prnT_ORDR_ID: { field: 'prnT_ORDR_ID' , caption: '', visible: false },
    prnT_FTN: { field: 'prnT_FTN' , caption: '', visible: false },
    // ordR_TYPE: { field: 'ordR_TYPE' , caption: '', visible: false },
    iS_EXP: { field: 'iS_EXP', caption: 'Is Expedite', visible: true },
    instL_ESCL_CD: { field: 'instL_ESCL_CD', caption: '', visible: false },
    //ordR_SUB_TYPE: { field: 'ordR_SUB_TYPE', caption: '', visible: false },
    adr: { field: 'adr', caption: '', visible: false },
    inbox: { field: 'inbox', caption: 'INBOX', visible: true },
    isLocked: { field: 'isLocked', caption: 'Locked', visible: false },
    lockedBy: { field: 'lockedBy', caption: 'Locked By', visible: false },
    tsuP_PRS_QOT_NBR: { field: 'tsuP_PRS_QOT_NBR', caption: '', visible: false },
    ordR_HIGHLIGHT_CD: { field: 'ordR_HIGHLIGHT_CD', caption: '', visible: false },
    rtS_CCD_HIGHLIGHT_CD: { field: 'rtS_CCD_HIGHLIGHT_CD', caption: '', visible: false }, 
  };

  @ViewChild('grid', { static: true }) dataGrid: DxDataGridComponent;

  constructor(private helper: Helper, private router: Router, private spinner: NgxSpinnerService,
    private workGroupService: WorkGroupService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.userId = Number(localStorage.getItem('userID'));
    if (this.workGroup == 2 || this.workGroup == 16 || this.workGroup == 100 || this.workGroup == 58 || this.workGroup == 114 || this.workGroup == 156) {
      this.showViewOptions = true;
    }
    else {
      this.showViewOptions = false;
    }
    this.pageLoad();
  }

  pageLoad(): void {
    this.viewOptions = [
      { "id": 1, "name": "Create View" },
      { "id": 2, "name": "Modify View" },
      { "id": 3, "name": "Delete View" }
    ]

    this.getEventViews(this.userId);
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = WorkGroup[this.workGroup] + '_' + this.helper.formatDate(new Date());
  }

  contentReadyHandler(e) {
    // For Search Panel
    if (!this.helper.isEmpty(this.dataGrid.searchPanel.text.trim())) {
      this.dataGrid.instance.searchByText(this.dataGrid.searchPanel.text.trim())
    }

    // For Column Options
    this.dataGrid.instance.columnOption('ftn', 'cellTemplate', 'orderIdTemplate');

    this.dataGrid.instance.columnOption('cusT_CMMT_DT', 'dataType', 'date');
    this.dataGrid.instance.columnOption('cusT_CMMT_DT', 'format', 'MMM/dd/yy');

    this.dataGrid.instance.columnOption('ordR_SBMT_DT', 'dataType', 'date');
    this.dataGrid.instance.columnOption('ordR_SBMT_DT', 'format', 'MMM/dd/yy');
    
    this.dataGrid.instance.columnOption('xncI_UPDT_DT', 'dataType', 'datetime');
    this.dataGrid.instance.columnOption('xncI_UPDT_DT', 'format', 'shortDateShortTime');

    this.dataGrid.instance.columnOption('trgT_DLVRY_DT', 'dataType', 'datetime');
    this.dataGrid.instance.columnOption('trgT_DLVRY_DT', 'format', 'shortDateShortTime');
    
    this.dataGrid.instance.columnOption('cusT_WANT_DT', 'dataType', 'datetime');
    this.dataGrid.instance.columnOption('cusT_WANT_DT', 'format', 'shortDateShortTime');

    //this.dataGrid.instance.columnOption('rtS_DT', 'dataType', 'date');
    //this.dataGrid.instance.columnOption('rtS_DT', 'format', 'MMM/dd/yy');

    this.dataGrid.instance.columnOption('ordR_RCVD_DT', 'dataType', 'date');
    this.dataGrid.instance.columnOption('ordR_RCVD_DT', 'format', 'shortDate');

    //this.dataGrid.instance.columnOption('evenT_START_TIME', 'dataType', 'datetime');
    //this.dataGrid.instance.columnOption('evenT_START_TIME', 'format', 'shortDateShortTime');
  }

  onCellPrepared(e) {
    if (e.rowType == 'data') {
      e.cellElement.style.textAlign = 'left'
    }
    if (e.rowType == 'header') {
      e.cellElement.style.textAlign = 'left'
    }
  }

  onRowPrepared(e) {
    if (e.rowType == 'data' && !this.helper.isEmpty(e.key)
      && !this.helper.isEmpty(e.data.isLocked)) {
      e.rowElement.className = e.rowElement.className + ' checked-in-row';
      e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
      e.rowElement.title = 'This order is locked out by ' + e.data.lockedBy;
    }

    if(e.rowType == 'data') {
      if(this.workGroup == WorkGroup.GOM ) {
          if (e.data['instL_ESCL_CD'] == 'Yes' && e.data['fsA_EXP_TYPE_CD'] != '')
          {
            e.rowElement.className = 'dx-row dx-data-row dx-column-lines escalate-expedite';
          }
          else if (e.data['instL_ESCL_CD'] == 'Yes')
          {
              e.rowElement.className = "dx-row dx-data-row dx-column-lines escalate";
          }
          else if (e.data['fsA_EXP_TYPE_CD'] != '')
          {
              e.rowElement.className = "dx-row dx-data-row dx-column-lines expedite";
          }
      }
      else if(this.workGroup == WorkGroup.xNCIAmerica || this.workGroup == WorkGroup.xNCIAsia || this.workGroup == WorkGroup.xNCIEurope) {
        if (e.data['ordR_HIGHLIGHT_CD'] == 1)
        {
          e.rowElement.className = 'dx-row dx-data-row dx-column-lines new-ordr-assigned-row';
        }
        else if (e.data['rtS_CCD_HIGHLIGHT_CD'])
        {
            e.rowElement.className = "dx-row dx-data-row dx-column-lines rts-ccd-notification-assigned-row";
            e.rowElement.title = 'New CCD/RTS notification has been issued.';
        }
      } 
    }
  }

  selectionChangedHandler(row: any) {
    let selectedRow = row.data;
    if (this.workGroup == 2)
      this.router.navigate(['order/amnci/' + this.workGroup + '/' + selectedRow.ordR_ID], { queryParams: { taskId: selectedRow.tasK_ID }});
    else if (this.workGroup == 16)
      this.router.navigate(['order/anci/' + this.workGroup + '/' + selectedRow.ordR_ID], { queryParams: { taskId: selectedRow.tasK_ID } });
    else if (this.workGroup == 100)
      this.router.navigate(['order/enci/' + this.workGroup + '/' + selectedRow.ordR_ID], { queryParams: { taskId: selectedRow.tasK_ID } });
    else if (this.workGroup == 114) {
      const hasHyphen = selectedRow['ftn'].includes('-');
      if(hasHyphen) {
        const arr = selectedRow['ftn'].split('-');
        sessionStorage.setItem('deviceId', arr[1]);
      }
      this.router.navigate(['order/gom/' + this.workGroup + '/' + selectedRow.ordR_ID]);
    }
    else if (this.workGroup == 86)
      this.router.navigate(['order/dcpe/' + this.workGroup + '/' + selectedRow.ordR_ID]);
    else if (this.workGroup == 44)
      this.router.navigate(['order/3rd-party-cpe-tech/' + this.workGroup + '/' + selectedRow.ordR_ID]);
    else if (this.workGroup == 170)
      this.router.navigate(['order/vcpe/' + this.workGroup + '/' + selectedRow.ordR_ID]);
    else if (this.workGroup == 128) 
      this.router.navigate(['order/cpe-receipts/' + this.workGroup + '/' + selectedRow.ordR_ID + '/' + selectedRow.tasK_ID + '/' + selectedRow.prcH_ORDR_NBR]);     
    else if (this.workGroup == 156)
      this.router.navigate(['order/rts/' + this.workGroup + '/' + selectedRow.ordR_ID + '/' + selectedRow.tasK_ID]);
    else if (this.workGroup == 58)
      this.router.navigate(['order/csc/' + this.workGroup + '/' + selectedRow.ordR_ID]);
  }

  getEventViews(userId: number) {
    //this.spinner.show();
    this.workGroupService.GetOrderDisplayViewData(userId, this.workGroup).subscribe(
      data => {
        let viewList = data.table;
        if(this.workGroup === WorkGroup.GOM) {
          viewList = data.table.filter(x => x.dspL_VW_ID != 199 && x.dspL_VW_ID != 198);
          viewList = this.helper.ArrayValueStringReplace(viewList, ['dspL_VW_NME']);
        }

        this.availableViewsList = viewList;
        if (!this.helper.isEmpty(localStorage.getItem('selectedOrderViewId')) && this.availableViewsList.find(a => a.dspL_VW_ID == localStorage.getItem('selectedOrderViewId'))) {
          this.availableDefaultViewID = Number(localStorage.getItem('selectedOrderViewId'));
        }
        else {
          this.availableDefaultViewID = this.availableViewsList[0].dspL_VW_ID;
          localStorage.setItem('selectedOrderViewId', this.availableViewsList[0].dspL_VW_ID);
        }        
        this.displayViewsList = data.table1;
        // this.displayViewsListByView = this.displayViewsList.filter(a => a.dspL_VW_ID == this.availableDefaultViewID).map(a => a.srC_TBL_COL_NME_NEW);
        //this.getViewData(this.availableDefaultViewID); // COMMENTED SINCE WE ARE ALREADY SETTING THE this.availableDefaultViewID BEFORE THIS
      },
      error => {
        console.log(error)
        this.spinner.hide();
      });
  }

  getViewData(value: number) {
    this.spinner.show();
    this.displayViewsListByView = this.displayViewsList.filter(a => a.dspL_VW_ID == value).map(a => a.srC_TBL_COL_NME_NEW);
    this.displayColumnsOnly = this.displayViewsListByView;
    this.displayViewsListByView.push("ordR_ID");
    this.displayViewsListByView.push("tasK_ID");
    this.displayViewsListByView.push("isLocked");
    this.displayViewsListByView.push("lockedBy");
    if(this.workGroup == WorkGroup.xNCIAmerica || this.workGroup == WorkGroup.xNCIAsia || this.workGroup == WorkGroup.xNCIEurope ) {
      const index = this.displayViewsListByView.indexOf('ttrpT_SPD_OF_SRVC_BDWD_DES');
      this.displayViewsListByView.splice(index, 0, "accessBandwidth");
    }

    // for row css purpose
    this.displayViewsListByView.push("ordR_HIGHLIGHT_CD");
    this.displayViewsListByView.push("rtS_CCD_HIGHLIGHT_CD");
    
    this.selectedViewId = value; 
    localStorage.setItem('selectedOrderViewId', value.toString());
    this.dispColumns = [];
    this.displayViewsListByView.map(key => {
      if(this.columns[key] !== undefined) {
        this.dispColumns.push(this.columns[key]);
      }
    })
    if (this.availableViewsList.find(a => a.dspL_VW_ID == value).pblC_VW_CD == 'Y') {
      this.viewOptions.splice(1, 2);
    }
    else {
      this.viewOptions = [
        { "id": 1, "name": "Create View" },
        { "id": 2, "name": "Modify View" },
        { "id": 3, "name": "Delete View" }
      ]
    }

    let isCompleted = this.availableViewsList.find(a => a.dspL_VW_ID == value).dspL_VW_NME.includes('Completed');
    let myViews = this.availableViewsList.find(a => a.dspL_VW_ID == value).dspL_VW_NME.includes('My');
    let filter = this.availableViewsList.filter(b => b.dspL_VW_ID == value).map(a => a.filter)[0];
    
    if(filter != null) {
      if (filter.includes("IN") > 0) {
        filter = filter.replace("'(", "(").replace(")'", ")")
      }
    }

    let userId = myViews ? this.userId : 0;

    filter = (filter == null) ? "" : filter;

    this.workGroupService.GetWGData(this.workGroup, userId, 0, isCompleted, filter.toString().trim()).subscribe(
      data => {
        console.log("A")
        //this.spinner.show();
        let jsonData = JSON.stringify(data, this.displayViewsListByView);
        this.eventViewDetailsList = JSON.parse(jsonData);

        //for (let i = 0; i < this.eventViewDetailsList.length; i++) {
        //  if (this.eventViewDetailsList[i]["cusT_CMMT_DT"] != null) {
        //    var ccd = new Date(this.eventViewDetailsList[i]["cusT_CMMT_DT"]);
        //    this.eventViewDetailsList[i]["cusT_CMMT_DT"] = this.helper.formatDate(ccd);
        //    //this.eventViewDetailsList[i]["cusT_CMMT_DT"] = this.datePipe.transform(ccd, "MMM/dd/yy")
        //  }

        //  if (this.eventViewDetailsList[i]["ordR_SBMT_DT"] != null) {
        //    var submitDate = new Date(this.eventViewDetailsList[i]["ordR_SBMT_DT"]);
        //    this.eventViewDetailsList[i]["ordR_SBMT_DT"] = this.helper.formatDate(submitDate);
        //    //this.eventViewDetailsList[i]["ordR_SBMT_DT"] = this.datePipe.transform(submitDate, "yyyy/MM/dd")
        //  }

        //  if (this.eventViewDetailsList[i]["xncI_UPDT_DT"] != null) {
        //    var xnciUpdateDate = new Date(this.eventViewDetailsList[i]["xncI_UPDT_DT"]);
        //    this.eventViewDetailsList[i]["xncI_UPDT_DT"] = this.helper.formatDate(xnciUpdateDate);
        //  }

        //  if (this.eventViewDetailsList[i]["cusT_WANT_DT"] != null) {
        //    var cwd = new Date(this.eventViewDetailsList[i]["cusT_WANT_DT"]);
        //    this.eventViewDetailsList[i]["cusT_WANT_DT"] = this.helper.formatDate(cwd);
        //  }

        //  if (this.eventViewDetailsList[i]["trgT_DLVRY_DT"] != null) {
        //    var trgtDlvyDate = new Date(this.eventViewDetailsList[i]["trgT_DLVRY_DT"]);
        //    this.eventViewDetailsList[i]["trgT_DLVRY_DT"] = this.helper.formatDate(trgtDlvyDate);
        //  }

        //  //if (this.eventViewDetailsList[i]["rtS_DT"] != null) {
        //  //  var rtsDate = new Date(this.eventViewDetailsList[i]["rtS_DT"]);
        //  //  this.eventViewDetailsList[i]["rtS_DT"] = this.helper.formatDate(rtsDate);
        //  //}

        //  if (this.eventViewDetailsList[i]["ordR_RCVD_DT"] != null) {
        //    var ordrRcvdDate = new Date(this.eventViewDetailsList[i]["ordR_RCVD_DT"]);
        //    this.eventViewDetailsList[i]["ordR_RCVD_DT"] = this.helper.formatDate(ordrRcvdDate);
        //  }

        //  //if (this.eventViewDetailsList[i]["evenT_START_TIME"] != null) {
        //  //  var evntStrtTime = new Date(this.eventViewDetailsList[i]["evenT_START_TIME"]);
        //  //  this.eventViewDetailsList[i]["evenT_START_TIME"] = this.helper.formatDate(evntStrtTime);
        //  //}
        //}

        //if (this.selectedViewId == 198 || this.selectedViewId == 199 || this.selectedViewId == 201
        //  || this.selectedViewId == 202 || this.selectedViewId == 215 || this.selectedViewId == 222
        //  || this.selectedViewId == 223 // GOM Orders

        //  || this.selectedViewId == 630 || this.selectedViewId == 631 // MDS Orders

        //  || this.selectedViewId == 217 || this.selectedViewId == 205 // CSC Orders

        //  || this.selectedViewId == 126 || this.selectedViewId == 127 || this.selectedViewId == 207
        //  || this.selectedViewId == 208 || this.selectedViewId == 218 // RTS Orders

        //  || this.selectedViewId == 209 || this.selectedViewId == 210 || this.selectedViewId == 219 // AMNCI

        //  || this.selectedViewId == 211 || this.selectedViewId == 212 || this.selectedViewId == 220 // ANCI

        //  || this.selectedViewId == 125 // My Orders
        //  ) {
        //  for(let i = 0; i < this.eventViewDetailsList.length; i++) {
        //    if (this.eventViewDetailsList[i]["cusT_CMMT_DT"] != null) {
        //      var startDate = new Date(this.eventViewDetailsList[i]["cusT_CMMT_DT"]);
        //      this.eventViewDetailsList[i]["cusT_CMMT_DT"] = this.datePipe.transform(startDate, "MMM/dd/yy")
        //    }

        //    if (this.eventViewDetailsList[i]["ordR_SBMT_DT"] != null) {
        //      var submitDate = new Date(this.eventViewDetailsList[i]["ordR_SBMT_DT"]);
        //      this.eventViewDetailsList[i]["ordR_SBMT_DT"] = this.datePipe.transform(submitDate, "yyyy/MM/dd")
        //    }

        //    if (this.eventViewDetailsList[i]["xncI_UPDT_DT"] != null) {
        //      var xnciUpdateDate = new Date(this.eventViewDetailsList[i]["xncI_UPDT_DT"]);
        //      this.eventViewDetailsList[i]["xncI_UPDT_DT"] = this.helper.formatDate(xnciUpdateDate);
        //    }

        //    if (this.eventViewDetailsList[i]["cusT_WANT_DT"] != null) {
        //      var cwd = new Date(this.eventViewDetailsList[i]["cusT_WANT_DT"]);
        //      this.eventViewDetailsList[i]["cusT_WANT_DT"] = this.helper.formatDate(cwd);
        //    }

        //    if (this.eventViewDetailsList[i]["trgT_DLVRY_DT"] != null) {
        //      var trgtDlvyDate = new Date(this.eventViewDetailsList[i]["trgT_DLVRY_DT"]);
        //      this.eventViewDetailsList[i]["trgT_DLVRY_DT"] = this.helper.formatDate(trgtDlvyDate);
        //    }
        //  }
        //}
      },
      error => {
        console.log(error)
        this.spinner.hide();
      },
      () => {
        this.spinner.hide();
      });
  }
  
  createView(value) {
    if (value == 1) {
      this.router.navigate(['order/custom-order-view/' + this.workGroup + '/' + 0]);
    }
    else if (value == 2) {
      this.router.navigate(['order/custom-order-view/' + this.workGroup + '/' + this.selectedViewId]);
    }
    else if (value == 3) {
      if (confirm("Are you sure you want to delete the selected view?")) {
        //this.workGroupService.DeleteView(this.selectedViewId).toPromise().then(
        //  data => {
        //    if (data) {
        //      this.helper.notify(`Successfully Deleted Event View`, Global.NOTIFY_TYPE_SUCCESS);
        //      this.pageLoad();
        //      this.selectionChangedHandler('');
        //    }
        //    else {
        //      this.helper.notify(`Failed to Delete Event View. `, Global.NOTIFY_TYPE_ERROR);
        //    }
        //  },
        //  error => {
        //    this.helper.notify(`Failed to Delete Event View. ` + error.message, Global.NOTIFY_TYPE_ERROR);
        //  });
      }
    }
  }
}
