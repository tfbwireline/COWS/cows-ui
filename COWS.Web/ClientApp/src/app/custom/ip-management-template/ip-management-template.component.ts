import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import ArrayStore from 'devextreme/data/array_store';

import { take } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { IPManagementService } from '../../../services';
import { IpType } from '../../../shared/global';
import { NgxSpinnerService } from 'ngx-spinner';
import { DxDataGridComponent } from "devextreme-angular";

@Component({
    selector: 'app-ip-management-template',
    templateUrl: './ip-management-template.component.html',
    styleUrls: ['./ip-management-template.component.scss'],
    encapsulation: 2
})

export class IPManagementTemplateComponent implements OnInit, AfterViewInit {
    @ViewChild("ipDataGrid", { static: false}) dataGrid: DxDataGridComponent
    @Input() type: IpType;
    testSearch : any;
    public ipDetailsColumn = [];
    public ipDetailsColumns = {
        carrier : [
            { caption : 'System', field : 'systeM_CD' },
            { caption : 'Start IP', field : 'strT_IP_ADR' },
            { caption : 'End IP', field : 'enD_IP_ADR' },
            { caption : 'IP Address', field : 'iP_ADR' },
            { caption : 'NID SN', field: 'niD_SERIAL_NBR' },
            { caption : 'NID Hostname', field: 'niD_HOST_NME' },
            { caption : 'Mach5 Order#', field: 'm5_ORDR_NBR' },
            { caption : 'Mach5 Device#', field: 'devicE_ID' },
            { caption : 'H6', field: 'h6' },
            { caption : 'MDS EventID', field: 'evenT_ID' },
            { caption : 'Status', field: 'stuS_DES' },
        ],
        sip : [
            { caption : 'System', field : 'systeM_CD' },
            { caption : 'Start IP', field : 'strT_IP_ADR' },
            { caption : 'End IP', field : 'enD_IP_ADR' },
            { caption : 'IP Address', field : 'iP_ADR' },
            { caption : 'Design Doc #', field: 'dD_APRVL_NBR' },
            { caption : 'H6', field: 'h6' },
            { caption : 'Status', field: 'stuS_DES' },
        ]
    }

    ipData = [];
    systemData = [];
    
    ipDataSourceStorage = [];
    ipDetailsDataSourceStorage = [];

    // ipData = [];
    ipDataDetails = [];

    test: any;
    triggered = false;

    constructor(private spinner: NgxSpinnerService, private ipManagementService: IPManagementService ) {

    }

    ngOnInit() {
        this.spinner.show();
        this.ipManagementService.GetIPRecords(this.type).subscribe(data => {
            this.ipData = data;
            console.log(data);
            this.spinner.hide();
        })  
    }

    ngAfterViewInit() {
        switch(this.type) {
            case IpType.CarrierEthernet:
                this.ipDetailsColumn = this.ipDetailsColumns.carrier;
                break;
            case IpType.SipNat:
                this.ipDetailsColumn = this.ipDetailsColumns.sip;
                break;     

        }
    }

}