//import { Component, OnInit, Input } from '@angular/core';
//import { FormGroup } from '@angular/forms';
//import { Helper } from '../../../shared';
//import { NgxSpinnerService } from 'ngx-spinner';
//import { NccoOrderFormComponent } from '../../ncco';
//import { zip } from 'rxjs';
//import {
//  OrderTypeService,
//  ProductTypeService,
//  CountryService,
//  VendorService, } from '../../../services';
//import { SearchTypes } from '../../search/search.criteria';

//@Component({
//  selector: 'app-ncco-order-details',
//  templateUrl: './ncco-order-details.component.html'
//})
//export class NccoOrderDetailsComponent implements OnInit {
//  //@Input("parent") parent: any;
//  @Input("group") form: FormGroup;
//  @Input() showEditButton: boolean

//  isSubmitted: boolean = false;
//  productTypes: any[];
//  orderTypes: any[];
//  countries: any[];
//  vendors: any[];
//  lassies: SearchTypes[]

//  get comment() { return this.form.get("comment") }

//  constructor(private helper: Helper, private spinner: NgxSpinnerService,
//    private OrderTypeService: OrderTypeService,
//    private ProductTypeService: ProductTypeService,
//    private CountryService: CountryService,
//    private VendorService: VendorService) { }

//  ngOnInit() {
//    //this.isSubmitted = this.parent.isSubmitted;
//    this.LoadData();
//  }

//  LoadData() {
//    let data = zip(
//      this.ProductTypeService.getProductTypeByOrdrCatId(4),
//      this.OrderTypeService.get(),
//      this.CountryService.get(),
//      this.VendorService.get()
//    )

//    this.spinner.show();
//    data.subscribe(res => {
//      this.productTypes = res[0]
//      this.orderTypes = res[1]
//      this.countries = res[2]
//      this.vendors = res[3]

//      this.lassies = [
//        new SearchTypes("Yes", 1, []),
//        new SearchTypes("No", 2, [])
//      ]

//    }, error => {
//      this.spinner.hide();
//    }, () => {
//      this.spinner.hide();
//    });
//  }

//  //characterCount() {
//  //  try {
//  //    var len = 0;
//  //    len = this.comment.value.length == null ? 0 : this.comment.value.length;
//  //    len = this.comment.value.length == null ? 0 : this.comment.value.length;
//  //    if (len >= 500) {
//  //      this.parent.mainCommentCount = 0;
//  //    } else {
//  //      this.parent.mainCommentCount = this.parent.maxchar - len - 1;
//  //    }
//  //  } catch (e) {
//  //    console.log(e);
//  //  }
//  //}
//}
