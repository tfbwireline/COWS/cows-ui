import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import { Global } from "./../../../shared/global";
import { SearchService } from "./../../../services/";
import { AdEvent, OdieRspn } from "./../../../models/";

@Component({
  selector: 'app-lookup-h1',
  templateUrl: './lookup-h1.component.html'
})
export class LookupH1Component implements OnInit {
  @Input("group") form: FormGroup // FormGroup passed from parent
  @Input() option: any
  @Output() result = new EventEmitter<OdieRspn[]>()

  constructor(private helper: Helper, private spinner: NgxSpinnerService, private searchService: SearchService) { }

  ngOnInit() {
  }

  lookup() {
    let q = this.getFormControlValue(this.option.searchQuery) || null

    if (!this.helper.isEmpty(q)) {
      // Proceed to lookup
      this.spinner.show()
      this.searchService.getH1Lookup(q).subscribe(res => {
        this.result.emit(res as OdieRspn[])
        //for (let field of this.option.mappings) {
        //  this.setFormControlValue(field[0], res[field[1]])
        //}
      }, error => {
        this.spinner.hide()
      }, () => this.spinner.hide());
    }
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";

    if (this.form.get(name)) {
      value = this.form.get(name).value
    }

    return value;
  }
}
