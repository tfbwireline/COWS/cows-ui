import { Component, OnInit, Input } from '@angular/core';
import { Appointment } from '../../../models/appointment-model';
import { ApptType } from '../../../models/appointment-types.model';
import { Resource } from '../../../models/appointment-resource.model';
import { CalendarService, UserService } from '../../../services';
import { CalendarEventType } from '../../../shared/global';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.css']
})
export class CalendarViewComponent implements OnInit {
  @Input() apptTypeId: number;
  currentDate: Date = new Date();
  currentView = "month";

  // Data for displaying calendar appointments to dx-scheduler
  appointmentsData: Appointment[] = [];
  apptTypeData: ApptType[] = [];
  resourceData: Resource[] = [];

  appointment: Appointment;
  appointmentType: ApptType;
  appointmentResource: Resource;

  // Data from the API call
  appointmentData: any;
  appointmentTypeData: any;
  appointmentResouceData: any;

  resourcesIds: number[] = [];
  resources: Resource[] = [];

  constructor(private spinner: NgxSpinnerService, private calendarSrvc: CalendarService, private userService: UserService,
    private router: Router, private datePipe: DatePipe) { }

  ngOnInit() {
    this.spinner.show();
    // Get the Appointment Types
    this.getApptTypeData();

    // Updated by Sarah Sandoval [20200611] - Incorporate the two methods
    // getApptTypeData() and getResourcesData() is calling the same api
    // Get the Resources
    //this.getResourcesData();

    // Get Calendar data
    this.getCalendarData();
  }

  getCalendarData() {
    this.calendarSrvc.getCalendarData(0, this.userService.loggedInUser.userId, this.apptTypeId).subscribe( 
      res => {
        this.appointmentData = res["table"];

        for (var i = 0; i < this.appointmentData.length; i++) {
          this.appointment = new Appointment();

          // Will break the string and get the event ID
          let eventIdLayer1 = this.appointmentData[i].subJ_TXT.split('Event ID :');

          // String must be break into 2 else there's no event ID
          if(eventIdLayer1.length > 1) {
            // Some subject has following strings after the Event ID
            let eventIdLayer2 = eventIdLayer1[1].split(',');
            if(eventIdLayer2.length > 1) {
              this.appointment.eventid = eventIdLayer2[0].trim();
            } else {
              this.appointment.eventid = eventIdLayer1[1].trim();
            }
          }
          this.appointment.text = this.appointmentData[i].subJ_TXT
          if(this.apptTypeId === 4) {
            this.appointment.softAssigned = this.appointmentData[i].sofT_ASSIGN_CD ? "True" : "false";
          }

          let startDate = new Date(this.appointmentData[i].strT_TMST);
          let endDate = new Date(this.appointmentData[i].enD_TMST);

          let appointmentStartDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startDate.getHours(), startDate.getMinutes());
          let appointmentEndDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), endDate.getHours(), endDate.getMinutes());

          this.appointment.startDate = appointmentStartDate;
          this.appointment.endDate = appointmentEndDate;

          this.appointment.allDay = this.isAllday(startDate.getHours(), startDate.getMinutes(), endDate.getHours(), endDate.getMinutes());

          this.appointment.description = this.appointmentData[i].des;

          // TODO: Set the Recurrence Rule from rcurnC_DES_TXT field
          //this.appointment.recurrenceRule = this.appointmentData[i].rcurnC_DES_TXT;

          // Appointment data additonal fields.
          this.appointment.location = this.appointmentData[i].appT_LOC_TXT;
          this.appointment.apptType = Number(this.appointmentData[i].appT_TYPE_ID)

          // Logic to Build Resource Object
          let currentResource: Resource;
          let currentResourceData: Resource[] = [];
          //let resourceIds = [7001,6241,7002];
          let resourceIds = [];

          let resourceText = this.appointmentData[i].asN_TO_USER_ID_LIST_TXT;
          if (resourceText) {
            var dom = new DOMParser().parseFromString(resourceText, "text/xml");
            let resourceId = dom.documentElement.getElementsByTagName("ResourceId");

            for (var y = 0; y < resourceId.length; y++) {
              resourceIds.push(Number(resourceId[y].getAttribute("Value")));
            }
          }

          for (var x = 0; x < this.resourceData.length; x++) {
            if (resourceIds.indexOf(this.resourceData[x].id) > -1) {
              currentResource = new Resource();
              currentResource.id = this.resourceData[x].id;
              currentResource.text = this.resourceData[x].text;
              currentResource.color = "#fcb65e";

              currentResourceData.push(currentResource);
            }
          }
          
          this.appointment.resource = currentResourceData;

          // Build the RecurreceInfo from API
          /*
          let recurrenceText = this.appointmentData[i].rcurnC_DES_TXT;
          console.log("recurrenceText == " + recurrenceText);

          if (recurrenceText) 
          {          
            var dom = new DOMParser().parseFromString(recurrenceText, "text/xml");

            let element = dom.documentElement;
            console.log("nodeName = " + element.nodeName );
            
            console.log("RecurrenceInfo Start = " + element.getAttribute("Start") );
            console.log("RecurrenceInfo End = " + element.getAttribute("End") );
            console.log("RecurrenceInfo DayNumber = " + element.getAttribute("DayNumber") );
            console.log("RecurrenceInfo WeekOfMonth = " + element.getAttribute("WeekOfMonth") );
            console.log("RecurrenceInfo WeekDays = " + element.getAttribute("WeekDays") );
            console.log("RecurrenceInfo OccurrenceCount = " + element.getAttribute("OccurrenceCount") );
            console.log("RecurrenceInfo Range = " + element.getAttribute("Range") );
            console.log("RecurrenceInfo Type = " + element.getAttribute("Type") );
            console.log("RecurrenceInfo FirstDayOfWeek = " + element.getAttribute("FirstDayOfWeek") );            
          }
          */

          // Add Calendar Appointment info to Appointments array for display.
          this.appointmentsData.push(this.appointment);
          
          //console.log("-------------------------------------------------------------");
        }
        //console.log(this.appointmentsData);
      },
      error => {
        console.log(error)
        this.spinner.hide();
      },
      () => {
        this.spinner.hide();
      });
  }

  getApptTypeData() {
    this.calendarSrvc.getApptTypes(1).subscribe(
      res => {
        // Get the Appointment Types
        this.appointmentTypeData = res.table;
        for (var i = 0; i < this.appointmentTypeData.length; i++) {
          this.appointmentType = new ApptType();
          this.appointmentType.id = this.appointmentTypeData[i].appT_TYPE_ID;
          this.appointmentType.text = this.appointmentTypeData[i].appT_TYPE_DES;
          this.appointmentType.color = "#fcb65e";

          this.apptTypeData.push(this.appointmentType);
        }

        // Get the Resources
        this.appointmentResouceData = res.table1;

        for (var i = 0; i < this.appointmentResouceData.length; i++) {
          this.appointmentResource = new Resource();
          this.appointmentResource.id = this.appointmentResouceData[i].id;
          this.appointmentResource.text = this.appointmentResouceData[i].model;
          this.appointmentResource.color = "#fcb65e";

          this.resourceData.push(this.appointmentResource);
        }
      }
    )
  }

  getResourcesData() {
    this.calendarSrvc.getApptTypes(1).subscribe(
      res => {
        this.appointmentResouceData = res.table1;

        for (var i = 0; i < this.appointmentResouceData.length; i++) {
          this.appointmentResource = new Resource();
          this.appointmentResource.id = this.appointmentResouceData[i].id;
          this.appointmentResource.text = this.appointmentResouceData[i].model;
          this.appointmentResource.color = "#fcb65e";

          this.resourceData.push(this.appointmentResource);
        }
      }
    )
  }

  isAllday(s_hour: number, s_minute: number, e_hour: number, e_minute: number) {
    var allDay = ((s_hour + s_minute + e_hour + e_minute) == 0) ? true : false;

    return allDay;
  }

  onAppointmentFormOpening(e) 
  {
    this.spinner.show();
    //console.log('Call onAppontmentFormOpening');
    let form = e.form
    let formItems = form.option("items")

    //Remove all day option
    formItems[0].items[2].items[0].visible = false;
    form.option('items', formItems)

    let eventURL = "event/";
       
    if (this.apptTypeId == CalendarEventType.ADB
      || this.apptTypeId == CalendarEventType.ADG
      || this.apptTypeId == CalendarEventType.ADI
      || this.apptTypeId == CalendarEventType.ADN
      || this.apptTypeId == CalendarEventType.ADTMT
      || this.apptTypeId == CalendarEventType.Fedline
      || this.apptTypeId == CalendarEventType.MDS
      || this.apptTypeId == CalendarEventType.MDSFT
      || this.apptTypeId == CalendarEventType.MPLS
      || this.apptTypeId == CalendarEventType.MPLSVAS
      || this.apptTypeId == CalendarEventType.NGVN
      || this.apptTypeId == CalendarEventType.SIPT
      || this.apptTypeId == CalendarEventType.SLNK
      || this.apptTypeId == CalendarEventType.UCaaS
      || this.apptTypeId == CalendarEventType.NtwkIntl)
    {
      let eventID = e.appointmentData.eventid;

      switch (this.apptTypeId)
      {        
        case CalendarEventType.ADB: eventURL = eventURL + "access-delivery/" + eventID; break;
        case CalendarEventType.ADG: eventURL = eventURL + "access-delivery/" + eventID; break;
        case CalendarEventType.ADI: eventURL = eventURL + "access-delivery/" + eventID; break;
        case CalendarEventType.ADN: eventURL = eventURL + "access-delivery/" + eventID; break;
        case CalendarEventType.ADTMT: eventURL = eventURL + "access-delivery/" + eventID; break;        
        case CalendarEventType.Fedline: eventURL = eventURL + "fedline/" + eventID; break;
        case CalendarEventType.MDS: eventURL = eventURL + "mds/" + eventID; break;
        case CalendarEventType.MDSFT: eventURL = eventURL + "mds/" + eventID; break;
        case CalendarEventType.NtwkIntl: eventURL = eventURL + "mds/" + eventID; break;
        case CalendarEventType.MPLS: eventURL = eventURL + "mpls/" + eventID; break;
        case CalendarEventType.MPLSVAS: eventURL = eventURL + "mpls/" + eventID; break;
        case CalendarEventType.NGVN: eventURL = eventURL + "ngvn/" + eventID; break;
        case CalendarEventType.SIPT: eventURL = eventURL + "sipt/" + eventID; break;
        case CalendarEventType.SLNK: eventURL = eventURL + "sprint-link/" + eventID; break;
        case CalendarEventType.UCaaS: eventURL = eventURL + "ucaas/" + eventID; break;
        default: 
          // do nothing
          break;
      }

      // Event ID Field Link
      if (!formItems.find((i: { dataField: string; }) => i.dataField === "eventid"))
      {
        formItems.push(
        {
          label:
          {
            text: "Event ID"
          },
          editorType: "dxButton",
          dataField: "eventid",
          colSpan: "2",
          editorOptions:
          {
            width: "80%",
            text: [eventID],
            disabled: false,
            stylingMode: "contained",
            type: "default",
            onClick: function(data) 
            {
              //window.open(eventURL, '_blank')
              window.location.href = eventURL;
            }                
          }
        });
        form.option("items", formItems);
      }
        else 
      {
        form.itemOption("eventid", 
        { 
          label:
          {
            text: "Event ID"
          },
          editorType: "dxButton",
          dataField: "eventid",
                colSpan: "2",
                editorOptions:
                {
                  width: "80%",
                  text: [eventID],
                  disabled: false,
                  stylingMode: "contained",
                  type: "default",
                  onClick: function(data) 
                  {  
                    window.location.href = eventURL;
                  }                
                } 
        });
      }
    }
    else 
    {
      if (formItems.find((x: { dataField: string; }) => x.dataField === "eventid")) 
      {
        let index = formItems.findIndex((y: { dataField: string; }) => y.dataField === "eventid");
        formItems.splice(index, 1);
      }
    }

    // Location Field  
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "location")) {
      formItems.push
        (
          {
            label:
            {
              text: "Location"
            },
            editorType: "dxTextBox",
            dataField: "location",
            colSpan: "2",
            editorOptions:
            {
              width: "80%"
            }
          }
        );
      form.option("items", formItems);
    }

    // Appt Type field
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "apptType")) {
      formItems.push
        (
          {
            label:
            {
              text: "Appt Type"
            },
            editorType: "dxSelectBox",
            dataField: "apptType",
            colSpan: "2",
            editorOptions:
            {
              items: this.apptTypeData,
              displayExpr: "text",
              valueExpr: "id",
              width: "80%"
            },
          }
        );
      form.option("items", formItems);
    }

    // Resource field
    if (!formItems.find((i: { dataField: string; }) => i.dataField === "resource")) {
      formItems.push
        (
          {
            label:
            {
              text: "Resource"
            },
            editorType: "dxTagBox",
            dataField: "resource",
            colSpan: "2",
            editorOptions:
            {
              items: this.resourceData,
              displayExpr: "text",
              valueExpr: "id",
              width: "80%"
            },
          }
        );
      form.option("items", formItems);
    }

    form.itemOption("startDate",
      {
        visible: true,
        colSpan: "2"
      }
    );

    form.itemOption("endDate",
      {
        visible: true,
        colSpan: "2"
      }
    );


    this.spinner.hide();    
  }

  listViewAction() 
  {
    //console.log("list view action = " + this.apptTypeId );
    let url = this.router.url + '/list';
    //console.log('listviewaction url: ' +url)
    this.router.navigate([url]);
  }

  customizeDateNavigatorText = (e) => {
    if (!this.currentView || this.currentView === 'day')
      return this.datePipe.transform(e.startDate, 'EEEE, d MMMM y');

    if (this.currentView === 'month')
      return this.datePipe.transform(e.startDate, 'MMMM y');

    return e.startDate.getDate() + "-" + this.datePipe.transform(e.endDate, 'd MMMM y');
  }
}
