import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-csg-error-page',
  templateUrl: './csg-error-page.component.html'
})
export class CsgErrorPageComponent implements OnInit {
  message: string;
  constructor() { }

  ngOnInit() {
    this.message = 'CSG level 2 is required to view Fedline events. ';
  }
}
