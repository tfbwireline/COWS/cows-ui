import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from "devextreme-angular";
import { Observable } from "rxjs";
import { RedesignCustBypass } from "../../../models/index";
import { RedesignCustBypassService } from '../../../services/index';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from './../../../shared';
import { Global } from "../../../shared/global";
declare let $: any;

@Component({
  selector: 'app-customer-bypass',
  templateUrl: './customer-bypass.component.html'
})

export class CustomerBypassComponent implements OnInit {
  @ViewChild('grid', { static: false }) grid: DxDataGridComponent;
  items: Observable<RedesignCustBypass[]>;

  constructor(public helper: Helper, private spinner: NgxSpinnerService,
    private redesignCustBypassSrvc: RedesignCustBypassService) { }

  ngOnInit() {
    this.getCustomerBypass();
  }

  getCustomerBypass() {
    this.spinner.show();
    this.redesignCustBypassSrvc.getCustomersBypass().toPromise().then(
      res => {
        this.items = res;
      },
      error => {
        this.helper.notify('Error loading Customer Bypass list', Global.NOTIFY_TYPE_ERROR);
        console.log(error)
      }).then(() => { this.spinner.hide(); } );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'before',
      template: 'addTemplate'
    });
  }

  addForm() {
    this.grid.instance.addRow();
  }

  saveForm(e) {
    this.spinner.show();
    //console.log(e.key)
    //console.log(e)
    let formData = new RedesignCustBypass();
    if (this.helper.isEmpty(e.key)) {
      formData = e.data;
      formData.id = 0;
      this.redesignCustBypassSrvc.create(formData).toPromise().then(
        data => {
          this.helper.notifySavedFormMessage(formData.custNme,
            Global.NOTIFY_TYPE_SUCCESS, true, null);
        },
        error => {
          this.helper.notifySavedFormMessage(formData.custNme,
            Global.NOTIFY_TYPE_ERROR, true, error);
        }).then(() => {
          this.spinner.hide();
          this.getCustomerBypass();
        });
    }
    else {
      formData = e.oldData;
      formData.h1Cd = this.helper.isEmpty(e.newData.h1Cd) ? e.oldData.h1Cd : e.newData.h1Cd;
      formData.custNme = this.helper.isEmpty(e.newData.custNme) ? e.oldData.custNme : e.newData.custNme;
      formData.recStusId = this.helper.isEmpty(e.newData.recStusId) ? e.oldData.recStusId : e.newData.recStusId;
      this.redesignCustBypassSrvc.update(formData.id, formData).toPromise().then(
        data => {
          this.helper.notifySavedFormMessage(formData.custNme,
            Global.NOTIFY_TYPE_SUCCESS, false, null);
        },
        error => {
          this.helper.notifySavedFormMessage(formData.custNme,
            Global.NOTIFY_TYPE_ERROR, false, error);
        }).then(() => {
          this.spinner.hide();
          this.getCustomerBypass();
        });
    }
  }
}
