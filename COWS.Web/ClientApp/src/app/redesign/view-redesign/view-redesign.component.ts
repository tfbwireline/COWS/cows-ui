import { Component, OnInit, ViewChild } from '@angular/core';
import { EventViewsService, UserService, RedesignService } from '../../../services';
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from '../../../shared';
import { Router, ActivatedRoute } from '@angular/router';
import { DxDataGridComponent } from "devextreme-angular";
import { Global } from '../../../shared/global';
import { zip } from 'rxjs';

@Component({
  selector: 'app-view-redesign',
  templateUrl: './view-redesign.component.html'
})

export class ViewRedesignComponent implements OnInit {
  @ViewChild('grid', { static: false }) dataGrid: DxDataGridComponent;
  @ViewChild('grid1', { static: false }) searchGrid: DxDataGridComponent;
  isPageLoad: boolean = false;
  siteContentId: number = 18; // For Redesign
  availableViewsList: any;
  selectedViewId: any;
  availableDefaultViewID: number;
  redesignList: any[] = [];
  redesignSearchList: any[] = [];

  fromQuickSearch: boolean = false;
  searchText: string = '';
  searchColumn: string = '';

  // User Access
  isSDE: boolean = false;
  isNTE: boolean = false;
  isSSE: boolean = false;
  isMDSAdmin: boolean = false;
  isMDSReviewer: boolean = false;
  isMVSAdmin: boolean = false;
  isMVSReviewer: boolean = false;

  constructor(private helper: Helper, private router: Router, private avRoute: ActivatedRoute,
    private spinner: NgxSpinnerService, private eventService: EventViewsService,
    private userSrvc: UserService, private redesignSrvc: RedesignService) { }

  ngOnInit() {
    let avRoute = this.avRoute.snapshot;
    if (!this.helper.isEmpty(avRoute)
      && !this.helper.isEmpty(avRoute.queryParams)
      && !this.helper.isEmpty(avRoute.queryParams.column)
      && !this.helper.isEmpty(avRoute.queryParams.text)) {
        this.fromQuickSearch = true;
        this.searchColumn = avRoute.queryParams.column;
        this.searchText = avRoute.queryParams.text;
    }

    this.getUserAccess().then(res => {
      this.getEventViews(this.userSrvc.loggedInUser.userId, this.siteContentId);
    });
  }

  getUserAccess() {
    return new Promise(resolve => {
      let data = zip(
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "Sales Design Engineer"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "Network Engineer"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "System Security Engineer"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "MDS Admin"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "MDS Event Reviewer"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "SIPTnUCaaS Admin"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "SIPTnUCaaS Event Reviewer")
      )

      data.toPromise().then(
        res => {
          this.isSDE = res[0] ? true : false;
          this.isNTE = res[1] ? true : false;
          this.isSSE = res[2] ? true : false;
          this.isMDSAdmin = res[3] ? true : false;
          this.isMDSReviewer = res[4] ? true : false;
          this.isMVSAdmin = res[5] ? true : false;
          this.isMVSReviewer = res[6] ? true : false;
          resolve(true);
        },
        error => {
          this.helper.notify('An error occurred while getting user access.', Global.NOTIFY_TYPE_ERROR);
          resolve(false);
        });
    });
  }

  getEventViews(userId: number, siteCntnt: number) {
    this.eventService.getEventViews(userId, siteCntnt).toPromise().then(
      data => {
        this.availableViewsList = data;
        this.availableDefaultViewID = this.availableViewsList[0].dspL_VW_ID;
      }).then(() => {
        if (this.fromQuickSearch) {
          this.search(this.searchColumn, this.searchText);
          this.isPageLoad = true;
        }
        //else {
        //  this.getViewData(this.availableDefaultViewID, true);
        //}
      });
  }

  search(col: any, text: any) {
    let rdsgnNo: string = '';
    let custName: string = '';
    let nte: string = '';
    let devices: string = '';
    let pm: string = '';
    if (col == 1) {
      rdsgnNo = text;
    }
    else if (col == 2) {
      custName = text;
    }
    else if (col == 3) {
      nte = text;
    }
    else if (col == 4) {
      devices = text;
    }
    else if (col == 5) {
      pm = text;
    }

    this.spinner.show();
    this.redesignSrvc.getRedesignSearchResults(rdsgnNo, custName, nte, devices, pm).toPromise().then(
      res => {
        this.redesignSearchList = res;
      },
      error => { }).then(() => { this.spinner.hide(); });
  }

  getViewData(value, isPageLoad) {
    if (!isPageLoad) {//!this.fromQuickSearch
      //this.selectedViewId = this.fromQuickSearch ? 500 : value;
      this.selectedViewId = this.isPageLoad ? 500 : value;
      this.fromQuickSearch = this.isPageLoad;
      this.isPageLoad = false;

      this.spinner.show();
      this.eventService.getEventViewDetails(this.selectedViewId, this.siteContentId, this.userSrvc.loggedInUser.userId, "0").toPromise().then(
        data => {
          this.redesignList = data;
        },
        error => { }).then(() => { this.spinner.hide(); });
    }
  }

  // For Redesign View
  contentReadyHandler(e) {
    // For Search Panel
    if (!this.helper.isEmpty(this.dataGrid.searchPanel.text.trim())) {
      this.dataGrid.instance.searchByText(this.dataGrid.searchPanel.text.trim())
    }

    // Grid Column Options
    this.dataGrid.instance.columnOption('redesign ID', 'visible', false);
    this.dataGrid.instance.columnOption('csg Level', 'visible', false);
    this.dataGrid.instance.columnOption('redesign Status ID', 'visible', false);
    this.dataGrid.instance.columnOption('isLocked', 'visible', false);
    this.dataGrid.instance.columnOption('lockedBy', 'visible', false);

    this.dataGrid.instance.columnOption('redesign Number', 'cellTemplate', 'linkIdTemplate');
    this.dataGrid.instance.columnOption('redesign Number', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('redesign Number', 'caption', 'Redesign #');
    this.dataGrid.instance.columnOption('redesign Number', 'visibleIndex', 0);
    this.dataGrid.instance.columnOption('bpm Redesign Number', 'caption', 'PR #');
    this.dataGrid.instance.columnOption('bpm Redesign Number', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('bpm Redesign Number', 'visibleIndex', 1);
    this.dataGrid.instance.columnOption('submitted Date', 'caption', 'Date Submitted');
    this.dataGrid.instance.columnOption('submitted Date', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('submitted Date', 'visibleIndex', 2);
    this.dataGrid.instance.columnOption('submitted Date', 'cellTemplate', 'submitDateTemplate');
    this.dataGrid.instance.columnOption('sla Due Date', 'caption', 'SLA Due Date');
    this.dataGrid.instance.columnOption('sla Due Date', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('sla Due Date', 'visibleIndex', 3);
    this.dataGrid.instance.columnOption('sla Due Date', 'cellTemplate', 'slaDueDateTemplate');
    this.dataGrid.instance.columnOption('expiration Date', 'caption', 'Expiration Date');
    this.dataGrid.instance.columnOption('expiration Date', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('expiration Date', 'visibleIndex', 4);
    this.dataGrid.instance.columnOption('expiration Date', 'cellTemplate', 'expirationDateTemplate');
    this.dataGrid.instance.columnOption('redesign Type', 'caption', 'Redesign Type');
    this.dataGrid.instance.columnOption('redesign Type', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('redesign Type', 'visibleIndex', 5);
    this.dataGrid.instance.columnOption('h1 Value', 'caption', 'H1');
    this.dataGrid.instance.columnOption('h1 Value', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('h1 Value', 'visibleIndex', 6);
    this.dataGrid.instance.columnOption('customer Name', 'caption', 'Customer');
    this.dataGrid.instance.columnOption('customer Name', 'visibleIndex', 7);
    this.dataGrid.instance.columnOption('redesign Status', 'caption', 'Status');
    this.dataGrid.instance.columnOption('redesign Status', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('redesign Status', 'visibleIndex', 8);
    this.dataGrid.instance.columnOption('assigned NTE', 'caption', 'NTE Assigned');
    this.dataGrid.instance.columnOption('assigned NTE', 'visibleIndex', 9);
    this.dataGrid.instance.columnOption('assignedPM', 'caption', 'PM Assigned');
    this.dataGrid.instance.columnOption('assignedPM', 'visibleIndex', 10);
    this.dataGrid.instance.columnOption('mss SE Assigned', 'caption', 'MSS SE Assigned');
    this.dataGrid.instance.columnOption('mss SE Assigned', 'visibleIndex', 11);
    this.dataGrid.instance.columnOption('ne Assigned', 'caption', 'NE Assigned');
    this.dataGrid.instance.columnOption('ne Assigned', 'visibleIndex', 12);
    this.dataGrid.instance.columnOption('device Name', 'caption', 'Site Name');
    this.dataGrid.instance.columnOption('device Name', 'allowHeaderFiltering', false);
    this.dataGrid.instance.columnOption('device Name', 'visibleIndex', 13);
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onExporting(e) {
    e.fileName = "Redesign_" + this.helper.formatDate(new Date());
  }

  selectionChangedHandler(id) {
    this.router.navigateByUrl('redesign/update-redesign/' + id);
  }

  onRowPrepared(e: any) {
    if (e.rowType === 'data') {
      if ((e.cells[7].value === 'Pending-NTE' && this.isNTE)
        || (e.cells[7].value === 'Pending-SDE' && this.isSDE)) {
        e.rowElement.className = e.rowElement.className + ' redesign-pending';
      }

      if (!this.helper.isEmpty(e.data.isLocked)) {
        e.rowElement.className = e.rowElement.className + ' checked-in-row';
        e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
        e.rowElement.title = 'This redesign is locked by ' + e.data.lockedBy;
      }
    }
  }


  // For Redesign Search View
  contentSearchReadyHandler(e) {
    // For Search Panel
    if (!this.helper.isEmpty(this.searchGrid.searchPanel.text.trim())) {
      this.searchGrid.instance.searchByText(this.searchGrid.searchPanel.text.trim())
    }

    // Grid Column Options
    this.searchGrid.instance.columnOption('redesignId', 'visible', false);
    this.searchGrid.instance.columnOption('csgLvlId', 'visible', false);
    this.searchGrid.instance.columnOption('eventId', 'visible', false);
    this.searchGrid.instance.columnOption('redesignCategory', 'visible', false);
    this.searchGrid.instance.columnOption('status', 'visible', false);
    this.searchGrid.instance.columnOption('redesignStatus', 'visible', false);
    this.searchGrid.instance.columnOption('type', 'visible', false);

    this.searchGrid.instance.columnOption('redesignNo', 'cellTemplate', 'linkIdTemplate');
    this.searchGrid.instance.columnOption('redesignNo', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('redesignNo', 'caption', 'Redesign #');
    this.searchGrid.instance.columnOption('redesignNo', 'visibleIndex', 0);
    this.searchGrid.instance.columnOption('bpmRedesignNbr', 'caption', 'PR #');
    this.searchGrid.instance.columnOption('bpmRedesignNbr', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('bpmRedesignNbr', 'visibleIndex', 1);
    this.searchGrid.instance.columnOption('submittedDate', 'caption', 'Date Submitted');
    this.searchGrid.instance.columnOption('submittedDate', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('submittedDate', 'visibleIndex', 2);
    this.searchGrid.instance.columnOption('submittedDate', 'cellTemplate', 'submitDateTemplate');
    this.searchGrid.instance.columnOption('slaDueDate', 'caption', 'SLA Due Date');
    this.searchGrid.instance.columnOption('slaDueDate', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('slaDueDate', 'visibleIndex', 3);
    this.searchGrid.instance.columnOption('slaDueDate', 'cellTemplate', 'slaDueDateTemplate');
    this.searchGrid.instance.columnOption('expirationDate', 'caption', 'Expiration Date');
    this.searchGrid.instance.columnOption('expirationDate', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('expirationDate', 'visibleIndex', 4);
    this.searchGrid.instance.columnOption('expirationDate', 'cellTemplate', 'expirationDateTemplate');
    this.searchGrid.instance.columnOption('redesignType', 'caption', 'Redesign Type');
    this.searchGrid.instance.columnOption('redesignType', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('redesignType', 'visibleIndex', 5);
    this.searchGrid.instance.columnOption('h1', 'caption', 'H1');
    this.searchGrid.instance.columnOption('h1', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('h1', 'visibleIndex', 6);
    this.searchGrid.instance.columnOption('customer', 'caption', 'Customer');
    this.searchGrid.instance.columnOption('customer', 'visibleIndex', 7);
    this.searchGrid.instance.columnOption('statusDesc', 'caption', 'Status');
    this.searchGrid.instance.columnOption('statusDesc', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('statusDesc', 'visibleIndex', 8);
    this.searchGrid.instance.columnOption('nteAssigned', 'caption', 'NTE Assigned');
    this.searchGrid.instance.columnOption('nteAssigned', 'visibleIndex', 9);
    this.searchGrid.instance.columnOption('pmAssigned', 'caption', 'PM Assigned');
    this.searchGrid.instance.columnOption('pmAssigned', 'visibleIndex', 10);
    this.searchGrid.instance.columnOption('mssSeAssigned', 'caption', 'MSS SE Assigned');
    this.searchGrid.instance.columnOption('mssSeAssigned', 'visibleIndex', 11);
    this.searchGrid.instance.columnOption('neAssigned', 'caption', 'NE Assigned');
    this.searchGrid.instance.columnOption('neAssigned', 'visibleIndex', 12);
    this.searchGrid.instance.columnOption('odieDeviceName', 'caption', 'Site Name');
    this.searchGrid.instance.columnOption('odieDeviceName', 'allowHeaderFiltering', false);
    this.searchGrid.instance.columnOption('odieDeviceName', 'visibleIndex', 13);
  }

  onSearchToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      template: 'exportTemplate'
    });
  }

  onSearchExporting(e) {
    e.fileName = "Redesign_" + this.helper.formatDate(new Date());
  }

  selectionSearchChangedHandler(id) {
    this.router.navigateByUrl('redesign/update-redesign/' + id);
  }

  onSearchRowPrepared(e: any) {
    if (e.rowType === 'data') {
      if ((e.cells[7].value === 'Pending-NTE' && this.isNTE)
        || (e.cells[7].value === 'Pending-SDE' && this.isSDE)) {
        e.rowElement.className = e.rowElement.className + ' redesign-pending';
      }

      if (!this.helper.isEmpty(e.data.isLocked)) {
        e.rowElement.className = e.rowElement.className + ' checked-in-row';
        e.rowElement.className = e.rowElement.className.replace("dx-row-alt", "");
        e.rowElement.title = 'This redesign is locked by ' + e.data.lockedBy;
      }
    }
  }
}
