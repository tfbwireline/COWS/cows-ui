import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxListComponent } from "devextreme-angular";
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { Helper } from "./../../../shared";
import {
  Global, KeyValuePair, RB_CONDITION, ESimpleOrComplex, EDispatchReady,
  ERedesignCategory, ERedesignStatus, ContactDetailObjType, HierLevel, REDESIGN_STATUS_EMAIL_CD
} from "./../../../shared/global";
import {
  RedesignService, UserService, RedesignRecLockService, RedesignCategoryService,
  RedesignTypeService, RedesignWorkflowService, SearchService, OdieRspnInfoService, ContactDetailService
} from '../../../services/index';
import { CustomValidator } from '../../../shared/validator/custom-validator';
import { zip, of } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import {
  FormControlItem, OdieRspn, Redesign, RedesignDoc, RedesignNotes,
  RedesignEmailNotification, RedesignType, RedesignDevicesInfo, DocEntity, ContactDetail
} from '../../../models';
import * as moment from 'moment';
import DataSource from 'devextreme/data/data_source';
import { FileSizePipe } from '../../../shared/pipe/file-size.pipe';
import { ContactListComponent } from '../../custom/contact-list/contact-list.component';
declare let $: any;

@Component({
  selector: 'app-new-redesign',
  templateUrl: './new-redesign.component.html'
})

export class NewRedesignComponent implements OnInit, OnDestroy {
  @ViewChild('gridDevices', { static: false }) gridDevices: DxDataGridComponent;
  @ViewChild('listDevices', { static: false }) list: DxListComponent;
  @ViewChild('cntctList', { static: false }) cntctList: ContactListComponent;
  // Form Properties
  form: FormGroup;
  option: any;
  errors: string[] = [];
  showWarningMessage: boolean = true;
  warningMessage: string;
  isSubmitted: boolean = false;
  isSaveExit: boolean = false;

  // Page Properties
  redesignId: number = 0;
  redesignData: Redesign = new Redesign();
  editMode: boolean = true;
  isLocked: boolean = false;
  lockedByUser: string;
  isDeviceInfoDisabled: boolean = false;
  isFormDisabled: boolean = false;
  selectedDevices: string[] = [];
  isH1LookupClicked: boolean = false;
  odieTimeout: boolean = false;
  filterOnDeviceInfo: boolean = false;
  odieReqCd: boolean = false;
  odieRspnInfoReqId: number = 0;
  isBillable: boolean = false;
  extendExpiration: boolean = false;
  showNteInfo: boolean = false;

  // UI Fields Properties
  custNamePDL: string;
  redesignCategoryList: any = [];
  h1CustomerLookupList: any = [];
  h1CustomerLookupDS: DataSource;
  customerNameList: any = [];
  redesignTypeList: RedesignType[] = [];
  sdwanList: KeyValuePair[];
  ciscoSmartList: KeyValuePair[];
  deviceNameList: FormControlItem[] = [];
  devicesLookupDS: DataSource;
  selectedDeviceDS: any = [];
  docList: DocEntity[] = [];
  statusList: KeyValuePair[] = [];
  noteHistoryList: RedesignNotes[] = [];
  simpleComplexList: KeyValuePair[];
  dispatchReadyList: KeyValuePair[];
  redesignDevices: RedesignDevicesInfo[] = [];
  odieRspnList: OdieRspn[] = []; // For CustomerName DDL
  emailNotificationList: RedesignEmailNotification[] = [];
  addlDocList: string[] = [];
  
  // User Access
  isSDE: boolean = false;
  isNTE: boolean = false;
  isSSE: boolean = false;
  isMDSAdmin: boolean = false;
  isMDSReviewer: boolean = false;
  isMVSAdmin: boolean = false;
  isMVSReviewer: boolean = false;

  // Form Field Properties
  get redesignCategory() { return this.form.get("redesignCategory") }
  get h1Customer() { return this.form.get('h1Customer'); }
  get h1() { return this.form.get('h1'); }
  get status() { return this.form.get('status'); }

  get redesignType() { return this.form.get("redesignTypeInfo.redesignType") }
  get deviceFilter() { return this.form.get("redesignTypeInfo.deviceFilter") }
  get ciscoSmartLicense() { return this.form.get("redesignTypeInfo.ciscoSmartLicense") }
  get smartAccountDomain() { return this.form.get("redesignTypeInfo.smartAccountDomain") }
  get virtualAccount() { return this.form.get("redesignTypeInfo.virtualAccount") }

  
  get customerRspnId() { return this.form.get('customerRspnId'); }
  get customerName() { return this.form.get('customerName'); }
  get odieCustId() { return this.form.get('odieCustId'); }
  // Updated by Sarah Sandoval [20210615] - Removed for Contact Details (PJ025845)
  //get customerRspnId() { return this.form.get('custInfo.customerRspnId'); }
  //get customerName() { return this.form.get('custInfo.customerName'); }
  //get odieCustId() { return this.form.get('custInfo.odieCustId'); }
  //get sowsFolderPathName() { return this.form.get('custInfo.sowsFolderPathName'); }
  //get custTeamPdl() { return this.form.get("custInfo.custTeamPdl") }
  //get nteAssigned() { return this.form.get("custInfo.nteAssigned") }
  //get mssSeAssigned() { return this.form.get("custInfo.mssSeAssigned") }
  //get pmAssigned() { return this.form.get("custInfo.pmAssigned") }
  //get neAssigned() { return this.form.get("custInfo.neAssigned") }
  //get customerEmailPDL() { return this.form.get("custInfo.customerEmailPDL") }

  get designDocUrl() { return this.form.get("commentEmailInfo.designDocUrl") }
  get emailNotifications() { return this.form.get("commentEmailInfo.emailNotifications") }
  get customerBypass() { return this.form.get("nteInfo.customerBypass") }
  get totalCost() { return this.form.get("nteInfo.totalCost") }
  get devicesInfo() { return this.form.get("devicesInfo") }
  get devices() { return this.form.get('devicesInfo.deviceNameList') as FormArray; }
  get redesignTypeInfo() { return this.form.get("redesignTypeInfo") }

  // Contact Details
  objType: string;
  additionalEmails: string[] = [];
  contactList: ContactDetail[] = [];
  showContactDetails = false;

  constructor(public helper: Helper, private spinner: NgxSpinnerService, private router: Router,
    private activatedRoute: ActivatedRoute, private fb: FormBuilder, private userSrvc: UserService,
    private searchSrvc: SearchService, private fileSizePipe: FileSizePipe,
    private redCatSrvc: RedesignCategoryService, private redTypeSrvc: RedesignTypeService,
    private redesignWrkflwSrvc: RedesignWorkflowService, private redesignService: RedesignService, 
    private redesignRecLockSrvc: RedesignRecLockService, private cntctDtlSrvc: ContactDetailService) { }

  ngOnDestroy() {
    //this.helper.form = null;
    //this.unlockRedesign();
  }

  ngOnInit() {
    this.redesignId = this.activatedRoute.snapshot.params["id"] || 0;
    this.objType = ContactDetailObjType.Redesign;
    this.showContactDetails = this.redesignId > 0 ? true : false;

    this.form = new FormGroup({
      redesignNumber: new FormControl(null),
      redesignCategory: new FormControl({ value: null }, Validators.required),
      h1Customer: new FormControl({ value: null }),
      h1: new FormControl(null, Validators.compose([Validators.required, CustomValidator.h1Validator])),
      file: new FormControl(),
      redesignDocument: new FormControl(),
      status: new FormControl(null),
      suppressEmails: new FormControl(false),
      customerRspnId: new FormControl(0),
      customerName: new FormControl({ value: null }, Validators.required),
      odieCustId: new FormControl(null),
      contactEmails: new FormControl(),
      //custInfo: new FormGroup({
      //  customerRspnId: new FormControl(0),
      //  customerName: new FormControl({ value: null }, Validators.required),
      //  odieCustId: new FormControl(null),
      //  custTeamPdl: new FormControl('', Validators.required),
      //  sowsFolderPathName: new FormControl(null),
      //  nteAssigned: new FormControl(null),
      //  nteAssignedUserId: new FormControl(null),
      //  nteAssignedUserAdId: new FormControl(null),
      //  mssSeAssigned: new FormControl(null),
      //  mssSeAssignedUserId: new FormControl(null),
      //  mssSeAssignedUserAdId: new FormControl(null),
      //  pmAssigned: new FormControl(null),
      //  pmAssignedUserId: new FormControl(null),
      //  pmAssignedUserAdId: new FormControl(null),
      //  neAssigned: new FormControl(null),
      //  neAssignedUserId: new FormControl(null),
      //  neAssignedUserAdId: new FormControl(null),
      //  customerEmailPDL: new FormControl(null)
      //}),
      redesignTypeInfo: new FormGroup({
        deviceFilter: new FormControl(null),
        redesignType: new FormControl(null),
        sdwan: new FormControl(null),
        ciscoSmartLicense: new FormControl(null),
        smartAccountDomain: new FormControl(null, Validators.pattern("^[a-zA-Z0-9.\-]{1,100}$")),
        virtualAccount: new FormControl(null, Validators.pattern("^[a-zA-Z0-9.\-]{1,100}$")),
      }),
      devicesInfo: new FormGroup({
        entireNetwork: new FormControl(false),
        deviceFilter: new FormControl(null),
        deviceNameList: this.fb.array([]),
        newDevice: new FormControl(null)
      }),
      commentEmailInfo: new FormGroup({
        caveats: new FormControl(null),
        description: new FormControl(null),
        emailNotifications: new FormControl(null),
        designDocUrl: new FormControl(null),
        designDocBPM: new FormControl(null),
      }),
      nteInfo: new FormGroup({
        nteLoe: new FormControl(null),
        nteOtLoe: new FormControl(null),
        pmLoe: new FormControl(null),
        pmOtLoe: new FormControl(null),
        neLoe: new FormControl(null),
        neOtLoe: new FormControl(null),
        spsLoe: new FormControl(null),
        spsOtLoe: new FormControl(null),
        mssImplementEst: new FormControl(null),
        customerBypass: new FormControl(false),
        totalCost: new FormControl(null),
        overrideCost: new FormControl(false),
        overrideCostAmt: new FormControl(false)
      }),
      extensionInfo: new FormGroup({
        chkExtensionFlag: new FormControl(false)
      })
    });
    this.option = {
      custInfo: {
        userFinderForNteAssigned: {
          isEnabled: true,
          searchQuery: "nteAssigned",
          mappings: [
            ["nteAssigned", "dsplNme"],
            ["nteAssignedUserId", "userId"],
            ["nteAssignedUserAdId", "userAdid"]
          ]
        },
        userFinderForMssSeAssigned: {
          isEnabled: true,
          searchQuery: "mssSeAssigned",
          mappings: [
            ["mssSeAssigned", "dsplNme"],
            ["mssSeAssignedUserId", "userId"],
            ["mssSeAssignedUserAdId", "userAdid"]
          ]
        },
        userFinderForPmAssigned: {
          isEnabled: true,
          searchQuery: "pmAssigned",
          mappings: [
            ["pmAssigned", "dsplNme"],
            ["pmAssignedUserId", "userId"],
            ["pmAssignedUserAdId", "userAdid"]
          ]
        },
        userFinderForNeAssigned: {
          isEnabled: true,
          searchQuery: "neAssigned",
          mappings: [
            ["neAssigned", "dsplNme"],
            ["neAssignedUserId", "userId"],
            ["neAssignedUserAdId", "userAdid"]
          ]
        }
      },
      userFinderForEmailNotifications: {
        isEnabled: true,
        isMultiple: true,
        searchQuery: "emailNotifications",
        mappings: [
          ["emailNotifications", "emailAdr"],
          ["emailNotificationsUserId", "userId"],
          ["emailNotificationsUserAdId", "userAdid"]
        ]
      },
      documentEntity: {
        fileSizeValidation: true,
        inputFileControl: {
          isEnabled: true,
          inputFileField: {
            isShown: false
          }
        },
        grid: {
          column: {
            fileName: {
              caption: "Redesign Document"
            },
            fileSize: {
              byte: false
            }
          }
        }
      },
      validationSummaryOptions: {
        title: "Validation Summary"
      }
    }

    // Set the helper form to this form
    //this.helper.form = this.form;
    this.init();
  }

  /**
   * Method called to initialized Redesign form
   */
  init() {
    this.spinner.show();
    this.loadInitialData().then(() => {
      this.getUserAccess().then(() => {
        this.loadRedesignData().then(() => { this.spinner.hide(); });
      });
    });
  }

  /**
   * Method called to get User access for Redesign form
   */
  getUserAccess() {
    //let profiles = this.userSrvc.loggedInUser.profiles;
    //console.log(profiles)
    //this.isSDE = profiles.filter(i => i.includes('Sales Design Engineer')).length > 0 ? true : false;
    //this.isNTE = profiles.filter(i => i.includes('Network Engineer')).length > 0 ? true : false;
    //this.isSSE = profiles.filter(i => i.includes('System Security Engineer')).length > 0 ? true : false;
    //this.isMDSAdmin = profiles.includes('MDS Admin');
    //this.isMDSReviewer = profiles.includes('MDS Event Reviewer');
    //this.isMVSAdmin = profiles.includes('SIPTnUCaaS Admin');
    //this.isMVSReviewer = profiles.includes('SIPTnUCaaS Event Reviewer');

    //console.log(profiles.includes('Network Engineer'))
    //console.log(profiles.includes('System Security Engineer'))

    //console.log('isSDE: ' + this.isSDE + ' isNTE: ' + this.isNTE + ' isSSE: ' + this.isSSE
    //  + ' isMDSAdmin: ' + this.isMDSAdmin + ' isMDSReviewer: ' + this.isMDSReviewer
    //  + ' isMVSAdmin: ' + this.isMVSAdmin + ' isMVSReviewer: ' + this.isMVSReviewer)

    return new Promise(resolve => {
      // TODO: Check cached profiles instead of calling db multiple times (see commented codes)
      // Commented the checking of profiles on DB since profiles are save already on loggedin user session
      // Also the hierarchy of profiles is not needed since the condition on setRedesignFormFields() function
      // already cater that and setRedesignWorkflow() need to display appropriate status per profiles
      let data = zip(
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "Sales Design Engineer"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "Network Engineer"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "System Security Engineer"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "MDS Admin"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "MDS Event Reviewer"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "SIPTnUCaaS Admin"),
        this.userSrvc.userActiveWithProfileName(this.userSrvc.loggedInUser.userId, "SIPTnUCaaS Event Reviewer")
      )

      data.toPromise().then(
        res => {
          this.isSDE = res[0] ? true : false;
          this.isNTE = res[1] ? true : false;
          this.isSSE = res[2] ? true : false;
          this.isMDSAdmin = res[3] ? true : false;
          this.isMDSReviewer = res[4] ? true : false;
          this.isMVSAdmin = res[5] ? true : false;
          this.isMVSReviewer = res[6] ? true : false;

          resolve(true);
        },
        error => {
          this.helper.notify('An error occurred while getting user access.', Global.NOTIFY_TYPE_ERROR);
          this.form.disable();
          resolve(false);
        }).then(() => {
          // Set hierarchy role
          //if (this.isSSE) {
          //  this.isNTE = false;
          //  this.isSDE = false;
          //}
          //else if (this.isNTE) {
          //  this.isSDE = false;
          //}

          //if (this.isMDSAdmin || this.isMVSAdmin) {
          //  this.isMDSReviewer = false;
          //  this.isMVSReviewer = false;
          //}
        });
    });
  }

  /**
   * Method called to load initial lookup data
   */
  loadInitialData() {
    return new Promise(resolve => {
      this.sdwanList = this.helper.getEnumKeyValuePair(RB_CONDITION);
      this.ciscoSmartList = this.helper.getEnumKeyValuePair(RB_CONDITION);
      this.simpleComplexList = this.helper.getEnumKeyValuePair(ESimpleOrComplex);
      this.dispatchReadyList = this.helper.getEnumKeyValuePair(EDispatchReady);

      let data = zip(
        this.redCatSrvc.get(),
        this.searchSrvc.getODIECustomerH1Data(),
      )

      data.toPromise().then(
        res => {
          this.redesignCategoryList = res[0];
          this.h1CustomerLookupList = res[1];
          this.h1CustomerLookupDS = new DataSource({
            store: {
              data: this.h1CustomerLookupList,
              type: 'array',
              key: 'h1'
            }
          });
          resolve(true);
        },
        error => {
          this.helper.notify('An error occurred while loading initial data.', Global.NOTIFY_TYPE_ERROR);
          resolve(false);
        });
    });
  }

  /**
   * Method called to load Redesign data for View/Update
   */
  loadRedesignData() {
    return new Promise(resolve => {
      if (this.redesignId == 0) {
        this.redesignWrkflwSrvc.getByStatus(0).subscribe(
          res => {
            this.statusList = res;
            this.status.setValue(ERedesignStatus.Draft);
            //if (!this.isMDSAdmin && !this.isMVSAdmin) {
            //  this.nteAssigned.disable();
            //  this.option.custInfo.userFinderForNteAssigned.isEnabled = false;
            //  this.pmAssigned.disable();
            //  this.option.custInfo.userFinderForPmAssigned.isEnabled = false;
            //  this.neAssigned.disable();
            //  this.option.custInfo.userFinderForNeAssigned.isEnabled = false;
            //  this.mssSeAssigned.disable();
            //  this.option.custInfo.userFinderForMssSeAssigned.isEnabled = false;
            //}
          }
        );
        resolve(true);
      }
      else {
        let data = zip(
          this.redesignRecLockSrvc.checkLock(this.redesignId),
          this.redesignService.getById(this.redesignId).pipe(
            concatMap(
              res => zip(
                of(res as Redesign),
                //this.searchSrvc.getH1Lookup((res as Redesign).h1Cd),
                this.redesignWrkflwSrvc.getByStatus((res as Redesign).stusId),
                this.redTypeSrvc.getByRedesignCategoryId((res as Redesign).redsgnCatId),
                //this.searchSrvc.getSysCfgValue((res as Redesign).redsgnCatId == ERedesignCategory.Voice ? 'UCaaSRedesignTeamPDL' : 'RedesignDataSecurityEmailPDL'),
                this.redesignService.getRedesignCustomerBypassCD((res as Redesign).h1Cd, (res as Redesign).custNme),
                this.cntctDtlSrvc.getContactDetails(this.redesignId, this.objType, '', '', (res as Redesign).odieCustId),
              )
            )
          )
        );

        data.toPromise().then(
          res => {
            //console.log(res)
            let lockUser = res[0] as any;
            if (lockUser != null) {
              this.lockedByUser = lockUser;
              this.isLocked = true;
            }

            this.redesignData = res[1][0] as Redesign;
            console.log(this.redesignData)

            // Set Form values
            this.custNamePDL = this.redesignData.redsgnCatId == ERedesignCategory.Voice ? 'UCaaSRedesignTeamPDL' : 'RedesignDataSecurityEmailPDL';
            // Updated by Sarah Sandoval [20200909] - No need to call ODIE for this Customer Name
            // Just add the customer name saved on DB to the list (this.odieRspnList)
            //this.odieRspnList = res[1][1] as OdieRspn[];
            this.statusList = res[1][1];
            this.redesignTypeList = res[1][2];
            //this.customerEmailPDL.setValue(res[1][3]);
            this.customerBypass.setValue(res[1][3]);
            this.contactList = res[1][4] as ContactDetail[];
            // Construct Email Code Text on Update
            this.contactList.forEach(item => {
              if (!this.helper.isEmpty(item.emailCd)) {
                const statusText: string[] = [];
                item.emailCdIds = item.emailCd.split(',').map(Number);
                item.emailCdIds.forEach(row => {
                  statusText.push(REDESIGN_STATUS_EMAIL_CD[row])
                });
                item.emailCdTxt = statusText.join(',');
              }

              if (item.id != 0 && (item.creatByUserId > 1 && item.creatByUserId != this.userSrvc.loggedInUser.userId)) {
                item.allowEdit = false;
              } else {
                item.allowEdit = true;
              }

            });

            this.redesignCategory.setValue(this.redesignData.redsgnCatId);
            this.h1.setValue(this.redesignData.h1Cd);

            // Updated by Sarah Sandoval [20200909] - No need to call ODIE for this Customer Name
            // Just add the customer name saved on DB to the list (this.odieRspnList)
            //let customerRspn = this.odieRspnList.filter(i => i.custNme == this.redesignData.custNme);
            //if (!this.helper.isEmpty(customerRspn)) {
            //  this.customerRspnId.setValue(customerRspn[0].rspnId);
            //  this.redesignService.getCustNameDetails(this.customerRspnId.value).subscribe(data => {
            //    //console.log(data)
            //    this.odieCustId.setValue(data[0].odiE_CUST_ID);
            //    //console.log(this.odieCustId.value);
            //  });
            //}

            //console.log(this.redesignData.custNme)
            let x = new OdieRspn({
              custNme: this.redesignData.custNme,
              rspnId: 1
            });
            this.odieRspnList.push(x);
            this.customerName.setValue(this.redesignData.custNme);
            this.customerRspnId.setValue(1);
            this.odieCustId.setValue(this.redesignData.odieCustId);
            //this.custTeamPdl.setValue(this.redesignData.custEmailAdr);
            //this.nteAssigned.setValue(this.redesignData.nteAssignedFullName);
            //this.form.get('custInfo.nteAssignedUserAdId').setValue(this.redesignData.nteAssigned);
            //this.mssSeAssigned.setValue(this.redesignData.sdeAsnFullNme);
            //this.form.get('custInfo.mssSeAssignedUserAdId').setValue(this.redesignData.sdeAsnNme);
            //this.pmAssigned.setValue(this.redesignData.pmAssignedFullName);
            //this.form.get('custInfo.pmAssignedUserAdId').setValue(this.redesignData.pmAssigned);
            //this.neAssigned.setValue(this.redesignData.neAsnFullNme);
            //this.form.get('custInfo.neAssignedUserAdId').setValue(this.redesignData.neAsnNme);

            let redType = this.redesignTypeList.find(i => i.redsgnTypeId == this.redesignData.redsgnTypeId);
            if (!this.helper.isEmpty(redType)) {
              this.odieReqCd = redType.odieReqCd;
              this.isBillable = redType.isBlblCd;
            }
            this.redesignType.setValue(this.redesignData.redsgnTypeId);
            this.setFormControlValue('redesignTypeInfo.sdwan', this.redesignData.isSdwan ? 1 : 0);
            this.ciscoSmartLicense.setValue(this.helper.isEmpty(this.redesignData.ciscSmrtLicCd) ? 0 :
              (this.redesignData.ciscSmrtLicCd ? 1 : 0));
            this.smartAccountDomain.setValue(this.redesignData.smrtAccntDmn);
            this.virtualAccount.setValue(this.redesignData.vrtlAccnt);

            this.redesignDevices = this.redesignData.devices.filter(i => i.recStusId);
            this.redesignDevices.forEach(i => {
              i.isSelected = true;
              i.devCmpltnCd = i.devCmpltnCd == null ? false : i.devCmpltnCd;
            });

            this.setFormControlValue('devicesInfo.entireNetwork', this.redesignData.entireNwCkdId);

            if (this.redesignData.docs != null && this.redesignData.docs.length > 0) {
              this.docList = this.redesignData.docs.map(item => {
                return new DocEntity({
                  docId: item.redsgnDocId,
                  id: item.redsgnId,
                  fileNme: item.fileNme,
                  fileCntnt: item.fileCntnt,
                  fileSizeQty: item.fileSizeQty,
                  fileSizeQtyStr: this.fileSizePipe.transform(item.fileSizeQty),
                  creatDt: item.creatDt,
                  creatByUserId: item.creatByUserId,
                  creatByUserAdId: item.creatByUserName,
                  base64string: item.base64string
                })
              })
            }

            this.setFormControlValue('commentEmailInfo.description', this.redesignData.description);
            this.setFormControlValue('commentEmailInfo.caveats', this.redesignData.caveats);
            this.setFormControlValue('commentEmailInfo.designDocUrl', this.redesignData.dsgnDocLocTxt);
            this.setFormControlValue('commentEmailInfo.designDocBPM', this.redesignData.dsgnDocNbrBpm);

            this.emailNotificationList = this.redesignData.emails;
            this.addlDocList = this.helper.isEmpty(this.redesignData.addlDocTxt) ? [] : this.redesignData.addlDocTxt.split(';');

            this.showNteInfo = this.redesignData.stusId > ERedesignStatus.Submitted;
            this.setFormControlValue('nteInfo.nteLoe', this.redesignData.nteLvlEffortAmt);
            this.setFormControlValue('nteInfo.nteOtLoe', this.redesignData.nteOtLvlEffortAmt);
            this.setFormControlValue('nteInfo.pmLoe', this.redesignData.pmLvlEffortAmt);
            this.setFormControlValue('nteInfo.pmOtLoe', this.redesignData.pmOtLvlEffortAmt);
            this.setFormControlValue('nteInfo.neLoe', this.redesignData.neLvlEffortAmt);
            this.setFormControlValue('nteInfo.neOtLoe', this.redesignData.neOtLvlEffortAmt);
            this.setFormControlValue('nteInfo.spsLoe', this.redesignData.spsLvlEffortAmt);
            this.setFormControlValue('nteInfo.spsOtLoe', this.redesignData.spsOtLvlEffortAmt);
            this.setFormControlValue('nteInfo.mssImplementEst', this.redesignData.mssImplEstAmt);
            if (this.redesignData.billOvrrdnCd) {
              this.setFormControlValue('nteInfo.overrideCost', this.redesignData.billOvrrdnCd);
              this.setFormControlValue('nteInfo.overrideCostAmt', this.redesignData.billOvrrdnAmt);
              this.setFormControlValue('nteInfo.totalCost',
                this.helper.isEmpty(this.redesignData.billOvrrdnAmt) ? this.redesignData.billOvrrdnAmt : this.redesignData.billOvrrdnAmt.toFixed(2));
            }
            else {
              this.setFormControlValue('nteInfo.totalCost',
                this.helper.isEmpty(this.redesignData.costAmt) ? this.redesignData.costAmt : this.redesignData.costAmt.toFixed(2));
            }

            // Extend Expiration
            this.extendExpiration = false;
            if ((!this.redesignData.extXpirnFlgCd || this.helper.isEmpty(this.redesignData.extXpirnFlgCd))
              && !this.helper.isEmpty(this.redesignData.exprtnDt)) {
              let exprtnDate = moment(this.redesignData.exprtnDt).subtract(90, 'days').toDate();
              this.extendExpiration = exprtnDate < new Date() ? true : false;
            }

            this.noteHistoryList = this.redesignData.notes;
            this.status.setValue(this.redesignData.stusId);
            resolve(true);
          },
          error => {
            this.helper.notify(`An error occurred while retrieving Redesign data.
              ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
            this.disableForm();
            resolve(false);
          }).then(() => {
            if (!this.isLocked && this.redesignId > 0) {
              this.redesignRecLockSrvc.lock(this.redesignId).toPromise().then(() => { });
            }
            this.setRedesignFormFields();
          });
      }
    });
  }

  /**
   * Method called to set form fields depending on Roles (User access),
   * Category Type (Voice, Data, Security) and Mode (Create/Update)
   */
  setRedesignFormFields() {
    // Enable Form and buttons
    this.form.enable();
    this.setRedesignWorkflow();
    this.filterOnDeviceInfo = false;
    this.isDeviceInfoDisabled = false;
    if (this.redesignData.redsgnCatId == ERedesignCategory.Security
      && (this.isNTE || this.isSDE || this.isMDSAdmin || this.isMVSAdmin)) {
      this.form.get('nteInfo.mssImplementEst').enable();
    }
    // Updated by Sarah Sandoval [20200323] - based on Mar2020 Redesign Enhancement
    // Updated the status since Pending status(223) is replaced by PendingNTE(230) and PendingSDE(231)
    if (this.isSSE) {
      if (this.redesignData.stusId >= ERedesignStatus.Submitted) {
        this.form.get("redesignCategory").disable();
        this.form.get("h1Customer").disable();
        this.form.get("h1").disable();
        if (!this.helper.isEmpty($('#btnH1Lookup')) && $('#btnH1Lookup').length > 0) {
          $('#btnH1Lookup')[0].disabled = true;
        }

        if (this.redesignData.stusId == ERedesignStatus.Submitted) {
          //this.form.get("custInfo").enable();
          this.customerRspnId.disable();
          //this.form.get("custInfo.customerRspnId").disable();
          //this.form.get("custInfo.custTeamPdl").disable();
          //this.option.custInfo.userFinderForNteAssigned.isEnabled = true;
          //this.option.custInfo.userFinderForPmAssigned.isEnabled = true;
          //this.option.custInfo.userFinderForNeAssigned.isEnabled = true;
          //this.option.custInfo.userFinderForMssSeAssigned.isEnabled = true;
        }
        else {
          //this.form.get("custInfo").disable();
          //this.option.custInfo.userFinderForNteAssigned.isEnabled = false;
          //this.option.custInfo.userFinderForPmAssigned.isEnabled = false;
          //this.option.custInfo.userFinderForNeAssigned.isEnabled = false;
          //this.option.custInfo.userFinderForMssSeAssigned.isEnabled = false;
        }

        this.showNteInfo = true;
        this.showNteInfo = this.redesignData.stusId > ERedesignStatus.Submitted;
        this.form.get("nteInfo.totalCost").disable();
        this.form.get("nteInfo.overrideCost").disable();
        
        // Added by Sarah Sandoval [20200513]
        // To prevent New Device validation when changing to Reviewing Status
        if (this.redesignData.redsgnCatId == ERedesignCategory.Voice) {
          this.form.get("devicesInfo.newDevice").disable();
        }

        if (this.redesignData.stusId >= ERedesignStatus.Approved
          && this.redesignData.stusId < ERedesignStatus.PendingNTE) {
          // 225 Approved, 226 Cancelled, 227 Completed, 228 Delete
          this.form.get("redesignTypeInfo").disable();
          this.form.get("nteInfo").disable();
          this.form.get("extensionInfo").disable();
          this.form.get("commentEmailInfo.emailNotifications").disable();
          this.option.userFinderForEmailNotifications.isEnabled = false;
          this.form.get("commentEmailInfo.designDocUrl").disable();
          this.form.get("commentEmailInfo.designDocBPM").disable();
          this.form.get("status").disable();

          if (this.redesignData.stusId == ERedesignStatus.Approved) {
            this.option.documentEntity.inputFileControl.isEnabled = false;
          }
          else {
            this.form.get("devicesInfo").disable();
            this.isDeviceInfoDisabled = true;
            if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
              $('#btnDeviceFilter')[0].disabled = true;
            }
          }

          // Cater role combination
          if (this.isNTE) {
            this.form.get("status").enable();
            this.form.get("extensionInfo").enable();
          }
        }
      }
    }
    else if (this.isNTE) {
      this.form.get("redesignCategory").disable();
      this.form.get("h1Customer").disable();
      this.form.get("h1").disable();
      if (!this.helper.isEmpty($('#btnH1Lookup')) && $('#btnH1Lookup').length > 0) {
        $('#btnH1Lookup')[0].disabled = true;
      }
      //this.form.get("custInfo").disable();
      //this.option.custInfo.userFinderForNteAssigned.isEnabled = false;
      //this.option.custInfo.userFinderForPmAssigned.isEnabled = false;
      //this.option.custInfo.userFinderForNeAssigned.isEnabled = false;
      //this.option.custInfo.userFinderForMssSeAssigned.isEnabled = false;
      this.showNteInfo = this.redesignData.stusId > ERedesignStatus.Draft;
      this.form.get("nteInfo.totalCost").disable();
      this.form.get("nteInfo.overrideCost").disable();

      if (this.redesignData.stusId <= ERedesignStatus.Submitted
        || (this.redesignData.stusId >= ERedesignStatus.Cancelled
          && this.redesignData.stusId < ERedesignStatus.PendingNTE)) {
        this.form.get("redesignTypeInfo").disable();
        this.form.get("devicesInfo").disable();
        this.isDeviceInfoDisabled = true;
        if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
          $('#btnDeviceFilter')[0].disabled = true;
        }
        this.form.get("nteInfo").disable();
        this.form.get("extensionInfo").disable();
        this.form.get("commentEmailInfo.caveats").disable();
        this.form.get("commentEmailInfo.emailNotifications").disable();
        this.option.userFinderForEmailNotifications.isEnabled = false;
        this.form.get("commentEmailInfo.designDocUrl").disable();
        this.form.get("commentEmailInfo.designDocBPM").disable();

        if (this.redesignData.stusId == ERedesignStatus.Cancelled
          || this.redesignData.stusId == ERedesignStatus.Completed) {
          this.form.get("status").disable();
        }
      }
      else if (this.redesignData.stusId == ERedesignStatus.Approved) {
        this.form.get("redesignTypeInfo").disable();
        this.form.get("devicesInfo.deviceFilter").disable();
        if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
          $('#btnDeviceFilter')[0].disabled = true;
        }
        this.option.documentEntity.inputFileControl.isEnabled = false;
        this.option.userFinderForEmailNotifications.isEnabled = false;
        this.form.get("nteInfo").disable();
        this.form.get("commentEmailInfo.emailNotifications").disable();
        this.option.userFinderForEmailNotifications.isEnabled = false;
        this.form.get("commentEmailInfo.designDocUrl").disable();
        this.form.get("commentEmailInfo.designDocBPM").disable();
      }
      else {
        // For Reviewing (222), Pending SDE (2230 and Pending NTE (230)
        if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
          $('#btnDeviceFilter')[0].disabled = false;
        }
      }
    }
    else if (this.isSDE) {
      if (this.redesignData.stusId == ERedesignStatus.Draft) {
        //this.nteAssigned.disable();
        //this.option.custInfo.userFinderForNteAssigned.isEnabled = false;
        //this.pmAssigned.disable();
        //this.option.custInfo.userFinderForPmAssigned.isEnabled = false;
        //this.neAssigned.disable();
        //this.option.custInfo.userFinderForNeAssigned.isEnabled = false;
        //this.mssSeAssigned.disable();
        //this.option.custInfo.userFinderForMssSeAssigned.isEnabled = false;
        this.showNteInfo = false;
      }
      else {
        this.form.get("redesignCategory").disable();
        this.form.get("h1Customer").disable();
        this.form.get("h1").disable();
        if (!this.helper.isEmpty($('#btnH1Lookup')) && $('#btnH1Lookup').length > 0) {
          $('#btnH1Lookup')[0].disabled = true;
        }
        //this.form.get("custInfo").disable();
        //this.option.custInfo.userFinderForNteAssigned.isEnabled = false;
        //this.option.custInfo.userFinderForPmAssigned.isEnabled = false;
        //this.option.custInfo.userFinderForNeAssigned.isEnabled = false;
        //this.option.custInfo.userFinderForMssSeAssigned.isEnabled = false;
        this.form.get("redesignTypeInfo").disable();
        this.form.get("devicesInfo").disable();
        this.isDeviceInfoDisabled = true;
        if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
          $('#btnDeviceFilter')[0].disabled = true;
        }
        this.form.get("nteInfo").disable();
        this.showNteInfo = true;
        this.form.get("extensionInfo").disable();
        this.form.get("commentEmailInfo.caveats").disable();
        this.form.get("commentEmailInfo.emailNotifications").disable();
        this.option.userFinderForEmailNotifications.isEnabled = false;
        this.form.get("commentEmailInfo.designDocUrl").disable();

        if (this.redesignData.stusId == ERedesignStatus.Reviewing) {
          this.form.get("status").disable();
          this.form.get("suppressEmails").disable();
        }
        if (this.redesignData.stusId == ERedesignStatus.Approved) {
          this.form.disable();
          this.isFormDisabled = true;
        }
        if (this.redesignData.stusId == ERedesignStatus.Cancelled
          || this.redesignData.stusId == ERedesignStatus.Completed) {
          this.form.get("commentEmailInfo.designDocBPM").disable();
          this.form.get("status").disable();
        }
      }
    }

    // Added by Sarah Sandoval [20200520] - To cater MDS Admin/Reviewer and MVS Admin/Reviewer
    if (this.isMDSAdmin || this.isMDSReviewer || this.isMVSAdmin || this.isMVSReviewer) {
      //this.form.get("custInfo").enable();
      this.showNteInfo = false;
      if (this.isMDSAdmin || this.isMVSAdmin) {
        //this.option.custInfo.userFinderForNteAssigned.isEnabled = true;
        //this.option.custInfo.userFinderForPmAssigned.isEnabled = true;
        //this.option.custInfo.userFinderForNeAssigned.isEnabled = true;
        //this.option.custInfo.userFinderForMssSeAssigned.isEnabled = true;
      }
      else {
        if (this.redesignData.stusId > ERedesignStatus.Submitted || this.isSDE) {
          //this.form.get("custInfo.nteAssigned").disable();
          //this.option.custInfo.userFinderForNteAssigned.isEnabled = false;
          //this.form.get("custInfo.pmAssigned").disable();
          //this.option.custInfo.userFinderForPmAssigned.isEnabled = false;
          //this.form.get("custInfo.neAssigned").disable();
          //this.option.custInfo.userFinderForNeAssigned.isEnabled = false;
          //this.form.get("custInfo.mssSeAssigned").disable();
          //this.option.custInfo.userFinderForMssSeAssigned.isEnabled = false;
        }
      }

      if (this.redesignData.stusId == ERedesignStatus.Draft) {
        if (this.isNTE) {
          this.customerRspnId.disable();
          //this.form.get("custInfo.customerRspnId").disable();
          // Commented by Sarah Sandoval [20210308] - To allow Admin/Reviewer with NTE roles to still update PDL
          //this.form.get("custInfo.custTeamPdl").disable();
        }
      }
      else {
        this.form.get("redesignCategory").disable();
        this.form.get("h1Customer").disable();
        this.form.get("h1").disable();
        if (!this.helper.isEmpty($('#btnH1Lookup')) && $('#btnH1Lookup').length > 0) {
          $('#btnH1Lookup')[0].disabled = true;
        }
        this.customerRspnId.disable();
        //this.form.get("custInfo.customerRspnId").disable();
        // Added by Sarah Sandoval [20210326] - Enable PDL for submitted status due to BMP Redesign
        //if (this.redesignData.stusId == ERedesignStatus.Submitted) {
        //  this.form.get("custInfo.custTeamPdl").enable();
        //}
        //else {
        //  this.form.get("custInfo.custTeamPdl").disable();
        //}
        this.form.get("nteInfo").disable();
        if (this.redesignData.stusId > ERedesignStatus.Submitted) {
          this.showNteInfo = true;

          if (this.redesignData.stusId == ERedesignStatus.Reviewing
              || this.redesignData.stusId == ERedesignStatus.PendingSDE
              || this.redesignData.stusId == ERedesignStatus.PendingNTE
              || this.redesignData.stusId == ERedesignStatus.Approved) {
            if (!this.isSDE) {
              this.form.get("nteInfo").enable();
              this.form.get("nteInfo.customerBypass").disable();
              this.form.get("nteInfo.overrideCost").disable();
            }
          }
        }

        if (this.redesignData.stusId == ERedesignStatus.Reviewing
          && (this.isMDSAdmin || this.isMVSAdmin)) {
          this.form.get("devicesInfo").enable();
          if (this.isSDE) {
            this.isDeviceInfoDisabled = true;
            if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
              $('#btnDeviceFilter')[0].disabled = false;
            }
          }
          else if (this.isNTE) {
            this.form.get("devicesInfo.deviceFilter").enable();
            if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
              $('#btnDeviceFilter')[0].disabled = false;
            }
          }
        }

        if (this.redesignData.stusId > ERedesignStatus.Reviewing
          && (this.isMDSReviewer || this.isMVSReviewer)) {
          this.form.get("devicesInfo.deviceFilter").disable();
          if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
            $('#btnDeviceFilter')[0].disabled = true;
          }
        }

        if (this.redesignData.stusId == ERedesignStatus.Approved) {
          this.form.get("redesignTypeInfo").disable();
          this.form.get("devicesInfo.deviceFilter").disable();
          if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
            $('#btnDeviceFilter')[0].disabled = true;
          }
          this.option.documentEntity.inputFileControl.isEnabled = false;
          this.form.get("commentEmailInfo.emailNotifications").disable();
          this.option.userFinderForEmailNotifications.isEnabled = false;
          this.form.get("commentEmailInfo.designDocBPM").disable();
          if (!this.isSDE) {
            this.form.get("nteInfo").enable();
            this.form.get("nteInfo.overrideCost").disable();
          }
          this.isFormDisabled = false;
          this.form.get("status").enable();
        }

        if (this.redesignData.stusId == ERedesignStatus.Cancelled
          || this.redesignData.stusId == ERedesignStatus.Completed) {
          this.form.get("redesignTypeInfo").disable();
          this.form.get("devicesInfo").disable();
          this.isDeviceInfoDisabled = true;
          if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
            $('#btnDeviceFilter')[0].disabled = true;
          }
          this.form.get("nteInfo").disable();
          this.form.get("extensionInfo").disable();
          this.form.get("commentEmailInfo.caveats").disable();
          this.form.get("commentEmailInfo.emailNotifications").disable();
          this.option.userFinderForEmailNotifications.isEnabled = false;
          this.form.get("commentEmailInfo.designDocBPM").disable();

          this.form.get("status").enable();
        }
      }
    }

    // Regardless of User Profiles
    if (this.redesignData.stusId == ERedesignStatus.Cancelled
      || this.redesignData.stusId == ERedesignStatus.Completed) {
      this.form.get("extensionInfo").disable();
    }

    // Always disable
    this.customerBypass.disable();

    // Disable form if locked or no access
    if (!(this.isSDE || this.isNTE || this.isSSE || this.isMDSAdmin
      || this.isMVSAdmin || this.isMDSReviewer || this.isMVSReviewer) || this.isLocked) {
      this.disableForm();
      if (this.redesignId > 0) {
        this.editMode = this.redesignId > 0 ? false : true;
      }
    }
  }

  /**
   * Method called to disable form on locked as well as the buttons
   */
  disableForm() {
    this.form.disable();
    this.isFormDisabled = true;
    if (!this.helper.isEmpty($('#btnH1Lookup')) && $('#btnH1Lookup').length > 0) {
      $('#btnH1Lookup')[0].disabled = true;
    }
    //this.option.custInfo.userFinderForNteAssigned.isEnabled = false;
    //this.option.custInfo.userFinderForMssSeAssigned.isEnabled = false;
    //this.option.custInfo.userFinderForPmAssigned.isEnabled = false;
    //this.option.custInfo.userFinderForNeAssigned.isEnabled = false;
    this.option.userFinderForEmailNotifications.isEnabled = false;
    this.isDeviceInfoDisabled = true;
    if (!this.helper.isEmpty($('#btnDeviceFilter')) && $('#btnDeviceFilter').length > 0) {
      $('#btnDeviceFilter')[0].disabled = true;
    }
    if (!this.helper.isEmpty($('#btnDesignDocUrl')) && $('#btnDesignDocUrl').length > 0) {
      $('#btnDesignDocUrl')[0].disabled = true;
    }
  }

  setRedesignWorkflow() {
    if (this.redesignId > 0 && this.statusList != null && this.statusList.length > 0) {
      let origStatusList = this.statusList;

      if (this.isSSE) {
        if (this.redesignData.stusId == ERedesignStatus.Reviewing) {
          this.statusList = this.statusList.filter(i => i.value != ERedesignStatus.PendingNTE
            && i.value != ERedesignStatus.PendingSDE);
        }
        else if (this.redesignData.stusId == ERedesignStatus.PendingSDE) {
          this.statusList = this.statusList.filter(i => i.value != ERedesignStatus.PendingNTE);
        }
        else if (this.redesignData.stusId == ERedesignStatus.PendingNTE) {
          this.statusList = this.statusList.filter(i => i.value != ERedesignStatus.PendingSDE);
        }
        else if (this.redesignData.stusId >= ERedesignStatus.Approved
          && this.redesignData.stusId < ERedesignStatus.PendingNTE) {
          this.statusList = this.statusList.filter(i => i.value == this.redesignData.stusId);
        }

        // Additional Status for combination profiles
        if (this.isNTE) {
          // Add Pending NTE and Pending SDE for Reviewing, Pending SDE and Pending NTE status
          if (this.redesignData.stusId == ERedesignStatus.Reviewing
            || this.redesignData.stusId == ERedesignStatus.PendingSDE
            || this.redesignData.stusId == ERedesignStatus.PendingNTE) {
            if (this.statusList.findIndex(i => i.value == ERedesignStatus.PendingSDE) == -1) {
              this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.PendingSDE], ERedesignStatus.PendingSDE));
            }

            if (this.statusList.findIndex(i => i.value == ERedesignStatus.PendingNTE) == -1) {
              this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.PendingNTE], ERedesignStatus.PendingNTE));
            }
          }

          // Add Cancelled and Completed for Approved Status
          if (this.redesignData.stusId == ERedesignStatus.Approved) {
            if (this.statusList.findIndex(i => i.value == ERedesignStatus.Cancelled) == -1) {
              this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.Cancelled], ERedesignStatus.Cancelled));
            }

            if (this.statusList.findIndex(i => i.value == ERedesignStatus.Completed) == -1) {
              this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.Completed], ERedesignStatus.Completed));
            }
          }
        }
      }
      else if (this.isNTE) {
        if (this.redesignData.stusId > ERedesignStatus.Approved
          && this.redesignData.stusId < ERedesignStatus.PendingNTE) {
          this.statusList = this.statusList.filter(i => i.value == this.redesignData.stusId);
        }
      }
      else if (this.isSDE) {
        // Pre Workflow Status is Submitted
        if (this.redesignData.stusId == ERedesignStatus.Submitted) {
          this.statusList = this.statusList.filter(i => i.value != ERedesignStatus.Reviewing);
        }
        // Pre Workflow Status is greater than Submitted
        else if (this.redesignData.stusId > ERedesignStatus.Submitted) {
          if (this.redesignData.stusId == ERedesignStatus.PendingSDE) {
            this.statusList = this.statusList.filter(i => i.value == ERedesignStatus.PendingNTE
              || i.value == ERedesignStatus.PendingSDE);
          }
          else {
            this.statusList = this.statusList.filter(i => i.value == this.redesignData.stusId);
          }
        }
      }

      // Added by Sarah Sandoval [20200604] - MDS Admin/UCaaS Admin can return back to Reviewing
      if (this.isMDSAdmin || this.isMVSAdmin || this.isMDSReviewer || this.isMVSReviewer) {
        if (this.redesignData.stusId == ERedesignStatus.Reviewing) {
          if (!this.isNTE) {
            this.statusList = origStatusList.filter(i => i.value != ERedesignStatus.PendingNTE
              && i.value != ERedesignStatus.PendingSDE);
          }
        }
        else if (this.redesignData.stusId == ERedesignStatus.PendingSDE) {
          if (this.isSDE) {
            this.statusList = origStatusList.filter(i => i.value == ERedesignStatus.PendingSDE
              || i.value == ERedesignStatus.PendingNTE);
          }
          else if (!this.isNTE) {
            this.statusList = origStatusList.filter(i => i.value != ERedesignStatus.PendingNTE);
          }
        }
        else if (this.redesignData.stusId == ERedesignStatus.PendingNTE) {
          if (this.isSDE) {
            this.statusList = origStatusList.filter(i => i.value == ERedesignStatus.PendingNTE);
          }
          else if (!this.isNTE) {
            this.statusList = origStatusList.filter(i => i.value != ERedesignStatus.PendingSDE);
          }
        }
        else if (this.redesignData.stusId == ERedesignStatus.Approved) {
          if (this.isMDSAdmin || this.isMVSAdmin) {
            if (this.statusList.findIndex(i => i.value == ERedesignStatus.Reviewing) == -1) {
              this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.Reviewing], ERedesignStatus.Reviewing));
            }
          }
          if (!this.isNTE) {
            if (this.statusList.findIndex(i => i.value == ERedesignStatus.Cancelled) == -1) {
              this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.Cancelled], ERedesignStatus.Cancelled));
            }
            
            if (this.statusList.findIndex(i => i.value == ERedesignStatus.Completed) == -1) {
              this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.Completed], ERedesignStatus.Completed));
            }
          }
        }
        else if (this.redesignData.stusId == ERedesignStatus.Cancelled) {
          if (this.statusList.findIndex(i => i.value == ERedesignStatus.Approved) == -1) {
            this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.Approved], ERedesignStatus.Approved));
          }
          
          if (this.isMDSAdmin || this.isMVSAdmin) {
            if (this.statusList.findIndex(i => i.value == ERedesignStatus.Reviewing) == -1) {
              this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.Reviewing], ERedesignStatus.Reviewing));
            }
          }
        }
        else if (this.redesignData.stusId == ERedesignStatus.Completed) {
          if (this.statusList.findIndex(i => i.value == ERedesignStatus.Approved) == -1) {
            this.statusList.push(new KeyValuePair(ERedesignStatus[ERedesignStatus.Approved], ERedesignStatus.Approved));
          }
        }
      }
    }
  }

  onCustomContactCreating(args) {
    let newValue = args.text;
    this.additionalEmails.push(newValue)
    args.customItem = newValue;
  }

  onContactDetailSaved(result) {
    this.contactList = result;
  }

  /**
   * Event called to handle the Entire Network checked box
   */
  //onChanges() {
  //  this.form.get('devicesInfo.entireNetwork').valueChanges.subscribe(bool => {
  //    if (this.redesignId > 0) {
  //      if (this.filterOnDeviceInfo) {
  //        this.devices
  //          .patchValue(Array(this.deviceNameList.length).fill(bool), { emitEvent: false });
  //      }
  //      else {
  //        if (this.redesignDevices != null && this.redesignDevices.length > 0) {
  //          this.redesignDevices.forEach(i => { i.isSelected = bool; }) //recStusId
  //        }
  //      }
  //    }
  //    else {
  //      this.devices
  //        .patchValue(Array(this.deviceNameList.length).fill(bool), { emitEvent: false });
  //    }
  //  });
  //}

  /**
   * Event called to handle the Entire Network checked box
   */
  onEntireNetworkChanged(e: any) {
    if (this.redesignId > 0) {
      //if (this.filterOnDeviceInfo && this.devices != null && this.devices.length > 0) {
      //  this.devices
      //    .patchValue(Array(this.deviceNameList.length).fill(e.value), { emitEvent: false });
      //}
      if (this.filterOnDeviceInfo) {
        if (e.value)
          this.list.instance.selectAll()
        else
          this.list.instance.unselectAll()
      }
      else if (this.redesignDevices != null && this.redesignDevices.length > 0) {
        this.redesignDevices.forEach(i => { i.isSelected = e.value; })
      }
    }
    else {
      //this.devices
      //  .patchValue(Array(this.deviceNameList.length).fill(e.value), { emitEvent: false });
      //this.deviceNameList.forEach(i => { i.selected = e.value; })
      if (e.value)
        this.list.instance.selectAll()
      else
        this.list.instance.unselectAll()
    }
  }

  /**
   * Event called when Redesign Category is changed
   * @param e for event
   */
  onRedesignCategoriesChanged(e) {
    if (this.redesignId > 0) {
      // Error or warning note when changing category on update
      // No requirement for this yet
      if (this.redesignCategory.value != e.value) { }
    }
    else {
      this.customerRspnId.setValue(0);
      //console.log(1111)
      this.h1.reset();
      this.h1.markAsUntouched();
      //this.redesignType.reset();
      this.redesignTypeInfo.reset();
      this.odieRspnList = [];
      //this.form.get('custInfo').reset();
      this.isH1LookupClicked = false;
      this.custNamePDL = e.value == ERedesignCategory.Voice ? 'UCaaSRedesignTeamPDL' : 'RedesignDataSecurityEmailPDL';
      // Clear Redesign Contact List
      this.showContactDetails = false;
      this.contactList = [];
    }
  }

  /**
   * Event called when the Customer H1 Lookup is changed
   * @param e for event
   */
  onH1CustomerLookupChanged(e) {
    //console.log(e)
    if (!this.helper.isEmpty(e.value)) {
      if (this.redesignId > 0) {
        // Error or warning note when changing customer H1 on update
        // No requirement for this yet
      }
      else {
        //console.log(this.h1CustomerLookupList.filter(i => i.customerH1 == e.value))
        this.h1.setValue(this.h1CustomerLookupList.filter(i => i.customerH1 == e.value)[0].h1);
        //this.h1.setValue(e.selectedItem.h1);
        this.customerRspnId.setValue(0);
        //this.form.get('custInfo').reset();
        //this.redesignType.reset();
        this.redesignTypeInfo.reset();
        this.odieRspnList = [];
        this.isH1LookupClicked = false;
        this.odieReqCd = false;
        // Clear Redesign Contact List
        this.showContactDetails = false;
        this.contactList = [];
      }
    }
  }

  /**
   * Event called when the Customer H1 Lookup is changed
   * @param e for event
   */
  onH1Changed() {
    if (this.redesignId > 0) {
      // Error or warning note when changing customer H1 on update
      // No requirement for this yet
    }
    else {
      this.h1Customer.reset();
      this.customerRspnId.setValue(0);
      //this.form.get('custInfo').reset();
      //this.redesignType.reset();
      this.redesignTypeInfo.reset();
      this.odieRspnList = [];
      this.isH1LookupClicked = false;
      this.odieReqCd = false;
      // Clear Redesign Contact List
      this.showContactDetails = false;
      this.contactList = [];
    }
  }

  /**
   * Event called to mimic the H1 lookup on custom form
   */
  onH1Lookup() {
    //console.log(this.redesignCategory.value)
    //console.log(this.h1.value)
    this.odieTimeout = false;
    if (this.helper.isEmpty(this.redesignCategory.value) || this.helper.isEmpty(this.h1.value)) {
      this.helper.notify("Please select Redesign Category and/or H1.", Global.NOTIFY_TYPE_WARNING);
      return;
    }
    else if (!this.helper.isEmpty(this.h1.value) && this.h1.invalid) {
      this.helper.notify("H1 field is not in correct format. H1 should contain 9 digits only.", Global.NOTIFY_TYPE_WARNING);
      return;
    }
    else {
      // To prevent calling this on View and will be called only on Create or when H1 or Category is changed
      if ((this.redesignId <= 0) ||
        (this.redesignId > 0 && !this.helper.isEmpty(this.redesignData)
          && this.redesignData.h1Cd != this.h1.value
          && this.redesignData.redsgnCatId != this.redesignCategory.value)) {

        this.spinner.show();
        this.isH1LookupClicked = true;

        let data = zip(
          this.redTypeSrvc.getByRedesignCategoryId(this.redesignCategory.value),
          //this.searchSrvc.getSysCfgValue(this.custNamePDL),
          this.searchSrvc.getH1Lookup(this.h1.value)
        );

        data.subscribe(
          res => {
            this.redesignTypeList = res[0];
            //this.customerEmailPDL.setValue(res[1]);
            this.odieRspnList = (res[1] as OdieRspn[]).filter(i => !this.helper.isEmpty(i.custNme));
            //console.log('onH1Lookup')
            //console.log(this.odieRspnList)
            if (!this.helper.isEmpty(this.odieRspnList) && this.odieRspnList.length > 0) {
              if (this.odieRspnList.length > 1) {
                let x = new OdieRspn({
                  custNme: "Please Select a Customer",
                  rspnId: 0
                });
                //x.custNme = "Please Select a Customer";
                //x.rspnId = 0;
                var _h1 = ' - ' + this.h1.value;
                var _h1Cust = this.odieRspnList.filter(i => i.custNme + _h1 == this.h1Customer.value);
                //console.log(_h1Cust)
                if (_h1Cust != null && _h1Cust.length > 0 && !this.helper.isEmpty(this.h1Customer.value)) {
                  //console.log(_h1Cust[0].rspnId)
                  this.customerRspnId.setValue(_h1Cust[0].rspnId)
                  //console.log(4444)
                }
                else {
                  this.odieRspnList.unshift(x)
                  this.customerRspnId.setValue(0)
                  //console.log(5555)
                }
              }
              else {
                this.customerRspnId.setValue(this.odieRspnList[0].rspnId);
                //console.log(6666)
                this.onOdieCustomerNameChanged(null);
              }
            }
            else {
              // ODIE return no record
              //this.odieTimeout = true;
            }
            this.spinner.hide();
          },
          error => {
            // Did not receive an ODIE response.
            //console.log(error)
            this.odieTimeout = true;
            this.spinner.hide();
          });

        //if (this.showContactDetails) {
        //  this.cntctList.refreshContact()
        //}
      }
    }
  }

  /**
   * Event called when Customer Name on Customer Info section is changed
   * @param e
   */
  onOdieCustomerNameChanged(e) {
    // To prevent calling this on View and will be called only on Create or when H1 or Category is changed
    //console.log('onOdieCustomerNameChanged: ' + this.redesignData.custNme != this.customerName.value)
    if ((this.redesignId <= 0) ||
      (this.redesignId > 0 && !this.helper.isEmpty(this.redesignData))
      && this.redesignData.custNme != this.customerName.value) {
      if (!this.helper.isEmpty(this.customerRspnId.value) && this.customerRspnId.value > 0) {
        this.redesignService.getCustNameDetails(this.customerRspnId.value).subscribe(data => {
          //console.log(data)
          this.customerName.setValue(data[0].cusT_NME);
          this.odieCustId.setValue(data[0].odiE_CUST_ID);
          this.showContactDetails = true;

          // Updated by Sarah Sandoval [20210615] - Removed for Contact Details (PJ025845)
          //this.sowsFolderPathName.setValue(data[0].sowS_FOLDR_PATH_NME);
          //this.custTeamPdl.setValue(this.redesignCategory.value == 1
          //  ? this.customerEmailPDL.value : data[0].cusT_TEAM_PDL);

          //if (this.redesignCategory.value != ERedesignCategory.Voice) {
          //  if (this.helper.isEmpty(this.nteAssigned.value)) {
          //    this.nteAssigned.setValue(data[0].nteAssigned);
          //    this.form.get('custInfo.nteAssignedUserAdId').setValue(data[0].ntE_ID);
          //  }

          //  if (this.helper.isEmpty(this.mssSeAssigned.value)) {
          //    this.mssSeAssigned.setValue(data[0].sdeAssigned);
          //    this.form.get('custInfo.mssSeAssignedUserAdId').setValue(data[0].sdE_ID);
          //  }

          //  if (this.helper.isEmpty(this.pmAssigned.value)) {
          //    this.pmAssigned.setValue(data[0].mnspM_USER);
          //    this.form.get('custInfo.pmAssignedUserAdId').setValue(data[0].mnspM_ID);
          //  }
          //}

          this.cntctList.refreshContact()
        });
      }
    }
  }

  /**
   * Event called when Device Filter is clicked on Redesign Type section
   */
  onRedesignTypeFilterDevice() {
    // To prevent calling this on View and will be called only on Create or when H1 or Category is changed
    if ((this.redesignId <= 0
      || (this.redesignId > 0 && !this.helper.isEmpty(this.redesignData)
        && this.redesignData.redsgnTypeId != this.redesignType.value))
      && this.redesignCategory.value != 0) {
      //console.log('onRedesignTypeFilterDevice')
      //console.log(this.redesignType)
      if (this.helper.isEmpty(this.redesignType.value) || this.redesignType.value == 0) {
        this.helper.notify('Please select redesign type.', Global.NOTIFY_TYPE_WARNING);
        return;
      }
      else {
        this.onRedesignTypeChanged(null);
      }
    }
  }

  /**
   * Event called when Redesign Type is changed
   * @param e
   */
  onRedesignTypeChanged(e) {
    //console.log('onRedesignTypeChanged')
    //console.log(e)
    // Added condition by Sarah Sandoval [20210407] - this.helper.isEmpty(e) when null comes from onRedesignTypeFilterDevice for Create
    if (this.helper.isEmpty(e)
      || (!this.helper.isEmpty(e) && !this.helper.isEmpty(e.value))) {
      //console.log('redesign type changed')
      // Customer Bypass Check
      this.redesignService.getRedesignCustomerBypassCD(this.h1.value, this.customerName.value)
        .subscribe(data => {
          this.setFormControlValue("nteInfo.customerBypass", data);
        });

      //if (this.validateDeviceFilter()) {
        let deviceFilter = this.validateDeviceFilter() ? this.form.get("redesignTypeInfo.deviceFilter").value : '';
        //if (this.redesignId == 0) {
          let redType = this.redesignTypeList.find(i => i.redsgnTypeId == this.redesignType.value);
          //console.log(redType)
          if (!this.helper.isEmpty(redType)) {
            this.odieReqCd = redType.odieReqCd;
            this.isBillable = redType.isBlblCd;
            // If odieReqCd is true then call Odie for devices; otherwise, it's a new device
            if (redType.odieReqCd) {
              this.spinner.show();
              this.redesignService.getODIEDevices(this.h1.value, this.customerName.value, this.odieCustId.value,
                deviceFilter, this.redesignCategory.value, this.redesignId, true).toPromise().then(
                  res => {
                    this.deviceNameList = res;
                    this.devicesLookupDS = new DataSource({
                      store: {
                        data: this.deviceNameList,
                        type: 'array',
                        //key: 'redsgnDevId'
                      }
                    });
                  },
                  error => {
                    //console.log('onDeviceInfoFilterDevice')
                    this.spinner.hide();
                    this.helper.notify('Error in getting devices info. ' + this.helper.getErrorMessage(error), Global.NOTIFY_TYPE_ERROR);
                  }).then(() => this.spinner.hide());

              //this.redesignService.chkODIEResponse(this.h1.value, this.customerName.value,
              //  this.odieCustId.value, deviceFilter, this.redesignCategory.value).toPromise().then(
              //    res => {
              //      if (res > 0) {
              //        this.odieRspnInfoReqId = res;
              //        this.getODIEDevices(res);
              //      }
              //    },
              //    error => {
              //      //this.spinner.hide();
              //      //console.log('onDeviceInfoFilterDevice')
              //      this.helper.notify('Error in getting devices info.', Global.NOTIFY_TYPE_ERROR);
              //    }).then(() => this.spinner.hide());
            }
          }
          //else {
          //  // If for a reason the redesign type is null or undefined
          //  this.helper.notify('Please select Redesign Type.', Global.NOTIFY_TYPE_WARNING);
          //}
        //}
      //}
    }
  }

  /**
   * Even called to load ODIE Devices combo box
   * @param _iReqID
   */
  //getODIEDevices(_iReqID: number) {
  //  if (_iReqID > 0) {
  //    this.redesignDevices = null;
  //    this.deviceNameList = null;
  //    // Get ODIE devices from ODIE Response Info table
  //    this.odieRspnInfoSrvc.getByReqId(_iReqID).toPromise().then(
  //      data => {
  //        if (!this.helper.isEmpty(data) && data.length > 0) {
  //          this.redesignDevices = data;
  //          this.deviceNameList = data.map((item) => new FormControlItem(item.rspnInfoId, item.devId));
  //          this.deviceNameList.forEach(item => {
  //            this.devices.push(this.fb.control(false));
  //          });
  //        }
  //        else {
  //          this.helper.notify('No matching devices are available in ODIE.', Global.NOTIFY_TYPE_WARNING);
  //        }
  //      },
  //      error => {
  //        this.spinner.hide();
  //        this.helper.notify('Error in getting devices.', Global.NOTIFY_TYPE_ERROR);
  //      }).then(() => this.spinner.hide());
  //  }
  //}

  /**
   * Event called when Device Filter is clicked on Device Info section
   *
   * */
  onDeviceInfoFilterDevice() {
    if (this.validateDeviceFilter()) {
      this.spinner.show();
      let deviceFilter = this.form.get("devicesInfo.deviceFilter").value;
      //console.log('h1: ' + this.h1.value + '   customerName: ' + this.customerName.value +
      //  '   odieCustId: ' + this.odieCustId.value + '   deviceFilter: ' + deviceFilter +
      //  '   redesignCategory: ' + this.redesignCategory.value)
      
      this.redesignService.getODIEDevices(this.h1.value, this.customerName.value, this.odieCustId.value,
        deviceFilter, this.redesignCategory.value, this.redesignId, true).toPromise().then(
          res => {
            //console.log('data: ' + res.length)
            this.selectedDeviceDS = this.redesignDevices.map((item) => item.devNme)
            this.filterOnDeviceInfo = true;
            this.deviceNameList = res;
            this.devicesLookupDS = new DataSource({
              store: {
                data: this.deviceNameList,
                type: 'array',
                //key: 'redsgnDevId'
              }
            });
          },
          error => {
            //console.log('onDeviceInfoFilterDevice')
            this.spinner.hide();
            this.helper.notify('Error in getting devices info. ' + this.helper.getErrorMessage(error), Global.NOTIFY_TYPE_ERROR);
          }).then(() => this.spinner.hide());

      //this.redesignService.chkODIEResponse(this.h1.value, this.customerName.value,
      //  this.odieCustId.value, deviceFilter, this.redesignCategory.value).toPromise().then(
      //    res => {
      //      this.odieRspnInfoReqId = res;
      //      //console.log('chkODIEResponse - odieRspnInfoReqId: ' + res)
      //      if (this.odieRspnInfoReqId > 0) {
      //        // Get ODIE devices from ODIE Response Info table
      //        this.odieRspnInfoSrvc.getByReqId(this.odieRspnInfoReqId).subscribe(
      //          data => {
      //            console.log('data: ' + data.length)
      //            if (!this.helper.isEmpty(data) && data.length > 0) {
      //              this.filterOnDeviceInfo = true;
      //              this.deviceNameList = data.map((item) => new FormControlItem(item.rspnInfoId, item.devId));
                    
      //              this.deviceNameList.forEach(item => {
      //                if (this.redesignDevices.findIndex(i => i.devNme === item.text) !== -1) {
      //                  this.devices.push(this.fb.control(true));
      //                } else {
      //                  this.devices.push(this.fb.control(false));
      //                }
      //              });
      //              //console.log('deviceNameList: ' + this.deviceNameList.length)
      //              //console.log('devices: ' + this.devices.length)
      //            }
      //            else {
      //              this.helper.notify('No matching devices are available in ODIE.', Global.NOTIFY_TYPE_WARNING);
      //            }
      //          },
      //          error => {
      //            this.spinner.hide();
      //            this.helper.notify('Failed in getting devices. ' + this.helper.getErrorMessage(error), Global.NOTIFY_TYPE_ERROR);
      //          });
      //      }
      //    },
      //    error => {
      //      //console.log('onDeviceInfoFilterDevice')
      //      this.spinner.hide();
      //      this.helper.notify('Error in getting devices info.', Global.NOTIFY_TYPE_ERROR);
      //    }).then(() => this.spinner.hide() );
    }
  }

  onDeviceSelectionChanged(e: any) {
    if (e.removedItems.length > 0) {
      this.selectedDeviceDS = this.selectedDeviceDS.filter((item) => !e.removedItems.includes(item));
    }

    if (e.addedItems.length > 0) {
      this.selectedDeviceDS = this.selectedDeviceDS.concat(e.addedItems)
    }
  }

  /**
   * Event called to validate Device Filter
   * */
  validateDeviceFilter(): any {
    const regEx = /^[0-9A-Za-z,]+$/;
    let deviceFilter = '';
    if (this.redesignId > 0) {
      deviceFilter = this.form.get("devicesInfo.deviceFilter").value;
    } else {
      deviceFilter = this.form.get("redesignTypeInfo.deviceFilter").value;
    }

    if (!this.helper.isEmpty(deviceFilter)) {
      if (!regEx.test(deviceFilter)) {
        this.helper.notify('Please only use comma separation for multiple Device filters', Global.NOTIFY_TYPE_WARNING);
        return false;
      }
    }

    return true;
  }

  onDeviceSelectAll(e: any) {
    //console.log(e)
    this.redesignDevices.forEach(i => i.isSelected = e.value);
  }

  onSelectionSCChanged(e) {
    //console.log(e)
    this.redesignDevices.forEach(i => {
      //i.scCd = e.selectedItem.value;
      i.scCd = e.value;
    });
  }

  onSCChanged(devId, val) {
    this.redesignDevices.forEach(i => {
      if (i.redsgnDevId == devId) {
        i.scCd = val;
      }
    });
  }

  onSelectionDRChanged(e) {
    this.redesignDevices.forEach(i => {
      //i.dispatchReadyCd = e.selectedItem.value;
      i.dispatchReadyCd = e.value;
    });
  }

  onDRChanged(devId, val) {
    this.redesignDevices.forEach(i => {
      if (i.redsgnDevId == devId) {
        i.dispatchReadyCd = val;
      }
    });
  }

  onTotalCostChanged(e) {
    if (this.totalCost.enabled
      && this.redesignCategory.value == ERedesignCategory.Data
      && !this.helper.isEmpty(this.totalCost.value)) {
      let newCost = this.totalCost.value;
      //console.log('newCost: ' + newCost)
      //console.log('costAmt: ' + this.redesignData.costAmt)
      if (this.redesignData.costAmt != newCost) {
        this.form.get('nteInfo.overrideCost').setValue(true);
      }
      else {
        this.form.get('nteInfo.overrideCost').setValue(false);
      }
    }
  }

  /**
   * Event called to open Design Doc Url on new tab
   */
  openDesignDocUrl() {
    let designDocUrl = this.getFormControlValue("commentEmailInfo.designDocUrl");
    if (!this.helper.isEmpty(designDocUrl)) {
      window.open(designDocUrl, "_blank", "toolbar=0,location=0,menubar=0")
    }
  }

  /**
   * Event called when status is changed
   */
  onStatusUpdateChange(e) {
    if (e.value == ERedesignStatus.Reviewing)
      this.setFormControlValue("suppressEmails", true);
    else
      this.setFormControlValue("suppressEmails", false);
  }

  /**
   * Event called on Save
   */
  save(isSaveExit: boolean) {
    //console.log('hasEditData: ' + this.gridDevices.instance.hasEditData())
    if (this.gridDevices != null && this.gridDevices.instance.hasEditData()) {
      //this.helper.notify('Changes made on Devices table is not yet saved. Please navigate away from the cell after updating it before clicking the Save button.', Global.NOTIFY_TYPE_WARNING);
      this.helper.notify('Changes made on Devices table have been saved. Please click the Save button again.',
        Global.NOTIFY_TYPE_WARNING);
      return;
    }
    this.isSaveExit = isSaveExit;
    if (this.validateFormData()) {
      this.saveRedesignData();
    }
  }

  /**
   * Event called on Cancel
   */
  cancel() {
    if (this.editMode) {
      this.unlockRedesign();
    }

    this.goBackToList();
  }

  /**
   * Event called to validate form data
   */
  validateFormData() {
    this.errors = [];
    this.option.validationSummaryOptions.title = null;
    this.option.validationSummaryOptions.message = null;
    if (this.status.value == ERedesignStatus.Cancelled) {
      if (this.redesignData.stusId == ERedesignStatus.Approved
        && this.redesignData.stusId != this.status.value) {
        this.redesignDevices.filter(i => i.isSelected && i.devCmpltnCd).forEach(i => { this.selectedDevices.push(i.devNme) });
        if (this.redesignId > 0 && this.selectedDevices != null && this.selectedDevices.length > 0) {
          if (!this.isSubmitted) {
            this.option.validationSummaryOptions.title = "Warning";
            this.option.validationSummaryOptions.message = 'Warning: Are you sure you want to change the status of this redesign. The following sites have been marked as "Completed":';
            this.selectedDevices.forEach(i => this.errors.push(i));
            this.isSubmitted = true;
            return false;
          }
          else {
            this.showWarningMessage = false;
            return true;
          }
        }
        else {
          return true;
        }
      }
      else {
        return true;
      }
    }

    // Added by Sarah Sandoval [20210101] - To prevent creating Redesign without required fields
    if (this.helper.isEmpty(this.redesignCategory.value)) {
      this.errors.push("Redesign Category is required.");
    }

    if (this.helper.isEmpty(this.h1.value)) {
      this.errors.push("H1 is required.");
    }

    if (this.helper.isEmpty(this.customerName.value)) {
      this.errors.push("Customer Name is required.");
    }

    //if (this.helper.isEmpty(this.custTeamPdl.value)) {
    //  this.errors.push("Customer Team PDL is required.");
    //}

    //console.log('Contact List hasEditData(): ' + this.cntctList.dataGrid.instance.hasEditData())
    if (this.cntctList.dataGrid.instance.hasEditData()) {
      this.errors.push("Changes made on Contact List table is not yet saved.");
    }
    if (this.contactList.filter(a =>
      this.helper.isEmpty(a.hierId) ||
      this.helper.isEmpty(a.hierLvlCd) ||
      this.helper.isEmpty(a.roleId) ||
      this.helper.isEmpty(a.emailAdr) ||
      this.helper.isEmpty(a.emailCdIds))
      .length > 0) {
      this.errors.push("Please enter the required fields in the Contact List")
    }

    if (this.helper.isEmpty(this.redesignType.value)) {
      this.errors.push("Redesign Type is required.");
    }

    if (this.redesignCategory.value != ERedesignCategory.Voice) {
      // helper.isEmpty is replaced for some reason '0' value always return true
      if (this.getFormControlValue("redesignTypeInfo.sdwan") === null) {
        this.errors.push("Please select SD-WAN option.");
      }
    }

    if (this.ciscoSmartLicense.value === null) {
      this.errors.push("Please select activation of a Cisco Smart License option.");
    } else {
      if (this.ciscoSmartLicense.value == 1) {
        const regExp = new RegExp('^[a-zA-Z0-9.\-]{1,100}$');
        if (this.helper.isEmpty(this.smartAccountDomain.value)) {
          this.errors.push("Smart Account Domain is required.");
        }
        else {
          if (!regExp.test(this.smartAccountDomain.value)) {
            this.errors.push("Smart Account Domain should be alpha numeric, maximum allowed characters of 100.");
          }
        }
        if (this.helper.isEmpty(this.virtualAccount.value)) {
          this.errors.push("Virtual Account is required.");
        }
        else {
          if (!regExp.test(this.virtualAccount.value)) {
            this.errors.push("Virtual Account should be alpha numeric, maximum allowed characters of 100.");
          }
        }
      }
    }

    // Check devices
    //console.log('Validate Devices: odieReqCd: ' + this.odieReqCd)
    const redsgnType = this.redesignTypeList.find(i => i.redsgnTypeId == this.redesignType.value);
    //if (this.odieReqCd) {
    if (redsgnType != null && redsgnType.odieReqCd) {
      // Check for ODIE Devices
      if (this.redesignId > 0) {
        if (this.filterOnDeviceInfo) {
          // Commented below due to new implementation - use dxList instead of checkbox array
          // Validate selection made on device checkboxes
          //var selectedDevices = this.form.value.devicesInfo.deviceNameList
          //  .map((checked, index) => checked ? this.deviceNameList[index].value : null)
          //  .filter(value => value !== null);
          
          //if (!(selectedDevices != null && selectedDevices.length > 0)) {
          //  this.errors.push("Please select at least one device for this Redesign.");
          //}

          if (!(this.selectedDeviceDS != null && this.selectedDeviceDS.length > 0)) {
            this.errors.push("Please select at least one device for this Redesign.");
          }
        }
        else {
          // Validate selection made on device table
          if (this.redesignDevices != null && this.redesignDevices.length > 0) {
            let dev = this.redesignDevices.filter(i => i.isSelected);
            if (!(dev != null && dev.length > 0)) {
              this.errors.push("Please select at least one device for this Redesign.");
            }
          }
        }
      }
      else {
        // Validate selection made on device checkboxes
        //var selectedDevices = this.form.value.devicesInfo.deviceNameList
        //  .map((checked, index) => checked ? this.deviceNameList[index].value : null)
        //  .filter(value => value !== null);

        //if (!(selectedDevices != null && selectedDevices.length > 0)) {
        //  this.errors.push("Please select at least one device to create Redesign number.");
        //}

        if (!(this.selectedDeviceDS != null && this.selectedDeviceDS.length > 0)) {
          this.errors.push("Please select at least one device for this Redesign.");
        }
      }
    }
    else {
      // Check for new devices
      if (this.redesignCategory.value == ERedesignCategory.Voice) {
        let newDevice = this.form.get('devicesInfo.newDevice');
        const regExp = new RegExp('^[a-zA-Z0-9\\s-_.,;]{3,}$');
        if (!(this.redesignDevices.length != null && this.redesignDevices.length > 0)
          && newDevice.valid
          && (this.helper.isEmpty(newDevice.value) || !regExp.test(newDevice.value))) {
          this.errors.push("Please enter valid device name in 'New Device' textbox, minimum 3 characters, allowed characters are alphanumeric - _ . , ;");
        }
      }

      //console.log('status: ' + this.redesignData.stusId + ' odieReqCd: ' + this.odieReqCd)
      //this.redesignData.stusId
      if (this.status.value >= ERedesignStatus.Reviewing) {
        if (this.filterOnDeviceInfo) {
          // Validate selection made on device checkboxes
          //console.log(this.form.value.devicesInfo)
          //console.log(this.form.value.devicesInfo.deviceNameList)
          //var selectedDevices = this.form.value.devicesInfo.deviceNameList
          //  .map((checked, index) => checked ? this.deviceNameList[index].value : null)
          //  .filter(value => value !== null);
          //if (!(selectedDevices != null && selectedDevices.length > 0)) {
          //  this.errors.push("Please select at least one device for this Redesign.");
          //}

          if (!(this.selectedDeviceDS != null && this.selectedDeviceDS.length > 0)) {
            this.errors.push("Please select at least one device for this Redesign.");
          }

          // Added validation by Sarah Sandoval [20210114]
          // For ITSM Ticket IM5982849 - NTE/SDE should be able to switch back with device validation
          if (this.selectedDeviceDS != null && this.selectedDeviceDS.length > 0
            && (this.status.value == ERedesignStatus.Approved || this.status.value == ERedesignStatus.Completed)) {
            this.errors.push("Please select Simple or Complex on all selected devices before approving or completing the design.");
            this.errors.push("Please select Dispatch Ready on all selected devices before approving or completing the design.");
          }
        }
        else {
          // Validate selection made on device table
          if (this.redesignDevices != null && this.redesignDevices.length > 0) {
            var dev = this.redesignDevices.filter(i => i.isSelected);
            if (!(dev != null && dev.length > 0)) {
              this.errors.push("Please select at least one device for this Redesign.");
            }
          }
          else {
            // Added validation by Sarah Sandoval [20210111]
            // For ITSM Ticket IM5982849 - NTE/SDE should be able to switch back with device validation
            if (this.status.value != ERedesignStatus.PendingNTE
              && this.status.value != ERedesignStatus.PendingSDE
              && this.status.value != ERedesignStatus.Reviewing) {
              this.errors.push("Please enter device name for newly added site.");
            }
          }
        }
      }
    }

    if (this.helper.isReallyEmpty(this.getFormControlValue("commentEmailInfo.description"))
      && ((this.status.value == ERedesignStatus.PendingNTE
        && this.redesignData.stusId != ERedesignStatus.PendingNTE)
      || (this.status.value == ERedesignStatus.PendingSDE
        && this.redesignData.stusId != ERedesignStatus.PendingSDE)
      || (this.status.value == ERedesignStatus.Cancelled
        && this.redesignData.stusId != ERedesignStatus.Cancelled))) {
      this.errors.push("Please enter comments/Description.");
    }

    // Validate Description/Caveats
    let data = zip(
      this.searchSrvc.getSysCfgValue('RedesignRegExOnDescription'),
      this.searchSrvc.getSysCfgValue('RedesignAllowedCharactersOnDescription')
    )

    data.toPromise().then(
      res => {
        let regex = res[0];
        let allowedCharacters = res[1];

        const regExp = new RegExp(regex);
        let caveats = this.form.get('commentEmailInfo.caveats').value;
        if (!this.helper.isReallyEmpty(caveats) && !regExp.test(caveats)) {
          this.errors.push("Caveats field is not in correct format. " +
            "Only allowed characters are as follows: Alphanumeric values and special characters like " +
            allowedCharacters);
        }

        let comments = this.form.get('commentEmailInfo.description').value;
        if (!this.helper.isReallyEmpty(comments) && !regExp.test(comments)) {
          this.errors.push("Description field is not in correct format. " +
            "Only allowed characters are as follows: Alphanumeric values and special characters like " +
            allowedCharacters);
        }
      });

    if (this.redesignId > 0
      && (this.status.value == ERedesignStatus.Approved || this.status.value == ERedesignStatus.Completed)) {
      if (this.redesignCategory.value == ERedesignCategory.Security) {
        if (this.helper.isReallyEmpty(this.form.get('nteInfo.mssImplementEst').value)) {
          this.errors.push("Please enter MSS Implementation Estimation before approving or completing the design.");
        } else {
          if (this.form.get('nteInfo.mssImplementEst').value <= 0) {
            this.errors.push("MSS Implementation Estimation value should be greater than 0.");
          }
        }
      }

      if (this.redesignCategory.value == ERedesignCategory.Voice) {
        // Updated by Sarah Sandoval [20210503] - Zero value is allowed for Voice LOE (IM6168883)
        if (this.helper.isEmpty(this.form.get('nteInfo.nteLoe').value)
          || this.helper.isEmpty(this.form.get('nteInfo.nteOtLoe').value)
          || this.helper.isEmpty(this.form.get('nteInfo.pmLoe').value)
          || this.helper.isEmpty(this.form.get('nteInfo.pmOtLoe').value)
          || this.helper.isEmpty(this.form.get('nteInfo.neLoe').value)
          || this.helper.isEmpty(this.form.get('nteInfo.neOtLoe').value)
          || this.helper.isEmpty(this.form.get('nteInfo.spsLoe').value)
          || this.helper.isEmpty(this.form.get('nteInfo.spsOtLoe').value)) {
          this.errors.push("Please enter LOE before approving or completing the design.");
        }
      }
      else {
        if (this.helper.isEmpty(this.form.get('nteInfo.nteLoe').value)
          || this.form.get('nteInfo.nteLoe').value == 0) {
          this.errors.push("Please enter LOE before approving or completing the design.");
        }
      }

      // Device Validation
      if (this.redesignDevices !== null && this.redesignDevices.length > 0) {
        const scCode = this.redesignDevices.filter(i => i.isSelected && this.helper.isEmpty(i.scCd.trim()));
        if (scCode !== null && scCode.length > 0) {
          this.errors.push("Please select Simple or Complex on all selected devices before approving or completing the design.");
        }
        const drCd = this.redesignDevices.filter(i => i.isSelected && i.dispatchReadyCd === null);
        if (drCd !== null && drCd.length > 0) {
          this.errors.push("Please select Dispatch Ready on all selected devices before approving or completing the design.");
        }
      }
      else {
        // Added Device Validation to prevent Approving or Completing Redesign without devices
        this.errors.push("Please select at least one device for this Redesign.");
      }
    }

    if (this.errors != null && this.errors.length > 0) {
      this.option.validationSummaryOptions.title = "Validation Summary";
      return false;
    }
    else {
      this.redesignDevices.filter(i => i.isSelected && i.devCmpltnCd).forEach(i => { this.selectedDevices.push(i.devNme) });
      if (this.redesignId > 0 && this.selectedDevices != null && this.selectedDevices.length > 0) {
        if (this.redesignData.stusId == ERedesignStatus.Approved
          && this.redesignData.stusId != this.status.value) {
          if (!this.isSubmitted) {
            this.option.validationSummaryOptions.title = "Warning";
            this.option.validationSummaryOptions.message = 'Warning: Are you sure you want to change the status of this redesign. The following sites have been marked as "Completed":';
            this.selectedDevices.forEach(i => this.errors.push(i));
            this.isSubmitted = true;
            return false;
          }
          else {
            this.showWarningMessage = false;
            return true;
          }
        }
        else {
          return true;
        }
      }
      else {
        return true;
      }
    }
  }

  /**
   * Event called to get and save Redesign Data
   */
  saveRedesignData() {
    if (this.redesignId == 0) {
      this.redesignData = new Redesign();
    }

    this.redesignData.redsgnCatId = this.redesignCategory.value;
    this.redesignData.h1Cd = this.h1.value;

    this.redesignData.custNme = this.customerName.value;
    this.redesignData.odieCustId = this.odieCustId.value;
    //this.redesignData.custEmailAdr = this.custTeamPdl.value;
    //this.redesignData.pmAssigned = this.form.get('custInfo.pmAssignedUserAdId').value;
    //this.redesignData.pmAssignedFullName = this.pmAssigned.value;
    //this.redesignData.nteAssigned = this.form.get('custInfo.nteAssignedUserAdId').value;
    //this.redesignData.nteAssignedFullName = this.nteAssigned.value;
    //this.redesignData.sdeAsnNme = this.form.get('custInfo.mssSeAssignedUserAdId').value;
    //this.redesignData.sdeAsnFullNme = this.mssSeAssigned.value;
    //this.redesignData.neAsnNme = this.form.get('custInfo.neAssignedUserAdId').value;
    //this.redesignData.neAsnFullNme = this.neAssigned.value;

    this.redesignData.redsgnTypeId = this.redesignType.value;
    let sdWan = this.getFormControlValue("redesignTypeInfo.sdwan");
    this.redesignData.isSdwan = this.helper.isEmpty(sdWan) ? false : sdWan;
    this.redesignData.ciscSmrtLicCd = this.helper.isEmpty(this.ciscoSmartLicense.value) ? false : this.ciscoSmartLicense.value;
    this.redesignData.smrtAccntDmn = this.getFormControlValue("redesignTypeInfo.smartAccountDomain");
    this.redesignData.vrtlAccnt = this.getFormControlValue("redesignTypeInfo.virtualAccount");

    let entireNW = this.getFormControlValue("devicesInfo.entireNetwork");
    this.redesignData.entireNwCkdId = this.helper.isEmpty(entireNW) ? false : entireNW;

    //if (!this.helper.isEmpty(this.form.value.devicesInfo)
    //  || (this.redesignId > 0 && this.filterOnDeviceInfo)) {
    //  var selectedDevices = this.form.value.devicesInfo.deviceNameList
    //    .map((checked, index) => checked ? this.deviceNameList[index].value : null)
    //    .filter(value => value !== null);
    //  this.redesignData.selectedOdieDevices = selectedDevices;
    //}
    this.redesignData.odieRspnInfoReqId = this.odieRspnInfoReqId;
    this.redesignData.newDevice = this.getFormControlValue("devicesInfo.newDevice");
    if (this.redesignId > 0) {
      if (this.filterOnDeviceInfo) {
        //var selectedDevices = this.form.value.devicesInfo.deviceNameList
        //  .map((checked, index) => checked ? this.deviceNameList[index].value : null)
        //  .filter(value => value !== null);
        //this.redesignData.selectedOdieDevices = selectedDevices;
        //this.redesignData.devices = [];
        this.redesignData.selectedOdieDevices = this.selectedDeviceDS;
      }
      else {
        this.redesignDevices.forEach(i => {
          i.dspchRdyCd = i.dispatchReadyCd == null ? null : (i.dispatchReadyCd == 1 ? true : false);
        });
        this.redesignData.devices = this.redesignDevices;
        this.redesignData.selectedOdieDevices = [];
      }
    }
    else {
      //var selectedDevices = this.form.value.devicesInfo.deviceNameList
      //  .map((checked, index) => checked ? this.deviceNameList[index].value : null)
      //  .filter(value => value !== null);
      //this.redesignData.selectedOdieDevices = selectedDevices;
      this.redesignData.selectedOdieDevices = this.selectedDeviceDS;
    }

    this.redesignData.docs = [];
    if (this.docList != null && this.docList.length > 0) {
      this.docList.forEach(i => {
        let doc = new RedesignDoc();
        doc.redsgnDocId = i.docId;
        doc.redsgnId = i.id;
        doc.fileNme = i.fileNme;
        // Commented by Sarah Sandoval [20220113] - To prevent UI error for maximum entity when uploading > 20 docs
        //doc.fileCntnt = i.fileCntnt;
        doc.fileSizeQty = i.fileSizeQty;
        doc.creatByUserId = i.creatByUserId;
        doc.creatDt = i.creatDt;
        doc.base64string = i.base64string;
        this.redesignData.docs.push(doc);
      });
    }

    this.redesignData.caveats = (this.getFormControlValue("commentEmailInfo.caveats") || "").replace(/\r?\n/g, "<br/>");
    this.redesignData.description = (this.getFormControlValue("commentEmailInfo.description") || "").replace(/\r?\n/g, "<br/>");
    this.redesignData.emailNotifications = this.getFormControlValue("commentEmailInfo.emailNotifications");
    this.redesignData.dsgnDocLocTxt = this.getFormControlValue("commentEmailInfo.designDocUrl");
    this.redesignData.dsgnDocNbrBpm = this.getFormControlValue("commentEmailInfo.designDocBPM");

    if (this.redesignId > 0) {
      this.redesignData.isBillable = this.isBillable;
      this.redesignData.nteLvlEffortAmt = this.getFormControlValue('nteInfo.nteLoe');
      this.redesignData.nteOtLvlEffortAmt = this.getFormControlValue('nteInfo.nteOtLoe');
      this.redesignData.pmLvlEffortAmt = this.getFormControlValue('nteInfo.pmLoe');
      this.redesignData.pmOtLvlEffortAmt = this.getFormControlValue('nteInfo.pmOtLoe');
      this.redesignData.neLvlEffortAmt = this.getFormControlValue('nteInfo.neLoe');
      this.redesignData.neOtLvlEffortAmt = this.getFormControlValue('nteInfo.neOtLoe');
      this.redesignData.spsLvlEffortAmt = this.getFormControlValue('nteInfo.spsLoe');
      this.redesignData.spsOtLvlEffortAmt = this.getFormControlValue('nteInfo.spsOtLoe');
      this.redesignData.mssImplEstAmt = this.getFormControlValue('nteInfo.mssImplementEst');
      this.redesignData.costAmt = this.getFormControlValue('nteInfo.totalCost');
      let override = this.convertToBool(this.getFormControlValue('nteInfo.overrideCost'));
      this.redesignData.billOvrrdnCd = override;
      if (override) {
        this.redesignData.billOvrrdnAmt = this.getFormControlValue('nteInfo.totalCost');
      }

      this.redesignData.extXpirnFlgCd = this.convertToBool(this.getFormControlValue('extensionInfo.chkExtensionFlag'));
    }

    this.redesignData.stusId = this.getFormControlValue("status");
    const statList = this.statusList.filter(i => i.value == this.redesignData.stusId);
    if(statList.length > 0) {
      this.redesignData.statusDesc = statList[0].description;
    }
     
    this.redesignData.suppressEmail = this.convertToBool(this.getFormControlValue("suppressEmails"));

    // Added Contact Details (PJ025845)
    this.contactList.forEach(i => i.emailCd = this.helper.isEmpty(i.emailCdIds) ? '' : i.emailCdIds.join(','));
    this.redesignData.contactDetails = this.contactList;

    //console.log(this.redesignData);
    this.spinner.show();

    if (this.redesignId > 0) {
      // Update
      this.redesignService.update(this.redesignId, this.redesignData).toPromise().then(
        res => {
          //this.redesignData = res;
          this.helper.notify('Successfully updated Redesign.', Global.NOTIFY_TYPE_SUCCESS);
        }, error => {
          this.spinner.hide();
          this.helper.notify(`An error occurred while updating Redesign data. ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
      }).then(() => {
          this.form.get("devicesInfo.newDevice").reset();
          this.form.get("commentEmailInfo.emailNotifications").reset();
          this.isSaveExit ? this.cancel() : this.loadRedesignData().then(() => { this.spinner.hide(); });
        });
    } else {
      // Create
      this.redesignService.create(this.redesignData).toPromise().then(
        res => {
          //this.redesignData = res;
          //this.redesignId = this.redesignData.redsgnId;
          //this.helper.notify('Redesign Number: ' + this.redesignData.redsgnNbr, Global.NOTIFY_TYPE_SUCCESS);
          this.redesignId = res.redsgnId;
          this.helper.notify('Redesign Number: ' + res.redsgnNbr, Global.NOTIFY_TYPE_SUCCESS);
        }, error => {
          this.spinner.hide();
          this.helper.notify(`An error occurred while creating Redesign data. ${this.helper.getErrorMessage(error)}`, Global.NOTIFY_TYPE_ERROR);
      }).then(() => {
          this.form.get("devicesInfo.newDevice").reset();
          this.form.get("commentEmailInfo.emailNotifications").reset();
          this.isSaveExit ? this.cancel() : this.loadRedesignData().then(() => { this.spinner.hide(); });
        });
    }
  }

  /**
   * Event called to unlock Redesign
   */
  unlockRedesign() {
    // Unlock Redesign
    if (this.redesignId > 0) {
      this.redesignRecLockSrvc.unlock(this.redesignId).subscribe(
        res => { },
        error => {
          this.helper.notify('An error occurred while unlocking the Redesign.', Global.NOTIFY_TYPE_ERROR);
        }
      );
    }
  }

  goBackToList() {
    this.spinner.hide();
    this.router.navigate(['redesign']);
  }

  convertToNull(cost: any): number {
    return this.helper.isEmpty(cost) ? null : cost;
  }

  convertToBool(str: any): boolean {
    return this.helper.isEmpty(str) ? false : str;
  }

  setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value;
    }

    return value;
  }
}
