import { Component, OnInit, Input, ViewChild, TemplateRef, ViewContainerRef } from '@angular/core';
import { NavLinkService, UserService, MenuService } from './../../services';
import { NavLink } from "./../../models";
import { Router } from '@angular/router';
import { OverlayRef, Overlay } from '@angular/cdk/overlay';
import { Subscription, fromEvent } from 'rxjs';
import { TemplatePortal } from '@angular/cdk/portal';
import { filter, take } from 'rxjs/operators';

@Component({
  selector: 'app-nav-links',
  templateUrl: './nav-links.component.html',
  styleUrls: ['./nav-links.component.css']
})
export class NavLinksComponent implements OnInit {
  @Input() suffix: string;
  @ViewChild('childrenMenu', {static: true}) childrenMenu: TemplateRef<any>;

  overlayRef: OverlayRef | null;
  sub: Subscription;
  
  menu: NavLink[];
  activeParentMenu = '';
  padClass = {
    'padding-top': 0
  };

  externalLinks = [];

  get isDev() { return this.devAdids.includes(this.userService.loggedInUser.adid) ? true : false }

  devAdids: string = "km967761,kh946640,vn370313,qi931353,tw299608,he240924,na157643,jrg7298,dlp0278,pferna01,vsrini01,dws1835,dsj4208,dx160495"

  constructor(private router: Router, public navLinkService: NavLinkService,
    public overlay: Overlay, public viewContainerRef: ViewContainerRef, public userService: UserService, public menuService: MenuService) { }

  ngOnInit() {
    if (localStorage.getItem('validUser') != 'false') {
      this.navLinkService.getMenu();
    } else {
      this.router.navigate(['/authorize']);
    }

    this.menuService.getExternalLinks().subscribe(data => {
      if(data) {
        let count = data.filter(x => x['prmtrNme'] == 'externalLinkKey');
        if(count.length == 0) {  // External key is not present. Links will not display
          return;
        }

        this.externalLinks = data.map(record => {
          if(record['prmtrValuTxt'].includes('|')) {
            let arr = record['prmtrValuTxt'].split('|');
            record['menu'] = arr[0];
            record['link'] = arr[1];
          }
          return record;
        });
      }
    })
  }

  getMenu() {
    this.navLinkService.getMenus().subscribe(res => {
      this.menu = res;
    });
  }

  clearActiveMenu() {
    if (this.activeParentMenu !== '') {
      const activeParentMenu = document.getElementById(this.activeParentMenu);
      activeParentMenu.className = "collapse list-unstyled ng-star-inserted";

      // Adjust page body
      const pageBodyElement = document.getElementById("pageBody");
      pageBodyElement.className = "";
    }
  }

  setParentActive(parent, e) {

    if (parent.children.length === 0) {
      this.removeActiveLinks()
      const currentElement = e['currentTarget'];
      currentElement.className = "ng-star-inserted active";

      const pageBodyElement = document.getElementById("pageBody");
      pageBodyElement.setAttribute("style", `padding-top:0px;`);
    }
  }

  removeActiveLinks() {
    // Remove all active class
    const activeLinks = document.getElementsByName("subMenu");
    for (let i = 0; i < activeLinks.length; i++) {
      activeLinks[i].className = "ng-star-inserted"
    }
    // Remove all active class
    const parentSubMenus = document.getElementsByName("parentMenu");
    for (let i = 0; i < parentSubMenus.length; i++) {
      parentSubMenus[i].className = "ng-star-inserted"
    }
  }

  setTopMenuPage(e, parent, event) {
    const pageBodyElement = document.getElementById("pageBody");
    if (parent !== null) {
      this.removeActiveLinks();

      // Set link to active after click
      const currentElement = event['currentTarget'];
      currentElement.className = "ng-star-inserted active";

      let parentMenuId = `${parent['menuNme'].replace(/\s/g, "-").toLowerCase()}-nav`;
      const parentMenu = document.getElementById(parentMenuId);

      // Set Parent menu to active
      parentMenu.parentNode['className'] = "ng-star-inserted active"

      if (this.activeParentMenu !== parentMenuId) {
        if (this.activeParentMenu !== '') {
          // Return class of old active menu
          const activeParentMenu = document.getElementById(this.activeParentMenu);
          activeParentMenu.className = "collapse list-unstyled ng-star-inserted";
        }

        // Set new Active Parent Menu
        this.activeParentMenu = parentMenuId;

        // Update class of active menu
        parentMenu.className = "list-unstyled ng-star-inserted show"
      }
      else {
        parentMenu.className = "list-unstyled ng-star-inserted show"
      }

      // Adjust page body
      const height = parentMenu.offsetHeight;
      pageBodyElement.setAttribute("style", `padding-top: ${height}px;`);
    }
    else {
      if (this.activeParentMenu !== '') {
        const activeParentMenu = document.getElementById(this.activeParentMenu);
        activeParentMenu.className = "collapse list-unstyled ng-star-inserted";
      }

      this.removeActiveLinks();
      // Set link to active after click
      const currentElement = event['currentTarget'];
      currentElement.className = "ng-star-inserted active";

      const pageBodyElement = document.getElementById("pageBody");
      pageBodyElement.setAttribute("style", `padding-top: 0px; min-height: 300px;`); // Added min-height for the footer to be seen on Admin Menu
    }

    if(e == 'external') {

    } else {
      this.router.navigateByUrl(e);
    }

    

    // collapse list-unstyled ng-star-inserted
    // list-unstyled ng-star-inserted collapse show
  }

  onRightClick(event: MouseEvent, children) {
    event.preventDefault();
    this.close();
    let x = event.clientX + 130; // move menu on the right side
    let y = event.clientY;

    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo({ x, y })
      .withPositions([
        {
          originX: 'end',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        }
      ]);

    this.overlayRef = this.overlay.create({
      positionStrategy,
      scrollStrategy: this.overlay.scrollStrategies.close()
    });

    this.overlayRef.attach(new TemplatePortal(this.childrenMenu, this.viewContainerRef, {
      $implicit: children
    }));

    this.sub = fromEvent<MouseEvent>(document, 'click')
      .pipe(
        filter(event => {
          const clickTarget = event.target as HTMLElement;
          return !!this.overlayRef && !this.overlayRef.overlayElement.contains(clickTarget);
        }),
        take(1)
      ).subscribe(() => this.close())

  }

  close() {
    this.sub && this.sub.unsubscribe();
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }
  
  openNewTab(children) {
    this.close();
    let url = location.href + children.srcPath;
    window.open(url);
  }

  openLink(url: string) {
    window.open(url, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
  }

}
