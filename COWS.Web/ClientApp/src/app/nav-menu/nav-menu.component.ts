import { Component } from '@angular/core';
import { NavLinksComponent } from "./../nav-links/nav-links.component";
import { MenuService, UserService } from '../../services';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {


  constructor(public menuService: MenuService, public userService: UserService) { }


  //isExpanded = false;

  //collapse() {
  //  this.isExpanded = false;
  //}

  //toggle() {
  //  this.isExpanded = !this.isExpanded;
  //}
}
