export class Ccd {
  id: number;
  orderId: number;
  orderIds: string;
  ccdReasons: string;
  notes: string;
  ccdDate: Date;

  constructor();
  constructor(id?: number, orderId?: number, orderIds?: string,
    ccdReasons?: string, notes?: string, ccdDate?: Date) {
    this.id = id || 0;
    this.orderId = orderId || 0;
    this.orderIds = orderIds;
    this.ccdReasons = ccdReasons;
    this.notes = notes;
    this.ccdDate = ccdDate || new Date();
  }
}
