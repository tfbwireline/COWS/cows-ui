export class CptProvision {
  cptPrvsnId: number;
  cptId: number;
  cptPrvsnTypeId: number;
  cptPrvsnType: string;
  prvsnStusCd: boolean;
  prvsnStus: string;
  prvsnNteTxt: string;
  srvrNme: string;
  creatDt: Date;
  creatByUserId: number;
  modfdDt?: Date;
  modfdByUserId?: number;
  recStusId: number;
  recStus: string;

  constructor();
  constructor(cptPrvsnId?: number, cptId?: number, cptPrvsnTypeId?: number,
    cptPrvsnType?: string, prvsnStusCd?: boolean, prvsnStus?: string,
    prvsnNteTxt?: string, srvrNme?: string, creatDt?: Date, creatByUserId?: number,
    modfdDt?: Date, modfdByUserId?: number, recStusId?: number, recStus?: string
  ) {
    this.cptPrvsnId = cptPrvsnId || 0;
    this.cptId = cptId || 0;
    this.cptPrvsnTypeId = cptPrvsnTypeId || 0;
    this.cptPrvsnType = cptPrvsnType;
    this.prvsnStusCd = prvsnStusCd || false;
    this.prvsnStus = prvsnStus;
    this.prvsnNteTxt = prvsnNteTxt;
    this.srvrNme = srvrNme;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || null;
    this.recStusId = recStusId || 0;
    this.recStus = recStus;
  }
}
