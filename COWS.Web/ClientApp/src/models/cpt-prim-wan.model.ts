export class CptPrimWan {
  cptPrimWanId: number;
  cptId: number;
  cptPlnSrvcTierId: number;
  cptPrimScndyTprtId: number;
  qty?: number;
  creatDt: Date;

  constructor();
  constructor(cptPrimWanId?: number, cptId?: number, cptPlnSrvcTierId?: number,
    cptPrimScndyTprtId?: number, qty?: number, creatDt?: Date
  ) {
    this.cptPrimWanId = cptPrimWanId || 0;
    this.cptId = cptId || 0;
    this.cptPlnSrvcTierId = cptPlnSrvcTierId || null;
    this.cptPrimScndyTprtId = cptPrimScndyTprtId;
    this.qty = qty || null;
    this.creatDt = creatDt || new Date();
  }
}
