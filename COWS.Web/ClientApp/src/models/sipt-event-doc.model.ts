export class SiptEventDoc {
  siptEventDocId: number;
  eventId: number;
  fileNme: string;
  fileCntnt: Int8Array;
  fileSizeQty: number;
  creatDt: Date;
  creatByUserId: number;
  creatByAdId: string;
  recStusId: number;
  base64string: string;

  constructor();
  constructor(siptEventDocId?: number, eventId?: number, fileNme?: string,
    fileCntnt?: Int8Array, fileSizeQty?: number, creatDt?: Date,
    creatByUserId?: number, creatByAdId?: string, recStusId?: number, base64string?: string
  ) {
    this.siptEventDocId = siptEventDocId || 0;
    this.eventId = eventId || 0;
    this.fileNme = fileNme;
    this.fileCntnt = fileCntnt || null;
    this.fileSizeQty = fileSizeQty || 0;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.creatByAdId = creatByAdId;
    this.recStusId = recStusId || 0;
    this.base64string = base64string || null;
  }
}
