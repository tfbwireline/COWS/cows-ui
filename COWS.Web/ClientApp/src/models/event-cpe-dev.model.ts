
export class EventCpeDev {
  eventCpeDevId: number;
  cpeOrdrId: string;
  cpeOrdrNbr: string;
  ordrId: number;
  eventId: number;
  deviceId: string;
  assocH6: string;
  maintVndrPo: string;
  reqstnNbr: string;
  ccd: Date;
  cmpntStus: string;
  rcptStus: string;
  recStusId: number;
  odieCd: boolean;
  odieDevNme: string;
  ordrStus: string;
  creatDt: Date;

  constructor();
  constructor(eventCpeDevId?: number, cpeOrdrId?: string, cpeOrdrNbr?: string,
    ordrId?: number, eventId?: number, deviceId?: string, assocH6?: string, maintVndrPo?: string,
    reqstnNbr?: string, ccd?: Date, cmpntStus?: string, rcptStus?: string, odieDevNme?: string,
    ordrStus?: string, recStusId?: number, odieCd?: boolean, creatDt?: Date) {
    this.eventCpeDevId = eventCpeDevId || 0;
    this.cpeOrdrId = cpeOrdrId;
    this.cpeOrdrNbr = cpeOrdrNbr;
    this.ordrId = ordrId || 0;
    this.eventId = eventId || 0;
    this.deviceId = deviceId;
    this.assocH6 = assocH6;
    this.maintVndrPo = maintVndrPo;
    this.reqstnNbr = reqstnNbr;
    this.ccd = ccd || null;
    this.cmpntStus = cmpntStus;
    this.rcptStus = rcptStus;
    this.recStusId = recStusId || 0;
    this.odieCd = odieCd || false;
    this.odieDevNme = odieDevNme;
    this.ordrStus = ordrStus;
    this.creatDt = creatDt || new Date();
  }
}
