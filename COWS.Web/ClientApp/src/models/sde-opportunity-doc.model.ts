import { BrowserDynamicTestingModule } from "@angular/platform-browser-dynamic/testing"

export class SDEOpportunityDoc {
  index: number; // For grid purpose
  sdeDocID: number;
  fileName: string;
  file: any; // For front end func.
  fileContent: any;
  base64string: string;
  fileSizeQuantity: any;
  fileSizeQuantityDisp: string; // Display purpose
  createdDateTime: Date;
  recStusId: number;
  constructor();
  constructor(index?: number, sdeDocID?: number,fileName?: string, file?: any,fileContent?: any, base64string?: string, fileSizeQuantity?: any ,fileSizeQuantityDisp?: string, createdDateTime?: Date, recStusId?: number)
  {
    this.index = index || 0;
    this.sdeDocID = sdeDocID || 0;
    this.fileName = fileName || "";
    this.file = file || null;
    this.fileContent = fileContent || null;
    this.base64string = base64string || "";
    this.fileSizeQuantity = fileSizeQuantity || null;
    this.fileSizeQuantityDisp = fileSizeQuantityDisp || "";
    this.createdDateTime = createdDateTime || null;
    this.recStusId = recStusId || 0;
  }
}
