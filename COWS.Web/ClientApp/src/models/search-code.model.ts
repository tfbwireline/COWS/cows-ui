import { SearchTypes } from "../app/search/search.criteria";

export class SearchCode {
  codes: number[] = [];
  allowSearch: boolean = false;
  allowEventSearch: boolean = false;
  allowOrderSearch: boolean = false;
  allowRedesignSearch: boolean = false;
  isCandUser: boolean = false;
  isOrderUser: boolean = false;
  types: SearchTypes[] = [];

  constructor();
  constructor(allowEventSearch?: boolean, allowOrderSearch?: boolean, allowRedesignSearch?: boolean,
    isCandUser?: boolean, isOrderUser?: boolean, allowSearch?: boolean, codes?: number[],
    types?: SearchTypes[]) {
    this.allowEventSearch = allowEventSearch || false;
    this.allowOrderSearch = allowOrderSearch || false;
    this.allowRedesignSearch = allowRedesignSearch || false;
    this.isCandUser = isCandUser || false;
    this.isOrderUser = isOrderUser || false;
    this.codes = codes || [];
    this.allowSearch = allowSearch || false;
    this.types = types || [];
  }
}
