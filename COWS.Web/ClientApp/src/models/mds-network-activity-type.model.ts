
export class MDSNetworkActivityType {
  ntwkActyTypeId: number;
  ntwkActyTypeCd: string;
  ntwkActyTypeDes: string;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  modfdDt: Date;
  disabled: boolean;

  constructor();
  constructor(ntwkActyTypeId?: number, ntwkActyTypeCd?: string, ntwkActyTypeDes?: string,
    recStusId?: number, recStatus?: boolean, creatDt?: Date, modfdDt?: Date, disabled?: boolean) {
    this.ntwkActyTypeId = ntwkActyTypeId || 0;
    this.ntwkActyTypeCd = ntwkActyTypeCd;
    this.ntwkActyTypeDes = ntwkActyTypeDes;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;
    this.modfdDt = modfdDt || null;
    this.disabled = disabled || false;
  }
}
