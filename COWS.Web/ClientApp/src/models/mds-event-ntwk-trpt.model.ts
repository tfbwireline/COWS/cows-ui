
export class MDSEventNtwkTrpt {
  mdsEventNtwkTrptId: number;
  eventId: number;
  mdsTrnsprtType: string;
  lecPrvdrNme: string;
  lecCntctInfo: string;
  mach5OrdrNbr: string;
  ipNuaAdr: string;
  mplsAccsBdwd: string;
  scaNbr: string;
  taggdCd: boolean;
  vlanNbr: string;
  multiVrfReq: string;
  ipVer: string;
  locCity: string;
  locSttPrvn: string;
  locCtry: string;  
  creatDt: Date;
  recStusId: number;
  modfdDt: Date;
  assocH6: string;
  ddAprvlNbr: string;
  //salsEngrPhn: string;
  //salsEngrEmail: string;
  plNbr: string;
  ceSrvcId: string;
  nidHostName: string;
  nidSerialNbr: string;
  nidIpAdr: string;
  vasType: string; // ONLOAD RETURN
  vasTypeDes: string; // ON SEARCH RETURN
  guid: string;
  bdwdNme: string;
  isNidHostNameNative: boolean;
  isNidSerialNbrNative: boolean;

  constructor();
  constructor(mdsEventNtwkTrptId?: number, eventId?: number, mdsTrnsprtType?: string, lecPrvdrNme?: string, lecCntctInfo?: string, mach5OrdrNbr?: string,
    ipNuaAdr?: string, mplsAccsBdwd?: string, scaNbr?: string, taggdCd?: boolean,
    vlanNbr?: string, multiVrfReq?: string, ipVer?: string, locCity?: string,
    locSttPrvn?: string, locCtry?: string, creatDt?: Date, recStusId?: number, modfdDt?: Date, assocH6?: string, ddAprvlNbr?: string
    , plNbr?: string, ceSrvcId?: string, nidHostName?: string
    , nidSerialNbr?: string, nidIpAdr?: string, vasType?: string, vasTypeDes?: string, guid?: string, bdwdNme?: string) {
    this.mdsEventNtwkTrptId = mdsEventNtwkTrptId || 0;
    this.eventId = eventId || 0;
    this.mdsTrnsprtType = mdsTrnsprtType;
    this.lecPrvdrNme = lecPrvdrNme;
    this.lecCntctInfo = lecCntctInfo;
    this.mach5OrdrNbr = mach5OrdrNbr;
    this.ipNuaAdr = ipNuaAdr;
    this.mplsAccsBdwd = mplsAccsBdwd || null;
    this.scaNbr = scaNbr;
    this.taggdCd = taggdCd;
    this.vlanNbr = vlanNbr;
    this.multiVrfReq = multiVrfReq;
    this.ipVer = ipVer || null;
    this.locCity = locCity;
    this.locSttPrvn = locSttPrvn;
    this.locCtry = locCtry || "";
    this.creatDt = creatDt || null;
    this.recStusId = recStusId || 1;
    this.modfdDt = modfdDt || null;
    this.assocH6 = assocH6 || null;
    this.ddAprvlNbr = ddAprvlNbr || null;
    //this.salsEngrPhn = salsEngrPhn || null;
    //this.salsEngrEmail = salsEngrEmail || null;
    this.plNbr = plNbr || null;
    this.ceSrvcId = ceSrvcId || null;
    this.nidHostName = nidHostName || null;
    this.nidSerialNbr = nidSerialNbr || null;
    this.nidIpAdr = nidIpAdr || null;
    this.vasType = vasType || null;
    this.vasTypeDes = vasTypeDes || null,
    this.guid = guid || null;
    this.bdwdNme = bdwdNme || null;
  }
}
