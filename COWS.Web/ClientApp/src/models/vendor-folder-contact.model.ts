export class VendorFolderContact {
  cntctId: number;
  vndrFoldrId: number;
  cntctFrstNme: string;
  cntctLstNme: string;
  cntctEmailAdr: string;
  cntctProdTypeTxt: string;
  cntctProdRoleTxt: string;
  creatDt: Date;
  cntctPhnNbr: string;

  constructor();
  constructor(cntctId?: number, vndrFoldrId?: number, cntctFrstNme?: string,
    cntctLstNme?: string, cntctEmailAdr?: string, cntctProdTypeTxt?: string,
    cntctProdRoleTxt?: string, creatDt?: Date, cntctPhnNbr?: string) {
    this.cntctId = cntctId || 0;
    this.vndrFoldrId = vndrFoldrId || 0;
    this.cntctFrstNme = cntctFrstNme;
    this.cntctLstNme = cntctLstNme;
    this.cntctEmailAdr = cntctEmailAdr;
    this.cntctProdTypeTxt = cntctProdTypeTxt;
    this.cntctProdRoleTxt = cntctProdRoleTxt;
    this.creatDt = creatDt || new Date();
    this.cntctPhnNbr = cntctPhnNbr;    
  }
}
