export class EventHistory {
  eventHistId: number
  eventId: number
  actnId: number
  actnDes: string
  performedBy: string
  cmntTxt: string
  preCfgCmpltCd: string
  failReasId: number
  fsaMdsEventId: number
  creatDt: Date
  eventStrtTmst: Date
  eventEndTmst: Date

  constructor();
  constructor(eventHistId?: number, eventId?: number, actnId?: number, actnDes?: string, performedBy?: string,
    cmntTxt?: string, preCfgCmpltCd?: string,
    failReasId?: number, fsaMdsEventId?: number, creatDt?: Date, eventStrtTmst?: Date,
    eventEndTmst?: Date) {
    this.eventHistId = eventHistId || 0;
    this.eventId = eventId || 0;
    this.actnId = actnId || 0;
    this.actnDes = actnDes || null;
    this.performedBy = performedBy || null;
    this.cmntTxt = cmntTxt || null;
    this.preCfgCmpltCd = preCfgCmpltCd || null;
    this.failReasId = failReasId || null;
    this.fsaMdsEventId = fsaMdsEventId || 0;
    this.creatDt = creatDt || null;
    this.eventStrtTmst = eventStrtTmst || null;
    this.eventEndTmst = eventEndTmst || null;
  }
}
