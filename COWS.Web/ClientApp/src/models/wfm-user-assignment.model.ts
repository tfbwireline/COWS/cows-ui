
export class WfmUserAssignments {
  constructor(public userId?: number, public grpId?: number, public roleList?: string, public orderActionList?: string, public prodPlatList?: string
    , public wfmAsmtLvlId?: number, public countryOrigList?: string, public usrPrfId?: number) {
  }
}

export interface IWfmUserAssignmentsResponse {
  map(arg0: (wfm: any) => WfmUserAssignments): any;
  results: WfmUserAssignments[];
}
