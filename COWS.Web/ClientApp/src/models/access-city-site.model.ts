
export class AccessCitySite {
  accsCtySiteId: number;
  accsCtyNmeSiteCd: string;
  creatDt: Date;

  constructor();
  constructor(accsCtySiteId?: number, accsCtyNmeSiteCd?: string, creatDt?: Date) {
    this.accsCtySiteId = accsCtySiteId || 0;
    this.accsCtyNmeSiteCd = accsCtyNmeSiteCd;
    this.creatDt = creatDt || null;
  }
}
