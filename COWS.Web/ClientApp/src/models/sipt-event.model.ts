import { SiptRltdOrdr } from "./sipt-rltd-ordr.model";
import { SiptEventDoc } from "./sipt-event-doc.model";

//import { AdEventAccessTag } from './ad-event-access-tag.model'

export class SiptEvent {
  eventId: number;
  m5OrdrNbr: string;
  h1: string;
  siteId: string;
  h6: string;
  eventCsgLvlId: number;
  charsId: string;
  teamPdlNme: string;
  siptDocLocTxt: string;
  custReqStDt: any;
  custReqEndDt: any;
  wrkDes: string;
  reqorUserId: number;
  reqorUserName: string;
  reqorUserPhone: string;
  ntwkEngrId: number;
  ntwkTechEngrId: number;
  pmId: number;
  pmName: string;
  pmPhone: string;
  pmEmail: string;
  eventStusId: number;
  wrkflwStusId: number;
  creatByUserId: number;
  modfdByUserId: number;
  modfdDt: Date;
  creatDt: Date;
  recStusId: number;
  recStatus: boolean;
  siptProdTypeId: number;
  prjId: string;
  focDt: any;
  usIntlCd: string;
  siptDesgnDoc: string;
  streetAdr: string;
  eventTitleTxt: string;
  custNme: string;
  siteCntctNme: string;
  siteCntctPhnNbr: string;
  siteCntctHrNme: string;
  siteCntctEmailAdr: string;
  siteAdr: string;
  flrBldgNme: string;
  ctyNme: string;
  sttPrvnNme: string;
  ctryRgnNme: string;
  zipCd: string;
  createdByAdId: string;
  modifiedByAdId: string;
  createdByDisplayName: string;
  modifiedByDisplayName: string;
  tnsTgCd?: boolean;
  siptEventActyIds: number[];
  siptEventTollTypeIds: number[];
  siptReltdOrdrIds: number[];
  siptReltdOrdrNos: string[];
  siptReltdOrdr: SiptRltdOrdr[];
  siptEventDoc: SiptEventDoc[];
  reviewCmntTxt: string;

  isLocked: boolean;
  lockedBy: string;

  constructor();
  constructor(
    eventId?: number, m5OrdrNbr?: string, h1?: string, siteId?: string, h6?: string,
    charsId?: string, teamPdlNme?: string, siptDocLocTxt?: string, custReqStDt?: Date,
    eventCsgLvlId?: number, custReqEndDt?: Date, wrkDes?: string, reqorUserId?: number,
    reqorUserName?: string, reqorUserPhone?: string, isLocked?: boolean, lockedBy?: string,
    ntwkEngrId?: number, ntwkTechEngrId?: number, pmId?: number, pmName?: string,
    pmEmail?: string, pmPhone?: string, eventStusId?: number, wrkflwStusId?: number,
    creatByUserId?: number, creatDt?: Date, createdByAdId?: string, createdByDisplayName?: string,
    modfdByUserId?: number, modfdDt?: Date, modifiedByAdId?: string, modifiedByDisplayName?: string,
    recStusId?: number, recStatus?: boolean, siptProdTypeId?: number, prjId?: string,
    focDt?: Date, usIntlCd?: string, siptDesgnDoc?: string, streetAdr?: string,
    eventTitleTxt?: string, custNme?: string, siteCntctNme?: string, siteCntctPhnNbr?: string,
    siteCntctHrNme?: string, siteCntctEmailAdr?: string, siteAdr?: string, flrBldgNme?: string,
    ctyNme?: string, sttPrvnNme?: string, ctryRgnNme?: string, zipCd?: string,           
    siptEventActyIds?: number[], siptEventTollTypeIds?: number[],
    siptReltdOrdrIds?: number[], siptReltdOrdrNos?: string[], siptReltdOrdr?: SiptRltdOrdr[],
    siptEventDoc?: SiptEventDoc[], reviewCmntTxt?: string, tnsTgCd?: boolean
  ) {
    this.eventId = eventId || 0;
    this.m5OrdrNbr = m5OrdrNbr;
    this.h1 = h1;
    this.siteId = siteId;
    this.h6 = h6;
    this.eventCsgLvlId = eventCsgLvlId || 0;
    this.charsId = charsId;
    this.teamPdlNme = teamPdlNme;
    this.siptDocLocTxt = siptDocLocTxt;
    this.custReqStDt = custReqStDt || new Date();
    this.custReqEndDt = custReqEndDt || new Date();
    this.wrkDes = wrkDes;
    this.reqorUserId = reqorUserId || 0;
    this.reqorUserName = reqorUserName;
    this.reqorUserPhone = reqorUserPhone;
    this.ntwkEngrId = ntwkEngrId || 0;
    this.ntwkTechEngrId = ntwkTechEngrId || 0;
    this.pmId = pmId || 0;
    this.pmName = pmName;
    this.pmEmail = pmEmail;
    this.pmPhone = pmPhone;
    this.eventStusId = eventStusId || 0;
    this.wrkflwStusId = wrkflwStusId || 0;
    this.creatByUserId = creatByUserId || 0;
    this.modfdByUserId = modfdByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.creatDt = creatDt || new Date();
    this.recStusId = recStusId || 0;
    this.recStatus = recStatus || true;
    this.siptProdTypeId = siptProdTypeId || 0;
    this.prjId = prjId;
    this.focDt = focDt || new Date();
    this.usIntlCd = usIntlCd;
    this.siptDesgnDoc = siptDesgnDoc;
    this.streetAdr = streetAdr;
    this.siptDesgnDoc = siptDesgnDoc;
    this.eventTitleTxt = eventTitleTxt;
    this.custNme = custNme;
    this.siteCntctNme = siteCntctNme;
    this.siteCntctEmailAdr = siteCntctEmailAdr;
    this.siteCntctHrNme = siteCntctHrNme;
    this.siteCntctPhnNbr = siteCntctPhnNbr;
    this.siteAdr = siteAdr;
    this.flrBldgNme = flrBldgNme;
    this.ctyNme = ctyNme;
    this.sttPrvnNme = sttPrvnNme;
    this.ctryRgnNme = ctryRgnNme;
    this.zipCd = zipCd;
    this.createdByAdId = createdByAdId;
    this.modifiedByAdId = modifiedByAdId;
    this.createdByDisplayName = createdByDisplayName;
    this.modifiedByDisplayName = modifiedByDisplayName;
    this.siptEventActyIds = siptEventActyIds || [];
    this.siptEventTollTypeIds = siptEventTollTypeIds || [];
    this.siptReltdOrdrIds = siptReltdOrdrIds || [];
    this.siptReltdOrdrNos = siptReltdOrdrNos || [];
    this.siptReltdOrdr = siptReltdOrdr || [];
    this.siptEventDoc = siptEventDoc || [];
    this.reviewCmntTxt = reviewCmntTxt;
    this.tnsTgCd = tnsTgCd || null;
    this.isLocked = isLocked || false;
    this.lockedBy = lockedBy;
  }
}
