
export class PlatForm {
  pltfrmCd: string;
  pltfrmNme: string;
  creatDt: Date;


  constructor();
  constructor(pltfrmCd?: string, pltfrmNme?: string, creatDt?: Date) {
    this.pltfrmCd = pltfrmCd ;
    this.pltfrmNme = pltfrmNme;
    this.creatDt = creatDt || null;

  }
}
