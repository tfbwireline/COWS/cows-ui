export class DocEntity {
  docId: number;
  id: number;
  fileNme: string;
  fileCntnt: Int8Array;
  fileSizeQty: number;
  cmntTxt: string;
  creatDt: Date;
  creatByUserId: number;
  creatByUserAdId: string;
  modfdDt?: Date
  modfdByUserId?: number;
  modfdByUserAdId?: string;
  recStusId: number;
  base64string: string;
  fileSizeQtyStr: string;

  //constructor();
  constructor({ docId = 0, id = 0, fileNme = "", fileCntnt = null, fileSizeQty = 0, fileSizeQtyStr = "",
    cmntTxt = "", creatDt = new Date(), modfdDt = null, creatByUserId = 0,
    creatByUserAdId = "", modfdByUserId = 0, modfdByUserAdId = "",
    recStusId = 1, base64string = null })
  //constructor(docId?: number, id?: number, fileNme?: string, fileCntnt?: Int8Array,
  //  fileSizeQty?: number, cmntTxt?: string, creatDt?: Date, modfdDt?: Date,
  //  creatByUserId?: number, creatByUserAdId?: string, modfdByUserId?: number,
  //  modfdByUserAdId?: string, recStusId?: number, base64string?: string)
  {
    this.docId = docId || 0;
    this.id = id || 0;
    this.fileNme = fileNme;
    this.fileCntnt = fileCntnt || null;
    this.fileSizeQty = fileSizeQty || 0;
    this.fileSizeQtyStr = fileSizeQtyStr || ""
    this.cmntTxt = cmntTxt;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.creatByUserAdId = creatByUserAdId;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || null;
    this.modfdByUserAdId = modfdByUserAdId;
    this.recStusId = recStusId || 0;
    this.base64string = base64string || null;
  }
}
