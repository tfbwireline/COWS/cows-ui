export class MdsEventOdieDevice {
  mdsEventOdieDevId: number;
  eventId: number;
  rdsnNbr: string;
  rdsnExpDt: Date;
  odieDevNme: string;
  fastTrkCd: any;
  redsgnDevId: number;
  devModelId: number;
  frwlProdCd: string;
  optoutCd: boolean;
  intlCtryCd: string;
  phnNbr: string;
  woobCd: string;
  srvcAssrnSiteSuppId: number;
  manfId: number;
  creatDt: Date;
  sysCd: boolean;
  scCd: string;
  woobIpAddress:string;

  // I used deconstruction due to multiple type mismatch and default values
  constructor({ mdsEventOdieDevId = 0, eventId = 0, rdsnNbr = "", rdsnExpDt = new Date(), odieDevNme = "", redsgnDevId = 0, devModelId = null,
    manfId = null, creatDt = new Date(), fastTrkCd = null, frwlProdCd = null, optoutCd = false, intlCtryCd = "", phnNbr = null, woobCd = null,
    srvcAssrnSiteSuppId = null, sysCd = (mdsEventOdieDevId == 0) ? true : false, scCd = "" ,woobIpAddress = ""}) {
    this.mdsEventOdieDevId = mdsEventOdieDevId
    this.eventId = eventId
    this.rdsnNbr = rdsnNbr;
    this.rdsnExpDt = rdsnExpDt
    this.odieDevNme = odieDevNme;
    this.fastTrkCd = (fastTrkCd == true || fastTrkCd == "Y") ? "Y" : "N";
    this.redsgnDevId = redsgnDevId
    this.devModelId = devModelId
    this.manfId = manfId
    this.creatDt = creatDt
    this.frwlProdCd = frwlProdCd
    this.optoutCd = optoutCd
    this.intlCtryCd = intlCtryCd
    this.phnNbr = phnNbr
    this.woobCd = woobCd
    this.srvcAssrnSiteSuppId = srvcAssrnSiteSuppId
    this.sysCd = sysCd
    this.scCd = scCd
    this.woobIpAddress = woobIpAddress
  }
}
