export class RedesignDoc {
  redsgnDocId: number;
  redsgnId: number;
  fileNme: string;
  fileCntnt: Int8Array;
  fileSizeQty: number;
  creatDt: Date;
  creatByUserId: number;
  creatByUserName: string;
  modfdDt: Date;
  modfdByUserId: number;
  recStusId: number;
  redsgnDevStusId: number;
  base64string: string;

  constructor();
  constructor(redsgnDocId?: number, redsgnId?: number, fileNme?: string, fileCntnt?: Int8Array,
    fileSizeQty?: number, creatDt?: Date, creatByUserId?: number, creatByUserName?: string,
    modfdDt?: Date, modfdByUserId?: number, recStusId?: number, redsgnDevStusId?: number,
    base64string?: string) {
    this.redsgnDocId = redsgnDocId || 0;
    this.redsgnId = redsgnId || 0;
    this.fileNme = fileNme;
    this.fileCntnt = fileCntnt || null;
    this.fileSizeQty = fileSizeQty || 0;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.creatByUserName = creatByUserName;
    this.modfdDt = modfdDt;
    this.modfdByUserId = modfdByUserId || 0;
    this.recStusId = recStusId;
    this.redsgnDevStusId = redsgnDevStusId || 0;
    this.base64string = base64string;
  }
}
