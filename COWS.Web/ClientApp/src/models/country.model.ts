
export class Country {
  ctryNme: string;
  ctryCd: string;
  countryRegion: string;
  rgnId: number;
  l2pCtryCd: string;
  isdCd: string;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;
  modfdDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;

  constructor({ ctryNme = '', ctryCd = '', countryRegion = null, rgnId = null,
    l2pCtryCd = null, isdCd = null, recStusId = null, recStatus = null,
    creatDt = null, creatByUserId = null, createdByAdId = null,
    modfdDt = null, modfdByUserId = null, modifiedByAdId = null }) {
    this.ctryNme = ctryNme;
    this.ctryCd = ctryCd;
    this.countryRegion = countryRegion;
    this.rgnId = rgnId;
    this.l2pCtryCd = l2pCtryCd;
    this.isdCd = isdCd;
    this.recStusId = recStusId;
    this.recStatus = recStatus;
    this.creatDt = creatDt;
    this.creatByUserId = creatByUserId;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt;
    this.modfdByUserId = modfdByUserId;
    this.modifiedByAdId = modifiedByAdId;
  }
}
