export class UcaasEventBilling {
  ucaaSEventBillingId: number;
  eventId: number;
  ucaaSBillActyId: number;
  ucaaSBillActyDes: string;
  ucaaSBicType: string;
  mrcNrcCd: string;
  ucaaSPlanTypeId: number;
  incQty: number;
  dcrQty: number;

  constructor(ucaaSEventBillingId?: number, eventId?: number, ucaaSBillActyId?: number,
    ucaaSBillActyDes?: string, ucaaSBicType?: string, mrcNrcCd?: string,
    ucaaSPlanTypeId?: number, incQty?: number, dcrQty?: number
  ) {
    this.ucaaSEventBillingId = ucaaSEventBillingId || 0;
    this.eventId = eventId || 0;
    this.ucaaSBillActyId = ucaaSBillActyId || 0;
    this.ucaaSBillActyDes = ucaaSBillActyDes;
    this.ucaaSBicType = ucaaSBicType;
    this.mrcNrcCd = mrcNrcCd;
    this.ucaaSPlanTypeId = ucaaSPlanTypeId || 0;
    this.incQty = incQty || 0;
    this.dcrQty = dcrQty;
  }
}
