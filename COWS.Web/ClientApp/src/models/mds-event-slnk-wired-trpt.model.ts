
export class MDSEventSlnkWiredTrpt {
  slnkWiredTrptId: number|string;
  eventId: number;
  mdsTrnsprtType: string;
  primBkupCd: string;
  vndrPrvdrTrptCktId: string;
  vndrPrvdrNme: string;
  bdwdChnlNme: string;
  plNbr: string;
  ipNuaAdr: string;
  oldCktId: string;
  multiLinkCktCd: string;
  dedAccsCd: string;
  sprintMngdCd: string;
  odieDevNme: string;
  creatDt: Date;
  fmsNbr: string;
  dlciVpiVci: string;
  vlanNbr: string;
  spaNua: string;
  recStusId: number;
  modfdDt: Date;
  readyBeginCd: string;
  optInCktCd: string;
  optInHCd: string;
  h6: string;
  readyBeginCdBoolean: boolean;
  optInCktCdBoolean: boolean;
  optInHCdBoolean: boolean;
  isFromLookup: boolean;
  isNew: boolean;

  //constructor();
  constructor({ slnkWiredTrptId = 0, eventId = 0, mdsTrnsprtType = "", primBkupCd = "P", vndrPrvdrTrptCktId = null, vndrPrvdrNme = null,
    bdwdChnlNme = null, plNbr = null, ipNuaAdr = null, oldCktId = null, multiLinkCktCd = null, dedAccsCd = null, sprintMngdCd = "S",
    odieDevNme = "", creatDt = new Date(), fmsNbr = null, dlciVpiVci = null, vlanNbr = null, spaNua = null, recStusId = 0, modfdDt = null,
    readyBeginCd = null, optInCktCd = null, optInHCd = null, h6 = "", isFromLookup = false, isNew = false }) {
    this.slnkWiredTrptId = slnkWiredTrptId || 0;
    this.eventId = eventId || 0;
    this.mdsTrnsprtType = mdsTrnsprtType;
    this.primBkupCd = primBkupCd;
    this.vndrPrvdrTrptCktId = vndrPrvdrTrptCktId;
    this.vndrPrvdrNme = vndrPrvdrNme;
    this.bdwdChnlNme = bdwdChnlNme;
    this.plNbr = plNbr;
    this.ipNuaAdr = ipNuaAdr;
    this.oldCktId = oldCktId;
    this.multiLinkCktCd = multiLinkCktCd;
    this.dedAccsCd = dedAccsCd;
    this.sprintMngdCd = sprintMngdCd;
    this.odieDevNme = odieDevNme;
    this.creatDt = creatDt || null;
    this.fmsNbr = fmsNbr;
    this.dlciVpiVci = dlciVpiVci;
    this.vlanNbr = vlanNbr;
    this.spaNua = spaNua;
    this.recStusId = recStusId || 1;
    this.modfdDt = modfdDt || null;
    this.readyBeginCd = readyBeginCd;
    this.optInCktCd = optInCktCd;
    this.optInHCd = optInHCd;
    this.h6 = h6;
    this.isFromLookup = isFromLookup || false
    this.isNew = isNew || false
  }
}
