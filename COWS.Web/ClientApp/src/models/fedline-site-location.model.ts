
export class FedlineSiteLocation {

  siteAddress1: string
  siteAddress2: string
  city: string
  stateProv: string
  country: string
  zip: string
  primaryPocName: string
  primaryPocPhone: string
  primaryPocPhoneExtension: string
  primaryPocEmail: string
  secondaryPocName: string
  secondaryPocPhone: string
  secondaryPocPhoneExtension: string
  secondaryPocEmail: string

  constructor();
  constructor(siteAddress1?: string, siteAddress2?: string, city?: string, stateProv?: string, country?: string,
    zip?: string, primaryPocName?: string, primaryPocPhone?: string, primaryPocPhoneExtension?: string, primaryPocEmail?: string,
    secondaryPocName?: string, secondaryPocPhone?: string, secondaryPocPhoneExtension?: string, secondaryPocEmail?: string) {
    this.siteAddress1 = siteAddress1 || null
    this.siteAddress2 = siteAddress2 || null
    this.city = city || null
    this.stateProv = stateProv || null
    this.country = country || null
    this.zip = zip || null
    this.primaryPocName = primaryPocName || null
    this.primaryPocPhone = primaryPocPhone || null
    this.primaryPocPhoneExtension = primaryPocPhoneExtension || null
    this.primaryPocEmail = primaryPocEmail || null
    this.secondaryPocName = secondaryPocName || null
    this.secondaryPocPhone = secondaryPocPhone || null
    this.secondaryPocPhoneExtension = secondaryPocPhoneExtension || null
    this.secondaryPocEmail = secondaryPocEmail || null
  }
}
