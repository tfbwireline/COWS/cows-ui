export class SDEOpportunityProduct {
  productTypeId:number
  productType:string
  productTypeDesc:string
  qty: number
  constructor();
  constructor(productTypeId?: number, productType?: string, productTypeDesc?: string, qty?: number) {
    this.productTypeId = productTypeId || 0;
    this.productType = productType || "";
    this.productTypeDesc = productTypeDesc || "";
    this.qty= qty||0
  }
}
