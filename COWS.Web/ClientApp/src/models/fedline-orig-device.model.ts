
export class FedlineOrigDevice {
  deviceName: string
  serialNum: string
  origReqId: number
  model: string

  constructor();
  constructor(deviceName?: string, serialNum?: string, origReqId?: number, model?: string) {
    this.deviceName = deviceName || null
    this.serialNum = serialNum || null
    this.origReqId = origReqId || 0
    this.model = model || null
  }
}
