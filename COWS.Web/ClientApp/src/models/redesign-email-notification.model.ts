export class RedesignEmailNotification {
  redsgnEmailNtfctnId: number;
  redsgnId: number;
  userIdntfctnEmail: string;
  recStusId: boolean;
  cretdUserId: number;
  cretdDt: Date;

  constructor();
  constructor(redsgnEmailNtfctnId?: number, redsgnId?: number, userIdntfctnEmail?: string,
    recStusId?: boolean, cretdUserId?: number, cretdDt?: Date) {
    this.redsgnEmailNtfctnId = redsgnEmailNtfctnId || 0;
    this.redsgnId = redsgnId || 0;
    this.userIdntfctnEmail = userIdntfctnEmail;
    this.recStusId = recStusId || true;
    this.cretdUserId = cretdUserId || 0;
    this.cretdDt = cretdDt || new Date();
  }
}
