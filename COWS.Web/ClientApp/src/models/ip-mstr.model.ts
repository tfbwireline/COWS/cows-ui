export class IpMstr {
    ipMstrId: number;
    ipAdr: string;
    recStusId: number;
  
    constructor();
    constructor(ipMstrId?: number, ipAdr?: string, recStusId?: number
        ) {
      this.ipMstrId = ipMstrId || 0;
      this.ipAdr = ipAdr || '';
      this.recStusId = recStusId || 0;
    }
  }