import { EntityBase } from "./entity-base.model";
import { NgvnEventCktIdNua } from './ngvn-event-ckt-id-nua.model';
import { NgvnEventSiptTrnk } from './ngvn-event-sipt-trnk.model';
import { User } from "./user.model";

export class NgvnEvent {
  eventId: number
  eventStusId: number
  ftn: string
  charsId: string
  h1: string
  h6: string
  ngvnProdTypeId: number  
  pubEmailCcTxt: string
  cmpltdEmailCcTxt: string
  dsgnCmntTxt: string
  gsrCfgrnDes: string
  cablAddDueDt: Date
  cablPrcolTypeNme: string
  sbcPairDes: string
  esclCd: boolean
  primReqDt: Date
  scndyReqDt: Date
  esclReasId: number
  strtTmst: Date
  extraDrtnTmeAmt: number
  endTmst: Date
  wrkflwStusId: number
  recStusId: number
  creatByUserId: number
  modfdByUserId: number
  modfdDt: Date
  creatDt: Date
  ipVerId: number
  cnfrcBrdgNbr: string
  cnfrcPinNbr: string
  eventDrtnInMinQty: number
  sowsEventId: number
  reltdCmpsNcrCd: boolean
  reltdCmpsNcrNme: string
  custNme: string
  custCntctNme: string
  custCntctPhnNbr: string
  custEmailAdr: string
  custCntctCellPhnNbr: string
  custCntctPgrNbr: string
  custCntctPgrPinNbr: string
 
  eventTitleTxt: string
  eventDes: string
  creatByUserDsplNme: string
  enhncSrvcNme: string
  eventStusDes: string
  wrkflwStusDes: string

  //LkUser CreatByUser
  //LkEnhncSrvc EnhncSrvc
  //LkEsclReas EsclReas
  //Event Event
  //LkEventStus EventStus
  //LkIpVer IpVer
  //LkUser ModfdByUser
  //LkRecStus RecStus
  //LkUser ReqorUser
  //LkUser SalsUser
  //LkWrkflwStus WrkflwStus
  ngvnEventCktIdNua: NgvnEventCktIdNua[]
  ngvnEventSipTrnk: NgvnEventSiptTrnk[]
  activators: number[]
  eventSucssActyIds: number[] = [];
  eventFailActyIds: number[] = [];
  //reviewCmntTxt: string;
  preCfgConfgCode: string;
  profile: string;
  failCode: number;
  activatorComments: string;
  reviewerComments: string;
  entityBase: EntityBase;
  reqorUserId: number  
  reqorUser: User;
  salsUserId: number
  salsUser: User;
  reviewerUserId: number;
  activatorUserId: number;

  constructor();
  constructor(eventId?: number, eventStusId?: number, ftn?: string, charsId?: string, h1?: string, h6?: string, ngvnProdTypeId?: number, reqorUserId?: number, salsUserId?: number,
    pubEmailCcTxt?: string, cmpltdEmailCcTxt?: string, dsgnCmntTxt?: string, gsrCfgrnDes?: string, cablAddDueDt?: Date, cablPrcolTypeNme?: string, sbcPairDes?: string, docLinkTxt?: string, esclCd?: boolean, primReqDt?: Date, scndyReqDt?: Date,
    esclReasId?: number, strtTmst?: Date, extraDrtnTmeAmt?: number, endTmst?: Date, wrkflwStusId?: number, recStusId?: number, creatByUserId?: number, modfdByUserId?: number,
    modfdDt?: Date, creatDt?: Date, ipVerId?: number, cnfrcBrdgNbr?: string, cnfrcPinNbr?: string, eventDrtnInMinQty?: number, sowsEventId?: number, reltdCmpsNcrCd?: boolean, reltdCmpsNcrNme?: string, custNme?: string,
    custCntctNme?: string, custCntctPhnNbr?: string, custEmailAdr?: string, custCntctCellPhnNbr?: string, custCntctPgrNbr?: string, custCntctPgrPinNbr?: string,
    eventTitleTxt?: string, eventDes?: string, creatByUserDsplNme?: string, enhncSrvcNme?: string, eventStusDes?: string, wrkflwStusDes?: string,
    activators?: number[], ngvnEventCktIdNua?: NgvnEventCktIdNua[], ngvnEventSipTrnk?: NgvnEventSiptTrnk[], activatorComments?: string, reviewerComments?: string,
    eventSucssActyIds?: number[], eventFailActyIds?: number[], preCfgConfgCode?: string, failCode?: number, profile?: string,
    entityBase?: EntityBase, reqorUser?: User, salsUser?: User, reviewerUserId?: number, activatorUserId?: number) {
    this.eventId = eventId || 0;
    this.eventTitleTxt = eventTitleTxt || null;
    this.eventDes = eventDes || null;
    this.eventStusId = eventStusId || 0;
    this.ngvnProdTypeId = ngvnProdTypeId || 0;
    this.ftn = ftn;
    this.charsId = charsId || "";
    this.h1 = h1 || null;
    this.h6 = h6 || "";
    this.custNme = custNme || "";
    this.custEmailAdr = custEmailAdr || "";
    this.custCntctPhnNbr = custCntctPhnNbr || "";
    this.custCntctCellPhnNbr = custCntctCellPhnNbr || "";
    this.custCntctNme = custCntctNme || "";
    this.custCntctPgrNbr = custCntctPgrNbr || "";
    this.custCntctPgrPinNbr = custCntctPgrPinNbr || "";
    this.reqorUserId = reqorUserId || 0;
    this.cnfrcBrdgNbr = cnfrcBrdgNbr || null;
    this.cnfrcPinNbr = cnfrcPinNbr || null;
    this.salsUserId = salsUserId || 0;
    this.pubEmailCcTxt = pubEmailCcTxt || "";
    this.cmpltdEmailCcTxt = cmpltdEmailCcTxt || "";
    this.dsgnCmntTxt = dsgnCmntTxt || "";
    this.gsrCfgrnDes = gsrCfgrnDes || "";
    this.cablAddDueDt = cablAddDueDt || new Date();
    this.cablPrcolTypeNme = cablPrcolTypeNme || "";
    this.sbcPairDes = sbcPairDes || "";
    
    this.ngvnEventCktIdNua = ngvnEventCktIdNua || [];
    this.ngvnEventSipTrnk = ngvnEventSipTrnk || [];
    this.esclCd = esclCd || false;
    this.esclReasId = esclReasId || 0;
    this.primReqDt = primReqDt || null;
    this.scndyReqDt = scndyReqDt || null;
    this.strtTmst = strtTmst || new Date();
    this.eventDrtnInMinQty = eventDrtnInMinQty || 60;
    this.extraDrtnTmeAmt = extraDrtnTmeAmt || 0;
    this.endTmst = endTmst || new Date();
    this.wrkflwStusId = wrkflwStusId || 0;

    this.recStusId = recStusId || 0;
    this.creatByUserId = creatByUserId || 0;
    this.creatDt = creatDt || new Date();
    this.modfdByUserId = modfdByUserId || null;
    this.modfdDt = modfdDt || new Date();

    this.ipVerId = ipVerId || null;
    this.sowsEventId = sowsEventId || null;
    this.reltdCmpsNcrCd = reltdCmpsNcrCd || false;
    this.reltdCmpsNcrNme = reltdCmpsNcrNme || null;
    this.creatByUserDsplNme = creatByUserDsplNme || null;
    this.enhncSrvcNme = enhncSrvcNme || null;
    this.eventStusDes = eventStusDes || null;
    this.wrkflwStusDes = wrkflwStusDes || null;

    this.activators = activators || [];
    this.activatorComments = activatorComments;
    this.reviewerComments = reviewerComments;

    this.eventSucssActyIds = eventSucssActyIds || [];
    this.eventFailActyIds = eventFailActyIds || [];
    this.profile = profile;
    this.preCfgConfgCode = preCfgConfgCode;
    this.failCode = failCode || 1;
    this.entityBase = entityBase || new EntityBase();
    this.reqorUser = reqorUser || null;
    this.salsUser = salsUser || null;
    this.activatorUserId = activatorUserId || 0;
    this.reviewerUserId = reviewerUserId || 0;
  }
}
