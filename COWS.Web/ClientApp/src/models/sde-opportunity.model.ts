import { EntityBase } from "./entity-base.model";
import { SDEOpportunityDoc } from "./sde-opportunity-doc.model";
import { SDEOpportunityNote } from "./sde-opportunity-note.model";
import { SDEOpportunityProduct } from "./sde-opportunity-product.model";
import { User } from "./user.model";
export class SdeOpportunity {
  sdeOpportunityId: number
  statusID: number
  status: string
  statusDesc: string
  companyName: string
  address: string
  region: string
  state: string
  city: string
  contactName: string
  contactEmail: string
  contactPhone: string
  saleType: string
  saleTypeDesc: string
  isRFP: boolean
  isWirelineCustomer: boolean
  isManagedServicesCustomer: boolean
  comments: string
  requestorLocation: string
  assignedTo: string
  assignedToId: number
  ccEmail: string
  createdDateTime: Date
  createdByFullName: string
  createdByUserId: number
  reqorUserId: number

  reqorUser: User;
  entityBase: EntityBase;

  sdeDocs: Array<SDEOpportunityDoc>
  sdeProducts: Array<SDEOpportunityProduct>
  sdeNotes: Array<SDEOpportunityNote>
  constructor();
  constructor(sdeOpportunityId?: number ,statusID?: number, status?: string, statusDesc?: string ,companyName?: string, address?: string, region?: string ,state?: string, city?: string,
  contactName?: string,contactEmail?: string, contactPhone?: string ,saleType?: string ,saleTypeDesc?: string ,isRFP?: boolean, isWirelineCustomer?: boolean, isManagedServicesCustomer?: boolean,
  comments?: string, requestorLocation?: string ,assignedTo?: string, assignedToId?: number, ccEmail?: string ,createdDateTime?: Date ,createdByFullName?: string, createdByUserId?: number,
    reqorUserId?: number, reqorUser?: User, entityBase?: EntityBase,sdeDocs?: Array<SDEOpportunityDoc>, sdeProducts?: Array<SDEOpportunityProduct>, sdeNotes?: Array<SDEOpportunityNote>) {
    this.sdeOpportunityId = sdeOpportunityId || 0;
    this.statusID = statusID || 0;
    this.status = status || "";
    this.statusDesc = statusDesc || "";
    this.companyName = companyName || "";
    this.address = address || "";
    this.region = region || "";
    this.state = state || "";
    this.city = city || "";
    this.contactName = contactName || "";
    this.contactEmail = contactEmail || "";
    this.contactPhone = contactPhone || "";
    this.saleType = saleType || "";
    this.saleTypeDesc = saleTypeDesc || "";
    this.isRFP = isRFP || false
    this.isWirelineCustomer = isWirelineCustomer || false
    this.isManagedServicesCustomer = isManagedServicesCustomer || false
    this.comments = comments || "";
    this.requestorLocation = requestorLocation || "";
    this.assignedTo = assignedTo || "";
    this.assignedToId = assignedToId || 0
    this.ccEmail = ccEmail || "";
    this.createdDateTime = createdDateTime|| new Date();
    this.createdByFullName = createdByFullName || "";
    this.createdByUserId = createdByUserId|| 0
    this.reqorUserId = reqorUserId || 0;
    this.reqorUser = reqorUser || null;
    this.entityBase = entityBase || new EntityBase();
    this.sdeDocs = sdeDocs || []
    this.sdeProducts = sdeProducts || []
    this.sdeNotes = sdeNotes || []
  }
}

