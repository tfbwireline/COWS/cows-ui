
export class CancelOrder {
  ftn: string;
  h5_H6: string;
  ordrType: string;
  productType: string;
  custName: string;
  ccd: string;
  notes: string;
  userId: number;

  constructor();
  constructor(ftn?: string, h5_H6?: string, ordrType?: string, productType?: string, custName?: string, ccd?: string, notes?: string,userId?: number) {
    this.ftn = ftn;
    this.h5_H6 = h5_H6;
    this.ordrType = ordrType;
    this.productType = productType;
    this.custName = custName ;
    this.ccd = ccd;
    this.notes = notes;
    this.userId = userId || 0;
  }
}
