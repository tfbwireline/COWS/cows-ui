import { EventCpeDev } from "./event-cpe-dev.model";

export class Event {
  eventId: number;
  eventCpeDev: EventCpeDev[];

  constructor();
  constructor(eventId?: number, eventCpeDev?: EventCpeDev[]) {
    this.eventId = eventId || 0;
    this.eventCpeDev = eventCpeDev || [];
  }
}
