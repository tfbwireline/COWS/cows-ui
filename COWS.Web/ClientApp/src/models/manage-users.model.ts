
export class ManageUsers {
  constructor(public userId?: number, public userAdid?: string, public fullNme?: string, public menuPref?: string, public usrPrfNme?: string, public usrPrfDes?: string, public recStusId?: number, public poolCd?: number) {
  }
}

export interface IManageUsersResponse {
  map(arg0: (user: any) => ManageUsers): any;
  results: ManageUsers[];
}
