export class RedesignType {
  redsgnTypeId: number;
  redsgnTypeNme: string;
  redsgnTypeDes: string;
  redsgnCatId: number;
  isBlblCd: boolean;
  odieReqCd: boolean;

  constructor();
  constructor(redsgnTypeId?: number, redsgnTypeNme?: string, redsgnTypeDes?: string,
    redsgnCatId?: number, isBlblCd?: boolean, odieReqCd?: boolean) {
    this.redsgnTypeId = redsgnTypeId || 0;
    this.redsgnTypeNme = redsgnTypeNme || null;
    this.redsgnTypeDes = redsgnTypeDes || null;
    this.redsgnCatId = redsgnCatId || 0;
    this.isBlblCd = isBlblCd || false;
    this.odieReqCd = odieReqCd || false;
  }
}
