export class EventFailActy {
  eventHistId: number;
  failActyId: number;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;


  constructor();
  constructor(eventHistId?: number, failActyId?: number, recStusId?: number, recStatus?: boolean, creatDt?: Date) {
    this.eventHistId = eventHistId || 0;
    this.failActyId = failActyId || 0;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;

  }
}

