
export class Calendar {
  subjTxt: string;
  des: string;
  strtTmst: any;
  endTmst: any;
  apptLocTxt: string;
  rcurncDesTxt: string;
  apptId: number;
  apptTypeId: number;
  asnToUserIdListTxt: string;
  rcurncCd: number;
  creatByUserId: number;
  modfdByUserId: number;
  delFlg: number;

  constructor();
  constructor(subjTxt?: string, des?: string, strtTmst?: Date, endTmst?: Date,
    apptLocTxt?: string, rcurncDesTxt?: string, apptId?: number, apptTypeId?: number,
    asnToUserIdListTxt?: string, rcurncCd?: number, creatByUserId?: number, modfdByUserId?: number, delFlg?: number) {
    this.subjTxt = subjTxt;
    this.des = des;
    this.strtTmst = strtTmst || null;
    this.endTmst = endTmst || null;
    this.apptLocTxt = apptLocTxt || null;
    this.rcurncDesTxt = rcurncDesTxt;
    this.apptId = apptId || 0;
    this.apptTypeId = apptTypeId || 0;
    this.asnToUserIdListTxt = asnToUserIdListTxt;
    this.rcurncCd = rcurncCd || 0;
    this.creatByUserId = creatByUserId || 0;
    this.modfdByUserId = modfdByUserId || 0;
    this.delFlg = delFlg || 0;
  }
}
