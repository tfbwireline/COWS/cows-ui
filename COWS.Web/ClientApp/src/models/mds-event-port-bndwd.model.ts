
export class MDSEventPortBndwd {
  portBndwdId: number|string;
  eventId: number;
  odieDevNme: string;
  m5OrdrNbr: string;
  m5OrdrCmpntId: string;
  nua: string;
  portBndwd: string;
  creatDt: Date;
  recStusId: number;
  modfdDt: Date;

  constructor();
  constructor(portBndwdId?: number, eventId?: number, odieDevNme?: string, m5OrdrNbr?: string, m5OrdrCmpntId?: string, nua?: string,
    portBndwd?: string, creatDt?: Date, recStusId?: number, modfdDt?: Date) {
    this.portBndwdId = portBndwdId || 0;
    this.eventId = eventId || 0;
    this.odieDevNme = odieDevNme;
    this.m5OrdrNbr = m5OrdrNbr;
    this.m5OrdrCmpntId = m5OrdrCmpntId;
    this.nua = nua;
    this.portBndwd = portBndwd;
    this.creatDt = creatDt || null;
    this.recStusId = recStusId || 1;
    this.modfdDt = modfdDt || null;
  }
}
