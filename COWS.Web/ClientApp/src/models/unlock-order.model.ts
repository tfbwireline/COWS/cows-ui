
export class UnlockOrder {
  ordrId: string;
  LockByUserId: string;
  lockedBy: string;
  ftn: string;
  productType: string;
  ordrType: string;


  constructor();
  constructor(ordrId?: string, LockByUserId?: string,lockedBy?: string, ftn?: string, productType?: string, ordrType?: string) {
    this.ordrId = ordrId;
    this.LockByUserId = LockByUserId;
    this.lockedBy = lockedBy;
    this.ftn = ftn;
    this.productType = productType ;
    this.ordrType = ordrType;
    
  }
}
