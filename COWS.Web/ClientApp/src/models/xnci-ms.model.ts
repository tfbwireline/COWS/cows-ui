
export class XnciMs {
  msId: number;
  msDes: string;
  msTaskId: number;
  creatDt: Date;


  constructor();
  constructor(msId?: number, msDes?: string, msTaskId?: number,creatDt?: Date) {
    this.msId = msId || 0;
    this.msDes = msDes;
    this.msTaskId = msTaskId || 0;
    this.creatDt = creatDt || null;

  }
}
