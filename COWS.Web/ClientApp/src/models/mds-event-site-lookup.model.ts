import { EventCpeDev } from "./event-cpe-dev.model";
import { EventDevSrvcMgmt } from "./event-dev-srvc-mgmt.model";
import { MdsEventSiteSrvc } from "./mds-event-site-srvc.model";
import { MDSEventDslSbicCustTrpt } from "./mds-event-dsl-sbic-cust-trpt.model";
import { MDSEventSlnkWiredTrpt } from "./mds-event-slnk-wired-trpt.model";
import { MDSEventWrlsTrpt } from "./mds-event-wrls-trpt.model";
import { MDSEventPortBndwd } from "./mds-event-port-bndwd.model";

export class MDSEventSiteLookup {
  public instlSitePocName: string
  public instlSitePocIntlPhnCd: string
  public instlSitePocPhnNbr: string
  public srvcAssrnPocNme: string
  public srvcAssrnPocIntlPhnCd: string
  public srvcAssrnPocPhnNbr: string
  public srvcAvlbltyHrs: string
  public srvcTmeZnCd: string
  public siteIdTxt: string
  public streetAdr: string
  public flrBldNme: string
  public ctyNme: string
  public sttPrvnNme: string
  public ctryRgnNme: string
  public zipCd: string
  public cpeDispatchEmail: string
  public cpeDevices: EventCpeDev[]
  public mnsOrders: EventDevSrvcMgmt[]
  public relatedMnsOrders: EventDevSrvcMgmt[]
  public mnsAsasTypes: EventDevSrvcMgmt[]
  public siteServices: MdsEventSiteSrvc[]
  public thirdParty: MDSEventDslSbicCustTrpt[]
  public wiredDeviceTransports: MDSEventSlnkWiredTrpt[]
  public wirelessDeviceTransports: MDSEventWrlsTrpt[]
  public portBandwidth: MDSEventPortBndwd[]

  constructor({ instlSitePocName = "", instlSitePocIntlPhnCd = "", instlSitePocPhnNbr = "", srvcAssrnPocNme = "", srvcAssrnPocIntlPhnCd = "", srvcAssrnPocPhnNbr = "", srvcAvlbltyHrs = "",
    srvcTmeZnCd = "", siteIdTxt = "", streetAdr = "", flrBldNme = "", ctyNme = "", sttPrvnNme = "", ctryRgnNme = "", zipCd = "", cpeDispatchEmail = "", cpeDevices = [], mnsOrders = [],
    relatedMnsOrders = [], mnsAsasTypes = [], siteServices = [], thirdParty = [], wiredDeviceTransports = [], wirelessDeviceTransports = [], portBandwidth = [] }) {
    this.instlSitePocName = instlSitePocName || "";
    this.instlSitePocIntlPhnCd = instlSitePocIntlPhnCd || "";
    this.instlSitePocPhnNbr = instlSitePocPhnNbr || "";
    this.srvcAssrnPocNme = srvcAssrnPocNme || "";
    this.srvcAssrnPocIntlPhnCd = srvcAssrnPocIntlPhnCd || "";
    this.srvcAssrnPocPhnNbr = srvcAssrnPocPhnNbr || "";
    this.srvcAvlbltyHrs = srvcAvlbltyHrs || "";
    this.srvcTmeZnCd = srvcTmeZnCd || "";
    this.siteIdTxt = siteIdTxt || "";
    this.streetAdr = streetAdr || "";
    this.flrBldNme = flrBldNme || "";
    this.ctyNme = ctyNme || "";
    this.sttPrvnNme = sttPrvnNme || "";
    this.ctryRgnNme = ctryRgnNme || "";
    this.zipCd = zipCd || "";
    this.cpeDevices = cpeDevices || [];
    this.mnsOrders = mnsOrders || [];
    this.mnsAsasTypes = mnsAsasTypes || [];
    this.relatedMnsOrders = relatedMnsOrders || [];
    this.siteServices = siteServices || [];
    this.thirdParty = thirdParty || [];
    this.wiredDeviceTransports = wiredDeviceTransports || [];
    this.wirelessDeviceTransports = wirelessDeviceTransports || [];
    this.portBandwidth = portBandwidth || [];
    this.cpeDispatchEmail = cpeDispatchEmail || "";
  }
}
