import { CptPrimWan } from ".";

export class CptRelatedInfo {
  cptRltdInfoId: number;
  cptId: number;
  cptPlnSrvcTierId?: number;
  cptCustTypeId: number;
  hstdUcCd?: boolean;
  cubeSiptSmiCd: boolean;
  e2eNddCd?: boolean;
  ntwkDsgnDocTxt: string;
  cmntTxt: string;
  custUcdCd?: boolean;
  spsSowCd?: boolean;
  creatDt: Date;
  sdwanCustCd?: boolean;
  suplmntlEthrntCd?: boolean;
  cntctEmailList: string;

  cptPrimWan: CptPrimWan[];
  secTrnsprtTypes: number[];

  constructor();
  constructor(cptRltdInfoId?: number, cptId?: number, cptPlnSrvcTierId?: number,
    cptCustTypeId?: number, hstdUcCd?: boolean, cubeSiptSmiCd?: boolean,
    e2eNddCd?: boolean, ntwkDsgnDocTxt?: string, cmntTxt?: string, custUcdCd?: boolean,
    spsSowCd?: boolean, creatDt?: Date, sdwanCustCd?: boolean, suplmntlEthrntCd?: boolean,
    cntctEmailList?: string, cptPrimWan?: CptPrimWan[], secTrnsprtTypes?: number[]
  ) {
    this.cptRltdInfoId = cptRltdInfoId || 0;
    this.cptId = cptId || 0;
    this.cptPlnSrvcTierId = cptPlnSrvcTierId || null;
    this.cptCustTypeId = cptCustTypeId;
    this.hstdUcCd = hstdUcCd || null;
    this.cubeSiptSmiCd = cubeSiptSmiCd || false;
    this.e2eNddCd = e2eNddCd || null;
    this.ntwkDsgnDocTxt = ntwkDsgnDocTxt;
    this.cmntTxt = cmntTxt;
    this.custUcdCd = custUcdCd || null;
    this.spsSowCd = spsSowCd || null;
    this.creatDt = creatDt || new Date();
    this.sdwanCustCd = sdwanCustCd || null;
    this.suplmntlEthrntCd = suplmntlEthrntCd || null;
    this.cntctEmailList = cntctEmailList;
    this.cptPrimWan = cptPrimWan || [];
    this.secTrnsprtTypes = secTrnsprtTypes || [];
  }
}
