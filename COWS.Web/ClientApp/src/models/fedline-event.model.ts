import { FedlineTadpoleData } from "./fedline-tadpole-data.model";
import { FedlineUserData } from "./fedline-user-data.model";

export class FedlineEvent {
  eventId: number
  eventStatusId: number
  eventStatus: string
  workflowStatusId: number
  workflowStatus: string
  failCodeId: number
  failCode: string
  statusComments: string
  tadpoleData: FedlineTadpoleData
  userData: FedlineUserData
  changeList: string[]
  activatorWorkflowStatusId: number
  activatorFailCodeId: number

  constructor();
  constructor(eventId?: number, eventStatusId?: number, eventStatus?: string, workflowStatusId?: number, workflowStatus?: string,
    failCodeId?: number, failCode?: string, statusComments?: string, tadpoleData?: FedlineTadpoleData, userData?: FedlineUserData,
    changeList?: string[], activatorWorkflowStatusId?: number, activatorFailCodeId?: number) {
    this.eventId = eventId || 0
    this.eventStatusId = eventStatusId || 0
    this.eventStatus = eventStatus || null
    this.workflowStatusId = workflowStatusId || 0
    this.workflowStatus = workflowStatus || null

    this.failCodeId = failCodeId || 0
    this.failCode = failCode || null
    this.statusComments = statusComments || null
    this.tadpoleData = tadpoleData || null
    this.userData = userData || null
    this.changeList = changeList || null

    this.activatorWorkflowStatusId = activatorWorkflowStatusId || 0
    this.activatorFailCodeId = activatorFailCodeId || 0
  }
}
