
export class FedlineManagedDevice {
  deviceName: string
  serialNum: string
  vendor: string
  model: string
  deviceType: string

  constructor();
  constructor(deviceName?: string, serialNum?: string, vendor?: string, model?: string, deviceType?: string) {
    this.deviceName = deviceName || null
    this.serialNum = serialNum || null
    this.vendor = vendor || null
    this.model = model || null
    this.deviceType = deviceType || null
  }
}
