
export class FormControlItem {
  value: string;
  text: string;
  selected: boolean;

  constructor(value: string, text: string) {
    this.value = value;
    this.text = text;
    this.selected = false;
  }
}
