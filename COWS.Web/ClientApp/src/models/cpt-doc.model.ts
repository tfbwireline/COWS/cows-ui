export class CptDoc {
  cptDocId: number;
  cptId: number;
  fileNme: string;
  fileCntnt: Int8Array;
  fileSizeQty: number;
  cmntTxt: string;
  creatDt: Date;
  modfdDt?: Date
  creatByUserId: number;
  creatByAdId: string;
  modfdByUserId?: number;
  recStusId: number;
  base64string: string;

  constructor();
  constructor(cptDocId?: number, cptId?: number, fileNme?: string, fileCntnt?: Int8Array,
    fileSizeQty?: number, cmntTxt?: string, creatDt?: Date, modfdDt?: Date,
    creatByUserId?: number, creatByAdId?: string, modfdByUserId?: number,
    recStusId?: number, base64string?: string
  ) {
    this.cptDocId = cptDocId || 0;
    this.cptId = cptId || 0;
    this.fileNme = fileNme;
    this.fileCntnt = fileCntnt || null;
    this.fileSizeQty = fileSizeQty || 0;
    this.cmntTxt = cmntTxt;
    this.creatDt = creatDt || new Date();
    this.modfdDt = modfdDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.creatByAdId = creatByAdId;
    this.modfdByUserId = modfdByUserId || null;
    this.recStusId = recStusId || 0;
    this.base64string = base64string || null;
  }
}
