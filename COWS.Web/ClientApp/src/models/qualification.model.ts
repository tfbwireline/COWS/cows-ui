
export class Qualification {
  qualificationID: number;
  assignToUserID: number;
  assignToUser: string;
  assignToUserAdId: string;
  customerIDs: string;
  customerNames: string;
  deviceIDs: string;
  deviceNames: string;
  enhanceServiceIDs: string;
  enhanceServiceNames: string;
  eventTypeIDs: string;
  eventTypeNames: string;
  ipVersionIDs: string;
  ipVersionNames: string;
  networkActivityIDs: string;
  networkActivityNames: string;
  mplsActyTypeIDs: string;
  mplsActyTypeNames: string;
  specialProjectIDs: string;
  specialProjectNames: string;
  vendorModelIDs: string;
  vendorModelNames: string;
  recStusId: number;
  recStatus: boolean;
  createdDate: Date;
  createdByUserID: number;
  createdByAdId: string;
  createdByUser: string;
  modifiedDate: Date;
  modifiedByUserID: number;
  modifiedByAdId: string;
  modifiedByUser: string;

  constructor();
  constructor(qualificationID?: number, assignToUserID?: number, assignToUser?: string, assignToUserAdId?: string,
    customerIDs?: string, customerNames?: string, deviceIDs?: string, deviceNames?: string,
    enhanceServiceIDs?: string, enhanceServiceNames?: string, eventTypeIDs?: string, eventTypeNames?: string,
    ipVersionIDs?: string, ipVersionNames?: string, networkActivityIDs?: string, networkActivityNames?: string,
    mplsActyTypeIDs?: string, mplsActyTypeNames?: string, specialProjectIDs?: string, specialProjectNames?: string,
    vendorModelIDs?: string, vendorModelNames?: string, recStusId?: number, recStatus?: boolean,
    createdDate?: Date, createdByUserID?: number, createdByAdId?: string, createdByUser?: string,
    modifiedDate?: Date, modifiedByUserID?: number, modifiedByAdId?: string, modifiedByUser?: string) {
    this.qualificationID = qualificationID || 0;
    this.assignToUserID = assignToUserID || 0;
    this.assignToUser = assignToUser;
    this.assignToUserAdId = assignToUserAdId;
    this.customerIDs = customerIDs;
    this.customerNames = customerNames;
    this.deviceIDs = deviceIDs;
    this.deviceNames = deviceNames;
    this.enhanceServiceIDs = enhanceServiceIDs;
    this.enhanceServiceNames = enhanceServiceNames;
    this.eventTypeIDs = eventTypeIDs;
    this.eventTypeNames = eventTypeNames;
    this.ipVersionIDs = ipVersionIDs;
    this.ipVersionNames = ipVersionNames;
    this.networkActivityIDs = networkActivityIDs;
    this.networkActivityNames = networkActivityNames;
    this.mplsActyTypeIDs = mplsActyTypeIDs;
    this.mplsActyTypeNames = mplsActyTypeNames;
    this.specialProjectIDs = specialProjectIDs;
    this.specialProjectNames = specialProjectNames;
    this.vendorModelIDs = vendorModelIDs;
    this.vendorModelNames = vendorModelNames;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.createdDate = createdDate || null;
    this.createdByUserID = createdByUserID || 0;
    this.createdByAdId = createdByAdId;
    this.createdByUser = createdByUser;
    this.modifiedDate = modifiedDate || null;
    this.modifiedByUserID = modifiedByUserID || 0;
    this.modifiedByAdId = modifiedByAdId;
    this.modifiedByUser = modifiedByUser;
  }
}
