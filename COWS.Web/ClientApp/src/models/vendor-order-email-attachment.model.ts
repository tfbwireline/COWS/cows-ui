export class VendorOrderEmailAttachment {
  vndrOrdrEmailAtchmtId: number;
  vndrOrdrEmailId: number;
  fileNme: string;
  fileCntnt: Int8Array;
  fileSizeQty: number;
  fileSizeQtyStr: string;
  creatDt: Date;
  creatByUserId: number;
  recStusId: number;
  creatByUserFullName: string;
  base64string: any;

  constructor({ vndrOrdrEmailAtchmtId = 0, vndrOrdrEmailId = 0, fileNme = "", fileCntnt = null, fileSizeQty = 0, fileSizeQtyStr = '', recStusId = 1,
    creatDt = new Date(), creatByUserId = 1, creatByUserFullName = "", base64string = null }) {
    this.vndrOrdrEmailAtchmtId = vndrOrdrEmailAtchmtId
    this.vndrOrdrEmailId = vndrOrdrEmailId
    this.fileNme = fileNme
    this.fileCntnt = fileCntnt
    this.fileSizeQty = fileSizeQty
    this.fileSizeQtyStr = fileSizeQtyStr
    this.creatDt = creatDt
    this.creatByUserId = creatByUserId
    this.recStusId = recStusId
    this.creatByUserFullName = creatByUserFullName
    this.base64string = base64string
  }
}
