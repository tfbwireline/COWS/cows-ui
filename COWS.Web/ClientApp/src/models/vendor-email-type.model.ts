export class VendorEmailType {
  vndrEmailTypeId: number;
  vndrEmailTypeDes: string;
  creatDt: Date;

  constructor({ vndrEmailTypeId = 0, vndrEmailTypeDes = null, creatDt = new Date() }) {
    this.vndrEmailTypeId = vndrEmailTypeId;
    this.vndrEmailTypeDes = vndrEmailTypeDes;
    this.creatDt = creatDt
  }
}
