export class AdvancedSearchEvent {
  type: number
  status: number
  m5_no: string
  assignedName: string
  requestorName: string
  eventId: string
  eventType: number
  eventStatus: number
  fmsCircuit: string
  nua: string
  h5_h6: string
  ftActivityType: string
  customer: string
  deviceName: string
  deviceSerialNo: string
  frbRequestId: string
  frbOrgId: string
  frbConfigurationType: number
  apptStartDate: string
  apptEndDate: string
  frbActivityType: string
  redesignNo: string
  mdsActivityType: number
  mdsMacType: number[]
  mdsNtwkActyType: string
  adType: string
  sowsId: string

  constructor() {

  }
}
