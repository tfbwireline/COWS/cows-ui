export class SearchedFTNResultModel {
    ordrId: number;
    ftn: string;
    ordrType: string;
    prodType: string;
    ol: string;
    isLocked : boolean // Added for UI manipulation
  
    constructor();
    constructor(ordrId?: number, ftn?: string, ordrType?: string, prodType?: string, ol?: string, isLocked?: boolean) {
        this.ordrId = ordrId || 0;
        this.ftn = ftn || null;
        this.ordrType = ordrType || null;
        this.prodType = prodType || null;
        this.ol = ol || null;
        this.isLocked = isLocked || false;
    }
  }
  