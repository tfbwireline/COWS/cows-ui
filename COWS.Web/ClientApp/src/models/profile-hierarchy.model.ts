export class ProfileHierarchy {
  id: number
  chldPrfId: number
  prntPrfId: number
  recStusId: number
  modfdDt: Date
  creatDt: Date

  constructor({ id = 0, chldPrfId = 0, prntPrfId = 0, recStusId = 0, modfdDt = null, creatDt = null }) {
    this.id = id
    this.chldPrfId = chldPrfId
    this.prntPrfId = prntPrfId
    this.recStusId = recStusId
    this.modfdDt = modfdDt
    this.creatDt = creatDt
  }
}
