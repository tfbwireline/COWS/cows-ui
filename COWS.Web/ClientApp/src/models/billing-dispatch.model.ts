export interface BillingDispatch {
  id: number;
  customerId: string;
  ticketLocation: string;
  dispositionCode: string;
  ticketCategoryName: string;
  ticketSubCategoryName: string;
  actionTaken: string;
  closeDate?: Date;
  ticketNumber: string;
  acceptRejectCode?: string;
  acceptReject?: string;
  comment: string;
  statusId?: number;
  statusDescription?: string;
  createdByUserID?: number;
  createdBy: string;
  modifiedByUserID?: number;
  modifiedBy: string;
  createdDate?: Date;
  modifiedDate?: Date;
}
