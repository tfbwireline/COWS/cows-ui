
export class EventAsnToUser {
  asnToUserId: number;
  eventId: number;
  userDsplNme: string;
  userEmail: string;
  recStusId: number;
  roleId: number
  creatDt: Date;
  modfdDt: Date;

  constructor();
  constructor(asnToUserId?: number, eventId?: number, userDsplNme?: string, userEmail?: string,
    recStusId?: number, roleId?: number, creatDt?: Date, modfdDt?: Date) {
    this.asnToUserId = asnToUserId || null
    this.eventId = eventId || null
    this.userDsplNme = userDsplNme || null
    this.userEmail = userEmail || null
    this.recStusId = recStusId || null
    this.roleId = roleId || null
    this.creatDt = creatDt || new Date()
    this.modfdDt = modfdDt || null
  }
}
