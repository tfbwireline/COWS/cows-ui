export class CurrencyFile {
  srcCurFileId: number;
  fileNme: string;
  fileCntntBtsm: Int8Array
  prsdCurListTxt: string
  prcsdDt: Date;
  recStusId: number;
  creatByUserId: number;
  createdByAdId: string;
  createdByName: string;
  creatDt: Date;

  constructor();
  constructor(srcCurFileId?: number, fileNme?: string, fileCntntBtsm?: Int8Array, prsdCurListTxt?: string, prcsdDt?: Date, recStusId?: number,
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string, createdByName?: string) {
    this.srcCurFileId = srcCurFileId || 0;
    this.fileNme = fileNme || "";
    this.fileCntntBtsm = fileCntntBtsm || null;
    this.prsdCurListTxt = prsdCurListTxt || null;
    this.recStusId = recStusId || 1;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.createdByName = createdByName || "";
    this.createdByAdId = createdByAdId;
  }
}
