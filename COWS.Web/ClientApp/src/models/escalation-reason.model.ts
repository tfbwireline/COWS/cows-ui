export class EscalationReason {
  esclReasId?: number;
  esclReasDes?: string;
  recStusId?: boolean;
  creatByUserId?: number;
  modfdByUserId?: number;

  constructor();
  constructor(esclReasId?: number, esclReasDes?: string, recStusId?: boolean, creatByUserId?: number, modfdByUserId?: number) {
    this.esclReasId = esclReasId || 0;
    this.esclReasDes = esclReasDes || null;
    this.recStusId = recStusId || false;
    this.creatByUserId = creatByUserId || null;
    this.modfdByUserId = modfdByUserId || null;
  }
}
