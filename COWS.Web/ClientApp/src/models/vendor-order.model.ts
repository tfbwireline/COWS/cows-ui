import { VendorOrderEmail } from "./vendor-order-email.model";
import { VendorOrderMs } from "./vendor-order-ms.model";
import { H5Folder } from "./h5-folder.model";

export class VendorOrder {
  vndrOrdrId: number;
  vndrFoldrId: number;
  ordrId: number;
  vndrOrdrTypeId: number;
  trmtgCd: boolean;
  prevOrdrId: number;
  bypasVndrOrdrMsCd: boolean;
  recStusId: number;
  creatDt: Date;
  creatByUserId: number;
  modfdDt: Date;
  modfdByUserId: number;
  vndrNme: string;
  vndrCd: string;
  ctryNme: string;

  vndrOrdrEmail: VendorOrderEmail[]
  vndrOrdrMs: VendorOrderMs[]
  h5Foldr: H5Folder

  constructor({ vndrOrdrId = 0, vndrFoldrId = 0, ordrId = null, vndrOrdrTypeId = 1, trmtgCd = true, prevOrdrId = null, bypasVndrOrdrMsCd = null, recStusId = 1,
    creatDt = new Date(), creatByUserId = 1, modfdDt = null, modfdByUserId = null, vndrNme = null, vndrCd = null, ctryNme = null, vndrOrdrEmail = null, vndrOrdrMs = null, h5Foldr = null }) {
    this.vndrOrdrId = vndrOrdrId
    this.vndrFoldrId = vndrFoldrId
    this.ordrId = ordrId
    this.vndrOrdrTypeId = vndrOrdrTypeId
    this.trmtgCd = trmtgCd
    this.prevOrdrId = prevOrdrId
    this.bypasVndrOrdrMsCd = bypasVndrOrdrMsCd
    this.recStusId = recStusId
    this.creatDt = creatDt
    this.creatByUserId = creatByUserId
    this.modfdDt = modfdDt
    this.modfdByUserId = modfdByUserId
    this.vndrNme = vndrNme
    this.vndrCd = vndrCd
    this.ctryNme = ctryNme
    this.vndrOrdrEmail = vndrOrdrEmail
    this.vndrOrdrMs = vndrOrdrMs
    this.h5Foldr = h5Foldr
  }
}
