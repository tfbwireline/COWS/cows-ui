
export class FedlineUserData {
  eventTitle: string
  emailCcpdl: string
  shippingCarrier: string
  trackingNumber: string
  arrivalDate: Date
  preStageComplete: boolean
  preStagingEngineerUserId: number
  preStagingEngineerUserDisplay: string
  mnsActivatorUserId: number
  mnsActivatorUserDisplay: string
  deviceType: string
  sOWLocation: string
  redesign: string
  tunnelStatus: string
  failCodeId: number
  slaCd: string

  constructor();
  constructor(eventTitle?: string, emailCcpdl?: string, shippingCarrier?: string, trackingNumber?: string, arrivalDate?: Date,
    preStageComplete?: boolean, preStagingEngineerUserId?: number, mnsActivatorUserId?: number, mnsActivatorUserDisplay?: string, deviceType?: string,
    sOWLocation?: string, redesign?: string, tunnelStatus?: string, failCodeId?: number, slaCd?: string) {
    this.eventTitle = eventTitle || null
    this.emailCcpdl = emailCcpdl || null
    this.shippingCarrier = shippingCarrier || null
    this.trackingNumber = trackingNumber || null
    this.arrivalDate = arrivalDate || null

    this.preStageComplete = preStageComplete || false
    this.preStagingEngineerUserId = preStagingEngineerUserId || 0
    this.mnsActivatorUserId = mnsActivatorUserId || 0
    this.mnsActivatorUserDisplay = mnsActivatorUserDisplay || null
    this.deviceType = deviceType || null

    this.sOWLocation = sOWLocation || null
    this.redesign = redesign || null
    this.tunnelStatus = tunnelStatus || null
    this.failCodeId = failCodeId || 0
    this.slaCd = slaCd || null
  }
}
