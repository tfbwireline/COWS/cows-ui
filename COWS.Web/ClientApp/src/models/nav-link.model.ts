export class NavLink {
  menuId: number;
  menuNme: string;
  menuDes: string;
  dpthLvl: number;
  prntMenuId: number;
  dsplOrdr: number;
  srcPath: string;
  recStusId: number;
  children: NavLink;

  constructor();
  constructor(menuId?: number, menuNme?: string, menuDes?: string, dpthLvl?: number, prntMenuId?: number, dsplOrdr?: number, srcPath?: string, recStusId?: number, children?: NavLink) {
    this.menuId = menuId || 0;
    this.menuNme = menuNme || "";
    this.menuDes = menuDes || "";
    this.dpthLvl = dpthLvl || 0;
    this.prntMenuId = prntMenuId || 0;
    this.dsplOrdr = dsplOrdr || 0;
    this.srcPath = srcPath || "";
    this.recStusId = recStusId || 0;
    this.children = children || null;
  }
}
