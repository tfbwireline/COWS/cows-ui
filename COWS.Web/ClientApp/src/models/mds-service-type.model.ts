export class MdsServiceType {
  srvcTypeId: number;
  srvcTypeDes: string;
  recStusId: number;
  creatDt: Date;

  constructor({ srvcTypeId = null, srvcTypeDes = null, recStusId = null, creatDt = null }) {
    this.srvcTypeId = srvcTypeId
    this.srvcTypeDes = srvcTypeDes
    this.recStusId = recStusId
    this.creatDt = creatDt
  }
}
