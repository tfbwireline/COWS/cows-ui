import { MDSEventNtwkCust } from "./mds-event-ntwk-cust.model";
import { MDSEventNtwkTrpt } from "./mds-event-ntwk-trpt.model";
import { MdsEventOdieDevice } from "./mds-event-odie-device.model";
import { MDSEventDiscOdieDev } from "./mds-event-disc-odie-dev.model";
import { EventDeviceCompletion } from "./event-device-completion.model";
import { EventCpeDev } from "./event-cpe-dev.model";
import { EventDevSrvcMgmt } from "./event-dev-srvc-mgmt.model";
import { MdsEventSiteSrvc } from "./mds-event-site-srvc.model";
import { MDSEventDslSbicCustTrpt } from "./mds-event-dsl-sbic-cust-trpt.model";
import { MDSEventSlnkWiredTrpt } from "./mds-event-slnk-wired-trpt.model";
import { MDSEventWrlsTrpt } from "./mds-event-wrls-trpt.model";
import { MDSEventPortBndwd } from "./mds-event-port-bndwd.model";
import { EventDiscoDev } from "./event-disco-dev.model";
import { MDSEventNtwkActy } from "./mds-event-ntwk-acty.model";
import { MDSRelatedCeEvent } from "./mds-related-ce-event.model";
import { MDSEventSiteLookup } from "./mds-event-site-lookup.model";
import { ContactDetail } from "./contact-detail.model";

export class MdsEvent {
  eventId: number;
  ntwkActyTypeId: number; /* Activity Type */
  mdsEventNtwkActyIds: number[];
  eventStusId: number;
  eventTitleTxt: string;
  shrtDes: string; // Short desc for both Disco and MAC/Pre-Staging
  h1: string;
  custNme: string;
  custAcctTeamPdlNme: string;
  custSowLocTxt: string;
  mnsPmId: string;
  mdsActyTypeId: number;
  mdsEventMacActyIds: number[];
  ntwkH6: string;
  ntwkH1: string;
  ntwkCustNme: string;
  ntwkEventTypeId: number; /* Network Event Type */
  mplsEventActyTypeIds: number[]; /* Network Activity Types */
  vpnPltfrmTypeId: number;
  ddAprvlNbr: string; /* Design Document # */
  reqorUserId: number;
  reqorUserCellPhnNbr: string;
  h6: string;
  ccd: any;
  siteIdTxt: string;
  streetAdr: string; /* Address */
  sttPrvnNme: string; /* State */
  flrBldgNme: string;
  ctryRgnNme: string; /* Country */
  ctyNme: string; /* City */
  zipCd: string;
  usIntlCd: string;
  srvcAvlbltyHrs: string; /* SA Contact Hours */
  srvcTmeZnCd: string; /* SA Timezone */
  instlSitePocNme: string;
  instlSitePocIntlPhnCd: string;
  instlSitePocPhnNbr: string;
  instlSitePocIntlCellPhnCd: string;
  instlSitePocCellPhnNbr: string;
  srvcAssrnPocNme: string;
  srvcAssrnPocIntlPhnCd: string;
  srvcAssrnPocPhnNbr: string;
  srvcAssrnPocIntlCellPhnCd: string;
  srvcAssrnPocCellPhnNbr: string;
  sprintCpeNcrId: number;
  shippedDt: Date;
  shipTrkRefrNbr: string;
  shipCustEmailAdr: string;
  shipDevSerialNbr: string;
  shipCxrNme: string;
  discMgmtCd: string;
  fullCustDiscCd: boolean;
  fullCustDiscReasTxt: string;
  esclCd: boolean;
  esclReasId: number;
  primReqDt: any;
  scndyReqDt: any;
  esclBusJustnTxt: string;
  pubEmailCcTxt: string[];
  cmpltdEmailCcTxt: string;
  strtTmst: any;
  endTmst: any;
  eventDrtnInMinQty: number;
  extraDrtnTmeAmt: number;
  activators: number[];
  wrkflwStusId: number;
  cmntTxt: string;
  isSuppressedEmails: boolean;
  mdsFastTrkTypeId: string;
  cpeOrdrCd: boolean;
  mnsOrdrCd: boolean;
  frcdftCd: boolean;
  cnfrcBrdgId: number;
  cnfrcBrdgNbr: string;
  cnfrcPinNbr: string;
  onlineMeetingAdr: string;
  cpeDspchEmailAdr: string;
  cpeDspchCmntTxt: string;
  softAssignCd: boolean;
  nidHostNme: string;
  nidSerialNbr: string;
  custLtrOptOut: boolean;
  
  
  //Alternate Shiiping Address
  altShipPocNme: string;
  altShipPocPhnNbr: string;
  altShipPocEmail: string;
  altShipStreetAdr: string;
  altShipFlrBldgNme: string;
  altShipCtyNme: string;
  altShipSttPrvnNme: string;
  altShipCtryRgnNme: string;
  altShipZipCd: string;
  mdsCmpltCd: boolean;
  ntwkCmpltCd: boolean;
  vasCmpltCd: boolean;

  ipmDes: string;
  rltdCmpsNcrCd: boolean;
  rltdCmpsNcrNme: string;
  woobIpAdr: string;
  failReasId: number;
  preCfgCmpltCd: boolean;
  cntctEmailList: string;
  odieCustId: string;
  mdrNumber: string;

  contactDetails: ContactDetail[];

  // NETWORK
  networkCustomer: MDSEventNtwkCust[];
  networkTransport: MDSEventNtwkTrpt[];
  mplsEventActyType: any[]; // Should have model for this
  rltdCEEvent: MDSRelatedCeEvent[];
  reviewerUserId: number
  activatorUserId: number

  // REDESIGN
  mdsRedesignDevInfo: MdsEventOdieDevice[];

  // DISCONNECT
  eventDiscoDev: EventDiscoDev[];

  // SITE SUB TABLES
  devCompletion: EventDeviceCompletion[];
  cpeDevice: EventCpeDev[];
  mnsOrder: EventDevSrvcMgmt[];
  siteService: MdsEventSiteSrvc[];
  thirdParty: MDSEventDslSbicCustTrpt[];
  wiredTransport: MDSEventSlnkWiredTrpt[];
  wirelessTransport: MDSEventWrlsTrpt[];
  portBandwidth: MDSEventPortBndwd[];
  mdsEventNtwkActy: MDSEventNtwkActy[];

  h6Lookup: MDSEventSiteLookup;

  constructor();
  constructor(eventId?: number, ntwkActyTypeId?: number,  eventStusId?: number, eventTitleTxt?: string, shrtDes?: string, h1?: string,
    custNme?: string, custAcctTeamPdlNme?: string, custSowLocTxt?: string, mnsPmId?: string, mdsActyTypeId?: number, mdsEventMacActyIds?: number[],
    ntwkH6?: string, ntwkH1?: string, ntwkCustNme?: string, ntwkEventTypeId?: number, mplsEventActyTypeIds?: number[], vpnPltfrmTypeId?: number,
    ddAprvlNbr?: string, reqorUserId?: number, reqorUserCellPhnNbr?: string, h6?: string, ccd?: string, siteIdAddress?: string,
    streetAdr?: string, sttPrvnNme?: string, flrBldgNme?: string, ctryRgnNme?: string, ctyNme?: string, zipCd?: string, usIntlCd?: string,
    srvcAvlbltyHrs?: string, srvcTmeZnCd?: string, instlSitePocNme?: string, instlSitePocIntlPhnCd?: string, instlSitePocPhnNbr?: string,
    instlSitePocIntlCellPhnCd?: string, instlSitePocCellPhnNbr?: string, srvcAssrnPocNme?: string, srvcAssrnPocIntlPhnCd?: string,
    srvcAssrnPocPhnNbr?: string, srvcAssrnPocIntlCellPhnCd?: string, srvcAssrnPocCellPhnNbr?: string, sprintCpeNcrId?: number, shippedDt?: Date,
    shipTrkRefrNbr?: string, shipCustEmailAdr?: string, shipDevSerialNbr?: string, shipCxrNme?: string, discMgmtCd?: string, fullCustDiscCd?: boolean,
    fullCustDiscReasTxt?: string, esclCd?: boolean, esclReasId?: number, primReqDt?: Date, scndyReqDt?: Date, esclBusJustnTxt?: string, pubEmailCcTxt?: string[],
    cmpltdEmailCcTxt?: string, strtTmst?: Date, endTmst?: Date, eventDrtnInMinQty?: number, extraDrtnTmeAmt?: number, activators?: number[], wrkflwStusId?: number, cmntTxt?: string,
    isSuppressedEmails?: boolean, mdsFastTrkTypeId?: string, cpeOrdrCd?: boolean, mnsOrdrCd?: boolean, frcdftCd?: boolean, cnfrcBrdgId?: number, cnfrcBrdgNbr?: string,
    cnfrcPinNbr?: string, cpeDspchEmailAdr?: string, cpeDspchCmntTxt?: string, softAssignCd?: boolean, nidHostNme?: string, nidSerialNbr?: string, custLtrOptOut?: boolean, altShipPocNme?: string, altShipPocPhnNbr?: string, altShipPocEmail?: string,
    altShipStreetAdr?: string, altShipFlrBldgNme?: string, altShipCtyNme?: string, altShipSttPrvnNme?: string, altShipCtryRgnNme?: string, altShipZipCd?: string,
    mdsCmpltCd?: boolean, ntwkCmpltCd?: boolean, vasCmpltCd?: boolean, ipmDes?: string, rltdCmpsNcrCd?: boolean, rltdCmpsNcrNme?: string, woobIpAdr?: string,
    cntctEmailList?: string,

    networkCustomer?: MDSEventNtwkCust[], networkTransport?: MDSEventNtwkTrpt[], mplsEventActyType?: any[], rltdCEEvent?: MDSRelatedCeEvent, mdsRedesignDevInfo?: MdsEventOdieDevice[], eventDiscoDev?: EventDiscoDev[],
    devCompletion?: EventDeviceCompletion[], cpeDevice?: EventCpeDev[], mnsOrder?: EventDevSrvcMgmt[], siteService?: MdsEventSiteSrvc[],
    thirdParty?: MDSEventDslSbicCustTrpt[], wiredTransport?: MDSEventSlnkWiredTrpt[], wirelessTransport?: MDSEventWrlsTrpt[], portBandwidth?: MDSEventPortBndwd[],
    mdsEventNtwkActy?: MDSEventNtwkActy[]) {
    this.eventId = eventId || 0
  }
}
