
export class MDSRelatedCeEvent {
  ceServiceId: string;
  rltdCEEventID: number;

  constructor();
  constructor(ceServiceId?: string, rltdCEEventID?: number) {
    this.ceServiceId = ceServiceId || null;
    this.rltdCEEventID = rltdCEEventID || 0;
  }
}
