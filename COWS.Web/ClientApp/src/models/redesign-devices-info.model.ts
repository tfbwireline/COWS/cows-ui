export class RedesignDevicesInfo {
  redsgnDevId: number = 0;
  redsgnId: number;
  redesignNbr: string;
  expirationDate: Date;
  seqNbr: number;
  devNme: string;
  fastTrkCd?: boolean;
  dspchRdyCd?: boolean;
  dispatchReadyCd?: number;
  recStusId: boolean;
  cretdByCd: number;
  cretdDt: Date;
  modfdByCd?: number;
  modfdDt?: Date;
  nteChrgCd: boolean;
  devBillDt: Date;
  devCmpltnCd?: boolean;
  h6CustId: string;
  eventCmpltnDt?: Date;
  scCd: string;

  isSelected: boolean;
  eventId: number;

  constructor();
  constructor(redsgnDevId?: number, redsgnId?: number, redesignNbr?: string, expirationDate?: Date,
    seqNbr?: number, devNme?: string, fastTrkCd?: boolean, dspchRdyCd?: boolean,
    dispatchReadyCd?: number, recStusId?: boolean,
    cretdByCd?: number, cretdDt?: Date, modfdByCd?: number, modfdDt?: Date, nteChrgCd?: boolean,
    devBillDt?: Date, devCmpltnCd?: boolean, h6CustId?: string, eventCmpltnDt?: Date, scCd?: string,
    isSelected?: boolean, eventId?: number) {
    this.redsgnDevId = redsgnDevId || 0;
    this.redsgnId = redsgnId || 0;
    this.redesignNbr = redesignNbr;
    this.expirationDate = expirationDate || null;
    this.seqNbr = seqNbr || 0;
    this.devNme = devNme;
    this.fastTrkCd = fastTrkCd || null;
    this.dspchRdyCd = dspchRdyCd || null;
    this.dispatchReadyCd = dispatchReadyCd || null;
    this.recStusId = recStusId || true;
    this.cretdByCd = cretdByCd || 0;
    this.cretdDt = cretdDt || null;
    this.modfdByCd = modfdByCd || null;
    this.modfdDt = modfdDt || null;
    this.nteChrgCd = nteChrgCd || false;
    this.devBillDt = devBillDt || null;
    this.devCmpltnCd = devCmpltnCd || null;
    this.h6CustId = h6CustId;
    this.eventCmpltnDt = eventCmpltnDt || null;
    this.scCd = scCd || null;
    this.isSelected = isSelected || true;
    this.eventId = eventId || 0;
  }
}
