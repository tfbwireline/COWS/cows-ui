export class OdieCustRspn {
  rspnId: number;
  custTeamPdl: string;
  mnspmId: string;
  sowsFoldrPathNme: string;

  //constructor();
  constructor({ rspnId = 0, custTeamPdl = "", mnspmId = "", sowsFoldrPathNme = ""}) {
    this.rspnId = rspnId
    this.custTeamPdl = custTeamPdl
    this.mnspmId = mnspmId
    this.sowsFoldrPathNme = sowsFoldrPathNme
  }
}
