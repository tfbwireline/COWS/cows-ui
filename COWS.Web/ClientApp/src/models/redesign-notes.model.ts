export class RedesignNotes {
  redsgnNotesId: number;
  redsgnId: number
  redsgnNteTypeId: number;
  redsgnNteType: string;
  notes: string;
  cretdByCd: number;
  cretdBy: string;
  cretdDt: Date;

  constructor();
  constructor(redsgnNotesId?: number, redsgnId?: number, redsgnNteTypeId?: number,
    redsgnNteType?: string, notes?: string, cretdByCd?: number, cretdBy?: string, cretdDt?: Date)  {
    this.redsgnNotesId = redsgnNotesId || 0;
    this.redsgnId = redsgnId || 0;
    this.redsgnNteTypeId = redsgnNteTypeId || 0;
    this.redsgnNteType = redsgnNteType;
    this.notes = notes;
    this.cretdByCd = cretdByCd || 0;
    this.cretdBy = cretdBy;
    this.cretdDt = cretdDt || new Date();
    
  }
}
