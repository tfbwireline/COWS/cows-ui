export class ContactDetail {
  id: number;
  objId: number;
  objTypCd: string;
  hierLvlCd: string;
  hierId: string;
  roleId: number;
  roleNme: string;
  emailAdr: string;
  phnNbr: string;
  autoRfrshCd: boolean;
  emailCd: string;
  emailCdIds: number[];;
  emailCdTxt: string;
  recStusId: number;
  creatDt: Date;
  creatByUserId: number;
  modfdDt: Date;
  modfdByUserId: number;
  allowEdit: boolean;
  suprsEmail: boolean;

  constructor(id?: number, objId?: number, objTypCd?: string, hierLvlCd?: string, hierId?: string,
    roleId?: number, roleNme?: string, emailAdr?: string, phnNbr?: string, autoRfrshCd?: boolean,
    emailCd?: string, emailCdIds?: number[], emailCdTxt?: string, recStusId?: number,
    creatDt?: Date, creatByUserId?: number, modfdDt?: Date, modfdByUserId?: number, allowEdit?: boolean) {
    this.id = id;
    this.objId = objId;
    this.objTypCd = objTypCd;
    this.hierLvlCd = hierLvlCd;
    this.hierId = hierId;
    this.roleId = roleId;
    this.roleNme = roleNme;
    this.emailAdr = emailAdr;
    this.phnNbr = phnNbr;
    this.autoRfrshCd = autoRfrshCd;
    this.emailCd = emailCd;
    this.emailCdIds = emailCdIds || [];
    this.emailCdTxt = emailCdTxt;
    this.recStusId = recStusId;
    this.creatDt = creatDt;
    this.creatByUserId = creatByUserId;
    this.modfdDt = modfdDt;
    this.modfdByUserId = modfdByUserId;
    this.allowEdit = true;

  }
}
