
export class MDSEventDslSbicCustTrpt {
  dslSbicCustTrptId: number;
  eventId: number;
  primBkupCd: string;
  vndrPrvdrTrptCktId: string;
  vndrPrvdrNme: string;
  isWirelessCd: string;
  bwEsnMeid: string;
  scaNbr: string;
  ipAdr: string;
  subnetMaskAdr: string;
  nxthopGtwyAdr: string;
  sprintMngdCd: string;
  odieDevNme: string;
  creatDt: Date;
  mdsTrnsprtType: string;
  recStusId: number;
  modfdDt: Date;

  //constructor();
  constructor({ dslSbicCustTrptId = 0, eventId = 0, primBkupCd = "P", vndrPrvdrTrptCktId = null, vndrPrvdrNme = null, isWirelessCd = "N",
    bwEsnMeid = null, scaNbr = null, ipAdr = null, subnetMaskAdr = null, nxthopGtwyAdr = null, sprintMngdCd = "S", odieDevNme = "",
    creatDt = new Date(), mdsTrnsprtType = "", recStusId = 1, modfdDt = null }) {
    this.dslSbicCustTrptId = dslSbicCustTrptId;
    this.eventId = eventId
    this.primBkupCd = primBkupCd;
    this.vndrPrvdrTrptCktId = vndrPrvdrTrptCktId;
    this.vndrPrvdrNme = vndrPrvdrNme;
    this.isWirelessCd = isWirelessCd;
    this.bwEsnMeid = bwEsnMeid;
    this.scaNbr = scaNbr;
    this.ipAdr = ipAdr;
    this.subnetMaskAdr = subnetMaskAdr;
    this.nxthopGtwyAdr = nxthopGtwyAdr;
    this.sprintMngdCd = sprintMngdCd;
    this.odieDevNme = odieDevNme;
    this.creatDt = creatDt
    this.mdsTrnsprtType = mdsTrnsprtType;
    this.recStusId = recStusId
    this.modfdDt = modfdDt
  }
}
