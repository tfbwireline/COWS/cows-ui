import { AdEventAccessTag } from './ad-event-access-tag.model'

export class AdEvent {
  eventId: number
  eventStusId: number
  ftn: string
  charsId: string
  h1: string
  h6: string
  enhncSrvcId: number
  reqorUserId: number
  salsUserId: number
  pubEmailCcTxt: string
  cmpltdEmailCcTxt: string
  desCmntTxt: string
  cpeAtndCd: boolean
  docLinkTxt: string
  esclCd: boolean
  primReqDt: Date
  scndyReqDt: Date
  esclReasId: number
  strtTmst: Date
  extraDrtnTmeAmt: number
  endTmst: Date
  wrkflwStusId: number
  recStusId: number
  creatByUserId: number
  modfdByUserId: number
  modfdDt: Date
  creatDt: Date
  ipVerId: number
  cnfrcBrdgNbr: string
  cnfrcPinNbr: string
  eventDrtnInMinQty: number
  sowsEventId: number
  custNme: string
  custCntctNme: string
  custCntctPhnNbr: string
  custEmailAdr: string
  custCntctCellPhnNbr: string
  custCntctPgrNbr: string
  custCntctPgrPinNbr: string
  eventTitleTxt: string
  eventDes: string
  creatByUserDsplNme: string
  creatByUserAdid: string
  enhncSrvcNme: string
  eventStusDes: string
  wrkflwStusDes: string

  adEventAccsTag: AdEventAccessTag[]
  activators: number[]
  reviewerUserId: number;
  reviewerComments: string;

  activatorUserId: number;
  activatorComments: string;

  eventSucssActyIds: number[] = [];
  eventFailActyIds: number[] = [];
  preCfgConfgCode: string;
  profile: string;
  failCode: number;

  constructor();
  constructor(eventId?: number, eventStusId?: number, ftn?: string, charsId?: string, h1?: string,
    h6?: string, enhncSrvcId?: number, reqorUserId?: number, salsUserId?: number, pubEmailCcTxt?: string,
    cmpltdEmailCcTxt?: string, desCmntTxt?: string, cpeAtndCd?: boolean, docLinkTxt?: string,
    esclCd?: boolean, primReqDt?: Date, scndyReqDt?: Date, esclReasId?: number, strtTmst?: Date,
    extraDrtnTmeAmt?: number, endTmst?: Date, wrkflwStusId?: number, recStusId?: number,
    creatByUserId?: number, modfdByUserId?: number, modfdDt?: Date, creatDt?: Date, ipVerId?: number,
    cnfrcBrdgNbr?: string, cnfrcPinNbr?: string, eventDrtnInMinQty?: number, sowsEventId?: number,
    custNme?: string, custCntctNme?: string, custCntctPhnNbr?: string, custEmailAdr?: string,
    custCntctCellPhnNbr?: string, custCntctPgrNbr?: string, custCntctPgrPinNbr?: string,
    eventTitleTxt?: string, eventDes?: string, creatByUserDsplNme?: string, creatByUserAdid?: string,
    enhncSrvcNme?: string, eventStusDes?: string, wrkflwStusDes?: string, activators?: number[],
    adEventAccsTag?: AdEventAccessTag[], reviewerUserId?: number, reviewerComments?: string,
    activatorUserId?: number, activatorComments?: string, eventSucssActyIds?: number[],
    eventFailActyIds?: number[], preCfgConfgCode?: string, profile?: string, failCode?: number) {
    this.eventId = eventId || 0
    this.eventTitleTxt = eventTitleTxt || null
    this.eventDes = eventDes || null
    this.eventStusId = eventStusId || 0
    this.enhncSrvcId = enhncSrvcId || 0
    this.ftn = ftn || ""
    this.charsId = charsId || ""
    this.h1 = h1 || null
    this.h6 = h6 || ""
    this.custNme = custNme || ""
    this.custEmailAdr = custEmailAdr || ""
    this.custCntctPhnNbr = custCntctPhnNbr || ""
    this.custCntctCellPhnNbr = custCntctCellPhnNbr || ""
    this.custCntctNme = custCntctNme || ""
    this.custCntctPgrNbr = custCntctPgrNbr || ""
    this.custCntctPgrPinNbr = custCntctPgrPinNbr || ""
    this.reqorUserId = reqorUserId || 0
    this.cnfrcBrdgNbr = cnfrcBrdgNbr || null
    this.cnfrcPinNbr = cnfrcPinNbr || null
    this.salsUserId = salsUserId || 0
    this.pubEmailCcTxt = pubEmailCcTxt || ""
    this.cmpltdEmailCcTxt = cmpltdEmailCcTxt || ""
    this.desCmntTxt = desCmntTxt || ""
    this.cpeAtndCd = cpeAtndCd || false
    this.docLinkTxt = docLinkTxt || ""
    this.adEventAccsTag = adEventAccsTag || []
    this.esclCd = esclCd || false
    this.esclReasId = esclReasId || 0
    this.primReqDt = primReqDt || null
    this.scndyReqDt = scndyReqDt || null
    this.strtTmst = strtTmst || new Date()
    this.eventDrtnInMinQty = eventDrtnInMinQty || 60
    this.extraDrtnTmeAmt = extraDrtnTmeAmt || 0
    this.endTmst = endTmst || new Date()
    this.wrkflwStusId = wrkflwStusId || 0

    this.recStusId = recStusId || 0
    this.creatByUserId = creatByUserId || 0
    this.creatDt = creatDt || new Date()
    this.modfdByUserId = modfdByUserId || null
    this.modfdDt = modfdDt || new Date()
    this.creatByUserAdid = creatByUserAdid || null
    this.creatByUserDsplNme = creatByUserDsplNme || null

    this.ipVerId = ipVerId || null
    this.sowsEventId = sowsEventId || null
    this.enhncSrvcNme = enhncSrvcNme || null
    this.eventStusDes = eventStusDes || null
    this.wrkflwStusDes = wrkflwStusDes || null

    this.activators = activators || []
    this.reviewerUserId = reviewerUserId || 0;
    this.reviewerComments = reviewerComments;

    this.activatorUserId = activatorUserId || 0;
    this.activatorComments = activatorComments;

    this.eventSucssActyIds = eventSucssActyIds || [];
    this.eventFailActyIds = eventFailActyIds || [];
    this.preCfgConfgCode = preCfgConfgCode;
    this.profile = profile;
    this.failCode = failCode || 1;
  }
}
