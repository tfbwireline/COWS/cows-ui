import { Resource } from "./appointment-resource.model";

export class Appointment 
{
    text: string;
    startDate: any;
    endDate: any;
    allDay?: boolean;
    
    description: string;
    recurrenceRule: string;

    appointmentId: number;
    location: string;
    apptType: number;
    resource: Resource[];

  eventid: number;
  softAssigned: string;

  createdBy: string;
  modifiedBy: string;
  createdDate: Date;
  modifiedDate: Date;

}
