export class State {
  usStateName: string;
  usStateId: string;
  creatByUserId: number;
  modfdByUserId: number;
  modfdDt: Date;
  creatDt: Date;
  recStusId: number;
  tmeZoneId: string; 

  constructor();
  constructor(usStateName?: string, usStateId?: string, creatByUserId?: number, modfdByUserId?: number, modfdDt?: Date, creatDt?: Date, recStusId?: number, tmeZoneId?: string) {

    this.usStateName = usStateName;
    this.usStateId = usStateId;
    this.creatByUserId = creatByUserId || 0
    this.modfdByUserId = modfdByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.creatDt = creatDt || null;
    this.recStusId = recStusId;
    this.tmeZoneId = tmeZoneId;    
  }
}
