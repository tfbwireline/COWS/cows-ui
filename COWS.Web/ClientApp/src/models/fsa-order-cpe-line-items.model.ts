export class FsaOrderCpeLineItems {
  fsaCpeLineItemId: number
  ordrId: number
  srvcLineItemCd: string
  eqptTypeId: string
  eqptId: string
  mfrNme: string
  cntrcTypeId: string
  cntrcTermId: string
  instlnCd: string
  mntcCd: string
  creatDt: Date
  discPlNbr: string
  discFmsCktNbr: number
  discNwAdr: string
  mdsDes: string
  cmpntId: number
  lineItemCd: string
  fsaOrgnlInstlOrdrId: string
  cxrSrvcId: string
  cxrCktId: string
  ttrptAccsTypeCd: string
  ttrptSpdOfSrvcBdwdDes: string
  ttrptAccsTypeDes: string
  fsaOrdrVasId: number
  plsftRqstnNbr: string
  rqstnDt: Date
  prchOrdrNbr: string
  eqptItmRcvdDt: Date
  matlCd: string
  unitMsr: string
  manfPartCd: string
  vndrCd: string
  ordrQty: number
  unitPrice: string
  manfDiscntCd: string
  fmsCktNbr: number
  eqptRcvdByAdid: string
  cmplDt: Date
  poLnNbr: number
  rcvdQty: number
  psRcvdStus: number
  pid: string
  dropShp: string
  deviceId: string
  supplier: string
  ordrCmpntId: number
  itmStus: number
  cmpntFmly: string
  rltdCmpntId: number
  stusCd: string
  mfrPsId: string
  cpeReuseCd: boolean
  zsclrQuoteId: string
  instlDsgnDocNbr: string
  ttrptAccsArngtCd: string
  ttrptEncapCd: string
  ttrptFbrHandOffCd: string
  cxrAccsCd: string
  cktId: string
  ttrptQotXpirnDt: Date
  ttrptAccsTermDes: string
  ttrptAccsBdwdType: string
  plNbr: string
  portRtTypeCd: string
  tportIpVerTypeCd: string
  tportIpv4AdrPrvdrCd: string
  tportIpv4AdrQty: string
  tportIpv6AdrPrvdrCd: string
  tportIpv6AdrQty: string
  tportVlanQty: number
  tportEthrntNrfcIndcr: string
  tportCnctrTypeId: string
  tportCustRoutrTagTxt: string
  tportCustRoutrAutoNegotCd: string
  spaAccsIndcrDes: string
  tportDiaNocToNocTxt: string
  tportProjDes: string
  tportVndrQuoteId: string
  tportEthAccsTypeCd: string
  ttrptCosCd: string
  nidSerialNbr: string
  sprintMntdFlg: string

  constructor({ fsaCpeLineItemId = null, ordrId = null, srvcLineItemCd = null, eqptTypeId = null, eqptId = null, mfrNme = null, cntrcTypeId = null, cntrcTermId = null, instlnCd = null,
    mntcCd = null, creatDt = null, discPlNbr = null, discFmsCktNbr = null, discNwAdr = null, mdsDes = null, cmpntId = null, lineItemCd = null, fsaOrgnlInstlOrdrId = null, cxrSrvcId = null,
    cxrCktId = null, ttrptAccsTypeCd = null, ttrptSpdOfSrvcBdwdDes = null, ttrptAccsTypeDes = null, fsaOrdrVasId = null, plsftRqstnNbr = null, rqstnDt = null, prchOrdrNbr = null,
    eqptItmRcvdDt = null, matlCd = null, unitMsr = null, manfPartCd = null, vndrCd = null, ordrQty = null, unitPrice = null, manfDiscntCd = null, fmsCktNbr = null, eqptRcvdByAdid = null,
    cmplDt = null, poLnNbr = null, rcvdQty = null, psRcvdStus = null, pid = null, dropShp = null, deviceId = null, supplier = null, ordrCmpntId = null, itmStus = null, cmpntFmly = null,
    rltdCmpntId = null, stusCd = null, mfrPsId = null, cpeReuseCd = null, zsclrQuoteId = null, instlDsgnDocNbr = null, ttrptAccsArngtCd = null, ttrptEncapCd = null, ttrptFbrHandOffCd = null,
    cxrAccsCd = null, cktId = null, ttrptQotXpirnDt = null, ttrptAccsTermDes = null, ttrptAccsBdwdType = null, plNbr = null, portRtTypeCd = null, tportIpVerTypeCd = null, tportIpv4AdrPrvdrCd = null,
    tportIpv4AdrQty = null, tportIpv6AdrPrvdrCd = null, tportIpv6AdrQty = null, tportVlanQty = null, tportEthrntNrfcIndcr = null, tportCnctrTypeId = null, tportCustRoutrTagTxt = null,
    tportCustRoutrAutoNegotCd = null, spaAccsIndcrDes = null, tportDiaNocToNocTxt = null, tportProjDes = null, tportVndrQuoteId = null, tportEthAccsTypeCd = null, ttrptCosCd = null,
    nidSerialNbr = null, sprintMntdFlg = null }) {
    this.fsaCpeLineItemId = fsaCpeLineItemId
    this.ordrId = ordrId
    this.srvcLineItemCd = srvcLineItemCd
    this.eqptTypeId = eqptTypeId
    this.eqptId = eqptId
    this.mfrNme = mfrNme
    this.cntrcTypeId = cntrcTypeId
    this.cntrcTermId = cntrcTermId
    this.instlnCd = instlnCd
    this.mntcCd = mntcCd
    this.creatDt = creatDt
    this.discPlNbr = discPlNbr
    this.discFmsCktNbr = discFmsCktNbr
    this.discNwAdr = discNwAdr
    this.mdsDes = mdsDes
    this.cmpntId = cmpntId
    this.lineItemCd = lineItemCd
    this.fsaOrgnlInstlOrdrId = fsaOrgnlInstlOrdrId
    this.cxrSrvcId = cxrSrvcId
    this.cxrCktId = cxrCktId
    this.ttrptAccsTypeCd = ttrptAccsTypeCd
    this.ttrptSpdOfSrvcBdwdDes = ttrptSpdOfSrvcBdwdDes
    this.ttrptAccsTypeDes = ttrptAccsTypeDes
    this.fsaOrdrVasId = fsaOrdrVasId
    this.plsftRqstnNbr = plsftRqstnNbr
    this.rqstnDt = rqstnDt
    this.prchOrdrNbr = prchOrdrNbr
    this.eqptItmRcvdDt = eqptItmRcvdDt
    this.matlCd = matlCd
    this.unitMsr = unitMsr
    this.manfPartCd = manfPartCd
    this.vndrCd = vndrCd
    this.ordrQty = ordrQty
    this.unitPrice = unitPrice
    this.manfDiscntCd = manfDiscntCd
    this.fmsCktNbr = fmsCktNbr
    this.eqptRcvdByAdid = eqptRcvdByAdid
    this.cmplDt = cmplDt
    this.poLnNbr = poLnNbr
    this.rcvdQty = rcvdQty
    this.psRcvdStus = psRcvdStus
    this.pid = pid
    this.dropShp = dropShp
    this.deviceId = deviceId
    this.supplier = supplier
    this.ordrCmpntId = ordrCmpntId
    this.itmStus = itmStus
    this.cmpntFmly = cmpntFmly
    this.rltdCmpntId = rltdCmpntId
    this.stusCd = stusCd
    this.mfrPsId = mfrPsId
    this.cpeReuseCd = cpeReuseCd
    this.zsclrQuoteId = zsclrQuoteId
    this.instlDsgnDocNbr = instlDsgnDocNbr
    this.ttrptAccsArngtCd = ttrptAccsArngtCd
    this.ttrptEncapCd = ttrptEncapCd
    this.ttrptFbrHandOffCd = ttrptFbrHandOffCd
    this.cxrAccsCd = cxrAccsCd
    this.cktId = cktId
    this.ttrptQotXpirnDt = ttrptQotXpirnDt
    this.ttrptAccsTermDes = ttrptAccsTermDes
    this.ttrptAccsBdwdType = ttrptAccsBdwdType
    this.plNbr = plNbr
    this.portRtTypeCd = portRtTypeCd
    this.tportIpVerTypeCd = tportIpVerTypeCd
    this.tportIpv4AdrPrvdrCd = tportIpv4AdrPrvdrCd
    this.tportIpv4AdrQty = tportIpv4AdrQty
    this.tportIpv6AdrPrvdrCd = tportIpv6AdrPrvdrCd
    this.tportIpv6AdrQty = tportIpv6AdrQty
    this.tportVlanQty = tportVlanQty
    this.tportEthrntNrfcIndcr = tportEthrntNrfcIndcr
    this.tportCnctrTypeId = tportCnctrTypeId
    this.tportCustRoutrTagTxt = tportCustRoutrTagTxt
    this.tportCustRoutrAutoNegotCd = tportCustRoutrAutoNegotCd
    this.spaAccsIndcrDes = spaAccsIndcrDes
    this.tportDiaNocToNocTxt = tportDiaNocToNocTxt
    this.tportProjDes = tportProjDes
    this.tportVndrQuoteId = tportVndrQuoteId
    this.tportEthAccsTypeCd = tportEthAccsTypeCd
    this.ttrptCosCd = ttrptCosCd
    this.nidSerialNbr = nidSerialNbr
    this.sprintMntdFlg = sprintMntdFlg
  }
}
