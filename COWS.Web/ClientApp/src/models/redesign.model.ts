import { RedesignDevicesInfo, RedesignNotes, RedesignEmailNotification, RedesignDoc, ContactDetail } from ".";

export class Redesign {
  redsgnId: number;
  redsgnNbr: string;
  redsgnCatId: number;
  h1Cd: string;

  custNme: string;
  custEmailAdr: string;
  odieCustId: string;
  nteAssigned: string;
  pmAssigned: string;
  sdeAsnNme: string;
  neAsnNme: string;
  nteAssignedFullName: string;
  pmAssignedFullName: string;
  sdeAsnFullNme: string;
  neAsnFullNme: string;

  redsgnTypeId: number;
  isSdwan: boolean;

  entireNwCkdId: boolean;
  redsgnDevStusId: number;

  csgLvlId: number;
  cretdByCd: number;
  cretdDt: Date;
  modfdByCd: number;
  modfdDt: Date;
  submitDt: Date;
  modSubmitDt: Date;
  slaDt: Date;
  
  dsgnDocLocTxt: string;
  dsgnDocNbrBpm: string;
  sowsFoldrPathNme: string;
  stusId: number;
  suppressEmail: boolean;
  
  approvNbr: string;
  costAmt?: number;
  exprtnDt: Date;
  extXpirnFlgCd: boolean;

  billOvrrdnCd: boolean;
  billOvrrdnAmt?: number;
  nteLvlEffortAmt?: number;
  pmLvlEffortAmt?: number;
  neLvlEffortAmt?: number;
  nteOtLvlEffortAmt?: number;
  pmOtLvlEffortAmt?: number;
  neOtLvlEffortAmt?: number;
  spsLvlEffortAmt?: number;
  spsOtLvlEffortAmt?: number;

  ciscSmrtLicCd?: boolean;
  smrtAccntDmn: string;
  vrtlAccnt: string;
  mssImplEstAmt?: number;
  bpmRedsgnNbr?: string;

  description: string;
  caveats: string;
  emailNotifications: string;
  oldStusId: number;
  statusDesc: string;
  newDevice: string;
  isBillable: boolean;
  selectedOdieDevices: string[];
  odieRspnInfoReqId: number;

  devices: RedesignDevicesInfo[];
  docs: RedesignDoc[];
  notes: RedesignNotes[];
  emails: RedesignEmailNotification[];
  contactDetails: ContactDetail[];

  systemCreatedRedesign: boolean = false;
  addlDocTxt: string

  constructor();
  constructor(redsgnId?: number, redsgnNbr?: string, redsgnCatId?: number,
    h1Cd?: string, custNme?: string, custEmailAdr?: string, odieCustId?: string,
    nteAssigned?: string, pmAssigned?: string, sdeAsnNme?: string, neAsnNme?: string,
    nteAssignedFullName?: string, pmAssignedFullName?: string,
    sdeAsnFullNme?: string, neAsnFullNme?: string,
    redsgnTypeId?: number, isSdwan?: boolean, entireNwCkdId?: boolean, redsgnDevStusId?: number,
    csgLvlId?: number, cretdByCd?: number, cretdDt?: Date, modfdByCd?: number, modfdDt?: Date,
    submitDt?: Date, modSubmitDt?: Date, slaDt?: Date, dsgnDocLocTxt?: string,
    dsgnDocNbrBpm?: string, sowsFoldrPathNme?: string, stusId?: number, suppressEmail?: boolean,
    approvNbr?: string, costAmt?: number, exprtnDt?: Date, extXpirnFlgCd?: boolean,
    billOvrrdnCd?: boolean, billOvrrdnAmt?: number, nteLvlEffortAmt?: number,
    pmLvlEffortAmt?: number, neLvlEffortAmt?: number, spsLvlEffortAmt?: number, 
    nteOtLvlEffortAmt?: number, pmOtLvlEffortAmt?: number, neOtLvlEffortAmt?: number,
    spsOtLvlEffortAmt?: number, description?: string, caveats?: string, statusDesc?: string,
    newDevice?: string, oldStusId?: number, emailNotifications?: string, isBillable?: boolean,
    selectedOdieDevices?: string[], odieRspnInfoReqId?: number, devices?: RedesignDevicesInfo[],
    docs?: RedesignDoc[], notes?: RedesignNotes[], emails?: RedesignEmailNotification[], contactDetails?: ContactDetail[],
    ciscSmrtLicCd?: boolean, smrtAccntDmn?: string, vrtlAccnt?: string, mssImplEstAmt?: number,
    systemCreatedRedesign?: boolean, bpmRedsgnNbr?: string, addlDocTxt?: string) {
    this.redsgnId = redsgnId || 0;
    this.redsgnNbr = redsgnNbr;
    this.redsgnCatId = redsgnCatId || 0;
    this.h1Cd = h1Cd;
    this.custNme = custNme;
    this.custEmailAdr = custEmailAdr;
    this.odieCustId = odieCustId;
    this.nteAssigned = nteAssigned;
    this.pmAssigned = pmAssigned;
    this.sdeAsnNme = sdeAsnNme;
    this.neAsnNme = neAsnNme;
    this.nteAssignedFullName = nteAssignedFullName;
    this.pmAssignedFullName = pmAssignedFullName;
    this.sdeAsnFullNme = sdeAsnFullNme;
    this.neAsnFullNme = neAsnFullNme;
    this.redsgnTypeId = redsgnTypeId || 0;
    this.isSdwan = isSdwan || false;
    this.ciscSmrtLicCd = ciscSmrtLicCd || false;
    this.smrtAccntDmn = smrtAccntDmn;
    this.vrtlAccnt = vrtlAccnt;
    this.mssImplEstAmt = mssImplEstAmt || null;
    this.entireNwCkdId = entireNwCkdId || false;
    this.redsgnDevStusId = redsgnDevStusId || 0;
    this.csgLvlId = csgLvlId || 0;
    this.cretdByCd = cretdByCd || 0;
    this.cretdDt = cretdDt || new Date();
    this.modfdByCd = modfdByCd || 0;
    this.modfdDt = modfdDt || null;
    this.submitDt = submitDt || null;
    this.modSubmitDt = modSubmitDt || null;
    this.slaDt = slaDt || null;
    this.dsgnDocLocTxt = dsgnDocLocTxt;
    this.dsgnDocNbrBpm = dsgnDocNbrBpm;
    this.sowsFoldrPathNme = sowsFoldrPathNme;
    this.stusId = stusId || 0;
    this.suppressEmail = suppressEmail || false;
    this.approvNbr = approvNbr;
    this.costAmt = costAmt || null;
    this.exprtnDt = exprtnDt || null;
    this.extXpirnFlgCd = extXpirnFlgCd || false;
    this.billOvrrdnCd = billOvrrdnCd || false;
    this.billOvrrdnAmt = billOvrrdnAmt || null;
    this.nteLvlEffortAmt = nteLvlEffortAmt || null;
    this.pmLvlEffortAmt = pmLvlEffortAmt || null;
    this.neLvlEffortAmt = neLvlEffortAmt || null;
    this.nteOtLvlEffortAmt = nteOtLvlEffortAmt || null;
    this.pmOtLvlEffortAmt = pmOtLvlEffortAmt || null;
    this.neOtLvlEffortAmt = neOtLvlEffortAmt || null;
    this.spsLvlEffortAmt = spsLvlEffortAmt || null;
    this.spsOtLvlEffortAmt = spsOtLvlEffortAmt || null;
    this.description = description;
    this.caveats = caveats;
    this.statusDesc = statusDesc;
    this.newDevice = newDevice;
    this.oldStusId = oldStusId || 0;
    this.isBillable = isBillable || false;
    this.emailNotifications = emailNotifications;
    this.selectedOdieDevices = selectedOdieDevices || [];
    this.odieRspnInfoReqId = odieRspnInfoReqId || 0;

    this.devices = devices || [];
    this.docs = docs || [];
    this.notes = notes || [];
    this.emails = emails || [];
    this.contactDetails = contactDetails || [];

    this.systemCreatedRedesign = systemCreatedRedesign || false;
    this.bpmRedsgnNbr = bpmRedsgnNbr;
    this.addlDocTxt = addlDocTxt;
  }
}
