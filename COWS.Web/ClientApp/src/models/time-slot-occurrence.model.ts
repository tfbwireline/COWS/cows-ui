export class TimeSlotOccurence {
  timeSlotID: number
  curFilledTimeSlots: number
  maxTimeSlots: number
  totalTimeSlots: number
  day: string
  dayDisp: string;
  rsrcAvlbltyID: number
  curAvailTimeSlots: number
  isOverbooked: string
  displayTimeSlot: string
  eventTypeDes: string
  rAP: number
  timeSlotEnd: string
  timeSlotStart: string
  eventTypeID: number

  constructor() {

  }
}
