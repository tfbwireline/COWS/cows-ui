export interface CannedReport {
    report: Report;
    file:   File | null;
}

export interface File{
    name:        string;
    size:        number;
    dateCreated: Date;
    namePattern: string;
    fullPath:    string;
}

export interface Report {
    reportTypeId:        number;
    name:                string;
    description:         string;
    id:                  number;
    groupId:             number;
    intervalCode:        string;
    scheduleName:        string;
    intervalDescription: string;
    groupName:           string;
    ownerName:           string;
    isEncrypted:         number;
    folderTypeCode:      string;
}
