//import { AdEventAccessTag } from './ad-event-access-tag.model'

export class NccoOrder {
  //eventId: number;
  //m5OrdrNbr: string;
  //h1: string;
  //siteId: string;
  //h6: string;
  //eventCsgLvlId: number;
  //charsId: string;
  //teamPdlNme: string;
  //siptDocLocTxt: string;
  //custReqStDt: Date;
  //custReqEndDt: Date;
  //wrkDes: string;
  //reqorUserId: number;
  //ntwkEngrId: number;
  //ntwkTechEngrId: number;
  //pmId: number;
  //eventStusId: number;
  //wrkflwStusId: number;
  //creatByUserId: number;
  //modfdByUserId: number;
  //modfdDt: Date;
  //creatDt: Date;
  //recStusId: number;
  //recStatus: boolean;
  //siptProdTypeId: number;
  //prjId: string;
  //focDt: Date;
  //usIntlCd: string;
  //siptDesgnDoc: string;
  //streetAdr: string;
  //eventTitleTxt: string;
  //custNme: string;
  //siteCntctNme: string;
  //siteCntctPhnNbr: string;
  //siteCntctHrNme: string;
  //siteCntctEmailAdr: string;
  //siteAdr: string;
  //flrBldgNme: string;
  //ctyNme: string;
  //sttPrvnNme: string;
  //ctryRgnNme: string;
  //zipCd: string;
  //createdByAdId: string;
  //modifiedByAdId: string;
  //createdByDisplayName: string;
  //modifiedByDisplayName: string;
  //siptEventActyIds: number[];
  //siptEventTollTypeIds: number[];
  //siptReltdOrdrIds: number[];
  //siptReltdOrdrNos: string[];
  //reviewCmntTxt: string;


  ordrId: number;
  siteCityNme: string;
  ctryCd: string;
  h5AcctNbr: number;
  solNme: string;
  oeNme: string;
  sotsNbr: string;
  billCycNbr: string;
  prodTypeId: number;
  ordrTypeId: number;
  cwdDt: Date;
  ccsDt: Date;
  emailAdr: string;
  cmntTxt: string;
  creatDt: Date;
  creatByUserId: number;
  plNbr: string;
  ordrByLassieCd: string;
  vndrCd: string;
  custNme: string;
  prodType: string;
  ordrType: string;
  ctry: string;
  ordrStatus: string;
  csgLvlId: number;
  PLNumberId: number[];
  PLNumber: string[];
  MyPLNumber: string;
  constructor();
  constructor(


    ordrId?: number,
    siteCityNme?: string,
    ctryCd?: string,
    h5AcctNbr?: number,
    solNme?: string,
    oeNme?: string,
    sotsNbr?: string,
    billCycNbr?: string,
    prodTypeId?: number,
    ordrTypeId?: number,
    cwdDt?: Date,
    ccsDt?: Date,
    emailAdr?: string,
    cmntTxt?: string,
    creatDt?: Date,
    creatByUserId?: number,
    plNbr?: string,
    ordrByLassieCd?: string,
    vndrCd?: string,
    custNme?: string,
    prodType?: string,
    ordrType?: string,
    ctry?: string,
    ordrStatus?: string,
    csgLvlId?: number,
    PLNumberId?: number[],
    PLNumber?: string[],
    MyPLNumber?: string
  ) {

    this.ordrId = ordrId||0
    this.siteCityNme = siteCityNme||null
    this.ctryCd = ctryCd || null
    this.h5AcctNbr = h5AcctNbr || 0
    this.solNme = solNme || null
    this.oeNme = oeNme || null
    this.sotsNbr = sotsNbr || null
    this.billCycNbr = billCycNbr || null
    this.prodTypeId = prodTypeId || 0
    this.ordrTypeId = ordrTypeId || 0
    this.cwdDt = cwdDt || new Date()
    this.ccsDt = ccsDt || new Date()
    this.emailAdr = emailAdr || null
    this.cmntTxt = cmntTxt || null
    this.creatDt = creatDt || new Date()
    this.creatByUserId = creatByUserId || 0
    this.plNbr = plNbr || null
    this.ordrByLassieCd = ordrByLassieCd || "N"
    this.vndrCd = vndrCd || null
    this.custNme = custNme || null
    this.prodType = prodType || null
    this.ordrType = ordrType || null
    this.ctry = ctry || null
    this.ordrStatus = ordrStatus || null
    this.csgLvlId = csgLvlId || 0
    this.PLNumberId = PLNumberId || []
    this.PLNumber = PLNumber || []
    this.MyPLNumber = MyPLNumber || null
  }
}
