export class User {
  constructor(public userId?: number, public userAdid?: string, public fullNme?: string,
    public dsplNme?: string, public emailAdr?: string, public menuPref?: string,
    public recStusId?: boolean, public mgrAdid?: string, public cellPhnNbr?: string,
    public phnNbr?: string, public pgrNbr?: string, public pgrPinNbr?: string,
    public ctyNme?: string, public sttCd?: string) { }
}

export interface IUserResponse {
    map(arg0: (user: any) => User): any;
  results: User[];
}

export class LoggedInUser {
  user: User
  userId: number
  adid: string
  fullName: string
  isAuthorize: boolean
  profiles: string[]
  profilesDisp: string[]
  profileIds: number[]
  activeEventProfile: string
  activeOrderProfile: string
  phnNbr: string
  cellPhnNbr: string
  //isSideNav: boolean = true

  constructor();
  constructor(user?: User, userId?: number, adid?: string, fullName?: string, isAuthorize?: boolean, profiles?: string[],
    profilesDisp?: string[], activeEventProfile?: string, activeOrderProfile?: string, profileIds?: number[],
    phnNbr?: string, cellPhnNbr?: string) {
    this.user = user || null
    this.userId = userId || 0
    this.adid = adid || null
    this.fullName = fullName || null
    this.phnNbr = phnNbr || null
    this.cellPhnNbr = cellPhnNbr || null
    this.isAuthorize = isAuthorize || false
    this.profiles = profiles || []
    this.profilesDisp = profilesDisp || []
    this.profileIds = profileIds || []
    this.activeEventProfile = activeEventProfile || "ReadOnly"
    this.activeOrderProfile = activeOrderProfile || "ReadOnly"
  }
}
