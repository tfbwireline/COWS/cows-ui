export class VendorOrderForm {
  vndrOrdrFormId: number;
  vndrOrdrId: number;
  fileNme: string;
  fileCntnt: Int8Array;
  fileSizeQty: number;
  fileSizeQtyStr: string;
  creatDt: Date;
  creatByUserId: number;
  recStusId: number;
  tmpltCd: boolean;
  tmpltId: number;
  creatByUserFullName: string;
  base64string: any;

  constructor({ vndrOrdrFormId = 0, vndrOrdrId = 0, fileNme = "", fileCntnt = null, fileSizeQty = 0, fileSizeQtyStr = '', recStusId = 1,
    creatDt = new Date(), creatByUserId = 1, tmpltCd = false, tmpltId = null, creatByUserFullName = "", base64string = null }) {
    this.vndrOrdrFormId = vndrOrdrFormId
    this.vndrOrdrId = vndrOrdrId
    this.fileNme = fileNme
    this.fileCntnt = fileCntnt
    this.fileSizeQty = fileSizeQty
    this.fileSizeQtyStr = fileSizeQtyStr
    this.creatDt = creatDt
    this.creatByUserId = creatByUserId
    this.recStusId = recStusId
    this.tmpltCd = tmpltCd
    this.tmpltId = tmpltId
    this.creatByUserFullName = creatByUserFullName
    this.base64string = base64string
  }
}
