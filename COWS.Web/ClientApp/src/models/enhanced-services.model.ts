
export class EnhncSrvc {
  enhncSrvcId: number;
  enhncSrvcNme: string;
  eventTypeId: number;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;
  modfdDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;

  constructor();
  constructor(enhncSrvcId?: number, enhncSrvcNme?: string, eventTypeId?: number,
    recStusId?: number, recStatus?: boolean,
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string,
    modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
    this.enhncSrvcId = enhncSrvcId || 0;
    this.enhncSrvcNme = enhncSrvcNme;
    this.eventTypeId = eventTypeId || 0;
    //this.eventTypeNme = eventTypeNme;
    //this.mdsSrvcTierCd = mdsSrvcTierCd;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.modifiedByAdId = modifiedByAdId;
  }
}
