
export class EventDevSrvcMgmt {
  eventDevSrvcMgmtId: number;
  //devSrvcMgmtID: number;
  eventId: number;
  deviceId: string;
  mnsOrdrId: number;
  mnsOrdrNbr: string;
  odieDevNme: string;
  mnsSrvcTierId: number;
  mnsSrvcTier: string;
  cmpntId: string;
  cmpntStus: string;
  cmpntType: string;
  cmpntNme: string;
  sendToOdie: boolean;
  recStusId: number
  creatDt: Date;

  constructor();
  constructor(eventDevSrvcMgmtId?: number, eventId?: number, deviceId?: string, mnsOrdrId?: number,
    mnsOrdrNbr?: string, odieDevNme?: string, mnsSrvcTierId?: number, mnsSrvcTier?: string,
    cmpntId?: string, cmpntStus?: string, cmpntType?: string, cmpntNme?: string,
    sendToOdie?: boolean, recStusId?: number, creatDt?: Date) {
    this.eventDevSrvcMgmtId = eventDevSrvcMgmtId || 0;
    this.eventId = eventId || 0;
    this.deviceId = deviceId;
    this.mnsOrdrId = mnsOrdrId || 0;
    this.mnsOrdrNbr = mnsOrdrNbr;
    this.odieDevNme = odieDevNme;
    this.mnsSrvcTierId = mnsSrvcTierId || 0;
    this.mnsSrvcTier = mnsSrvcTier;
    this.cmpntId = cmpntId;
    this.cmpntStus = cmpntStus;
    this.cmpntType = cmpntType;
    this.cmpntNme = cmpntNme;
    this.sendToOdie = sendToOdie || false;
    this.recStusId = recStusId || 0;
    this.creatDt = creatDt;
  }
}
