
export class DeviceModel {
  devModelId: number;
  devModelNme: string;
  minDrtnTmeReqrAmt: number;
  manfId: number;
  manfModelName: string;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;
  modfdDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;

  isAlreadyInUse: boolean;

  constructor();
  constructor(devModelId?: number, devModelNme?: string, minDrtnTmeReqrAmt?: number,
    manfId?: number, manfModelName?: string, recStusId?: number, recStatus?: boolean,
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string,
    modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
    this.devModelId = devModelId || 0;
    this.devModelNme = devModelNme;
    this.minDrtnTmeReqrAmt = minDrtnTmeReqrAmt || 0;
    this.manfId = manfId || 0;
    this.manfModelName = manfModelName;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.modifiedByAdId = modifiedByAdId;
  }
}
