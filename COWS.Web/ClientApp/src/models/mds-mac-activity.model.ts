
export class MDSMACActivity {
  mdsMacActyId: number;
  mdsMacActyNme: string;
  minDrtnTmeReqrAmt: number;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;
  modfdDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;

  constructor();
  constructor(mdsMacActyId?: number, mdsMacActyNme?: string, minDrtnTmeReqrAmt?: number,
    recStusId?: number, recStatus?: boolean,
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string,
    modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
    this.mdsMacActyId = mdsMacActyId || 0;
    this.mdsMacActyNme = mdsMacActyNme;
    this.minDrtnTmeReqrAmt = minDrtnTmeReqrAmt || 0;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt || new Date();
    this.modfdByUserId = modfdByUserId || 0;
    this.modifiedByAdId = modifiedByAdId;
  }
}
