export class MplsEventAccessTag {
  mplsEventAccsTagId: number = 0;
  eventId: number;
  locCtyNme: string;
  locSttNme: string;
  vasSolNbr: string;
  trsNet449Adr: string;
  mplsAccsBdwdId: number;
  mnsSipAdr: string;
  mnsOffrgRoutrId: number;
  mnsRoutgTypeId: string;
  mnsPrfmcRptId: string;
  ctryCd: string;
  plDalCktNbr: string;
  trnsprtOeFtnNbr: string;
  creatDt: Date;

  constructor();

  //constructor(mplsEventAccsTagId?: number, eventId?: number, locCtyNme?: string, locSttNme?: string,
  //  vasSolNbr?: string, trsNet449Adr?: string, mplsAccsBdwdId?: number, mnsSipAdr?: string,
  //  mnsOffrgRoutrId?: number, mnsRoutgTypeId?: string, mnsPrfmcRptId?: string, ctryCd?: string,
  //  plDalCktNbr?: string, trnsprtOeFtnNbr?: string);

  //constructor(mplsEventAccsTagId?: number, eventId?: number, locCtyNme?: string, locSttNme?: string,
  //  vasSolNbr?: string, trsNet449Adr?: string, mplsAccsBdwdId?: number, mnsSipAdr?: string,
  //  mnsOffrgRoutrId?: number, mnsRoutgTypeId?: string, mnsPrfmcRptId?: string, ctryCd?: string,
  //  plDalCktNbr?: string, trnsprtOeFtnNbr?: string, creatDt?: Date);

  constructor(mplsEventAccsTagId?: number, eventId?: number, locCtyNme?: string, locSttNme?: string,
    vasSolNbr?: string, trsNet449Adr?: string, mplsAccsBdwdId?: number, mnsSipAdr?: string,
    mnsOffrgRoutrId?: number, mnsRoutgTypeId?: string, mnsPrfmcRptId?: string, ctryCd?: string,
    plDalCktNbr?: string, trnsprtOeFtnNbr?: string, creatDt?: Date) {
    this.mplsEventAccsTagId = mplsEventAccsTagId || 0;
    this.eventId = eventId || 0;
    this.locCtyNme = locCtyNme;
    this.locSttNme = locSttNme;
    this.vasSolNbr = vasSolNbr;
    this.trsNet449Adr = trsNet449Adr;
    this.mplsAccsBdwdId = mplsAccsBdwdId || null;
    this.mnsSipAdr = mnsSipAdr;
    this.mnsOffrgRoutrId = mnsOffrgRoutrId || null;
    this.mnsRoutgTypeId = mnsRoutgTypeId || null;
    this.mnsPrfmcRptId = mnsPrfmcRptId || null;
    this.ctryCd = ctryCd;
    this.plDalCktNbr = plDalCktNbr;
    this.trnsprtOeFtnNbr = trnsprtOeFtnNbr;
    this.creatDt = creatDt || null;
  }
}
