export class UcaasEventOdieDevice {
  ucaaSEventOdieDevId: number;
  eventId: number;
  rdsnNbr: string;
  rdsnExpDt: Date;
  odieDevNme: string;
  redsgnDevId: number;
  devModelId: number;
  manfId: number;
  creatDt: Date;

  //constructor();
  //constructor(ucaaSEventOdieDevId?: number, eventId?: number, rdsnNbr?: string, rdsnExpDt?: Date,
  //  odieDevNme?: string, redsgnDevId?: number, devModelId?: number, manfId?: number, creatDt?: Date)
  constructor({ ucaaSEventOdieDevId = 0, eventId = 0, rdsnNbr = '', rdsnExpDt = new Date(),
    odieDevNme = '', redsgnDevId = 0, devModelId = 0, manfId = 0, creatDt = new Date()}){
    this.ucaaSEventOdieDevId = ucaaSEventOdieDevId || 0;
    this.eventId = eventId || 0;
    this.rdsnNbr = rdsnNbr;
    this.rdsnExpDt = rdsnExpDt || null;
    this.odieDevNme = odieDevNme;
    this.redsgnDevId = redsgnDevId || 0;
    this.devModelId = devModelId || 0;
    this.manfId = manfId || 0;
    this.creatDt = creatDt || new Date();
  }
}
