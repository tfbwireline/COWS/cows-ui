import { User } from "./user.model";
import { EventCpeDev } from "./event-cpe-dev.model";
import { EntityBase } from "./entity-base.model";
import { Event } from './event.model';
import { UcaasEventOdieDevice } from "./ucaas-event-odie-device.model";
import { UcaasEventBilling } from "./ucaas-event-billing.model";
import { EventDeviceCompletion } from "./event-device-completion.model";
import { EventDeviceMgmtService } from "../services";
import { EventDiscoDev } from "./event-disco-dev.model";
import { EventDevSrvcMgmt } from "./event-dev-srvc-mgmt.model";

export class UcaasEvent {
  eventId: number;
  event: Event;
  eventStusId: number;
  eventTitleTxt: string;
  h1: string;
  custNme: string;
  charsId: string;
  custAcctTeamPdlNme: string;
  custSowLocTxt: string;
  ucaaSProdTypeId: number;
  ucaaSPlanTypeId: number;
  ucaaSActyTypeId: number;
  ucaaSDesgnDoc: string;
  shrtDes: string;
  
  h6: string;
  ccd: any;
  siteId: string;
  siteAdr: string;

  usIntlCd: string;
  instlSitePocNme: string;
  instlSitePocIntlPhnCd: string;
  instlSitePocPhnNbr: string;
  instlSitePocIntlCellPhnCd: string;
  instlSitePocCellPhnNbr: string;
  srvcAssrnPocNme: string;
  srvcAssrnPocIntlPhnCd: string;
  srvcAssrnPocPhnNbr: string;
  srvcAssrnPocIntlCellPhnCd: string;
  srvcAssrnPocCellPhnNbr: string;

  streetAdr: string;
  sttPrvnNme: string;
  flrBldgNme: string;
  ctryRgnNme: string;
  ctyNme: string;
  zipCd: string;

  sprintCpeNcrId: number;
  cpeDspchEmailAdr: string;
  cpeDspchCmntTxt: string;

  discMgmtCd: string;
  fullCustDiscCd: boolean;
  fullCustDiscReasTxt: string;

  esclCd: boolean;
  esclReasId: number;
  primReqDt: any;
  scndyReqDt: any;
  busJustnTxt: string;
  cnfrcBrdgId: number;
  cnfrcBrdgNbr: string;
  cnfrcPinNbr: string;
  onlineMeetingAdr: string;
  pubEmailCcTxt: string;
  cmpltdEmailCcTxt: string;

  reqorUserId: number;
  reqorUser: User;
  reqorUserCellPhnNbr: string;
  cmntTxt: string;

  strtTmst: any;
  endTmst: any;
  extraDrtnTmeAmt: number;
  eventDrtnInMinQty: number;
  mnsPmId: string;
  failReasId: number;
  wrkflwStusId: number;
  suppressEmail: boolean;
  oldEventStusId: number;
  entityBase: EntityBase;

  ucaasEventBilling: UcaasEventBilling[] = [];
  ucaasEventOdieDevice: UcaasEventOdieDevice[] = [];
  eventCpeDev: EventCpeDev[] = [];
  eventDeviceCompletion: EventDeviceCompletion[] = [];
  eventDevServiceMgmt: EventDevSrvcMgmt[] = [];
  eventDiscoDev: EventDiscoDev[] = [];

  //additional for UI - TODO: look for uses
  orderCode: number;
  activators: number[] = [];

  constructor();
  constructor(eventId?: number, event?: Event, eventStusId?: number, eventTitleTxt?: string, h1?: string,
    custNme?: string, charsId?: string, custAcctTeamPdlNme?: string, custSowLocTxt?: string,
    ucaaSProdTypeId?: number, ucaaSPlanTypeId?: number, ucaaSActyTypeId?: number,
    ucaaSDesgnDoc?: string, shrtDes?: string, h6?: string, ccd?: Date, siteId?: string,
    siteAdr?: string, usIntlCd?: string, instlSitePocNme?: string,
    instlSitePocIntlPhnCd?: string, instlSitePocPhnNbr?: string, instlSitePocIntlCellPhnCd?: string,
    instlSitePocCellPhnNbr?: string, srvcAssrnPocNme?: string, srvcAssrnPocIntlPhnCd?: string,
    srvcAssrnPocPhnNbr?: string, srvcAssrnPocIntlCellPhnCd?: string, srvcAssrnPocCellPhnNbr?: string,
    sprintCpeNcrId?: number, cpeDspchEmailAdr?: string, cpeDspchCmntTxt?: string,
    discMgmtCd?: string, fullCustDiscCd?: boolean, fullCustDiscReasTxt?: string,
    esclCd?: boolean, esclReasId?: number, primReqDt?: Date, scndyReqDt?: Date, busJustnTxt?: string,
    cnfrcBrdgId?: number, cnfrcBrdgNbr?: string, cnfrcPinNbr?: string, onlineMeetingAdr?: string,
    pubEmailCcTxt?: string, cmpltdEmailCcTxt?: string, reqorUserId?: number, reqorUser?: User,
    reqorUserCellPhnNbr?: string, cmntTxt?: string, strtTmst?: Date, endTmst?: Date,
    extraDrtnTmeAmt?: number, eventDrtnInMinQty?: number, mnsPmId?: string, failReasId?: number,
    wrkflwStusId?: number, suppressEmail?: boolean, oldEventStusId?: number, entityBase?: EntityBase,
    ucaasEventBilling?: UcaasEventBilling[], ucaasEventOdieDevice?: UcaasEventOdieDevice[],
    eventCpeDev?: EventCpeDev[], eventDeviceCompletion?: EventDeviceCompletion[],
    eventDevServiceMgmt?: EventDevSrvcMgmt[], eventDiscoDev?: EventDiscoDev[], orderCode?: number,
    activators?: number[]) {
    this.eventId = eventId || 0;
    this.event = event || new Event();
    this.eventStusId = eventStusId || 0;
    this.eventTitleTxt = eventTitleTxt;
    this.h1 = h1;
    this.custNme = custNme;
    this.charsId = charsId;
    this.custAcctTeamPdlNme = custAcctTeamPdlNme;
    this.custSowLocTxt = custSowLocTxt;
    this.ucaaSProdTypeId = ucaaSProdTypeId || 0;
    this.ucaaSPlanTypeId = ucaaSPlanTypeId || 0;
    this.ucaaSActyTypeId = ucaaSActyTypeId || 0;
    this.ucaaSDesgnDoc = ucaaSDesgnDoc;
    this.shrtDes = shrtDes;

    this.h6 = h6;
    this.ccd = ccd || null;
    this.siteId = siteId;
    this.siteAdr = siteAdr;

    this.usIntlCd = usIntlCd;
    this.instlSitePocNme = instlSitePocNme;
    this.instlSitePocIntlPhnCd = instlSitePocIntlPhnCd;
    this.instlSitePocPhnNbr = instlSitePocPhnNbr;
    this.instlSitePocIntlCellPhnCd = instlSitePocIntlCellPhnCd;
    this.instlSitePocCellPhnNbr = instlSitePocCellPhnNbr;
    this.srvcAssrnPocNme = srvcAssrnPocNme;
    this.srvcAssrnPocIntlPhnCd = srvcAssrnPocIntlPhnCd;
    this.srvcAssrnPocPhnNbr = srvcAssrnPocPhnNbr;
    this.srvcAssrnPocIntlCellPhnCd = srvcAssrnPocIntlCellPhnCd;
    this.srvcAssrnPocCellPhnNbr = srvcAssrnPocCellPhnNbr;

    this.sprintCpeNcrId = sprintCpeNcrId || 0;
    this.cpeDspchEmailAdr = cpeDspchEmailAdr;
    this.cpeDspchCmntTxt = cpeDspchCmntTxt;

    this.discMgmtCd = discMgmtCd;
    this.fullCustDiscCd = fullCustDiscCd || false;
    this.fullCustDiscReasTxt = fullCustDiscReasTxt;

    this.esclCd = esclCd || false;
    this.esclReasId = esclReasId || 0;
    this.primReqDt = primReqDt || new Date();
    this.scndyReqDt = scndyReqDt || new Date();
    this.busJustnTxt = busJustnTxt;
    this.cnfrcBrdgId = cnfrcBrdgId || 0;
    this.cnfrcBrdgNbr = cnfrcBrdgNbr;
    this.cnfrcPinNbr = cnfrcPinNbr;
    this.onlineMeetingAdr = onlineMeetingAdr;
    this.pubEmailCcTxt = pubEmailCcTxt;
    this.cmpltdEmailCcTxt = cmpltdEmailCcTxt;

    this.reqorUserId = reqorUserId || 0;
    this.reqorUser = reqorUser || null;
    this.reqorUserCellPhnNbr = reqorUserCellPhnNbr;
    this.cmntTxt = cmntTxt;
    this.strtTmst = strtTmst || null;
    this.endTmst = endTmst || null;
    this.extraDrtnTmeAmt = extraDrtnTmeAmt || 0;
    this.eventDrtnInMinQty = eventDrtnInMinQty || 0;
    this.mnsPmId = mnsPmId;
    this.failReasId = failReasId || 0;
    this.wrkflwStusId = wrkflwStusId || 0;
    this.suppressEmail = suppressEmail || false;
    this.oldEventStusId = oldEventStusId || 0;
    this.entityBase = entityBase || null;
    this.ucaasEventBilling = ucaasEventBilling || [];
    this.ucaasEventOdieDevice = ucaasEventOdieDevice || [];
    this.eventCpeDev = eventCpeDev || [];
    this.eventDeviceCompletion = eventDeviceCompletion || [];
    this.eventDevServiceMgmt = eventDevServiceMgmt || [];
    this.eventDiscoDev = eventDiscoDev || [];

    this.orderCode = orderCode || 0;
    this.activators = activators || [];
  }
}
