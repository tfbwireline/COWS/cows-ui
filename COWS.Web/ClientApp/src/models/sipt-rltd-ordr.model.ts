export class SiptRltdOrdr {
  siptReltdOrdrId: number;
  eventId: number;
  m5OrdrNbr: string;
  creatDt: Date;

  constructor();
  constructor(siptReltdOrdrId?: number, eventId?: number, m5OrdrNbr?: string,  creatDt?: Date) {
    this.siptReltdOrdrId = siptReltdOrdrId || 0;
    this.eventId = eventId || 0;
    this.m5OrdrNbr = m5OrdrNbr;
    this.creatDt = creatDt || null;
  }
}
