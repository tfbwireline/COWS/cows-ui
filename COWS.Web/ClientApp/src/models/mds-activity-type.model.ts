
export class MDSActivityType {
  mdsActyTypeId: number;
  mdsActyTypeDes: string;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  modfdDt: Date;

  constructor();
  constructor(mdsActyTypeId?: number, mdsActyTypeDes?: string,
    recStusId?: number, recStatus?: boolean, creatDt?: Date, modfdDt?: Date) {
    this.mdsActyTypeId = mdsActyTypeId || 0;
    this.mdsActyTypeDes = mdsActyTypeDes;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;
    this.modfdDt = modfdDt || null;
  }
}
