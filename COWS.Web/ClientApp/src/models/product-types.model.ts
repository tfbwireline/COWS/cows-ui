export class ProductTypes {
  sdePrdctTypeNme: string;
  sdePrdctTypeDesc: string;
  ordrBySeqNbr: number;
  recStusId: number;
  outsrcdCd: boolean;
  
  constructor(sdePrdctTypeNme?: string, sdePrdctTypeDesc?: string, ordrBySeqNbr?: number, recStusId?: number, outsrcdCd?:boolean) {
    this.sdePrdctTypeNme = sdePrdctTypeNme;
    this.sdePrdctTypeDesc = sdePrdctTypeDesc;
    this.ordrBySeqNbr = ordrBySeqNbr || 0;
    this.recStusId = recStusId|| 0;
    this.outsrcdCd = outsrcdCd || false;
  }
}
