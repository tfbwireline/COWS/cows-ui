export class VendorFolderTemplate {
  vndrTmpltId: number;
  vndrFoldrId: number;
  fileNme: string;
  fileCntnt: Int8Array;
  fileSizeQty: number;
  creatDt: Date;
  creatByUserId: number;
  creatByUserAdId: string;
  recStusId: number;
  base64string: string;

  constructor();
  constructor(vndrTmpltId?: number, vndrFoldrId?: number, fileNme?: string,
    fileCntnt?: Int8Array, fileSizeQty?: number, creatDt?: Date,
    creatByUserId?: number, creatByUserAdId?: string, recStusId?: number, base64string?: string
  ) {
    this.vndrTmpltId = vndrTmpltId || 0;
    this.vndrFoldrId = vndrFoldrId || 0;
    this.fileNme = fileNme;
    this.fileCntnt = fileCntnt || null;
    this.fileSizeQty = fileSizeQty || 0;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.creatByUserAdId = creatByUserAdId;
    this.recStusId = recStusId || 1;
    this.base64string = base64string || null;
  }
}
