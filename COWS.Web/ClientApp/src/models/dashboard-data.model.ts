export interface DashboardData {
    
    mrcnrc: MRCNRCData[];
    averageInstallations: AverageInstallationData[];
    newOrders: OrderData[];
    installOrders: OrderData[];
}

export interface AverageInstallationData {
    monthYear: string;
    averageInstallationInterval: number;
}

export interface MRCNRCData {
    monthYear: string;
    amncimrc: number;
    amncinrc: number;
    encimrc: number;
    ancimrc: number;
    ancinrc: number;

}

export interface OrderData {
    monthYear: string;
    amnci: number;
    enci: number;
    anci: number;
}