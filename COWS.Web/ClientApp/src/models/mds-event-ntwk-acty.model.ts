export class MDSEventNtwkActy {
  id: number;
  eventId: number;
  ntwkActyTypeId: number;
  creatDt: Date;
  ntwkActyType: string;

  constructor({ id = 0, eventId = 0, ntwkActyTypeId = 0, creatDt = new Date(), ntwkActyType = null }) {
    this.id = id
    this.eventId = eventId
    this.ntwkActyTypeId = ntwkActyTypeId
    this.creatDt = creatDt
    this.ntwkActyType = ntwkActyType
  }
}
