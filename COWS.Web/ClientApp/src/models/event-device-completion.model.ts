export class EventDeviceCompletion {
  eventDevCmpltId: number;
  eventId: number;
  odieDevNme: string;
  h6: string;
  cmpltdCd: boolean;
  creatDt: Date;
  redsgnDevId: number;
  recStusId: number;
  odieSentDt: Date;
  isAlreadyCompleted: boolean;

  //constructor();
  constructor({ eventDevCmpltId = 0, eventId = 0, odieDevNme = "", h6 = null, cmpltdCd = false,
    creatDt = new Date(), redsgnDevId = null, recStusId = 0, odieSentDt = null }) {
    this.eventDevCmpltId = eventDevCmpltId;
    this.eventId = eventId;
    this.odieDevNme = odieDevNme;
    this.h6 = h6;
    this.cmpltdCd = cmpltdCd;
    this.creatDt = creatDt;
    this.redsgnDevId = redsgnDevId;
    this.recStusId = recStusId;
    this.odieSentDt = odieSentDt;
  }
}
