
export class MDSEventWrlsTrpt {
  mdsEventWrlsTrptId: number;
  fsaMdsEventId: number;
  primBkupCd: string;
  esnMacId: string;
  tabSeqNbr: number;
  eventId: number;
  wrlsTypeCd: string;
  billAcctNme: string;
  billAcctNbr: string;
  acctPin: string;
  salsCd: string;
  soc: string;
  staticIpAdr: string;
  imei: string;
  uiccIdNbr: string;
  pricePln: string;
  odieDevNme: string;
  creatDt: Date;

  //constructor();
  constructor({ mdsEventWrlsTrptId = 0, fsaMdsEventId = null, primBkupCd = "P", esnMacId = "", tabSeqNbr = 0, eventId = null,
    wrlsTypeCd = null, billAcctNme = null, billAcctNbr = null, acctPin = null, salsCd = null, soc = null, staticIpAdr = null,
    imei = null, uiccIdNbr = null, pricePln = null, odieDevNme = null, creatDt = new Date() }) {
    this.mdsEventWrlsTrptId = mdsEventWrlsTrptId || 0;
    this.fsaMdsEventId = fsaMdsEventId || 0;
    this.primBkupCd = primBkupCd;
    this.esnMacId = esnMacId;
    this.tabSeqNbr = tabSeqNbr || 0;
    this.eventId = eventId || 0;
    this.wrlsTypeCd = wrlsTypeCd;
    this.billAcctNme = billAcctNme;
    this.billAcctNbr = billAcctNbr;
    this.acctPin = acctPin;
    this.salsCd = salsCd;
    this.soc = soc;
    this.staticIpAdr = staticIpAdr;
    this.imei = imei;
    this.uiccIdNbr = uiccIdNbr;
    this.pricePln = pricePln;
    this.odieDevNme = odieDevNme;
    this.creatDt = creatDt || null;
  }
}
