
export class FedlineConfig {
  fedlineConfigId: number
  fedlineEventId: number
  portName: string
  ipAddress: string
  subnetMask: string
  defaultGateway: string
  speed: string
  duplex: string
  macAddress: string

  constructor();
  constructor(fedlineConfigId?: number, fedlineEventId?: number, portName?: string, ipAddress?: string, subnetMask?: string,
    defaultGateway?: string, speed?: string, duplex?: string, macAddress?: string) {
    this.fedlineConfigId = fedlineConfigId || 0
    this.fedlineEventId = fedlineEventId || 0
    this.portName = portName || null
    this.ipAddress = ipAddress || null
    this.subnetMask = subnetMask || null
    this.defaultGateway = defaultGateway || null
    this.speed = speed || null
    this.duplex = duplex || null
    this.macAddress = macAddress || null
  }
}
