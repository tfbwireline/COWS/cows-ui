export class OdieRspn {
  rspnId: number;
  reqId: number;
  rspnDt: Date;
  custTeamPdl: string;
  mnspmId: string;
  rspnErrorTxt: string;
  creatDt: Date;
  ackCd: boolean;
  sowsFoldrPathNme: string;
  docUrlAdr: string;
  actCd: boolean;
  odieCustId: string;
  nteId: string;
  sdeAssigned: string;
  custNme: string;

  constructor({ rspnId = 0, reqId = 0, rspnDt = null, custTeamPdl = null, mnspmId = null, rspnErrorTxt = null, creatDt = null, ackCd = false, sowsFoldrPathNme = null, docUrlAdr = null, actCd = false,
    odieCustId = null, nteId = null, sdeAssigned = null, custNme = null }) {
    this.rspnId = rspnId
    this.reqId = reqId
    this.rspnDt = rspnDt
    this.custTeamPdl = custTeamPdl
    this.mnspmId = mnspmId
    this.rspnErrorTxt = rspnErrorTxt
    this.creatDt = creatDt
    this.ackCd = ackCd
    this.sowsFoldrPathNme = sowsFoldrPathNme
    this.docUrlAdr = docUrlAdr
    this.actCd = actCd
    this.odieCustId = odieCustId
    this.nteId = nteId
    this.sdeAssigned = sdeAssigned
    this.custNme = custNme
  }
}
