import { VendorOrderMs } from "./vendor-order-ms.model";

export class VendorOrderEmail {
  vndrOrdrEmailId: number
  vndrOrdrId: number
  verNbr: number
  fromEmailAdr: string
  toEmailAdr: string
  ccEmailAdr: string
  emailBody: string
  emailStusId: number
  sentDt: Date
  creatDt: Date
  creatByUserId: number
  recStusId: number
  modfdByUserId: number
  modfdDt: Date
  emailSubjTxt: string
  vndrEmailTypeId: number
  vndrOrdrEmailAtchmt: any[]
  emailTypeDesc: string
  emailAttachmentName: string[]
  emailAttachmentSize: number[]
  creatByUserFullName: string
  modfdByUserFullName: string
  emailStatusDesc: string
  sentToVendorDate: Date
  ackByVendorDate: Date
  ableToAddAckDate: boolean // Flag for preloaded data;
  vndrOrdrMs: VendorOrderMs[]

  constructor({ vndrOrdrEmailId = 0, vndrOrdrId = 0, verNbr = 1, fromEmailAdr = "", toEmailAdr = "", ccEmailAdr = "", emailBody = "", emailStusId = 13, sentDt = null,
    creatDt = new Date(), creatByUserId = 1, recStusId = 1, modfdByUserId = null, modfdDt = null, emailSubjTxt = "", vndrEmailTypeId = null, vndrOrdrEmailAtchmt = null,
    emailTypeDesc = "", emailAttachmentName = [], emailAttachmentSize = [], creatByUserFullName = "", modfdByUserFullName = null, emailStatusDesc = "", sentToVendorDate = null,
    ackByVendorDate = null, vndrOrdrMs = [] }) {
    this.vndrOrdrEmailId = vndrOrdrEmailId
    this.vndrOrdrId = vndrOrdrId
    this.verNbr = verNbr
    this.fromEmailAdr = fromEmailAdr
    this.toEmailAdr = toEmailAdr
    this.ccEmailAdr = ccEmailAdr
    this.emailBody = emailBody
    this.emailStusId = emailStusId
    this.sentDt = sentDt
    this.creatDt = creatDt
    this.creatByUserId = creatByUserId
    this.recStusId = recStusId
    this.modfdByUserId = modfdByUserId
    this.modfdDt = modfdDt
    this.emailSubjTxt = emailSubjTxt
    this.vndrEmailTypeId = vndrEmailTypeId
    this.vndrOrdrEmailAtchmt = vndrOrdrEmailAtchmt
    this.emailTypeDesc = emailTypeDesc
    this.emailAttachmentName = emailAttachmentName
    this.emailAttachmentSize = emailAttachmentSize
    this.creatByUserFullName = creatByUserFullName
    this.modfdByUserFullName = modfdByUserFullName
    this.emailStatusDesc = emailStatusDesc
    this.sentToVendorDate = sentToVendorDate
    this.ackByVendorDate = ackByVendorDate
    this.vndrOrdrMs = vndrOrdrMs
  }
}
