export class EntityBase {
  recStatusId: number;
  recStatus: boolean;
  createdByUserId: number;
  createdByUserName: string;
  createdByFullName: string;
  createdByDisplayName: string;
  createdDateTime: Date;
  modifiedByUserId: number;
  modifiedByUserName: string;
  modifiedByFullName: string;
  modifiedByDisplayName: string;
  modifiedDateTime: Date;

  constructor();
  constructor(recStatusId?: number, recStatus?: boolean, createdByUserId?: number,
    createdByUserName?: string, createdByFullName?: string, createdByDisplayName?: string,
    createdDateTime?: Date, modifiedByUserId?: number, modifiedByUserName?: string,
    modifiedByFullName?: string, modifiedByDisplayName?: string, modifiedDateTime?: Date) {
    this.recStatusId = recStatusId || 1;
    this.recStatus = recStatus || true;
    this.createdByUserId = createdByUserId || 0;
    this.createdByUserName = createdByUserName;
    this.createdByFullName = createdByFullName;
    this.createdByDisplayName = createdByDisplayName;
    this.createdDateTime = createdDateTime || new Date();
    this.modifiedByUserId = modifiedByUserId || null;
    this.modifiedByUserName = modifiedByUserName;
    this.modifiedByFullName = modifiedByFullName;
    this.modifiedByDisplayName = modifiedByDisplayName;
    this.modifiedDateTime = modifiedDateTime;
  }
}
