export class VendorOrderMs {
  vndrOrdrMsId: number;
  vndrOrdrId: number;
  verId: number;
  vndrOrdrEmailId: number;
  sentToVndrDt: Date;
  ackByVndrDt: Date;
  creatByUserId: number;
  creatDt: Date;

  constructor({ vndrOrdrMsId = 0, vndrOrdrId = 0, verId = 1, vndrOrdrEmailId = 0, sentToVndrDt = null, ackByVndrDt = null, creatByUserId = 1, creatDt = new Date() }) {
    this.vndrOrdrMsId = vndrOrdrMsId;
    this.vndrOrdrId = vndrOrdrId;
    this.verId = verId;
    this.vndrOrdrEmailId = vndrOrdrEmailId;
    this.sentToVndrDt = sentToVndrDt;
    this.ackByVndrDt = ackByVndrDt;
    this.creatByUserId = creatByUserId;
    this.creatDt = creatDt;
  }
}
