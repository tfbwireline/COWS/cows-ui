
export class IPVersion {
  ipVerId: number;
  ipVerNme: string;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  modfdDt: Date;
  modfdByUserId: number;

  constructor();
  constructor(ipVerId?: number, ipVerNme?: string,
    recStusId?: number, recStatus?: boolean,
    creatDt?: Date, creatByUserId?: number,
    modfdDt?: Date, modfdByUserId?: number) {
    this.ipVerId = ipVerId || 0;
    this.ipVerNme = ipVerNme;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
  }
}
