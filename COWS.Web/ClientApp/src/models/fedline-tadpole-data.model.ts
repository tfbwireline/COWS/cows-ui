import { FedlineConfig } from "./fedline-config.model";
import { FedlineManagedDevice } from "./fedline-managed-device.model";
import { FedlineOrigDevice } from "./fedline-orig-device.model";
import { FedlineSiteLocation } from "./fedline-site-location.model";

export class FedlineTadpoleData {

  cowsFedlineId: number
  frbRequestId: number
  version: number
  custName: string
  custOrgId: number
  designType: string
  designDisplay: string
  macType: number 
  orderTypeCode: string
  moveType: string
  relatedEvent: string
  installType: string
  orderSubType: string
  reorderReason: string
  managedDeviceData: FedlineManagedDevice
  siteInstallInfo: FedlineSiteLocation
  siteShippingInfo: FedlineSiteLocation
  origDeviceData: FedlineOrigDevice
  configs: FedlineConfig
  wan: FedlineConfig
  lan: FedlineConfig
  existingDevice: string
  startDate: Date
  endDate: Date
  frbSentDate: Date
  frbModifyDate: Date
  isMove: boolean
  isExpedite: boolean
  isInstallorHeadend: boolean
  isDisconnect: boolean
  isRefresh: boolean
  isHeadend: boolean
  timeBlock: string
  activityDate: Date

  constructor();
  constructor(cowsFedlineId?: number, frbRequestId?: number, version?: number, custName?: string, custOrgId?: number, designType?: string, designDisplay?: string, macType?: number,
    orderTypeCode?: string, moveType?: string, relatedEvent?: string, installType?: string, orderSubType?: string, reorderReason?: string,
    managedDeviceData?: FedlineManagedDevice, siteInstallInfo?: FedlineSiteLocation, siteShippingInfo?: FedlineSiteLocation, origDeviceData?: FedlineOrigDevice,
    configs?: FedlineConfig, wan?: FedlineConfig, lan?: FedlineConfig,
    existingDevice?: string, startDate?: Date, endDate?: Date, frbSentDate?: Date, frbModifyDate?: Date, isMove?: boolean, isExpedite?: boolean,
    isInstallorHeadend?: boolean, isDisconnect?: boolean, isRefresh?: boolean, isHeadend?: boolean, timeBlock?: string, activityDate?: Date) {
    this.cowsFedlineId = cowsFedlineId || 0
    this.frbRequestId = frbRequestId || 0
    this.version = version || 0
    this.custName = custName || null
    this.custOrgId = custOrgId || 0
    this.designType = designType || ""
    this.designDisplay = designDisplay || ""
    this.macType = macType || 0
    this.orderTypeCode = orderTypeCode || null
    this.moveType = moveType || null
    this.relatedEvent = relatedEvent || null
    this.installType = installType || null
    this.orderSubType = orderSubType || null
    this.reorderReason = reorderReason || null
    this.managedDeviceData = managedDeviceData || null
    this.siteInstallInfo = siteInstallInfo || null
    this.siteShippingInfo = siteShippingInfo || null
    this.origDeviceData = origDeviceData || null
    this.configs = configs || null
    this.wan = wan || null
    this.lan = lan || null
    this.existingDevice = existingDevice || null
    this.startDate = startDate || null
    this.endDate = endDate || null
    this.frbSentDate = frbSentDate || null
    this.frbModifyDate = frbModifyDate || null
    this.isMove = isMove || false
    this.isExpedite = isExpedite || false
    this.isInstallorHeadend = isInstallorHeadend || false
    this.isDisconnect = isDisconnect || false
    this.isRefresh = isRefresh || false
    this.isHeadend = isHeadend || false
    this.timeBlock = timeBlock || null
    activityDate = activityDate || null
  }
}
