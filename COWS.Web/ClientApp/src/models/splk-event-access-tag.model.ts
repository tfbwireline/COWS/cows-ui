export class SplkEventAccessTag {
  splkEventAccsId: number = 0;
  eventId: number;
  oldPlNbr: string;
  oldPortSpeedDes: string;
  newPlNbr: string;
  newPortSpeedDes: string;
  creatDt: Date;
  oldVpiVciDlciNme: string;
  newVpiVciDlciNme: string;

  constructor();

  constructor(splkEventAccsId?: number, eventId?: number, oldPlNbr?: string, oldPortSpeedDes?: string,
    newPlNbr?: string, newPortSpeedDes?: string, oldVpiVciDlciNme?: string, newVpiVciDlciNme?: string,
    creatDt?: Date) {
    this.splkEventAccsId = splkEventAccsId || 0;
    this.eventId = eventId || 0;
    this.oldPlNbr = oldPlNbr;
    this.oldPortSpeedDes = oldPortSpeedDes;
    this.newPlNbr = newPlNbr;
    this.newPortSpeedDes = newPortSpeedDes;
    this.oldVpiVciDlciNme = oldVpiVciDlciNme ;
    this.newVpiVciDlciNme = newVpiVciDlciNme;
    this.creatDt = creatDt || null;
  }
}
