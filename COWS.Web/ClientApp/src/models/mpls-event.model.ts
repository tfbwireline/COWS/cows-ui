import { EntityBase } from "./entity-base.model";
//import { MplsEventAccessTag, User } from ".";
import { User } from "./user.model";
import { MplsEventAccessTag } from "./mpls-event-access-tag.model";

export class MplsEvent {
  authStatus: number;
  eventTitleTxt: string;
  eventDes: string;
  eventId: number;
  eventStusId: number;
  eventStusDes: string;
  ftn: string;
  charsId: string;
  h1: string;
  h6: string;
  eventCsgLvlId: number;

  custNme: string;
  custCntctNme: string;
  custCntctPhnNbr: string;
  custEmailAdr: string;
  custCntctCellPhnNbr: string;
  custCntctPgrNbr: string;
  custCntctPgrPinNbr: string;
  reqorUserId: number;
  reqorUser: User;
  cnfrcBrdgNbr: string;
  cnfrcPinNbr: string;
  salsUserId: number;
  salsUser: User;

  pubEmailCcTxt: string;
  cmpltdEmailCcTxt: string;
  docLinkTxt: string;
  ddAprvlNbr: string;
  nddUpdtdCd: boolean;
  desCmntTxt: string;
  mplsEventTypeId: number;
  vpnPltfrmTypeId: number;
  ipVerId?: number;
  actyLocaleId: string;
  multiVrfReqId: string;
  vrfNme: string;
  mdsMngdCd: boolean;
  addE2eMontrgCd: boolean;
  mdsVrfNme: string;
  mdsIpAdr: string;
  mdsDlci: string;
  mdsStcRteDes: string;
  onNetMontrgCd: boolean;
  whlslPtnrCd: boolean;
  whlslPtnrId: number;
  nniDsgnDocNme: string;
  cxrPtnrCd: boolean;
  mplsCxrPtnrId: string;
  offNetMontrgCd: boolean;
  mgrtnCd: boolean;
  mplsMgrtnTypeId: number;
  reltdCmpsNcrCd: boolean;
  reltdCmpsNcrNme: string;

  esclCd: boolean;
  esclReasId: number;
  strtTmst: Date;
  eventDrtnInMinQty: number;
  extraDrtnTmeAmt: number;
  endTmst: Date;
  primReqDt: Date;
  scndyReqDt: Date;
  wrkflwStusId: any;
  wrkflwStusDes: string;
  sowsEventId: number;
  mplsEventActyTypeIds: number[] = [];
  mplsEventVasTypeIds: number[] = [];

  entityBase: EntityBase;
  mplsEventAccsTag: MplsEventAccessTag[] = [];
  activators: number[] = [];

  reviewerUserId: number;
  reviewerComments: string;

  activatorUserId: number;
  activatorComments: string;

  eventSucssActyIds: number[] = [];
  eventFailActyIds: number[] = [];
  preCfgConfgCode: string;
  profile: string;
  failCode: number;

  constructor();
  constructor(eventTitleTxt?: string, eventDes?: string, eventId?: number, eventStusId?: number,
    eventStusDes?: string, ftn?: string, h1?: string, h6?: string, eventCsgLvlId?: number,
    custNme?: string, custCntctNme?: string, custCntctPhnNbr?: string, custEmailAdr?: string,
    custCntctCellPhnNbr?: string, custCntctPgrNbr?: string, custCntctPgrPinNbr?: string,
    reqorUserId?: number, reqorUser?: User, cnfrcBrdgNbr?: string, cnfrcPinNbr?: string,
    salsUserId?: number, salsUser?: User, pubEmailCcTxt?: string, cmpltdEmailCcTxt?: string,
    docLinkTxt?: string, ddAprvlNbr?: string, nddUpdtdCd?: boolean, desCmntTxt?: string,
    mplsEventTypeId?: number, vpnPltfrmTypeId?: number, ipVerId?: number, actyLocaleId?: string,
    multiVrfReqId?: string, vrfNme?: string, mdsMngdCd?: boolean, addE2eMontrgCd?: boolean,
    mdsVrfNme?: string, mdsIpAdr?: string, mdsDlci?: string, mdsStcRteDes?: string, onNetMontrgCd?: boolean,
    whlslPtnrCd?: boolean, whlslPtnrId?: number, nniDsgnDocNme?: string, cxrPtnrCd?: boolean,
    mplsCxrPtnrId?: string, offNetMontrgCd?: boolean, mgrtnCd?: boolean, mplsMgrtnTypeId?: number,
    reltdCmpsNcrCd?: boolean, reltdCmpsNcrNme?: string, esclCd?: boolean, esclReasId?: number,
    strtTmst?: Date, eventDrtnInMinQty?: number, extraDrtnTmeAmt?: number, endTmst?: Date,
    primReqDt?: Date, scndyReqDt?: Date, wrkflwStusId?: number, wrkflwStusDes?: string,
    sowsEventId?: number, mplsEventActyTypeIds?: number[], mplsEventVasTypeIds?: number[],
    entityBase?: EntityBase, mplsEventAccsTag?: MplsEventAccessTag[], activators?: number[],
    reviewerUserId?: number, reviewerComments?: string, activatorUserId?: number,
    activatorComments?: string, eventSucssActyIds?: number[], eventFailActyIds?: number[],
    preCfgConfgCode?: string, profile?: string, failCode?: number, authStatus?: number) {
    this.authStatus = authStatus || 0;
    this.eventTitleTxt = eventTitleTxt;
    this.eventDes = eventDes;
    this.eventId = eventId || 0;
    this.eventStusId = eventStusId || 0;
    this.eventStusDes = eventStusDes;
    this.ftn = ftn;
    this.h1 = h1 || null;
    this.h6 = h6;
    this.eventCsgLvlId = eventCsgLvlId || 0;

    this.custNme = custNme;
    this.custCntctNme = custCntctNme;
    this.custCntctPhnNbr = custCntctPhnNbr;
    this.custEmailAdr = custEmailAdr;
    this.custCntctCellPhnNbr = custCntctCellPhnNbr;
    this.custCntctPgrNbr = custCntctPgrNbr;
    this.custCntctPgrPinNbr = custCntctPgrPinNbr;
    this.reqorUserId = reqorUserId || 0;
    this.reqorUser = reqorUser || null;
    this.cnfrcBrdgNbr = cnfrcBrdgNbr;
    this.cnfrcPinNbr = cnfrcPinNbr;
    this.salsUserId = salsUserId || 0;
    this.salsUser = salsUser || null;

    this.pubEmailCcTxt = pubEmailCcTxt;
    this.cmpltdEmailCcTxt = cmpltdEmailCcTxt;
    this.docLinkTxt = docLinkTxt;
    this.ddAprvlNbr = ddAprvlNbr;
    this.nddUpdtdCd = nddUpdtdCd || false;
    this.desCmntTxt = desCmntTxt;
    this.mplsEventTypeId = mplsEventTypeId || 0;
    this.vpnPltfrmTypeId = vpnPltfrmTypeId || 0;
    this.ipVerId = ipVerId || 0;
    this.actyLocaleId = actyLocaleId;
    this.multiVrfReqId = multiVrfReqId;
    this.vrfNme = vrfNme;
    this.mdsMngdCd = mdsMngdCd || false;
    this.addE2eMontrgCd = addE2eMontrgCd || false;
    this.mdsVrfNme = mdsVrfNme;
    this.mdsIpAdr = mdsIpAdr;
    this.mdsDlci = mdsDlci;
    this.mdsStcRteDes = mdsStcRteDes;
    this.onNetMontrgCd = onNetMontrgCd || false;
    this.whlslPtnrCd = whlslPtnrCd || false;
    this.whlslPtnrId = whlslPtnrId || 0;
    this.nniDsgnDocNme = nniDsgnDocNme;
    this.cxrPtnrCd = cxrPtnrCd || false;
    this.mplsCxrPtnrId = mplsCxrPtnrId;
    this.offNetMontrgCd = offNetMontrgCd || false;
    this.mgrtnCd = mgrtnCd || false;
    this.mplsMgrtnTypeId = mplsMgrtnTypeId || 0;
    this.reltdCmpsNcrCd = reltdCmpsNcrCd || false;
    this.reltdCmpsNcrNme = reltdCmpsNcrNme;

    this.esclCd = esclCd || false;
    this.esclReasId = esclReasId || 0;
    this.strtTmst = strtTmst || new Date();
    this.eventDrtnInMinQty = eventDrtnInMinQty || 60;
    this.extraDrtnTmeAmt = extraDrtnTmeAmt || 0;
    this.endTmst = endTmst || new Date();
    this.primReqDt = primReqDt || null;
    this.scndyReqDt = scndyReqDt || null;
    this.wrkflwStusId = wrkflwStusId || 0;
    this.wrkflwStusDes = wrkflwStusDes;
    this.sowsEventId = sowsEventId || 0;
    this.mplsEventActyTypeIds = mplsEventActyTypeIds || [];
    this.mplsEventVasTypeIds = mplsEventVasTypeIds || [];
    
    this.entityBase = entityBase || new EntityBase();
    this.mplsEventAccsTag = mplsEventAccsTag || [];
    this.activators = activators || [];

    this.reviewerUserId = reviewerUserId || 0;
    this.reviewerComments = reviewerComments;

    this.activatorUserId = activatorUserId || 0;
    this.activatorComments = activatorComments;

    this.eventSucssActyIds = eventSucssActyIds || [];
    this.eventFailActyIds = eventFailActyIds || [];
    this.preCfgConfgCode = preCfgConfgCode;
    this.profile = profile;
    this.failCode = failCode || 1;
  }
}
