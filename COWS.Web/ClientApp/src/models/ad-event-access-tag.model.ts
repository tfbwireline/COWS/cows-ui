export class AdEventAccessTag {
  eventId: number;
  cktId: string;
  creatDt: Date;
  oldCktId: string;

  constructor();
  constructor(eventId?: number, cktId?: string, oldCktId?: string);
  constructor(eventId?: number, cktId?: string, oldCktId?: string, creatDt?: Date)
  constructor(eventId?: number, cktId?: string, oldCktId?: string, creatDt?: Date) {
    this.eventId = eventId || 0;
    this.cktId = cktId || null;
    this.creatDt = creatDt || null;
    this.oldCktId = oldCktId || null;
  }
}
