export class FedlineManageUserEvent {
  eventID: number
  frbid: number
  startTime: string
  endTime: string
  activator: string
  preConfigEng: string
  activity: string
  status: string
  timeBlock: string

  constructor() {

  }
}
