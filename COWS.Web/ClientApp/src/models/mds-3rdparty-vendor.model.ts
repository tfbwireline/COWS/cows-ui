
export class MDS3rdPartyVendor {
  thrdPartyVndrId: number;
  thrdPartyVndrDes: string;
  srvcTypeId: number;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;
  modfdDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;

  constructor();
  constructor(thrdPartyVndrId?: number, thrdPartyVndrDes?: string, srvcTypeId?: number,
    recStusId?: number, recStatus?: boolean,
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string,
    modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
    this.thrdPartyVndrId = thrdPartyVndrId || 0;
    this.thrdPartyVndrDes = thrdPartyVndrDes;
    this.srvcTypeId = srvcTypeId || 0;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.modifiedByAdId = modifiedByAdId;
  }
}
