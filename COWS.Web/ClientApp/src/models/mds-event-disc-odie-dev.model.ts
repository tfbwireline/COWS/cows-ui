export class MDSEventDiscOdieDev {
  eventDiscoDevId: number;
  eventId: number;
  odieDevNme: string;
  h5H6: string;
  siteId: string;
  deviceId: string;
  //vendor: string;
  devModelId: number;
  manfId: number;
  serialNbr: string;
  thrdPartyCtrct: string;
  creatDt: Date;
  recStusId: number;
  modfdDt: Date;
  readyBeginCd: string;
  optInCktCd: string;
  optInHCd: string;
  manfName: string;
  mach5OrderId: string;
  oldReadyBeginCd: string;
  oldOptInCktCd: string;
  oldOptInHCd: string;

  constructor();
  constructor(eventDiscoDevId?: number, eventId?: number, odieDevNme?: string, h5H6?: string, siteId?: string, deviceId?: string, /*vendor?: string,*/ devModelId?: number,
    manfId?: number, serialNbr?: string, thrdPartyCtrct?: string, creatDt?: Date, recStusId?: number, modfdDt?: Date, readyBeginCd?: string,
    optInCktCd?: string, optInHCd?: string, manfName?: string, mach5OrderId?: string, oldReadyBeginCd?: string, oldOptInCktCd?: string, oldOptInHCd?: string) {
    this.eventDiscoDevId = eventDiscoDevId || 0;
    this.eventId = eventId || 0;
    this.odieDevNme = odieDevNme || null;
    this.h5H6 = h5H6 || null;
    this.siteId = siteId || null;
    this.deviceId = deviceId || null;
    //this.vendor = vendor || null;
    this.devModelId = devModelId || null;
    this.manfId = manfId || null;
    this.serialNbr = serialNbr || null;
    this.thrdPartyCtrct = thrdPartyCtrct || null;
    this.creatDt = creatDt || null;
    this.recStusId = recStusId || 1;
    this.modfdDt = modfdDt || null;
    this.readyBeginCd = readyBeginCd || null;
    this.optInCktCd = optInCktCd || null;
    this.optInHCd = optInHCd || null;
    this.manfName = manfName || null;
    this.mach5OrderId = mach5OrderId || null;
    this.oldReadyBeginCd = oldReadyBeginCd || null;
    this.oldOptInCktCd = oldOptInCktCd || null;
    this.oldOptInHCd = oldOptInHCd || null;
  }
}
