export class MdsEventSiteSrvc {
  mdsEventSiteSrvcId: number;
  eventId: number;
  mach5SrvcOrdrId: string;
  srvcTypeId: string;
  thrdPartyVndrId: string;
  thrdPartyId: string;
  thrdPartySrvcLvlId: string;
  actvDt: string;
  cmntTxt: string;
  emailCd: boolean;
  creatDt: Date;
  odieDevNme: string;
  m5OrdrNbr: string;

  constructor({ mdsEventSiteSrvcId = 0, eventId = 0, mach5SrvcOrdrId = "", srvcTypeId = "", thrdPartyVndrId = null, thrdPartyId = null, thrdPartySrvcLvlId = null, actvDt = null,
    cmntTxt = null, emailCd = false, creatDt = new Date(), odieDevNme = "", m5OrdrNbr = null }) {
    this.mdsEventSiteSrvcId = mdsEventSiteSrvcId
    this.eventId = eventId
    this.mach5SrvcOrdrId = mach5SrvcOrdrId
    this.srvcTypeId = srvcTypeId
    this.thrdPartyVndrId = thrdPartyVndrId
    this.thrdPartyId = thrdPartyId
    this.thrdPartySrvcLvlId = thrdPartySrvcLvlId
    this.actvDt = actvDt
    this.cmntTxt = cmntTxt
    this.emailCd = emailCd
    this.creatDt = creatDt
    this.odieDevNme = odieDevNme
    this.m5OrdrNbr = m5OrdrNbr
  }
}
