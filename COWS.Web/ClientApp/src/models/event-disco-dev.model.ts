export class EventDiscoDev {
  eventDiscoDevId: number;
  eventId: number;
  odieDevNme: string;
  h5H6: string;
  siteId: string;
  deviceId: string;
  devModelId: number;
  manfId: number;
  serialNbr: string;
  thrdPartyCtrct: string;
  creatDt: Date;
  recStusId: boolean;
  modfdDt: Date;
  readyBeginCd: string;
  optInCktCd: string;
  optInHCd: string;
  readyBegin: boolean;
  optInCkt: boolean;
  optInH: boolean;

  //constructor();
  constructor(eventDiscoDevId?: number, eventId?: number, odieDevNme?: string, h5H6?: string,
    siteId?: string, deviceId?: string, devModelId?: number, manfId?: number, serialNbr?: string,
    thrdPartyCtrct?: string, creatDt?: Date, recStusId?: boolean, modfdDt?: Date,
    readyBeginCd?: string, optInCktCd?: string, optInHCd?: string) {
    this.eventDiscoDevId = eventDiscoDevId || 0;
    this.eventId = eventId || 0;
    this.odieDevNme = odieDevNme;
    this.h5H6 = h5H6;
    this.siteId = siteId;
    this.deviceId = deviceId;
    this.devModelId = devModelId || 0;
    this.manfId = manfId || 0;
    this.serialNbr = serialNbr;
    this.thrdPartyCtrct = thrdPartyCtrct;
    this.creatDt = creatDt || null;
    this.recStusId = recStusId || false;
    this.modfdDt = modfdDt || null;
    this.readyBeginCd = readyBeginCd;
    this.optInCktCd = optInCktCd;
    this.optInHCd = optInHCd;
    this.readyBegin = readyBeginCd == "Y" ? true : false;
    this.optInCkt = optInCktCd == "Y" ? true : false;
    this.optInH = optInHCd == "Y" ? true : false;
  }
}
