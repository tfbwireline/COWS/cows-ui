
export class MDSEventNtwkCust {
  mdsEventNtwkCustId: number;
  eventId: number;
  instlSitePocNme: string;
  instlSitePocPhn: string;
  instlSitePocEmail: string;
  roleNme: string;
  creatDt: Date;
  recStusId: number;
  modfdDt: Date;
  assocH6: string;
  guid: string;

  constructor();
  constructor(mdsEventNtwkCustId?: number, eventId?: number, instlSitePocNme?: string, instlSitePocPhn?: string,
    instlSitePocEmail?: string, roleNme?: string, creatDt?: Date, recStusId?: number, modfdDt?: Date, assocH6?: string) {
    this.mdsEventNtwkCustId = mdsEventNtwkCustId || 0;
    this.eventId = eventId || 0;
    this.instlSitePocNme = instlSitePocNme;
    this.instlSitePocPhn = instlSitePocPhn;
    this.instlSitePocEmail = instlSitePocEmail;
    this.roleNme = roleNme;
    this.creatDt = creatDt || null;
    this.recStusId = recStusId || 1;
    this.modfdDt = modfdDt || null;
    this.assocH6 = assocH6 || null;
  }
}
