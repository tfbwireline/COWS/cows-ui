import { H5Doc } from "./h5-doc.model";

export class H5Folder {
  h5FoldrId: number;
  custId: number;
  custCtyNme: string;
  ctryCd: string;
  csgLvlId: number;
  custNme: string;
  recStusId: number;
  creatDt: Date;
  creatByUserAdId: string;
  creatByUserId: number;
  modfdDt: Date;
  modfdByUserAdId: string;
  modfdByUserId: number;

  h5Docs: H5Doc[];
  h5DocCnt: number;

  constructor();
  constructor(h5FoldrId?: number, custId?: number, custCtyNme?: string,
    ctryCd?: string, csgLvlId?: number, custNme?: string,
    h5DocCnt?: number, recStusId?: number,
    creatDt?: Date, creatByUserAdId?: string, creatByUserId?: number,
    modfdDt?: Date, modfdByUserAdId?: string, modfdByUserId?: number) {
    this.h5FoldrId = h5FoldrId || 0;
    this.custId = custId || 0;
    this.custCtyNme = custCtyNme;
    this.ctryCd = ctryCd;
    this.csgLvlId = csgLvlId || 0;
    this.custNme = custNme;
    this.h5DocCnt = h5DocCnt || 0;
    this.recStusId = recStusId || 1;
    this.creatDt = creatDt || new Date();
    this.creatByUserAdId = creatByUserAdId;
    this.creatByUserId = creatByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.modfdByUserAdId = modfdByUserAdId;
    this.modfdByUserId = modfdByUserId || 0;
  }
}
