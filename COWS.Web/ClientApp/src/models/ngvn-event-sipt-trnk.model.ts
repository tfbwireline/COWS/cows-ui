export class NgvnEventSiptTrnk {
  eventId: number;
  sipTrnkNme: string;
  creatDt: Date;

  constructor(eventId?: number, sipTrnkNme?: string, creatDt?: Date) {
    this.eventId = eventId || 0;
    this.sipTrnkNme = sipTrnkNme || null;
    this.creatDt = creatDt || null;
  }
}
