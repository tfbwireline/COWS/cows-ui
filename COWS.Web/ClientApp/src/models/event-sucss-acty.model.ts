export class EventSucssActy {
  eventHistId: number;
  sucssActyId: number;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;


  constructor();
  constructor(eventHistId?: number, sucssActyId?: number, recStusId?: number, recStatus?: boolean, creatDt?: Date) {
    this.eventHistId = eventHistId || 0;
    this.sucssActyId = sucssActyId || 0;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;

  }
}
