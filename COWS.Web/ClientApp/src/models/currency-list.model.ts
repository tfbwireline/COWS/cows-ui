
export class CurrencyList {
  curId: string;
  curNme: string;
  curCnvrsnFctrFromUsdQty: number;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;
  modfdDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;

  constructor();
  constructor(curId?: string, curNme?: string, curCnvrsnFctrFromUsdQty?: number,
    recStusId?: number, recStatus?: boolean,
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string,
    modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
    this.curId = curId;
    this.curNme = curNme;
    this.curCnvrsnFctrFromUsdQty = curCnvrsnFctrFromUsdQty || 0;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.modifiedByAdId = modifiedByAdId;
  }
}
