
export class Workflow {
  wrkflwStusId: number;
  wrkflwStusDes: string;
  recStusId: number;
  creatDt: Date;
  creatByUserId: number;
  modfdDt: Date;
  modfdByUserId: number;

  //constructor();
  //constructor(wrkflwStusId?: number, wrkflwStusDes?: string, recStusId?: number,
  //  creatDt?: Date, creatByUserId?: number, createdByAdId?: string,
  //  modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
  constructor({ wrkflwStusId = 0, wrkflwStusDes = '', recStusId = 0,
    creatDt = new Date(), creatByUserId = 0, createdByAdId = null,
    modfdDt = new Date(), modfdByUserId = 0, modifiedByAdId = null }) {
    this.wrkflwStusId = wrkflwStusId || 0;
    this.wrkflwStusDes = wrkflwStusDes || null;
    this.recStusId = recStusId || 1;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
  }
}
