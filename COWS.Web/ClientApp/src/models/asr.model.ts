export class Asr {
  asrId: number
  ordrId: number
  asrTypeId: number
  ipNodeTxt: string
  accsBdwdDes: string
  accsCtyNmeSiteCd: string
  trnspntCd: boolean
  entrncAsmtTxt: string
  mstrFtnCd: string
  dlciDes: string
  h1MatchMstrVasCd: boolean
  asrNotesTxt: string
  creatDt: Date
  stusId: number
  creatByUserId: number
  ptnrCxrCd: string
  emailReqId: number
  lecNniNbr: string
  intlDomEmailCd: string
  notes: string

  constructor({ asrId = 0, ordrId = 0, asrTypeId = 0, ipNodeTxt = "", accsBdwdDes = "", accsCtyNmeSiteCd = "", trnspntCd = false, entrncAsmtTxt = "", mstrFtnCd = null, dlciDes = "",
    h1MatchMstrVasCd = false, asrNotesTxt = "", creatDt = new Date(), stusId = 0, creatByUserId = 0, ptnrCxrCd = "", emailReqId = null, lecNniNbr = "", intlDomEmailCd = "" }) {
    this.asrId = asrId
    this.ordrId = ordrId
    this.asrTypeId = asrTypeId
    this.ipNodeTxt = ipNodeTxt
    this.accsBdwdDes = accsBdwdDes
    this.accsCtyNmeSiteCd = accsCtyNmeSiteCd
    this.trnspntCd = trnspntCd
    this.entrncAsmtTxt = entrncAsmtTxt
    this.mstrFtnCd = mstrFtnCd
    this.dlciDes = dlciDes
    this.h1MatchMstrVasCd = h1MatchMstrVasCd
    this.asrNotesTxt = asrNotesTxt
    this.creatDt = creatDt
    this.stusId = stusId
    this.creatByUserId = creatByUserId
    this.ptnrCxrCd = ptnrCxrCd
    this.emailReqId = emailReqId
    this.lecNniNbr = lecNniNbr
    this.intlDomEmailCd = intlDomEmailCd
  }
}
