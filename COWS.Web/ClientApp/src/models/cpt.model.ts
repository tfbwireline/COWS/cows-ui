import { CptRelatedInfo, CptDoc, CptProvision } from ".";
import { ContactDetail } from "./contact-detail.model";

export class Cpt {
  cptId: number;
  cptCustNbr: string;
  custShrtNme: string;
  subnetIpAdr: string;
  startIpAdr: string;
  endIpAdr: string;
  qipSubnetNme: string;
  mplsIpAdr: string;
  wpaaSIpAdr: string;
  submtrUserId: number;
  seEmailAdr: string;
  amEmailAdr: string;
  ipmEmailAdr: string;
  h1: string;
  devCnt: number;
  govtCustCd?: boolean;
  scrtyClrnceCd?: boolean;
  sprntWhlslReslrCd: boolean;
  sprntWhlslReslrNme: string;
  tacacsCd: boolean;
  encMdmCd: boolean;
  hstMgndSrvcCd: boolean;
  cptStusId: number;
  creatDt?: Date;
  creatByUserId?: number;
  modfdDt?: Date;
  modfdByUserId?: number;
  recStusId: number;
  preShrdKey: string;
  revwdPmCd: boolean;
  retrnSdeCd: boolean;
  sdwanCd: boolean;
  csgLvlId: number;
  compnyNme: string;
  custAdr: string;
  custFlrBldgNme: string;
  custCtyNme: string;
  custSttPrvnNme: string;
  custCtryRgnNme: string;
  custZipCd: string;
  preStgdKey: string;

  submitterName: string;
  submitterEmail: string;
  submitterPhone: string;
  notes: string;
  cancelCpt: boolean;

  customerTypes: number[];
  hostedManagedServices: number[];
  mdsPlnSrvcTiers: number[];
  mdsSupportTiers: number[];
  mssPlnSrvcTypes: number[];
  mssMngdAuthenticationProds: number[];
  mvsProdTypes: number[];
  primSites: number[];

  relatedInfos: CptRelatedInfo[];
  cptDocs: CptDoc[];
  cptPrvsn: CptProvision[];
  cptUserAsmt: any[];
  cptHist: any[];

  gatekeeper: number;
  manager: number;
  mss: number;
  nte: number;
  pm: number;
  crm: number;

  reviewedByPMDate: string;
  returnedToSDEDate: string;

  cptStatus: string;
  contactDetails: ContactDetail[];

  constructor();
  constructor(cptId?: number, cptCustNbr?: string, custShrtNme?: string, subnetIpAdr?: string,
    startIpAdr?: string, endIpAdr?: string, qipSubnetNme?: string, mplsIpAdr?: string,
    wpaaSIpAdr?: string, submtrUserId?: number, seEmailAdr?: string, amEmailAdr?: string,
    ipmEmailAdr?: string, h1?: string, devCnt?: number, govtCustCd?: boolean,
    scrtyClrnceCd?: boolean, sprntWhlslReslrCd?: boolean, sprntWhlslReslrNme?: string,
    tacacsCd?: boolean, encMdmCd?: boolean, hstMgndSrvcCd?: boolean, cptStusId?: number,
    creatDt?: Date, creatByUserId?: number, modfdDt?: Date, modfdByUserId?: number, recStusId?: number,
    preShrdKey?: string, revwdPmCd?: boolean, retrnSdeCd?: boolean, sdwanCd?: boolean,
    csgLvlId?: number, compnyNme?: string, custAdr?: string, custFlrBldgNme?: string,
    custCtyNme?: string, custSttPrvnNme?: string, custCtryRgnNme?: string, custZipCd?: string,
    preStgdKey?: string, submitterName?: string, submitterEmail?: string, submitterPhone?: string,
    notes?: string, cancelCpt?: boolean, customerTypes?: number[], hostedManagedServices?: number[],
    mdsPlnSrvcTiers?: number[], mdsSupportTiers?: number[], mssPlnSrvcTypes?: number[],
    mssMngdAuthenticationProds?: number[], mvsProdTypes?: number[], primSites?: number[],
    relatedInfos?: CptRelatedInfo[], cptDocs?: CptDoc[], cptPrvsn?: CptProvision[], cptHist?: any[],
    gatekeeper?: number, manager?: number, mss?: number, nte?: number, pm?: number, crm?: number,
    reviewedByPMDate?: string, returnedToSDEDate?: string, cptStatus?: string, contactDetails?: ContactDetail[]
  ) {
    this.cptId = cptId || 0;
    this.cptCustNbr = cptCustNbr;
    this.custShrtNme = custShrtNme;
    this.subnetIpAdr = subnetIpAdr;
    this.startIpAdr = startIpAdr;
    this.endIpAdr = endIpAdr;
    this.qipSubnetNme = qipSubnetNme;
    this.mplsIpAdr = mplsIpAdr;
    this.wpaaSIpAdr = wpaaSIpAdr;
    this.submtrUserId = submtrUserId || 0;
    this.seEmailAdr = seEmailAdr;
    this.amEmailAdr = amEmailAdr;
    this.ipmEmailAdr = ipmEmailAdr;
    this.h1 = h1;
    this.devCnt = devCnt || 0;
    this.govtCustCd = govtCustCd || null;
    this.scrtyClrnceCd = scrtyClrnceCd || null;
    this.sprntWhlslReslrCd = sprntWhlslReslrCd || false;
    this.sprntWhlslReslrNme = sprntWhlslReslrNme;
    this.tacacsCd = tacacsCd || false;
    this.encMdmCd = encMdmCd || false;
    this.hstMgndSrvcCd = hstMgndSrvcCd || false;
    this.cptStusId = cptStusId || 0;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.recStusId = recStusId || 1;
    this.preShrdKey = preShrdKey;
    this.revwdPmCd = revwdPmCd || false;
    this.retrnSdeCd = retrnSdeCd || false;
    this.sdwanCd = sdwanCd || false;
    this.csgLvlId = csgLvlId || 0;
    this.compnyNme = compnyNme;
    this.custAdr = custAdr;
    this.custFlrBldgNme = custFlrBldgNme;
    this.custCtyNme = custCtyNme;
    this.custSttPrvnNme = custSttPrvnNme;
    this.custCtryRgnNme = custCtryRgnNme;
    this.custZipCd = custZipCd;
    this.preStgdKey = preStgdKey;

    this.submitterName = submitterName;
    this.submitterEmail = submitterEmail;
    this.submitterPhone = submitterPhone;
    this.notes = notes;
    this.cancelCpt = cancelCpt || false;

    this.customerTypes = customerTypes || [];
    this.hostedManagedServices = hostedManagedServices || [];
    this.mdsPlnSrvcTiers = mdsPlnSrvcTiers || [];
    this.mdsSupportTiers = mdsSupportTiers || [];
    this.mssPlnSrvcTypes = mssPlnSrvcTypes || [];
    this.mssMngdAuthenticationProds = mssMngdAuthenticationProds || [];
    this.mvsProdTypes = mvsProdTypes || [];
    this.primSites = primSites || [];

    this.relatedInfos = relatedInfos || [];
    this.cptDocs = cptDocs || [];
    this.cptPrvsn = cptPrvsn || [];
    this.cptHist = cptHist || [];

    this.gatekeeper = gatekeeper || 0;
    this.manager = manager || 0;
    this.mss = mss || 0;
    this.nte = nte || 0;
    this.pm = pm || 0;
    this.crm = crm || 0;

    this.reviewedByPMDate = reviewedByPMDate;
    this.returnedToSDEDate = returnedToSDEDate;

    this.cptStatus = cptStatus;
    this.contactDetails = contactDetails || [];
  }
}
