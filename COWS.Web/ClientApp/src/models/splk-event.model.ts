import { EntityBase } from "./entity-base.model";
import { SplkEventAccessTag } from "./splk-event-access-tag.model";
import { User } from "./user.model";

export class SplkEvent {
  

  eventId: number;
  eventStusId: number;
  ftn: string;
  charsId: string;
  h1: string;
  h6: string;
  reqorUserId: number;
  salsUserId: number;
  pubEmailCcTxt: string;
  cmpltdEmailCcTxt: string;
  dsgnCmntTxt: string;
  splkEventTypeId : number;
  splkActyTypeId: number;
  ipVerId: number;
  mdsMngdCd: boolean;
  esclCd: boolean;
  primReqDt: Date;
  scndyReqDt: Date;
  esclReasId: number;
  strtTmst: Date;
  extraDrtnTmeAmt: number;
  endTmst: Date;
  wrkflwStusId: number;
  eventCsgLvlId: number;
  eventStusDes: string;
  cnfrcBrdgNbr: string;
  cnfrcPinNbr: string;
  eventDrtnInMinQty: number;
  sowsEventId : number;
  reltdCmpsNcrCd: boolean;
  reltdCmpsNcrNme: string;
  custNme: string;
  custCntctNme: string;
  custCntctPhnNbr: string;
  custEmailAdr: string;
  custCntctCellPhnNbr: string;
  custCntctPgrNbr: string;
  custCntctPgrPinNbr: string;
  eventTitleTxt: string;
  eventDes: string;
  wrkflwStusDes: string;
  reqorUser: User;
  salsUser: User;
  entityBase: EntityBase;
  splkEventAccsTag: SplkEventAccessTag[] = [];
  activators: number[] = [];
  eventSucssActyIds: number[] = [];
  eventFailActyIds: number[] = [];
  reviewCmntTxt: string;
  preCfgConfgCode: string;
  profile: string;
  failCode: number;
  constructor();

  constructor(
    eventId?: number, eventStusId?: number, ftn?: string, charsId?: string, h1?: string, h6?: string, reqorUserId?: number,
    salsUserId?: number, pubEmailCcTxt?: string, cmpltdEmailCcTxt?: string, dsgnCmntTxt?: string, splkEventTypeId?: number, splkActyTypeId?: number,
    ipVerId?: number, mdsMngdCd?: boolean, esclCd?: boolean, primReqDt?: Date, scndyReqDt?: Date, esclReasId?: number, strtTmst?: Date, extraDrtnTmeAmt?: number,
    endTmst?: Date, wrkflwStusId?: number, eventCsgLvlId?: number, eventStusDes?: string, cnfrcBrdgNbr?: string, cnfrcPinNbr?: string, eventDrtnInMinQty?: number,
    sowsEventId?: number, reltdCmpsNcrCd?: boolean, reltdCmpsNcrNme?: string, custNme?: string, custCntctNme?: string, custCntctPhnNbr?: string, custEmailAdr?: string,
    custCntctCellPhnNbr?: string, custCntctPgrNbr?: string, custCntctPgrPinNbr?: string, eventTitleTxt?: string, eventDes?: string, wrkflwStusDes?: string, reqorUser?: User, salsUser?: User,
    entityBase?: EntityBase, splkEventAccsTag?: SplkEventAccessTag[], activators?: number[], eventSucssActyIds?: number[],
    eventFailActyIds?: number[], reviewCmntTxt?: string, preCfgConfgCode?: string, profile?: string, failCode?: number) {
    this.eventTitleTxt = eventTitleTxt;
    this.eventDes = eventDes;
    this.eventId = eventId || 0;
    this.eventStusId = eventStusId || 0;
    this.eventStusDes = eventStusDes;
    this.ftn = ftn;
    this.h1 = h1 || null;
    this.h6 = h6;
    this.eventCsgLvlId = eventCsgLvlId || 0;
    this.custNme = custNme;
    this.custCntctNme = custCntctNme;
    this.custCntctPhnNbr = custCntctPhnNbr;
    this.custEmailAdr = custEmailAdr;
    this.custCntctCellPhnNbr = custCntctCellPhnNbr;
    this.custCntctPgrNbr = custCntctPgrNbr;
    this.custCntctPgrPinNbr = custCntctPgrPinNbr;
    this.reqorUserId = reqorUserId || 0;
    this.reqorUser = reqorUser || null;
    this.cnfrcBrdgNbr = cnfrcBrdgNbr;
    this.cnfrcPinNbr = cnfrcPinNbr;
    this.salsUserId = salsUserId || 0;
    this.salsUser = salsUser || null;
    this.pubEmailCcTxt = pubEmailCcTxt;
    this.cmpltdEmailCcTxt = cmpltdEmailCcTxt;
    this.dsgnCmntTxt = dsgnCmntTxt;
    this.reviewCmntTxt = reviewCmntTxt;
    this.ipVerId = ipVerId || 0;
    this.mdsMngdCd = mdsMngdCd || false;
    this.reltdCmpsNcrCd = reltdCmpsNcrCd || false;
    this.reltdCmpsNcrNme = reltdCmpsNcrNme;
    this.esclCd = esclCd || false;
    this.esclReasId = esclReasId || 0;
    this.strtTmst = strtTmst || new Date();
    this.eventDrtnInMinQty = eventDrtnInMinQty || 60;
    this.extraDrtnTmeAmt = extraDrtnTmeAmt || 0;
    this.endTmst = endTmst || new Date();
    this.primReqDt = primReqDt || null;
    this.scndyReqDt = scndyReqDt || null;
    this.wrkflwStusId = wrkflwStusId || 0;
    this.wrkflwStusDes = wrkflwStusDes;
    this.sowsEventId = sowsEventId || 0;
    this.entityBase = entityBase || new EntityBase();
    this.splkEventAccsTag = splkEventAccsTag || [];
    this.activators = activators || [];
    this.eventSucssActyIds = eventSucssActyIds || [];
    this.eventFailActyIds = eventFailActyIds || [];
    this.preCfgConfgCode = preCfgConfgCode;
    this.profile = profile;
    this.failCode = failCode || 1;
  }
}
