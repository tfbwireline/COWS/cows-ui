export class LkIpMstr {
    id: number;
    strtIpAdr: string;
    endIpAdr: string;
    systmCd: string;
    recStusId: number;
  
    constructor();
    constructor(id?: number, strtIpAdr?: string,
        endIpAdr?: string, systmCd?: string,
        recStusId?: number) {
      this.id = id || 0;
      this.strtIpAdr = strtIpAdr || '';
      this.endIpAdr = endIpAdr || '';
      this.systmCd = systmCd || '';
      this.recStusId = recStusId || 0;
    }
  }
  