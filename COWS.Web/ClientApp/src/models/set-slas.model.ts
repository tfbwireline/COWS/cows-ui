
export class XnciMsSla {
  msSlaId: number;
  msId: number;
  msDesFrom: string;
  msDesTo: string;
  pltfrmNme: string;
  pltfrmCd: string;
  ordrTypeId: number;
  slaInDayQty: number;
  toMsId: number;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;


  constructor();
  constructor(msSlaId?: number, msId?: number, msDesFrom?: string, msDesTo?: string, pltfrmCd?: string, pltfrmNme?: string,ordrTypeId?: number, slaInDayQty?: number, toMsId?: number, 
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string) {
    this.msSlaId = msSlaId || 0;
    this.msId = msId || 0;
    this.msDesFrom = msDesFrom;
    this.msDesTo = msDesTo;
    this.pltfrmCd = pltfrmCd;
    this.pltfrmNme = pltfrmNme;
    this.ordrTypeId = ordrTypeId || 0;
    this.slaInDayQty = slaInDayQty || 0;
    this.toMsId = toMsId || 0;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;

  }
}
