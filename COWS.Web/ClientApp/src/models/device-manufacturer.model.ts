
export class DeviceManufacturer {
  manfId: number;
  manfNme: string;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;

  constructor();
  constructor(manfId?: number, manfNme?: string,
    recStusId?: number, recStatus?: boolean,
    creatDt?: Date, creatByUserId?: number) {
    this.manfId = manfId || 0;
    this.manfNme = manfNme;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
  }
}
