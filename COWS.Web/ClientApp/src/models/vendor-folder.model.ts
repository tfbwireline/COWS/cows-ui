import { VendorFolderContact } from "./vendor-folder-contact.model";
import { VendorFolderTemplate } from "./vendor-folder-template.model";

export class VendorFolder {
  vndrFoldrId: number;
  vndrCd: string;
  vndrNme: string;
  streetAdr1: string;
  streetAdr2: string;
  ctyNme: string;
  prvnNme: string;
  zipPstlCd: string;
  sttCd: string;
  sttNme: string;
  cmntTxt: string;
  vndrEmailListTxt: string;
  creatDt: Date;
  modfdDt?: Date;
  modfdByUserId?: number;
  creatByUserId: number;
  recStusId: number;
  ctryCd: string;
  ctryNme: string;
  bldgNme: string;
  flrId: string;
  rmNbr: string;
  templateCnt: number;

  vndrFoldrCntct: VendorFolderContact[];
  vndrTmplt: VendorFolderTemplate[];
  vndrOrdr: any[];

  constructor();
  constructor(vndrFoldrId?: number, vndrCd?: string, vndrNme?: string,
    streetAdr1?: string, streetAdr2?: string, ctyNme?: string, prvnNme?: string,
    zipPstlCd?: string, sttCd?: string, sttNme?: string, cmntTxt?: string, vndrEmailListTxt?: string,
    creatDt?: Date, modfdDt?: Date, modfdByUserId?: number, creatByUserId?: number,
    recStusId?: number, ctryCd?: string, ctryNme?: string, bldgNme?: string,
    flrId?: string, rmNbr?: string, templateCnt?: number, vndrOrdr?: any[],
    vndrFoldrCntct?: VendorFolderContact[], vndrTmplt?: VendorFolderTemplate[]) {

    this.vndrFoldrId = vndrFoldrId || 0;
    this.vndrCd = vndrCd;
    this.vndrNme = vndrNme;
    this.streetAdr1 = streetAdr1;
    this.streetAdr2 = streetAdr2;
    this.ctyNme = ctyNme;
    this.prvnNme = prvnNme;
    this.zipPstlCd = zipPstlCd;
    this.sttCd = sttCd;
    this.sttNme = sttNme;
    this.cmntTxt = cmntTxt;
    this.vndrEmailListTxt = vndrEmailListTxt;
    this.creatDt = creatDt || new Date();
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || null;
    this.creatByUserId = creatByUserId || 1;
    this.recStusId = recStusId || 1;
    this.ctryCd = ctryCd;
    this.ctryNme = ctryNme;
    this.bldgNme = bldgNme;
    this.flrId = flrId;
    this.rmNbr = rmNbr;
    this.templateCnt = templateCnt || 0;

    this.vndrFoldrCntct = vndrFoldrCntct || [];
    this.vndrTmplt = vndrTmplt || [];
    this.vndrOrdr = vndrOrdr || [];
  }
}
