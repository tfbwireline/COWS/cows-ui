export class UserProfile {
  constructor(public usrPrfId?: number, public usrPrfNme?: string, public usrPrfDes?: string, public recStusId?: number, public srchCd?: number) {
  }
}

export interface IUserProfileResponse {
  map(arg0: (user: any) => UserProfile): any;
  results: UserProfile[];
}

