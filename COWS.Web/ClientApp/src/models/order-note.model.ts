export class OrderNote {
  nteId: number
  nteTypeId: number
  ordrId: number
  nteTxt: string
  creatByUserId: number
  modfdByUserId: number
  recStusId: number
  creatDt: Date
  modfdDt: Date
  creatByUser: string
  nteType: string


  constructor();
  constructor(nteId?: number, nteTypeId?: number, ordrId?: number, nteTxt?: string, creatByUserId?: number,
    modfdByUserId?: number, recStusId?: number, creatDt?: Date, modfdDt?: Date, nteType?: string, creatByUser?: string) {
    this.nteId = nteId || 0;
    this.nteTypeId = nteTypeId || 0;
    this.ordrId = ordrId || 0;
    this.nteTxt = nteTxt || null;
    this.creatByUserId = creatByUserId || 0;
    this.modfdByUserId = modfdByUserId || 0;
    this.recStusId = recStusId || 0;
    this.creatDt = creatDt || null;
    this.modfdDt = modfdDt || null;
    this.creatByUser = creatByUser || null;
    this.nteType = nteType || null;
  }
}
