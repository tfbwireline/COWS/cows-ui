export class RedesignCustBypass {
  id: number;
  h1Cd: string;
  recStusId: number;
  cretdByUserId: number;
  cretdDt: Date;
  modfdByUserId: number;
  modfdDt: Date;
  csgLvlId: number;
  custNme: string;
  custNmeDisp: string;

  constructor();
  constructor(id?: number, h1Cd?: string, recStusId?: number,
    cretdByUserId?: number, cretdDt?: Date, modfdByUserId?: number, modfdDt?: Date, csgLvlId?: number,
    custNme?: string, custNmeDisp?: string) {
    this.id = id || 0;
    this.h1Cd = h1Cd || null;
    this.recStusId = recStusId || 0;
    this.cretdByUserId = cretdByUserId || 0;
    this.cretdDt = cretdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.csgLvlId = csgLvlId || 0;
    this.custNme = custNme || null;
    this.custNmeDisp = custNmeDisp || null;
  }
}
