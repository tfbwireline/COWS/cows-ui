export class H5Doc {
  h5DocId: number;
  h5FoldrId: number;
  fileNme: string;
  fileCntnt: Int8Array;
  fileSizeQty: number;
  cmntTxt: string;
  creatDt: Date;
  modfdDt?: Date
  creatByUserId: number;
  creatByUserAdid: string;
  modfdByUserId?: number;
  recStusId: number;
  base64string: string;

  constructor();
  constructor(h5DocId?: number, h5FoldrId?: number, fileNme?: string, fileCntnt?: Int8Array,
    fileSizeQty?: number, cmntTxt?: string, creatDt?: Date, modfdDt?: Date,
    creatByUserId?: number, creatByUserAdid?: string, modfdByUserId?: number,
    recStusId?: number, base64string?: string
  ) {
    this.h5DocId = h5DocId || 0;
    this.h5FoldrId = h5FoldrId || 0;
    this.fileNme = fileNme;
    this.fileCntnt = fileCntnt || null;
    this.fileSizeQty = fileSizeQty || 0;
    this.cmntTxt = cmntTxt;
    this.creatDt = creatDt || new Date();
    this.modfdDt = modfdDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.creatByUserAdid = creatByUserAdid;
    this.modfdByUserId = modfdByUserId || null;
    this.recStusId = recStusId || 0;
    this.base64string = base64string || null;
  }
}
