import { MDSEventNtwkTrpt } from "./mds-event-ntwk-trpt.model";
import { MDSEventNtwkCust } from "./mds-event-ntwk-cust.model";
import { MDSRelatedCeEvent } from "./mds-related-ce-event.model";

export class MDSEventH6Lookup {
  h1: string;
  h1CustNme: string;
  h6: string;
  customerName: string;
  csgLvlId: number;
  designDocumentApprovalNumber: string;
  salesEngineerEmail: string;
  salesEngineerPhone: string;
  groupName: string;
  networkCustomer: MDSEventNtwkCust[];
  networkTransport: MDSEventNtwkTrpt[];
  relatedCEEvents: MDSRelatedCeEvent[];

  constructor();
  constructor(h1?: string, h6?: string, customerName?: string, csgLvlId?: number,
    designDocumentApprovalNumber?: string, salesEngineerEmail?: string, salesEngineerPhone?: string, groupName?: string,
    networkCustomer?: MDSEventNtwkCust[], networkTransport?: MDSEventNtwkTrpt[], relatedCEEvents?: MDSRelatedCeEvent[]) {
    this.h1 = h1 || "";
    this.h6 = h6 || "";
    this.customerName = customerName || "";
    this.csgLvlId = csgLvlId || 0;
    this.designDocumentApprovalNumber = designDocumentApprovalNumber || "";
    this.salesEngineerEmail = salesEngineerEmail || "";
    this.salesEngineerPhone = salesEngineerPhone || "";
    this.groupName = groupName || "";
    this.networkCustomer = networkCustomer || null;
    this.networkTransport = networkTransport || null;
    this.relatedCEEvents = relatedCEEvents || null;
  }
}
