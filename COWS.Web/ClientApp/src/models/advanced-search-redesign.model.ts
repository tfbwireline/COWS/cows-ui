export class AdvancedSearchRedesign {
  type: number
  status: number
  redesignNo: string
  customer: string
  h1: string
  redesignCategory: number
  redesignType: number
  redesignStatus: number
  odieDeviceName: string
  nteAssigned: string
  pmAssigned: string
  eventId: string
  mssSeAssigned: string
  neAssigned: string
  submittedDate: string
  slaDueDate: string
  expirationDate: string

  redesignId: number
  statusDesc: string

  constructor() {

  }
}
