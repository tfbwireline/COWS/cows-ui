export class RedesignWrkflw {
  preWorkflowStatusID: number
  preWorkFlowStatus: string
  desiredWorkflowStatusID: number
  desiredWorkflowStatus: string

  constructor();
  constructor(preWorkflowStatusID?: number, preWorkFlowStatus?: string, desiredWorkflowStatusID?: number, desiredWorkflowStatus?: string) {
    this.preWorkflowStatusID = preWorkflowStatusID || 0;
    this.preWorkFlowStatus = preWorkFlowStatus || null;
    this.desiredWorkflowStatusID = desiredWorkflowStatusID || 0;
    this.desiredWorkflowStatus = desiredWorkflowStatus || null;
  }
}
