export class NgvnEventCktIdNua {
  eventId: number;
  cktIdNua: string;
  creatDt: Date;

  constructor(eventId?: number, cktIdNua?: string, creatDt?: Date) {
    this.eventId = eventId || 0;
    this.cktIdNua = cktIdNua || null;
    this.creatDt = creatDt || null;
  }
}
