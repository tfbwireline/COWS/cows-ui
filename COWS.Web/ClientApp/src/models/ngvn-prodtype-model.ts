
export class NgvnProdType {
  ngvnProdTypeId: number;
  ngvnProdTypeDes: string;
  recStusId: number;
  creatDt: Date;
  creatByUserId: number;
  modfdDt: Date;
  modfdByUserId: number;

  constructor();
  constructor(ngvnProdTypeId?: number, ngvnProdTypeDes?: string, recStusId?: number, 
    creatDt?: Date, creatByUserId?: number,
    modfdDt?: Date, modfdByUserId?: number,) {
    this.ngvnProdTypeId = ngvnProdTypeId || 0;
    this.ngvnProdTypeDes = ngvnProdTypeDes;
    this.recStusId = recStusId || 1;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
  }
}
