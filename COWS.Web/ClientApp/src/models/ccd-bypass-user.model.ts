export class CCDBypassUser {
  configID: number
  userADID: string
  userName: string

  constructor();
  constructor(configID?: number, userADID?: string, userName?: string) {
    this.configID = configID || 0;
    this.userADID = userADID;
    this.userName = userName;
  }
}
