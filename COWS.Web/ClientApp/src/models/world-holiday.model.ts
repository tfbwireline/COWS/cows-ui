
export class WorldHoliday {
  wrldHldyId: number;
  wrldHldyDes: string;
  wrldHldyDt: Date;
  ctyNme: string;
  ctryCd: string;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;
  modfdDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;

  constructor();
  constructor(wrldHldyId?: number, wrldHldyDes?: string, wrldHldyDt?: Date,
    ctyNme?: string, ctryCd?: string, recStusId?: number, recStatus?: boolean,
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string,
    modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
    this.wrldHldyId = wrldHldyId || 0;
    this.wrldHldyDes = wrldHldyDes;
    this.wrldHldyDt = wrldHldyDt || null;
    this.ctyNme = ctyNme;
    this.ctryCd = ctryCd;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.modifiedByAdId = modifiedByAdId;
  }
}
