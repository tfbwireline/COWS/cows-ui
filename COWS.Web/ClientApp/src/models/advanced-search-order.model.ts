export class AdvancedSearchOrder {
  type: number
  status: number
  m5_no: string
  isRelatedFtn: boolean
  name: string
  ccd: string
  privateLine: string
  h1: string
  vendorName: string
  prsQuote: string
  vendorCircuit: string
  productType: number
  soi: string
  h5_h6: string
  orderSubType: number
  orderType: number
  workGroup: number
  nua: string
  platformType: string
  region: number
  parentM5_no: string
  purchaseOrderNumber: string
  atlasWorkOrderNumber: string
  preQualId: string
  deviceId: string
  siteId: string
  peopleSoftRequisitionNumber: string

  constructor() {

  }
}
