
export class SetPreference {
  cnfrcBrdgId: number;
  cnfrcBrdgNbr: string;
  cnfrcPinNbr: string;
  soi: string;
  onlineMeetingAdr: string;
  recStusId: number;
  recStatus: boolean;
  creatDt: Date;
  creatByUserId: number;
  createdByAdId: string;
  modfdDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;

  constructor();
  constructor(cnfrcBrdgId?: number, cnfrcBrdgNbr?: string, cnfrcPinNbr?: string, soi?: string, onlineMeetingAdr?: string,
    recStusId?: number, recStatus?: boolean,creatDt?: Date, creatByUserId?: number, createdByAdId?: string, modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
    this.cnfrcBrdgId = cnfrcBrdgId || 0;
    this.cnfrcBrdgNbr = cnfrcBrdgNbr;
    this.cnfrcPinNbr = cnfrcPinNbr ;
    this.soi = soi;
    this.onlineMeetingAdr = onlineMeetingAdr;
    this.recStusId = recStusId || 1;
    this.recStatus = recStatus || true;
    this.creatDt = creatDt || null;
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.modifiedByAdId = modifiedByAdId;
  }
}
