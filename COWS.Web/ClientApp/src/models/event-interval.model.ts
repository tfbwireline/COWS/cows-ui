
export class EventInterval {
  eventTypeId: number;
  eventTypeNme: string;
  nonCpeNtvlInDayQty: number;
  cpeNtvlInDayQty: number;
  recStusId: number;
  creatByUserId: number;
  createdByAdId: string;
  creatDt: Date;
  modfdByUserId: number;
  modifiedByAdId: string;
  modfdDt: Date;

  constructor();
  constructor(eventTypeId?: number, eventTypeNme?: string, nonCpeNtvlInDayQty?: number, cpeNtvlInDayQty?: number, recStusId?: number,
    creatDt?: Date, creatByUserId?: number, createdByAdId?: string,
    modfdDt?: Date, modfdByUserId?: number, modifiedByAdId?: string) {
    this.eventTypeId = eventTypeId || 0;
    this.eventTypeNme = eventTypeNme;
    this.nonCpeNtvlInDayQty = nonCpeNtvlInDayQty || 0;
    this.cpeNtvlInDayQty = cpeNtvlInDayQty || 0;
    this.recStusId = recStusId || 1;
    this.creatDt = creatDt || new Date();
    this.creatByUserId = creatByUserId || 0;
    this.createdByAdId = createdByAdId;
    this.modfdDt = modfdDt || null;
    this.modfdByUserId = modfdByUserId || 0;
    this.modifiedByAdId = modifiedByAdId;
  }
}
