export class AdditionalCosts {
  orderID: number;
  chargeTypeID: number;
  chargeTypeDesc: string;
  version: number;
  isTerm: boolean;
  currencyCd: string;
  chargeNRC: number;
  chargeNRCUSD: number;
  createdByDspNme: string;
  createdByUserName: string;
  createdByUserId: number;
  createdDateTime: Date;
  taxRate: number;
  note: string;
  billOnly: boolean;
  expirationTime: Date;
  statusID: number;
  statusDes: string;
  isUpdated: boolean;
  isAdded: boolean;

  constructor({ orderID = 0, chargeTypeID = null, chargeTypeDesc = null, version = null, isTerm = null, currencyCd = null, chargeNRC = 0, chargeNRCUSD = 0, createdByDspNme = null, createdByUserName = null, createdByUserId = null,
    createdDateTime = null, taxRate = 0, note = "", billOnly = false, expirationTime = null, statusID = 0, statusDes = null, isUpdated = false, isAdded = false
  }) {
    this.orderID = orderID
    this.chargeTypeID = chargeTypeID
    this.chargeTypeDesc = chargeTypeDesc
    this.version = version
    this.isTerm = isTerm
    this.currencyCd = currencyCd
    this.chargeNRC = chargeNRC
    this.chargeNRCUSD = chargeNRCUSD
    this.createdByDspNme = createdByDspNme
    this.createdByUserName = createdByUserName
    this.createdByUserId = createdByUserId
    this.createdDateTime = createdDateTime
    this.taxRate = taxRate
    this.note = note
    this.billOnly = billOnly
    this.expirationTime = expirationTime
    this.statusID = statusID
    this.statusDes = statusDes
    this.isUpdated = isUpdated
    this.isAdded = isAdded
  }
  //constructor(isTerm: boolean, billOnly: boolean, orderID?: number, chargeTypeID?: number, chargeTypeDesc?: string, version?: number,
  //  currencyCd?: string, chargeNRC?: number, chargeNRCUSD?: number,
  //  createdByDspNme?: string, createdByUserName?: string, createdByUserId?: number, createdDateTime?: Date,
  //  taxRate?: number, note?: string, expirationTime?: Date, salesSupportStatusID?: number, salesSupportStatusDes?: string) {
  //  this.orderID = orderID || 0;
  //  this.chargeTypeID = chargeTypeID || 0;
  //  this.chargeTypeDesc = chargeTypeDesc || null;
  //  this.version = version || 0;
  //  this.isTerm = isTerm;
  //  this.currencyCd = currencyCd || null;
  //  this.chargeNRC = chargeNRC || 0;
  //  this.chargeNRCUSD = chargeNRCUSD || 0;
  //  this.createdByDspNme = createdByDspNme || null;
  //  this.createdByUserName = createdByUserName || null;
  //  this.createdByUserId = createdByUserId || 0;
  //  this.createdDateTime = createdDateTime || null;
  //  this.taxRate = taxRate || 0;
  //  this.note = note || null;
  //  this.billOnly = billOnly;
  //  this.expirationTime = expirationTime || null;
  //  this.salesSupportStatusID = salesSupportStatusID || 0;
  //  this.salesSupportStatusDes = salesSupportStatusDes || null;
  //}
}
