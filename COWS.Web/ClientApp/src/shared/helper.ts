import { Injectable } from "@angular/core";
import { Global, KeyValuePair, ENUM_OPTIONS, EEventProfile, WorkGroup, OldWorkGroup } from './global';
import notify from 'devextreme/ui/notify';
import { FormGroup } from "@angular/forms";
import { UserProfile } from "../models";

@Injectable()
export class Helper {
  public form: FormGroup;

  constructor() {

  }

  public toLocalDate(date: Date) {
    return (date == undefined) ? date : date.toString() + "Z";
  }

  public formatDate(date: Date) {
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let seconds = date.getSeconds();
    let ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    //minutes = minutes < 10 ? '0' + minutes : minutes;
    //seconds = seconds < 10 ? '0' + seconds : seconds;
    let minStr = minutes < 10 ? '0' + minutes : minutes;
    let secStr = seconds < 10 ? '0' + seconds : seconds;
    let strTime = hours + ':' + minStr + ':' + secStr + ' ' + ampm;
    let fullDate = month + '/' + day + '/' + year + ' ' + strTime;
    return fullDate;
  }

  // CREDITS TO THE OWNER (Kip ~ StackOverflow)
  /**
   * Adds time to a date. Modelled after MySQL DATE_ADD function.
   * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
   * https://stackoverflow.com/a/1214753/18511
   * 
   * @param date  Date to start with
   * @param interval  One of: year, quarter, month, week, day, hour, minute, second
   * @param units  Number of units of the given interval to add.
   */
  public dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }

  /**
   * Get all keys of enum
   *
   * @param enumType Enum of values
   * @param option ENUM_OPTIONS.Keys or 0 = keys only; ENUM_OPTIONS.Aliases or 1 = aliases only; ENUM_OPTIONS.All or 2 = keys and aliases
   */
  public getEnumKeys(enumType, option: number = ENUM_OPTIONS.Keys) {
    if (option == ENUM_OPTIONS.Keys) {
      return Object.keys(enumType).filter(key => isNaN(+key) && !this.hasWhiteSpace(key))
    } else if (option == ENUM_OPTIONS.Aliases) {
      return Object.keys(enumType).filter(key => isNaN(+key) && this.hasWhiteSpace(key))
    }

    return Object.keys(enumType).filter(key => isNaN(+key))
  }

  /**
   * Get all key-value pair of enum
   * 
   * @param enumType Enum of values
   * @param option 0 = keys only, 1 = aliases only, 2 = keys and aliases
   */
  public getEnumKeyValuePair(enumType, option: number = ENUM_OPTIONS.Keys): KeyValuePair[] {
    return this.getEnumKeys(enumType, option).map(key => new KeyValuePair(key, enumType[key]))
  }

  public hasWhiteSpace(str) {
    return /\s/g.test(str);
  }

  public isEmpty(a): boolean {
    //return (a === null || a === "null" || a === undefined || a === "undefined" || a === "") ? true : false;
    return (a == null || a == undefined || a === "") ? true : false;
  }

  public isReallyEmpty(a): boolean {
    let status = (a == null || a == undefined || a === "") ? true : false;
    if(!status && typeof(a) == 'string'){
      return a.trim().length > 0 ? false : true
    }
    return status;
  }

  public ifEmpty(a, b): any {
    return this.isEmpty(a) ? b : a;
  }

  public getDistinct(arr: any[]) {
    let temp: any[] = []

    arr.map(a => {
      if (!temp.includes(a)) {
        temp.push(a)
      }
    });

    return temp;
  }

  public getFileSizeFormat(bytes: number = 0, precision: number = 2): string {
    if (bytes == 0) return '0 Byte';
    var kb = 1024,
      dec = precision <= 0 ? 0 : precision || 2,
      units = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(kb));
    return parseFloat((bytes / Math.pow(kb, i)).toFixed(dec)) + ' ' + units[i];
  }

  public notify(msg: string, type: string) {
    // Use this block of commented codes when needed to change the position of the notification
    //var opt = {
    //  position: { my: Global.NOTIFY_POSITION, at: Global.NOTIFY_POSITION, of: window },
    //  message: msg
    //};

    //notify(opt, type, Global.NOTIFY_DURATION);
    notify(msg, type, Global.NOTIFY_DURATION);
  }

  public getErrorMessage(error: any) {
    if (!this.isEmpty(error) && !this.isEmpty(error.message))
      return error.message;

    return '';
  }

  public notifySavedFormMessage(savedFormObj: string, type: string, isCreated: boolean, error: any) {
    var msg;

    // Create Notification Message for Successful create/update
    if (type == Global.NOTIFY_TYPE_SUCCESS) {
      if (isCreated) {
        msg = `Successfully created ${savedFormObj}.`;
      }
      else {
        msg = `Successfully updated ${savedFormObj}.`;
      }
    }
    // Create Notification Message for Unsuccessful create/update
    else if (type == Global.NOTIFY_TYPE_ERROR) {
      msg = this.hasProperty(error, "message", true)
      if (msg != undefined) {
        if (msg.hasOwnProperty("duplicate") || msg.indexOf('duplicate') !== -1) {
          msg = `${savedFormObj} already exists.`;
        }
      }
      else {
        if (isCreated) {
          msg = `Failed to create ${savedFormObj}. ` + error;
          //msg = `Failed to create ${savedFormObj}. `;
        }
        else {
          msg = `Failed to update ${savedFormObj}. ` + error;
          //msg = `Failed to update ${savedFormObj}. `;
        }
      }
    }
    
    this.notify(msg, type);
  }

  public notifyRemoveFormMessage(hasError: boolean, length: string, failed: boolean) {
    var msg;
    var type;

    if (hasError) {
      if (failed) {
        msg = `Failed to deleted record/s.`;
        type = Global.NOTIFY_TYPE_ERROR;
      } else {
        msg = `Successfully deleted record/s except for ${length} selected items.`;
        type = Global.NOTIFY_TYPE_WARNING;
      }
    } else {
      msg = 'Successfully deleted record/s.';
      type = Global.NOTIFY_TYPE_SUCCESS;
    }

    this.notify(msg, type);
  }

  public notifyDeactivateFormMessage(hasError: boolean, length: string, failed: boolean) {
    var msg;
    var type;

    if (hasError) {
      if (failed) {
        msg = `Failed to deactivated record/s.`;
        type = Global.NOTIFY_TYPE_ERROR;
      } else {
        msg = `Successfully deactivated record/s except for ${length} selected items.`;
        type = Global.NOTIFY_TYPE_WARNING;
      }
    } else {
      msg = 'Successfully deactivated record/s.';
      type = Global.NOTIFY_TYPE_SUCCESS;
    }

    this.notify(msg, type);
  }

  public setFormControlValue(name: string, value: any) {
    if (this.form.get(name)) {
      this.form.get(name).setValue(value);
    }
  }

  public getFormControlValue(name: string): any {
    let value = "";
    if (this.form.get(name)) {
      value = this.form.get(name).value;
    }

    return value;
  }
/**
 * Search thru hieararchy of object/s if a property name (first instance) of dotten notation exists
 * Example: hasProperty({name: 'John Wick', likes: { gun: true, dog: true}}, 'name', )  //returns true
 * Example: hasProperty({name: 'John Wick', likes: { gun: true, dog: true}}, 'dog')  //returns true
 * Example: hasProperty({name: 'John Wick', likes: { gun: true, dog: true}}, 'killer')  //returns false
 *
 * @param obj  Object to be searched with
 * @param prop  Property name to be searched
 */
  public hasProperty(obj: object, prop: string, value: boolean) {
    if (obj == null) return value ? undefined : false
    if (obj[prop] != undefined) return value ? obj[prop] : true

    let result = value ? undefined : false
    for (let child in obj) {
      if (obj.hasOwnProperty(prop) && typeof obj[child] == "object") {
        result = this.hasProperty(obj[child], prop, value)

        if (result) {
          return result
        }
      }
    }

    return result;
  }

  public getFinalProfileName(userProfile: UserProfile) {
    if (userProfile.usrPrfNme.includes(EEventProfile.MEMBER) || userProfile.usrPrfNme.includes("SALESSUPPORT")) {
      return EEventProfile.MEMBER;
    } else if (userProfile.usrPrfNme.includes(EEventProfile.REVIEWER)) {
      return EEventProfile.REVIEWER;
    } else if (userProfile.usrPrfNme.includes(EEventProfile.ACTIVATOR)) {
      return EEventProfile.ACTIVATOR;
    } else if (userProfile.usrPrfNme.includes(EEventProfile.MNSPreConfigEng)) {
      return EEventProfile.MNSPreConfigEng;
    } else if (userProfile.usrPrfNme.includes(EEventProfile.MDSPM)) {
      return EEventProfile.MDSPM;
    } else if (userProfile.usrPrfNme.includes(EEventProfile.MNSActivator)) {
      return EEventProfile.MNSActivator;
    }

    return null
  }

  public validateEventDeleteStatus(wfStatus: number, eventStatus: number,
    createdBy: string, logonUser: string): string {
    let error = '';

    if (wfStatus == 11) {
      if (createdBy != logonUser) {
        error = "Cannot delete an event that is created by other members.";
      }

      if (!(eventStatus == 1 || eventStatus == 3)) {
        error = "Cannot delete an event that is not in Rework or Visible status.";
      }
    }

    return error;
  }

  public checkStartTime(hrs: number, startDate: Date, endDate: Date): boolean {
    let start = new Date(startDate);
    let end = new Date(endDate);
    if (hrs == -1) {
      if (start.getTime() >= end.getTime()) {
        return false;
      }
      else {
        return true;
      }
    }
    else if (hrs == -2) {
      // Difference 5 mins
      if (end.getTime() < (start.getTime() + (5 * 60 * 1000))) {
        return false;
      }
      else {
        return true;
      }
    }
    else {
      let newDate = new Date();

      if (hrs == 48) {
        if ((newDate.getDay() == 4) || (newDate.getDay() == 5)) {
          hrs = 96;
        }
      }

      var diffhrs = hrs * 60 * 60 * 1000;

      if ((newDate.getTime() + diffhrs) > start.getTime()) {
        return false;
      }
      else {
        return true;
      }
    }
  }

  public isValidFileType(fileName: string): boolean {

    // this method can be modified to set certain extensions that are valid.
    const validExtensions = ["doc", "docx", "xls", "xlsx", "ppt", "pptx","pdf", "vsd", "jpg", "png", "gif", "txt"]
    const extension = fileName.split('.').pop().toLowerCase(); 
    return validExtensions.includes(extension);
  }

  public isFileSizeValid(size: number): boolean {
    // 1MB = 100000B
    return size <= 10000000;
  }

  public getReWriteWorkGroup(workGroup: number) {
    switch(workGroup) {
      case OldWorkGroup.GOM:
        return WorkGroup.GOM;
      case OldWorkGroup.MDS:
        return WorkGroup.MDS;
      case OldWorkGroup.CSC:
        return WorkGroup.CSC;
      // case OldWorkGroup.SalesSupport:
      //   return WorkGroup.SalesSupport;
      case OldWorkGroup.AMNCI:
        return WorkGroup.xNCIAmerica;
      case OldWorkGroup.ANCI:
        return WorkGroup.xNCIAsia;
      case OldWorkGroup.ENCI:
        return WorkGroup.xNCIEurope;
      // case OldWorkGroup.WBO:
      //   return WorkGroup.WBO;
      // case OldWorkGroup.CAND:
      //   return WorkGroup.CAND;
      case OldWorkGroup.SIPTnUCaaS:
        return WorkGroup.UCaaS;
    }
  }
  
  public gotoEventHistory(id: number) {
    window.open('/tools/event-history/' + id, "_blank", "toolbar=0,location=0,menubar=0")
  }

  public getMaxDateTime(date: any) {

    // Use to max out the date and time example
    // July 25 ,2020: 12:00:00 AM
    // Returns July 25 : 11:59:59 PM
    let newDate: any = new Date(new Date(date).toLocaleDateString())
    newDate.setDate(newDate.getDate() + 1);

    return new Date(newDate - 1000);
  }

  public dateObjectToString(d: any, toMidnight : boolean = false) {

    if (this.isEmpty(d))
      return null;

    let newDate: any = toMidnight ?  new Date(new Date(d).setHours(0,0,0,0)) : new Date(new Date(d))
    // Added option on toLocaleDateString() to fix European DateTime Issue
    // enUS format is mm/dd/yyyy while en-GB format is dd/mm/yyyy
    //const date = newDate.toLocaleDateString();
    const date = newDate.toLocaleDateString('en-US', {
      month: '2-digit', day: '2-digit', year: 'numeric'
    });
    const time = newDate.toLocaleTimeString();
    return `${date} ${time}`;
  }

  public formatRecurrenceDesText(recurrenceRule: string, endDate: string) {

    // No need to restructure rule if no Until
    if(!recurrenceRule.includes('UNTIL')) {
      return recurrenceRule;
    }

    // End Date Example : 2021-03-15T11:00:00
    // Get Time only and remove all  colon(s)':' and Transform to Example T121500Z = 12:15:00PM 
    var endTime = `T${endDate.substring(11).replace(/:\s*/g,'')}Z`;

    // ReccurenceRule Example : FREQ=DAILY;UNTIL=20210318T155959Z
    var arr = recurrenceRule.split(';');

    // Find index of UNTIL to be manipulated
    const index = arr.findIndex(x => x.includes('UNTIL'));
    
    // After finding the index the end date time will be attached to the Rule.
    if(index >= 0) {
      arr[index] = arr[index].substring(0,14) + endTime;
    }
    
    // Restructure  back the rule to its 
    return arr.join(';');
  }

  public ArrayValueStringReplace(data: any, keys: Array<string> = []): any {

    let newData = []
    if(keys.length > 0) {
      newData = data.map(record => {
        const newRecord = record;
        for(let i = 0; i < keys.length; i++) {
          const key = keys[i];
          const value = record[key];
  
          newRecord[key] = this.Replace(value);
        }
        return newRecord;
      })
    } else {
      newData = data.map(record => {
        return this.Replace(record);
      });
    }
    return newData;
  }

  public Replace(textToReplace : string, replaceWith: string = null, regex: RegExp = null): string {

    if(regex === null) {
      regex = /GOM/g;
    }

    if(replaceWith === null) {
      replaceWith = "Intl CPE";
    }
    let newText = textToReplace.replace(regex, replaceWith);
    return newText;

  } 
  public checkDateIfWeekend(date: any) {
    
    if(typeof(date) == 'string') {
      return (new Date(date).getDay() == 6 || new Date(date).getDay() == 0)
    }
    else {
      return (new Date(date).getDay() == 6 || new Date(date).getDay() == 0)
    }
  }

  public removeUnknownCharacter(str: string) {
    return str.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
  }

  public removeSpaces(string : string) {
    if(string) {
      return string.replace(/\s/g, '');
    }
    return string;
  }

}