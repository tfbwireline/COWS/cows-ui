import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { NG_VALIDATORS, FormControl, Form } from '@angular/forms';

@Directive({
  selector: '[textValidator][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: TextValidatorDirective,
      multi: true
    }
  ]
})
export class TextValidatorDirective {
  validator: Function;
  formControl: FormControl;

  constructor() {
    this.validator = this.validateTextField();
  }

  validate(c: FormControl) {
    return this.validator(c);
  }

  @HostListener('blur', ['$event'])
  public onInput(event): void {
    // Regular Expresion for Html Tags
    var regTag = /<\/?\w+((\s+\w+(\s*=\s*(?:\".*?"|'.*?'|[^'\">\s]+))?)+\s*|\s*)\/?>/ig;

    // Replace with whitespace all possible tags
    var val = String(event.target.value).trim();
    val = val.replace(regTag, "").trim();

    // Set new value to the form control
    this.formControl.setValue(val);
  }

  validateTextField(): Function {
    return (c: FormControl) => {
      this.formControl = c;
      // Regex for not allowed character: Single Quote ('), Double (")
      // Additional characters not allowed as requested by Kumar due to Ban Validation:
      //        Less Than(<), Greater Than(>)
      var regChar = new RegExp("\"|'|<|>", "i");

      return regChar.test(c.value)
        ? {
          textValidator: {
            valid: false
          }
        }
        : null;
    };
  }
}
