import { Injectable } from '@angular/core'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router'
import { NavLinkService } from '../services/nav-link.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  private validMenu: string;

  constructor(private router: Router, private navLinkService: NavLinkService) {
  }

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    let validUser = localStorage.getItem('validUser');
    let url = state.url.split('/');
    let menuCount = localStorage.getItem('menuCount');

    this.navLinkService.validateMenu(url).subscribe(res => {
      this.validMenu = res;
      // Inactive users will be redirected to unauthorized page.
      // Should only happen if the user tries to force their way
      if (validUser == "true" && this.validMenu == "authorized") {
        //console.log("A")
        //if (menuCount == '0') {
        //  this.router.navigate(['/authorize'], { queryParams: { returnUrl: state.url } })
        //  return false;
        //} else { return true; }
        return true
      }
      else if (validUser == "true" && this.validMenu == "You are not an authorized user to initiate cancel") {
        //console.log("B")
        this.router.navigate(['/cancel'], { queryParams: { returnUrl: state.url } })
        return false;
      }
      else if (validUser == "true" && this.validMenu == "CSG level 2 is required to view Fedline events") {
        //console.log("B")
        this.router.navigate(['/cancel'], { queryParams: { returnUrl: state.url } })
        return false;
      }
      else {
        //console.log("C")
        this.router.navigate(['/authorize'], { queryParams: { returnUrl: state.url } })
        return false;
      }
    });
    return true;
    
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(childRoute, state);
  }
}
