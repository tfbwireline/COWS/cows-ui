import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from '../services/user.service';
import { SearchCode } from '../models';

@Injectable()
export class UserResolver implements Resolve<any> {

  constructor(public userSrvc: UserService) { }

  resolve() {
    return this.userSrvc.getLoggedInUser2().then(res => {
      return res as SearchCode;
    });
  }
}
