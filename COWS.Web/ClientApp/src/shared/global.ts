export const Global = Object.freeze({
  NOTIFY_DURATION: 4000,
  NOTIFY_POSITION: "bottom",
  NOTIFY_TYPE_SUCCESS: "success",
  NOTIFY_TYPE_ERROR: "error",
  NOTIFY_TYPE_WARNING: "warning",
  NOTIFY_TYPE_INFO: "info",
  REC_STATUS_ACTIVE: 1,
  REC_STATUS_INACTIVE: 0
});

export const ErrorMsg = Object.freeze({
  EVENT_H1_APPROVEDREDESIGN: "There is no approved redesign data associated for this H1. Please try with a different H1 or check Redesign Data.",
  EVENT_H1_NOCUSTOMERNAME_UCAAS: "ODIE returned no customer names for this H1 value. If product type is 'Smart UC' or 'Smart UC Toll Free' then select it and Lookup by H6 and CCD to populate Customer Name."
});

// General Enums
export enum ENUM_OPTIONS {
  Keys = 0,
  Aliases = 1,
  All = 2
}

export enum RB_CONDITION {
  Yes = 1,
  No = 0
}

export enum AUTH_STATUS {
  Authorized = 1,
  UnAuthorized = 2,
  NonSensitive = 3
}

// Event Enums
export enum EVENT_STATUS {
  Visible = 1,
  Pending = 2,
  Rework = 3,
  Published = 4,
  InProgress = 5,
  Completed = 6,
  OnHold = 7,
  Delete = 8,
  Fulfilling = 9,
  Shipped = 10,
  Cancelled = 13,
  CompletedDeviceInaccessible = 14,
  DisconnectRequest = 15,
  PendingDeviceReturn = 16,
  Submitted = 17,
  CompletePendingUAT = 18,
  InProgressUAT = 19,
  OnHoldUAT = 20
}

export enum EVENT_STATUS_EMAIL_CD {
  Visible = 1,
  Pending = 2,
  Submitted = 17,
  Published = 4,
  InProgress = 5,
  Fulfilling = 9,
  Shipped = 10,
  Rework = 3,
  OnHold = 7,
  Delete = 8,
  Completed = 6,
}

export enum WORKFLOW_STATUS {
  Visible = 1,
  Submit = 2,
  Retract = 3,
  Publish = 4,
  Reschedule = 5,
  InProgress = 6,
  Complete = 7,
  Return = 8,
  Reject = 9,
  OnHold = 10,
  Delete = 11,
  Fulfilling = 12,
  Shipped = 13,
  PendingDeviceReturn = 14,
  CompletePendingUAT = 15,
  InprogressUAT = 16
}

export enum EVENT_VIEWS {
  MyEvents = 1,
  MyCompleted = 2,
  MyPending = 3,
  MyPublished = 4,
  MyRework = 5,
  MyVisible = 6,
  AllEvents = 7,
  AllCompleted = 8,
  AllPending = 9,
  AllPublished = 10,
  AllRework = 11,
  AllVisible = 12,
  'My Events' = MyEvents,
  'My Completed' = MyCompleted,
  'My Pending' = MyPending,
  'My Published' = MyPublished,
  'My Rework' = MyRework,
  'My Visible' = MyVisible,
  'All Events' = AllEvents,
  'All Completed' = AllCompleted,
  'All Pending' = AllPending,
  'All Published' = AllPublished,
  'All Rework' = AllRework,
  'All Visible' = AllVisible
}

export enum EEventType {
  AD = 1,
  NGVN = 2,
  MPLS = 3,
  SprintLink = 4,
  MDS = 5,
  Fedline = 9,
  SIPT = 10,
  UCaaS = 19,
  All = 99
}

export enum EEnhanceService {
  STANDARD = 1,
  VAS = 2,
  SLNK_STANDARD = 3,
  NGVN_STANDARD = 4,
  MDS = 5,
  Firewall_Security = 6,
  AD_BROADBAND = 7,
  AD_NARROWBAND = 8,
  AD_GOVERNMENT = 9,
  MSS = 10,
  AD_INTERNATIONAL = 11,
  AD_TMT = 12,
  UCaaS = 12
}

export enum EEventProfile {
  MEMBER = "Member",
  REVIEWER = "Reviewer",
  ACTIVATOR = "Activator",
  MNSPreConfigEng = "Fedline Preconfig Engineer",
  MDSPM = "Fedline MDS PM",
  MNSActivator = "Fedline MNS Activator"
}

export enum FedlineActivityType {
  Install = 0,
  Disconnect = 1,
  HeadendInstall = 2,
  HeadendMAC = 3,
  HeadendDisconnect = 4,
  HeadendCreate = 5,
  Refresh = 6,
}

export enum EUcaasActivityType {
  Install = 1,
  Move = 2,
  Disconnect = 3,
  Change_AddRemoveUser = 4,
  Change_UserFeatureGroupChange = 5,
  Change_ProgrammingChange = 6,
  Change_Other = 7,
  Change_SPS = 8
}

export enum EUcaasPlanType {
  SCC = 1,
  SEVEN7GL = 2,
  TWO2GJ = 3,
  UC7 = 4,
  UCB = 5,
  UCT = 6,
  MET = 7,
  MEL = 8,
  SMP = 9
}

export enum EUcaasProdType {
  CISCO_HCS = 1,
  MIPT = 2,
  SMART_UC = 3,
  SMART_UC_TOLL_FREE = 4,
  SIPT = 5,
  SDV = 6
}


export enum PRIMARY_OR_BACKUP {
  Primary = "P",
  Backup = "B"
}

export enum YES_OR_NO {
  Yes = "Y",
  No = "N"
}

export enum SPRINT_OR_CUSTOMER {
  "T-Mobile" = "S",
  Customer = "C"
}

export enum WIRELESS_TYPE_CD {
  GGG = "3G",
  GGGG = "4G"
}

export enum RB_CableProtocolType {
  SIP = "SIP",
  MGCP = "MGCP"
}

// Order Enums
export enum EordrCat {
  IPL = 1,
  FSA,
  Vendor,
  NCCO,
  DPL,
  Mach5
}

export enum EProdType {
  IPLCanada = 1,
  IPLMexico,
  IPLOffshore,
  IPLBilateral,
  IPLLeased,
  IPLResale,
  E2E,
  ICBH,
  CPEStandalone,
  DIAOnnet,
  DIAOffnet,
  GenericProduct,
  DedicatedIP,
  ManagedNetworkServices,
  MPLSOnnet,
  ManagedSecurityServices,
  SLFROnnet,
  SLFROffnet,
  MPLSOffnet,
  ATM,
  GFR,
  IP,
  SLFR,
  MPLS,
  DPL,
}

export enum EOrderNoteTypeID {
  MilestoneHold = 1,
  MilestoneRelease = 2
}

export enum EOrderPlatform {
  CP,
  RS,
  SF
}

export enum EOrderProduct {
  IPLCanada = 1,
  IPLMexico = 2,
  OPLOffshore = 3,
  IPLBilateral = 4,
  IPLLeased = 5,
  IPLResale = 6,
  IPLE2E = 7,
  IPLICBH = 8,
  CPE = 9,
  DIAOnnet = 10,
  DIAOffnet = 11,
  GenericProduct = 12,
  DedicatedIP = 13,
  ManagedNetworkServices = 14,
  MPLSOnnet = 15,
  ManagedSecurityServices = 16,
  SLFROnnet = 17,
  SLFROffnet = 18,
  MPLSOffnet = 19
}

export enum EOrderTypes {
  Install = 1,
  Upgrade = 2,
  Downgrade = 3,
  Move = 4,
  Change = 5,
  BillingChange = 6,
  Disconnect = 7,
  Cancel = 8
}

export enum EVendorEmailTypes {
  VendorOrder = 1,
  CCD = 2,
  ACR = 3,
  VendOrdConfirmation = 4,
  VendOrdDiscConfirmation = 5,
  ASR = 6
}

export enum EPrntProfile {
  GOM	= 114,
  MDS	= 128,
  CSC	= 58,
  SalesSupport = 156,
  AMNCI = 2,
  ANCI = 16,
  ENCI = 100,
  SIPTnUCaaS = 170,
  DCPE = 86,
  CPETECH = 44,
  DBB = 72,
  System = 184
}

export enum ETasks {
  Start = 0,
  End = 1,
  GOMPreSubmitReady = 100,
  GOMError = 101,
  GOMReview = 102,
  GOMPL = 103,
  GOMSubmitReady = 105,
  GOMCCDReady = 106,
  GOMCCDPL = 107,
  GOMCancelReady = 108,
  GOMMissingBillingOrders = 109,
  GOMInitiateBillingOrder = 110,
  GOMNCCOErrorNotification = 111,
  GOMIPLIPASR = 112,
  xNCIOrderValidatedDate = 200,
  xNCIOrderSenttoVendorDate = 201,
  xNCIOrderAcknowledgedbyVendorDate = 202,
  xNCITargetDeliveryDate = 203,
  xNCITargetDeliveryDateReceivedDate = 204,
  xNCIAccessDeliveryDate = 205,
  xNCIAccessAcceptedDate = 206,
  xNCIBillClearInstallDate = 207,
  xNCIHold = 208,
  xNCIRelease = 209,
  xNCIReady = 210,
  xNCICCDReady = 211,
  xNCICnclReady = 212,
  xNCICloseDate = 213,
  xNCIVendorConfirmDisconnectDate = 214,
  xNCIDisconnectCompletedDate = 215,
  xNCIConfirmedRenewalDate = 216,
  xNCIACRInitiatedbySalesSupport = 217,
  xNCIACRResponseRecieved = 218,

  //xNCIMilestonesComplete = 220,
  xNCIOrderInstallDate = 221,

  xNCICPEDiscEmail = 223,

  //Domestic BroadBand  DBB
  DBBOrderValidatedDate = 650,

  DBBOrderSenttoVendorDate = 651,
  DBBOrderAcknowledgedbyVendorDate = 652,
  DBBTargetDeliveryDate = 653,
  DBBTargetDeliveryDateReceivedDate = 654,
  DBBAccessDeliveryDate = 655,
  DBBAccessAcceptedDate = 656,
  DBBBillClearInstallDate = 657,
  DBBHold = 658,
  DBBRelease = 659,
  DBBReady = 660,
  DBBCCDReady = 660,
  DBBCnclReady = 661,
  DBBCloseDate = 662,
  DBBVendorConfirmDisconnectDate = 663,
  DBBDisconnectCompletedDate = 664,
  DBBConfirmedRenewalDate = 665,
  DBBACRInitiatedbySalesSupport = 666,

  DBBACRResponseRecieved = 668,

  DBBMilestonesComplete = 669,
  DBBOrderInstallDate = 670,

  NewOrderReview = 300,
  Review = 301,
  SmartMaintenance = 400,
  SSRTSPending = 500,
  EquipReview = 600,
  MatlReq = 601,
  EquipReceipt = 602,
  CPETechAssign = 604,
  BillActivationReady = 1000,
  BillActivated = 1001
}

export enum CalendarEventType {
  ADB = 1,
  ADN = 2,
  ADG = 3,
  MDS = 4,
  MDSFT = 5,
  MPLS = 6,
  NGVN = 7,
  SLNK = 8,
  OnShift = 9,
  ADShift = 10,
  ATMShift = 11,
  MDSScheduledShift = 12,
  MDSFastTrackShift = 13,
  NGVNShift = 14,
  MPLSShift = 15,
  SprintLinkShift = 16,
  ADI = 28,
  MPLSVAS = 29,
  Fedline = 30,
  ADTMT = 32,
  UCaaSShift = 33,
  UCaaS = 34,
  SIPT = 35,
  NtwkIntl = 37,
  ES = 100,
  WF = 200
}

export enum ERedesignStatus {
  Draft = 220,
  Submitted = 221,
  Reviewing = 222,
  PendingSDE = 223,
  Approved = 225,
  Cancelled = 226,
  Completed = 227,
  Delete = 228,
  PendingNTE = 230
}

export enum REDESIGN_STATUS_EMAIL_CD {
  Submitted = 221,
  //Reviewing = 222,
  PendingSDE = 223,
  Approved = 225,
  Cancelled = 226,
  Completed = 227,
  PendingNTE = 230
}

//export enum EODIEMsgTypes {
//  ODIECustomerNameRequest = 1,
//  ODIECustomerInfoRequest = 2,
//  ODIEDesignComplete = 3,
//  ODIEDiscoResponse = 4,
//  ODIECPEInfo = 5,
//  ODIECPEStus = 6,
//  ODIEShortNameValidationRequest = 7
//}

//export enum EODIECustInfoReqCatType {
//  DataConverged = 1,
//  VoiceConverged = 2,
//  DataVoiceConverged = 3
//}

export enum ESimpleOrComplex {
  'S' = 'S',
  'C' = 'C'
}

export enum EDispatchReady {
  'N' = 0,
  'Y' = 1
}

//export enum ERedesignNoteTypes {
//  Status = 1,
//  Notes = 2,
//  Caveats = 3,
//  System = 4
//}

export enum ERedesignCategory {
  Voice = 1,
  Data = 2,
  Security = 3
}

export enum ECPTHostedManagedSrvc {
  WpaaS = 1,
  Scc = 2
}

export enum ECPTCustomerType {
  ManagedData = 1,
  ManagedSecurity = 2,
  ManagedVoice = 3,
  UnmanagedE2E = 4,
  Sps = 5
}

export enum ECPTPlanServiceTier {
  MssSolution = 1,
  MdsComplete = 2,
  MdsSupport = 3,
  WholesaleCarrier = 4,
  WholesaleVar = 5
}

export enum ECPTMdsSupportTier {
  Design = 1,
  Implementation = 2,
  MonitorNotify = 3
}

export enum ECPTPrimTransport {
  IpSprintLink = 1,
  Gmpls = 2,
  Pip = 3,
  SprintLinkFr = 4,
  IpSecVpn = 5,
  GmplsOver3rdPartyDsl = 6,
  WirelessWan = 7,
  CarrierEthernet = 8
}

export enum ECPTStatus {
  Submitted = 305,
  Provisioned = 306,
  Assigned = 307,
  Deleted = 308,
  Completed = 309,
  ReturnedToSDE = 310,
  Cancelled = 311,
  ReviewedByPM = 312
}

export enum CPT_STATUS_EMAIL_CD {
  Submitted = 305,
  Assigned = 307,
  Completed = 309,
  ReturnedToSDE = 310,
  Cancelled = 311,
}

export enum ECPTAssignmentProfileId {
  Gatekeeper = 212,
  Manager = 211,
  MSS = 139,
  NTE = 130,
  PM = 132,
  CRM = 213,
}

export class KeyValuePair {
  description: string
  value: any
  disabled: boolean

  constructor(description: string, value: any) {
    this.description = description;
    this.value = value;
    this.disabled = false;
  }
}

export enum WorkGroup {
  GOM = 114,//1
  MDS = 128,//2
  CSC = 58,//3
  RTS = 156,//4
  xNCIAmerica = 2,//5
  xNCIAsia = 16,//6
  xNCIEurope = 100,//7
  UCaaS = 170,
  DCPE = 86,//13
  //VCPE = 14,//14
  CPETech = 44,//15
  //DBB = 72//16
  System = 98
}

export enum OldWorkGroup {
  GOM = 1,
  MDS = 2,
  CSC = 3,
  SalesSupport = 4,
  AMNCI = 5,
  ANCI = 6,
  ENCI = 7,
  WBO = 8,
  CAND = 10,
  SIPTnUCaaS = 11,
  DCPE = 13,
  System = 98
}

export enum OldWorkGroupStr {
  GOM = "GOM",
  MDS = "MDS",
  CSC = "CSC",
  SalesSupport = "Sales Support",
  AMNCI = "AMNCI",
  ANCI = "ANCI",
  ENCI = "ENCI",
  WBO = "WBO",
  CAND = "CAND",
  SIPTnUCaaS = "SIPTnUCaaS",
  DCPE = "DCPE",
  System = "System",
}


export enum NCCOProfiles {
  AMNCIAdmin = 2,
  ANCIAdmin = 16,
  CPETECHAdmin = 44,
  CSCAdmin = 58,
  DBBAdmin = 72,
  DCPEAdmin = 86,
  ENCIAdmin = 100,
  GOMAdmin = 114,
  MDSAdmin = 128,
  SALESSUPPORTAdmin = 156,
  SIPTnUCaaSAdmin = 170,
}

export enum OrderStatus {
  New = 0,
  Pending = 1,
  Completed = 2,
  Rejected = 3,
  Cancelled = 4,
  Disconnected = 5,
  'Presubmit Delete' = 6
}

export enum ESDEStatus {
  Unassigned = 501,
  Draft = 502,
  Assigned = 503,
  Win = 504,
  Lost = 505,
  Ongoing = 506,
  Close = 507,
}

export enum ERecStatus {
  InActive = 0,
  Active = 1
}

export enum ELkSysCfgMenus {
  CurrencyFileUpload = "menuCurrencyUpdt",
  ManageCCDBypass = "menuCCD",
  CompleteOrderEvent = "menuM5CmpltMsg"
}

export enum EmailStus {
  SentEnc = 16,
  SentUnenc = 14
}

export enum IpType {
  CarrierEthernet = 1,
  SipNat = 2,
}

export enum ELkSysCfgAccessKeys {
  xNCIAdminEdit = "xNCIAdminEdit",
}

export enum ContactDetailObjType {
  Event = "E",
  Redesign = "R",
  CPT = "C"
}

export enum HierLevel {
  H1 = "H1",
  H2 = "H2",
  H4 = "H4",
  H6 = "H6",
  OD = "OD"
}
