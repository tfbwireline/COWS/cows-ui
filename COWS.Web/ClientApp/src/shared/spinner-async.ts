import { NgxSpinnerService } from 'ngx-spinner';

export class SpinnerAsync {
    private spinner: NgxSpinnerService
    private spinners: number

    constructor (private sp: NgxSpinnerService) {
      this.spinners = 0
      this.spinner = sp
    }

    /**
    * @param str Acts as a spinner tracker. If used on async calls, use manageSpinner("show") before the call is made, and manageSpinner("hide") on callbacks [success|error|finally|add]
    */
   public manageSpinner(str: string) {
    if (str == "show") {
      if (this.spinners == 0) {
        setTimeout(() => { this.spinner.show();}, 20);
      }
      this.spinners++;
    } else if (str == "hide") {
      this.spinners--;
      if (this.spinners <= 0){
        this.spinners = 0;
        setTimeout(() => { this.spinner.hide();}, 20);
      } 
    }
  }
}