import { Mask } from './mask.interface';

export class MaskUtil {

    private static PHONE_SMALL = '(999) 999-9999';
    private static PHONE_BIG = '(999) 9999-9999';
    private static CPF = '999.999.999-99';
    private static CNPJ = '99.999.999/9999-99';
    private static BAN = '999999999';
    private static PTN = '999-999-9999';
    private static PTN_AC = '9-999-999-9999';

    public static BAN_PTN_MASK_GENERATOR: Mask = {
      generateMask: (value: string) => {
        return MaskUtil.hasMoreDigits(value, MaskUtil.BAN) ? MaskUtil.PTN : MaskUtil.BAN;
      }
    }

    public static PTN_MASK_GENERATOR: Mask = {
      generateMask: (value: string) => {
        //console.log(MaskUtil.hasMoreDigits(value, MaskUtil.PTN))
        return MaskUtil.hasMoreDigits(value, MaskUtil.PTN) ? MaskUtil.PTN_AC : MaskUtil.PTN;

        //return MaskUtil.PTN_AC;
      }
    }

    public static PHONE_MASK_GENERATOR: Mask = {
        generateMask: () =>  MaskUtil.PHONE_SMALL,
    }

    public static DYNAMIC_PHONE_MASK_GENERATOR: Mask = {
        generateMask: (value: string) => {
            return MaskUtil.hasMoreDigits(value, MaskUtil.PHONE_SMALL) ? 
                MaskUtil.PHONE_BIG : 
                MaskUtil.PHONE_SMALL;
        },
    }

    public static CPF_MASK_GENERATOR: Mask = {
        generateMask: () => MaskUtil.CPF,
    }

    public static CNPJ_MASK_GENERATOR: Mask = {
        generateMask: () => MaskUtil.CNPJ,
    }

    public static PERSON_MASK_GENERATOR: Mask = {
        generateMask: (value: string) => {
            return MaskUtil.hasMoreDigits(value, MaskUtil.CPF) ? 
                MaskUtil.CNPJ : 
                MaskUtil.CPF;
        },
    }

    private static hasMoreDigits(v01: string, v02: string): boolean {
        let d01 = this.onlyDigits(v01);
        let d02 = this.onlyDigits(v02);
        let len01 = (d01 && d01.length) || 0;
        let len02 = (d02 && d02.length) || 0;
        let moreDigits = (len01 > len02);
        return moreDigits;      
    }

    private static onlyDigits(value: string): string {
        let onlyDigits = (value != null) ? value.replace(/\D/g, '') : null;
        return onlyDigits;      
    }
}
