export interface Mask {
    generateMask: (value: string) => string;
}
