import { Injectable } from '@angular/core'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router'
import { UserProfileService } from '../services/user-profile.service';

@Injectable()
export class EventAuthGuard implements CanActivate, CanActivateChild {
  private validMenu: boolean;

  constructor(private router: Router, private userProfileService: UserProfileService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let userID = Number(localStorage.getItem('userID'));
    this.userProfileService.isEventUserProfile(userID, route.data.eventType).subscribe(res => {
      if (res) {
        return true
      }
      else {
        this.router.navigate(['/sorry'], { queryParams: { returnUrl: state.url } })
        return false
      }
    });

    return true;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(childRoute, state);
  }
}
