import { Injectable, Inject } from '@angular/core'
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError, retry } from 'rxjs/operators'
import notify from 'devextreme/ui/notify';
import { Global } from './global';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class ApiHelperService {
  
  baseUrl: string = '';
  blindRequest: boolean = false;
  timeout: number = 1000 * 60 * 5;

  constructor(private httpClient: HttpClient,
              @Inject('BASE_URL') baseUrl: string,
              @Inject(DOCUMENT) private document: any) { 
                this.baseUrl = baseUrl
  }
  
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error(error.message, JSON.stringify(error))
      // console.log(error.message)

      if (!this.blindRequest) {
        notify(error.message, 'error', Global.NOTIFY_DURATION)
      }
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`, JSON.stringify(error))
      
      //console.log('message ' + error.message)

      if (!this.blindRequest) {
        console.log(1)
        if (error.status === 500) {
          console.log(11)
          notify(error.error.message ? error.error.message :
            'Your request encountered an internal server error. Please contact your system administrator.',
            'error', Global.NOTIFY_DURATION)
        }
        else if (error.status === 400) {
          console.log(111)
          notify(error.error.message ? error.error.message :
            'Bad Request.', 'error', Global.NOTIFY_DURATION)
        }
        else if (error.status === 401) {
          console.log(1111)
          notify('You are not authorized to perform this action', 'error', Global.NOTIFY_DURATION)
        }
        else if (error.status === 404) {
          console.log(11111)
          notify(error.error.message ? error.error.message :
            'No Results found.', 'error', Global.NOTIFY_DURATION)
        }
        else if (error.status === 406) {
          console.log(111111)
          //notify(error.error.message ? error.error.message : 'No Results found.', 'error', Global.NOTIFY_DURATION)
          // not acceptable is received when the SM Session is timed out but token is valid.
          this.document.location.href = this.baseUrl
        }
        else {
          console.log(1111111)
          notify(error.message, 'error', Global.NOTIFY_DURATION)
        }
      }
        
      return throwError(error.error);
    }

    this.blindRequest = false;
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.')
  }

  get(url: string): Observable<any>
  get(url: string, params?: any): Observable<any>
  get(url: string, params?: any): Observable<any> {
    let options = params || { params: null }
    options.headers = new HttpHeaders({ timeout: `${this.timeout}` })
    return this.httpClient.get(this.baseUrl + url, options)
        .pipe(
          //retry(2),
          catchError(this.handleError)
        );          
  }

  post(url: string, body: Object): Observable<any>
  post(url: string, body: Object, params?: any): Observable<any>
  post(url: string, body: Object, params?: any): Observable<any> {
    let options = params || { params: null }
    options.headers = new HttpHeaders({ timeout: `${this.timeout}` })
    return this.httpClient.post(this.baseUrl + url, body, options)
        .pipe(
          catchError(this.handleError)
        )
  }

  put(url: string, body: Object): Observable<any> {
    return this.httpClient.put(this.baseUrl + url, body, { headers: new HttpHeaders({ timeout: `${this.timeout}` }) })
        .pipe(
          catchError(this.handleError)
        )
  }

  delete(url: string) : Observable<any> {
    return this.httpClient.delete(this.baseUrl + url, { headers: new HttpHeaders({ timeout: `${this.timeout}` }) })
        .pipe(
          catchError(this.handleError)
        )
  }
}
