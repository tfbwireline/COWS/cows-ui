import { NgControl } from '@angular/forms';
import { Directive, Input } from "@angular/core";

@Directive({
  selector: '[isEnabled]'
})
export class DisableControlDirective {
  @Input() set isEnabled(condition: boolean) {
    const action = condition ? 'enable' : 'disable';
    this.ngControl.control[action]();
  }

  constructor(private ngControl: NgControl) {
  }

}
