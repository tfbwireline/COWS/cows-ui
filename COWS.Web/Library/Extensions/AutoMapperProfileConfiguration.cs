﻿using AutoMapper;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.ViewModels;
using System.Linq;
using COWS.Data.Extensions;
using System;
using COWS.Data.Interface;
using COWS.Entities.Enums;

namespace COWS.Web.Library.Extensions
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration() : this("COWSTrackerConfig")
        {
        }

        private EntityBaseViewModel SetEntityBase(LkUser createdUser, LkUser modifiedUser,
            byte? recStatusId, DateTime createdDate, DateTime? modifiedDate)
        {
            EntityBaseViewModel entity = new EntityBaseViewModel();
            entity.RecStatusId = recStatusId;
            entity.CreatedByUserId = createdUser.UserId;
            entity.CreatedByUserName = createdUser.UserAdid;
            entity.CreatedByFullName = createdUser.FullNme;
            entity.CreatedByDisplayName = createdUser.DsplNme;
            entity.CreatedDateTime = createdDate;
            if (modifiedUser != null)
            {
                entity.ModifiedByUserId = modifiedUser.UserId;
                entity.ModifiedByUserName = modifiedUser.UserAdid;
                entity.ModifiedByFullName = modifiedUser.FullNme;
                entity.ModifiedByDisplayName = modifiedUser.DsplNme;
                entity.ModifiedDateTime = modifiedDate;
            }
            
            return entity;
        }

        public AutoMapperProfileConfiguration(string profileName) : base(profileName)
        {
            CreateMap<LkUser, UserViewModel>().ReverseMap();
            CreateMap<LkUser, LogInUserViewModel>().ReverseMap();
            CreateMap<LkUsrPrf, UserProfileViewModel>().ReverseMap();
            CreateMap<LkDedctdCust, DedicatedCustomerViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkSrvcAssrnSiteSupp, ServiceAssuranceSiteSupportViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkTelco, TelcoViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkMdsSrvcTier, ServiceTierViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkMds3rdpartySrvcLvl, MDS3rdPartyServiceViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore());
            CreateMap<LkMds3rdpartyVndr, MDS3rdPartyVendorViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkMdsSrvcType, MDSServiceTypeViewModel>().ReverseMap();
            CreateMap<LkEnhncSrvc, EnhncSrvcViewModel>().ReverseMap();
            CreateMap<LkEventStus, EventStatusViewModel>().ReverseMap();
            CreateMap<LkXnciMs, XnciMsViewModel>().ReverseMap();
            CreateMap<LkXnciMsSla, SetSlasViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore());
            CreateMap<LkEventType, EventTypeViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkMdsActyType, MDSActivityTypeViewModel>().ReverseMap();
            CreateMap<LkMdsMacActy, MDSMACActivityViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkSprintCpeNcr, ServiceDeliveryViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkSpclProj, SpecialProjectViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkSprintHldy, SprintHolidayViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkMenu, MenuViewModel>().ReverseMap();
            CreateMap<MdsFastTrkType, MDSFastTrackTypeViewModel>().ReverseMap();
            CreateMap<LkProdType, ProductTypeViewModel>().ReverseMap();
            CreateMap<LkOrdrType, OrderTypeViewModel>().ReverseMap();
            CreateMap<LkOrdrSubType, OrderSubTypeViewModel>().ReverseMap();
            CreateMap<LkGrp, GroupViewModel>().ReverseMap();
            CreateMap<LkPltfrm, PlatformViewModel>().ReverseMap();
            CreateMap<LkXnciRgn, RegionViewModel>().ReverseMap();
            CreateMap<LkRedsgnCat, RedesignCategoryViewModel>().ReverseMap();
            CreateMap<LkRedsgnType, RedesignTypeViewModel>().ReverseMap();
            CreateMap<LkStus, StatusViewModel>().ReverseMap();
            CreateMap<LkFedlineOrdrType, FedlineOrderTypeViewModel>().ReverseMap();
            CreateMap<GetSCMInterfaceView, GetSCMInterfaceViewModel>()
                .ForMember(d => d.DeviceId, opt => opt.MapFrom(s => s.DEVICE_ID))
                .ForMember(d => d.PlsftRqstnNbr, opt => opt.MapFrom(s => s.PLSFT_RQSTN_NBR))
                .ForMember(d => d.RqstnDt, opt => opt.MapFrom(s => s.RQSTN_DT))
                .ForMember(d => d.PrchOrdrNbr, opt => opt.MapFrom(s => s.PRCH_ORDR_NBR))
                .ReverseMap();
            CreateMap<GetNRMBPMInterfaceView, GetNRMBPMInterfaceViewModel>()
                .ForMember(d => d.CpeDeviceId, opt => opt.MapFrom(s => s.CPE_DEVICE_ID))
                .ForMember(d => d.M5OrdrNbr, opt => opt.MapFrom(s => s.M5_ORDR_NBR))
                .ForMember(d => d.M5RltdOrdrNbr, opt => opt.MapFrom(s => s.M5_RLTD_ORDR_NBR))
                .ForMember(d => d.SubTypeNme, opt => opt.MapFrom(s => s.SUB_TYPE_NME))
                .ForMember(d => d.TypeNme, opt => opt.MapFrom(s => s.TYPE_NME))
                .ForMember(d => d.ProdCd, opt => opt.MapFrom(s => s.PROD_CD))
                .ForMember(d => d.XtrctCd, opt => opt.MapFrom(s => s.XTRCT_CD))
                .ForMember(d => d.XtrctDt, opt => opt.MapFrom(s => s.XTRCT_DT))
                .ReverseMap();
            CreateMap<MapUsrPrf, MapUserProfileViewModel>().ReverseMap();
            CreateMap<MapUserProfileViewModel, UserProfileViewModel>()
                .ForMember(d => d.UserId, opt => opt.MapFrom(s => s.UserId))
                .ForMember(d => d.UsrPrfId, opt => opt.MapFrom(s => s.UsrPrfId))
                .ForMember(d => d.RecStusId, opt => opt.MapFrom(s => s.RecStusId))
                .ForMember(d => d.ModfdDt, opt => opt.MapFrom(s => s.ModfdDt))
                .ForMember(d => d.CreatDt, opt => opt.MapFrom(s => s.CreatDt))
                .ReverseMap();
            CreateMap<GetOrdrUnlockView, GetOrdrUnlockOrdrViewModel>()
                .ForMember(d => d.OrdrId, opt => opt.MapFrom(s => s.ORDR_ID))
                .ForMember(d => d.LockByUserId, opt => opt.MapFrom(s => s.LOCK_BY_USER_ID))
                .ForMember(d => d.LockedBy, opt => opt.MapFrom(s => s.FULL_NME))
                .ForMember(d => d.FTN, opt => opt.MapFrom(s => s.FTN))
                .ForMember(d => d.ProductType, opt => opt.MapFrom(s => s.PROD_TYPE_DES))
                .ForMember(d => d.OrdrType, opt => opt.MapFrom(s => s.ORDR_TYPE_DES))
                .ReverseMap();
            CreateMap<GetOrdrUnlockView, GetOrdrUnlockEventViewModel>()
                .ForMember(d => d.EventId, opt => opt.MapFrom(s => s.ORDR_ID))
                .ForMember(d => d.LockByUserId, opt => opt.MapFrom(s => s.LOCK_BY_USER_ID))
                .ForMember(d => d.LockedBy, opt => opt.MapFrom(s => s.FULL_NME))
                .ForMember(d => d.EventType, opt => opt.MapFrom(s => s.PROD_TYPE_DES))
                .ReverseMap();
            CreateMap<GetOrdrUnlockView, GetOrdrUnlockRedsgnViewModel>()
                .ForMember(d => d.RedsgnId, opt => opt.MapFrom(s => s.ORDR_ID))
                .ForMember(d => d.RedsgnNumber, opt => opt.MapFrom(s => s.FTN))
                .ForMember(d => d.LockByUserId, opt => opt.MapFrom(s => s.LOCK_BY_USER_ID))
                .ForMember(d => d.LockedBy, opt => opt.MapFrom(s => s.FULL_NME))
                .ForMember(d => d.RedsgnType, opt => opt.MapFrom(s => s.PROD_TYPE_DES))
                .ReverseMap();
            CreateMap<GetOrdrUnlockView, GetOrdrUnlockCptViewModel>()
                .ForMember(d => d.cptId, opt => opt.MapFrom(s => s.ORDR_ID))
                .ForMember(d => d.LockByUserId, opt => opt.MapFrom(s => s.LOCK_BY_USER_ID))
                .ForMember(d => d.LockedBy, opt => opt.MapFrom(s => s.FULL_NME))
                .ForMember(d => d.cptCustNumber, opt => opt.MapFrom(s => s.FTN))
                .ForMember(d => d.CustName, opt => opt.MapFrom(s => s.PROD_TYPE_DES))
                .ReverseMap();
            CreateMap<LkCur, CurrencyListViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkWrldHldy, WorldHolidayViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkCtry, CountryViewModel>()
                .ForMember(d => d.RgnDes, opt => opt.MapFrom(s => s.Rgn == null ? "" : s.Rgn.RgnDes))
                .ReverseMap()
                .ForMember(d => d.Rgn, opt => opt.Ignore());
            CreateMap<LkCtryCty, CityViewModel>().ReverseMap();
            CreateMap<GetOrdrCancelView, GetCancelOrdrViewModel>()
                .ForMember(d => d.h5_h6, opt => opt.MapFrom(s => s.H5_H6))
                .ForMember(d => d.custName, opt => opt.MapFrom(s => s.CUST_NME))
                .ForMember(d => d.ccd, opt => opt.MapFrom(s => s.CCD))
                .ForMember(d => d.FTN, opt => opt.MapFrom(s => s.FTN))
                .ForMember(d => d.ProductType, opt => opt.MapFrom(s => s.PROD_TYPE))
                .ForMember(d => d.OrdrType, opt => opt.MapFrom(s => s.ORDR_TYPE))
                .ForMember(d => d.notes, opt => opt.MapFrom(s => s.NOTES))
                .ReverseMap();
			CreateMap<LkCnfrcBrdg, SetPreferenceViewModel>()
                .ForMember(d => d.CreatedByAdid, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdid, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<TimeSlotOccurrence, TimeSlotOccurrenceViewModel>().ReverseMap();
            CreateMap<LkAccsCtySite, AccessCitySiteViewModel>().ReverseMap();
            CreateMap<InsertM5CompleteMsg, CompleteOrderEventViewModel>().ReverseMap();
            CreateMap<CCDBypassUserView, CCDBypassUserViewModel>().ReverseMap();
            CreateMap<LkVndr, VendorViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ForMember(d => d.CtryCd, opt => opt.MapFrom(s => s.UserWfm.ElementAtOrDefault(0).OrgtngCtryCdNavigation.CtryCd))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<FTAvailEvent, FTAvailEventViewModel>().ReverseMap();
            CreateMap<FedlineManageUserEvent, FedlineManageUserEventViewModel>().ReverseMap();
            CreateMap<LkMdsImplmtnNtvl, MDSImplementationIntervalViewModel>()
                .ForMember(d => d.EventTypeName, opt => opt.MapFrom(s => s.EventType.EventTypeNme))
                .ForMember(d => d.CreatedByAdid, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdid, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<LkDevManf, DeviceManufacturerViewModel>().ReverseMap();
            CreateMap<LkDevModel, DeviceModelViewModel>()
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ForMember(d => d.ManfModelName, opt => opt.MapFrom(s => s.Manf.ManfNme + " - " + s.DevModelNme))
                .ForMember(d => d.IsAlreadyInUse, opt => opt.MapFrom(s => s.QlfctnVndrModel.Any() || s.MdsEventOdieDev.Any() || s.EventDiscoDev.Any()))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<SrcCurFile, CurrencyFileViewModel>()
                .ForMember(d => d.CreatedByAdid, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.CreatedByName, opt => opt.MapFrom(s => s.CreatByUser.DsplNme))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore());
            CreateMap<LkOrdrActn, OrderActionViewModel>().ReverseMap();
            CreateMap<UserWfm, UserWfmViewModel>().ReverseMap();
            CreateMap<WFMUserAssignment, WFMUserAssignmentViewModel>().ReverseMap();
            CreateMap<CustomerUserProfileView, CustomerUserProfileViewModel>().ReverseMap();
            //.ForMember(d => d.custID, opt => opt.MapFrom(s => s.custID))
            //.ForMember(d => d.custNme, opt => opt.MapFrom(s => s.custNme))
            //.ForMember(d => d.crtyNme, opt => opt.MapFrom(s => s.crtyNme))
            //.ForMember(d => d.usrNme, opt => opt.MapFrom(s => s.usrNme))
            //.ForMember(d => d.userId, opt => opt.MapFrom(s => s.userId))
            //.ForMember(d => d.mns, opt => opt.MapFrom(s => s.mns))
            //.ForMember(d => d.isip, opt => opt.MapFrom(s => s.isip))
            //.ForMember(d => d.status, opt => opt.MapFrom(s => s.status))
            //.ForMember(d => d.assignDT, opt => opt.MapFrom(s => s.assignDT))
            //.ReverseMap();
            CreateMap<InsertWFMUserAssignments, InsertWFMUserAssignmentsViewModel>().ReverseMap();
            CreateMap<LkIpVer, IPVersionViewModel>().ReverseMap();
            CreateMap<LkMdsNtwkActyType, MDSNetworkActivityTypeViewModel>().ReverseMap();
            CreateMap<GetQualificationView, GetQualificationViewModel>().ReverseMap();
            CreateMap<LkQlfctn, GetQualificationViewModel>()
                .ForMember(d => d.QualificationID, opt => opt.MapFrom(s => s.QlfctnId))
                .ForMember(d => d.AssignToUser, opt => opt.MapFrom(s => s.AsnToUser.FullNme))
                .ForMember(d => d.AssignToUserID, opt => opt.MapFrom(s => s.AsnToUser.UserId))
                .ForMember(d => d.AssignToUserAdId, opt => opt.MapFrom(s => s.AsnToUser.UserAdid))
                .ForMember(d => d.CreatedByUserID, opt => opt.MapFrom(s => s.CreatByUser.UserId))
                .ForMember(d => d.CreatedByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModifiedByUserID, opt => opt.MapFrom(s => s.ModfdByUser.UserId))
                .ForMember(d => d.ModifiedByUserAdId, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ForMember(d => d.CustomerIDs, opt => opt.MapFrom(s => (s.QlfctnDedctdCust != null && s.QlfctnDedctdCust.Count() > 0)
                                ? string.Join(",", s.QlfctnDedctdCust.Select(i => i.CustId)) : string.Empty))
                .ForMember(d => d.CustomerNames, opt => opt.MapFrom(s => (s.QlfctnDedctdCust != null && s.QlfctnDedctdCust.Count() > 0)
                                ? string.Join(",", s.QlfctnDedctdCust.Select(i => i.Cust.CustNme)) : string.Empty))
                .ForMember(d => d.DeviceIDs, opt => opt.MapFrom(s => (s.QlfctnDev != null && s.QlfctnDev.Count() > 0)
                                ? string.Join(",", s.QlfctnDev.Select(i => i.DevId)) : string.Empty))
                .ForMember(d => d.DeviceNames, opt => opt.MapFrom(s => (s.QlfctnDev != null && s.QlfctnDev.Count() > 0)
                                ? string.Join(",", s.QlfctnDev.Select(i => i.Dev.DevNme)) : string.Empty))
                .ForMember(d => d.EnhanceServiceIDs, opt => opt.MapFrom(s => (s.QlfctnEnhncSrvc != null && s.QlfctnEnhncSrvc.Count() > 0)
                                ? string.Join(",", s.QlfctnEnhncSrvc.Select(i => i.EnhncSrvcId)) : string.Empty))
                .ForMember(d => d.EnhanceServiceNames, opt => opt.MapFrom(s => (s.QlfctnEnhncSrvc != null && s.QlfctnEnhncSrvc.Count() > 0)
                                ? string.Join(",", s.QlfctnEnhncSrvc.Select(i => i.EnhncSrvc.EnhncSrvcNme)) : string.Empty))
                .ForMember(d => d.EventTypeIDs, opt => opt.MapFrom(s => (s.QlfctnEventType != null && s.QlfctnEventType.Count() > 0)
                                ? string.Join(",", s.QlfctnEventType.Select(i => i.EventTypeId)) : string.Empty))
                .ForMember(d => d.EventTypeNames, opt => opt.MapFrom(s => (s.QlfctnEventType != null && s.QlfctnEventType.Count() > 0)
                                ? string.Join(",", s.QlfctnEventType.Select(i => i.EventType.EventTypeNme)) : string.Empty))
                .ForMember(d => d.IPVersionIDs, opt => opt.MapFrom(s => (s.QlfctnIpVer != null && s.QlfctnIpVer.Count() > 0)
                                ? string.Join(",", s.QlfctnIpVer.Select(i => i.IpVerId)) : string.Empty))
                .ForMember(d => d.IPVersionNames, opt => opt.MapFrom(s => (s.QlfctnIpVer != null && s.QlfctnIpVer.Count() > 0)
                                ? string.Join(",", s.QlfctnIpVer.Select(i => i.IpVer.IpVerNme)) : string.Empty))
                .ForMember(d => d.NetworkActivityIDs, opt => opt.MapFrom(s => (s.QlfctnNtwkActyType != null && s.QlfctnNtwkActyType.Count() > 0)
                                ? string.Join(",", s.QlfctnNtwkActyType.Select(i => i.NtwkActyTypeId)) : string.Empty))
                .ForMember(d => d.NetworkActivityNames, opt => opt.MapFrom(s => (s.QlfctnNtwkActyType != null && s.QlfctnNtwkActyType.Count() > 0)
                                ? string.Join(",", s.QlfctnNtwkActyType.Select(i => i.NtwkActyType.NtwkActyTypeDes)) : string.Empty))
                .ForMember(d => d.MplsActyTypeIDs, opt => opt.MapFrom(s => (s.QlfctnMplsActyType != null && s.QlfctnMplsActyType.Count() > 0)
                                ? string.Join(",", s.QlfctnMplsActyType.Select(i => i.MplsActyTypeId)) : string.Empty))
                .ForMember(d => d.MplsActyTypeNames, opt => opt.MapFrom(s => (s.QlfctnMplsActyType != null && s.QlfctnMplsActyType.Count() > 0)
                                ? string.Join(",", s.QlfctnMplsActyType.Select(i => i.MplsActyType.MplsActyTypeDes)) : string.Empty))
                .ForMember(d => d.SpecialProjectIDs, opt => opt.MapFrom(s => (s.QlfctnSpclProj != null && s.QlfctnSpclProj.Count() > 0)
                                ? string.Join(",", s.QlfctnSpclProj.Select(i => i.SpclProjId)) : string.Empty))
                .ForMember(d => d.SpecialProjectNames, opt => opt.MapFrom(s => (s.QlfctnSpclProj != null && s.QlfctnSpclProj.Count() > 0)
                                ? string.Join(",", s.QlfctnSpclProj.Select(i => i.SpclProj.SpclProjNme)) : string.Empty))
                .ForMember(d => d.VendorModelIDs, opt => opt.MapFrom(s => (s.QlfctnVndrModel != null && s.QlfctnVndrModel.Count() > 0)
                                ? string.Join(",", s.QlfctnVndrModel.Select(i => i.DevModelId)) : string.Empty))
                .ForMember(d => d.VendorModelNames, opt => opt.MapFrom(s => (s.QlfctnVndrModel != null && s.QlfctnVndrModel.Count() > 0)
                                ? string.Join(",", s.QlfctnVndrModel.Select(i => i.DevModel.Manf.ManfNme + ' ' + i.DevModel.DevModelNme)) : string.Empty))
                .ReverseMap()
                .ForMember(d => d.AsnToUser, opt => opt.Ignore())
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            //CreateMap<LkQlfctn, QualificationViewModel>()
            //    .ForMember(d => d.AsnToUser, opt => opt.MapFrom(s => s.AsnToUser.DsplNme))
            //    .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => s.CreatByUser.DsplNme))
            //    .ForMember(d => d.ModifiedBy, opt => opt.MapFrom(s => s.ModfdByUser.DsplNme))
            //    .ReverseMap()
            //    .ForMember(d => d.AsnToUser, opt => opt.Ignore())
            //    .ForMember(d => d.CreatByUser, opt => opt.Ignore())
            //    .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<AdEvent, AdEventViewModel>()
                .ForMember(d => d.EventCsgLvlId, opt => opt.MapFrom(s => s.Event.CsgLvlId))
                .ForMember(d => d.CreatByUserAdid, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.CreatByUserDsplNme, opt => opt.MapFrom(s => s.CreatByUser.DsplNme))
                .ForMember(d => d.EventStusDes, opt => opt.MapFrom(s => s.EventStus.EventStusDes))
                .ForMember(d => d.EnhncSrvcNme, opt => opt.MapFrom(s => s.EnhncSrvc.EnhncSrvcNme))
                .ForMember(d => d.WrkflwStusDes, opt => opt.MapFrom(s => s.WrkflwStus.WrkflwStusDes))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.EventStus, opt => opt.Ignore())
                .ForMember(d => d.EnhncSrvc, opt => opt.Ignore())
                .ForMember(d => d.WrkflwStus, opt => opt.Ignore());
            CreateMap<NgvnEvent, NgvnEventViewModel>()
                .ForMember(d => d.EntityBase, opt => opt.MapFrom(s => SetEntityBase(s.CreatByUser, s.ModfdByUser, s.RecStusId, s.CreatDt, s.ModfdDt)))
                .ForMember(d => d.EventCsgLvlId, opt => opt.MapFrom(s => s.Event.CsgLvlId))
                .ForMember(d => d.EventStusDes, opt => opt.MapFrom(s => s.EventStus.EventStusDes))
                .ForMember(d => d.ReqorUser, opt => opt.MapFrom(s => s.ReqorUser))
                .ForMember(d => d.SalsUser, opt => opt.MapFrom(s => s.SalsUser))
                .ForMember(d => d.WrkflwStusDes, opt => opt.MapFrom(s => s.WrkflwStus.WrkflwStusDes))
                .ReverseMap()
                .ForMember(d => d.CreatByUserId, opt => opt.MapFrom(s => s.EntityBase.CreatedByUserId))
                .ForMember(d => d.CreatDt, opt => opt.MapFrom(s => s.EntityBase.CreatedDateTime))
                .ForMember(d => d.ModfdByUserId, opt => opt.MapFrom(s => s.EntityBase.ModifiedByUserId))
                .ForMember(d => d.ModfdDt, opt => opt.MapFrom(s => s.EntityBase.ModifiedDateTime))
                .ForMember(d => d.RecStusId, opt => opt.MapFrom(s => s.EntityBase.RecStatusId))
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.EventStus, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.WrkflwStus, opt => opt.Ignore());
            CreateMap<AdEventAccsTag, AdEventAccessTagViewModel>().ReverseMap();
            CreateMap<LkEsclReas, EscalationReasonViewModel>().ReverseMap();
            CreateMap<LkFiltrOpr, FilterOperatorViewModel>().ReverseMap();
            CreateMap<LkActyLocale, ActivityLocaleViewModel>().ReverseMap();
            CreateMap<LkMplsAccsBdwd, MplsAccessBandwidthViewModel>().ReverseMap();
            CreateMap<LkMplsActyType, MplsActivityTypeViewModel>().ReverseMap();
            CreateMap<LkMplsEventType, MplsEventTypeViewModel>().ReverseMap();
            CreateMap<LkMplsMgrtnType, MplsMigrationTypeViewModel>().ReverseMap();
            CreateMap<LkMultiVrfReq, MultiVrfReqViewModel>().ReverseMap();
            CreateMap<LkVasType, VasTypeViewModel>().ReverseMap();
            CreateMap<LkVpnPltfrmType, VpnPlatformTypeViewModel>().ReverseMap();
            CreateMap<LkWhlslPtnr, WholesalePartnerViewModel>().ReverseMap();
            CreateMap<LkMnsOffrgRoutr, MnsOfferingRouterViewModel>().ReverseMap();
            CreateMap<LkMnsPrfmcRpt, MnsPerformanceReportingViewModel>().ReverseMap();
            CreateMap<LkMnsRoutgType, MnsRoutingTypeViewModel>().ReverseMap();
            CreateMap<DynamicParameters, DynamicParametersViewModel>().ReverseMap();
            CreateMap<LkWrkflwStus, WorkflowViewModel>().ReverseMap();
            CreateMap<RetrieveCustomerByH1, RetrieveCustomerByH1ViewModel>().ReverseMap();
            CreateMap<UserCsgLvl, UserCsgLevelViewModel>().ReverseMap();
            CreateMap<EventRecLock, EventLockViewModel>()
                .ForMember(d => d.LockByFullName, opt => opt.MapFrom(s => s.LockByUser.FullNme))
                .ReverseMap()
                .ForMember(d => d.Event, opt => opt.Ignore())
                .ForMember(d => d.LockByUser, opt => opt.Ignore());
            CreateMap<MplsEvent, MplsEventViewModel>()
                .ForMember(d => d.EntityBase, opt => opt.MapFrom(s => SetEntityBase(s.CreatByUser, s.ModfdByUser, s.RecStusId, s.CreatDt, s.ModfdDt)))
                .ForMember(d => d.EventCsgLvlId, opt => opt.MapFrom(s => s.Event.CsgLvlId))
                .ForMember(d => d.EventStusDes, opt => opt.MapFrom(s => s.EventStus.EventStusDes))
                .ForMember(d => d.ReqorUser, opt => opt.MapFrom(s => s.ReqorUser))
                .ForMember(d => d.SalsUser, opt => opt.MapFrom(s => s.SalsUser))
                .ForMember(d => d.WrkflwStusDes, opt => opt.MapFrom(s => s.WrkflwStus.WrkflwStusDes))
                .ForMember(d => d.MplsEventActyTypeIds, opt => opt.MapFrom(s => (s.Event.MplsEventActyType == null)
                                   ? null : s.Event.MplsEventActyType.Select(i => i.MplsActyTypeId)))
                .ForMember(d => d.MplsEventVasTypeIds, opt => opt.MapFrom(s => (s.Event.MplsEventVasType == null)
                                    ? null : s.Event.MplsEventVasType.Select(i => i.VasTypeId)))
                .ReverseMap()
                .ForMember(d => d.CreatByUserId, opt => opt.MapFrom(s => s.EntityBase.CreatedByUserId))
                .ForMember(d => d.CreatDt, opt => opt.MapFrom(s => s.EntityBase.CreatedDateTime))
                .ForMember(d => d.ModfdByUserId, opt => opt.MapFrom(s => s.EntityBase.ModifiedByUserId))
                .ForMember(d => d.ModfdDt, opt => opt.MapFrom(s => s.EntityBase.ModifiedDateTime))
                .ForMember(d => d.RecStusId, opt => opt.MapFrom(s => s.EntityBase.RecStatusId))
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                //.ForMember(d => d.Event, opt => opt.Ignore())
                //.ForMember(d => d.EsclReas, opt => opt.Ignore())
                .ForMember(d => d.ActyLocale, opt => opt.Ignore())
                .ForMember(d => d.EventStus, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.WrkflwStus, opt => opt.Ignore());
            CreateMap<MplsEventAccsTag, MplsEventAccessTagViewModel>()
                .ReverseMap()
                .ForMember(d => d.Event, opt => opt.Ignore())
                .ForMember(d => d.MnsOffrgRoutr, opt => opt.Ignore())
                .ForMember(d => d.MnsPrfmcRpt, opt => opt.Ignore())
                .ForMember(d => d.MnsRoutgType, opt => opt.Ignore())
                .ForMember(d => d.MplsAccsBdwd, opt => opt.Ignore())
                .ForMember(d => d.CtryCdNavigation, opt => opt.Ignore());
            CreateMap<LkSiptActyType, SiptActyTypeViewModel>().ReverseMap();
            CreateMap<LkSiptTollType, SiptTollTypeViewModel>().ReverseMap();
            CreateMap<LkSiptProdType, SiptProdTypeViewModel>().ReverseMap();
            CreateMap<SiptReltdOrdr, SiptRltdOrdrViewModel>().ReverseMap();
            CreateMap<SiptEventDoc, SiptEventDocViewModel>()
                .ForMember(a => a.CreatByUserAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(a => a.Base64string, opt => opt.MapFrom(s => Convert.ToBase64String(s.FileCntnt)))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.Event, opt => opt.Ignore());
            CreateMap<SiptEvent, SiptEventViewModel>()
                .ForMember(d => d.EventCsgLvlId, opt => opt.MapFrom(s => s.Event.CsgLvlId))
                .ForMember(d => d.CreatedByDisplayName, opt => opt.MapFrom(s => s.CreatByUser.DsplNme))
                .ForMember(d => d.ModifiedByDisplayName, opt => opt.MapFrom(s => s.ModfdByUser.DsplNme))
                .ForMember(d => d.PmName, opt => opt.MapFrom(s => s.Pm == null ? string.Empty : s.Pm.FullNme))
                .ForMember(d => d.PmEmail, opt => opt.MapFrom(s => s.Pm == null ? string.Empty : s.Pm.EmailAdr))
                .ForMember(d => d.PmPhone, opt => opt.MapFrom(s => s.Pm == null ? string.Empty : s.Pm.PhnNbr))
                .ForMember(d => d.SiptReltdOrdr, opt => opt.MapFrom(s => s.Event.SiptReltdOrdr))
                .ForMember(d => d.SiptEventActyIds, opt => opt.MapFrom(s => (s.Event.SiptEventActy == null)
                                   ? null : s.Event.SiptEventActy.Select(i => i.SiptActyTypeId)))
                .ForMember(d => d.SiptEventTollTypeIds, opt => opt.MapFrom(s => (s.Event.SiptEventTollType == null)
                                   ? null : s.Event.SiptEventTollType.Select(i => i.SiptTollTypeId)))
                .ForMember(d => d.SiptReltdOrdrIds, opt => opt.MapFrom(s => (s.Event.SiptReltdOrdr == null)
                                    ? null : s.Event.SiptReltdOrdr.Select(i => i.SiptReltdOrdrId)))
                .ForMember(d => d.SiptReltdOrdrNos, opt => opt.MapFrom(s => (s.Event.SiptReltdOrdr == null)
                                    ? null : s.Event.SiptReltdOrdr.Select(i => i.M5OrdrNbr)))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.EventStus, opt => opt.Ignore());
            CreateMap<EventAsnToUser, EventAsnToUserViewModel>()
                .ForMember(d => d.UserDsplNme, opt => opt.MapFrom(s => s.AsnToUser.DsplNme))
                .ForMember(d => d.UserEmail, opt => opt.MapFrom(s => s.AsnToUser.EmailAdr))
                .ReverseMap();
            CreateMap<LkNgvnProdType, NgvnProdTypeViewModel>().ReverseMap();
            CreateMap<NgvnEventCktIdNua, NgvnEventCktIdNuaViewModel>().ReverseMap();
            CreateMap<NgvnEventSipTrnk, NgvnEventSipTrunkViewModel>().ReverseMap();
            CreateMap<LkUcaaSActyType, UcaasActivityTypeViewModel>().ReverseMap();
            CreateMap<LkUcaaSBillActy, UcaasBillActivityViewModel>().ReverseMap();
            CreateMap<LkUcaaSPlanType, UcaasPlanTypeViewModel>().ReverseMap();
            CreateMap<LkUcaaSProdType, UcaasProdTypeViewModel>().ReverseMap();
            CreateMap<UcaaSEvent, UcaasEventViewModel>()
                .ForMember(d => d.UcaasEventBilling, opt => opt.MapFrom(s => s.Event.UcaaSEventBilling))
                .ForMember(d => d.UcaasEventOdieDevice, opt => opt.MapFrom(s => s.Event.UcaaSEventOdieDev))
                .ForMember(d => d.EventCpeDev, opt => opt.MapFrom(s => s.Event.EventCpeDev))
                .ForMember(d => d.EventDeviceCompletion, opt => opt.MapFrom(s => s.Event.EventDevCmplt))
                .ForMember(d => d.EventDevServiceMgmt, opt => opt.MapFrom(s => s.Event.EventDevSrvcMgmt))
                .ForMember(d => d.EventDiscoDev, opt => opt.MapFrom(s => s.Event.EventDiscoDev))
                .ForMember(d => d.ReqorUser, opt => opt.MapFrom(s => s.ReqorUser))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.ReqorUser, opt => opt.Ignore())
                .ForMember(d => d.EsclReas, opt => opt.Ignore())
                .ForMember(d => d.EventStus, opt => opt.Ignore())
                .ForMember(d => d.WrkflwStus, opt => opt.Ignore());
            CreateMap<UcaaSEventOdieDev, UcaasEventOdieDevIceViewModel>()
                .ReverseMap()
                .ForMember(d => d.Event, opt => opt.Ignore())
                .ForMember(d => d.DevModel, opt => opt.Ignore())
                .ForMember(d => d.Manf, opt => opt.Ignore());
            CreateMap<UcaaSEventBilling, UcaasEventBillingViewModel>()
                .ForMember(d => d.UcaaSBillActyDes, opt => opt.MapFrom(s => s.UcaaSBillActy.UcaaSBillActyDes))
                .ForMember(d => d.UcaaSBicType, opt => opt.MapFrom(s => s.UcaaSBillActy.UcaaSBicType))
                .ForMember(d => d.MrcNrcCd, opt => opt.MapFrom(s => s.UcaaSBillActy.MrcNrcCd))
                .ForMember(d => d.UcaaSPlanTypeId, opt => opt.MapFrom(s => s.UcaaSBillActy.UcaaSPlanTypeId))
                .ReverseMap()
                .ForMember(d => d.UcaaSBillActy, opt => opt.Ignore());
            CreateMap<EventCpeDev, EventCpeDevViewModel>()
                .ReverseMap()
                .ForMember(d => d.Event, opt => opt.Ignore());
            CreateMap<EventDevCmplt, EventDeviceCompletionViewModel>()
                .ReverseMap()
                .ForMember(d => d.Event, opt => opt.Ignore());
            CreateMap<EventDevSrvcMgmt, EventDevSrvcMgmtViewModel>()
                .ReverseMap()
                .ForMember(d => d.Event, opt => opt.Ignore());
            CreateMap<EventDiscoDev, EventDiscoDevViewModel>()
                .ReverseMap()
                .ForMember(d => d.Event, opt => opt.Ignore());
            CreateMap<SplkEvent, SplkEventViewModel>()
                .ForMember(d => d.EntityBase, opt => opt.MapFrom(s => SetEntityBase(s.CreatByUser, s.ModfdByUser, s.RecStusId, s.CreatDt, s.ModfdDt)))
                .ForMember(d => d.EventStusDes, opt => opt.MapFrom(s => s.EventStus.EventStusDes))
                .ForMember(d => d.ReqorUser, opt => opt.MapFrom(s => s.ReqorUser))
                .ForMember(d => d.SalsUser, opt => opt.MapFrom(s => s.SalsUser))
                .ForMember(d => d.WrkflwStusDes, opt => opt.MapFrom(s => s.WrkflwStus.WrkflwStusDes))
                .ForMember(d => d.SplkEventAccsTag, opt => opt.MapFrom(s => s.SplkEventAccs ))
                .ForMember(d => d.EventCsgLvlId, opt => opt.MapFrom(s => s.Event.CsgLvlId))
                .ReverseMap()
                .ForMember(d => d.CreatByUserId, opt => opt.MapFrom(s => s.EntityBase.CreatedByUserId))
                .ForMember(d => d.CreatDt, opt => opt.MapFrom(s => s.EntityBase.CreatedDateTime))
                .ForMember(d => d.ModfdByUserId, opt => opt.MapFrom(s => s.EntityBase.ModifiedByUserId))
                .ForMember(d => d.ModfdDt, opt => opt.MapFrom(s => s.EntityBase.ModifiedDateTime))
                .ForMember(d => d.RecStusId, opt => opt.MapFrom(s => s.EntityBase.RecStatusId))
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.EventStus, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.WrkflwStus, opt => opt.Ignore());
            CreateMap<SplkEventAccs, SplkEventAccessTagViewModel>()
                .ReverseMap()
                .ForMember(d => d.Event, opt => opt.Ignore());
            CreateMap<LkSplkActyType, SplkActyTypeViewModel>().ReverseMap();
            CreateMap<LkSplkEventType, SplkEventTypeViewModel>().ReverseMap();
            CreateMap<FsaOrdr, FsaOrderViewModel>()
                .ForPath(d => d.OrdrTypeDes, opt => opt.MapFrom(s => 
                    s.Ordr.Pprt != null ? 
                        s.Ordr.Pprt.OrdrType.OrdrTypeDes : null))
                .ForMember(d => d.FsaOrdrCpeLineItem, opt => opt.MapFrom(s => s.FsaOrdrCpeLineItem))
                .ForMember(d => d.OrdrCsgLvlId, opt => opt.MapFrom(s => s.Ordr.CsgLvlId))
                .ForMember(a => a.OrdrStusDes, opt => opt.MapFrom(s => s.Ordr.OrdrStus.OrdrStusDes))
                .ForPath(d => d.CustNme, opt => opt.MapFrom(s => (s.FsaOrdrCust == null)
                                   ? null : s.FsaOrdrCust.FirstOrDefault().CustNme))
                .ForPath(d => d.Region, opt => opt.MapFrom(s =>
                    (s.Ordr.TrptOrdr.AEndRgnId != null) ?
                        s.Ordr.TrptOrdr.AEndRgnId : (s.Ordr.RgnId != null) ?
                            s.Ordr.RgnId : 1))
                //.ForPath(d => d.PortSpeed, opt => opt.MapFrom(s =>
                //    (s.FsaOrdrCpeLineItem == null) ?
                //        null : s.FsaOrdrCpeLineItem.FirstOrDefault().TtrptSpdOfSrvcBdwdDes))
                .ForPath(d => d.AccessMegabyteValue, opt => opt.MapFrom(s => s.Ordr.TrptOrdr.AccsMbDes))
                .ForPath(d => d.AccessCityName, opt => opt.MapFrom(s => s.Ordr.TrptOrdr.AccsCtySiteId))
                .ForPath(d => d.OrdrActnDes, opt => opt.MapFrom(s => s.OrdrActn.OrdrActnDes))
                .ForPath(d => d.OrdrCatId, opt => opt.MapFrom(s => s.Ordr.OrdrCatId))
                .ForPath(d => d.NrmUpdate, opt => opt.MapFrom(s => s.Ordr.TrptOrdr.NrmUpdtCd))
                .ReverseMap();
            CreateMap<EventHist, EventHistViewModel>()
                .ForMember(d => d.ActnDes, opt => opt.MapFrom(s => s.Actn.ActnDes))
                .ForMember(d => d.PerformedBy, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ReverseMap();
            CreateMap<EventSucssActy, EventSucssActyViewModel>().ReverseMap();
            CreateMap<EventFailActy, EventFailActyViewModel>().ReverseMap();
            CreateMap<MdsEventWrlsTrpt, MDSEventWrlsTrptViewModel>().ReverseMap();
            CreateMap<MdsEventSlnkWiredTrpt, MDSEventSlnkWiredTrptViewModel>().ReverseMap();
            CreateMap<MdsEventDslSbicCustTrpt, MDSEventDslSbicCustTrptViewModel>().ReverseMap();
            CreateMap<GetEventDevSrvcMgmt, GetEventDevSrvcMgmtViewModel>()
                .ReverseMap()
                .ForMember(d => d.ServiceTier, opt => opt.Ignore());
            CreateMap<GetMdsEventSiteSrvc, GetMdsEventSiteSrvcViewModel>().ReverseMap();
            CreateMap<GetEventCpeDev, GetEventCpeDevViewModel>().ReverseMap();
            CreateMap<MdsEventNtwkTrpt, MDSEventNtwkTrptViewModel>().ReverseMap();
            CreateMap<MdsEventNtwkCust, MDSEventNtwkCustViewModel>().ReverseMap();
            CreateMap<MdsEventNtwkActy, MdsEventNtwkActyViewModel>()
                //.ForMember(d => d.NtwkActyType, opt => opt.MapFrom(s => s.NtwkActyType))
                .ReverseMap();
            CreateMap<RedsgnDevicesInfo, RedesignDevicesInfoViewModel>()
                .ForMember(d => d.RedesignNbr, opt => opt.MapFrom(s => s.Redsgn.RedsgnNbr))
                .ForMember(d => d.ExpirationDate, opt => opt.MapFrom(s => s.Redsgn.ExprtnDt))
                .ForMember(d => d.EventId, opt => opt.MapFrom(s =>
                    s.RedsgnDevEventHist.SingleOrDefault(i => i.RedsgnDevId == s.RedsgnDevId) != null
                        ? s.RedsgnDevEventHist.SingleOrDefault(i => i.RedsgnDevId == s.RedsgnDevId).EventId : 0))
                .ForMember(d => d.EventCmpltnDt, opt => opt.MapFrom(s =>
                    s.RedsgnDevEventHist.SingleOrDefault(i => i.RedsgnDevId == s.RedsgnDevId) != null
                        ? s.RedsgnDevEventHist.SingleOrDefault(i => i.RedsgnDevId == s.RedsgnDevId).CmpltdDt : (DateTime?)null))
                .ReverseMap()
                .ForMember(d => d.CretdByCdNavigation, opt => opt.Ignore())
                .ForMember(d => d.ModfdByCdNavigation, opt => opt.Ignore())
                .ForMember(d => d.Redsgn, opt => opt.Ignore())
                .ForMember(d => d.DspchRdyCd, opt => opt.MapFrom(s => 
                    s.DispatchReadyCd.HasValue ? (s.DispatchReadyCd == 1 ? true : false) : (bool?)null));
            CreateMap<FedlineEvent, FedlineEventViewModel>()
                .ForMember(d => d.EventId, opt => opt.MapFrom(s => s.EventID))
                .ForMember(d => d.EventStatusId, opt => opt.MapFrom(s => s.EventStatusID))
                .ForMember(d => d.WorkflowStatusId, opt => opt.MapFrom(s => s.WorkflowStatusID))
                .ForMember(d => d.FailCodeId, opt => opt.MapFrom(s => s.FailCodeID))
                .ReverseMap();
            CreateMap<FedlineTadpoleDataEntity, FedlineTadpoleDataViewModel>()
                .ForMember(d => d.CowsFedlineId, opt => opt.MapFrom(s => s.CowsFedlineID))
                .ForMember(d => d.FrbRequestId, opt => opt.MapFrom(s => s.FRBRequestID))
                .ForMember(d => d.CustOrgId, opt => opt.MapFrom(s => s.CustOrgID))
                .ForMember(d => d.MacType, opt => opt.MapFrom(s => s.MACType))
                .ForMember(d => d.Wan, opt => opt.MapFrom(s => s.WAN))
                .ForMember(d => d.Lan, opt => opt.MapFrom(s => s.LAN))
                .ForMember(d => d.FrbSentDate, opt => opt.MapFrom(s => s.FRBSentDate))
                .ForMember(d => d.FrbModifyDate, opt => opt.MapFrom(s => s.FRBModifyDate))
                .ReverseMap();
            CreateMap<FedlineManagedDeviceEntity, FedlineManagedDeviceViewModel>()
                .ReverseMap();
            CreateMap<FedlineOrigDeviceEntity, FedlineOrigDeviceViewModel>()
                .ForMember(d => d.OrigReqId, opt => opt.MapFrom(s => s.OrigReqID))
                .ReverseMap();
            CreateMap<FedlineSiteLocationEntity, FedlineSiteLocationViewModel>()
               .ForMember(d => d.PrimaryPocName, opt => opt.MapFrom(s => s.PrimaryPOCName))
               .ForMember(d => d.PrimaryPocPhone, opt => opt.MapFrom(s => s.PrimaryPOCPhone))
               .ForMember(d => d.PrimaryPocPhoneExtension, opt => opt.MapFrom(s => s.PrimaryPOCPhoneExtension))
               .ForMember(d => d.PrimaryPocEmail, opt => opt.MapFrom(s => s.PrimaryPOCEmail))
               .ForMember(d => d.SecondaryPocName, opt => opt.MapFrom(s => s.SecondaryPOCName))
               .ForMember(d => d.SecondaryPocPhone, opt => opt.MapFrom(s => s.SecondaryPOCPhone))
               .ForMember(d => d.SecondaryPocPhoneExtension, opt => opt.MapFrom(s => s.SecondaryPOCPhoneExtension))
               .ForMember(d => d.SecondaryPocEmail, opt => opt.MapFrom(s => s.SecondaryPOCEmail))
               .ReverseMap();
            CreateMap<FedlineConfigEntity, FedlineConfigViewModel>()
              .ForMember(d => d.FedlineConfigId, opt => opt.MapFrom(s => s.FedlineConfigID))
              .ForMember(d => d.FedlineEventId, opt => opt.MapFrom(s => s.FedlineEventID))
              .ForMember(d => d.IpAddress, opt => opt.MapFrom(s => s.IPAddress))
              .ForMember(d => d.Speed, opt => opt.MapFrom(s => s.SPEED))
              .ForMember(d => d.MacAddress, opt => opt.MapFrom(s => s.MACAddress))
              .ReverseMap();
            CreateMap<FedlineUserDataEntity, FedlineUserDataViewModel>()
               .ForMember(d => d.EmailCcpdl, opt => opt.MapFrom(s => s.EmailCCPDL))
               .ForMember(d => d.PreStagingEngineerUserId, opt => opt.MapFrom(s => s.PreStagingEngineerUserID))
               .ForMember(d => d.MnsActivatorUserId, opt => opt.MapFrom(s => s.MNSActivatorUserID))
               .ForMember(d => d.MnsActivatorUserDisplay, opt => opt.MapFrom(s => s.MNSActivatorUserDisplay))
               .ForMember(d => d.FailCodeId, opt => opt.MapFrom(s => s.FailCodeID))
               .ForMember(d => d.SowLocation, opt => opt.MapFrom(s => s.SOWLocation))
               .ReverseMap();
            CreateMap<OrdrNte, OrderNoteViewModel>()
                .ForMember(d => d.creatByUserFullName, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ForMember(d => d.NteTypeDes, opt => opt.MapFrom(s => s.NteType.NteTypeDes))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.NteType, opt => opt.Ignore());
            CreateMap<OrderNoteView, OrderNoteViewModel>()
                .ForMember(d => d.creatByUserFullName, opt => opt.MapFrom(s => s.CreatByUser))
                .ForMember(d => d.NteTypeDes, opt => opt.MapFrom(s => s.NteType))
                .ReverseMap();
            CreateMap<NccoOrdr, NccoOrderViewModel>()
                .ForMember(d => d.ProdType, opt => opt.MapFrom(s => s.ProdType.ProdTypeDes))
                .ForMember(d => d.OrdrType, opt => opt.MapFrom(s => s.OrdrType.OrdrTypeDes))
                .ForMember(d => d.Ctry, opt => opt.MapFrom(s => s.CtryCdNavigation.CtryNme))
                .ForMember(d => d.OrdrStatus, opt => opt.MapFrom(s => s.Ordr.OrdrStus.OrdrStusDes))
                .ReverseMap();
            CreateMap<NccoOrderView, NccoOrderViewModel>().ReverseMap();
            CreateMap<LkSdePrdctType, MssProductTypeViewModel>().ReverseMap();
            CreateMap<MdsEventSiteSrvc, MdsEventSiteSrvcViewModel>().ReverseMap();
            CreateMap<MdsEventDslSbicCustTrpt, MDSEventDslSbicCustTrptViewModel>().ReverseMap();
            CreateMap<MdsEventSlnkWiredTrpt, MDSEventSlnkWiredTrptViewModel>().ReverseMap();
            CreateMap<MdsEventWrlsTrpt, MDSEventWrlsTrptViewModel>().ReverseMap();
            CreateMap<MdsEventPortBndwd, MDSEventPortBndwdViewModel>().ReverseMap();
            //CreateMap<MdsEventViewModel, MdsEvent>()
            //   .ForPath(d => d.Event.MdsEventNtwkCust, opt => opt.MapFrom(s => s.NetworkCustomer))
            //   .ForPath(d => d.Event.MdsEventNtwkTrpt, opt => opt.MapFrom(s => s.NetworkTransport))
            //   .ForPath(d => d.Event.MdsEventOdieDev, opt => opt.MapFrom(s => s.MdsRedesignDevInfo))
            //   .ForPath(d => d.Event.EventDiscoDev, opt => opt.MapFrom(s => s.DiscOdie))
            //   .ForPath(d => d.Event.EventDevCmplt, opt => opt.MapFrom(s => s.DevCompletion))
            //   .ForPath(d => d.Event.EventCpeDev, opt => opt.MapFrom(s => s.CpeDevice))
            //   .ForPath(d => d.Event.EventDevSrvcMgmt, opt => opt.MapFrom(s => s.MnsOrder))
            //   .ForPath(d => d.Event.MdsEventSiteSrvc, opt => opt.MapFrom(s => s.SiteService))
            //   .ForPath(d => d.Event.MdsEventDslSbicCustTrpt, opt => opt.MapFrom(s => s.ThirdParty))
            //   .ForPath(d => d.Event.MdsEventSlnkWiredTrpt, opt => opt.MapFrom(s => s.WiredTransport))
            //   .ForPath(d => d.Event.MdsEventWrlsTrpt, opt => opt.MapFrom(s => s.WirelessTransport))
            //   .ForPath(d => d.Event.MdsEventPortBndwd, opt => opt.MapFrom(s => s.PortBandwidth))
            //   .ForPath(d => d.Event.MplsEventActyType, opt => opt.MapFrom(s => s.MplsEventActyType))
            //   .ReverseMap();
            CreateMap<MdsEvent, MdsEventViewModel>()
               .ForMember(d => d.NetworkCustomer, opt => opt.MapFrom(s => s.Event.MdsEventNtwkCust))
               .ForMember(d => d.NetworkTransport, opt => opt.MapFrom(s => s.Event.MdsEventNtwkTrpt))
               //.ForMember(d => d.NetworkTransport, opt => opt.MapFrom(s => s.Event.NidActy))
               .ForMember(d => d.MdsRedesignDevInfo, opt => opt.MapFrom(s => s.Event.MdsEventOdieDev))
               .ForMember(d => d.EventDiscoDev, opt => opt.MapFrom(s => s.Event.EventDiscoDev))
               .ForMember(d => d.DevCompletion, opt => opt.MapFrom(s => s.Event.EventDevCmplt))
               .ForMember(d => d.CpeDevice, opt => opt.MapFrom(s => s.Event.EventCpeDev))
               .ForMember(d => d.MnsOrder, opt => opt.MapFrom(s => s.Event.EventDevSrvcMgmt))
               .ForMember(d => d.SiteService, opt => opt.MapFrom(s => s.Event.MdsEventSiteSrvc))
               .ForMember(d => d.ThirdParty, opt => opt.MapFrom(s => s.Event.MdsEventDslSbicCustTrpt))
               .ForMember(d => d.WiredTransport, opt => opt.MapFrom(s => s.Event.MdsEventSlnkWiredTrpt))
               .ForMember(d => d.WirelessTransport, opt => opt.MapFrom(s => s.Event.MdsEventWrlsTrpt))
               .ForMember(d => d.PortBandwidth, opt => opt.MapFrom(s => s.Event.MdsEventPortBndwd))
               .ForMember(d => d.MdsEventNtwkActy, opt => opt.MapFrom(s => s.Event.MdsEventNtwkActy))
               .ForPath(d => d.MdsEventNtwkActyIds, opt => opt.MapFrom(s => (s.Event.MdsEventNtwkActy == null)
                                   ? null : s.Event.MdsEventNtwkActy.Select(i => i.NtwkActyTypeId)))
               .ForMember(d => d.MplsEventActyType, opt => opt.MapFrom(s => s.Event.MplsEventActyType))
               .ForPath(d => d.MplsEventActyTypeIds, opt => opt.MapFrom(s => (s.Event.MplsEventActyType == null)
                                   ? null : s.Event.MplsEventActyType.Select(i => i.MplsActyTypeId)))
               .ForMember(d => d.MdsEventMacActy, opt => opt.MapFrom(s => s.Event.MdsEventMacActy))
               .ForPath(d => d.MdsEventMacActyIds, opt => opt.MapFrom(s => (s.Event.MdsEventMacActy == null)
                                   ? null : s.Event.MdsEventMacActy.Select(i => i.MdsMacActyId)))
               .ReverseMap();
            CreateMap<NidActy, MDSEventNtwkTrptViewModel>()
                .ForMember(d => d.AssocH6, opt => opt.MapFrom(s => s.H6))
                .ReverseMap();
            CreateMap<MplsEventActyType, MplsActivityTypeViewModel>().ReverseMap();
            CreateMap<OrdrRecLock, OrderLockViewModel>()
                .ForMember(d => d.LockByFullName, opt => opt.MapFrom(s => s.LockByUser.FullNme))
                .ReverseMap()
                .ForMember(d => d.Ordr, opt => opt.Ignore())
                .ForMember(d => d.LockByUser, opt => opt.Ignore());
            //CreateMap<MdsEvent, MdsEventView>()
            //    .ForMember(d => d.EventId, opt => opt.MapFrom(s => s.EventId))
            //    .ForMember(d => d.NtwkActyTypeId, opt => opt.MapFrom(s => s.NtwkActyTypeId))
            //    .ForMember(d => d.EventStusId, opt => opt.MapFrom(s => s.EventStusId))
            //    .ForMember(d => d.EventTitleTxt, opt => opt.MapFrom(s => s.EventTitleTxt))
            //    .ForMember(d => d.ShrtDes, opt => opt.MapFrom(s => s.ShrtDes))
            //    .ForMember(d => d.H1, opt => opt.MapFrom(s => s.H1))
            //    .ForMember(d => d.CustNme, opt => opt.MapFrom(s => s.CustNme))
            //    .ForMember(d => d.CustSowLocTxt, opt => opt.MapFrom(s => s.CustSowLocTxt))
            //    .ForMember(d => d.MdsActyTypeId, opt => opt.MapFrom(s => s.MdsActyTypeId))
            //    .ForMember(d => d.CustAcctTeamPdlNme, opt => opt.MapFrom(s => s.CustAcctTeamPdlNme))
            //    .ForMember(d => d.MdsEventMacActy, opt => opt.MapFrom(s => s.Event.MdsEventMacActy))
            //    .ForMember(d => d.NetworkInfo.H6, opt => opt.MapFrom(s => s.NtwkH6))
            //    .ForMember(d => d.NetworkInfo.H1, opt => opt.MapFrom(s => s.NtwkH1))
            //    .ForMember(d => d.NetworkInfo.CustomerName, opt => opt.MapFrom(s => s.NtwkCustNme))
            //    .ForMember(d => d.NetworkInfo.NtwkEventTypeId, opt => opt.MapFrom(s => s.NtwkEventTypeId))
            //    .ForMember(d => d.NetworkInfo.VpnPltfrmTypeId, opt => opt.MapFrom(s => s.VpnPltfrmTypeId))
            //    .ForMember(d => d.NetworkInfo.MplsEventActyType, opt => opt.MapFrom(s => s.Event.MplsEventActyType))
            //    //.ForMember(d => d.NetworkInfo.DesignDocumentApprovalNumber, opt => opt.MapFrom(s => s.))
            //    .ForMember(d => d.NetworkInfo.SalesEngineerEmail, opt => opt.MapFrom(s => s.SalsEngrEmail))
            //    .ForMember(d => d.NetworkInfo.SalesEngineerPhone, opt => opt.MapFrom(s => s.SalsEngrPhn))
            //    .ForMember(d => d.NetworkInfo.CsgLvlId, opt => opt.MapFrom(s => s.Event.CsgLvlId))
            //    //.ForMember(d => d.NetworkInfo.GroupName, opt => opt.MapFrom(s => s.G))
            //    .ForMember(d => d.NetworkInfo.NetworkCustomer, opt => opt.MapFrom(s => s.Event.MdsEventNtwkCust))
            //    .ForMember(d => d.NetworkInfo.NetworkTransport, opt => opt.MapFrom(s => s.Event.MdsEventNtwkTrpt))
            //    .ForMember(d => d.MdsOdieDev, opt => opt.MapFrom(s => s.Event.MdsEventOdieDev))
            //    .ForMember(d => d.SiteInfo.H6, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.Ccd, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.InstlSitePocNme, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.InstlSitePocIntlPhnCd, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.InstlSitePocPhnNbr, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.SrvcAssrnPocNme, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.SrvcAssrnPocIntlPhnCd, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.SrvcAssrnPocPhnNbr, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.SiteIdTxt, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.StreetAdr, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.SttPrvnNme, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.FlrBldgNme, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.CtryRgnNme, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.CtyNme, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.ZipCd, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.UsIntlCd, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.SrvcAvlbltyHrs, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo.SrvcTmeZnCd, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ForMember(d => d.SiteInfo, opt => opt.MapFrom(s => s.CmpltdEmailCcTxt))
            //    .ReverseMap();
            CreateMap<CalendarData, CalendarDataViewModel>().ReverseMap();
            CreateMap<SdeOpportunity, SdeOpportunityViewModel>()
                .ForMember(d => d.SdeOpportunityId, opt => opt.MapFrom(s => s.SDEOpportunityID))
                .ForMember(d => d.StatusId, opt => opt.MapFrom(s => s.StatusID))
                //.ForMember(d => d.AssignedToId, opt => opt.MapFrom(s => s.AssignedToID))
                //.ForMember(d => d.CreatedByUserId, opt => opt.MapFrom(s => s.CreatedByUserId))
                .ReverseMap();
            CreateMap<SDEOpportunityProduct, SDEOpportunityProductViewModel>().ReverseMap();
            CreateMap<SDEOpportunityNote, SDEOpportunityNoteViewModel>().ReverseMap();
            CreateMap<SDEOpportunityDoc, SDEOpportunityDocViewModel>().ReverseMap();
            CreateMap<LkCptCustType, CptCustomerTypeViewModel>().ReverseMap();
            CreateMap<LkCptHstMngdSrvc, CptHostManagedServiceViewModel>().ReverseMap();
            CreateMap<LkCptMdsSuprtTier, CptManagedAuthProdViewModel>().ReverseMap();
            CreateMap<LkCptMdsSuprtTier, CptMdsSupportTierViewModel>().ReverseMap();
            CreateMap<LkCptMvsProdType, CptMvsProdTypeViewModel>().ReverseMap();
            CreateMap<LkCptPlnSrvcTier, CptPlanServiceTierViewModel>().ReverseMap();
            CreateMap<LkCptPlnSrvcType, CptPlanServiceTypeViewModel>().ReverseMap();
            CreateMap<LkCptPrimScndyTprt, CptPrimSecondaryTransportViewModel>().ReverseMap();
            CreateMap<LkCptPrimSite, CptPrimSiteViewModel>().ReverseMap();
            CreateMap<LkCptPrvsnType, CptProvisionTypeViewModel>().ReverseMap();
            CreateMap<GetAllCptView, GetAllCptViewModel>().ReverseMap();
            CreateMap<Cpt, CptViewModel>()
                .ForMember(d => d.SubmitterName, opt => opt.MapFrom(s => s.SubmtrUserId != 0
                                    ? s.SubmtrUser.FullNme : string.Empty))
                .ForMember(d => d.SubmitterEmail, opt => opt.MapFrom(s => s.SubmtrUserId != 0
                                    ? s.SubmtrUser.EmailAdr : string.Empty))
                .ForMember(d => d.SubmitterPhone, opt => opt.MapFrom(s => s.SubmtrUserId != 0
                                    ? s.SubmtrUser.PhnNbr : string.Empty))
                .ForMember(d => d.CustomerTypes, opt => opt.MapFrom(s => (s.CptRltdInfo == null)
                                    ? null : s.CptRltdInfo.Select(i => i.CptCustTypeId).Distinct()))
                .ForMember(d => d.HostedManagedServices, opt => opt.MapFrom(s => (s.CptMngdSrvc == null)
                                    ? null : s.CptMngdSrvc.Select(i => i.CptHstMngdSrvcId)))
                .ForMember(d => d.MdsPlnSrvcTiers, opt => opt.MapFrom(s => (s.CptRltdInfo == null)
                                    ? null : s.CptRltdInfo.Select(i => i.CptPlnSrvcTierId).Distinct()))
                .ForMember(d => d.MdsSupportTiers, opt => opt.MapFrom(s => (s.CptSuprtTier == null)
                                    ? null : s.CptSuprtTier.Select(i => i.CptMdsSuprtTierId).Distinct()))
                .ForMember(d => d.MssPlnSrvcTypes, opt => opt.MapFrom(s => (s.CptSrvcType == null)
                                    ? null : s.CptSrvcType.Select(i => i.CptPlnSrvcTypeId).Distinct()))
                .ForMember(d => d.MssMngdAuthenticationProds, opt => opt.MapFrom(s => (s.CptMngdAuth == null)
                                    ? null : s.CptMngdAuth.Select(i => i.CptMngdAuthPrdId)))
                .ForMember(d => d.MvsProdTypes, opt => opt.MapFrom(s => (s.CptMvsProd == null)
                                    ? null : s.CptMvsProd.Select(i => i.CptMvsProdTypeId).Distinct()))
                .ForMember(d => d.PrimSites, opt => opt.MapFrom(s => (s.CptPrimSite == null)
                                    ? null : s.CptPrimSite.Select(i => i.CptPrimSiteId).Distinct()))
                .ForMember(d => d.RelatedInfos, opt => opt.MapFrom(s => s.CptRltdInfo))
                .ForMember(d => d.CptUserAsmt, opt => opt.MapFrom(s => s.CptUserAsmt.Where(i => i.RecStusId == 1)))
                .ForMember(d => d.CptDocs, opt => opt.MapFrom(s => s.CptDoc))
                .ForMember(d => d.CptHist, opt => opt.MapFrom(s => s.CptHist.OrderByDescending(i => i.CptHistId)))
                .ForMember(d => d.CptStatus, opt => opt.MapFrom(s => s.CptStus.StusDes))
                .ForMember(d => d.ReviewedByPMDate, opt => opt.MapFrom(s => s.RevwdPmCd ? "Reviewed last: " + s.CptHist.OrderByDescending(i => i.CreatDt).FirstOrDefault(i => i.ActnId == (byte)Actions.CPTReviewedByPM).CreatDt.ToString("MM/dd/yyyy HH:mm:ss tt") : string.Empty))
                .ForMember(d => d.ReturnedToSDEDate, opt => opt.MapFrom(s => s.RetrnSdeCd ? "Returned last: " + s.CptHist.OrderByDescending(i => i.CreatDt).FirstOrDefault(i => i.ActnId == (byte)Actions.CPTReturnedToSDE).CreatDt.ToString("MM/dd/yyyy HH:mm:ss tt") : string.Empty))
            .ReverseMap()
                .ForMember(d => d.RecStus, opt => opt.Ignore())
                .ForMember(d => d.CptStus, opt => opt.Ignore())
                .ForMember(d => d.CreatByUser, opt => opt.Ignore());
            CreateMap<CptRltdInfo, CptRelatedInfoViewModel>()
                .ReverseMap()
                .ForMember(d => d.Cpt, opt => opt.Ignore())
                .ForMember(d => d.CptCustType, opt => opt.Ignore())
                .ForMember(d => d.CptPlnSrvcTier, opt => opt.Ignore());
            CreateMap<CptMngdSrvc, CptHostManagedServiceViewModel>()
                .ReverseMap()
                .ForMember(d => d.Cpt, opt => opt.Ignore())
                .ForMember(d => d.CptHstMngdSrvc, opt => opt.Ignore());
            CreateMap<CptMngdAuth, CptManagedAuthProdViewModel>()
                .ReverseMap()
                .ForMember(d => d.Cpt, opt => opt.Ignore())
                .ForMember(d => d.CptMngdAuthPrd, opt => opt.Ignore());
            CreateMap<CptPrimWan, CptPrimWanViewModel>()
                .ReverseMap()
                .ForMember(d => d.Cpt, opt => opt.Ignore())
                .ForMember(d => d.CptPlnSrvcTier, opt => opt.Ignore())
                .ForMember(d => d.CptPrimScndyTprt, opt => opt.Ignore());
            CreateMap<CptDoc, CptDocViewModel>()
                .ForMember(d => d.CreatByAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.Base64string, opt => opt.MapFrom(s => Convert.ToBase64String(s.FileCntnt)))
                .ReverseMap()
                .ForMember(d => d.Cpt, opt => opt.Ignore())
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.RecStus, opt => opt.Ignore());
            CreateMap<CptHist, CptHistViewModel>()
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ForMember(d => d.ActionName, opt => opt.MapFrom(s => s.Actn.ActnDes))
                .ReverseMap()
                .ForMember(d => d.Cpt, opt => opt.Ignore())
                .ForMember(d => d.Actn, opt => opt.Ignore())
                .ForMember(d => d.CreatByUser, opt => opt.Ignore());
            CreateMap<CptPrvsn, CptProvisionViewModel>()
                .ForMember(d => d.CptPrvsnType, opt => opt.MapFrom(s => s.CptPrvsnType.CptPrvsnType))
                .ForMember(d => d.RecStus, opt => opt.MapFrom(s => s.PrvsnStusCd.GetValueOrDefault() ? s.RecStus.StusDes : "N/A"))
                .ReverseMap()
                .ForMember(d => d.Cpt, opt => opt.Ignore())
                .ForMember(d => d.CptPrvsnType, opt => opt.Ignore())
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.RecStus, opt => opt.Ignore());
            CreateMap<CptUserAsmt, CptUserAsmtViewModel > ()
                .ForMember(d => d.CptUser, opt => opt.MapFrom(s => s.CptUser.FullNme))
                .ForMember(d => d.CptUserEmail, opt => opt.MapFrom(s => s.CptUser.EmailAdr))
                .ReverseMap()
                .ForMember(d => d.Cpt, opt => opt.Ignore())
                .ForMember(d => d.CptUser, opt => opt.Ignore())
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.UsrPrf, opt => opt.Ignore());
            CreateMap<OdieCustRspnModel, OdieCustRspnViewModel>().ReverseMap();
            CreateMap<VndrFoldr, VendorFolderViewModel>()
                .ForMember(d => d.CtryNme, opt => opt.MapFrom(s => s.CtryCdNavigation.CtryNme))
                .ForMember(d => d.SttNme, opt => opt.MapFrom(s => s.SttCdNavigation.UsStateName))
                .ForMember(d => d.CreatByUserName, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.ModfdByUserName, opt => opt.MapFrom(s => s.ModfdByUser.UserAdid))
                .ForMember(d => d.VndrNme, opt => opt.MapFrom(s => s.VndrCdNavigation.VndrNme))
                .ForMember(d => d.VndrCd, opt => opt.MapFrom(s => s.VndrCdNavigation.VndrCd))
                .ReverseMap()
                .ForMember(d => d.SttCd, opt => opt.MapFrom(s => string.IsNullOrWhiteSpace(s.SttCd) ? null : s.SttCd))
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.CtryCdNavigation, opt => opt.Ignore())
                .ForMember(d => d.SttCdNavigation, opt => opt.Ignore())
                .ForMember(d => d.VndrCdNavigation, opt => opt.Ignore());
            CreateMap<VndrFoldrCntct, VendorFolderContactViewModel>().ReverseMap();
            CreateMap<VndrTmplt, VendorTemplateViewModel>()
                .ForMember(d => d.CreatByUserAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.Base64string, opt => opt.MapFrom(s => Convert.ToBase64String(s.FileCntnt)))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.RecStus, opt => opt.Ignore());
            CreateMap<LkUsStt, StateViewModel>().ReverseMap();
            CreateMap<DocEntity, DocEntityViewModel>().ReverseMap();
            CreateMap<AdvancedSearchRedesign, AdvancedSearchRedesignViewModel>().ReverseMap();
            CreateMap<AdvancedSearchOrder, AdvancedSearchOrderViewModel>().ReverseMap();
            CreateMap<AdvancedSearchEvent, AdvancedSearchEventViewModel>().ReverseMap();
            CreateMap<LkSysCfg, SystemConfigViewModel>().ReverseMap();
            CreateMap<OdieCustomerH1, OdieCustomerH1ViewModel>().ReverseMap();
            CreateMap<FsaOrdrCust, FsaOrderCustViewModel>().ReverseMap();
            CreateMap<OrdrAdr, OrderAddressViewModel>()
                .ForMember(a => a.AdrTypeDes, opt => opt.MapFrom(s => s.AdrType.AdrTypeDes))
                .ForMember(a => a.CtryNme, opt => opt.MapFrom(s => s.CtryCdNavigation.CtryNme))
                .ReverseMap();
            CreateMap<OrdrCntct, OrderContactViewModel>()
                .ForMember(a => a.CntctTypeDes, opt => opt.MapFrom(s => s.CntctType.CntctTypeDes))
                .ForMember(a => a.RoleNme, opt => opt.MapFrom(s => s.Role.RoleNme))
                .ReverseMap();
            CreateMap<OrdrVlan, OrderVlanViewModel>().ReverseMap();
            CreateMap<H5Foldr, H5FolderViewModel>()
                .ForMember(a => a.CreatByUserAdId, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(a => a.H5DocCnt, opt => opt.MapFrom(s => s.H5Doc != null ? s.H5Doc.Count() : 0))
                //.ForMember(a => a.Orders, opt => opt.MapFrom(s => s.Ordr))
                .ForMember(a => a.H5Docs, opt => opt.MapFrom(s => s.H5Doc))
                .ReverseMap()
                .ForMember(d => d.CsgLvl, opt => opt.Ignore())
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.CtryCd, opt => opt.Ignore());
            CreateMap<H5Doc, H5DocViewModel>()
                .ForMember(a => a.CreatByUserAdid, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(a => a.Base64string, opt => opt.MapFrom(s => Convert.ToBase64String(s.FileCntnt)))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.H5Foldr, opt => opt.Ignore());
            CreateMap<RedsgnWrkflw, RedesignWorkflowViewModel>()
                .ForMember(d => d.PreWrkflwStus, opt => opt.MapFrom(s => s.PreWrkflwStus.StusDes))
                .ForMember(d => d.DesrdWrkflwStus, opt => opt.MapFrom(s => s.DesrdWrkflwStus.StusDes))
                .ReverseMap()
                .ForMember(d => d.PreWrkflwStus, opt => opt.Ignore())
                .ForMember(d => d.DesrdWrkflwStus, opt => opt.Ignore());
            CreateMap<RedsgnCustBypass, RedesignCustBypassViewModel>()
                .ReverseMap()
                .ForMember(d => d.CsgLvl, opt => opt.Ignore());
            CreateMap<RedsgnDoc, RedesignDocViewModel>()
                .ForMember(d => d.CreatByUserName, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(d => d.Base64string, opt => opt.MapFrom(s => Convert.ToBase64String(s.FileCntnt)))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.RecStus, opt => opt.Ignore())
                .ForMember(d => d.Redsgn, opt => opt.Ignore());
            CreateMap<RedsgnEmailNtfctn, RedesignEmailNotificationViewModel>()
                .ReverseMap()
                .ForMember(d => d.CretdUser, opt => opt.Ignore())
                .ForMember(d => d.Redsgn, opt => opt.Ignore());
            CreateMap<RedsgnNotes, RedesignNotesViewModel>()
                .ForMember(d => d.RedsgnNteType, opt => opt.MapFrom(s => s.RedsgnNteType.RedsgnNteTypeCd))
                .ForMember(d => d.CretdBy, opt => opt.MapFrom(s => s.CretdByCdNavigation.FullNme))
                .ReverseMap()
                .ForMember(d => d.CretdByCdNavigation, opt => opt.Ignore())
                .ForMember(d => d.Redsgn, opt => opt.Ignore())
                .ForMember(d => d.RedsgnNteType, opt => opt.Ignore());
            CreateMap<Redsgn, RedesignViewModel>()
                .ForMember(d => d.Devices, opt => opt.MapFrom(s => s.RedsgnDevicesInfo))
                .ForMember(d => d.Docs, opt => opt.MapFrom(s => s.RedsgnDoc))
                .ForMember(d => d.Emails, opt => opt.MapFrom(s => s.RedsgnEmailNtfctn))
                .ForMember(d => d.Notes, opt => opt.MapFrom(s => s.RedsgnNotes.OrderByDescending(i => i.RedsgnNotesId)))
                .ForMember(d => d.RedesignRecLock, opt => opt.MapFrom(s => s.RedsgnRecLock))
                .ForMember(d => d.StatusDesc, opt => opt.MapFrom(s => s.Stus.StusDes))
                .ForMember(d => d.OldStusId, opt => opt.MapFrom(s => s.StusId))
                .ForMember(d => d.SystemCreatedRedesign, opt => opt.MapFrom(s => s.RedsgnNotes != null 
                    && s.RedsgnNotes.FirstOrDefault(i => i.Notes.Contains("created systematically from CPT process")) != null))
                .ReverseMap()
                .ForMember(d => d.CptRedsgn, opt => opt.Ignore())
                .ForMember(d => d.CretdByCdNavigation, opt => opt.Ignore())
                .ForMember(d => d.CsgLvl, opt => opt.Ignore())
                .ForMember(d => d.RedsgnCat, opt => opt.Ignore())
                .ForMember(d => d.RedsgnDevStus, opt => opt.Ignore())
                .ForMember(d => d.RedsgnRecLock, opt => opt.Ignore())
                .ForMember(d => d.RedsgnType, opt => opt.Ignore())
                .ForMember(d => d.Stus, opt => opt.Ignore());
            CreateMap<OdieRspnInfo, OdieRspnInfoViewModel>()
                .ReverseMap()
                .ForMember(d => d.Req, opt => opt.Ignore())
                .ForMember(d => d.Event, opt => opt.Ignore());
            CreateMap<OdieRspnInfo, OdieRspnInfoViewModel>()
                .ReverseMap()
                .ForMember(d => d.Req, opt => opt.Ignore())
                .ForMember(d => d.Event, opt => opt.Ignore());
            CreateMap<RedsgnRecLock, RedesignRecLockViewModel>()
                .ReverseMap()
                .ForMember(d => d.Redsgn, opt => opt.Ignore());
            CreateMap<LkXnciRgn, XnciRegionViewModel>()
                .ReverseMap();
            CreateMap<OrdrStdiHist, OrderStdiHistoryViewModel>()
                .ForMember(d => d.StdiReasDes, opt => opt.MapFrom(s => s.StdiReas.StdiReasDes))
                .ForMember(d => d.CreatByUserFullNme, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ForMember(d => d.Ccd, opt => opt.MapFrom(s => (s.Ordr.OrdrCatId == 1) ?
                    s.Ordr.IplOrdr.CustCmmtDt : (s.Ordr.OrdrCatId == 2) ?
                        s.Ordr.FsaOrdr.CustCmmtDt : (s.Ordr.OrdrCatId == 4) ?
                            s.Ordr.NccoOrdr.CcsDt : null))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.StdiReas, opt => opt.Ignore());
            CreateMap<VndrOrdr, VendorOrderViewModel>()
                .ForMember(d => d.VndrNme, opt => opt.MapFrom(s => s.VndrFoldr.VndrCdNavigation.VndrNme))
                .ForMember(d => d.CtryNme, opt => opt.MapFrom(s => s.VndrFoldr.CtryCdNavigation.CtryNme))
                .ForMember(d => d.VndrCd, opt => opt.MapFrom(s => s.VndrFoldr.VndrCdNavigation.VndrCd))
                .ForMember(d => d.VndrOrdrTypeDes, opt => opt.MapFrom(s => s.VndrOrdrType.VndrOrdrTypeDes))
                .ForMember(d => d.CsgLvlId, opt => opt.MapFrom(s => s.VndrOrdrNavigation.CsgLvlId))
                .ForMember(d => d.CustId, opt => opt.MapFrom(s => s.VndrOrdrNavigation.H5Foldr.CustId))
                .ForMember(d => d.H5Foldr, opt => opt.MapFrom(s => s.VndrOrdrNavigation.H5Foldr))
                .ForMember(d => d.Ftn, opt => opt.MapFrom(s => s.Ordr.FsaOrdr.Ftn))
                //.ForMember(d => d.ProductType, opt => opt.MapFrom(s => s.Ordr.FsaOrdr.ProdTypeCdNavigation.LkProdType.))
                //.ForMember(d => d.OrderType, opt => opt.MapFrom(s => s.VndrFoldr.CtryCdNavigation.CtryNme))
                .ReverseMap()
                .ForMember(d => d.VndrFoldr, opt => opt.Ignore());
            CreateMap<VndrOrdrEmail, VendorOrderEmailViewModel>()
                .ForMember(d => d.EmailTypeDesc, opt => opt.MapFrom(s => s.VndrEmailType.VndrEmailTypeDes))
                .ForPath(
                    d => d.EmailAttachmentName,
                    opt => opt.MapFrom(s => (s.VndrOrdrEmailAtchmt == null) ? null
                        : s.VndrOrdrEmailAtchmt.Select(a => a.FileNme).ToList()))
                .ForPath(
                    d => d.EmailAttachmentSize,
                    opt => opt.MapFrom(s => (s.VndrOrdrEmailAtchmt == null) ? null
                        : s.VndrOrdrEmailAtchmt.Select(a => a.FileSizeQty).ToList()))
                .ForMember(d => d.ModfdByUserFullName, opt => opt.MapFrom(s => s.ModfdByUser.FullNme))
                .ForMember(d => d.CreatByUserFullName, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ForMember(d => d.EmailStatusDesc, opt => opt.MapFrom(s => s.EmailStus.StusDes))
                .ForPath(
                    d => d.SentToVendorDate,
                    opt => opt.MapFrom(s => (s.VndrOrdrMs == null) ? null
                        : s.VndrOrdrMs.OrderByDescending(a => a.VerId).FirstOrDefault().SentToVndrDt))
                .ForPath(
                    d => d.AckByVendorDate,
                    opt => opt.MapFrom(s => (s.VndrOrdrMs == null) ? null
                        : s.VndrOrdrMs.OrderByDescending(a => a.VerId).FirstOrDefault().AckByVndrDt))
                .ReverseMap()
                .ForMember(d => d.VndrEmailType, opt => opt.Ignore())
                .ForMember(d => d.VndrOrdrMs, opt => opt.MapFrom(a => a.VndrOrdrMs))
                .ForMember(d => d.EmailStus, opt => opt.Ignore())
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore());
            CreateMap<FsaOrdrCpeLineItem, FsaOrderCpeLineItemViewModel>()
                .ForPath(
                    d => d.PrchOrdrBackOrdrShipDt,
                    opt => opt.MapFrom(s => (s.FsaOrdrGomXnci == null) ? null
                        : s.FsaOrdrGomXnci.FirstOrDefault().PrchOrdrBackOrdrShipDt))
                .ForPath(
                    d => d.CpeVndrNme,
                    opt => opt.MapFrom(s => (s.FsaOrdrGomXnci == null) ? null
                        : s.FsaOrdrGomXnci.FirstOrDefault().CpeVndrNme))
                .ForPath(
                    d => d.ShpmtTrkNbr,
                    opt => opt.MapFrom(s => (s.FsaOrdrGomXnci == null) ? null
                        : s.FsaOrdrGomXnci.FirstOrDefault().ShpmtTrkNbr))
                .ForPath(
                    d => d.CustDlvryDt,
                    opt => opt.MapFrom(s => (s.FsaOrdrGomXnci == null) ? null
                        : s.FsaOrdrGomXnci.FirstOrDefault().CustDlvryDt))
                .ReverseMap();
            CreateMap<FsaOrdrGomXnci, FsaOrdrGomXnciViewModel>()
                .ReverseMap();
            CreateMap<CCDHistory, CCDHistoryViewModel>().ReverseMap();
            CreateMap<Ordr, OrderVendorViewModel>().ReverseMap();
            CreateMap<OrdrCktChg, CircuitAddlCost>()
                .ForMember(d => d.OrderID, opt => opt.MapFrom(s => s.OrdrId))
                .ForMember(d => d.ChargeTypeID, opt => opt.MapFrom(s => s.CktChgTypeId))
                .ForMember(d => d.Version, opt => opt.MapFrom(s => s.VerId))
                .ForMember(d => d.CurrencyCd, opt => opt.MapFrom(s => s.ChgCurId))
                .ForMember(d => d.ChargeNRC, opt => opt.MapFrom(s => s.ChgNrcAmt))
                .ForMember(d => d.ChargeNRCUSD, opt => opt.MapFrom(s => s.ChgNrcInUsdAmt))
                .ForMember(d => d.TaxRate, opt => opt.MapFrom(s => s.TaxRtPctQty))
                .ForMember(d => d.Note, opt => opt.MapFrom(s => s.NteTxt))
                .ForMember(d => d.StatusID, opt => opt.MapFrom(s => s.SalsStusId))
                .ForMember(d => d.BillOnly, opt => opt.MapFrom(s => s.ReqrBillOrdrCd))
                .ForMember(d => d.ExpirationTime, opt => opt.MapFrom(s => s.XpirnDt))
                .ForMember(d => d.IsTerm, opt => opt.MapFrom(s => s.TrmtgCd))
                .ForMember(d => d.CreatedByUserId, opt => opt.MapFrom(s => s.CreatByUserId))
                .ForMember(d => d.CreatedDateTime, opt => opt.MapFrom(s => s.CreatDt))
                .ReverseMap()
                .ForMember(d => d.CreatDt, opt => opt.MapFrom(s => DateTime.Now));
            CreateMap<LkVndrEmailType, VendorEmailTypeViewModel>().ReverseMap();
            CreateMap<VndrOrdrMs, VendorOrderMsViewModel>().ReverseMap();
            CreateMap<CpeClli, CpeClliRequestDetailsViewModel>().ReverseMap();
            CreateMap<VndrOrdrForm, VendorOrderFormViewModel>()
                .ForMember(d => d.CreatByUserFullName, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore());
            CreateMap<Ordr, xNCIViewModel>().ReverseMap();
            CreateMap<OrdrMs, OrderMsViewModel>().ReverseMap();
            CreateMap<Ckt, CircuitViewModel>().ReverseMap();
            CreateMap<CktMs, CircuitMsViewModel>().ReverseMap();
            CreateMap<LkTask, TaskViewModel>().ReverseMap();
            CreateMap<VndrOrdrEmailAtchmt, VendorOrderEmailAttachmentViewModel>()
                .ForMember(d => d.CreatByUserFullName, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore());


            CreateMap<Ordr, xNCI2ViewModel>().ReverseMap();
            CreateMap<FsaOrdr, FsaOrder2ViewModel>().ReverseMap();
            CreateMap<NccoOrdr, NccoOrder2ViewModel>().ReverseMap();
            CreateMap<TrptOrdr, TrptOrdrViewModel>().ReverseMap();
            CreateMap<H5Foldr, H5Folder2ViewModel>().ReverseMap();
            CreateMap<H5Doc, H5Doc2ViewModel>()
                .ForMember(a => a.CreatByUserAdid, opt => opt.MapFrom(s => s.CreatByUser.UserAdid))
                .ForMember(a => a.Base64string, opt => opt.MapFrom(s => Convert.ToBase64String(s.FileCntnt)))
                .ReverseMap();
            CreateMap<OrdrStdiHist, OrderStdiHistory2ViewModel>()
                .ForMember(d => d.StdiReasDes, opt => opt.MapFrom(s => s.StdiReas.StdiReasDes))
                .ForMember(d => d.CreatByUserFullNme, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ForMember(d => d.Ccd, opt => opt.MapFrom(s =>(s.Ordr.OrdrCatId == 4) ? s.Ordr.NccoOrdr.CcsDt :
                    (s.Ordr.OrdrCatId == 2 || s.Ordr.OrdrCatId == 3 || s.Ordr.OrdrCatId == 6) ? s.Ordr.FsaOrdr.CustCmmtDt : null))
                .ReverseMap();
            CreateMap<OrdrNte, OrderNote2ViewModel>()
                .ForMember(d => d.creatByUserFullName, opt => opt.MapFrom(s => s.CreatByUser.FullNme))
                .ForMember(d => d.NteTypeDes, opt => opt.MapFrom(s => s.NteType.NteTypeDes))
                .ReverseMap();
            CreateMap<Ckt, Circuit2ViewModel>().ReverseMap();
            CreateMap<CktMs, CircuitMs2ViewModel>().ReverseMap();

            CreateMap<Asr, AsrViewModel>().ReverseMap();
            CreateMap<LkIpMstr, IpMstrViewModel>()
                .ForMember(d => d.RecStusDes, opt => opt.MapFrom(s => s.RecStus.StusDes))
                .ReverseMap();

            CreateMap<LkCustCntrcLgth, CustContractLengthViewModel>().ReverseMap();

            CreateMap<CcdHist, CCDHistoryViewModel>()
                .ForMember(a => a.CCDNew, b => b.MapFrom(c => c.NewCcdDt))
                .ForMember(a => a.CCDOld, b => b.MapFrom(c => c.OldCcdDt))
                .ForMember(a => a.Note, b => b.MapFrom(c => c.Nte != null ? c.Nte.NteTxt : string.Empty))
                .ForMember(a => a.CreatedByDspNme, b => b.MapFrom(c => c.CreatByUser.FullNme))
                .ForMember(a => a.CreatedDateTime, b => b.MapFrom(c => c.CreatByUser.CreatDt))
                .ForMember(a => a.ReasonPipeSV, b => b.MapFrom(c => c.CcdHistReas != null && c.CcdHistReas.Count() > 0
                    ? c.CcdHistReas.OrderByDescending(i => i.CcdHistId). FirstOrDefault().CcdMissdReas.CcdMissdReasDes : string.Empty))
                .ReverseMap();

            CreateMap<Ordr, OrderDetailViewModel>()
                .ForMember(a => a.OrderId, b => b.MapFrom(c => c.OrdrId))
                .ForMember(a => a.Ftn, b => b.MapFrom(c => c.FsaOrdr.Ftn))
                .ForMember(a => a.RelatedFtn, b => b.MapFrom(c => c.FsaOrdr.ReltdFtn))
                .ForMember(a => a.ParentFtn, b => b.MapFrom(c => c.FsaOrdr.PrntFtn))
                .ForMember(a => a.OrderSubTypeCd, b => b.MapFrom(c => c.FsaOrdr.OrdrSubTypeCd))
                .ForMember(a => a.FsaOrderTypeDesc, b => b.MapFrom(c => c.FsaOrdr.OrdrTypeCdNavigation.FsaOrdrTypeDes))
                .ForMember(a => a.OrderTypeDesc, b => b.MapFrom(c => c.Pprt.OrdrType.OrdrTypeDes))
                .ForMember(a => a.ProductTypeDesc, b => b.MapFrom(c => c.Pprt.ProdType.ProdTypeDes))
                .ForMember(a => a.Ccd, b => b.MapFrom(c => c.CustCmmtDt))
                .ForMember(a => a.FsaCcd, b => b.MapFrom(c => c.FsaOrdr.CustCmmtDt))
                .ForMember(a => a.H5FolderId, b => b.MapFrom(c => c.H5FoldrId))
                .ForMember(a => a.OrderCategoryId, b => b.MapFrom(c => c.OrdrCatId))
                .ForMember(a => a.RegionId, b => b.MapFrom(c => c.RgnId))
                .ForMember(a => a.PlatformCd, b => b.MapFrom(c => c.PltfrmCd))
                .ForMember(a => a.OrderStatusId, b => b.MapFrom(c => c.OrdrStusId))
                .ForMember(a => a.CsgLvlId, b => b.MapFrom(c => c.CsgLvlId))
                .ReverseMap()
                .ForMember(d => d.Pprt, opt => opt.Ignore())
                .ForMember(d => d.H5Foldr, opt => opt.Ignore());

            CreateMap<CPEOrdrUpdInfo, CPEOrdrUpdInfoView>()
                .ForMember(a => a.CPELineItemID, b => b.MapFrom(c => c.CPELineItemID))
                .ReverseMap();
            CreateMap<Ckt, CircuitCostViewModel>()
                .ForMember(a => a.VndrCktId, b => b.MapFrom(c => c.VndrCktId))
                .ForMember(a => a.BndlCd, b => b.MapFrom(c => c.CktCost.FirstOrDefault().BndlCd))
                .ForMember(a => a.QotNbr, b => b.MapFrom(c => c.CktCost.FirstOrDefault().QotNbr))
                .ForMember(a => a.VndrOrdrNbr, b => b.MapFrom(c => c.CktCost.FirstOrDefault().VndrOrdrNbr))
                .ForMember(a => a.VndrMrcAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().VndrMrcAmt))
                .ForMember(a => a.AsrMrcAty, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AsrMrcAty))
                .ForMember(a => a.AccsCustMrcAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AccsCustMrcAmt))
                .ForMember(a => a.VndrNrcAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().VndrNrcAmt))
                .ForMember(a => a.AsrNrcAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AsrNrcAmt))
                .ForMember(a => a.AccsCustNrcAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AccsCustNrcAmt))
                .ForMember(a => a.TaxRtPctQty, b => b.MapFrom(c => c.CktCost.FirstOrDefault().TaxRtPctQty))
                .ForMember(a => a.VndrCurId, b => b.MapFrom(c => c.CktCost.FirstOrDefault().VndrCurId))
                .ForMember(a => a.AsrCurId, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AsrCurId))
                .ForMember(a => a.AccsCustCurId, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AccsCustCurId))
                .ForMember(a => a.VndrMrcInUsdAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().VndrMrcInUsdAmt))
                .ForMember(a => a.AsrMrcInUsdAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AsrMrcInUsdAmt))
                .ForMember(a => a.AccsCustMrcInUsdAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AccsCustMrcInUsdAmt))
                .ForMember(a => a.VndrNrcInUsdAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().VndrNrcInUsdAmt))
                .ForMember(a => a.AsrNrcInUsdAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AsrNrcInUsdAmt))
                .ForMember(a => a.AccsCustNrcInUsdAmt, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AccsCustNrcInUsdAmt))
                .ForMember(a => a.VndrCurNme, b => b.MapFrom(c => c.CktCost.FirstOrDefault().VndrCur.CurNme))
                .ForMember(a => a.AsrCurNme, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AsrCur.CurNme))
                .ForMember(a => a.AccsCustCurNme, b => b.MapFrom(c => c.CktCost.FirstOrDefault().AccsCustCur.CurNme));


            CreateMap<MdsEvent, MdsEventView>()
               .ForMember(d => d.NetworkCustomer, opt => opt.MapFrom(s => s.Event.MdsEventNtwkCust))
               .ForMember(d => d.NetworkTransport, opt => opt.MapFrom(s => s.Event.MdsEventNtwkTrpt))
               //.ForMember(d => d.NetworkTransport, opt => opt.MapFrom(s => s.Event.NidActy))
               .ForMember(d => d.MdsRedesignDevInfo, opt => opt.MapFrom(s => s.Event.MdsEventOdieDev))
               .ForMember(d => d.EventDiscoDev, opt => opt.MapFrom(s => s.Event.EventDiscoDev))
               .ForMember(d => d.DevCompletion, opt => opt.MapFrom(s => s.Event.EventDevCmplt))
               .ForMember(d => d.CpeDevice, opt => opt.MapFrom(s => s.Event.EventCpeDev))
               .ForMember(d => d.MnsOrder, opt => opt.MapFrom(s => s.Event.EventDevSrvcMgmt))
               .ForMember(d => d.SiteService, opt => opt.MapFrom(s => s.Event.MdsEventSiteSrvc))
               .ForMember(d => d.ThirdParty, opt => opt.MapFrom(s => s.Event.MdsEventDslSbicCustTrpt))
               .ForMember(d => d.WiredTransport, opt => opt.MapFrom(s => s.Event.MdsEventSlnkWiredTrpt))
               .ForMember(d => d.WirelessTransport, opt => opt.MapFrom(s => s.Event.MdsEventWrlsTrpt))
               .ForMember(d => d.PortBandwidth, opt => opt.MapFrom(s => s.Event.MdsEventPortBndwd))
               .ForMember(d => d.MdsEventNtwkActy, opt => opt.MapFrom(s => s.Event.MdsEventNtwkActy))
               .ForPath(d => d.MdsEventNtwkActyIds, opt => opt.MapFrom(s => (s.Event.MdsEventNtwkActy == null)
                                   ? null : s.Event.MdsEventNtwkActy.Select(i => i.NtwkActyTypeId)))
               .ForMember(d => d.MplsEventActyType, opt => opt.MapFrom(s => s.Event.MplsEventActyType))
               .ForPath(d => d.MplsEventActyTypeIds, opt => opt.MapFrom(s => (s.Event.MplsEventActyType == null)
                                   ? null : s.Event.MplsEventActyType.Select(i => i.MplsActyTypeId)))
               .ForMember(d => d.MdsEventMacActy, opt => opt.MapFrom(s => s.Event.MdsEventMacActy))
               .ForPath(d => d.MdsEventMacActyIds, opt => opt.MapFrom(s => (s.Event.MdsEventMacActy == null)
                                   ? null : s.Event.MdsEventMacActy.Select(i => i.MdsMacActyId)))
               .ReverseMap();
            CreateMap<MdsEventNtwkCust, MdsEventNtwkCustView>().ReverseMap();
            CreateMap<MdsEventNtwkTrpt, MdsEventNtwkTrptView>().ReverseMap();
            CreateMap<MdsEventOdieDev, MdsEventOdieDevView>().ReverseMap();
            CreateMap<EventDiscoDev, MdsEventDiscOdieDevView>().ReverseMap();
            CreateMap<EventDevCmplt, EventDeviceCompletionView>().ReverseMap();
            CreateMap<EventCpeDev, MdsEventCpeDevView>().ReverseMap();
            CreateMap<EventDevSrvcMgmt, MdsEventDevSrvcMgmtView>()
                .ReverseMap()
                .ForMember(d => d.MnsSrvcTier, opt => opt.Ignore());
            CreateMap<MdsEventSiteSrvc, MdsEventSiteSrvcView>().ReverseMap();
            CreateMap<MdsEventDslSbicCustTrpt, MDSEventDslSbicCustTrptView>().ReverseMap();
            CreateMap<MdsEventSlnkWiredTrpt, MDSEventSlnkWiredTrptView>().ReverseMap();
            CreateMap<MdsEventWrlsTrpt, MDSEventWrlsTrptView>().ReverseMap();
            CreateMap<MdsEventPortBndwd, MDSEventPortBndwdView>().ReverseMap();
            CreateMap<MdsEventNtwkActy, MdsEventNtwkActyView>().ReverseMap();
            CreateMap<MplsEventActyType, MplsActivityTypeView>().ReverseMap();
            CreateMap<LkRole, RoleViewModel>()
                .ForMember(d => d.DisplayText, opt => opt.MapFrom(s => string.IsNullOrWhiteSpace(s.RoleCd) ? s.RoleNme : string.Format("{0} - {1}", s.RoleCd, s.RoleNme)))
                .ReverseMap();
            CreateMap<CntctDetl, ContactDetailViewModel>()
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.Role, opt => opt.Ignore())
                .ForMember(d => d.RecStus, opt => opt.Ignore());
            CreateMap<CntctDetl, ContactDetailView>()
                .ReverseMap()
                .ForMember(d => d.CreatByUser, opt => opt.Ignore())
                .ForMember(d => d.ModfdByUser, opt => opt.Ignore())
                .ForMember(d => d.Role, opt => opt.Ignore())
                .ForMember(d => d.RecStus, opt => opt.Ignore());
        }
    }
}
