﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace COWS.Web.Library.Extensions
{
    public static class CacheManager
    {
        //private static readonly IConfiguration _configuration;

        //static CacheManager(IConfiguration configuration)
        //{
        //    _configuration = configuration;
        //}

        public static void Set(this IMemoryCache _cache, string skey, object obj, int ts)
        {
            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(System.TimeSpan.FromSeconds(ts));
            // Save data in cache.
            _cache.Set(skey, obj, cacheEntryOptions);
        }

        public static void Set(this IMemoryCache _cache, string skey, object obj)
        {
            //if (_configuration.GetSection("AppSettings:AbsCacheTime").Exists())
            //    AbsCacheTime = int.Parse(_configuration.GetSection("AppSettings:AbsCacheTime").Value);
            //if (_configuration.GetSection("AppSettings:SldCacheTime").Exists())
            //    SldCacheTime = int.Parse(_configuration.GetSection("AppSettings:SldCacheTime").Value);
            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(System.DateTimeOffset.Now.AddSeconds(3600)); //Save for 1 hour
            // Save data in cache.
            _cache.Set(skey, obj, cacheEntryOptions);
        }
    }
}