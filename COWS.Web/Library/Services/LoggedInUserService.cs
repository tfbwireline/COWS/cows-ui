﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Library.Services
{
    public class LoggedInUserService : ILoggedInUserService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<LoggedInUserService> _logger;
        private readonly IUserRepository _repo;
        private readonly IHttpContextAccessor _http;

        public LoggedInUserService(IMapper mapper,
            ILogger<LoggedInUserService> logger,
            IUserRepository repo,
            IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _logger = logger;
            _repo = repo;
            _http = httpContextAccessor;
        }

        public LogInUserViewModel GetLoggedInUser(bool addLog = false)
        {
            var user = _http.HttpContext.User.Identity.Name;
            if (user == null)
            {
                user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }
            user = user.ToLower();
            if(addLog)
            {
                _logger.LogInformation($"Current User : {user}");
            }
           

            user = user.Replace("ad\\", "").Trim();
            user = user.Replace("gsm1900\\", "").Trim();
            return _mapper.Map<LogInUserViewModel>(_repo.Find(s => s.UserAdid.ToLower() == user).SingleOrDefault());
        }

        public int GetLoggedInUserId()
        {
            var userID = _http.HttpContext.Session.GetInt32("_loggedInUserID");
            if (userID == null)
            {
                var user = _http.HttpContext.User.Identity.Name;
                if (user == null)
                {
                    user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                }
                user = user.ToLower();
                _logger.LogInformation($"Current User : {user}");

                user = user.Replace("ad\\", "").Trim();
                user = user.Replace("gsm1900\\", "").Trim();
                userID = _repo.Find(s => s.UserAdid.ToLower() == user).SingleOrDefault().UserId;
                _http.HttpContext.Session.SetInt32("_loggedInUserID", (int)userID);
                return _repo.Find(s => s.UserAdid.ToLower() == user).SingleOrDefault().UserId;
            } else
            {
                return (int)userID;
            }
        }

        public string GetLoggedInUserAdid()
        {
            var user = _http.HttpContext.User.Identity.Name;
            if (user == null)
            {
                user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }
            user = user.ToLower();
            _logger.LogInformation($"Current User : {user}");

            user = user.Replace("ad\\", "").Trim();
            user = user.Replace("gsm1900\\", "").Trim();
            return _repo.Find(s => s.UserAdid.ToLower() == user).SingleOrDefault().UserAdid;
        }

        public byte GetLoggedInUserCsgLvlId()
        {
            int userId = GetLoggedInUserId();

            return _repo.GetCsgLevel(userId);
        }
    }
}