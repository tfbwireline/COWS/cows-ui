﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.ViewModels;
using System.Collections.Generic;

namespace COWS.Web.Library.Services
{
    public interface IL2PInterfaceService
    {
        bool IsUserAuthorizedToWorkOnH1(string h1);
        byte GetH1CsgLevelId(string h1);
        bool IsUserAuthorizedToWorkOnM5(string m5);
        //byte GetM5CsgLevelId(string m5);
        byte GetM5CsgLevelId(string h1, string h6, string m5);
        List<RetrieveCustomerByH1> SearchByH1(string h1);
        bool VerifyL2PSecurityWithUserSecurity(List<RetrieveCustomerByH1> _l2pDB, List<UserCsgLvl> _usgDB);
        bool chkM5Validation(string h1, string h6, string m5OrdrNbr);
        List<UserCsgLevelViewModel> GetUserSecurityGroup();
    }
}