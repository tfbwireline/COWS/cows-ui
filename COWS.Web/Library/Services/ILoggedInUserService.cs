﻿using COWS.Web.ViewModels;

namespace COWS.Web.Library.Services
{
    public interface ILoggedInUserService
    {
        int GetLoggedInUserId();

        string GetLoggedInUserAdid();

        LogInUserViewModel GetLoggedInUser(bool addLog = false);

        byte GetLoggedInUserCsgLvlId();
    }
}