﻿using System.Collections;
using System.Data;

namespace COWS.Web.Library.Services
{
    public interface ILDAPUserManagementService
    {
        void setConfig();

        Hashtable GetUserFromLDAP(string sUserName, string sUserDom);

        DataTable GetPickerUsersFromLDAP(string sSearchString, string sUserDom);
    }
}