﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Data;
using System.DirectoryServices;
using System.Text;

namespace COWS.Web.Library.Services
{
    public class LDAPUserManagementService : ILDAPUserManagementService
    {
        private readonly IConfiguration _configuration;

        #region "Data Members"

        public string _rootPath = string.Empty;
        public string _userName = string.Empty;
        public string _password = string.Empty;

        public static string _LDAPXmlPath = string.Empty;

        #endregion "Data Members"

        #region "Constructor"

        public LDAPUserManagementService(IConfiguration configuration)
        {
            _configuration = configuration;
            setConfig();
        }

        #endregion "Constructor"

        #region "public methods"

        public void setConfig()
        {
            try
            {
                _rootPath = _configuration.GetSection("LDAPConfigSettings:rootPath").Value;
                _userName = _configuration.GetSection("LDAPConfigSettings:userName").Value;
                _password = _configuration.GetSection("LDAPConfigSettings:password").Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Hashtable GetUserFromLDAP(string sUserName, string sUserDom)
        {
            Hashtable ht = new Hashtable();

            DirectoryEntry _root = null;
            DirectorySearcher _searcher = null;

            try
            {
                _root = new DirectoryEntry(_rootPath);
                _root.Username = _userName;
                _root.Password = _password;
                _root.AuthenticationType = AuthenticationTypes.ServerBind;

                _searcher = new DirectorySearcher(_root);

                //_searcher.Filter = "(" + "zcAnyAttribute" + "=" + sUserName + ")";
                //_searcher.Filter = "(" + "sAMAccountName" + "=" + sSearchString + ")";
                StringBuilder sb = new StringBuilder();
                sb.Append("(|(sAMAccountName=");
                sb.Append(sUserName);
                sb.Append(")(mail=");
                sb.Append(sUserName);
                sb.Append("))");

                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, sb.ToString(), string.Empty);
                _searcher.Filter = sb.ToString();
                _searcher.PropertiesToLoad.Add("sn");
                _searcher.PropertiesToLoad.Add("mail");
                _searcher.PropertiesToLoad.Add("fonNickName");
                _searcher.PropertiesToLoad.Add("givenName");
                _searcher.PropertiesToLoad.Add("telephonenumber");
                _searcher.PropertiesToLoad.Add("displayName");
                _searcher.PropertiesToLoad.Add("cn");
                _searcher.PropertiesToLoad.Add("sAMAccountName");
                _searcher.PropertiesToLoad.Add("middleName");
                _searcher.PropertiesToLoad.Add("initials");
                _searcher.PropertiesToLoad.Add("st");
                _searcher.PropertiesToLoad.Add("l");

                //_searcher.PropertiesToLoad.Add("uid");
                //_searcher.PropertiesToLoad.Add("fonUnixID");
                //_searcher.PropertiesToLoad.Add("ADsPath");
                //_searcher.PropertiesToLoad.Add("fonacf2id");
                //_searcher.PropertiesToLoad.Add("street");
                //_searcher.PropertiesToLoad.Add("postalcode");
                //_searcher.PropertiesToLoad.Add("roomnumber");

                Hashtable htResult = new Hashtable();

                SearchResult searchResult = null;

                try
                {
                    searchResult = _searcher.FindOne();
                }
                catch (Exception ex)
                {
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                    string str = ex.Message;
                }

                if (searchResult != null)
                {
                    foreach (string props in _searcher.PropertiesToLoad)
                    {
                        try
                        {
                            string sTemp = (string)searchResult.Properties[props][0];
                            htResult[props] = sTemp;

                            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, sTemp, string.Empty);
                        }
                        catch (Exception)
                        {
                            //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                            try
                            {
                                htResult[props] = System.Text.Encoding.UTF7.GetString((byte[])searchResult.Properties[props][0]);
                            }
                            catch
                            {
                                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                                htResult[props] = string.Empty;
                            }
                        }
                    }

                    searchResult = null;

                    if (htResult.Count > 0)
                    {
                        //ht["USERNAME"] = sUserName;
                        //ht["DOMAIN_NAME"] = sUserDom;

                        if (!object.ReferenceEquals(htResult["fonNickName"], string.Empty))
                        {
                            ht["FIRST_NAME"] = htResult["fonNickName"];
                        }
                        else
                        {
                            ht["FIRST_NAME"] = htResult["givenName"];
                        }
                        ht["MIDDLE_NAME"] = (htResult["middleName"] == null) ? ((htResult["initials"] == null) ? string.Empty : htResult["initials"]) : htResult["middleName"];
                        ht["LAST_NAME"] = htResult["sn"];
                        ht["EMAIL"] = htResult["mail"];

                        //ht["LDAP_ID"] = htResult["fonUnixID"];
                        //ht["ACF2_ID"] = htResult["fonacf2id"];
                        //ht["STREET"] = htResult["street"];
                        //ht["CITY"] = htResult["l"];
                        //ht["STATE"] = htResult["st"];
                        //ht["ZIP"] = htResult["postalcode"];
                        //ht["CUBE"] = htResult["roomnumber"];
                        ht["DISPLAYNAME"] = htResult["displayName"];
                        ht["PHONE"] = htResult["telephonenumber"];
                        ht["ADID"] = htResult["sAMAccountName"];
                        ht["STT_CD"] = htResult["st"];
                        ht["CTY_NME"] = htResult["l"];
                    }

                    _root.Close();
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("sUserName", sUserName);

                //info.Add("sUserDom", sUserDom);

                //ExceptionManager.Publish(ex, info)
            }

            _root = null;
            _searcher = null;

            return ht;
        }

        public DataTable GetPickerUsersFromLDAP(string sSearchString, string sUserDom)
        {
            Hashtable ht = new Hashtable();
            DataTable dt = new DataTable();

            DirectoryEntry _root = null;
            DirectorySearcher _searcher = null;

            try
            {
                _root = new DirectoryEntry(_rootPath);
                _root.Username = _userName;
                _root.Password = _password;
                _root.AuthenticationType = AuthenticationTypes.ServerBind;

                _searcher = new DirectorySearcher(_root);

                string FName = string.Empty;
                string LName = string.Empty;
                StringBuilder sb = new StringBuilder();

                if (sSearchString.Contains(","))
                {
                    FName = sSearchString.Split(new Char[] { ',' })[1].Trim();
                    LName = sSearchString.Split(new Char[] { ',' })[0].Trim();
                    sb.Append("(&(|(givenName=");
                    sb.Append(FName + "*");
                    sb.Append(")(fonNickName=");
                    sb.Append(FName + "*");
                    sb.Append("))(sn=");
                    sb.Append(LName);
                    sb.Append("))");
                }
                else
                {
                    //_searcher.Filter = "(" + "sAMAccountName" + "=" + sSearchString + ")";

                    sb.Append("(|(sAMAccountName=");
                    sb.Append(sSearchString);
                    sb.Append(")(givenName=");
                    sb.Append(sSearchString);
                    sb.Append(")(fonNickName=");
                    sb.Append(sSearchString);
                    sb.Append(")(sn=");
                    sb.Append(sSearchString);
                    sb.Append(")(mail=");
                    sb.Append(sSearchString);
                    sb.Append("))");
                }

                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, sb.ToString(), string.Empty);
                _searcher.Filter = sb.ToString();
                _searcher.PropertiesToLoad.Add("sn");
                _searcher.PropertiesToLoad.Add("mail");
                _searcher.PropertiesToLoad.Add("fonNickName");
                _searcher.PropertiesToLoad.Add("givenName");
                _searcher.PropertiesToLoad.Add("telephonenumber");
                _searcher.PropertiesToLoad.Add("displayName");
                _searcher.PropertiesToLoad.Add("cn");
                _searcher.PropertiesToLoad.Add("sAMAccountName");
                _searcher.PropertiesToLoad.Add("middleName");
                _searcher.PropertiesToLoad.Add("initials");

                //_searcher.PropertiesToLoad.Add("uid");
                //_searcher.PropertiesToLoad.Add("fonUnixID");
                //_searcher.PropertiesToLoad.Add("ADsPath");
                //_searcher.PropertiesToLoad.Add("fonacf2id");
                //_searcher.PropertiesToLoad.Add("street");
                //_searcher.PropertiesToLoad.Add("postalcode");
                //_searcher.PropertiesToLoad.Add("l");
                //_searcher.PropertiesToLoad.Add("st");
                //_searcher.PropertiesToLoad.Add("roomnumber");

                Hashtable htResult = new Hashtable();

                SearchResultCollection searchResultColl = null;

                try
                {
                    searchResultColl = _searcher.FindAll();
                }
                catch (Exception ex)
                {
                    //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                    string str = ex.Message;
                }

                if (searchResultColl != null)
                {
                    dt.Columns.Add(new DataColumn("FIRSTNAME"));
                    dt.Columns.Add(new DataColumn("LASTNAME"));
                    dt.Columns.Add(new DataColumn("MIDDLENAME"));
                    dt.Columns.Add(new DataColumn("EMAIL"));
                    dt.Columns.Add(new DataColumn("PHONE"));
                    dt.Columns.Add(new DataColumn("DISPLAYNAME"));
                    dt.Columns.Add(new DataColumn("ADID"));

                    foreach (SearchResult searchResult in searchResultColl)
                    {
                        foreach (string props in _searcher.PropertiesToLoad)
                        {
                            try
                            {
                                string sTemp = (string)searchResult.Properties[props][0];
                                htResult[props] = sTemp;

                                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.text, 0, string.Empty, sTemp, string.Empty);
                            }
                            catch (Exception)
                            {
                                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                                try
                                {
                                    htResult[props] = System.Text.Encoding.UTF7.GetString((byte[])searchResult.Properties[props][0]);
                                }
                                catch
                                {
                                    htResult[props] = string.Empty;
                                }
                            }
                        }

                        //searchResult = null;

                        if ((htResult.Count > 0) && (htResult["sAMAccountName"].ToString().Length > 0))
                        {
                            //ht["USERNAME"] = sUserName;
                            //ht["DOMAIN_NAME"] = sUserDom;
                            DataRow dr = dt.NewRow();

                            if (!object.ReferenceEquals(htResult["fonNickName"], string.Empty))
                            {
                                dr["FIRSTNAME"] = htResult["fonNickName"];
                            }
                            else
                            {
                                dr["FIRSTNAME"] = htResult["givenName"];
                            }
                            dr["MIDDLENAME"] = (htResult["middleName"] == null) ? ((htResult["initials"] == null) ? string.Empty : htResult["initials"]) : htResult["middleName"];
                            dr["LASTNAME"] = htResult["sn"];
                            dr["EMAIL"] = htResult["mail"];

                            //ht["LDAP_ID"] = htResult["fonUnixID"];
                            //ht["ACF2_ID"] = htResult["fonacf2id"];
                            //ht["STREET"] = htResult["street"];
                            //ht["CITY"] = htResult["l"];
                            //ht["STATE"] = htResult["st"];
                            //ht["ZIP"] = htResult["postalcode"];
                            //ht["CUBE"] = htResult["roomnumber"];
                            dr["DISPLAYNAME"] = htResult["displayName"];
                            dr["PHONE"] = htResult["telephonenumber"];
                            dr["ADID"] = htResult["sAMAccountName"];
                            dt.Rows.Add(dr);
                        }
                    }

                    _root.Close();
                }
            }
            catch (Exception)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.StackTrace + " : " + ex.Message, string.Empty);
                System.Collections.Specialized.NameValueCollection info = new System.Collections.Specialized.NameValueCollection();
                info.Add("sUserName", sSearchString);
            }

            _root = null;
            _searcher = null;

            return dt;
        }

        #endregion "public methods"
    }
}