﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Controllers;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

namespace COWS.Web.Library.Services
{
    public class L2PInterfaceService : IL2PInterfaceService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<L2PInterfaceService> _logger;
        private readonly IL2PInterfaceRepository _repo;
        private readonly IUserCsgLevelRepository _repoCsgLvl;
        private readonly IFsaOrderRepository _repoFsaOrder;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IConfiguration _config;

        public L2PInterfaceService(IMapper mapper,
            ILogger<L2PInterfaceService> logger,
            IL2PInterfaceRepository repo,
            IUserCsgLevelRepository repoCsgLvl,
            IFsaOrderRepository repoFsaOrder,
            ILoggedInUserService loggedInUser,
            IMemoryCache cache,
            IConfiguration config)
        {
            _mapper = mapper;
            _logger = logger;
            _repo = repo;
            _repoCsgLvl = repoCsgLvl;
            _repoFsaOrder = repoFsaOrder;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _config = config;
        }

        public bool IsUserAuthorizedToWorkOnH1(string h1)
        {
            _logger.LogInformation($"Validate if user is authorized to work on H1: { h1 }.");

            var isAuthorized = true;

            // SearchByH1
            var list = _mapper.Map<IEnumerable<RetrieveCustomerByH1ViewModel>>(_repo
                                                                .RetrieveCustomerByH1(h1));

            if (list != null && list.Count() > 0)
            {
                // GetUserSecurityGroup
                //var loggedInUser = _loggedInUser.GetLoggedInUser();
                //IEnumerable<UserCsgLevelViewModel> listUserCsg = null;
                //if (!_cache.TryGetValue(CacheKeys.UserSecurityGroup, out listUserCsg))
                //{
                //    listUserCsg = _mapper.Map<IEnumerable<UserCsgLevelViewModel>>(_repoCsgLvl
                //                                                    .Find(i => i.UserId == loggedInUser.UserId));
                //    CacheManager.Set(_cache, CacheKeys.UserSecurityGroup, listUserCsg);
                //}
                List<UserCsgLevelViewModel> listUserCsg = GetUserSecurityGroup();

                // VerifyL2PSecurityWithUserSecurity
                // If User doesn't exist in USER_CSG_LVL table, status = UnAuthorized
                if (listUserCsg == null || listUserCsg.Count() <= 0)
                {
                    return !isAuthorized;
                }

                foreach (RetrieveCustomerByH1ViewModel item in list)
                {
                    // H1 is secured
                    // User is authorized as long as user CSGLevel != 0 and <= H1 CSGLevel
                    int tempCsgLvl = 0;
                    bool isNum = Int32.TryParse(item.SCTY_GRP_CD, out tempCsgLvl);
                    if (!isNum || string.IsNullOrWhiteSpace(item.SCTY_GRP_CD) ||
                        listUserCsg.FirstOrDefault(i => i.CsgLvlId != 0 && i.CsgLvlId <= tempCsgLvl) == null)
                    {
                        return !isAuthorized;
                    }
                }
            }

            return isAuthorized;
        }

        public byte GetH1CsgLevelId(string h1)
        {
            var list = _mapper.Map<IEnumerable<RetrieveCustomerByH1ViewModel>>(_repo
                                                                .RetrieveCustomerByH1(h1));
            if (list != null && list.Count() > 0)
            {
                return Byte.Parse(list.FirstOrDefault().SCTY_GRP_CD);
            }

            return 0;
        }

        public bool IsUserAuthorizedToWorkOnM5(string m5)
        {
            _logger.LogInformation($"Validate if user is authorized to work on M5: { m5 }.");

            var isAuthorized = true;

            // GetOrderSecurity
            var ordr = _mapper.Map<FsaOrderViewModel>(_repoFsaOrder.Find(i => i.Ftn == m5).SingleOrDefault());
            if (ordr != null && ordr.OrdrCsgLvlId > 0)
            {
                // GetUserSecurityGroup
                //var loggedInUser = _loggedInUser.GetLoggedInUser();
                //IEnumerable<UserCsgLevelViewModel> listUserCsg = null;
                //if (!_cache.TryGetValue(CacheKeys.UserSecurityGroup, out listUserCsg))
                //{
                //    listUserCsg = _mapper.Map<IEnumerable<UserCsgLevelViewModel>>(_repoCsgLvl
                //                                                    .Find(i => i.UserId == loggedInUser.UserId));
                //    CacheManager.Set(_cache, CacheKeys.UserSecurityGroup, listUserCsg);
                //}
                List<UserCsgLevelViewModel> listUserCsg = GetUserSecurityGroup();

                if (listUserCsg.FirstOrDefault(i => i.CsgLvlId != 0 && i.CsgLvlId <= ordr.OrdrCsgLvlId) == null)
                {
                    return !isAuthorized;
                }
            }

            return isAuthorized;
        }

        public byte GetM5CsgLevelId(string h1, string h6, string m5)
        {
            List<byte> csgLevel = new List<byte>();

            SqlConnection connection = new SqlConnection(_config.GetConnectionString("SqlDbConn"));
            SqlCommand command = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            command = new SqlCommand("web.getM5SecrdValidation", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@H1", h1);
            command.Parameters.AddWithValue("@H6", h6);
            command.Parameters.AddWithValue("@M5OrdrNbr", m5);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            connection.Close();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    csgLevel.Add(byte.Parse(row["CSG_LVL"].ToString()));
                }
            }

            return csgLevel == null ? (byte)0 :
                csgLevel.Count == 0 ? (byte)0 : csgLevel.FirstOrDefault();

            //var ordr = _mapper.Map<FsaOrderViewModel>(_repoFsaOrder.Find(i => i.Ftn == m5).SingleOrDefault());
            //if (ordr != null)
            //{
            //    return ordr.OrdrCsgLvlId;
            //}

            //return 0;
        }

        public List<RetrieveCustomerByH1> SearchByH1(string h1)
        {
            var _l2pDB = _repo.RetrieveCustomerByH1(h1).ToList();
            if (_l2pDB == null || _l2pDB.Count == 0)
                _l2pDB = null;

            return _l2pDB;
        }

        //public void AddL2PDatatoCacheObject(List<RetrieveCustomerByH1> _l2pDB)
        //{
        //    lock (this)
        //    {
        //        if (_l2pDB.Count() > 0)
        //        {
        //            List<RetrieveCustomerByH1> _l2p = new List<RetrieveCustomerByH1>();

        //            for (int i = 0; i < _l2pDB.Count; i++)
        //            {
        //                _l2p.Add(_l2pDB[i]);
        //            }
        //        }

        //    }
        //}

        public bool VerifyL2PSecurityWithUserSecurity(List<RetrieveCustomerByH1> _l2pDB, List<UserCsgLvl> _usgDB)
        {
            bool bAuthStatus = false;

            //If User doesn't exist in USER_CSG_LVL table, status = UnAuthorized
            if (_usgDB.Count() == 0 || _usgDB == null)
                return bAuthStatus;

            for (int i = 0; i < _l2pDB.Count(); i++)
            {
                string sSCTYGrpCD = _l2pDB[i].SCTY_GRP_CD;
                Regex _isNumber = new Regex(@"^\d+$");

                if (sSCTYGrpCD == string.Empty || (!_isNumber.IsMatch(sSCTYGrpCD)))
                    return bAuthStatus;
                else
                {
                    if (!(_usgDB.Exists(u => (u.CsgLvlId != 0) && (u.CsgLvlId <= Convert.ToByte(sSCTYGrpCD)))))
                        return bAuthStatus;
                }
            }
            bAuthStatus = true;
            return bAuthStatus;
        }

        public bool chkM5Validation(string h1, string h6, string m5OrdrNbr)
        {
            _logger.LogInformation($"Validate if user is authorized to work on H1: { h1 }, H6: { h6 }, M5 Order Nbr: { m5OrdrNbr }.");

            var isAuthorized = true;

            // Get CSGLevel
            var csgLvlList = _repo.GetM5CSGLvl(h1, h6, m5OrdrNbr);

            if (csgLvlList != null && csgLvlList.Count() > 0)
            {
                // GetUserSecurityGroup
                //var loggedInUser = _loggedInUser.GetLoggedInUser();
                //IEnumerable<UserCsgLevelViewModel> listUserCsg = null;
                //if (!_cache.TryGetValue(CacheKeys.UserSecurityGroup, out listUserCsg))
                //{
                //    listUserCsg = _mapper.Map<IEnumerable<UserCsgLevelViewModel>>(_repoCsgLvl
                //                                                    .Find(i => i.UserId == loggedInUser.UserId));
                //    CacheManager.Set(_cache, CacheKeys.UserSecurityGroup, listUserCsg);
                //}
                List<UserCsgLevelViewModel> listUserCsg = GetUserSecurityGroup();

                // If User doesn't exist in USER_CSG_LVL table, status = UnAuthorized
                if (listUserCsg == null || listUserCsg.Count() <= 0)
                {
                    return !isAuthorized;
                }

                var x = (from csg in csgLvlList
                         from usg in listUserCsg
                         where csg >= usg.CsgLvlId
                         && (usg.CsgLvlId != 0)
                         && (csg != 0)
                         select usg);

                if (x == null)
                    return !isAuthorized;
                else
                    return isAuthorized;
            }

            return isAuthorized;
        }

        public List<UserCsgLevelViewModel> GetUserSecurityGroup()
        {
            // GetUserSecurityGroup
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            IEnumerable<UserCsgLevelViewModel> listUserCsg = _mapper.Map<IEnumerable<UserCsgLevelViewModel>>(_repoCsgLvl
                                                                .Find(i => i.UserId == loggedInUser.UserId));
            return listUserCsg.ToList();
        }
    }
}