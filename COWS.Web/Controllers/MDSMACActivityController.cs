﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSMACActivities")]
    [ApiController]
    public class MDSMACActivityController : ControllerBase
    {
        private readonly IMDSMACActivityRepository _repo;
        private readonly ICommonRepository _commonRepo;
        private readonly ILogger<MDSMACActivityController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public MDSMACActivityController(IMapper mapper,
                               IMDSMACActivityRepository repo,
                               ICommonRepository commonRepo,
                               ILogger<MDSMACActivityController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _commonRepo = commonRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MDSMACActivityViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MDSMACActivityViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.MdsMacActyNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MDS MAC Activity by Id: { id }.");

            var obj = _repo.Find(s => s.MdsMacActyId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSMACActivityViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDS MAC Activity by Id: { id } not found.");
                return NotFound(new { Message = $"MDS MAC Activity Id: { id } not found." });
            }
        }

        [HttpGet("GetForLookup")]
        public ActionResult<IEnumerable<MDSMACActivityViewModel>> GetForLookup()
        {
            var list = _mapper.Map<IEnumerable<MDSMACActivityViewModel>>(_repo.GetAll()
                                                                .OrderBy(s => s.MdsMacActyNme));
            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] MDSMACActivityViewModel model)
        {
            _logger.LogInformation($"Create MDS MAC Activity { model.MdsMacActyNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkMdsMacActy>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                    obj.ModfdByUserId = null;
                    obj.ModfdDt = null;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkMdsMacActy();
                var duplicate = _repo.Find(i => i.MdsMacActyNme.Trim().ToLower() == obj.MdsMacActyNme.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.MdsMacActyNme + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.MinDrtnTmeReqrAmt = obj.MinDrtnTmeReqrAmt;
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.MdsMacActyId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    _logger.LogInformation($"MDS MAC Activity Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/MDSMACActivities/{ newData.MdsMacActyId }", model);
                }
            }

            return BadRequest(new { Message = "MDS MAC Activity Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] MDSMACActivityViewModel model)
        {
            _logger.LogInformation($"Update MDS MAC Activity Id: { id }.");

            var obj = _mapper.Map<LkMdsMacActy>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.MdsMacActyNme.Trim().ToLower() == obj.MdsMacActyNme.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active
                    && duplicate.MdsMacActyId != obj.MdsMacActyId)
                {
                    return BadRequest(new { Message = obj.MdsMacActyNme + " already exists." });
                }
                else if (duplicate.RecStusId == (byte)ERecStatus.InActive
                    && duplicate.MdsMacActyId != obj.MdsMacActyId)
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.MdsMacActyId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"MDS MAC Activity Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/MDSMACActivities/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating MDS MAC Activity by Id: { id }.");

            var mds = _repo.Find(s => s.MdsMacActyId == id);
            if (mds != null)
            {
                var obj = _mapper.Map<LkMdsMacActy>(mds.SingleOrDefault());
                obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.InActive;

                _repo.Update(id, obj);

                _logger.LogInformation($"MDS MAC Activity by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to MDS MAC Activity by Id: { id } not found.");
            }
        }

        [HttpGet("SysCfg/{name}")]
        public IActionResult UpdateSysCfgValue([FromRoute] string name, [FromQuery] string value)
        {
            _logger.LogInformation($"Update SysCfg Param Value by Param Name: { name }.");

            try
            {
                var res = _commonRepo.UpdateSysCfgValue(name, value);
                return Ok(res);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Failed to Update SysCfg Param Value by Param Name: { name }.");
                return BadRequest(new { Message = $"Failed to Update SysCfg Param Value by Param Name: { name }." });
            }
        }
    }
}
