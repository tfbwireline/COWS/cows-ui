﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/XnciMs")]
    [ApiController]
    public class XnciMsController : ControllerBase
    {
        private readonly IXnciMsRepository _repo;
        private readonly ILogger<XnciMsController> _logger;
        private readonly IMapper _mapper;

        public XnciMsController(IMapper mapper,
                               IXnciMsRepository repo,
                               ILogger<XnciMsController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<XnciMsViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<XnciMsViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderByDescending(s => s.MsDes)
                                                                .ThenBy(s => s.MsTaskId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search  Xnci Ms by Id: { id }.");

            var obj = _repo.Find(s => s.MsId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<XnciMsViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($" Xnci Ms by  Id: { id } not found.");
                return NotFound(new { Message = $"Xnci Ms Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] XnciMsViewModel model)
        {
            _logger.LogInformation($"Create Xnci Ms with MS Id: { model.MsId }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkXnciMs>(model);
                obj.CreatDt = DateTime.Now;

                var rep = _repo.Create(obj);
                if (rep != null)
                {
                    _logger.LogInformation($"Xnci Ms Created. { JsonConvert.SerializeObject(rep).ToString() } ");
                    return Created($"api/XnciMs/{ rep.MsId }", model);
                }
            }

            return BadRequest(new { Message = "Xnci Ms Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] XnciMsViewModel model)
        {
            _logger.LogInformation($"Update Xnci Ms Id: { id }.");

            var obj = _mapper.Map<LkXnciMs>(model);
            _repo.Update(id, obj);

            _logger.LogInformation($"Xnci Ms Updated. { JsonConvert.SerializeObject(obj).ToString() } ");
            return Created($"api/XnciMs/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Delete Xnci Ms Id: { id }.");
            _repo.Delete(id);
            _logger.LogInformation($"Xnci Ms Id { id } Deleted.");
        }
    }
}