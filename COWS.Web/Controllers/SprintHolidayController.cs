﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/SprintHolidays")]
    [ApiController]
    public class SprintHolidayController : ControllerBase
    {
        private readonly ISprintHolidayRepository _repo;
        private readonly ILogger<SprintHolidayController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private string _includePast;

        public SprintHolidayController(IMapper mapper,
                               ISprintHolidayRepository repo,
                               ILogger<SprintHolidayController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _includePast = "false";
        }

        [HttpGet]
        public ActionResult<IEnumerable<SprintHolidayViewModel>> Get([FromQuery] string includePast)
        {
            IEnumerable<SprintHolidayViewModel> list;
            if (_includePast != includePast) _cache.Remove(CacheKeys.SprintHolidayList);
            if (!_cache.TryGetValue(CacheKeys.SprintHolidayList, out list))
            {
                if ((includePast != null) && includePast == "true")
                {
                    _includePast = "true";
                    list = _mapper.Map<IEnumerable<SprintHolidayViewModel>>(_repo
                                                                    .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                    .OrderByDescending(s => s.SprintHldyDt.Year)
                                                                    .ThenBy(s => s.SprintHldyDt.Month));
                }
                else
                {
                    _includePast = "false";
                    list = _mapper.Map<IEnumerable<SprintHolidayViewModel>>(_repo
                                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active
                                                                                    && s.SprintHldyDt.Year >= DateTime.Now.Year)
                                                                                .OrderByDescending(s => s.SprintHldyDt.Year)
                                                                                .ThenBy(s => s.SprintHldyDt.Month));
                }

                CacheManager.Set(_cache, CacheKeys.SprintHolidayList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Sprint Holiday by Id: { id }.");

            var obj = _repo.Find(s => s.SprintHldyId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<SprintHolidayViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Sprint Holiday by Id: { id } not found.");
                return NotFound(new { Message = $"Sprint Holiday Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] SprintHolidayViewModel model)
        {
            _logger.LogInformation($"Create Sprint Holiday: { model.SprintHldyDes }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkSprintHldy>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkSprintHldy();
                var duplicate = _repo.Find(i => i.SprintHldyDt == obj.SprintHldyDt).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = "The selected date is already active." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.SprintHldyDt = obj.SprintHldyDt;
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.SprintHldyId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    // Update Cache
                    _cache.Remove(CacheKeys.SprintHolidayList);
                    Get(_includePast);

                    _logger.LogInformation($"Sprint Holiday Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/SprintHolidays/{ newData.SprintHldyId }", model);
                }
            }

            return BadRequest(new { Message = "Sprint Holiday Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] SprintHolidayViewModel model)
        {
            _logger.LogInformation($"Update Sprint Holiday Id: { id }.");

            var obj = _mapper.Map<LkSprintHldy>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.SprintHldyDt == obj.SprintHldyDt).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active
                    && duplicate.SprintHldyId != obj.SprintHldyId)
                {
                    return BadRequest(new { Message = "The selected date is already active." });
                }
                else if (duplicate.RecStusId == (byte)ERecStatus.InActive
                    && duplicate.SprintHldyId != obj.SprintHldyId)
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.SprintHldyId);
                }
            }

            _repo.Update(id, obj);

            // Update Cache
            _cache.Remove(CacheKeys.SprintHolidayList);
            Get(_includePast);

            _logger.LogInformation($"Sprint Holiday Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/SprintHolidays/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating Sprint Holiday by Id: { id }.");

            var hday = _repo.Find(s => s.SprintHldyId == id);
            if (hday != null)
            {
                var obj = _mapper.Map<LkSprintHldy>(hday.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);

                // Update Cache
                _cache.Remove(CacheKeys.SprintHolidayList);
                Get(_includePast);
                _logger.LogInformation($"Sprint Holiday by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Sprint Holiday by Id: { id } not found.");
            }
        }
    }
}
