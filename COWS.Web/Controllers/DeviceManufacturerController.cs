﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/DeviceManufacturers")]
    [ApiController]
    public class DeviceManufacturerController : ControllerBase
    {
        private readonly IDeviceManufacturerRepository _repo;
        private readonly ILogger<DeviceManufacturerController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public DeviceManufacturerController(IMapper mapper,
                               IDeviceManufacturerRepository repo,
                               ILogger<DeviceManufacturerController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<DeviceManufacturerViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<DeviceManufacturerViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.ManfNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Device Manufacturer by Id: { id }.");

            var obj = _repo.Find(s => s.ManfId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<DeviceManufacturerViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Device Manufacturer by Id: { id } not found.");
                return NotFound(new { Message = $"Device Manufacturer Id: { id } not found." });
            }
        }

        [HttpGet("GetByName/{name}")]
        public IActionResult GetByName([FromRoute] string name)
        {
            _logger.LogInformation($"Search Device Manufacturer by Name: { name }.");

            var obj = _repo.Find(s => s.ManfNme.ToLower().Trim() == name.ToLower().Trim());
            if (obj != null)
            {
                return Ok(_mapper.Map<DeviceManufacturerViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Device Manufacturer by Name: { name } not found.");
                return NotFound(new { Message = $"Device Manufacturer Name: { name } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] DeviceManufacturerViewModel model)
        {
            _logger.LogInformation($"Create Device Manufacturer: { model.ManfNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkDevManf>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkDevManf();
                var duplicate = _repo.Find(i => i.ManfNme.Trim().ToLower() == obj.ManfNme.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.ManfNme + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.ManfId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    _logger.LogInformation($"Device Manufacturer Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/DeviceManufacturers/{ newData.ManfId }", model);
                }
            }

            return BadRequest(new { Message = "Device Manufacturer Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] DeviceManufacturerViewModel model)
        {
            _logger.LogInformation($"Update Device Manufacturer Id: { id }.");

            var obj = _mapper.Map<LkDevManf>(model);
            obj.RecStusId = (byte)ERecStatus.Active;

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.ManfNme.Trim().ToLower() == obj.ManfNme.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.ManfNme + " already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.ManfId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Device Manufacturer Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/DeviceManufacturers/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating Device Manufacturer by Id: { id }.");

            var manf = _repo.Find(s => s.ManfId == id);
            if (manf != null)
            {
                var obj = _mapper.Map<LkDevManf>(manf.SingleOrDefault());
                obj.RecStusId = (byte)ERecStatus.InActive;
                _repo.Update(id, obj);

                _logger.LogInformation($"Device Manufacturer by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Device Manufacturer by Id: { id } not found.");
            }
        }
    }
}
