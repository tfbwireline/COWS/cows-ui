﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/StdiHistory")]
    [ApiController]
    public class OrderStdiHistoryController : ControllerBase
    {
        private readonly IOrderStdiHistoryRepository _repo;
        private readonly ILogger<OrderStdiHistoryController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public OrderStdiHistoryController(IMapper mapper,
                               IOrderStdiHistoryRepository repo,
                               ILogger<OrderStdiHistoryController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OrderStdiHistoryViewModel>> Get()
        {
            IEnumerable<OrderStdiHistoryViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.OrderTypeList, out list))
            {
                list = _mapper.Map<IEnumerable<OrderStdiHistoryViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrStdiId));

                CacheManager.Set(_cache, CacheKeys.OrderTypeList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order STDI History by Id: { id }.");

            var obj = _repo.Find(s => s.OrdrStdiId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<OrderStdiHistoryViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Order STDI History by Id: { id } not found.");
                return NotFound(new { Message = $"Order STDI History Id: { id } not found." });
            }
        }

        [HttpGet("order/{id}")]
        public IActionResult GetByOrderId([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order STDI History by Order Id: { id }.");

            var obj = _repo.Find(s => s.OrdrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<OrderStdiHistoryViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Order STDI History by Order Id: { id } not found.");
                return NotFound(new { Message = $"Order STDI History Order Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] OrderStdiHistoryViewModel model)
        {
            int userId = _loggedInUser.GetLoggedInUserId();

            if (ModelState.IsValid)
            {
                OrdrStdiHist orderStdi = _mapper.Map<OrdrStdiHist>(model);
                orderStdi.CreatDt = DateTime.Now;
                orderStdi.CreatByUserId = userId;
                orderStdi.ModfdByUserId = userId;

                var stdi = _repo.Create(orderStdi);

                if (stdi != null)
                {
                    _logger.LogInformation($"Order STDI History Created. { JsonConvert.SerializeObject(stdi, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    var json = new JsonSerializerSettings()
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                        Formatting = Formatting.Indented
                    };

                    return Created($"api/StdiHistory/{ stdi.OrdrStdiId}", stdi);
                }
            }

            return BadRequest(new { Message = "Order STDI History Could Not Be Created" });
        }
    }
}