﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/PrimSecondaryTransports")]
    [ApiController]
    public class CptPrimSecondaryTransportController : ControllerBase
    {
        private readonly ICptPrimSecondaryTransportRepository _repo;
        private readonly ILogger<CptPrimSecondaryTransportController> _logger;
        private readonly IMapper _mapper;

        public CptPrimSecondaryTransportController(IMapper mapper,
                               ICptPrimSecondaryTransportRepository repo,
                               ILogger<CptPrimSecondaryTransportController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptPrimSecondaryTransportViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptPrimSecondaryTransportViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptPrimScndyTprtId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT Primary/Secondary Transport Type by Id: { id }.");

            var obj = _repo.Find(s => s.CptPrimScndyTprtId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptPrimSecondaryTransportViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT Primary/Secondary Transport Type by Id: { id } not found.");
                return NotFound(new { Message = $"CPT Primary/Secondary Transport Type Id: { id } not found." });
            }
        }
    }
}