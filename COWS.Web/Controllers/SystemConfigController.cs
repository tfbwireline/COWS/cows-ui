﻿using COWS.Data.Interface;
using AutoMapper;
using COWS.Entities.Models;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Config")]
    [ApiController]
    public class SystemConfigController : ControllerBase
    {
        private readonly ISystemConfigRepository _repo;
        private readonly ILogger<StatusController> _logger;
        private readonly IMapper _mapper;

        public SystemConfigController(IMapper mapper,
                               ISystemConfigRepository repo,
                               ILogger<StatusController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<SystemConfigViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<SystemConfigViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.PrmtrNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search System Config by Id: { id }.");

            var obj = _repo.Find(s => s.CfgId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<SystemConfigViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"System Config by Id: { id } not found.");
                return NotFound(new { Message = $"System Config Id: { id } not found." });
            }
        }

        [HttpGet("{name}")]
        public IActionResult Get([FromRoute] string name)
        {
            _logger.LogInformation($"Search System Config by Name: { name }.");

            var obj = _repo.Find(s => s.PrmtrNme == name);
            if (obj != null)
            {
                return Ok(_mapper.Map<SystemConfigViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"System Config by Name: { name } not found.");
                return NotFound(new { Message = $"System Config Name: { name } not found." });
            }
        }

        [HttpGet("GetByName/{name}")]
        public ActionResult<IEnumerable<SystemConfigViewModel>> GetByName([FromRoute] string name)
        {
            _logger.LogInformation($"Search System Config by Name: { name }.");

            var obj = _repo.Find(s => s.PrmtrNme.Contains(name));
            if (obj != null)
            {
                foreach (LkSysCfg sysCfg in obj)
                {
                    sysCfg.PrmtrNme = sysCfg.PrmtrNme.Replace("^MDSSlotDuration", string.Empty);
                }
                return Ok(_mapper.Map<IEnumerable<SystemConfigViewModel>>(obj.OrderBy(i => i.PrmtrNme)));
            }
            else
            {
                _logger.LogInformation($"System Config by Name: { name } not found.");
                return NotFound(new { Message = $"System Config Name: { name } not found." });
            }
        }

        [HttpGet("UpdateSysCfgValue/{name}")]
        public IActionResult UpdateSysCfgValue([FromRoute] string name, [FromQuery] string value)
        {
            _logger.LogInformation($"Update SysCfg Param Value by Param Name: { name }.");

            try
            {
                var obj = _repo.Find(s => s.PrmtrNme == name);
                if (obj != null)
                {
                    var sysCfg = obj.SingleOrDefault();
                    sysCfg.PrmtrValuTxt = value;
                    _repo.Update(sysCfg.CfgId, sysCfg);
                    return Ok(sysCfg);
                }
                else
                {
                    var sysCfg = new LkSysCfg();
                    sysCfg.PrmtrNme = name;
                    sysCfg.PrmtrValuTxt = value;
                    sysCfg.CreatDt = DateTime.Now;
                    sysCfg.RecStusId = (byte)ERecStatus.Active;
                    var res = _repo.Create(sysCfg);
                    return Ok(res);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Failed to Update SysCfg Param Value by Param Name: { name }.");
                return BadRequest(new { Message = $"Failed to Update SysCfg Param Value by Param Name: { name }." });
            }
        }
    }
}
