﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/FsaOrders")]
    [ApiController]
    public class FsaOrdersController : ControllerBase
    {
        private readonly IFsaOrderRepository _repo;
        private readonly IOrderTypeRepository _orderTypeRepo;
        private readonly IOrderSubTypeRepository _orderSubTypeRepo;
        private readonly IFsaProdTypeRepository _fsaProdTypeRepo;
        private readonly ILogger<FsaOrdersController> _logger;
        private readonly IMapper _mapper;

        public FsaOrdersController(IMapper mapper,
                               IFsaOrderRepository repo,
                               IOrderTypeRepository orderTypeRepo,
                               IOrderSubTypeRepository orderSubTypeRepo,
                               IFsaProdTypeRepository fsaProdTypeRepo,
                               ILogger<FsaOrdersController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _orderTypeRepo = orderTypeRepo;
            _orderSubTypeRepo = orderSubTypeRepo;
            _fsaProdTypeRepo = fsaProdTypeRepo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<FsaOrderViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<FsaOrderViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order by Id: { id }.");

            var obj = _repo.Find(a => a.OrdrId == id);
            if (obj != null)
            {
                IEnumerable<FsaOrderViewModel> res = GetSubDetails(_mapper.Map<IEnumerable<FsaOrderViewModel>>(obj.ToList()));

                return Ok(res.FirstOrDefault());
            }
            else
            {
                _logger.LogInformation($"Order by Id: { id } not found.");
                return NotFound(new { Message = $"Order Id: { id } not found." });
            }
        }

        [HttpGet("related/ftn/{ftn}")]
        public IActionResult GetRelatedOrdersByFtn([FromRoute] string ftn)
        {
            _logger.LogInformation($"Search Related Orders by FTN: { ftn }.");

            var relatedOrder = _repo.Find(a => a.ReltdFtn == ftn);
            //var res = order.Concat(relatedOrder);

            if (relatedOrder != null)
            {
                IEnumerable<FsaOrderViewModel> res = GetSubDetails(_mapper.Map<IEnumerable<FsaOrderViewModel>>(relatedOrder.ToList()));

                return Ok(res);
            }
            else
            {
                _logger.LogInformation($"Related Orders by FTN: { ftn } not found.");
                return NotFound(new { Message = $"Related Orders by FTN: { ftn } not found." });
            }
        }

        [HttpGet("related/{id}")]
        public IActionResult GetRelatedOrdersById([FromRoute] int id)
        {
            _logger.LogInformation($"Search Related Orders by Id: { id }.");

            var fsa = _repo.Find(a => a.OrdrId == id).FirstOrDefault();

            if (fsa != null)
            {
                var relatedOrder = _repo.Find(a => a.ReltdFtn == fsa.Ftn);

                if (relatedOrder != null)
                {
                    IEnumerable<FsaOrderViewModel> res = GetSubDetails(_mapper.Map<IEnumerable<FsaOrderViewModel>>(relatedOrder.ToList()));

                    return Ok(res);
                }
            }

            _logger.LogInformation($"Related Orders by Id: { id } not found.");
            return NotFound(new { Message = $"Related Orders by Id: { id } not found." });
        }

        [HttpGet("transport/{id}")]
        public IActionResult GetTransport([FromRoute] int id, [FromQuery] bool isTerminating)
        {
            _logger.LogInformation($"Search Transport Order Data by Id: { id }.");

            var transportData = _repo.Find(a => a.OrdrId == id);
            //var res = order.Concat(relatedOrder);

            if (transportData != null)
            {
                IEnumerable<FsaOrderViewModel> res = GetSubDetails(_mapper.Map<IEnumerable<FsaOrderViewModel>>(transportData.ToList()));

                return Ok(res.FirstOrDefault());
            }
            else
            {
                _logger.LogInformation($"Transport Order Data by Id: { id } not found.");
                return NotFound(new { Message = $"Transport Order Data by Id: { id } not found." });
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] FsaOrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                FsaOrdr order = _mapper.Map<FsaOrdr>(model);

                _repo.Update(id, order);
                if (_repo.SaveAll() > 0)
                {
                    var fsaOrder = _repo.GetById(id);

                    _logger.LogInformation($"FSA Order Updated. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/FsaOrders/{ fsaOrder.OrdrId}", fsaOrder);
                }
            }

            return BadRequest(new { Message = "FSA Order Could Not Be Updated." });
        }

        [HttpGet("GetFSADisconnectDetails/{id}")]
        public ActionResult GetFSADisconnectDetails([FromRoute] int id)
        {
            var data = _repo.GetFSADisconnectDetails(id);
            return Ok(data);
        }

        private IEnumerable<FsaOrderViewModel> GetSubDetails(IEnumerable<FsaOrderViewModel> orders)
        {
            var orderTypes = _orderTypeRepo.GetAll();
            var orderSubTypes = _orderSubTypeRepo.GetAll();
            var fsaProdTypes = _fsaProdTypeRepo.GetAll();

            return orders
                //.GroupJoin(
                //    orderTypes,
                //    order => order.OrdrTypeCd,
                //    orderType => orderType.FsaOrdrTypeCd,
                //    (order, orderType) => {
                //        var temp = orderType.FirstOrDefault();
                //        order.OrdrTypeDes = temp != null ? temp.OrdrTypeDes : null;

                //        return order;
                //    })
                .GroupJoin(
                    orderSubTypes,
                    order => order.OrdrSubTypeCd,
                    orderType => orderType.OrdrSubTypeCd,
                    (order, orderSubType) => {
                        var temp = orderSubType.FirstOrDefault();
                        order.OrdrSubTypeDes = temp != null ? temp.OrdrSubTypeDes : null;

                        return order;
                    })
                .GroupJoin(
                    fsaProdTypes,
                    order => order.ProdTypeCd,
                    fsaProdType => fsaProdType.FsaProdTypeCd,
                    (order, fsaProdType) => {
                        var temp = fsaProdType.FirstOrDefault();
                        order.ProdTypeDes = temp != null ? temp.FsaProdTypeDes : null;

                        return order;
                    });
        }

        [HttpPut("UpdateVendorAccessCostInfo/{id}")]
        public ActionResult UpdateVendorAccessCostInfo([FromRoute] int id, [FromBody] FsaOrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                FsaOrdr order = _mapper.Map<FsaOrdr>(model);

                _repo.UpdateVendorAccessCostInfo(id, order);
                return Ok(order);
            }

            return BadRequest(new { Message = "Vendor Access Cost Information Could Not Be Updated." });
        }
    }
}
