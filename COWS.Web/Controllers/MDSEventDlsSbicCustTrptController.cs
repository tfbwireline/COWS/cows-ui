﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSEventDslSbicCustTrpt")]
    [ApiController]
    public class MDSEventDslSbicCustTrptController : ControllerBase
    {
        private readonly IMDSEventDslSbicCustTrptRepository _repo;
        private readonly ILogger<MDSEventDslSbicCustTrptController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MDSEventDslSbicCustTrptController(IMapper mapper,
                               IMDSEventDslSbicCustTrptRepository repo,
                               ILogger<MDSEventDslSbicCustTrptController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"MDSEventDslSbicCustTrpt by EventId: { id }.");

            var obj = _repo.GetMdsEventDslSbicCustTrptByEventId(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSEventDslSbicCustTrptViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDSEventDslSbicCustTrpt by EventId: { id } not found.");
                return NotFound(new { Message = $"MDSEventDslSbicCustTrpt By EventId: { id } not found." });
            }
        }
    }
}