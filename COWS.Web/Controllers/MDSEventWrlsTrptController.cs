﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSEventWrlsTrpt")]
    [ApiController]
    public class MDSEventWrlsTrptController : ControllerBase
    {
        private readonly IMDSEventWrlsTrptRepository _repo;
        private readonly ILogger<MDSEventWrlsTrptController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MDSEventWrlsTrptController(IMapper mapper,
                               IMDSEventWrlsTrptRepository repo,
                               ILogger<MDSEventWrlsTrptController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"MDSEventWrlsTrpt by EventId: { id }.");

            var obj = _repo.GetMDSEventWrlsTrptByEventId(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSEventWrlsTrptViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDSEventWrlsTrpt by EventId: { id } not found.");
                return NotFound(new { Message = $"MDSEventWrlsTrpt by EventId: { id } not found." });
            }
        }
    }
}