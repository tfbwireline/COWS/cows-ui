﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VendorOrder")]
    [ApiController]
    public class VendorOrderController : ControllerBase
    {
        private readonly ICommonRepository _commonRepo;
        private readonly IOrderRepository _repoOrder;
        private readonly IVendorOrderRepository _repo;
        private readonly IVendorOrderEmailRepository _vendorEmailRepo;
        private readonly IVendorOrderFormRepository _vendorFormRepo;
        private readonly IProfileHierarchyRepository _profileHierarchyRepo;
        private readonly ISystemConfigRepository _systemConfigRepo;
        private readonly ILogger<VendorOrderController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public VendorOrderController(IMapper mapper,
                       ICommonRepository commonRepo,
                       IOrderRepository repoOrder,
                       IVendorOrderRepository repo,
                       IVendorOrderEmailRepository vendorEmailRepo,
                       IVendorOrderFormRepository vendorFormRepo,
                       IProfileHierarchyRepository profileHierarchyRepo,
                       ISystemConfigRepository systemConfigRepo,
                       ILogger<VendorOrderController> logger,
                       ILoggedInUserService loggedInUser,
                       IMemoryCache cache)
        {
            _mapper = mapper;
            _commonRepo = commonRepo;
            _repoOrder = repoOrder;
            _repo = repo;
            _vendorEmailRepo = vendorEmailRepo;
            _vendorFormRepo = vendorFormRepo;
            _profileHierarchyRepo = profileHierarchyRepo;
            _systemConfigRepo = systemConfigRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Get Vendor Order By Id: {id}");

            var data = _repo.Find(i => i.VndrOrdrId == id && i.RecStusId == 1);

            if (data != null)
            {
                if (data.SingleOrDefault().Ordr == null)
                {
                    var order = _repoOrder.GetById(id);
                    data.SingleOrDefault().Ordr = order;
                }
                var list = _mapper.Map<VendorOrderViewModel>(data.SingleOrDefault());
                if(list.CsgLvlId > 0)
                {
                    var adid = _loggedInUser.GetLoggedInUserAdid();
                    var userCsg = _loggedInUser.GetLoggedInUserCsgLvlId();
                    _commonRepo.LogWebActivity((byte)WebActyType.VendorOrderDetails, list.OrdrId.ToString(), adid, (byte)userCsg, list.CsgLvlId);
                }


                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"Vendor Order not found.");
                return NotFound(new { Message = $"Vendor Order not found." });
            }
        }

        [HttpGet("{id}/Version2")]
        public IActionResult Get2([FromRoute] int id)
        {
            _logger.LogInformation($"Get Vendor Order By Id: {id}");
            GetVendorOrderDetailsViewModel data = new GetVendorOrderDetailsViewModel();

            var startDate = DateTime.Now;
            var vendorOrder = _repo.GetForVendorOrderPage(id, _loggedInUser.GetLoggedInUserCsgLvlId());
            var endDate = DateTime.Now;

            if (vendorOrder.VndrOrdrNavigation.CsgLvlId > 0)
            {
                var adid = _loggedInUser.GetLoggedInUserAdid();
                var userCsg = _loggedInUser.GetLoggedInUserCsgLvlId();
                _commonRepo.LogWebActivity((byte)WebActyType.VendorOrderDetails, vendorOrder.Ordr.OrdrId.ToString(), adid, (byte)userCsg, vendorOrder.Ordr.CsgLvlId);
            }

            if (vendorOrder != null)
            {
                data.VendorOrder = _mapper.Map<VendorOrderViewModel>(vendorOrder);
                var endDatee = DateTime.Now;

                if (vendorOrder.OrdrId != null)
                {
                    data.Workgroup = _repoOrder.GetWgData((int)vendorOrder.OrdrId, _loggedInUser.GetLoggedInUserId(), 0, _loggedInUser.GetLoggedInUserCsgLvlId());
                    var endDateee = DateTime.Now;

                    data.ProfileHierarchy = _mapper.Map<IEnumerable<ProfileHierarchyViewModel>>(_profileHierarchyRepo
                        .Find(s => s.RecStusId == 1)
                        .OrderBy(s => s.PrntPrfId)
                        .ToList());
                    var endDateeee = DateTime.Now;

                    data.IsBasic = IsBasic((int)vendorOrder.OrdrId);
                    var endDateeeee = DateTime.Now;
                }

                return Ok(data);
            }
            else
            {
                _logger.LogInformation($"Vendor Order not found.");
                return NotFound(new { Message = $"Vendor Order not found." });
            }
        }

        [HttpGet("{id}/Emails")]
        public IActionResult GetEmailsByVendorId([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Emails by Vendor Order Id: { id }.");

            var obj = _vendorEmailRepo.Find(s => s.VndrOrdrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<VendorOrderEmailViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Vendor Emails by Vendor Order Id: { id } not found.");
                return NotFound(new { Message = $"Vendor Emails by Vendor Order Id: { id } not found." });
            }
        }

        [HttpGet("{id}/Forms")]
        public IActionResult GetFormsByVendorId([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Forms by Vendor Order Id: { id }.");

            var obj = _vendorFormRepo.Find(s => s.VndrOrdrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<VendorOrderFormViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Vendor Forms by Vendor Order Id: { id } not found.");
                return NotFound(new { Message = $"Vendor Forms by Vendor Order Id: { id } not found." });
            }
        }

        [HttpGet("Order/{orderId}")]
        public IActionResult GetByOrderId([FromRoute] int orderId)
        {
            _logger.LogInformation($"Get Vendor Order");

            var data = _repo.Find(i => i.OrdrId == orderId && i.RecStusId == 1);
            //var data = _repo.GetVendorOrderByOrderId(vendorOrderId);

            if (data != null)
            {
                var list = _mapper.Map<VendorOrderViewModel>(data.FirstOrDefault());
                //CacheManager.Set(_cache, CacheKeys.VendorOrderList, list);

                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"Vendor Order not found.");
                return NotFound(new { Message = $"Vendor Order not found." });
            }
        }

        [HttpGet("GetVendorOrderDetailsByOrderId")]
        public IActionResult GetVendorOrderDetailsByOrderId([FromQuery] int vendorOrderId)
        {
            IEnumerable<VendorOrderViewModel> list;

            _logger.LogInformation($"Get Vendor Order Details");

            var data = _repo.Find(i => i.VndrOrdrId == vendorOrderId && i.RecStusId == 1);
            //var data = _repo.GetVendorOrderByOrderId(vendorOrderId);

            if (data != null)
            {
                list = _mapper.Map<IEnumerable<VendorOrderViewModel>>(data);
                CacheManager.Set(_cache, CacheKeys.VendorOrderList, list);
            }
            else
            {
                _logger.LogInformation($"Vendor Order Details not found.");
                return NotFound(new { Message = $"Vendor Order Details not found." });
            }

            return Ok(list);
        }

        [HttpGet("GetEmailAttachment")]
        public IActionResult GetEmailAttachment([FromQuery] int vendorOrderId)
        {
            IEnumerable<VendorOrderEmailAttachmentViewModel> list;

            _logger.LogInformation($"Get Vendor Order Email Attachment");

            var data = _repo.GetEmailAttachment(vendorOrderId);

            if (data != null)
            {
                list = _mapper.Map<IEnumerable<VendorOrderEmailAttachmentViewModel>>(data);
                CacheManager.Set(_cache, CacheKeys.VendorOrderEmailAttachmentList, list);
            }
            else
            {
                _logger.LogInformation($"Vendor Order Email Attachment not found.");
                return NotFound(new { Message = $"Vendor Order  Email Attachment not found." });
            }

            return Ok(list);
        }

        [HttpGet("GetVendorOrderEmailType")]
        public IActionResult GetVendorOrderEmailType()
        {
            IEnumerable<VendorOrderEmailTypeViewModel> list;

            _logger.LogInformation($"Get Vendor Order Email Type");

            var data = _repo.GetVendorOrderEmailType();

            if (data != null)
            {
                list = _mapper.Map<IEnumerable<VendorOrderEmailTypeViewModel>>(data);
                //CacheManager.Set(_cache, CacheKeys.VendorOrderEmailTypeList, list);
            }
            else
            {
                _logger.LogInformation($"Vendor Order Email Type not found.");
                return NotFound(new { Message = $"Vendor Order Email Type not found." });
            }

            return Ok(list);
        }

        [HttpGet("GetVendorOrderTerminating")]
        public IActionResult GetVendorOrder([FromQuery] string vendorOrderId, [FromQuery] int h5FolderCustomerId, [FromQuery] string ftn, [FromQuery] bool isTerminating)
        {
            _logger.LogInformation($"Get Vendor Order");

            int vendorOrderID = GetVendorOrderID(vendorOrderId);
            //var response = _mapper.Map<IEnumerable<VendorOrderViewModel>>(_repo.GetVendorOrders(vendorOrderID, h5FolderCustomerId, ftn)).ToList();
            var response = _repo.GetVendorOrders(vendorOrderID, h5FolderCustomerId, ftn);

            //var response = _mapper.Map<IEnumerable<VendorOrderViewModel>>(data.AsEnumerable()).ToList();
            if (response != null && response.Tables.Count != 0)
            {
                List<VendorOrderViewModel> orders = new List<VendorOrderViewModel>();
                foreach (DataRow dt in response.Tables[0].Rows) {
                    VendorOrderViewModel vndr = new VendorOrderViewModel {
                        VndrOrdrId = Convert.ToInt32(dt["VNDR_ORDR_ID"]),
                        OrdrId = string.IsNullOrEmpty(dt["ORDR_ID"].ToString()) ? 0 : Convert.ToInt32(dt["ORDR_ID"]),
                        TrmtgCd = Convert.ToBoolean(dt["TRMTG_CD"]),
                        VndrOrdrTypeId = Convert.ToByte(dt["VNDR_ORDR_TYPE_ID"]),
                        VndrOrdrTypeDes = dt["VNDR_ORDR_TYPE_DESC"].ToString(),
                        VndrCd = dt["VNDR_CD"].ToString(),
                        VndrNme = dt["VNDR_NME"].ToString() + " [" + dt["VNDR_CD"].ToString() + "]",
                        CtryNme = dt["CTRY_NME"].ToString(),
                        CustId = Convert.ToInt32(dt["CUST_ID"]),
                        Ftn = dt["FTN"].ToString(),
                        CsgLvlId = Convert.ToByte(dt["CSG_LVL_ID"])
                    };
                    orders.Add(vndr);
                }

                List<VendorOrderViewModel> filtered = new List<VendorOrderViewModel>();
                foreach (VendorOrderViewModel vod in orders)
                {
                    if (vod.TrmtgCd == isTerminating)
                        filtered.Add(vod);
                }
                //response = filtered;
                var list = ApplySearchFilter(filtered);
                if(list.Count() == 1)
                {
                    var data = list[0];

                    if (data.CsgLvlId > 0)
                    {
                        var adid = _loggedInUser.GetLoggedInUserAdid();
                        var UserCsg = _loggedInUser.GetLoggedInUserCsgLvlId();
                        // LogWebActivity
                        _commonRepo.LogWebActivity((byte)WebActyType.VendorOrderSearch, data.VndrOrdrId.ToString(), adid, (byte)UserCsg, data.CsgLvlId);
                    }
                }

                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"Vendor Order not found.");
                return NotFound(new { Message = $"Vendor Order not found." });
            }
            //if (response != null)
            //{
            //    List<VendorOrderViewModel> orders = response;
            //    List<VendorOrderViewModel> filtered = new List<VendorOrderViewModel>();
            //    foreach (VendorOrderViewModel vod in orders)
            //    {
            //        if (vod.TrmtgCd == isTerminating)
            //            filtered.Add(vod);
            //    }
            //    response = filtered;
            //    var list = ApplySearchFilter(response);
            //    return Ok(list);
            //}
            //else
            //{
            //    _logger.LogInformation($"Vendor Order not found.");
            //    return NotFound(new { Message = $"Vendor Order not found." });
            //}            
        }

        [HttpGet("GetVendorOrder")]
        public IActionResult GetVendorOrder([FromQuery] string vendorOrderId, [FromQuery] int h5FolderCustomerId, [FromQuery] string ftn)
        {
            _logger.LogInformation($"Get Vendor Order");

            int vendorOrderID = GetVendorOrderID(vendorOrderId);
            var data = _repo.GetVendorOrders(vendorOrderID, h5FolderCustomerId, ftn);

            if (data != null && data.Tables.Count != 0)
            {
                List<VendorOrderViewModel> vendorOrderList = new List<VendorOrderViewModel>();
                vendorOrderList = (from DataRow dr in data.Tables[0].Rows
                                   select new VendorOrderViewModel()
                                   {
                                       VndrOrdrId = Convert.ToInt32(dr["VNDR_ORDR_ID"]),
                                       OrdrId = dr["ORDR_ID"] == DBNull.Value ? null : (int?)dr["ORDR_ID"],
                                       TrmtgCd = Convert.ToBoolean(dr["TRMTG_CD"]),
                                       VndrOrdrTypeId = Convert.ToByte(dr["VNDR_ORDR_TYPE_ID"]),
                                       VndrOrdrTypeDes = dr["VNDR_ORDR_TYPE_DESC"].ToString(),
                                       VndrCd = dr["VNDR_CD"].ToString(),
                                       VndrNme = dr["VNDR_NME"].ToString(),
                                       CtryNme = dr["CTRY_NME"].ToString(),
                                       //co = dr["EMAILCOUNT"].ToString(),
                                       CustId = Convert.ToInt32(dr["CUST_ID"]),
                                       Ftn = dr["FTN"].ToString(),
                                       CsgLvlId = Convert.ToByte(dr["CSG_LVL_ID"]),
                                   }).ToList();

                var response = _mapper.Map<IEnumerable<VendorOrderViewModel>>(vendorOrderList).ToList();
                if (response.Count() != 0)
                {
                    var list = ApplySearchFilter(response);
                    return Ok(list);
                }
                else
                {
                    _logger.LogInformation($"Vendor Order not found.");
                    return NotFound(new { Message = $"Vendor Order not found." });
                }
            }
            else
            {
                _logger.LogInformation($"Vendor Order not found.");
                return NotFound(new { Message = $"Vendor Order not found." });
            }
        }

        private List<VendorOrderViewModel> ApplySearchFilter(List<VendorOrderViewModel> orders)
        {
            List<VendorOrderViewModel> filtered = new List<VendorOrderViewModel>();

            foreach (VendorOrderViewModel d in orders)
            {
                if (d.OrdrId == 0 || ((d.OrdrId != 0 && _repo.IsOrderCancelled(Convert.ToInt32(d.OrdrId)))))
                    filtered.Add(d);
            }

            if (filtered.Count() == 0)
                filtered = orders;

            return filtered;
        }

        private int GetVendorOrderID(string inVON)
        {
            int vendorOrderID = 0;
            if (inVON != null && inVON.IndexOf("VON") >= 0)
                inVON = inVON.Replace("VON", "");
            try
            {
                vendorOrderID = int.Parse(inVON);
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Get Vendor Order By Id: { inVON } failed. { e.Message }");
            }
            return vendorOrderID;
        }

        [HttpPost]
        public ActionResult Post([FromBody] VendorOrderViewModel model)
        {
            _logger.LogInformation($"Create Vendor Order: { JsonConvert.SerializeObject(model).ToString() }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrOrdr>(model);
                //obj.Ordr = obj.OrdrId != null ? obj.Ordr : null;
                obj.Ordr = null;
                obj.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                obj.CreatDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;

                if (obj.OrdrId != null)
                {
                    var vndrOrdr = _repo.Find(a => a.OrdrId == obj.OrdrId).SingleOrDefault();
                    if (vndrOrdr != null)
                    {
                        return Ok(new { isExisting = true, vendorOrder = vndrOrdr });
                    }
                    else
                    {
                        var order = new Ordr
                        {
                            CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                            OrdrCatId = 3,
                            OrdrStusId = 0,
                            CreatDt = DateTime.Now,
                            H5FoldrId = _repoOrder.GetById(Convert.ToInt32(obj.OrdrId)).H5FoldrId,
                            RecStusId = 0,
                            RgnId = 1
                        };

                        var orderSaved = _repo.CreateOrder(order);
                        if (orderSaved != null)
                        {
                            orderSaved.RecStusId = 0;
                            orderSaved.OrdrStusId = 0;
                            orderSaved.PrntOrdrId = orderSaved.OrdrId;
                            //obj.Ordr = orderSaved;
                            //obj.OrdrId = obj.OrdrId;
                            obj.BypasVndrOrdrMsCd = null;

                            var rep = _repo.Create(obj);

                            if (rep != null)
                            {
                                _logger.LogInformation($"Vendor Order Created");
                                return Created($"api/VendorOrder/{ rep.VndrOrdrId }", new { isExisting = false, vendorOrderId = rep.VndrOrdrId });
                            }
                        }
                    }
                }
                else
                {
                    //Create base Order (ORDR)
                    var order = new Ordr
                    {
                        CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                        OrdrCatId = 3,
                        OrdrStusId = 0,
                        CreatDt = DateTime.Now,
                        H5FoldrId = model.H5Foldr.H5FoldrId,
                        RecStusId = 0,
                        RgnId = 3
                    };

                    var orderSaved = _repo.CreateOrder(order);
                    if (orderSaved != null)
                    {
                        orderSaved.RecStusId = 0;
                        orderSaved.OrdrStusId = 0;
                        orderSaved.PrntOrdrId = orderSaved.OrdrId;
                        //obj.Ordr = orderSaved;
                        //obj.OrdrId = null;
                        obj.BypasVndrOrdrMsCd = null;

                        var rep = _repo.Create(obj);

                        if (rep != null)
                        {
                            _logger.LogInformation($"Vendor Order Created");
                            return Created($"api/VendorOrder/{ rep.VndrOrdrId }", new { isExisting = false, vendorOrderId = rep.VndrOrdrId });
                        }
                    }
                }

            }

            return BadRequest(new { Message = "Vendor Order Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] VendorOrderViewModel model)
        {
            _logger.LogInformation($"Update Vendor Order: { JsonConvert.SerializeObject(model).ToString() }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrOrdr>(model);
                obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                obj.ModfdDt = DateTime.Now;

                obj.VndrOrdrNavigation = new Ordr();
                if (model.H5Foldr != null)
                {
                    obj.VndrOrdrNavigation.H5FoldrId = model.H5Foldr.H5FoldrId;
                }

                _repo.Update(id, obj);
                int count = _repo.SaveAll();

                if (count > 0)
                {
                    //var list = _mapper.Map<VendorOrderViewModel>(_repo
                    //                                            .Find(a => a.VndrOrdrId == obj.VndrOrdrId)
                    //                                            .SingleOrDefault());

                    _logger.LogInformation($" Vendor Order Updated. Vendor Order Id: { id }");
                    return Created($"api/VendorOrder/{ obj.VndrOrdrId }", obj);
                }
            }

            return BadRequest(new { Message = "Vendor Order Could Not Be Updated." });
        }

        private bool IsBasic(int orderId)
        {
            var isBasic = false;

            var basic = _repoOrder.GetBasic(orderId);
            if (basic.Count() > 0)
            {

                var list = _mapper.Map<OrderBasic>(basic.FirstOrDefault());
                var lRogers = GetVendorCodeList("lRogers").ToList();
                var lRogersVPN = GetVendorCodeList("lRogersVPN").ToList();
                var lOrange = GetVendorCodeList("lOrange").ToList();
                var lOrangeVPN = GetVendorCodeList("lOrangeVPN").ToList();
                var lNavega = GetVendorCodeList("lNavega").ToList();
                var lNavegaVPN = GetVendorCodeList("lNavegaVPN").ToList();
                var lGCI = GetVendorCodeList("lGCI").ToList();
                var lGCIVPN = GetVendorCodeList("lGCIVPN").ToList();
                var lBellCanada = GetVendorCodeList("lBellCanada").ToList();
                var lBellCanadaVPN = GetVendorCodeList("lBellCanadaVPN").ToList();
                var lChinaTelecom = GetVendorCodeList("lChinaTelecom").ToList();
                var lChinaTelecomVPN = GetVendorCodeList("lChinaTelecomVPN").ToList();

                if (list.OrdrCatId == 2 || list.OrdrCatId == 6)
                {
                    var orderCategories = new int[]
                    {
                                (int)OrderTypes.Install,
                                (int)OrderTypes.Upgrade,
                                (int)OrderTypes.Downgrade,
                                (int)OrderTypes.Move,
                                (int)OrderTypes.Change,
                                (int)OrderTypes.BillingChange,
                                (int)OrderTypes.Disconnect,
                    };

                    if (orderCategories.Any(a => a == list.OrdrCatId))
                    {
                        if (list.ProdTypeId == (int)OrderProduct.MPLSOffnet
                            && ((lRogers.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lRogersVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lOrange.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lOrangeVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lNavega.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lNavegaVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lGCI.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lGCIVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lBellCanada.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lBellCanadaVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lChinaTelecom.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lChinaTelecomVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))))
                        {
                            isBasic = true;
                        }
                        else if (list.ProdTypeId == (int)OrderProduct.DIAOffnet
                            && (lRogers.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) || lOrange.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase))))
                        {
                            isBasic = true;
                        }
                        else if (list.ProdTypeId == (int)OrderProduct.SLFROffnet
                            && (lRogers.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) || lGCI.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase))))
                        {
                            isBasic = true;
                        }
                    }
                }
                else if (list.OrdrCatId == 0 && list.InstlVndrCd == string.Empty && list.VndrVpnCd == string.Empty)
                {
                    isBasic = true;
                }
            }

            return isBasic;
        }
        private string GetSystemConfig(string param)
        {
            var systemConfig = _systemConfigRepo.Find(a => a.PrmtrNme == param).SingleOrDefault();

            if (systemConfig != null)
            {
                return systemConfig.PrmtrValuTxt;
            }

            return String.Empty;
        }
        private List<string> GetVendorCodeList(string param)
        {
            var result = GetSystemConfig(param).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(s => s.Trim())
            .ToList();

            return result;
        }
    }
}
