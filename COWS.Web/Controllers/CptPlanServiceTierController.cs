﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/PlanServiceTiers")]
    [ApiController]
    public class CptPlanServiceTierController : ControllerBase
    {
        private readonly ICptPlanServiceTierRepository _repo;
        private readonly ILogger<CptPlanServiceTierController> _logger;
        private readonly IMapper _mapper;

        public CptPlanServiceTierController(IMapper mapper,
                               ICptPlanServiceTierRepository repo,
                               ILogger<CptPlanServiceTierController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptPlanServiceTierViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptPlanServiceTierViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptPlnSrvcTierId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT Plan Service Tier by Id: { id }.");

            var obj = _repo.Find(s => s.CptPlnSrvcTierId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptPlanServiceTierViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT Plan Service Tier by Id: { id } not found.");
                return NotFound(new { Message = $"CPT Plan Service Tier Id: { id } not found." });
            }
        }
    }
}