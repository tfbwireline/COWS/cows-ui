﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/UnlockItems")]
    [ApiController]
    public class UnlockItemsController : ControllerBase
    {
        private readonly IOrdrUnlockRepository _UnlockRepo;
        private readonly ILogger<UnlockItemsController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;
        //private object _loggedInUser;

        public UnlockItemsController(IMapper mapper,
                               IOrdrUnlockRepository UnlockRepo,
                               ILogger<UnlockItemsController> logger,
                               IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _UnlockRepo = UnlockRepo;
            _logger = logger;
            _cache = memoryCache;
        }

        [HttpPost]
        //[HttpGet("")]
        public IActionResult Post([FromBody] GetOrdrUnlockOrdrViewModel model)
        //public IActionResult Post([FromQuery] string ordrId, [FromQuery] string userId, [FromQuery] string itemType)
        //public IActionResult get([FromQuery] string ordrId, [FromQuery] string userId, [FromQuery] string itemType)
        {
            _logger.LogInformation($"Starterted Unlock Operation:..");

            if (ModelState.IsValid)
            {
                int ORDR_ID = 0;
                int EVENT_ID = 0;
                int USER_ID = 0;
                int IsOrder = 0;
                int Unlock = 0;
                int IsLocked = 0;
                int ItemType = 0;
                IEnumerable<GetOrdrUnlockOrdrViewModel> ordrView;
                if (ItemType == 0)  //Ordr
                {
                    ORDR_ID = int.Parse(model.OrdrId);
                    EVENT_ID = ORDR_ID;
                    USER_ID = int.Parse(model.LockByUserId);
                    IsOrder = 1;
                    IsLocked = 0;
                    Unlock = 1;
                    ItemType = int.Parse(model.ProductType);
                    // logging for audit
                    _logger.LogInformation($"Starterted Unlock Order Id: { ORDR_ID }.");

                    var data = _UnlockRepo.unlockItems(ORDR_ID, EVENT_ID, USER_ID, IsOrder, Unlock, IsLocked, ItemType);

                    if (data != null)
                    {
                        if (data.FirstOrDefault().FTN == "Success")
                        {
                            // Key not in cache, so get data.
                            ordrView = _mapper.Map<IEnumerable<GetOrdrUnlockOrdrViewModel>>(data);
                            //CacheManager.Set(_cache, CacheKeys.ordrView, ordrView);
                            _logger.LogInformation($"Completed Unlock Order Id: { ORDR_ID }.");
                            return Ok(ordrView);
                        }
                        else
                        {
                            _logger.LogInformation($"Unlock Redesign Id: { ORDR_ID }. Not Successfull");
                            return NotFound(new { Message = $"Unlock Redesign Id: { ORDR_ID }. Not Successfull" });
                        }
                    }
                    else
                    {
                        _logger.LogInformation($"Unlock Order Id: { ORDR_ID }. Not Successfull");
                        return NotFound(new { Message = $"Unlock Order Id: { ORDR_ID }. Not Successfull" });
                    }

                    //return Ok("1");
                }
                else if (ItemType == 1)  //Event
                {
                    ORDR_ID = int.Parse(model.OrdrId);
                    EVENT_ID = ORDR_ID;
                    USER_ID = int.Parse(model.LockByUserId);
                    IsOrder = 0;
                    IsLocked = 0;
                    Unlock = 1;
                    ItemType = int.Parse(model.ProductType);
                    // logging for audit
                    _logger.LogInformation($"Starterted Unlock Event Id: { ORDR_ID }.");

                    var data = _UnlockRepo.unlockItems(ORDR_ID, EVENT_ID, USER_ID, IsOrder, Unlock, IsLocked, ItemType);

                    if (data != null)
                    {
                        if (data.FirstOrDefault().FTN == "Success")
                        {
                            // Key not in cache, so get data.
                            ordrView = _mapper.Map<IEnumerable<GetOrdrUnlockOrdrViewModel>>(data);
                            //CacheManager.Set(_cache, CacheKeys.ordrView, ordrView);
                            _logger.LogInformation($"Completed Unlock Event Id: { ORDR_ID }.");
                            return Ok(ordrView);
                        }
                        else
                        {
                            _logger.LogInformation($"Unlock Redesign Id: { ORDR_ID }. Not Successfull");
                            return NotFound(new { Message = $"Unlock Redesign Id: { ORDR_ID }. Not Successfull" });
                        }
                    }
                    else
                    {
                        _logger.LogInformation($"Unlock Event Id: { ORDR_ID }. Not Successfull");
                        return NotFound(new { Message = $"Unlock Event Id: { ORDR_ID }. Not Successfull" });
                    }
                    //return Ok("1");
                }
                else if (ItemType == 2)  //Redesign
                {
                    ORDR_ID = int.Parse(model.OrdrId);
                    EVENT_ID = ORDR_ID;
                    USER_ID = int.Parse(model.LockByUserId);
                    IsOrder = 0;  //Doesnt matter
                    IsLocked = 0;
                    Unlock = 1;
                    ItemType = int.Parse(model.ProductType);
                    // logging for audit
                    _logger.LogInformation($"Starterted Unlock Redesign Id: { ORDR_ID }.");

                    var data = _UnlockRepo.unlockItems(ORDR_ID, EVENT_ID, USER_ID, IsOrder, Unlock, IsLocked, ItemType);

                    if (data != null)
                    {
                        if (data.FirstOrDefault().FTN == "Success")
                        {
                            // Key not in cache, so get data.
                            ordrView = _mapper.Map<IEnumerable<GetOrdrUnlockOrdrViewModel>>(data);
                            //CacheManager.Set(_cache, CacheKeys.ordrView, ordrView);
                            _logger.LogInformation($"Completed Unlock Redesign Id: { ORDR_ID }.");
                            return Ok(ordrView);
                        }
                        else
                        {
                            _logger.LogInformation($"Unlock Redesign Id: { ORDR_ID }. Not Successfull");
                            return NotFound(new { Message = $"Unlock Redesign Id: { ORDR_ID }. Not Successfull" });
                        }
                    }
                    else
                    {
                        _logger.LogInformation($"Unlock Redesign Id: { ORDR_ID }. Not Successfull");
                        return NotFound(new { Message = $"Unlock Redesign Id: { ORDR_ID }. Not Successfull" });
                    }
                    //return Ok("1");
                }
                else if (ItemType == 3)  //CPT
                {
                    ORDR_ID = int.Parse(model.OrdrId);
                    EVENT_ID = ORDR_ID;
                    USER_ID = int.Parse(model.LockByUserId);
                    IsOrder = 0;  //Doesnt matter
                    IsLocked = 0;
                    Unlock = 1;
                    ItemType = int.Parse(model.ProductType);
                    // logging for audit
                    _logger.LogInformation($"Starterted Unlock CPT Id: { ORDR_ID }.");

                    var data = _UnlockRepo.unlockItems(ORDR_ID, EVENT_ID, USER_ID, IsOrder, Unlock, IsLocked, ItemType);

                    if (data != null)
                    {
                        if (data.FirstOrDefault().FTN == "Success")
                        {
                            // Key not in cache, so get data.
                            ordrView = _mapper.Map<IEnumerable<GetOrdrUnlockOrdrViewModel>>(data);
                            //CacheManager.Set(_cache, CacheKeys.ordrView, ordrView);
                            _logger.LogInformation($"Completed Unlock CPT Id: { ORDR_ID }.");
                            return Ok(ordrView);
                        }
                        else
                        {
                            _logger.LogInformation($"Unlock Redesign Id: { ORDR_ID }. Not Successfull");
                            return NotFound(new { Message = $"Unlock Redesign Id: { ORDR_ID }. Not Successfull" });
                        }
                    }
                    else
                    {
                        _logger.LogInformation($"Unlock CPT Id: { ORDR_ID }. Not Successfull");
                        return NotFound(new { Message = $"Unlock CPT Id: { ORDR_ID }. Not Successfull" });
                    }
                    //return Ok("1");
                }
            }
            return BadRequest(new { Message = "Unlock Items can not be Done." });
        }

        [HttpGet("")]
        //[ResponseCache(CacheProfileName = "Default")]
        //[Authorize]
        [ActionName("Search")]
        //[RequestHeaderMatchesMediaType("Accept", new[] { "application/vnd.sst.tracker.options+json", "application/json" })]
        public IActionResult Get([FromQuery] string refId, [FromQuery] int flag)
        {
            if (string.IsNullOrEmpty(refId))
            {
                return BadRequest(new { Message = "Reference Id cannot be blank." });
            }
            IEnumerable<GetOrdrUnlockOrdrViewModel> ordrView;
            IEnumerable<GetOrdrUnlockEventViewModel> EventView;
            IEnumerable<GetOrdrUnlockRedsgnViewModel> RedsgnView;
            IEnumerable<GetOrdrUnlockCptViewModel> CptView;
            //if (!_cache.TryGetValue(CacheKeys.ordrView, out ordrView))
            //{
            if (flag == 0)  //Ordr
            {
                //// logging for audit
                _logger.LogInformation($"Get  lock Ordr View");

                var data = _UnlockRepo.GetOrdrUnlockOrdrView(refId);

                if (data.Count() > 0)
                {
                    // Key not in cache, so get data.
                    ordrView = _mapper.Map<IEnumerable<GetOrdrUnlockOrdrViewModel>>(data);
                    //CacheManager.Set(_cache, CacheKeys.ordrView, ordrView);
                    return Ok(ordrView);
                }
                else
                {
                    _logger.LogInformation($"Get lock Ordr View not found.");
                    return NotFound(new { Message = $"lock Ordr View not found." });
                }
            }
            else if (flag == 1)  //Event
            {
                //// logging for audit
                _logger.LogInformation($"Get Lock Events View");

                try
                {
                    Convert.ToInt32(refId);
                }
                catch
                {
                    _logger.LogInformation($"Get lock Event View not found.");
                    return NotFound(new { Message = $"lock Event View not found." });
                }
                var data = _UnlockRepo.GetOrdrUnlockEventView(refId);

                if (data.Count() > 0)
                {
                    // Key not in cache, so get data.
                    EventView = _mapper.Map<IEnumerable<GetOrdrUnlockEventViewModel>>(data);
                    //CacheManager.Set(_cache, CacheKeys.EventView, EventView);
                    return Ok(EventView);
                }
                else
                {
                    _logger.LogInformation($"Get lock Event View not found.");
                    return NotFound(new { Message = $"lock Event View not found." });
                }
            }
            else if (flag == 2)  //Redsgn
            {
                //// logging for audit
                _logger.LogInformation($"Get Lock Redisgn View");

                var data = _UnlockRepo.GetOrdrUnlockRedsgnView(refId);

                if (data.Count() > 0)
                {
                    // Key not in cache, so get data.
                    RedsgnView = _mapper.Map<IEnumerable<GetOrdrUnlockRedsgnViewModel>>(data);
                    //CacheManager.Set(_cache, CacheKeys.RedsgnView, RedsgnView);
                    return Ok(RedsgnView);
                }
                else
                {
                    _logger.LogInformation($"Get lock Redisgn View not found.");
                    return NotFound(new { Message = $"lock Redisgn View not found." });
                }
            }
            else if (flag == 3)  //CPT
            {
                //// logging for audit
                _logger.LogInformation($"Get Lock CPT View");

                var data = _UnlockRepo.GetOrdrUnlockCptView(refId);

                if (data.Count() > 0)
                {
                    // Key not in cache, so get data.
                    CptView = _mapper.Map<IEnumerable<GetOrdrUnlockCptViewModel>>(data);
                    //CacheManager.Set(_cache, CacheKeys.CptView, CptView);
                    return Ok(CptView);
                }
                else
                {
                    _logger.LogInformation($"Get lock CPT View not found.");
                    return NotFound(new { Message = $"lock CPT View not found." });
                }
            }
            else
            {
                return BadRequest(new { Message = "Flag cannot be blank." });
            }

            //}
        }
    }
}