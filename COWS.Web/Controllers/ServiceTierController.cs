﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/ServiceTier")]
    [ApiController]
    public class ServiceTierController : ControllerBase
    {
        private readonly IServiceTierRepository _repo;
        private readonly ILogger<ServiceTierController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public ServiceTierController(IMapper mapper,
                               IServiceTierRepository repo,
                               ILogger<ServiceTierController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ServiceTierViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<ServiceTierViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.MdsSrvcTierDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Service Tier by Id: { id }.");

            var obj = _repo.Find(s => s.MdsSrvcTierId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<ServiceTierViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Service Tier by Id: { id } not found.");
                return NotFound(new { Message = $"Service Tier Id: { id } not found." });
            }
        }

        [HttpGet("GetForLookup")]
        public ActionResult<IEnumerable<ServiceTierViewModel>> GetForLookup()
        {
            var list = _mapper.Map<IEnumerable<ServiceTierViewModel>>(_repo.GetAll()
                                                                .OrderBy(s => s.MdsSrvcTierDes));
            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ServiceTierViewModel model)
        {
            _logger.LogInformation($"Create Service Tier: { model.MdsSrvcTierDes }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkMdsSrvcTier>(model);
                obj.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                obj.CreatDt = DateTime.Now;

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkMdsSrvcTier();
                var duplicate = _repo.Find(i => 
                    i.MdsSrvcTierDes.Trim().ToLower() == obj.MdsSrvcTierDes.Trim().ToLower()).FirstOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        //return BadRequest(new { Message = obj.MdsSrvcTierDes + " already exists for this Event Type." });
                        return BadRequest(new { Message = "Service Delivery Site Support name already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        newData.MdsSrvcTierDes = obj.MdsSrvcTierDes;
                        newData.EventTypeId = obj.EventTypeId;
                        newData.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                        newData.ModfdDt = DateTime.Now;
                        newData.RecStusId = (byte)ERecStatus.Active;

                        _repo.Update(newData.MdsSrvcTierId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }
                
                if (newData != null)
                {

                    _logger.LogInformation($"Service Tier Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/ServiceTier/{ newData.MdsSrvcTierId }", model);
                }
            }

            return BadRequest(new { Message = "Service Tier Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] ServiceTierViewModel model)
        {
            _logger.LogInformation($"Update Service Tier Id: { id }.");

            var obj = _mapper.Map<LkMdsSrvcTier>(model);
            obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
            obj.ModfdDt = DateTime.Now;
            obj.RecStusId = (byte)ERecStatus.Active;

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i =>
                    i.MdsSrvcTierDes.Trim().ToLower() == obj.MdsSrvcTierDes.Trim().ToLower()
                    && i.MdsSrvcTierId != id).FirstOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    //return BadRequest(new { Message = obj.MdsSrvcTierDes + " already exists for this Event Type." });
                    return BadRequest(new { Message = "Service Delivery Site Support name already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.MdsSrvcTierId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Service Tier Updated. { JsonConvert.SerializeObject(model).ToString() } ");
            return Created($"api/ServiceTier/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivate Service Tier Id: { id }.");

            var tier = _repo.Find(s => s.MdsSrvcTierId == id);
            if (tier != null)
            {
                var obj = _mapper.Map<LkMdsSrvcTier>(tier.SingleOrDefault());
                obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.InActive;

                _repo.Update(id, obj);

                _logger.LogInformation($"Service Tier by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Service Tier by Id: { id } not found.");
            }
        }
    }
}
