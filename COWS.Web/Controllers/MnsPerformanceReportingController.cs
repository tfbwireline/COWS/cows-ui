﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MnsPerformanceReporting")]
    [ApiController]
    public class MnsPerformanceReportingController : ControllerBase
    {
        private readonly IMnsPerformanceReportingRepository _repo;
        private readonly ILogger<MnsPerformanceReportingController> _logger;
        private readonly IMapper _mapper;

        public MnsPerformanceReportingController(IMapper mapper,
                               IMnsPerformanceReportingRepository repo,
                               ILogger<MnsPerformanceReportingController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MnsPerformanceReportingViewModel>> Get()
        {
           var list = _mapper.Map<IEnumerable<MnsPerformanceReportingViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.MnsPrfmcRptId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MNS Performance Reporting by Id: { id }.");

            var obj = _repo.Find(s => s.MnsPrfmcRptId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MnsPerformanceReportingViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MNS Performance Reporting by Id: { id } not found.");
                return NotFound(new { Message = $"MNS Performance Reporting Id: { id } not found." });
            }
        }
    }
}