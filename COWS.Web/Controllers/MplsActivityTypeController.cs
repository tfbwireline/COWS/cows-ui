﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MplsActivityTypes")]
    [ApiController]
    public class MplsActivityTypeController : ControllerBase
    {
        private readonly IMplsActivityTypeRepository _repo;
        private readonly ILogger<MplsActivityTypeController> _logger;
        private readonly IMapper _mapper;

        public MplsActivityTypeController(IMapper mapper,
                               IMplsActivityTypeRepository repo,
                               ILogger<MplsActivityTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MplsActivityTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MplsActivityTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.MplsActyTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MPLS Activity Type by Id: { id }.");

            var obj = _repo.Find(s => s.MplsActyTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MplsActivityTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MPLS Activity Type by Id: { id } not found.");
                return NotFound(new { Message = $"MPLS Activity Type Id: { id } not found." });
            }
        }

        [HttpGet("getByEventId/{id}")]
        public ActionResult<IEnumerable<MplsActivityTypeViewModel>> getByEventId([FromRoute] int id)
        {
            var list = _mapper.Map<IEnumerable<MplsActivityTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active
                                                                    && s.EventTypeId == id)
                                                                .OrderBy(s => s.MplsActyTypeDes));
            return Ok(list);
        }
    }
}