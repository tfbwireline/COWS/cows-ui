﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MplsEventAccessTags")]
    [ApiController]
    public class MplsEventAccessTagController : ControllerBase
    {
        private readonly IMplsEventAccessTagRepository _repo;
        private readonly ILogger<MplsEventAccessTagController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MplsEventAccessTagController(IMapper mapper,
                               IMplsEventAccessTagRepository repo,
                               ILogger<MplsEventAccessTagController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MplsEventAccessTagViewModel>> Get()
        {
            IEnumerable<MplsEventAccessTagViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.MplsEventAccessTagList, out list))
            {
                list = _mapper.Map<IEnumerable<MplsEventAccessTagViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.LocCtyNme));

                CacheManager.Set(_cache, CacheKeys.MplsEventAccessTagList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MPLS Event Access Tag by Id: { id }.");

            var obj = _repo.Find(s => s.MplsEventAccsTagId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MplsEventAccessTagViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MPLS Event Access Tag by Id: { id } not found.");
                return NotFound(new { Message = $"MPLS Event Access Tag Id: { id } not found." });
            }
        }

        [HttpGet("getByEventId/{id}")]
        public ActionResult<IEnumerable<MplsEventAccessTagViewModel>> getByEventId([FromRoute] int id)
        {
            IEnumerable<MplsEventAccessTagViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.MplsEventAccessTagList, out list))
            {
                list = _mapper.Map<IEnumerable<MplsEventAccessTagViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1 && s.EventId == id)
                                                                .OrderBy(s => s.LocCtyNme));

                CacheManager.Set(_cache, CacheKeys.MplsEventAccessTagList, list);
            }

            return Ok(list);
        }
    }
}