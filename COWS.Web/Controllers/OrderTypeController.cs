﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/OrderTypes")]
    [ApiController]
    public class OrderTypeController : ControllerBase
    {
        private readonly IOrderTypeRepository _repo;
        private readonly ILogger<OrderTypeController> _logger;
        private readonly IMapper _mapper;

        public OrderTypeController(IMapper mapper,
                               IOrderTypeRepository repo,
                               ILogger<OrderTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OrderTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<OrderTypeViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order Type by Id: { id }.");

            var obj = _repo.Find(s => s.OrdrTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<OrderTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Order Type by Id: { id } not found.");
                return NotFound(new { Message = $"Order Type Id: { id } not found." });
            }
        }
    }
}