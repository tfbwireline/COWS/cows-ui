﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EscalationReasons")]
    [ApiController]
    public class EscalationReasonController : ControllerBase
    {
        private readonly IEscalationReasonRepository _repo;
        private readonly ILogger<EscalationReasonController> _logger;
        private readonly IMapper _mapper;

        public EscalationReasonController(IMapper mapper,
                               IEscalationReasonRepository repo,
                               ILogger<EscalationReasonController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EscalationReasonViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<EscalationReasonViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.EsclReasDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Escalation Reason by Id: { id }.");

            var obj = _repo.GetById(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<EscalationReasonViewModel>(obj));
            }
            else
            {
                _logger.LogInformation($"Escalation Reason by Id: { id } not found.");
                return NotFound(new { Message = $"Escalation Reason Id: { id } not found." });
            }
        }
    }
}