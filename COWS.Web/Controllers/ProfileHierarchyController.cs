﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/ProfileHierarchy")]
    [ApiController]
    public class ProfileHierarchyController : ControllerBase
    {
        private readonly IProfileHierarchyRepository _repo;
        private readonly IMapper _mapper;

        public ProfileHierarchyController(IMapper mapper,
                               IProfileHierarchyRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProfileHierarchyViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<ProfileHierarchyViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.PrntPrfId));
            return Ok(list);
        }
    }
}