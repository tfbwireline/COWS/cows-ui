﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MplsEventTypes")]
    [ApiController]
    public class MplsEventTypeController : ControllerBase
    {
        private readonly IMplsEventTypeRepository _repo;
        private readonly ILogger<MplsEventTypeController> _logger;
        private readonly IMapper _mapper;

        public MplsEventTypeController(IMapper mapper,
                               IMplsEventTypeRepository repo,
                               ILogger<MplsEventTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MplsEventTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MplsEventTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.MplsEventTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MPLS Event Type by Id: { id }.");

            var obj = _repo.Find(s => s.MplsEventTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MplsEventTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MPLS Event Type by Id: { id } not found.");
                return NotFound(new { Message = $"MPLS Event Type Id: { id } not found." });
            }
        }

        [HttpGet("getByEventId/{id}")]
        public ActionResult<IEnumerable<MplsEventTypeViewModel>> getByEventId([FromRoute] int id)
        {
            var list = _mapper.Map<IEnumerable<MplsEventTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1 && s.EventTypeId == id)
                                                                .OrderBy(s => s.MplsEventTypeDes));
            return Ok(list);
        }
    }
}