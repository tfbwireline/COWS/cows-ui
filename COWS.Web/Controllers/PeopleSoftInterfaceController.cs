﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/PeopleSoftInterface")]
    [ApiController]
    public class PeopleSoftInterfaceController : ControllerBase
    {
        private readonly IPeopleSoftInterfaceRepository _peoplesoftInterfaceRepo;
        private readonly ILogger<PeopleSoftInterfaceController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public PeopleSoftInterfaceController(IMapper mapper,
                               IPeopleSoftInterfaceRepository peoplesoftInterfaceRepo,
                               ILogger<PeopleSoftInterfaceController> logger,
                               IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _peoplesoftInterfaceRepo = peoplesoftInterfaceRepo;
            _logger = logger;
            _cache = memoryCache;
        }

        // GET: api/<controller>
        [HttpGet]
        public ActionResult<IEnumerable<GetSCMInterfaceViewModel>> Get()
        {
            IEnumerable<GetSCMInterfaceViewModel> psftInterfaceView;
            if (!_cache.TryGetValue(CacheKeys.SCMInterfaceView, out psftInterfaceView))
            {
                _logger.LogInformation($"Get top 500 PSFT Interface View");
                var data = _peoplesoftInterfaceRepo.GetSCMInterfaceView();

                if (data != null)
                {
                    // Key not in cache, so get data.
                    psftInterfaceView = _mapper.Map<IEnumerable<GetSCMInterfaceViewModel>>(data);
                    CacheManager.Set(_cache, CacheKeys.SCMInterfaceView, psftInterfaceView);
                }
                else
                {
                    _logger.LogInformation($"Get top 500 PSFT Interface View not found.");
                    return NotFound(new { Message = $"Top top 500 PSFT Interface View not found." });
                }
            }
            return Ok(psftInterfaceView);
        }
    }
}