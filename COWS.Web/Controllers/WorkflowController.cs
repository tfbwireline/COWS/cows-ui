﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Workflows")]
    [ApiController]
    public class WorkflowController : ControllerBase
    {
        private readonly IWorkflowRepository _repo;
        private readonly IUserRepository _userRepository;
        private readonly IEventRuleRepository _eventRuleRepository;
        private readonly ILogger<WorkflowController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public WorkflowController(IMapper mapper,
                               IWorkflowRepository repo,
                               IUserRepository userRepository,
                               IEventRuleRepository eventRuleRepository,
                               ILogger<WorkflowController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _userRepository = userRepository;
            _eventRuleRepository = eventRuleRepository;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<WorkflowViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<WorkflowViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.WrkflwStusDes));
            return Ok(list);
        }

        [HttpGet("Events")]
        public IActionResult Get([FromQuery] int eventTypeId, [FromQuery] int eventStusId, [FromQuery] int orderType)
        {
            _logger.LogInformation($"Search Workflow by Event Type Id: { eventTypeId } and Event Status Id: { eventStusId }.");

            if (eventTypeId != 9)
            {
                // Update condition by Sarah Sandoval [20200506]
                // Updated condition to handle null value on GetFinalUserProfile
                // because it throws an error when user doesn't have an event profile
                IEnumerable<LkWrkflwStus> workflow;
                var usrProfiles = _userRepository.GetFinalUserProfile(_loggedInUser.GetLoggedInUserId(), eventTypeId);
                if (usrProfiles != null)
                {
                    int profileId = usrProfiles.UsrPrfId;

                    workflow = _eventRuleRepository
                    .Find(a => a.EventTypeId == eventTypeId &&
                        a.UsrPrfId == profileId &&
                        a.StrtEventStusId == eventStusId &&
                        a.RecStusId == 1 && 
                        a.StrtEventStusId != 6)
                    .Select(a => new { a.WrkflwStus, DsplCd = a.DsplCd ?? 1 })
                    .Distinct()
                    .OrderBy(a => a.DsplCd)
                    .Select(b=>b.WrkflwStus)
                    .Distinct()
                    .ToList();

                    // Added by Sarah Sandoval to get workflow of next in hierarchy for multiple profiles
                    if (eventTypeId != (int)EventType.MDS &&  workflow != null && workflow.Count() <= 0)
                    {
                        var eventProfiles = _userRepository.GetActiveProfileIdByEventTypeId(_loggedInUser.GetLoggedInUserId(), eventTypeId);
                        eventProfiles.Remove(profileId);
                        if (eventProfiles != null && eventProfiles.Count() > 0)
                        {
                            eventProfiles.Remove(profileId);
                            workflow = _eventRuleRepository
                                .Find(a => a.EventTypeId == eventTypeId &&
                                    a.UsrPrfId == eventProfiles[0] &&
                                    a.StrtEventStusId == eventStusId &&
                                    a.RecStusId == 1 &&
                                    a.StrtEventStusId != 6)
                                .Select(a => new { a.WrkflwStus, DsplCd = a.DsplCd ?? 1 })
                               .Distinct()
                                .OrderBy(a => a.DsplCd)
                                .Select(b => b.WrkflwStus)
                                .Distinct()
                                .ToList();
                        }
                    }
                }
                else
                {
                    workflow = _eventRuleRepository
                    .Find(a => a.EventTypeId == eventTypeId &&
                        a.StrtEventStusId == eventStusId &&
                        a.RecStusId == 1 &&
                        a.StrtEventStusId != 6)
                    .Select(a => new { a.WrkflwStus, DsplCd = a.DsplCd ?? 1 })
                               .Distinct()
                                .OrderBy(a => a.DsplCd)
                                .Select(b => b.WrkflwStus)
                                .Distinct()
                    .ToList();
                }

                if (workflow != null)
                {
                    //return Ok(_mapper.Map<IEnumerable<WorkflowViewModel>>(workflow.Where(a => workflowStatusIds.Contains(a.WrkflwStusId)).ToList()));
                    return Ok(_mapper.Map<IEnumerable<WorkflowViewModel>>(workflow));
                }
                else
                {
                    _logger.LogInformation($"Workflow by Event Type Id: { eventTypeId } and Event Status Id: { eventStusId } not found.");
                    return NotFound(new { Message = $"Workflow by Event Type Id: { eventTypeId } and Event Status Id: { eventStusId } not found." });
                }
            }
            else
            {
                // Dictionary<FedlineActivityType, string> activityConverter = new Dictionary<FedlineActivityType, string>()
                //{
                //    {FedlineActivityType.Disconnect, "DCC"},
                //    {FedlineActivityType.Disconnect, "DCS"},
                //    {FedlineActivityType.HeadendDisconnect, "HED"},
                //    {FedlineActivityType.HeadendInstall, "HEI"},
                //    {FedlineActivityType.HeadendMAC, "HEM"},
                //    {FedlineActivityType.Install, "NCC"},
                //    {FedlineActivityType.Install, "NCI"},
                //    {FedlineActivityType.Install, "NCM"},
                //    {FedlineActivityType.Install, "NDM"},
                //    {FedlineActivityType.Install, "NIC"},
                //    {FedlineActivityType.Install, "NIM"}
                //};

                int[] activeProfileIds = _userRepository.GetActiveProfileIdByEventTypeId(_loggedInUser.GetLoggedInUserId(), eventTypeId).ToArray();

                IEnumerable<LkEventRule> eventStus = _eventRuleRepository
                .Find(a => a.EventTypeId == eventTypeId &&
                a.OrdrTypeCdNavigation.PrntOrdrTypeDes == Enum.GetName(typeof(FedlineActivityType), orderType) &&
                    activeProfileIds.Contains(a.UsrPrfId) &&
                    a.StrtEventStusId == eventStusId)
                //.Select(a => a.EndEventStus)
                //.OrderBy(a => a.EventStusDes)
                .ToList();

                if (eventStus != null)
                {
                    //return Ok(_mapper.Map<IEnumerable<EventStatusViewModel>>(eventStus));
                    return Ok(eventStus);
                }
                else
                {
                    _logger.LogInformation($"EventStus by Event Type Id: { eventTypeId } and Event Status Id: { eventStusId } not found.");
                    return NotFound(new { Message = $"EventStus by Event Type Id: { eventTypeId } and Event Status Id: { eventStusId } not found." });
                }
            }
        }
    }
}