﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventLock")]
    [ApiController]
    public class EventLockContoller : ControllerBase
    {
        private readonly IEventLockRepository _repo;
        private readonly ILogger<EventLockContoller> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public EventLockContoller(IMapper mapper,
                               IEventLockRepository repo,
                               ILogger<EventLockContoller> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("CheckLock/{id}")]
        public IActionResult CheckLock([FromRoute] int id)
        {
            _logger.LogInformation($"Check Event Lock by Id: { id }.");

            var obj = _repo.CheckLock(id);
            if (obj != null && _loggedInUser.GetLoggedInUserId() == obj.LockByUserId)
                return Ok();

            return Ok(_mapper.Map<EventLockViewModel>(obj));
        }

        [HttpGet("Lock/{id}")]
        public IActionResult Lock([FromRoute] int id)
        {
            _logger.LogInformation($"Lock Event by Id: { id }.");

            var obj = _repo.CheckLock(id);
            if (obj != null)
            {
                if (_loggedInUser.GetLoggedInUserId() != obj.LockByUserId)
                {
                    return BadRequest(new { Message = $"This event is currently locked by {obj.LockByUser.FullNme}" });
                }
            }
            else
            {
                int eventId = _repo.Lock(new EventRecLock
                {
                    EventId = id,
                    LockByUserId = _loggedInUser.GetLoggedInUserId(),
                    StrtRecLockTmst = DateTime.Now
                });

                if (eventId == 0)
                {
                    return BadRequest(new { Message = $"An error occurred while locking the Event. Please try again" });
                }
            }

            return Ok();
        }

        [HttpGet("Unlock/{id}")]
        public IActionResult Unlock([FromRoute] int id)
        {
            _logger.LogInformation($"Unlock Event by Id: { id }.");

            _repo.Unlock(id, _loggedInUser.GetLoggedInUserId());
            return Ok();
        }
    }
}