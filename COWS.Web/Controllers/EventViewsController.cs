﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventViews")]
    [ApiController]
    public class EventViewsController : ControllerBase
    {
        private readonly IEventViewRepository _repo;
        private readonly ILogger<EventViewsController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public EventViewsController(IMapper mapper,
                               IEventViewRepository repo,
                               ILogger<EventViewsController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("GetEventViews")]
        public IActionResult GetEventViews([FromQuery] int userId, [FromQuery] int siteCntnt)
        {
            return Ok(_repo.GetEventViews(userId, siteCntnt));
        }

        [HttpGet("GetEventViewDetails")]
        public IActionResult GetEventViewDetails([FromQuery] int viewId, [FromQuery] int siteCntnt, [FromQuery] int userId, [FromQuery] string newMDS)
        {
            var userCsgLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            return Ok(_repo.GetEventViewDetailsAsync(viewId, siteCntnt, userId, newMDS, userCsgLevel).Result);
        }

        [HttpGet("GetCalendarViewDetails")]
        public IActionResult GetCalendarViewDetails([FromQuery] int viewId, [FromQuery] int eID, [FromQuery] string date, [FromQuery] string type, [FromQuery] string user, [FromQuery] string group)
        {
            if (date != "null")
                return Ok(_repo.GetCalendarViewDetails(viewId, eID, Convert.ToDateTime(date), type, user, group));

            return Ok(_repo.GetCalendarViewDetails(viewId, eID, null, type, user, group));
        }

        [HttpGet("GetEventViewColumns")]
        public IActionResult GetEventViewColumns([FromQuery] int grpId, [FromQuery] int siteCntnt)
        {
            return Ok(_repo.GetEventViewColumns(grpId, siteCntnt));
        }

        [HttpGet("GetEventViewInfo")]
        public ActionResult<DataSet> GetEventViewInfo([FromQuery] int viewId)
        {
            DataTable dt = new DataTable();
            var dataSet = _repo.GetEventViewInfo(viewId);
            if (dataSet != null)
            {
                return Ok(dataSet);
            }
            return BadRequest(new { Message = "No Event Information found for this view." });
            //var table1 = result2.Tables[0];
            //var table2 = result2.Tables[1];
            //return (table1, table2);
        }

        [HttpGet("GetFilterOperators")]
        public IActionResult GetFilterOperators()
        {
            var list = _repo.GetFilterOperators().Where(a => a.LogicOprCd == "N").AsEnumerable()
                .Select(a => new { a.FiltrOprId, a.FiltrOprDes, a.LogicOprCd });

            //FilterOperatorViewModel result = _mapper.Map<FilterOperatorViewModel>(list);
            return Ok(list);
        }

        [HttpGet("GetLogicOperators")]
        public IActionResult GetLogicOperators()
        {
            var list = _repo.GetFilterOperators().Where(a => a.LogicOprCd == "Y" && a.FiltrOprId != " IN").AsEnumerable()
                .Select(a => new { a.FiltrOprId, a.FiltrOprDes, a.LogicOprCd });
            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] EventViewModel model)
        {
            var addView = _repo.AddEventView(model.UserId, model.DsplVwNme, model.SiteCntntId, model.DfltVwCd, model.PblcVwCd, model.FiltrCol1OprId, model.FiltrCol2OprId, model.FiltrCol1Id, model.FiltrCol2Id, model.FiltrCol1ValuTxt, model.FiltrCol2ValuTxt, model.FiltrOnCd, model.SortByCol1Id, model.SortByCol1AscOrdrCd, model.SortByCol2Id, model.SortByCol2AscOrdrCd, model.FiltrLogicOprId, model.ColIDs, model.ColPos);

            if (addView)
            {
                _logger.LogInformation($"Event View Created.");
                return Ok(true);
            }

            return BadRequest(new { Message = "An error occurred while attempting to create New Event View" });
        }

        [HttpPut("{viewId}")]
        public IActionResult Put([FromRoute] int viewId, [FromBody] EventViewModel model)
        {
            var addView = _repo.UpdateEventView(viewId, model.UserId, model.DsplVwNme, model.SiteCntntId, model.DfltVwCd, model.PblcVwCd, model.FiltrCol1OprId, model.FiltrCol2OprId, model.FiltrCol1Id, model.FiltrCol2Id, model.FiltrCol1ValuTxt, model.FiltrCol2ValuTxt, model.FiltrOnCd, model.SortByCol1Id, model.SortByCol1AscOrdrCd, model.SortByCol2Id, model.SortByCol2AscOrdrCd, model.FiltrLogicOprId, model.ColIDs, model.ColPos);

            if (addView)
            {
                _logger.LogInformation($"Event View Updated.");
                return Ok(true);
            }

            return BadRequest(new { Message = "An error occurred while attempting to update Event View" });
        }

        [HttpDelete("{viewId}")]
        public IActionResult Delete(int viewId)
        {
            _logger.LogInformation($"Delete View by ViewId: { viewId }");
            var deleteView = _repo.DeleteDsplViewColsByViewId(viewId);

            if (deleteView)
            {
                _logger.LogInformation($"Event View Deleted.");
                return Ok(true);
            }

            return BadRequest(new { Message = "An error occurred while attempting to delete Event View" });
        }
    }
}