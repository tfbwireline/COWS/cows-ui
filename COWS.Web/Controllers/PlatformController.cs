﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Platforms")]
    [ApiController]
    public class PlatformController : ControllerBase
    {
        private readonly IPlatformRepository _repo;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public PlatformController(IMapper mapper,
                               IPlatformRepository repo,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<PlatformViewModel>> Get()
        {
            IEnumerable<PlatformViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.PlatformList, out list))
            {
                list = _mapper.Map<IEnumerable<PlatformViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.PltfrmNme));

                CacheManager.Set(_cache, CacheKeys.PlatformList, list);
            }

            return Ok(list);
        }
    }
}