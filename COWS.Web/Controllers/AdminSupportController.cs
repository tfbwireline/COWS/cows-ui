﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/AdminSupport")]
    [ApiController]
    public class AdminSupportController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IAdminSupportRepository _repo;

        public AdminSupportController(IMapper mapper,
            ILoggedInUserService loggedInUser,
            IAdminSupportRepository repo)
        {
            _mapper = mapper;
            _loggedInUser = loggedInUser;
            _repo = repo;
        }

        [HttpGet("Event/{eventId}")]
        public IActionResult GetEventByEventId([FromRoute] int eventId)
        {

            var tables = _repo.GetEventByEventId(eventId, _loggedInUser.GetLoggedInUserCsgLvlId());

            foreach (DataTable table in tables.Tables)
            {
                foreach (DataColumn col in table.Columns)
                {
                    col.ColumnName = ToCamelCase(ToTitleCase(col.ColumnName));
                }

                if (table.Rows.Count == 0)
                {
                    table.Rows.Add();
                }
            }

            return Ok(tables);
        }

        [HttpGet("Order/{ftn}")]
        public IActionResult GetOrderByFtn([FromRoute] string ftn)
        {

            var tables = _repo.GetOrderByFtn(ftn, _loggedInUser.GetLoggedInUserCsgLvlId());

            foreach (DataTable table in tables.Tables)
            {
                foreach (DataColumn col in table.Columns)
                {
                    col.ColumnName = ToCamelCase(ToTitleCase(col.ColumnName));
                }

                if (table.Rows.Count == 0)
                {
                    table.Rows.Add();
                }
            }

            return Ok(tables);
        }

        [HttpGet("CPT/{cptNo}")]
        public IActionResult GetCPTByCPTNo([FromRoute] string cptNo)
        {

            var tables = _repo.GetCPTOrRedesign(cptNo, _loggedInUser.GetLoggedInUserCsgLvlId(), 0);

            foreach (DataTable table in tables.Tables)
            {
                foreach (DataColumn col in table.Columns)
                {
                    col.ColumnName = ToCamelCase(ToTitleCase(col.ColumnName));
                }

                if (table.Rows.Count == 0)
                {
                    table.Rows.Add();
                }
            }

            return Ok(tables);
        }

        [HttpGet("Redesign/{redesign}")]
        public IActionResult GetRedesign([FromRoute] string redesign)
        {

            var tables = _repo.GetCPTOrRedesign(redesign, _loggedInUser.GetLoggedInUserCsgLvlId(), 1);

            foreach (DataTable table in tables.Tables)
            {
                foreach (DataColumn col in table.Columns)
                {
                    col.ColumnName = ToCamelCase(ToTitleCase(col.ColumnName));
                }

                if (table.Rows.Count == 0)
                {
                    table.Rows.Add();
                }
            }

            return Ok(tables);
        }

        [HttpGet("BPM/{h6}")]
        public IActionResult GetBPM([FromRoute] string h6)
        {

            var tables = _repo.GetBPM(h6);

            foreach (DataTable table in tables.Tables)
            {
                foreach (DataColumn col in table.Columns)
                {
                    col.ColumnName = ToCamelCase(ToTitleCase(col.ColumnName));
                }

                if (table.Rows.Count == 0)
                {
                    table.Rows.Add();
                }
            }

            return Ok(tables);
        }

        [HttpDelete("Order/{orderId}/delete")]
        public IActionResult DeleteOrder([FromRoute] int orderId)
        {

            int isDeleted = _repo.DeleteOrder(orderId);

            if (isDeleted == 1)
            {
                return Ok(isDeleted);
            }

            //_logger.LogInformation($"Get top 1000 NRMBPM Interface View not found.");
            return BadRequest(new { Message = $"Order Could not be deleted" });
        }

        [HttpPost("Order/Extract")]
        public IActionResult ExtractOrder([FromBody] ExtractOrderViewModel model)
        {

            string orderId = _repo.ExtractOrder(model.M5OrderId, model.RelatedM5OrderId, model.OrderType, model.DeviceId, model.IsTransaction);

            if (orderId != null)
            {
                return Ok(orderId);
            }

            //_logger.LogInformation($"Get top 1000 NRMBPM Interface View not found.");
            return BadRequest(new { Message = $"Order Could not be deleted" });
        }

        private string ToTitleCase(string str)
        {
            return new CultureInfo("en").TextInfo.ToTitleCase(str.ToLower().Replace("_", " ")).Replace(" ", "");
        }

        private string ToCamelCase(string str)
        {
            return $"{str.First().ToString().ToLowerInvariant()}{str.Substring(1)}";
        }
    }
}