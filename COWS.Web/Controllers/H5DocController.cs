﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/H5Docs")]
    [ApiController]
    public class H5DocController : ControllerBase
    {
        private readonly IH5DocRepository _repo;
        private readonly ILogger<H5DocController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public H5DocController(IMapper mapper,
                               IH5DocRepository repo,
                               ILogger<H5DocController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<H5DocViewModel>> Get()
        {
            IEnumerable<H5DocViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.H5DocList, out list))
            {
                list = _mapper.Map<IEnumerable<H5DocViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.FileNme));

                CacheManager.Set(_cache, CacheKeys.H5DocList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search H5 Doc by Id: { id }.");

            var obj = _repo.Find(s => s.H5DocId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<H5DocViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"H5 Doc by Id: { id } not found.");
                return NotFound(new { Message = $"H5 Doc Id: { id } not found." });
            }
        }

        [HttpGet("order/{id}")]
        public IActionResult GetByOrderId([FromRoute] int id)
        {
            _logger.LogInformation($"Search H5 Folder by Order Id: { id }.");

            var obj = _repo.Find(a => a.H5Foldr.Ordr.Any(b => b.OrdrId == id));
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<H5DocViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"H5 Folder by Order Id: { id } not found.");
                return NotFound(new { Message = $"H5 Folder by Order Id: { id } not found." });
            }
        }

        [HttpGet("folder/{id}")]
        public IActionResult GetByFolderId([FromRoute] int id)
        {
            _logger.LogInformation($"Search H5 Folder by Folder Id: { id }.");

            var obj = _repo.Find(a => a.H5FoldrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<H5DocViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"H5 Folder by Folder Id: { id } not found.");
                return NotFound(new { Message = $"H5 Folder by Folder Id: { id } not found." });
            }
        }
    }
}