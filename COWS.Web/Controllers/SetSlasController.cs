﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/SetSlas")]
    [ApiController]
    public class SetSlasController : ControllerBase
    {
        private readonly IXnciMsSlaRepository _repo;
        private readonly ILogger<SetSlasController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public SetSlasController(IMapper mapper,
                               IXnciMsSlaRepository repo,
                               ILogger<SetSlasController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<SetSlasViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<SetSlasViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderByDescending(s => s.PltfrmCd)
                                                                .ThenBy(s => s.OrdrType));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search SLA by Id: { id }.");

            var obj = _repo.Find(s => s.MsSlaId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<SetSlasViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"SLA by Id: { id } not found.");
                return NotFound(new { Message = $"SLA Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] SetSlasViewModel model)
        {
            _logger.LogInformation($"Create Set Slas with Platform code: { model.PltfrmCd }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkXnciMsSla>(model);
                obj.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                obj.CreatDt = DateTime.Now;

                var rep = _repo.Create(obj);
                if (rep != null)
                {
                    _logger.LogInformation($"Set Slas Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/SetSlas/{ rep.MsSlaId }", model);
                }
            }

            return BadRequest(new { Message = "Set Slas Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] SetSlasViewModel model)
        {
            _logger.LogInformation($"Update Slas Id: { id }.");

            var obj = _mapper.Map<LkXnciMsSla>(model);
            _repo.Update(id, obj);
            
            _logger.LogInformation($"Slas Updated. { JsonConvert.SerializeObject(model).ToString() } ");
            return Created($"api/SetSlas/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Set Sla's to 0 for Id: { id }.");

            var Slas = _mapper.Map<IEnumerable<SetSlasViewModel>>(_repo.Find(s => s.MsSlaId == id));
            var obj = _mapper.Map<LkXnciMsSla>(Slas.SingleOrDefault());
            obj.SlaInDayQty = 0;

            _repo.Update(id, obj);

            _logger.LogInformation($"Set Sla's to 0 done. { JsonConvert.SerializeObject(Slas).ToString() } ");
        }
    }
}