﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Data;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Activators")]
    [ApiController]
    public class ActivatorsController : ControllerBase
    {
        private readonly IActivatorsRepository _repo;
        private readonly ILogger<ActivatorsController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public ActivatorsController(IMapper mapper,
                               IActivatorsRepository repo,
                               ILogger<ActivatorsController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("GetAllFailActy/{eventTypeID}")]
        public ActionResult<DataTable> GetAllFailActy([FromRoute] int eventTypeID)
        {
            _logger.LogInformation($"GetAllFailActy by EventTypeId: { eventTypeID }.");

            DataTable dt = _repo.GetAllFailActy(eventTypeID);
            if (dt != null)
            {
                return Ok(dt);
            }
            else
            {
                _logger.LogInformation($"GetAllFailActy by EventTypeId: { eventTypeID } not found.");
                return NotFound(new { Message = $"GetAllFailActy by EventTypeId: { eventTypeID } not found." });
            }
        }

        [HttpGet("GetAllSuccActy/{eventTypeID}")]
        public ActionResult<DataTable> GetAllSuccActy([FromRoute] int eventTypeID)
        {
            _logger.LogInformation($"GetAllSuccActy by EventTypeId: { eventTypeID }.");

            DataTable dt = _repo.GetAllSuccActy(eventTypeID);
            if (dt != null)
            {
                return Ok(dt);
            }
            else
            {
                _logger.LogInformation($"GetAllSuccActy by EventTypeId: { eventTypeID } not found.");
                return NotFound(new { Message = $"GetAllSuccActy by EventTypeId: { eventTypeID } not found." });
            }
        }

        [HttpGet("GetAllFailCodes")]
        public ActionResult<DataTable> GetAllFailCodes()
        {
            _logger.LogInformation($"GetAllFailCodes.");

            DataTable dt = _repo.GetAllFailCodes();
            if (dt != null)
            {
                return Ok(dt);
            }
            else
            {
                _logger.LogInformation($"GetAllFailCodes not found.");
                return NotFound(new { Message = $"GetAllFailCodes not found." });
            }
        }
    }
}