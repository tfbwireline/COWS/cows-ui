﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/OrderAddress")]
    [ApiController]
    public class OrderAddressController : ControllerBase
    {
        private readonly IOrderAddressRepository _repo;
        private readonly ILogger<OrderAddressController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public OrderAddressController(IMapper mapper,
                               IOrderAddressRepository repo,
                               ILogger<OrderAddressController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OrderAddressViewModel>> Get()
        {
            IEnumerable<OrderAddressViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.OrderAddressList, out list))
            {
                list = _mapper.Map<IEnumerable<OrderAddressViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrId));

                CacheManager.Set(_cache, CacheKeys.OrderAddressList, list);
            }
           
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order Address by Order Id: { id }.");

            var obj = _repo.Find(a => a.OrdrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<OrderAddressViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Order Address by Order Id: { id } not found.");
                return NotFound(new { Message = $"Order Address by Order Id: { id } not found." });
            }
        }

        [HttpGet("{id}/{cisLevelId}")]
        public IActionResult Get([FromRoute] int id, [FromRoute] string cisLevelId)
        {
            _logger.LogInformation($"Search Order Address by CisLvlId: { cisLevelId } and Id: { id }.");

            var obj = _repo.Find(a => a.CisLvlType == cisLevelId && a.OrdrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<OrderAddressViewModel>(obj.FirstOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Order Address by CisLvlId: { cisLevelId } and Id: { id } not found.");
                return NotFound(new { Message = $"CisLvlId: { cisLevelId } and Order Id: { id } not found." });
            }
        }

        //[HttpPost]
        //public IActionResult Post([FromBody] OrderAddressViewModel model)
        //{
        //    _logger.LogInformation($"Create Event Type: { model.EventTypeNme }.");

        //    if (ModelState.IsValid)
        //    {
        //        var obj = _mapper.Map<LkEventType>(model);

        //        var loggedInUser = _loggedInUser.GetLoggedInUser();
        //        if (loggedInUser != null)
        //        {
        //            obj.CreatByUserId = loggedInUser.UserId;
        //            obj.CreatDt = DateTime.Now;
        //        }

        //        // Sarah Sandoval [20190915] - Added condition to check if name is duplicate
        //        var duplicate = _repo.Find(i => i.EventTypeNme.Trim().ToLower() == obj.EventTypeNme.Trim().ToLower()).SingleOrDefault();
        //        if (duplicate != null)
        //        {
        //            // Throw duplicate error if name already exists
        //            return BadRequest(new { Message = obj.EventTypeNme + " already exists." });
        //        }

        //        var rep = _repo.Create(obj);
        //        if (rep != null)
        //        {
        //            // Update Cache
        //            _cache.Remove(CacheKeys.EventTypeList);
        //            Get();

        //            _logger.LogInformation($"Event Type Created. { JsonConvert.SerializeObject(model).ToString() } ");
        //            return Created($"api/EventTypes/{ rep.EventTypeId }", model);
        //        }
        //    }

        //    return BadRequest(new { Message = "Event Type Could Not Be Created." });
        //}

        //[HttpPut("{id}")]
        //public IActionResult Put([FromRoute] int id, [FromBody] OrderAddressViewModel model)
        //{
        //    _logger.LogInformation($"Update Event Type Id: { id }.");

        //    var obj = _mapper.Map<LkEventType>(model);

        //    var loggedInUser = _loggedInUser.GetLoggedInUser();
        //    if (loggedInUser != null)
        //    {
        //        obj.ModfdByUserId = loggedInUser.UserId;
        //        obj.ModfdDt = DateTime.Now;
        //    }

        //    // Sarah Sandoval [20190915] - Added condition to check if name is duplicate
        //    var duplicate = _repo.Find(i => i.EventTypeNme.Trim().ToLower() == obj.EventTypeNme.Trim().ToLower()).SingleOrDefault();
        //    if (duplicate != null)
        //    {
        //        // Throw duplicate error if name already exists
        //        return BadRequest(new { Message = obj.EventTypeNme + " already exists." });
        //    }

        //    _repo.Update(id, obj);

        //    // Update Cache
        //    _cache.Remove(CacheKeys.EventTypeList);
        //    Get();

        //    _logger.LogInformation($"Event Type Updated. { JsonConvert.SerializeObject(model) } ");
        //    return Created($"api/EventTypes/{ id }", model);
        //}

        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //    _logger.LogInformation($"Deleting Event Type by Id: { id }.");
        //    _repo.Delete(id);

        //    // Update Cache
        //    _cache.Remove(CacheKeys.EventTypeList);
        //    Get();
        //    _logger.LogInformation($"Event Type by Id: { id } has been deleted.");
        //}
    }
}
