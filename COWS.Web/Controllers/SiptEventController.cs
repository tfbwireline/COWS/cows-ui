﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/SiptEvent")]
    [ApiController]
    public class SiptEventController : ControllerBase
    {
        private readonly ISIPTEventRepository _repo;
        private readonly ILogger<SiptEventController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IL2PInterfaceService _l2pInterface;
        private readonly IUserRepository _userRepository;
        private readonly IApptRepository _ApptRepository;
        private readonly IEmailReqRepository _emailReqRepository;
        private readonly IEventRuleRepository _evntRuleRepository;
        private readonly IEventHistoryRepository _evntHistRepository;
        private readonly IEventAsnToUserRepository _repoEventAsnToUser;
        private readonly IEventLockRepository _evntLockRepo;
        private readonly ICommonRepository _commonRepo;
        private IMemoryCache _cache;

        public SiptEventController(IMapper mapper,
                               ISIPTEventRepository repo,
                               ILogger<SiptEventController> logger,
                               ILoggedInUserService loggedInUser,
                               IL2PInterfaceService l2pInterface,
                               IUserRepository userRepository,
                               IApptRepository ApptRepository,
                               IEmailReqRepository emailReqRepository,
                               IEventRuleRepository evntRuleRepository,
                               IEventHistoryRepository evntHistRepository,
                               IEventAsnToUserRepository repoEventAsnToUser,
                               IEventLockRepository evntLockRepo,
                               ICommonRepository commonRepo,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _l2pInterface = l2pInterface;
            _userRepository = userRepository;
            _ApptRepository = ApptRepository;
            _emailReqRepository = emailReqRepository;
            _evntRuleRepository = evntRuleRepository;
            _evntHistRepository = evntHistRepository;
            _repoEventAsnToUser = repoEventAsnToUser;
            _evntLockRepo = evntLockRepo;
            _commonRepo = commonRepo;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search SIPT Event by Id: { id }.");

            var obj = _repo.GetById(id);
            if (obj != null)
            {
                var sipt = _mapper.Map<SiptEventViewModel>(obj);

                if (!string.IsNullOrWhiteSpace(sipt.WrkDes))
                {
                    sipt.WrkDes = sipt.WrkDes.Replace("<br/>", "\r\n");
                }

                if (sipt.ReqorUserId.HasValue && sipt.ReqorUserId.GetValueOrDefault() > 0)
                {
                    var reqUsr = _userRepository.GetById(sipt.ReqorUserId.GetValueOrDefault());
                    if (reqUsr != null)
                    {
                        sipt.ReqorUserName = reqUsr.FullNme;
                        sipt.ReqorUserPhone = reqUsr.PhnNbr;
                    }
                }

                var lockUser = _evntLockRepo.CheckLock(id);
                if (lockUser != null && _loggedInUser.GetLoggedInUserId() != lockUser.LockByUserId)
                {
                    sipt.IsLocked = true;
                    sipt.LockedBy = lockUser.LockByUser.FullNme;
                }

                if (obj.Event.CsgLvlId > 0)
                {
                    _commonRepo.LogWebActivity(((byte)WebActyType.EventDetails),
                        obj.EventId.ToString(), _loggedInUser.GetLoggedInUserAdid(), 
                        _loggedInUser.GetLoggedInUserCsgLvlId(), obj.Event.CsgLvlId);
                }

                return Ok(sipt);
            }
            else
            {
                _logger.LogInformation($"SIPT Event by Id: { id } not found.");
                return NotFound(new { Message = $"SIPT Event Id: { id } not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] SiptEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.TnsTgCd = model.TnsTgCd.HasValue ? model.TnsTgCd.Value : false;
                SiptModel SiptEvent = _mapper.Map<SiptModel>(model);
                SiptEvent.CreatDt = DateTime.Now;
                SiptEvent.CreatByUserId = _loggedInUser.GetLoggedInUserId();
               // SiptEvent.TnsTgCd = model.TnsTgCd;

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(SiptEvent.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                SiptEvent.EventCsgLvlId = _l2pInterface.GetH1CsgLevelId(SiptEvent.H1);

                if (model.SiptEventDoc != null && model.SiptEventDoc.Count() > 0)
                {
                    List<SiptEventDoc> siptDocs = new List<SiptEventDoc>();
                    foreach (SiptEventDocViewModel doc in model.SiptEventDoc)
                    {
                        SiptEventDoc newDoc = new SiptEventDoc();
                        newDoc.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        newDoc.CreatDt = DateTime.Now;
                        newDoc.RecStusId = (byte)ERecStatus.Active;
                        newDoc.EventId = doc.EventId > 0 ? doc.EventId : 0;
                        newDoc.FileNme = doc.FileNme;
                        newDoc.FileSizeQty = doc.FileSizeQty;
                        newDoc.FileCntnt = Convert.FromBase64String(doc.Base64string);
                        siptDocs.Add(newDoc);
                    }

                    SiptEvent.SiptEventDoc = siptDocs;
                }

                if (model.SiptReltdOrdr != null && model.SiptReltdOrdr.Count() > 0)
                {
                    SiptEvent.SiptReltdOrdrNos = model.SiptReltdOrdr.Select(i => i.M5OrdrNbr).ToList();
                }

                // Create Event and Sipt Event
                LkEventRule eventRule = _evntRuleRepository.GetEventRule(SiptEvent.EventStusId,
                    (byte)SiptEvent.WrkflwStusId, _loggedInUser.GetLoggedInUserId(), (int)EventType.SIPT);

                if (eventRule != null)
                {
                    SiptEvent.EventStusId = eventRule.EndEventStusId;
                    var Sipt = _repo.Create(SiptEvent, _loggedInUser.GetLoggedInUser().UserAdid);

                    // Create EventAsnToUser for Calendar Entry
                    var pm = model.PmId.ToString().Select(x => Convert.ToInt32(x.ToString())).ToList();
                    List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                    if (model.PmId != null && model.PmId > 0)
                    {
                        EventAsnToUser s = new EventAsnToUser();
                        s.EventId = Sipt.EventId;
                        s.AsnToUserId = (int)model.PmId;
                        s.CreatDt = Sipt.CreatDt;
                        s.RecStusId = 0;
                        s.RoleId = 23;
                        assignUsers.Add(s);
                        _repoEventAsnToUser.Create(assignUsers);
                    }

                    if (Sipt != null)
                    {
                        _logger.LogInformation($"SIPT Event Created. { JsonConvert.SerializeObject(Sipt, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                        // Do things needed based on eventRule
                        EventHist evntHist = new EventHist
                        {
                            EventId = Sipt.EventId,
                            ActnId = eventRule.ActnId,
                            CmntTxt = SiptEvent.WrkDes,
                            EventStrtTmst = SiptEvent.CustReqStDt,
                            EventEndTmst = SiptEvent.CustReqEndDt,
                            CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                            CreatDt = DateTime.Now
                        };
                        _evntHistRepository.CreateEventHistory(evntHist);

                        EventWorkflow esw = new EventWorkflow();
                        esw.CsgLvlId = Sipt.Event.CsgLvlId;
                        esw.AssignUser = assignUsers;
                        esw.EventId = Sipt.EventId;
                        esw.UserId = _loggedInUser.GetLoggedInUserId();
                        esw.EventRule = eventRule;
                        esw.OldStartTime = Sipt.CustReqStDt.GetValueOrDefault();
                        esw.OldEndTime = Sipt.CustReqEndDt.GetValueOrDefault();
                        esw.StartTime = Sipt.CustReqStDt.GetValueOrDefault();
                        esw.EndTime = Sipt.CustReqEndDt.GetValueOrDefault();
                        esw.EventStatusId = Sipt.EventStusId;
                        esw.RequestorId = Sipt.ReqorUserId.GetValueOrDefault();
                        esw.UserId = Sipt.ModfdByUserId == null 
                            ? _loggedInUser.GetLoggedInUserId() : Sipt.ModfdByUserId.GetValueOrDefault();
                        esw.WorkflowId = (int)Sipt.WrkflwStusId;
                        esw.EventTitle = Sipt.EventTitleTxt;
                        esw.Comments = Sipt.WrkDes.Replace("<br/>", "\r\n");
                        esw.TeamPdlNme = Sipt.TeamPdlNme;
                        esw.SiptProdTypeId = model.SiptProdTypeId;
                        esw.SiptReltdOrdrNos = model.SiptReltdOrdrNos;
                        esw.SiptEventTollTypeIds = model.SiptEventTollTypeIds;
                        esw.SiptEventActyIds = model.SiptEventActyIds;
                        esw.M5OrdrNbr = Sipt.M5OrdrNbr;
                        esw.H1 = Sipt.H1;
                        esw.H6 = Sipt.H6;
                        esw.SiteId = Sipt.SiteId;
                        //esw.SiteAdr = Sipt.SiteAdr;
                        esw.FlrBldgNme = Sipt.FlrBldgNme;
                        esw.CtyNme = Sipt.CtyNme;
                        esw.SttPrvnNme = Sipt.SttPrvnNme;
                        esw.CtryRgnNme = Sipt.CtryRgnNme;
                        esw.ZipCd = Sipt.ZipCd;
                        esw.UsIntlCd = Sipt.UsIntlCd;
                        esw.SiteCntctNme = Sipt.SiteCntctNme;
                        esw.SiteCntctPhnNbr = Sipt.SiteCntctPhnNbr;
                        esw.SiteCntctHrNme = Sipt.SiteCntctHrNme;
                        esw.ConferenceBridgeNbr = Sipt.SiteCntctPhnNbr;
                        esw.ConferenceBridgePin = Sipt.SiteCntctHrNme;
                        esw.SiptDesgnDoc = Sipt.SiptDesgnDoc;
                        esw.EventTypeId = (int)EventType.SIPT;
                        esw.NewEvent = false;
                        esw.EventType = "SIPT";

                        if (_ApptRepository.CalendarEntry(esw))
                            _logger.LogInformation($"Calender Entry success for EventID. { JsonConvert.SerializeObject(Sipt.EventId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                        if (_emailReqRepository.SIPTSendMail(esw))
                            _logger.LogInformation($"Email Entry success for EventID. { JsonConvert.SerializeObject(Sipt.EventId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                        return Created($"api/SiptEvent/{ Sipt.EventId}", new { EventId = Sipt.EventId });
                    }
                }
                else
                {
                    return BadRequest(new { Message = "Not authorize to perform this operation Check your Profile" });
                }
            }

            return BadRequest(new { Message = "SIPT Event Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] SiptEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.TnsTgCd = model.TnsTgCd.HasValue ? model.TnsTgCd.Value : false;
                SiptModel SIPT = _mapper.Map<SiptModel>(model);
                SIPT.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                SIPT.ModfdDt = DateTime.Now;
                // _repo.Update(id, SIPT);
                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(SIPT.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }
                SiptEvent OldSipt = _repo.GetById(id);

                // Get CsgLvlId by H1
                SIPT.EventCsgLvlId = _l2pInterface.GetH1CsgLevelId(SIPT.H1);

                if (model.SiptEventDoc != null && model.SiptEventDoc.Count() > 0)
                {
                    List<SiptEventDoc> siptDocs = new List<SiptEventDoc>();
                    foreach (SiptEventDocViewModel doc in model.SiptEventDoc)
                    {
                        SiptEventDoc newDoc = new SiptEventDoc();
                        newDoc.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        newDoc.CreatDt = DateTime.Now;
                        newDoc.RecStusId = (byte)ERecStatus.Active;
                        newDoc.EventId = doc.EventId > 0 ? doc.EventId : 0;
                        newDoc.FileNme = doc.FileNme;
                        newDoc.FileSizeQty = doc.FileSizeQty;
                        newDoc.FileCntnt = Convert.FromBase64String(doc.Base64string);
                        siptDocs.Add(newDoc);
                    }

                    SIPT.SiptEventDoc = siptDocs;
                }

                if (model.SiptReltdOrdr != null && model.SiptReltdOrdr.Count() > 0)
                {
                    SIPT.SiptReltdOrdrNos = model.SiptReltdOrdr.Select(i => i.M5OrdrNbr).ToList();
                }

                LkEventRule eventRule = _evntRuleRepository.GetEventRule(SIPT.EventStusId,
                    (byte)SIPT.WrkflwStusId, _loggedInUser.GetLoggedInUserId(), (int)EventType.SIPT);

                if (eventRule != null)
                {
                    SIPT.EventStusId = eventRule.EndEventStusId;
                    SIPT.TnsTgCd = model.TnsTgCd;
                    var Upd = _repo.Update(id, SIPT);
                    var sipt = _repo.GetById(id);

                    _logger.LogInformation($"SIPT Event Updated. { JsonConvert.SerializeObject(sipt, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    // Create EventAsnToUser for Calendar Entry
                    var pm = model.PmId.ToString().Select(x => Convert.ToInt32(x.ToString())).ToList();
                    List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                    if (model.PmId != null && model.PmId > 0)
                    {
                        EventAsnToUser s = new EventAsnToUser();
                        s.EventId = id;
                        s.AsnToUserId = (int)model.PmId;
                        s.CreatDt = sipt.CreatDt;
                        // PM inactivates only if Rejected and Reschedule
                        s.RecStusId = 1; // Updated by Sarah Sandoval [20220502] to fix INC70253110
                        s.RoleId = 23;
                        assignUsers.Add(s);
                        _repoEventAsnToUser.Update(id, assignUsers);
                    }

                    EventWorkflow esw = new EventWorkflow();
                    esw.CsgLvlId = sipt.Event.CsgLvlId;
                    esw.AssignUser = assignUsers;
                    esw.EventId = id;
                    esw.UserId = _loggedInUser.GetLoggedInUserId();
                    esw.EventRule = eventRule;
                    esw.OldStartTime = (DateTime)OldSipt.CustReqStDt;
                    esw.OldEndTime = (DateTime)OldSipt.CustReqEndDt;
                    esw.StartTime = (DateTime)sipt.CustReqStDt;
                    esw.EndTime = (DateTime)sipt.CustReqEndDt;
                    esw.EventStatusId = sipt.EventStusId;
                    esw.RequestorId = (int)sipt.ReqorUserId;
                    esw.UserId = sipt.ModfdByUserId == null ? _loggedInUser.GetLoggedInUserId() : (int)sipt.ModfdByUserId;
                    esw.WorkflowId = (int)sipt.WrkflwStusId;
                    esw.EventTitle = sipt.EventTitleTxt;
                    esw.Comments = sipt.WrkDes.Replace("<br/>", "\r\n");
                    esw.TeamPdlNme = sipt.TeamPdlNme;
                    esw.SiptProdTypeId = model.SiptProdTypeId;
                    esw.M5OrdrNbr = sipt.M5OrdrNbr;
                    esw.H1 = sipt.H1;
                    esw.H6 = sipt.H6;
                    esw.SiteId = sipt.SiteId;
                    esw.SiteAdr = sipt.StreetAdr;
                    esw.FlrBldgNme = sipt.FlrBldgNme;
                    esw.CtyNme = sipt.CtyNme;
                    esw.SttPrvnNme = sipt.SttPrvnNme;
                    esw.CtryRgnNme = sipt.CtryRgnNme;
                    esw.ZipCd = sipt.ZipCd;
                    esw.UsIntlCd = sipt.UsIntlCd;
                    esw.SiteCntctNme = sipt.SiteCntctNme;
                    esw.SiteCntctPhnNbr = sipt.SiteCntctPhnNbr;
                    esw.SiteCntctHrNme = sipt.SiteCntctHrNme;
                    esw.SiptDesgnDoc = sipt.SiptDesgnDoc;
                    esw.SiptReltdOrdrNos = SIPT.SiptReltdOrdrNos;
                    esw.SiptEventActyIds = SIPT.SiptEventActyIds;
                    esw.SiptEventTollTypeIds = SIPT.SiptEventTollTypeIds;
                    esw.EventTypeId = (int)EventType.SIPT;
                    esw.NewEvent = false;   //EDIT
                    esw.EventType = "SIPT";

                    if (_ApptRepository.CalendarEntry(esw))
                        _logger.LogInformation($"Calender Entry success for EventID. { JsonConvert.SerializeObject(id, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    if (_emailReqRepository.SIPTSendMail(esw))
                        _logger.LogInformation($"Email Entry success for EventID. { JsonConvert.SerializeObject(id, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    // Do things needed based on eventRule
                    EventHist evntHist = new EventHist
                    {
                        EventId = id,
                        ActnId = eventRule.ActnId,
                        CmntTxt = SIPT.WrkDes,
                        EventStrtTmst = SIPT.CustReqStDt,
                        EventEndTmst = SIPT.CustReqEndDt,
                        CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                        CreatDt = DateTime.Now
                    };
                    _evntHistRepository.CreateEventHistory(evntHist,null, EventType.SIPT);
                    _logger.LogInformation($"Event History Created for EventID. { JsonConvert.SerializeObject(id, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    return Created($"api/SiptEvent/{ sipt.EventId}", new { EventId = sipt.EventId });
                }
                else
                {
                    return BadRequest(new { Message = "Not authorize to perform this operation Check your Profile" });
                }
            }

            return BadRequest(new { Message = "SIPT Event Could Not Be Updated." });
        }

        [HttpPost("Download")]
        public IActionResult Download([FromBody] SiptEventFileDownloadViewModel model)
        {
            byte[] fileCntnt = Convert.FromBase64String(model.Base64string);

            try
            {
                return new FileContentResult(fileCntnt, "application/octet")
                {
                    FileDownloadName = model.FileName
                };
            }
            catch (Exception)
            {
                return NotFound(new { Message = $"SIPT Event Doc File not found." });
            }
        }

        [HttpGet("getSiptActyType")]
        public ActionResult<IEnumerable<SiptActyTypeViewModel>> getSiptActyType()
        {
            IEnumerable<SiptActyTypeViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.SiptActyTypeList, out list))
            {
                list = _mapper.Map<IEnumerable<SiptActyTypeViewModel>>(_repo.GetAllSIPTActyType());

                CacheManager.Set(_cache, CacheKeys.SiptActyTypeList, list);
            }

            return Ok(list);
        }

        [HttpGet("getSiptProdType")]
        public ActionResult<IEnumerable<SiptProdTypeViewModel>> getSiptProdType()
        {
            IEnumerable<SiptProdTypeViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.SiptProdTypeList, out list))
            {
                list = _mapper.Map<IEnumerable<SiptProdTypeViewModel>>(_repo.GetAllSIPTProdTypes());

                CacheManager.Set(_cache, CacheKeys.SiptProdTypeList, list);
            }

            return Ok(list);
        }

        [HttpGet("getSiptTollType")]
        public ActionResult<IEnumerable<SiptTollTypeViewModel>> getSiptTollType()
        {
            IEnumerable<SiptTollTypeViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.SiptTollTypeList, out list))
            {
                list = _mapper.Map<IEnumerable<SiptTollTypeViewModel>>(_repo.GetAllSIPTTollTypes());

                CacheManager.Set(_cache, CacheKeys.SiptTollTypeList, list);
            }

            return Ok(list);
        }

        [HttpGet("GetDocument/{id}")]
        public IActionResult GetSiptDocument([FromRoute] int id)
        {
            _logger.LogInformation($"Search SIPT Document by Id: { id }.");

            var obj = _repo.GetSiptDocument(id);
            if (obj != null)
            {
                DocEntityViewModel doc = new DocEntityViewModel();
                doc.DocId = obj.SiptEventDocId;
                doc.FileNme = obj.FileNme;
                doc.FileSizeQty = obj.FileSizeQty;
                doc.FileCntnt = obj.FileCntnt;
                doc.Base64string = Convert.ToBase64String(obj.FileCntnt);
                doc.Id = obj.EventId;
                doc.CreatByUserId = obj.CreatByUserId;
                doc.CreatDt = obj.CreatDt;

                return Ok(doc);
            }
            else
            {
                _logger.LogInformation($"SIPT Document by Id: { id } not found.");
                return NotFound(new { Message = $"SIPT Document Id: { id } not found." });
            }
        }
    }
}