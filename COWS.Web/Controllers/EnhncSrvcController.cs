﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EnhncSrvc")]
    [ApiController]
    public class EnhncSrvcController : ControllerBase
    {
        private readonly IEnhncSrvcRepository _repo;
        private readonly ILogger<EnhncSrvcController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        public EnhncSrvcController(IMapper mapper,
                               IEnhncSrvcRepository repo,
                               ILogger<EnhncSrvcController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EnhncSrvcViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<EnhncSrvcViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.EnhncSrvcNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Enhanced Service Tier by Id: { id }.");

            var obj = _repo.Find(s => s.EnhncSrvcId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<EnhncSrvcViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Enhanced Service by Id: { id } not found.");
                return NotFound(new { Message = $"Enhanced Service Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] EnhncSrvcViewModel model)
        {
            _logger.LogInformation($"Create Enhanced Service: { model.EnhncSrvcNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkEnhncSrvc>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkEnhncSrvc();
                var duplicate = _repo.Find(i => i.EnhncSrvcNme.Trim().ToLower() == obj.EnhncSrvcNme.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.EnhncSrvcNme + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.EventTypeId = obj.EventTypeId;
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.EnhncSrvcId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }
                
                if (newData != null)
                {
                    _logger.LogInformation($"Enhanced Service Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/EnhncSrvc/{ newData.EnhncSrvcId }", model);
                }
            }

            return BadRequest(new { Message = "Enhanced Service Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] EnhncSrvcViewModel model)
        {
            _logger.LogInformation($"Update Enhanced Service Id: { id }.");

            var obj = _mapper.Map<LkEnhncSrvc>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.EnhncSrvcNme.Trim().ToLower() == obj.EnhncSrvcNme.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.EnhncSrvcNme + " already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.EnhncSrvcId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Enhanced Service Updated. { JsonConvert.SerializeObject(model).ToString() } ");
            return Created($"api/EnhncSrvc/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivate Enhanced Service by Id: { id }.");

            var enhncSrvc = _repo.Find(s => s.EnhncSrvcId == id);
            if (enhncSrvc != null)
            {
                var obj = _mapper.Map<LkEnhncSrvc>(enhncSrvc.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);

                _logger.LogInformation($"Enhanced Service by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Enhanced Service by Id: { id } not found.");
            }
        }
    }
}
