﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MplsAccessBandwidth")]
    [ApiController]
    public class MplsAccessBandwidthController : ControllerBase
    {
        private readonly IMplsAccessBandwidthRepository _repo;
        private readonly ILogger<MplsAccessBandwidthController> _logger;
        private readonly IMapper _mapper;

        public MplsAccessBandwidthController(IMapper mapper,
                               IMplsAccessBandwidthRepository repo,
                               ILogger<MplsAccessBandwidthController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MplsAccessBandwidthViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MplsAccessBandwidthViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.MplsAccsBdwdId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MPLS Access Bandwidth by Id: { id }.");

            var obj = _repo.Find(s => s.MplsAccsBdwdId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MplsAccessBandwidthViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MPLS Access Bandwidth by Id: { id } not found.");
                return NotFound(new { Message = $"MPLS Access Bandwidth Id: { id } not found." });
            }
        }
    }
}