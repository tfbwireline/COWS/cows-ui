﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Cities")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ICityRepository _repo;
        private readonly IMapper _mapper;

        public CityController(IMapper mapper,
                               ICityRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CityViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CityViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.CtyNme));
            return Ok(list);
        }
    }
}