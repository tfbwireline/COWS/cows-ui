﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/XnciRegion")]
    [ApiController]
    public class XnciRegionController : ControllerBase
    {
        private readonly IXnciRegionRepository _repo;
        private readonly ILogger<XnciRegionController> _logger;
        private readonly IMapper _mapper;

        public XnciRegionController(IMapper mapper,
                               IXnciRegionRepository repo,
                               ILogger<XnciRegionController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<XnciRegionViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<XnciRegionViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.RgnId)
                                                                .ThenBy(s => s.RgnDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Xnci Region by Id: { id }.");

            var obj = _repo.Find(s => s.RgnId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<XnciRegionViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Xnci Region by  Id: { id } not found.");
                return NotFound(new { Message = $"Xnci Region Id: { id } not found." });
            }
        }
    }
}