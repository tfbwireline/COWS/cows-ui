﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/NRMBPMInterface")]
    [ApiController]
    public class NRMBPMInterfaceController : ControllerBase
    {
        private readonly INRMBPMInterfaceRepository _nrmbpmInterfaceRepo;
        private readonly ILogger<NRMBPMInterfaceController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public NRMBPMInterfaceController(IMapper mapper,
                               INRMBPMInterfaceRepository nrmbpmInterfaceRepo,
                               ILogger<NRMBPMInterfaceController> logger,
                               IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _nrmbpmInterfaceRepo = nrmbpmInterfaceRepo;
            _logger = logger;
            _cache = memoryCache;
        }

        // GET: api/<controller>
        [HttpGet]
        public ActionResult<IEnumerable<GetNRMBPMInterfaceViewModel>> Get()
        {
            IEnumerable<GetNRMBPMInterfaceViewModel> nrmbpmInterfaceView;
            if (!_cache.TryGetValue(CacheKeys.NRMBPMInterfaceView, out nrmbpmInterfaceView))
            {
                _logger.LogInformation($"Get top 1000 NRMBPM Interface View");
                var data = _nrmbpmInterfaceRepo.GetNRMBPMInterfaceView();

                if (data != null)
                {
                    // Key not in cache, so get data.
                    nrmbpmInterfaceView = _mapper.Map<IEnumerable<GetNRMBPMInterfaceViewModel>>(data);
                    CacheManager.Set(_cache, CacheKeys.NRMBPMInterfaceView, nrmbpmInterfaceView);
                }
                else
                {
                    _logger.LogInformation($"Get top 1000 NRMBPM Interface View not found.");
                    return NotFound(new { Message = $"Top top 1000 NRMBPM Interface View not found." });
                }
            }
            return Ok(nrmbpmInterfaceView);
        }
    }
}