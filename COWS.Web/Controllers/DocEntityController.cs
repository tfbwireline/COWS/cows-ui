﻿using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/DocEntity")]
    [ApiController]
    public class DocEntityController : ControllerBase
    {
        public DocEntityController()
        {
            
        }

        [HttpPost("Download")]
        public IActionResult Download([FromBody] DocEntityViewModel doc)
        {
            byte[] fileCntnt = Convert.FromBase64String(doc.Base64string);

            try
            {
                return new FileContentResult(fileCntnt, "application/octet")
                {
                    FileDownloadName = doc.FileNme
                };
            }
            catch (Exception)
            {
                return NotFound(new { Message = $"File not found." });
            }
        }
    }
}
