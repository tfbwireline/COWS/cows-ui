﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventDeviceCompletion")]
    [ApiController]
    public class EventDeviceCompletionController : ControllerBase
    {
        private readonly IEventDeviceCompletionRepository _repo;
        private readonly ILogger<EventDeviceCompletionController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public EventDeviceCompletionController(IMapper mapper,
                               IEventDeviceCompletionRepository repo,
                               ILogger<EventDeviceCompletionController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EventDeviceCompletionViewModel>> Get()
        {
            IEnumerable<EventDeviceCompletionViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.EventDeviceCompletionList, out list))
            {
                list = _mapper.Map<IEnumerable<EventDeviceCompletionViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OdieDevNme));

                CacheManager.Set(_cache, CacheKeys.EventDeviceCompletionList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Event Device Completion by Id: { id }.");

            var obj = _repo.GetById(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<EventDeviceCompletionViewModel>(obj));
            }
            else
            {
                _logger.LogInformation($"Event Device Completion by Id: { id } not found.");
                return NotFound(new { Message = $"Event Device Completion Id: { id } not found." });
            }
        }

        [HttpGet("/GetByEventId/{id}")]
        public ActionResult<IEnumerable<EventDeviceCompletionViewModel>> GetByEventId([FromRoute] int id)
        {
            IEnumerable<EventDeviceCompletionViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.EventDeviceCompletionList, out list))
            {
                list = _mapper.Map<IEnumerable<EventDeviceCompletionViewModel>>(_repo
                                                                .Find(i => i.EventId == id)
                                                                .OrderBy(s => s.OdieDevNme));

                CacheManager.Set(_cache, CacheKeys.EventDeviceCompletionList, list);
            }

            return Ok(list);
        }
    }
}