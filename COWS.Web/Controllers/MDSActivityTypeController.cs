﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSActivityTypes")]
    [ApiController]
    public class MDSActivityTypeController : ControllerBase
    {
        private readonly IMDSActivityTypeRepository _repo;
        private readonly ILogger<MDSActivityTypeController> _logger;
        private readonly IMapper _mapper;

        public MDSActivityTypeController(IMapper mapper,
                               IMDSActivityTypeRepository repo,
                               ILogger<MDSActivityTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MDSActivityTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MDSActivityTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .Where(s => !s.MdsActyTypeDes.Contains("Install"))
                                                                .OrderBy(s => s.MdsActyTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MDS Activity Type by Id: { id }.");

            var obj = _repo.Find(s => s.MdsActyTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSActivityTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDS Activity Type by Id: { id } not found.");
                return NotFound(new { Message = $"MDS Activity Type Id: { id } not found." });
            }
        }
    }
}