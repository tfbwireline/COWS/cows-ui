﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CustomerUserProfile")]
    [ApiController]
    public class CustomerUserProfileController : ControllerBase
    {
        private readonly ICustomerUserProfileRepository _repo;
        private readonly ILogger<CustomerUserProfileController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public CustomerUserProfileController(IMapper mapper,
                               ICustomerUserProfileRepository repo,
                               ILogger<CustomerUserProfileController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CustomerUserProfileViewModel>> Get()
        {
            IEnumerable<CustomerUserProfileViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.CustomerUserProfileList, out list))
            {
                var data = _repo.GetAllH5Customer();
                list = _mapper.Map<IEnumerable<CustomerUserProfileViewModel>>(data);

                CacheManager.Set(_cache, CacheKeys.CustomerUserProfileList, list);
            }

            return Ok(list);
        }

        //[HttpGet("{id}")]
        [HttpGet("CheckH5/{id}")]
        public bool CheckH5(int id)
        {
            _logger.LogInformation($"Search H5 Customer by Id: { id }.");
            var data = _repo.CheckH5(id);
            if (data == false)
                _logger.LogInformation($"Search H5 Customer by Id: { id } not found.");
            return data;
        }

        //[HttpGet("{id}")]
        //public IActionResult Get([FromRoute] int id)
        //{
        //    _logger.LogInformation($"Search Telco by Id: { id }.");

        //    var obj = _repo.Find(s => s.TelcoId == id);
        //    if (obj != null)
        //    {
        //        return Ok(_mapper.Map<TelcoViewModel>(obj.SingleOrDefault()));
        //    }
        //    else
        //    {
        //        _logger.LogInformation($"Telco by Id: { id } not found.");
        //        return NotFound(new { Message = $"Telco Id: { id } not found." });
        //    }
        //}

        [HttpPost]
        public IActionResult Post([FromBody] CustomerUserProfileViewModel model)
        {
            _logger.LogInformation($"Create H5 Customer: { model.custID }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<CustomerUserProfileView>(model);

                var rep = _repo.CreateH5CustomerToUserAssignment(obj);
                //var createdEmail = _repo.SendEmailNtfy(obj);
                if (rep != 0)
                {
                    // Update Cache
                    var data = _repo.GetAllH5Customer();
                    var list = _mapper.Map<IEnumerable<CustomerUserProfileViewModel>>(data);
                    CacheManager.Set(_cache, CacheKeys.CustomerUserProfileList, list);

                    _logger.LogInformation($"H5 Cutomer to User Assignment Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/h5-customer-assgnmnt/", model);
                }
            }

            return BadRequest(new { Message = "An error occured attempting to insert." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] CustomerUserProfileViewModel model)
        {
            _logger.LogInformation($"Update H5 Cutomer to User Assignment Id: { model.custID }.");

            var obj = _mapper.Map<CustomerUserProfileView>(model);

            _repo.Update(id, obj);
            // Update Cache
            var data = _repo.GetAllH5Customer();
            var list = _mapper.Map<IEnumerable<CustomerUserProfileViewModel>>(data);
            //var list = _mapper.Map<IEnumerable<CustomerUserProfileViewModel>>(_repo
            //                                                    .Find(s => s.CUST_NME != null)
            //                                                    .OrderBy(s => s.CUST_NME));

            CacheManager.Set(_cache, CacheKeys.CustomerUserProfileList, list);
            _logger.LogInformation($"H5 Cutomer to User Assignment Updated. { JsonConvert.SerializeObject(model).ToString() } ");
            return Created($"api/h5-customer-assgnmnt/", model.custID);
        }

        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //    //_logger.LogInformation($"Delete Telco Id: { id }.");
        //    //_repo.Delete(id);
        //    //_logger.LogInformation($"Telco Id { id } Deleted.");
        //    _logger.LogInformation($"Deactivate Telco Id: { id }.");

        //    var Telco = _mapper.Map<IEnumerable<TelcoViewModel>>(_repo.Find(s => s.TelcoId == id));
        //    var obj=_mapper.Map<LkTelco>(Telco.SingleOrDefault());

        //    // Remove this comment once logged in user service is available
        //    var loggedInUser = _loggedInUser.GetLoggedInUser();
        //    if (loggedInUser != null)
        //    {
        //        obj.ModfdByUserId = loggedInUser.UserId;
        //        obj.ModfdDt = DateTime.Now;
        //    }
        //    obj.RecStusId = 0;
        //    _repo.Update(id, obj);
        //    // Update Cache
        //    var list = _mapper.Map<IEnumerable<TelcoViewModel>>(_repo
        //                                                .Find(s => s.RecStusId == 1)
        //                                                .OrderBy(s => s.TelcoNme));

        //    CacheManager.Set(_cache, CacheKeys.TelcoList, list);
        //    _logger.LogInformation($"Telco Deactivate. { JsonConvert.SerializeObject(Telco).ToString() } ");
        //}
    }
}