﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/RedesignDoc")]
    [ApiController]
    public class RedesignDocController : ControllerBase
    {
        private readonly IRedesignDocRepository _repo;
        private readonly ILogger<RedesignDocController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public RedesignDocController(IMapper mapper,
                                IRedesignDocRepository repo,
                               ILogger<RedesignDocController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        //[HttpGet("GetRedesignDocsByRedesignId")]
        //public IActionResult GetRedesignDocsByRedesignId(int rId)
        //{
        //    var docList = _repo.Select("FileNme ASC", string.Format("RedsgnId == {0}", rId));

        //    return Ok(docList);
        //}

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var doc = _repo.Get(id);

            return Ok(doc);
        }

        [HttpGet("GetDocument/{id}")]
        public IActionResult GetDocument([FromRoute] int id)
        {
            _logger.LogInformation($"Search Redesign Document by Id: { id }.");

            var obj = _repo.GetById(id);
            if (obj != null)
            {
                DocEntityViewModel doc = new DocEntityViewModel();
                doc.DocId = obj.RedsgnDocId;
                doc.FileNme = obj.FileNme;
                doc.FileSizeQty = obj.FileSizeQty;
                doc.FileCntnt = obj.FileCntnt;
                doc.Base64string = Convert.ToBase64String(obj.FileCntnt);
                doc.Id = obj.RedsgnId;
                doc.CreatByUserId = obj.CreatByUserId;
                doc.CreatDt = obj.CreatDt.GetValueOrDefault();

                return Ok(doc);
            }
            else
            {
                _logger.LogInformation($"Redesign Document by Id: { id } not found.");
                return NotFound(new { Message = $"Redesign Document Id: { id } not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromForm] RedesignDocViewModel model)
        {
            _logger.LogInformation($"Create Redesign Doc File: { model.FileNme }.");

            model.CreatByUserId = _loggedInUser.GetLoggedInUserId();
            model.CreatByUserName = _loggedInUser.GetLoggedInUser().UserAdid;
            model.CreatDt = DateTime.Now;
            model.UploadFile = null;
            return Created($"api/RedesignDoc/", model);
        }

        [HttpGet("Download")]
        public IActionResult Download([FromQuery] string fileName, [FromQuery] string base64string)
        {
            byte[] fileCntnt = Convert.FromBase64String(base64string);

            try
            {
                return new FileContentResult(fileCntnt, "application/octet")
                {
                    FileDownloadName = fileName
                };
            }
            catch (Exception)
            {
                return NotFound(new { Message = $"Redesign Doc File not found." });
            }
        }
    }
}