﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/OrderSubTypes")]
    [ApiController]
    public class OrderSubTypeController : ControllerBase
    {
        private readonly IOrderSubTypeRepository _repo;
        private readonly ILogger<OrderSubTypeController> _logger;
        private readonly IMapper _mapper;

        public OrderSubTypeController(IMapper mapper,
                               IOrderSubTypeRepository repo,
                               ILogger<OrderSubTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OrderSubTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<OrderSubTypeViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrSubTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order Sub Type by Id: { id }.");

            var obj = _repo.Find(s => s.OrdrSubTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<OrderSubTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Order Sub Type by Id: { id } not found.");
                return NotFound(new { Message = $"Order Sub Type Id: { id } not found." });
            }
        }
    }
}