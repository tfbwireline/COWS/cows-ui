﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/PrimSites")]
    [ApiController]
    public class CptPrimSiteController : ControllerBase
    {
        private readonly ICptPrimSiteRepository _repo;
        private readonly ILogger<CptPrimSiteController> _logger;
        private readonly IMapper _mapper;

        public CptPrimSiteController(IMapper mapper,
                               ICptPrimSiteRepository repo,
                               ILogger<CptPrimSiteController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptPrimSiteViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptPrimSiteViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptPrimSite));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT Prim Site by Id: { id }.");

            var obj = _repo.Find(s => s.CptPrimSiteId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptPrimSiteViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT Prim Site by Id: { id } not found.");
                return NotFound(new { Message = $"CPT Prim Site Id: { id } not found." });
            }
        }
    }
}