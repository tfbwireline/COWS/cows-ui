﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/AccessCitySites")]
    [ApiController]
    public class AccessCitySiteController : ControllerBase
    {
        private readonly IAccessCitySiteRepository _repo;
        private readonly ILogger<AccessCitySiteController> _logger;
        private readonly IMapper _mapper;

        public AccessCitySiteController(IMapper mapper,
                               IAccessCitySiteRepository repo,
                               ILogger<AccessCitySiteController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AccessCitySiteViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<AccessCitySiteViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(i => i.AccsCtyNmeSiteCd));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Access City Site by Id: { id }.");

            var obj = _repo.Find(s => s.AccsCtySiteId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<AccessCitySiteViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Access City Site by Id: { id } not found.");
                return NotFound(new { Message = $"Access City Site Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] AccessCitySiteViewModel model)
        {
            _logger.LogInformation($"Create Access City Site: { model.AccsCtyNmeSiteCd }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkAccsCtySite>(model);
                obj.CreatDt = DateTime.Now;

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var duplicate = _repo.Find(i => i.AccsCtyNmeSiteCd.Trim().ToLower() == obj.AccsCtyNmeSiteCd.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    return BadRequest(new { Message = obj.AccsCtyNmeSiteCd + " already exists." });
                }

                var rep = _repo.Create(obj);
                if (rep != null)
                {
                    _logger.LogInformation($"Access City Site Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/AccessCitySites/{ rep.AccsCtySiteId }", model);
                }
            }

            return BadRequest(new { Message = "Access City Site Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] AccessCitySiteViewModel model)
        {
            _logger.LogInformation($"Update Access City Site Id: { id }.");

            var obj = _mapper.Map<LkAccsCtySite>(model);

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.AccsCtyNmeSiteCd.Trim().ToLower() == obj.AccsCtyNmeSiteCd.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null && duplicate.AccsCtySiteId != obj.AccsCtySiteId)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                return BadRequest(new { Message = obj.AccsCtyNmeSiteCd + " already exists." });
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Access City Site Updated. { JsonConvert.SerializeObject(obj) } ");
            return Created($"api/AccessCitySites/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deleting Access City Site by Id: { id }.");

            var city = _repo.Find(s => s.AccsCtySiteId == id);
            if (city != null)
            {
                _repo.Delete(id);

                _logger.LogInformation($"Access City Site by Id: { id } Deleted.");
            }
            else
            {
                _logger.LogInformation($"Deleting record failed due to Access City Site by Id: { id } not found.");
            }
        }
    }
}