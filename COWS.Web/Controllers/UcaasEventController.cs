﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Events/Ucaas")]
    [ApiController]
    public class UcaasEventController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUcaasEventRepository _repo;
        private readonly IEventAsnToUserRepository _repoEventAsnToUser;
        private readonly IEventRuleRepository _repoEventRule;
        private readonly IEventHistoryRepository _repoEventHistory;
        private readonly IApptRepository _repoAppt;
        private readonly IEmailReqRepository _repoEmailReq;
        private readonly ICommonRepository _commonRepo;
        private readonly ICustScrdDataRepository _custScrdRepo;
        private readonly ISearchRepository _searchRepo;
        private readonly ILogger<UcaasEventController> _logger;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IL2PInterfaceService _l2pInterface;
        private IMemoryCache _cache;

        public UcaasEventController(IMapper mapper,
                               IUcaasEventRepository repo,
                               IEventAsnToUserRepository repoEventAsnToUser,
                               IEventRuleRepository repoEventRule,
                               IEventHistoryRepository repoEventHistory,
                               IApptRepository repoAppt,
                               IEmailReqRepository repoEmailReq,
                               ICommonRepository commonRepo,
                               ICustScrdDataRepository custScrdRepo,
                               ISearchRepository searchRepo,
                               ILogger<UcaasEventController> logger,
                               ILoggedInUserService loggedInUser,
                               IL2PInterfaceService l2pInterface,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _repoEventAsnToUser = repoEventAsnToUser;
            _l2pInterface = l2pInterface;
            _repoEventRule = repoEventRule;
            _repoEventHistory = repoEventHistory;
            _repoAppt = repoAppt;
            _repoEmailReq = repoEmailReq;
            _commonRepo = commonRepo;
            _custScrdRepo = custScrdRepo;
            _searchRepo = searchRepo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UcaasEventViewModel>> Get()
        {
            IEnumerable<UcaasEventViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.UcaasEventList, out list))
            {
                list = _mapper.Map<IEnumerable<UcaasEventViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.EventId));

                CacheManager.Set(_cache, CacheKeys.UcaasEventList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search UCaaS Event by Id: { id }.");
            string adid = _loggedInUser.GetLoggedInUserAdid();

            //var obj = _repo.Find(s => s.EventId == id, adid).FirstOrDefault();
            var ucaas = _repo.GetById(id);
            if (ucaas != null)
            {
                // Handle Secured Data and Web Activity Log
                if (ucaas.Event.CsgLvlId > 0)
                {
                    var csd = _custScrdRepo
                        .Find(i => i.ScrdObjId == id && i.ScrdObjTypeId == (byte)SecuredObjectType.UCaaS_EVENT)
                        .FirstOrDefault();

                    if (csd != null)
                    {
                        if (!ucaas.UcaaSActyTypeId.Equals((short)UcaasActivityType.Disconnect))
                        {
                            ucaas.InstlSitePocNme = _commonRepo.GetDecryptValue(csd.CustCntctNme);
                            ucaas.InstlSitePocPhnNbr = _commonRepo.GetDecryptValue(csd.CustCntctPhnNbr);
                            ucaas.InstlSitePocCellPhnNbr = _commonRepo.GetDecryptValue(csd.CustCntctCellPhnNbr);
                            ucaas.SrvcAssrnPocNme = _commonRepo.GetDecryptValue(csd.SrvcAssrnPocNme);
                            ucaas.SrvcAssrnPocPhnNbr = _commonRepo.GetDecryptValue(csd.SrvcAssrnPocPhnNbr);
                            ucaas.SrvcAssrnPocCellPhnNbr = _commonRepo.GetDecryptValue(csd.SrvcAssrnPocCellPhnNbr);
                            ucaas.StreetAdr = _commonRepo.GetDecryptValue(csd.StreetAdr1);
                            ucaas.FlrBldgNme = _commonRepo.GetDecryptValue(csd.FlrId);
                            ucaas.CtyNme = _commonRepo.GetDecryptValue(csd.CtyNme);
                            ucaas.SttPrvnNme = _commonRepo.GetDecryptValue(csd.SttPrvnNme);
                            ucaas.CtryRgnNme = _commonRepo.GetDecryptValue(csd.CtryRgnNme);
                            ucaas.ZipCd = _commonRepo.GetDecryptValue(csd.ZipPstlCd);
                        }

                        ucaas.CustNme = _commonRepo.GetDecryptValue(csd.CustNme);
                        ucaas.EventTitleTxt = _commonRepo.GetDecryptValue(csd.EventTitleTxt);
                        ucaas.CustAcctTeamPdlNme = _commonRepo.GetDecryptValue(csd.CustEmailAdr);
                    }

                    _commonRepo.LogWebActivity((byte)WebActyType.EventDetails,
                        ucaas.EventId.ToString(), _loggedInUser.GetLoggedInUserAdid(),
                        _loggedInUser.GetLoggedInUserCsgLvlId(), ucaas.Event.CsgLvlId);
                }

                // Handle H6/CCD Combination
                if (ucaas.Event.EventCpeDev != null && ucaas.Event.EventCpeDev.Count > 0
                    && ucaas.EventStusId != (byte)EventStatus.Completed)
                {
                    //ReloadCPEData();
                    string siteAddress = string.Empty;
                    List<EventCpeDev> ucaasCpeDev = new List<EventCpeDev>();
                    List<EventDevCmplt> ucaasDevCmplt = new List<EventDevCmplt>();
                    List<EventDevSrvcMgmtViewModel> ucaasDevSrvcMgmt = new List<EventDevSrvcMgmtViewModel>();
                    if (!string.IsNullOrWhiteSpace(ucaas.H6) && ucaas.Ccd.HasValue)
                    {
                        GetM5EventDatabyH6Results res = _searchRepo.M5LookupByH6CCD(ucaas.H6, ucaas.Ccd.ToString(), "Y", false);
                        if (res != null)
                        {
                            var siteAdr = res.M5AdrTable.FirstOrDefault();
                            if (siteAdr != null)
                            {
                                siteAddress = siteAdr.SiteID + "_" + siteAdr.City + "," + siteAdr.ProvState;
                            }

                            if (res.M5CpeTable != null && res.M5CpeTable.Count() > 0)
                            {
                                List<EventCpeDev> cpeDev = res.M5CpeTable
                                                                    .Select(a => new EventCpeDev
                                                                    {
                                                                        CpeOrdrId = a.CPEOrdrID.ToString(),
                                                                        CpeOrdrNbr = a.M5OrdrNbr,
                                                                        DeviceId = a.DeviceID,
                                                                        AssocH6 = a.AssocH6,
                                                                        ReqstnNbr = a.RqstnNbr,
                                                                        MaintVndrPo = a.MntVndrPO,
                                                                        OdieCd = false
                                                                    })
                                                                    .ToList();

                                // CPE Dev Table
                                //if (ucaas.Event.EventCpeDev != null && ucaas.Event.EventCpeDev.Count > 0)
                                //{
                                //    List<EventCpeDev> notInEventCpeDev = new List<EventCpeDev>();
                                //    foreach (EventCpeDev ci in cpeDev)
                                //    {
                                //        var cpeDevItem = ucaas.Event.EventCpeDev.FirstOrDefault(a => a.CpeOrdrId == ci.CpeOrdrId && a.DeviceId == ci.DeviceId && a.AssocH6 == ci.AssocH6);
                                //        if (cpeDevItem == null)
                                //        {
                                //            notInEventCpeDev.Add(ci);
                                //        }
                                //    }

                                //    ucaasCpeDev.AddRange(ucaas.Event.EventCpeDev);
                                //    ucaasCpeDev.AddRange(notInEventCpeDev);
                                //}

                                // Dev Complete Table
                                //if (ucaas.Event.EventDevCmplt != null && ucaas.Event.EventDevCmplt.Count > 0)
                                //{
                                //    List<EventDevCmplt> notInEventDevCmplt = new List<EventDevCmplt>();
                                //    foreach (EventDevCmplt dc in ucaas.Event.EventDevCmplt)
                                //    {
                                //        var eventDevCmplt = cpeDev.FirstOrDefault(a => !string.IsNullOrWhiteSpace(a.OdieDevNme) && a.OdieDevNme.Equals(dc.OdieDevNme));
                                //        if (eventDevCmplt == null)
                                //        {
                                //            notInEventDevCmplt.Add(dc);
                                //        }
                                //    }

                                //    ucaasDevCmplt.AddRange(ucaas.Event.EventDevCmplt);
                                //    ucaasDevCmplt.AddRange(notInEventDevCmplt);
                                //}

                                // Device Service Management Table
                                if (cpeDev != null && cpeDev.Count > 0
                                    && ucaas.Event.EventCpeDev != null && ucaas.Event.EventCpeDev.Count > 0
                                    && ((ucaas.Event.EventDevSrvcMgmt != null && ucaas.Event.EventDevSrvcMgmt.Count > 0)
                                        || (ucaas.Event.EventDevSrvcMgmt != null && ucaas.Event.EventDevSrvcMgmt.Count == 0
                                            && (ucaas.WrkflwStusId == 4 || ucaas.WrkflwStusId == 2))))
                                {
                                    foreach (EventCpeDev item in ucaas.Event.EventCpeDev)
                                    {
                                        var devSrvcMgmtItem = ucaas.Event.EventDevSrvcMgmt.FirstOrDefault(i => i.OdieDevNme == item.OdieDevNme);
                                        if (ucaas.UcaaSProdTypeId == 1)
                                        {
                                            ucaasDevSrvcMgmt.Add(new EventDevSrvcMgmtViewModel
                                            {
                                                OdieDevNme = item.OdieDevNme,
                                                OdieCd = devSrvcMgmtItem != null ? devSrvcMgmtItem.OdieCd : false,
                                                MnsSrvcTierId = devSrvcMgmtItem != null ? devSrvcMgmtItem.MnsSrvcTierId : (byte)0
                                            });
                                        }
                                        else
                                        {
                                            var devItem = ucaas.Event.EventDevSrvcMgmt.FirstOrDefault(i => i.OdieDevNme == item.OdieDevNme && i.DeviceId == item.DeviceId);
                                            ucaasDevSrvcMgmt.Add(new EventDevSrvcMgmtViewModel
                                            {
                                                DeviceId = item.DeviceId,
                                                MnsOrdrId = int.Parse(item.CpeOrdrId),
                                                MnsOrdrNbr = item.CpeOrdrNbr,
                                                OdieDevNme = item.OdieDevNme,
                                                OdieCd = devSrvcMgmtItem != null ? devSrvcMgmtItem.OdieCd : false,
                                                MnsSrvcTierId = devItem != null ? devItem.MnsSrvcTierId : (byte)0
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //ucaas.Event.EventCpeDev = ucaasCpeDev;
                    //ucaas.Event.EventDevCmplt = ucaasDevCmplt;

                    var ucaasVM = _mapper.Map<UcaasEventViewModel>(ucaas);
                    ucaasVM.SiteAdr = siteAddress;
                    ucaasVM.EventDevServiceMgmt = ucaasDevSrvcMgmt;

                    return Ok(ucaasVM);
                }

                return Ok(_mapper.Map<UcaasEventViewModel>(ucaas));
            }
            else
            {
                _logger.LogInformation($"UCaaS Event by Id: { id } not found.");
                return NotFound(new { Message = $"UCaaS Event Id: { id } not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] UcaasEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                UcaaSEvent ucaas = _mapper.Map<UcaaSEvent>(model);

                ucaas.Event = new Event();
                ucaas.Event.EventTypeId = (int)EventType.UCaaS;
                ucaas.CreatDt = ucaas.Event.CreatDt = DateTime.Now;
                ucaas.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                ucaas.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                ucaas.ModfdDt = DateTime.Now;
                ucaas.StreetAdr = model.SiteAdr;
                ucaas.EsclReasId = ucaas.EsclReasId == 0 ? null : ucaas.EsclReasId;
                ucaas.OldEventStusId = ucaas.OldEventStusId == 0 ? null : ucaas.OldEventStusId;
                ucaas.FailReasId = ucaas.FailReasId == 0 ? null : ucaas.FailReasId;
                //ucaas.CnfrcBrdgId = ucaas.CnfrcBrdgId == 0 ? null : ucaas.CnfrcBrdgId;

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(ucaas.H1))
                {
                    _logger.LogInformation("You are not an authorized user for this H1.");
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                ucaas.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(ucaas.H1);

                // Get Event Rule
                LkEventRule eventRule = _repoEventRule
                    .GetEventRule(ucaas.EventStusId, ucaas.WrkflwStusId,
                        _loggedInUser.GetLoggedInUserId(), (int)EventType.UCaaS);

                ucaas.Event.UcaaSEventBilling = model.UcaasEventBilling == null ? null
                    : _mapper.Map<IEnumerable<UcaaSEventBilling>>(model.UcaasEventBilling).ToList();
                ucaas.Event.UcaaSEventOdieDev = model.UcaasEventOdieDevice == null ? null
                    : _mapper.Map<IEnumerable<UcaaSEventOdieDev>>(model.UcaasEventOdieDevice).ToList();
                ucaas.Event.EventCpeDev = model.EventCpeDev == null ? null
                    : _mapper.Map<IEnumerable<EventCpeDev>>(model.EventCpeDev).ToList();
                ucaas.Event.EventDevCmplt = model.EventDeviceCompletion == null ? null
                    : _mapper.Map<IEnumerable<EventDevCmplt>>(model.EventDeviceCompletion).ToList();
                ucaas.Event.EventDevSrvcMgmt = model.EventDevServiceMgmt == null ? null
                    : _mapper.Map<IEnumerable<EventDevSrvcMgmt>>(model.EventDevServiceMgmt).ToList();
                ucaas.Event.EventDiscoDev = model.EventDiscoDev == null ? null
                    : _mapper.Map<IEnumerable<EventDiscoDev>>(model.EventDiscoDev).ToList();

                if (eventRule != null)
                {
                    ucaas.EventStusId = eventRule.EndEventStusId;
                    var ue = _repo.Create(ucaas);

                    if (ue != null)
                    {
                        // Create EventAsnToUser
                        if (model.Activators != null && model.Activators.Count > 0)
                        {
                            _repoEventAsnToUser.Create(model.Activators
                                .Select(a => new EventAsnToUser
                                {
                                    EventId = ue.EventId,
                                    AsnToUserId = a,
                                    CreatDt = DateTime.Now,
                                    RecStusId = 0,
                                    RoleId = 0
                                }).ToList());
                        }
                        List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                        assignUsers = _repoEventAsnToUser.Find(a => a.EventId == ue.EventId).ToList();

                        // Do things needed based on eventRule like event history and email sender
                        // Create Event History
                        EventHist eh = new EventHist();
                        eh.EventId = ue.EventId;
                        eh.ActnId = eventRule.ActnId;
                        eh.EventStrtTmst = ue.StrtTmst;
                        eh.EventEndTmst = ue.EndTmst;
                        eh.CmntTxt = model.CmntTxt;
                        eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        _repoEventHistory.CreateEventHistory(eh);

                        // Save Calendar Entry and Email Sender
                        EventWorkflow esw = SetEventWorkFlow(ue);
                        esw.AssignUser = assignUsers;
                        esw.EventRule = eventRule;
                        esw.ReviewerId = model.ReviewerUserId;
                        esw.IsSupressEmail = model.SuppressEmail;

                        if (_repoAppt.CalendarEntry(esw))
                            _logger.LogInformation($"UCaaS Event Calender Entry success for EventID. { JsonConvert.SerializeObject(model).ToString() } ");

                        if (!model.SuppressEmail)
                        {
                            if (_repoEmailReq.UcaasSendMail(esw))
                                _logger.LogInformation($"UCaaS Event Email Entry success for EventID. { JsonConvert.SerializeObject(model).ToString() } ");
                        }

                        _logger.LogInformation($"UCaaS Event Created. { JsonConvert.SerializeObject(model).ToString() } ");
                        return Created($"api/Events/Ucaas/{ ue.EventId }", new { EventId = ue.EventId });
                    }
                }
            }

            return BadRequest(new { Message = "UCaaS Event Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] UcaasEventViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UcaaSEvent ucaas = _mapper.Map<UcaaSEvent>(model);
                    ucaas.StreetAdr = model.SiteAdr;
                    ucaas.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                    ucaas.ModfdDt = DateTime.Now;
                    ucaas.EventId = id;
                    ucaas.EsclReasId = ucaas.EsclReasId == 0 ? null : ucaas.EsclReasId;
                    ucaas.OldEventStusId = ucaas.OldEventStusId == 0 ? null : ucaas.OldEventStusId;
                    ucaas.FailReasId = ucaas.FailReasId == 0 ? null : ucaas.FailReasId;
                    //ucaas.CnfrcBrdgId = ucaas.CnfrcBrdgId == 0 ? null : ucaas.CnfrcBrdgId;

                    // Validate H1
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(ucaas.H1))
                    {
                        _logger.LogInformation("You are not an authorized user for this H1.");
                        return BadRequest(new { Message = "You are not an authorized user for this H1." });
                    }

                    // Get CsgLvlId by H1
                    ucaas.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(ucaas.H1);

                    LkEventRule eventRule = _repoEventRule
                        .GetEventRule(ucaas.EventStusId, ucaas.WrkflwStusId,
                            _loggedInUser.GetLoggedInUserId(), (int)EventType.UCaaS);

                    ucaas.Event.UcaaSEventBilling = model.UcaasEventBilling == null ? null
                        : _mapper.Map<IEnumerable<UcaaSEventBilling>>(model.UcaasEventBilling).ToList();
                    ucaas.Event.UcaaSEventOdieDev = model.UcaasEventOdieDevice == null ? null
                        : _mapper.Map<IEnumerable<UcaaSEventOdieDev>>(model.UcaasEventOdieDevice).ToList();
                    ucaas.Event.EventCpeDev = model.EventCpeDev == null ? null
                        : _mapper.Map<IEnumerable<EventCpeDev>>(model.EventCpeDev).ToList();
                    ucaas.Event.EventDevCmplt = model.EventDeviceCompletion == null ? null
                        : _mapper.Map<IEnumerable<EventDevCmplt>>(model.EventDeviceCompletion).ToList();
                    ucaas.Event.EventDevSrvcMgmt = model.EventDevServiceMgmt == null ? null
                        : _mapper.Map<IEnumerable<EventDevSrvcMgmt>>(model.EventDevServiceMgmt).ToList();
                    ucaas.Event.EventDiscoDev = model.EventDiscoDev == null ? null
                        : _mapper.Map<IEnumerable<EventDiscoDev>>(model.EventDiscoDev).ToList();

                    if (eventRule != null)
                    {
                        ucaas.EventStusId = eventRule.EndEventStusId;
                        _repo.Update(id, ucaas);

                        // Do things needed based on eventRule like calendar entry and email sender
                        UcaaSEvent ue = _repo.GetById(id);

                        // Create/Update EventAsnToUser
                        List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                        assignUsers = _repoEventAsnToUser.Find(a => a.EventId == ue.EventId).ToList();
                        if (model.Activators != null && model.Activators.Count > 0
                            && assignUsers.Count == 0)
                        {
                            // Create
                            _repoEventAsnToUser.Create(model.Activators
                                .Select(a => new EventAsnToUser
                                {
                                    EventId = ue.EventId,
                                    AsnToUserId = a,
                                    CreatDt = DateTime.Now,
                                    RecStusId = 0,
                                    RoleId = 0
                                }).ToList());
                        }
                        else
                        {
                            // Update
                            _repoEventAsnToUser.Update(id, model.Activators
                                .Select(a => new EventAsnToUser
                                {
                                    EventId = id,
                                    AsnToUserId = a,
                                    CreatDt = DateTime.Now,
                                    RecStusId = 0,
                                    RoleId = 0
                                }).ToList());
                        }

                        assignUsers = _repoEventAsnToUser.Find(a => a.EventId == ue.EventId).ToList();

                        // Event History, Reviewer and Activator Task
                        EventHist eh = new EventHist();
                        eh.EventId = ue.EventId;
                        eh.ActnId = eventRule.ActnId;
                        eh.EventStrtTmst = ue.StrtTmst;
                        eh.EventEndTmst = ue.EndTmst;
                        eh.CmntTxt = model.CmntTxt;
                        eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        int evntHistId = _repoEventHistory.CreateEventHistory(eh);

                        // Save Calendar Entry and Email Sender
                        EventWorkflow esw = SetEventWorkFlow(ue);
                        esw.AssignUser = assignUsers;
                        esw.EventRule = eventRule;
                        esw.ReviewerId = model.ReviewerUserId;
                        esw.IsSupressEmail = model.SuppressEmail;

                        if (_repoAppt.CalendarEntry(esw))
                            _logger.LogInformation($"UCaaS Event Calender Entry success for EventID. { JsonConvert.SerializeObject(model).ToString() } ");

                        if (!model.SuppressEmail)
                        {
                            if (_repoEmailReq.UcaasSendMail(esw))
                                _logger.LogInformation($"UCaaS Event Email Entry success for EventID. { JsonConvert.SerializeObject(model).ToString() } ");
                        }

                        _logger.LogInformation($"UCaaS Event Updated. { JsonConvert.SerializeObject(model).ToString() } ");
                        return Created($"api/Events/Ucaas/{ id }", new { EventId = ue.EventId });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"UCaaS Event Could Not Be Updated. { ex.Message } ");
                return BadRequest(new { Message = "UCaaS Event Could Not Be Updated." });
            }

            return Created($"api/Events/Ucaas/{ id }", _repo.GetById(id));
        }

        private EventWorkflow SetEventWorkFlow(UcaaSEvent ue)
        {
            EventWorkflow esw = new EventWorkflow();
            esw.EventId = ue.EventId;
            esw.Comments = ue.CmntTxt;
            esw.EventStatusId = ue.EventStusId;
            esw.WorkflowId = ue.WrkflwStusId;
            esw.EventTitle = ue.EventTitleTxt;
            esw.EventTypeId = (int)EventType.UCaaS;
            esw.EventType = "UCaaS";
            esw.SOWSEventID = ue.CustSowLocTxt;
            esw.RequestorId = ue.ReqorUserId;
            esw.UserId = _loggedInUser.GetLoggedInUserId();
            esw.ConferenceBridgeNbr = ue.CnfrcBrdgNbr;
            esw.ConferenceBridgePin = ue.CnfrcPinNbr;
            esw.OnlineMeetingAdr = ue.OnlineMeetingAdr;
            esw.StartTime = ue.StrtTmst;
            esw.EndTime = ue.EndTmst;
            esw.CsgLvlId = ue.Event.CsgLvlId;

            return esw;
        }
    }
}