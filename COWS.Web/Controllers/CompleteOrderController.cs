﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using Microsoft.AspNetCore.Mvc;
using COWS.Entities.QueryModels;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

namespace COWS.Web.Controllers
{
   
    [Produces("application/json")]
    [Route("api/CompleteOrder")]
    [ApiController]
    public class CompleteOrderController : ControllerBase
    {
        private readonly ICompleteOrderRepository _repo;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public CompleteOrderController(IMapper mapper,
                               ICompleteOrderRepository repo,                     
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }
        [HttpGet]
        public bool GetAdminExcPerm(string name)
        {
            var result = false;

            DataTable dt = _repo.GetAdminExcPerm("menuM5CmpltMsg");
            for (int i = 0; i < dt.Rows.Count; i++)
                if (dt.Rows[i]["PRMTR_VALU_TXT"].ToString().Contains(name))
                {
                    result = true;
                    break;
                }
            //return Ok(_repo.GetAdminExcPerm(name));
            return result;
        }

        // [HttpGet("M5CmpltMsg")]
        [HttpPost]
        public ActionResult<InsertM5CompleteMsg> M5CmpltMsg(InsertM5CompleteMsg data)
        {
            if (data != null)
            {
                InsertM5CompleteMsg insertM5CompleteMsg = data;
                insertM5CompleteMsg.FTN = data.FTN;
                insertM5CompleteMsg.MsgType = data.MsgType;
                return Ok(_repo.M5CmpltMsg(insertM5CompleteMsg.FTN, insertM5CompleteMsg.MsgType));
            }

            return BadRequest(data);           
        }

    }
}
