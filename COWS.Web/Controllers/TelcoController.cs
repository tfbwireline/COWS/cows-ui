﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Telco")]
    [ApiController]
    public class TelcoController : ControllerBase
    {
        private readonly ITelcoRepository _repo;
        private readonly ILogger<TelcoController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public TelcoController(IMapper mapper,
                               ITelcoRepository repo,
                               ILogger<TelcoController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TelcoViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<TelcoViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.TelcoNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Telco by Id: { id }.");

            var obj = _repo.Find(s => s.TelcoId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<TelcoViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Telco by Id: { id } not found.");
                return NotFound(new { Message = $"Telco Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] TelcoViewModel model)
        {
            _logger.LogInformation($"Create Telco: { model.TelcoNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkTelco>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkTelco();
                var duplicate = _repo.Find(i => i.TelcoNme.Trim().ToLower() == obj.TelcoNme.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.TelcoNme + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.TelcoCntctPhnNbr = obj.TelcoCntctPhnNbr;
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.TelcoId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    _logger.LogInformation($"Telco Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/Telco/{ newData.TelcoId }", model);
                }
            }

            return BadRequest(new { Message = "Telco Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] TelcoViewModel model)
        {
            _logger.LogInformation($"Update Telco Id: { id }.");

            var obj = _mapper.Map<LkTelco>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.TelcoNme.Trim().ToLower() == obj.TelcoNme.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.TelcoNme + " already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.TelcoId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Telco Updated. { JsonConvert.SerializeObject(model).ToString() } ");
            return Created($"api/Telco/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivate Telco by Id: { id }.");

            var telco = _repo.Find(s => s.TelcoId == id);
            if (telco != null)
            {
                var obj = _mapper.Map<LkTelco>(telco.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);

                _logger.LogInformation($"Telco by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Telco by Id: { id } not found.");
            }
        }
    }
}