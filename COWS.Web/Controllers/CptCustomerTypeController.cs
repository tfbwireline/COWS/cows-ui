﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/CustomerTypes")]
    [ApiController]
    public class CptCustomerTypeController : ControllerBase
    {
        private readonly ICptCustomerTypeRepository _repo;
        private readonly ILogger<CptCustomerTypeController> _logger;
        private readonly IMapper _mapper;

        public CptCustomerTypeController(IMapper mapper,
                               ICptCustomerTypeRepository repo,
                               ILogger<CptCustomerTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptCustomerTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptCustomerTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptCustTypeId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT Customer Type by Id: { id }.");

            var obj = _repo.Find(s => s.CptCustTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptCustomerTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT Customer Type by Id: { id } not found.");
                return NotFound(new { Message = $"CPT Customer Type Id: { id } not found." });
            }
        }
    }
}