﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.Controllers
{
    [Route("api/IPManagement")]
    [ApiController]
    public class IpManagementController : ControllerBase
    {
        private readonly IIpManagementRepository _repo;
        private readonly ILogger<IpManagementController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;


        public IpManagementController(IMapper mapper,
                               IIpManagementRepository repo,
                               ILogger<IpManagementController> logger,
                               ILoggedInUserService loggedInUser
                               )
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var record = _mapper.Map<IEnumerable<IpMstrViewModel>>(_repo.GetAll());
            return Ok(record);
        }

        [HttpGet("GetIPRecords/{id}")]
        public IActionResult GetIPRecords([FromRoute] int id)
        {

            var record = _repo.GetIPRecords(id);
            if (record != null)
            {
                return Ok(record);
            }
            else
            {
                _logger.LogInformation($"IP_MSTR records for: { id } not found.");
                return NotFound(new { Message = $"IP_MSTR records for: { id } not found." });
            }
        }

        [HttpGet("GetIpMstrRelatedAdress/{id}")]
        public IActionResult GetIpMstrRelatedAdress([FromRoute] int id)
        {

            var record = _repo.GetIpMstrRelatedAdress(id);
            if (record != null)
            {
                return Ok(record);
            }
            else
            {
                _logger.LogInformation($"IP_MSTR records for: { id } not found.");
                return NotFound(new { Message = $"IP_MSTR records for: { id } not found." });
            }
        }


        [HttpGet("GetRelatedActyForSelectedIPAddress")]
        public IActionResult GetRelatedActyForSelectedIPAddress([FromQuery] int id, [FromQuery] int type)
        {

            var record = _repo.GetRelatedActyForSelectedIPAddress(id, type);
            if (record != null)
            {
                return Ok(record);
            }
            else
            {
                _logger.LogInformation($"Related ACTY records for: { id } not found.");
                return NotFound(new { Message = $"Related ACTY records for: { id } not found." });
            }
        }
    }
}
