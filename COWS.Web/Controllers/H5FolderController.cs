﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/H5Folders")]
    [ApiController]
    public class H5FolderController : ControllerBase
    {
        private readonly IH5FolderRepository _repo;
        private readonly ICommonRepository _commonRepo;
        private readonly IH5DocRepository _repoDoc;
        private readonly IL2PInterfaceService _l2pInterface;
        private readonly ILogger<H5FolderController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public H5FolderController(IMapper mapper,
                               IH5FolderRepository repo,
                               ICommonRepository commonRepo,
                               IH5DocRepository repoDoc,
                               IL2PInterfaceService l2pInterface,
                               ILogger<H5FolderController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _commonRepo = commonRepo;
            _repoDoc = repoDoc;
            _l2pInterface = l2pInterface;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<H5FolderViewModel>> Get()
        {
            IEnumerable<H5FolderViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.H5FolderList, out list))
            {
                list = _mapper.Map<IEnumerable<H5FolderViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.CustNme));

                CacheManager.Set(_cache, CacheKeys.H5FolderList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search H5 Folder by Id: { id }.");

            var obj = _repo.GetById(id, _loggedInUser.GetLoggedInUserAdid());
            if (obj != null)
            {
                return Ok(_mapper.Map<H5FolderViewModel>(obj));
            }
            else
            {
                _logger.LogInformation($"H5 Folder by Id: { id } not found.");
                return NotFound(new { Message = $"H5 Folder Id: { id } not found." });
            }
        }

        [HttpGet("SearchByParams")]
        public ActionResult<IEnumerable<H5FolderViewModel>> SearchByParams([FromQuery] int custId, [FromQuery] string custNme,
            [FromQuery] string m5Ctn, [FromQuery] string city, [FromQuery] string country)
        {
            _logger.LogInformation($"Search H5 Folder by H6: { custId }, Customer Name: { custNme }, M5 Order: { m5Ctn }, City: { city }, Country: { country }.");

            // IsUserAuthorizedToWorkOnH6
            if (custId > 0 && !_l2pInterface.chkM5Validation(string.Empty, custId.ToString(), string.Empty))
            {
                return BadRequest(new { Message = "You are not an authorized user for this H5." });
            }

            // IsUserAuthorizedToWorkOnM5Ordr
            if (!string.IsNullOrWhiteSpace(m5Ctn) && !_l2pInterface.chkM5Validation(string.Empty, string.Empty, m5Ctn))
            {
                return BadRequest(new { Message = "You are not an authorized user for this M5 Order." });
            }

            var obj = _repo.Search(0, custId, m5Ctn ?? "", custNme ?? "", city ?? "", country ?? "", _loggedInUser.GetLoggedInUserId());

            if (obj.Count() == 1)
            {
                var data = obj.FirstOrDefault();

                if(data.CsgLvlId > 0)
                {
                    var adid = _loggedInUser.GetLoggedInUserAdid();
                    var userCsg = _loggedInUser.GetLoggedInUserCsgLvlId();
                    _commonRepo.LogWebActivity((byte)WebActyType.H5FolderSearch, data.H5FoldrId.ToString(), adid, (byte)userCsg, data.CsgLvlId);
                }
            }

            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<H5FolderViewModel>>(obj.OrderBy(s => s.CustId)));
            }

            //var obj = _repo.Find(i => i.CustNme.Trim().Contains(string.IsNullOrWhiteSpace(custNme) ? string.Empty : custNme)
            //                        || i.CustCtyNme.Trim().Contains(string.IsNullOrWhiteSpace(city) ? string.Empty : city));

            //if (obj != null)
            //{
            //    if (custId > 0 && !string.IsNullOrWhiteSpace(custId.ToString()))
            //    {
            //        obj = obj.Where(i => i.CustId.ToString().Equals(custId.ToString()));
            //    }

            //    if (!string.IsNullOrWhiteSpace(country))
            //    {
            //        obj = obj.Where(i => i.CtryCd.Trim().ToUpper().Equals(string.IsNullOrWhiteSpace(country) ? string.Empty : country.Trim().ToUpper()));
            //    }

            //    if (!string.IsNullOrWhiteSpace(m5Ctn))
            //    {
            //        //return Ok(_mapper.Map<IEnumerable<H5FolderViewModel>>(obj
            //        //    .Where(i => i.Ordr.Where(o => o.OrdrId.ToString().Contains(m5Ctn)) != null)
            //        //    .OrderBy(s => s.CustId)));
            //        obj = obj.Where(i => i.Ordr.Where(o => o.OrdrId.ToString().Contains(m5Ctn)) != null);
            //    }

            //    return Ok(_mapper.Map<IEnumerable<H5FolderViewModel>>(obj.OrderBy(s => s.CustId)));
            //}
            else
            {
                _logger.LogInformation($"H5 Folder by H6: { custId }, Customer Name: { custNme }, M5 Order: { m5Ctn }, City: { city }, Country: { country } not found.");
                return NotFound(new { Message = $"H5 Folder by H6: { custId }, Customer Name: { custNme }, M5 Order: { m5Ctn }, City: { city }, Country: { country } not found." });
            }
        }

        [HttpGet("GetOrderByH5FolderId/{id}")]
        public IActionResult GetOrderByH5FolderId([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order by H5 Folder Id: { id }.");

            var obj = _repo.GetH5FolderAssociations(id);
            if (obj != null)
            {
                return Ok(obj);
            }
            else
            {
                _logger.LogInformation($"Search Order by H5 Folder Id: { id } not found.");
                return NotFound(new { Message = $"Search Order by H5 Folder Id: { id } not found." });
            }
        }

        [HttpGet("order/{id}")]
        public IActionResult GetByOrderId([FromRoute] int id)
        {
            _logger.LogInformation($"Search H5 Folder by Order Id: { id }.");

            var obj = _repo.Find(a => a.Ordr.Any(b => b.OrdrId == id));
            if (obj != null)
            {
                // GetDecryptValue
                var data = _mapper.Map<H5FolderViewModel>(obj.SingleOrDefault());

                if(data != null)
                {
                    if (data.CsgLvlId == 0)
                    {
                        // Do nothing
                    }
                    else if (_loggedInUser.GetLoggedInUserCsgLvlId() != 0 && data.CsgLvlId > 0 && _loggedInUser.GetLoggedInUserCsgLvlId() <= data.CsgLvlId)
                    {
                        CustScrdData securedData = _commonRepo.GetSecuredData(data.H5FoldrId, (int)SecuredObjectType.H5_FOLDR);

                        data.CustNme = _commonRepo.GetDecryptValue(securedData.CustNme);
                        data.CtryCd = _commonRepo.GetDecryptValue(securedData.CtryCd);
                        data.CustCtyNme = _commonRepo.GetDecryptValue(securedData.CtyNme);
                    }
                    else
                    {
                        data.CustNme = string.Empty;
                        data.CtryCd = string.Empty;
                        data.CustCtyNme = string.Empty;
                    }
                    return Ok(data);
                }

                return Ok();
            }
            else
            {
                _logger.LogInformation($"H5 Folder by Order Id: { id } not found.");
                return NotFound(new { Message = $"H5 Folder by Order Id: { id } not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] H5FolderViewModel model)
        {
            if (ModelState.IsValid)
            {
                H5Foldr h5 = _mapper.Map<H5Foldr>(model);
                h5.CtryCd = model.CtryCd; // Included for some reason not handled by the mapper
                h5.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                h5.CreatDt = DateTime.Now;
                h5.ModfdByUserId = null;
                h5.ModfdDt = null;

                // Validate H5
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(h5.CustId.ToString()))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H5." });
                }

                // Get CsgLvlId by H5
                h5.CsgLvlId = _l2pInterface.GetH1CsgLevelId(h5.CustId.ToString());

                // Check for unique Customer ID
                var searchH5 = _repo.Find(i => i.CustId == h5.CustId).ToList();

                if (searchH5 != null && searchH5.Count() > 0)
                {
                    return BadRequest(new { Message = "Customer ID must be unique." });
                }

                h5.H5Doc = GetH5Documents(model);

                var h5New = _repo.Create(h5);

                if (h5New != null)

                {
                    // Update Cache
                    _cache.Remove(CacheKeys.H5FolderList);
                    _logger.LogInformation($"H5 Folder Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/H5Folders/{ h5New.H5FoldrId }", h5New);
                }
            }

            return BadRequest(new { Message = "H5 Folder Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] H5FolderViewModel model)
        {
            if (ModelState.IsValid)
            {
                H5Foldr h5 = _mapper.Map<H5Foldr>(model);
                h5.CtryCd = model.CtryCd; // Included for some reason not handled by the mapper
                h5.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                h5.ModfdDt = DateTime.Now;

                // Validate H5
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(h5.CustId.ToString()))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H5." });
                }

                // Get CsgLvlId by H5
                h5.CsgLvlId = _l2pInterface.GetH1CsgLevelId(h5.CustId.ToString());

                //// Check for unique Customer ID
                //var searchH5 = _repo.Find(i => i.CustId == h5.CustId && i.H5FoldrId != h5.H5FoldrId).ToList();

                //if (searchH5 != null && searchH5.Count() > 0)
                //{
                //    return BadRequest(new { Message = "Customer ID must be unique." });
                //}

                h5.H5Doc = GetH5Documents(model);

                _repo.Update(id, h5);

                // Update Cache
                _cache.Remove(CacheKeys.H5FolderList);

                _logger.LogInformation($"H5 Folder Updated. { JsonConvert.SerializeObject(model).ToString() } ");
                return Created($"api/H5Folders/{ id }", h5);
            }

            return BadRequest(new { Message = "H5 Folder Could Not Be Updated." });
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deleting H5 Folder by Id: { id }.");

            _repo.Delete(id);

            _logger.LogInformation($"H5 Folder by Id: { id } has been deleted.");

            //var h5Folder = _repo.Find(s => s.H5FoldrId == id);
            //if (h5Folder != null)
            //{
            //    var obj = _mapper.Map<H5Foldr>(h5Folder.SingleOrDefault());
            //    var loggedInUser = _loggedInUser.GetLoggedInUser();
            //    if (loggedInUser != null)
            //    {
            //        obj.ModfdByUserId = loggedInUser.UserId;
            //        obj.ModfdDt = DateTime.Now;
            //        obj.RecStusId = (byte)ERecStatus.InActive;
            //    }

            //    _repo.Update(id, obj);

            //    // Update Cache
            //    _cache.Remove(CacheKeys.H5FolderList);
            //    _logger.LogInformation($"H5 Folder by Id: { id } Deactivated.");
            //}
            //else
            //{
            //    _logger.LogInformation($"Deactivating record failed due to H5 Folder by Id: { id } not found.");
            //}
        }

        private List<H5Doc> GetH5Documents(H5FolderViewModel model)
        {
            List<H5Doc> docs = new List<H5Doc>();

            if (model.H5Docs != null & model.H5Docs.Count() > 0)
            {
                foreach (H5DocViewModel doc in model.H5Docs)
                {
                    var h5Doc = _mapper.Map<H5Doc>(doc);
                    h5Doc.RecStusId = (byte)ERecStatus.Active;
                    if (!string.IsNullOrWhiteSpace(doc.Base64string))
                    {
                        h5Doc.FileCntnt = Convert.FromBase64String(doc.Base64string);
                    }

                    if (doc.H5FoldrId > 0)
                    {
                        // For Update
                        h5Doc.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                        h5Doc.ModfdDt = DateTime.Now;
                    }
                    else
                    {
                        // For Create
                        h5Doc.H5DocId = 0;
                        h5Doc.H5FoldrId = model.H5FoldrId;
                        h5Doc.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        h5Doc.CreatDt = DateTime.Now;
                    }
                    
                    docs.Add(h5Doc);
                }

            }

            return docs;
        }
    }
}