﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Events/Ngvn")]
    [ApiController]
    public class NgvnEventController : ControllerBase
    {
        private readonly INgvnEventRepository _repo;
        private readonly IEventAsnToUserRepository _eventAsnToUserRepository;
        private readonly IUserRepository _userRepository;
        private readonly IEventHistoryRepository _eventHistoryRepository;
        private readonly ILogger<NgvnEventController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IL2PInterfaceService _l2pInterface;
        private readonly IEventFailActyRepository _repoFailEventActy;
        private readonly IEventSucssActyRepository _repoSucssEventActy;
        private readonly IEventRuleRepository _repoEventRule;
        private readonly IApptRepository _repoAppt;
        private readonly IEmailReqRepository _emailReqRepository;

        public NgvnEventController(IMapper mapper,
                               INgvnEventRepository repo,
                               IEventAsnToUserRepository eventAsnToUserRepository,
                               IUserRepository userRepository,
                               IEventHistoryRepository eventHistoryRepository,
                               ILogger<NgvnEventController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache,
                               IL2PInterfaceService l2pInterface,
                               IEventFailActyRepository repoFailEventActy,
                               IEventSucssActyRepository repoSucssEventActy,
                               IEventRuleRepository repoEventRule,
                               IApptRepository repoAppt,
                               IEmailReqRepository emailReqRepository)
        {
            _mapper = mapper;
            _repo = repo;
            _eventAsnToUserRepository = eventAsnToUserRepository;
            _userRepository = userRepository;
            _eventHistoryRepository = eventHistoryRepository;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _l2pInterface = l2pInterface;
            _repoFailEventActy = repoFailEventActy;
            _repoSucssEventActy = repoSucssEventActy;
            _repoEventRule = repoEventRule;
            _repoAppt = repoAppt;
            _emailReqRepository = emailReqRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<NgvnEventViewModel>> Get()
        {
            IEnumerable<NgvnEventViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.NgvnEventList, out list))
            {
                list = _mapper.Map<IEnumerable<NgvnEventViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderByDescending(s => s.EventId));

                CacheManager.Set(_cache, CacheKeys.NgvnEventList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search NGVN Event by Id: { id }.");
            string adid = _loggedInUser.GetLoggedInUserAdid();

            var obj = _repo.Find(s => s.EventId == id, adid);
            if (obj != null)
            {
                return Ok(_mapper.Map<NgvnEventViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"NGVN Event by Id: { id } not found.");
                return NotFound(new { Message = $"NGVN Event Id: { id } not found." });
            }
        }

        [HttpGet("GetNgvnProdType")]
        public ActionResult<IEnumerable<NgvnProdTypeViewModel>> GetNgvnProdType()
        {
            IEnumerable<NgvnProdTypeViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.NgvnProdTypeList, out list))
            {
                list = _mapper.Map<IEnumerable<NgvnProdTypeViewModel>>(_repo.GetNgvnProdType());

                CacheManager.Set(_cache, CacheKeys.NgvnProdTypeList, list);
            }

            return Ok(list);
        }

        [HttpPost]
        public ActionResult Post([FromBody] NgvnEventViewModel model)
        {
            int userId = _loggedInUser.GetLoggedInUserId();

            if (ModelState.IsValid)
            {
                NgvnEvent ngvnEvent = _mapper.Map<NgvnEvent>(model);

                ngvnEvent.Event = new Event();
                ngvnEvent.Event.EventTypeId = (int)EventType.NGVN;
                ngvnEvent.Event.CreatDt = DateTime.Now;
                ngvnEvent.CreatByUserId = userId;
                ngvnEvent.ModfdByUserId = userId;

                // Validate H1
                _logger.LogInformation($"Checking if user is authorized for H1: { ngvnEvent.H1 }.");
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(ngvnEvent.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                ngvnEvent.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(ngvnEvent.H1);

                // Validate M5
                if (!string.IsNullOrWhiteSpace(ngvnEvent.Ftn))
                {
                    _logger.LogInformation($"Checking if user is authorized for FTN: { ngvnEvent.Ftn }.");
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(ngvnEvent.Ftn))
                    {
                        _logger.LogInformation($"Checking if UserID: { userId } is authorized for FTN: { ngvnEvent.Ftn }.");
                        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                    }

                    ngvnEvent.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(string.Empty, string.Empty, ngvnEvent.Ftn);
                }

                LkEventRule eventRule = _repoEventRule.GetEventRule(ngvnEvent.EventStusId, ngvnEvent.WrkflwStusId.Value, userId, (int)EventType.NGVN);

                int profileId = _userRepository.GetFinalUserProfile(_loggedInUser.GetLoggedInUserId(), (int)EventType.NGVN).UsrPrfId;
                if (eventRule != null)
                {
                    ngvnEvent.EventStusId = eventRule.EndEventStusId;
                    var ngvn = _repo.Create(ngvnEvent);

                    if (ngvn != null)
                    {
                        // Create EventAsnToUser
                        List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                        if (model.Activators != null && model.Activators.Count > 0)
                        {
                            assignUsers = _eventAsnToUserRepository.Create(model.Activators
                            .Select(a => new EventAsnToUser
                            {
                                EventId = ngvn.EventId,
                                AsnToUserId = a,
                                CreatDt = DateTime.Now,
                                RecStusId = 0,
                                RoleId = 0
                            }).ToList());
                        }

                        var json = new JsonSerializerSettings()
                        {
                            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                            Formatting = Formatting.Indented
                        };

                        // Create Event History; Reviewer and Activator is not needed for new NGVN Event
                        EventHist eh = new EventHist();
                        eh.EventId = ngvnEvent.EventId;
                        eh.ActnId = eventRule.ActnId;
                        eh.CmntTxt = model.DsgnCmntTxt;
                        eh.EventStrtTmst = ngvnEvent.StrtTmst;
                        eh.EventEndTmst = ngvnEvent.EndTmst;
                        eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        _eventHistoryRepository.CreateEventHistory(eh);

                        // Save Calendar Entry and Email Sender
                        EventWorkflow esw = SetEventWorkFlow(ngvn);
                        esw.AssignUser = assignUsers;
                        esw.EventRule = eventRule;

                        if (_repoAppt.CalendarEntry(esw))
                            _logger.LogInformation($"NGVN Event Calender Entry success for EventID. { JsonConvert.SerializeObject(ngvn.EventId, json) } ");

                        if (_emailReqRepository.SendMail(esw))
                            _logger.LogInformation($"NGVN Event Email Entry success for EventID. { JsonConvert.SerializeObject(ngvn.EventId, json) } ");

                        _logger.LogInformation($"NGVN Event Created. { JsonConvert.SerializeObject(ngvn, json) } ");
                        return Created($"api/Events/Ngvn/{ ngvn.EventId}", ngvn);
                    }
                }
            }
            return BadRequest(new { Message = "NGVN Event Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] NgvnEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                NgvnEvent ngvnEvent = _mapper.Map<NgvnEvent>(model);

                int userId = _loggedInUser.GetLoggedInUserId();
                ngvnEvent.ModfdByUserId = userId;
                ngvnEvent.ModfdDt = DateTime.Now;

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(ngvnEvent.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                ngvnEvent.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(ngvnEvent.H1);

                // Validate M5
                if (!string.IsNullOrWhiteSpace(ngvnEvent.Ftn))
                {
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(ngvnEvent.Ftn))
                    {
                        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                    }

                    ngvnEvent.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(string.Empty, string.Empty, ngvnEvent.Ftn);
                }

                LkEventRule eventRule = _repoEventRule.GetEventRule(ngvnEvent.EventStusId, ngvnEvent.WrkflwStusId.Value, userId, (int)EventType.NGVN);

                if (eventRule != null)
                {
                    // Prceed to update
                    ngvnEvent.EventStusId = eventRule.EndEventStusId;

                    _repo.Update(id, ngvnEvent);
                    //var ngvn = _repo.GetById(id);

                    // Update EventAsnToUser
                    _eventAsnToUserRepository.Update(id, model.Activators
                        .Select(a => new EventAsnToUser
                        {
                            EventId = id,
                            AsnToUserId = a,
                            CreatDt = DateTime.Now,
                            RecStusId = 0,
                            RoleId = 0
                        }).ToList());

                    // Do things needed based on eventRule like calendar entry and email sender
                    NgvnEvent me = _repo.GetById(id);
                    List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                    assignUsers = _eventAsnToUserRepository.Find(a => a.EventId == me.EventId).ToList();

                    var json = new JsonSerializerSettings()
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                        Formatting = Formatting.Indented
                    };

                    EventHist eh = new EventHist();
                    eh.EventId = me.EventId;
                    eh.ActnId = eventRule.ActnId;
                    eh.EventStrtTmst = me.StrtTmst;
                    eh.EventEndTmst = me.EndTmst;
                    eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                    eh.PreCfgCmpltCd = (model.PreCfgConfgCode == "1" || model.PreCfgConfgCode == "Yes") ? "Y" : "N";

                    if (model.ReviewerUserId > 0)
                    {
                        eh.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                        eh.CmntTxt = model.ReviewerComments;
                    }
                    else if (model.ActivatorUserId > 0)
                    {
                        model.PreCfgConfgCode = (string.IsNullOrWhiteSpace(model.PreCfgConfgCode)
                            || model.PreCfgConfgCode == "0") ? "N" : "Y";
                        eh.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                        eh.CmntTxt = model.ActivatorComments;
                    }
                    else if ((me.WrkflwStusId != (byte)WorkflowStatus.Visible
                                || me.WrkflwStusId != (byte)WorkflowStatus.Retract)
                                        && me.EventStusId != (byte)EventStatus.Visible)
                    {
                        eh.CmntTxt = model.DsgnCmntTxt;
                    }
                    int eventHistID = _eventHistoryRepository.CreateEventHistory(eh);

                    if (model.ActivatorUserId > 0)
                    {
                        var Fail = _repoFailEventActy.GetByEventId(me.EventId);
                        if (Fail.Count() > 0)
                        {
                            _repoFailEventActy.DeleteByEventId(me.EventId);
                        }
                        foreach (var es in model.EventFailActyIds)
                        {
                            EventFailActy esa = new EventFailActy();
                            esa.EventHistId = eventHistID;
                            esa.FailActyId = (short)es;
                            esa.RecStusId = (byte)1;
                            esa.CreatDt = DateTime.Now;
                            _repoFailEventActy.CreateActy(esa);
                        }

                        var Sucss = _repoSucssEventActy.GetByEventId(me.EventId);
                        if (Sucss.Count() > 0)
                        {
                            _repoSucssEventActy.DeleteByEventId(me.EventId);
                        }
                        foreach (var es in model.EventSucssActyIds)
                        {
                            EventSucssActy esa = new EventSucssActy();
                            esa.EventHistId = eventHistID;
                            esa.SucssActyId = (short)es;
                            esa.RecStusId = (byte)1;
                            esa.CreatDt = DateTime.Now;
                            _repoSucssEventActy.CreateActy(esa);
                        }
                    }

                    EventWorkflow esw = SetEventWorkFlow(me);
                    esw.AssignUser = assignUsers;
                    esw.EventRule = eventRule;
                    esw.ReviewerId = model.ReviewerUserId;

                    if (_repoAppt.CalendarEntry(esw))
                        _logger.LogInformation($"NGVN Event Calender Entry success for EventID. { JsonConvert.SerializeObject(me.EventId, json) } ");

                    if (_emailReqRepository.SendMail(esw))
                        _logger.LogInformation($"NGVN Event Email Entry success for EventID. { JsonConvert.SerializeObject(me.EventId, json) } ");

                    _logger.LogInformation($"NGVN Event Updated. { JsonConvert.SerializeObject(me, json) } ");
                    return Created($"api/Events/Ngvn/{ id }", me);
                }
            }
            return BadRequest(new { Message = "NGVN Event Could Not Be Updated." });
        }

        private EventWorkflow SetEventWorkFlow(NgvnEvent ngvn)
        {
            EventWorkflow esw = new EventWorkflow();
            esw.EventId = ngvn.EventId;
            esw.Comments = ngvn.DsgnCmntTxt;
            esw.EventStatusId = ngvn.EventStusId;
            esw.WorkflowId = (int)ngvn.WrkflwStusId;
            esw.EventTitle = ngvn.EventTitleTxt;
            esw.EventTypeId = (int)EventType.NGVN;
            esw.EventType = "NGVN";
            esw.SOWSEventID = ngvn.SowsEventId.HasValue ? ngvn.SowsEventId.Value.ToString() : string.Empty;
            esw.RequestorId = (int)ngvn.ReqorUserId;
            esw.UserId = _loggedInUser.GetLoggedInUserId();
            esw.ConferenceBridgeNbr = ngvn.CnfrcBrdgNbr;
            esw.ConferenceBridgePin = ngvn.CnfrcPinNbr;
            esw.StartTime = ngvn.StrtTmst;
            esw.EndTime = ngvn.EndTmst;

            return esw;
        }
    }
}