﻿using System;
using System.Collections.Generic;
using System.Linq;
using COWS.Data.Repository;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace COWS.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BillingDispatchController : ControllerBase
    {
        private readonly ILogger<BillingDispatchController> _logger;
        private readonly IBillingDispatchRepository _repo;
        private readonly ILoggedInUserService _loggedInUser;

        public BillingDispatchController(ILogger<BillingDispatchController> logger,
                                         IBillingDispatchRepository repo,
                                         ILoggedInUserService loggedInUser)
        {
            _logger = logger;
            _repo = repo;
            _loggedInUser = loggedInUser;
        }

        [HttpGet("")]
        public ActionResult<IEnumerable<BillingDispatchData>> Get([FromQuery] bool history)
        {
            _logger.LogInformation("Getting Billing Dispatch Data");

            if(history)
            {
                return Ok(_repo.GetHistory());
            }

            return Ok(_repo.Get());
        }



        [HttpGet("{Id}")]
        public ActionResult<BillingDispatchData> GetById(int Id, [FromQuery] bool fromHistory)
        {
            if (fromHistory)
            {
                return Ok(_repo.GetHistory()
                            .Where(i => i.Id == Id)
                            .FirstOrDefault());
            }

            return Ok(_repo.GetById(Id));
        }

        [HttpPost]
        public ActionResult<BillingDispatchData> Post(BillingDispatchData data)
        {
            if (data != null)
            {
                _logger.LogInformation($"Create Billing Dispatch Data: { Environment.NewLine }{ JsonConvert.SerializeObject(data, Formatting.Indented) }");

                BillingDispatchData billingDispatch = data;

                billingDispatch.CreatedDate = billingDispatch.CreatedDate ?? DateTime.Now;
                billingDispatch.CreatedByUserId = billingDispatch.CreatedByUserId ?? _loggedInUser.GetLoggedInUser().UserId;

                billingDispatch.ModifiedDate = billingDispatch.ModifiedDate ?? DateTime.Now;
                billingDispatch.ModifiedByUserId = _loggedInUser.GetLoggedInUser().UserId;

                billingDispatch.StatusId = 241; // hard coded for web updates

                _repo.Create(billingDispatch);

                return Ok(billingDispatch);
            }

            return BadRequest(data);

        }
    }
}