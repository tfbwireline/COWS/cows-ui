﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSEventSlnkWiredTrpt")]
    [ApiController]
    public class MDSEventSlnkWiredTrptController : ControllerBase
    {
        private readonly IMDSEventSlnkWiredTrptRepository _repo;
        private readonly ILogger<MDSEventSlnkWiredTrptController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MDSEventSlnkWiredTrptController(IMapper mapper,
                               IMDSEventSlnkWiredTrptRepository repo,
                               ILogger<MDSEventSlnkWiredTrptController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"MDSEventSlnkWiredTrpt by EventId: { id }.");

            var obj = _repo.GetMDSEventSlnkWiredTrptByEventId(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSEventSlnkWiredTrptViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDSEventSlnkWiredTrpt by EventId: { id } not found.");
                return NotFound(new { Message = $"MDSEventSlnkWiredTrpt By EventId: { id } not found." });
            }
        }
    }
}