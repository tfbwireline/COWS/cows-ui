﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Events/Fedline")]
    [ApiController]
    public class FedlineEventController : ControllerBase
    {
        private readonly IFedlineEventRepository _repo;
        private readonly IApptRepository _repoAppt;
        private readonly IEmailReqRepository _emailReqRepository;
        private readonly ILogger<FedlineEventController> _logger;
        private readonly IEventRuleRepository _repoEventRule;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IConfiguration _configuration;

        public FedlineEventController(IMapper mapper,
                               IFedlineEventRepository repo,
                               IApptRepository repoAppt,
                               IEmailReqRepository emailReqRepository,
                               ILogger<FedlineEventController> logger,
                               IEventRuleRepository repoEventRule,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache,
                               IConfiguration configuration)
        {
            _mapper = mapper;
            _repo = repo;
            _repoAppt = repoAppt;
            _emailReqRepository = emailReqRepository;
            _logger = logger;
            _repoEventRule = repoEventRule;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _configuration = configuration;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Fedline Event by Id: { id }.");

            var obj = _repo.GetFedEvent(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<FedlineEventViewModel>(obj));
                //return Ok(obj);
            }
            else
            {
                _logger.LogInformation($"Fedline Event by Id: { id } not found.");
                return NotFound(new { Message = $"Fedline Event Id: { id } not found." });
            }
        }

        [HttpGet("checkCancelExists/{eventId}")]
        public IActionResult CheckCancelExists(int eventId)
        {
            var obj = _repo.CheckCancelExists(eventId);
            return Ok(obj);
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] FedlineEventViewModel fedlineEvent)
        {
            LkEventRule rule = null;
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            int UserID = loggedInUser.UserId;
            string userEmail = loggedInUser.EmailAdr;

            if (fedlineEvent.WorkflowStatusId != 0)
            {
                if (fedlineEvent.TadpoleData.OrderTypeCode == "NCC" || fedlineEvent.TadpoleData.OrderTypeCode == "NCM" || fedlineEvent.TadpoleData.OrderTypeCode == "NIM" ||
                    fedlineEvent.TadpoleData.OrderTypeCode == "NDM" || fedlineEvent.TadpoleData.OrderTypeCode == "NIC") {
                    fedlineEvent.TadpoleData.OrderTypeCode = "NCI";
                }

                if (fedlineEvent.TadpoleData.OrderTypeCode == "DCC")
                {
                    fedlineEvent.TadpoleData.OrderTypeCode = "DCS";
                }
                var eventRule = _repoEventRule.GetEventRule(fedlineEvent.EventStatusId, fedlineEvent.WorkflowStatusId, UserID, (int)EventType.Fedline, fedlineEvent.TadpoleData.OrderTypeCode);

                //rule = eventRule.Where(a => a.StartEStatus == fedlineEvent.EventStatusID && a.EndEStatus == fedlineEvent.WorkflowStatusID && a.OrderType == fedlineEvent.TadpoleData.MACType).Single();
                rule = eventRule;
                fedlineEvent.EventStatusId = fedlineEvent.WorkflowStatusId;
            }

            //If completed, take it off the invalid time slot list
            if (fedlineEvent.WorkflowStatusId == 6)
                _repo.ClearInvalidTimeSlot(id);

            if (fedlineEvent.TadpoleData.isHeadend)
                fedlineEvent.UserData.EventTitle = string.Format("Install -- {0}, {1}, {2}, {3} -- {4:MMM/dd/yy}",
                    fedlineEvent.TadpoleData.CustName ?? "", fedlineEvent.TadpoleData.SiteInstallInfo.City ?? "",
                    fedlineEvent.TadpoleData.SiteInstallInfo.StateProv ?? "", fedlineEvent.TadpoleData.SiteInstallInfo.Country ?? "",
                    fedlineEvent.TadpoleData.StartDate);

            FedlineEvent fedEvent = _mapper.Map<FedlineEvent>(fedlineEvent);
            fedEvent.EventID = id;

            _repo.SaveEventUserData(fedEvent);
            if (fedlineEvent.TadpoleData.isHeadend)
                _repo.SaveEventTadpoleHeadendData(fedEvent);
            SaveFieldUpdateHistory(id, fedlineEvent.changeList.ToList(), UserID);

            //Take action on rules for this status change.  If no status change, insert history for "Data Updated" action
            if (rule != null)
            {
                _repo.SaveStatusHistory(id, rule.ActnId, fedlineEvent.StatusComments, UserID, fedlineEvent.UserData.PreStageComplete);
                //Scenario 7b - 4) COWS will not notify TADPOLE for any events that are moved to REWORK-REJECT status.
                //Setting rework-reject messages to status 9 (On hold) instead of status 10 (pending).
                if (fedlineEvent.WorkflowStatusId == 3 && fedlineEvent.FailCodeId == 9)
                {
                    if (!fedlineEvent.TadpoleData.isHeadend)
                        _repo.InsertMessage(rule.FedMsg.FedMsgDes, fedEvent, true);

                    if (rule.FedMsg.FedMsgDes == "Rework")
                    {
                        if (rule.StrtEventStus.EventStusId == 9)
                        {
                            _repo.InsertEndProcess("ShippingConf", id, rule.EndEventStus.EventStusId);
                        }
                        else
                        {
                            _repo.InsertEndProcess("In Progress", id, rule.EndEventStus.EventStusId);
                            if ((rule.FedMsg.FedMsgDes == "Rework") && (rule.EventRuleId.ToString() != "3"))
                            {
                                if (!fedlineEvent.TadpoleData.isDisconnect)
                                {
                                    _repo.UpdateSLA_cd(id);
                                }
                            }
                        }
                    }

                    //Pramod wants an email sent instead of monitoring the table, so we'll send an email.......
                    EmailHelper emailHelper = new EmailHelper(_configuration);
                    emailHelper.SendSingleEmail(_configuration.GetSection("AppSettings:CalenderEntryURL").Value,
                                                _configuration.GetSection("AppSettings:RejectEmail").Value,
                                                _configuration.GetSection("AppSettings:FedlineRejectEmail").Value, //CC
                                                string.Empty, //BCC
                                                string.Format("Fedline User Reject - {0}", id),//subject
                                                string.Format("Event ID: {0}{1}FRB Request ID: {2}{3}Rejecting user's email: {4}{5}Comments: {6}",
                                                    id, Environment.NewLine, fedlineEvent.TadpoleData.FrbRequestId, Environment.NewLine,
                                                    userEmail, Environment.NewLine, fedlineEvent.StatusComments),//body
                                                null);//attachment
                }
                else if (!fedlineEvent.TadpoleData.isHeadend)
                {
                    _repo.InsertMessage(rule.FedMsg.FedMsgDes, fedEvent, false);
                    if ((rule.FedMsg.FedMsgDes == "StatusMessage" && fedlineEvent.WorkflowStatusId == (byte)EventStatus.Fulfilling) ||
                        (rule.FedMsg.FedMsgDes == "StatusMessage" && fedlineEvent.WorkflowStatusId == (byte)EventStatus.InProgress))
                    {
                        if (fedlineEvent.WorkflowStatusId == (byte)EventStatus.Fulfilling)
                            _repo.InsertStrtProcess("Fulfilling", id);
                        else
                            if ((fedlineEvent.TadpoleData.OrderSubType == "Replacement Order") ||
                                (fedlineEvent.TadpoleData.OrderSubType == "Reorder"))
                        {
                            if (_repo.CheckStatusExists(id) == false)
                            {
                                InsertReOrderEmailToDB(fedEvent);
                            }
                        }
                        _repo.InsertStrtProcess("In Progress", id);
                    }
                    else if ((rule.FedMsg.FedMsgDes == "ShippingConf") || (rule.FedMsg.FedMsgDes == "InstallComplete")
                            || (rule.FedMsg.FedMsgDes == "Rework") || (rule.FedMsg.FedMsgDes == "DiscoComplete")
                                || (rule.FedMsg.FedMsgDes == "Complete - Device Inaccessible"))
                    {
                        if (rule.FedMsg.FedMsgDes == "ShippingConf")
                            _repo.InsertEndProcess("ShippingConf", id, rule.EndEventStusId);
                        else
                        {
                            _repo.InsertEndProcess("In Progress", id, rule.EndEventStusId);
                            if ((rule.FedMsg.FedMsgDes == "Rework") && (rule.EventRuleId != 195))
                            {
                                if (!fedlineEvent.TadpoleData.isDisconnect)
                                {
                                    _repo.UpdateSLA_cd(id);
                                }
                            }
                        }
                    }
                }
                var exists = _repoEventRule.GetById(rule.EventRuleId);
                if (exists.EventRuleData.Count() > 0)
                    InsertEmailToDB(fedEvent);
            }
            else
            {
                _repo.SaveStatusHistory(id, 20, fedlineEvent.StatusComments, UserID, fedlineEvent.UserData.PreStageComplete);
            }

            //reset workflow status.
            fedlineEvent.EventStatusId = fedlineEvent.WorkflowStatusId;
            fedlineEvent.EventStatus = fedlineEvent.WorkflowStatus;
            fedlineEvent.WorkflowStatusId = 0;
            fedlineEvent.WorkflowStatus = "";
            return Ok();
        }

        public bool InsertReOrderEmailToDB(FedlineEvent fedlineEvent)
        {
            FedlineReOrderEmailEntity emailEntity = new FedlineReOrderEmailEntity();
            emailEntity.SerialNum = fedlineEvent.TadpoleData.ManagedDeviceData.SerialNum.ToString();
            emailEntity.DeviceName = fedlineEvent.TadpoleData.ManagedDeviceData.DeviceName.ToString();
            emailEntity.OrdrId = fedlineEvent.TadpoleData.FRBRequestID.ToString();
            emailEntity.OrdrTyp = fedlineEvent.TadpoleData.OrderSubType.ToString();
            emailEntity.EventId = fedlineEvent.EventID.ToString();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(FedlineReOrderEmailEntity));
            MemoryStream memoryStream = new MemoryStream();
            xmlSerializer.Serialize(memoryStream, emailEntity);
            string xmlEmailEntity = Encoding.UTF8.GetString(memoryStream.ToArray());
            List<int> userIDs = new List<int>();
            string emailTo = "";
            string emailCC = "";

            string subject = "FRB Device Certificate Needs To Be Revoked Manually – FRB ID " + emailEntity.OrdrId.ToString() + " / Event ID " + emailEntity.EventId.ToString();
            _emailReqRepository.InsertIntoEmailReq((int)EmailREQType.FedlineReorderNtfyEmail, fedlineEvent.EventID, emailTo, emailCC, subject, xmlEmailEntity, false);

            return true;
        }

        private IActionResult SaveFieldUpdateHistory(int EventID, List<string> changeList, int UserID)
        {
            if (changeList.Count > 0)
            {
                DataTable dt = new DataTable("EventChanges");
                dt.Columns.Add("MDS_EVENT_ID", typeof(int));
                dt.Columns.Add("CHNG_TYPE_NME", typeof(string));
                dt.Columns.Add("CREAT_BY_USER_ID", typeof(int));

                foreach (string s in changeList)
                {
                    DataRow dr = dt.NewRow();
                    dr["MDS_EVENT_ID"] = EventID;
                    dr["CHNG_TYPE_NME"] = s;
                    dr["CREAT_BY_USER_ID"] = UserID;

                    dt.Rows.Add(dr);
                }

                string xmlString = string.Empty;
                StringBuilder sb = new StringBuilder();
                using (StringWriter sw = new StringWriter(sb))
                {
                    dt.WriteXml(sw, XmlWriteMode.IgnoreSchema);
                    xmlString = sw.ToString();
                }
                _repo.SaveFieldUpdateHistory(EventID, xmlString);
            }
            return Ok();
        }

        public IActionResult SubmitHeadendEvent(FedlineEvent fedlineEvent, int UserID)
        {
            fedlineEvent.UserData.EventTitle = string.Format("Install -- {0}, {1}, {2}, {3} -- {4:MMM/dd/yy}",
                fedlineEvent.TadpoleData.CustName ?? "", fedlineEvent.TadpoleData.SiteInstallInfo.City ?? "",
                fedlineEvent.TadpoleData.SiteInstallInfo.StateProv ?? "", fedlineEvent.TadpoleData.SiteInstallInfo.Country ?? "",
                fedlineEvent.TadpoleData.StartDate);

            var obj = _repo.SubmitHeadendEvent(fedlineEvent, UserID);

            return Ok(obj);
        }

        public IActionResult SearchUsers(string input, byte role)
        {
            var obj = _repo.SearchUsers(input, role);

            return Ok(obj);
        }

        [HttpGet("getEventUpdateHistByEventID/{eventId}")]
        public IActionResult GetEventUpdateHistByEventID(int eventId)
        {
            var obj = _repo.GetEventUpdateHistByEventID(eventId);

            return Ok(obj);
        }

        [HttpGet("getCCDHistory/{eventId}")]
        public IActionResult GetCCDHistory(int eventId)
        {
            var obj = _repo.GetCCDHistory(eventId);

            return Ok(obj);
        }

        [HttpGet("getFedlineEventFailCodes/{eventRuleId}")]
        public IActionResult GetFedlineEventFailCodes(int eventRuleId)
        {
            var obj = _repo.GetFedlineEventFailCodes(eventRuleId);

            return Ok(obj);
        }

        public bool InsertEmailToDB(FedlineEvent fedlineEvent)
        {
            string maskedName = "Private Customer";
            string maskedNumber = "9999999999";
            string maskedAddress1 = "6200 Sprint Parkway";
            string maskedAddress2 = "";
            string maskedCity = "Overland Park";
            string maskedState_Province = "KS";
            string maskedCountry_Region = "US";
            string maskedZip = "66251";
            string maskedUS_International = "US";

            FedlineEmailEntity emailEntity = new FedlineEmailEntity();
            emailEntity.AssignedActivator = fedlineEvent.UserData.MNSActivatorUserDisplay;
            emailEntity.ScheduledEventDateTime = string.Format("{0:MMM/dd/yy hh:mm tt} - {1:hh:mm tt}", fedlineEvent.TadpoleData.StartDate, fedlineEvent.TadpoleData.EndDate);
            emailEntity.EventID = fedlineEvent.EventID.ToString();
            emailEntity.EventTitle = string.Format("{0} - {1}, {2}, {3}, {4} - {5:MMM/dd/yy hh:mm tt}", fedlineEvent.TadpoleData.MACType, maskedName, maskedCity,
                maskedState_Province, maskedCountry_Region, fedlineEvent.TadpoleData.StartDate);
            emailEntity.CustomerName = maskedName;
            emailEntity.MDSActivityType = fedlineEvent.TadpoleData.MACType.ToString();
            emailEntity.InstallSitePOC = maskedName;
            emailEntity.InstallSitePOCPhone = maskedNumber;
            emailEntity.InstallSiteBackupPOCPhone = maskedNumber;
            emailEntity.Address1 = maskedAddress1;
            emailEntity.Address2 = maskedAddress2;
            emailEntity.City = maskedCity;
            emailEntity.State_Province = maskedState_Province;
            emailEntity.Country_Region = maskedCountry_Region;
            emailEntity.Zip = maskedZip;
            emailEntity.US_International = maskedUS_International;
            emailEntity.EventLink = string.Format("{0}{1}{2}", _configuration.GetSection("AppSettings:ViewEventURL").Value, "Fedline.aspx?eid=", emailEntity.EventID);
            emailEntity.CalendarEntryURL = string.Format("{0}?eid={1}&mode=2", _configuration.GetSection("AppSettings:CalenderEntryURL").Value, emailEntity.EventID);

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(FedlineEmailEntity));
            MemoryStream memoryStream = new MemoryStream();
            xmlSerializer.Serialize(memoryStream, emailEntity);
            string xmlEmailEntity = Encoding.UTF8.GetString(memoryStream.ToArray());
            List<int> userIDs = new List<int>();
            string emailTo = "";
            userIDs.Add(fedlineEvent.UserData.MNSActivatorUserID ?? 0);
            userIDs.Add(fedlineEvent.UserData.PreStagingEngineerUserID ?? 0);
            foreach (string s in _repo.GetEmailAddresses(userIDs))
            {
                if (s != "")
                    emailTo += "," + s;
            }
            if (emailTo != "")
                emailTo = emailTo.Substring(1);     //Remove starting ,

            string subject = ((EventStatus)fedlineEvent.WorkflowStatusID).ToString() + ": " + emailEntity.EventTitle + "; Event ID: " + fedlineEvent.EventID.ToString();

            _emailReqRepository.InsertIntoEmailReq((int)EmailREQType.FedlineEmail, fedlineEvent.EventID, emailTo, fedlineEvent.UserData.EmailCCPDL, subject, xmlEmailEntity, false);

            return true;
        }

        [HttpGet("SearchUsers")]
        public IActionResult SearchUsers([FromQuery] string input, [FromQuery] int profileId)
        {
            input = string.IsNullOrEmpty(input) ? "" : input;
            //if (input == null)
            //    input = "";

            var obj = _repo.SearchUsers(input, profileId);

            return Ok(obj);
        }
    }
}