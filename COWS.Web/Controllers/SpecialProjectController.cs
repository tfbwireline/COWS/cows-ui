﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/SpecialProjects")]
    [ApiController]
    public class SpecialProjectController : ControllerBase
    {
        private readonly ISpecialProjectRepository _repo;
        private readonly ILogger<SpecialProjectController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public SpecialProjectController(IMapper mapper,
                               ISpecialProjectRepository repo,
                               ILogger<SpecialProjectController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<SpecialProjectViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<SpecialProjectViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.SpclProjNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Special Project by Id: { id }.");

            var obj = _repo.Find(s => s.SpclProjId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<SpecialProjectViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Special Project by Id: { id } not found.");
                return NotFound(new { Message = $"Special Project Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] SpecialProjectViewModel model)
        {
            _logger.LogInformation($"Create Special Project: { model.SpclProjNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkSpclProj>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkSpclProj();
                var duplicate = _repo.Find(i => i.SpclProjNme.Trim().ToLower() == obj.SpclProjNme.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.SpclProjNme + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.SpclProjId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    _logger.LogInformation($"Special Project Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/SpecialProjects/{ newData.SpclProjId }", model);
                }
            }

            return BadRequest(new { Message = "Special Project Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] SpecialProjectViewModel model)
        {
            _logger.LogInformation($"Update Special Project Id: { id }.");

            var obj = _mapper.Map<LkSpclProj>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.SpclProjNme.Trim().ToLower() == obj.SpclProjNme.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.SpclProjNme + " already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.SpclProjId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Special Project Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/SpecialProjects/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating Special Project by Id: { id }.");

            var proj = _repo.Find(s => s.SpclProjId == id);
            if (proj != null)
            {
                var obj = _mapper.Map<LkSpclProj>(proj.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);
                _logger.LogInformation($"Special Project by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Special Project by Id: { id } not found.");
            }
        }
    }
}