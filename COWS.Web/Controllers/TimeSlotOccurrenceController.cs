﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/TimeSlotOccurrence")]
    [ApiController]
    public class TimeSlotOccurrenceController : ControllerBase
    {
        private readonly ITimeSlotOccurrenceRepository _timeSlotRepo;
        private readonly ILogger<TimeSlotOccurrenceController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;
        private readonly ILoggedInUserService _loggedInUser;

        public TimeSlotOccurrenceController(IMapper mapper,
                               ITimeSlotOccurrenceRepository timeSlotRepo,
                               ILogger<TimeSlotOccurrenceController> logger,
                               IMemoryCache memoryCache,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _timeSlotRepo = timeSlotRepo;
            _logger = logger;
            _cache = memoryCache;
            _loggedInUser = loggedInUser;
        }

        [HttpGet("")]
        //public ActionResult<IEnumerable<TimeSlotOccurrenceViewModel>> GetTimeSlotOccurrences(short eventID)
        public IActionResult Get([FromQuery] short eventID, [FromQuery] bool bShowAfterHrsSlots)
        {
            IEnumerable<TimeSlotOccurrenceViewModel> timeSlots;

            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            switch (eventID)
            {
                case 5:
                    startDate = DateTime.Now.AddDays(-1);
                    endDate = DateTime.Now.AddMonths(1);
                    break;

                case 9:
                    startDate = DateTime.Now.AddDays(-1);
                    endDate = DateTime.Now.AddDays(7 * 6);
                    break;

                default:
                    return BadRequest(new { Message = "Event Type does not have a date range set up." });
            }

            timeSlots = _mapper.Map<IEnumerable<TimeSlotOccurrenceViewModel>>(_timeSlotRepo.GetAll(eventID, startDate, endDate, bShowAfterHrsSlots)).ToList();
            return Ok(timeSlots);
        }

        [HttpGet("getFTEventsForTS/{resourceID}")]
        public ActionResult<IEnumerable<FTAvailEventViewModel>> GetFTEventsForTS(int resourceID)
        {
            IEnumerable<FTAvailEventViewModel> events;
            events = _mapper.Map<IEnumerable<FTAvailEventViewModel>>(_timeSlotRepo.GetFTEventsForTS(resourceID));

            return Ok(events);
        }

        [HttpGet("getFedEventsForTS/{resourceID}")]
        public ActionResult<IEnumerable<FedlineManageUserEventViewModel>> getFedEventsForTS(int resourceID)
        {
            IEnumerable<FedlineManageUserEventViewModel> events;
            events = _mapper.Map<IEnumerable<FedlineManageUserEventViewModel>>(_timeSlotRepo.GetFedEventsForTS(resourceID));

            return Ok(events);
        }

        [HttpGet("getInvalidTSEvents")]
        public ActionResult<IEnumerable<FedlineManageUserEventViewModel>> GetInvalidTSEvents()
        {
            IEnumerable<FedlineManageUserEventViewModel> events;
            events = _mapper.Map<IEnumerable<FedlineManageUserEventViewModel>>(_timeSlotRepo.GetInvalidTSEvents());

            return Ok(events);
        }

        [HttpGet("getDisconnectEvents")]
        public ActionResult<IEnumerable<FedlineManageUserEventViewModel>> GetDisconnectEvents()
        {
            IEnumerable<FedlineManageUserEventViewModel> events;
            events = _mapper.Map<IEnumerable<FedlineManageUserEventViewModel>>(_timeSlotRepo.GetDisconnectEvents());

            return Ok(events);
        }

        [HttpGet("updateResourceAvailability/{updResultset}")]
        public void UpdateResourceAvailability(string updResultset)
        {
            var loggedInUser = _loggedInUser.GetLoggedInUser();

            _timeSlotRepo.UpdateResourceAvailability(updResultset, loggedInUser.UserId);
        }

        [HttpGet("checkUserAccessToAfterHrsSlots/{adid}")]
        public bool CheckUserAccessToAfterHrsSlots(string adid)
        {
            return _timeSlotRepo.CheckUserAccessToAfterHrsSlots(adid);
        }
    }
}