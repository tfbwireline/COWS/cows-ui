﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/ProductTypes")]
    [ApiController]
    public class ProductTypeController : ControllerBase
    {
        private readonly IProductTypeRepository _repo;
        private readonly IPlatformRepository _repoPlatform;
        private readonly ILogger<ProductTypeController> _logger;
        private readonly IMapper _mapper;

        public ProductTypeController(IMapper mapper,
                               IProductTypeRepository repo,
                               IPlatformRepository repoPlatform,
                               ILogger<ProductTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _repoPlatform = repoPlatform;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProductTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<ProductTypeViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.ProdTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Product Type by Id: { id }.");

            var obj = _repo.Find(s => s.ProdTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<ProductTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Product Type by Id: { id } not found.");
                return NotFound(new { Message = $"Product Type Id: { id } not found." });
            }
        }

        [HttpGet("GetProductTypeByOrdrCatId/{id}")]
        public ActionResult<IEnumerable<ProductTypeViewModel>> GetProductTypeByOrdrCatId([FromRoute] int id)
        {
            _logger.LogInformation($"Search Product Type by Order Category Id: { id }.");
            IEnumerable<ProductTypeViewModel> list;
            list = _mapper.Map<IEnumerable<ProductTypeViewModel>>(_repo.Find(s => s.OrdrCatId == id).OrderBy(s => s.ProdTypeDes));
            if (list != null)
            {
                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"Product Type by Order Category Id: { id } not found.");
                return NotFound(new { Message = $"Product Type Order Category Id: { id } not found." });
            }
        }

        [HttpGet("getProductPlatformCombinations")]
        public IActionResult GetProductPlatformCombinations()
        {
            List<ProductPlatformCombinationViewModel> cblProductType = new List<ProductPlatformCombinationViewModel>();

            //if (!_cache.TryGetValue(CacheKeys.ProductPlatformCombinationList, out cblProductType))
            //{
            //Load PlatformTypes
            IEnumerable<LkPltfrm> platformTypes = _repoPlatform.GetAll();

            //LoadProductTypes
            var productTypes = Get();

            var combinedList = _mapper.Map<IEnumerable<string>>(_repo.GetProductPlatformCombinations());

            bool InsertIPL = true;
            bool InsertNCCO = true;

            if (combinedList != null)
            {
                foreach (string s in combinedList)
                {
                    try
                    {
                        string prodTypeId = s.Substring(0, s.IndexOf("-"));
                        string pltfrmCd = s.Substring(s.IndexOf("-") + 1);

                        //s is stored as ProductID-Platform
                        var prod = _mapper.Map<IEnumerable<ProductTypeViewModel>>(_repo.Find(prd => prd.ProdTypeId.ToString() == prodTypeId)).SingleOrDefault();
                        var pt = _mapper.Map<IEnumerable<PlatformViewModel>>(_repoPlatform.Find(prd => prd.PltfrmCd == pltfrmCd)).SingleOrDefault();

                        if (InsertIPL && string.Compare("IPL", prod.ProdTypeDes) < 0)
                        {
                            cblProductType.Add(new ProductPlatformCombinationViewModel { ProdNme = "IPL/DPL", ProdTypeId = "-0" });
                            InsertIPL = false;
                        }

                        if (InsertNCCO && string.Compare("NCCO", prod.ProdTypeDes) < 0)
                        {
                            cblProductType.Add(new ProductPlatformCombinationViewModel { ProdNme = "NCCO", ProdTypeId = "-00" });
                            InsertNCCO = false;
                        }

                        if (pt != null)
                        {
                            prod.ProdTypeDes = prod.ProdTypeDes + " " + pt.PltfrmNme;
                        }
                        cblProductType.Add(new ProductPlatformCombinationViewModel { ProdNme = prod.ProdTypeDes, ProdTypeId = s });
                    }
                    catch (Exception e)
                    {
                        _logger.LogInformation($"GetProductPlatformCombinations Failed. { e.Message }.");
                    }
                }
            }
            else
            {
                return BadRequest(new { Message = "An error occurred while attempting to Get Product Platform Combined list." });
            }

            //    CacheManager.Set(_cache, CacheKeys.ProductPlatformCombinationList, cblProductType);
            //}

            return Ok(cblProductType);
        }
    }
}