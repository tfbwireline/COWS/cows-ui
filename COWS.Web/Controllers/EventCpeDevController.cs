﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventCpeDev")]
    [ApiController]
    public class EventCpeDevController : ControllerBase
    {
        private readonly IEventCpeDevRepository _repo;
        private readonly ILogger<EventCpeDevController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public EventCpeDevController(IMapper mapper,
                               IEventCpeDevRepository repo,
                               ILogger<EventCpeDevController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{eventId}")]
        public IActionResult Get([FromRoute] int eventId)
        {
            _logger.LogInformation($"EventCpeDev by EventId: { eventId }.");

            var obj = _repo.GetEventCpeDevByEventId(eventId);
            if (obj != null)
            {
                return Ok(_mapper.Map<GetEventCpeDevViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"EventCpeDev by EventId: { eventId } not found.");
                return NotFound(new { Message = $"EventCpeDev by EventId: { eventId } not found." });
            }
        }
    }
}