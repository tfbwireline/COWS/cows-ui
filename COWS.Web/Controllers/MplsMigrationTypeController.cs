﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MplsMigrationTypes")]
    [ApiController]
    public class MplsMigrationTypeController : ControllerBase
    {
        private readonly IMplsMigrationTypeRepository _repo;
        private readonly ILogger<MplsMigrationTypeController> _logger;
        private readonly IMapper _mapper;

        public MplsMigrationTypeController(IMapper mapper,
                               IMplsMigrationTypeRepository repo,
                               ILogger<MplsMigrationTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MplsMigrationTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MplsMigrationTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.MplsMgrtnTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MPLS Migration Type by Id: { id }.");

            var obj = _repo.Find(s => s.MplsMgrtnTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MplsMigrationTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MPLS Migration Type by Id: { id } not found.");
                return NotFound(new { Message = $"MPLS Migration Type Id: { id } not found." });
            }
        }
    }
}