﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/ManagedAuthProd")]
    [ApiController]
    public class CptManagedAuthProdController : ControllerBase
    {
        private readonly ICptManagedAuthProdRepository _repo;
        private readonly ILogger<CptManagedAuthProdController> _logger;
        private readonly IMapper _mapper;

        public CptManagedAuthProdController(IMapper mapper,
                               ICptManagedAuthProdRepository repo,
                               ILogger<CptManagedAuthProdController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptManagedAuthProdViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptManagedAuthProdViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptMngdAuthPrdId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT Managed Auth Prod by Id: { id }.");

            var obj = _repo.Find(s => s.CptMngdAuthPrdId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptManagedAuthProdViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT Managed Auth Prod by Id: { id } not found.");
                return NotFound(new { Message = $"CPT Managed Auth Prod Id: { id } not found." });
            }
        }
    }
}