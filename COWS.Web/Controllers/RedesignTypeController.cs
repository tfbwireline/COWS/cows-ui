﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/RedesignTypes")]
    [ApiController]
    public class RedesignTypeController : ControllerBase
    {
        private readonly IRedesignTypeRepository _repo;
        private readonly ILogger<RedesignTypeController> _logger;
        private readonly IMapper _mapper;

        public RedesignTypeController(IMapper mapper,
                               IRedesignTypeRepository repo,
                               ILogger<RedesignTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<RedesignTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<RedesignTypeViewModel>>(_repo
                                                                .Find(s => s.RecstusId == true)
                                                                .OrderBy(s => s.RedsgnTypeId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Redesign Type by Id: { id }.");

            var obj = _repo.Find(s => s.RedsgnTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<RedesignTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Redesign Type by Id: { id } not found.");
                return NotFound(new { Message = $"Redesign Type Id: { id } not found." });
            }
        }
    }
}