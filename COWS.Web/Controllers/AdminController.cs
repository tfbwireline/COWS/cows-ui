﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Admin")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminRepository _adminRepo;
        private readonly ILogger<UsersController> _logger;
        private readonly IMapper _mapper;

        public AdminController(IMapper mapper,
                               IAdminRepository adminRepo,
                               ILogger<UsersController> logger)
        {
            _mapper = mapper;
            _adminRepo = adminRepo;
            _logger = logger;
        }

        [HttpGet("GetSCMInterfaceView")]
        public IActionResult GetSCMInterfaceView()
        {
            _logger.LogInformation($"Get top 500 PSFT Interface View");

            var psftInterfaceView = _adminRepo.GetSCMInterfaceView();

            if (psftInterfaceView != null)
            {
                return Ok(_mapper.Map<IEnumerable<GetSCMInterfaceViewModel>>(psftInterfaceView));
            }
            else
            {
                _logger.LogInformation($"Get top 500 PSFT Interface View not found.");

                return NotFound(new { Message = $"Top top 500 PSFT Interface View not found." });
            }
        }

        [HttpGet("GetNRMBPMInterfaceView")]
        public IActionResult GetNRMBPMInterfaceView()
        {
            _logger.LogInformation($"Get top 1000 NRMBPM Interface View");

            var nrmbpmInterfaceView = _adminRepo.GetNRMBPMInterfaceView();

            if (nrmbpmInterfaceView != null)
            {
                return Ok(_mapper.Map<IEnumerable<GetNRMBPMInterfaceViewModel>>(nrmbpmInterfaceView));
            }
            else
            {
                _logger.LogInformation($"Get top 1000 NRMBPM Interface View not found.");

                return NotFound(new { Message = $"Top top 1000 NRMBPM Interface View not found." });
            }
        }

        [HttpGet("IsAdmin")]
        [Authorize]
        public bool IsAdmin()
        {
            var userId = HttpContext.User.Identity.Name;
            _logger.LogInformation($"Windows User for request : {userId}");
            return true;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}