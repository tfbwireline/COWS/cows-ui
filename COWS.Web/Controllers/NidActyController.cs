﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/NidActy")]
    [ApiController]
    public class NidActyController : ControllerBase
    {
        private readonly INidActyRepository _nidActyRepo;
        private readonly ILogger<NidActyController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public NidActyController(IMapper mapper,
                               INidActyRepository nidActyRepo,
                               ILogger<NidActyController> logger,
                               IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _nidActyRepo = nidActyRepo;
            _logger = logger;
            _cache = memoryCache;
        }

        //[HttpGet("GetSerialNumberByH6")]
        //public IActionResult GetNidActyServiceInstanceList(string h6)
        //{
        //    var data = _nidActyRepo.GetSerialNumberByH6(h6);

        //    return Ok(data);
        //}

        [HttpGet("GetSerialNumberAndIP")]
        public IActionResult GetSerialNumberAndIP([FromQuery]int ordrId, [FromQuery] string deviceId)
        {
            var data = _nidActyRepo.GetSerialNumberAndIP(ordrId, deviceId);
            return Ok(data);
        }

        
    }
}