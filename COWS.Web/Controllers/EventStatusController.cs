﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventStatus")]
    [ApiController]
    public class EventStatusController : ControllerBase
    {
        private readonly IEventStatusRepository _repo;
        private readonly ILogger<EventStatusController> _logger;
        private readonly IMapper _mapper;

        public EventStatusController(IMapper mapper,
                               IEventStatusRepository repo,
                               ILogger<EventStatusController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EventStatusViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<EventStatusViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.EventStusDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Event Status by Id: { id }.");

            var obj = _repo.Find(s => s.EventStusId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<EventStatusViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Event Status by Id: { id } not found.");
                return NotFound(new { Message = $"Event Status Id: { id } not found." });
            }
        }
    }
}