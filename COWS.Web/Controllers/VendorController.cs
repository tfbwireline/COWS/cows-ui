﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Vendors")]
    [ApiController]
    public class VendorController : ControllerBase
    {
        private readonly IVendorRepository _repo;
        private readonly ILogger<VendorController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public VendorController(IMapper mapper,
                               IVendorRepository repo,
                               ILogger<VendorController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VendorViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<VendorViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.VndrNme));
            return Ok(list);
        }

        [HttpGet("{code}")]
        public IActionResult Get([FromRoute] string code)
        {
            _logger.LogInformation($"Search Vendor by Code: { code }.");

            var obj = _repo.Find(s => s.VndrCd == code);
            if (obj != null)
            {
                return Ok(_mapper.Map<VendorViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Vendor by Code: { code } not found.");
                return NotFound(new { Message = $"Vendor Code: { code } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] VendorViewModel model)
        {
            _logger.LogInformation($"Create Vendor: { model.VndrCd }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkVndr>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkVndr();
                var duplicate = _repo.Find(i => i.VndrCd.Trim().ToLower() == obj.VndrCd.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.VndrCd + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                        }

                        _repo.UpdateVendor(newData, model.CtryCd);
                    }
                }
                else
                {
                    newData = _repo.CreateVendor(obj, model.CtryCd);
                }

                if (newData != null)
                {
                    _logger.LogInformation($"Vendor Created. { JsonConvert.SerializeObject(model) } ");
                    return Created($"api/Vendors/{ newData.VndrCd }", model);
                }
            }

            return BadRequest(new { Message = "Vendor Could Not Be Created." });
        }

        [HttpPut("{code}")]
        public IActionResult Put([FromRoute] string code, [FromBody] VendorViewModel model)
        {
            _logger.LogInformation($"Update Vendor Code: { code }.");

            var obj = _mapper.Map<LkVndr>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.VndrCd.Trim().ToLower() == obj.VndrCd.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active
                    && duplicate.VndrCd.Trim().ToLower() != obj.VndrCd.Trim().ToLower())
                {
                    return BadRequest(new { Message = obj.VndrCd + " already exists." });
                }
                else if (duplicate.RecStusId == (byte)ERecStatus.InActive
                    && duplicate.VndrCd.Trim().ToLower() != obj.VndrCd.Trim().ToLower())
                {
                    // Delete duplicate inactive record
                    _repo.DeleteVendor(duplicate.VndrCd);
                }
            }

            _repo.UpdateVendor(obj, model.CtryCd);

            _logger.LogInformation($"Vendor Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/Vendors/{ model.VndrCd }", model);
        }

        [HttpDelete("{code}")]
        public void Delete(string code)
        {
            _logger.LogInformation($"Deactivating Vendor by Code: { code }.");

            var vendor = _repo.Find(s => s.VndrCd == code);
            if (vendor != null)
            {
                var obj = _mapper.Map<LkVndr>(vendor.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.UpdateVendor(obj, string.Empty);
                _logger.LogInformation($"Vendor by Code: { code } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Vendor by Code: { code } not found.");
            }
        }
    }
}
