﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Redesign")]
    [ApiController]
    public class RedesignController : ControllerBase
    {
        private readonly ICommonRepository _commonRepo;
        private readonly IRedesignRepository _repo;
        private readonly IRedesignTypeRepository _repoRedType;
        private readonly IRedesignCustBypassRepository _repoCustBypass;
        private readonly IRedesignDevInfoRepository _repoDevInfo;
        private readonly IUserRepository _repoUser;
        private readonly IOdieReqRepository _odieReqRepo;
        private readonly IOdieRspnRepository _odieRspnRepo;
        private readonly IOdieRspnInfoRepository _odieRspnInfoRepo;
        private readonly IContactDetailRepository _contctRepo;
        private readonly ILogger<RedesignController> _logger;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IL2PInterfaceService _l2pInterface;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public bool isMRCAcct = false;

        public RedesignController(IMapper mapper,
                               IRedesignRepository repo,
                               IRedesignTypeRepository repoRedType,
                               IRedesignCustBypassRepository repoCustBypass,
                               IRedesignDevInfoRepository repoDevInfo,
                               IUserRepository repoUser,
                               ICommonRepository commonRepo,
                               IOdieReqRepository odieReqRepo,
                               IOdieRspnRepository odieRspnRepo,
                               IOdieRspnInfoRepository odieRspnInfoRepo,
                               IContactDetailRepository contctRepo,
                               ILogger<RedesignController> logger,
                               ILoggedInUserService loggedInUser,
                               IL2PInterfaceService l2pInterface,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _repoRedType = repoRedType;
            _repoCustBypass = repoCustBypass;
            _repoDevInfo = repoDevInfo;
            _repoUser = repoUser;
            _commonRepo = commonRepo;
            _odieReqRepo = odieReqRepo;
            _odieRspnRepo = odieRspnRepo;
            _odieRspnInfoRepo = odieRspnInfoRepo;
            _contctRepo = contctRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _l2pInterface = l2pInterface;
        }

        [HttpGet("GetCustNameDetails/{rspnId}")]
        public IActionResult GetCustNameDetails([FromRoute] int rspnId)
        {
            var userCsgLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            return Ok(_odieRspnRepo.GetCustNameDetails(rspnId, userCsgLevel));
        }

        [HttpGet("ChkODIEResponse")]
        public IActionResult ChkODIEResponse([FromQuery] string h1, [FromQuery] string custName,
            [FromQuery] string odieCustId, [FromQuery] string deviceFilter, [FromQuery] int redesignCatId)
        {
            var odieCustInfoCatType = redesignCatId == (int)RedesignCategory.Voice
                ? (int)OdieCustInfoReqCatType.VoiceConverged : (int)OdieCustInfoReqCatType.DataConverged;

            var csgLvl = _loggedInUser.GetLoggedInUserCsgLvlId();

            deviceFilter = (string.IsNullOrWhiteSpace(deviceFilter) || deviceFilter == "null"
                || deviceFilter == null) ? string.Empty : deviceFilter;

            OdieReq req = _odieReqRepo.Request(null, (int)OdieMessageType.CustomerInfoRequest, h1,
                custName, false, odieCustId, csgLvl, odieCustInfoCatType, deviceFilter);

            // Added to prevent returning zero device
            // When not in debug it always return zero
            System.Threading.Thread.Sleep(4000);

            return Ok(req.ReqId);
        }

        [HttpGet("GetODIEDevices")]
        public IActionResult GetODIEDevices([FromQuery] string h1, [FromQuery] string custName,
            [FromQuery] string odieCustId, [FromQuery] string deviceFilter, [FromQuery] int redesignCatId,
            [FromQuery] int redesignId)
        {
            _logger.LogInformation($"Get ODIE Devices for H1: { h1 } and Customer Name: { custName } .");

            // Validate H1
            if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(h1))
            {
                _logger.LogInformation($"You are not an authorized user for this H1: { h1 }.");
                return BadRequest(new { Message = "You are not an authorized user for this H1." });
            }

            var odieCustInfoCatType = redesignCatId == (int)RedesignCategory.Voice
                ? (int)OdieCustInfoReqCatType.VoiceConverged : (int)OdieCustInfoReqCatType.DataConverged;

            var csgLvl = _loggedInUser.GetLoggedInUserCsgLvlId();

            deviceFilter = (string.IsNullOrWhiteSpace(deviceFilter) || deviceFilter == "null"
                || deviceFilter == null) ? string.Empty : deviceFilter;

            OdieReq req = _odieReqRepo.Request(null, (int)OdieMessageType.CustomerInfoRequest, h1,
                custName, false, odieCustId, csgLvl, odieCustInfoCatType, deviceFilter);

            // Added to prevent returning zero device
            // When not in debug it always return zero
            //System.Threading.Thread.Sleep(4000);

            bool bGotOdieResp = false;
            int _iCntr = 0;
            int oldDevCnt = 0;
            //List<RedsgnDevicesInfo> odieDevices = new List<RedsgnDevicesInfo>();
            List<string> odieDevices = new List<string>();
            while (!bGotOdieResp && _iCntr <= 10)
            {
                _iCntr++;
                //odieDevices = _odieRspnInfoRepo.GetRedesignDevicesInfo(req.ReqId);
                odieDevices = _odieRspnInfoRepo.Find(i => i.ReqId == req.ReqId).OrderBy(i => i.DevId)
                    .Select(i => i.DevId).Distinct().ToList();
                if (odieDevices.Count() > 0 && !string.IsNullOrWhiteSpace(odieDevices.First()))
                {
                    if (oldDevCnt == odieDevices.Count())
                        bGotOdieResp = true;
                    else
                        oldDevCnt = odieDevices.Count();
                }

                if (!bGotOdieResp)
                {
                    System.Threading.Thread.Sleep(4000);
                }
            }

            // Return Error if ODIE Timed Out or didn't return Request ID
            if (!bGotOdieResp)
            {
                return BadRequest(new { Message = "ODIE request timed out for Customer Info request." });
            }

            if (odieDevices != null && odieDevices.Count() > 0)
            {
                // Map to View Model
                //List<RedesignDevicesInfoViewModel> odieDevicesList = _mapper.Map<IEnumerable<RedesignDevicesInfoViewModel>>(odieDevices).ToList();

                //if (redesignId > 0)
                //{
                //    // Call the SP (getRdsnDevices) to compare to Redesign Devices Info table
                //    var dev = _repoDevInfo.GetRedesignDevices(redesignId);
                //    if (dev != null && dev.Rows.Count > 0)
                //    {
                //        foreach (DataRow dr in dev.Rows)
                //        {
                //            RedesignDevicesInfoViewModel rde = new RedesignDevicesInfoViewModel();
                //            if (odieDevicesList.Where(i => i.DevNme.ToLower() == dr["DEV_NME"].ToString().ToLower()).Count() == 0)
                //            {
                //                rde.DevNme = dr["DEV_NME"].ToString();
                //                rde.DspchRdyCd = string.IsNullOrWhiteSpace(dr["DSPCH_RDY_CD"].ToString())
                //                    ? (bool?)null : Convert.ToBoolean(dr["DSPCH_RDY_CD"].ToString());
                //                rde.FastTrkCd = string.IsNullOrWhiteSpace(dr["FAST_TRK_CD"].ToString())
                //                    ? (bool?)null : Convert.ToBoolean(dr["FAST_TRK_CD"].ToString());
                //                rde.ScCd = dr["SC_CD"].ToString();
                //                rde.RedsgnId = redesignId;
                //                rde.IsSelected = Convert.ToBoolean(dr["IsChecked"].ToString());
                //                rde.SeqNbr = Convert.ToInt16(dr["SEQ_NBR"]);
                //                rde.RecStusId = Convert.ToBoolean(dr["REC_STUS_ID"].ToString());
                //                rde.RedsgnDevId = Convert.ToInt32(dr["REDSGN_DEV_ID"].ToString());
                //                rde.DevCmpltnCd = Convert.ToBoolean(dr["DEV_CMPLTN_CD"].ToString());
                //                rde.H6CustId = dr["H6_CUST_ID"].ToString();
                //                odieDevicesList.Add(rde);
                //            }
                //        }
                //    }
                //}

                //return Ok(odieDevicesList.OrderBy(i => i.DevNme));
                _logger.LogInformation($"ODIE return devices for this H1: { h1 } and ReqId: { req.ReqId }.");
                return Ok(odieDevices);
            }
            else
            {
                // Return Error if response results is null
                return BadRequest(new { Message = "No matching devices are available in ODIE." });
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Redesign by Id: { id }.");
            string adid = _loggedInUser.GetLoggedInUserAdid();

            var obj = _repo.GetById(id, adid, true);
            if (obj != null)
            {
                var redsgn = _mapper.Map<RedesignViewModel>(obj);

                if (!string.IsNullOrWhiteSpace(redsgn.NteAssigned)
                    && _repoUser.Find(i => i.UserAdid == redsgn.NteAssigned, true).FirstOrDefault() != null)
                {
                    redsgn.NteAssignedFullName = _repoUser.Find(i => i.UserAdid == redsgn.NteAssigned, true).FirstOrDefault().DsplNme;
                }

                if (!string.IsNullOrWhiteSpace(redsgn.PmAssigned)
                    && _repoUser.Find(i => i.UserAdid == redsgn.PmAssigned, true).FirstOrDefault() != null)
                {
                    redsgn.PmAssignedFullName = _repoUser.Find(i => i.UserAdid == redsgn.PmAssigned, true).FirstOrDefault().DsplNme;
                }

                if (!string.IsNullOrWhiteSpace(redsgn.NeAsnNme)
                    && _repoUser.Find(i => i.UserAdid == redsgn.NeAsnNme, true).FirstOrDefault() != null)
                {
                    redsgn.NeAsnFullNme = _repoUser.Find(i => i.UserAdid == redsgn.NeAsnNme, true).FirstOrDefault().DsplNme;
                }

                if (!string.IsNullOrWhiteSpace(redsgn.SdeAsnNme)
                    && _repoUser.Find(i => i.UserAdid == redsgn.SdeAsnNme, true).FirstOrDefault() != null)
                {
                    redsgn.SdeAsnFullNme = _repoUser.Find(i => i.UserAdid == redsgn.SdeAsnNme, true).FirstOrDefault().DsplNme;
                }

                // Repopulate ODIE CUST ID to prevent ODIE Device error
                if (string.IsNullOrWhiteSpace(redsgn.OdieCustId))
                {
                    byte csgLvlId = _l2pInterface.GetH1CsgLevelId(redsgn.H1Cd);
                    OdieReq req = _odieReqRepo.Request(0, (int)OdieMessageType.CustomerNameRequest,
                        redsgn.H1Cd, "", false, "", csgLvlId);

                    IEnumerable<OdieRspn> res = _odieRspnRepo.Response(req.ReqId, csgLvlId);
                    if (res != null && res.Count() > 0)
                    {
                        var h1Lookup = _mapper.Map<IEnumerable<OdieRspnViewModel>>(res);
                        var odieH1 = h1Lookup.FirstOrDefault(i => i.CustNme == redsgn.CustNme);
                        if (odieH1 != null)
                        {
                            redsgn.OdieCustId = odieH1.OdieCustId;
                        }
                    }
                }

                // Format Notes on Note History that was created on OLD COWS (DE39553)
                // Format text displays ok on OLD COWS but not the same as on Rewrite
                foreach (RedesignNotesViewModel note in redsgn.Notes)
                {
                    if (note.RedsgnNteTypeId == 2)
                    {
                        note.Notes = note.Notes.Replace("\r\n", "<br/>");
                    }
                }

                return Ok(redsgn);
            }
            else
            {
                _logger.LogInformation($"Redesign by Id: { id } not found.");
                return NotFound(new { Message = $"Redesign Id: { id } not found." });
            }
        }

        [HttpGet("GetRedesignSearchResults")]
        public IActionResult GetRedesignSearchResults([FromQuery] string rdsgnNos, [FromQuery] string custName,
            [FromQuery] string nte, [FromQuery] string devices, [FromQuery] string pm)
        {
            List<AdvancedSearchRedesignViewModel> redesignList = new List<AdvancedSearchRedesignViewModel>();

            DataTable dt = new DataTable();
            var adid = _loggedInUser.GetLoggedInUserAdid();
            dt = _repo.GetRedesignSearchResults(rdsgnNos, custName, nte, devices, pm, _loggedInUser.GetLoggedInUserCsgLvlId(), adid);

            if (dt != null && dt.Rows.Count > 0) {
                foreach (DataRow dr in dt.Rows) {
                    AdvancedSearchRedesignViewModel rvm = new AdvancedSearchRedesignViewModel();
                    rvm.RedesignId = Convert.ToInt32(dr["Redesign ID"].ToString());
                    rvm.RedesignNo = dr["Redesign Number"].ToString();
                    rvm.BpmRedesignNbr = dr["BPM Redesign Number"].ToString();
                    rvm.CsgLvlId = byte.Parse(dr["CSG Level"].ToString());
                    rvm.RedesignType = dr["Redesign Type"].ToString();
                    rvm.H1 = dr["H1"].ToString();
                    rvm.Customer = dr["Customer Name"].ToString();
                    rvm.OdieDeviceName = dr["Device Name"].ToString();
                    rvm.StatusDesc = dr["Redesign Status"].ToString();
                    rvm.Status = Convert.ToInt16(dr["Redesign Status ID"].ToString());
                    if (dr["Submitted Date"] != null && dr["Submitted Date"].ToString() != string.Empty)
                        rvm.SubmittedDate = Convert.ToDateTime(dr["Submitted Date"]);
                    if (dr["Expiration Date"] != null && dr["Expiration Date"].ToString() != string.Empty)
                        rvm.ExpirationDate = Convert.ToDateTime(dr["Expiration Date"]);
                    if (dr["SLA Due Date"] != null && dr["SLA Due Date"].ToString() != string.Empty)
                        rvm.SlaDueDate = Convert.ToDateTime(dr["SLA Due Date"]);
                    rvm.NteAssigned = dr["Assigned NTE"].ToString();
                    rvm.PmAssigned = dr["Assigned PM"].ToString();
                    
                    redesignList.Add(rvm);
                }
            }

            return Ok(redesignList);
        }

        [HttpPost]
        public ActionResult Post([FromBody] RedesignViewModel model)
        {
            if (ModelState.IsValid)
            {
                Redsgn redesign = _mapper.Map<Redsgn>(model);

                redesign.CretdByCd = _loggedInUser.GetLoggedInUserId();
                redesign.CretdDt = DateTime.Now;
                redesign.ModfdByCd = null;
                redesign.ModfdDt = null;
                redesign.StusId = model.StusId; // Added for some reason this was not changed on map

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(redesign.H1Cd))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                redesign.CsgLvlId = _l2pInterface.GetH1CsgLevelId(redesign.H1Cd);

                if (redesign.StusId == (short)RedesignStatus.Submitted)
                {
                    redesign.SubmitDt = DateTime.Now;
                    redesign.SlaDt = DateTime.Parse(_commonRepo.CalculateDateWithSprintBusinessdays(redesign.SubmitDt.Value, "5", false));
                    //redesign.SlaResetCntr = 0;
                    redesign.ExprtnDt = redesign.SubmitDt.Value.AddDays(181);
                }

                redesign.RedsgnDoc = GetRedesignDocs(model);
                redesign.RedsgnEmailNtfctn = GetEmailNotifications(model);
                redesign.RedsgnDevicesInfo = GetRedesignDevices(model);
                redesign.RedsgnNotes = GetRedesignNotes(model, redesign.RedsgnDevicesInfo);

                var r = _repo.Create(redesign);

                // [Updated by Sarah Sandoval - 20210610] - Save Contact Details (PJ025845)
                if (model.ContactDetails != null && model.ContactDetails.Count() > 0)
                {
                    var cntctDtl = _mapper.Map<IEnumerable<CntctDetl>>(model.ContactDetails).ToList();
                    try
                    {
                        cntctDtl.ForEach(item => {
                            item.ObjTypCd = "R";
                            item.ObjId = r.RedsgnId;
                            item.RecStusId = (short)ERecStatus.Active;
                            //item.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                            item.CreatDt = DateTime.Now;
                            item.ModfdByUserId = null;
                            item.ModfdDt = null;
                        });

                        _contctRepo.Create(cntctDtl);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation($"CPT Contact Detail could not be created. { JsonConvert.SerializeObject(cntctDtl, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                }

                if (r != null)
                {
                    // Note Updated is set to true if OldStatus is equal to New Status
                    if (!model.SuppressEmail && r.StusId != (short)RedesignStatus.Draft)
                    {
                        _repo.InsertRedesignEmail(r.RedsgnId, r.StusId, r.ExtXpirnFlgCd.Value, false);
                    }

                    _logger.LogInformation($"Redesign Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    // Added Redesign Number to prevent error on UI [20220405]
                    return Created($"api/Redesign/{ r.RedsgnId }", new { RedsgnId = r.RedsgnId, RedsgnNbr = r.RedsgnNbr });
                }
            }

            return BadRequest(new { Message = "Redesign Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] RedesignViewModel model)
        {
            if (ModelState.IsValid)
            {
                Redsgn redesign = _mapper.Map<Redsgn>(model);
                redesign.ModfdByCd = _loggedInUser.GetLoggedInUserId();
                redesign.ModfdDt = DateTime.Now;
                redesign.StusId = model.StusId; // Added for some reason this was not changed on map

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(redesign.H1Cd))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                redesign.CsgLvlId = _l2pInterface.GetH1CsgLevelId(redesign.H1Cd);

                if (redesign.StusId == (short)RedesignStatus.Submitted
                    && model.OldStusId != (short)RedesignStatus.Submitted)
                {
                    redesign.SubmitDt = DateTime.Now;
                    redesign.SlaDt = DateTime.Parse(_commonRepo.CalculateDateWithSprintBusinessdays(redesign.SubmitDt.Value, "5", false));
                    //redesign.SlaResetCntr = 0;
                    redesign.ExprtnDt = redesign.SubmitDt.Value.AddDays(181);
                }

                // Changed Override back if LOE was changed
                if (redesign.BillOvrrdnCd)
                {
                    var old = _repo.GetById(id);
                    if (redesign.RedsgnCatId > 1)
                    {
                        redesign.BillOvrrdnCd = !(old.NteLvlEffortAmt != redesign.NteLvlEffortAmt);
                    }
                    else
                    {
                        redesign.BillOvrrdnCd = !(old.NteLvlEffortAmt != redesign.NteLvlEffortAmt
                        || old.NteOtLvlEffortAmt != redesign.NteOtLvlEffortAmt
                        || old.PmLvlEffortAmt != redesign.PmLvlEffortAmt
                        || old.PmOtLvlEffortAmt != redesign.PmOtLvlEffortAmt
                        || old.NeLvlEffortAmt != redesign.NeLvlEffortAmt
                        || old.NeOtLvlEffortAmt != redesign.NeOtLvlEffortAmt
                        || old.SpsOtLvlEffortAmt != redesign.SpsOtLvlEffortAmt
                        || old.SpsOtLvlEffortAmt != redesign.SpsOtLvlEffortAmt);
                    }
                }

                redesign.CostAmt = model.BillOvrrdnCd ? model.CostAmt : CalculateCost(model);
                // Added just for note purposes on GetRedesignNotes for CalculatedCost
                model.CostAmt = redesign.CostAmt;
                redesign.RedsgnDoc = GetRedesignDocs(model);
                redesign.RedsgnEmailNtfctn = GetEmailNotifications(model);
                redesign.RedsgnDevicesInfo = GetRedesignDevices(model);
                redesign.RedsgnNotes = GetRedesignNotes(model, redesign.RedsgnDevicesInfo);

                _repo.Update(id, redesign);

                // [Updated by Sarah Sandoval - 20210610] - Save Contact Details (PJ025845)
                if (model.ContactDetails != null && model.ContactDetails.Count() > 0)
                {
                    var cntctDtl = _mapper.Map<IEnumerable<CntctDetl>>(model.ContactDetails).ToList();
                    try
                    {
                        _contctRepo.SetInactive(i => i.ObjId == id && i.ObjTypCd == "R" && i.RecStusId == 1);
                        cntctDtl.ForEach(item => {
                            item.ObjTypCd = "R";
                            item.ObjId = id;
                            item.RecStusId = (short)ERecStatus.Active;
                            //item.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                            item.CreatDt = DateTime.Now;
                            item.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                            item.ModfdDt = DateTime.Now;
                        });

                        _contctRepo.Update(cntctDtl);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation($"CPT Contact Detail could not be updated. { JsonConvert.SerializeObject(cntctDtl, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                }

                // Note Updated is set to true if OldStatus is equal to New Status
                if (!model.SuppressEmail && redesign.StusId != (short)RedesignStatus.Draft)
                {
                    _repo.InsertRedesignEmail(id, redesign.StusId, 
                        redesign.ExtXpirnFlgCd.Value, model.OldStusId == redesign.StusId);
                }

                _logger.LogInformation($"Redesign Updated. { JsonConvert.SerializeObject(model).ToString() } ");
                return Created($"api/Redesign/{ id }", new { RedsgnId = id });
            }

            return BadRequest(new { Message = "Redesign Could Not Be Updated." });
        }

        [HttpGet("GetRedesignCustomerBypassCD")]
        public IActionResult GetRedesignCustomerBypassCD([FromQuery] string h1, [FromQuery] string custName)
        {
            var userCsgLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            return Ok(_repo.GetRedesignCustomerBypassCD(h1, custName, userCsgLevel));
        }

        private List<RedsgnNotes> GetRedesignNotes(RedesignViewModel model, ICollection<RedsgnDevicesInfo> devices)
        {
            List<RedsgnNotes> notes = new List<RedsgnNotes>();

            if (model.RedsgnId > 0)
            {
                Redsgn oldRedesign = _repo.GetById(model.RedsgnId);
                if (oldRedesign != null)
                {
                    if (oldRedesign.RedsgnTypeId != model.RedsgnTypeId)
                    {
                        RedsgnNotes note = new RedsgnNotes();
                        note.CretdByCd = 1;
                        note.CretdDt = DateTime.Now;
                        note.Notes = "Redesign type changed from " + oldRedesign.RedsgnType.RedsgnTypeNme
                            + " to " + _repoRedType.GetById(model.RedsgnTypeId).RedsgnTypeNme;
                        note.RedsgnNteTypeId = (byte)RedesignNoteType.System;
                        note.RedsgnId = model.RedsgnId;

                        notes.Add(note);
                    }
                    if (model.BillOvrrdnCd && model.CostAmt.HasValue && oldRedesign.CostAmt.HasValue && model.CostAmt != oldRedesign.CostAmt)
                    {
                        RedsgnNotes note = new RedsgnNotes();
                        note.CretdByCd = 1;
                        note.CretdDt = DateTime.Now;
                        note.Notes = "Total Cost was overridden from " + oldRedesign.CostAmt.Value + " to " + model.CostAmt.Value;
                        //note.Notes = model.BillOvrrdnCd
                        //    ? "Total Cost was overridden from " + oldRedesign.CostAmt.Value + " to " + model.CostAmt.Value
                        //    : "Redesign cost changed from " + oldRedesign.CostAmt.Value + " to " + model.CostAmt.Value;
                        note.RedsgnNteTypeId = (byte)RedesignNoteType.System;
                        note.RedsgnId = model.RedsgnId;

                        notes.Add(note);
                    }
                    if (oldRedesign.NteAssigned != model.NteAssigned)
                    {
                        var nteUsr = _repoUser.Find(i => i.UserAdid == oldRedesign.NteAssigned).SingleOrDefault();
                        var oldNte = string.IsNullOrWhiteSpace(oldRedesign.NteAssigned) ? "''"
                            : (nteUsr == null ? oldRedesign.NteAssigned : nteUsr.DsplNme);
                        RedsgnNotes note = new RedsgnNotes();
                        note.CretdByCd = 1;
                        note.CretdDt = DateTime.Now;
                        note.Notes = "NTE Assigned changed from " + oldNte + " to " 
                            + (string.IsNullOrWhiteSpace(model.NteAssignedFullName) ? "''" : model.NteAssignedFullName);
                        note.RedsgnNteTypeId = (byte)RedesignNoteType.System;
                        note.RedsgnId = model.RedsgnId;

                        notes.Add(note);
                    }
                    if (oldRedesign.PmAssigned != model.PmAssigned)
                    {
                        var pmUsr = _repoUser.Find(i => i.UserAdid == oldRedesign.PmAssigned).SingleOrDefault();
                        var oldPm = string.IsNullOrWhiteSpace(oldRedesign.PmAssigned) ? "''"
                            : (pmUsr == null ? oldRedesign.PmAssigned : pmUsr.DsplNme);
                        RedsgnNotes note = new RedsgnNotes();
                        note.CretdByCd = 1;
                        note.CretdDt = DateTime.Now;
                        note.Notes = "PM Assigned changed from " + oldPm + " to " 
                            + (string.IsNullOrWhiteSpace(model.PmAssignedFullName) ? "''" : model.PmAssignedFullName);
                        note.RedsgnNteTypeId = (byte)RedesignNoteType.System;
                        note.RedsgnId = model.RedsgnId;

                        notes.Add(note);
                    }
                    if (oldRedesign.NeAsnNme != model.NeAsnNme)
                    {
                        var neUsr = _repoUser.Find(i => i.UserAdid == oldRedesign.NeAsnNme).SingleOrDefault();
                        var oldNe = string.IsNullOrWhiteSpace(oldRedesign.NeAsnNme) ? "''"
                            : (neUsr == null ? oldRedesign.NeAsnNme : neUsr.DsplNme);
                        RedsgnNotes note = new RedsgnNotes();
                        note.CretdByCd = 1;
                        note.CretdDt = DateTime.Now;
                        note.Notes = "NE Assigned changed from " + oldNe + " to " 
                            + (string.IsNullOrWhiteSpace(model.NeAsnFullNme) ? "''" : model.NeAsnFullNme);
                        note.RedsgnNteTypeId = (byte)RedesignNoteType.System;
                        note.RedsgnId = model.RedsgnId;

                        notes.Add(note);
                    }
                    if (oldRedesign.SdeAsnNme != model.SdeAsnNme)
                    {
                        var sdeUsr = _repoUser.Find(i => i.UserAdid == oldRedesign.SdeAsnNme).SingleOrDefault();
                        var oldSde = string.IsNullOrWhiteSpace(oldRedesign.SdeAsnNme) ? "''"
                            : (sdeUsr == null ? oldRedesign.SdeAsnNme : sdeUsr.DsplNme);
                        RedsgnNotes note = new RedsgnNotes();
                        note.CretdByCd = 1;
                        note.CretdDt = DateTime.Now;
                        note.Notes = "MSS SE Assigned changed from " + oldSde + " to " 
                            + (string.IsNullOrWhiteSpace(model.SdeAsnFullNme) ? "''" : model.SdeAsnFullNme);
                        note.RedsgnNteTypeId = (byte)RedesignNoteType.System;
                        note.RedsgnId = model.RedsgnId;

                        notes.Add(note);
                    }
                    if (model.CostAmt.HasValue && !model.BillOvrrdnCd)
                    {
                        bool bNotes = (model.RedsgnCatId == (byte)RedesignCategory.Voice
                                && ((oldRedesign.NteLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.NteLvlEffortAmt.GetValueOrDefault())))
                                    || (oldRedesign.NteOtLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.NteOtLvlEffortAmt.GetValueOrDefault())))
                                    || (oldRedesign.PmLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.PmLvlEffortAmt.GetValueOrDefault())))
                                    || (oldRedesign.PmOtLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.PmOtLvlEffortAmt.GetValueOrDefault())))
                                    || (oldRedesign.NeLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.NeLvlEffortAmt.GetValueOrDefault())))
                                    || (oldRedesign.NeOtLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.NeOtLvlEffortAmt.GetValueOrDefault())))
                                    || (oldRedesign.SpsLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.SpsLvlEffortAmt.GetValueOrDefault())))
                                    || (oldRedesign.SpsOtLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.SpsOtLvlEffortAmt.GetValueOrDefault())))
                                    || model.RedsgnTypeId != oldRedesign.RedsgnTypeId))
                            || ((model.RedsgnCatId == (byte)RedesignCategory.Data
                                || model.RedsgnCatId == (byte)RedesignCategory.Security)
                                    && (oldRedesign.NteLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.NteLvlEffortAmt.GetValueOrDefault()))
                                        //oldRedesign.NteLvlEffortAmt.HasValue && oldRedesign.NteLvlEffortAmt.GetValueOrDefault() != Convert.ToDecimal(String.Format("{0:.00}", model.NteLvlEffortAmt.GetValueOrDefault()))
                                        || model.RedsgnTypeId != oldRedesign.RedsgnTypeId
                                        || oldRedesign.RedsgnDevicesInfo.Where(i => i.RecStusId).Count()
                                            != model.Devices.Where(i => i.RecStusId).Count()));

                        bool bRecalculatedCost = (model.RedsgnCatId == (byte)RedesignCategory.Voice
                            && (oldRedesign.NteLvlEffortAmt.HasValue || oldRedesign.NteOtLvlEffortAmt.HasValue
                                || oldRedesign.PmLvlEffortAmt.HasValue || oldRedesign.PmOtLvlEffortAmt.HasValue
                                || oldRedesign.NeLvlEffortAmt.HasValue || oldRedesign.NeOtLvlEffortAmt.HasValue
                                || oldRedesign.SpsLvlEffortAmt.HasValue || oldRedesign.SpsOtLvlEffortAmt.HasValue))
                            || ((model.RedsgnCatId == (byte)RedesignCategory.Data
                                || model.RedsgnCatId == (byte)RedesignCategory.Security)
                                    && oldRedesign.NteLvlEffortAmt.HasValue);

                        if (bNotes)
                        {
                            RedsgnNotes note = new RedsgnNotes();
                            note.RedsgnNteTypeId = (byte)RedesignNoteType.System;
                            note.RedsgnId = model.RedsgnId;
                            note.CretdByCd = 1;
                            note.CretdDt = DateTime.Now;
                            if (isMRCAcct)
                                note.Notes = !bRecalculatedCost
                                    ? "Calculated MRC cost for this Redesign is: $" + model.CostAmt
                                    : "Recalculated MRC cost for this Redesign is:$" + model.CostAmt;
                            else
                                note.Notes = !bRecalculatedCost
                                    ? "Calculated cost for this Redesign is: $" + model.CostAmt
                                    : "Recalculated cost for this Redesign is:$" + model.CostAmt;

                            notes.Add(note);
                        }
                    }

                    // FT/Dispatch Flag will be saved on Notes when selected status is Approved
                    // AND SessionDevice is changed OR (SessionDevice is same but old Status is not Approved)
                    // AND old status is not Completed or Cancelled
                    if (model.StusId == (short)RedesignStatus.Approved
                        && model.OldStusId != (short)RedesignStatus.Approved
                        && model.OldStusId != (short)RedesignStatus.Completed
                        && model.OldStusId != (short)RedesignStatus.Cancelled
                        && !Compare(oldRedesign.RedsgnDevicesInfo, devices))
                        //&& (!Compare(oldRedesign.RedsgnDevicesInfo, devices) 
                        //    || Compare(oldRedesign.RedsgnDevicesInfo, devices)))
                    {
                        RedsgnNotes note = new RedsgnNotes();
                        note.CretdByCd = _loggedInUser.GetLoggedInUserId();
                        note.CretdDt = DateTime.Now;
                        note.Notes = GetApprovedNote(model.Devices);
                        note.RedsgnNteTypeId = (byte)RedesignNoteType.Status;
                        note.RedsgnId = model.RedsgnId;

                        notes.Add(note);
                    }

                    // Added by Sarah Sandoval [20200911]
                    // Additional Requirement for the Rewrite
                    // Capture in notes those devices that we're manually completed
                    if (devices != null && devices.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var item in model.Devices)
                        {
                            if (item.IsSelected)
                            {
                                if (oldRedesign.RedsgnDevicesInfo.Any(i => i.DevNme.Equals(item.DevNme)
                                                                && i.DevCmpltnCd.GetValueOrDefault() != item.DevCmpltnCd.GetValueOrDefault()))
                                {
                                    sb.AppendLine(string.Format("- {0} to {1}<br />", item.DevNme,
                                        item.DevCmpltnCd.GetValueOrDefault().ToString()));
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(sb.ToString()))
                        {
                            sb.Insert(0, string.Format("{0} sets the following Device Completion Flag:<br />",
                                _loggedInUser.GetLoggedInUser().FullNme));
                            RedsgnNotes note = new RedsgnNotes();
                            note.CretdByCd = _loggedInUser.GetLoggedInUserId();
                            note.CretdDt = DateTime.Now;
                            note.Notes = sb.ToString();
                            note.RedsgnNteTypeId = (byte)RedesignNoteType.Notes;
                            note.RedsgnId = model.RedsgnId;

                            notes.Add(note);
                        }
                    }
                }
            }
            
            if (!string.IsNullOrWhiteSpace(model.Description))
            {
                RedsgnNotes note = new RedsgnNotes();
                note.CretdByCd = _loggedInUser.GetLoggedInUserId();
                note.CretdDt = DateTime.Now;
                note.Notes = model.Description;
                note.RedsgnNteTypeId = (byte)RedesignNoteType.Notes;
                note.RedsgnId = model.RedsgnId;

                notes.Add(note);
            }
            if (!string.IsNullOrWhiteSpace(model.Caveats))
            {
                RedsgnNotes note = new RedsgnNotes();
                note.CretdByCd = _loggedInUser.GetLoggedInUserId();
                note.CretdDt = DateTime.Now;
                note.Notes = model.Caveats;
                note.RedsgnNteTypeId = (byte)RedesignNoteType.Caveats;
                note.RedsgnId = model.RedsgnId;

                notes.Add(note);
            }
            if (model.OldStusId != model.StusId)
            {
                RedsgnNotes note = new RedsgnNotes();
                note.CretdByCd = _loggedInUser.GetLoggedInUserId();
                note.CretdDt = DateTime.Now;
                note.Notes = "Status changed to " + model.StatusDesc;
                note.RedsgnNteTypeId = (byte)RedesignNoteType.Status;
                note.RedsgnId = model.RedsgnId;

                notes.Add(note);
            }
            // Added by Sarah Sandoval [20220126] - Manual Contact List on Redesign History
            if (model.ContactDetails != null && model.ContactDetails.Count > 0)
            {
                var cntctDtl = _mapper.Map<IEnumerable<CntctDetl>>(model.ContactDetails).ToList();
                List<string> currentEmails = _contctRepo.Find(i => i.CreatByUserId > 1 && i.RecStusId == 1 && i.ObjTypCd == "R" && i.ObjId == model.RedsgnId).Select(i => i.EmailAdr).ToList();
                List<CntctDetl> newCntcts = cntctDtl.FindAll(i => !currentEmails.Contains(i.EmailAdr) && i.CreatByUserId > 1);
                if (newCntcts != null && newCntcts.Count() > 0)
                {
                    string emails = string.Empty;
                    emails = string.Join(",", newCntcts.FindAll(i => i.CreatByUserId > 1).Select(i => i.EmailAdr).ToList());

                    if (!string.IsNullOrWhiteSpace(emails))
                    {
                        RedsgnNotes note = new RedsgnNotes();
                        note.CretdByCd = _loggedInUser.GetLoggedInUserId();
                        note.CretdDt = DateTime.Now;
                        note.Notes = "The following email/s were added manually to Contact List table: " + emails;
                        note.RedsgnNteTypeId = (byte)RedesignNoteType.System;
                        note.RedsgnId = model.RedsgnId;

                        notes.Add(note);
                    }
                }
            }

            return notes;
        }

        private List<RedsgnEmailNtfctn> GetEmailNotifications(RedesignViewModel model)
        {
            List<RedsgnEmailNtfctn> emails = new List<RedsgnEmailNtfctn>();

            if (!string.IsNullOrWhiteSpace(model.EmailNotifications))
            {
                if (model.EmailNotifications.Contains(','))
                {
                    foreach (string emailAdr in model.EmailNotifications.Split(','))
                    {
                        // Added condition by Sarah Sandoval [20210202]
                        // To prevent saving empty and duplicate emails
                        if (!string.IsNullOrWhiteSpace(emailAdr)
                            && !emails.Any(i => i.UserIdntfctnEmail.Trim().Equals(emailAdr.Trim())))
                        {
                            RedsgnEmailNtfctn email = new RedsgnEmailNtfctn();
                            email.RecStusId = Convert.ToBoolean(ERecStatus.Active);
                            email.CretdUserId = _loggedInUser.GetLoggedInUserId();
                            email.CretdDt = DateTime.Now;
                            email.RedsgnId = model.RedsgnId;
                            email.UserIdntfctnEmail = emailAdr.Trim();
                            emails.Add(email);
                        }
                    }
                }
                else
                {
                    RedsgnEmailNtfctn email = new RedsgnEmailNtfctn();
                    email.RecStusId = Convert.ToBoolean(ERecStatus.Active);
                    email.CretdUserId = _loggedInUser.GetLoggedInUserId();
                    email.CretdDt = DateTime.Now;
                    email.RedsgnId = model.RedsgnId;
                    email.UserIdntfctnEmail = model.EmailNotifications;
                    emails.Add(email);
                }
            }

            if (model.Emails != null && model.Emails.Count() > 0)
            {
                // Added condition by Sarah Sandoval [20210202]
                // To prevent saving empty and duplicate emails
                foreach (var email in model.Emails)
                {
                    if (!emails.Any(i => i.UserIdntfctnEmail.Trim().Equals(email.UserIdntfctnEmail.Trim())))
                    {
                        emails.Add(_mapper.Map<RedsgnEmailNtfctn>(email));
                    }
                }
            }

            return emails;
        }

        private List<RedsgnDevicesInfo> GetRedesignDevices(RedesignViewModel model)
        {
            List<RedsgnDevicesInfo> devices = new List<RedsgnDevicesInfo>();
            short ctr = 0;
            
            // Create Redesign Devices from ODIE combo box
            if (model.SelectedOdieDevices != null && model.SelectedOdieDevices.Count() > 0)
            {
                // Updated by Sarah Sandoval [20201216]
                // To fix PROD Issue for newly created ODIE device not being saved on DB
                // Added Log Information as well for future reference
                var odieReq = _odieReqRepo.Find(i => i.H1CustId == model.H1Cd 
                        //&& i.CustNme == model.CustNme
                        && i.OdieMsgId == (int)OdieMessageType.CustomerInfoRequest)
                    .OrderByDescending(i => i.CreatDt).FirstOrDefault();
                int reqId = odieReq != null ? odieReq.ReqId : 0;
                _logger.LogInformation($"GetRedesignDevices by ODIE ReqID: { reqId }.");

                foreach (var item in model.SelectedOdieDevices)
                {
                    //OdieRspnInfo odie = _odieRspnInfoRepo.GetById(item);
                    OdieRspnInfo odie = _odieRspnInfoRepo.Find(i => i.DevId == item 
                        && i.ReqId == reqId).FirstOrDefault();
                    if (odie != null)
                    {
                        if (model.RedsgnId > 0 
                            && model.Devices.FirstOrDefault(i => i.DevNme.Equals(odie.DevId)) != null)
                        {
                            // Save existing selected devices
                            var dev = model.Devices.FirstOrDefault(i => i.DevNme.Equals(odie.DevId));
                            // Added by Sarah Sandoval [20210107] - To update H6 for older values
                            dev.H6CustId = odie.H6CustId;
                            devices.Add(_mapper.Map<RedsgnDevicesInfo>(dev));
                        }
                        else
                        {
                            // Create new devices from additional selection
                            RedsgnDevicesInfo device = new RedsgnDevicesInfo();
                            if (model.RedsgnId > 0)
                                device.RedsgnId = model.RedsgnId;
                            device.DevNme = odie.DevId;
                            device.H6CustId = odie.H6CustId;
                            device.CretdByCd = _loggedInUser.GetLoggedInUserId();
                            device.CretdDt = DateTime.Now;
                            device.FastTrkCd = null;
                            device.ScCd = "";
                            device.DspchRdyCd = null;
                            device.SeqNbr = ctr; //ctr++;
                            device.RecStusId = Convert.ToBoolean(ERecStatus.Active);

                            devices.Add(device);
                        }
                    }
                }
            }
            // Create Redesing Devices from User input
            else if (!string.IsNullOrWhiteSpace(model.NewDevice))
            {
                if (model.NewDevice.Contains(","))
                {
                    foreach (string item in model.NewDevice.Split(","))
                    {
                        RedsgnDevicesInfo device = new RedsgnDevicesInfo();
                        device.DevNme = item.Trim();
                        //device.H6CustId = odie.H6CustId;
                        device.CretdByCd = _loggedInUser.GetLoggedInUserId();
                        device.CretdDt = DateTime.Now;
                        device.FastTrkCd = null;
                        device.ScCd = string.Empty;
                        device.DspchRdyCd = null;
                        device.SeqNbr = ctr; // ctr++;
                        device.RecStusId = Convert.ToBoolean(ERecStatus.Active);

                        devices.Add(device);
                    }
                }
                else
                {
                    RedsgnDevicesInfo device = new RedsgnDevicesInfo();
                    device.DevNme = model.NewDevice.Trim();
                    //device.H6CustId = odie.H6CustId;
                    device.CretdByCd = _loggedInUser.GetLoggedInUserId();
                    device.CretdDt = DateTime.Now;
                    device.FastTrkCd = null;
                    device.ScCd = string.Empty;
                    device.DspchRdyCd = null;
                    device.SeqNbr = ctr; // ctr++;
                    device.RecStusId = Convert.ToBoolean(ERecStatus.Active);

                    devices.Add(device);
                }
            }
            else if (model.Devices != null && model.Devices.Count() > 0)
            {
                foreach (RedesignDevicesInfoViewModel item in model.Devices)
                {
                    if (item.IsSelected)
                    {
                        devices.Add(_mapper.Map<RedsgnDevicesInfo>(item));
                    }
                }
            }

            return devices;
        }

        private List<RedsgnDoc> GetRedesignDocs(RedesignViewModel model)
        {
            List<RedsgnDoc> redsgnDocs = new List<RedsgnDoc>();

            if (model.Docs != null && model.Docs.Count() > 0)
            {
                foreach (RedesignDocViewModel doc in model.Docs)
                {
                    RedsgnDoc redsgnDoc = _mapper.Map<RedsgnDoc>(doc);
                    redsgnDoc.RedsgnId = doc.RedsgnId > 0 ? doc.RedsgnId : 0;
                    redsgnDoc.RecStusId = (byte)ERecStatus.Active;
                    
                    if (!string.IsNullOrWhiteSpace(doc.Base64string))
                    {
                        redsgnDoc.FileCntnt = Convert.FromBase64String(doc.Base64string);
                    }

                    if (doc.RedsgnId > 0)
                    {
                        // For Update
                        redsgnDoc.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                        redsgnDoc.ModfdDt = DateTime.Now;
                    }
                    else
                    {
                        // For Create
                        redsgnDoc.RedsgnDocId = 0;
                        redsgnDoc.RedsgnId = model.RedsgnId;
                        redsgnDoc.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        redsgnDoc.CreatDt = DateTime.Now;
                    }

                    redsgnDocs.Add(redsgnDoc);
                }

            }

            return redsgnDocs;
        }

        private decimal? CalculateCost(RedesignViewModel model)
        {
            decimal cost = 0;
            decimal nteCost = 0;
            decimal sdeCost = 0;

            if (model.RedsgnCatId == (byte)RedesignCategory.Voice)
            {
                if (!model.NteLvlEffortAmt.HasValue && !model.NteOtLvlEffortAmt.HasValue
                    && !model.PmLvlEffortAmt.HasValue && !model.PmOtLvlEffortAmt.HasValue
                    && !model.NeLvlEffortAmt.HasValue && !model.NeOtLvlEffortAmt.HasValue
                    && !model.SpsLvlEffortAmt.HasValue && !model.SpsOtLvlEffortAmt.HasValue)
                {
                    return null;
                }
            }
            else if (model.RedsgnCatId == (byte)RedesignCategory.Data)
            {
                if (!model.NteLvlEffortAmt.HasValue)
                {
                    return null;
                }
            }

            try
            {
                bool customerBypass = _repoCustBypass.GetRedesignCustomerBypassCD(model.H1Cd,
                        model.CustNme, _loggedInUser.GetLoggedInUserCsgLvlId());

                if (model.RedsgnCatId == (byte)RedesignCategory.Voice)
                {
                    if (model.IsBillable && !customerBypass)
                    {
                        Dictionary<string, string> dc = new Dictionary<string, string>();
                        dc = _repo.GetRedesignCostValues();
                        if (dc != null && dc.Count() > 0)
                        {
                            if (dc["NTE_VOICE_CHARGE_RATE"] != "" && model.NteLvlEffortAmt.HasValue)
                            {
                                cost = cost + ((model.NteLvlEffortAmt.Value * Convert.ToDecimal(dc["NTE_VOICE_CHARGE_RATE"])) / 60);
                            }
                            if (dc["NTE_VOICE_OT_CHARGE_RATE"] != "" && model.NteOtLvlEffortAmt.HasValue)
                            {
                                cost = cost + ((model.NteOtLvlEffortAmt.Value * Convert.ToDecimal(dc["NTE_VOICE_OT_CHARGE_RATE"])) / 60);
                            }
                            if (dc["PM_VOICE_CHARGE_RATE"] != "" && model.PmLvlEffortAmt.HasValue)
                            {
                                cost = cost + ((model.PmLvlEffortAmt.Value * Convert.ToDecimal(dc["PM_VOICE_CHARGE_RATE"])) / 60);
                            }
                            if (dc["PM_VOICE_OT_CHARGE_RATE"] != "" && model.PmOtLvlEffortAmt.HasValue)
                            {
                                cost = cost + ((model.PmOtLvlEffortAmt.Value * Convert.ToDecimal(dc["PM_VOICE_OT_CHARGE_RATE"])) / 60);
                            }
                            if (dc["NE_VOICE_CHARGE_RATE"] != "" && model.NeLvlEffortAmt.HasValue)
                            {
                                cost = cost + ((model.NeLvlEffortAmt.Value * Convert.ToDecimal(dc["NE_VOICE_CHARGE_RATE"])) / 60);
                            }
                            if (dc["NE_VOICE_OT_CHARGE_RATE"] != "" && model.NeOtLvlEffortAmt.HasValue)
                            {
                                cost = cost + ((model.NeOtLvlEffortAmt.Value * Convert.ToDecimal(dc["NE_VOICE_OT_CHARGE_RATE"])) / 60);
                            }
                            if (dc["SPS_VOICE_CHARGE_RATE"] != "" && model.SpsLvlEffortAmt.HasValue)
                            {
                                cost = cost + ((model.SpsLvlEffortAmt.Value * Convert.ToDecimal(dc["SPS_VOICE_CHARGE_RATE"])) / 60);
                            }
                            if (dc["SPS_VOICE_OT_CHARGE_RATE"] != "" && model.SpsOtLvlEffortAmt.HasValue)
                            {
                                cost = cost + ((model.SpsOtLvlEffortAmt.Value * Convert.ToDecimal(dc["SPS_VOICE_OT_CHARGE_RATE"])) / 60);
                            }

                            cost = Decimal.Round(cost, 2);

                            var h1MrcValue = _repo.GetH1MRCValue(model.H1Cd, model.RedsgnCatId.Value);
                            if (h1MrcValue > 0)
                            {
                                cost = cost * h1MrcValue;
                                cost = Decimal.Round(cost, 2);
                                isMRCAcct = true;
                            }
                        }
                        else
                        {
                            cost = 0;
                        }
                    }
                }
                else if (model.RedsgnCatId == (byte)RedesignCategory.Data)
                {
                    if (model.IsBillable && !customerBypass)
                    {
                        Dictionary<string, string> dc = new Dictionary<string, string>();
                        dc = _repo.GetRedesignCostValues();
                        if (dc != null && dc.Count() > 0)
                        {
                            if (dc["NTE_CHARGE_RATE"] != "" && model.NteLvlEffortAmt.HasValue)
                            {
                                nteCost = ((model.NteLvlEffortAmt.Value * Convert.ToDecimal(dc["NTE_CHARGE_RATE"])) / 60);
                            }

                            int selectedDevicesCount = model.Devices.Select(a => a.IsSelected).Count();

                            if (dc["SDE_CHARGE_RATE"] != "" && selectedDevicesCount > 0)
                            {
                                sdeCost = selectedDevicesCount * Convert.ToDecimal(dc["SDE_CHARGE_RATE"]);
                            }
                            cost = Decimal.Round((nteCost + sdeCost), 2);

                            var h1MrcValue = _repo.GetH1MRCValue(model.H1Cd, model.RedsgnCatId.Value);
                            if (h1MrcValue > 0)
                            {
                                cost = cost * h1MrcValue;
                                cost = Decimal.Round(cost, 2);
                            }
                        }
                    }
                }
                else if (model.RedsgnCatId == (byte)RedesignCategory.Security)
                {
                    cost = 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return cost;
        }

        private string GetApprovedNote(ICollection<RedesignDevicesInfoViewModel> devices)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Status Update: Approved by NTE");
            sb.AppendLine();
            sb.Append("<table cellpadding=\"2\">");
            sb.Append("<tr>");
            sb.Append("<th align=\"left\"><b>Device</b></th>");
            sb.Append("<th align=\"center\"><b>Simple/Complex</b></th>");
            sb.Append("<th align=\"center\"><b>Dispatch Ready</b></th><th></th></tr>");

            foreach (RedesignDevicesInfoViewModel device in devices)
            {
                if (device.IsSelected)
                {
                    sb.Append("<tr>");
                    sb.Append("<td align=\"left\" width=\"50%\">");
                    sb.Append(device.DevNme);
                    sb.Append("</td>");
                    sb.Append("<td align=\"center\" width=\"10%\">");
                    sb.Append(device.ScCd);
                    sb.Append("</td>");
                    sb.Append("<td align=\"center\" width=\"15%\">");
                    sb.Append((device.DspchRdyCd.HasValue && device.DspchRdyCd.Value) ? "Y" : "N");
                    sb.Append("</td>");
                    sb.Append("<td width=\"25%\"></td>");
                    sb.Append("</tr>");
                }
            }
            sb.Append("</table>");
            return sb.ToString();
        }

        private bool Compare(object obj1, object obj2)
        {
            // If comparison object is null or is a different type, no further comparisons are necessary...
            if (obj1 == null || obj2 == null)
                return false;

            if (!obj1.GetType().Equals(obj2.GetType()))
                return false;

            // Compare objects using byte arrays...
            using (MemoryStream memStream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter(null, new StreamingContext(StreamingContextStates.Clone));

                // Get byte array of "this" object...
                binaryFormatter.Serialize(memStream, obj1);
                byte[] b1 = memStream.ToArray();

                // Get byte array of object to be compared...
                memStream.SetLength(0);
                binaryFormatter.Serialize(memStream, obj2);
                byte[] b2 = memStream.ToArray();

                // Compare array sizes. If equal, no further comparisons are necessary...
                if (b1.Length != b2.Length)
                    return false;

                // If sizes are equal, compare each byte while inequality is not found...
                for (int i = 0; i < b1.Length; i++)
                {
                    if (b1[i] != b2[i])
                        return false;
                }
            }

            return true;
        }
    }
}