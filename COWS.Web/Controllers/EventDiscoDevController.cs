﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventDiscoDev")]
    [ApiController]
    public class EventDiscoDevController : ControllerBase
    {
        private readonly IEventDiscoDevRepository _repo;
        private readonly ILogger<EventDiscoDevController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public EventDiscoDevController(IMapper mapper,
                               IEventDiscoDevRepository repo,
                               ILogger<EventDiscoDevController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EventDiscoDevViewModel>> Get()
        {
            IEnumerable<EventDiscoDevViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.EventDiscoDevList, out list))
            {
                list = _mapper.Map<IEnumerable<EventDiscoDevViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OdieDevNme));

                CacheManager.Set(_cache, CacheKeys.EventDiscoDevList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Event Disconnect Device by Id: { id }.");

            var obj = _repo.GetById(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<EventDiscoDevViewModel>(obj));
            }
            else
            {
                _logger.LogInformation($"Event Disconnect Device by Id: { id } not found.");
                return NotFound(new { Message = $"Event Disconnect Device Id: { id } not found." });
            }
        }

        [HttpGet("/EventId/{id}")]
        public ActionResult<IEnumerable<EventDiscoDevViewModel>> GetByEventId([FromRoute] int id)
        {
            IEnumerable<EventDiscoDevViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.EventDiscoDevList, out list))
            {
                list = _mapper.Map<IEnumerable<EventDiscoDevViewModel>>(_repo
                                                                .Find(i => i.EventId == id)
                                                                .OrderBy(s => s.OdieDevNme));

                CacheManager.Set(_cache, CacheKeys.EventDiscoDevList, list);
            }

            return Ok(list);
        }
    }
}