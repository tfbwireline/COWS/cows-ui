﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/WorldHolidays")]
    [ApiController]
    public class WorldHolidayController : ControllerBase
    {
        private readonly IWorldHolidayRepository _repo;
        private readonly ILogger<WorldHolidayController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private string _includePast;

        public WorldHolidayController(IMapper mapper,
                               IWorldHolidayRepository repo,
                               ILogger<WorldHolidayController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _includePast = "false";
        }

        [HttpGet]
        public ActionResult<IEnumerable<WorldHolidayViewModel>> Get([FromQuery] string includePast)
        {
            IEnumerable<WorldHolidayViewModel> list;
            if (_includePast != includePast) _cache.Remove(CacheKeys.WorldHolidayList);
            if (!_cache.TryGetValue(CacheKeys.WorldHolidayList, out list))
            {
                if ((includePast != null) && includePast == "true")
                {
                    _includePast = "true";
                    list = _mapper.Map<IEnumerable<WorldHolidayViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderByDescending(s => s.WrldHldyDt.Year)
                                                                .ThenBy(s => s.CtryCdNavigation.CtryNme)
                                                                .ThenBy(s => s.WrldHldyDt.Month));
                }
                else
                {
                    _includePast = "false";
                    list = _mapper.Map<IEnumerable<WorldHolidayViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active &&
                                                                    s.WrldHldyDt.Year >= DateTime.Now.Year)
                                                                .OrderByDescending(s => s.WrldHldyDt.Year)
                                                                .ThenBy(s => s.CtryCdNavigation.CtryNme)
                                                                .ThenBy(s => s.WrldHldyDt.Month));
                }   

                CacheManager.Set(_cache, CacheKeys.WorldHolidayList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search World Holiday by Id: { id }.");

            var obj = _repo.Find(s => s.WrldHldyId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<WorldHolidayViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"World Holiday by Id: { id } not found.");
                return NotFound(new { Message = $"World Holiday Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] WorldHolidayViewModel model)
        {
            _logger.LogInformation($"Create World Holiday: { model.WrldHldyDes }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkWrldHldy>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkWrldHldy();
                var duplicate = _repo.Find(i => i.WrldHldyDt == obj.WrldHldyDt
                    && i.CtryCd == obj.CtryCd).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        //return BadRequest(new { Message = "A holiday is already active for this Country/City/Date, unable to insert duplicate holiday." });
                        return BadRequest(new { Message = "A holiday is already active for this Country/City/Date." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.WrldHldyDt = obj.WrldHldyDt;
                            newData.CtryCd = obj.CtryCd;
                            newData.CtyNme = obj.CtyNme;
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.WrldHldyId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    // Update Cache
                    _cache.Remove(CacheKeys.WorldHolidayList);
                    Get(_includePast);

                    _logger.LogInformation($"World Holiday Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/WorldHolidays/{ newData.WrldHldyId }", model);
                }
            }

            return BadRequest(new { Message = "World Holiday Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] WorldHolidayViewModel model)
        {
            _logger.LogInformation($"Update World Holiday Id: { id }.");

            var obj = _mapper.Map<LkWrldHldy>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.WrldHldyDt == obj.WrldHldyDt
                    && i.CtryCd == obj.CtryCd).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active
                    && duplicate.WrldHldyId != obj.WrldHldyId)
                {
                    //return BadRequest(new { Message = "A holiday is already active for this Country/City/Date, unable to insert duplicate holiday." });
                    return BadRequest(new { Message = "A holiday is already active for this Country/City/Date." });
                }
                else if (duplicate.RecStusId == (byte)ERecStatus.InActive
                    && duplicate.WrldHldyId != obj.WrldHldyId)
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.WrldHldyId);
                }
            }

            _repo.Update(id, obj);

            // Update Cache
            _cache.Remove(CacheKeys.WorldHolidayList);
            Get(_includePast);

            _logger.LogInformation($"World Holiday Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/WorldHolidays/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating World Holiday by Id: { id }.");

            var hday = _repo.Find(s => s.WrldHldyId == id);
            if (hday != null)
            {
                var obj = _mapper.Map<LkWrldHldy>(hday.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);

                // Update Cache
                _cache.Remove(CacheKeys.WorldHolidayList);
                Get(_includePast);
                _logger.LogInformation($"World Holiday by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to World Holiday by Id: { id } not found.");
            }
        }
    }
}
