﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/OrderVlan")]
    [ApiController]
    public class OrderVlanController : ControllerBase
    {
        private readonly IOrderVlanRepository _repo;
        private readonly ILogger<OrderVlanController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public OrderVlanController(IMapper mapper,
                               IOrderVlanRepository repo,
                               ILogger<OrderVlanController> logger,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OrderVlanViewModel>> Get()
        {
            IEnumerable<OrderVlanViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.OrderVlanList, out list))
            {
                list = _mapper.Map<IEnumerable<OrderVlanViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrId));

                CacheManager.Set(_cache, CacheKeys.OrderVlanList, list);
            }
           
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order Vlan by Id: { id }.");

            var obj = _repo.Find(a => a.OrdrVlanId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<OrderVlanViewModel>(obj.FirstOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Order Vlan by Id: { id } not found.");
                return NotFound(new { Message = $"Order Vlan Id: { id } not found." });
            }
        }

        [HttpGet("Order/{id}")]
        public IActionResult GetByOrder([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order Vlan by Order Id: { id }.");

            var obj = _repo.Find(a => a.OrdrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<OrderVlanViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Order Vlan by Order Id: { id } not found.");
                return NotFound(new { Message = $"Order Id: { id } not found." });
            }
        }
    }
}
