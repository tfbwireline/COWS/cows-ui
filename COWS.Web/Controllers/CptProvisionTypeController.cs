﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/ProvisionTypes")]
    [ApiController]
    public class CptProvisionTypeController : ControllerBase
    {
        private readonly ICptProvisionTypeRepository _repo;
        private readonly ILogger<CptProvisionTypeController> _logger;
        private readonly IMapper _mapper;

        public CptProvisionTypeController(IMapper mapper,
                               ICptProvisionTypeRepository repo,
                               ILogger<CptProvisionTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptProvisionTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptProvisionTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptPrvsnType));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT Provision Type by Id: { id }.");

            var obj = _repo.Find(s => s.CptPrvsnTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptProvisionTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT Provision Type by Id: { id } not found.");
                return NotFound(new { Message = $"CPT Provision Type Id: { id } not found." });
            }
        }
    }
}