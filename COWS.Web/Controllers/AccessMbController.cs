﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/AccessMb")]
    [ApiController]
    public class AccessMbController : ControllerBase
    {
        private readonly IAccessMbRepository _repo;
        private readonly ILogger<AccessMbController> _logger;
        private readonly IMapper _mapper;

        public AccessMbController(IMapper mapper,
                               IAccessMbRepository repo,
                               ILogger<AccessMbController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AccessMbViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<AccessMbViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(i => i.AccsMbId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Access Mb by Id: { id }.");

            var obj = _repo.Find(s => s.AccsMbId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<AccessMbViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Access Mb by Id: { id } not found.");
                return NotFound(new { Message = $"Access Mb Id: { id } not found." });
            }
        }
    }
}