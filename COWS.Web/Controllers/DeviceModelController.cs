﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/DeviceModels")]
    [ApiController]
    public class DeviceModelController : ControllerBase
    {
        private readonly IDeviceModelRepository _repo;
        private readonly ILogger<DeviceModelController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public DeviceModelController(IMapper mapper,
                               IDeviceModelRepository repo,
                               ILogger<DeviceModelController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<DeviceModelViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<DeviceModelViewModel>>(_repo
                                                                //.Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .GetAll()
                                                                .OrderBy(s => s.DevModelNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Device Model by Id: { id }.");

            var obj = _repo.GetById(id);
            if (obj != null)
            {
                var test = _mapper.Map<DeviceModelViewModel>(obj);
                return Ok(test);
            }
            else
            {
                _logger.LogInformation($"Device Model by Id: { id } not found.");
                return NotFound(new { Message = $"Device Model Id: { id } not found." });
            }
        }

        [HttpGet("GetForLookup")]
        public ActionResult<IEnumerable<DeviceModelViewModel>> GetForLookup()
        {
            var list = _mapper.Map<IEnumerable<DeviceModelViewModel>>(_repo.GetAll()
                                                                .OrderBy(s => s.DevModelNme));
            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] DeviceModelViewModel model)
        {
            _logger.LogInformation($"Create Device Model: { model.DevModelNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkDevModel>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                    obj.ModfdByUserId = null;
                }

                // [202112] Removed checking since the user requested that they should have the ability to activate/deactivate record
                //// Added by Sarah Sandoval [20190909]
                //// Added condition to check if name is duplicate since most Admin pages
                //// don't delete actual db record but update RecStusId to Active/Inactive
                //var newData = new LkDevModel();
                var duplicate = _repo.Find(i => i.DevModelNme.Trim().ToLower() == obj.DevModelNme.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    return BadRequest(new { Message = obj.DevModelNme + " already exists." });
                }
                //if (duplicate != null)
                //{
                //    // Throw duplicate error if name already exists and RecStusId is Active
                //    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                //    {
                //        return BadRequest(new { Message = obj.DevModelNme + " already exists." });
                //    }
                //    else
                //    {
                //        // Update RecStusId to Active
                //        newData = duplicate;
                //        if (loggedInUser != null)
                //        {
                //            newData.ModfdByUserId = loggedInUser.UserId;
                //            newData.ModfdDt = DateTime.Now;
                //            newData.RecStusId = (byte)ERecStatus.Active;
                //        }

                //        _repo.Update(newData.DevModelId, newData);
                //    }
                //}
                //else
                //{
                //    newData = _repo.Create(obj);
                //}

                var newData = _repo.Create(obj);

                if (newData != null)
                {
                    _logger.LogInformation($"Device Model Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/DeviceModels/{ newData.DevModelId }", model);
                }
            }

            return BadRequest(new { Message = "Device Model Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] DeviceModelViewModel model)
        {
            _logger.LogInformation($"Update Device Model Id: { id }.");

            var obj = _mapper.Map<LkDevModel>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                //obj.RecStusId = (byte)ERecStatus.Active;
            }

            // [202112] Removed checking since the user requested that they should have the ability to activate/deactivate record
            //// Added by Sarah Sandoval [20190909]
            //// Added condition to check if name is duplicate since most Admin pages
            //// don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.DevModelNme.Trim().ToLower() == obj.DevModelNme.Trim().ToLower() && i.DevModelId != obj.DevModelId).SingleOrDefault();
            if (duplicate != null)
            {
                return BadRequest(new { Message = obj.DevModelNme + " already exists." });
            }
            
            //if (duplicate != null)
            //{
            //    // Throw duplicate error if name already exists and RecStusId is Active
            //    if (duplicate.RecStusId == (byte)ERecStatus.Active)
            //    {
            //        return BadRequest(new { Message = obj.DevModelNme + " already exists." });
            //    }
            //    else
            //    {
            //        // Delete duplicate inactive record
            //        _repo.Delete(duplicate.DevModelId);
            //    }
            //}

            _repo.Update(id, obj);

            _logger.LogInformation($"Device Model Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/DeviceModels/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating Device Model by Id: { id }.");

            var proj = _repo.Find(s => s.DevModelId == id);
            if (proj != null)
            {
                //var obj = _mapper.Map<LkDevModel>(proj.SingleOrDefault());
                //var loggedInUser = _loggedInUser.GetLoggedInUser();
                //if (loggedInUser != null)
                //{
                //    obj.ModfdByUserId = loggedInUser.UserId;
                //    obj.ModfdDt = DateTime.Now;
                //    obj.RecStusId = (byte)ERecStatus.InActive;
                //}

                //_repo.Update(id, obj);
                //_logger.LogInformation($"Device Model by Id: { id } Deactivated.");

                _repo.Delete(id);
                _logger.LogInformation($"Device Model by Id: { id } Deleted.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Device Model by Id: { id } not found.");
            }
        }
    }
}
