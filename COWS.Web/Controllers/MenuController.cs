﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Menu")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly ICommonRepository _commonRepo;
        private readonly IUserProfileMenuRepository _userProfileMenuRepo;
        private readonly IMenuRepository _menuRepo;
        private readonly ILogger<MenuController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IConfiguration _configuration;

        public MenuController(IMapper mapper,
                               IUserRepository userRepo,
                               IMenuRepository menuRepo,
                               ICommonRepository commonRepo,
                               IUserProfileRepository userProfile,
                               IUserProfileMenuRepository userProfileMenuRepo,
                               ILogger<MenuController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache,
                               IConfiguration configuration)
        {
            _mapper = mapper;
            _commonRepo = commonRepo;
            _userProfileMenuRepo = userProfileMenuRepo;
            _menuRepo = menuRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _configuration = configuration;
        }

        [HttpGet("")]
        public ActionResult<IEnumerable<MenuViewModel>> Get()
        {
            _logger.LogInformation($"Get Menu Links");

            IEnumerable<MenuViewModel> menu;
            menu = GetMenu();

            if (menu != null)
            {
                return Ok(menu);
            }
            else
            {
                _logger.LogInformation($"Menu Links not found");

                return NotFound(new { Message = $"Menu Links not found." });
            }
        }

        [HttpGet("{url}")]
        public ActionResult<string> Get(string url)
        {
            _logger.LogInformation($"Validate Menu Links");
            string[] urls = url.Split(',');
            string fedlineMsg = "CSG level 2 is required to view Fedline events";
            string cancelMsg = "You are not an authorized user to initiate cancel";
            string invalidUrl = "Url is invalid. Please check and try again";
            string authorized = "authorized";

            IEnumerable<LkMenu> menu = GetMenuAccessible();

            if (menu != null)
            {
                int i = 0;
                foreach (string u in urls)
                {
                    i++;
                    if (u.Contains("fedline"))
                    {
                        var loggedInUser = _loggedInUser.GetLoggedInUser();
                        var userCsgLvl = _commonRepo.GetUserSecurityGroupByADID(loggedInUser.UserAdid);
                        var csgLvl = userCsgLvl.Where(a => a.CsgLvlId > 0).FirstOrDefault();
                        if (csgLvl==null)
                        {
                            return fedlineMsg;
                        }
                    }
                    if (u.Contains("cancel-ordr"))
                    {
                        var loggedInUser = _loggedInUser.GetLoggedInUser();
                        var menuCancelProfile = _commonRepo.GetMenuCancelUserByADID(loggedInUser.UserAdid);
                        var csgLvl = menuCancelProfile.Where(a => a.PrmtrValuTxt.Contains(loggedInUser.UserAdid)).FirstOrDefault();
                        if (csgLvl == null)
                        {
                            return cancelMsg;
                        }
                    }
                    if (!string.IsNullOrEmpty(u) && i < 4)
                    {
                        if (u != "home")
                        {
                            if (!menu.Select(a => a.SrcPath).Contains("/" + u))
                            {
                                return invalidUrl;
                            }
                        }
                    }
                }
                return authorized;
            }
            else
            {
                _logger.LogInformation($"Menu Links not found");

                return NotFound(new { Message = $"Menu Links not found." });
            }
        }

        // Will also be called for every action made that will change user menu then cache
        private IEnumerable<MenuViewModel> GetMenu()
        {
            IEnumerable<MenuViewModel> all = _mapper.Map<IEnumerable<MenuViewModel>>(GetMenuAccessible());

            CacheManager.Set(_cache, CacheKeys.Menu, all);

            // Get parent menu
            IEnumerable<MenuViewModel> parents = all.AsQueryable()
                .Where(a => a.DpthLvl == 0)
                .OrderBy(a => a.MenuNme)
                .ToList();

            // Get children menu
            IEnumerable<MenuViewModel> children = all.AsQueryable()
                .Where(a => a.DpthLvl == 1)
                .Where(a => a.RecStusId == 1)
                //.Where(a => a.MenuId != 78)
                //.Where(a => a.MenuId != 82)
                //.Where(a=>a.MenuId!=81)
                //.OrderBy(a => a.DsplOrdr)

                .ToList();


            List<MenuViewModel> menu = new List<MenuViewModel>();

            // Creates parent - children menu
            foreach (MenuViewModel parent in parents)
            {
                // Added by Sarah Sandoval [20190807]
                // Add necessary parent menu to sort children menus by Display Order
                // If CPT, Tools and MSS, children menus should be sorted by Display Order
                // Others should be alphabetical like Admin, Events and Calendars
                if (parent.MenuNme == "CPT" || parent.MenuNme == "MSS" || parent.MenuNme == "Tools")
                {
                    parent.children = children.Where(a => a.PrntMenuId == parent.MenuId)
                    .OrderBy(a => a.DsplOrdr)
                    .ToList();
                }
                else
                {
                    parent.children = children.Where(a => a.PrntMenuId == parent.MenuId)
                    .OrderBy(a => a.MenuNme)
                    .ToList();
                }

                // GOM to Intl CPE
                if(parent.MenuNme == "Order" || parent.MenuNme == "Tools")
                {
                    foreach (var child in parent.children)
                    {
                        child.MenuNme = child.MenuNme.Replace("GOM", "Intl CPE");
                    }
                }

                menu.Add(parent);
            }

            return _mapper.Map<IEnumerable<MenuViewModel>>(menu);
        }

        private List<LkMenu> GetMenuAccessible()
        {
            // Get all menu accessible for this user
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if(loggedInUser == null)
            {
                return null;
            }

            var menus = _userProfileMenuRepo
                .Find(a => a.UsrPrf.MapUsrPrf.Where(b => b.UserId == loggedInUser.UserId && b.RecStusId == 1).Count() > 0
                && a.RecStusId == 1)
                .Select(a => new LkMenu
                {
                    MenuId = a.Menu.MenuId,
                    MenuNme = a.Menu.MenuNme,
                    MenuDes = a.Menu.MenuDes,
                    DpthLvl = a.Menu.DpthLvl,
                    PrntMenuId = a.Menu.PrntMenuId,
                    DsplOrdr = a.Menu.DsplOrdr,
                    RecStusId = a.Menu.RecStusId,
                    SrcPath = a.Menu.SrcPath
                })
                .AsQueryable()
                .Distinct()
                .ToList();

            // Commented by Sarh Sandoval [20200722] - LK_SYS_CFG menu settings will be removed on rewrite
            // Menu will be profile driven
            //// 73 - Initiating Cancel
            //// 80 - Manage CCD Bypass
            //var specialMenuId = new List<int>() { 73, 80 };

            //// Special Menu is a menu that only checks on LK_SYS_CFG
            //var specialMenus = _menuRepo.Find(x => specialMenuId.Contains(x.MenuId)).ToList();
            //for (var i = 0; i < specialMenus.Count(); i++)
            //{
            //    menus.Add(specialMenus[i]);
            //}

            //// Add menus with LkSysCfg Checking
            //// Key = LK_SYS_CFG.PRMTR_NME
            //// Value = LK_MENU.MENU_NME
            //Dictionary<string, string> menuWithSysCfgChecking = new Dictionary<string, string>()
            //                                {
            //                                    {"Currency File Upload", "menuCurrencyUpdt" },
            //                                    {"Manage CCD Bypass", "menuCCD" },
            //                                    {"Initiating Cancels", "menuCancels"},
            //                                    {"Vendor Management", "menuVndrMgmt"},
            //                                };

            //foreach (KeyValuePair<string, string> data in menuWithSysCfgChecking)
            //{

            //    // Check menu with LkSysCfg Checking if exist and get index on record
            //    var index = menus.FindIndex(x => x.MenuNme == data.Key);
            //    if (index >= 0)
            //    {
            //        // Check if current loggin user has access else remove it on the list.
            //        var hasAccess = _commonRepo.GetAdminExcPerm(data.Value, loggedInUser.UserAdid);

            //        //Check if user exist on LK_SYS_CFG and current Key to be check is Vendor
            //        if (hasAccess && data.Key == "Vendor Management")
            //        {
            //            // Check if user accessing the Admin > Vendor Management is an NCI admin
            //            hasAccess = _userRepo.UserActiveWithProfileName(loggedInUser.UserId, "NCI Admin");
            //        }

            //        if (!hasAccess)
            //        {
            //            menus.RemoveAt(index);
            //        }
            //    }
            //}

            return menus;
        }

        [HttpGet("GetOldSiteURL")]
        public IActionResult GetOldSiteUrl()
        {
            if (_configuration.GetSection("AppSettings:OldSiteURL").Exists())
                return Ok(_configuration.GetSection("AppSettings:OldSiteURL").Value);
            else
                return Ok(string.Empty);
        }

        [HttpGet("GetExternalLinks")]
        public IActionResult GetExternalLinks()
        {
            return Ok(_menuRepo.GetExternalLinks());
        }
    }
}