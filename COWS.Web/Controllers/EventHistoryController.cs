﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System;


namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventHistory")]
    [ApiController]
    public class EventHistoryController : ControllerBase
    {

        private readonly IEventHistoryRepository _repo;
        private readonly ILogger<EventHistoryController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public EventHistoryController(IMapper mapper,
                               IEventHistoryRepository repo,
                               ILogger<EventHistoryController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EventHistViewModel>> Get()
        {
            IEnumerable<EventHistViewModel> list;
            //if (!_cache.TryGetValue(CacheKeys.EventTypeList, out list))
            //{
            list = _mapper.Map<IEnumerable<EventHistViewModel>>(_repo
                .GetAll()
                .OrderBy(s => s.EventHistId));

            //    CacheManager.Set(_cache, CacheKeys.EventTypeList, list);
            //}

            return Ok(list);
        }

        [HttpGet("{eventId}")]
        public IActionResult Get([FromRoute] int eventId)
        {
            _logger.LogInformation($"Search Event History by Event Id: { eventId }.");
            List<EventHistViewModel> eventHistList = new List<EventHistViewModel>();
            var obj = _repo.Find(s => s.EventId == eventId).OrderByDescending(a => a.EventHistId);
            DataTable data = new DataTable();
            data = _repo.GetEventHistByEventID(eventId, Entities.Enums.EventType.MDS);
            if (data != null && data.Rows.Count > 0)
            {
                foreach (DataRow dr in data.Rows)
                {
                    foreach (DataRow dr1 in dr.ItemArray)
                    {
                        EventHistViewModel rvm = new EventHistViewModel();

                        {

                            rvm.EventId = Int32.Parse(dr1.ItemArray[0].ToString());
                            rvm.EventHistId = Int32.Parse(dr1.ItemArray[1].ToString());
                            rvm.ActnDes = (dr1.ItemArray[2].ToString() ?? "");
                            rvm.PerformedBy = (dr1.ItemArray[3].ToString() ?? "");
                            if (dr1.ItemArray[4] != null && dr1.ItemArray[4].ToString() != string.Empty)
                                rvm.CreatDt = Convert.ToDateTime(dr1.ItemArray[4]);
                            //rvm.FailReasId = dr1.ItemArray[5] == DBNull.Value ? null : (byte?)dr1.ItemArray[5];
                            //if (dr1.ItemArray[5] != null && dr1.ItemArray[5].ToString() != string.Empty)                           
                            //    rvm.FailReasId = Convert.ToByte(dr1.ItemArray[5]);
                            rvm.CmntTxt = dr1.ItemArray[8].ToString();
                            if (dr1.ItemArray[9] != null && dr1.ItemArray[9].ToString() != string.Empty)
                                rvm.EventStrtTmst = Convert.ToDateTime(dr1.ItemArray[9]);
                            if (dr1.ItemArray[10] != null && dr1.ItemArray[10].ToString() != string.Empty)
                                rvm.EventEndTmst = Convert.ToDateTime(dr1.ItemArray[10]);

                            eventHistList.Add(rvm);
                        };
                    }
                }
                var newList = eventHistList.OrderByDescending(x => x.CreatDt).ToList();
                return Ok(newList);
            }
            else
            {
                _logger.LogInformation($"Event History by Event Id: { eventId } not found.");
                return NotFound(new { Message = $"Event History by Event Id: { eventId } not found." });
            }
        }



        //[HttpPost]
        //public IActionResult Post([FromBody] EventHistViewModel model)
        //{
        //    _logger.LogInformation($"Create Event Type: { model.EventTypeNme }.");

        //    if (ModelState.IsValid)
        //    {
        //        var obj = _mapper.Map<LkEventType>(model);

        //        var loggedInUser = _loggedInUser.GetLoggedInUser();
        //        if (loggedInUser != null)
        //        {
        //            obj.CreatByUserId = loggedInUser.UserId;
        //            obj.CreatDt = DateTime.Now;
        //        }

        //        var rep = _repo.Create(obj);
        //        if (rep != null)
        //        {
        //            // Update Cache
        //            _cache.Remove(CacheKeys.EventTypeList);
        //            Get();

        //            var json = new JsonSerializerSettings()
        //            {
        //                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
        //                Formatting = Formatting.Indented
        //            };
        //            _logger.LogInformation($"Event Type Created. { JsonConvert.SerializeObject(rep, json) } ");
        //            return Created($"api/EventTypes/{ rep.EventTypeId }", model);
        //        }
        //    }

        //    return BadRequest(new { Message = "Event Type Could Not Be Created." });
        //}

        //[HttpPut("{id}")]
        //public IActionResult Put([FromRoute] int id, [FromBody] EventHistViewModel model)
        //{
        //    _logger.LogInformation($"Update Event Type Id: { id }.");

        //    var obj = _mapper.Map<LkEventType>(model);

        //    var loggedInUser = _loggedInUser.GetLoggedInUser();
        //    if (loggedInUser != null)
        //    {
        //        obj.ModfdByUserId = loggedInUser.UserId;
        //        obj.ModfdDt = DateTime.Now;
        //    }

        //    _repo.Update(id, obj);

        //    // Update Cache
        //    _cache.Remove(CacheKeys.EventTypeList);
        //    Get();

        //    _logger.LogInformation($"Event Type Updated. { JsonConvert.SerializeObject(obj) } ");
        //    return Created($"api/EventTypes/{ id }", model);
        //}

        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //    _logger.LogInformation($"Deactivating Event Type by Id: { id }.");

        //    var type = _repo.Find(s => s.EventTypeId == id);
        //    if (type != null)
        //    {
        //        var obj = _mapper.Map<LkEventType>(type.SingleOrDefault());
        //        var loggedInUser = _loggedInUser.GetLoggedInUser();
        //        if (loggedInUser != null)
        //        {
        //            obj.ModfdByUserId = loggedInUser.UserId;
        //            obj.ModfdDt = DateTime.Now;
        //            obj.RecStusId = 0;
        //        }

        //        _repo.Update(id, obj);

        //        // Update Cache
        //        _cache.Remove(CacheKeys.EventTypeList);
        //        Get();
        //        _logger.LogInformation($"Event Type by Id: { id } Deactivated.");
        //    }
        //    else
        //    {
        //        _logger.LogInformation($"Deactivating record failed due to Event Type by Id: { id } not found.");
        //    }
        //}
    }
}