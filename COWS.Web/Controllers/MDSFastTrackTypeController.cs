﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSFastTrackTypes")]
    [ApiController]
    public class MDSFastTrackTypeController : ControllerBase
    {
        private readonly IMDSFastTrackTypeRepository _repo;
        private readonly ILogger<MDSFastTrackTypeController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public MDSFastTrackTypeController(IMapper mapper,
                               IMDSFastTrackTypeRepository repo,
                               ILogger<MDSFastTrackTypeController> logger,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MDSFastTrackTypeViewModel>> Get()
        {
            IEnumerable<MDSFastTrackTypeViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.MDSFastTrackTypeList, out list))
            {
                list = _mapper.Map<IEnumerable<MDSFastTrackTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.MdsFastTrkTypeDes));

                CacheManager.Set(_cache, CacheKeys.MDSFastTrackTypeList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] string id)
        {
            _logger.LogInformation($"Search MDS Fast Track Type by Id: { id }.");

            var obj = _repo.Find(s => s.MdsFastTrkTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSFastTrackTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDS Fast Track Type by Id: { id } not found.");
                return NotFound(new { Message = $"MDS Fast Track Type Id: { id } not found." });
            }
        }
    }
}