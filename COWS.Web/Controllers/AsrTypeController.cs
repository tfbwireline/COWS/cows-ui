﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/AsrTypes")]
    [ApiController]
    public class AsrTypeController : ControllerBase
    {
        private readonly IAsrTypeRepository _repo;
        private readonly IOrderRepository _orderRepo;
        private readonly IEmailReqRepository _emailReqRepo;
        private readonly IAsrRepository _asrRepository;
        private readonly IOrderNoteRepository _orderNoteRepo;
        private readonly INRMRepository _nrmRepo;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly ILogger<AsrTypeController> _logger;
        private readonly IMapper _mapper;

        public AsrTypeController(IMapper mapper,
                               IAsrTypeRepository repo,
                               IOrderRepository orderRepo,
                               IEmailReqRepository emailReqRepo,
                               IAsrRepository asrRepository,
                               IOrderNoteRepository orderNoteRepo,
                               INRMRepository nrmRepo,
                               ILoggedInUserService loggedInUser,
                               ILogger<AsrTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _orderRepo = orderRepo;
            _emailReqRepo = emailReqRepo;
            _asrRepository = asrRepository;
            _orderNoteRepo = orderNoteRepo;
            _nrmRepo = nrmRepo;
            _loggedInUser = loggedInUser;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AsrTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<AsrTypeViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(i => i.AsrTypeDes));
            return Ok(list);
        }

        [HttpPost("SaveAsr")]
        public IActionResult Post([FromBody] AsrViewModel model)
        {
            _logger.LogInformation($"Saving ASR Template for Order: { model.OrdrId }");

            if (ModelState.IsValid)
            {
                string ftn = null;
                var order = _orderRepo.Find(a => a.OrdrId == model.OrdrId && (a.OrdrCatId == 2 || a.OrdrCatId == 6)).SingleOrDefault();
                if (order != null)
                {
                    ftn = order.FsaOrdr.Ftn;
                }

                EmailReq email = new EmailReq();
                email.CreatDt = DateTime.Now;
                email.EmailBodyTxt = "";
                email.EmailReqTypeId = (int)EmailREQType.ASR;
                email.EmailSubjTxt = $"{ ftn ?? model.OrdrId.ToString() } - ASR Detail Document.";
                email.OrdrId = model.OrdrId;
                email.StusId = 10;
                email.EmailListTxt = _loggedInUser.GetLoggedInUser().EmailAdr;

                _logger.LogInformation($"Creating Email Request for ASR Template of Order: { model.OrdrId }");

                if (_emailReqRepo.Create(email) != null)
                {
                    _logger.LogInformation($"Created Email Request: { JsonConvert.SerializeObject(email.EmailReqId).ToString() }");
                }

                Asr asr = _mapper.Map<Asr>(model);
                asr.CreatDt = DateTime.Now;
                asr.StusId = 10;
                asr.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                asr.EmailReqId = email.EmailReqId;

                _logger.LogInformation($"Creating ASR Template of Order: { model.OrdrId }");

                var asrResult = _asrRepository.Create(asr);
                if (asrResult != null)
                {
                    _logger.LogInformation($"Created ASR Template: { JsonConvert.SerializeObject(asrResult.AsrId).ToString() }");

                    var noteResult = InsertNote(model.OrdrId, 11, model.Notes);
                    _nrmRepo.SendASRMessageToNRMBPM(model.OrdrId, model.Notes.Replace("</br>", "  "));

                    var orderNote = _orderNoteRepo.Find(a => a.OrdrId == model.OrdrId).ToList();

                    return Ok(_mapper.Map<IEnumerable<OrderNote2ViewModel>>(orderNote));
                    //return Created($"api/AsrTypes/{asr.AsrId}", asrResult);
                }
            }

            return BadRequest(new { Message = "ASR Template could not be saved" });
        }

        private OrdrNte InsertNote(int orderId, byte nteType, string nte)
        {
            var note = new OrderNoteViewModel
            {
                OrdrId = orderId,
                NteTypeId = nteType,
                NteTxt = nte
            };

            var obj = _mapper.Map<OrdrNte>(note);
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.CreatByUserId = loggedInUser.UserId;
                obj.CreatDt = DateTime.Now;
            }

            _logger.LogInformation($"Creating Order Notes: { JsonConvert.SerializeObject(obj.NteId).ToString() }");

            return _orderNoteRepo.Create(obj);
        }
    }
}