﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/StdiReasons")]
    [ApiController]
    public class StdiReasonController : ControllerBase
    {
        private readonly IStdiReasonRepository _repo;
        private readonly ILogger<StdiReasonController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public StdiReasonController(IMapper mapper,
                               IStdiReasonRepository repo,
                               ILogger<StdiReasonController> logger,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<StdiReasonViewModel>> Get()
        {
            IEnumerable<StdiReasonViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.StdiReasonList, out list))
            {
                list = _mapper.Map<IEnumerable<StdiReasonViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.StdiReasDes));

                CacheManager.Set(_cache, CacheKeys.StdiReasonList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search STDI Reason by Id: { id }.");

            var obj = _repo.GetById(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<StdiReasonViewModel>(obj));
            }
            else
            {
                _logger.LogInformation($"STDI Reason by Id: { id } not found.");
                return NotFound(new { Message = $"STDI Reason Id: { id } not found." });
            }
        }
    }
}
