﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventSucssActy")]
    [ApiController]
    public class EventSucssActyController : ControllerBase
    {
        private readonly IEventSucssActyRepository _repo;
        private readonly ILogger<EventSucssActyController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public EventSucssActyController(IMapper mapper,
                               IEventSucssActyRepository repo,
                               ILogger<EventSucssActyController> logger,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EventSucssActyViewModel>> Get()
        {
            IEnumerable<EventSucssActyViewModel> list;
            //if (!_cache.TryGetValue(CacheKeys.EventTypeList, out list))
            //{
            list = _mapper.Map<IEnumerable<EventSucssActyViewModel>>(_repo
                .GetAll()
                .OrderBy(s => s.EventHistId));

            //    CacheManager.Set(_cache, CacheKeys.EventTypeList, list);
            //}

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Event Sucss Acty by  Id: { id }.");

            var obj = _repo.Find(s => s.SucssActyId == id).OrderByDescending(a => a.EventHistId);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<EventSucssActyViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Event Sucss Acty Id: { id } not found.");
                return NotFound(new { Message = $"Event Sucss Acty by  Id: { id } not found." });
            }
        }

        [HttpGet("GetbyHistId/{histId}")]
        public IActionResult GetbyHistId([FromRoute] int histId)
        {
            _logger.LogInformation($"Search Event Sucss Acty by  Id: { histId }.");

            var obj = _repo.Find(s => s.EventHistId == histId).OrderByDescending(a => a.EventHistId);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<EventSucssActyViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Event Sucss Acty HistId: { histId } not found.");
                return NotFound(new { Message = $"Event Sucss Acty by  HistId: { histId } not found." });
            }
        }

        [HttpGet("GetbyEventId/{Id}")]
        public IActionResult GetbyEventId([FromRoute] int Id)
        {
            _logger.LogInformation($"Search Event Sucss Acty by  eventId: { Id }.");

            var obj = _repo.GetByEventId(Id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<EventSucssActyViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Event Sucss Acty eventId: { Id } not found.");
                return NotFound(new { Message = $"Event Sucss Acty by  eventId: { Id } not found." });
            }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deleting Event Sucss Acty Type by Event Id: { id }.");

            var type = _repo.GetByEventId(id);
            if (type != null)
            {
                _repo.DeleteByEventId(id);
                _logger.LogInformation($"Event Sucss Acty Type by Event Id: { id } Deleted.");
            }
            else
            {
                _logger.LogInformation($"Deleting record failed due to Event Sucss Acty Type by Event Id: { id } not found.");
            }
        }
    }
}