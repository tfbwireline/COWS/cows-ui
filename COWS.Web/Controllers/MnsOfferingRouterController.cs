﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MnsOfferingRouters")]
    [ApiController]
    public class MnsOfferingRouterController : ControllerBase
    {
        private readonly IMnsOfferingRouterRepository _repo;
        private readonly ILogger<MnsOfferingRouterController> _logger;
        private readonly IMapper _mapper;

        public MnsOfferingRouterController(IMapper mapper,
                               IMnsOfferingRouterRepository repo,
                               ILogger<MnsOfferingRouterController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MnsOfferingRouterViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MnsOfferingRouterViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.MnsOffrgRoutrId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MNS Offering Router by Id: { id }.");

            var obj = _repo.Find(s => s.MnsOffrgRoutrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MnsOfferingRouterViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MNS Offering Router by Id: { id } not found.");
                return NotFound(new { Message = $"MNS Offering Router Id: { id } not found." });
            }
        }
    }
}