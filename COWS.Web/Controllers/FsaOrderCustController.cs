﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/FsaOrderCust")]
    [ApiController]
    public class FsaOrderCustController : ControllerBase
    {
        private readonly IFsaOrderCustRepository _repo;
        private readonly ILogger<FsaOrderCustController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public FsaOrderCustController(IMapper mapper,
                               IFsaOrderCustRepository repo,
                               ILogger<FsaOrderCustController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<FsaOrderCustViewModel>> Get()
        {
            IEnumerable<FsaOrderCustViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.FsaOrderCustList, out list))
            {
                list = _mapper.Map<IEnumerable<FsaOrderCustViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrId));

                CacheManager.Set(_cache, CacheKeys.FsaOrderCustList, list);
            }
           
            return Ok(list);
        }

        [HttpGet("{cisLevelId}/{id}")]
        public IActionResult Get([FromRoute] int id, [FromRoute] string cisLevelId)
        {
            _logger.LogInformation($"Search Order Cust by CisLvlId: { cisLevelId } and Id: { id }.");

            var obj = _repo.GetByOrderIdAndCISLevelType(id, cisLevelId, _loggedInUser.GetLoggedInUserCsgLvlId());
            if (obj != null)
            {
                return Ok(_mapper.Map<FsaOrderCustViewModel>(obj));
            }
            else
            {
                _logger.LogInformation($"Order Cust by CisLvlId: { cisLevelId } and Id: { id } not found.");
                return NotFound(new { Message = $"CisLvlId: { cisLevelId } and Order Id: { id } not found." });
            }
        }

        //[HttpPost]
        //public IActionResult Post([FromBody] FsaOrderCustViewModel model)
        //{
        //    _logger.LogInformation($"Create Event Type: { model.EventTypeNme }.");

        //    if (ModelState.IsValid)
        //    {
        //        var obj = _mapper.Map<LkEventType>(model);

        //        var loggedInUser = _loggedInUser.GetLoggedInUser();
        //        if (loggedInUser != null)
        //        {
        //            obj.CreatByUserId = loggedInUser.UserId;
        //            obj.CreatDt = DateTime.Now;
        //        }

        //        // Sarah Sandoval [20190915] - Added condition to check if name is duplicate
        //        var duplicate = _repo.Find(i => i.EventTypeNme.Trim().ToLower() == obj.EventTypeNme.Trim().ToLower()).SingleOrDefault();
        //        if (duplicate != null)
        //        {
        //            // Throw duplicate error if name already exists
        //            return BadRequest(new { Message = obj.EventTypeNme + " already exists." });
        //        }

        //        var rep = _repo.Create(obj);
        //        if (rep != null)
        //        {
        //            // Update Cache
        //            _cache.Remove(CacheKeys.EventTypeList);
        //            Get();
                    
        //            _logger.LogInformation($"Event Type Created. { JsonConvert.SerializeObject(model).ToString() } ");
        //            return Created($"api/EventTypes/{ rep.EventTypeId }", model);
        //        }
        //    }

        //    return BadRequest(new { Message = "Event Type Could Not Be Created." });
        //}

        //[HttpPut("{id}")]
        //public IActionResult Put([FromRoute] int id, [FromBody] FsaOrderCustViewModel model)
        //{
        //    _logger.LogInformation($"Update Event Type Id: { id }.");

        //    var obj = _mapper.Map<LkEventType>(model);

        //    var loggedInUser = _loggedInUser.GetLoggedInUser();
        //    if (loggedInUser != null)
        //    {
        //        obj.ModfdByUserId = loggedInUser.UserId;
        //        obj.ModfdDt = DateTime.Now;
        //    }

        //    // Sarah Sandoval [20190915] - Added condition to check if name is duplicate
        //    var duplicate = _repo.Find(i => i.EventTypeNme.Trim().ToLower() == obj.EventTypeNme.Trim().ToLower()).SingleOrDefault();
        //    if (duplicate != null)
        //    {
        //        // Throw duplicate error if name already exists
        //        return BadRequest(new { Message = obj.EventTypeNme + " already exists." });
        //    }

        //    _repo.Update(id, obj);

        //    // Update Cache
        //    _cache.Remove(CacheKeys.EventTypeList);
        //    Get();

        //    _logger.LogInformation($"Event Type Updated. { JsonConvert.SerializeObject(model) } ");
        //    return Created($"api/EventTypes/{ id }", model);
        //}

        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //    _logger.LogInformation($"Deleting Event Type by Id: { id }.");
        //    _repo.Delete(id);

        //    // Update Cache
        //    _cache.Remove(CacheKeys.EventTypeList);
        //    Get();
        //    _logger.LogInformation($"Event Type by Id: { id } has been deleted.");
        //}
    }
}
