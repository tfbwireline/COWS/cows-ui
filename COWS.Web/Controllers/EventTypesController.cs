﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventTypes")]
    [ApiController]
    public class EventTypesController : ControllerBase
    {
        private readonly IEventTypeRepository _repo;
        private readonly ILogger<EventTypesController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public EventTypesController(IMapper mapper,
                               IEventTypeRepository repo,
                               ILogger<EventTypesController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EventTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<EventTypeViewModel>>(_repo
                                                                .GetAll()
                                                                .Where(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.EventTypeNme));           
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Event Type by Id: { id }.");

            var obj = _repo.Find(s => s.EventTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<EventTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Event Type by Id: { id } not found.");
                return NotFound(new { Message = $"Event Type Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] EventTypeViewModel model)
        {
            _logger.LogInformation($"Create Event Type: { model.EventTypeNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkEventType>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Sarah Sandoval [20190915] - Added condition to check if name is duplicate
                //var duplicate = _repo.Find(i => i.EventTypeNme.Trim().ToLower() == obj.EventTypeNme.Trim().ToLower()).SingleOrDefault();
                //if (duplicate != null)
                //{
                //    // Throw duplicate error if name already exists
                //    return BadRequest(new { Message = obj.EventTypeNme + " already exists." });
                //}
                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive

                // Updated by Sarah Sandoval [20200611]
                // Above code will not be applicable since the removal of CAND Events
                var newData = new LkEventType();
                var duplicate = _repo.Find(i => i.EventTypeNme.Trim().ToLower() == obj.EventTypeNme.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.EventTypeNme + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.EventTypeNme = obj.EventTypeNme;
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.EventTypeId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {                    
                    _logger.LogInformation($"Event Type Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/EventTypes/{ newData.EventTypeId }", model);
                }
            }

            return BadRequest(new { Message = "Event Type Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] EventTypeViewModel model)
        {
            _logger.LogInformation($"Update Event Type Id: { id }.");

            var obj = _mapper.Map<LkEventType>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Sarah Sandoval [20190915] - Added condition to check if name is duplicate
            //var duplicate = _repo.Find(i => i.EventTypeNme.Trim().ToLower() == obj.EventTypeNme.Trim().ToLower()).SingleOrDefault();
            //if (duplicate != null)
            //{
            //    // Throw duplicate error if name already exists
            //    return BadRequest(new { Message = obj.EventTypeNme + " already exists." });
            //}
            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive

            // Updated by Sarah Sandoval [20200611]
            // Above code will not be applicable since the removal of CAND Events
            var duplicate = _repo.Find(i => i.EventTypeNme.Trim().ToLower() == obj.EventTypeNme.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.EventTypeNme + " already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.EventTypeId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Event Type Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/EventTypes/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            //_logger.LogInformation($"Deleting Event Type by Id: { id }.");
            //_repo.Delete(id);

            //// Update Cache
            //_cache.Remove(CacheKeys.EventTypeList);
            //Get();
            //_logger.LogInformation($"Event Type by Id: { id } has been deleted.");

            // Updated by Sarah Sandoval [20200611]
            // Above code will not be applicable since the removal of CAND Events
            _logger.LogInformation($"Deactivate Event Type by Id: { id }.");

            var evntype = _repo.Find(s => s.EventTypeId == id);
            if (evntype != null)
            {
                var obj = _mapper.Map<LkEventType>(evntype.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);

                _logger.LogInformation($"Event Type by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Event Type by Id: { id } not found.");
            }
        }
    }
}
