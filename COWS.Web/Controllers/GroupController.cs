﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Groups")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IGroupRepository _repo;
        private readonly ILogger<GroupController> _logger;
        private readonly IMapper _mapper;

        public GroupController(IMapper mapper,
                               IGroupRepository repo,
                               ILogger<GroupController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<GroupViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<GroupViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .Where(a => a.GrpId < 16)
                                                                .OrderBy(s => s.GrpNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Group by Id: { id }.");

            var obj = _repo.Find(s => s.GrpId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<GroupViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Group by Id: { id } not found.");
                return NotFound(new { Message = $"Group Id: { id } not found." });
            }
        }
    }
}