﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSEventNtwkTrpt")]
    [ApiController]
    public class MDSEventNtwkTrptController : ControllerBase
    {
        private readonly IMDSEventNtwkTrptRepository _repo;
        private readonly ILogger<MDSEventNtwkTrptController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MDSEventNtwkTrptController(IMapper mapper,
                               IMDSEventNtwkTrptRepository repo,
                               ILogger<MDSEventNtwkTrptController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"MDSEventNtwkTrpt by EventId: { id }.");

            var obj = _repo.GetMDSEventNtwkTrptByEventId(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSEventNtwkTrptViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDSEventNtwkTrpt by EventId: { id } not found.");
                return NotFound(new { Message = $"MDSEventNtwkTrpt by EventId: { id } not found." });
            }
        }
    }
}