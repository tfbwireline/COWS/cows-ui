﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VendorEmailTypes")]
    [ApiController]
    public class VendorEmailTypeController : ControllerBase
    {
        private readonly IVendorEmailTypeRepository _repo;
        private readonly IMapper _mapper;

        public VendorEmailTypeController(IMapper mapper,
                               IVendorEmailTypeRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VendorEmailTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<VendorEmailTypeViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.VndrEmailTypeDes));
            return Ok(list);
        }
    }
}
