﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/OrderNote")]
    [ApiController]
    public class OrderNoteController : ControllerBase
    {
        private readonly IOrderNoteRepository _repo;
        private readonly IVendorOrderRepository _vendorOrderRepo;
        private readonly ILogger<OrderNoteController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public OrderNoteController(IMapper mapper,
                               IOrderNoteRepository repo,
                               IVendorOrderRepository vendorOrderRepo,
                               ILogger<OrderNoteController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _vendorOrderRepo = vendorOrderRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OrderNoteViewModel>> Get()
        {
            IEnumerable<OrderNoteViewModel> list;
            //if (!_cache.TryGetValue(CacheKeys.EventTypeList, out list))
            //{
            list = _mapper.Map<IEnumerable<OrderNoteViewModel>>(_repo
                .GetAll()
                .OrderBy(s => s.NteId));

            //    CacheManager.Set(_cache, CacheKeys.EventTypeList, list);
            //}

            return Ok(list);
        }

        [HttpGet("{noteId}")]
        public IActionResult Get([FromRoute] int noteId)
        {
            _logger.LogInformation($"Search Event History by Event Id: { noteId }.");

            var obj = _repo.Find(s => s.NteId == noteId).OrderByDescending(a => a.NteId);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<OrderNoteViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Event History by Nte Id: { noteId } not found.");
                return NotFound(new { Message = $"Event History by Nte Id: { noteId } not found." });
            }
        }

        [HttpGet("{NteTypeId}")]
        public IActionResult GetByNoteTypeId([FromRoute] int NteTypeId)
        {
            _logger.LogInformation($"Search Event History by Event Id: { NteTypeId }.");

            var obj = _repo.Find(s => s.NteTypeId == NteTypeId).OrderByDescending(a => a.NteTypeId);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<OrderNoteViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Event History by NteType Id: { NteTypeId } not found.");
                return NotFound(new { Message = $"Event History by NteType Id: { NteTypeId } not found." });
            }
        }

        [HttpGet("order/{id}")]
        public IActionResult GetByid([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order Note by Ordr Id: { id }.");

            var obj = _repo.Find(s => s.OrdrId == id)
                .OrderByDescending(a => a.CreatDt)
                .ThenByDescending(a => a.NteId);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<OrderNoteViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Order Note by Ordr Id: { id } not found.");
                return NotFound(new { Message = $"Order Note by Ordr Id: { id } not found." });
            }
        }

        [HttpGet("order2/{id}")]
        public IActionResult GetByid2([FromRoute] int id)
        {
            _logger.LogInformation($"Search Order Note by Ordr Id: { id }.");

            var ordrId = _vendorOrderRepo.Find(x => x.VndrOrdrId == id).FirstOrDefault().OrdrId;


            var obj = _repo.Find(s => s.OrdrId == (ordrId != null ? ordrId : id))
                .OrderByDescending(a => a.CreatDt)
                .ThenByDescending(a => a.NteId);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<OrderNoteViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Order Note by Ordr Id: { id } not found.");
                return NotFound(new { Message = $"Order Note by Ordr Id: { id } not found." });
            }
        }

        [HttpGet("FindForGivenOrderAndNoteType")]
        public IActionResult GetWFMUserAssignments([FromQuery] int orderId, [FromQuery] int noteTypeId, [FromQuery] string sortExpression, [FromQuery] int PrntPrfID, [FromQuery]int userID)
        {
            _logger.LogInformation($"Search Order Note.");
            var obj = _repo.FindForGivenOrderAndNoteType(orderId, noteTypeId, sortExpression, PrntPrfID, userID);
            if (obj != null)
            {
                var list = _mapper.Map<IEnumerable<OrderNoteViewModel>>(obj.ToList());
                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"Order Note  not found.");
                return NotFound(new { Message = $"Order Note  not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] OrderNoteViewModel model)
        {
            _logger.LogInformation($"Create Order Note: { model.NteTypeId }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<OrdrNte>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                var rep = _repo.Create(obj);
                if (rep != null)
                {
                    _logger.LogInformation($"Order Note Created. {  JsonConvert.SerializeObject(model).ToString() } ");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "Could Not Inserted WFM User Assignments." });
                }
            }

            return BadRequest(new { Message = "Event Type Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] OrderNoteViewModel model)
        {
            _logger.LogInformation($"Update Order Note Id: { id }.");

            var obj = _mapper.Map<OrdrNte>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Order Note Updated. { JsonConvert.SerializeObject(obj) } ");
            return Ok(true);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating Order Note by Id: { id }.");

            var note = _repo.Find(s => s.NteId == id);
            if (note != null)
            {
                //var obj = _mapper.Map<OrdrNte>(type.SingleOrDefault());
                //var loggedInUser = _loggedInUser.GetLoggedInUser();
                //if (loggedInUser != null)
                //{
                //    obj.ModfdByUserId = loggedInUser.UserId;
                //    obj.ModfdDt = DateTime.Now;
                //    obj.RecStusId = 0;
                //}

                _repo.Delete(id);

                // Update Cache
                //_cache.Remove(CacheKeys.EventTypeList);
                //Get();
                _logger.LogInformation($"Order Note Deleted by Id: { id } .");
            }
            else
            {
                _logger.LogInformation($"Deleting  record failed due to Order Note by Id: { id } not found.");
            }
        }

        [HttpPost("CreateNote")]
        public IActionResult CreateNote([FromBody] OrderNoteViewModel model)
        {
            _logger.LogInformation($"Create Order Note: { model.NteTypeId }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<OrdrNte>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                var rep = _repo.CreateNote(obj);
                if (rep != 0)
                {
                    _logger.LogInformation($"Order Note Created. {  JsonConvert.SerializeObject(model).ToString() } ");
                    return Ok(rep);
                }
            }

            return BadRequest(new { Message = "Event Type Could Not Be Created." });
        }
    }
}