﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventAsnToUsers")]
    [ApiController]
    public class EventAsnToUsersController : ControllerBase
    {
        private readonly IEventAsnToUserRepository _repo;
        private readonly IUserRepository _userRepo;
        private readonly ILogger<EventAsnToUsersController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public EventAsnToUsersController(IMapper mapper,
                               IEventAsnToUserRepository repo,
                               IUserRepository userRepo,
                               ILogger<EventAsnToUsersController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _userRepo = userRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EventAsnToUserViewModel>> Get()
        {
            IEnumerable<EventAsnToUserViewModel> list = _mapper.Map<IEnumerable<EventAsnToUserViewModel>>(_repo
                .Find(s => s.RecStusId == 1)
                .OrderBy(s => s.AsnToUser.FullNme));

            return Ok(list);
        }

        [HttpGet("Event/{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Assigned Activators by Event Id: { id }.");

            var obj = _repo.Find(a => a.EventId == id).Where(a=>a.RecStusId == 1).ToList();
            if (obj != null)
            {
                //List<EventAsnToUserViewModel> result = new List<EventAsnToUserViewModel>();

                //foreach (EventAsnToUserViewModel res in _mapper.Map<IEnumerable<EventAsnToUserViewModel>>(obj.ToList()))
                //{
                //    res.UserDsplNme = _userRepo.GetById(res.AsnToUserId).DsplNme;

                //    result.Add(res);
                //}

                //return Ok(result.AsEnumerable());

                IEnumerable<EventAsnToUserViewModel> list = _mapper.Map<IEnumerable<EventAsnToUserViewModel>>(obj);
                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"Event Assigned Activators by Event Id: { id } not found.");
                return NotFound(new { Message = $"Assigned Activators by Event Id: { id } not found." });
            }
        }
    }
}