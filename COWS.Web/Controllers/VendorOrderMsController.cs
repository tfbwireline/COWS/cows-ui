﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VendorOrderMs")]
    [ApiController]
    public class VendorOrderMsController : ControllerBase
    {
        private readonly ILogger<VendorOrderMsController> _logger;
        private readonly IMapper _mapper;
        private readonly IVendorOrderMsRepository _repo;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public VendorOrderMsController(IMapper mapper,
                               IVendorOrderMsRepository repo,
                               ILogger<VendorOrderMsController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpPost("")]
        public IActionResult Post([FromBody] VendorOrderMsViewModel model)
        {
            _logger.LogInformation($"Create Vendor Order Ms: { model }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrOrdrMs>(model);
                obj.SentToVndrDt = obj.SentToVndrDt;
                obj.AckByVndrDt = obj.AckByVndrDt;
                obj.VerId = 99; // used in front end side as flag
                obj.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                obj.CreatDt = DateTime.Now;

                var rep = _repo.Create(obj);

                if (rep != null)
                {
                    _logger.LogInformation($"VendorOrderMs Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/VendorOrderMs/{ rep.VndrOrdrMsId }", rep);
                }
            }

            return BadRequest(new { Message = "Vendor Order Milestone Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] VendorOrderMsViewModel model)
        {
            _logger.LogInformation($"VendorOrderMs Id: { id }.");

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            var data = _repo.Find(i => i.VndrOrdrMsId == id).SingleOrDefault();
            if (data != null)
            {
                data.SentToVndrDt = model.SentToVndrDt;
                data.AckByVndrDt = model.AckByVndrDt;
            }
            _repo.Update(id, data);
          
            _logger.LogInformation($"VendorOrderMs Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/VendorOrderMs/{ id }", model);
        }
    }
}
