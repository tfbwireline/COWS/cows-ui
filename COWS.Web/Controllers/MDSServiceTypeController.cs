﻿using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSServiceTypes")]
    [ApiController]
    public class MDSServiceTypeController : ControllerBase
    {
        private readonly IMDSServiceTypeRepository _repo;

        public MDSServiceTypeController(IMDSServiceTypeRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MDSServiceTypeViewModel>> Get()
        {
           var list = _repo.Find(i => i.RecStusId == (byte)ERecStatus.Active);
            return Ok(list);
        }
    }
}