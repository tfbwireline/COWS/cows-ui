﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VendorFolder")]
    [ApiController]
    public class VendorFolderController : ControllerBase
    {
        private readonly IVendorFolderRepository _repo;
        private readonly IVendorFolderContactRepository _contactRepo;
        private readonly IVendorTemplateRepository _templateRepo;
        private readonly ILogger<VendorFolderController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public VendorFolderController(IMapper mapper,
                       IVendorFolderRepository repo,
                       IVendorFolderContactRepository contactRepo,
                       IVendorTemplateRepository templateRepo,
                       ILogger<VendorFolderController> logger,
                       ILoggedInUserService loggedInUser,
                       IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _contactRepo = contactRepo;
            _templateRepo = templateRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VendorFolderViewModel>> Get()
        {
            IEnumerable<VendorFolderViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.VendorFolderList, out list))
            {
                list = _mapper.Map<IEnumerable<VendorFolderViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active));

                CacheManager.Set(_cache, CacheKeys.VendorFolderList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Folder by Id: { id }.");

            var obj = _repo.Find(s => s.VndrFoldrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<VendorFolderViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Vendor Folder by Id: { id } not found.");
                return NotFound(new { Message = $"Vendor Folder Id: { id } not found." });
            }
        }

        [HttpGet("{id}/Contacts")]
        public IActionResult GetContactsByFolderId([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Folder Contacts by Folder Id: { id }.");

            var obj = _contactRepo.Find(s => s.VndrFoldrId == id);
            if (obj != null)
            {
                var ass = obj.Count();
                return Ok(_mapper.Map<IEnumerable<VendorFolderContactViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Vendor Folder Contacts by Folder Id: { id } not found.");
                return NotFound(new { Message = $"Vendor Folder Contacts by Folder Id: { id } not found." });
            }
        }

        [HttpGet("{id}/Templates")]
        public IActionResult GetTemplatesByFolderId([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Folder Templates by Folder Id: { id }.");

            var obj = _templateRepo.Find(s => s.VndrFoldrId == id);
            if (obj != null)
            {
                var ass = obj.Count();
                return Ok(_mapper.Map<IEnumerable<VendorTemplateViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Vendor Folder Templates by Folder Id: { id } not found.");
                return NotFound(new { Message = $"Vendor Folder Templates by Folder Id: { id } not found." });
            }
        }

        [HttpGet("GetVendorFolder")]
        public IActionResult GetVendorFolder([FromQuery] string vendorName, [FromQuery] string countryCode)
        {
            if (string.IsNullOrWhiteSpace(vendorName) || vendorName == "null") vendorName = string.Empty;
            if (string.IsNullOrWhiteSpace(countryCode) || countryCode == "null") countryCode = string.Empty;
            if (string.IsNullOrWhiteSpace(countryCode) && string.IsNullOrWhiteSpace(vendorName))
                return NotFound(new { Message = $"Vendor Folder not found." });

            var objList = _repo.GetVendorFolder(vendorName, countryCode);
            if (objList != null && objList.Count() > 0)
            {
                List<VendorFolderViewModel> vndrFldrList = new List<VendorFolderViewModel>();
                VendorFolderViewModel vndrFldr;
                foreach (var item in objList)
                {
                    vndrFldr = new VendorFolderViewModel();
                    vndrFldr.VndrFoldrId = item.VNDR_FOLDR_ID;
                    vndrFldr.VndrNme = item.VNDR_NME;
                    vndrFldr.VndrCd = item.VNDR_CD;
                    vndrFldr.CtryCd = item.CTRY_CD;
                    vndrFldr.CtryNme = item.CTRY_NME;
                    vndrFldr.TemplateCnt = item.VNDR_TMPLT_CNT;
                    vndrFldrList.Add(vndrFldr);
                }
                return Ok(vndrFldrList);
            }
            else
            {
                return NotFound(new { Message = $"Vendor Folder not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] VendorFolderViewModel model)
        {
            _logger.LogInformation($"Create Vendor Folder by Vendor Code: { model.VndrCd }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrFoldr>(model);
                obj.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                obj.CreatDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;

                // Check for unique Vendor Folder
                var searchVendor = _repo.Find(i => i.VndrCd == obj.VndrCd && i.CtryCd == obj.CtryCd).ToList();
                if (searchVendor != null && searchVendor.Count() > 0)
                {
                    return BadRequest(new { Message = "Folder already exists, unable to add duplicate Vendor/Country combination." });
                }

                obj.VndrTmplt = GetVendorTemplate(obj.VndrFoldrId, model.VndrTmplt.ToList());
                obj.VndrFoldrCntct = GetVendorFolderContact(obj.VndrFoldrId, model.VndrFoldrCntct.ToList());

                var rep = _repo.Create(obj);

                if (rep != null)
                {
                    _logger.LogInformation($"Vendor Folder Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/VendorFolder/{ rep.VndrFoldrId }", model);
                }
            }

            return BadRequest(new { Message = "Vendor Folder Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] VendorFolderViewModel model)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation($"Update Vendor Folder with Id: { id }.");

                var obj = _mapper.Map<VndrFoldr>(model);
                obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                obj.ModfdDt = DateTime.Now;

                // Check for unique Vendor Folder
                var searchVendor = _repo.Find(i => i.VndrCd == obj.VndrCd
                                        && i.CtryCd == obj.CtryCd && i.VndrFoldrId != id).ToList();
                if (searchVendor != null && searchVendor.Count() > 0)
                {
                    return BadRequest(new { Message = "Folder already exists, unable to add duplicate Vendor/Country combination." });
                }

                obj.VndrTmplt = GetVendorTemplate(obj.VndrFoldrId, model.VndrTmplt.ToList());
                obj.VndrFoldrCntct = GetVendorFolderContact(obj.VndrFoldrId, model.VndrFoldrCntct.ToList());

                _repo.Update(id, obj);

                _logger.LogInformation($"Vendor Folder Updated. { JsonConvert.SerializeObject(model).ToString() } ");
                return Created($"api/VendorFolder/{ id }", obj);
            }

            return BadRequest(new { Message = "Vendor Folder Could Not Be Updated." });
        }

        private List<VndrTmplt> GetVendorTemplate(int vndrFldrId, List<VendorTemplateViewModel> templates)
        {
            List<VndrTmplt> vndrTmplts = new List<VndrTmplt>();

            foreach (VendorTemplateViewModel template in templates)
            {
                VndrTmplt vndrTmplt = _mapper.Map<VndrTmplt>(template);
                vndrTmplt.VndrFoldrId = template.VndrFoldrId > 0 ? template.VndrFoldrId : vndrFldrId;
                vndrTmplt.VndrTmpltId = template.VndrFoldrId > 0 ? template.VndrTmpltId : 0;
                if (template.FileCntnt == null)
                {
                    vndrTmplt.FileCntnt = Convert.FromBase64String(template.Base64string);
                    vndrTmplt.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                    vndrTmplt.CreatDt = DateTime.Now;
                    vndrTmplt.RecStusId = (byte)ERecStatus.Active;
                }
                
                vndrTmplts.Add(vndrTmplt);
            }

            return vndrTmplts;
        }

        private List<VndrFoldrCntct> GetVendorFolderContact(int vndrFldrId, List<VendorFolderContactViewModel> contacts)
        {
            List<VndrFoldrCntct> vndrCntcts = new List<VndrFoldrCntct>();

            foreach (VendorFolderContactViewModel contact in contacts)
            {
                VndrFoldrCntct vndrCntct = _mapper.Map<VndrFoldrCntct>(contact);
                vndrCntct.VndrFoldrId = contact.VndrFoldrId > 0 ? contact.VndrFoldrId : vndrFldrId;
                vndrCntct.CntctId = contact.VndrFoldrId > 0 ? contact.CntctId : 0;
                vndrCntct.CntctFrstNme = contact.CntctFrstNme;
                vndrCntct.CntctLstNme = contact.CntctLstNme;
                vndrCntct.CntctPhnNbr = contact.CntctPhnNbr;
                vndrCntct.CntctEmailAdr = contact.CntctEmailAdr;
                vndrCntct.CntctProdRoleTxt = contact.CntctProdRoleTxt;
                vndrCntct.CntctProdTypeTxt = contact.CntctProdTypeTxt;
                vndrCntct.CreatDt = DateTime.Now;
                vndrCntcts.Add(vndrCntct);
            }

            return vndrCntcts;
        }

        //[HttpGet("GetVendorOrderEmails")]
        //public IActionResult GetVendorOrderEmails([FromQuery] int vendorOrderID)
        //{
        //    return Ok(_repo.GetVendorOrderEmails(vendorOrderID));
        //}
    }
}