﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CancelOrdr")]
    [ApiController]
    public class CancelOrdrController : ControllerBase
    {
        private readonly IOrdrCancelRepository _CancelRepo;
        private readonly ILogger<CancelOrdrController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;
        private readonly ILoggedInUserService _loggedInUser;

        public CancelOrdrController(IMapper mapper,
                               IOrdrCancelRepository UnlockRepo,
                               ILogger<CancelOrdrController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _CancelRepo = UnlockRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = memoryCache;
        }

        [HttpPost]
        //[HttpGet("")]
        public IActionResult Post([FromBody] GetCancelOrdrViewModel model)
        {
            _logger.LogInformation($"Starterted Cancel Operation:..");

            if (ModelState.IsValid)
            {
                int USER_ID = 0;

                //IEnumerable<GetCancelOrdrViewModel> ordrView;
                model.notes = model.notes == null ? "Cancel was initiated from COWS for this FTN" : model.notes;
                model.notes = model.notes == "" ? "Cancel was initiated from COWS for this FTN" : model.notes;
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    USER_ID = loggedInUser.UserId;
                }

                // logging for audit
                _logger.LogInformation($"Starterted Cancelled FTN: { model.FTN }.");

                var data = _CancelRepo.cancelOrdr(model.FTN, USER_ID, model.notes);

                if (data != 0)
                {
                    // Key not in cache, so get data.
                    //ordrView = _mapper.Map<IEnumerable<GetOrdrUnlockOrdrViewModel>>(data);
                    //ordrView = (data);
                    //CacheManager.Set(_cache, CacheKeys.ordrView, ordrView);
                    _logger.LogInformation($"Cancelled FTN: { model.FTN }.");
                    return Ok(data);
                }
                else
                {
                    _logger.LogInformation($"Cancelled FTN { model.FTN }. Not Successfull");
                    return NotFound(new { Message = $"Cancelled FTN: { model.FTN }. Not Successfull" });
                }
            }
            return BadRequest(new { Message = "Cancelled FTN can not be Done." });
        }

        [HttpGet("")]
        //[ResponseCache(CacheProfileName = "Default")]
        //[Authorize]
        [ActionName("Search")]
        //[RequestHeaderMatchesMediaType("Accept", new[] { "application/vnd.sst.tracker.options+json", "application/json" })]
        public IActionResult Get([FromQuery] string ftn, [FromQuery] string h5_H6)
        {
            if (string.IsNullOrEmpty(ftn) && string.IsNullOrEmpty(h5_H6))
            {
                return BadRequest(new { Message = "Both FTN and H5/H6 cannot be blank." });
            }
            IEnumerable<GetCancelOrdrViewModel> ordrView;

            if (string.IsNullOrEmpty(ftn)) ftn = " ";

            //// logging for audit
            _logger.LogInformation($"Get  lock Ordr View");

            var data = _CancelRepo.GetOrdrCancelView(ftn, h5_H6);

            if (data != null)
            {
                // Key not in cache, so get data.
                ordrView = _mapper.Map<IEnumerable<GetCancelOrdrViewModel>>(data);
                //CacheManager.Set(_cache, CacheKeys.ordrView, ordrView);
                return Ok(ordrView);
            }
            else
            {
                _logger.LogInformation($"Cancel Ordr View not found.");
                return NotFound(new { Message = $"Cancel Ordr View not found." });
            }
        }
    }
}