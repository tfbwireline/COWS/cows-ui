﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/ServiceAssuranceSiteSupport")]
    [ApiController]
    public class ServiceAssuranceSiteSupportController : ControllerBase
    {
        private readonly IServiceAssuranceRepository _repo;
        private readonly ILogger<ServiceAssuranceSiteSupportController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public ServiceAssuranceSiteSupportController(IMapper mapper,
                               IServiceAssuranceRepository repo,
                               ILogger<ServiceAssuranceSiteSupportController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ServiceAssuranceSiteSupportViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<ServiceAssuranceSiteSupportViewModel>>(_repo
                                                                 .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                 .OrderBy(s => s.SrvcAssrnSiteSuppDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Service Assurance Site Support by Id: { id }.");

            var obj = _repo.Find(s => s.SrvcAssrnSiteSuppId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<ServiceAssuranceSiteSupportViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Service Assurance Site Support by Id: { id } not found.");
                return NotFound(new { Message = $"Service Assurance Site Support Id: { id } not found." });
            }
        }

        [HttpGet("GetForLookup")]
        public ActionResult<IEnumerable<ServiceAssuranceSiteSupportViewModel>> GetForLookup()
        {
            var list = _mapper.Map<IEnumerable<ServiceAssuranceSiteSupportViewModel>>(_repo.GetAll()
                                                                 .OrderBy(s => s.SrvcAssrnSiteSuppDes));
            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ServiceAssuranceSiteSupportViewModel model)
        {
            _logger.LogInformation($"Create Service Assurance Site Support: { model.SrvcAssrnSiteSuppDes }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkSrvcAssrnSiteSupp>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkSrvcAssrnSiteSupp();
                var duplicate = _repo.Find(i => i.SrvcAssrnSiteSuppDes.Trim().ToLower() == obj.SrvcAssrnSiteSuppDes.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.SrvcAssrnSiteSuppDes + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.SrvcAssrnSiteSuppId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {

                    _logger.LogInformation($"Service Assurance Site Support Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/ServiceAssuranceSiteSupport/{ newData.SrvcAssrnSiteSuppId }", model);
                }
            }

            return BadRequest(new { Message = "Service Assurance Site Support Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] ServiceAssuranceSiteSupportViewModel model)
        {
            _logger.LogInformation($"Update Service Assurance Site Support Id: { id }.");

            var obj = _mapper.Map<LkSrvcAssrnSiteSupp>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.SrvcAssrnSiteSuppDes.Trim().ToLower() == obj.SrvcAssrnSiteSuppDes.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.SrvcAssrnSiteSuppDes + " already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.SrvcAssrnSiteSuppId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Service Assurance Site Support Updated. { JsonConvert.SerializeObject(model).ToString() } ");
            return Created($"api/ServiceAssuranceSiteSupport/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivate Service Assurance Site Support by Id: { id }.");

            var srvcAssurance = _repo.Find(s => s.SrvcAssrnSiteSuppId == id);
            if (srvcAssurance != null)
            {
                var obj = _mapper.Map<LkSrvcAssrnSiteSupp>(srvcAssurance.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);
                
                _logger.LogInformation($"Service Assurance Site Support by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Service Assurance Site Support by Id: { id } not found.");
            }
        }
    }
}