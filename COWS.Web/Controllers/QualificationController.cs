﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Qualifications")]
    [ApiController]
    public class QualificationController : ControllerBase
    {
        private readonly IQualificationRepository _repo;
        private readonly ILogger<QualificationController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public QualificationController(IMapper mapper,
                               IQualificationRepository repo,
                               ILogger<QualificationController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<GetQualificationViewModel>> Get()
        {
            IEnumerable<GetQualificationViewModel> list = _mapper.Map<IEnumerable<GetQualificationViewModel>>(_repo
                                                                .GetQualificationView(_loggedInUser.GetLoggedInUserId())
                                                                .OrderBy(s => s.AssignToUser));

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Qualification by Id: { id }.");
            var obj = _repo.Find(s => s.QlfctnId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<GetQualificationViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Qualification by Id: { id } not found.");
                return NotFound(new { Message = $"Qualification Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] GetQualificationViewModel model)
        {
            try
            {
                _logger.LogInformation($"Create Qualification: { model.QualificationID }.");

                var obj = _mapper.Map<GetQualificationView>(model);
                obj.CreatedByUserID = _loggedInUser.GetLoggedInUserId();
                obj.CreatedDate = DateTime.Now;

                var rep = _repo.Create(obj);

                var json = new JsonSerializerSettings()
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    Formatting = Formatting.Indented
                };
                _logger.LogInformation($"Qualification Created. { JsonConvert.SerializeObject(rep, json) } ");
                return Created($"api/Qualifications/{ rep.QlfctnId }", rep);
            }
            catch (Exception)
            {
                return BadRequest(new { Message = "Qualification Could Not Be Created." });
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] GetQualificationViewModel model)
        {
            _logger.LogInformation($"Update Qualification Id: { id }.");

            var obj = _mapper.Map<GetQualificationView>(model);
            obj.ModifiedByUserID = _loggedInUser.GetLoggedInUserId();
            obj.ModifiedDate = DateTime.Now;

            _repo.Update(id, obj);

            _logger.LogInformation($"Qualification Updated. { JsonConvert.SerializeObject(obj) } ");
            return Created($"api/Qualifications/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deleting Qualification by Id: { id }.");
            _repo.Delete(id);
            _logger.LogInformation($"Qualification by Id: { id } Deleted.");
        }

        [HttpGet("CheckDuplicate")]
        public IActionResult CheckDuplicate([FromQuery] int assignedUserId)
        {
            return Ok(_repo.CheckDuplicate(assignedUserId));
        }
    }
}