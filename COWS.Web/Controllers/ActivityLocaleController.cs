﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/ActivityLocale")]
    [ApiController]
    public class ActivityLocaleController : ControllerBase
    {
        private readonly IActivityLocaleRepository _repo;
        private readonly ILogger<ActivityLocaleController> _logger;
        private readonly IMapper _mapper;

        public ActivityLocaleController(IMapper mapper,
                               IActivityLocaleRepository repo,
                               ILogger<ActivityLocaleController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ActivityLocaleViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<ActivityLocaleViewModel>>(_repo
                                                               .Find(s => s.RecStusId == 1)
                                                               .OrderBy(s => s.ActyLocaleDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] string id)
        {
            _logger.LogInformation($"Search Activity Locale by Id: { id }.");

            var obj = _repo.Find(s => s.ActyLocaleId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<ActivityLocaleViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Activity Locale by Id: { id } not found.");
                return NotFound(new { Message = $"Activity Locale Id: { id } not found." });
            }
        }
    }
}