﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/DedicatedCustomers")]
    [ApiController]
    public class DedicatedCustomersController : ControllerBase
    {
        private readonly IDedicatedCustomerRepository _repo;
        private readonly ILogger<DedicatedCustomersController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public DedicatedCustomersController(IMapper mapper,
                               IDedicatedCustomerRepository repo,
                               ILogger<DedicatedCustomersController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<DedicatedCustomerViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<DedicatedCustomerViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.CustNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Dedicated Customer by Id: { id }.");

            var obj = _repo.Find(s => s.CustId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<DedicatedCustomerViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Dedicated Customer by Id: { id } not found.");
                return NotFound(new { Message = $"Dedicated Customer Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] DedicatedCustomerViewModel model)
        {
            _logger.LogInformation($"Create Dedicated Customer: { model.CustNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkDedctdCust>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkDedctdCust();
                var duplicate = _repo.Find(i => i.CustNme.Trim().ToLower() == obj.CustNme.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId.HasValue
                        && duplicate.RecStusId.Value == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.CustNme + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.CustId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    _logger.LogInformation($"Dedicated Customer Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/DedicatedCustomers/{ newData.CustId }", model);
                }
            }

            return BadRequest(new { Message = "Dedicated Customer Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] DedicatedCustomerViewModel model)
        {
            _logger.LogInformation($"Update Dedicated Customer Id: { id }.");

            var obj = _mapper.Map<LkDedctdCust>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.CustNme.Trim().ToLower() == obj.CustNme.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId.HasValue
                    && duplicate.RecStusId.Value == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.CustNme + " already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.CustId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Dedicated Customer Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/DedicatedCustomers/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating Dedicated Customer by Id: { id }.");

            var cust = _repo.Find(s => s.CustId == id);
            if (cust != null)
            {
                var obj = _mapper.Map<LkDedctdCust>(cust.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);

                _logger.LogInformation($"Dedicated Customer by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Dedicated Customer by Id: { id } not found.");
            }
        }
    }
}