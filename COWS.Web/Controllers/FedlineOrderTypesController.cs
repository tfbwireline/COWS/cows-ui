﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/FedlineOrderTypes")]
    [ApiController]
    public class FedlineOrderTypesController : ControllerBase
    {
        private readonly IFedlineOrderTypeRepository _repo;
        private readonly IMapper _mapper;

        public FedlineOrderTypesController(IMapper mapper,
                               IFedlineOrderTypeRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<FedlineOrderTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<FedlineOrderTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.PrntOrdrTypeDes));
            return Ok(list);
        }
    }
}