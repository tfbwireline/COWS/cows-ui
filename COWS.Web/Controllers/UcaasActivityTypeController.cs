﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/UcaasActivityTypes")]
    [ApiController]
    public class UcaasActivityTypeController : ControllerBase
    {
        private readonly IUcaasActivityTypeRepository _repo;
        private readonly ILogger<UcaasActivityTypeController> _logger;
        private readonly IMapper _mapper;

        public UcaasActivityTypeController(IMapper mapper,
                               IUcaasActivityTypeRepository repo,
                               ILogger<UcaasActivityTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UcaasActivityTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<UcaasActivityTypeViewModel>>(_repo
                                                              .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                              .OrderBy(s => s.UcaaSActyTypeId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search UCaaS Activity Type by Id: { id }.");

            var obj = _repo.Find(s => s.UcaaSActyTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<UcaasActivityTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"UCaaS Activity Type by Id: { id } not found.");
                return NotFound(new { Message = $"UCaaS Activity Type Id: { id } not found." });
            }
        }
    }
}