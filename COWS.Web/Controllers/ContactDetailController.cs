﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/ContactDetails")]
    [ApiController]
    public class ContactDetailController : ControllerBase
    {
        private readonly IContactDetailRepository _repo;
        private readonly ILogger<ContactDetailController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public ContactDetailController(IMapper mapper,
                               IContactDetailRepository repo,
                               ILogger<ContactDetailController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ContactDetailViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<ContactDetailViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.Role));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Contact Details by Id: { id }.");

            var obj = _repo.Find(s => s.Id == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<ContactDetailViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Contact Details by Id: { id } not found.");
                return NotFound(new { Message = $"Contact Detail Id: { id } not found." });
            }
        }

        [HttpGet("GetContactDetails")]
        public IActionResult GetContactDetails([FromQuery] int objId, [FromQuery] string objType, [FromQuery] string hierId,
            [FromQuery] string hierLvl, [FromQuery] string odieCustId, [FromQuery] bool isNetworkOnly)
        {
            var now = DateTime.Now;
            List<ContactDetailViewModel> contactList = new List<ContactDetailViewModel>();

            DataTable dt = new DataTable();
            int loggedInUserId= _loggedInUser.GetLoggedInUserId();
            var noww = DateTime.Now;
            dt = _repo.GetContactDetails(objId, objType, loggedInUserId, hierId, hierLvl, odieCustId, isNetworkOnly);
            var nowww = DateTime.Now;

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ContactDetailViewModel cdvm = new ContactDetailViewModel();
                    cdvm.Id = Convert.ToInt32(dr["ID"].ToString());
                    cdvm.ObjId = Convert.ToInt32(dr["OBJ_ID"].ToString());
                    cdvm.HierId = dr["HIER_ID"].ToString();
                    cdvm.HierLvlCd = dr["HIER_LVL_CD"].ToString();
                    cdvm.RoleId = byte.Parse(dr["ROLE_ID"].ToString());
                    cdvm.RoleNme = dr["ROLE_NME"].ToString();
                    cdvm.EmailAdr = dr["EMAIL_ADR"].ToString();
                    cdvm.PhnNbr = dr["PHN_NBR"].ToString();
                    cdvm.EmailCd = dr["EMAIL_CD"].ToString();
                    // Added by Sarah Sandoval [20210720] - AUTO_RFRSH_CD returns either 0/1 or true/false
                    bool autoRefresh = false;
                    bool isSucceed = Boolean.TryParse(dr["AUTO_RFRSH_CD"].ToString(), out autoRefresh);
                    cdvm.AutoRfrshCd = isSucceed ? autoRefresh : dr["AUTO_RFRSH_CD"].ToString() == "1" ? true : false;
                    bool suppressEmail = false;
                    Boolean.TryParse(dr["SUPRS_EMAIL"].ToString(), out suppressEmail);
                    cdvm.SuprsEmail = suppressEmail;
                    cdvm.CreatByUserId = Convert.ToInt32(dr["CREAT_BY_USER_ID"]);

                    contactList.Add(cdvm);
                    var nowwww = DateTime.Now;
                }
            }
            var nowwwww = DateTime.Now;

            return Ok(contactList);
        }
    }
}
