﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/RedesignWorkflow")]
    [ApiController]
    public class RedesignWorkflowController : ControllerBase
    {
        private readonly IRedesignWorkflowRepository _repo;
        private readonly ILogger<RedesignWorkflowController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public RedesignWorkflowController(IMapper mapper,
                               IRedesignWorkflowRepository repo,
                               ILogger<RedesignWorkflowController> logger,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<RedesignWorkflowViewModel>> Get()
        {
            IEnumerable<RedesignWorkflowViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.RedesignWorkflowList, out list))
            {
                list = _mapper.Map<IEnumerable<RedesignWorkflowViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.DesrdWrkflwStus.StusDes));

                CacheManager.Set(_cache, CacheKeys.RedesignWorkflowList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Redesign Workflow by Id: { id }.");

            var obj = _repo.Find(s => s.RedsgnWrkflwId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<RedesignWorkflowViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Redesign Workflow by Id: { id } not found.");
                return NotFound(new { Message = $"Redesign Workflow Id: { id } not found." });
            }
        }

        [HttpGet("GetByStatus/{id}")]
        public ActionResult<IEnumerable<RedesignWorkflowViewModel>> GetByStatus([FromRoute] int id)
        {
            _logger.LogInformation($"Search Redesign Status by Id: { id }.");

            return Ok(_mapper.Map<IEnumerable<RedesignWorkflowViewModel>>(_repo
                                                    .Find(s => s.PreWrkflwStusId == id
                                                        && s.RecStusId == (byte)ERecStatus.Active)
                                                    .OrderBy(s => s.DesrdWrkflwStus.StusDes)));
        }
    }
}
