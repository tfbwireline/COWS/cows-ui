﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Data;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/WorkGroup")]
    [ApiController]
    public class WorkGroupController : ControllerBase
    {
        private readonly IWorkGroupRepository _repo;
        private readonly ILogger<WorkGroupController> _logger;
        private readonly ICommonRepository _repoCommon;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public WorkGroupController(IMapper mapper,
                               IWorkGroupRepository repo,
                               ICommonRepository repoCommon,
                               ILogger<WorkGroupController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _repoCommon = repoCommon;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }


        [HttpGet("GetOrderDisplayViewData")]
        public ActionResult<DataSet> GetOrderDisplayViewData([FromQuery] int userId, [FromQuery] int usrPrfId)
        {
            var dataSet = _repo.GetOrderDisplayViewData(userId, usrPrfId);
            if (dataSet != null)
            {
                return Ok(dataSet);
            }
            return BadRequest(new { Message = "No Order Views found for this Profile." });
        }

        [HttpGet("GetWGData")]
        public IActionResult GetWGData([FromQuery] int usrPrfId, [FromQuery] int userId, [FromQuery] int orderId, [FromQuery] bool isCmplt, [FromQuery] string filter)
        {
            var userCsgLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            return Ok(_repo.GetWGData(usrPrfId, userId, orderId, isCmplt, userCsgLevel, filter).Result);
        }

        [HttpGet("VerifyUserIsAdminToEditCompletedOrder")]
        public IActionResult VerifyUserIsAdminToEditCompletedOrder([FromQuery] string userADID, [FromQuery] string parameter)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.VerifyUserIsAdminToEditCompletedOrder(userADID, parameter);
                if (rep != 0)
                {
                    _logger.LogInformation("VerifyUserIsAdminToEditCompletedOrder Success");
                    return Ok(true);
                }
                else
                {
                    return Ok(false);
                }
            }
            return BadRequest(new { Message = "VerifyUserIsAdminToEditCompletedOrder Failed." });
        }

        [HttpGet("GetFTNListDetails")]
        public IActionResult GetFTNListDetails([FromQuery] int orderId, [FromQuery] int usrPrfId)
        {
            return Ok(_repo.GetFTNListDetails(orderId, usrPrfId, _loggedInUser.GetLoggedInUserCsgLvlId()).Result);

        }

        //[HttpGet("CompleteActiveTask")]
        //public IActionResult CompleteActiveTask([FromQuery] int orderID, [FromQuery] int taskID, [FromQuery] Int16 taskStatus, [FromQuery] string comments)
        //{
        //    return Ok(_repo.CompleteActiveTask(orderID, taskID, taskStatus, comments).Result);

        //}

        [HttpGet("CompleteActiveTask")]
        public IActionResult CompleteActiveTask([FromQuery] int orderID, [FromQuery] int taskID, [FromQuery] Int16 taskStatus, [FromQuery] string comments, [FromQuery] int userID = 0)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.CompleteActiveTask(orderID, taskID, taskStatus, comments, userID);
                if (rep != 0)
                {
                    _logger.LogInformation("CompleteActiveTask Success");
                    return Ok(true);
                }
                else
                {
                    return Ok(false);
                }
            }
            return BadRequest(new { Message = "CompleteActiveTask Failed." });

        }

        [HttpGet("GetLatestNonSystemOrderNoteInfo")]
        public IActionResult GetLatestNonSystemOrderNoteInfo([FromQuery] int orderID)
        {
            var rep = _repo.GetLatestNonSystemOrderNoteInfo(orderID);
            return Ok(rep);
        }

        [HttpGet("GetPreSubmitRTSStatus")]
        public IActionResult GetPreSubmitRTSStatus([FromQuery] int orderId)
        {
            var result = false;
            if (_repo.GetPreSubmitRTSStatus(orderId) > 0) {
                result = true;
            }
            return Ok(result);
        }

        [HttpGet("OrderExistsInSalesSupportWG")]
        public IActionResult OrderExistsInSalesSupportWG([FromQuery] int orderId)
        {
            return Ok(_repo.OrderExistsInSalesSupportWG(orderId));
        }

        [HttpGet("HasBillMissingTask")]
        public IActionResult HasBillMissingTask([FromQuery] int orderId)
        {
            return Ok(_repo.HasBillMissingTask(orderId));
        }

        [HttpGet("GetH5FolderInfo")]
        public IActionResult GetH5FolderInfo([FromQuery] int orderId)
        {
            return Ok(_repo.GetH5FolderInfo(orderId));
        }

        [HttpGet("CompleteGOMIBillTask")]
        public IActionResult CompleteGOMIBillTask([FromQuery] int orderId)
        {
            return Ok(_repo.CompleteGOMIBillTask(orderId));
        }

        [HttpGet("IsOrderCompleted")]
        public IActionResult IsOrderCompleted([FromQuery] int orderId)
        {
            return Ok(_repo.IsOrderCompleted(orderId));
        }

        [HttpGet("GetCPEUpdtInfo")]
        public IActionResult GetCPEUpdtInfo([FromQuery] int orderId)
        {
            return Ok(_repo.GetCPEUpdtInfo(orderId));
        }

        [HttpGet("GetGOMSpecificData")]
        public IActionResult GetGOMSpecificData([FromQuery] int orderId)
        {
            return Ok(_repo.GetGOMSpecificData(orderId).Result);
        }

        [HttpGet("IsACRRTS")]
        public IActionResult IsACRRTS([FromQuery] int orderId)
        {
            return Ok(_repo.IsACRRTS(orderId));
        }

        [HttpGet("CompleteACR")]
        public IActionResult CompleteACR([FromQuery] int orderId, [FromQuery]string notes)
        {
            int updateCount = _repo.CompleteACRRTSTask(orderId);
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (updateCount > 0)
            {
                _repo.InsertOrderNotes(orderId, 14, loggedInUser.UserId, notes);
            }

            return Ok(updateCount);
        }

        [HttpGet("LoadNCITask")]
        public void LoadNCITask([FromQuery] int orderId, [FromQuery] short taskId)
        {
            _repo.LoadNCITask(orderId, taskId);
        }

        [HttpGet("GetCSCSpecificData")]
        public IActionResult GetCSCSpecificData([FromQuery] int orderId)
        {
            return Ok(_repo.GetCSCSpecificData(orderId));
        }

        [HttpGet("GetMDSEventData")]
        public IActionResult GetMDSEventData([FromQuery] int orderId)
        {
            return Ok(_repo.GetMDSEventData(orderId));
        }

        [HttpGet("GetAllSrvcSiteSupport")]
        public IActionResult GetAllSrvcSiteSupport()
        {
            return Ok(_repoCommon.GetAllSrvcSiteSupport());
        }

        [HttpGet("IsOrderExistInSalesSupportWG/{id}")]
        public IActionResult IsOrderExistInSalesSupportWG([FromRoute] int id)
        {
            return Ok(_repo.IsOrderExistInSalesSupportWG(id));
        } 
    }
}