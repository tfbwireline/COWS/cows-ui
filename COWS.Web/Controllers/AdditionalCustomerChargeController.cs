﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using COWS.Data.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/AdditionalCustomerCharge")]
    [ApiController]
    public class AdditionalCustomerChargeController : ControllerBase
    {
        private readonly IAdditionalCustomerChargeRepository _repo;
        private readonly IMapper _mapper;
        private readonly ILogger<AdditionalCustomerChargeController> _logger;

        public AdditionalCustomerChargeController(IMapper mapper,
                               IAdditionalCustomerChargeRepository repo,
                               ILogger<AdditionalCustomerChargeController> logger)
        {
            _repo = repo;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet("searchFTN")]
        public IActionResult GetFTNData([FromQuery] string ftn)
        {
            return Ok(_repo.searchFTN(ftn));
        }

        [HttpGet("getAdditionalCharges")]
        public IActionResult GetAdditionalCharges([FromQuery] int orderId, [FromQuery] bool isTerm)
        {
            return Ok(_repo.getAddlCharges(orderId, isTerm));
        }

        [HttpGet("getAdditionalCharges/history")]
        public IActionResult getAdditionalChargeHistory([FromQuery]  int orderId, [FromQuery]  int chargeTypeId, [FromQuery]  bool IsTerm)
        {
            return Ok(_repo.getAddlChargeHistory(orderId, chargeTypeId, IsTerm));
        }


    

    }
}