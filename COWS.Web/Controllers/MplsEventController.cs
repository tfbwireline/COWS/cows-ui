﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Events/Mpls")]
    [ApiController]
    public class MplsEventController : ControllerBase
    {
        private readonly IMplsEventRepository _repo;
        private readonly IEventAsnToUserRepository _repoEventAsnToUser;
        private readonly IEventRuleRepository _repoEventRule;
        private readonly IEventHistoryRepository _repoEventHistory;
        private readonly IApptRepository _repoAppt;
        private readonly IEmailReqRepository _emailReqRepository;
        private readonly IEventFailActyRepository _repoFailEventActy;
        private readonly IEventSucssActyRepository _repoSucssEventActy;
        private readonly ILogger<MplsEventController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IL2PInterfaceService _l2pInterface;
        private IMemoryCache _cache;

        public MplsEventController(IMapper mapper,
                               IMplsEventRepository repo,
                               IEventAsnToUserRepository repoEventAsnToUser,
                               IEventRuleRepository repoEventRule,
                               IEventHistoryRepository repoEventHistory,
                               IApptRepository repoAppt,
                               IEmailReqRepository emailReqRepository,
                               IEventFailActyRepository repoFailEventActy,
                               IEventSucssActyRepository repoSucssEventActy,
                               ILogger<MplsEventController> logger,
                               ILoggedInUserService loggedInUser,
                               IL2PInterfaceService l2pInterface,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _repoEventAsnToUser = repoEventAsnToUser;
            _l2pInterface = l2pInterface;
            _repoEventRule = repoEventRule;
            _repoEventHistory = repoEventHistory;
            _repoAppt = repoAppt;
            _emailReqRepository = emailReqRepository;
            _repoFailEventActy = repoFailEventActy;
            _repoSucssEventActy = repoSucssEventActy;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MplsEventViewModel>> Get()
        {
            IEnumerable<MplsEventViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.MplsEventList, out list))
            {
                list = _mapper.Map<IEnumerable<MplsEventViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.EventId));

                CacheManager.Set(_cache, CacheKeys.MplsEventList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MPLS Event by Id: { id }.");

            string adid = _loggedInUser.GetLoggedInUserAdid();

            var obj = _repo.Find(s => s.EventId == id, adid);
            var t = obj.SingleOrDefault();
            if (obj != null)
            {
                var mpls = _mapper.Map<MplsEventViewModel>(obj.SingleOrDefault());
                if (mpls.EventCsgLvlId > 0)
                {
                    var userCsgLvl = _loggedInUser.GetLoggedInUserCsgLvlId();
                    if (userCsgLvl != 0 && userCsgLvl <= mpls.EventCsgLvlId)
                        mpls.AuthStatus = (int)AuthStatus.Authorized;
                    else
                    {
                        mpls.AuthStatus = (int)AuthStatus.UnAuthorized;
                        mpls.CustNme = CustomerMaskedData.customerName;
                        mpls.CustCntctNme = CustomerMaskedData.customerName;
                        mpls.CustCntctPhnNbr = CustomerMaskedData.installSitePOCPhone;
                        mpls.CustCntctCellPhnNbr = CustomerMaskedData.installSitePOCPhone;
                        mpls.CustCntctPgrNbr = CustomerMaskedData.installSitePOCPhone;
                        mpls.CustCntctPgrPinNbr = CustomerMaskedData.installSitePOCPhone;
                        mpls.CustEmailAdr = CustomerMaskedData.customerEmailAddress;
                        mpls.EventTitleTxt = mpls.EventTitleTxt.Replace(CustomerMaskedData.customerName, mpls.CustNme);
                    }
                }
                else
                    mpls.AuthStatus = (int)AuthStatus.NonSensitive;

                return Ok(mpls);
            }
            else
            {
                _logger.LogInformation($"MPLS Event by Id: { id } not found.");
                return NotFound(new { Message = $"MPLS Event Id: { id } not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] MplsEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                MplsEvent mpls = _mapper.Map<MplsEvent>(model);

                mpls.Event = new Event();
                mpls.Event.EventTypeId = (int)EventType.MPLS;
                mpls.Event.CreatDt = DateTime.Now;
                mpls.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                mpls.ModfdByUserId = _loggedInUser.GetLoggedInUserId();

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(mpls.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                mpls.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(mpls.H1);

                // Validate M5
                if (!string.IsNullOrWhiteSpace(mpls.Ftn))
                {
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(mpls.Ftn))
                    {
                        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                    }

                    mpls.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(string.Empty, string.Empty, mpls.Ftn);
                }

                if (model.MplsEventActyTypeIds != null && model.MplsEventActyTypeIds.Count > 0)
                {
                    List<MplsEventActyType> actTypes = new List<MplsEventActyType>();
                    model.MplsEventActyTypeIds.ForEach(id =>
                    {
                        actTypes.Add(new MplsEventActyType { MplsActyTypeId = (byte)id });
                    });
                    mpls.Event.MplsEventActyType = actTypes;
                }

                if (model.MplsEventVasTypeIds != null && model.MplsEventVasTypeIds.Count > 0)
                {
                    List<MplsEventVasType> vasTypes = new List<MplsEventVasType>();
                    model.MplsEventVasTypeIds.ForEach(id =>
                    {
                        vasTypes.Add(new MplsEventVasType { VasTypeId = (byte)id });
                    });
                    //mpls.MplsEventVasType = vasTypes;
                    mpls.Event.MplsEventVasType = vasTypes;
                }

                LkEventRule eventRule = _repoEventRule
                    .GetEventRule(mpls.EventStusId, mpls.WrkflwStusId.Value,
                        _loggedInUser.GetLoggedInUserId(), (int)EventType.MPLS);

                if (eventRule != null)
                {
                    mpls.EventStusId = eventRule.EndEventStusId;
                    var me = _repo.Create(mpls);

                    if (me != null)
                    {
                        // Create EventAsnToUser; Save AssignUsers
                        if (model.Activators != null && model.Activators.Count > 0)
                        {
                            _repoEventAsnToUser.Create(model.Activators
                            .Select(a => new EventAsnToUser
                            {
                                EventId = me.EventId,
                                AsnToUserId = a,
                                CreatDt = DateTime.Now,
                                RecStusId = 0,
                                RoleId = 0
                            }).ToList());
                        }
                        List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                        assignUsers = _repoEventAsnToUser.Find(a => a.EventId == me.EventId).ToList();

                        // Do things needed based on eventRule like event history and email sender
                        var json = new JsonSerializerSettings()
                        {
                            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                            Formatting = Formatting.Indented
                        };

                        // Create Event History; Reviewer and Activator is not needed for new MPLS Event
                        EventHist eh = new EventHist();
                        eh.EventId = me.EventId;
                        eh.ActnId = eventRule.ActnId;
                        eh.EventStrtTmst = me.StrtTmst;
                        eh.EventEndTmst = me.EndTmst;
                        eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        _repoEventHistory.CreateEventHistory(eh);

                        // Save Calendar Entry and Email Sender
                        EventWorkflow esw = SetEventWorkFlow(me);
                        esw.AssignUser = assignUsers;
                        esw.EventRule = eventRule;

                        if (_repoAppt.CalendarEntry(esw))
                            _logger.LogInformation($"MPLS Event Calender Entry success for EventID. { JsonConvert.SerializeObject(me.EventId, json) } ");

                        if (_emailReqRepository.SendMail(esw))
                            _logger.LogInformation($"MPLS Event Email Entry success for EventID. { JsonConvert.SerializeObject(me.EventId, json) } ");

                        _logger.LogInformation($"MPLS Event Created. { JsonConvert.SerializeObject(me, json) } ");
                        return Created($"api/Events/Mpls/{ me.EventId }", me);
                    }
                }
            }

            return BadRequest(new { Message = "MPLS Event Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] MplsEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                MplsEvent mpls = _mapper.Map<MplsEvent>(model);
                mpls.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                mpls.ModfdDt = DateTime.Now;

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(mpls.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                mpls.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(mpls.H1);

                // Validate M5
                if (!string.IsNullOrWhiteSpace(mpls.Ftn))
                {
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(mpls.Ftn))
                    {
                        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                    }

                    mpls.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(string.Empty, string.Empty, mpls.Ftn);
                }

                if (model.MplsEventActyTypeIds != null && model.MplsEventActyTypeIds.Count > 0)
                {
                    List<MplsEventActyType> actTypes = new List<MplsEventActyType>();
                    model.MplsEventActyTypeIds.ForEach(item =>
                    {
                        actTypes.Add(new MplsEventActyType { MplsActyTypeId = (byte)item });
                    });
                    mpls.Event.MplsEventActyType = actTypes;
                }

                if (model.MplsEventVasTypeIds != null && model.MplsEventVasTypeIds.Count > 0)
                {
                    List<MplsEventVasType> vasTypes = new List<MplsEventVasType>();
                    model.MplsEventVasTypeIds.ForEach(item =>
                    {
                        vasTypes.Add(new MplsEventVasType { VasTypeId = (byte)item });
                    });
                    //mpls.MplsEventVasType = vasTypes;
                    mpls.Event.MplsEventVasType = vasTypes;
                }

                LkEventRule eventRule = _repoEventRule
                    .GetEventRule(mpls.EventStusId, mpls.WrkflwStusId.Value,
                        _loggedInUser.GetLoggedInUserId(), (int)EventType.MPLS);

                if (eventRule != null)
                {
                    mpls.EventStusId = eventRule.EndEventStusId;
                    _repo.Update(id, mpls);

                    // Update EventAsnToUser
                    _repoEventAsnToUser.Update(id, model.Activators
                        .Select(a => new EventAsnToUser
                        {
                            EventId = id,
                            AsnToUserId = a,
                            CreatDt = DateTime.Now,
                            RecStusId = 0,
                            RoleId = 0
                        }).ToList());

                    // Do things needed based on eventRule like calendar entry and email sender
                    MplsEvent me = _repo.GetById(id);
                    List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                    assignUsers = _repoEventAsnToUser.Find(a => a.EventId == me.EventId).ToList();
                    var json = new JsonSerializerSettings()
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                        Formatting = Formatting.Indented
                    };

                    // Event History, Reviewer and Activator Task
                    EventHist eh = new EventHist();
                    eh.EventId = me.EventId;
                    eh.ActnId = eventRule.ActnId;
                    eh.EventStrtTmst = me.StrtTmst;
                    eh.EventEndTmst = me.EndTmst;
                    eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                    if (model.ReviewerUserId > 0)
                    {
                        eh.ModfdByUserId = model.ReviewerUserId;
                        eh.CmntTxt = model.ReviewerComments;
                    }
                    else if (model.ActivatorUserId > 0)
                    {
                        eh.ModfdByUserId = model.ActivatorUserId;
                        eh.CmntTxt = model.ActivatorComments;
                        eh.PreCfgCmpltCd = (string.IsNullOrWhiteSpace(model.PreCfgConfgCode)
                            || model.PreCfgConfgCode == "0") ? "N" : "Y";
                    }
                    else if ((me.WrkflwStusId != (byte)WorkflowStatus.Visible
                                || me.WrkflwStusId != (byte)WorkflowStatus.Retract)
                                        && me.EventStusId != (byte)EventStatus.Visible)
                    {
                        eh.CmntTxt = model.DesCmntTxt;
                    }

                    int evntHistId = _repoEventHistory.CreateEventHistory(eh);
                    if (model.ActivatorUserId > 0)
                    {
                        var Fail = this._repoFailEventActy.GetByEventId(me.EventId);
                        if (Fail.Count() > 0)
                        {
                            _repoFailEventActy.DeleteByEventId(me.EventId);
                        }

                        if (model.EventFailActyIds != null && model.EventFailActyIds.Count() > 0)
                        {
                            foreach (var es in model.EventFailActyIds)
                            {
                                EventFailActy esa = new EventFailActy();
                                esa.EventHistId = evntHistId;
                                esa.FailActyId = (short)es;
                                esa.RecStusId = (byte)ERecStatus.Active;
                                esa.CreatDt = DateTime.Now;
                                _repoFailEventActy.CreateActy(esa);
                            }
                        }

                        var Sucss = _repoSucssEventActy.GetByEventId(me.EventId);
                        if (Sucss.Count() > 0)
                        {
                            _repoSucssEventActy.DeleteByEventId(me.EventId);
                        }

                        if (model.EventSucssActyIds != null && model.EventSucssActyIds.Count() > 0)
                        {
                            foreach (var es in model.EventSucssActyIds)
                            {
                                EventSucssActy esa = new EventSucssActy();
                                esa.EventHistId = evntHistId;
                                esa.SucssActyId = (short)es;
                                esa.RecStusId = (byte)ERecStatus.Active;
                                esa.CreatDt = DateTime.Now;
                                _repoSucssEventActy.CreateActy(esa);
                            }
                        }
                    }

                    // Save Calendar Entry and Email Sender
                    EventWorkflow esw = SetEventWorkFlow(me);
                    esw.AssignUser = assignUsers;
                    esw.EventRule = eventRule;
                    esw.ReviewerId = model.ReviewerUserId;

                    if (_repoAppt.CalendarEntry(esw))
                        _logger.LogInformation($"MPLS Event Calender Entry success for EventID. { JsonConvert.SerializeObject(me.EventId, json) } ");

                    if (_emailReqRepository.SendMail(esw))
                        _logger.LogInformation($"MPLS Event Email Entry success for EventID. { JsonConvert.SerializeObject(me.EventId, json) } ");

                    _logger.LogInformation($"MPLS Event Updated. { JsonConvert.SerializeObject(me, json) } ");
                    return Created($"api/Events/Mpls/{ id }", me);
                }
            }

            return BadRequest(new { Message = "Mpls Event Could Not Be Updated." });
        }

        private EventWorkflow SetEventWorkFlow(MplsEvent me)
        {
            EventWorkflow esw = new EventWorkflow();
            esw.EventId = me.EventId;
            esw.Comments = me.DesCmntTxt;
            esw.EventStatusId = me.EventStusId;
            esw.WorkflowId = (int)me.WrkflwStusId;
            esw.EventTitle = me.EventTitleTxt;
            esw.EventTypeId = (int)EventType.MPLS;
            esw.EventType = "MPLS";
            esw.SOWSEventID = me.SowsEventId.HasValue ? me.SowsEventId.Value.ToString() : string.Empty;
            esw.RequestorId = me.ReqorUserId;
            esw.UserId = _loggedInUser.GetLoggedInUserId();
            esw.ConferenceBridgeNbr = me.CnfrcBrdgNbr;
            esw.ConferenceBridgePin = me.CnfrcPinNbr;
            esw.StartTime = me.StrtTmst;
            esw.EndTime = me.EndTmst;
            esw.MplsEventTypeId = me.MplsEventTypeId;

            return esw;
        }
    }
}