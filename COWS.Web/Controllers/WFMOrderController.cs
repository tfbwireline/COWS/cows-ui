﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/WFMOrder")]
    [ApiController]
    public class WFMOrderController : ControllerBase
    {
        private readonly IWFMRepository _repo;
        private readonly ILogger<WFMOrderController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public WFMOrderController(IMapper mapper,
                               IWFMRepository repo,
                               ILogger<WFMOrderController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("GetWFMRole")]
        public IActionResult GetWFMRole([FromQuery] int userId, [FromQuery] string profile)
        {
            var result = _repo.GetWFMRole(userId, profile);
            return Ok(result);
        }

        [HttpGet("GetWFMprofile")]
        public IActionResult GetWFMprofile([FromQuery] int userId, [FromQuery] string profile)
        {
            var result = _repo.GetWFMprofile(userId, profile);
            return Ok(result);
        }

        [HttpGet("GetNCIUser")]
        public IActionResult GetNCIUser([FromQuery] int userId)
        {
            var result = _repo.GetNCIUser(userId);
            return Ok(result);
        }

        [HttpGet("UserValid")]
        public IActionResult UserValid([FromQuery] string userId, [FromQuery] string prodType, [FromQuery] string pltfrmType, [FromQuery] string subStatus)
        {
            var result = _repo.UserValid(userId, prodType, pltfrmType, subStatus);
            return Ok(result);
        }

        [HttpGet("Select")]
        public IActionResult Select([FromQuery] string sortExpression, [FromQuery] string searchCriteria, [FromQuery] byte view, [FromQuery] string profile)
        {
            var result = _repo.Select(sortExpression, searchCriteria, view, profile);
            return Ok(result);
        }

        [HttpGet("GetWFMData")]
        public IActionResult GetWFMData([FromQuery] string sortExpression, [FromQuery] string searchCriteria, [FromQuery] byte view, [FromQuery] string profile)
        {
            var result = _repo.GetWFMData(sortExpression, searchCriteria, view, profile);
            return Ok(result);
        }

        [HttpGet("MakeNCIAssignments")]
        public void MakeNCIAssignments([FromQuery] WFMOrdersModel wfmOrder)
        {
            _repo.MakeNCIAssignments(wfmOrder);
        }

        [HttpGet("MakeNCIUnAssignments")]
        public void MakeNCIUnAssignments([FromQuery] WFMOrdersModel wfmOrder)
        {
            _repo.MakeNCIUnAssignments(wfmOrder);
        }

        [HttpGet("MakeGOMAssignments")]
        public void MakeGOMAssignments([FromQuery] WFMOrdersModel wfmOrder)
        {
            _repo.MakeGOMAssignments(wfmOrder);
        }

        [HttpGet("MakeGOMUnAssignments")]
        public void MakeGOMUnAssignments([FromQuery] WFMOrdersModel wfmOrder)
        {
            _repo.MakeGOMUnAssignments(wfmOrder);
        }

        [HttpGet("InsertOrderNotes")]
        public IActionResult InsertOrderNotes([FromQuery] int orderID, [FromQuery] int noteTypeID, [FromQuery] int userID, [FromQuery] string notes)
        {
            var result = _repo.InsertOrderNotes(orderID, noteTypeID, userID, notes);
            if (result != 0)
            {
                return Ok(true);
            }
            else
            {
                return Ok(false);
            }
        }

        [HttpGet("GetXNCIOrderWeightageNumber")]
        public IActionResult GetXNCIOrderWeightageNumber([FromQuery] WFMOrdersModel wfmOrder)
        {
            var result = _repo.GetXNCIOrderWeightageNumber(wfmOrder);
            return Ok(result);
        }

        [HttpPost("AssignClick")]
        public IActionResult AssignClick([FromBody] WFMOrderViewModel model)
        {
            foreach (var row in model.WFMOrders)
            {
                string _PROD_TYPE = row.PROD_TYPE;
                string _PLTFRM_TYPE = row.PLTFM_TYPE;
                if (row.PLTFM_TYPE == "")
                {
                    _PLTFRM_TYPE = null;
                }
                string _SUB_STATUS = row.SUB_STATUS;
                bool _Valid = false;

                _Valid = _repo.UserValid(model.UserToAssign, _PROD_TYPE, _PLTFRM_TYPE, _SUB_STATUS);

                if (_Valid)
                {
                    WFMOrdersModel _WFM = new WFMOrdersModel
                    {
                        NEWASSIGNEE = model.UserToAssign,
                        ASSIGNED_BY = Convert.ToString(model.UserId),
                        ORDR_ID = Convert.ToInt32(row.ORDR_ID),
                        PROD_TYPE = _PROD_TYPE,
                        SUB_STATUS = _SUB_STATUS,
                        FTN = row.FTN,
                        REGION = row.REGION
                    };

                    if (row.ASSIGNED_USER != "")
                    {
                        _WFM.ASSIGNED_USER = row.ASSIGNED_USER;
                    }
                    else
                    {
                        _WFM.ASSIGNED_USER = string.Empty;
                    }

                    if (model.Profile == "NCI")
                    {
                        _repo.MakeNCIAssignments(_WFM);
                    }
                    else
                    {
                        _repo.MakeGOMAssignments(_WFM);
                    }
                    
                    string txtNotes = "Order manually assigned from " + _WFM.ASSIGNED_USER + " to " + _WFM.NEWASSIGNEE;
                    _repo.InsertOrderNotes(_WFM.ORDR_ID, 10, model.UserId, txtNotes.Trim());
                }
                else
                {
                    return BadRequest(new
                    {
                        Message = "User not authorized for order. " + row.ORDR_ID +
                                " Update user profile or select different user for this order. "
                    });
                }

            }
            var data = _repo.GetWFMData("ASSIGNED_USER asc", model.SearchCriteria, 1, model.Profile);
            return Ok(data);
        }

        [HttpPost("UnAssignClick")]
        public IActionResult UnAssignClick([FromBody] WFMOrderViewModel model)
        {
            foreach (var row in model.WFMOrders)
            {
                WFMOrdersModel _WFM = new WFMOrdersModel
                {
                    NEWASSIGNEE = string.Empty,
                    ASSIGNED_BY = Convert.ToString(model.UserId),
                    ORDR_ID = Convert.ToInt32(row.ORDR_ID),
                    PROD_TYPE = row.PROD_TYPE,
                    SUB_STATUS = row.SUB_STATUS,
                    FTN = row.FTN,
                    REGION = row.REGION
                };

                if (row.ASSIGNED_USER != "")
                {
                    _WFM.ASSIGNED_USER = row.ASSIGNED_USER;
                }
                else
                {
                    _WFM.ASSIGNED_USER = string.Empty;
                }

                if (model.Profile == "NCI")
                {
                    _repo.MakeNCIUnAssignments(_WFM);
                }
                else
                {
                    _repo.MakeGOMUnAssignments(_WFM);
                }
                string txtNotes = "Order manually Unassigned from " + _WFM.ASSIGNED_USER;
                _repo.InsertOrderNotes(_WFM.ORDR_ID, 10, model.UserId, txtNotes.Trim());
            }
            var data = _repo.GetWFMData("ASSIGNED_USER asc", model.SearchCriteria, 1, model.Profile);
            return Ok(data);
        }

        [HttpPost("TransferClick")]
        public IActionResult TransferClick([FromBody] WFMOrderViewModel model)
        {
            foreach (var row in model.WFMOrders)
            {
                if (row.ASSIGNED_USER != "")
                {
                    if (model.UserToAssign != row.ASSIGNED_USER)
                    {
                        WFMOrdersModel _WFM = new WFMOrdersModel
                        {
                            NEWASSIGNEE = string.Empty,
                            ASSIGNED_USER = row.ASSIGNED_USER,
                            ASSIGNED_BY = Convert.ToString(model.UserId),
                            ORDR_ID = Convert.ToInt32(row.ORDR_ID),
                            PROD_TYPE = row.PROD_TYPE,
                            REGION = row.REGION,
                            FTN = row.FTN
                        };
                        if (model.NCIAssigner)
                        {
                            _repo.MakeNCIUnAssignments(_WFM);

                            _WFM.NEWASSIGNEE = model.UserToAssign;
                            _repo.MakeNCIAssignments(_WFM);
                            string txtNotes = "Order manually transferred from " + _WFM.ASSIGNED_USER + " to " + _WFM.NEWASSIGNEE;
                            _repo.InsertOrderNotes(_WFM.ORDR_ID, 10, model.UserId, txtNotes.Trim());
                        }
                        else
                        {
                            return BadRequest(new
                            {
                                Message = "User not assigned to order. " + row.ORDR_ID + " Use the Assign instead of Transfer button. "
                            });
                        }
                    }
                    else
                    {
                        return BadRequest(new
                        {
                            Message = "Self transfers are not allowed. ie: Order is assigned to you. " + "Order: " + row.FTN
                        });
                    }
                }
                else
                {
                    return BadRequest(new
                    {
                        Message = "You are unauthorized to Transfer orders. You may only assign orders to yourself."
                    });
                }
            }
           
            var data = _repo.GetWFMData("ASSIGNED_USER asc", model.SearchCriteria, 0, model.Profile);
            return Ok(data);
        }
    }
}