﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/UcaasProdTypes")]
    [ApiController]
    public class UcaasProdTypeController : ControllerBase
    {
        private readonly IUcaasProdTypeRepository _repo;
        private readonly ILogger<UcaasProdTypeController> _logger;
        private readonly IMapper _mapper;

        public UcaasProdTypeController(IMapper mapper,
                               IUcaasProdTypeRepository repo,
                               ILogger<UcaasProdTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UcaasProdTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<UcaasProdTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.UcaaSProdTypeId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search UCaaS Prod Type by Id: { id }.");

            var obj = _repo.Find(s => s.UcaaSProdTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<UcaasProdTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"UCaaS Prod Type by Id: { id } not found.");
                return NotFound(new { Message = $"UCaaS Prod Type Id: { id } not found." });
            }
        }
    }
}