﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Collections.Generic;
using COWS.Entities.QueryModels;
using System.Linq;
using Newtonsoft.Json;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/RedesignCustomerBypass")]
    [ApiController]
    public class RedesignCustBypassController : ControllerBase
    {
        private readonly IRedesignCustBypassRepository _repo;
        private readonly ICommonRepository _common;
        private readonly ILogger<RedesignCustBypassController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public RedesignCustBypassController(IMapper mapper,
                               IRedesignCustBypassRepository repo,
                               ICommonRepository common,
                               ILogger<RedesignCustBypassController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _common = common;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("GetCustomersBypass")]
        public IActionResult GetCustomersBypass()
        {
            DataTable dt = new DataTable();
            dt = _repo.GetCustomersBypass();
            if (dt != null)
            {
                return Ok(dt);
            }
            return BadRequest(new { Message = "No data for Custome Bypass" });
        }

        [HttpGet("GetCustomersBypassByID/{id}")]
        public IActionResult GetCustomersBypassByID([FromRoute] int Id)
        {
            var userCsgLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            return Ok(_repo.GetCustomersBypassByID(Id, userCsgLevel));
        }

        [HttpGet("CheckRedesignCustomerBypassCDIfExist")]
        public IActionResult CheckRedesignCustomerBypassCDIfExist([FromQuery] string H1, [FromQuery] int ID)
        {
            var isExist = _repo.CheckRedesignCustomerBypassCDIfExist(H1, ID);
            return Ok(isExist);
        } 

        [HttpPost("CreateCustomerByPass")]
        public IActionResult CreateCustomerByPass([FromBody] RedesignCustBypassViewModel model)
        {
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            RedsgnCustBypass custBypass = _mapper.Map<RedsgnCustBypass>(model);
            return Ok(_repo.CreateCustomerByPass(custBypass, loggedInUser.UserId));
        }

        [HttpPost]
        public ActionResult Post([FromBody] RedesignCustBypassViewModel model)
        {
            if (ModelState.IsValid)
            {
                RedsgnCustBypass cust = _mapper.Map<RedsgnCustBypass>(model);

                cust.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                cust.CreatDt = DateTime.Now;
                cust.ModfdByUserId = null;
                cust.ModfdDt = null;
                cust.CsgLvlId = 0;

                // Check if it exist
                var isExist = _repo.CheckRedesignCustomerBypassCDIfExist(cust.H1Cd, cust.Id);
                if (isExist)
                {
                    return BadRequest(new { Message = "H1 already exists." });
                }

                var csgLevelList = _common.GetM5CSGLvl(cust.H1Cd, string.Empty, string.Empty).Distinct();
                if (csgLevelList != null && csgLevelList.Count() > 0)
                {
                    var list = csgLevelList.Select(i => i.CsgLevelID).ToList();
                    if (list != null && list.Count() > 0)
                    {
                        cust.CsgLvlId = list.FirstOrDefault();
                    }
                }

                var r = _repo.Create(cust);

                if (r != null)
                {
                    _logger.LogInformation($"Redesign Customer Bypass Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/RedesignCustomerBypass/{ r.Id }", r);
                }
            }

            return BadRequest(new { Message = "Redesign Customer Bypass Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] RedesignCustBypassViewModel model)
        {
            if (ModelState.IsValid)
            {
                RedsgnCustBypass cust = _mapper.Map<RedsgnCustBypass>(model);

                cust.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                cust.ModfdDt = DateTime.Now;
                cust.CsgLvlId = 0;

                // Check if it exist
                var isExist = _repo.CheckRedesignCustomerBypassCDIfExist(cust.H1Cd, cust.Id);
                if (isExist)
                {
                    return BadRequest(new { Message = "H1 already exists." });
                }

                var csgLevelList = _common.GetM5CSGLvl(cust.H1Cd, string.Empty, string.Empty).Distinct();
                if (csgLevelList != null && csgLevelList.Count() > 0)
                {
                    var list = csgLevelList.Select(i => i.CsgLevelID).ToList();
                    if (list != null && list.Count() > 0)
                    {
                        cust.CsgLvlId = list.FirstOrDefault();
                    }
                }

                _repo.Update(id, cust);

                _logger.LogInformation($"Redesign Customer Bypass Updated. { JsonConvert.SerializeObject(model).ToString() } ");
                return Created($"api/RedesignCustomerBypass/{ id }", cust);
            }

            return BadRequest(new { Message = "Redesign Customer Bypass Could Not Be Updated." });
        }
    }
}
