﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VendorTemplate")]
    [ApiController]
    public class VendorTemplateController : ControllerBase
    {
        private readonly IVendorTemplateRepository _repo;
        private readonly ILogger<VendorTemplateController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public VendorTemplateController(IMapper mapper,
                       IVendorTemplateRepository repo,
                       ILogger<VendorTemplateController> logger,
                       ILoggedInUserService loggedInUser,
                       IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VendorTemplateViewModel>> Get()
        {
            IEnumerable<VendorTemplateViewModel> list = 
                _mapper.Map<IEnumerable<VendorTemplateViewModel>>(_repo
                        .GetAll().Where(s => s.RecStusId == (byte)ERecStatus.Active));

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Template by Id: { id }.");

            var obj = _repo.Find(s => s.VndrTmpltId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<VendorTemplateViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Vendor Template by Id: { id } not found.");
                return NotFound(new { Message = $"Template Folder Id: { id } not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] VendorTemplateViewModel model)
        {
            _logger.LogInformation($"Create Vendor Template File: { model.FileNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrTmplt>(model);
                obj.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                obj.CreatDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
                obj.VndrTmpltId = 0; // In create, make this 0. Temp Id is used on the UI only.
                obj.FileCntnt = Convert.FromBase64String(model.Base64string);

                var rep = _repo.Create(obj);
                if (rep != null)
                {
                    _logger.LogInformation($"Vendor Template Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/VendorTemplate/{ rep.VndrTmpltId }", model);
                }
            }

            return BadRequest(new { Message = "Vendor Template Could Not Be Created." });
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deleting Vendor Template by Id: { id }.");

            _repo.Delete(id);

            _logger.LogInformation($"Vendor Template by Id: { id } Deleted.");
        }

        [HttpGet("{id}/download")]
        public IActionResult Download([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Folder Template by Id: { id }.");

            var obj = _repo.Find(s => s.VndrTmpltId == id).SingleOrDefault();
            if (obj != null)
            {
                try
                {
                    return new FileContentResult(obj.FileCntnt, "application/octet")
                    {
                        FileDownloadName = obj.FileNme
                    };
                }
                catch (Exception)
                {
                    return NotFound(new { Message = $"Vendor Folder Template Id: { id } not found." });
                }
            }
            else
            {
                _logger.LogInformation($"Vendor Folder Template by Id: { id } not found.");
                return NotFound(new { Message = $"Vendor Folder Template Id: { id } not found." });
            }
        }
    }
}
