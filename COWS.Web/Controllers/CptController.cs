﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT")]
    [ApiController]
    public class CptController : ControllerBase
    {
        private readonly ICptRepository _repo;
        private readonly ICptRecLockRepository _repoLock;
        private readonly IL2PInterfaceService _l2pInterface;
        private readonly ICommonRepository _commonRepo;
        private readonly IOdieReqRepository _odieReqRepo;
        private readonly IOdieRspnRepository _odieResRepo;
        private readonly IEmailReqRepository _emailReqRepo;
        private readonly IUserRepository _usrRepo;
        private readonly IContactDetailRepository _contctRepo;
        private readonly ILogger<CptController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IConfiguration _config;

        public CptController(IMapper mapper,
                               ICptRepository repo,
                               ICptRecLockRepository repoLock,
                               IL2PInterfaceService l2pInterface,
                               ICommonRepository commonRepo,
                               IOdieReqRepository odieReqRepo,
                               IOdieRspnRepository odieResRepo,
                               IEmailReqRepository emailReqRepo,
                               IUserRepository usrRepo,
                               IContactDetailRepository contctRepo,
                               ILogger<CptController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache,
                               IConfiguration config)
        {
            _mapper = mapper;
            _repo = repo;
            _repoLock = repoLock;
            _l2pInterface = l2pInterface;
            _commonRepo = commonRepo;
            _odieReqRepo = odieReqRepo;
            _odieResRepo = odieResRepo;
            _emailReqRepo = emailReqRepo;
            _usrRepo = usrRepo;
            _contctRepo = contctRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _config = config;
        }

        public short gk = 212;
        public short manager = 211;
        public short mss = 139;
        public short nte = 130;
        public short pm = 132;
        public short crm = 213;

        [HttpGet("SearchByParams")]
        public ActionResult<IEnumerable<GetAllCptViewModel>> SearchByParams([FromQuery] string cptCustNbr,
            [FromQuery] string companyName, [FromQuery] string custShortName)
        {
            string adid = _loggedInUser.GetLoggedInUserAdid();
            IEnumerable<GetAllCptViewModel> list = _mapper.Map<IEnumerable<GetAllCptViewModel>>(_repo
                                                                .GetAllCpt(_loggedInUser.GetLoggedInUserCsgLvlId(), 1, adid));

            if (list != null && list.Count() > 0)
            {
                if (!string.IsNullOrWhiteSpace(cptCustNbr))
                {
                    list = list.Where(i => i.CptCustNbr.ToLower().Trim().Contains(cptCustNbr.ToLower().Trim()));
                }

                if (!string.IsNullOrWhiteSpace(companyName))
                {
                    list = list.Where(i => !string.IsNullOrWhiteSpace(i.CompanyName)
                            && i.CompanyName.ToLower().Trim().Contains(companyName.ToLower().Trim()));
                }

                if (!string.IsNullOrWhiteSpace(custShortName))
                {
                    list = list.Where(i => !string.IsNullOrWhiteSpace(i.CustShortName)
                            && i.CustShortName.ToLower().Trim().Contains(custShortName.ToLower().Trim()));
                }

                return Ok(list.OrderByDescending(i => i.CreatedDateTime));
            }

            return Ok(list);
        }

        [HttpGet("GetReturnedToSde")]
        public ActionResult<IEnumerable<GetAllCptViewModel>> GetReturnedToSde()
        {
            IEnumerable<GetAllCptViewModel> list =
                _mapper.Map<IEnumerable<GetAllCptViewModel>>(_repo
                                                                .GetAllCpt(_loggedInUser.GetLoggedInUserCsgLvlId(), 0)
                                                                .Where(i => i.ReturnedToSDE == true)
                                                                .OrderByDescending(i => i.CreatedDateTime));

            return Ok(list);
        }

        [HttpGet("GetCptAssignmentStats")]
        public ActionResult<IEnumerable<CptAssignmentViewModel>> GetCptAssignmentStats()
        {
            IEnumerable<CptAssignmentViewModel> list =
                _mapper.Map<IEnumerable<CptAssignmentViewModel>>(_repo
                                                                .GetCptAssignmentStats()
                                                                .OrderByDescending(i => i.CreatedDate));

            return Ok(list);
        }

        [HttpGet("GetCptCancelStat")]
        public ActionResult<IEnumerable<CptAssignmentViewModel>> GetCptCancelStat()
        {
            IEnumerable<CptAssignmentViewModel> list =
                _mapper.Map<IEnumerable<CptAssignmentViewModel>>(_repo
                                                                .GetCptCancelStats(_loggedInUser.GetLoggedInUserCsgLvlId())
                                                                .OrderByDescending(i => i.CreatedDate));

            return Ok(list);
        }

        //[HttpGet("GetCustomerH1Data")]
        //public IActionResult GetCustomerH1Data()
        //{
        //    List<CptCustomerH1ViewModel> custList;
        //    if (!_cache.TryGetValue(CacheKeys.CptCustomerH1List, out custList))
        //    {
        //        DataTable dt = new DataTable();
        //        dt = _repo.GetCustomerH1Data(string.Empty);

        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            custList = new List<CptCustomerH1ViewModel>();
        //            foreach (DataRow dr in dt.Rows)
        //            {
        //                CptCustomerH1ViewModel cust = new CptCustomerH1ViewModel();
        //                cust.CustomerName = dr["CUST_NME"].ToString();
        //                cust.H1 = dr["H1_ID"].ToString();
        //                if (custList.SingleOrDefault(i => i.CustomerName.Equals(cust.CustomerName)
        //                    && i.H1.Equals(cust.H1)) == null)
        //                {
        //                    custList.Add(cust);
        //                }
        //            }
        //        }

        //        CacheManager.Set(_cache, CacheKeys.CptCustomerH1List, custList);
        //    }

        //    return Ok(custList);
        //}

        [HttpGet("GetCustomerH1Data")]
        public IActionResult GetCustomerH1Data([FromQuery] string h1, [FromQuery] string custName)
        {
            if (string.IsNullOrWhiteSpace(h1))
            {
                List<CptCustomerH1ViewModel> custList;
                if (!_cache.TryGetValue(CacheKeys.CptCustomerH1List, out custList))
                {
                    DataTable dt = new DataTable();
                    dt = _repo.GetCustomerH1Data(string.Empty);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        custList = new List<CptCustomerH1ViewModel>();
                        foreach (DataRow dr in dt.Rows)
                        {
                            CptCustomerH1ViewModel cust = new CptCustomerH1ViewModel();
                            cust.CustomerName = dr["CUST_NME"].ToString();
                            cust.H1 = dr["H1_ID"].ToString();
                            custList.Add(cust);
                        }
                    }

                    CacheManager.Set(_cache, CacheKeys.CptCustomerH1List, custList);
                }

                return Ok(custList);
            }
            else
            {
                CptCustomerH1ViewModel cust = new CptCustomerH1ViewModel();

                DataTable dt = new DataTable();
                dt = _repo.GetCustomerH1Data(h1);

                if (dt != null && dt.Rows.Count > 0)
                {
                    string nddLink = string.Empty;
                    string seEmail = string.Empty;
                    string amEmail = string.Empty;
                    string ipmEmail = string.Empty;

                    foreach (DataRow dr in dt.Rows)
                    {
                        string tempH1 = dr["H1_ID"].ToString();
                        string tempCustName = dr["CUST_NME"].ToString();

                        // H1
                        cust.H1 = h1;

                        // Company Name
                        cust.CustomerName = dr["CUST_NME"].ToString();

                        // NDD Link
                        if (!string.IsNullOrWhiteSpace(dr["DSGN_LINK_NME"].ToString()))
                        {
                            nddLink += dr["DSGN_LINK_NME"].ToString() + ",";
                        }

                        // Emails
                        string role = dr["ACCT_ROLE_NME"].ToString();
                        if (!string.IsNullOrWhiteSpace(role) && role.ToLower().Equals("solutions engineer"))
                        {
                            seEmail += dr["EMAIL_ADR"].ToString() + ",";
                        }
                        else if (!string.IsNullOrWhiteSpace(role) && role.ToLower().Equals("account manager"))
                        {
                            amEmail += dr["EMAIL_ADR"].ToString() + ",";
                        }
                        else if (!string.IsNullOrWhiteSpace(role) && role.ToLower().Equals("ipm"))
                        {
                            ipmEmail += dr["EMAIL_ADR"].ToString() + ",";
                        }
                    }

                    cust.DesignLinkName = nddLink.TrimEnd(',');
                    cust.SEEmail = seEmail.TrimEnd(',');
                    cust.AMEmail = amEmail.TrimEnd(',');
                    cust.IPMEmail = ipmEmail.TrimEnd(',');
                }

                return Ok(cust);
            }
        }

        [HttpGet("GetAssignmentByProfileId/{usrPrfId}")]
        public ActionResult<IEnumerable<CptUserViewModel>> GetAssignmentByProfileId([FromRoute] short usrPrfId)
        {
            IEnumerable<CptUserViewModel> list =
                _mapper.Map<IEnumerable<CptUserViewModel>>(_repo
                                                                .GetAssignmentByProfileId(usrPrfId)
                                                                .OrderBy(i => i.FullName));

            return Ok(list);
        }

        [HttpGet("GetAutoAssignNTE")]
        public ActionResult GetAutoAssignNTE()
        {
            var userId = SetAutoAssign(nte, 3);
            return Ok(userId);
        }

        #region CPT Form

        [HttpGet]
        public ActionResult<IEnumerable<GetAllCptViewModel>> Get()
        {
            IEnumerable<GetAllCptViewModel> list;
            //if (!_cache.TryGetValue(CacheKeys.CptList, out list))
            //{
                list = _mapper.Map<IEnumerable<GetAllCptViewModel>>(_repo
                                                                .GetAllCpt(_loggedInUser.GetLoggedInUserCsgLvlId(), 0)
                                                                .OrderByDescending(i => i.CreatedDateTime));

            //    CacheManager.Set(_cache, CacheKeys.CptList, list);
            //}

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT by Id: { id }.");

            string adid = _loggedInUser.GetLoggedInUserAdid();
            var cpt = _repo.GetById(id, adid);
            if (cpt != null)
            {
                cpt = _repo.GetSecuredData(cpt);
                var cptVm = _mapper.Map<CptViewModel>(cpt);
                if (cptVm.RelatedInfos != null && cptVm.RelatedInfos.Count() > 0)
                {
                    foreach (CptRelatedInfoViewModel item in cptVm.RelatedInfos)
                    {
                        item.CptPrimWan = _mapper.Map<IEnumerable<CptPrimWanViewModel>>(cpt.CptPrimWan
                            .Where(i => i.CptPlnSrvcTierId == item.CptPlnSrvcTierId)).ToList();
                        item.SecTrnsprtTypes = cpt.CptScndyTprt
                            .Where(i => i.CptPlnSrvcTierId == item.CptPlnSrvcTierId).Select(i => (int)i.CptPrimScndyTprtId).ToList();
                    }
                }
                if (cptVm.CptUserAsmt != null && cptVm.CptUserAsmt.Count() > 0)
                {
                    foreach (CptUserAsmtViewModel item in cptVm.CptUserAsmt)
                    {
                        // Gatekeeper
                        if (item.UsrPrfId.GetValueOrDefault() == gk || item.RoleId == 152)
                            cptVm.Gatekeeper = item.CptUserId;

                        // Manager
                        if (item.UsrPrfId.GetValueOrDefault() == manager || item.RoleId == 151)
                            cptVm.Manager = item.CptUserId;

                        // MSS
                        if (item.UsrPrfId.GetValueOrDefault() == mss || item.RoleId == 82)
                            cptVm.Mss = item.CptUserId;

                        // NTE
                        if (item.UsrPrfId.GetValueOrDefault() == nte || item.RoleId == 23)
                            cptVm.Nte = item.CptUserId;

                        // PM
                        if (item.UsrPrfId.GetValueOrDefault() == pm || item.RoleId == 22)
                            cptVm.Pm = item.CptUserId;

                        // CRM
                        if (item.UsrPrfId.GetValueOrDefault() == crm || item.RoleId == 153)
                            cptVm.Crm = item.CptUserId;
                    }
                }
                return Ok(cptVm);
            }
            else
            {
                _logger.LogInformation($"CPT by Id: { id } not found.");
                return NotFound(new { Message = $"CPT Id: { id } not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] CptViewModel model)
        {
            if (ModelState.IsValid)
            {
                Cpt cpt = _mapper.Map<Cpt>(model);

                cpt.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                cpt.CreatDt = DateTime.Now;
                cpt.ModfdByUserId = null;
                cpt.ModfdDt = null;
                cpt.CptCustNbr = "C" + DateTime.Now.ToString("MMddyyHHmmss");

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(cpt.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                cpt.CsgLvlId = _l2pInterface.GetH1CsgLevelId(cpt.H1);

                if (cpt.SprntWhlslReslrCd)
                {
                    cpt.CompnyNme = string.Format("{0} - {1}", cpt.SprntWhlslReslrNme, cpt.CompnyNme);
                }

                List<int> primSites = new List<int>();
                if (cpt.GovtCustCd.HasValue && cpt.GovtCustCd.Value)
                {
                    primSites.Add((int)Entities.Enums.CptPrimSite.FMNS);
                }

                if (cpt.HstMgndSrvcCd)
                {
                    cpt.CptMngdSrvc = GetCptMngdSrvc(model.HostedManagedServices, model.CptId).ToList();
                    // PrimSite for WPaaS Customers = IAAS
                    if (model.HostedManagedServices.Contains(1))
                    {
                        primSites.Add((int)Entities.Enums.CptPrimSite.IAAS);
                    }
                }
                
                cpt.CptRltdInfo = _mapper.Map<IEnumerable<CptRltdInfo>>(model.RelatedInfos).ToList();
                if (model.RelatedInfos != null && model.RelatedInfos.Count() > 0)
                {
                    foreach (CptRelatedInfoViewModel rltd in model.RelatedInfos)
                    {
                        if (rltd.CptCustTypeId == (short)CptCustomerType.ManagedData)
                        {
                            primSites.Add((int)Entities.Enums.CptPrimSite.MNSD);
                            cpt.CptPrimWan = cpt.CptPrimWan.Concat(GetCptPrimWan(rltd.CptPrimWan.ToList(), rltd.CptPlnSrvcTierId.Value)).ToList();
                            cpt.CptScndyTprt = cpt.CptScndyTprt.Concat(GetCptScndyTprt(rltd.SecTrnsprtTypes, rltd.CptPlnSrvcTierId.Value, model.CptId)).ToList();

                            if (rltd.CptPlnSrvcTierId == (short)CptPlnSrvcTier.Mss)
                            {
                                primSites.Add((int)Entities.Enums.CptPrimSite.MNSW);
                            }

                            if (rltd.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsSupport)
                            {
                                cpt.CptSuprtTier = cpt.CptSuprtTier.Concat(GetCptSuprtTier(model.MdsSupportTiers, model.CptId)).ToList();
                            }
                        }
                        else if ((rltd.CptCustTypeId == (short)CptCustomerType.ManagedSecurity))
                        {
                            primSites.Add((int)Entities.Enums.CptPrimSite.MNSW);
                            cpt.CptSrvcType = cpt.CptSrvcType.Concat(GetCptSrvcType(model.MssPlnSrvcTypes, model.CptId)).ToList();
                            cpt.CptMngdAuth = cpt.CptMngdAuth.Concat(GetCptMngdAuth(model.MssMngdAuthenticationProds, model.CptId)).ToList();
                        }
                        else if (rltd.CptCustTypeId == (short)CptCustomerType.ManagedVoice)
                        {
                            primSites.Add((int)Entities.Enums.CptPrimSite.MVSC);
                            cpt.CptMvsProd = cpt.CptMvsProd.Concat(GetCptMvsProd(model.MvsProdTypes, model.CptId)).ToList();
                            cpt.CptDoc = GetCptDocs(model.CptDocs.ToList());
                        }
                    }
                }

                // PrimSites
                cpt.CptPrimSite = GetCptPrimSite(primSites, model.CptId).ToList();

                // Provision
                cpt.CptPrvsn = GetCptPrvsn(model);

                // User Assignment
                cpt.CptUserAsmt = GetUserAssinments(model);

                cpt.CptStusId = (short)CptStatus.Submitted;

                // History
                cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTSubmitted, model.Notes, model.CptId));

                var c = _repo.Create(cpt);
                c = _repo.GetSecuredData(c);

                // [Updated by Sarah Sandoval - 20210610] - Save Contact Details (PJ025845)
                if (model.ContactDetails != null && model.ContactDetails.Count() > 0)
                {
                    var cntctDtl = _mapper.Map<IEnumerable<CntctDetl>>(model.ContactDetails).ToList();
                    try
                    {
                        // Added by Sarah Sandoval [20220126] - Manual Contact List on CPT History
                        List<string> currentEmails = _contctRepo.Find(i => i.CreatByUserId > 1 && i.RecStusId == 1 && i.ObjTypCd == "C" && i.ObjId == cpt.CptId).Select(i => i.EmailAdr).ToList();
                        List<CntctDetl> newCntcts = cntctDtl.FindAll(i => !currentEmails.Contains(i.EmailAdr) && i.CreatByUserId > 1);
                        if (newCntcts != null && newCntcts.Count() > 0)
                        {
                            string emails = string.Empty;
                            emails = string.Join(",", newCntcts.FindAll(i => i.CreatByUserId > 1).Select(i => i.EmailAdr).ToList());

                            if (!string.IsNullOrWhiteSpace(emails))
                            {
                                _repo.CreateCptHistory(CreateCptHistory((byte)Actions.ManualContactListData,
                                    "The following email/s were added manually to Contact List table: " + emails, cpt.CptId, true));
                            }
                        }

                        cntctDtl.ForEach(item => {
                            item.ObjTypCd = "C";
                            item.ObjId = c.CptId;
                            item.RecStusId = (short)ERecStatus.Active;
                            //item.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                            item.CreatDt = DateTime.Now;
                            item.ModfdByUserId = null;
                            item.ModfdDt = null;
                        });

                        _contctRepo.Create(cntctDtl);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation($"CPT Contact Detail could not be created. { JsonConvert.SerializeObject(cntctDtl, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                }

                if (c != null)
                {
                    model.CptId = c.CptId;

                    // Added by Sarah Sandoval [20210616] - Remove AM, IPM, SE Emails and include Contact Details and additional contact email
                    // Email Notification for created CPT Request
                    List<string> emailaddrs = new List<string>();
                    //emailaddrs.Add(cpt.AmEmailAdr);
                    //emailaddrs.Add(cpt.IpmEmailAdr);
                    //emailaddrs.Add(cpt.SeEmailAdr);
                    emailaddrs.Add(_usrRepo.GetById(cpt.SubmtrUserId).EmailAdr);
                    emailaddrs.Add(_commonRepo.GetCowsAppCfgValue("CPT Submit PDL"));
                    emailaddrs.AddRange(cpt.CptRltdInfo.Select(i => i.CntctEmailList).Distinct());
                    emailaddrs.AddRange(_contctRepo.GetEmailAddresses(cpt.CptId, "C", ((int)CptStatus.Submitted).ToString()));

                    var companyName = cpt.CsgLvlId > 0 ? CustomerMaskedData.customerName : cpt.CompnyNme;
                    string emailTo = string.Join(",", emailaddrs.Where(i => !string.IsNullOrEmpty(i)));
                    string emailMessage = "You have been notified that a new CPT request has been created.";
                    string subject = cpt.CptCustNbr + " has been Created; Customer Name: " + companyName;
                    c.EmailReq.Add(SaveEmailRequestForCPTNotification(true, model.Notes, subject, emailTo, emailMessage, cpt));

                    if (c.CptRltdInfo != null && c.CptRltdInfo.Count() == 1
                            && c.CptRltdInfo.SingleOrDefault(i =>
                                i.CptCustTypeId == (short)CptCustomerType.Sps) != null)
                    {
                        // Set Gatekeeper auto assignment for SPS
                        // since provisioning is not needed for this types.
                        c.CptUserAsmt = GetUserAssinments(SetAutoAssignmentValues(model));
                        c.CptStusId = (short)CptStatus.Assigned;
                        c.CptHist.Add(CreateCptHistory((byte)Actions.CPTAssigned,
                            GetSystemNotes(true, c), c.CptId, true));

                        var emailReq = SetEmailAssignmentNotification(c, cpt);

                        if (emailReq != null)
                        {
                            c.EmailReq.Add(emailReq);
                            c.CptHist.Add(new CptHist
                            {
                                ActnId = (byte)Actions.CPTSentAssignmentEmail,
                                CptId = cpt.CptId,
                                CmntTxt = "Email sent to " + emailReq.EmailListTxt,
                                CreatByUserId = 1, // COWS System
                                CreatDt = DateTime.Now
                            });
                        }
                    }
                    else
                    {
                        // Send 24 hour Email Notifcation for requests not equal to SPS
                        c.EmailReq = c.EmailReq.Concat(SaveEmailRequestFor24HourCPTNotification(true, model.Notes, c)).ToList();
                        c.CptHist = c.CptHist.Concat(SaveEmailOnCPTHistory(c.EmailReq.ToList(), c.CptId)).ToList();
                    }

                    _repo.Update(c.CptId, c);

                    // Update Cache
                    _cache.Remove(CacheKeys.CptList);
                    _logger.LogInformation($"CPT Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/CPT/{ c.CptId }", new { cptid = c.CptId });
                }
            }

            return BadRequest(new { Message = "CPT Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] CptViewModel model)
        {
            if (ModelState.IsValid)
            {
                Cpt cpt = _mapper.Map<Cpt>(model);
                cpt.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                cpt.ModfdDt = DateTime.Now;

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(cpt.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                cpt.CsgLvlId = _l2pInterface.GetH1CsgLevelId(cpt.H1);

                // Validate Shortname; No need to validate SPS since Provisioning is not needed for this type;
                bool isShortnameValid = false;
                if (cpt.CptStusId == (short)CptStatus.Submitted
                    && !string.IsNullOrWhiteSpace(cpt.CustShrtNme)
                    && (model.RelatedInfos != null
                        && (model.RelatedInfos.Count > 0
                            || (model.RelatedInfos.Count == 1
                                && model.RelatedInfos.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.Sps) == null))))
                {
                    int reqId = 0;
                    bool isDBLink = Boolean.Parse(_commonRepo.GetCowsAppCfgValue("CPT DB Shortname Validation"));

                    if (isDBLink)
                    {
                        isShortnameValid = _repo.ValidateShortNameByLinkedServer(cpt.H1, cpt.CustShrtNme);
                    }
                    else
                    {
                        int _iCntr = 0;
                        OdieRspn response;
                        reqId = _odieReqRepo.InsertOdieShortNameRequest(cpt.CptId, cpt.H1, cpt.CustShrtNme, cpt.CsgLvlId);
                        while (!isShortnameValid && _iCntr <= 10)
                        {
                            _iCntr++;
                            response = _odieResRepo.Find(i => i.ReqId == reqId && i.Req.StusId == 21 && i.AckCd == true).SingleOrDefault();
                            isShortnameValid = response != null ? response.AckCd.GetValueOrDefault(false) : false;
                            if (!isShortnameValid)
                                System.Threading.Thread.Sleep(4000);
                            else
                                _iCntr = 10;
                        }
                    }

                    // Save to CPT History
                    if (isShortnameValid)
                    {
                        cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTODIE, cpt.CustShrtNme + " is valid.", id, true));

                        // Once Shortname is validated for MVS/E2E, it will complete
                        if (cpt.CptRltdInfo != null && (cpt.CptRltdInfo.Count > 1
                            || (cpt.CptRltdInfo.Count == 1
                                && cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.UnmanagedE2e) == null
                                && cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedVoice) == null)))
                        {
                            cpt.CptStusId = (short)CptStatus.Provisioned;
                            cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTProvisioned, model.Notes, id, true));
                        }
                    }
                    else
                    {
                        string note = string.Format("{0} is invalid. ODIE Request ID is {1}.", cpt.CustShrtNme, reqId);
                        var hist = CreateCptHistory((byte)Actions.CPTODIE, model.Notes, id, true);
                        _repo.CreateCptHistory(hist);
                        return BadRequest(new { Message = "Customer Shortname mismatch. Kindly enter a new one or cancel this request." });
                    }
                }

                List<int> primSites = new List<int>();
                if (cpt.GovtCustCd.HasValue && cpt.GovtCustCd.Value)
                {
                    primSites.Add((int)Entities.Enums.CptPrimSite.FMNS);
                }

                if (cpt.HstMgndSrvcCd)
                {
                    cpt.CptMngdSrvc = GetCptMngdSrvc(model.HostedManagedServices, id).ToList();
                    // PrimSite for WPaaS Customers = IAAS
                    if (model.HostedManagedServices.Contains(1))
                    {
                        primSites.Add((int)Entities.Enums.CptPrimSite.IAAS);
                    }
                }

                if (model.RelatedInfos != null && model.RelatedInfos.Count() > 0)
                {
                    foreach (CptRelatedInfoViewModel rltd in model.RelatedInfos)
                    {
                        rltd.CptId = id;
                        if (rltd.CptCustTypeId == (short)CptCustomerType.ManagedData)
                        {
                            primSites.Add((int)Entities.Enums.CptPrimSite.MNSD);
                            cpt.CptPrimWan = cpt.CptPrimWan.Concat(GetCptPrimWan(rltd.CptPrimWan.ToList(), rltd.CptPlnSrvcTierId.Value)).ToList();
                            cpt.CptScndyTprt = cpt.CptScndyTprt.Concat(GetCptScndyTprt(rltd.SecTrnsprtTypes, rltd.CptPlnSrvcTierId.Value, id)).ToList();

                            if (rltd.CptPlnSrvcTierId == (short)CptPlnSrvcTier.Mss)
                            {
                                primSites.Add((int)Entities.Enums.CptPrimSite.MNSW);
                            }

                            if (rltd.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsSupport)
                            {
                                cpt.CptSuprtTier = cpt.CptSuprtTier.Concat(GetCptSuprtTier(model.MdsSupportTiers, id)).ToList();
                            }
                        }
                        else if ((rltd.CptCustTypeId == (short)CptCustomerType.ManagedSecurity))
                        {
                            primSites.Add((int)Entities.Enums.CptPrimSite.MNSW);
                            cpt.CptSrvcType = cpt.CptSrvcType.Concat(GetCptSrvcType(model.MssPlnSrvcTypes, id)).ToList();
                            cpt.CptMngdAuth = cpt.CptMngdAuth.Concat(GetCptMngdAuth(model.MssMngdAuthenticationProds, id)).ToList();
                        }
                        else if (rltd.CptCustTypeId == (short)CptCustomerType.ManagedVoice)
                        {
                            primSites.Add((int)Entities.Enums.CptPrimSite.MVSC);
                            cpt.CptMvsProd = cpt.CptMvsProd.Concat(GetCptMvsProd(model.MvsProdTypes, id)).ToList();
                            cpt.CptDoc = GetCptDocs(model.CptDocs.ToList());
                        }
                    }
                }
                cpt.CptRltdInfo = _mapper.Map<IEnumerable<CptRltdInfo>>(model.RelatedInfos).ToList();

                // PrimSites
                cpt.CptPrimSite = GetCptPrimSite(primSites, id).ToList();

                var isNotifyIP = cpt.TacacsCd
                            || cpt.CptPrimWan.FirstOrDefault(i => i.CptPlnSrvcTierId != (int)CptPlnSrvcTier.WholesaleCarrier
                                    && i.CptPrimScndyTprtId != (int)CptTransportType.IPSprintLink
                                    && i.CptPrimScndyTprtId != (int)CptTransportType.WirelessWANWDLS) != null;

                if (cpt.CptRltdInfo.FirstOrDefault(i =>
                            i.CptCustTypeId == (int)CptCustomerType.ManagedData
                            && i.CptPlnSrvcTierId == (int)CptPlnSrvcTier.MdsComplete
                            && i.SdwanCustCd.GetValueOrDefault(false)) != null)
                    isNotifyIP = false;

                Cpt oldCpt = _repo.GetById(id);
                string tempNote = string.Empty;
                // For Cancel CPT Request
                if (model.CancelCpt)
                {
                    // Delete User Assignment
                    cpt.CptUserAsmt = null;

                    // For Cancelled
                    cpt.CptStusId = (short)CptStatus.Cancelled;
                    tempNote = string.Format("Additional System Notes: {0} cancelled {1}.",
                        _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr);
                    cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTCancelled,
                        string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                            : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                }
                // Additional changes made after request is submitted but shortname is not provided
                else if (cpt.CptStusId == (short)CptStatus.Submitted)
                {
                    tempNote = string.Format("Additional System Notes: {0} updated {1}.",
                        _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr);
                    cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTUpdated,
                        string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                            : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                }
                // For IP Notification; No assignment yet until IP is provided
                else if (isNotifyIP && !string.IsNullOrWhiteSpace(cpt.CustShrtNme)
                    && oldCpt.CptUserAsmt.Count <= 0
                    && (string.IsNullOrWhiteSpace(cpt.SubnetIpAdr)
                            || string.IsNullOrWhiteSpace(cpt.StartIpAdr)
                            || string.IsNullOrWhiteSpace(cpt.EndIpAdr)))
                {
                    // Save notes as needed for IP Address
                    // Added condition to prevent duplicate notes in History
                    // On Valid Shortname, Notes is already save in History
                    if (!isShortnameValid)
                    {
                        tempNote = string.Format("Additional System Notes: {0} updated {1}.",
                        _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr);
                        cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTUpdated,
                            string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                                : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                    }

                    // Added this condition if ever IP is supplied one by one to prevent assigning users
                    // until all IP Address is provide and send only one notification per day
                    if (!oldCpt.CptHist.Any(i => i.ActnId == (byte)Actions.CPTSentIPEmail))
                    {
                        cpt.EmailReq = cpt.EmailReq.Concat(SaveEmailRequestFor24HourCPTNotification(false, string.Empty, cpt)).ToList();
                        cpt.CptHist = cpt.CptHist.Concat(SaveEmailOnCPTHistory(cpt.EmailReq.ToList(), id)).ToList();
                    }
                }
                // For Returned to SDE
                else if (model.RetrnSdeCd)
                {
                    // For Returned to SDE
                    cpt.CptStusId = (short)CptStatus.ReturnedToSDE;
                    tempNote = string.Format("Additional System Notes: {0} returned {1} to SDE.",
                        _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr);
                    cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTReturnedToSDE,
                        string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                            : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                }
                // For Resubmitting CPT Request
                else if (oldCpt != null && oldCpt.RetrnSdeCd && !model.RetrnSdeCd)
                {
                    // For Resubmitted
                    cpt.CptStusId = (short)CptStatus.Assigned;
                    tempNote = string.Format("Additional System Notes: {0} resubmitted {1}.",
                        _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr);
                    cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTResubmitted,
                        string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                            : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                }
                // For Reviewed by PM
                else if (model.RevwdPmCd)
                {
                    // For Reviewed by Pm
                    cpt.CptStusId = (short)CptStatus.ReviewedByPM;
                    tempNote = string.Format("Additional System Notes: {0} reviewed {1}.",
                        _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr);
                    cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTReviewedByPM,
                        string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                            : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                }
                // For Reassignment
                else if (oldCpt != null && oldCpt.RevwdPmCd && !model.RevwdPmCd)
                {
                    // For Reset ReviewedyPm
                    cpt.CptStusId = (short)CptStatus.Assigned;
                    tempNote = string.Format("Additional System Notes: {0} reassigned {1} from {2} to {3}. {4}",
                        _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr,
                        oldCpt.CptUserAsmt.OrderByDescending(i => i.CreatDt)
                            .FirstOrDefault(i => i.UsrPrfId == pm
                                && i.RecStusId == (byte)ERecStatus.InActive).CptUser.FullNme,
                        cpt.CptUserAsmt.SingleOrDefault(i => i.UsrPrfId == pm
                            && i.RecStusId == (byte)ERecStatus.Active).CptUser.FullNme,
                       GetSystemNotes(false, cpt));
                    cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTAssigned,
                        string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                            : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                }
                // For Assignment 
                else
                {
                    // No assignment section for MVS/E2E type
                    if (cpt.CptRltdInfo != null && (cpt.CptRltdInfo.Count > 1
                        || (cpt.CptRltdInfo.Count == 1
                            && cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.UnmanagedE2e) == null
                            && cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedVoice) == null)))
                    {
                        // Set User Auto Assignment for Provisioned Status
                        // User Assignment will happen only once Shortname and/or IP Address are supplied
                        if (cpt.CptStusId == (short)CptStatus.Provisioned)
                        {
                            cpt.CptUserAsmt = GetUserAssinments(SetAutoAssignmentValues(model));
                            cpt.CptStusId = (short)CptStatus.Assigned;
                            tempNote = string.IsNullOrWhiteSpace(model.Notes) ? GetSystemNotes(true, cpt)
                                    : string.Format("{0} {1}", string.Empty, GetSystemNotes(true, cpt));
                            cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTAssigned, tempNote, id, true));
                        }
                        else
                        {
                            cpt.CptUserAsmt = GetUserAssinments(model);
                            cpt.CptStusId = (short)CptStatus.Assigned;
                            tempNote = string.Format("Additional System Notes: {0} updated {1}. {2}",
                            _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr,
                            GetSystemNotes(false, cpt));
                            cpt.CptHist.Add(CreateCptHistory((byte)Actions.CPTAssigned,
                                string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                                    : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                        }

                        // Send Email Resource Notification
                        // Either Auto Assignment or Change User Assignment
                        var emailReq = SetEmailAssignmentNotification(cpt, oldCpt);
                        if (emailReq != null)
                        {
                            cpt.EmailReq.Add(emailReq);
                            cpt.CptHist.Add(new CptHist
                            {
                                ActnId = (byte)Actions.CPTSentAssignmentEmail,
                                CptId = cpt.CptId,
                                CmntTxt = "Email sent to " + emailReq.EmailListTxt,
                                CreatByUserId = 1, // COWS System
                                CreatDt = DateTime.Now
                            });
                        }
                    }
                }

                _repo.Update(id, cpt);

                cpt = _repo.GetById(id);

                // [Updated by Sarah Sandoval - 20210610] - Save Contact Details (PJ025845)
                if (model.ContactDetails != null && model.ContactDetails.Count() > 0)
                {
                    var cntctDtl = _mapper.Map<IEnumerable<CntctDetl>>(model.ContactDetails).ToList();
                    try
                    {
                        // Added by Sarah Sandoval [20220126] - Manual Contact List on CPT History
                        List<string> currentEmails = _contctRepo.Find(i => i.CreatByUserId > 1 && i.RecStusId == 1 && i.ObjTypCd == "C" && i.ObjId == cpt.CptId).Select(i => i.EmailAdr).ToList();
                        List<CntctDetl> newCntcts = cntctDtl.FindAll(i => !currentEmails.Contains(i.EmailAdr) && i.CreatByUserId > 1);
                        if (newCntcts != null && newCntcts.Count() > 0)
                        {
                            string emails = string.Empty;
                            emails = string.Join(",", newCntcts.FindAll(i => i.CreatByUserId > 1).Select(i => i.EmailAdr).ToList());

                            if (!string.IsNullOrWhiteSpace(emails))
                            {
                                _repo.CreateCptHistory(CreateCptHistory((byte)Actions.ManualContactListData,
                                    "The following email/s were added manually to Contact List table: " + emails, id, true));
                            }
                        }

                        _contctRepo.SetInactive(i=> i.ObjId == cpt.CptId && i.ObjTypCd == "C" && i.RecStusId == 1);
                        cntctDtl.ForEach(item => {
                            item.ObjTypCd = "C";
                            item.ObjId = cpt.CptId;
                            item.RecStusId = (short)ERecStatus.Active;
                            //item.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                            item.CreatDt = DateTime.Now;
                            item.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                            item.ModfdDt = DateTime.Now;
                        });

                        _contctRepo.Update(cntctDtl);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation($"CPT Contact Detail could not be updated. { JsonConvert.SerializeObject(cntctDtl, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                }

                // Delete other email notification (for Shortname/IP Address Notification)
                DeleteEmailRequests(cpt);

                if (model.CancelCpt)
                {
                    // Send email notification to Submitter, AM Email, Gatekeeper and
                    // CPT Shortname Notification PDL (IPS-MSOnBoarding@sprint.com)
                    cpt.CptHist.Add(SaveEmailCancelationNotice(cpt));

                    // Update Provisioning Status of those that are not yet picked up
                    // This is cater on Repository level, just passed null on CptPrvsn entity
                    //cpt.CptPrvsn = null;

                    _repo.Update(id, cpt);
                }
                else if (model.RetrnSdeCd)
                {
                    // Send email notification to Submitter, Sprint Sales Team (AM, IPM, SE) and Gatekeeper
                    SaveEmailReturnedNotice(cpt);
                }
                else
                {
                    if (CheckProvisioningCompletion(cpt))
                    {
                        if (cpt.RevwdPmCd)
                        {
                            if (CreateRedesign(cpt))
                            {
                                cpt.CptStusId = (short)CptStatus.Completed;
                                tempNote = string.Format("{0} reviewed {1}. CPT Request is now complete.",
                                    _loggedInUser.GetLoggedInUser().FullNme, model.CptCustNbr);
                                cpt.CptHist.Add(CreateCptHistory((byte)Actions.Completed, tempNote, id, true));
                                _repo.Update(id, cpt);

                                // Send CPT Complete Notification
                                SaveEmailCompletionNotice(cpt);
                            }
                            else
                            {
                                return BadRequest(new { Message = "Unable to complete CPT as Redesign creation failed, Please correct H1/Shortname combination in ODIE and retry." });
                            }
                        }
                        else
                        {
                            // MVS/E2E is completed upon shortname validation
                            if (cpt.CptRltdInfo != null && cpt.CptRltdInfo.Count() == 1
                                && (cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.UnmanagedE2e) != null
                                || cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedVoice) != null))
                            {
                                cpt.CptStusId = (short)CptStatus.Completed;
                                tempNote = string.Format("Additional System Notes: {0} is now complete.", model.CptCustNbr);
                                cpt.CptHist.Add(CreateCptHistory((byte)Actions.Completed,
                                    string.IsNullOrWhiteSpace(model.Notes) ? tempNote
                                        : string.Format("{0} {1}", model.Notes.Trim(), tempNote), id));
                                _repo.Update(id, cpt);

                                // Send CPT Complete Notification
                                SaveEmailCompletionNotice(cpt);
                            }
                        }
                    }
                }

                // Update Cache
                _cache.Remove(CacheKeys.CptList);
                _logger.LogInformation($"CPT Updated. { JsonConvert.SerializeObject(model).ToString() } ");
                return Created($"api/CPT/{ id }", new { cptid = cpt.CptId });
            }

            return BadRequest(new { Message = "CPT Could Not Be Updated." });
        }

        [HttpGet("CheckLock/{id}")]
        public IActionResult CheckLock([FromRoute] int id)
        {
            _logger.LogInformation($"Check CPT Lock by Id: { id }.");

            var obj = _repoLock.CheckLock(id);
            if (obj != null)
            {
                if (obj.LockByUser != null && obj.LockByUserId != _loggedInUser.GetLoggedInUserId())
                    return Ok(obj.LockByUser.FullNme);
            }

            return Ok();
        }

        [HttpGet("Lock/{id}")]
        public IActionResult Lock([FromRoute] int id)
        {
            _logger.LogInformation($"Lock CPT by Id: { id }.");

            var obj = _repoLock.CheckLock(id);
            if (obj != null)
            {
                if (_loggedInUser.GetLoggedInUserId() != obj.LockByUserId)
                {
                    return BadRequest(new { Message = $"This CPT is currently locked by {obj.LockByUser.FullNme}" });
                }
            }
            else
            {
                int cptId = _repoLock.Lock(new CptRecLock
                {
                    CptId = id,
                    LockByUserId = _loggedInUser.GetLoggedInUserId(),
                    CreatDt = DateTime.Now,
                    StrtRecLockTmst = DateTime.Now
                });

                if (cptId == 0)
                {
                    return BadRequest(new { Message = $"An error occurred while locking the CPT. Please try again" });
                }
            }

            return Ok();
        }

        [HttpGet("Unlock/{id}")]
        public IActionResult Unlock([FromRoute] int id)
        {
            _logger.LogInformation($"Unlock CPT by Id: { id }.");

            _repoLock.Unlock(id, _loggedInUser.GetLoggedInUserId());
            return Ok();
        }

        #endregion CPT Form

        #region CPT Table related data

        private List<CptMngdSrvc> GetCptMngdSrvc(List<int> ids, int cptId)
        {
            List<CptMngdSrvc> cptMngdSrvcs = new List<CptMngdSrvc>();

            if (ids != null && ids.Count() > 0)
            {
                foreach (int id in ids)
                {
                    CptMngdSrvc cptMngdSrvc = new CptMngdSrvc();
                    cptMngdSrvc.CptId = cptId;
                    cptMngdSrvc.CptHstMngdSrvcId = (short)id;
                    cptMngdSrvc.CreatDt = DateTime.Now;
                    cptMngdSrvcs.Add(cptMngdSrvc);
                }
            }

            return cptMngdSrvcs;
        }

        private List<CptPrimWan> GetCptPrimWan(List<CptPrimWanViewModel> primWan, short plnSrvcTierId)
        {
            List<CptPrimWan> cptPrimWan = new List<CptPrimWan>();

            if (primWan != null && primWan.Count() > 0)
            {
                foreach (CptPrimWanViewModel wan in primWan)
                {
                    CptPrimWan cptWan = _mapper.Map<CptPrimWan>(wan);
                    cptWan.CreatDt = DateTime.Now;
                    cptWan.CptPlnSrvcTierId = plnSrvcTierId;
                    cptPrimWan.Add(cptWan);
                }
            }

            return cptPrimWan;
        }

        private List<CptScndyTprt> GetCptScndyTprt(List<int> st, short plnSrvcTierId, int cptId)
        {
            List<CptScndyTprt> cptScndyTprts = new List<CptScndyTprt>();

            if (st != null && st.Count() > 0)
            {
                foreach (int stId in st)
                {
                    CptScndyTprt cptScndyTprt = new CptScndyTprt();
                    cptScndyTprt.CptPlnSrvcTierId = plnSrvcTierId;
                    cptScndyTprt.CptPrimScndyTprtId = (short)stId;
                    cptScndyTprt.CreatDt = DateTime.Now;
                    cptScndyTprt.Cpt = null;
                    cptScndyTprt.CptId = cptId;
                    cptScndyTprt.CptPlnSrvcTier = null;
                    cptScndyTprt.CptPrimScndyTprt = null;
                    cptScndyTprts.Add(cptScndyTprt);
                }
            }

            return cptScndyTprts;
        }

        private List<CptSuprtTier> GetCptSuprtTier(List<int> ids, int cptId)
        {
            List<CptSuprtTier> cptSuprtTiers = new List<CptSuprtTier>();

            if (ids != null && ids.Count() > 0)
            {
                foreach (int id in ids)
                {
                    CptSuprtTier cptSuprtTier = new CptSuprtTier();
                    cptSuprtTier.CptMdsSuprtTierId = (short)id;
                    cptSuprtTier.CreatDt = DateTime.Now;
                    cptSuprtTier.Cpt = null;
                    cptSuprtTier.CptId = cptId;
                    cptSuprtTier.CptMdsSuprtTier = null;
                    cptSuprtTiers.Add(cptSuprtTier);
                }
            }

            return cptSuprtTiers;
        }

        private List<CptSrvcType> GetCptSrvcType(List<int> ids, int cptId)
        {
            List<CptSrvcType> cptSrvcTypes = new List<CptSrvcType>();

            if (ids != null && ids.Count() > 0)
            {
                foreach (int id in ids)
                {
                    CptSrvcType cptSrvcType = new CptSrvcType();
                    cptSrvcType.CptPlnSrvcTypeId = (short)id;
                    cptSrvcType.CreatDt = DateTime.Now;
                    cptSrvcType.Cpt = null;
                    cptSrvcType.CptId = cptId;
                    cptSrvcType.CptPlnSrvcType = null;
                    cptSrvcTypes.Add(cptSrvcType);
                }
            }

            return cptSrvcTypes;
        }

        private List<CptMngdAuth> GetCptMngdAuth(List<int> ids, int cptId)
        {
            List<CptMngdAuth> cptMngdAuths = new List<CptMngdAuth>();

            if (ids != null && ids.Count() > 0)
            {
                foreach (int id in ids)
                {
                    CptMngdAuth cptMngdAuth = new CptMngdAuth();
                    cptMngdAuth.CptMngdAuthPrdId = (short)id;
                    cptMngdAuth.CreatDt = DateTime.Now;
                    cptMngdAuth.CptId = cptId;
                    cptMngdAuths.Add(cptMngdAuth);
                }
            }

            return cptMngdAuths;
        }

        private List<CptMvsProd> GetCptMvsProd(List<int> ids, int cptId)
        {
            List<CptMvsProd> cptMvsProds = new List<CptMvsProd>();

            if (ids != null && ids.Count() > 0)
            {
                foreach (int id in ids)
                {
                    CptMvsProd cptMvsProd = new CptMvsProd();
                    cptMvsProd.CptMvsProdTypeId = (short)id;
                    cptMvsProd.CreatDt = DateTime.Now;
                    cptMvsProd.Cpt = null;
                    cptMvsProd.CptId = cptId;
                    cptMvsProd.CptMvsProdType = null;
                    cptMvsProds.Add(cptMvsProd);
                }
            }

            return cptMvsProds;
        }

        private List<CptDoc> GetCptDocs(List<CptDocViewModel> docs)
        {
            List<CptDoc> cptDocs = new List<CptDoc>();

            foreach (CptDocViewModel doc in docs)
            {
                CptDoc cptDoc = _mapper.Map<CptDoc>(doc);
                cptDoc.CptDocId = 0;
                cptDoc.CptId = doc.CptId > 0 ? doc.CptId : 0;
                cptDoc.FileCntnt = Convert.FromBase64String(doc.Base64string);
                cptDoc.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                cptDoc.CreatDt = DateTime.Now;
                cptDoc.RecStusId = (byte)ERecStatus.Active;
                cptDocs.Add(cptDoc);
            }

            return cptDocs;
        }

        private List<Entities.Models.CptPrimSite> GetCptPrimSite(List<int> ids, int cptId)
        {
            List<Entities.Models.CptPrimSite> cptPrimSites = new List<Entities.Models.CptPrimSite>();

            if (ids != null && ids.Count() > 0)
            {
                foreach (int id in ids)
                {
                    Entities.Models.CptPrimSite cptPrimSite = new Entities.Models.CptPrimSite();
                    cptPrimSite.CptPrimSiteId = (short)id;
                    cptPrimSite.CreatDt = DateTime.Now;
                    cptPrimSite.Cpt = null;
                    cptPrimSite.CptId = cptId;
                    cptPrimSite.CptPrimSiteNavigation = null;
                    cptPrimSites.Add(cptPrimSite);
                }
            }

            return cptPrimSites;
        }

        private List<CptPrvsn> GetCptPrvsn(CptViewModel model)
        {
            List<CptPrvsn> cptPrvsns = new List<CptPrvsn>();

            // NOTE: MVS, E2E and SPS Type do not have Provisioning section
            if (model.RelatedInfos != null && model.RelatedInfos.Count() == 1
                && (model.RelatedInfos.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedVoice) != null
                || model.RelatedInfos.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.UnmanagedE2e) != null
                || model.RelatedInfos.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.Sps) != null))
            {
                return null;
            }

            if (model.RelatedInfos != null && model.RelatedInfos.Count() > 0)
            {
                CptPrvsn cptPrvsn;

                // Spectrum: ManagedData (Mss, MdsComplete, MdsSupport and WholesaleVar) and ManagedSecurity
                bool isSpectrum = (model.RelatedInfos.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedData
                        && (i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.Mss
                            || i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsComplete
                            || i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsSupport
                            || i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.WholesaleVar)) != null
                        && model.RelatedInfos.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedData
                            && i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsComplete && i.SdwanCustCd == true) == null)
                    || model.RelatedInfos.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedSecurity) != null;
                cptPrvsn = new CptPrvsn();
                cptPrvsn.CptPrvsnTypeId = (short)CptProvision.Spectrum;
                cptPrvsn.PrvsnStusCd = isSpectrum;
                cptPrvsn.RecStusId = (short)ProvisionStatus.PendingPickUp;
                cptPrvsn.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                cptPrvsn.CreatDt = DateTime.Now;
                cptPrvsns.Add(cptPrvsn);

                // Voyence: ManagedData  (Mss, MdsComplete and WholesaleVar)
                bool isVoyence = model.RelatedInfos.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedData
                        && (i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.Mss
                            || i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsComplete
                            || i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.WholesaleVar)) != null
                        && model.RelatedInfos.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedData
                            && i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsComplete && i.SdwanCustCd == true) == null;
                cptPrvsn = new CptPrvsn();
                cptPrvsn.CptPrvsnTypeId = (short)CptProvision.Voyence;
                cptPrvsn.PrvsnStusCd = isVoyence;
                cptPrvsn.RecStusId = (short)ProvisionStatus.PendingPickUp;
                cptPrvsn.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                cptPrvsn.CreatDt = DateTime.Now;
                cptPrvsns.Add(cptPrvsn);

                // QIP: ManagedData (Mss, MdsComplete(or SDWanOnly), MdsSupport and WholesaleVar)
                bool isQip = model.RelatedInfos.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedData
                        && (i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.Mss
                            || i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsComplete
                            || i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsSupport
                            || i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.WholesaleVar)) != null;
                cptPrvsn = new CptPrvsn();
                cptPrvsn.CptPrvsnTypeId = (short)CptProvision.Qip;
                cptPrvsn.PrvsnStusCd = isQip;
                cptPrvsn.RecStusId = (short)ProvisionStatus.PendingPickUp;
                cptPrvsn.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                cptPrvsn.CreatDt = DateTime.Now;
                cptPrvsns.Add(cptPrvsn);

                // TACACS Container gets Needed once "Is this customer requesting TACACS access to devices?" is set to Yes
                cptPrvsn = new CptPrvsn();
                cptPrvsn.CptPrvsnTypeId = (short)CptProvision.Tacacs;
                cptPrvsn.PrvsnStusCd = model.TacacsCd;
                cptPrvsn.RecStusId = (short)ProvisionStatus.PendingPickUp;
                cptPrvsn.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                cptPrvsn.CreatDt = DateTime.Now;
                cptPrvsns.Add(cptPrvsn);
            }
            
            return cptPrvsns;
        }

        private string GetTransportTypeName(int transportTypeID)
        {
            if (transportTypeID == (int)CptTransportType.IPSprintLink)
                return "IP/SprintLink";
            else if (transportTypeID == (int)CptTransportType.GMPLS)
                return CptTransportType.GMPLS.ToString();
            else if (transportTypeID == (int)CptTransportType.PIP)
                return CptTransportType.PIP.ToString();
            else if (transportTypeID == (int)CptTransportType.SprintLinkFR)
                return "SprintLink FR/L2TP";
            else if (transportTypeID == (int)CptTransportType.IPSecVPN)
                return "IP/IP Sec VPN";
            else if (transportTypeID == (int)CptTransportType.GMPLSBroadband)
                return "GMPLS over 3rd Party DSL and/or Broadband";
            else if (transportTypeID == (int)CptTransportType.WirelessWANWDLS)
                return "Wireless WAN/WDLS";

            return string.Empty;
        }

        private CptHist CreateCptHistory(byte actionId, string notes, int cptId, bool isSystem = false)
        {
            CptHist hist = new CptHist();
            hist.ActnId = actionId;
            hist.CptId = cptId;
            hist.CmntTxt = notes;
            hist.CreatByUserId = isSystem ? 1 : _loggedInUser.GetLoggedInUserId();
            hist.CreatDt = DateTime.Now;

            return hist;
        }

        #endregion CPT Table related data

        #region CPT Assignment

        // Get User Assignments from Model
        private List<CptUserAsmt> GetUserAssinments(CptViewModel model)
        {
            List<CptUserAsmt> users = new List<CptUserAsmt>();

            if (model.Gatekeeper.HasValue && model.Gatekeeper.Value != 0)
            {
                users.Add(SaveCptUserAsmtObj(model.Gatekeeper.Value, gk, model.CptId));
            }
            if (model.Manager.HasValue && model.Manager.Value != 0)
            {
                users.Add(SaveCptUserAsmtObj(model.Manager.Value, manager, model.CptId));
            }
            if (model.Mss.HasValue && model.Mss.Value != 0)
            {
                users.Add(SaveCptUserAsmtObj(model.Mss.Value, mss, model.CptId));
            }
            if (model.Nte.HasValue && model.Nte.Value != 0)
            {
                users.Add(SaveCptUserAsmtObj(model.Nte.Value, nte, model.CptId));
            }
            if (model.Pm.HasValue && model.Pm.Value != 0)
            {
                users.Add(SaveCptUserAsmtObj(model.Pm.Value, pm, model.CptId));
            }
            if (model.Crm.HasValue && model.Crm.Value != 0)
            {
                users.Add(SaveCptUserAsmtObj(model.Crm.Value, crm, model.CptId));
            }

            return users;
        }

        // Save CPT user Assignment Object
        private CptUserAsmt SaveCptUserAsmtObj(int usrId, short? prfId, int cptId)
        {
            CptUserAsmt user = new CptUserAsmt();
            user.CreatByUserId = _loggedInUser.GetLoggedInUserId();
            user.CreatDt = DateTime.Now;
            user.CptUserId = usrId;
            user.CptId = cptId;
            user.UsrPrfId = prfId;
            user.RecStusId = (byte)ERecStatus.Active;

            return user;
        }

        // Set Auto Assignment for some fields per CPT Type request
        private CptViewModel SetAutoAssignmentValues(CptViewModel model)
        {
            // Manual assignment for these type of requests
            if (model.RelatedInfos != null && model.RelatedInfos.Count() > 0)
            {
                if (model.RelatedInfos.SingleOrDefault(i =>
                        i.CptCustTypeId == (short)CptCustomerType.ManagedSecurity) != null
                    || model.RelatedInfos.SingleOrDefault(i =>
                        i.CptCustTypeId == (short)CptCustomerType.ManagedVoice) != null
                    || model.RelatedInfos.SingleOrDefault(i =>
                        i.CptCustTypeId == (short)CptCustomerType.Sps) != null)
                {
                    model.Gatekeeper = (model.Gatekeeper.HasValue && model.Gatekeeper.Value > 0)
                        ? model.Gatekeeper : SetAutoAssign(gk);
                }

                if (model.RelatedInfos.SingleOrDefault(i =>
                        i.CptCustTypeId == (short)CptCustomerType.Sps) != null)
                {
                    model.Pm = (model.Pm.HasValue && model.Pm.Value > 0)
                        ? model.Pm : SetAutoAssign(pm);
                }

                if (model.RelatedInfos.SingleOrDefault(i =>
                        i.CptCustTypeId == (short)CptCustomerType.ManagedData) != null)
                {
                    // Whole Carrier and Wholesale Var function the same as MDS Support Monitor and Notify
                    if (model.RelatedInfos.SingleOrDefault(i =>
                            i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.WholesaleCarrier) != null
                        || model.RelatedInfos.SingleOrDefault(i =>
                            i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.WholesaleVar) != null)
                    {
                        // Changed model.Manager.GetValueOrDefault() to 3 to autoselect only pooled NTE
                        model.Nte = (model.Nte.HasValue && model.Nte.Value > 0)
                            ? model.Nte : SetAutoAssign(nte, 3);
                        model.Pm = (model.Pm.HasValue && model.Pm.Value > 0)
                            ? model.Pm : SetAutoAssign(pm);
                        model.Crm = (model.Crm.HasValue && model.Crm.Value > 0)
                            ? model.Crm : SetAutoAssign(crm);
                    }

                    if (model.HostedManagedServices != null
                        && model.HostedManagedServices.Count() > 0
                        && model.HostedManagedServices.Contains((int)CptHostedManagedService.Wpaas))
                    {
                        model.Gatekeeper = (model.Gatekeeper.HasValue && model.Gatekeeper.Value > 0)
                            ? model.Gatekeeper : SetAutoAssign(gk);
                        model.Pm = (model.Pm.HasValue && model.Pm.Value > 0)
                            ? model.Pm : SetAutoAssign(pm);
                    }

                    if (model.RelatedInfos.SingleOrDefault(i =>
                            i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsSupport) != null)
                    {
                        if (model.MdsSupportTiers != null
                            && model.MdsSupportTiers.Count() > 0
                            && model.MdsSupportTiers.Contains((int)CptMdsSupportTier.Design))
                        {
                            model.Gatekeeper = (model.Gatekeeper.HasValue && model.Gatekeeper.Value > 0)
                            ? model.Gatekeeper : SetAutoAssign(gk);
                            model.Crm = (model.Crm.HasValue && model.Crm.Value > 0)
                                ? model.Crm : SetAutoAssign(crm);
                        }

                        if (model.MdsSupportTiers != null
                            && model.MdsSupportTiers.Count() > 0
                            && model.MdsSupportTiers.Contains((int)CptMdsSupportTier.Implementation))
                        {
                            model.Gatekeeper = (model.Gatekeeper.HasValue && model.Gatekeeper.Value > 0)
                                ? model.Gatekeeper : SetAutoAssign(gk);
                            model.Pm = (model.Pm.HasValue && model.Pm.Value > 0)
                                ? model.Pm : SetAutoAssign(pm);
                            model.Crm = (model.Crm.HasValue && model.Crm.Value > 0)
                                ? model.Crm : SetAutoAssign(crm);
                        }

                        if (model.MdsSupportTiers != null
                            && model.MdsSupportTiers.Count() > 0
                            && model.MdsSupportTiers.Contains((int)CptMdsSupportTier.MonitorNotify))
                        {
                            if (model.RelatedInfos.SingleOrDefault(i => i.SuplmntlEthrntCd.GetValueOrDefault()
                                && i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsSupport) != null)
                            {
                                model.Gatekeeper = (model.Gatekeeper.HasValue && model.Gatekeeper.Value > 0)
                                ? model.Gatekeeper : SetAutoAssign(gk);
                            }
                            else
                            {
                                // Changed model.Manager.GetValueOrDefault() to 3 to autoselect only pooled NTE
                                model.Nte = (model.Nte.HasValue && model.Nte.Value > 0)
                                                ? model.Nte : SetAutoAssign(nte, 3);
                            }

                            model.Pm = (model.Pm.HasValue && model.Pm.Value > 0)
                                ? model.Pm : SetAutoAssign(pm);
                            model.Crm = (model.Crm.HasValue && model.Crm.Value > 0)
                                ? model.Crm : SetAutoAssign(crm);
                        }
                    }

                    if (model.RelatedInfos.SingleOrDefault(i =>
                            i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsComplete) != null
                        || model.RelatedInfos.SingleOrDefault(i =>
                            i.CptPlnSrvcTierId == (short)CptPlnSrvcTier.Mss) != null)
                    {
                        model.Gatekeeper = (model.Gatekeeper.HasValue && model.Gatekeeper.Value > 0)
                                ? model.Gatekeeper : SetAutoAssign(gk);
                        model.Pm = (model.Pm.HasValue && model.Pm.Value > 0)
                            ? model.Pm : SetAutoAssign(pm);
                        model.Crm = (model.Crm.HasValue && model.Crm.Value > 0)
                            ? model.Crm : SetAutoAssign(crm);
                    }
                }
            }

            return model;
        }

        private int SetAutoAssign(short profileId, int managerId = 0)
        {
            int cptUsrId = 0;
            List<CptUser> prfUsers = _repo.GetAssignmentByProfileId(profileId);

            if (profileId == nte)
            {
                // For NTE Pool
                if (managerId == 3)
                {
                    prfUsers = prfUsers.Where(i => i.PoolCd).ToList();
                }
            }

            decimal lowest = decimal.MaxValue;
            int lowestCPT = int.MaxValue;

            if (prfUsers != null && prfUsers.Count > 0)
            {
                List<CptUser> lowestUserAssignment = new List<CptUser>();
                Random random;
                int index = 0;

                if (profileId == nte || profileId == pm)
                {
                    lowest = prfUsers.Min(i => i.CptQty);
                    lowestUserAssignment = prfUsers.FindAll(i => i.CptQty == lowest && i.UserId != 1);
                    random = new Random();
                    index = random.Next(lowestUserAssignment.Count);
                    cptUsrId = lowestUserAssignment[index].UserId;
                }
                else
                {
                    foreach (CptUser user in prfUsers)
                    {
                        if (user.FullName.ToLower() != "nte pool" && user.UserId != 1 && user.Tasks < lowestCPT)
                        {
                            lowestCPT = user.Tasks;
                            cptUsrId = user.UserId;
                        }
                    }
                }

                // If none was selected from above, sort users by CPT_QTY and get the first one.
                if (cptUsrId == 0)
                {
                    if (profileId == gk || profileId == manager || profileId == crm)
                    {
                        if (prfUsers.FirstOrDefault(i => i.UserId != 1 && i.FullName.ToLower() != "nte pool") != null)
                        {
                            cptUsrId = prfUsers.FirstOrDefault(i => i.UserId != 1 && i.FullName.ToLower() != "nte pool").UserId;
                        }
                    }
                    else
                    {
                        prfUsers.Sort((i1, i2) => i1.CptQty.CompareTo(i2.CptQty));
                        cptUsrId = prfUsers[0].UserId;
                    }
                }
            }

            return cptUsrId;
        }

        private string GetProfileName(short profileId)
        {
            string profile = string.Empty;

            if (profileId == gk)
                profile = "CPTGatekeeper";
            else if (profileId == manager)
                profile = "CPTManager";
            else if (profileId == mss)
                profile = "MSS Engineer";
            else if (profileId == nte)
                profile = "NTE";
            else if (profileId == pm)
                profile = "PM";
            else if (profileId == crm)
                profile = "CPTCRM";

            return profile;
        }

        private string GetSystemNotes(bool isAutoAssign, Cpt cpt)
        {
            string userName = string.Empty;
            StringBuilder temp = new StringBuilder();

            Cpt oldCpt = _repo.GetById(cpt.CptId);

            if (isAutoAssign)
            {
                if (cpt.CptUserAsmt != null && cpt.CptUserAsmt.Count > 0)
                {
                    temp.Append("Auto Assignment: ");
                    foreach (CptUserAsmt user in cpt.CptUserAsmt)
                    {
                        LkUser usr = user.CptUser != null ? user.CptUser : _usrRepo.GetById(user.CptUserId);
                        userName = usr.FullNme.Trim();
                        userName = userName.Contains("[") ? userName.Substring(0, userName.LastIndexOf('[')) : userName;
                        temp.AppendLine(string.Format("{0} assigned as {1}. ", userName, GetProfileName(user.UsrPrfId.GetValueOrDefault())));
                    }
                }
            }
            else
            {
                foreach (CptUserAsmt user in cpt.CptUserAsmt)
                {
                    LkUser usr = user.CptUser != null ? user.CptUser : _usrRepo.GetById(user.CptUserId);

                    // Generate System Notes when assignment was updated.
                    CptUserAsmt oldUser = oldCpt.CptUserAsmt.SingleOrDefault(i => 
                        i.UsrPrfId == user.UsrPrfId && i.RecStusId == (byte)ERecStatus.Active);
                    userName = usr.FullNme.Trim();
                    userName = userName.Contains("[") ? userName.Substring(0, userName.LastIndexOf('[')) : userName;

                    if (oldUser != null)
                    {
                        // Put to notes if assignment was change
                        if (!oldUser.CptUserId.Equals(user.CptUserId))
                        {
                            temp.AppendLine(string.Format("{0} was changed to {1} for {2}.",
                                    oldUser.CptUser.FullNme, userName,
                                    GetProfileName(user.UsrPrfId.GetValueOrDefault())));
                        }
                    }
                    else
                    {
                        // Generate System Notes for new assignment
                        temp.AppendLine(string.Format("{0} assigned as {1}. ", userName,
                            GetProfileName(user.UsrPrfId.GetValueOrDefault())));
                    }
                }
            }

            return temp.ToString();
        }

        #endregion CPT Assignment

        private bool CheckProvisioningCompletion(Cpt cpt)
        {
            // SPS does not have Provisioning Section
            if (cpt.CptRltdInfo != null && cpt.CptRltdInfo.Count() == 1
                && cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.Sps) != null)
                return true;

            // MVS/E2E does not have Assignment Section, once Shortname is given it is complete
            if (cpt.CptRltdInfo != null && cpt.CptRltdInfo.Count() == 1
                && (cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.UnmanagedE2e) != null
                || cpt.CptRltdInfo.SingleOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedVoice) != null))
            {
                if (string.IsNullOrWhiteSpace(cpt.CustShrtNme))
                    return false;
                else
                    return true;
            }

            // Returns false if customer shortname is not yet provided
            if (string.IsNullOrWhiteSpace(cpt.CustShrtNme))
                return false;

            // Returns false if one of the IP address is not yet provided
            bool isNotify = (cpt.TacacsCd
                            || cpt.CptPrimWan.FirstOrDefault(i => i.CptPlnSrvcTierId != (int)CptPlnSrvcTier.WholesaleCarrier
                                    && i.CptPrimScndyTprtId != (int)CptTransportType.IPSprintLink
                                    && i.CptPrimScndyTprtId != (int)CptTransportType.WirelessWANWDLS) != null);

            if (cpt.CptRltdInfo.FirstOrDefault(i =>
                        i.CptCustTypeId == (int)CptCustomerType.ManagedData
                        && i.CptPlnSrvcTierId == (int)CptPlnSrvcTier.MdsComplete
                        && i.SdwanCustCd.GetValueOrDefault(false)) != null)
                isNotify = false;

            if (isNotify && (string.IsNullOrWhiteSpace(cpt.SubnetIpAdr)
                || string.IsNullOrWhiteSpace(cpt.StartIpAdr) || string.IsNullOrWhiteSpace(cpt.EndIpAdr)))
                return false;

            // Checks provision type status for needed provisioning type
            if (cpt.CptPrvsn != null && cpt.CptPrvsn.Count() > 0)
            {
                foreach (CptPrvsn item in cpt.CptPrvsn)
                {
                    if (item.PrvsnStusCd.GetValueOrDefault() && item.RecStusId != (short)CptProvisionStatus.Success)
                        return false;
                }
            }

            return true;
        }

        private bool CreateRedesign(Cpt cpt)
        {
            // Create redesign only for Managed Data and Managed Service Customer Type
            if (cpt.CptRltdInfo != null && cpt.CptRltdInfo.Count > 0
                && (cpt.CptRltdInfo.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedData) != null
                    || cpt.CptRltdInfo.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedSecurity) != null))
            {
                // Check if redesign exist
                bool isRedesignExist = _repo.IsRedesignExist(cpt.CptId, cpt.CustShrtNme);

                // RedesignCategory = 1 for Voice, 2 for Data & 3 for Security
                // 2 = for MDS Data Customer Type CPT Request or both Managed Data and Managed Service
                // 3 = for Managed Service Customer Type CPT Request
                string redesignCategory = "2"; // Default to Data for now
                if (cpt.CptRltdInfo.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedData) != null)
                    redesignCategory = "2";
                else if (cpt.CptRltdInfo.FirstOrDefault(i => i.CptCustTypeId == (short)CptCustomerType.ManagedSecurity) != null)
                    redesignCategory = "3";

                if (!isRedesignExist)
                {
                    string redesignNumber = _repo.CreateRedesign(cpt.CustShrtNme,
                        cpt.H1, redesignCategory, cpt.CptId);

                    if (!string.IsNullOrWhiteSpace(redesignNumber))
                    {
                        // Save on CPT History
                        CreateCptHistory((byte)Actions.CPTRedesignCreated,
                            string.Format("Successfully created redesign - {0}.", redesignNumber, true),
                            cpt.CptId);
                    }
                    else
                        return false;
                }
            }

            return true;
        }

        #region Send/Save Email Request

        private EmailReq SaveEmailRequest(bool isCreated, int cptId, string subj, string body, string sendTo, int emailRequestType)
        {
            EmailReq email = new EmailReq();
            email.EmailReqTypeId = emailRequestType;
            email.StusId = (short)EmailStatus.Pending;
            email.EmailListTxt = sendTo;
            //email.EmailListTxt = "sarah.sandoval@sprint.com";
            email.EmailSubjTxt = subj;
            email.EmailBodyTxt = body;
            email.CptId = cptId;
            email.CreatDt = DateTime.Now;

            // Added to prevent the error on calling twice Email_Req Create
            if (!isCreated)
                _emailReqRepo.Create(email);

            return email;
        }

        private EmailReq SaveEmailRequestForCPTNotification(bool isCreated, string notes,
            string subject, string emailTo, string emailMessage, Cpt cpt)
        {
            string viewLink = string.Format(@"{0}{1}",
                _config.GetSection("AppSettings:ViewCPTURL").Value, cpt.CptId);

            string updatedBy = isCreated ? string.Empty : _loggedInUser.GetLoggedInUser().FullNme;

            string body = CreateXMLCPTEmailNotification(cpt, viewLink, updatedBy, emailMessage, notes);
            return SaveEmailRequest(isCreated, cpt.CptId, subject, body, emailTo.TrimEnd(','), (int)EmailREQType.CPTNotificationEmail);
        }

        private string CreateXMLCPTEmailNotification(Cpt cpt, string viewLink,
            string updatedBy, string emailMessage, string notes)
        {
            Cpt oldCpt = _repo.GetById(cpt.CptId);
            string header = string.Empty;
            string isResourceNotification = string.Empty;

            if (cpt.CptUserAsmt != null && cpt.CptUserAsmt.Count > 0)
            {
                header = "Resource Assignment Notification Automatic Email Notice";
                isResourceNotification = "Yes";
            }
            else
                header = "Automatic Email Notice";

            // Get Assignment Changes for CPT
            string oldGK = string.Empty; string oldManager = string.Empty;
            string oldMSS = string.Empty; string oldNTE = string.Empty;
            string oldPM = string.Empty; string oldCRM = string.Empty; string isChanged = string.Empty;
            if (oldCpt != null && oldCpt.CptUserAsmt != null && oldCpt.CptUserAsmt.Count > 0)
            {
                // GK PRFID = 212
                var oldGtkpr = oldCpt.CptUserAsmt
                        .FirstOrDefault(i => i.UsrPrfId == gk && i.RecStusId == (byte)ERecStatus.Active);
                var newGtkpr = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == gk 
                        && i.RecStusId == (byte)ERecStatus.Active);
                if (oldGtkpr != null && newGtkpr != null && oldGtkpr.CptUserId != newGtkpr.CptUserId)
                {
                    oldGK = oldGtkpr.CptUser.FullNme;
                    oldGK = oldGK.Contains("[") ? oldGK.Substring(0, oldGK.LastIndexOf('[')) : oldGK;
                    oldGK = oldGK.Trim();
                    isChanged = "Yes";
                }

                // Manager PRFID = 211
                var oldMngr = oldCpt.CptUserAsmt
                        .FirstOrDefault(i => i.UsrPrfId == manager && i.RecStusId == (byte)ERecStatus.Active);
                var newMngr = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == manager 
                        && i.RecStusId == (byte)ERecStatus.Active);
                if (oldMngr != null && newMngr != null && oldMngr.CptUserId != newMngr.CptUserId)
                {
                    oldManager = oldMngr.CptUser.FullNme;
                    oldManager = oldManager.Contains("[") ? oldManager.Substring(0, oldManager.LastIndexOf('[')) : oldManager;
                    oldManager = oldManager.Trim();
                    isChanged = "Yes";
                }

                // SSE PRFID = 139
                var oldSse = oldCpt.CptUserAsmt
                        .FirstOrDefault(i => i.UsrPrfId == mss && i.RecStusId == (byte)ERecStatus.Active);
                var newSse = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == mss 
                        && i.RecStusId == (byte)ERecStatus.Active);
                if (oldSse != null && newSse != null && oldSse.CptUserId != newSse.CptUserId)
                {
                    oldMSS = oldSse.CptUser.FullNme;
                    oldMSS = oldMSS.Contains("[") ? oldMSS.Substring(0, oldMSS.LastIndexOf('[')) : oldMSS;
                    oldMSS = oldMSS.Trim();
                    isChanged = "Yes";
                }

                // NTE/Event Activator PRFID = 130
                var oldNte = oldCpt.CptUserAsmt
                        .FirstOrDefault(i => i.UsrPrfId == nte && i.RecStusId == (byte)ERecStatus.Active);
                var newNte = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == nte 
                        && i.RecStusId == (byte)ERecStatus.Active);
                if (oldNte != null && newNte != null && oldNte.CptUserId != newNte.CptUserId)
                {
                    oldNTE = oldNte.CptUser.FullNme;
                    oldNTE = oldNTE.Contains("[") ? oldNTE.Substring(0, oldNTE.LastIndexOf('[')) : oldNTE;
                    oldNTE = oldNTE.Trim();
                    isChanged = "Yes";
                }

                // PM/Event Reviewer PRFID = 132
                var oldPm = oldCpt.CptUserAsmt
                        .FirstOrDefault(i => i.UsrPrfId == pm && i.RecStusId == (byte)ERecStatus.Active);
                var newPm = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == pm 
                        && i.RecStusId == (byte)ERecStatus.Active);
                if (oldPm != null && newPm != null && oldPm.CptUserId != newPm.CptUserId)
                {
                    oldPM = oldPm.CptUser.FullNme;
                    oldPM = oldPM.Contains("[") ? oldPM.Substring(0, oldPM.LastIndexOf('[')) : oldPM;
                    oldPM = oldPM.Trim();
                    isChanged = "Yes";
                }

                // CRM PRFID = 213
                var oldCrm = oldCpt.CptUserAsmt
                        .FirstOrDefault(i => i.UsrPrfId == crm && i.RecStusId == (byte)ERecStatus.Active);
                var newCrm = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == crm 
                        && i.RecStusId == (byte)ERecStatus.Active);
                if (oldCrm != null && newCrm != null && oldCrm.CptUserId != newCrm.CptUserId)
                {
                    oldCRM = oldCrm.CptUser.FullNme;
                    oldCRM = oldCRM.Contains("[") ? oldCRM.Substring(0, oldCRM.LastIndexOf('[')) : oldCRM;
                    oldCRM = oldCRM.Trim();
                    isChanged = "Yes";
                }
            }

            // Get Site Assignment Information
            List<string> primesites = new List<string>();
            if (cpt.CptPrimSite != null && cpt.CptPrimSite.Count > 0)
            {
                foreach (int item in cpt.CptPrimSite.Select(i => i.PrimSiteId))
                {
                    if (item == (int)Entities.Enums.CptPrimSite.FMNS)
                        primesites.Add("FMNS");
                    else if (item == (int)Entities.Enums.CptPrimSite.IAAS)
                        primesites.Add("IAAS");
                    else if (item == (int)Entities.Enums.CptPrimSite.MNSD)
                        primesites.Add("MNSD");
                    else if (item == (int)Entities.Enums.CptPrimSite.MNSW)
                        primesites.Add("MNSW");
                    else if (item == (int)Entities.Enums.CptPrimSite.MVSC)
                        primesites.Add("MVSC");
                }
            }

            List<string> primaryTransports = new List<string>();
            List<string> secondaryTransports = new List<string>();
            List<CptRelatedInfoViewModel> rltdInfos =
                    _mapper.Map<IEnumerable<CptRelatedInfoViewModel>>(cpt.CptRltdInfo).ToList();
            if (rltdInfos != null && rltdInfos.Count > 0)
            {
                foreach (CptRelatedInfoViewModel rltdInfo in rltdInfos)
                {
                    List<CptPrimWanViewModel> pwan = 
                        _mapper.Map<IEnumerable<CptPrimWanViewModel>>(cpt.CptPrimWan
                            .Where(i => i.CptPlnSrvcTierId == rltdInfo.CptPlnSrvcTierId)).ToList();
                    if (pwan != null && pwan.Count() > 0)
                    {
                        foreach (CptPrimWanViewModel item in pwan)
                        {
                            primaryTransports.Add(GetTransportTypeName(item.CptPrimScndyTprtId));
                        }
                    }

                    List<int> swan = cpt.CptScndyTprt
                                            .Where(i => i.CptPlnSrvcTierId == rltdInfo.CptPlnSrvcTierId)
                                            .Select(i => (int)i.CptPrimScndyTprtId).ToList();
                    if (swan != null && swan.Count() > 0)
                    {
                        foreach (int item in swan)
                        {
                            secondaryTransports.Add(GetTransportTypeName(item));
                        }
                    }
                }
            }

            var ipms = _contctRepo.Find(i => i.ObjId == cpt.CptId && i.ObjTypCd == "C" && i.RecStusId == (short)ERecStatus.Active
                                    && i.RoleId == 99).Select(i => i.EmailAdr).ToList();

            var ams = _contctRepo.Find(i => i.ObjId == cpt.CptId && i.ObjTypCd == "C" && i.RecStusId == (short)ERecStatus.Active
                                    && i.RoleId == 100).Select(i => i.EmailAdr).ToList();

            // General CPT Info details
            XElement _cptEmail = new XElement("CPTEmail");
            XElement xe = new XElement("CPTInfo",
                new XElement("CPTID", cpt.CptId),
                new XElement("CPTNumber", cpt.CptCustNbr),
                new XElement("CPTViewLink", viewLink),
                new XElement("CustomerName", cpt.CsgLvlId > 0 ? CustomerMaskedData.customerName : cpt.CompnyNme),
                new XElement("H1", cpt.H1),
                new XElement("NoOfDevices", cpt.DevCnt),
                new XElement("SEEmail", cpt.SeEmailAdr),
                new XElement("AMEmail", (ams != null) ? string.Join(",", ams) : cpt.AmEmailAdr),
                new XElement("IPMEmail", (ipms != null) ? string.Join(",", ipms) : cpt.IpmEmailAdr),
                new XElement("SubmittedBy", _usrRepo.GetById(cpt.SubmtrUserId).EmailAdr),
                new XElement("UpdatedBy", updatedBy),
                new XElement("EmailMessage", emailMessage),
                new XElement("Changes", isChanged),
                new XElement("EmailHeader", header),
                
                // For Additional Information
                new XElement("IsHostedManagedService", cpt.HstMgndSrvcCd ? "Yes" : "No"),
                new XElement("IsEncryptedModem", cpt.EncMdmCd ? "Yes" : "No"),
                new XElement("IsTacacs", cpt.TacacsCd ? "Yes" : "No"),
                new XElement("IsSdwan", cpt.SdwanCd ? "Yes" : "No"),
                new XElement("IsSdwanCustomer", (cpt.CptRltdInfo.FirstOrDefault(i => i.SdwanCustCd.GetValueOrDefault()) != null) ? "Yes" : "No"),
                
                // For Site Assignment Information
                new XElement("IsResourceAssignment", isResourceNotification),
                new XElement("PrimeSite", primesites.Count > 0 ? string.Join(",", primesites) : string.Empty),
                new XElement("GovtCustomer", cpt.GovtCustCd.GetValueOrDefault() ? "Yes" : "No"),
                new XElement("PrimaryTransport", primaryTransports.Count > 0 ? string.Join(",", primaryTransports.Distinct()) : string.Empty),
                new XElement("SecondaryTransport", secondaryTransports.Count > 0 ? string.Join(",", secondaryTransports.Distinct()) : string.Empty));
            _cptEmail.Add(xe);

            // CPT Related Info - Customer Type, Service Tier, NDD Link
            if (rltdInfos != null && rltdInfos.Count > 0)
            {
                string customerType = string.Empty;
                List<string> customerTypes = new List<string>();
                string mdsServiceTier = string.Empty;
                string nddLink = string.Empty;
                bool isSdwanCust = false;

                foreach (CptRelatedInfoViewModel item in rltdInfos)
                {
                    if (item.CptCustTypeId == (short)CptCustomerType.ManagedData)
                        customerTypes.Add("Managed Data");
                    else if (item.CptCustTypeId == (short)CptCustomerType.ManagedSecurity)
                        customerTypes.Add("Managed Service");
                    else if (item.CptCustTypeId == (short)CptCustomerType.ManagedVoice)
                        customerTypes.Add("Managed Voice");
                    else if (item.CptCustTypeId == (short)CptCustomerType.UnmanagedE2e)
                        customerTypes.Add("Unmanaged E2E");
                    else if (item.CptCustTypeId == (short)CptCustomerType.Sps)
                        customerTypes.Add("Professional Services");

                    if (item.CptPlnSrvcTierId != 0)
                    {
                        if (item.CptPlnSrvcTierId == (short)CptPlnSrvcTier.Mss)
                            mdsServiceTier += "MSS, ";
                        else if (item.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsComplete)
                            mdsServiceTier += "MDS Complete, ";
                        else if (item.CptPlnSrvcTierId == (short)CptPlnSrvcTier.MdsSupport)
                            mdsServiceTier += "MDS Support, ";
                        else if (item.CptPlnSrvcTierId == (short)CptPlnSrvcTier.WholesaleCarrier)
                            mdsServiceTier += "Wholesale Carrier, ";
                        else if (item.CptPlnSrvcTierId == (short)CptPlnSrvcTier.WholesaleVar)
                            mdsServiceTier += "Wholesale Var";
                    }

                    nddLink += item.NtwkDsgnDocTxt + ", ";
                    isSdwanCust = item.SdwanCustCd.GetValueOrDefault();
                }

                nddLink = nddLink.Trim(new Char[] { ' ', ',' });
                customerType = string.Join(", ", customerTypes.Distinct());
                mdsServiceTier = mdsServiceTier.Trim(new Char[] { ' ', ',' });
                XElement xa = new XElement("CPTRelatedInfo",
                new XElement("CustomerType", customerType),
                new XElement("ServiceTier", mdsServiceTier),
                new XElement("NDDLink", nddLink),
                new XElement("Shortname", cpt.CustShrtNme),
                new XElement("Description", notes));
                _cptEmail.Add(xa);
            }

            // CPT User Assignment
            if (cpt.CptUserAsmt != null && cpt.CptUserAsmt.Count > 0)
            {
                string roleName = string.Empty;
                string userName = string.Empty;

                foreach (CptUserAsmt item in cpt.CptUserAsmt)
                {
                    LkUser usr = item.CptUser != null ? item.CptUser : _usrRepo.GetById(item.CptUserId);
                    userName = usr.FullNme;
                    userName = userName.Contains("[") ? userName.Substring(0, userName.LastIndexOf('[')) : userName;
                    userName = userName.Trim();

                    if (item.UsrPrfId == gk || item.RoleId == 152)
                    {
                        if (string.IsNullOrWhiteSpace(oldGK) || userName.Equals(oldGK))
                            roleName = "Gatekeeper";
                        else
                        {
                            roleName = "NewGatekeeper";
                            XElement x1 = new XElement("Gatekeeper",
                                new XElement("UserName", oldGK));

                            _cptEmail.Add(x1);
                        }
                    }
                    else if (item.UsrPrfId == manager || item.RoleId == 151)
                    {
                        if (string.IsNullOrWhiteSpace(oldManager) || userName.Equals(oldManager))
                            roleName = "Manager";
                        else
                        {
                            roleName = "NewManager";
                            XElement x1 = new XElement("Manager",
                                new XElement("UserName", oldManager));

                            _cptEmail.Add(x1);
                        }
                    }
                    else if (item.UsrPrfId == mss || item.RoleId == 82)
                    {
                        if (string.IsNullOrWhiteSpace(oldMSS) || userName.Equals(oldMSS))
                            roleName = "MSS";
                        else
                        {
                            roleName = "NewMSS";
                            XElement x1 = new XElement("MSS",
                                new XElement("UserName", oldManager));

                            _cptEmail.Add(x1);
                        }
                    }
                    else if (item.UsrPrfId == nte || item.RoleId == 23)
                    {
                        if (string.IsNullOrWhiteSpace(oldNTE) || userName.Equals(oldNTE))
                            roleName = "NTE";
                        else
                        {
                            roleName = "NewNTE";
                            XElement x1 = new XElement("NTE",
                                new XElement("UserName", oldNTE));

                            _cptEmail.Add(x1);
                        }
                    }
                    else if (item.UsrPrfId == pm || item.RoleId == 22)
                    {
                        if (string.IsNullOrWhiteSpace(oldPM) || userName.Equals(oldPM))
                            roleName = "PM";
                        else
                        {
                            roleName = "NewPM";
                            XElement x1 = new XElement("PM",
                                new XElement("UserName", oldPM));

                            _cptEmail.Add(x1);
                        }
                    }
                    else if (item.UsrPrfId == crm || item.RoleId == 153)
                    {
                        if (string.IsNullOrWhiteSpace(oldCRM) || userName.Equals(oldCRM))
                            roleName = "CRM";
                        else
                        {
                            roleName = "NewCRM";
                            XElement x1 = new XElement("CRM",
                                new XElement("UserName", oldCRM));

                            _cptEmail.Add(x1);
                        }
                    }

                    XElement xb = new XElement(roleName,
                        new XElement("UserID", item.CptUserId),
                        new XElement("UserName", userName.Trim()),
                        new XElement("Email", usr.EmailAdr));

                    _cptEmail.Add(xb);
                }
            }

            return _cptEmail.ToString();
        }

        private string GetChangeUserAssignmentEmails(Cpt cpt, CptViewModel model)
        {
            List<string> emailAddrs = new List<string>();

            // If assign users were changed, old and new assignee is included on the email notification
            if (cpt.CptUserAsmt != null)
            {
                if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == gk && i.RecStusId == (byte)ERecStatus.Active))
                {
                    var gkUser = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == gk && i.RecStusId == (byte)ERecStatus.Active).CptUser;
                    if (model.Gatekeeper.HasValue && !gkUser.UserId.Equals(model.Gatekeeper.Value))
                    {
                        var oldGkUser = cpt.CptUserAsmt.OrderByDescending(i => i.CreatDt)
                            .FirstOrDefault(i => i.UsrPrfId == gk && i.RecStusId == (byte)ERecStatus.InActive).CptUser;
                        emailAddrs.Add(gkUser.EmailAdr);
                        emailAddrs.Add(oldGkUser.EmailAdr);
                    }
                }

                if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == manager && i.RecStusId == (byte)ERecStatus.Active))
                {
                    var managerUser = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == manager && i.RecStusId == (byte)ERecStatus.Active).CptUser;
                    if (model.Manager.HasValue && !managerUser.UserId.Equals(model.Manager.Value))
                    {
                        var oldmanagerUser = cpt.CptUserAsmt.OrderByDescending(i => i.CreatDt)
                            .FirstOrDefault(i => i.UsrPrfId == manager && i.RecStusId == (byte)ERecStatus.InActive).CptUser;
                        emailAddrs.Add(managerUser.EmailAdr);
                        emailAddrs.Add(oldmanagerUser.EmailAdr);
                    }
                }

                if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == mss && i.RecStusId == (byte)ERecStatus.Active))
                {
                    var sseUser = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == mss && i.RecStusId == (byte)ERecStatus.Active).CptUser;
                    if (model.Mss.HasValue && !sseUser.UserId.Equals(model.Mss.Value))
                    {
                        var oldSseUser = cpt.CptUserAsmt.OrderByDescending(i => i.CreatDt)
                            .FirstOrDefault(i => i.UsrPrfId == mss && i.RecStusId == (byte)ERecStatus.InActive).CptUser;
                        emailAddrs.Add(sseUser.EmailAdr);
                        emailAddrs.Add(oldSseUser.EmailAdr);
                    }
                }

                // Resource Notification for NTE, PM and CRM will be send only once all three are assigned
                if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == nte && i.RecStusId == (byte)ERecStatus.Active)
                    && cpt.CptUserAsmt.Any(i => i.UsrPrfId == pm && i.RecStusId == (byte)ERecStatus.Active)
                    || (cpt.CptRltdInfo.Any(i => i.CptCustTypeId == (short)CptCustomerType.ManagedData)
                        && cpt.CptUserAsmt.Any(i => i.UsrPrfId == crm && i.RecStusId == (byte)ERecStatus.Active)))
                {
                    // Send notification to old and new assignee if change NTE
                    if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == nte && i.RecStusId == (byte)ERecStatus.Active))
                    {
                        var nteUser = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == nte && i.RecStusId == (byte)ERecStatus.Active).CptUser;
                        if (model.Nte.HasValue && !nteUser.UserId.Equals(model.Nte.Value))
                        {
                            var oldNteUser = cpt.CptUserAsmt.OrderByDescending(i => i.CreatDt)
                                .FirstOrDefault(i => i.UsrPrfId == nte && i.RecStusId == (byte)ERecStatus.InActive).CptUser;
                            emailAddrs.Add(nteUser.EmailAdr);
                            emailAddrs.Add(oldNteUser.EmailAdr);
                        }
                    }

                    // Send notification to old and new assignee if change PM
                    if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == pm && i.RecStusId == (byte)ERecStatus.Active))
                    {
                        var pmUser = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == pm && i.RecStusId == (byte)ERecStatus.Active).CptUser;
                        if (model.Pm.HasValue && !pmUser.UserId.Equals(model.Pm.Value))
                        {
                            var oldPmUser = cpt.CptUserAsmt.OrderByDescending(i => i.CreatDt)
                                .FirstOrDefault(i => i.UsrPrfId == pm && i.RecStusId == (byte)ERecStatus.InActive).CptUser;
                            emailAddrs.Add(pmUser.EmailAdr);
                            emailAddrs.Add(oldPmUser.EmailAdr);
                        }
                    }

                    // Send notification to old and new assignee if change CRM
                    if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == crm && i.RecStusId == (byte)ERecStatus.Active))
                    {
                        var crmUser = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == crm && i.RecStusId == (byte)ERecStatus.Active).CptUser;
                        if (model.Pm.HasValue && !crmUser.UserId.Equals(model.Pm.Value))
                        {
                            var oldCrmUser = cpt.CptUserAsmt.OrderByDescending(i => i.CreatDt)
                                .FirstOrDefault(i => i.UsrPrfId == crm && i.RecStusId == (byte)ERecStatus.InActive).CptUser;
                            emailAddrs.Add(crmUser.EmailAdr);
                            emailAddrs.Add(oldCrmUser.EmailAdr);
                        }
                    }
                }
            }

            return emailAddrs.Count > 0 ? String.Join(",", emailAddrs.Where(i => !string.IsNullOrEmpty(i))) : string.Empty;
        }

        private EmailReq SetEmailAssignmentNotification(Cpt cpt, Cpt oldCpt)
        {
            bool isChanged = false;
            List<string> emailTos = new List<string>();

            // Send GK Email Resource Notification
            if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == gk))
            {
                var gkId = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == gk
                    && i.RecStusId == (byte)ERecStatus.Active).CptUserId;
                var gkUser = _usrRepo.GetById(gkId);
                if (oldCpt.CptUserAsmt.Any(i => i.UsrPrfId == gk))
                {
                    // If GK has been changed
                    var oldGk = oldCpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == gk 
                                        && i.RecStusId == (byte)ERecStatus.Active);
                    if (oldGk != null && oldGk.CptUserId != gkId)
                    {
                        isChanged = true;
                        emailTos.Add(gkUser.EmailAdr);
                        emailTos.Add(oldGk.CptUser.EmailAdr);
                    }
                }
                else
                {
                    // For initial assignment
                    emailTos.Add(gkUser.EmailAdr);
                }
            }

            // Send Manager Email Resource Notification
            if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == manager))
            {
                var managerId = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == manager
                    && i.RecStusId == (byte)ERecStatus.Active).CptUserId;
                var managerUser = _usrRepo.GetById(managerId);
                if (oldCpt.CptUserAsmt.Any(i => i.UsrPrfId == manager))
                {
                    // If Manager has been changed
                    var oldManager = oldCpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == manager
                                            && i.RecStusId == (byte)ERecStatus.Active);
                    if (oldManager != null && oldManager.CptUserId != managerId)
                    {
                        isChanged = true;
                        emailTos.Add(managerUser.EmailAdr);
                        emailTos.Add(oldManager.CptUser.EmailAdr);
                    }
                }
                else
                {
                    // For initial assignment
                    emailTos.Add(managerUser.EmailAdr);
                }
            }

            // Send MSS(SSE) Email Resource Notification
            if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == mss))
            {
                var mssId = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == mss
                    && i.RecStusId == (byte)ERecStatus.Active).CptUserId;
                var mssUser = _usrRepo.GetById(mssId);
                if (oldCpt.CptUserAsmt.Any(i => i.UsrPrfId == mss))
                {
                    // If MSS(SSE) has been changed
                    var oldMss = oldCpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == mss 
                                        && i.RecStusId == (byte)ERecStatus.Active);
                    if (oldMss != null && oldMss.CptUserId != mssId)
                    {
                        isChanged = true;
                        emailTos.Add(mssUser.EmailAdr);
                        emailTos.Add(oldMss.CptUser.EmailAdr);
                    }
                }
                else
                {
                    // For initial assignment
                    emailTos.Add(mssUser.EmailAdr);
                }
            }

            // Send 1 Email Notification for NTE/PM/CRM
            if (cpt.CptUserAsmt.Any(i => i.UsrPrfId == nte)
                && cpt.CptUserAsmt.Any(i => i.UsrPrfId == pm)
                && cpt.CptUserAsmt.Any(i => i.UsrPrfId == crm))
            {
                var nteId = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == nte
                    && i.RecStusId == (byte)ERecStatus.Active).CptUserId;
                var nteUser = _usrRepo.GetById(nteId);

                var pmId = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == pm
                        && i.RecStusId == (byte)ERecStatus.Active).CptUserId;
                var pmUser = _usrRepo.GetById(pmId);

                var crmId = cpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == crm
                        && i.RecStusId == (byte)ERecStatus.Active).CptUserId;
                var crmUser = _usrRepo.GetById(crmId);

                // If NTE has been changed
                var oldNte = oldCpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == nte
                                    && i.RecStusId == (byte)ERecStatus.Active);
                if (oldNte != null && oldNte.CptUserId != nteId)
                {
                    isChanged = true;
                    emailTos.Add(nteUser.EmailAdr);
                    emailTos.Add(oldNte.CptUser.EmailAdr);
                }

                // If PM has been changed
                var oldPm = oldCpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == pm
                                    && i.RecStusId == (byte)ERecStatus.Active);
                if (oldPm != null && oldPm.CptUserId != pmId)
                {
                    isChanged = true;
                    emailTos.Add(pmUser.EmailAdr);
                    emailTos.Add(oldPm.CptUser.EmailAdr);
                }

                // If CRM has been changed
                var oldCrm = oldCpt.CptUserAsmt.FirstOrDefault(i => i.UsrPrfId == crm
                                    && i.RecStusId == (byte)ERecStatus.Active);
                if (oldCrm != null && oldCrm.CptUserId != crmId)
                {
                    isChanged = true;
                    emailTos.Add(crmUser.EmailAdr);
                    emailTos.Add(oldCrm.CptUser.EmailAdr);
                }

                // Send final notification only once NTE/PM/CRM is supplied
                // and that would mean that atleast one assigment has no data on DB
                if (!isChanged
                    && (!oldCpt.CptUserAsmt.Any(i => i.UsrPrfId == nte)
                        || !oldCpt.CptUserAsmt.Any(i => i.UsrPrfId == pm)
                        || !oldCpt.CptUserAsmt.Any(i => i.UsrPrfId == crm)))
                {
                    // Site PDL is sent only on last resource email notification
                    emailTos.Add(_commonRepo.GetCowsAppCfgValue("CPT Resource Assignment"));
                    emailTos.Add(nteUser.EmailAdr);
                    emailTos.Add(pmUser.EmailAdr);
                    emailTos.Add(crmUser.EmailAdr);
                }
            }

            // Added by Sarah Sandoval [20210616] - Include Contact Details and additional contact email
            emailTos.AddRange(cpt.CptRltdInfo.Select(i => i.CntctEmailList).Distinct());
            emailTos.AddRange(_contctRepo.GetEmailAddresses(cpt.CptId, "C", ((int)CptStatus.Assigned).ToString()));

            if (emailTos != null && emailTos.Count > 0)
            {
                if (isChanged)
                {
                    // Add Email Address of the updater along with the former and new assignees
                    emailTos.Add(_loggedInUser.GetLoggedInUser().EmailAdr);
                }

                return SaveEmailAssignmentNotification(cpt, isChanged, string.Join(",", emailTos.Where(i => !string.IsNullOrEmpty(i))));
            }

            return null;
        }

        private EmailReq SaveEmailAssignmentNotification(Cpt cpt, bool isChanged, string emailTo)
        {
            var companyName = cpt.CsgLvlId > 0 ? CustomerMaskedData.customerName : cpt.CompnyNme;
            string intialSubject = "CPT Resource Assignment Notification - " +
                (string.IsNullOrWhiteSpace(cpt.CustShrtNme) ? companyName : cpt.CustShrtNme)
                + " - Assignment Type: Initial; Customer Name: " + companyName;
            string updatedSubject = "Changes To Existing Assignment - " +
                (string.IsNullOrWhiteSpace(cpt.CustShrtNme) ? companyName : cpt.CustShrtNme)
                + " - Assignment Type: Activity Reassignment; Customer Name: " + companyName;

            string subject = isChanged ? updatedSubject : intialSubject;
            string emailMessage = "You have been assigned to this CPT.";

            return SaveEmailRequestForCPTNotification(true, string.Empty, subject, emailTo, emailMessage, cpt);
        }

        private void SaveEmailReturnedNotice(Cpt cpt)
        {
            // Get Gatekeeper Email
            string gkEmail = string.Empty;
            if (cpt.CptUserAsmt != null && cpt.CptUserAsmt.Count() > 0
                && cpt.CptUserAsmt.SingleOrDefault(i => i.UsrPrfId == gk && i.RecStusId == (byte)ERecStatus.Active) != null)
            {
                gkEmail = cpt.CptUserAsmt.SingleOrDefault(i => i.UsrPrfId == gk && i.RecStusId == (byte)ERecStatus.Active).CptUser.EmailAdr;
            }

            // Updated by Sarah Sandoval [20210616] - Removed AM, IPM, SE Email and include Contact Details and additional contact email
            List<string> emailaddrs = new List<string>();
            emailaddrs.Add(cpt.SubmtrUser.EmailAdr);
            //emailaddrs.Add(cpt.AmEmailAdr);
            //emailaddrs.Add(cpt.IpmEmailAdr);
            //emailaddrs.Add(cpt.SeEmailAdr);
            emailaddrs.Add(gkEmail);
            emailaddrs.AddRange(cpt.CptRltdInfo.Select(i => i.CntctEmailList).Distinct());
            emailaddrs.AddRange(_contctRepo.GetEmailAddresses(cpt.CptId, "C", ((int)CptStatus.ReturnedToSDE).ToString()));

            var companyName = cpt.CsgLvlId > 0 ? CustomerMaskedData.customerName : cpt.CompnyNme;
            string emailTo = string.Join(",", emailaddrs.Where(i => !string.IsNullOrEmpty(i)));
            string subject = string.Format("{0} has been Returned; Customer Name: {1}",
                cpt.CptCustNbr, companyName);

            string emailMessage = string.Format("You have been notified that this CPT request has been returned.");
            SaveEmailRequestForCPTNotification(false, string.Empty, subject, emailTo, emailMessage, cpt);

            // Update CPT History
            var hist = CreateCptHistory((byte)Actions.CPTReturnedToSDE,
                "Returned to SDE notification was sent to " + emailTo, cpt.CptId, true);
            _repo.CreateCptHistory(hist);
        }

        private CptHist SaveEmailCancelationNotice(Cpt cpt)
        {
            string provTeam = _commonRepo.GetCowsAppCfgValue("CPT ShortName Notification");
            StringBuilder sb = new StringBuilder();

            // Update Status of provisioning grid
            string provType = string.Empty;
            foreach (CptPrvsn prov in cpt.CptPrvsn)
            {
                if (prov.RecStusId == (short)CptProvisionStatus.Pickup ||
                    prov.RecStusId == (short)CptProvisionStatus.Success)
                {
                    if (prov.CptPrvsnTypeId == (short)CptProvision.Qip)
                        provType += "QIP, ";
                    else if (prov.CptPrvsnTypeId == (short)CptProvision.Spectrum)
                        provType += "Spectrum, ";
                    else if (prov.CptPrvsnTypeId == (short)CptProvision.Tacacs)
                        provType += "TACACS, ";
                    else if (prov.CptPrvsnTypeId == (short)CptProvision.Voyence)
                        provType += "Voyancy, ";
                }
            }

            // Updated by Sarah Sandoval [20210616] - Removed AM Email and include Contact Details and additional contact email
            // Notification will be sent to Submitter, Gatekeeper, Provisioning PDL, and AM Email
            string gkEmail = string.Empty;
            if (cpt.CptUserAsmt != null && cpt.CptUserAsmt.Count() > 0
                && cpt.CptUserAsmt.SingleOrDefault(i => i.UsrPrfId == gk && i.RecStusId == (byte)ERecStatus.Active) != null)
            {
                gkEmail = cpt.CptUserAsmt.SingleOrDefault(i => i.UsrPrfId == gk && i.RecStusId == (byte)ERecStatus.Active).CptUser.EmailAdr;
            }
            List<string> emailaddrs = new List<string>();
            emailaddrs.Add(cpt.SubmtrUser.EmailAdr);
            //emailaddrs.Add(cpt.AmEmailAdr);
            emailaddrs.Add(provTeam);
            emailaddrs.Add(gkEmail);
            emailaddrs.AddRange(cpt.CptRltdInfo.Select(i => i.CntctEmailList).Distinct());
            emailaddrs.AddRange(_contctRepo.GetEmailAddresses(cpt.CptId, "C", ((int)CptStatus.Cancelled).ToString()));

            var companyName = cpt.CsgLvlId > 0 ? CustomerMaskedData.customerName : cpt.CompnyNme;
            string emailTo = string.Join(",", emailaddrs.Where(i => !string.IsNullOrEmpty(i)));
            string subject = string.Format("{0} has been Cancelled; Customer Name: {1}",
                cpt.CptCustNbr, companyName);

            if (string.IsNullOrWhiteSpace(cpt.CustShrtNme))
                sb = sb.AppendLine(string.Format("{0} has been cancelled by {1}.", 
                    cpt.CptCustNbr, _loggedInUser.GetLoggedInUser().EmailAdr));
            else
                sb = sb.AppendLine(string.Format("{0} has been cancelled by {1} with shortname {2}.",
                    cpt.CptCustNbr, _loggedInUser.GetLoggedInUser().EmailAdr, cpt.CustShrtNme));

            sb = sb.AppendLine("Please resubmit the CPT request. ");
            sb = sb.AppendLine(string.Format("Provisioning team: {0}.", provTeam));
            sb = sb.AppendLine("Please make sure if the containers are created for the request as undone. ");
            sb = sb.AppendLine(string.Format("For all further information contact {0}.", 
                _loggedInUser.GetLoggedInUser().EmailAdr));

            if (!string.IsNullOrWhiteSpace(provType))
            {
                sb = sb.AppendLine("Containers that are picked up or completed: " + provType.Trim() + ".");
            }

            string emailMessage = sb.ToString();
            //emailTo = "sarah.sandoval@sprint.com, Romeojr.Jamoner@sprint.com"; // For Testing
            SaveEmailRequestForCPTNotification(false, string.Empty, subject, emailTo, emailMessage, cpt);
            
            // Update CPT History
            return CreateCptHistory((byte)Actions.CPTSentCancelNotification,
                "Cancellation email was sent to " + emailTo, cpt.CptId, true);
        }

        private void SaveEmailCompletionNotice(Cpt cpt)
        {
            // Updated by Sarah Sandoval [20210616] - Removed Sprint Sales Team (AM, IPM, SE) and include Contact Details and additional contact email
            // Send email notification to Submitter, Sprint Sales Team (AM, IPM, SE) and to all assigned users informing that CPT is completed
            List<string> emailaddrs = new List<string>();
            //emailaddrs.Add(cpt.AmEmailAdr);
            //emailaddrs.Add(cpt.IpmEmailAdr);
            //emailaddrs.Add(cpt.SeEmailAdr);
            emailaddrs.Add(_usrRepo.GetById(cpt.SubmtrUserId).EmailAdr);
            emailaddrs.Add(_loggedInUser.GetLoggedInUser().EmailAdr);
            if (cpt.CptUserAsmt != null && cpt.CptUserAsmt.Count() > 0)
            {
                foreach (CptUserAsmt user in cpt.CptUserAsmt.Where(i => i.RecStusId == (byte)ERecStatus.Active))
                {
                    emailaddrs.Add(user.CptUser.EmailAdr);
                }
            }

            // If CPE Managed Voice and shortname validated successfully, add this email address for notification
            if (cpt.RevwdPmCd && !string.IsNullOrWhiteSpace(cpt.CustShrtNme)
                && cpt.CptRltdInfo != null && cpt.CptRltdInfo.Count() == 1
                    && cpt.CptRltdInfo.SingleOrDefault(i =>
                        i.CptCustTypeId == (short)CptCustomerType.ManagedVoice) != null)
            {
                emailaddrs.Add(_commonRepo.GetCowsAppCfgValue("CPT Mngd Voice Completion"));
            }

            emailaddrs.AddRange(cpt.CptRltdInfo.Select(i => i.CntctEmailList).Distinct());
            emailaddrs.AddRange(_contctRepo.GetEmailAddresses(cpt.CptId, "C", ((int)CptStatus.Completed).ToString()));

            var companyName = cpt.CsgLvlId > 0 ? CustomerMaskedData.customerName : cpt.CompnyNme;
            string emailTo = string.Join(",", emailaddrs.Where(i => !string.IsNullOrEmpty(i)));
            string subject = string.Format("{0} - {1} has been Completed; Customer Name: {2}",
                cpt.CptCustNbr, cpt.CustShrtNme, companyName);
            string viewLink = string.Format(@"{0}{1}", 
                _config.GetSection("AppSettings:ViewCPTURL").Value, cpt.CptId);

            string body = CreateXMLCPTCompleteEmailNotification(cpt, viewLink);
            SaveEmailRequest(false, cpt.CptId, subject, body, emailTo.TrimEnd(','), (int)EmailREQType.CPTCompleteEmail);
        }

        private string CreateXMLCPTCompleteEmailNotification(Cpt cpt, string viewLink)
        {
            bool qip = false;
            string spectrum = string.Empty;
            bool voyency = false;

            if (cpt.CptPrvsn.SingleOrDefault(i => i.CptPrvsnTypeId == (int)CptProvision.Qip) != null)
                qip = (bool)cpt.CptPrvsn.SingleOrDefault(i => i.CptPrvsnTypeId == (int)CptProvision.Qip).PrvsnStusCd;

            if (cpt.CptPrvsn.SingleOrDefault(i => i.CptPrvsnTypeId == (int)CptProvision.Spectrum) != null)
                spectrum = cpt.CptPrvsn.SingleOrDefault(i => i.CptPrvsnTypeId == (int)CptProvision.Spectrum).SrvrNme;

            if (cpt.CptPrvsn.SingleOrDefault(i => i.CptPrvsnTypeId == (int)CptProvision.Voyence) != null)
                voyency = (bool)cpt.CptPrvsn.SingleOrDefault(i => i.CptPrvsnTypeId == (int)CptProvision.Voyence).PrvsnStusCd;

            // General CPT Info details
            XElement _cptEmail = new XElement("CPTEmail");
            XElement xe = new XElement("CPTInfo",
                new XElement("CPTID", cpt.CptId),
                new XElement("CPTNumber", cpt.CptCustNbr),
                new XElement("CPTViewLink", viewLink),
                new XElement("Shortname", cpt.CustShrtNme),
                new XElement("QIPSubnetName", cpt.QipSubnetNme),
                new XElement("CreatedDate", cpt.CreatDt.ToString("MM/dd/yyyy HH:mm:ss tt")),
                new XElement("QIP", qip ? "Yes" : "No"),
                new XElement("Spectrum", spectrum),
                new XElement("Voyence", voyency ? "Yes" : "No"));
            _cptEmail.Add(xe);

            return _cptEmail.ToString();
        }

        private List<EmailReq> SaveEmailRequestFor24HourCPTNotification(bool isCreated, string notes, Cpt cpt)
        {
            // Method Description:
            // Save email requests for a 24-hour notification for Shortname and IP Address
            // Returns all the emailTo to be used for sending email notification after CPT creation
            List<EmailReq> emailRequests = new List<EmailReq>();
            EmailReq emailRequest;
            short status = (short)EmailStatus.Pending;
            string emailTo = string.Empty;
            List<string> emailaddrs = new List<string>();
            bool isNotify = false;

            // If CPE Managed Voice and shortname validated successfully,
            // add this email address for notification
            string UCaaSEmailAdr = string.Empty;
            if (cpt.RevwdPmCd && !string.IsNullOrWhiteSpace(cpt.CustShrtNme)
                && (cpt.CptRltdInfo != null && cpt.CptRltdInfo.Count() == 1
                    && cpt.CptRltdInfo.SingleOrDefault(i =>
                        i.CptCustTypeId == (short)CptCustomerType.ManagedVoice) != null))
            {
                UCaaSEmailAdr = _commonRepo.GetCowsAppCfgValue("CPT Mngd Voice Completion");
            }

            string viewLink = string.Format(@"{0}{1}",
                _config.GetSection("AppSettings:ViewCPTURL").Value, cpt.CptId);

            // Send string.Empty on UpdatedBy since this is a new CPT request
            string body = CreateXMLCPTEmailNotification(cpt, viewLink, string.Empty, string.Empty, notes);

            // Saved a 24-hour notification upon request created for Shortname and MNSD-MNSW
            if (isCreated)
            {
                // Get 24-hour notification email for Short Name
                emailRequest = new EmailReq();
                emailRequest.EmailReqTypeId = (int)EmailREQType.CPTShortnameEmail;
                emailRequest.StusId = status;
                emailRequest.EmailListTxt = _commonRepo.GetCowsAppCfgValue("CPT ShortName Notification");
                //if (!string.IsNullOrWhiteSpace(UCaaSEmailAdr))
                //    emailRequest.EmailListTxt = emailRequest.EmailListTxt + ", " + UCaaSEmailAdr;
                //emailRequest.EmailListTxt = "sarah.sandoval@sprint.com"; // For testing
                //emailaddrs.Add(emailRequest.EmailListTxt);
                var companyName = cpt.CsgLvlId > 0 ? CustomerMaskedData.customerName : cpt.CompnyNme;
                emailRequest.EmailSubjTxt = $"CPT ShortName Notification; Customer Name: {companyName}";
                emailRequest.EmailBodyTxt = body;
                emailRequest.CreatDt = DateTime.Now;
                emailRequest.CptId = cpt.CptId;
                emailRequests.Add(emailRequest);

                // For immediate email notification
                emailRequest = new EmailReq();
                emailRequest.EmailReqTypeId = (int)EmailREQType.CPTNotificationEmail;
                emailRequest.StusId = status;
                emailRequest.EmailListTxt = _commonRepo.GetCowsAppCfgValue("CPT ShortName Notification");
                //if (!string.IsNullOrWhiteSpace(UCaaSEmailAdr))
                //    emailRequest.EmailListTxt = emailRequest.EmailListTxt + ", " + UCaaSEmailAdr;
                //emailRequest.EmailListTxt = "sarah.sandoval@sprint.com"; // For testing
                //emailaddrs.Add(emailRequest.EmailListTxt);
                emailRequest.EmailSubjTxt = $"CPT ShortName Notification; Customer Name: {companyName}";
                emailRequest.EmailBodyTxt = body;
                emailRequest.CreatDt = DateTime.Now;
                emailRequest.CptId = cpt.CptId;
                emailRequests.Add(emailRequest);
            }

            // Send 24-hour IP Address notification only once Shortname is created
            else if (cpt.CptId > 0 && !string.IsNullOrWhiteSpace(cpt.CustShrtNme))
            {
                // Get notification email for IP Address
                // And added the following condition below.
                // IP Email will be sent only if it pass the following criteria:
                // (1) If Customer TACACS is selected to Yes regardless of the Transport type selected.
                // Update by Sarah Sandoval [20200803] - Commentted 2nd Condition - Combined 2 and 3
                // (2) If Customer Type is MDS and and Service Tier is not equal to Wholesale Carrier.
                // (3) If Primary Transport Types is not equal to IP/Sprintlink and Wireless/WAN/WDLS.
                isNotify = (cpt.TacacsCd
                            //|| cpt.CptRltdInfo.FirstOrDefault(i =>
                            //        i.CptCustTypeId == (int)CptCustomerType.ManagedData
                            //        && i.CptPlnSrvcTierId != (int)CptPlnSrvcTier.WholesaleCarrier) != null
                            || cpt.CptPrimWan.FirstOrDefault(i => i.CptPlnSrvcTierId != (int)CptPlnSrvcTier.WholesaleCarrier
                                    && i.CptPrimScndyTprtId != (int)CptTransportType.IPSprintLink
                                    && i.CptPrimScndyTprtId != (int)CptTransportType.WirelessWANWDLS) != null);

                // Updated by Sarah Sandoval [20200805] - Removed (4) condition above
                // (4) IP Address is not allowed for SDWAN only Customer (therefore if SDWAN is true dont send)
                if (cpt.CptRltdInfo.FirstOrDefault(i =>
                            i.CptCustTypeId == (int)CptCustomerType.ManagedData
                            && i.CptPlnSrvcTierId == (int)CptPlnSrvcTier.MdsComplete
                            && i.SdwanCustCd.GetValueOrDefault(false)) != null)
                    isNotify = false;

                if (isNotify
                        && (string.IsNullOrWhiteSpace(cpt.SubnetIpAdr) ||
                            string.IsNullOrWhiteSpace(cpt.StartIpAdr) ||
                            string.IsNullOrWhiteSpace(cpt.EndIpAdr)))
                {
                    var emails = _emailReqRepo
                        .Find(i => i.CptId == cpt.CptId && i.EmailReqTypeId == (int)EmailREQType.CPTIPAddressEmail).ToList();

                    if (emails == null || (emails != null && emails.Count() <= 0))
                    {
                        // For 24-hour notification
                        emailRequest = new EmailReq();
                        emailRequest.EmailReqTypeId = (int)EmailREQType.CPTIPAddressEmail;
                        emailRequest.StusId = status;
                        emailRequest.EmailListTxt = _commonRepo.GetCowsAppCfgValue("CPT IP Address Notification");
                        //if (!string.IsNullOrWhiteSpace(UCaaSEmailAdr))
                        //    emailRequest.EmailListTxt = emailRequest.EmailListTxt + ", " + UCaaSEmailAdr;
                        //emailRequest.EmailListTxt = "sarah.sandoval@sprint.com"; // For testing
                        //emailaddrs.Add(emailRequest.EmailListTxt);
                        var companyName = cpt.CsgLvlId > 0 ? CustomerMaskedData.customerName : cpt.CompnyNme;
                        emailRequest.EmailSubjTxt = $"CPT IP Address Notification; Customer Name: {companyName}";
                        emailRequest.EmailBodyTxt = body;
                        emailRequest.CreatDt = DateTime.Now;
                        emailRequest.CptId = cpt.CptId;
                        emailRequests.Add(emailRequest);

                        // For immediate email notification
                        emailRequest = new EmailReq();
                        emailRequest.EmailReqTypeId = (int)EmailREQType.CPTNotificationEmail;
                        emailRequest.StusId = status;
                        emailRequest.EmailListTxt = _commonRepo.GetCowsAppCfgValue("CPT IP Address Notification");
                        //if (!string.IsNullOrWhiteSpace(UCaaSEmailAdr))
                        //    emailRequest.EmailListTxt = emailRequest.EmailListTxt + ", " + UCaaSEmailAdr;
                        //emailRequest.EmailListTxt = "sarah.sandoval@sprint.com"; // For testing
                        //emailaddrs.Add(emailRequest.EmailListTxt);
                        emailRequest.EmailSubjTxt = $"CPT IP Address Notification; Customer Name: {companyName}";
                        emailRequest.EmailBodyTxt = body;
                        emailRequest.CreatDt = DateTime.Now;
                        emailRequest.CptId = cpt.CptId;
                        emailRequests.Add(emailRequest);
                    }
                }
            }

            if (UCaaSEmailAdr.Length > 0)
                emailRequests.ForEach(a => a.EmailListTxt = a.EmailListTxt + "," + UCaaSEmailAdr);

            return emailRequests;
        }

        private List<CptHist> SaveEmailOnCPTHistory(List<EmailReq> emails, int cptId)
        {
            List<CptHist> histories = new List<CptHist>();

            // Save email notification for Shortname and IP on CPT History
            if (emails != null || emails.Count > 0)
            {
                foreach (EmailReq email in emails)
                {
                    // Update CPT History
                    if (email.EmailReqTypeId.Equals((int)EmailREQType.CPTShortnameEmail))
                    {
                        histories.Add(CreateCptHistory((int)Actions.CPTSentShortnameEmail,
                            email.EmailSubjTxt + " was sent to " + email.EmailListTxt, cptId, true));
                    }
                    else if (email.EmailReqTypeId.Equals((int)EmailREQType.CPTIPAddressEmail))
                    {
                        histories.Add(CreateCptHistory((int)Actions.CPTSentIPEmail,
                            email.EmailSubjTxt + " was sent to " + email.EmailListTxt, cptId, true));
                    }
                }
            }

            return histories;
        }

        private void DeleteEmailRequests(Cpt cpt)
        {
            List<EmailReq> emails = new List<EmailReq>();

            // Delete Shortname Notification
            if (!string.IsNullOrWhiteSpace(cpt.CustShrtNme))
            {
                emails.AddRange(_emailReqRepo.Find(i => i.CptId == cpt.CptId 
                    && i.EmailReqTypeId == (int)EmailREQType.CPTShortnameEmail
                    && i.StusId == (int)EmailStatus.Pending));
            }

            // Delete IP Notification
            bool isNotify = (cpt.TacacsCd
                            || cpt.CptPrimWan.FirstOrDefault(i => i.CptPlnSrvcTierId != (int)CptPlnSrvcTier.WholesaleCarrier
                                    && i.CptPrimScndyTprtId != (int)CptTransportType.IPSprintLink
                                    && i.CptPrimScndyTprtId != (int)CptTransportType.WirelessWANWDLS) != null);

            if (cpt.CptRltdInfo.FirstOrDefault(i =>
                        i.CptCustTypeId == (int)CptCustomerType.ManagedData
                        && i.CptPlnSrvcTierId == (int)CptPlnSrvcTier.MdsComplete
                        && i.SdwanCustCd.GetValueOrDefault(false)) != null)
                isNotify = false;

            if (isNotify && !string.IsNullOrWhiteSpace(cpt.SubnetIpAdr)
                && !string.IsNullOrWhiteSpace(cpt.StartIpAdr) && !string.IsNullOrWhiteSpace(cpt.EndIpAdr))
            {
                emails.AddRange(_emailReqRepo.Find(i => i.CptId == cpt.CptId
                    && i.EmailReqTypeId == (int)EmailREQType.CPTIPAddressEmail
                    && i.StusId == (int)EmailStatus.Pending));
            }

            // Do not add range since all emails with pending status will be deleted
            if (cpt.CptStusId == (short)CptStatus.Cancelled || cpt.CptStusId == (short)CptStatus.Completed)
            {
                emails = _emailReqRepo.Find(i => i.CptId == cpt.CptId
                    && i.EmailReqTypeId != (int)EmailREQType.CPTCompleteEmail
                    && i.StusId == (int)EmailStatus.Pending).ToList();
            }

            _emailReqRepo.DeleteRequests(emails);
        }

        #endregion Send/Save Email Request
    }
}