﻿using AutoMapper;
using COWS.Entities.Enums;
using COWS.Web.Library.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CacheAdmin")]
    [ApiController]
    public class CacheAdminController : ControllerBase
    {
        
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public CacheAdminController(IMapper mapper,                               
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;           
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("RemoveCacheObject/{name}")]
        public IActionResult RemoveCacheObject([FromRoute] string name)
        {
            if (_cache != null && _cache.Get(name) != null)
            {
                if (_cache.TryGetValue(name, out _))
                {
                    _cache.Remove(name);
                    return Ok(true);
                }
            }

            return Ok(false);
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            List<string> cachedKeys;
            if (!_cache.TryGetValue(CacheKeys.CacheKeyList, out cachedKeys))
            {
                cachedKeys = new List<string>();
                cachedKeys.Add(CacheKeys.DedicatedCustomerList);
                cachedKeys.Add(CacheKeys.DeviceManufacturerList);
                cachedKeys.Add(CacheKeys.DeviceModelAdminList);
                cachedKeys.Add(CacheKeys.DeviceModelList);
                cachedKeys.Add(CacheKeys.EnhncSrvcList);
                cachedKeys.Add(CacheKeys.MDS3rdPartyServiceAdminList);
                cachedKeys.Add(CacheKeys.MDS3rdPartyServiceList);
                cachedKeys.Add(CacheKeys.MDS3rdPartyVendorAdminList);
                cachedKeys.Add(CacheKeys.MDS3rdPartyVendorList);
                cachedKeys.Add(CacheKeys.MDSMACActivityAdminList);
                cachedKeys.Add(CacheKeys.MDSMACActivityList);
                cachedKeys.Add(CacheKeys.EventIntervalList);
                cachedKeys.Add(CacheKeys.SrvcAssrSiteSpportAdminList);
                cachedKeys.Add(CacheKeys.SrvcAssrSiteSpportList);
                cachedKeys.Add(CacheKeys.ServiceDeliveryAdminList);
                cachedKeys.Add(CacheKeys.ServiceDeliveryList);
                cachedKeys.Add(CacheKeys.ServiceTierList);
                cachedKeys.Sort();
            }

            return Ok(cachedKeys);
        }
    }
}