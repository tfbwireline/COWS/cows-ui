﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSEventNtwkCust")]
    [ApiController]
    public class MDSEventNtwkCustController : ControllerBase
    {
        private readonly IMDSEventNtwkCustRepository _repo;
        private readonly ILogger<MDSEventNtwkCustController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MDSEventNtwkCustController(IMapper mapper,
                               IMDSEventNtwkCustRepository repo,
                               ILogger<MDSEventNtwkCustController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"MDSEventNtwkCust by EventId: { id }.");

            var obj = _repo.GetMDEventNtwkCustByEventId(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSEventNtwkCustViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDSEventNtwkCust by EventId: { id } not found.");
                return NotFound(new { Message = $"MDSEventNtwkCust by EventId: { id } not found." });
            }
        }
    }
}