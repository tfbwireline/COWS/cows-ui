﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/RedesignRecLock")]
    [ApiController]
    public class RedesignRecLockController : ControllerBase
    {
        private readonly IRedesignRecLockRepository _repo;
        private readonly IUserRepository _userRepo;
        private readonly ILogger<RedesignRecLockController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public RedesignRecLockController(IMapper mapper,
                               IRedesignRecLockRepository repo,
                               IUserRepository userRepo,
                               ILogger<RedesignRecLockController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _userRepo = userRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("CheckLock/{id}")]
        public IActionResult CheckLock([FromRoute] int id)
        {
            _logger.LogInformation($"Check Redesign Lock by Id: { id }.");

            var obj = _repo.CheckLock(id);
            if (obj != null)
            {
                if (_loggedInUser.GetLoggedInUserId() == obj.LockByUserId)
                    return Ok();

                var usr = _userRepo.GetById(obj.LockByUserId);
                if (usr != null)
                    return Ok(usr.FullNme);
            }

            return Ok();
        }

        [HttpGet("Lock/{id}")]
        public IActionResult Lock([FromRoute] int id)
        {
            var hasAccess = _userRepo.UserActiveWithProfileName(_loggedInUser.GetLoggedInUserId(), "Sales Design Engineer")
                    || _userRepo.UserActiveWithProfileName(_loggedInUser.GetLoggedInUserId(), "Network Engineer")
                    || _userRepo.UserActiveWithProfileName(_loggedInUser.GetLoggedInUserId(), "System Security Engineer")
                    || _userRepo.UserActiveWithProfileName(_loggedInUser.GetLoggedInUserId(), "MDS Admin")
                    || _userRepo.UserActiveWithProfileName(_loggedInUser.GetLoggedInUserId(), "MDS Event Reviewer")
                    || _userRepo.UserActiveWithProfileName(_loggedInUser.GetLoggedInUserId(), "SIPTnUCaaS Admin")
                    || _userRepo.UserActiveWithProfileName(_loggedInUser.GetLoggedInUserId(), "SIPTnUCaaS Event Reviewer");

            if (hasAccess)
            {
                _logger.LogInformation($"Lock Redesign by Id: { id }.");

                _repo.Lock(new RedsgnRecLock
                {
                    RedsgnId = id,
                    LockByUserId = _loggedInUser.GetLoggedInUserId(),
                    StrtRecLockTmst = DateTime.Now,
                    CreatDt = DateTime.Now
                });
            }

            return Ok();
        }

        [HttpGet("Unlock/{id}")]
        public IActionResult Unlock([FromRoute] int id)
        {
            _logger.LogInformation($"Unlock Redesign by Id: { id }.");

            _repo.Unlock(id, _loggedInUser.GetLoggedInUserId());
            return Ok();
        }
    }
}
