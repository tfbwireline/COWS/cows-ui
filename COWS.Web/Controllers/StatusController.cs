﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Status")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private readonly IStatusRepository _repo;
        private readonly ILogger<StatusController> _logger;
        private readonly IMapper _mapper;

        public StatusController(IMapper mapper,
                               IStatusRepository repo,
                               ILogger<StatusController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<StatusViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<StatusViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.StusDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Status by Id: { id }.");

            var obj = _repo.Find(s => s.StusId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<StatusViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Status by Id: { id } not found.");
                return NotFound(new { Message = $"Status Id: { id } not found." });
            }
        }
    }
}