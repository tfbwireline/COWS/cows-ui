﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/WholesalePartners")]
    [ApiController]
    public class WholesalePartnerController : ControllerBase
    {
        private readonly IWholesalePartnerRepository _repo;
        private readonly ILogger<WholesalePartnerController> _logger;
        private readonly IMapper _mapper;

        public WholesalePartnerController(IMapper mapper,
                               IWholesalePartnerRepository repo,
                               ILogger<WholesalePartnerController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<WholesalePartnerViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<WholesalePartnerViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.WhlslPtnrNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Wholesale Partner by Id: { id }.");

            var obj = _repo.Find(s => s.WhlslPtnrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<WholesalePartnerViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Wholesale Partner by Id: { id } not found.");
                return NotFound(new { Message = $"Wholesale Partner by Id: { id } not found." });
            }
        }
    }
}