﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderRepository _repo;
        private readonly IFsaOrderRepository _fsaRepo;
        private readonly ICircuitRepository _circuitRepo;
        private readonly ICircuitMsRepository _circuitMsRepo;
        private readonly IOrderMsRepository _orderMsRepo;
        private readonly IVendorOrderRepository _vendorOrderRepo;
        private readonly IActiveTaskRepository _activeTaskRepo;
        private readonly ISystemConfigRepository _systemConfigRepo;
        private readonly INRMRepository _nrmRepo;
        private readonly ILogger<OrdersController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IOrderNoteRepository _orderNoteRepo;
        private readonly ICircuitChargeRepository _circuitChargeRepo;
        private readonly IOrderVlanRepository _orderVlanRepo;

        private readonly INccoOrderRepository _nccoRepo;
        private readonly IH5FolderRepository _h5FldrRepo;
        private readonly ICcdRepository _ccdRepo;
        private readonly IOrderAddressRepository _ordrAddrsRepo;
        private readonly IOrderContactRepository _ordrCntctRepo;
        private readonly IOrderStdiHistoryRepository _ordrStdiRepo;
        private readonly IFsaOrderCustRepository _fsaOrderCustRepo;
        private readonly IWorkGroupRepository _workGroupRepo;
        private readonly ITaskRepository _taskRepo;
        private readonly IEmailReqRepository _emailReqRepository;

        public OrdersController(IMapper mapper,
                               IOrderRepository repo,
                               IFsaOrderRepository fsaRepo,
                               ICircuitRepository circuitRepo,
                               ICircuitMsRepository circuitMsRepo,
                               IOrderMsRepository orderMsRepo,
                               IVendorOrderRepository vendorOrderRepo,
                               IActiveTaskRepository activeTaskRepo,
                               ILogger<OrdersController> logger,
                               ILoggedInUserService loggedInUser,
                               IOrderNoteRepository orderNoteRepo,
                               ICircuitChargeRepository circuitChargeRepo,
                               IOrderVlanRepository orderVlanRepo,
                               ISystemConfigRepository systemConfigRepo,
                               INRMRepository nrmRepo,
                               INccoOrderRepository nccoRepo,
                               IH5FolderRepository h5FldrRepo,
                               ICcdRepository ccdRepo,
                               IOrderAddressRepository ordrAddrsRep,
                               IOrderContactRepository ordrCntctRepo,
                               IOrderStdiHistoryRepository ordrStdiRepo,
                               IFsaOrderCustRepository fsaOrderCustRepo,
                               IWorkGroupRepository workGroupRepo,
                               ITaskRepository taskRepo,
                               IEmailReqRepository emailReqRepository)
        {
            _mapper = mapper;
            _repo = repo;
            _fsaRepo = fsaRepo;
            _circuitRepo = circuitRepo;
            _circuitMsRepo = circuitMsRepo;
            _orderMsRepo = orderMsRepo;
            _vendorOrderRepo = vendorOrderRepo;
            _activeTaskRepo = activeTaskRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _orderNoteRepo = orderNoteRepo;
            _circuitChargeRepo = circuitChargeRepo;
            _orderVlanRepo = orderVlanRepo;
            _systemConfigRepo = systemConfigRepo;
            _nrmRepo = nrmRepo;
            _nccoRepo = nccoRepo;
            _h5FldrRepo = h5FldrRepo;
            _ccdRepo = ccdRepo;
            _ordrAddrsRepo = ordrAddrsRep;
            _ordrCntctRepo = ordrCntctRepo;
            _ordrStdiRepo = ordrStdiRepo;
            _fsaOrderCustRepo = fsaOrderCustRepo;
            _workGroupRepo = workGroupRepo;
            _taskRepo = taskRepo;
            _emailReqRepository = emailReqRepository;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Get Order By Id: {id}");
            var adid = _loggedInUser.GetLoggedInUserAdid();

            var orderDetails = _repo.GetOrderDetails(i => i.OrdrId == id && (i.RecStusId == 0 || i.RecStusId == 1), adid);

            if (orderDetails != null)
            {
                //_activeTaskRepo.LoadRtsTask(id);
                //var list = _mapper.Map<OrderDetailViewModel>(orderDetails.SingleOrDefault());

                return Ok(_mapper.Map<OrderDetailViewModel>(orderDetails.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Order not found.");
                return NotFound(new { Message = $"Order not found." });
            }
        }

        [HttpGet("{id}/Version2")]
        public IActionResult Get2([FromRoute] int id)
        {
            _logger.LogInformation($"Get Order By Id: {id}");
            var adid = _loggedInUser.GetLoggedInUserAdid();
            var data = _repo.Find(i => i.OrdrId == id, adid);// && i.RecStusId == 1);

            if (data != null)
            {
                var list = _mapper.Map<xNCI2ViewModel>(data.SingleOrDefault());

                // Get Parent Order Type Code
                if (list.OrdrCatId == 2 || list.OrdrCatId == 6)
                {
                    var parentOrderId = data.SingleOrDefault().PrntOrdrId;
                    if (parentOrderId != null)
                    {
                        var parent = _repo.Find(a => a.OrdrId == parentOrderId).SingleOrDefault();

                        list.FsaOrdr.ParentOrderTypeCd = parent.FsaOrdr.OrdrTypeCd;
                    }
                }
                //_activeTaskRepo.LoadRtsTask(id);

                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"Order not found.");
                return NotFound(new { Message = $"Order not found." });
            }
        }
       
        [HttpGet("{id}/Version3")]
        public IActionResult Get3([FromRoute] int id, [FromQuery] int wgId, [FromQuery] int taskId)
        {
            _logger.LogInformation($"Get Order By Id: {id}");
            
            var order = _repo.GetOrderDetails(i => i.OrdrId == id, _loggedInUser.GetLoggedInUserAdid()).FirstOrDefault();

            if (order != null)
            {
                GetOrderDetailsViewModel orderDetails = new GetOrderDetailsViewModel();

                var ordrVM = _mapper.Map<OrderDetailViewModel>(order);

                // Get Parent Order Type Code
                string prntOrdrType = string.Empty;
                if (ordrVM.OrderTypeDesc == "CANCEL" && ordrVM.OrderCategoryId == 2)
                {
                    prntOrdrType = ordrVM.FsaOrderTypeDesc;
                }

                // Get related entities
                if (order.OrdrCatId == 4)
                {
                    ordrVM.NccoOrdr = _mapper.Map<NccoOrderViewModel>(
                        _nccoRepo.Find(i => i.OrdrId == id).FirstOrDefault());
                }
                else
                {
                    ordrVM.FsaOrdr = _mapper.Map<FsaOrderViewModel>(
                        _fsaRepo.Find(i => i.OrdrId == id).FirstOrDefault());

                    // H1 Info
                    orderDetails.H1Info = _mapper.Map<FsaOrderCustViewModel>(
                        _fsaOrderCustRepo.GetByOrderIdAndCISLevelType(id, "H1", _loggedInUser.GetLoggedInUserCsgLvlId()));

                    // H4 Info
                    orderDetails.H4Info = _mapper.Map<FsaOrderCustViewModel>(
                        _fsaOrderCustRepo.GetByOrderIdAndCISLevelType(id, "H4", _loggedInUser.GetLoggedInUserCsgLvlId()));

                    // H6 Info
                    orderDetails.H6Info = _mapper.Map<FsaOrderCustViewModel>(
                        _fsaOrderCustRepo.GetByOrderIdAndCISLevelType(id, "H6", _loggedInUser.GetLoggedInUserCsgLvlId()));

                    // FSA Disconnect Details
                    orderDetails.FSADisconnectDetails = _fsaRepo.GetFSADisconnectDetails(id);
                }

                // Get H5Folder
                if (ordrVM.H5FolderId.HasValue)
                {
                    ordrVM.H5Foldr = _mapper.Map<H5FolderViewModel>(
                                        _h5FldrRepo.GetByIdForOrderView(ordrVM.H5FolderId.Value,
                                            _loggedInUser.GetLoggedInUserCsgLvlId()));
                }

                // Get Vendor Order
                ordrVM.VndrOrdr = _mapper.Map<VendorOrderViewModel>(
                    _vendorOrderRepo.Find(i => i.OrdrId == id && i.RecStusId == (byte)ERecStatus.Active).FirstOrDefault());

                // Get Order Address
                ordrVM.OrdrAdr = _mapper.Map<IEnumerable<OrderAddressViewModel>>(
                    _ordrAddrsRepo.GetOrderAddressByOrderId(id, _loggedInUser.GetLoggedInUserCsgLvlId()))
                    .OrderByDescending(i => i.CreatDt).ToList();

                // Get Order Contact
                //ordrVM.OrdrCntct = _mapper.Map<IEnumerable<OrderContactViewModel>>(
                //    _ordrCntctRepo.Find(i => i.OrdrId == id))
                //    .OrderByDescending(i => i.CreatDt).ToList();
                var data = _ordrCntctRepo.GetSecuredOrdrCntctById(id);
                ordrVM.OrdrCntct = _mapper.Map<IEnumerable<OrderContactViewModel>>(data).OrderByDescending(i => i.CreatDt).ToList();
                ordrVM.H6Contact = _mapper.Map<OrderContactViewModel>(_ordrCntctRepo.GetH6ContactByOrderId(id, _loggedInUser.GetLoggedInUserCsgLvlId()));

                // Get Order VLAN
                ordrVM.OrdrVlan = _mapper.Map<IEnumerable<OrderVlanViewModel>>(
                    _orderVlanRepo.Find(i => i.OrdrId == id && i.VlanSrcNme == "COWS"))
                    .OrderByDescending(i => i.CreatDt).ToList();

                // Get STDI History
                ordrVM.OrdrStdiHist = _mapper.Map<IEnumerable<OrderStdiHistoryViewModel>>(
                    _ordrStdiRepo.Find(i => i.OrdrId == id))
                    .OrderByDescending(i => i.CreatDt).ToList();

                // Get CCD History
                ordrVM.CcdHist = _mapper.Map<IEnumerable<CCDHistoryViewModel>>(
                    _ccdRepo.GetCcdHistory(id))
                    .OrderByDescending(i => i.CreatDt).ToList();

                // Get Notes History
                ordrVM.OrdrNte = _mapper.Map<IEnumerable<OrderNoteViewModel>>(
                    _orderNoteRepo.GetByOrdrId(id))
                    .OrderByDescending(i => i.CreatDt).ToList();

                // Get Transport Order Data
                orderDetails.TransportOrderDetails = _repo.GetTransportOrder(id);

                // Get FTN List Details
                orderDetails.FTNListDetails = _workGroupRepo
                    .GetFTNListDetailsForOrderView(id, wgId, _loggedInUser.GetLoggedInUserCsgLvlId());

                // Get Pre-Qual Line Info (VENDOR ACCESS COST)
                orderDetails.PreQualLineInfo = _circuitRepo.GetPreQualLineInfo(id);

                // Get Current Status
                orderDetails.CurrentTaskName = _taskRepo.GetCurrentStatus(taskId, wgId, id);

                // Get Latest Non System Order Note Info (LAST NOTE INSERTED BY)
                // Sample: (Last updated by: Phillips, David L [IT] - Profile: Fedline LCT PM)
                orderDetails.LatestNonSystemOrderNote = _workGroupRepo.GetLatestNonSystemOrderNoteInfo(id);

                var gomCancelTask = _activeTaskRepo.Find(a => a.OrdrId == id && a.TaskId == 108).FirstOrDefault();
                if (gomCancelTask != null)
                {
                    orderDetails.IsGomCancelTaskCompleted = gomCancelTask.StusId != 0 ? true : false;
                }
                else
                {
                    orderDetails.IsGomCancelTaskCompleted = false;
                }

                var asd = _circuitRepo.GetAccessCost(id);
                orderDetails.CircuitCost = _mapper.Map<IEnumerable<CircuitCostViewModel>>(asd)
                    .ToList();

                var installPortList = _repo.GetInstallPort(id);
                orderDetails.InstallPort = installPortList.ToList();

                orderDetails.OrderInfo = ordrVM;
                return Ok(orderDetails);
            }
            else
            {
                _logger.LogInformation($"Order not found.");
                return NotFound(new { Message = $"Order not found." });
            }
        }

        [HttpGet("{id}/IsDynamic")]
        public IActionResult GetBasic([FromRoute] int id)
        {
            _logger.LogInformation($"Get Order Basic By Id: {id}");

            var data = _repo.GetBasic(id);// && i.RecStusId == 1);

            if (data.Count() > 0)
            {
                var list = _mapper.Map<OrderBasic>(data.FirstOrDefault());
                var lRogers = GetVendorCodeList("lRogers").ToList();
                var lRogersVPN = GetVendorCodeList("lRogersVPN").ToList();
                var lOrange = GetVendorCodeList("lOrange").ToList();
                var lOrangeVPN = GetVendorCodeList("lOrangeVPN").ToList();
                var lNavega = GetVendorCodeList("lNavega").ToList();
                var lNavegaVPN = GetVendorCodeList("lNavegaVPN").ToList();
                var lGCI = GetVendorCodeList("lGCI").ToList();
                var lGCIVPN = GetVendorCodeList("lGCIVPN").ToList();
                var lBellCanada = GetVendorCodeList("lBellCanada").ToList();
                var lBellCanadaVPN = GetVendorCodeList("lBellCanadaVPN").ToList();
                var lChinaTelecom = GetVendorCodeList("lChinaTelecom").ToList();
                var lChinaTelecomVPN = GetVendorCodeList("lChinaTelecomVPN").ToList();

                if (list.OrdrCatId == 2 || list.OrdrCatId == 6)
                {
                    var orderCategories = new int[]
                    {
                        (int)OrderTypes.Install,
                        (int)OrderTypes.Upgrade,
                        (int)OrderTypes.Downgrade,
                        (int)OrderTypes.Move,
                        (int)OrderTypes.Change,
                        (int)OrderTypes.BillingChange,
                        (int)OrderTypes.Disconnect,
                    };

                    if (orderCategories.Any(a => a == list.OrdrCatId))
                    {
                        if (list.ProdTypeId == (int)OrderProduct.MPLSOffnet
                            && ((lRogers.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lRogersVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lOrange.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lOrangeVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lNavega.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lNavegaVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lGCI.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lGCIVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lBellCanada.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lBellCanadaVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                                || (lChinaTelecom.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) && lChinaTelecomVPN.Any(a => (list.VndrVpnCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)))))
                        {
                            return Ok(true);
                        }
                        else if (list.ProdTypeId == (int)OrderProduct.DIAOffnet
                            && (lRogers.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) || lOrange.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase))))
                        {
                            return Ok(true);
                        }
                        else if (list.ProdTypeId == (int)OrderProduct.SLFROffnet
                            && (lRogers.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase)) || lGCI.Any(a => (list.InstlVndrCd ?? "").Contains(a, StringComparison.CurrentCultureIgnoreCase))))
                        {
                            return Ok(true);
                        }
                    }
                }
                else if (list.OrdrCatId == 0 && list.InstlVndrCd == string.Empty && list.VndrVpnCd == string.Empty)
                {
                    return Ok(true);
                }

                return Ok(false);
            }
            else
            {
                _logger.LogInformation($"Order not found.");
                return NotFound(new { Message = $"Order not found." });
            }
        }

        [HttpPut("{id}/LoadRtsTask")]
        public IActionResult LoadRtsTask([FromRoute] int id, [FromBody] OrderFormViewModel model)
        {
            _logger.LogInformation($"Load RTS Task for Order Id: {id}");

            _activeTaskRepo.LoadRtsTask(id);
            var message = "Loading RTS(Sales Support) task from xNCI queue.";
            if(!String.IsNullOrEmpty(model.nteTxt))
            {
                message = model.nteTxt + "-" + message;
            }
            //  var result = InsertNote(id, 11, "Loading RTS(Sales Support) task from xNCI queue.");
            var result = InsertNote(id, 11, message);
            if (result != null)
            {
                _circuitChargeRepo.SetJeopardy(id, result.NteId, "103");
            }
            else
            {
                _logger.LogInformation($"An error occurred while returning order to sales queue.");
                return NotFound(new { Message = $"An error occurred while returning order to sales queue" });
            }
            _logger.LogInformation($"Loaded RTS Task for Order Id: {id}");

            return Ok();
        }
        [HttpPut("{id}/UpdateNrm")]
        //public bool UpdateNrm([FromRoute] int id, [FromBody] bool nrmupdate)
        //{
        //    var res = new TrptOrdr
        //    {
        //        NrmUpdtCd = nrmupdate,
        //        OrdrId = id
        //    };

        //    var obj = _mapper.Map<TrptOrdr>(res);
        //    var loggedInUser = _loggedInUser.GetLoggedInUser();
        //    if (loggedInUser != null)
        //    {
        //        obj.CreatByUserId = loggedInUser.UserId;
        //        obj.CreatDt = DateTime.Now;
        //        obj.OrdrId = id;
        //        obj.NrmUpdtCd = nrmupdate;
        //    }

        //    return _repo.UpdateNrm(id, nrmupdate);

        //    //if (insertNote == null)
        //    //    return false;
        //    //else
        //    //    return true;
        //}


        private OrdrNte InsertNote(int orderId, byte nteType, string nte)
        {
            var note = new OrderNoteViewModel
            {
                OrdrId = orderId,
                NteTypeId = nteType,
                NteTxt = nte
            };

            var obj = _mapper.Map<OrdrNte>(note);
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.CreatByUserId = loggedInUser.UserId;
                obj.CreatDt = DateTime.Now;
            }

            return _orderNoteRepo.Create(obj);

            //if (insertNote == null)
            //    return false;
            //else
            //    return true;
        }

        [HttpPut("{id}/MoveToGom")]
        public IActionResult MoveToGom([FromRoute] int id, [FromBody] OrderFormViewModel model)
        {
            _logger.LogInformation($"Moving Order Id: {id} to GOM");

            if (_activeTaskRepo.MoveToGom(id, _loggedInUser.GetLoggedInUserId()) > 0)
            {
                _logger.LogInformation($"Moved Order Id: {id} to GOM");

                return Ok();
            }
            else
            {
                _logger.LogInformation($"Could not move to GOM");
                return NotFound(new { Message = $"Could not move to GOM" });
            }
        }

        [HttpPut("{id}/ClearAcrTask")]
        public IActionResult ClearAcrTask([FromRoute] int id, [FromBody] OrderFormViewModel model)
        {
            _logger.LogInformation($"Clear ACR Task for Order Id: {id}");

            if (_activeTaskRepo.ClearAcrTask(id, model.TaskId, _loggedInUser.GetLoggedInUserId()) > 0)
            {
                _logger.LogInformation($"Cleared ACR Task for Order Id: {id}");

                return Ok();
            }
            else
            {
                _logger.LogInformation($"Could not clear ACR Task");
                return NotFound(new { Message = $"Could not clear ACR Task" });
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] OrderFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.FsaInfo.FsaOrdrCpeLineItem = model.FsaInfo.FsaOrdrCpeLineItem.Select(a =>
                {
                    if (a.FsaOrdrGomXnci.Count > 0)
                    {
                        a.FsaOrdrGomXnci = a.FsaOrdrGomXnci.Select(b =>
                        {
                            b.PrchOrdrBackOrdrShipDt = a.PrchOrdrBackOrdrShipDt;
                            b.CpeVndrNme = a.CpeVndrNme;
                            b.ShpmtTrkNbr = a.ShpmtTrkNbr;
                            b.CustDlvryDt = a.CustDlvryDt;

                            return b;
                        }).ToList();
                    }
                    else
                    {

                        a.FsaOrdrGomXnci.Add(new FsaOrdrGomXnciViewModel()
                        {
                            OrdrId = a.OrdrId,
                            GrpId = 1, // Default as it is a NOT NULL column
                            CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                            CreatDt = DateTime.Now,
                            FsaCpeLineItemId = a.FsaCpeLineItemId == 0 ? null : (int?)a.FsaCpeLineItemId, // Default to "NULL" if value is 0 due to FK contraint
                            UsrPrfId = (short)model.UserProfileId,
                            PrchOrdrBackOrdrShipDt = a.PrchOrdrBackOrdrShipDt,
                            CpeVndrNme = a.CpeVndrNme,
                            ShpmtTrkNbr = a.ShpmtTrkNbr,
                            CustDlvryDt = a.CustDlvryDt
                        });
                    }

                    return a;
                }).ToList();
                model.FsaInfo.NrmUpdate = model.FsaInfo.NrmUpdate.HasValue ? model.FsaInfo.NrmUpdate.Value : false;
                FsaOrdr fsa = _mapper.Map<FsaOrdr>(model.FsaInfo);
                fsa.Ordr.OrdrVlan = new List<OrdrVlan>();
                fsa.Ordr.OrdrVlan.Add(new OrdrVlan
                {
                    OrdrId = id,
                    VlanId = model.CircuitInfo.VlanId,
                    VlanSrcNme = "COWS",
                    VlanPctQty = 0,
                    CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                    CreatDt = DateTime.Now
                });

                //var asd = fsa.FsaOr
                //if ()

                int orderId = id;
                bool isNCCO = false;
                var fsaOrder = _fsaRepo.Find(x => x.OrdrId == id).FirstOrDefault();
                if(fsaOrder != null)
                {
                    _fsaRepo.Update(id, fsa);
                    if (_fsaRepo.SaveAll() > 0)
                    {
                        _logger.LogInformation($"FSA Order Updated. { JsonConvert.SerializeObject(model.FsaInfo).ToString() } ");
                    }

                    orderId = model.FsaInfo.OrdrId;
                } else
                {
                    isNCCO = true;
                }
                //Ordr transportOrder = _repo.Find(a => a.OrdrId == orderId).SingleOrDefault();
                //if(transportOrder!= null)
                //{                         
                //    transportOrder.TrptOrdr.NrmUpdtCd = model.NrmUpdate;
                //    bool newBool = transportOrder.TrptOrdr.NrmUpdtCd.HasValue ? transportOrder.TrptOrdr.NrmUpdtCd.Value : false;
                //    _repo.UpdateNrm(orderId, newBool);
                //}
                
                Ordr order = _repo.Find(a => a.OrdrId == orderId).SingleOrDefault();
                if (order != null)
                {
                    var vendorOrder = _vendorOrderRepo.Find(a => a.OrdrId == order.OrdrId).FirstOrDefault();

                    if (vendorOrder != null)
                    {
                        if (vendorOrder.BypasVndrOrdrMsCd != model.MilestoneInfo.BypassVendorOrderCode)
                        {
                            vendorOrder.BypasVndrOrdrMsCd = model.MilestoneInfo.BypassVendorOrderCode;

                            _vendorOrderRepo.Update(vendorOrder.VndrOrdrId, vendorOrder);

                            if (_vendorOrderRepo.SaveAll() > 0)
                            {
                                _logger.LogInformation($"Vendor Order Bypass Code Updated. VendorOrderId: { vendorOrder.VndrOrdrTypeId }");
                            }
                        }
                    }

                    var obj = _repo.GetOrderDetails(i => i.OrdrId == id && (i.RecStusId == 0 || i.RecStusId == 1));
                    if (obj != null)
                    {
                        var orderDetails = _mapper.Map<OrderDetailViewModel>(obj.SingleOrDefault());
                        var productTypeDesc = orderDetails.ProductTypeDesc;

                        if (productTypeDesc != "CPE")
                        {
                            SaveOrderMilestone(order, model.MilestoneInfo, orderDetails);
                            SaveCircuitMilestone(order, model.MilestoneInfo, model.CircuitInfo);


                            var isBypass = model.MilestoneInfo.BypassVendorOrderCode == true ? true : false;
                            List<StateMachine> sm = GetStateMachines(order, orderDetails, isBypass, isBypass);
                            if (sm != null && sm.Count > 0)
                            {
                                CompleteActiveTask(sm);

                                var activeTasks = _activeTaskRepo
                                    .Find(a => a.OrdrId == id && a.StusId == 0);

                                if (activeTasks.Count() > 0)
                                {
                                    return Ok(activeTasks.FirstOrDefault().TaskId);
                                }

                            }
                        }
                    }

                    //bool isFsa = order.FsaOrdr != null ? true : false;

                    //if (isFsa)
                    //{
                    //    bool isCpe = order.FsaOrdr.ProdTypeCd == "CP" ? true : false;

                    //    if (!isCpe)
                    //    {
                    //        SaveOrderMilestone(order, model.MilestoneInfo);
                    //        SaveCircuitMilestone(order, model.MilestoneInfo, model.CircuitInfo);


                    //        var isBypass = model.MilestoneInfo.BypassVendorOrderCode == true ? true : false;
                    //        List<StateMachine> sm = GetStateMachines(order, isBypass, isBypass);
                    //        if (sm != null && sm.Count > 0)
                    //        {
                    //            CompleteActiveTask(sm);

                    //            var activeTasks = _activeTaskRepo
                    //                .Find(a => a.OrdrId == id && a.StusId == 0);

                    //            if (activeTasks.Count() > 0)
                    //            {
                    //                return Ok(activeTasks.FirstOrDefault().TaskId);
                    //            }

                    //        }
                    //    }
                    //} else {
                    //    SaveOrderMilestone(order, model.MilestoneInfo);
                    //    SaveCircuitMilestone(order, model.MilestoneInfo, model.CircuitInfo);

                    //    return Ok(0);
                    //}
                }

                return Ok(0);
            }

            return BadRequest(new { Message = "Order Could Not Be Updated." });
        }

        [HttpGet("GetOrderPPRT/{id}")]
        public ActionResult GetOrderPPRT([FromRoute] int id)
        {
            var data = _repo.GetOrderPPRT(id);
            return Ok(data);
        }



        private Ordr PopulateMilestone(OrderFormViewModel model)
        {
            Ordr obj = new Ordr();
            Ordr order = _repo.Find(a => a.OrdrId == model.FsaInfo.OrdrId).SingleOrDefault();

            if (order != null)
            {
                bool isFsa = order.FsaOrdr != null ? true : false;

                if (isFsa)
                {
                    //obj.OrdrMs = PopulateOrderMilestone(order, model.MilestoneInfo);
                    //obj.Ckt = new List<Ckt>();
                    //obj.Ckt.Add(PopulateCircuitMilestone(order, model.MilestoneInfo, model.CircuitInfo));
                    //var value = SaveCircuitMilestone(order, model.MilestoneInfo, model.CircuitInfo);
                }
            }

            return obj;
        }

        private void SaveOrderMilestone(Ordr order, MilestoneViewModel model, OrderDetailViewModel orderDetails)
        {
            //var fsa = order.FsaOrdr;
            var productTypeDesc = orderDetails.ProductTypeDesc;
            var orderType = orderDetails.OrderTypeDesc.ToUpper();
            var parentOrderType = orderDetails.FsaOrderTypeDesc.ToUpper();
            //var parentOrderType = fsa.PrntFtn != null ? _fsaRepo.Find(a => a.Ftn == fsa.PrntFtn).SingleOrDefault().OrdrTypeCd : null;

            OrdrMs orderMs = order.OrdrMs.Count > 0 ? order.OrdrMs.SingleOrDefault() : new OrdrMs();
            orderMs.CreatByUserId = _loggedInUser.GetLoggedInUserId();
            orderMs.CreatDt = DateTime.Now;
            //orderMs.Ordr.TrptOrdr.NrmUpdtCd = model.NrmUpdate;
           // var ckt = order.Ckt.Count > 0 ? order.Ckt.FirstOrDefault() : null;
            if (orderType == "DISCONNECT" || (orderType == "CANCEL" && parentOrderType == "DISCONNECT"))
            {
                //(sOrderType.ToUpper() == "DISCONNECT") || ((sOrderType.ToUpper() == "CANCEL") && (sParentOrderType.ToUpper() == "DISCONNECT"))
                //orderMs.PreSbmtDt; no pre-submit date
                orderMs.OrdrId = model.OrdrId;
                orderMs.SbmtDt = model.SubmitDate;
                orderMs.VldtdDt = model.ValidatedDate;
                orderMs.OrdrDscnctDt = model.DisconnectCompletedDate;
                orderMs.CustAcptcTurnupDt = model.ClosedDate;
                orderMs.VndrCnclnDt = model.VendorCancellationDate;

                if (orderDetails.OrderCategoryId == 4)
                {
                    order.NccoOrdr.CcsDt = model.CustomerCommitDate;
                    order.NccoOrdr.CwdDt = model.CustomerWantDate;
                }
                else
                {
                    order.FsaOrdr.CustCmmtDt = model.CustomerCommitDate;
                    order.FsaOrdr.CustWantDt = model.CustomerWantDate;
                }
                //var cktMs = ckt.CktMs.Count > 0 ? ckt.CktMs.FirstOrDefault() : null;
                //if (cktMs == null)
                //{
                //    cktMs = new CktMs()
                //    {
                //        CktId = ckt.CktId,
                //        VerId = 1,
                //        TrgtDlvryDt = model.TargetDeliveryDate,
                //        TrgtDlvryDtRecvDt = model.TargetDeliveryDateReceived,
                //        AccsDlvryDt = model.AccessDeliveryDate,
                //        AccsAcptcDt = model.AccessAcceptDate,
                //        CntrcTermId = model.VendorContractTerm,
                //        CntrcStrtDt = model.VendorContractStartDate,
                //        VndrCnfrmDscnctDt = model.VendorConfirmedDisconnectDate,
                //        CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                //        CreatDt = DateTime.Now,
                //    };

                //    cktMs = _circuitMsRepo.Create(cktMs);
                //    _logger.LogInformation($"Circuit Milestone Created.");
                //}
                //else
                //{
                //    cktMs.VerId = 1;
                //    cktMs.TrgtDlvryDt = model.TargetDeliveryDate;
                //    cktMs.TrgtDlvryDtRecvDt = model.TargetDeliveryDateReceived;
                //    cktMs.AccsDlvryDt = model.AccessDeliveryDate;
                //    cktMs.AccsAcptcDt = model.AccessAcceptDate;
                //    cktMs.CntrcTermId = model.VendorContractTerm;
                //    cktMs.CntrcStrtDt = model.VendorContractStartDate;
                //    cktMs.VndrCnfrmDscnctDt = model.VendorConfirmedDisconnectDate;

                //    _circuitMsRepo.Update(cktMs.CktId, cktMs);
                //    if (_circuitMsRepo.SaveAll() > 0)
                //    {
                //        _logger.LogInformation($"Circuit Updated.");
                //    }
                //    //else
                //    //{
                //    //    return new { Message = "Circuit Milestone Could Not Be Updated." };
                //    //}
                //}

            }
            else if (orderType == "BILLING ONLY" || (orderType == "CANCEL" && parentOrderType == "BILLING ONLY"))
            {
                //sOrderType.ToUpper() == "BILLING ONLY" || ((sOrderType.ToUpper() == "CANCEL") && (sParentOrderType.ToUpper() == "BILLING ONLY"))

                orderMs.OrdrId = model.OrdrId;
                orderMs.VldtdDt = model.ValidatedDate;
                //to.sCnfrmRnwlDt = txtCnfrmRnwlDt.Text;
                orderMs.CustAcptcTurnupDt = model.ClosedDate;
                orderMs.VndrCnclnDt = model.VendorCancellationDate;


                // No renewal info provided in requirements
                //bool bAccessRenewal = false;
                //foreach (ListItem li in rblAccessRenewal.Items)
                //{
                //    if (li.Selected)
                //    {
                //        bAccessRenewal = true;
                //        to.sAccessRenewal = li.Value;
                //    }
                //}
                //txtCnfrmRnwlDt.Enabled = to.sAccessRenewal == "1" ? true : false;
                //if (txtxNCIValidatedDate.Text != string.Empty && txtCnfrmRnwlDt.Text == string.Empty)
                //{
                //    if (!bAccessRenewal)
                //        lblBillingErrors.Text = sErrorMessage = "Please select Access Renewal before completing xNCI validated date milestone.";
                //}

                //if (rblAccessRenewal.Items.FindByValue("1").Selected && (sMilestoneStatus.Contains("xNCI Close Date") || sMilestoneStatus.Contains("xNCI Confirmed Renewal Date")) && txtCnfrmRnwlDt.Text == string.Empty)
                //{
                //    lblBillingErrors.Text = sErrorMessage = "Please enter Confirmed Renewal Date or Select No for Access Renewal.";
                //}
            }
            else
            {
                orderMs.OrdrId = model.OrdrId;
                //to.sPreSubmitDate = lblPreSubmitDateValue.Text;
                orderMs.SbmtDt = model.SubmitDate;
                orderMs.VldtdDt = model.ValidatedDate;
                orderMs.OrdrBillClearInstlDt = model.BillClearInstallDate;
                orderMs.CustAcptcTurnupDt = model.ClosedDate;
                orderMs.VndrCnclnDt = model.VendorCancellationDate;
                if (orderDetails.OrderCategoryId == 4)
                {
                    order.NccoOrdr.CcsDt = model.CustomerCommitDate;
                    order.NccoOrdr.CwdDt = model.CustomerWantDate;
                }
                else
                {
                    order.FsaOrdr.CustCmmtDt = model.CustomerCommitDate;
                    order.FsaOrdr.CustWantDt = model.CustomerWantDate;
                }
            }

            //List<OrdrMs> obj = new List<OrdrMs>();
            //obj.Add(orderMs);

            //return obj;
            //return orderMs;
            _orderMsRepo.Update(order.OrdrId, orderMs);
            if (_orderMsRepo.SaveAll() > 0)
            {
                _logger.LogInformation($"Order Milestone Updated.");
            }

            if (model.IsBillClearInstallDateChanged) {
                var rep = _orderMsRepo.UpdateBillClearDate(order.OrdrId, model.BillClearInstallDate);
                if (rep != 0)
                {
                    _logger.LogInformation("UpdateBillClearDate Success");                    
                }
            }

        }

        private void SaveCircuitMilestone(Ordr order, MilestoneViewModel orderMilestone, CircuitFormViewModel circuitMilestone)
        {
            var ckt = order.Ckt.Count > 0 ? order.Ckt.FirstOrDefault() : null;
            if (ckt == null)
            {
                ckt = new Ckt()
                {
                    OrdrId = order.OrdrId,
                    VndrCktId = circuitMilestone.VendorCircuitId,
                    LecId = circuitMilestone.LecId,
                    CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                    CreatDt = DateTime.Now,
                    IpAdrQty = circuitMilestone.NoOfIpAddresses,
                    RtngPrcol = circuitMilestone.RoutingProtocol,
                    CustIpAdr = circuitMilestone.CustomerIpAddresses,
                    ScaDet = circuitMilestone.SpecialCustomerAccessDetails,
                   // TrmtgCd = ckt.TrmtgCd,
                    TrmtgCd = true,
                    RltdVndrOrdrId = circuitMilestone.rltdVendorOrderId,
                    XCnnctPrvdr = circuitMilestone.XCnnctPrvdr,
                    TDwnAssn = circuitMilestone.TDwnAssn,
                    TDwnReq = circuitMilestone.TDwnReq,
                    CktMs = new List<CktMs>()
                };

                ckt = _circuitRepo.Create(ckt);
                _logger.LogInformation($"Circuit Created.");
            }
            else
            {
                ckt.VndrCktId = circuitMilestone.VendorCircuitId;
                ckt.LecId = circuitMilestone.LecId;
                ckt.IpAdrQty = circuitMilestone.NoOfIpAddresses;
                ckt.RtngPrcol = circuitMilestone.RoutingProtocol;
                ckt.CustIpAdr = circuitMilestone.CustomerIpAddresses;
                ckt.ScaDet = circuitMilestone.SpecialCustomerAccessDetails;
                ckt.TrmtgCd = true;
                ckt.RltdVndrOrdrId = circuitMilestone.rltdVendorOrderId;
                ckt.XCnnctPrvdr = circuitMilestone.XCnnctPrvdr;
                ckt.TDwnAssn = circuitMilestone.TDwnAssn;
                ckt.TDwnReq = circuitMilestone.TDwnReq;
                _circuitRepo.Update(ckt.CktId, ckt);
                if (_circuitRepo.SaveAll() > 0)
                {
                    _logger.LogInformation($"Circuit Updated.");
                }
            }

            var vlan = order.OrdrVlan.Count > 0 ? order.OrdrVlan.FirstOrDefault() : null;
            if (vlan == null)
            {
                vlan = new OrdrVlan()
                {
                    OrdrId = order.OrdrId,
                    VlanId = circuitMilestone.VlanId,
                    //VlanSrcNme = circuitMilestone.VlanSrcNme,
                    //VlanPctQty = circuitMilestone.VlanPctQty,
                    //TrmtgCd = circuitMilestone.TrmtgCd,
                    CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                    CreatDt = DateTime.Now,
                };

                vlan = _orderVlanRepo.Create(vlan);
                _logger.LogInformation($"Order Vlan Created.");
            }
            else
            {
                vlan.VlanId = circuitMilestone.VlanId;

                _orderVlanRepo.Update(vlan.OrdrVlanId, vlan);
                if (_orderVlanRepo.SaveAll() > 0)
                {
                    _logger.LogInformation($"Order Vlan Updated.");
                }
            }

            var cktMs = ckt.CktMs.Count > 0 ? ckt.CktMs.OrderByDescending(a => a.VerId).FirstOrDefault() : null;
            if (cktMs == null)
            {
                cktMs = new CktMs()
                {
                    CktId = ckt.CktId,
                    VerId = 1,
                    TrgtDlvryDt = orderMilestone.TargetDeliveryDate,
                    TrgtDlvryDtRecvDt = orderMilestone.TargetDeliveryDateReceived,
                    AccsDlvryDt = orderMilestone.AccessDeliveryDate,
                    AccsAcptcDt = orderMilestone.AccessAcceptDate,
                    CntrcTermId = orderMilestone.VendorContractTerm,
                    CntrcStrtDt = orderMilestone.VendorContractStartDate,
                    VndrCnfrmDscnctDt = orderMilestone.VendorConfirmedDisconnectDate,
                    CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                    CreatDt = DateTime.Now,
                };

                cktMs = _circuitMsRepo.Create(cktMs);
                _logger.LogInformation($"Circuit Milestone Created.");
            }
            else
            {
                //cktMs.VerId = 1;
                cktMs.TrgtDlvryDt = orderMilestone.TargetDeliveryDate;
                cktMs.TrgtDlvryDtRecvDt = orderMilestone.TargetDeliveryDateReceived;
                cktMs.AccsDlvryDt = orderMilestone.AccessDeliveryDate;
                cktMs.AccsAcptcDt = orderMilestone.AccessAcceptDate;
                cktMs.CntrcTermId = orderMilestone.VendorContractTerm;
                cktMs.CntrcStrtDt = orderMilestone.VendorContractStartDate;
                cktMs.VndrCnfrmDscnctDt = orderMilestone.VendorConfirmedDisconnectDate;

                _circuitMsRepo.Update(cktMs.CktId, cktMs);
                if (_circuitMsRepo.SaveAll() > 0)
                {
                    _logger.LogInformation($"Circuit Updated.");
                }
                //else
                //{
                //    return new { Message = "Circuit Milestone Could Not Be Updated." };
                //}
            }

            //return null;
        }

        private List<StateMachine> GetStateMachines(Ordr order, OrderDetailViewModel orderDetails, bool isBypassVendorOrig, bool isBypassVendorTerm)
        {
            int orderId = order.OrdrId;
            OrdrMs orderMs = order.OrdrMs.FirstOrDefault();
            Ckt ckt = order.Ckt.FirstOrDefault();
            List<StateMachine> sm = new List<StateMachine>();

            var productTypeDesc = orderDetails.ProductTypeDesc;
            var orderType = orderDetails.OrderTypeDesc.ToUpper();
            var parentOrderType = orderDetails.FsaOrderTypeDesc;

            if (orderMs.VldtdDt != null)
            {
                sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIOrderValidatedDate));
            }

            if (isBypassVendorOrig || isBypassVendorTerm)
            {
                sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIOrderSenttoVendorDate, "bypassing xNCI Order Sent to Vendor Date milestone."));
                sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIOrderAcknowledgedbyVendorDate, "bypassing xNCI Order Acknowledged by Vendor Date milestone."));
            }
            else
            {
                if (CheckVendorSentDate(orderId))
                {
                    sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIOrderSenttoVendorDate));
                }

                if (CheckVendorAckDate(orderId))
                {
                    sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIOrderAcknowledgedbyVendorDate));
                }
            }

            if (ckt != null)
            {
                CktMs cktMs = ckt.CktMs.OrderByDescending(a => a.VerId).FirstOrDefault();
                if (orderType == "CHANGE")
                {
                    if (cktMs.TrgtDlvryDt != null)
                    {
                        sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCITargetDeliveryDate));
                    }
                    if (cktMs.TrgtDlvryDtRecvDt != null)
                    {
                        sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCITargetDeliveryDateReceivedDate));
                    }
                }
                else if (orderType == "DISCONNECT")
                {
                    if (cktMs.VndrCnfrmDscnctDt != null)
                    {
                        sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIVendorConfirmDisconnectDate));
                    }
                }
                else
                {
                    if (cktMs.TrgtDlvryDt != null)
                    {
                        sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCITargetDeliveryDate));
                    }
                    if (cktMs.TrgtDlvryDtRecvDt != null)
                    {
                        sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCITargetDeliveryDateReceivedDate));
                    }
                    if (cktMs.AccsDlvryDt != null)
                    {
                        sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIAccessDeliveryDate));
                    }
                    if (cktMs.AccsAcptcDt != null)
                    {
                        sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIAccessAcceptedDate));
                    }
                }
            }

            if (orderMs.VndrCnclnDt != null)
            {
                sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCICnclReady));
            }

            if (orderMs.OrdrDscnctDt != null)
            {
                sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIDisconnectCompletedDate));
            }

            if (orderMs.OrdrBillClearInstlDt != null)
            {
                sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIOrderInstallDate));

                if (orderType == "CHANGE" || order.OrdrCatId == 6)
                {
                    sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIBillClearInstallDate));
                }
            }

            if (orderMs.XnciCnfrmRnlDt != null)
            {
                sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCIConfirmedRenewalDate));
            }
            // Supposed to be for Renewal, but no renewal info provided from user
            //else if (orderMs.XnciCnfrmRnlDt == null)
            //{

            //}

            if (orderMs.CustAcptcTurnupDt != null)
            {
                sm.Add(CreateStateMachine(orderId, (int)Tasks.xNCICloseDate));
            }

            return sm;
        }

        private bool CheckVendorSentDate(int orderId)
        {
            bool isTerminating = true; // Always true due to IPL being ignored. See TransportOrderDB.CheckForSentMS on old COWS
            int count = _vendorOrderRepo
                .Find(a =>
                    a.OrdrId == orderId &&
                    a.TrmtgCd == isTerminating &&
                    a.VndrOrdrMs.Any(b => b.SentToVndrDt != null))
                .Count();

            if (count > 0)
                return true;
            else
                return false;
        }

        private bool CheckVendorAckDate(int orderId)
        {
            bool isTerminating = true; // Always true due to IPL being ignored. See TransportOrderDB.CheckForSentMS on old COWS
            int count = _vendorOrderRepo
                .Find(a =>
                    a.OrdrId == orderId &&
                    a.TrmtgCd == isTerminating &&
                    a.VndrOrdrMs.Any(b => b.AckByVndrDt != null))
                .Count();

            if (count > 0)
                return true;
            else
                return false;
        }

        private StateMachine CreateStateMachine(int orderId, int taskId, string comments = null)
        {
            StateMachine sm = new StateMachine();
            sm.OrderId = orderId;
            sm.TaskId = taskId;
            sm.UserId = (taskId == 201 || taskId == 202) && (comments ?? "").Contains("bypass") ? 1 : _loggedInUser.GetLoggedInUserId();
            sm.Comments = (comments != null ? comments : $"Milestone: { GetMilestoneInfo(taskId) } completed.");
            //if (sComments != null)
            //    sm.sComments = sComments;//"Bypassing Vendor Order Milestones.";
            //else
            //    sm.sComments = bOrdrCncld == false ? "Milestone: " + GetxNCIMilestoneInfo(iTaskID) + " completed." : "Cancel Milestone: " + GetxNCIMilestoneInfo(iTaskID) + " completed.";
            return sm;
        }

        private string GetMilestoneInfo(int taskId)
        {
            try
            {
                switch (taskId)
                {
                    case (int)Tasks.xNCIOrderValidatedDate:
                        return "Order Validated Date";
                    case (int)Tasks.xNCIOrderSenttoVendorDate:
                        return "Order Sent to Vendor Date";
                    case (int)Tasks.xNCIOrderAcknowledgedbyVendorDate:
                        return "Order Acknowledged by Vendor Date";
                    case (int)Tasks.xNCIDisconnectCompletedDate:
                        return "Disconnect Completed Date";
                    case (int)Tasks.xNCITargetDeliveryDate:
                        return "Target Delivery Date";
                    case (int)Tasks.xNCITargetDeliveryDateReceivedDate:
                        return "Target Delivery Date Received Date";
                    case (int)Tasks.xNCIAccessDeliveryDate:
                        return "Access Delivery Date";
                    case (int)Tasks.xNCIAccessAcceptedDate:
                        return "Access Accepted Date";
                    case (int)Tasks.xNCIVendorConfirmDisconnectDate:
                        return "Vendor Confirmed Disconnect Date";
                    case (int)Tasks.xNCIOrderInstallDate:
                        return "Order Install Date";
                    case (int)Tasks.xNCICnclReady:
                        return "Vendor Cancellation Date";
                    case (int)Tasks.xNCICloseDate:
                        return "Close Date";
                    case (int)Tasks.xNCIBillClearInstallDate:
                        return "Bill Clear Date";
                    default:
                        return "No Milestone Info";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CompleteActiveTask(IEnumerable<StateMachine> list)
        {
            foreach (StateMachine sm in list)
            {
                bool isActive = _activeTaskRepo
                    .Find(a => a.OrdrId == sm.OrderId && a.TaskId == sm.TaskId)
                    .Any(a => a.StusId == 0);

                if (isActive)
                {
                    _activeTaskRepo.CompleteActiveTask(sm);

                    if (sm.TaskId == 203 || sm.TaskId == 207)
                    {
                        _nrmRepo.SendASRMessageToNRMBPM(sm.OrderId, sm.TaskId == 203 ? "FOC" : "BILL_CLEAR_DT", 1);

                        var orderNote = new OrdrNte
                        {
                            OrdrId = sm.OrderId,
                            NteTypeId = 11,
                            CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                            CreatDt = DateTime.Now
                        };

                        if (sm.TaskId == 203)
                        {
                            // Insert Notes for FOC
                            orderNote.NteTxt = "FOC Messages sent back to NRM BPM";
                        }
                        else
                        {
                            // Insert Notes for Bill Clear
                            orderNote.NteTxt = "Bill clear message sent back to NRM BPM";
                        }

                        _orderNoteRepo.CreateNote(orderNote);
                    }
                }
            }
        }


        [HttpGet("{id}/WgData")]
        public IActionResult GetWgData([FromRoute] int id, [FromQuery] int usrPrfId)
        {
            _logger.LogInformation($"Get Workgroup Data by Order Id: {id}");

            var data = _repo.GetWgData(id, _loggedInUser.GetLoggedInUserId(), usrPrfId, _loggedInUser.GetLoggedInUserCsgLvlId());

            if (data != null)
            {
                return Ok(data);
            }
            else
            {
                _logger.LogInformation($"Workgroup Data not found.");
                return NotFound(new { Message = $"Workgroup Data not found." });
            }
        }

        [HttpGet("{id}/AttachH5Folder")]
        public IActionResult AttachH5Folder([FromRoute] int id, [FromQuery] int h5FolderId)
        {
            _logger.LogInformation($"Attach H5 Folder {h5FolderId} for Order Id: {id}");

            _repo.AttachH5Folder(id, h5FolderId, _loggedInUser.GetLoggedInUserId());

            _logger.LogInformation($"Attached H5 Folder {h5FolderId} for Order Id: {id}");

            return Ok();
        }
        private string GetSystemConfig(string param)
        {
            var systemConfig = _systemConfigRepo.Find(a => a.PrmtrNme == param).SingleOrDefault();

            if (systemConfig != null)
            {
                return systemConfig.PrmtrValuTxt;
            }

            return String.Empty;
        }
        private List<string> GetVendorCodeList(string param)
        {
            var result = GetSystemConfig(param).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(s => s.Trim())
            .ToList();

            return result;
        }

        [HttpGet("{id}/TransportOrder")]
        public IActionResult GetTransportOrder([FromRoute] int id)
        {
            _logger.LogInformation($"Get Transport Order By Id: {id}");

            TransportOrderDetails transportOrder = _repo.GetTransportOrder(id);

            if (transportOrder != null)
            {
                return Ok(transportOrder);
            }
            else
            {
                _logger.LogInformation($"Transport Order not found.");
                return NotFound(new { Message = $"Transport Order not found." });
            }
        }

        [HttpGet("{id}/ParentOrderType")]
        public IActionResult GetParentOrderType([FromRoute] int id)
        {
            _logger.LogInformation($"Get Parent Order Type By Id: {id}");

            var data = _repo.GetOrderDetails(i => i.OrdrId == id && (i.RecStusId == 0 || i.RecStusId == 1));

            if (data != null)
            {
                OrderDetailViewModel orderDetails = _mapper.Map<OrderDetailViewModel>(data.SingleOrDefault());

                if (orderDetails != null)
                {
                    if (orderDetails.OrderTypeDesc == "CANCEL" && orderDetails.OrderCategoryId == 2)
                    {
                        return Ok(orderDetails.FsaOrderTypeDesc);
                    }
                    //else
                    //{
                    //    return Ok(orderDetails.FsaOrderTypeDesc);
                    //}
                }

                return Ok();
            }
            else
            {
                _logger.LogInformation($"Order not found.");
                return NotFound(new { Message = $"Order not found." });
            }
        }

        [HttpGet("{id}/RelatedOrderId")]
        public IActionResult GetRelatedOrderId([FromRoute] int id)
        {
            _logger.LogInformation($"Get Related Order Id of Order by Id: {id}");

            var mainOrder = _repo.GetById(id);

            if (mainOrder.OrdrCatId == 2 && mainOrder.FsaOrdr.ReltdFtn != null)
            {
                var relatedOrder = _repo.Find(a => a.FsaOrdr.Ftn == mainOrder.FsaOrdr.ReltdFtn).FirstOrDefault();

                if (relatedOrder != null)
                {
                    return Ok(relatedOrder.OrdrId);
                }
            }

            _logger.LogInformation($"Related Order Id not found.");
            return Ok();
        }

        [HttpGet("UpdateCustomerTurnUpTask/{id}")]
        public IActionResult UpdateCustomerTurnUpTask([FromRoute] int id)
        {
            InsertNote(id, 12, "Order re-opened for editing.");
            return Ok(_repo.UpdateCustomerTurnUpTask(id));
        }

        [HttpGet("CheckIfOrderIsBAR/{id}")]
        public IActionResult CheckIfOrderIsBAR([FromRoute] int id)
        {
            return Ok(_activeTaskRepo.CheckIfOrderIsBAR(id));
        }  

        [HttpPost("RequestToLogicallis")]
        public IActionResult RequestToLogicallis([FromBody] OrderNoteViewModel model)
        {
            int id = model.OrdrId;
            string notes = model.NteTxt;
            Ordr order = _repo.GetById(id);

            _logger.LogInformation($"Starting CPE Disconnect Request for Order Id: {id}");
            // Call SP for Data Fetching
            LogicallisData obj = _repo.RequestToLogicallis(id).FirstOrDefault();

            // Transform data to EmailReq
            XElement CPEDisconnectEmail = new XElement("CPEDisconnectEmail");

            XElement CustomerDetails = new XElement("CustomerDetails",
                new XElement("Customer", obj.Customer),
                new XElement("Address1", obj.Address1),
                new XElement("Address2", obj.Address2),
                new XElement("Address3", obj.Address3),
                new XElement("City", obj.City),
                new XElement("Country", obj.Country),
                new XElement("Province", obj.Province),
                new XElement("State", obj.State),
                new XElement("Zip", obj.Zip),
                new XElement("Building", obj.Building),
                new XElement("Floor", obj.Floor),
                new XElement("Room", obj.Room),
                new XElement("SiteContact", obj.SiteContact),
                new XElement("SiteOfficePhone", obj.SiteOfficePhone),
                new XElement("SiteMobilePhone", obj.SiteMobilePhone),
                new XElement("SiteEmail", obj.SiteEmail));
            CPEDisconnectEmail.Add(CustomerDetails);

            XElement CPE = new XElement("CPE",
                new XElement("OriginalPO", obj.OriginalPO),
                new XElement("DisconnectionDate", obj.DisconnectionDate),
                new XElement("SpecialRemarks", notes));
            CPEDisconnectEmail.Add(CPE);

            _logger.LogInformation($"Creating email with Body: {CPEDisconnectEmail.ToString()}");
            // Add EmailReq to Context
            var emailTo = _systemConfigRepo.Find(a => a.PrmtrNme == "CPEDiscRequestEmail").FirstOrDefault();

            EmailReq email = new EmailReq();
            email.CreatDt = DateTime.Now;
            email.EmailBodyTxt = CPEDisconnectEmail.ToString();
            email.EmailReqTypeId = (int)EmailREQType.CPEDisconnectRequestEmail;
            email.EmailSubjTxt = $"CPE Rental Disconnect Request - {order.FsaOrdr.Ftn}";
            email.OrdrId = id;
            email.StusId = (int)EmailStatus.Pending;
            email.EmailListTxt = emailTo != null ? emailTo.PrmtrValuTxt : "";
            email.EmailCcTxt = _loggedInUser.GetLoggedInUser().EmailAdr;

            var result = _emailReqRepository.Create(email);

            if (result != null)
            {
                _logger.LogInformation($"Created Email Request with Id: {email.EmailReqId}");
                return Ok();
            }

            return BadRequest(new { Message = "Email Request Could Not be Created" });
        }
    }
}
