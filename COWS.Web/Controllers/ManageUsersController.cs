﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/ManageUsers")]
    [ApiController]
    public class ManageUsersController : ControllerBase
    {
        private readonly IUserRepository _userRepo;
        private readonly IUserProfileRepository _userProfileRepo;
        private readonly IMapUserProfileRepository _mapUserProfileRepo;
        private readonly IOrderActionRepository _orderActionRepo;
        private readonly IWFMUserAssignmentRepository _wfmUserRepo;
        private readonly ILogger<ManageUsersController> _logger;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IMapper _mapper;

        public ManageUsersController(IMapper mapper,
                               IUserRepository userRepo,
                               IUserProfileRepository userProfileRepo,
                               IMapUserProfileRepository mapUserProfileRepo,
                               IOrderActionRepository orderActionRepo,
                               IWFMUserAssignmentRepository wfmUserRepo,
                               ILogger<ManageUsersController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _userRepo = userRepo;
            _userProfileRepo = userProfileRepo;
            _mapUserProfileRepo = mapUserProfileRepo;
            _orderActionRepo = orderActionRepo;
            _wfmUserRepo = wfmUserRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserProfileViewModel>> Get()
        {
            var userProfile = _mapper.Map<IEnumerable<UserProfileViewModel>>(_userProfileRepo
                                                                            .Find(a => a.MapUsrPrf.Where(b => b.RecStusId == 1)
                                                                                .Count() > 0)
                                                                            .ToList());
            return Ok(userProfile);
        }

        [HttpGet("{userId}")]
        public ActionResult<IEnumerable<MapUserProfileViewModel>> Get(int userId)
        {
            IEnumerable<MapUserProfileViewModel> mapUserProfile;
            mapUserProfile = _mapper.Map<IEnumerable<MapUserProfileViewModel>>(_mapUserProfileRepo
                                                                        .Find(a => a.UserId == userId && a.RecStusId == 1).ToList());

            return Ok(mapUserProfile);
        }

        [HttpGet("getOrderActions")]
        public IActionResult GetOrderActions()
        {
            var userProfile = _orderActionRepo.GetAll();

            return Ok(userProfile);
        }

        [HttpGet("getWFMUserAssignments")]
        public IActionResult GetWFMUserAssignments([FromQuery] int userID, [FromQuery] int userProfileID)
        {
            return Ok(_wfmUserRepo.getWFMUserAssignments(userID, userProfileID));
        }

        [HttpGet("updateWFMUserAssignments/{selectedAssignmentKeys}")]
        public void UpdateWFMUserAssignments(string selectedAssignmentKeys)
        {
            var loggedInUser = _loggedInUser.GetLoggedInUser();

            _wfmUserRepo.DeleteWFMUserAssignments(selectedAssignmentKeys, loggedInUser.UserId);
        }

        [HttpPost]
        public IActionResult Post([FromBody] InsertWFMUserAssignmentsViewModel model)
        {
            _logger.LogInformation($"Create Manage Users for : { model.UserId }.");

            var obj = _mapper.Map<InsertWFMUserAssignments>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
            }

            if (_wfmUserRepo.InsertWFMAssignment(obj))
            {
                _logger.LogInformation($"Inserted WFM User Assignments. { JsonConvert.SerializeObject(model).ToString() } ");
                return Ok(true);
            }
            else
            {
                return BadRequest(new { Message = "Could Not Inserted WFM User Assignments." });
            }
        }

        [HttpPost("updateUserPermissions")]
        public IActionResult UpdateUserPermissions([FromBody] UserProfileViewModel model)
        {
            UserViewModel userData = _mapper.Map<UserViewModel>(_userRepo.GetById(model.UserId));

            //_wfmUserRepo.DeleteWFMUserAssignments("All", model.UserId);
            _logger.LogInformation($"DeleteWFMUserAssignments for UserID: { model.UserId }.");

            if (model.NewStatus != null && model.NewStatus != "")
            {
                userData.RecStusId = Convert.ToByte(model.NewStatus == "1" ? 1 : 0);
                var updatedUserData = _mapper.Map<LkUser>(userData);
                _userRepo.Update(model.UserId, updatedUserData);
                _logger.LogInformation($"Updated User RecStatusID for UserID: { model.UserId } to RecStatusID { model.NewStatus }");

                if (model.NewStatus == "0")
                {
                    _wfmUserRepo.ResetOldWFMAssignments(model.UserId);
                    _logger.LogInformation($"ResetOldWFMAssignments for UserID: { model.UserId }.");
                }
            }

            if (model.NewManagerAdid != null && model.NewManagerAdid != "")
            {
                userData.MgrAdid = model.NewManagerAdid;

                var updatedUserData = _mapper.Map<LkUser>(userData);
                _userRepo.Update(model.UserId, updatedUserData);
                _logger.LogInformation($"Updated User Manager ADID for UserID: { model.UserId } to { model.NewManagerAdid }");
            }

            if (model.NewAutoAssignment != null && model.NewAutoAssignment != "")
            {
                _wfmUserRepo.Update(model.UserId, Convert.ToByte(model.NewAutoAssignment == "1" ? 1 : 0));
                _logger.LogInformation($"Updated Assignment Type for UserID: { model.UserId } to Type { model.NewAutoAssignment }");
            }

            return Ok();
        }
    }
}