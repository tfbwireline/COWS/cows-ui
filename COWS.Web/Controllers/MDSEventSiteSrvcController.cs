﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSEventSiteSrvc")]
    [ApiController]
    public class MDSEventSiteSrvcController : ControllerBase
    {
        private readonly IMDSEventSiteSrvcRepository _repo;
        private readonly ILogger<MDSEventSiteSrvcController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MDSEventSiteSrvcController(IMapper mapper,
                               IMDSEventSiteSrvcRepository repo,
                               ILogger<MDSEventSiteSrvcController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"MDSEventSiteSrvc by EventId: { id }.");

            var obj = _repo.GetMDSEventSiteSrvcByEventId(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<GetMdsEventSiteSrvcViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDSEventSiteSrvc by EventId: { id } not found.");
                return NotFound(new { Message = $"MDSEventSiteSrvc by EventId: { id } not found." });
            }
        }
    }
}