﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MSS")]
    [ApiController]
    public class MSSController : ControllerBase
    {
        private readonly IMSSRepository _repo;
        private readonly ILogger<MSSController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MSSController(IMapper mapper,
                               IMSSRepository repo,
                               ILogger<MSSController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("GetSdeView")]
        public IActionResult GetSdeView([FromQuery] int statusID, [FromQuery] int userID)
        {
            return Ok(_repo.GetSdeView(statusID, userID));
        }

        [HttpGet("GetAvailableUsers")]
        public IActionResult GetAvailableUsers()
        {
            return Ok(_repo.GetAvailableUsers());
        }

        [HttpGet("GetViewOptions")]
        public IActionResult GetViewOptions()
        {
            return Ok(_repo.GetViewOptions());
        }

        [HttpGet("GetSDEProductTypesForAdmin")]
        public IActionResult GetSDEProductTypesForAdmin()
        {
            return Ok(_repo.GetSDEProductTypesForAdmin());
        }

        [HttpGet("GetSDEProductTypesByIDForAdmin")]
        public IActionResult GetSDEProductTypesByIDForAdmin([FromQuery] int id)
        {
            return Ok(_repo.GetSDEProductTypesByIDForAdmin(id));
        }

        // [HttpPost("InsertProductType")]
        // //[HttpPost]
        // public IActionResult InsertProductType([FromBody] MssProductTypeViewModel product)
        // {
        //         return Ok(_repo.InsertProductType(product.SdePrdctTypeNme,product.SdePrdctTypeDesc,product.OrdrBySeqNbr,product.RecStusId,product.OutsrcdCd));
        //     }
        // [HttpPut("UpdateProductType")]
        // //[HttpPost]
        //// [HttpPut("{id}")]
        // public IActionResult UpdateProductType(int id, [FromBody] MssProductTypeViewModel product)
        //     {
        //         return Ok(_repo.UpdateProductType(id, product.SdePrdctTypeNme, product.SdePrdctTypeDesc, product.OrdrBySeqNbr, product.RecStusId, product.OutsrcdCd));
        //     }
        // [HttpGet("DeleteProductType")]
        // public IActionResult DeleteProductType(int id)
        // {
        //     return Ok(_repo.DeleteProductType(id));
        // }
        [HttpPost]
        public ActionResult Post([FromBody] MssProductTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                LkSdePrdctType mss = _mapper.Map<LkSdePrdctType>(model);
                //mss.SdePrdctTypeId = model.SdePrdctTypeId;
                mss.RecStusId = (byte)model.RecStusId;
                mss.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                mss.ModfdByUserId = _loggedInUser.GetLoggedInUserId();

                return Ok(_repo.InsertProductType(mss));
            }
            return BadRequest(new { Message = "MSS Product Type cannot be Created." });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] MssProductTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                LkSdePrdctType mss = _mapper.Map<LkSdePrdctType>(model);
                //mss.SdePrdctTypeId = model.SdePrdctTypeId;

                mss.ModfdByUserId = _loggedInUser.GetLoggedInUserId();

                return Ok(_repo.UpdateProductType(id, mss));
            }
            return BadRequest(new { Message = "MSS Product Type cannot be Updated." });
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating Event Type by Id: { id }.");

            var type = _repo.Find(s => s.SdePrdctTypeId == id);
            if (type != null)
            {
                var obj = _mapper.Map<LkSdePrdctType>(type.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = 0;
                }

                _repo.Update(id, obj);

                // Update Cache
                // _cache.Remove(CacheKeys.EventTypeList);
                //Get();
                _logger.LogInformation($"Event Type by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Event Type by Id: { id } not found.");
            }
        }

        //[HttpDelete("{id}")]
        //public ActionResult Delete(int id)
        //{
        //    return Ok(_repo.DeleteProductType(id));

        //    }
        [HttpGet("GetByID/{id}")]
        public IActionResult GetByID(int id)
        {
            return Ok(_repo.GetByID(id));
        }

        [HttpGet("GetSDEProductTypes")]
        public IActionResult GetSDEProductTypes()
        {
            return Ok(_repo.GetSDEProductTypes());
        }

        [HttpGet("GetWorkflowStatus/{id}")]
        public IActionResult GetWorkflowStatus(int id)
        {
            return Ok(_repo.GetWorkflowStatus(id));
        }

        [HttpGet("GetSDENotes/{id}")]
        public IActionResult GetSDENotes(int id)
        {
            return Ok(_repo.GetSDENotes(id));
        }

        [HttpGet("GetSDEDocs/{id}")]
        public IActionResult GetSDEDocs(int id)
        {
            return Ok(_repo.GetSDEDocs(id));
        }

        [HttpGet("GetSDEProds/{id}")]
        public IActionResult GetSDEProds(int id)
        {
            return Ok(_repo.GetSDEProds(id));
        }

        [HttpPost("AddSDENote")]
        public IActionResult AddSDENote(SdeOpptntyDoc model)
        {
            var success = _repo.AddSDENote(model.SdeOpptntyId, model.FileNme, _loggedInUser.GetLoggedInUserId());
            if (success)
            {
                return Ok(success);
            }
            return BadRequest(new { Message = "Error occured while adding a new Note." });
        }

        [HttpGet("GetCowsAppCfgValue")]
        public IActionResult GetCowsAppCfgValue([FromQuery] string cfgKeyName)
        {
            return Ok(_repo.GetCowsAppCfgValue(cfgKeyName));
        }

        [HttpGet("GetAllSdeProductType")]
        public IActionResult GetAllSdeProductType()
        {
            IEnumerable<SdeProductTypeViewModel> list;

            _logger.LogInformation($"Get SDE Product Types");

            var data = _repo.GetAllSdeProductType(i => i.RecStusId == 1);

            if (data != null)
            {
                list = _mapper.Map<IEnumerable<SdeProductTypeViewModel>>(data);
                CacheManager.Set(_cache, CacheKeys.SdeProductTypeList, list);
            }
            else
            {
                _logger.LogInformation($"SDE Product Types not found.");
                return NotFound(new { Message = $"SDE Product Types not found." });
            }

            return Ok(list);
        }

        [HttpPost("Insert")]
        public IActionResult Insert([FromBody] SdeOpportunityViewModel model)
        {
            if (ModelState.IsValid)
            {
                SdeOpportunity mss = _mapper.Map<SdeOpportunity>(model);
                var loggedInUser = _loggedInUser.GetLoggedInUser();

                for (var i = 0; i < model.SdeDocs.Count; i++)
                {
                    var doc = model.SdeDocs[i];
                    if (!String.IsNullOrEmpty(doc.Base64string))
                    {
                        mss.SdeDocs[i].FileContent = _repo.byteStringToByteArray(doc.Base64string);
                    }
                }
                return Ok(_repo.Insert(mss, loggedInUser.UserId));
            }
            return BadRequest(new { Message = "MSS  cannot be Inserted." });
        }

        [HttpPut("Update")]
        public IActionResult Update([FromBody] SdeOpportunityViewModel model)
        {
            if (ModelState.IsValid)
            {
                SdeOpportunity mss = _mapper.Map<SdeOpportunity>(model);
                var loggedInUser = _loggedInUser.GetLoggedInUser();

                for(var i = 0; i < model.SdeDocs.Count; i++)
                {
                    var doc = model.SdeDocs[i];
                    if(!String.IsNullOrEmpty(doc.Base64string))
                    {
                        mss.SdeDocs[i].FileContent = _repo.byteStringToByteArray(doc.Base64string);
                    }
                }

                return Ok(_repo.Update(mss, loggedInUser.UserId));
            }
            return BadRequest(new { Message = "MSS  cannot be Inserted." });
        }


        [HttpGet("DownloadSdeDoc/{id}")]
        public IActionResult Download([FromRoute] int id)
        {
            //_logger.LogInformation($"Search Currency File by Id: { id }.");

            var obj = _repo.GetSDEDocByID(id);
            if (obj != null)
            {
                try
                {
                    string noteText = string.Format("{0} is downloaded.", obj.FileName);

                    _repo.AddSDENote(obj.SdeOpptntyDocId, noteText, _loggedInUser.GetLoggedInUserId());
                    return new FileContentResult(obj.FileContent, "application/octet")
                    {
                        FileDownloadName = obj.FileName
                    };
                }
                catch (Exception)
                {
                    return NotFound(new { Message = $"SDE Doc File Id: { id } not found." });
                }
            }
            else
            {
                _logger.LogInformation($"SDE Doc  File by Id: { id } not found.");
                return NotFound(new { Message = $"SDE Doc  File Id: { id } not found." });
            }
        }

        //public ActionResult DeleteSde(int sdeID)
        //{
        //    return Ok(_repo.Delete(sdeID));
        //}
    }
}