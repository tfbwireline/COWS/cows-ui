﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Countries")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly ICountryRepository _repo;
        private readonly ILogger<CountryController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public CountryController(IMapper mapper,
                               ICountryRepository repo,
                               ILogger<CountryController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = memoryCache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CountryViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CountryViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.CtryNme));
            return Ok(list);
        }

        [HttpGet("{code}")]
        public IActionResult Get([FromRoute] string code)
        {
            _logger.LogInformation($"Search Country by Code: { code }.");

            var obj = _repo.Find(s => s.CtryCd == code);
            if (obj != null)
            {
                return Ok(_mapper.Map<CountryViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Country by Code: { code } not found.");
                return NotFound(new { Message = $"Country by Code: { code } not found." });
            }
        }

        [HttpGet("Lookup")]
        public ActionResult<IEnumerable<object>> GetForLokkup()
        {
            if (!_cache.TryGetValue(CacheKeys.CountryListLookup, out IEnumerable<object> list))
            {
                list = _repo.GetAllForLookup().ToList();

                CacheManager.Set(_cache, CacheKeys.CountryListLookup, list);
            }

            return Ok(list);
        }

        [HttpPost("{code}/Region")]
        public IActionResult Post([FromBody] int regionId, [FromRoute] string code)
        {
            _logger.LogInformation($"Update Country by Code: { code }.");

            var obj = _repo.Find(s => s.CtryCd == code).SingleOrDefault();
            if (obj != null)
            {
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.RgnId = Convert.ToInt16(regionId);
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                }

                _repo.UpdateRegion(code, obj);

                // Update Cache
                _cache.Remove(CacheKeys.CountryList);
                Get();

                var json = new JsonSerializerSettings()
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    Formatting = Formatting.Indented
                };
                _logger.LogInformation($"Country Updated. { JsonConvert.SerializeObject(obj, json) } ");
                return Created($"api/Countries/{ code }", obj);
            }

            return BadRequest(new { Message = "Country Could Not Be Updated." });
        }
    }
}