﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.Controllers
{
    [Route("api/CPECLLI")]
    [ApiController]
    public class CpeClliController : ControllerBase
    {
        private readonly ICpeClliRepository _repo;
        private readonly ILogger<CpeClliController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;


        public CpeClliController(IMapper mapper,
                               ICpeClliRepository repo,
                               ILogger<CpeClliController> logger,
                               ILoggedInUserService loggedInUser
                               )
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<DataTable> GetAllRequestData([FromQuery] string statusId, [FromQuery] string clliId, [FromQuery] string zipCd)
        {
            var dataSet = _repo.GetAllRequestData(statusId, clliId, zipCd);
            if (dataSet != null)
            {
                return Ok(dataSet);
            }
            return BadRequest(new { Message = "No CPE CLLI Request found." });
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            var obj = _repo.Find(x => x.CpeClliId == id);
            if (obj != null)
            {

                var record = _mapper.Map<CpeClliRequestDetailsViewModel>(obj.SingleOrDefault());
                return Ok(record);
            }
            else
            {
                _logger.LogInformation($"CPE CLLI Request by Id: { id } not found.");
                return NotFound(new { Message = $"CPE CLLI Request Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] CpeClliRequestDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<CpeClli>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

       
                var newData = new CpeClli();
                newData = _repo.Create(obj);

                if (newData != null)
                {
                    _logger.LogInformation($"CPE CLLI Request Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/CPECLLI/{ newData.CpeClliId }", model);
                }
            }

            return BadRequest(new { Message = "Device Manufacturer Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] CpeClliRequestDetailsViewModel model)
        {
            _logger.LogInformation($"CPE CLLI Request Id: { id }.");

            var obj = _mapper.Map<CpeClli>(model);

            var duplicate = _repo.Find(i => i.CpeClliId == id).SingleOrDefault();
            if (duplicate != null)
            {
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                }
                _repo.Update(id, obj);
            }

            _logger.LogInformation($"CPE CLLI Request Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/CPECLLI/{ id }", model);
        }

        [HttpGet("GetClliID")]
        public string GetClliID([FromQuery] string cityNme, [FromQuery] string stateCode)
        {
            string clliId = _repo.getClliID(cityNme, stateCode);
            return clliId;
        }

        [HttpGet("CheckClliIdIfExist")]
        public bool CheckClliIdIfExist([FromQuery] int cpeClliId, [FromQuery] string clliId)
        {
            bool isExist = _repo.checkClliIdIfExist(cpeClliId, clliId);
            return isExist;
        }
    }
}
