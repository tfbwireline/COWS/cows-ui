﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/NccoOrder")]
    [ApiController]
    public class NccoOrderController : ControllerBase
    {
        private readonly INccoOrderRepository _repo;
        private readonly ILogger<NccoOrderController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IL2PInterfaceService _l2pInterface;

        public NccoOrderController(IMapper mapper,
                               INccoOrderRepository repo,
                               ILogger<NccoOrderController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache,
                               IL2PInterfaceService l2pInterface)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _l2pInterface = l2pInterface;
        }

        [HttpGet]
        public ActionResult<IEnumerable<NccoOrderViewModel>> Get()
        {
            IEnumerable<NccoOrderViewModel> list;
            //if (!_cache.TryGetValue(CacheKeys.EventTypeList, out list))
            //{
            list = _mapper.Map<IEnumerable<NccoOrderViewModel>>(_repo
                .GetAll()
                .OrderBy(s => s.OrdrId));

            //    CacheManager.Set(_cache, CacheKeys.EventTypeList, list);
            //}

            return Ok(list);
        }

        /// <summary>
        /// https://localhost:44314/api/NccoOrder/NccoOrderViewModel/value
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetNCCOOrderData/{orderId}")]
        public ActionResult<NccoOrderViewModel> GetNCCOOrderData([FromRoute] int orderId)
        {
            NccoOrderViewModel list;
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();

            list = _mapper.Map<NccoOrderViewModel>(_repo.GetNCCOOrderData(orderId, loggedInUserCSGLevel));
            if (list != null)
            {
                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"NCCO Order by orderId: { orderId } not found.");
                return NotFound(new { Message = $"NCCO Order by orderId: { orderId } not found." });
            }
        }

        [HttpGet("{orderId}")]
        public IActionResult Get([FromRoute] int orderId)
        {
            _logger.LogInformation($"Search Event History by Event Id: { orderId }.");

            var obj = _repo.Find(s => s.OrdrId == orderId).OrderByDescending(a => a.OrdrId);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<NccoOrderViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Event History by Event Id: { orderId } not found.");
                return NotFound(new { Message = $"Event History by Event Id: { orderId } not found." });
            }
        }

        [HttpGet("FindOrdNccoOrder")]
        public IActionResult FindOrdNccoOrder([FromQuery] string sortExpression, [FromQuery] string searchCriteria)
        {
            _logger.LogInformation($"Search NCCO Order.");
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            var obj = _repo.Select(sortExpression, searchCriteria, loggedInUserCSGLevel, loggedInUser.UserId);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<NccoOrderViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"NCCO Order not found.");
                return NotFound(new { Message = $"NCCO Order not found." });
            }
        }

        [HttpGet("GetNCCOWGDetails")]
        public ActionResult<DataTable> GetNCCOWGDetails([FromQuery] int orderId)
        {
            //DataTable dt = new DataTable();
            var dataTable = _repo.GetNCCOWGDetails(orderId);
            if (dataTable != null)
            {
                return Ok(dataTable);
            }
            return BadRequest(new { Message = "No Order Information found for this Order." });
            //var table1 = result2.Tables[0];
            //var table2 = result2.Tables[1];
            //return (table1, table2);
        }

        [HttpGet("PassUserCSGLevel/{csgLvlId}")]
        public IActionResult PassUserCSGLevel([FromRoute] int csgLvlId)
        {
            _logger.LogInformation($"Get Current User CSG Level");
            var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            if (loggedInUserCSGLevel != null && loggedInUserCSGLevel > 0)
            {
                if (csgLvlId != null && csgLvlId > 0)
                {
                    if (loggedInUserCSGLevel < csgLvlId)
                    {
                        _logger.LogInformation($"Insufficient CSG privileges to view secured customer's order data.");
                        return BadRequest(new { Message = "Insufficient CSG privileges to view secured customer's order data." });
                    }
                    else
                    {
                        return Ok(true);
                    }
                }
                else
                {
                    return Ok(true);
                }
            }
            else
            {
                _logger.LogInformation($"Insufficient CSG privileges to view secured customer's order data.");
                return BadRequest(new { Message = "Insufficient CSG privileges to view secured customer's order data." });
            }
        }

        [HttpGet("FindAllNccoOrder")]
        public IActionResult FindAllNccoOrder([FromQuery] string sortExpression, [FromQuery] string searchCriteria)
        {
            _logger.LogInformation($"Search NCCO Order.");
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            var obj = _repo.Select(sortExpression, searchCriteria, loggedInUserCSGLevel, 0);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<NccoOrderViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"NCCO Order not found.");
                return NotFound(new { Message = $"NCCO Order not found." });
            }
        }

        /// <summary>
        /// https://localhost:44314/api/NccoOrder/value
        /// this.api.post('api/NccoOrder/', model);
        /// CREATE
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Post([FromBody] NccoOrderViewModel model)
        {
            //
            if (ModelState.IsValid)
            {
                NccoOrderView Ncco = _mapper.Map<NccoOrderView>(model);
                bool rspn2 = false;
                bool rspn1 = false;
                int rspn = 0;
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
                Ncco.CreatDt = DateTime.Now;
                Ncco.CreatByUserId = loggedInUser.UserId;
                Ncco.CsgLvlId = loggedInUserCSGLevel;
                var sCustName = _repo.GetH5CustomerName(Ncco, Ncco.CsgLvlId);
                if (sCustName != string.Empty && sCustName != null)
                    Ncco.CustNme = sCustName;

                if (model.OrdrId != null || model.OrdrId != 0)
                {
                    //// Validate M5
                    //if (!string.IsNullOrWhiteSpace(model.H5AcctNbr.ToString()))
                    //{
                    //    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(model.H5AcctNbr.ToString()))
                    //    {
                    //        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                    //    }

                    //}
                    //COPY  Existing Order
                    if (model.OrdrId > 0)
                    {
                        rspn1 = _repo.UpdateH5FolderID(Ncco, loggedInUser.UserId);

                        if (rspn1)
                        {
                            rspn2 = _repo.UpdateNCCOOrderTable(Ncco);
                            if (!rspn2)
                            {
                                _logger.LogInformation($"Problem while UpdateNCCOOrderTable. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                                return BadRequest(new { Message = "NCCO Order Could Not Be Created." });
                            }
                        }
                        else
                        {
                            _logger.LogInformation($"Problem while UpdateH5FolderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                            return BadRequest(new { Message = "NCCO Order Could Not Be Created." });
                        }
                        Int16 taskID = 0;
                        var dataTable = _repo.GetNCCOWGDetails(model.OrdrId);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            if (dataTable.Rows[0]["taskId"].ToString().Length > 0)
                            {
                                taskID = Int16.Parse(dataTable.Rows[0]["taskId"].ToString());
                                // IF Copy / New Order created how the
                                if (taskID == 111)
                                {
                                    rspn1 = _repo.MoveFromGOMFOtoXNCI(Convert.ToInt32(model.OrdrId), loggedInUser.UserId);
                                    if (!rspn1)
                                    {
                                        _logger.LogInformation($"Problem while MoveFromGOMFOtoXNCI. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                                        return BadRequest(new { Message = "NCCO Order Created with Error." });
                                    }
                                }
                            }
                        }
                        _logger.LogInformation($"Order Copied for OrderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                    else
                    //NEW Create new NCCO Order  //model.OrdrId=0
                    {
                        rspn = _repo.insertIntoOrderTable(loggedInUserCSGLevel, loggedInUser.UserId);
                        model.OrdrId = rspn;
                        if (rspn > 0)
                        {
                            Ncco.OrdrId = rspn;
                            rspn1 = _repo.UpdateNCCOOrderTable(Ncco);

                            if (rspn1)
                            {
                                rspn2 = _repo.UpdateH5FolderID(Ncco, loggedInUser.UserId);
                                if (!rspn2)
                                {
                                    _logger.LogInformation($"Problem while UpdateH5FolderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                                    return BadRequest(new { Message = "NCCO Order Could Not Be Created." });
                                }
                            }
                            else
                            {
                                _logger.LogInformation($"Problem while UpdateNCCOOrderTable. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                                return BadRequest(new { Message = "NCCO Order Could Not Be Created." });
                            }
                            //Create Order Note
                            OrdrNte onte = new OrdrNte();
                            onte.OrdrId = rspn;
                            onte.NteTxt = "Order created by " + loggedInUser.FullNme + ".";
                            onte.NteTypeId = 10;
                            onte.CreatByUserId = loggedInUser.UserId;
                            onte.CreatDt = DateTime.UtcNow;
                            onte.RecStusId = 1;
                            if (!_repo.InsertOrderNotes(onte))
                            {
                                _logger.LogInformation($"Problem while InsertOrderNotes. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                                return BadRequest(new { Message = "NCCO Order Created with Error." });
                            }
                            if (!_repo.LoadSM(model.OrdrId, 100, 0))
                            {
                                _logger.LogInformation($"Problem while Load initial Task. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                                return BadRequest(new { Message = "NCCO Order Created with Error." });
                            }
                        }
                        else
                        {
                            _logger.LogInformation($"Problem while insertIntoOrderTable. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                            return BadRequest(new { Message = "NCCO Order Could Not Be Created." });
                        }
                    }
                    _logger.LogInformation($"Order Created OrderID:  { JsonConvert.SerializeObject(rspn, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                }

                //_logger.LogInformation($"Order Copied for OrderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                return Created($"api/NccoOrder/{ model.OrdrId}", Ncco);
            }
            return BadRequest(new { Message = "NCCO Order Could Not Be Created." });
        }

        /// UPDATE
        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] NccoOrderViewModel model)
        {
            String MyBadMessage = "NCCO Order Could Not Be Updated.";//
            if (ModelState.IsValid)
            {
                bool rspn1 = false;
                //// Validate M5
                //if (!string.IsNullOrWhiteSpace(model.H5AcctNbr.ToString()))
                //{
                //    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(model.H5AcctNbr.ToString()))
                //    {
                //        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                //    }

                //}
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
                NccoOrderView Ncco = _mapper.Map<NccoOrderView>(model);
                if (id == 2)  //Cancel
                {
                    MyBadMessage = "NCCO Order Could Not Be Cancelled.";
                    //_ncco = FetchOrderDetails(Convert.ToInt32(txtOrderID.Text.Trim()));
                    Ncco.CsgLvlId = loggedInUserCSGLevel;
                    if (Ncco.CmntTxt != string.Empty)
                    {
                        _repo.UpdatePreviousComments(Convert.ToInt32(model.OrdrId), model.CmntTxt);
                    }
                    rspn1 = _repo.UpdateNCCOOrderToCancel(Convert.ToInt32(model.OrdrId), Ncco, loggedInUser.UserId);

                    if (!rspn1)
                    {
                        return BadRequest(new { Message = "Update NCCOOrder To Cancel Failed ." });
                    }

                    rspn1 = _repo.ResendEmailOnCancel(Convert.ToInt32(model.OrdrId));

                    if (!rspn1)
                    {
                        return BadRequest(new { Message = "Resend Email On Cancel Failed ." });
                    }
                    _logger.LogInformation($"Order Cancelled for OrderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    return Created($"api/NccoOrder/{ model.OrdrId}", Ncco);
                }
                else if (id == 3)  //UPDATE
                {
                    MyBadMessage = "An error occurred while completing the Task";
                    Ncco.CsgLvlId = loggedInUserCSGLevel;
                    if (Ncco.SolNme == "CCD")
                    {
                        MyBadMessage = "An error occurred completing the CCD Task";
                        rspn1 = _repo.CompleteCCDTask(Convert.ToInt32(model.OrdrId));
                        if (!rspn1)
                        {
                            return BadRequest(new { Message = "An error occurred completing the CCD Task" });
                        }
                        _logger.LogInformation($"Order completed the CCD for OrderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                    else if (Ncco.SolNme == "GOV")
                    {
                        MyBadMessage = "An error occurred completing GOM Order Verification  Task";
                        //ncco.ordrId = this.id;
                        //ncco.solNme = "GOV";  // User Control  this is just to hold a parameter which will not be saved in DB
                        //ncco.creatByUserId = this.taskId; // TaskID  this is just to hold a parameter which will not be saved in DB
                        //ncco.ordrTypeId = actionGov;// Action Approve  or Approve By Pass XNCI  this is just to hold a parameter which will not be saved in DB
                        //ncco.cmntTxt = this.getFormControlValue("noteGov");  //Action Notes  this is just to hold a parameter which will not be saved in DB
                        //CompleteTask(int iOrderID, int iTaskID, int iTaskStatus, string sComments, int iUserID, int noteTypeID)
                        int taskid = model.CreatByUserId;
                        string txtNotes = model.CmntTxt == null ? string.Empty : model.CmntTxt;
                        int GOMAction = model.OrdrTypeId == 0 ? 1 : model.OrdrTypeId;
                        int iTaskStatus = 0;
                        string sComments = "";
                        int noteTypeID = 0;
                        int iUserID = loggedInUser.UserId;
                        int iOrderID = model.OrdrId;
                        if (taskid == 108 || taskid == 110)
                        {
                            if (txtNotes != string.Empty)
                            {
                                rspn1 = _repo.InsertOrderNotes(iOrderID, 10, iUserID, txtNotes.Trim());
                                if (!rspn1)
                                {
                                    return BadRequest(new { Message = "An error occurred completing GOM Order Verification  Task" });
                                }
                            }

                            if (taskid == 110)
                            {
                                if (_repo.CompleteGOMIBillTask(iOrderID) == 0)
                                    return BadRequest(new { Message = "An error occurred completing GOM BILL  Task" });
                            }
                            else
                            {
                                taskid = Convert.ToInt32(Tasks.GOMCancelReady);
                                iTaskStatus = 2;
                                sComments = "Move the order out from GOM Cancel Ready - Pending to Complete state.";
                            }
                        }
                        else
                        {
                            if (iOrderID != 0 && iOrderID != 0)
                            {
                                taskid = Convert.ToInt32(Tasks.GOMReview);
                                if (GOMAction == 1)
                                {
                                    iTaskStatus = 5;
                                    sComments = "Order Approved, Moved to XNCI";
                                }
                                else if (GOMAction == 2)
                                {
                                    iTaskStatus = 2;
                                    sComments = "Order Approved,By passed XNCI";
                                }
                            }
                        }

                        rspn1 = _repo.CompleteTask(iOrderID, taskid, iTaskStatus, sComments, iUserID, noteTypeID);
                        if (!rspn1)
                        {
                            return BadRequest(new { Message = "An error occurred completing GOM Order Verification  Task" });
                        }

                        _logger.LogInformation($"Order completed GOM Order Verification  Task for OrderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                    else if (Ncco.SolNme == "XOV")
                    {
                        MyBadMessage = "An error occurred while XNCI Order Verification Process";
                        //ncco.ordrId = this.id;
                        //ncco.solNme = "GOV";  // User Control  this is just to hold a parameter which will not be saved in DB
                        //ncco.creatByUserId = this.taskId; // TaskID  this is just to hold a parameter which will not be saved in DB
                        //ncco.ordrTypeId = actionGov;// Action Approve  or Approve By Pass XNCI  this is just to hold a parameter which will not be saved in DB
                        //ncco.cmntTxt = this.getFormControlValue("noteGov");  //Action Notes  this is just to hold a parameter which will not be saved in DB
                        //CompleteTask(int iOrderID, int iTaskID, int iTaskStatus, string sComments, int iUserID, int noteTypeID)
                        int taskid = model.CreatByUserId;
                        string txtNotes = model.CmntTxt == null ? string.Empty : model.CmntTxt;
                        int XNCIAction = model.OrdrTypeId == 0 ? 1 : model.OrdrTypeId;
                        int iTaskStatus = 0;
                        string sComments = "";
                        int noteTypeID = 0;
                        int iUserID = loggedInUser.UserId;
                        int iOrderID = model.OrdrId;
                        taskid = Convert.ToInt32(Tasks.xNCIReady);
                        if (XNCIAction == 2)  //Reject
                        {
                            iTaskStatus = 1;
                            sComments = "Order Rejected, Moved to GOM";

                            noteTypeID = 14;
                            rspn1 = _repo.InsertJeopardy(iOrderID, "103", txtNotes.Trim(), 21, iUserID);
                            if (!rspn1)
                            {
                                return BadRequest(new { Message = "An error occurred while XNCI Order Verification Process" });
                            }
                        }
                        else  //APPROVE  1
                        {
                            iTaskStatus = 5;
                            sComments = "Order Approved, XNCI Needs to enter Milestones";
                        }

                        rspn1 = _repo.CompleteTask(iOrderID, taskid, iTaskStatus, sComments, iUserID, noteTypeID);
                        if (!rspn1)
                        {
                            return BadRequest(new { Message = "An error occurred completing XNCI Order Verification Process" });
                        }

                        _logger.LogInformation($"Order XNCI Order Verification Process for OrderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                    else if (Ncco.SolNme == "GPL")
                    {
                        MyBadMessage = "An error occurred while completing GOM Private Line Tasks";
                        //ncco.ordrId = this.id;
                        //ncco.solNme = "GOV";  // User Control  this is just to hold a parameter which will not be saved in DB
                        //ncco.creatByUserId = this.taskId; // TaskID  this is just to hold a parameter which will not be saved in DB
                        //ncco.ordrTypeId = actionGov;// Action Approve  or Approve By Pass XNCI  this is just to hold a parameter which will not be saved in DB
                        //ncco.cmntTxt = this.getFormControlValue("noteGov");  //Action Notes  this is just to hold a parameter which will not be saved in DB
                        //CompleteTask(int iOrderID, int iTaskID, int iTaskStatus, string sComments, int iUserID, int noteTypeID)
                        int taskid = model.CreatByUserId;
                        string txtNotes = model.CmntTxt == null ? string.Empty : model.CmntTxt;
                        int XNCIAction = model.OrdrTypeId == 0 ? 1 : model.OrdrTypeId;
                        int iTaskStatus = 0;
                        string sComments = "";
                        int noteTypeID = 0;
                        int iUserID = loggedInUser.UserId;
                        int iOrderID = model.OrdrId;
                        taskid = Convert.ToInt32(Tasks.GOMPL);

                        rspn1 = _repo.insertPLSequence(iOrderID, model.MyPLNumber, model.PLNumber, iUserID);

                        if (!rspn1)
                        {
                            return BadRequest(new { Message = "An error occurred while completing GOM Private Line Tasks" });
                        }
                        if (txtNotes != string.Empty)
                        {
                            rspn1 = _repo.InsertOrderNotes(iOrderID, 10, iUserID, txtNotes.Trim());
                            if (!rspn1)
                            {
                                return BadRequest(new { Message = "An error occurred completing GOM Private Line Tasks" });
                            }
                        }
                        iTaskStatus = 2;
                        sComments = "PL Sequence entered, completing GOM Private Line Tasks.";

                        rspn1 = _repo.CompleteTask(iOrderID, taskid, iTaskStatus, sComments, iUserID, noteTypeID);
                        if (!rspn1)
                        {
                            return BadRequest(new { Message = "An error occurred completing XNCI Order Verification Process" });
                        }

                        _logger.LogInformation($"Order completed GOM Private Line Tasks for OrderID. { JsonConvert.SerializeObject(model.OrdrId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }

                    return Created($"api/NccoOrder/{ model.OrdrId}", Ncco);
                }
            }
            return BadRequest(new { Message = MyBadMessage });
        }
    }
}