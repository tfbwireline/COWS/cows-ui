﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/PlanServiceTypes")]
    [ApiController]
    public class CptPlanServiceTypeController : ControllerBase
    {
        private readonly ICptPlanServiceTypeRepository _repo;
        private readonly ILogger<CptPlanServiceTypeController> _logger;
        private readonly IMapper _mapper;

        public CptPlanServiceTypeController(IMapper mapper,
                               ICptPlanServiceTypeRepository repo,
                               ILogger<CptPlanServiceTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptPlanServiceTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptPlanServiceTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptPlnSrvcTypeId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT Plan Service Type by Id: { id }.");

            var obj = _repo.Find(s => s.CptPlnSrvcTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptPlanServiceTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT CPT Plan Service Type by Id: { id } not found.");
                return NotFound(new { Message = $"CPT CPT Plan Service Type Id: { id } not found." });
            }
        }
    }
}