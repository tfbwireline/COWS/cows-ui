﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Circuit")]
    [ApiController]
    public class CircuitController : ControllerBase
    {
        private readonly ICircuitChargeRepository _repo;
        private readonly ICircuitRepository _repoCircuit;
        private readonly ILogger<CircuitController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public CircuitController(IMapper mapper,
                               ICircuitChargeRepository repo,
                               ICircuitRepository repoCircuit,
                               ILogger<CircuitController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _repo = repo;
            _repoCircuit = repoCircuit;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = memoryCache;
        }

        [HttpGet("GetAddlCharges")]
        public IActionResult GetAddlCharges([FromQuery] int orderId, [FromQuery] bool isTerm)
        {
            List<OrdrCktChg> data = _repo.GetAddlCharges(orderId, isTerm);

            //if (data != null && data.Count != 0) {
            //    while (data.Count < 6)
            //        data.Add(new CircuitAddlCost());
            //}

            return Ok(_mapper.Map<IEnumerable<CircuitAddlCost>>(data));
        }

        [HttpGet("GetAddlChargeHistory")]
        public IActionResult GetAddlChargeHistory([FromQuery] int orderId, [FromQuery] int chargeTypeId, [FromQuery] bool isTerm)
        {
            List<CircuitAddlCost> data = _repo.GetAddlChargeHistory(orderId, chargeTypeId, isTerm);
            return Ok(data);
        }

        [HttpGet("GetAddlCostTypes")]
        public IActionResult GetAddlCostTypes()
        {
            List<CircuitAddlCost> data = _repo.GetAddlCostTypes();
            return Ok(data);
        }

        [HttpGet("GetStatus")]
        public IActionResult GetStatus()
        {
            List<CircuitAddlCost> data = _repo.GetStatus();
            return Ok(data);
        }

        [HttpGet("GetCurrencyList")]
        public IActionResult GetCurrencyList()
        {
            List<CircuitAddlCost> data = _repo.GetCurrencyList();
            return Ok(data);
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] List<CircuitAddlCost> model)
        //[HttpPost("UpdateAdditionalCosts")]
        //public IActionResult UpdateAdditionalCosts([FromBody] List<CircuitAddlCost> model)
        {
            if (model.Count == 0)
            {
                IEnumerable<OrdrCktChg> toDelete = _repo
                    .Find(a => a.OrdrId == id)
                    .AsEnumerable();

                if (toDelete.Count() > 0)
                {
                    _repo.Delete(toDelete);

                    _logger.LogInformation($"Additional Costs Records Deleted. { JsonConvert.SerializeObject(toDelete, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                }

                List<OrdrCktChg> data = _repo.Find(a => a.OrdrId == id).ToList();

                return Ok(_mapper.Map<IEnumerable<CircuitAddlCost>>(data));
            }
            else
            {
                int userId = _loggedInUser.GetLoggedInUserId();
                IEnumerable<OrdrCktChg> allAddtlCosts = _mapper.Map<IEnumerable<OrdrCktChg>>(model);
                IEnumerable<OrdrCktChg> updatedAddtlCosts = _mapper.Map<IEnumerable<OrdrCktChg>>(model.Where(a => a.IsUpdated == true).ToList());

                IEnumerable<OrdrCktChg> toDelete = _repo
                    .Find(a =>
                        a.OrdrId == id &&
                        a.TrmtgCd == model.First().IsTerm &&
                        !model.Any(b => b.ChargeTypeID == a.CktChgTypeId))
                    .AsEnumerable();

                if (toDelete.Count() > 0)
                {
                    _repo.Delete(toDelete);

                    _logger.LogInformation($"Additional Costs Records Deleted. { JsonConvert.SerializeObject(toDelete, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                }

                if (updatedAddtlCosts.Count() > 0)
                {
                    updatedAddtlCosts = updatedAddtlCosts.Select(a =>
                    {
                        a.CreatByUserId = userId;
                        a.CreatDt = DateTime.Now;

                        return a;
                    });

                    var obj = _repo.Create(updatedAddtlCosts);

                    if (obj != null)
                    {
                        _logger.LogInformation($"Additional Costs Table Updated. { JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                        List<OrdrCktChg> data = _repo.GetAddlCharges(obj.First().OrdrId, obj.First().TrmtgCd);

                        return Ok(_mapper.Map<IEnumerable<CircuitAddlCost>>(data));
                    }
                }
                else
                {
                    _logger.LogInformation($"No Additional Costs Records to be Updated.");

                    return Ok(_mapper.Map<IEnumerable<CircuitAddlCost>>(allAddtlCosts));
                }

                return BadRequest(new { Message = "You are not authorized to perform this action" });
            }
        }

        [HttpGet("AcrOrderStatus")]
        public IActionResult AcrOrderStatus([FromQuery] int orderId, [FromQuery] bool isTerm)
        {
            int data = _repo.AcrOrderStatus(orderId, isTerm);
            return Ok(data);
        }

        [HttpGet("LoadRTSTask")]
        public IActionResult LoadRTSTask([FromQuery] int orderId)
        {
            return Ok(_repo.LoadRTSTask(orderId));
        }

        [HttpGet("SetJeopardy")]
        public void SetJeopardy([FromQuery] int orderId, [FromQuery] int noteId, [FromQuery] string jepCode)
        {
            _repo.SetJeopardy(orderId, noteId, jepCode);
        }

        [HttpGet("NotifyNCI")]
        public IActionResult NotifyNCI([FromQuery] int orderId, [FromQuery] short taskId)
        {
            return Ok(_repo.LoadNCITask(orderId, taskId));
        }

        [HttpGet("InsertGOMBillingTask")]
        public IActionResult InsertGOMBillingTask([FromQuery] int orderId)
        {
            return Ok(_repo.InsertGOMBillingTask(orderId));
        }

        [HttpGet("GetPreQualLineInfo")]
        public IActionResult GetPreQualLineInfo([FromQuery] int orderId)
        {
            return Ok(_repoCircuit.GetPreQualLineInfo(orderId));
        }
    }
}