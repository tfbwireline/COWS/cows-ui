﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Events/AccessDeliveries")]
    [ApiController]
    public class AdEventController : ControllerBase
    {
        private readonly IAdEventRepository _repo;
        private readonly IAdEventAccessTagRepository _circuitRepo;
        private readonly IEventAsnToUserRepository _eventAsnToUserRepository;
        private readonly IEventHistoryRepository _eventHistoryRepository;
        private readonly IEventRuleRepository _eventRuleRepository;
        private readonly IApptRepository _apptRepository;
        private readonly IEmailReqRepository _emailReqRepository;
        private readonly IEventFailActyRepository _failEventActyRepository;
        private readonly IEventSucssActyRepository _sucssEventActyRepository;
        private readonly ILogger<AdEventController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IL2PInterfaceService _l2pInterface;
        private IMemoryCache _cache;

        public AdEventController(IMapper mapper,
                               IAdEventRepository repo,
                               IAdEventAccessTagRepository circuitRepo,
                               IEventAsnToUserRepository eventAsnToUserRepository,
                               IEventHistoryRepository eventHistoryRepository,
                               IEventRuleRepository eventRuleRepository,
                               IApptRepository apptRepository,
                               IEmailReqRepository emailReqRepository,
                               IEventFailActyRepository failEventActyRepository,
                               IEventSucssActyRepository sucssEventActyRepository,
                               ILogger<AdEventController> logger,
                               ILoggedInUserService loggedInUser,
                               IL2PInterfaceService l2pInterface,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _circuitRepo = circuitRepo;
            _eventAsnToUserRepository = eventAsnToUserRepository;
            _eventHistoryRepository = eventHistoryRepository;
            _eventRuleRepository = eventRuleRepository;
            _apptRepository = apptRepository;
            _emailReqRepository = emailReqRepository;
            _failEventActyRepository = failEventActyRepository;
            _sucssEventActyRepository = sucssEventActyRepository;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _l2pInterface = l2pInterface;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AdEventViewModel>> Get()
        {
            IEnumerable<AdEventViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.AdEventList, out list))
            {
                list = _mapper.Map<IEnumerable<AdEventViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderByDescending(s => s.EventId));

                CacheManager.Set(_cache, CacheKeys.AdEventList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Access Delivery Event by Id: { id }.");
            var userCsgLvl = _loggedInUser.GetLoggedInUserCsgLvlId();
            string adid = _loggedInUser.GetLoggedInUserAdid();

            var obj = _repo.GetById(id, adid);
            if (obj != null)
            {
                var ad = _mapper.Map<AdEventViewModel>(obj);
                if (ad.EventCsgLvlId > 0)
                {
                    if (userCsgLvl != 0 && userCsgLvl <= ad.EventCsgLvlId)
                        ad.AuthStatus = (int)AuthStatus.Authorized;
                    else
                    {
                        ad.AuthStatus = (int)AuthStatus.UnAuthorized;
                        ad.CustNme = CustomerMaskedData.customerName;
                        ad.CustCntctNme = CustomerMaskedData.customerName;
                        ad.CustCntctPhnNbr = CustomerMaskedData.installSitePOCPhone;
                        ad.CustCntctCellPhnNbr = CustomerMaskedData.installSitePOCPhone;
                        ad.CustCntctPgrNbr = CustomerMaskedData.installSitePOCPhone;
                        ad.CustCntctPgrPinNbr = CustomerMaskedData.installSitePOCPhone;
                        ad.CustEmailAdr = CustomerMaskedData.customerEmailAddress;
                        ad.EventTitleTxt = ad.EventTitleTxt.Replace(CustomerMaskedData.customerName, ad.CustNme);
                    }
                }
                else
                    ad.AuthStatus = (int)AuthStatus.NonSensitive;

                return Ok(_mapper.Map<AdEventViewModel>(obj));
            }
            else
            {
                _logger.LogInformation($"Access Delivery Event by Id: { id } not found.");
                return NotFound(new { Message = $"Access Delivery Event Id: { id } not found." });
            }
        }

        [HttpGet("{id}/Circuits")]
        [ActionName("GetCircuitInformation")]
        public ActionResult<IEnumerable<AdEventAccessTagViewModel>> GetCircuitInformation([FromRoute] int id)
        {
            _logger.LogInformation($"Search Circuit Information by Event Id: { id }.");

            var obj = _circuitRepo.Find(s => s.EventId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<AdEventAccessTagViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"Circuit Information by Event Id: { id } not found.");
                return NotFound(new { Message = $"Circuit Information by Event Id: { id } not found." });
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] AdEventViewModel model)
        {
            int userId = _loggedInUser.GetLoggedInUserId();

            if (ModelState.IsValid)
            {
                AdEvent adEvent = _mapper.Map<AdEvent>(model);

                adEvent.Event = new Event();
                adEvent.Event.EventTypeId = (int)EventType.AD;
                adEvent.Event.CreatDt = DateTime.Now;
                adEvent.CreatByUserId = userId;
                adEvent.ModfdByUserId = userId;

                // Validate H1
                _logger.LogInformation($"Checking if user is authorized for H1: { adEvent.H1 }.");
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(adEvent.H1))
                {
                    _logger.LogInformation($"Checking if UserID: { userId } is authorized for H1: { adEvent.H1 }.");
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                adEvent.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(adEvent.H1);

                // Validate M5
                if (!string.IsNullOrWhiteSpace(adEvent.Ftn))
                {
                    _logger.LogInformation($"Checking if user is authorized for FTN: { adEvent.Ftn }.");
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(adEvent.Ftn))
                    {
                        _logger.LogInformation($"Checking if UserID: { userId } is authorized for FTN: { adEvent.Ftn }.");
                        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                    }

                    adEvent.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(string.Empty, string.Empty, adEvent.Ftn);
                }

                // AD Govenment Event is always secure
                if (adEvent.EnhncSrvcId == (int)EnhanceService.ADGovernment)
                    adEvent.Event.CsgLvlId = 2;

                LkEventRule eventRule = _eventRuleRepository.GetEventRule(adEvent.EventStusId, adEvent.WrkflwStusId, userId, (int)EventType.AD);

                if (eventRule != null)
                {
                    adEvent.EventStusId = eventRule.EndEventStusId;

                    //Create Event and AdEvent
                    var ad = _repo.Create(adEvent);

                    //Create EventAsnToUser
                    if (model.Activators != null && model.Activators.Count > 0)
                    {
                        _eventAsnToUserRepository.Create(model.Activators
                        .Select(a => new EventAsnToUser
                        {
                            EventId = ad.EventId,
                            AsnToUserId = a,
                            CreatDt = ad.CreatDt,
                            RecStusId = 0,
                            RoleId = 0
                        }).ToList());
                    }
                    List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                    assignUsers = _eventAsnToUserRepository.Find(a => a.EventId == ad.EventId).ToList();

                    if (ad != null)
                    {
                        _logger.LogInformation($"Access Delivery Event Created. { JsonConvert.SerializeObject(ad, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                        // Do things needed based on eventRule like event history and email sender
                        var json = new JsonSerializerSettings()
                        {
                            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                            Formatting = Formatting.Indented
                        };
                        //_eventHistoryRepository.CreateEventHistory(ad.EventId, eventRule.ActnId, ad.StrtTmst, ad.EndTmst, userId);
                        // Create Event History; Reviewer and Activator is not needed for new MPLS Event
                        EventHist eh = new EventHist();
                        eh.EventId = ad.EventId;
                        eh.ActnId = eventRule.ActnId;
                        eh.EventStrtTmst = ad.StrtTmst;
                        eh.EventEndTmst = ad.EndTmst;
                        eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        _eventHistoryRepository.CreateEventHistory(eh);

                        // Save Calendar Entry and Email Sender
                        EventWorkflow esw = SetEventWorkFlow(ad);
                        esw.AssignUser = assignUsers;
                        esw.EventRule = eventRule;

                        if (_apptRepository.CalendarEntry(esw))
                            _logger.LogInformation($"Access Delivery Event Calender Entry success for EventID. { JsonConvert.SerializeObject(ad.EventId, json) } ");

                        if (_emailReqRepository.SendMail(esw))
                            _logger.LogInformation($"Access Delivery Event Email Entry success for EventID. { JsonConvert.SerializeObject(ad.EventId, json) } ");

                        return Created($"api/AccessDeliveries/{ ad.EventId}", ad);
                    }
                }
                else
                {
                    return BadRequest(new { Message = "You are not authorized to perform this action" });
                }
            }

            return BadRequest(new { Message = "Access Delivery Event Could Not Be Created" });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] AdEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                AdEvent adEvent = _mapper.Map<AdEvent>(model);

                int userId = _loggedInUser.GetLoggedInUserId();
                adEvent.ModfdByUserId = userId;

                LkEventRule eventRule = _eventRuleRepository.GetEventRule(adEvent.EventStusId, adEvent.WrkflwStusId, userId, (int)EventType.AD);

                if (eventRule != null)
                {
                    // Prceed to update
                    adEvent.EventStusId = eventRule.EndEventStusId;
                    _repo.Update(id, adEvent);
                    var ad = _repo.GetById(id);

                    if (model.Activators != null)
                    {
                        _eventAsnToUserRepository.Update(id, model.Activators
                        .Select(a => new EventAsnToUser
                        {
                            EventId = id,
                            AsnToUserId = a,
                            CreatDt = ad.CreatDt,
                            RecStusId = 0,
                            RoleId = 0
                        }).ToList());
                    }
                    List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                    assignUsers = _eventAsnToUserRepository.Find(a => a.EventId == ad.EventId).ToList();

                    _logger.LogInformation($"Access Delivery Event Updated. { JsonConvert.SerializeObject(ad, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    // Do things needed based on eventRule
                    var json = new JsonSerializerSettings()
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                        Formatting = Formatting.Indented
                    };
                    //_eventHistoryRepository.CreateEventHistory(ad.EventId, eventRule.ActnId, ad.StrtTmst, ad.EndTmst, userId);
                    // Event History, Reviewer and Activator Task
                    EventHist eh = new EventHist();
                    eh.EventId = ad.EventId;
                    eh.ActnId = eventRule.ActnId;
                    eh.EventStrtTmst = ad.StrtTmst;
                    eh.EventEndTmst = ad.EndTmst;
                    eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                    if (model.ReviewerUserId > 0)
                    {
                        eh.ModfdByUserId = model.ReviewerUserId;
                        eh.CmntTxt = model.ReviewerComments;
                    }
                    else if (model.ActivatorUserId > 0)
                    {
                        eh.ModfdByUserId = model.ActivatorUserId;
                        eh.CmntTxt = model.ActivatorComments;
                        eh.PreCfgCmpltCd = (string.IsNullOrWhiteSpace(model.PreCfgConfgCode)
                            || model.PreCfgConfgCode == "0") ? "N" : "Y";
                    }
                    else if ((ad.WrkflwStusId != (byte)WorkflowStatus.Visible
                                || ad.WrkflwStusId != (byte)WorkflowStatus.Retract)
                                        && ad.EventStusId != (byte)EventStatus.Visible)
                    {
                        eh.CmntTxt = model.DesCmntTxt;
                    }

                    int evntHistId = _eventHistoryRepository.CreateEventHistory(eh);
                    if (model.ActivatorUserId > 0)
                    {
                        var Fail = this._failEventActyRepository.GetByEventId(ad.EventId);
                        if (Fail.Count() > 0)
                        {
                            _failEventActyRepository.DeleteByEventId(ad.EventId);
                        }

                        if (model.EventFailActyIds != null && model.EventFailActyIds.Count() > 0)
                        {
                            foreach (var es in model.EventFailActyIds)
                            {
                                EventFailActy esa = new EventFailActy();
                                esa.EventHistId = evntHistId;
                                esa.FailActyId = (short)es;
                                esa.RecStusId = (byte)ERecStatus.Active;
                                esa.CreatDt = DateTime.Now;
                                _failEventActyRepository.CreateActy(esa);
                            }
                        }

                        var Sucss = _sucssEventActyRepository.GetByEventId(ad.EventId);
                        if (Sucss.Count() > 0)
                        {
                            _sucssEventActyRepository.DeleteByEventId(ad.EventId);
                        }

                        if (model.EventSucssActyIds != null && model.EventSucssActyIds.Count() > 0)
                        {
                            foreach (var es in model.EventSucssActyIds)
                            {
                                EventSucssActy esa = new EventSucssActy();
                                esa.EventHistId = evntHistId;
                                esa.SucssActyId = (short)es;
                                esa.RecStusId = (byte)ERecStatus.Active;
                                esa.CreatDt = DateTime.Now;
                                _sucssEventActyRepository.CreateActy(esa);
                            }
                        }
                    }

                    // Save Calendar Entry and Email Sender
                    EventWorkflow esw = SetEventWorkFlow(ad);
                    esw.AssignUser = assignUsers;
                    esw.EventRule = eventRule;
                    esw.ReviewerId = model.ReviewerUserId;

                    if (_apptRepository.CalendarEntry(esw))
                        _logger.LogInformation($"Access Delivery Event Calender Entry success for EventID. { JsonConvert.SerializeObject(ad.EventId, json) } ");

                    if (_emailReqRepository.SendMail(esw))
                        _logger.LogInformation($"Access Delivery Event Email Entry success for EventID. { JsonConvert.SerializeObject(ad.EventId, json) } ");

                    return Created($"api/AccessDeliveries/{ ad.EventId}", ad);
                }
                else
                {
                    return BadRequest(new { Message = "You are not authorized to perform this action" });
                }
            }

            return BadRequest(new { Message = "Access Delivery Event Could Not Be Updated." });
        }

        private EventWorkflow SetEventWorkFlow(AdEvent ad)
        {
            EventWorkflow esw = new EventWorkflow();
            esw.EventId = ad.EventId;
            esw.Comments = ad.DesCmntTxt;
            esw.EventStatusId = ad.EventStusId;
            esw.WorkflowId = (int)ad.WrkflwStusId;
            esw.EventTitle = ad.EventTitleTxt;
            esw.EventTypeId = (int)EventType.AD;
            esw.EventType = "AD";
            esw.SOWSEventID = ad.SowsEventId.HasValue ? ad.SowsEventId.Value.ToString() : string.Empty;
            esw.RequestorId = ad.ReqorUserId;
            esw.UserId = _loggedInUser.GetLoggedInUserId();
            esw.ConferenceBridgeNbr = ad.CnfrcBrdgNbr;
            esw.ConferenceBridgePin = ad.CnfrcPinNbr;
            esw.StartTime = ad.StrtTmst;
            esw.EndTime = ad.EndTmst;
            esw.EnhanceServiceId = ad.EnhncSrvcId;

            return esw;
        }
    }
}