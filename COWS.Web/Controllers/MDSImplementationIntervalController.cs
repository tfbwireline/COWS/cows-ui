﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventIntervals")]
    [ApiController]
    public class MDSImplementationIntervalController : ControllerBase
    {
        private readonly IMDSImplementationIntervalRepository _repo;
        private readonly ILogger<MDSImplementationIntervalController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public MDSImplementationIntervalController(IMapper mapper,
                               IMDSImplementationIntervalRepository repo,
                               ILogger<MDSImplementationIntervalController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MDSImplementationIntervalViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MDSImplementationIntervalViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.EventType.EventTypeNme));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Event Interval by Id: { id }.");

            var obj = _repo.Find(s => s.EventTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSImplementationIntervalViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Event Interval by Id: { id } not found.");
                return NotFound(new { Message = $"Event Interval Id: { id } not found." });
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] MDSImplementationIntervalViewModel model)
        {
            _logger.LogInformation($"Update Event Interval Id: { id }.");

            var obj = _mapper.Map<LkMdsImplmtnNtvl>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
            }

            _repo.Update(id, obj);
            
            _logger.LogInformation($"Event Interval Updated. { JsonConvert.SerializeObject(obj) } ");
            return Created($"api/Intervals/{ id }", model);
        }
    }
}