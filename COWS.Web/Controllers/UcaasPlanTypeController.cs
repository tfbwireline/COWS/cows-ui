﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/UcaasPlanTypes")]
    [ApiController]
    public class UcaasPlanTypeController : ControllerBase
    {
        private readonly IUcaasPlanTypeRepository _repo;
        private readonly ILogger<UcaasPlanTypeController> _logger;
        private readonly IMapper _mapper;

        public UcaasPlanTypeController(IMapper mapper,
                               IUcaasPlanTypeRepository repo,
                               ILogger<UcaasPlanTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UcaasPlanTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<UcaasPlanTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.UcaaSPlanTypeId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search UCaaS Plan Type by Id: { id }.");

            var obj = _repo.Find(s => s.UcaaSPlanTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<UcaasPlanTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"UCaaS Plan Type by Id: { id } not found.");
                return NotFound(new { Message = $"UCaaS Plan Type Id: { id } not found." });
            }
        }
    }
}