﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/ServiceDelivery")]
    [ApiController]
    public class ServiceDeliveryController : ControllerBase
    {
        private readonly IServiceDeliveryRepository _repo;
        private readonly ILogger<ServiceDeliveryController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public ServiceDeliveryController(IMapper mapper,
                               IServiceDeliveryRepository repo,
                               ILogger<ServiceDeliveryController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ServiceDeliveryViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<ServiceDeliveryViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.SprintCpeNcrDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Service Delivery by Id: { id }.");

            var obj = _repo.Find(s => s.SprintCpeNcrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<ServiceDeliveryViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Service Delivery by Id: { id } not found.");
                return NotFound(new { Message = $"Service Delivery Id: { id } not found." });
            }
        }

        [HttpGet("GetForLookup")]
        public ActionResult<IEnumerable<ServiceDeliveryViewModel>> GetForLookup()
        {
            var list = _mapper.Map<IEnumerable<ServiceDeliveryViewModel>>(_repo.GetAll()
                                                                .OrderBy(s => s.SprintCpeNcrDes));
            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ServiceDeliveryViewModel model)
        {
            _logger.LogInformation($"Create Service Delivery: { model.SprintCpeNcrDes }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkSprintCpeNcr>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkSprintCpeNcr();
                var duplicate = _repo.Find(i => i.SprintCpeNcrDes.Trim().ToLower() == obj.SprintCpeNcrDes.Trim().ToLower()).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.SprintCpeNcrDes + " already exists." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.SprintCpeNcrId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    _logger.LogInformation($"Service Delivery Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/ServiceDelivery/{ newData.SprintCpeNcrId }", model);
                }
            }

            return BadRequest(new { Message = "Service Delivery Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] ServiceDeliveryViewModel model)
        {
            _logger.LogInformation($"Update Service Delivery by Id: { id }.");

            var obj = _mapper.Map<LkSprintCpeNcr>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i => i.SprintCpeNcrDes.Trim().ToLower() == obj.SprintCpeNcrDes.Trim().ToLower()).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.SprintCpeNcrDes + " already exists." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.SprintCpeNcrId);
                }
            }

            _repo.Update(id, obj);

            _logger.LogInformation($"Service Delivery Updated. { JsonConvert.SerializeObject(obj) } ");
            return Created($"api/ServiceDelivery/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating Service Delivery by Id: { id }.");

            var srvc = _repo.Find(s => s.SprintCpeNcrId == id);
            if (srvc != null)
            {
                var obj = _mapper.Map<LkSprintCpeNcr>(srvc.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);

                _logger.LogInformation($"Service Delivery by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to Service Delivery by Id: { id } not found.");
            }
        }
    }
}