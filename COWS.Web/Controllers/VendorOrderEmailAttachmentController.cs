﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VendorOrderEmailAttachments")]
    [ApiController]
    public class VendorOrderEmailAttachmentController : ControllerBase
    {
        private readonly IVendorOrderEmailAttachmentRepository _repo;
        private readonly IVendorOrderEmailRepository _vndrOrdrEmailRepo;
        private readonly IVendorOrderRepository _vndrOrdrRepo;
        private readonly IOrderRepository _orderRepo;
        private readonly ISystemConfigRepository _systemConfigRepo;
        private readonly ILogger<VendorOrderEmailAttachmentController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public VendorOrderEmailAttachmentController(IMapper mapper,
                               IVendorOrderEmailAttachmentRepository repo,
                               IVendorOrderEmailRepository vndrOrdrEmailRepo,
                               IVendorOrderRepository vndrOrdrRepo,
                               IOrderRepository orderRepo,
                               ISystemConfigRepository systemConfigRepo,
                               ILogger<VendorOrderEmailAttachmentController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _vndrOrdrEmailRepo = vndrOrdrEmailRepo;
            _vndrOrdrRepo = vndrOrdrRepo;
            _orderRepo = orderRepo;
            _systemConfigRepo = systemConfigRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VendorOrderEmailAttachmentViewModel>> Get()
        {
            IEnumerable<VendorOrderEmailAttachmentViewModel> list;
            //if (!_cache.TryGetValue(CacheKeys.VendorOrderEmailAttachmentList, out list))
            //{
                list = _mapper.Map<IEnumerable<VendorOrderEmailAttachmentViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.VndrOrdrEmailAtchmtId));

                //CacheManager.Set(_cache, CacheKeys.VendorOrderEmailAttachmentList, list);
            //}

            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] VendorOrderEmailAttachmentViewModel model)
        {
            model.FileCntnt = Convert.FromBase64String(model.Base64string);

            _logger.LogInformation($"Uploading Vendor Order Email Attachment: { model.FileNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrOrdrEmailAtchmt>(model);

                // Remove this comment once logged in user service is available
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                int createdBy = loggedInUser.UserId;
                int vndrOrderEmailId = model.VndrOrdrEmailId;
                var vendorOrderEmail = _vndrOrdrEmailRepo.GetById(model.VndrOrdrEmailId);

                if (vendorOrderEmail.EmailStusId == (short)EmailStatus.SentEnc || vendorOrderEmail.EmailStusId == (short)EmailStatus.SentUnenc)
                {
                    vndrOrderEmailId = _vndrOrdrEmailRepo.CloneVendorOrderEmail(model.VndrOrdrEmailId, "", createdBy);
                    obj.VndrOrdrEmailId = vndrOrderEmailId;

                    //if (_repo.IsAttachmentDistinct(model.VndrOrdrEmailId, obj.FileNme))
                    //{
                    //    var rep = _repo.Create(obj);
                    //    if (rep != null)
                    //    {
                    //        var list = _mapper.Map<IEnumerable<VendorOrderEmailAttachmentViewModel>>(_repo
                    //            .Find(a => a.VndrOrdrEmailId == vndrOrderEmailId)
                    //            .ToList());

                    //        _logger.LogInformation($"Vendor Order Email Attachment Uploaded. { rep.FileNme }");
                    //        return Created($"api/VendorOrderEmailAttachments/{ rep.FileNme }", list);
                    //    }
                    //}
                    //else
                    //{
                    //    return BadRequest(new { Message = "Attachment file name already exists." });
                    //}
                }

                if (_repo.IsAttachmentDistinct(model.VndrOrdrEmailId, obj.FileNme))
                {
                    var rep = _repo.Create(obj);
                    if (rep != null)
                    {
                        var list = _mapper.Map<IEnumerable<VendorOrderEmailAttachmentViewModel>>(_repo
                            .Find(a => a.VndrOrdrEmailId == vndrOrderEmailId)
                            .ToList());

                        _logger.LogInformation($"Vendor Order Email Attachment Uploaded. { rep.FileNme }");
                        return Created($"api/VendorOrderEmailAttachments/{ rep.FileNme }", list);
                    }
                }
                else
                {
                    return BadRequest(new { Message = "Attachment file name already exists." });
                }

                //else
                //{
                //    if (_repo.IsAttachmentDistinct(model.VndrOrdrEmailId, obj.FileNme))
                //    {
                //        var rep = _repo.Create(obj);
                //        if (rep != null)
                //        {
                //            var list = _mapper.Map<IEnumerable<VendorOrderEmailAttachmentViewModel>>(_repo
                //                .Find(a => a.VndrOrdrEmailId == model.VndrOrdrEmailId)
                //                .ToList());

                //            _logger.LogInformation($"Vendor Order Email Attachment Uploaded. { rep.FileNme }");
                //            return Created($"api/VendorOrderEmailAttachments/{ rep.FileNme }", list);
                //        }
                //    }
                //    else
                //    {
                //        return BadRequest(new { Message = "Attachment file name already exists." });
                //    }
                //}
            }

            return BadRequest(new { Message = "Vendor Order Email Attachment Could Not Be Uploaded." });
        }

        [HttpPut("{id}/Delete")]
        public IActionResult Delete([FromRoute] int id, [FromBody] VendorOrderEmailViewModel model)
        {
            _logger.LogInformation($"Delete Vendor Order Email Attachment By Id: {id}");
            var user = _loggedInUser.GetLoggedInUser();
            int createdBy = user.UserId;

            // Updated by Sarah Sandoval [20200928] - Commented this section due to reason below
            // This call is not needed since the ID being passed from the API is the Email Attachment Id
            // If you still need this call, this section is throwing a null error because VndrOrdrEmailAtchmt
            // from Model is not updated, it does not include the newly created attachment
            //var emailAttchId = model.VndrOrdrEmailAtchmt.Where(a => a.VndrOrdrEmailAtchmtId == id).SingleOrDefault().VndrOrdrEmailAtchmtId.ToString();

            int vndrEmailId = 0;
            if (model.EmailStusId == (short)EmailStatus.SentEnc || model.EmailStusId == (short)EmailStatus.SentUnenc)
            {
                vndrEmailId = _vndrOrdrEmailRepo.CloneVendorOrderEmail(model.VndrOrdrEmailId, id.ToString(), createdBy);
                //_repo.Delete(id);
            }
            else
            {
                _repo.Delete(id);
            }
            var count = _repo.SaveAll();

            if (count > 0)
            {
                _logger.LogInformation($"Deleted Vendor Order Email Attachment By Id: {id}");
            }

            // if vndrEmailId returns 0 it means there's no cloning happend;
            return Ok(vndrEmailId);
        }

        [HttpGet("{id}/download")]
        public IActionResult Download([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Order Email Attachment by Id: { id }.");

            var obj = _repo.Find(s => s.VndrOrdrEmailAtchmtId == id).SingleOrDefault();
            if (obj != null)
            {
                try
                {
                    return new FileContentResult(obj.FileCntnt, "application/octet")
                    {
                        FileDownloadName = obj.FileNme
                    };
                }
                catch (Exception)
                {
                    return NotFound(new { Message = $"Vendor Order Email Attachment Id: { id } not found." });
                }
            }
            else
            {
                _logger.LogInformation($"Vendor Order Email Attachment by Id: { id } not found.");
                return NotFound(new { Message = $"Vendor Order Email Attachment Id: { id } not found." });
            }
        }

        [HttpGet("uploadOffnetForm/{emailId}")]
        public IActionResult uploadOffnetForm(int emailId)
        {
            _logger.LogInformation($"Get Order Basic By Id: {emailId}");

            var email = _vndrOrdrEmailRepo.GetById(emailId);
            var vendorOrder = _vndrOrdrRepo.GetById(email.VndrOrdrId);
            var order = vendorOrder.Ordr;
            var orderBasic = _orderRepo.GetBasic(order.OrdrId).FirstOrDefault();

            OffnetForm formData = _vndrOrdrRepo.GetVendorOrderFormDynamic(order.OrdrId).FirstOrDefault();
            if (formData != null)
            {
                if (formData.ORDER_TYPE > 0 && formData.ORDER_TYPE != 8)
                {
                    orderBasic.OrdrTypeId = formData.ORDER_TYPE;
                }

                string vendorName = GetVendorName(orderBasic.InstlVndrCd);
                string formHTML = BuildVendorForm(formData, orderBasic, vendorName);
                if (formHTML.Length > 0)
                {
                    System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                    Byte[] fileContent = encoding.GetBytes(formHTML.ToString());

                    VndrOrdrEmailAtchmt attachment = new VndrOrdrEmailAtchmt
                    {
                        VndrOrdrEmailId = emailId,
                        FileNme = GetVendorOrderFormName(vendorName, orderBasic.InstlVndrCd, Enum.GetName(typeof(OrderTypes), orderBasic.OrdrTypeId), false),
                        FileSizeQty = fileContent.Length,
                        FileCntnt = fileContent,
                        RecStusId = (int)ERecStatus.Active,
                        CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                        CreatDt = DateTime.Now
                    };

                    var rep = _repo.Create(attachment);
                    if (rep != null)
                    {
                        var list = _mapper.Map<IEnumerable<VendorOrderEmailAttachmentViewModel>>(_repo
                            .Find(a => a.VndrOrdrEmailId == emailId)
                            .ToList());

                        _logger.LogInformation($"Vendor Offnet Form Attachment Uploaded. { rep.FileNme }");
                        return Created($"api/VendorOrderEmailAttachments/{ rep.FileNme }", list);
                    }
                    else
                    {
                        return BadRequest(new { Message = "Vendor Offnet Form Attachment Could Not Be Uploaded." });
                    }
                }
            }

            return BadRequest(new { Message = "Vendor Offnet Form Attachment Could Not Be Created." });
        }

        private string BuildVendorForm(OffnetForm form, OrderBasic orderBasic, string vendorName)
        {
            string vendorForm = string.Empty;
            if (form != null)
                vendorForm = Build(form, orderBasic, vendorName);
            return vendorForm;
        }

        //private string Build(string vendorName, string ftn, DateTime? custCommitDate, DateTime? custWantDate, DateTime? custSignDate, DateTime? custSubmitDate, string custEarlyAccept, string multiCustOrders,
        //    string orderIndex, string totalOrders, string tspCode)
        //{
        //    StringBuilder form = new StringBuilder();
        //    form.Append(mainTblBegin(vendorName));
        //    form.Append(GeneralFields(ftn, custCommitDate, custWantDate, custSignDate, custSubmitDate, custEarlyAccept, multiCustOrders, orderIndex, totalOrders, tspCode));
        //    form.Append(SiteInfo());
        //    form.Append(OrderType());
        //    form.Append(ProdSelection());
        //    form.Append(IntlLclAccessInfo());
        //    form.Append(IntlPort());
        //    form.Append(IPVersionInfo());
        //    form.Append(LclNetEnv());
        //    form.Append(IPAdrInfo());
        //    form.Append(ImpAddInfo());
        //    form.Append(Instructions());
        //    form.Append(Billing());
        //    form.Append(DisconnectInfo());
        //    form.Append(mainTblend());
        //    return form.ToString();
        //}
        private string Build(OffnetForm form, OrderBasic orderBasic, string vendorName)
        {
            int sectionNo = 0;
            //string vendorName = GetVendorName(vendorCode);
            StringBuilder html = new StringBuilder();
            html.Append(mainTblBegin(vendorName));
            html.Append(GeneralFields(form));
            html.Append(SiteInfo(form, sectionNo, orderBasic.OrdrTypeId, orderBasic.InstlVndrCd));
            html.Append(OrderType(form, sectionNo, orderBasic.OrdrTypeId, orderBasic.InstlVndrCd, vendorName, orderBasic.ProdTypeId));
            html.Append(ProdSelection(form, sectionNo, orderBasic.InstlVndrCd, orderBasic.ProdTypeId));
            html.Append(IntlLclAccessInfo(form, sectionNo, orderBasic.OrdrTypeId, orderBasic.InstlVndrCd, vendorName, orderBasic.ProdTypeId));
            html.Append(IntlPort(form, sectionNo, orderBasic.InstlVndrCd, orderBasic.ProdTypeId));
            html.Append(IPVersionInfo(form, sectionNo, orderBasic.OrdrTypeId, orderBasic.InstlVndrCd, orderBasic.ProdTypeId));
            html.Append(LclNetEnv(form, sectionNo, orderBasic.InstlVndrCd, orderBasic.ProdTypeId));
            html.Append(IPAdrInfo(form, sectionNo, orderBasic.InstlVndrCd, orderBasic.ProdTypeId));
            html.Append(ImpAddInfo(form, sectionNo));
            html.Append(Instructions(form, sectionNo));
            html.Append(Billing(form, sectionNo, orderBasic.InstlVndrCd));
            html.Append(DisconnectInfo(form, sectionNo, orderBasic.OrdrTypeId));
            html.Append(mainTblend());
            return html.ToString();
        }

        private string mainTblBegin(string vendorName)
        {
            string sTopTblBgn;
            sTopTblBgn = "<table border=0 width=600px>" + Environment.NewLine;
            sTopTblBgn = sTopTblBgn + "<tr><td align=center width=600px><h1>" + vendorName + "</h1></td></tr>" + Environment.NewLine;
            return sTopTblBgn;
        }

        private string GetVendorName(string vendorCode)
        {
            var lRogers = GetVendorCodeList("lRogers").ToList();
            //var lRogersVPN = GetVendorCodeList("lRogersVPN").ToList();
            var lOrange = GetVendorCodeList("lOrange").ToList();
            //var lOrangeVPN = GetVendorCodeList("lOrangeVPN").ToList();
            var lNavega = GetVendorCodeList("lNavega").ToList();
            //var lNavegaVPN = GetVendorCodeList("lNavegaVPN").ToList();
            var lGCI = GetVendorCodeList("lGCI").ToList();
            //var lGCIVPN = GetVendorCodeList("lGCIVPN").ToList();
            var lBellCanada = GetVendorCodeList("lBellCanada").ToList();
            //var lBellCanadaVPN = GetVendorCodeList("lBellCanadaVPN").ToList();
            var lChinaTelecom = GetVendorCodeList("lChinaTelecom").ToList();
            //var lChinaTelecomVPN = GetVendorCodeList("lChinaTelecomVPN").ToList();

            if (vendorCode != null)
            {
                if (!string.IsNullOrWhiteSpace(vendorCode) && lRogers.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    return "Rogers Communications";
                else if (!string.IsNullOrWhiteSpace(vendorCode) && lBellCanada.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    return "Bell Canada";
                else if (!string.IsNullOrWhiteSpace(vendorCode) && lGCI.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    return "GCI/Alaska";
                else if (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    return "Orange Business";
                else if (!string.IsNullOrWhiteSpace(vendorCode) && lChinaTelecom.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    return "China Telecom";
                else if (!string.IsNullOrWhiteSpace(vendorCode) && lNavega.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    return "Navega";
            }

            return "";
        }
        private string GetSystemConfig(string param)
        {
            var systemConfig = _systemConfigRepo.Find(a => a.PrmtrNme == param).SingleOrDefault();

            if (systemConfig != null)
            {
                return systemConfig.PrmtrValuTxt;
            }

            return String.Empty;
        }
        private List<string> GetVendorCodeList(string param)
        {
            var result = GetSystemConfig(param).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(s => s.Trim())
            .ToList();

            return result;
        }

        private string GeneralFields(OffnetForm form)
        {
            string strGen;
            strGen = "<tr><td><table border=0>" + Environment.NewLine;
            strGen = strGen + "<tr><td colspan=2>&nbsp;</td></tr>";
            strGen = strGen + formTag("FSA FTN:", form.FTN);
            strGen = strGen + formTag("Customer Commit Date:", (ConvertToShortDate(form.CUST_CMMT_DT) == "1/1/0001" ? "" : ConvertToShortDate(form.CUST_CMMT_DT)));
            strGen = strGen + formTag("Customer Want Date:", (ConvertToShortDate(form.CUST_WANT_DT) == "1/1/0001" ? "" : ConvertToShortDate(form.CUST_WANT_DT)));
            strGen = strGen + formTag("Customer Sign Date:", (ConvertToShortDate(form.CUST_SIGNED_DT) == "1/1/0001" ? "" : ConvertToShortDate(form.CUST_SIGNED_DT)));
            strGen = strGen + formTag("Customer Order Submit Date:", (ConvertToShortDate(form.CUST_ORDR_SBMT_DT) == "1/1/0001" ? "" : ConvertToShortDate(form.CUST_ORDR_SBMT_DT)));
            strGen = strGen + formTag("Will Customer accept service prior to CCD date?:", form.CUST_ACPT_ERLY_SRVC_CD);
            strGen = strGen + formTag("Are you submitting multiple orders for this customer?:", form.MULT_CUST_ORDR_CD);
            if (form.INDXNBR != null && form.TOTNBR != null)
                strGen = strGen + "<tr height=\"30px\"><td colspan=2>This is order number " + form.INDXNBR + " of " + form.TOTNBR + " total orders</td></tr>" + Environment.NewLine;
            strGen = strGen + formTag("TSP Code:", form.TSP_CD);
            strGen = strGen + "</table></td></tr>" + Environment.NewLine;
            strGen = strGen + AddSectionSeparator();
            return strGen;
        }

        private string formTag(string tagName, string tagValue)
        {
            return "<tr height=\"30px\"><td width=\"300px\">" + tagName + "</td><td align=\"right\" width=\"300px\">" + tagValue + "</td></tr>" + Environment.NewLine;
        }

        private string ConvertToShortDate(DateTime? inDate)
        {
            string shortDate = string.Empty;
            if (inDate != null)
            {
                shortDate = Convert.ToDateTime(inDate).ToShortDateString();
            }
            return shortDate;
        }

        private string AddSectionSeparator()
        {
            return "<tr><td width=600px>&nbsp;</td></tr>" + Environment.NewLine;
        }

        private string SiteInfo(OffnetForm form, int sectionNo, int orderTypeId, string vendorCode)
        {
            var lRogers = GetVendorCodeList("lRogers").ToList();
            var lOrange = GetVendorCodeList("lOrange").ToList();
            var lBellCanada = GetVendorCodeList("lBellCanada").ToList();
            var lGCI = GetVendorCodeList("lGCI").ToList();

            string strSiteInfo;
            string NPA = form.NPA;
            string NXX = form.NXX;

            if (form.CTRY_NME == "Canada" || form.CTRY_NME == "United States")
            {
                if (NPA == string.Empty && (form.SitePhn ?? "").Length >= 6)
                {
                    NPA = (form.SitePhn ?? "").Substring(0, 3);
                }
                if (NXX == string.Empty && (form.SitePhn ?? "").Length >= 6)
                {
                    NXX = (form.SitePhn ?? "").Substring(0, 3);
                }
            }
                strSiteInfo = "<tr><td><table>" + Environment.NewLine;
            sectionNo++;
            strSiteInfo = strSiteInfo + formHeaderTag("INTERNATIONAL SITE INFORMATION", sectionNo);//"<tr><td colspan=2><b>SECTION " + mySectionNo.ToString() + ":   INTERNATIONAL SITE formATION</b></td></tr>" + Environment.NewLine;
            strSiteInfo = strSiteInfo + formTag("Account/Customer Name:", form.CUST_NME);
            strSiteInfo = strSiteInfo + formTag("Site Contact Name:", form.CNTCT_NME != string.Empty ? form.CNTCT_NME : $"{form.FRST_NME} {form.LST_NME}");
            strSiteInfo = strSiteInfo + formTag("Site Address:", form.STREET_ADR_1);
            strSiteInfo = strSiteInfo + formTag("Additional Address:", form.STREET_ADR_2);
            strSiteInfo = strSiteInfo + formTag("City:", form.CTY_NME);
            strSiteInfo = strSiteInfo + formTag("Province Postal Code:", form.ZIP_PSTL_CD);
            if (((orderTypeId == Convert.ToInt16(OrderTypes.Install))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Disconnect))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Change))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Upgrade))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Downgrade))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Move))
                || (orderTypeId == Convert.ToInt16(OrderTypes.BillingChange)))
                // Added !string.IsNullOrWhiteSpace(vendorCode) condition to prevent throwing of error on vendorCode.Contains
                && ((!string.IsNullOrWhiteSpace(vendorCode) && lRogers.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    || (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    || (!string.IsNullOrWhiteSpace(vendorCode) && lBellCanada.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))))
            {
                strSiteInfo = strSiteInfo + formTag("NPA-NXX:", $"{NPA}-{NXX}");
            }
            strSiteInfo = strSiteInfo + formTag("Country:", form.CTRY_NME);
            strSiteInfo = strSiteInfo + formTag("Site Contact Phone:", form.PHN_NBR);
            strSiteInfo = strSiteInfo + formTag("Alternate Site Contact Name:", form.ALT_CNTCT_NME != string.Empty ? form.ALT_CNTCT_NME : $"{form.ALT_FRST_NME} {form.ALT_LST_NME}");
            strSiteInfo = strSiteInfo + formTag("Alternate Site Contact Phone:", form.ALT_CNTCT_PHN);
            if ((!string.IsNullOrWhiteSpace(vendorCode) && lGCI.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))))
            {
                strSiteInfo = strSiteInfo + formTag("Service Assurance Site Contact Name:", form.SRVCNTNME);
                strSiteInfo = strSiteInfo + formTag("Service Assurance Site Contact Phone:", form.SRVCNTPHN);
            }
            strSiteInfo = strSiteInfo + formTag("Is customer premise currently occupied?:", form.CUST_PRMS_OCPY_CD);
            strSiteInfo = strSiteInfo + "</table></td></tr>" + Environment.NewLine;
            strSiteInfo = strSiteInfo + AddSectionSeparator();
            return strSiteInfo;
        }

        private string formHeaderTag(string tagName, int sectionNo)
        {
            return "<tr><td colspan=2><b>SECTION " + sectionNo + ":   " + tagName + "</b></td></tr>" + Environment.NewLine;
        }

        private string OrderType(OffnetForm form, int sectionNo, int orderTypeId, string vendorCode, string vendorName, int prodTypeId)
        {
            string strOrdType;
            var lRogers = GetVendorCodeList("lRogers").ToList();
            var lBellCanada = GetVendorCodeList("lBellCanada").ToList();
            var lGCI = GetVendorCodeList("lGCI").ToList();

            strOrdType = "<tr><td><table>" + Environment.NewLine;
            sectionNo++;
            strOrdType = strOrdType + formHeaderTag("ORDER TYPE", sectionNo);
            strOrdType = strOrdType + formTag("Order Type:", form.ORDR_TYPE_DES);
            if ((orderTypeId == Convert.ToInt16(OrderTypes.Disconnect))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Change)))
            {
                strOrdType = strOrdType + formTag("Original access FTN Number of Disconnect/Change:", form.RelatedFTN);
            }
            else if ((orderTypeId == Convert.ToInt16(OrderTypes.Upgrade))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Downgrade))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Move)))
            {
                strOrdType = strOrdType + formTag("Related FTN Number:", form.RelatedFTN);
            }

            if (form.ORDER_TYPE == 8)
                strOrdType = strOrdType + formTag("FTN Number of prior pending order:", form.PARENTFTN);

            if ((orderTypeId == Convert.ToInt16(OrderTypes.Disconnect))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Change))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Upgrade))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Downgrade))
                || (orderTypeId == Convert.ToInt16(OrderTypes.Move)))
            {
                strOrdType = strOrdType + formTag("Original " + vendorName + " Service ID:", form.ORIGSRVID);
            }
            if (((!string.IsNullOrWhiteSpace(vendorCode) && lRogers.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))) && (orderTypeId == Convert.ToInt16(OrderTypes.Disconnect) || orderTypeId == Convert.ToInt16(OrderTypes.Change)) && prodTypeId != Convert.ToInt16(OrderProduct.MPLSOffnet))
                    || ((!string.IsNullOrWhiteSpace(vendorCode) && lGCI.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))) && (orderTypeId != Convert.ToInt16(OrderTypes.Install)))
                )
            {
                //do nothing, avoiding negative logic
            }
            else
                strOrdType = strOrdType + formTag("Expedite Option:", (form.FSA_EXP_TYPE_CD ?? "").Length > 0 ? "Yes" : "No");
            strOrdType = strOrdType + "</table></td></tr>" + Environment.NewLine;
            strOrdType = strOrdType + AddSectionSeparator();

            return strOrdType;
        }

        private string ProdSelection(OffnetForm form, int sectionNo, string vendorCode, int prodTypeId)
        {
            string strProdSel;
            var lOrange = GetVendorCodeList("lOrange").ToList();

            strProdSel = "<tr><td><table>" + Environment.NewLine;
            sectionNo++;
            strProdSel = strProdSel + formHeaderTag("PRODUCT SELECTION", sectionNo);
            strProdSel = strProdSel + formTag("Product Selection:", form.PROD_SELECTION);
            if (prodTypeId == Convert.ToInt16(OrderProduct.MPLSOffnet))
                strProdSel = strProdSel + formTag("Network Design Document Number:", form.INSTL_DSGN_DOC_NBR);
            if (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
            {
                //strProdSel = strProdSel + formTag("Orange Community Name:", myFormData.OrangeCommunityName);
                //strProdSel = strProdSel + formTag("Orange Site ID #:", myFormData.OrangeSiteID);
                strProdSel = strProdSel + formTag("Orange Offer Code #:", form.ORANGE_OFFR_CD);
            }
            strProdSel = strProdSel + "</table></td></tr>" + Environment.NewLine;
            strProdSel = strProdSel + AddSectionSeparator();
            return strProdSel;
        }

        private string IntlLclAccessInfo(OffnetForm form, int sectionNo, int orderTypeId, string vendorCode, string vendorName, int prodTypeId)
        {
            string strIntlLclAccInfo;
            var lRogers = GetVendorCodeList("lRogers").ToList();
            var lOrange = GetVendorCodeList("lOrange").ToList();
            var lNavega = GetVendorCodeList("lNavega").ToList();
            var lGCI = GetVendorCodeList("lGCI").ToList();
            var lBellCanada = GetVendorCodeList("lBellCanada").ToList();
            var lChinaTelecom = GetVendorCodeList("lChinaTelecom").ToList();

            strIntlLclAccInfo = "<tr><td><table>" + Environment.NewLine;
            sectionNo++;
            strIntlLclAccInfo = strIntlLclAccInfo + formHeaderTag("INTERNATIONAL LOCAL ACCESS INFORMATION", sectionNo);
            strIntlLclAccInfo = strIntlLclAccInfo + formTag("Loop Speed:", form.LoopSpeed);
            strIntlLclAccInfo = strIntlLclAccInfo + formTag("Telco Demarcation:", form.TelcoDemarc);
            strIntlLclAccInfo = strIntlLclAccInfo + formTag("Is CSU/DSU provided by " + vendorName + "?:", ConvertToYesNo(form.CSUDSU ?? ""));

            //TODO - Upgrade, Downgrade, Move, Install
            if (((!string.IsNullOrWhiteSpace(vendorCode) && lRogers.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))) && ((prodTypeId == Convert.ToInt16(OrderProduct.MPLSOffnet)) || (prodTypeId == Convert.ToInt16(OrderProduct.DIAOffnet))))
                || (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                || ((!string.IsNullOrWhiteSpace(vendorCode) && lChinaTelecom.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))) && (prodTypeId == Convert.ToInt16(OrderProduct.MPLSOffnet)))
                || ((!string.IsNullOrWhiteSpace(vendorCode) && lBellCanada.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))) && (prodTypeId == Convert.ToInt16(OrderProduct.MPLSOffnet)))
                || ((!string.IsNullOrWhiteSpace(vendorCode) && lNavega.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))) && (prodTypeId == Convert.ToInt16(OrderProduct.MPLSOffnet)))
                || ((!string.IsNullOrWhiteSpace(vendorCode) && lGCI.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))) && (prodTypeId == Convert.ToInt16(OrderProduct.MPLSOffnet)))
                )
            {
                strIntlLclAccInfo = strIntlLclAccInfo + formTag("Encapsulation Type:", form.ENCAP);
                if (orderTypeId == 1 || orderTypeId == 2 || orderTypeId == 3 || orderTypeId == 4)
                    strIntlLclAccInfo = strIntlLclAccInfo + formTag("Number of VLANS:", form.VLANQTY != null ? form.VLANQTY.ToString() : "");
            }
            if ((!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                || (!string.IsNullOrWhiteSpace(vendorCode) && lGCI.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                || (!string.IsNullOrWhiteSpace(vendorCode) && lChinaTelecom.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                || (!string.IsNullOrWhiteSpace(vendorCode) && lNavega.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))))
            {
                strIntlLclAccInfo = strIntlLclAccInfo + formTag("Interface Type:", form.INTERFACETYPE);
            }
            if (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
            {
                strIntlLclAccInfo = strIntlLclAccInfo + formTag("Interface Protocol:", form.INTPROTOCOL);
            }
            strIntlLclAccInfo = strIntlLclAccInfo + "</table></td></tr>" + Environment.NewLine;
            strIntlLclAccInfo = strIntlLclAccInfo + AddSectionSeparator();
            return strIntlLclAccInfo;
        }

        private string ConvertToYesNo(string inValue)
        {
            if (inValue.Trim().Length > 0)
            {
                inValue = (inValue == "Y" ? "Yes" : "No");
            }
            return inValue;
        }

        private string IntlPort(OffnetForm form, int sectionNo, string vendorCode, int prodTypeId)
        {
            string strIntlPort;
            var lRogers = GetVendorCodeList("lRogers").ToList();
            var lOrange = GetVendorCodeList("lOrange").ToList();

            strIntlPort = "<tr><td><table>" + Environment.NewLine;
            sectionNo++;
            strIntlPort = strIntlPort + formHeaderTag("REQUESTED INTERNATIONAL PORT CONNECTION", sectionNo);
            strIntlPort = strIntlPort + formTag("Port Speed:", form.PortSpeed);
            if (!string.IsNullOrWhiteSpace(vendorCode) && lRogers.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
            {
                if (prodTypeId == Convert.ToInt16(OrderProduct.DIAOffnet))
                    strIntlPort = strIntlPort + formTag("VAS (Value Added Service):", form.VAS);
                if (prodTypeId == Convert.ToInt16(OrderProduct.SLFROffnet))
                    strIntlPort = strIntlPort + formTag("Interface Protocol:", form.INTPROTOCOL);
            }
            else if (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
            {
                if (prodTypeId == Convert.ToInt16(OrderProduct.DIAOffnet))
                    strIntlPort = strIntlPort + formTag("VAS (Value Added Service):", form.VAS);
            }
            strIntlPort = strIntlPort + "</table></td></tr>" + Environment.NewLine;
            strIntlPort = strIntlPort + AddSectionSeparator();
            return strIntlPort;
        }

        private string IPVersionInfo(OffnetForm form, int sectionNo, int orderTypeId, string vendorCode, int prodTypeId)
        {
            var lRogers = GetVendorCodeList("lRogers").ToList();
            var lOrange = GetVendorCodeList("lOrange").ToList();
            var lNavega = GetVendorCodeList("lNavega").ToList();
            var lGCI = GetVendorCodeList("lGCI").ToList();
            var lBellCanada = GetVendorCodeList("lBellCanada").ToList();
            var lChinaTelecom = GetVendorCodeList("lChinaTelecom").ToList();

            StringBuilder response = new StringBuilder();
            if ((((!string.IsNullOrWhiteSpace(vendorCode) && lRogers.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))) && ((prodTypeId == Convert.ToByte(OrderProduct.DIAOffnet)) || (prodTypeId == Convert.ToByte(OrderProduct.MPLSOffnet))))
                || (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                || (((!string.IsNullOrWhiteSpace(vendorCode) && lChinaTelecom.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    || (!string.IsNullOrWhiteSpace(vendorCode) && lNavega.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    || (!string.IsNullOrWhiteSpace(vendorCode) && lBellCanada.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    || (!string.IsNullOrWhiteSpace(vendorCode) && lGCI.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))) && (prodTypeId == Convert.ToByte(OrderProduct.MPLSOffnet)))
               )
                && ((orderTypeId == Convert.ToByte(OrderTypes.Upgrade))
                    || (orderTypeId == Convert.ToByte(OrderTypes.Downgrade))
                    || (orderTypeId == Convert.ToByte(OrderTypes.Move))
                    || (orderTypeId == Convert.ToByte(OrderTypes.Install))))
            {
                response.Append("<tr><td><table>" + Environment.NewLine);
                sectionNo++;
                response.Append(formHeaderTag("IP VERSION INFORMATION", sectionNo));
                response.Append(formTag("IP Version:", form.IPVER));
                response.Append(formTag("Number of Sprint Provided IPv6 Addresses?:", form.PROVIDEQTY));
                response.Append(formTag("IPv4 Address Provider:", form.IPV4PROVIDER));
                response.Append(formTag("IPv6 Address Provider:", form.IPV6PROVIDER));
                response.Append("</table></td></tr>" + Environment.NewLine);
                response.Append(AddSectionSeparator());
            }
            return response.ToString();
        }

        private string LclNetEnv(OffnetForm form, int sectionNo, string vendorCode, int prodTypeId)
        {
            var lRogers = GetVendorCodeList("lRogers").ToList();
            var lOrange = GetVendorCodeList("lOrange").ToList();

            string strLclNetEnv = string.Empty;
            if (prodTypeId == Convert.ToInt16(OrderProduct.DIAOffnet))
            {
                if ((!string.IsNullOrWhiteSpace(vendorCode) && lRogers.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    || (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))))
                {
                    strLclNetEnv = "<tr><td><table>" + Environment.NewLine;
                    sectionNo++;
                    strLclNetEnv = strLclNetEnv + formHeaderTag("LOCAL AREA NETWORK ENVIRONMENT/SITE SURVEY", sectionNo);
                    strLclNetEnv = strLclNetEnv + formTag("Protocols:", form.PROTOCOLS);
                    strLclNetEnv = strLclNetEnv + "</table></td></tr>" + Environment.NewLine;
                    strLclNetEnv = strLclNetEnv + AddSectionSeparator();
                }
            }
            return strLclNetEnv;
        }

        private string IPAdrInfo(OffnetForm form, int sectionNo, string vendorCode, int prodTypeId)
        {
            string strIPAdrInfo = string.Empty;
            var lRogers = GetVendorCodeList("lRogers").ToList();
            var lOrange = GetVendorCodeList("lOrange").ToList();

            if (prodTypeId == Convert.ToInt16(OrderProduct.DIAOffnet))
            {
                if ((!string.IsNullOrWhiteSpace(vendorCode) && lRogers.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                    || (!string.IsNullOrWhiteSpace(vendorCode) && lOrange.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase))))
                {
                    int ipv4Qty = 0;
                    int.TryParse(form.IPV4QTY, out ipv4Qty);

                    strIPAdrInfo = "<tr><td><table>" + Environment.NewLine;
                    sectionNo++;
                    strIPAdrInfo = strIPAdrInfo + formHeaderTag("DNS SERVICES/IP ADDRESS INFORMATION", sectionNo);
                    strIPAdrInfo = strIPAdrInfo + formTag("DNS Service Information:", "&nbsp");
                    strIPAdrInfo = strIPAdrInfo + formTag("Are additional IP Addresses needed?:", form.ADDIPADR);
                    strIPAdrInfo = strIPAdrInfo + formTag("Quantity of IP Addresses Requested:", ipv4Qty == 0 ? string.Empty : ipv4Qty.ToString());
                    strIPAdrInfo = strIPAdrInfo + formTag("Current Provider of IP Addresses:", form.IPPROVIDER);
                    strIPAdrInfo = strIPAdrInfo + formTag("Existing IP Addresses:", form.EXISTIPFLG);
                    strIPAdrInfo = strIPAdrInfo + formTag("Subnet Mask Range (Existing IP Addresses):", form.SUBMASK);
                    strIPAdrInfo = strIPAdrInfo + "<tr height=\"30px\"><td valign=\"top\">List Existing Addresses:</td><td align=\"right\">" + (form.IPLIST ?? "").Replace(",", "<br>") + "</td></tr>" + Environment.NewLine;
                    strIPAdrInfo = strIPAdrInfo + formTag("Domain Name:", form.DOMAIN);
                    strIPAdrInfo = strIPAdrInfo + formTag("Does Customer have Firewall?:", form.Firewall);
                    strIPAdrInfo = strIPAdrInfo + formTag("DNS Administrator Name:", form.DNSNME);
                    strIPAdrInfo = strIPAdrInfo + formTag("DNS Administrator Phone:", form.DNSPHN);
                    strIPAdrInfo = strIPAdrInfo + formTag("DNS Administrator Ext:", form.DNS_PHN_EXT);
                    strIPAdrInfo = strIPAdrInfo + formTag("DNS Administrator Email:", form.DNSEMAIL);
                    strIPAdrInfo = strIPAdrInfo + "</table></td></tr>" + Environment.NewLine;
                    strIPAdrInfo = strIPAdrInfo + AddSectionSeparator();
                }
            }
            return strIPAdrInfo;
        }

        private string ImpAddInfo(OffnetForm form, int sectionNo)
        {
            string strImpAdInfo;
            strImpAdInfo = "<tr><td><table>" + Environment.NewLine;
            sectionNo++;
            strImpAdInfo = strImpAdInfo + formHeaderTag("IMPORTANT ADDITIONAL INFORMATION", sectionNo);
            strImpAdInfo = strImpAdInfo + formTag("Sprint GSL Quote Number:", form.TSUP_PRS_QOT_NBR);
            strImpAdInfo = strImpAdInfo + formTag("SCA/AST Number:", form.SCA_NBR);
            strImpAdInfo = strImpAdInfo + formTag("Is there an associated MNS Order:", form.TTRPT_MNGD_DATA_SRVC_CD);
            strImpAdInfo = strImpAdInfo + "</table></td></tr>" + Environment.NewLine;
            strImpAdInfo = strImpAdInfo + AddSectionSeparator();
            return strImpAdInfo;
        }

        private string Instructions(OffnetForm form, int sectionNo)
        {
            string strIns;
            strIns = "<tr><td><table>" + Environment.NewLine;
            sectionNo++;
            strIns = strIns + formHeaderTag("INSTRUCTIONS/COMMENTS/DESCRIPTION", sectionNo);
            strIns = strIns + formTag("General Instructions/Comments:", form.COMMENTS);
            strIns = strIns + "</table></td></tr>" + Environment.NewLine;
            strIns = strIns + AddSectionSeparator();
            return strIns;
        }

        private string Billing(OffnetForm form, int sectionNo, string vendorCode)
        {
            string strBilling;
            var lBellCanada = GetVendorCodeList("lBellCanada").ToList();

            strBilling = "<tr><td><table>" + Environment.NewLine;
            sectionNo++;
            if (!string.IsNullOrWhiteSpace(vendorCode) && lBellCanada.Any(a => vendorCode.Contains(a, StringComparison.CurrentCultureIgnoreCase)))
                strBilling = strBilling + formHeaderTag("SPRINT BILLING INFORMATION", sectionNo);
            else
                strBilling = strBilling + formHeaderTag("BILLING INFORMATION", sectionNo);
            strBilling = strBilling + formTag("H5 International:", form.H5_CUST_ID.ToString());
            strBilling = strBilling + formTag("H6 Domestic:", form.H6_CUST_ID.ToString());
            strBilling = strBilling + formTag("Service Term:", form.VNDRTERM);
            strBilling = strBilling + "</table></td></tr>" + Environment.NewLine;
            strBilling = strBilling + AddSectionSeparator();
            return strBilling;
        }

        private string DisconnectInfo(OffnetForm form, int sectionNo, int orderTypeId)
        {
            StringBuilder returnValue = new StringBuilder();
            sectionNo++;
            if (orderTypeId == Convert.ToInt16(OrderTypes.Move))
            {
                returnValue.Append("<tr><td><table>" + Environment.NewLine);
                returnValue.Append(formHeaderTag("RELATED DISCONNECT INFORMATION", sectionNo));
                returnValue.Append(formTag("Site Information:", string.Empty));
                returnValue.Append(formTag("Customer Name:", form.RD_CUST_NME));
                returnValue.Append(formTag("Site Address:", form.RD_STREET_ADR_1));
                returnValue.Append(formTag("Site Contact Name:", form.RD_CNTCT_NME != string.Empty ? form.RD_CNTCT_NME : $"{form.RD_FRST_NME} {form.RD_LST_NME}"));
                returnValue.Append(formTag("Additional Address:", form.RD_STREET_ADR_2));
                returnValue.Append(formTag("City:", form.RD_CTY_NME));
                returnValue.Append(formTag("Province Postal Code:", form.RD_ZIP_PSTL_CD));
                returnValue.Append(formTag("Country:", form.RD_CTRY_NME));
                returnValue.Append(formTag("Site Contact Number:", form.RD_PHN_NBR));
                returnValue.Append(formTag("Alternate Site Contact Name:", form.RD_ALT_CNTCT_NME != string.Empty ? form.RD_ALT_CNTCT_NME : $"{form.RD_ALT_FRST_NME} {form.RD_ALT_LST_NME}"));
                returnValue.Append(formTag("Alternate Site Contact Number:", form.RD_ALT_CNTCT_PHN));
                returnValue.Append("</table></td></tr>" + Environment.NewLine);
            }
            return returnValue.ToString();
        }

        private string mainTblend()
        {
            string sTopTblEnd;
            sTopTblEnd = "</table>" + Environment.NewLine;
            return sTopTblEnd;
        }

        private string GetVendorOrderFormName(string inVendorName, string inVendorID, string inOrderType, bool isExcel)
        {
            Regex exp = new Regex("[^A-Za-z0-9]");
            var lBellCanada = GetVendorCodeList("lBellCanada").ToList();
            inVendorName = exp.Replace(inVendorName, string.Empty);

            //inVendorID = exp.Replace(inVendorID, string.Empty);
            StringBuilder formName = new StringBuilder(inVendorName);
            formName.Append("-");
            // Added condition to prevent throwing on inVendorID null value
            if (!string.IsNullOrWhiteSpace(inVendorID))
            {
                formName.Append(exp.Replace(inVendorID, string.Empty));
                formName.Append("_");
            }
            
            formName.Append(inOrderType);
            formName.Append("_");
            formName.Append(DateTime.Now.Month.ToString());
            formName.Append("-");
            formName.Append(DateTime.Now.Day.ToString());
            formName.Append("-");
            formName.Append(DateTime.Now.Year.ToString());
            formName.Append("_");
            formName.Append(DateTime.Now.Hour.ToString());
            formName.Append("-");
            formName.Append(DateTime.Now.Minute.ToString());
            formName.Append("-");
            formName.Append(DateTime.Now.Second.ToString());
            if ((lBellCanada.Where(p => (p == inVendorID)).Count() > 0) && isExcel)
                formName.Append(".xls");
            else
                formName.Append(".html");
            return formName.ToString();
        }
    }
}
