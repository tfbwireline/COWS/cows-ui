﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSNetworkActivityTypes")]
    [ApiController]
    public class MDSNetworkActivityTypeController : ControllerBase
    {
        private readonly IMDSNetworkActivityTypeRepository _repo;
        private readonly ILogger<MDSNetworkActivityTypeController> _logger;
        private readonly IMapper _mapper;

        public MDSNetworkActivityTypeController(IMapper mapper,
                               IMDSNetworkActivityTypeRepository repo,
                               ILogger<MDSNetworkActivityTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MDSNetworkActivityTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MDSNetworkActivityTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.NtwkActyTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MDS Network Activity Type by Id: { id }.");

            var obj = _repo.Find(s => s.NtwkActyTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSNetworkActivityTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDS Network Activity Type by Id: { id } not found.");
                return NotFound(new { Message = $"MDS Network Activity Type Id: { id } not found." });
            }
        }
    }
}