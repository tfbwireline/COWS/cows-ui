﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Events/MDS")]
    [ApiController]
    public class MdsEventController : ControllerBase
    {
        private readonly IMdsEventRepository _repo;
        private readonly ICommonRepository _commonRepo;
        //private readonly IMpls _mplsActivityTypeRepository;
        private readonly IEventAsnToUserRepository _eventAsnToUserRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapUserProfileRepository _mapUserProfileRepository;
        private readonly IEventHistoryRepository _eventHistoryRepository;
        private readonly IEventRuleRepository _eventRuleRepository;
        private readonly ILogger<MdsEventController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IL2PInterfaceService _l2pInterface;
        private readonly IApptRepository _apptRepository;
        private readonly IEmailReqRepository _emailReqRepository;
        private readonly IEventFailActyRepository _failEventActyRepository;
        private readonly IEventSucssActyRepository _sucssEventActyRepository;
        private readonly IFsaOrderRepository _fsaOrderRepo;
        private readonly INidActyRepository _nidActyRepo;
        private readonly IContactDetailRepository _contctRepo;
        private readonly IEventLockRepository _eventLockRepo;
        private IMemoryCache _cache;

        public MdsEventController(IMapper mapper,
                               IMdsEventRepository repo,
                               //IMplsActivityTypeRepository mplsActivityTypeRepository,
                               IEventAsnToUserRepository eventAsnToUserRepository,
                               IUserRepository userRepository,
                               IMapUserProfileRepository mapUserProfileRepository,
                               IEventHistoryRepository eventHistoryRepository,
                               IEventRuleRepository eventRuleRepository,
                               ILogger<MdsEventController> logger,
                               ILoggedInUserService loggedInUser,
                               IL2PInterfaceService l2pInterface,
                               IApptRepository apptRepository,
                               IEmailReqRepository emailReqRepository,
                               IEventFailActyRepository failEventActyRepository,
                               IEventSucssActyRepository sucssEventActyRepository,
                               IFsaOrderRepository fsaOrderRepo,
                               INidActyRepository nidActyRepo,
                               IContactDetailRepository contctRepo,
                               IEventLockRepository eventLockRepo,
                               IMemoryCache cache,
                               ICommonRepository commonRepo)
        {
            _mapper = mapper;
            _repo = repo;
            //_mplsActivityTypeRepository = mplsActivityTypeRepository;
            _eventAsnToUserRepository = eventAsnToUserRepository;
            _userRepository = userRepository;
            _mapUserProfileRepository = mapUserProfileRepository;
            _eventHistoryRepository = eventHistoryRepository;
            _eventRuleRepository = eventRuleRepository;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _l2pInterface = l2pInterface;
            _apptRepository = apptRepository;
            _emailReqRepository = emailReqRepository;
            _failEventActyRepository = failEventActyRepository;
            _sucssEventActyRepository = sucssEventActyRepository;
            _fsaOrderRepo = fsaOrderRepo;
            _nidActyRepo = nidActyRepo;
            _contctRepo = contctRepo;
            _eventLockRepo = eventLockRepo;
            _cache = cache;
            _commonRepo = commonRepo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MdsEventViewModel>> Get()
        {
            IEnumerable<MdsEventViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.MdsEventList, out list))
            {
                list = _mapper.Map<IEnumerable<MdsEventViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderByDescending(s => s.EventId));

                CacheManager.Set(_cache, CacheKeys.MdsEventList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"TEST KM(^&&^!");
            _logger.LogInformation($"Search IPSD Event by Id: { id }.");
            string adid = _loggedInUser.GetLoggedInUserAdid();


            var obj = _repo.GetById(id, adid);
            if (obj != null)
            {
                var result = _mapper.Map<MdsEventViewModel>(obj);

                result.NetworkTransport = result.NetworkTransport.Where(a => a.RecStusId == 1).ToList();
                if (result.NetworkTransport.Count > 0)
                {
                    var nid = _nidActyRepo.GetSerialNumberByH6(id, string.Join(',', result.NetworkTransport.Select(a => a.AssocH6)));

                    result.NetworkTransport.ToList().ForEach(a =>
                    {
                        var temp = nid.Where(b => b.H6 == a.AssocH6).FirstOrDefault();
                        a.NidActyId = temp != null ? temp.NidActyId : 0;
                        a.NIDSerialNbr = temp != null ? temp.NidSerialNbr : string.Empty;
                        a.NidHostName = temp != null ? temp.NidHostname : string.Empty;
                        a.NidIpAdr = temp != null ? temp.NidIpAdr : string.Empty;
                    });
                }

                return Ok(result);
            }
            else
            {
                _logger.LogInformation($"IPSD Event by Id: { id } not found.");
                return NotFound(new { Message = $"IPSD Event Id: { id } not found." });
            }
        }

        //[HttpPost("GetDistinctH6Serial")]
        //public ActionResult GetDistinctH6Serial([FromBody] MdsEventViewModel model)
        //{
        //    _logger.LogInformation($"Search Distinct combination of inactive H6-NID_SERIAL_NBR");

        //    if (model.NetworkTransport.Count > 0)
        //    {
        //        var nid = _nidActyRepo.GetSerialNumberByH6(model.EventId, string.Join(',', model.NetworkTransport.Select(a => a.AssocH6)));

        //        return Ok(nid.Result2);
        //    }
        //    else
        //    {
        //        _logger.LogInformation($"Cannot search inactive H6-SERIAL_NBR with the given data");
        //        return NotFound(new { Message = $"Cannot search inactive H6-SERIAL_NBR with the given data" });
        //    }
        //}

        [HttpPost]
        public ActionResult Post([FromBody] MdsEventView model)
        {
            int userId = _loggedInUser.GetLoggedInUserId();

            if (ModelState.IsValid)
            {
                // Added by Sarah Sandoval [20210922] - To cater more than 20 chars on Port Bandwidth for DB and Email Notification
                if (model.NetworkTransport != null && model.NetworkTransport.Count() > 0)
                {
                    model.NetworkTransport = model.NetworkTransport.Select(a =>
                    {
                        a.BdwdNme = a.BdwdNme.Length > 20 ? a.BdwdNme.Substring(0, 20) : a.BdwdNme;
                        return a;
                    }).ToList();
                }

                MdsEvent MdsEvent = _mapper.Map<MdsEvent>(model);

                //MdsEvent.Event = new Event();
                MdsEvent.Event.EventTypeId = (int)EventType.MDS;
                MdsEvent.Event.CreatDt = DateTime.Now;
                MdsEvent.CreatByUserId = userId;
                MdsEvent.SiteIdTxt = model.SiteIdTxt;
                MdsEvent.MdsFastTrkTypeId = model.MdsFastTrkTypeId;
                //MdsEvent.ModfdByUserId = userId;

                // Validate H1
                if (!string.IsNullOrWhiteSpace(MdsEvent.H1))
                {
                    _logger.LogInformation($"Checking if user is authorized for H1: { MdsEvent.H1 }.");
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(MdsEvent.H1))
                    {
                        _logger.LogInformation($"Checking if UserID: { userId } is authorized for H1: { MdsEvent.H1 }.");
                        //if (MdsEvent.NtwkActyTypeId == 1 || MdsEvent.NtwkActyTypeId == 3) // MDS ONLY
                        //{
                        return BadRequest(new { Message = "You are not an authorized user for this H1." });
                        //}
                    }

                    MdsEvent.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(MdsEvent.H1, string.Empty, string.Empty);
                }

                // Validate Network H1
                if (!string.IsNullOrWhiteSpace(MdsEvent.NtwkH1))
                {
                    _logger.LogInformation($"Checking if user is authorized for H1 (Network): { MdsEvent.NtwkH1 }.");
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(MdsEvent.NtwkH1))
                    {
                        _logger.LogInformation($"Checking if UserID: { userId } is authorized for H1 (Network): { MdsEvent.NtwkH1 }.");
                        //if (MdsEvent.NtwkActyTypeId == 2 || MdsEvent.NtwkActyTypeId == 3) // NETWORK ONLY
                        //{
                        return BadRequest(new { Message = "You are not an authorized user for this H1 (Network)." });
                        //}
                    }

                    MdsEvent.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(MdsEvent.NtwkH1, string.Empty, string.Empty);
                }

                LkEventRule eventRule = _eventRuleRepository.GetEventRule2(MdsEvent.EventStusId, MdsEvent.WrkflwStusId, userId, (int)EventType.MDS, null, model.FailReasId, model.MdsActyTypeId);
                //LkEventRule eventRule = _eventRuleRepository.GetEventRule(MdsEvent.EventStusId, MdsEvent.WrkflwStusId, userId, (int)EventType.MDS);

                if (eventRule != null)
                {
                    MdsEvent.EventStusId = eventRule.EndEventStusId;
                    MdsEvent.Event.MdsEventNtwkActy = model.MdsEventNtwkActyIds
                        .Select(a =>
                        {
                            return new MdsEventNtwkActy
                            {
                                Id = 0,
                                EventId = MdsEvent.EventId,
                                NtwkActyTypeId = (byte)a,
                                CreatDt = DateTime.Now
                            };
                        })
                        .ToList();

                    if (CheckMDSNtwkActy(ref MdsEvent, "hasNtwk") || CheckMDSNtwkActy(ref MdsEvent, "hasVAS")) // IF NETWORK
                    {
                        MdsEvent.Event.MplsEventActyType = model.MplsEventActyTypeIds
                            .Select(a => new MplsEventActyType
                            {
                                EventId = MdsEvent.EventId,
                                MplsActyTypeId = (byte)a
                            })
                            .ToList();

                        //if (MdsEvent.Event.MplsEventActyType.Any(a => a.MplsActyTypeId == 21) && model.NetworkTransport.Any(a => (a.NIDSerialNbr ?? "").Length > 0))
                        //{
                        //    //var distinct_h6Serial = _nidActyRepo.GetSerialNumberByH6(model.EventId, string.Join(',', model.NetworkTransport.Select(a => a.AssocH6)));

                        //    model.NetworkTransport
                        //        .Where(a => (a.NIDSerialNbr ?? "").Length > 0)
                        //        .DistinctBy(a => new { a.AssocH6, a.DdAprvlNbr, a.NidHostName, a.NIDSerialNbr })
                        //        .ToList()
                        //        .ForEach(a =>
                        //        {
                        //            //var isExist = distinct_h6Serial.Result2.Any(b => b.H6 == a.AssocH6 && b.NidSerialNbr == a.NIDSerialNbr);
                        //            var isExist = _nidActyRepo.Find(b =>
                        //                b.RecStusId == 251 &&
                        //                b.NidSerialNbr == a.NIDSerialNbr &&
                        //                b.NidHostNme == a.NidHostName &&
                        //                ((b.DdAprvlNbr ?? "").Length <= 0 || b.DdAprvlNbr == a.DdAprvlNbr) &&
                        //                b.H6 == a.AssocH6
                        //            ).Count() > 0;

                        //            if (!isExist)
                        //            {
                        //                NidActy nid = new NidActy();
                        //                nid.NidSerialNbr = a.NIDSerialNbr;
                        //                nid.NidHostNme = a.NidHostName;
                        //                nid.FsaCpeLineItemId = null;
                        //                nid.M5OrdrNbr = null;
                        //                nid.DdAprvlNbr = a.DdAprvlNbr;
                        //                nid.H6 = a.AssocH6;
                        //                nid.RecStusId = 251;
                        //                nid.CreatDt = DateTime.Now;
                        //                nid.CreatByUserId = _loggedInUser.GetLoggedInUserId();

                        //                MdsEvent.Event.NidActy.Add(nid);
                        //            }
                        //        });
                        //}
                        if (MdsEvent.Event.MplsEventActyType.Any(a => a.MplsActyTypeId == 20 || a.MplsActyTypeId == 21) && model.NetworkTransport.Any(a => (a.NIDSerialNbr ?? "").Length > 0))
                        {
                            model.NetworkTransport
                                .Where(a => (a.NIDSerialNbr ?? "").Length > 0)
                                .ToList()
                                .ForEach(a =>
                                {
                                    //var activeNid = _nidActyRepo.Find(b => b.RecStusId == 251).ToList();
                                    //var fsaOrder = _fsaOrderRepo.GetFsaOrderCpeLineItem(b =>
                                    //    ((int)b.Ordr.H5H6CustId).ToString() == a.AssocH6 &&
                                    //    b.FsaOrdrCpeLineItem.Any(c =>
                                    //        c.CmpntFmly == "NID" &&
                                    //        c.SprintMntdFlg == "Y" &&
                                    //        c.ItmStus == 401 &&
                                    //        !activeNid.Any(d => d.FsaCpeLineItemId == c.FsaCpeLineItemId)
                                    //    )
                                    //).FirstOrDefault();

                                    var existing = _nidActyRepo.Find(b =>
                                        b.RecStusId == 251 &&
                                        b.NidSerialNbr == a.NIDSerialNbr &&
                                        (b.DdAprvlNbr ?? "").Length <= 0 &&
                                        EF.Functions.Like(b.FsaCpeLineItem.CmpntFmly, "NID%") &&
                                        b.FsaCpeLineItem.ItmStus == 401 &&
                                        ((int)b.FsaCpeLineItem.Ordr.Ordr.H5H6CustId).ToString() == a.AssocH6
                                    ).ToList();

                                    if (existing != null)
                                    {
                                        existing.ForEach(b =>
                                        {
                                            //b.EventId = MdsEvent.EventId;
                                            b.NidHostNme = a.NidHostName;
                                            b.DdAprvlNbr = a.DdAprvlNbr;
                                            b.ModfdDt = DateTime.Now;
                                            b.ModfdByUserId = _loggedInUser.GetLoggedInUserId();

                                            MdsEvent.Event.NidActy.Add(b);
                                        });

                                        //MdsEvent.Event.NidActy = existing;
                                    }

                                    if (MdsEvent.Event.MplsEventActyType.Any(b => b.MplsActyTypeId == 21))
                                    {
                                        var record_existing = _nidActyRepo.Find(b =>
                                            b.RecStusId == 251 &&
                                            b.NidSerialNbr == a.NIDSerialNbr &&
                                            b.H6 == a.AssocH6
                                        ).Any();

                                        if (!record_existing)
                                        {
                                            NidActy nid = new NidActy();
                                            nid.NidSerialNbr = a.NIDSerialNbr;
                                            nid.NidHostNme = a.NidHostName;
                                            nid.FsaCpeLineItemId = null;
                                            nid.M5OrdrNbr = null;
                                            nid.DdAprvlNbr = a.DdAprvlNbr;
                                            nid.H6 = a.AssocH6;
                                            nid.RecStusId = 251;
                                            nid.CreatDt = DateTime.Now;
                                            nid.CreatByUserId = _loggedInUser.GetLoggedInUserId();

                                            MdsEvent.Event.NidActy.Add(nid);
                                        }
                                    }
                                });
                        }
                    }

                    if (CheckMDSNtwkActy(ref MdsEvent, "hasMDS")) // IF MDS
                    {
                        if (MdsEvent.MdsActyTypeId != 3)
                        {
                            MdsEvent.Event.MdsEventMacActy = model.MdsEventMacActyIds
                                .Select(a => new MdsEventMacActy
                                {
                                    EventId = MdsEvent.EventId,
                                    MdsMacActyId = (byte)a
                                })
                                .ToList();
                        }
                        //MdsEvent.Event.MdsEventOdieDev = MdsEvent.Event.MdsEventOdieDev
                        //    .Select(a =>
                        //    {
                        //        a.CreatDt = (a.MdsEventOdieDevId == 0) ? DateTime.Now : a.CreatDt;
                        //        a.SysCd = (a.MdsEventOdieDevId == 0) ? true : false;

                        //        return a;
                        //    })
                        //    .ToList();

                        //MdsEvent.Event.EventCpeDev = MdsEvent.Event.EventCpeDev
                        //    .Select(a => {
                        //        a.Ccd = DateTime.Now;
                        //        a.CreatDt = DateTime.Now;

                        //        return a;
                        //    })
                        //    .ToList();
                    }

                    ////Create Event and MdsEvent
                    //var mds = new MdsEvent();
                    var mds = _repo.Create(MdsEvent);


                    // [Updated by Joshua Bolano - 20210614] - Save Contact Details (PJ025845)
                    if (model.ContactDetails != null && model.ContactDetails.Count() > 0)
                    {
                        var cntctDtl = _mapper.Map<IEnumerable<CntctDetl>>(model.ContactDetails).ToList();
                        try
                        {
                            cntctDtl.ForEach(item => {
                                item.ObjTypCd = "E";
                                item.ObjId = mds.EventId;
                                item.RecStusId = (short)ERecStatus.Active;
                                //item.CreatByUserId = item.CreatByUserId == 1 ? item.CreatByUserId : _loggedInUser.GetLoggedInUserId();
                                item.CreatDt = DateTime.Now;
                                item.ModfdByUserId = null;
                                item.ModfdDt = null;
                            });

                            _contctRepo.Create(cntctDtl);

                            // Added by Sarah Sandoval [20211115] as per new requirement: Add manually entered Contact List to Event History
                            if (cntctDtl.Any(i => i.CreatByUserId > 1))
                            {
                                string emails = string.Empty;
                                emails = string.Join(",", cntctDtl.FindAll(i => i.CreatByUserId > 1).Select(i => i.EmailAdr).ToList());
                                if (!string.IsNullOrWhiteSpace(emails))
                                {
                                    _eventHistoryRepository.Create(new EventHist {
                                        EventId = mds.EventId,
                                        ActnId = (byte)Actions.ManualContactListData,
                                        CmntTxt = "The following email/s were added manually to Contact List table: " + emails,
                                        CreatDt = DateTime.Now,
                                        CreatByUserId = _loggedInUser.GetLoggedInUserId()
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogInformation($"IPSD Contact Detail could not be created. { JsonConvert.SerializeObject(cntctDtl, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                        }
                    }

                    //Create EventAsnToUser
                    _eventAsnToUserRepository.Create(model.Activators
                        .Select(a => new EventAsnToUser
                        {
                            EventId = mds.EventId,
                            AsnToUserId = a,
                            CreatDt = mds.CreatDt,
                            RecStusId = 0,
                            RoleId = 0
                        }).ToList());

                    List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                    assignUsers = _eventAsnToUserRepository.Find(a => a.EventId == mds.EventId).ToList();

                    if (mds != null)
                    {
                        _logger.LogInformation($"IPSD Event Created. { JsonConvert.SerializeObject(mds, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                        // Do things needed based on eventRule like event history and email sender
                        var json = new JsonSerializerSettings()
                        {
                            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                            Formatting = Formatting.Indented
                        };
                        //_eventHistoryRepository.CreateEventHistory(mds.EventId, eventRule.ActnId, mds.StrtTmst, mds.EndTmst, userId);
                        // Create Event History; Reviewer and Activator is not needed for new MPLS Event
                        EventHist eh = new EventHist();
                        eh.EventId = mds.EventId;
                        eh.CmntTxt = model.CmntTxt;
                        eh.ActnId = eventRule.ActnId;
                        eh.EventStrtTmst = mds.StrtTmst;
                        eh.EventEndTmst = mds.EndTmst;
                        eh.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                        _eventHistoryRepository.CreateEventHistory(eh);

                        // Save Calendar Entry and Email Sender
                        EventWorkflow esw = SetEventWorkFlow(mds, assignUsers);
                        bool isMDSFT = false;
                        if (mds.FrcdftCd == true || mds.MdsFastTrkTypeId == "A")
                        {
                            isMDSFT = true;
                        }

                        bool isNtwkIntl = false;
                        isNtwkIntl = mds.Event.MdsEventNtwkActy.Any(i => i.NtwkActyTypeId == 4);
                        esw.IsNetworkIntl = isNtwkIntl;
                        esw.IsNetwork = CheckMDSNtwkActy(ref MdsEvent, "hasNtwk");
                        //esw.AssignUser = assignUsers;
                        esw.EventRule = eventRule;
                        esw.IsSupressEmail = model.IsSuppressedEmails;
                        // Added by Sarah Sandoval [20220222]
                        // Pass MNSPM Assignment to include in Submitted Escalation Email
                        esw.ReviewerAdId = (esw.Profile == "Member" && esw.ReviewerId <= 0) ? mds.MnsPmId : string.Empty;

                        if (_apptRepository.CalendarEntry(esw, isMDSFT, isNtwkIntl))
                            _logger.LogInformation($"IPSD Event Calender Entry success for EventID. { JsonConvert.SerializeObject(mds.EventId, json) } ");

                        //if (_emailReqRepository.SendMDSMail(esw, mds))
                        //    _logger.LogInformation($"IPSD Event Email Entry success for EventID. { JsonConvert.SerializeObject(mds.EventId, json) } ");

                        // Added for Network International Email
                        var ntwkUser = _userRepository.Find(i => i.UserAdid == "NtwkIntl").FirstOrDefault();
                        if ((model.MdsEventNtwkActyIds.Contains(4)
                            && model.WrkflwStusId == (byte)WorkflowStatus.Submit
                            && ntwkUser != null && model.Activators.Contains(ntwkUser.UserId))
                            || esw.IsEscalation)
                        //if (esw.IsEscalation)
                        {

                            if (_emailReqRepository.SendMDSMail(mds, esw, model.NetworkTransport))
                                _logger.LogInformation($"IPSD Event Email Entry success for EventID. { JsonConvert.SerializeObject(mds.EventId, json) } ");
                            else
                                _logger.LogError($"IPSD Event Email Entry failed for EventID. { JsonConvert.SerializeObject(mds.EventId, json) } ");

                        }

                        return Created($"api/MDS/{ mds.EventId}", mds);
                    }
                }
                else
                {
                    return BadRequest(new { Message = "You are not authorized to perform this action" });
                }
            }

            return BadRequest(new { Message = "IPSD Event Could Not Be Created" });
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] MdsEventView model)
        {
            var start = DateTime.Now;
            var json = new JsonSerializerSettings()
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            if (ModelState.IsValid)
            {
                int userId = _loggedInUser.GetLoggedInUserId();
                //var end = DateTime.Now;
                // Added by Sarah Sandoval [20210922] - To cater more than 20 chars on Port Bandwidth for DB and Email Notification
                if (model.NetworkTransport != null && model.NetworkTransport.Count() > 0)
                {
                    model.NetworkTransport = model.NetworkTransport.Select(a =>
                    {
                        a.BdwdNme = a.BdwdNme.Length > 20 ? a.BdwdNme.Substring(0, 20) : a.BdwdNme;
                        return a;
                    }).ToList();
                }
                MdsEvent MdsEvent = _mapper.Map<MdsEvent>(model);
                //var endd = DateTime.Now;
                MdsEvent.FailReasId = model.FailReasId;
                MdsEvent.ModfdByUserId = userId;
                MdsEvent.ModfdDt = DateTime.Now;
                MdsEvent.SiteIdTxt = model.SiteIdTxt;
                MdsEvent.WoobIpAdr = model.WoobIpAdr;
                MdsEvent.CustLtrOptOut = model.CustLtrOptOut;
                //MdsEvent.Event.MdsEventMacActy = model.MdsEventMacActyIds
                //                .Select(a => new MdsEventMacActy
                //                {
                //                    EventId = MdsEvent.EventId,
                //                    MdsMacActyId = (byte)a,
                //                    CreatDt = DateTime.Now
                //                })
                //                .ToList();
                //if (MdsEvent.MdsActyTypeId == null) {
                //    MdsEvent.MdsActyTypeId = 0;
                //}

                //if (String.IsNullOrWhiteSpace(MdsEvent.MdsFastTrkTypeId))
                //{
                //    MdsEvent.MdsFastTrkTypeId = "";
                //}

                var mdsOld = _repo.GetById(MdsEvent.EventId);//~2s
                //var enddd = DateTime.Now;
                _eventHistoryRepository.InsertEventFormChanges(MdsEvent, mdsOld);
                //var endddd = DateTime.Now;

                // Validate H1
                if (!string.IsNullOrWhiteSpace(MdsEvent.H1))
                {
                    _logger.LogInformation($"Checking if user is authorized for H1: { MdsEvent.H1 }.");
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(MdsEvent.H1))
                    {
                        _logger.LogInformation($"Checking if UserID: { userId } is authorized for H1: { MdsEvent.H1 }.");
                        //if (MdsEvent.NtwkActyTypeId == 1 || MdsEvent.NtwkActyTypeId == 3) // MDS ONLY
                        //{
                        return BadRequest(new { Message = "You are not an authorized user for this H1." });
                        //}
                    }

                    MdsEvent.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(MdsEvent.H1, string.Empty, string.Empty);
                    //MdsEvent.Event.CsgLvlId = 2; // FOR TESTING
                }

                // Validate Network H1
                if (!string.IsNullOrWhiteSpace(MdsEvent.NtwkH1))
                {
                    _logger.LogInformation($"Checking if user is authorized for H1 (Network): { MdsEvent.NtwkH1 }.");
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(MdsEvent.NtwkH1))
                    {
                        _logger.LogInformation($"Checking if UserID: { userId } is authorized for H1 (Network): { MdsEvent.NtwkH1 }.");
                        //if (MdsEvent.NtwkActyTypeId == 2 || MdsEvent.NtwkActyTypeId == 3) // NETWORK ONLY
                        //{
                        return BadRequest(new { Message = "You are not an authorized user for this H1 (Network)." });
                        //}
                    }

                    MdsEvent.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(MdsEvent.NtwkH1, string.Empty, string.Empty);
                    //MdsEvent.Event.CsgLvlId = 2; // FOR TESTING
                }

                //var enddddd = DateTime.Now;
                LkEventRule eventRule = _eventRuleRepository.GetEventRule2(MdsEvent.EventStusId, MdsEvent.WrkflwStusId, userId, (int)EventType.MDS, null, model.FailReasId, model.MdsActyTypeId);//1s
                //var endddddd = DateTime.Now;

                if (eventRule != null)
                {
                    //var enddddddd = DateTime.Now;
                    #region MAIN INFO
                    //MdsEvent.EventStusId = eventRule.EndEventStusId;
                    MdsEvent.Event.MdsEventNtwkActy = model.MdsEventNtwkActyIds
                        .Select(a =>
                        {
                            return new MdsEventNtwkActy
                            {
                                Id = 0,
                                EventId = MdsEvent.EventId,
                                NtwkActyTypeId = (byte)a,
                                CreatDt = DateTime.Now
                            };
                        })
                        .ToList();
                    #endregion
                    #region NETWORK
                    if (CheckIfNetwork(MdsEvent))
                    {
                        // NETWORK ACTIVITY TYPE
                        if (model.MplsEventActyTypeIds != null)
                        {
                            MdsEvent.Event.MplsEventActyType = model.MplsEventActyTypeIds
                                .Select(a => new MplsEventActyType
                                {
                                    EventId = id,
                                    MplsActyTypeId = (byte)a,
                                    CreatDt = DateTime.Now
                                })
                                .ToList();
                        }


                        if (MdsEvent.Event.MplsEventActyType.Any(a => a.MplsActyTypeId == 20 || a.MplsActyTypeId == 21) && model.NetworkTransport.Any(a => (a.NIDSerialNbr ?? "").Length > 0))
                        {
                            model.NetworkTransport
                                .Where(a => (a.NIDSerialNbr ?? "").Length > 0)
                                .ToList()
                                .ForEach(a =>
                                {
                                    //var activeNid = _nidActyRepo.Find(b => b.RecStusId == 251).ToList();
                                    //var fsaOrder = _fsaOrderRepo.GetFsaOrderCpeLineItem(b =>
                                    //    ((int)b.Ordr.H5H6CustId).ToString() == a.AssocH6 &&
                                    //    b.FsaOrdrCpeLineItem.Any(c =>
                                    //        c.CmpntFmly == "NID" &&
                                    //        c.SprintMntdFlg == "Y" &&
                                    //        c.ItmStus == 401 &&
                                    //        !activeNid.Any(d => d.FsaCpeLineItemId == c.FsaCpeLineItemId)
                                    //    )
                                    //).FirstOrDefault();

                                    var existing = _nidActyRepo.Find(b =>
                                        b.RecStusId == 251 &&
                                        b.NidSerialNbr == a.NIDSerialNbr &&
                                        (b.DdAprvlNbr ?? "").Length <= 0 &&
                                        EF.Functions.Like(b.FsaCpeLineItem.CmpntFmly, "NID%") &&
                                        b.FsaCpeLineItem.ItmStus == 401 &&
                                        ((int)b.FsaCpeLineItem.Ordr.Ordr.H5H6CustId).ToString() == a.AssocH6
                                    ).ToList();

                                    if (existing != null)
                                    {
                                        existing.ForEach(b =>
                                        {
                                            b.EventId = MdsEvent.EventId;
                                            b.NidHostNme = a.NidHostName;
                                            b.DdAprvlNbr = a.DdAprvlNbr;
                                            b.ModfdDt = DateTime.Now;
                                            b.ModfdByUserId = userId;
                                        });
                                    }

                                    if (MdsEvent.Event.MplsEventActyType.Any(b => b.MplsActyTypeId == 21))
                                    {
                                        var record = _nidActyRepo.Find(b =>
                                            b.EventId == MdsEvent.EventId &&
                                            b.RecStusId == 251 &&
                                            b.NidSerialNbr == a.NIDSerialNbr &&
                                            b.H6 == a.AssocH6 &&
                                            b.M5OrdrNbr == a.Mach5OrdrNbr
                                        ).FirstOrDefault();

                                        if (record == null)
                                        {
                                            NidActy nid = new NidActy();
                                            nid.NidSerialNbr = a.NIDSerialNbr;
                                            nid.NidHostNme = a.NidHostName;
                                            nid.FsaCpeLineItemId = null;
                                            nid.M5OrdrNbr = a.Mach5OrdrNbr;
                                            nid.DdAprvlNbr = a.DdAprvlNbr;
                                            nid.H6 = a.AssocH6;
                                            nid.RecStusId = 251;
                                            nid.CreatDt = DateTime.Now;
                                            nid.CreatByUserId = userId;

                                            MdsEvent.Event.NidActy.Add(nid);
                                        }
                                        else
                                        {
                                            var nid_acty = _nidActyRepo.Find(b =>
                                                b.RecStusId == 251 && b.NidActyId == record.NidActyId
                                            ).FirstOrDefault();

                                            nid_acty.NidHostNme = a.NidHostName;
                                            nid_acty.DdAprvlNbr = a.DdAprvlNbr;
                                            nid_acty.ModfdDt = DateTime.Now;
                                            nid_acty.ModfdByUserId = userId;
                                        }
                                    }
                                });

                            //model.NetworkTransport
                            //    .Where(a => (a.NIDSerialNbr ?? "").Length > 0)
                            //    .DistinctBy(a => new { a.AssocH6, a.DdAprvlNbr, a.NidHostName, a.NIDSerialNbr })
                            //    .ToList()
                            //    .ForEach(a =>
                            //    {
                            //        var isExist = _nidActyRepo.Find(b =>
                            //            b.RecStusId == 251 &&
                            //            b.NidSerialNbr == a.NIDSerialNbr &&
                            //            b.NidHostNme == a.NidHostName &&
                            //            ((b.DdAprvlNbr ?? "").Length <= 0 || b.DdAprvlNbr == a.DdAprvlNbr) &&
                            //            b.H6 == a.AssocH6
                            //        ).Count() > 0;

                            //        if (!isExist)
                            //        {
                            //            //var activeNid = _nidActyRepo.Find(b => b.RecStusId == 251).ToList();
                            //            //var fsaOrder = _fsaOrderRepo.GetFsaOrderCpeLineItem(b =>
                            //            //    ((int)b.Ordr.H5H6CustId).ToString() == a.AssocH6 &&
                            //            //    b.FsaOrdrCpeLineItem.Any(c =>
                            //            //        c.CmpntFmly == "NID" &&
                            //            //        c.SprintMntdFlg == "Y" &&
                            //            //        c.ItmStus == 401 &&
                            //            //        !activeNid.Any(d => d.FsaCpeLineItemId == c.FsaCpeLineItemId)
                            //            //    )
                            //            //).FirstOrDefault();

                            //            //if (fsaOrder != null)
                            //            //{
                            //            NidActy nid = new NidActy();
                            //            nid.NidSerialNbr = a.NIDSerialNbr;
                            //            nid.NidHostNme = a.NidHostName;
                            //            nid.FsaCpeLineItemId = null;
                            //            nid.M5OrdrNbr = null;
                            //            nid.DdAprvlNbr = a.DdAprvlNbr;
                            //            nid.H6 = a.AssocH6;
                            //            nid.RecStusId = 251;
                            //            nid.CreatDt = DateTime.Now;
                            //            nid.CreatByUserId = _loggedInUser.GetLoggedInUserId();

                            //            MdsEvent.Event.NidActy.Add(nid);
                            //            //}
                            //        }
                            //    });

                            //MdsEvent.Event.NidActy = _mapper.Map<ICollection<NidActy>>(model.NetworkTransport);
                        }
                    }
                    #endregion
                    #region MDS
                    if (CheckIfMds(MdsEvent))
                    {
                        if (CheckIfDisconnect(MdsEvent)) // DISCONNECT
                        {

                        }

                        if (!CheckIfDisconnect(MdsEvent)) // MDS/PRE-STAGING
                        {
                            MdsEvent.Event.MdsEventMacActy = model.MdsEventMacActyIds
                                .Select(a => new MdsEventMacActy
                                {
                                    EventId = MdsEvent.EventId,
                                    MdsMacActyId = (byte)a,
                                    CreatDt = DateTime.Now
                                })
                                .ToList();
                        }
                    }
                    #endregion
                    //var endddddddd = DateTime.Now;

                    // Added by Sarah Sandoval [20200812]
                    // Activator Completion is not saved in mapping
                    foreach (var item in MdsEvent.Event.EventDevCmplt)
                    {
                        item.CmpltdCd = model.DevCompletion
                            .FirstOrDefault(i => i.OdieDevNme == item.OdieDevNme
                                && i.H6 == item.H6 && i.RedsgnDevId == item.RedsgnDevId).CmpltdCd;
                    }

                    //var enddddddddd = DateTime.Now;
                    var mds = _repo.Update(id, MdsEvent, model.NetworkTransport, mdsOld);//3.8s
                    //var endddddddddd = DateTime.Now;

                    //string adid = _loggedInUser.GetLoggedInUserAdid();

                    //var mds = _repo.GetById(id, adid);
                    //var enddddddddddddd = DateTime.Now;
                    //var mds = _repo.GetById(id, null, false);
                    //var endddddddddddddd = DateTime.Now;

                    // [Updated by Joshua Bolano - 20210611] - Save Contact Details (PJ025845)
                    if (model.ContactDetails != null && model.ContactDetails.Count > 0)
                    {
                        var cntctDtl = _mapper.Map<IEnumerable<CntctDetl>>(model.ContactDetails).ToList();
                        try
                        {
                            _logger.LogInformation($"Saving IPSD Contact Detail. { JsonConvert.SerializeObject(cntctDtl, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                            List<string> currentEmails = _contctRepo.Find(i => i.CreatByUserId > 1 && i.RecStusId == 1 && i.ObjTypCd == "E" && i.ObjId == mds.EventId).Select(i => i.EmailAdr).ToList();
                            List<CntctDetl> newCntcts = cntctDtl.FindAll(i => !currentEmails.Contains(i.EmailAdr) && i.CreatByUserId > 1);
                            _contctRepo.SetInactive(i => i.ObjId == mds.EventId && i.ObjTypCd == "E" && i.RecStusId == 1);
                            cntctDtl.ForEach(item => {
                                item.ObjTypCd = "E";
                                item.ObjId = mds.EventId;
                                item.RecStusId = (short)ERecStatus.Active;
                                //item.CreatByUserId = item.CreatByUserId == 1 ? item.CreatByUserId : _loggedInUser.GetLoggedInUserId();
                                item.CreatDt = DateTime.Now;
                                item.ModfdByUserId = userId;
                                item.ModfdDt = DateTime.Now;
                            });

                            _contctRepo.Update(cntctDtl);//1.7s

                            // Added by Sarah Sandoval [20211115] as per new requirement: Add manually entered Contact List to Event History
                            if (newCntcts != null && newCntcts.Count() > 0)
                            {
                                string emails = string.Empty;
                                emails = string.Join(",", newCntcts.FindAll(i => i.CreatByUserId > 1).Select(i => i.EmailAdr).ToList());

                                if (!string.IsNullOrWhiteSpace(emails))
                                {
                                    _eventHistoryRepository.Create(new EventHist
                                    {
                                        EventId = mds.EventId,
                                        ActnId = (byte)Actions.ManualContactListData,
                                        CmntTxt = "The following email/s were added manually to Contact List table: " + emails,
                                        CreatDt = DateTime.Now,
                                        CreatByUserId = userId
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogInformation($"IPSD Contact Detail could not be updated. { JsonConvert.SerializeObject(cntctDtl, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                        }
                    }

                    // Added by Sarah Sandoval [20200812]
                    // Update Redesign Table and Send MACH5 messages
                    //var enddddddddddd = DateTime.Now;
                    _logger.LogInformation($"Saving started for Event Device Complete Table. { JsonConvert.SerializeObject(mds, json).ToString() } ");
                    _repo.SaveEventDevComplete(mds, model);//1.2s
                    _logger.LogInformation($"Saving ended for Event Device Complete Table for EventID: { id }. ");
                    //var endddddddddddd = DateTime.Now;

                    #region ASSIGNED ACTIVATORS
                    List<EventAsnToUser> assignUsers = new List<EventAsnToUser>();
                    assignUsers = _eventAsnToUserRepository.Find(a => a.EventId == mds.EventId && a.RecStusId == 1).ToList();

                    _eventAsnToUserRepository.Update(id, model.Activators
                        .Select(a => new EventAsnToUser
                        {
                            EventId = id,
                            AsnToUserId = a,
                            //CreatDt = mds.CreatDt,
                            RecStusId = 0,
                            RoleId = 0
                        }).ToList());

                    var assignUsers1 = _eventAsnToUserRepository.Find(a => a.EventId == mds.EventId && a.RecStusId == 1).ToList();
                    var newList = assignUsers.Union(assignUsers1).ToList();
                    assignUsers.Clear();
                    assignUsers.AddRange(newList);
                    _logger.LogInformation($"Assigned Activators. { JsonConvert.SerializeObject(newList, json).ToString() } ");
                    #endregion

                    //var enddddddddddddddd = DateTime.Now;
                    _logger.LogInformation($"IPSD Event Updated. { JsonConvert.SerializeObject(mds, json).ToString() } ");
                    //var endddddddddddddddd = DateTime.Now;

                    // Do things needed based on eventRule
                    // Event History, Reviewer and Activator Task
                    #region EVENT HISTORY
                    EventHist eh = new EventHist()
                    {
                        EventId = mds.EventId,
                        ActnId = eventRule.ActnId,
                        EventStrtTmst = mds.StrtTmst,
                        EventEndTmst = mds.EndTmst,
                        CreatByUserId = userId
                    };

                    if (model.ReviewerUserId > 0)
                    {
                        eh.ModfdByUserId = model.ReviewerUserId;
                        eh.CmntTxt = model.CmntTxt;
                    }
                    else if (model.ActivatorUserId > 0)
                    {
                        eh.FailReasId = model.FailReasId;
                        eh.ModfdByUserId = model.ActivatorUserId;
                        // eh.CmntTxt = model.ActivatorComments;
                        eh.PreCfgCmpltCd = (string.IsNullOrWhiteSpace(model.PreCfgConfgCode)
                            || model.PreCfgConfgCode == "0") ? "N" : "Y";
                        eh.CmntTxt = model.CmntTxt;
                        if (model.FailReasId != null)
                        {
                            eh.EventStrtTmst = model.StrtTmst;
                            eh.EventEndTmst = model.EndTmst;
                        }
                    }
                    else if ((mds.WrkflwStusId != (byte)WorkflowStatus.Visible
                                || mds.WrkflwStusId != (byte)WorkflowStatus.Retract)
                                        && mds.EventStusId != (byte)EventStatus.Visible)
                    {
                        //eh.FailReasId = model.FailReasId;
                        eh.CmntTxt = model.CmntTxt;
                    }

                    int[] toExcludeAction = null;
                    if (model.MdsActyTypeId == 3 ||
                        (model.ActivatorUserId > 0 && model.EventStusId == (int)EventStatus.Published && model.WrkflwStusId == (int)WorkflowStatus.Publish) ||
                        !(model.SprintCpeNcrId == 2 || model.SprintCpeNcrId == 9)
                        )
                    {
                        toExcludeAction = new int[] { 8 };
                    }
                    else
                    {
                        toExcludeAction = new int[] { 0 };
                    }

                    int evntHistId = _eventHistoryRepository.CreateEventHistory(eh, toExcludeAction, null, model.CpeDspchEmailAdr);
                    _logger.LogInformation($"IPSD Event History Created. { JsonConvert.SerializeObject(eh, json).ToString() } ");
                    #endregion

                    if (model.ActivatorUserId > 0)
                    {
                        #region EVENT FAIL ACTIVITY
                        var Fail = _failEventActyRepository.GetByEventId(mds.EventId);
                        if (Fail.Count() > 0)
                        {
                            _failEventActyRepository.DeleteByEventId(mds.EventId);
                        }

                        if (model.EventFailActyIds != null && model.EventFailActyIds.Count() > 0)
                        {
                            foreach (var es in model.EventFailActyIds)
                            {
                                EventFailActy efa = new EventFailActy()
                                {
                                    EventHistId = evntHistId,
                                    FailActyId = (short)es,
                                    RecStusId = (byte)ERecStatus.Active,
                                    CreatDt = DateTime.Now
                                };
                                _failEventActyRepository.CreateActy(efa);
                                _logger.LogInformation($"IPSD Event Fail Activity Created. { JsonConvert.SerializeObject(efa, json).ToString() } ");
                            }
                        }
                        #endregion
                        #region EVENT SUCCESS ACTIVITY
                        var Sucss = _sucssEventActyRepository.GetByEventId(mds.EventId);
                        if (Sucss.Count() > 0)
                        {
                            _sucssEventActyRepository.DeleteByEventId(mds.EventId);
                        }

                        if (model.EventSucssActyIds != null && model.EventSucssActyIds.Count() > 0)
                        {
                            foreach (var es in model.EventSucssActyIds)
                            {
                                EventSucssActy esa = new EventSucssActy()
                                {
                                    EventHistId = evntHistId,
                                    SucssActyId = (short)es,
                                    RecStusId = (byte)ERecStatus.Active,
                                    CreatDt = DateTime.Now
                                };
                                _sucssEventActyRepository.CreateActy(esa);
                                _logger.LogInformation($"IPSD Event Success Activity Created. { JsonConvert.SerializeObject(esa, json).ToString() } ");
                            }
                        }
                        #endregion
                    }

                    // Save Calendar Entry and Email Sender
                    EventWorkflow esw = SetEventWorkFlow(mds, assignUsers);
                    bool isMDSFT = false;
                    if (mds.FrcdftCd == true || mds.MdsFastTrkTypeId == "A")
                    {
                        isMDSFT = true;
                    }

                    bool isNtwkIntl = false;
                    isNtwkIntl = mds.Event.MdsEventNtwkActy.Any(i => i.NtwkActyTypeId == 4);
                    esw.IsNetworkIntl = isNtwkIntl;
                    esw.IsNetwork = CheckMDSNtwkActy(ref MdsEvent, "hasNtwk");
                    esw.EventRule = eventRule;
                    var eventHist = _eventHistoryRepository.GetReviewerId(mds.EventId);
                    if (eventHist != null)
                    {
                        esw.ReviewerId = eventHist.CreatByUserId;//model.ReviewerUserId;
                    }
                    else
                    {
                        esw.ReviewerId = 0;
                        // Added by Sarah Sandoval [20220222]
                        // Pass MNSPM Assignment to include in Submitted Escalation Email
                        esw.ReviewerAdId = (esw.Profile == "Member" && esw.ReviewerId <= 0) ? mds.MnsPmId : string.Empty;
                    }
                    esw.IsSupressEmail = model.IsSuppressedEmails;
                    esw.CPEDispatchEmail = model.CpeDspchEmailAdr;
                    esw.ActivatorUserId = model.ActivatorUserId;
                    esw.MdsEventDevSrvcMgmtView = (model.MnsOrder != null) ? model.MnsOrder.ToList() : null;

                    // km967761 - 10/28/2021 - add condition for SSTAT_SENDER to send CPE Dispatch Email only if CPE_VNDR is not "Goodman"
                    if (mds.Event.EventCpeDev.Where(a => a.RecStusId == 1 && a.RcptStus != "Cancelled").Any())
                    {
                        string ftn = mds.Event.EventCpeDev
                            .Where(a => a.RecStusId == 1 && a.RcptStus != "Cancelled")
                            .FirstOrDefault().CpeOrdrNbr;

                        esw.IsGoodman = _fsaOrderRepo.Find(a => a.Ftn == ftn && a.CpeVndr == "Goodman").Any();
                    }

                    //var enddddddddddddddddd = DateTime.Now;
                    if (_apptRepository.CalendarEntry(esw, isMDSFT, isNtwkIntl))
                        _logger.LogInformation($"IPSD Event Calender Entry success for EventID. { JsonConvert.SerializeObject(mds.EventId, json) } ");
                    //var endddddddddddddddddd = DateTime.Now;

                    // km967761 - 07/06/2021 - commented out for new SUBMITTED email if esw.IsEscalation = true
                    //if (!model.MdsEventNtwkActyIds.Contains(4) && model.WrkflwStusId == (byte)WorkflowStatus.Submit)
                    //{
                    //    esw.IsSupressEmail = true;
                    //}


                    string primaryContact = null;
                    if(model.NetworkCustomer != null)
                    {
                        if (model.NetworkCustomer.Count > 0)
                        {
                            var email = model.NetworkCustomer.Where(x => x.RoleNme.Contains("Primary Contact")).Select(y => y.InstlSitePocEmail).ToList();
                            primaryContact = (email.Count > 1) ? string.Join(",", email) : email.FirstOrDefault();
                        }
                    }
                   
                    if (_emailReqRepository.SendMDSMail(mds, esw, model.NetworkTransport, primaryContact))//~2s
                        _logger.LogInformation($"IPSD Event Email Entry success for EventID. { JsonConvert.SerializeObject(mds.EventId, json) } ");
                    // Removed by Sarah Sandoval [20220207] - Transferred EmailReqRepository
                    // Because it will always log as error even for Suppress Emails
                    //else
                    //    _logger.LogError($"IPSD Event Email Entry failed for EventID. { JsonConvert.SerializeObject(mds.EventId, json) } ");

                    // Updated by Sarah Sandoval [20220201] - Transfer unlocking here to prevent another API call
                    // Do the unlocking here so that after the IPSD Event it will redirect to IPSD Event View for Performance Issue
                    // except if the user has Activator Profile and Workflow Status is InProgress which will stay on Event Form
                    _logger.LogInformation($"Unlocking Event by Id: { id }.");
                    _eventLockRepo.Unlock(id, userId);
                    _logger.LogInformation($"Event by Id: { id } unlocked.");

                    //var endddddddddddddddddddd = DateTime.Now;
                    return Created($"api/MDS/{ mds.EventId}", new { EventId = mds.EventId });
                }
            }

            return BadRequest(new { Message = "IPSD Event Could Not Be Updated." });
        }

        [HttpPost("DesignDocDetail")]
        public ActionResult GetDesignDocDetail([FromBody] MdsEventViewModel model)
        {
            _logger.LogInformation($"Get Design Doc Detail.");

            var obj = _repo.GetDesignDocDetail(model.H6, model.DD, model.VASCEFlg, model.CEChngFlg);
            if (obj != null)
            {
                return Ok(obj);
            }
            else
            {
                _logger.LogInformation($"Design Doc Detail not found.");
                return NotFound(new { Message = $"Design Doc Detail not found." });
            }
        }

        [EnableCors("DefaultPolicy")]
        [HttpGet("GenerateICSFile/{id}")]
        public IActionResult GenerateICSFile([FromRoute] int id, [FromQuery] int mode, [FromQuery] int apptId, [FromQuery] string userIds)
        {
            var loggedInUser = _loggedInUser.GetLoggedInUser();

            var eventStr = _repo.GenerateICSFile(id, mode, loggedInUser.UserAdid, apptId, userIds);
            byte[] bytes = Encoding.ASCII.GetBytes(eventStr);

            try
            {
                return new FileContentResult(bytes, "application/octet")
                {
                    FileDownloadName = id == 0 ? string.Format("{0}.ics", apptId) : string.Format("{0}.ics", id)
                };
            }
            catch (Exception)
            {
                return NotFound(new { Message = $"ICS Cant be created." });
            }
        }

        private EventWorkflow SetEventWorkFlow(MdsEvent mds, List<EventAsnToUser> assignUser)
        {
            EventWorkflow esw = new EventWorkflow();
            esw.EventId = mds.EventId;
            esw.Comments = mds.CmntTxt != null ? mds.CmntTxt : string.Empty;
            esw.EventStatusId = mds.EventStusId;
            esw.WorkflowId = (int)mds.WrkflwStusId;
            esw.EventTitle = mds.EventTitleTxt;
            esw.EventTypeId = (int)EventType.MDS;
            esw.EventType = mds.MdsActyTypeId == null ? "" :
                mds.MdsActyTypeId == 1 ? "Initial Install" :
                mds.MdsActyTypeId == 2 ? "MAC" :
                mds.MdsActyTypeId == 3 ? "Disconnect" :
                mds.MdsActyTypeId == 4 ? "Install+Pre-Staging" :
                mds.MdsActyTypeId == 5 ? "MDS Pre-Staging" :
                mds.MdsActyTypeId == 6 ? "MDS Pre-Staging" :
                mds.MdsActyTypeId == 7 ? "Install WPaaS" : "";
            esw.SOWSEventID = string.Empty;
            esw.RequestorId = mds.ReqorUserId;
            esw.UserId = _loggedInUser.GetLoggedInUserId();
            esw.ConferenceBridgeNbr = mds.CnfrcBrdgNbr != null ? mds.CnfrcBrdgNbr : string.Empty;
            esw.ConferenceBridgePin = mds.CnfrcPinNbr != null ? mds.CnfrcPinNbr : string.Empty;
            esw.StartTime = mds.StrtTmst;
            esw.EndTime = Convert.ToDateTime(mds.EndTmst);
            esw.Profile = GetUserProfile(_userRepository.GetFinalUserProfile(_loggedInUser.GetLoggedInUserId(), (int)EventType.MDS));
            //esw.EnhanceServiceId = mds.EnhncSrvcId;

            // Updated by Sarah Sandoval [20220608] - INC70658536 to include on email notification only active assignment
            esw.AssignUser = assignUser.Where(i => i.RecStusId == 1).ToList();
            //esw.AssignUser = assignUser;
            esw.CmpltdEmailCcTxt = mds.CmpltdEmailCcTxt;
            esw.PubEmailCcTxt = mds.PubEmailCcTxt;
            esw.ShipCustEmailAddr = mds.ShipCustEmailAdr;
            esw.TeamPdlNme = mds.CntctEmailList;
            esw.MdsEventOdieDev = mds.Event.MdsEventOdieDev.ToList();
            esw.IsEscalation = mds.EsclCd;

            return esw;
        }

        private string GetUserProfile(LkUsrPrf profile)
        {
            string profileDesc = profile.UsrPrfDes;

            if (profileDesc.Contains("Member"))
            {
                return "Member";
            }
            else if (profileDesc.Contains("Reviewer"))
            {
                return "Reviewer";
            }
            else if (profileDesc.Contains("Activator"))
            {
                return "Activator";
            }
            else
            {
                return string.Empty;
            }
        }

        private bool CheckMDSNtwkActy(ref MdsEvent Obj, string sMDSNtwkActy)
        {
            bool hasMDS = false, hasNtwk = false, hasVAS = false;
            foreach (var item in Obj.Event.MdsEventNtwkActy)
            {
                if (!hasMDS)
                    hasMDS = (item.NtwkActyTypeId == 1);
                if (!hasNtwk)
                    hasNtwk = (item.NtwkActyTypeId == 2 || item.NtwkActyTypeId == 4);
                if (!hasVAS)
                    hasVAS = (item.NtwkActyTypeId == 3);
            }
            if (sMDSNtwkActy == "hasMDS")
                return hasMDS;
            else if (sMDSNtwkActy == "hasNtwk")
                return hasNtwk;
            else if (sMDSNtwkActy == "hasVAS")
                return hasVAS;
            else if (sMDSNtwkActy == "MDSOnly")
                return hasMDS && !hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "NtwkOnly")
                return !hasMDS && hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "VASOnly")
                return !hasMDS && !hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "MDSNtwk")
                return hasMDS && hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "MDSVAS")
                return hasMDS && !hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "NtwkVAS")
                return !hasMDS && hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "MDSNtwkVAS")
                return hasMDS && hasNtwk && hasVAS;
            else
                return false;
        }

        [HttpGet("GetVASSubTypes")]
        public IActionResult GetVASSubTypes()
        {
            _logger.LogInformation($"GetVASSubTypes");
            //return Ok(_repo.GetVASSubTypes().Result);
            return Ok(_repo.GetVASSubTypes());
        }

        [HttpGet("IsEventEmailSent")]
        public IActionResult IsEventEmailSent([FromQuery] int eventId, [FromQuery] int emailReqTypeId)
        {
            return Ok(_emailReqRepository.IsEventEmailSent(eventId, emailReqTypeId));
        }

        [HttpGet("CalDtwithBusiDays")]
        public IActionResult CalDtwithBusiDays([FromQuery] string inputDays)
        {
            return Ok(_commonRepo.CalculateDateWithSprintBusinessdays(DateTime.Now, inputDays, true));
        }


        [HttpGet("CalDtwithBusiDaysIncludingWeekend")]
        public IActionResult CalDtwithBusiDaysIncludingWeekend([FromQuery] string inputDays)
        {
            return Ok(_commonRepo.CalculateDateWithSprintBusinessdays(DateTime.Now, inputDays, true, true));
        }


        

        [HttpGet("CheckIfSprintHoliday")]
        public IActionResult CheckIfSprintHoliday([FromQuery] string dtInput)
        {
            return Ok(_commonRepo.IsSprintHoliday(dtInput));
        }

        private bool CheckIfMds(MdsEvent obj)
        {
            return obj.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 1);
        }

        private bool CheckIfNetwork(MdsEvent obj)
        {
            return obj.Event.MdsEventNtwkActy.Any(a => a.NtwkActyTypeId == 2 || a.NtwkActyTypeId == 3 || a.NtwkActyTypeId == 4);
        }

        private bool CheckIfDisconnect(MdsEvent obj)
        {
            return CheckIfMds(obj) && obj.MdsActyTypeId == 3;
        }

        private bool CheckIfNID(MdsEvent obj)
        {
            return CheckIfNetwork(obj) && obj.Event.MplsEventActyType.Any(a => a.MplsActyTypeId == 3);
        }
    }
}