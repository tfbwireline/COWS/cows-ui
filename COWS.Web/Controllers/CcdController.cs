﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Ccd")]
    [ApiController]
    public class CcdController : ControllerBase
    {
        private readonly ICcdRepository _repo;
        private readonly ILogger<CcdController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public CcdController(IMapper mapper,
                               ICcdRepository repo,
                               ILogger<CcdController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("GetCcdReason")]
        public ActionResult<IEnumerable<CcdMissedReasonViewModel>> GetCcdReason()
        {
            IEnumerable<CcdMissedReasonViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.CcdMissedReasonList, out list))
            {
                list = _mapper.Map<IEnumerable<CcdMissedReasonViewModel>>(_repo
                                                                .GetCcdReason()
                                                                .Where(i => i.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.CcdMissdReasDes))
                                                                .ToList();

                CacheManager.Set(_cache, CacheKeys.CcdMissedReasonList, list);
            }

            return Ok(list);
        }

        [HttpGet("SearchCcd")]
        public IActionResult SearchCcd([FromQuery] string m5Ctn, [FromQuery] int h5)
        {
            List<OrderViewModel> ordersList = new List<OrderViewModel>();
            m5Ctn = string.IsNullOrWhiteSpace(m5Ctn) ? string.Empty : m5Ctn;

            // Use Active for SalesSupport group and Bypass Users
            var orders = _repo.SearchCcd(m5Ctn, h5, (byte)ERecStatus.Active);
            if (orders != null && orders.Rows.Count > 0)
            {
                foreach (DataRow dr in orders.Rows)
                {
                    OrderViewModel order = new OrderViewModel();
                    order.OrderId = Convert.ToInt32(dr["OrderID"].ToString());
                    order.FTN = dr["FTN"].ToString();
                    order.DeviceID = dr["DeviceID"].ToString();
                    order.ProductType = dr["ProductType"].ToString();
                    order.OrderType = dr["OrderType"].ToString();
                    order.H5 = Convert.ToInt32(dr["H5"].ToString());
                    order.DomesticFlag = dr["DomesticFlag"].ToString();
                    order.H1 = Convert.ToInt32(dr["H1"].ToString());
                    order.OrderSubType = dr["OrderSubType"].ToString();
                    order.Ccd = Convert.ToDateTime(dr["CCD"].ToString());
                    ordersList.Add(order);
                }
            }
            else
            {
                return BadRequest(new { Message = "No eligible order sets found to perform CCD change on." });
            }

            return Ok(ordersList);
        }

        [HttpPost]
        public ActionResult Post([FromBody] CCDHistoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isChanged = _repo.UpdateCcd(model.OrderIds, model.CcdDate.Date, model.CcdReasons,
                    model.Notes, _loggedInUser.GetLoggedInUserId());

                if (isChanged)
                {
                    return Created($"api/Ccd/{ model.CcdHistId }", model);
                }
            }

            return BadRequest(new { Message = "CCD Could Not Be changed." });
        }
    }
}
