﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VasTypes")]
    [ApiController]
    public class VasTypeController : ControllerBase
    {
        private readonly IVasTypeRepository _repo;
        private readonly ILogger<VasTypeController> _logger;
        private readonly IMapper _mapper;

        public VasTypeController(IMapper mapper,
                               IVasTypeRepository repo,
                               ILogger<VasTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VasTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<VasTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.VasTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search VAS Type by Id: { id }.");

            var obj = _repo.Find(s => s.VasTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<VasTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"VAS Type by Id: { id } not found.");
                return NotFound(new { Message = $"VAS Type by Id: { id } not found." });
            }
        }
    }
}