﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDSEventPortBndwd")]
    [ApiController]
    public class MDSEventPortBndwdController : ControllerBase
    {
        private readonly IMDSEventPortBndwdRepository _repo;
        private readonly ILogger<MDSEventPortBndwdController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public MDSEventPortBndwdController(IMapper mapper,
                               IMDSEventPortBndwdRepository repo,
                               ILogger<MDSEventPortBndwdController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"MDSEventPortBndwd by EventId: { id }.");

            var obj = _repo.GetMDSEventPortBndwdByEventId(id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDSEventPortBndwdViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDSEventPortBndwd by EventId: { id } not found.");
                return NotFound(new { Message = $"MDSEventPortBndwd by EventId: { id } not found." });
            }
        }
    }
}