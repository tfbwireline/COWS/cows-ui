﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/States")]
    [ApiController]
    public class StateController : ControllerBase
    {
        private readonly IStateRepository _repo;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public StateController(IMapper mapper,
                               IStateRepository repo,
                               IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _repo = repo;
            _cache = memoryCache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<StateViewModel>> Get()
        {
            IEnumerable<StateViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.StateList, out list))
            {
                list = _mapper.Map<IEnumerable<StateViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.UsStateName));

                CacheManager.Set(_cache, CacheKeys.StateList, list);
            }

            return Ok(list);
        }
    }
}