﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/HostManagedServices")]
    [ApiController]
    public class CptHostManagedServiceController : ControllerBase
    {
        private readonly ICptHostManagedServiceRepository _repo;
        private readonly ILogger<CptHostManagedServiceController> _logger;
        private readonly IMapper _mapper;

        public CptHostManagedServiceController(IMapper mapper,
                               ICptHostManagedServiceRepository repo,
                               ILogger<CptHostManagedServiceController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptHostManagedServiceViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptHostManagedServiceViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptHstMngdSrvcId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT Host Managed Service by Id: { id }.");

            var obj = _repo.Find(s => s.CptHstMngdSrvcId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptCustomerTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT Host Managed Service by Id: { id } not found.");
                return NotFound(new { Message = $"CPT Host Managed Service Id: { id } not found." });
            }
        }
    }
}