﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/EventDevSrvcMgmt")]
    [ApiController]
    public class EventDevSrvcMgmtController : ControllerBase
    {
        private readonly IEventDevSrvcMgmtRepository _repo;
        private readonly ILogger<EventDevSrvcMgmtController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public EventDevSrvcMgmtController(IMapper mapper,
                               IEventDevSrvcMgmtRepository repo,
                               ILogger<EventDevSrvcMgmtController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("Event/{id}")]
        public ActionResult<IEnumerable<GetEventDevSrvcMgmtViewModel>> Get([FromRoute] int id)
        {
            _logger.LogInformation($"EventDevSrvcMgmt by EventId: { id }.");

            IEnumerable<GetEventDevSrvcMgmtViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.GetEventDevSrvcMgmtList, out list))
            {
                list = _mapper.Map<IEnumerable<GetEventDevSrvcMgmtViewModel>>(_repo.GetEventDevSrvcMgmtByEventId(id));

                CacheManager.Set(_cache, CacheKeys.GetEventDevSrvcMgmtList, list);
            }

            return Ok(list);
        }
    }
}