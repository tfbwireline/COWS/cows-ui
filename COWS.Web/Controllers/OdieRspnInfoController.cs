﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/OdieRspnInfo")]
    [ApiController]
    public class OdieRspnInfoController : ControllerBase
    {
        private readonly IOdieRspnInfoRepository _repo;
        private readonly ILogger<OdieRspnInfoController> _logger;
        private readonly IMapper _mapper;

        public OdieRspnInfoController(IMapper mapper,
                               IOdieRspnInfoRepository repo,
                               ILogger<OdieRspnInfoController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OdieRspnInfoViewModel>> Get()
        {
            return Ok(_mapper.Map<IEnumerable<OdieRspnInfoViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.DevId)));
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search ODIE Response Info by Id: { id }.");

            var obj = _repo.Find(s => s.RspnInfoId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<OdieRspnInfoViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"ODIE Response Info by Id: { id } not found.");
                return NotFound(new { Message = $"ODIE Response Info Id: { id } not found." });
            }
        }

        [HttpGet("GetByReqId/{reqId}")]
        public ActionResult<IEnumerable<OdieRspnInfoViewModel>> GetByReqId([FromRoute] int reqId)
        {
            _logger.LogInformation($"Search ODIE Response Info by Id: { reqId }.");

            var obj = _repo.Find(s => s.ReqId == reqId).Distinct();
            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<OdieRspnInfoViewModel>>(obj
                                                                            .Where(i => !string.IsNullOrWhiteSpace(i.DevId))
                                                                            .GroupBy(i => i.DevId)
                                                                            .Select(i => i.FirstOrDefault())));
            }
            else
            {
                _logger.LogInformation($"ODIE Response Info by Id: { reqId } not found.");
                return NotFound(new { Message = $"ODIE Response Info Id: { reqId } not found." });
            }
        }
    }
}
