﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Search")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly ISearchRepository _repo;
        private readonly IOdieReqRepository _odieReqRepo;
        private readonly IOdieRspnRepository _odieRspnRepo;
        private readonly IOdieCustomerH1Repository _odieCustH1;
        private readonly ICommonRepository _commonRepo;
        private readonly IL2PInterfaceService _l2pInterface;
        private readonly ILogger<SearchController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public SearchController(IMapper mapper,
                               ISearchRepository repo,
                               IOdieReqRepository odieReqRepo,
                               IOdieRspnRepository odieRspnRepo,
                               IOdieCustomerH1Repository odieCustH1,
                               ICommonRepository commonRepo,
                               IL2PInterfaceService l2pInterface,
                               ILogger<SearchController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _odieReqRepo = odieReqRepo;
            _odieRspnRepo = odieRspnRepo;
            _odieCustH1 = odieCustH1;
            _commonRepo = commonRepo;
            _l2pInterface = l2pInterface;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpPost("Event")]
        public ActionResult Post([FromBody] AdvancedSearchEventViewModel model)
        {
            _logger.LogInformation($"Search Event by: { JsonConvert.SerializeObject(model).ToString() }.");

            if (ModelState.IsValid)
            {
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                model.ReqByUsrId = loggedInUser.UserId;
                var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
                model.CsgLvlId = loggedInUserCSGLevel;

                // Added by Sarah Sandoval [20200603] for Quick Search
                model.EventType = model.EventType != null ? GetEventTypeId(model.EventType) : model.EventType;

                List<AdvancedSearchEvent> res = _repo.GetAdvancedSearchEventResult(model, loggedInUser.UserAdid).ToList();

                if (res != null)
                {
                    _logger.LogInformation($"Search Event Result. { JsonConvert.SerializeObject(res).ToString() } ");

                    return Ok(res.OrderBy(i => i.EventID));
                }
            }

            return BadRequest(new { Message = "Event Type Could Not Be Created." });
        }

        [HttpPost("Order")]
        public ActionResult Post([FromBody] AdvancedSearchOrderViewModel model)
        {
            _logger.LogInformation($"Search Order by: { JsonConvert.SerializeObject(model).ToString() }.");

            if (ModelState.IsValid)
            {
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                model.ReqByUsrId = loggedInUser.UserId;
                var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
                model.CsgLvlId = loggedInUserCSGLevel;
                
                List<AdvancedSearchOrder> res = _repo.GetAdvancedSearchOrderResult(model, loggedInUser.UserAdid).ToList();

                if (res != null)
                {
                    _logger.LogInformation($"Search Order Result. { JsonConvert.SerializeObject(res).ToString() } ");
                    return Ok(res);
                }
            }

            return BadRequest(new { Message = "Event Type Could Not Be Created." });
        }

        [HttpPost("Redesign")]
        public ActionResult Post([FromBody] AdvancedSearchRedesignViewModel model)
        {
            _logger.LogInformation($"Search Redesign by: { JsonConvert.SerializeObject(model).ToString() }.");

            if (ModelState.IsValid)
            {
                var adid = _loggedInUser.GetLoggedInUserAdid();
                var loggedInUserCSGLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
                model.CsgLvlId = loggedInUserCSGLevel;
                List<AdvancedSearchRedesign> res = _repo.GetAdvancedSearchRedesignResult(model, adid).ToList();

                if (res != null)
                {
                    _logger.LogInformation($"Search Redesign Result. { JsonConvert.SerializeObject(res).ToString() } ");

                    return Ok(res);
                }
            }

            return BadRequest(new { Message = "Event Type Could Not Be Created." });
        }

        [HttpGet("Lookup/EventType/{eventType}/M5/{m5}")]
        public IActionResult GetLookupM5([FromRoute] int eventType, [FromRoute] string m5)
        {
            _logger.LogInformation($"Lookup for Event Type Id: {eventType} by M5 #: { m5 }.");

            //if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(m5))
            //{
            //    return BadRequest(new { Message = "You are not an authorized user for this M5." });
            //}

            GetM5CANDEventDataResults res = _repo.M5OrH6Lookup(m5, string.Empty, eventType, 0);

            AdEvent obj = new AdEvent();
            GetM5CANDEventDataResult1 res1 = res.Result1.SingleOrDefault();

            if (res1 != null)
            {
                obj.H1 = res1.H1;
                obj.H6 = res1.H6;
                obj.CharsId = res1.CharsId;
                obj.CustNme = res1.CustomerName;
                obj.CustEmailAdr = res1.CustomerEmail;
                obj.CustCntctPhnNbr = res1.CustomerContactPhone;
                obj.CustCntctNme = res1.CustomerContactName;

                return Ok(_mapper.Map<AdEvent>(obj));
            }
            else
            {
                _logger.LogInformation($"Lookup for Event Type Id: {eventType}  by M5 #: { m5 } not found.");
                return NotFound(new { Message = $"Lookup for Event Type Id: {eventType}  by M5 #: { m5 } not found." });
            }
        }

        [HttpGet("Lookup/EventType/{h6}")]
        public IActionResult GetLookupH6([FromRoute] string h6, [FromQuery] int eventType, [FromQuery] int vasCd, [FromQuery] bool isCEChng)
        {
            _logger.LogInformation($"Lookup for Event Type Id: {eventType} by H6 #: { h6 }.");

            var lstH6 = h6.Split(new Char[] { '\n', '\r', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
            var sH6 = String.Join(",", lstH6.Select(a => a.Trim()).Where(b => b.All(char.IsDigit)).ToList());
            //byte csgLvlId = _loggedInUser.GetLoggedInUserCsgLvlId();
            List<UserCsgLevelViewModel> l = _l2pInterface.GetUserSecurityGroup();

            GetM5CANDEventDataResults res = _repo.M5OrH6Lookup(string.Empty, h6, eventType, vasCd, isCEChng);

            List<GetM5CANDEventDataResult1> res1 = res.Result1.ToList();
            List<GetM5CANDEventDataResult2> res2 = res.Result2.ToList();
            List<GetM5CANDEventDataResult3> res3 = res.Result3.ToList();
            List<GetM5CANDEventDataResult4> res4 = res.Result4.ToList();
            List<GetM5CANDEventDataResult5> res5 = res.Result5.ToList();
            List<GetM5CANDEventDataResult6> res6 = res.Result6.ToList();
            List<GetM5CANDEventDataResult7> res7 = res.Result7 != null ? res.Result7.ToList() : null;

            if (res != null)
            {
                if (eventType == (int)EventType.MDS)
                {
                    MdsH6LookupView result = new MdsH6LookupView();

                    if (res4 != null && res4.Count > 0)
                    {
                        var obj = res4
                            .Take(1)
                            .SingleOrDefault();

                        result.H1 = obj.H1;
                        result.H1CustNme = obj.H1_CUST_NME;
                        result.H6 = obj.H6;
                        result.CustomerName = obj.CustomerName;
                        result.CsgLvlId = obj.CsgLvlId;
                    }

                    if (res5 != null && res5.Count > 0)
                    {
                        result.NetworkCustomer = res5.Select(a => new MdsEventNtwkCustView
                        {
                            Guid = Guid.NewGuid().ToString(),
                            AssocH6 = a.H6 ?? "",
                            InstlSitePocNme = a.CustomerContactName ?? "",
                            RoleNme = a.RoleNme ?? "",
                            InstlSitePocEmail = a.CustomerEmail ?? "",
                            InstlSitePocPhn = a.CustomerContactPhone ?? ""
                        })
                        .ToList();
                    }

                    if (res6 != null && res6.Count > 0)
                    {
                        var obj = res6
                            .Take(1)
                            .SingleOrDefault();

                        //result.CustomerName = obj.CustNme;
                        result.DesignDocumentApprovalNumber = obj.DdNbr;
                        result.SalesEngineerEmail = obj.EmailAdr;
                        result.SalesEngineerPhone = obj.WrkPhnNbr;
                        result.GroupName = obj.GrpNme;

                        result.NetworkTransport = res6.Select(a =>
                        {
                            return new MdsEventNtwkTrptView
                            {
                                Guid = Guid.NewGuid().ToString(),
                                MdsTrnsprtType = a.ProdId ?? "",
                                LecPrvdrNme = a.AccsVndrNme,
                                Mach5OrdrNbr = a.FtnNbr,
                                IpNuaAdr = a.NuaAdr,
                                //MplsAccsBdwd = a.BdwdNme,
                                ScaNbr = a.ScaNbr,
                                VlanNbr = a.VlanId,
                                //MultiVrfReq = a.VrfNme,
                                //TaggdCd = Convert.ToBoolean(a.CustRoutrTagNme),
                                //TaggdCd = (a.CustRoutrTagNme == "Untagged") ? false : true,
                                IpVer = a.AdrFmlyTypeNme,
                                CustNme = a.CustNme,
                                LocCity = a.LocCity,
                                LocSttPrvn = a.LocStt,
                                LocCtry = a.LocCtry,
                                AssocH6 = a.H6h5Id,
                                DdAprvlNbr = a.DdNbr,
                                //SalsEngrEmail = a.EmailAdr,
                                //SalsEngrPhn = a.WrkPhnNbr,
                                BdwdNme = a.BdwdNme,
                                VASTypeDes = a.SubType,
                                VASCd = a.VasCd,
                                CeSrvcId = a.CeSrvcId,
                                NidHostName = a.NidHostNme,
                                NIDSerialNbr = a.NidSerialNbr,
                                NidIpAdr = a.NidIpAdr
                            };
                        })
                        .ToList();
                    }

                    if (res7 != null && res7.Count > 0)
                    {
                        result.RelatedCEEvents = res7.Select(a => new MDSRltdCEEventView
                        {
                            CEServiceID = a.CeSrvcId,
                            RltdCEEventID = a.EventId
                        })
                        .ToList();
                    }

                    if (result.CsgLvlId > 0)
                    {
                        if (!(l.Exists(ll => ll.CsgLvlId != 0 && ll.CsgLvlId <= result.CsgLvlId)))
                        {
                            return BadRequest(new { Message = "You are not an authorized user for this H1." });
                        }
                    }

                    return Ok(result);
                    //return Ok(res);
                }
                else if (eventType == (int)EventType.SIPT)
                {
                    SiptEvent obj = new SiptEvent();

                    if (res1 != null && res1.Count > 0)
                    {
                        obj.H1 = res1[0].H1;
                        obj.H6 = res1[0].H6;
                        obj.CharsId = res1[0].CharsId;
                        obj.SiteId = res1[0].SiteId;
                        obj.CustNme = res1[0].CustomerName;
                        obj.SiteCntctEmailAdr = res1[0].CustomerEmail;
                        obj.SiteCntctPhnNbr = res1[0].CustomerContactPhone;
                        obj.SiteCntctNme = res1[0].CustomerContactName;
                    }

                    if (res3 != null && res3.Count > 0)
                    {
                        obj.StreetAdr = res3[0].StAddr;
                        obj.FlrBldgNme = res3[0].FlrBldg;
                        obj.SttPrvnNme = res3[0].ProvState;

                        obj.CtyNme = res3[0].City;
                        obj.ZipCd = res3[0].ZipCd;
                        obj.CtryRgnNme = res3[0].Ctry;
                    }

                    return Ok(_mapper.Map<SiptEvent>(obj));
                }
                else
                {
                    AdEvent obj = new AdEvent();

                    if (res1 != null && res1.Count > 0)
                    {
                        obj.H1 = res1[0].H1;
                        obj.H6 = res1[0].H6;
                        obj.CharsId = res1[0].CharsId;
                        obj.CustNme = res1[0].CustomerName;
                        obj.CustEmailAdr = res1[0].CustomerEmail;
                        obj.CustCntctPhnNbr = res1[0].CustomerContactPhone;
                        obj.CustCntctNme = res1[0].CustomerContactName;
                    }

                    return Ok(_mapper.Map<AdEvent>(obj));
                }
            }
            else
            {
                _logger.LogInformation($"Lookup for Event Type Id: {eventType} by H6 #: { h6 } not found.");
                return NotFound(new { Message = $"Lookup for Event Type Id: {eventType} by H6 #: { h6 } not found." });
            }
        }

        [HttpGet("Lookup/H6Sipt/{h6}")]
        public IActionResult GetLookupH6Sipt([FromRoute] string h6)
        {
            _logger.LogInformation($"Lookup for Sipt by H6 #: { h6 }.");

            GetM5CANDEventDataResults res = _repo.M5OrH6Lookup(String.Empty, h6);

            SiptEvent obj = new SiptEvent();
            GetM5CANDEventDataResult1 res1 = res.Result1.SingleOrDefault();
            GetM5CANDEventDataResult3 res3 = res.Result3.SingleOrDefault();
            if (res3 != null)
            {
                obj.StreetAdr = res3.StAddr;
                obj.FlrBldgNme = res3.FlrBldg;
                obj.SttPrvnNme = res3.ProvState;

                obj.CtyNme = res3.City;
                obj.ZipCd = res3.ZipCd;
                obj.CtryRgnNme = res3.Ctry;
            }
            if (res1 != null)
            {
                obj.H1 = res1.H1;
                obj.H6 = res1.H6;
                obj.CharsId = res1.CharsId;
                obj.SiteId = res1.SiteId;
                obj.CustNme = res1.CustomerName;
                obj.SiteCntctEmailAdr = res1.CustomerEmail;
                obj.SiteCntctPhnNbr = res1.CustomerContactPhone;
                obj.SiteCntctNme = res1.CustomerContactName;

                return Ok(_mapper.Map<SiptEvent>(obj));
            }
            else
            {
                _logger.LogInformation($"Lookup for Sipt by H6 #: { h6 } not found.");
                return NotFound(new { Message = $"Lookup for Sipt by H6 #: { h6 } not found." });
            }
        }

        [HttpGet("Lookup/Site/{h6}")]
        public IActionResult GetLookupM5ByH6([FromRoute] string h6, [FromQuery] string ccd, [FromQuery] string ucaasCd, [FromQuery] bool isCarrierEthernet)
        {
            _logger.LogInformation($"Lookup Site by H6 { h6 } and CCD { ccd }.");

            // For testing only
            //h6 = "928536768";
            //ccd = "10/21/2017";

            GetM5EventDatabyH6Results res = _repo.M5LookupByH6CCD(h6, ccd, ucaasCd, isCarrierEthernet);

            if (res != null)
            {
                if (ucaasCd == "Y")
                {
                    // For UCaaS
                    UcaaSEvent obj = new UcaaSEvent();

                    var cust = res.M5CustTable.FirstOrDefault();
                    if (cust != null)
                    {
                        obj.InstlSitePocNme = cust.InstallSitePOCName;
                        obj.InstlSitePocIntlPhnCd = cust.InstallSitePOCPhnCD;
                        obj.InstlSitePocPhnNbr = cust.InstallSitePOCPhn;
                        obj.SrvcAssrnPocNme = cust.SrvcAssurncePOCName;
                        obj.SrvcAssrnPocIntlPhnCd = cust.SrvcAssurncePOCPhnCD;
                        obj.SrvcAssrnPocPhnNbr = cust.SrvcAssurncePOCPhn;
                    }

                    var siteAdr = res.M5AdrTable.FirstOrDefault();
                    if (siteAdr != null)
                    {
                        obj.SiteId = siteAdr.SiteID;
                        //obj.SiteAdr = string.Format("{0}_{1},{2}", siteAdr.SiteID, siteAdr.City, siteAdr.ProvState);
                        obj.StreetAdr = siteAdr.StAddr;
                        obj.FlrBldgNme = siteAdr.FlrBldg;
                        obj.CtyNme = siteAdr.City;
                        obj.SttPrvnNme = siteAdr.ProvState;
                        obj.CtryRgnNme = siteAdr.Ctry;
                        obj.ZipCd = siteAdr.ZipCD;
                    }

                    List<EventCpeDev> cpeDev = new List<EventCpeDev>();
                    if (res.M5CpeTable != null && res.M5CpeTable.Count() > 0)
                    {
                        cpeDev = res.M5CpeTable
                            .Select(a => new EventCpeDev
                            {
                                CpeOrdrId = a.CPEOrdrID.ToString(),
                                CpeOrdrNbr = a.M5OrdrNbr,
                                DeviceId = a.DeviceID,
                                AssocH6 = a.AssocH6,
                                ReqstnNbr = a.RqstnNbr,
                                MaintVndrPo = a.MntVndrPO,
                                OdieCd = false
                            })
                            .ToList();

                        //EventCpeDev cpe;
                        //foreach (var item in res.M5CpeTable)
                        //{
                        //    cpe = new EventCpeDev();
                        //    cpe.CpeOrdrId = item.CPEOrdrID.ToString();
                        //    cpe.CpeOrdrNbr = item.M5OrdrNbr;
                        //    cpe.DeviceId = item.DeviceID;
                        //    cpe.AssocH6 = item.AssocH6;
                        //    cpe.ReqstnNbr = item.RqstnNbr;
                        //    cpe.MaintVndrPo = item.MntVndrPO;
                        //    cpe.OdieCd = false;
                        //    cpeDev.Add(cpe);
                        //}
                    }

                    obj.Event = new Event();
                    obj.Event.EventCpeDev = cpeDev;
                    obj.CpeDspchEmailAdr = res.CPEDispatchEmail.FirstOrDefault() == null
                                    ? string.Empty : res.CPEDispatchEmail.FirstOrDefault().CPEDispatchEmail;
                    obj.CustNme = res.M5H1BasedOnH6.FirstOrDefault() == null
                                    ? string.Empty : res.M5H1BasedOnH6.FirstOrDefault().CustName;

                    return Ok(_mapper.Map<UcaaSEvent>(obj));
                }
                else
                {
                    // For MDS
                    MdsEvent obj = new MdsEvent();
                    obj.Event = new Event();

                    MdsSiteLookupView asd = new MdsSiteLookupView();

                    // CPE Equipment Device Information Summary
                    if (res.M5CpeTable != null)
                    {
                        asd.CpeDevices = res.M5CpeTable
                            .Select(a => new MdsEventCpeDevView
                            {
                                CpeOrdrId = a.CPEOrdrID.ToString(),
                                CpeOrdrNbr = a.M5OrdrNbr,
                                DeviceId = a.DeviceID,
                                AssocH6 = a.AssocH6,
                                ReqstnNbr = a.RqstnNbr,
                                OdieDevNme = string.Empty,
                                Ccd = Convert.ToDateTime(a.CCD),
                                OrdrStus = a.CmpntStus,
                                RcptStus = a.RcptStus,
                                DlvyClli = a.DLVY_CLLI,
                                CpeCmpntFmly = a.CPE_CMPN_FMLY,
                                OrdrId = a.OrdrID
                            })
                            .ToList();
                    }
                     
                    // Install Site & Service Assurance Info
                    var cust = res.M5CustTable.FirstOrDefault();
                    if (cust != null)
                    {
                        asd.InstlSitePocNme = cust.InstallSitePOCName;
                        asd.InstlSitePocIntlPhnCd = cust.InstallSitePOCPhnCD;
                        asd.InstlSitePocPhnNbr = cust.InstallSitePOCPhn;
                        asd.SrvcAssrnPocNme = cust.SrvcAssurncePOCName;
                        asd.SrvcAssrnPocIntlPhnCd = cust.SrvcAssurncePOCPhnCD;
                        asd.SrvcAssrnPocPhnNbr = cust.SrvcAssurncePOCPhn;
                        asd.SrvcAvlbltyHrs = cust.SrvcAssuranceAvlbltyHrs;
                        asd.SrvcTmeZnCd = cust.SrvcAssuranceTimeZone;
                    }

                    // Address Info
                    var siteAdr = res.M5AdrTable.FirstOrDefault();
                    if (siteAdr != null)
                    {
                        asd.SiteIdTxt = siteAdr.SiteID;
                        asd.StreetAdr = siteAdr.StAddr;
                        asd.FlrBldgNme = siteAdr.FlrBldg;
                        asd.CtyNme = siteAdr.City;
                        asd.SttPrvnNme = siteAdr.ProvState;
                        asd.CtryRgnNme = siteAdr.Ctry;
                        asd.ZipCd = siteAdr.ZipCD;
                    }

                    // Device Based MNS Management Service Order Table
                    if (res.M5MnsTable != null)
                    {
                        asd.MnsOrders = res.M5MnsTable
                            .Select(a => new MdsEventDevSrvcMgmtView
                            {
                                MnsSrvcTierId = (byte)a.MDS_SRVC_TIER_ID,
                                MnsSrvcTier = a.MNS_SRVC_TIER,
                                DeviceId = a.DEVICE_ID,
                                M5OrdrCmpntId = a.ORDR_CMPNT_ID,
                                MnsOrdrNbr = a.M5_ORDR_NBR,
                                CmpntStus = a.CMPNT_STUS,
                                CmpntTypeCd = a.CMPNT_TYPE_CD,
                                CmpntNme = a.NME,
                                RltdCmpntID = a.RLTD_CMPNT_ID,
                                AccsCarrier = a.ACCS_CXR_NME,
                                AssocTrptType = a.ASSOC_TRNSPRT_TYPE,
                                MNSOrdrID = Convert.ToInt32(a.M5_ORDR_ID)
                            })
                            .ToList();
                    }

                    if (res.M5RelMnsTable != null)
                    {
                        asd.RelatedMnsOrders = res.M5RelMnsTable
                            .Select(a => new MdsEventDevSrvcMgmtView
                            {
                                MnsSrvcTierId = (byte)a.MDS_SRVC_TIER_ID,
                                MnsSrvcTier = a.MNS_SRVC_TIER,
                                DeviceId = a.DEVICE_ID,
                                M5OrdrCmpntId = a.ORDR_CMPNT_ID,
                                MnsOrdrNbr = a.M5_ORDR_NBR,
                                CmpntStus = a.CMPNT_STUS,
                                CmpntTypeCd = a.CMPNT_TYPE_CD,
                                CmpntNme = a.NME,
                                RltdCmpntID = a.RLTD_CMPNT_ID,
                                MNSOrdrID = Convert.ToInt32(a.M5_ORDR_ID)
                            })
                            .ToList();
                    }

                    // DSL/SBIC/Third Party/Customer Provider Transport
                    if (res.M5DslsbicTable != null)
                    {

                        asd.ThirdParty = res.M5DslsbicTable
                            .Select(a => new MDSEventDslSbicCustTrptView
                            {
                                MdsTrnsprtType = a.TRNSPRT_TYPE,
                                VndrPrvdrTrptCktId = a.TRNSPRT_ACCT_NBR
                            })
                            .ToList();
                    }

                    if (res.M5CpeAsasTable != null)
                    {
                        asd.MnsAsasTypes = res.M5CpeAsasTable
                            .Select(a => new MdsEventDevSrvcMgmtView
                            {
                                MnsSrvcTierId = (byte)a.MDS_SRVC_TIER_ID,
                                MnsSrvcTier = a.MNS_SRVC_TIER,
                                DeviceId = a.DEVICE_ID,
                                M5OrdrCmpntId = a.ORDR_CMPNT_ID,
                                RltdCmpntID = a.RLTD_CMPNT_ID,
                                AccsCarrier = a.ACCS_CXR_NME,
                                AssocTrptType = a.ASSOC_TRNSPRT_TYPE,
                                MNSOrdrID = Convert.ToInt32(a.M5_ORDR_ID),
                                MnsOrdrNbr = a.M5_ORDR_NBR,
                                CmpntStus = a.CMPNT_STUS,
                                CmpntTypeCd = a.CMPNT_TYPE_CD,
                                CmpntNme = a.NME
                            })
                            .ToList();
                    }

                    // Port Bandwidth Upgrade
                    if (res.M5PortTable != null)
                    {
                        asd.PortBandwidth = res.M5PortTable
                            .Select(a => new MDSEventPortBndwdView
                            {
                                M5OrdrNbr = a.M5_ORDR_NBR,
                                M5OrdrCmpntId = a.M5_ORDR_CMPNT_ID,
                                Nua = a.NUA,
                                PortBndwd = a.PORT_BNDWD
                            })
                            .ToList();
                    }
                  

                    asd.CpeDispatchEmail = res.CPEDispatchEmail.FirstOrDefault() == null
                                    ? string.Empty : res.CPEDispatchEmail.FirstOrDefault().CPEDispatchEmail;

                    // Wired Device Transport (SPA)
                    if (res.NrmWiredTable != null)
                    {
                        asd.WiredDeviceTransports = res.NrmWiredTable
                            .Select(a => new MDSEventSlnkWiredTrptView
                            {
                                MdsTrnsprtType = a.PROD_ID,
                                H6 = a.H6_ID,
                                VndrPrvdrNme = a.VNDR_NME,
                                VndrPrvdrTrptCktId = a.VEN_CIRCUIT_ID,
                                BdwdChnlNme = a.BDWD_NME,
                                PlNbr = a.PLN,
                                IpNuaAdr = a.IP_NUA,
                                MultiLinkCktCd = String.IsNullOrWhiteSpace(a.MULTILINK_CIRCUIT) ?
                                    "0" : a.MULTILINK_CIRCUIT.Trim().ToUpper() == "YES" ?
                                        "Y" : "N",
                                DedAccsCd = String.IsNullOrWhiteSpace(a.DEDCTD_ACCS_CD) ?
                                    "0" : a.DEDCTD_ACCS_CD.Trim().ToUpper() == "YES" ?
                                        "Y" : "N",
                                FmsNbr = a.FMS_CKT_ID,
                                VlanNbr = a.VLAN_ID,
                                SpaNua = a.SPA_NUA,
                                SprintMngdCd = String.IsNullOrWhiteSpace(a.SPRINT_MNGE_NME) ?
                                    "0" : a.SPRINT_MNGE_NME.Trim().ToUpper() == "YES" ?
                                        "S" : "C",
                                DlciVpiVci = a.DLCI_VPI,
                                ReadyBeginCd = a.READY_BEGIN_FLG_NME,
                                OptInCktCd = a.OPT_IN_CKT_CD,
                                OptInHCd = a.OPT_IN_H_CD
                            })
                            .ToList();
                    }
                   

                    return Ok(asd);
                }
            }
            else
            {
                _logger.LogInformation($"Lookup Site by H6 { h6 } and CCD { ccd } not found.");
                return NotFound(new { Message = $"Lookup Site by H6 { h6 } and CCD { ccd } not found." });
            }
        }

        [HttpGet("Lookup/H1/{h1}")]
        public IActionResult GetLookupH1([FromRoute] string h1)
        {
            _logger.LogInformation($"Lookup by H1: { h1 }.");

            if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(h1))
            {
                return BadRequest(new { Message = "You are not an authorized user for this H1." });
            }

            byte csgLvlId = _l2pInterface.GetH1CsgLevelId(h1);

            OdieReq req = _odieReqRepo.Request(0, (int)OdieMessageType.CustomerNameRequest, h1, "", false, "", csgLvlId);

            // Added to prevent returning null wen not in debug mode
            //System.Threading.Thread.Sleep(4000);

            IEnumerable<OdieRspn> res = _odieRspnRepo.Response(req.ReqId, csgLvlId);

            if (res != null && res.Count() > 0)
            {
                return Ok(_mapper.Map<IEnumerable<OdieRspnViewModel>>(res));
            }
            else
            {
                if (res == null)
                {
                    _logger.LogInformation($"Did not receive an ODIE response.");
                    return NotFound(new { Message = $"Did not receive an ODIE response." });
                }
                else if (res.Count() == 0)
                {
                    _logger.LogInformation($"Lookup by H1: { h1 } not found.");
                    return NotFound(new { Message = $"Lookup by H1: { h1 } not found." });
                }
                else
                {
                    _logger.LogInformation($"An error occurred while getting response from ODIE, Please try again.");
                    return BadRequest(new { Message = "An error occurred while getting response from ODIE, Please try again." });
                }
            }
        }

        [HttpGet("Lookup/CPEOrder/{h6}")]
        public IActionResult GetM5CPEOrderDetails([FromRoute] string h6, [FromQuery] DateTime ccd,
            [FromQuery] string ucaasCd, [FromQuery] int eventId)
        {
            _logger.LogInformation($"Lookup View CPE Order Details by H6: { h6 }, CCD: { ccd }, EventID: { eventId }.");

            // For testing only
            //h6 = "928536768";
            //ccd = "10/21/2017";
            //eventId = 192781;

            string date = ccd.ToString("MM/dd/yyyy");
            GetM5CPEOrderDataResults res = _repo.M5CPEOrderData(h6, date, eventId, ucaasCd);

            if (res != null)
            {
                return Ok(res);
            }
            else
            {
                _logger.LogInformation($"Lookup View CPE Order Details by H6: { h6 }, CCD: { ccd }, EventID: { eventId } not found.");
                return NotFound(new { Message = $"Lookup View CPE Order Details by H6: { h6 }, CCD: { ccd }, EventID: { eventId } not found." });
            }
        }

        [HttpGet("Redesign/OdieDeviceInfo/{h1}")]
        public IActionResult GetRedesignOdieDeviceInfo([FromRoute] string h1, [FromQuery] string custName, [FromQuery] int eventTypeId)
        {
            _logger.LogInformation($"Get Redesign Odie Device Info by H1: { h1 }, Customer Name: { custName }, and Event Type ID: { eventTypeId }.");

            // For testing only
            //h1 = "921427171";
            //custName = "OILGEAR COMPANY (THE)";
            //eventTypeId = (int)EventType.UCaaS;

            var res = _repo.RetrieveRedesign(h1, custName, eventTypeId, _loggedInUser.GetLoggedInUserCsgLvlId());

            if (res != null)
            {
                return Ok(_mapper.Map<IEnumerable<RedesignDevicesInfoViewModel>>(res));
            }
            else
            {
                _logger.LogInformation($"Redesign Odie Device Info by H1: { h1 }, Customer Name: { custName }, and Event Type ID: { eventTypeId } not found.");
                return NotFound(new { Message = $"Redesign Odie Device Info by H1: { h1 }, Customer Name: { custName }, and Event Type ID: { eventTypeId } not found." });
            }
        }

        [HttpPost("Redesign/OdieDiscoInfo")]
        public IActionResult GetRedesignOdieDiscoInfo([FromBody] GetOdieDataViewModel body)
        {
            _logger.LogInformation($"GetRedesignOdieDiscoInfo by H1: { body.H1 }.");

            IEnumerable<EventDiscoDev> odieDiscoDevs = _mapper.Map<IEnumerable<EventDiscoDev>>(body.EventDiscoDev);

            int reqId = _odieReqRepo.CreateODIEDiscoRequest(body.H1, odieDiscoDevs.ToList());

            if (reqId < 0)
            {
                return BadRequest(new { Message = "An error occurred while inserting request to ODIE, Please try again." });
            }

            IEnumerable<GetOdieDiscoResponse> res = _odieRspnRepo.GetOdieDiscoResponse(reqId);

            if (res != null && res.Count() > 0)
            {
                List<EventDiscoDev> discoDevs = new List<EventDiscoDev>();

                discoDevs = res
                    .Select(a => new EventDiscoDev
                    {
                        H5H6 = a.H5H6CustId.ToString(),
                        DevModelId = (short)a.DevModelId, //Model
                        ManfId = (short)a.ManfId, //Vendor
                        OdieDevNme = a.OdieDevName,
                        SerialNbr = a.SerialNumber,
                        DeviceId = a.M5DeviceId,
                        SiteId = a.SiteId,
                        ThrdPartyCtrct = a.ContractNumber,
                        OptInHCd = a.OptInHCd,
                        OptInCktCd = a.OptInCktCd,
                        ReadyBeginCd = a.ReadyBeginFlagName
                    })
                    .ToList();

                return Ok(_mapper.Map<IEnumerable<EventDiscoDevViewModel>>(discoDevs));
            }
            else
            {
                _logger.LogInformation($"Did not receive a ODIE response for the H1 request: { body.H1 }.");
                return NotFound(new { Message = $"Did not receive a ODIE response for the H1 request: { body.H1 }." });
            }
        }

        [HttpGet("Lookup/OdieCustomerH1Data")]
        public ActionResult<IEnumerable<OdieCustomerH1ViewModel>> GetODIECustomerH1Data()
        {
            IEnumerable<OdieCustomerH1ViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.OdieCustomerH1List, out list))
            {
                list = _mapper.Map<IEnumerable<OdieCustomerH1ViewModel>>(_odieCustH1
                                                                .GetOdieCustomerH1Data()
                                                                .OrderBy(s => s.Customer));

                CacheManager.Set(_cache, CacheKeys.OdieCustomerH1List, list);
            }

            return Ok(list);
        }

        [HttpGet("Lookup/SysCfg/{name}")]
        public IActionResult GetSysCfgValue([FromRoute] string name)
        {
            _logger.LogInformation($"Lookup Sys Cfg Param Value by Param Name: { name }.");

            string val = _commonRepo.GetSysCfgValue(name);

            return Ok(val);
        }

        [HttpGet("Lookup/Role/{roleCds}")]
        public ActionResult<IEnumerable<RoleViewModel>> GetAllRoleByRoleCodes([FromRoute] string roleCds)
        {
            var contctDetailRoles = roleCds.Split(",");
            IEnumerable<RoleViewModel> list = _mapper.Map<IEnumerable<RoleViewModel>>(_commonRepo
                                                                .FindRole(i => contctDetailRoles.Contains(i.RoleCd))
                                                                .OrderBy(s => s.RoleNme));

            return Ok(list);
        }

        private string GetEventTypeId(string eventType)
        {
            if (eventType.ToUpper().Contains("ACCESS") || eventType.ToUpper().Contains("AD"))
            {
                return "1";
            }
            else if (eventType.ToUpper().Contains("NGVN"))
            {
                return "2";
            }
            else if (eventType.ToUpper().Contains("MPLS"))
            {
                return "3";
            }
            else if (eventType.ToUpper().Contains("SPRINT"))
            {
                return "4";
            }
            else if (eventType.ToUpper().Contains("FAST"))
            {
                return "6";
            }
            else if (eventType.ToUpper().Contains("MDS"))
            {
                return "5";
            }
            else if (eventType.ToUpper().Contains("FED"))
            {
                return "9";
            }
            else if (eventType.ToUpper().Contains("SIPT"))
            {
                return "10";
            }
            else if (eventType.ToUpper().Contains("UCAAS"))
            {
                return "19";
            }

            return eventType;
        }
    }
}