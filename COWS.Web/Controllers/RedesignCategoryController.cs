﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/RedesignCategories")]
    [ApiController]
    public class RedesignCategoryController : ControllerBase
    {
        private readonly IRedesignCategoryRepository _repo;
        private readonly ILogger<RedesignCategoryController> _logger;
        private readonly IMapper _mapper;

        public RedesignCategoryController(IMapper mapper,
                               IRedesignCategoryRepository repo,
                               ILogger<RedesignCategoryController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<RedesignCategoryViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<RedesignCategoryViewModel>>(_repo
                                                                .Find(s => s.RecStusId == true)
                                                                .OrderBy(s => s.RedsgnCatId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Redesign Category by Id: { id }.");

            var obj = _repo.Find(s => s.RedsgnCatId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<RedesignCategoryViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Redesign Category by Id: { id } not found.");
                return NotFound(new { Message = $"Redesign Category Id: { id } not found." });
            }
        }
    }
}