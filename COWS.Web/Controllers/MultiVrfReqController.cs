﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MultiVrfReqs")]
    [ApiController]
    public class MultiVrfReqController : ControllerBase
    {
        private readonly IMultiVrfReqRepository _repo;
        private readonly ILogger<MultiVrfReqController> _logger;
        private readonly IMapper _mapper;

        public MultiVrfReqController(IMapper mapper,
                               IMultiVrfReqRepository repo,
                               ILogger<MultiVrfReqController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MultiVrfReqViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MultiVrfReqViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.MultiVrfReqDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] string id)
        {
            _logger.LogInformation($"Search Multi VRF Req by Id: { id }.");

            var obj = _repo.Find(s => s.MultiVrfReqId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MultiVrfReqViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Multi VRF Req by Id: { id } not found.");
                return NotFound(new { Message = $"Multi VRF Req by Id: { id } not found." });
            }
        }
    }
}