﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/OrderAction")]
    [ApiController]
    public class OrderActionController : ControllerBase
    {
        private readonly IOrderActionRepository _repo;
        private readonly IMapper _mapper;

        public OrderActionController(IMapper mapper,
                               IOrderActionRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OrderActionViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<OrderActionViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrActnDes));
            return Ok(list);
        }
    }
}