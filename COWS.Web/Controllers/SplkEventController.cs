﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/SplkEvent")]
    [ApiController]
    public class SplkEventController : ControllerBase
    {
        private readonly ISPLKEventRepository _repo;
        private readonly ILogger<SplkEventController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IL2PInterfaceService _l2pInterface;
        private readonly IUserRepository _userRepository;
        private readonly IMapUserProfileRepository _mapUserProfileRepository;
        private readonly IEventRuleRepository _eventRuleRepository;
        private readonly IEventHistoryRepository _eventHistoryRepository;
        private readonly IEventFailActyRepository _repoFailEventActy;
        private readonly IEventSucssActyRepository _repoSucssEventActy;
        private readonly IEventAsnToUserRepository _eventAsnToUserRepository;
        private readonly IApptRepository _ApptRepository;
        private readonly IEmailReqRepository _emailReqRepository;
        private IMemoryCache _cache;

        public SplkEventController(IMapper mapper,
                               ISPLKEventRepository repo,
                               ILogger<SplkEventController> logger,
                               ILoggedInUserService loggedInUser,
                               IL2PInterfaceService l2pInterface,
                               IUserRepository userRepository,
                               IMapUserProfileRepository mapUserProfileRepository,
                               IEventRuleRepository eventRuleRepository,
                               IEventHistoryRepository eventHistoryRepository,
                               IEventFailActyRepository repoFailEventActy,
                               IEventSucssActyRepository repoSucssEventActy,
                               IEventAsnToUserRepository eventAsnToUserRepository,
                               IApptRepository ApptRepository,
                               IEmailReqRepository emailReqRepository,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _l2pInterface = l2pInterface;
            _userRepository = userRepository;
            _mapUserProfileRepository = mapUserProfileRepository;
            _eventRuleRepository = eventRuleRepository;
            _eventHistoryRepository = eventHistoryRepository;
            _repoFailEventActy = repoFailEventActy;
            _repoSucssEventActy = repoSucssEventActy;
            _eventAsnToUserRepository = eventAsnToUserRepository;
            _ApptRepository = ApptRepository;
            _emailReqRepository = emailReqRepository;
        }

        /// <summary>
        /// https://localhost:44314/api/SplkEvent/GetSplkEventById/value
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetSplkEventById/{id}")]
        public ActionResult<SplkEventViewModel> GetSplkEventById([FromRoute] int id)
        {
            SplkEventViewModel list;
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            //if (!_cache.TryGetValue(CacheKeys.SipteventList, out list))
            //{
            list = _mapper.Map<SplkEventViewModel>(_repo.GetSplkEventById(id, loggedInUser.UserAdid));
            if (list != null)
            {
                return Ok(list);
            }
            else
            {
                _logger.LogInformation($"Splk Event by Id: { id } not found.");
                return NotFound(new { Message = $"Splk Event Id: { id } not found." });
            }

            //    CacheManager.Set(_cache, CacheKeys.SipteventList, list);
            //}

            //return Ok(list);
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] SplkEventViewModel model)
        {
            //SplkEventViewModel  Was created because  Activators is not part of SPLKevent Table and originally i have handled it in the repository Much before Joshua did the
            //Create EventAsnToUser. And Inititially it was thought to be a bad idea to handle different repository from 1 Controller
            if (ModelState.IsValid)
            {
                SplkEvent updSplk = _mapper.Map<SplkEvent>(model);
                SplkEvent Splk;
                SplkEvent OldSplk;
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                updSplk.ModfdByUserId = loggedInUser.UserId;
                updSplk.EventId = id;
                OldSplk = _repo.GetSplkEventById(id, loggedInUser.UserAdid);
                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(updSplk.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                updSplk.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(updSplk.H1);

                // Validate M5
                if (!string.IsNullOrWhiteSpace(updSplk.Ftn))
                {
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(updSplk.Ftn))
                    {
                        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                    }

                    updSplk.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(string.Empty, string.Empty, updSplk.Ftn);
                }

                LkEventRule eventRule = GetEventRule(updSplk.EventStusId, (byte)updSplk.WrkflwStusId);
                //32 Activator
                //34 Reviewer
                //33 Member
                int profileId = _userRepository.GetFinalUserProfile(_loggedInUser.GetLoggedInUserId(), (int)EventType.SprintLink).UsrPrfId;
                int eventHistID = 0;
                if (eventRule != null)
                {
                    updSplk.EventStusId = eventRule.EndEventStusId;
                    var Upd = _repo.UpdateSplkEvent(id, updSplk);
                    Splk = _repo.GetSplkEventById(id, loggedInUser.UserAdid);

                    _logger.LogInformation($"Splk Event Updated. { JsonConvert.SerializeObject(Splk, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    List<EventAsnToUser> assignUsers = model.Activators
                        .Select(a => new EventAsnToUser
                        {
                            EventId = id,
                            AsnToUserId = a,
                            CreatDt = Splk.CreatDt,
                            RecStusId = 0,
                            RoleId = 0
                        }).ToList();
                    _eventAsnToUserRepository.Update(id, assignUsers);

                    _logger.LogInformation($"Activator Updated. { JsonConvert.SerializeObject(model.Activators, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    EventWorkflow esw = new EventWorkflow();
                    esw.AssignUser = assignUsers;
                    esw.EventId = id;
                    esw.UserId = loggedInUser.UserId;
                    esw.EventRule = eventRule;
                    esw.OldStartTime = OldSplk.StrtTmst;
                    esw.OldEndTime = OldSplk.EndTmst;
                    esw.StartTime = Splk.StrtTmst;
                    esw.EndTime = Splk.EndTmst;
                    esw.EventStatusId = Splk.EventStusId;
                    esw.RequestorId = (int)Splk.ReqorUserId;
                    esw.UserId = Splk.ModfdByUserId == null ? loggedInUser.UserId : (int)Splk.ModfdByUserId;
                    esw.PubEmailCcTxt = Splk.PubEmailCcTxt;
                    esw.CmpltdEmailCcTxt = Splk.CmpltdEmailCcTxt;
                    esw.WorkflowId = (int)Splk.WrkflwStusId;
                    esw.EventTitle = Splk.EventTitleTxt;
                    esw.Comments = Splk.DsgnCmntTxt;
                    esw.ConferenceBridgeNbr = Splk.CnfrcBrdgNbr;
                    esw.ConferenceBridgePin = Splk.CnfrcPinNbr;
                    esw.EventTypeId = (int)EventType.SprintLink;
                    esw.NewEvent = false;   //EDIT
                    esw.EventType = "SprintLink";

                    if (_ApptRepository.CalendarEntry(esw))
                        _logger.LogInformation($"Calender Entry success for EventID. { JsonConvert.SerializeObject(id, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    if (_emailReqRepository.SendMail(esw))
                        _logger.LogInformation($"Email Entry success for EventID. { JsonConvert.SerializeObject(id, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    if (model.Profile == "Reviewer")
                    //if(profileId==34)
                    {
                        // Do things needed based on eventRule
                        CreateEventHistory(id, eventRule.ActnId, updSplk.StrtTmst, updSplk.EndTmst, "", 1, model.ReviewCmntTxt);
                    }
                    if (model.Profile == "Activator")
                    //if (profileId == 32)
                    {
                        model.PreCfgConfgCode = (string.IsNullOrWhiteSpace(model.PreCfgConfgCode)
                            || model.PreCfgConfgCode == "0") ? "N" : "Y";
                        eventHistID = CreateEventHistory(id, eventRule.ActnId, updSplk.StrtTmst, updSplk.EndTmst, model.PreCfgConfgCode, model.FailCode, model.ReviewCmntTxt);
                        var Fail = this._repoFailEventActy.GetByEventId(Splk.EventId);
                        if (Fail.Count() > 0)
                        {
                            this._repoFailEventActy.DeleteByEventId(Splk.EventId);
                        }
                        if (model.EventFailActyIds != null)
                        {
                            foreach (var es in model.EventFailActyIds)
                            {
                                EventFailActy esa = new EventFailActy();
                                esa.EventHistId = eventHistID;
                                esa.FailActyId = (short)es;
                                esa.RecStusId = (byte)1;
                                esa.CreatDt = System.DateTime.Now;
                                this._repoFailEventActy.CreateActy(esa);
                            }
                        }

                        var Sucss = this._repoSucssEventActy.GetByEventId(Splk.EventId);
                        if (Sucss.Count() > 0)
                        {
                            this._repoSucssEventActy.DeleteByEventId(Splk.EventId);
                        }
                        if (model.EventSucssActyIds != null)
                        {
                            foreach (var es in model.EventSucssActyIds)
                            {
                                EventSucssActy esa = new EventSucssActy();
                                esa.EventHistId = eventHistID;
                                esa.SucssActyId = (short)es;
                                esa.RecStusId = (byte)1;
                                esa.CreatDt = System.DateTime.Now;
                                this._repoSucssEventActy.CreateActy(esa);
                            }
                        }
                    }
                    //else  //33
                    if (model.Profile == "Member")
                    {
                        CreateEventHistory(id, eventRule.ActnId, updSplk.StrtTmst, updSplk.EndTmst, "", 1, "");
                    }
                    return Created($"api/SplkEvent/{ Splk.EventId}", Splk);
                }
                else
                {
                    return BadRequest(new { Message = "Not authorize to perform this operation Check your Profile" });
                }
            }

            return BadRequest(new { Message = "Splk Event Could Not Be Updated." });
        }

        /// <summary>
        /// https://localhost:44314/api/SplkEvent/value
        /// this.api.post('api/Events/SiptEvent/', model);
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Post([FromBody] SplkEventViewModel model)
        {
            //SplkEventViewModel  Was created because  Activators is not part of SPLKevent Table and originally i have handled it in the repository Much before Joshua did the
            //Create EventAsnToUser. And Inititially it was thought to be a bad idea to handle different repository from 1 Controller
            if (ModelState.IsValid)
            {
                SplkEvent SplkEvent = _mapper.Map<SplkEvent>(model);

                //SiptEvent.Event = new Event();
                //SiptEvent..EventTypeId = (int)EventType.SIPT;
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                SplkEvent.CreatDt = DateTime.Now;
                SplkEvent.CreatByUserId = loggedInUser.UserId;
                SplkEvent.ModfdByUserId = loggedInUser.UserId;
                //SplkEvent.WrkflwStusId = (byte)WorkflowStatus.Submit;  //// During Create its just Submitted Status

                // Validate H1
                if (!_l2pInterface.IsUserAuthorizedToWorkOnH1(SplkEvent.H1))
                {
                    return BadRequest(new { Message = "You are not an authorized user for this H1." });
                }

                // Get CsgLvlId by H1
                SplkEvent.Event.CsgLvlId = _l2pInterface.GetH1CsgLevelId(SplkEvent.H1);

                // Validate M5
                if (!string.IsNullOrWhiteSpace(SplkEvent.Ftn))
                {
                    if (!_l2pInterface.IsUserAuthorizedToWorkOnM5(SplkEvent.Ftn))
                    {
                        return BadRequest(new { Message = "You are not authorized user for this FTN." });
                    }

                    SplkEvent.Event.CsgLvlId = _l2pInterface.GetM5CsgLevelId(string.Empty, string.Empty, SplkEvent.Ftn);
                }
                LkEventRule eventRule = GetEventRule(SplkEvent.EventStusId, (byte)SplkEvent.WrkflwStusId);
                //32 Activator
                //34 Reviewer
                //33 Member
                int profileId = _userRepository.GetFinalUserProfile(_loggedInUser.GetLoggedInUserId(), (int)EventType.SprintLink).UsrPrfId;
                if (eventRule != null)
                {
                    SplkEvent.EventStusId = eventRule.EndEventStusId;
                    // Create Event and AdEvent
                    var Splk = _repo.Create(SplkEvent, loggedInUser.UserAdid);

                    List<EventAsnToUser> assignUsers = model.Activators
                        .Select(a => new EventAsnToUser
                        {
                            EventId = Splk.EventId,
                            AsnToUserId = a,
                            CreatDt = Splk.CreatDt,
                            RecStusId = 0,
                            RoleId = 0
                        }).ToList();
                    _eventAsnToUserRepository.Create(assignUsers);
                    _logger.LogInformation($"Activator Updated. { JsonConvert.SerializeObject(model.Activators, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    EventWorkflow esw = new EventWorkflow();
                    esw.AssignUser = assignUsers;
                    esw.EventId = Splk.EventId;
                    esw.UserId = loggedInUser.UserId;
                    esw.EventRule = eventRule;
                    esw.OldStartTime = Splk.StrtTmst;
                    esw.OldEndTime = Splk.EndTmst;
                    esw.StartTime = Splk.StrtTmst;
                    esw.EndTime = Splk.EndTmst;
                    esw.EventStatusId = Splk.EventStusId;
                    esw.RequestorId = (int)Splk.ReqorUserId;
                    esw.UserId = Splk.ModfdByUserId == null ? loggedInUser.UserId : (int)Splk.ModfdByUserId; ;
                    esw.PubEmailCcTxt = Splk.PubEmailCcTxt;
                    esw.CmpltdEmailCcTxt = Splk.CmpltdEmailCcTxt;
                    esw.WorkflowId = (int)Splk.WrkflwStusId;
                    esw.EventTitle = Splk.EventTitleTxt;
                    esw.Comments = Splk.DsgnCmntTxt;
                    esw.ConferenceBridgeNbr = Splk.CnfrcBrdgNbr;
                    esw.ConferenceBridgePin = Splk.CnfrcPinNbr;
                    esw.EventTypeId = (int)EventType.SprintLink;
                    esw.NewEvent = true;   //New
                    esw.EventType = "SprintLink";

                    if (_ApptRepository.CalendarEntry(esw))
                        _logger.LogInformation($"Calender Entry success for EventID. { JsonConvert.SerializeObject(Splk.EventId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    if (_emailReqRepository.SendMail(esw))
                        _logger.LogInformation($"Email Entry success for EventID. { JsonConvert.SerializeObject(Splk.EventId, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                    if (Splk != null)
                    {
                        _logger.LogInformation($"Splk Event Created. { JsonConvert.SerializeObject(Splk, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");

                        // Do things needed based on eventRule
                        if (model.Profile == "Reviewer" || model.Profile == "Activator")
                        //if (profileId == 34 || profileId == 32)
                        {
                            // Do things needed based on eventRule
                            CreateEventHistory(Splk.EventId, eventRule.ActnId, SplkEvent.StrtTmst, SplkEvent.EndTmst, "", 1, model.ReviewCmntTxt);
                        }
                        if (model.Profile == "Member")
                        //if (profileId == 33)
                        {
                            CreateEventHistory(Splk.EventId, eventRule.ActnId, SplkEvent.StrtTmst, SplkEvent.EndTmst, "", 1, "");
                        }
                        return Created($"api/SplkEvent/{Splk.EventId}", Splk);
                    }
                }
                else
                {
                    return BadRequest(new { Message = "Not authorize to perform this operation Check your Profile" });
                }
            }

            return BadRequest(new { Message = "Splk Event Could Not Be Created." });
        }

        /// <summary>
        /// https://localhost:44314/api/SplkEvent/getSplkActyType
        /// </summary>
        /// <returns></returns>
        [HttpGet("getSplkActyType")]
        public ActionResult<IEnumerable<SplkActyTypeViewModel>> getSplkActyType()
        {
            IEnumerable<SplkActyTypeViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.SplkActyTypeList, out list))
            {
                list = _mapper.Map<IEnumerable<SplkActyTypeViewModel>>(_repo.GetAllSPLKActyType());

                CacheManager.Set(_cache, CacheKeys.SplkActyTypeList, list);
            }

            return Ok(list);
        }

        /// <summary>
        /// https://localhost:44314/api/SplkEvent/getSplkEventType
        /// </summary>
        /// <returns></returns>
        [HttpGet("getSplkEventType")]
        public ActionResult<IEnumerable<SplkEventTypeViewModel>> getSplkEventType()
        {
            IEnumerable<SplkEventTypeViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.SplkEventTypeList, out list))
            {
                list = _mapper.Map<IEnumerable<SplkEventTypeViewModel>>(_repo.GetAllSPLKEventType());

                CacheManager.Set(_cache, CacheKeys.SplkEventTypeList, list);
            }

            return Ok(list);
        }

        /// <summary>
        /// /https://localhost:44314/api/SplkEvent/getSysCfgValue/UCaaSTeamPDL
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet("getSysCfgValue/{param}")]
        public string getSysCfgValue([FromRoute] string param)
        {
            string list;
            if (!_cache.TryGetValue(CacheKeys.SiptTeamPDL, out list))
            {
                list = _repo.GetSysCfgValue(param);

                CacheManager.Set(_cache, CacheKeys.SiptTeamPDL, list);
            }

            return list;
        }

        private LkEventRule GetEventRule(byte eventStatusId, byte workflowStatusId)
        {
            //32 Activator
            //34 Reviewer
            //33 Member
            int profileId = _userRepository.GetFinalUserProfile(_loggedInUser.GetLoggedInUserId(), (int)EventType.SprintLink).UsrPrfId;

            return _eventRuleRepository.GetEventRule(eventStatusId, workflowStatusId, _loggedInUser.GetLoggedInUserId(), (int)EventType.SprintLink);
            //return _eventRuleRepository
            //    .Find(a => a.UsrPrfId == profileId &&
            //        a.EventTypeId == (int)EventType.SprintLink &&
            //        a.StrtEventStusId == eventStatusId &&
            //        a.WrkflwStusId == workflowStatusId)
            //    .SingleOrDefault();
        }

        private int CreateEventHistory(int eventId, byte actnId, DateTime? start, DateTime? end, string PrCfgConfgCode, int failCode, string reviewCmntTxt)
        {
            if (PrCfgConfgCode == null) PrCfgConfgCode = "N";
            if (PrCfgConfgCode == "Yes") PrCfgConfgCode = "Y";
            if (PrCfgConfgCode == "No") PrCfgConfgCode = "N";
            if (PrCfgConfgCode == "") PrCfgConfgCode = "N";
            //int EventHistID=0;
            EventHist entity = new EventHist
            {
                EventId = eventId,
                ActnId = actnId,
                EventStrtTmst = start,
                EventEndTmst = end,
                CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                CmntTxt = reviewCmntTxt,
                PreCfgCmpltCd = PrCfgConfgCode,
                //FailReasId = (short)failCode ,
                CreatDt = DateTime.Now,
                ModfdDt = DateTime.Now
            };
            var rep = _eventHistoryRepository.CreateEventHistory(entity);
            return rep;
        }

        #region
        //private bool CalendarEntry(List<EventAsnToUser> asgnUsr, SplkEvent Newmodel, SplkEvent Oldmodel, LkEventRule eventRule, int id)
        //{
        //    bool bActChange = false;
        //    bool bTimeChange = false;
        //    bool bCalendarUpdate = false;
        //    bool bUpdFlg = false;
        //    StringBuilder sbActUserName = new StringBuilder();
        //    var loggedInUser = _loggedInUser.GetLoggedInUser();
        //    if ((asgnUsr != null) && (asgnUsr.Count > 0))
        //    {
        //        foreach (var au in asgnUsr)
        //        {
        //            sbActUserName.Append((_userRepository.GetById(au.AsnToUserId).FullNme.Equals(string.Empty) ? string.Empty : (_userRepository.GetById(au.AsnToUserId).FullNme + "; ")));
        //        }
        //        if (sbActUserName.Length > 0)
        //            sbActUserName.Remove(sbActUserName.Length - 2, 2);
        //    }
        //    string sCalDesc = ((asgnUsr != null) && (asgnUsr.Count > 0)) ? ((Newmodel.DsgnCmntTxt.Trim().Length > 0) ? Newmodel.DsgnCmntTxt.Trim() + " ; Assigned Activators : " + sbActUserName.ToString() : "Assigned Activators : " + sbActUserName.ToString()) : Newmodel.DsgnCmntTxt.Trim();
        //    string sCalSubj = Newmodel.EventTitleTxt + " Event ID : " + Newmodel.EventId.ToString();
        //    if (Oldmodel != null)  /// New Event
        //    {
        //        bActChange = _eventAsnToUserRepository.AccountChange(asgnUsr, id);

        //        if (Oldmodel.StrtTmst != Newmodel.StrtTmst || Oldmodel.EndTmst != Newmodel.EndTmst)
        //        {
        //            bTimeChange = true;
        //        }
        //    }

        //    if ((Newmodel.EventStusId == (byte)EventStatus.Published && eventRule.EndEventStusId == (byte)WorkflowStatus.Publish && (bActChange || bTimeChange))
        //        || (Newmodel.EventStusId != (byte)EventStatus.Published) || (eventRule.EndEventStusId == (byte)WorkflowStatus.Publish))
        //    {
        //        /*
        //        * [CAL_CD] column is replacement for CalendarAdd and CalendarDelOld elements. If both add/delold elements are 0
        //        * then column value will be NULL;likewise value 0 for add=0/del=1;value 1 for add=1/del=0;value 2 for add=1/del=1
        //        * dbo.[LK_EVENT_RULE]
        //        */
        //        if ((eventRule.CalCd != null)) ///CalendarAdd=1 or CalendarDelOld=1
        //        {
        //            //if (asgnUsr == null || asgnUsr.Count <= 0 || (eventRule.CalCd == 0 || eventRule.CalCd == 2)) ///CalendarDelOld=1
        //            if (asgnUsr == null || asgnUsr.Count <= 0 || (eventRule.CalCd == 0 )) ///CalendarDelOld=1
        //            {
        //                lock (this)
        //                {
        //                    Appt cal = _ApptRepository
        //                    .Find(a => a.EventId == id)
        //                    .SingleOrDefault();

        //                    bUpdFlg = false;

        //                    if (cal != null)
        //                    {
        //                        cal.RecStusId = ((int)ERecStatus.DeleteHold);
        //                        cal.ModfdByUserId = loggedInUser.UserId;
        //                        cal.ModfdDt = DateTime.Now;

        //                        _ApptRepository.Update(id, cal);
        //                    }

        //                }
        //            }
        //            if ((eventRule.CalCd == 1 || eventRule.CalCd == 2))  //CalendarAdd=1
        //            {
        //                if (Newmodel.StrtTmst != null && Newmodel.EndTmst != null)
        //                {
        //                    if (asgnUsr != null || asgnUsr.Count > 0)
        //                    {
        //                        lock (this)
        //                        {
        //                            XElement xe = new XElement("ResourceIds",
        //                                                        from c in asgnUsr
        //                                                        select new XElement("ResourceId",
        //                                                            new XAttribute("Type", "System.Int32"),
        //                                                            new XAttribute("Value", c.AsnToUserId)));
        //                            Appt cal = new Appt();

        //                            var x = _ApptRepository
        //                            .Find(a => a.EventId == id)
        //                            .SingleOrDefault();

        //                            bUpdFlg = true;

        //                            if (x == null)
        //                            {
        //                                cal.EventId = id;
        //                                cal.EndTmst = Newmodel.EndTmst;
        //                                cal.StrtTmst = Newmodel.StrtTmst;
        //                                //cal.RecStusId = ((int)ERecStatus.Active);
        //                                cal.ModfdByUserId = loggedInUser.UserId;
        //                                cal.ModfdDt = DateTime.Now;
        //                                cal.CreatByUserId = Newmodel.ReqorUserId == null ? 0 : (int)Newmodel.ReqorUserId;
        //                                cal.CreatDt = DateTime.Now;
        //                                cal.AsnToUserIdListTxt = xe.ToString();
        //                                cal.ApptTypeId = (int)EventType.SprintLink;
        //                                cal.Des = sCalDesc.Trim();
        //                                cal.SubjTxt = sCalSubj.Trim();
        //                                cal.RcurncCd = false;
        //                                cal.RcurncDesTxt = string.Empty;
        //                                cal.ApptLocTxt = "Bridge Number;Pin : " + Newmodel.CnfrcBrdgNbr + ";" + Newmodel.CnfrcPinNbr;
        //                                cal.RecStusId = Byte.Parse(((int)ERecStatus.Active).ToString());

        //                                _ApptRepository.Create(cal);
        //                            }
        //                            else
        //                            {
        //                                cal.EventId = id;
        //                                //cal.EndTmst = Newmodel.EndTmst;
        //                                cal.ModfdByUserId = loggedInUser.UserId;
        //                                cal.ModfdDt = DateTime.Now;
        //                                cal.AsnToUserIdListTxt = xe.ToString();
        //                                cal.ApptTypeId = (int)EventType.SprintLink;
        //                                cal.StrtTmst = Newmodel.StrtTmst;
        //                                cal.EndTmst = Newmodel.EndTmst;
        //                                cal.Des = sCalDesc.Trim();
        //                                cal.SubjTxt = sCalSubj.Trim();
        //                                cal.RcurncCd = false;
        //                                cal.RcurncDesTxt = string.Empty;
        //                                cal.ApptLocTxt = "Bridge Number;Pin : " + Newmodel.CnfrcBrdgNbr + ";" + Newmodel.CnfrcPinNbr;
        //                                if (eventRule.WrkflwStusId == (byte)WorkflowStatus.Complete)
        //                                    cal.RecStusId = Byte.Parse(((int)ERecStatus.InActive).ToString());
        //                                else
        //                                    cal.RecStusId = Byte.Parse(((int)ERecStatus.Active).ToString());

        //                                _ApptRepository.Update(id, cal);
        //                            }

        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return bCalendarUpdate;
        //}

        #endregion
    }
}