﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/SetPreference")]
    [ApiController]
    public class SetPreferenceController : ControllerBase
    {
        private readonly ISetPreferenceRepository _repo;
        private readonly ILogger<SetPreferenceController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public SetPreferenceController(IMapper mapper,
                               ISetPreferenceRepository repo,
                               ILogger<SetPreferenceController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<SetPreferenceViewModel>> Get()
        {
            var userID = _loggedInUser.GetLoggedInUserId();
            var list = _mapper.Map<IEnumerable<SetPreferenceViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active
                                                                    && s.CreatByUserId == userID)
                                                                .OrderBy(s => s.CnfrcBrdgNbr));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Set Preference by Cnfrc Brdg Id: { id }.");

            var obj = _repo.Find(s => s.CnfrcBrdgId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<SetPreferenceViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Cnfrc Brdg: { id } not found.");
                return NotFound(new { Message = $"Cnfrc Brdg: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] SetPreferenceViewModel model)
        {
            _logger.LogInformation($"Set Preference by User Id: { _loggedInUser.GetLoggedInUserId() }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkCnfrcBrdg>(model);
                obj.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                obj.CreatDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;

                var conf = _repo.Create(obj);

                if (conf != null)
                {
                    _logger.LogInformation($"Set Preference Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/SetPreference/{ conf.CnfrcBrdgId }", model);
                }
            }

            return BadRequest(new { Message = "Set Preference Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] SetPreferenceViewModel model)
        {
            _logger.LogInformation($"Update Set Preference by Cnfrc Brdg Id: { id }.");

            var obj = _mapper.Map<LkCnfrcBrdg>(model);
            obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
            obj.ModfdDt = DateTime.Now;
            obj.RecStusId = (byte)ERecStatus.Active;

            _repo.Update(id, obj);

            _logger.LogInformation($"Set Preference Updated. { JsonConvert.SerializeObject(model).ToString() } ");
            return Created($"api/SetPreference/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deleting Set Preference by Conference Bridge Id: { id }.");

            var conf = _repo.Find(s => s.CnfrcBrdgId == id);
            if (conf != null)
            {
                _repo.Delete(id);

                _logger.LogInformation($"Set Preference by Conference Bridge Id: { id } Deleted.");
            }
            else
            {
                _logger.LogInformation($"Deleting record failed due to Set Preference by Conference Bridge Id: { id } not found.");
            }
        }
    }
}
