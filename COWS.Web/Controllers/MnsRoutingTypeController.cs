﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MnsRoutingTypes")]
    [ApiController]
    public class MnsRoutingTypeController : ControllerBase
    {
        private readonly IMnsRoutingTypeRepository _repo;
        private readonly ILogger<MnsRoutingTypeController> _logger;
        private readonly IMapper _mapper;

        public MnsRoutingTypeController(IMapper mapper,
                               IMnsRoutingTypeRepository repo,
                               ILogger<MnsRoutingTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MnsRoutingTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MnsRoutingTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.MnsRoutgTypeId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] string id)
        {
            _logger.LogInformation($"Search MNS Routing Type by Id: { id }.");

            var obj = _repo.Find(s => s.MnsRoutgTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MnsRoutingTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MNS Routing Type by Id: { id } not found.");
                return NotFound(new { Message = $"MNS Routing Type Id: { id } not found." });
            }
        }
    }
}