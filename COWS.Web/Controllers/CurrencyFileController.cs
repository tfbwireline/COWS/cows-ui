﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CurrencyFiles")]
    [ApiController]
    public class CurrencyFileController : ControllerBase
    {
        private readonly ICurrencyFileRepository _repo;
        private readonly ILogger<CurrencyFileController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public CurrencyFileController(IMapper mapper,
                               ICurrencyFileRepository repo,
                               ILogger<CurrencyFileController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CurrencyFileViewModel>> Get()
        {
            IEnumerable<CurrencyFileViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.CurrencyFileList, out list))
            {
                list = _mapper.Map<IEnumerable<CurrencyFileViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderByDescending(s => s.SrcCurFileId));

                CacheManager.Set(_cache, CacheKeys.CurrencyFileList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Currency File by Id: { id }.");

            var obj = _repo.Find(s => s.SrcCurFileId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CurrencyFileViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Currency File by Id: { id } not found.");
                return NotFound(new { Message = $"Currency File Id: { id } not found." });
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] CurrencyFileViewModel model)
        {
            model.FileCntntBtsm = Convert.FromBase64String(model.Base64string);

            _logger.LogInformation($"Create Currency File: { model.FileNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<SrcCurFile>(model);

                MemoryStream stream = new MemoryStream(obj.FileCntntBtsm);

                try
                {
                    obj.PrsdCurListTxt = ReadExcelFile(stream);
                }
                catch (Exception)
                {
                    return BadRequest(new { Message = "Currency File Could Not Be Uploaded." });
                }

                // Remove this comment once logged in user service is available
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                var rep = _repo.Create(obj);
                _repo.UpdateCurrencyConverter(loggedInUser.UserId);
                if (rep != null)
                {
                    // Update Cache
                    _cache.Remove(CacheKeys.CurrencyFileList);

                    _logger.LogInformation($"Currency File Uploaded. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/CurrencyFiles/{ rep.FileNme }", model);
                }
            }

            return BadRequest(new { Message = "Currency File Could Not Be Uploaded." });
        }

        [HttpGet("{id}/download")]
        public IActionResult Download([FromRoute] int id)
        {
            _logger.LogInformation($"Search Currency File by Id: { id }.");

            var obj = _repo.Find(s => s.SrcCurFileId == id).SingleOrDefault();
            if (obj != null)
            {
                try
                {
                    return new FileContentResult(obj.FileCntntBtsm, "application/octet")
                    {
                        FileDownloadName = obj.FileNme
                    };
                }
                catch (Exception)
                {
                    return NotFound(new { Message = $"Currency File Id: { id } not found." });
                }
            }
            else
            {
                _logger.LogInformation($"Currency File by Id: { id } not found.");
                return NotFound(new { Message = $"Currency File Id: { id } not found." });
            }
        }

        private string ReadExcelFile(Stream stream)
        {
            StringBuilder sb = new StringBuilder();
            //using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileName, false))
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(stream, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                string sheetName = "AVG to USD";
                string relId = workbookPart.Workbook.Descendants<Sheet>()
                                 .Where(s => sheetName.Equals(s.Name))
                                 .First()
                                 .Id;

                //WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                WorksheetPart worksheetPart = (WorksheetPart)workbookPart.GetPartById(relId);
                int i = 3;
                string curr;
                string conv;
                while (i < 500)
                {
                    curr = GetCellValue(workbookPart, worksheetPart, "A" + i);
                    conv = GetCellValue(workbookPart, worksheetPart, "C" + i);

                    if (curr != null && conv != null)
                    {
                        sb.Append(curr + '|' + conv + '|');
                    }
                    i++;
                }
                return sb.ToString();
                //Console.WriteLine(sb.ToString());
                //Console.ReadLine();

                //OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
                //string text;
                //while (reader.Read())
                //{
                //    if (reader.ElementType == typeof(CellValue))
                //    {
                //        text = reader.GetText();
                //        Console.Write(text + " ");
                //    }
                //}
                //Console.WriteLine();
                //Console.ReadKey();
            }
        }

        private static string GetCellValue(WorkbookPart _workbookPart, WorksheetPart _worksheetPart, string addressName)
        {
            string value = null;
            Cell theCell = _worksheetPart.Worksheet.Descendants<Cell>().
                  Where(c => c.CellReference == addressName).FirstOrDefault();

            // If the cell does not exist, return an empty string:
            if (theCell != null && theCell.InnerText != null & theCell.InnerText != string.Empty)
            {
                value = theCell.InnerText;
                if (value != null)
                {
                    CellValue cv = theCell.CellValue;
                    value = cv.InnerText;
                }

                // If the cell represents a numeric value, you are done.
                // For dates, this code returns the serialized value that
                // represents the date. The code handles strings and Booleans
                // individually. For shared strings, the code looks up the
                // corresponding value in the shared string table. For Booleans,
                // the code converts the value into the words TRUE or FALSE.
                if (theCell.DataType != null)
                {
                    switch (theCell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            // For shared strings, look up the value in the shared
                            // strings table.
                            var stringTable = _workbookPart.
                              GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                            // If the shared string table is missing, something is
                            // wrong. Return the index that you found in the cell.
                            // Otherwise, look up the correct text in the table.
                            if (stringTable != null)
                            {
                                value = stringTable.SharedStringTable.
                                  ElementAt(int.Parse(value)).InnerText;
                            }
                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;

                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                    }
                }
            }
            return value;
        }
    }
}