﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/UserProfile")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly IUserRepository _userRepo;
        private readonly IUserProfileRepository _userProfileRepo;
        private readonly IMapUserProfileRepository _mapUserProfileRepo;
        private readonly ILogger<UserProfileController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        public IConfiguration _configuration { get; }

        public UserProfileController(IMapper mapper,
                               IUserRepository userRepo,
                               IUserProfileRepository userProfileRepo,
                               IMapUserProfileRepository mapUserProfileRepo,
                               ILogger<UserProfileController> logger,
                               ILoggedInUserService loggedInUser,
                               IConfiguration configuration)
        {
            _mapper = mapper;
            _userRepo = userRepo;
            _userProfileRepo = userProfileRepo;
            _mapUserProfileRepo = mapUserProfileRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _configuration = configuration;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserProfileViewModel>> Get()
        {
            var userProfile = _mapper.Map<IEnumerable<UserProfileViewModel>>(_userProfileRepo
                                                                .GetAll().Where(a => a.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.UsrPrfDes));
            return Ok(userProfile);
        }

        [HttpGet("getUserProfileByUserId")]
        public IActionResult GetUserProfileByUserId([FromQuery] int userId, [FromQuery] int loggedInUserId)
        {
            // km967761 - 01/23/2020
            // Added where clause to filter inactive profiles
            var result = _userProfileRepo.GetUserProfilesByUserId(userId, loggedInUserId);
            //result = result.Where(a => a.RecStusId == 1);

            if ((result.Count() > 0))
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(new { Message = "No data found for this User" });
            }
        }

        [HttpPost("updateUserProfile")]
        public IActionResult UpdateUserProfile([FromBody] IEnumerable<MapUserProfileViewModel> model)
        {

            var userId = 0;
            var selectedUserProfiles = model.Where(a => a.RecStusId == 1 || a.RecStusId == 2);
            if (selectedUserProfiles.Count() > 0)
            {
                 userId = selectedUserProfiles.FirstOrDefault().UserId;
                _logger.LogInformation($"Deleting Map User Profile by UserId: { userId }.");

                var userProfile = _mapUserProfileRepo.Find(s => s.UserId == userId);
                if (userProfile != null)
                {
                    foreach (var profile in userProfile)
                    {
                        _logger.LogInformation($"Map User Profile by UserId: { userId } Deleting.");
                        var obj = _mapper.Map<MapUsrPrf>(profile);
                        _mapUserProfileRepo.Delete(obj.Id);
                        // Get();
                        _logger.LogInformation($"Map User Profile by UserId: { userId } Deleted.");
                    }
                }
                else
                {
                    return BadRequest(new { Message = $"Deleting record failed due to Map User Profile by UserId: { userId } not found." });
                }
            }
            else
            {
                return BadRequest(new { Message = "You must have at least one role selected to go with the group assignment." });
            }
            foreach (var profile in selectedUserProfiles)
            {
                _logger.LogInformation($"Map User Profile by UserId: { userId } Creating.");
                var obj = _mapper.Map<MapUsrPrf>(profile);
                // Remove this comment once logged in user service is available
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }
                if (obj.RecStusId == 2)
                {
                    obj.RecStusId = 1;
                }
                if (obj.PoolCd == 2)
                {
                    obj.PoolCd = 1;
                }
                var rep = _mapUserProfileRepo.Create(obj);
                _logger.LogInformation($"Map User Profile by UserId: { userId } Created");
                //if (rep != null)
                //{
                //    Created($"api/UserProfile/{ rep.Id }", profile);
                //}
            }
            _mapUserProfileRepo.SaveAll();
            //return Created($"api/UserProfile/getUserProfileByUserId", userId);
            return Ok(userId);
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] UserViewModel model)
        {
            var user = _mapper.Map<LkUser>(model);
            _userRepo.Update(id, user);

            _logger.LogInformation($"User Updated. { JsonConvert.SerializeObject(user).ToString() } ");
            return Created($"api/Users/{ user.UserId }", model);
        }

        [HttpGet("isAdminUserProfile/{id}")]
        public ActionResult<bool> IsAdminUserProfile(int id)
        {
            var result = _userProfileRepo.Find(a => a.UsrPrfNme.Contains("Admin")
                                                && a.MapUsrPrf.Where(b => b.UserId == id
                                                    && b.RecStusId == 1).Count() > 0).ToList();

            if (result != null && result.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpGet("isEventUserProfile/{userId}/EventType/{eventTypeId}")]
        public ActionResult<bool> IsEventUserProfile([FromRoute] int userId, [FromRoute] int eventTypeId)
        {
            var userProfiles = _userProfileRepo.Find(a => (a.UsrPrfNme.Contains("EActivator")
                                                    || a.UsrPrfNme.Contains("EReviewer")
                                                    || a.UsrPrfNme.Contains("EMember"))
                                                && a.MapUsrPrf.Where(b => b.UserId == userId
                                                    && b.RecStusId == 1).Count() > 0).ToList();

            if (userProfiles != null && userProfiles.Count() > 0)
            {
                if (eventTypeId == (int)EventType.AD || eventTypeId == (int)EventType.MPLS
                    || eventTypeId == (int)EventType.NGVN || eventTypeId == (int)EventType.SprintLink)
                {
                    var cand = userProfiles.FindAll(i => EF.Functions.Like(i.UsrPrfNme, "%CAND%") || EF.Functions.Like(i.UsrPrfNme, "%SALESSUPPORT%")).ToList();

                    return (cand != null && cand.Count() > 0) ? true : false;
                }
                else if (eventTypeId == (int)EventType.SIPT || eventTypeId == (int)EventType.UCaaS)
                {
                    var siptUcaas = userProfiles.FindAll(i => EF.Functions.Like(i.UsrPrfNme, "%SIPTnUCaaS%") || EF.Functions.Like(i.UsrPrfNme, "%SALESSUPPORT%")).ToList();

                    return (siptUcaas != null && siptUcaas.Count() > 0) ? true : false;
                }
                else if (eventTypeId == (int)EventType.MDS)
                {
                    var mds = userProfiles.FindAll(i => EF.Functions.Like(i.UsrPrfNme, "%MDS%") || EF.Functions.Like(i.UsrPrfNme, "%SALESSUPPORT%")).ToList();

                    return (mds != null && mds.Count() > 0) ? true : false;
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        [HttpGet("getMapUserProfilesByUserId")]
        public IActionResult GetMapUserProfilesByUserId([FromQuery] int userId)
        {
            var result = _userProfileRepo.Find(a => a.MapUsrPrf.Where(b => b.UserId == userId
                                                    && b.RecStusId == 1).Count() > 0).ToList();

            return Ok(result);
        }
    }
}