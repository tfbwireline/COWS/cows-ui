﻿using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/SlotPicker")]
    [ApiController]
    public class SlotPickerController : ControllerBase
    {
        private readonly ISlotPickerRepository _slotPickerRepo;
        private readonly IMDSNetworkActivityTypeRepository _mdsNetworkActivityTypeRepo; 
        private ICommonRepository _commonRepo;
        private readonly ISystemConfigRepository _systemConfigRepo;
        private readonly ILogger<SlotPickerController> _logger;

        private string _profile = "Event Activator";
        private Hashtable _htSlotData = new Hashtable();
        private string _sIsMDSFT = string.Empty;
        private DataTable _dtUserProfiles = new DataTable();
        private DataTable _dtProductUser = new DataTable("product");
        private DataTable _dtSkilledMember = new DataTable("skilled");
        private DataTable _dtFreeCollection = new DataTable();
        private DataTable _dtFreeUserCollection = new DataTable();
        private DataTable _dtLaunchPickerCollection = new DataTable("schedule");
        private DataTable _dtOpenSlotsCollection = new DataTable("OpenSlots");
        private DataTable _dtRandomBusyCollection = new DataTable();
        private DataTable _dtTempRandomBusyCollection = new DataTable();
        private DataTable _dtScheduleMember = new DataTable();
        private DataTable _dtWFCollection = new DataTable();
        private DataTable _dtBusyCollection = new DataTable();
        private int _ProductID;
        private string _sAction;
        private int _iSlotLimit = 5;
        private Single _duration = 0.00F;
        private Single _increment = 5.00F; // Changeed interval from 30 to 5
        private string sError;
        private DateTime _dtSt;
        private DateTime _dtOrigSt;
        private DateTime _dtEnd;
        private static short _eOffSet = 3;

        public Hashtable htSlotData
        {
            get { return _htSlotData; }
            set { _htSlotData = value; }
        }

        public DateTime dtSt
        {
            get { return _dtSt; }
            set { _dtSt = value; }
        }

        public DateTime dtEnd
        {
            get { return _dtEnd; }
            set { _dtEnd = value; }
        }

        private List<LkMdsNtwkActyType> _MDSNtwkActy;

        public List<LkMdsNtwkActyType> lMDSNtwkActy
        {
            get { return _MDSNtwkActy; }
            set { _MDSNtwkActy = value; }
        }

        public class SlotData
        {
            public SlotData(string uid, DateTime sdt)
            {
                UserID = uid;
                StartDt = sdt;
            }

            #region "Private Members"

            private string _UserID;
            private DateTime _StartDt;

            #endregion "Private Members"

            #region "Public Properties"

            public string UserID
            {
                get { return _UserID; }
                set { _UserID = value; }
            }

            public DateTime StartDt
            {
                get { return _StartDt; }
                set { _StartDt = value; }
            }

            #endregion "Public Properties"
        }

        public SlotPickerController(ICommonRepository commonRepo,
                               ISlotPickerRepository slotPickerRepo,
                               ISystemConfigRepository systemConfigRepo,
                               ILogger<SlotPickerController> logger,
                               IMDSNetworkActivityTypeRepository mdsNetworkActivityTypeRepo)
        {
            _commonRepo = commonRepo;
            _slotPickerRepo = slotPickerRepo;
            _systemConfigRepo = systemConfigRepo;
            _logger = logger;
            _mdsNetworkActivityTypeRepo = mdsNetworkActivityTypeRepo;
        }

        protected string GetSysCfgVal(string sParam)
        {
            var list = _commonRepo.GetAllSysCfg();

            var sVal = list.FirstOrDefault(a => a.PrmtrNme.Equals(sParam));
            return (sVal == null) ? string.Empty : sVal.PrmtrValuTxt;
        }

        [HttpPost("CheckCurrentSlots")]
        public ActionResult<DataTable> CheckCurrentSlots([FromBody] DynamicParametersViewModel model)
        {
            _logger.LogInformation($"Check Current Slots: { JsonConvert.SerializeObject(model).ToString() }.");

            GetSlotsForCurrentSlot(model);

            DataTable dtSlots = new DataTable();
            dtSlots.Columns.Add(new DataColumn("SLOT_ID"));
            dtSlots.Columns.Add(new DataColumn("SLOT_DESC"));
            dtSlots.Columns.Add(new DataColumn("SLOT_USER"));
            string sUserID = string.Empty;
            DateTime dtStart = _dtSt;

            if (_dtFreeCollection.Rows.Count > 0)
            {
                string sFinalUserID = GetUserIDByRandomBusyCollection(-1);

                DataRow[] drFreeArr = new DataRow[_dtFreeCollection.Rows.Count];

                if (sFinalUserID != string.Empty)
                {
                    _dtFreeCollection.Select("USER_ID = '" + sFinalUserID + "'").CopyTo(drFreeArr, 0);

                    int j = 0;

                    //DataRow[] drTempFreeArr = new DataRow[_dtFreeCollection.Rows.Count];

                    while ((drFreeArr.Count(a => (a != null)) < 5) && (j < _dtRandomBusyCollection.Rows.Count))
                    {
                        string s = GetUserIDByRandomBusyCollection(j);

                        //sFinalUserID = (s != sFinalUserID) ? s : sFinalUserID;
                        /*if (drFreeArr.Length > 0)
                            _dtFreeCollection.Select("USER_ID = '" + sFinalUserID + "'").CopyTo(drFreeArr, drFreeArr.Length - 1);
                        else*/

                        //drTempFreeArr = _dtFreeCollection.Select("USER_ID = '" + sFinalUserID + "'");
                        if ((s != sFinalUserID) && (s != string.Empty))
                        {
                            sFinalUserID = s;
                            _dtFreeCollection.Select("USER_ID = '" + sFinalUserID + "'").CopyTo(drFreeArr, drFreeArr.Count(a => (a != null)));
                        }
                        j++;
                    }
                }
                else
                    _dtFreeCollection.Rows.CopyTo(drFreeArr, 0);

                int i = 0;
                foreach (DataRow drf in drFreeArr.Where(a => (a != null)).ToList())
                {
                    if ((DateTime.Parse(drf["StartDt"].ToString()).Date >= (dtStart.Date))
                        && (i < _iSlotLimit))
                    {
                        //if ((sUserID.Equals(string.Empty)) || (sUserID.Equals(drf["User_ID"].ToString())))
                        //{
                        DataRow dr = dtSlots.NewRow();
                        //dr["SLOT_ID"] = DateTime.Parse(drf["StartDt"].ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "|" + (DateTime.Parse(drf["EndDt"].ToString())).ToString("yyyy-MM-dd HH:mm:ss") + "|" + drf["USER_ID"].ToString() + "^" + _dtFreeUserCollection.Select("USER_ID = '" + drf["USER_ID"].ToString() + "'")[0]["DSPL_NME"].ToString() + "|" + _duration.ToString();
                        dr["SLOT_ID"] = DateTime.Parse(drf["StartDt"].ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "|" + (DateTime.Parse(drf["EndDt"].ToString())).ToString("yyyy-MM-dd HH:mm:ss") + "|" + drf["USER_ID"].ToString() + "|" + _duration.ToString();
                        dr["SLOT_DESC"] = DateTime.Parse(drf["StartDt"].ToString()).ToString() + " TO " + DateTime.Parse(drf["EndDt"].ToString()).ToString();
                        dr["SLOT_USER"] = drf["DSPL_NME"].ToString();
                        dtSlots.Rows.Add(dr);
                        dtStart = dtStart.AddMinutes(_increment);
                        sUserID = drf["User_ID"].ToString();
                        i++;

                        //}
                        /*else
                        {
                            if (_dtFreeCollection.Select("USER_ID = '" + sUserID + "'  AND StartDt = '" + dtStart.ToString() + "'").Length <= 0)
                                sUserID = string.Empty;
                        }*/
                    }
                }
            }
            CleanUpGlobalTables();
            _logger.LogInformation($"Check Current Slot Result. { JsonConvert.SerializeObject(dtSlots).ToString() } ");
            return dtSlots;
        }


        [HttpPost("GetLaunchPicker")]
        public ActionResult<DataSet> GetLaunchPicker([FromBody] DynamicParametersViewModel model)
        {
            _logger.LogInformation($"Get Launch Picker: { JsonConvert.SerializeObject(model).ToString() }.");
            //LoadHashTable(model);
            //htSlotData = htSlotPickData;
            _iSlotLimit = 1;
            if (_duration == 0.00F)
                _duration = 60.00F;
            DataTable dtDur = new DataTable();
            dtDur.Columns.Add("Duration");

            GetSlotsForCurrentSlot(model);

            dtDur.Rows.Add(new String[] { _duration.ToString() });

            var skilled = _dtSkilledMember.Copy();
            var product = _dtProductUser.Copy();
            skilled.TableName = "skilled";
            product.TableName = "product";


            DataSet ds = new DataSet();
            ds.Tables.Add(_dtLaunchPickerCollection.Copy());
            ds.Tables.Add(skilled);
            ds.Tables.Add(product);
            ds.Tables.Add(dtDur);

            CleanUpGlobalTables();
            
            _logger.LogInformation($"Get Launch Picker Result. { JsonConvert.SerializeObject(ds).ToString() } ");
            return ds;
        }

        [HttpPost("ChkForDblBooking")]
        public string ChkForDblBooking([FromBody] DynamicParametersViewModel model)
        {
            return _slotPickerRepo.ChkForDblBooking(model.Start, model.End, model.UID, model.EventId);
        }

        private void CleanUpGlobalTables()
        {
            _dtBusyCollection.Clear();
            _dtWFCollection.Clear();
            _dtOpenSlotsCollection.Clear();
            _dtScheduleMember.Clear();
            _dtSkilledMember.Clear();
            _dtProductUser.Clear();
            _dtRandomBusyCollection.Clear();
            _dtTempRandomBusyCollection.Clear();
            _dtFreeCollection.Clear();
            _dtFreeUserCollection.Clear();

            _dtBusyCollection = null;
            _dtWFCollection = null;
            _dtOpenSlotsCollection = null;
            _dtScheduleMember = null;
            _dtSkilledMember = null;
            _dtProductUser = null;
            _dtRandomBusyCollection = null;
            _dtTempRandomBusyCollection = null;
            _dtFreeCollection = null;
            _dtFreeUserCollection = null;
        }

        protected string GetUserIDByRandomBusyCollection(int indx)
        {
            DataTable _dtFreeCollectionTQ = new DataTable();
            _dtFreeCollectionTQ.Columns.Add(new DataColumn("USER_ID"));
            _dtFreeCollectionTQ.Columns.Add(new DataColumn("StartDt", typeof(DateTime)));
            _dtFreeCollectionTQ.Columns.Add(new DataColumn("EndDt", typeof(DateTime)));

            DataRow[] drF = _dtFreeCollection.Select("StartDt <= #" + dtSt.ToString() + "# AND EndDt >= #" + dtEnd.ToString() + "#");
            if ((drF.Length <= 0) && (_dtFreeCollection.Rows.Count > 0))
            {
                var c1 = (from fc in _dtFreeCollection.AsEnumerable()
                          select new
                          {
                              StartDt = DateTime.Parse(fc.Field<Object>("StartDt").ToString()),
                              EndDt = DateTime.Parse(fc.Field<Object>("EndDt").ToString()),
                              USER_ID = Int32.Parse(fc.Field<Object>("USER_ID").ToString())
                          }).Distinct().OrderBy(w => w.StartDt).Take(1).ToList();

                drF = _dtFreeCollection.Select("StartDt <= #" + c1[0].StartDt.ToString() + "# AND EndDt >= #" + c1[0].EndDt.ToString() + "#");
            }
            foreach (DataRow dr in drF)
                _dtFreeCollectionTQ.Rows.Add(new Object[] { dr["USER_ID"].ToString(), dr["StartDt"].ToString(), dr["EndDt"].ToString() });

            if (_dtFreeCollectionTQ != null && _dtFreeCollectionTQ.Rows.Count == 1)
                return _dtFreeCollectionTQ.Rows[0]["USER_ID"].ToString();
            else
            {
                if ((_dtBusyCollection != null) && (_dtBusyCollection.Rows.Count > 0))
                {
                    List<string> lBusyUserID = new List<string>();
                    List<SlotData> lFreeUserID = new List<SlotData>();

                    if (_dtFreeCollectionTQ.Rows.Count > 0)
                    {
                        lBusyUserID = (from bc in _dtBusyCollection.AsEnumerable()
                                       join fc in _dtFreeCollectionTQ.AsEnumerable() on Convert.ToString(bc.Field<Object>("USER_ID").ToString()) equals Convert.ToString(fc.Field<Object>("USER_ID").ToString())
                                       select Convert.ToString(bc.Field<Object>("USER_ID").ToString())).Distinct().ToList();
                    }
                    else
                    {
                        lBusyUserID = (from bc in _dtBusyCollection.AsEnumerable()
                                       join fc in _dtFreeCollection.AsEnumerable() on Convert.ToString(bc.Field<Object>("USER_ID").ToString()) equals Convert.ToString(fc.Field<Object>("USER_ID").ToString())
                                       select Convert.ToString(bc.Field<Object>("USER_ID").ToString())).Distinct().ToList();
                    }

                    if (lBusyUserID.Count > 0)
                    {
                        if (indx == -1)
                        {
                            if (_dtFreeCollectionTQ.Rows.Count > 0)
                            {
                                var FreeUserID = (from fc in _dtFreeCollectionTQ.AsEnumerable()
                                                  where (!(lBusyUserID.Contains(Convert.ToString(fc.Field<Object>("USER_ID").ToString()))))
                                                  select new { User_ID = Convert.ToString(fc.Field<Object>("USER_ID").ToString()), StartDt = Convert.ToString(fc.Field<Object>("StartDt").ToString()) }).ToList();

                                foreach (var fui in FreeUserID)
                                    lFreeUserID.Add(new SlotData(fui.User_ID, DateTime.Parse(fui.StartDt)));
                            }
                            else
                            {
                                var FreeUserID = (from fc in _dtFreeCollection.AsEnumerable()
                                                  where (!(lBusyUserID.Contains(Convert.ToString(fc.Field<Object>("USER_ID").ToString()))))
                                                  select new { User_ID = Convert.ToString(fc.Field<Object>("USER_ID").ToString()), StartDt = Convert.ToString(fc.Field<Object>("StartDt").ToString()) }).ToList();

                                foreach (var fui in FreeUserID)
                                    lFreeUserID.Add(new SlotData(fui.User_ID, DateTime.Parse(fui.StartDt)));
                            }

                            if (lFreeUserID.Count > 0)
                            {
                                foreach (SlotData s in lFreeUserID)
                                {
                                    _dtRandomBusyCollection.Rows.Add(new Object[] { s.UserID, s.StartDt, "0" });
                                }
                            }

                            foreach (string s in lBusyUserID)
                            {
                                int diffMinutes = 0;
                                //Ignore Lunch, Personal, Vacation from busy utilization time
                                foreach (DataRow dr in _dtBusyCollection.Select("USER_ID = '" + s + "' AND APPT_TYPE_ID NOT IN (18,19,20)"))
                                {
                                    if (DateTime.Parse(dr["EndTmst"].ToString()) > DateTime.Parse(dr["StrtTmst"].ToString()))
                                        diffMinutes = diffMinutes + (int)DateTime.Parse(dr["EndTmst"].ToString()).Subtract(DateTime.Parse(dr["StrtTmst"].ToString())).TotalMinutes;
                                }

                                foreach (DataRow dr in _dtBusyCollection.Select("USER_ID = '" + s + "'"))
                                {
                                    _dtRandomBusyCollection.Rows.Add(new Object[] { s, DateTime.Parse(dr["StrtTmst"].ToString()), diffMinutes });
                                }
                            }

                            var ra = (from rb in _dtRandomBusyCollection.AsEnumerable()
                                      select new
                                      {
                                          USER_ID = Int32.Parse(rb.Field<Object>("USER_ID").ToString()),
                                          StartDt = DateTime.Parse(rb.Field<Object>("StartDt").ToString()),
                                          TotalTime = Int32.Parse(rb.Field<Object>("TotalTime").ToString())
                                      }).Distinct().OrderBy(s => s.StartDt).OrderBy(r => r.TotalTime);

                            _dtTempRandomBusyCollection = LinqHelper.CopyToDataTable(ra, null, null);

                            DataRow[] drArr = _dtTempRandomBusyCollection.Select("StartDt = #" + _dtTempRandomBusyCollection.Rows[0]["StartDt"] + "# AND TotalTime = '" + _dtTempRandomBusyCollection.Rows[0]["TotalTime"] + "'");

                            if (drArr.Length > 1)
                            {
                                string sActvWhenMany = _slotPickerRepo.GetActivatorWhenConflict(_dtTempRandomBusyCollection.AsEnumerable().Select(x => x.Field<int>("USER_ID").ToString()).ToArray(), _eOffSet);
                                if (String.IsNullOrEmpty(sActvWhenMany))
                                {
                                    Random random = new Random();
                                    int i = random.Next(0, drArr.Length);
                                    return _dtTempRandomBusyCollection.Rows[i]["USER_ID"].ToString();
                                }
                                else return sActvWhenMany;
                            }
                            else
                                return _dtTempRandomBusyCollection.Rows[0]["USER_ID"].ToString();
                        }
                        else
                            return _dtTempRandomBusyCollection.Rows[indx]["USER_ID"].ToString();
                    }
                    else
                        return string.Empty;
                }
                else
                    return string.Empty;
            }
        }

        private void LoadHashTable(DynamicParametersViewModel model)
        {
            htSlotData["StTime"] = model.StTime;
            htSlotData["StHr"] = model.StHr;
            htSlotData["StMin"] = model.StMin;
            htSlotData["Product"] = model.Product; // EventTypeID
            htSlotData["SubmitType"] = model.SubmitType; // EnhanceServiceName
            htSlotData["ExtraDuration"] = model.ExtraDuration;
            htSlotData["EventID"] = model.EventId;
            htSlotData["IsMDSFT"] = model.IsMDSFT;
            var lOrderedMDSNtwkActy = model.mdsNtwkActy != null ? _mdsNetworkActivityTypeRepo.Find(a => model.mdsNtwkActy.Any(b => b == a.NtwkActyTypeId)).ToList().OrderBy(c=>c.NtwkActyTypeDes).ToList() : null;
            htSlotData["MDSNtwkActy"] = lOrderedMDSNtwkActy;
            htSlotData["MACActivityType"] = model.macActivityType;
            htSlotData["NoOfAccess"] = model.NoOfAccess;
            htSlotData["MPLSActivityType"] = model.MplsActivityTypes;
            htSlotData["MPLSEventType"] = model.MplsEventType;
            htSlotData["VPNPltType"] = model.VpnPlatformType;
            htSlotData["lVasType"] = model.MplsVasTypes;
            htSlotData["IsWhlSalePrtnr"] = model.IsWhlSalePrtnr;
            htSlotData["IPVersion"] = model.IpVersionId;
            htSlotData["SLNKEventType"] = model.SlnkEventType;
            htSlotData["SLNKActyType"] = model.SlnkActyType;

            htSlotData["H1"] = model.h1;
            htSlotData["CustomerName"] = model.customerName;
            htSlotData["IsFirewallSecurity"] = model.isFirewallSecurity;

            // For MDS
            htSlotData["MdsOld"] = model.mdsOld;
            htSlotData["MDSMACAct"] = model.mdsMacAct;
            htSlotData["MngDevTbl"] = model.mngDevTbl;
            htSlotData["MngDevTblCnt"] = model.mngDevTblCnt;
            htSlotData["AnyVirtualConn"] = model.anyVirtualConn;
            htSlotData["VirtualTable"] = model.virtualTable;
            htSlotData["IsUSInternational"] = model.isUSInternational;
            htSlotData["IsSprintCPETabCnt"] = model.isSprintCPETabCnt;
            //htSlotData["IsSPAETransportRequired"] = model.mdsMacAct;
            //htSlotData["MDSSPAETable"] = model.mdsMacAct;
            //htSlotData["IsDSLTransportRequired"] = model.mdsMacAct;
            //htSlotData["MDSDSLTable"] = model.mdsMacAct;
            //htSlotData["IsSpRFTransportRequired"] = model.mdsMacAct;
            //htSlotData["MDSSPRFTable"] = model.mdsMacAct;
            //htSlotData["IsWiredDeviceTransportRequired"] = model.mdsMacAct;
            //htSlotData["MDSWiredTable"] = model.mdsMacAct;
            htSlotData["IsSPRFWiredDevTrptReq"] = model.isSPRFWiredDevTrptReq;
            htSlotData["MDSSPRFWiredTable"] = model.mdsSPRFWiredTable;
            htSlotData["IsDSLSBICCustTrptReq"] = model.isDSLSBICCustTrptReq;
            htSlotData["MDSDSLSBICCustTable"] = model.mdsDSLSBICCustTable;
            htSlotData["IsWirelessDeviceTransportRequired"] = model.isWirelessDeviceTransportRequired;
            htSlotData["MDSWirelessTable"] = model.mdsWirelessTable;
            htSlotData["MDSNtwkTrpt"] = model.mdsNtwkTrpt;
            htSlotData["SCFlag"] = model.scFlag;
            //htSlotData["OPTOUTREAS"] = model.mdsMacAct;
            htSlotData["VASCECd"] = model.vASCECd;
            htSlotData["IsSpclPrj"] = model.isSpclPrj;
        }

        public void GetSlotsForCurrentSlot(DynamicParametersViewModel model)
        {
            LoadHashTable(model);

            _ProductID = Int32.Parse(htSlotData["Product"].ToString());
            _sAction = htSlotData["SubmitType"].ToString();
            lMDSNtwkActy = (List<LkMdsNtwkActyType>)htSlotData["MDSNtwkActy"];

            if (_dtProductUser.Rows.Count <= 0)
                LoadInitialData();

            Single extraDuration = htSlotData["ExtraDuration"] != null
                ? Single.Parse(htSlotData["ExtraDuration"].ToString()) : 0;
            extraDuration = (extraDuration < 0) ? 0.00F : extraDuration;

            if (_sAction.Equals("MDS"))
            {
                _sIsMDSFT = htSlotData["IsMDSFT"].ToString();
                Single calcDuration = CheckMDSNtwkActy("MDSOnly") ? CalculateMDSEventDuration() : ((CheckMDSNtwkActy("NtwkOnly") || CheckMDSNtwkActy("VASOnly") || CheckMDSNtwkActy("NtwkVAS")) ? CalculateNtwkVASEventDuration() : (CalculateNtwkVASEventDuration() + CalculateMDSEventDuration()));
                //Single calcDuration = mdsNtwkActy.Equals(((byte)EMDSNtwkActyType.MDSOnly).ToString()) ? CalculateMDSEventDuration() : (mdsNtwkActy.Equals(((byte)EMDSNtwkActyType.NetworkOnly).ToString()) ? CalculateMDSNtwkEventDuration() : (CalculateMDSNtwkEventDuration() + CalculateMDSEventDuration()));
                _duration = extraDuration + calcDuration;
                SetSkilledMemberObjectForMDSUCaaS();
            }
            else if (_sAction.Equals("UCaaS"))
            {
                Single calcDuration = CalculateUCaaSEventDuration();
                _duration = extraDuration + calcDuration;
                SetSkilledMemberObjectForMDSUCaaS();
            }
            else if ((_sAction.Equals("STANDARD")) || (_sAction.Equals("VAS")))
            {
                Single calcDuration = CalculateMPLSEventDuration();
                _duration = extraDuration + calcDuration;
                SetSkilledMemberObjectForSubmitType();
            }
            else if (_sAction.Equals("SLNK STANDARD"))
            {
                Single calcDuration = CalculateSLNKEventDuration();
                _duration = extraDuration + calcDuration;
                SetSkilledMemberObjectForSubmitType();
            }
            else
            {
                Single calcDuration = 60.00F;
                _duration = extraDuration + calcDuration;
                SetSkilledMemberObjectForSubmitType();
            }

            if ((_duration % 5) != 0)
            {
                float rem = _duration % 5;
                float result = _duration - rem;
                _duration = result + 5;
            }

            if (_duration == 0.00F)
                sError = "System could not calculate Duration for event.";

            if (_dtSkilledMember != null)
            {
                if (_dtSkilledMember.Rows.Count <= 0)
                    sError = "No resources are SKILL available.";
            }
            else
                sError = "No resources are SKILL available.";

            CalculateEndTime(model.StTime, model.StHr, model.StMin, _duration);

            SetFreeCollectionObject(_duration, _increment);
        }

        public void LoadInitialData()
        {
            GetRoleUsers();
            FilterUsersByProduct();
            _dtSkilledMember.Columns.Add(new DataColumn("USER_ID"));
            _dtSkilledMember.Columns.Add(new DataColumn("USER_ADID"));
            _dtSkilledMember.Columns.Add(new DataColumn("DSPL_NME"));
            _dtSkilledMember.Columns.Add(new DataColumn("FULL_NME"));

            _dtFreeCollection.Columns.Add(new DataColumn("USER_ID"));
            _dtFreeCollection.Columns.Add(new DataColumn("DSPL_NME"));
            _dtFreeCollection.Columns.Add(new DataColumn("StartDt", typeof(DateTime)));
            _dtFreeCollection.Columns.Add(new DataColumn("EndDt", typeof(DateTime)));

            _dtFreeUserCollection.Columns.Add(new DataColumn("USER_ID"));
            _dtFreeUserCollection.Columns.Add(new DataColumn("USER_ADID"));
            _dtFreeUserCollection.Columns.Add(new DataColumn("DSPL_NME"));
            _dtFreeUserCollection.Columns.Add(new DataColumn("FULL_NME"));

            //_dtLaunchPickerCollection.TableName = "Schedule";
            _dtLaunchPickerCollection.Columns.Add(new DataColumn("USER_ID"));
            _dtLaunchPickerCollection.Columns.Add(new DataColumn("USER_ADID"));
            _dtLaunchPickerCollection.Columns.Add(new DataColumn("DSPL_NME"));
            _dtLaunchPickerCollection.Columns.Add(new DataColumn("FULL_NME"));
            _dtLaunchPickerCollection.Columns.Add(new DataColumn("StartDt", typeof(DateTime)));
            _dtLaunchPickerCollection.Columns.Add(new DataColumn("EndDt", typeof(DateTime)));

            _dtOpenSlotsCollection.Columns.Add(new DataColumn("USER_ID"));
            _dtOpenSlotsCollection.Columns.Add(new DataColumn("FULL_NME"));
            _dtOpenSlotsCollection.Columns.Add(new DataColumn("TimeSlot"));

            _dtRandomBusyCollection.Columns.Add(new DataColumn("USER_ID"));
            _dtRandomBusyCollection.Columns.Add(new DataColumn("StartDt", typeof(DateTime)));
            _dtRandomBusyCollection.Columns.Add(new DataColumn("TotalTime"));

            _dtTempRandomBusyCollection.Columns.Add(new DataColumn("USER_ID"));
            _dtTempRandomBusyCollection.Columns.Add(new DataColumn("TotalTime"));
        }

        protected void GetRoleUsers()
        {
            _dtUserProfiles = _slotPickerRepo.GetUsersByProfile(_profile);
        }

        protected void FilterUsersByProduct()
        {
            DataTable dt = new DataTable();
            dt = _slotPickerRepo.getUsersByProduct(_ProductID);
            _dtProductUser = _slotPickerRepo.getUsersByPrdProfile(dt, _dtUserProfiles);
        }

        protected Single CalculateMDSEventDuration()
        {
            float fSCMulti = 1.0F;
            if ((!(htSlotData["MACActivityType"].ToString().Equals("Disconnect")))
                && (bool.Parse(htSlotData["SCFlag"].ToString())))
            {
                string sSCMulti = GetSysCfgVal("SCMultiplier");
                bool bSCMulti = float.TryParse(sSCMulti, out fSCMulti);
                fSCMulti = (!bSCMulti) ? 1.0F : fSCMulti;
            }

            Single Duration = 0.00F;
            if ((htSlotData["MACActivityType"].ToString().Equals("MAC") || (htSlotData["MACActivityType"].ToString().Equals("MDS Pre-Staging"))) && (!(DoesMDSMACHasAddSite())))
            {
                Duration = GetDurationForMDSActivityTypeMAC(fSCMulti) +
                           GetDurationForVirtualConnections() +
                           GetDurationForInternational() +
                           GetDurationForBackup();
            }
            else if (htSlotData["MACActivityType"].ToString().Equals("Disconnect"))
            {
                float fMDSDiscMulti;
                string sMDSDiscMulti = GetSysCfgVal("MDSDiscoDuration");
                bool bMDSDiscMulti = float.TryParse(sMDSDiscMulti, out fMDSDiscMulti);
                if (!bMDSDiscMulti)
                    fMDSDiscMulti = 1.0F;

                Duration = (int.Parse(htSlotData["MngDevTblCnt"].ToString()) * fMDSDiscMulti);
            }
            else
            {
                Duration = GetDurationForManagedDevices(fSCMulti) +
                           GetDurationForVirtualConnections() +
                           GetDurationForInternational() +
                           GetDurationForBackup();
            }

            return Duration;
        }

        protected Single CalculateUCaaSEventDuration()
        {
            Single Duration = 0.00F;
            Duration = GetDurationForManagedDevices(1.0F);
            return Duration;
        }

        protected Single GetDurationForMDSActivityTypeMAC(float fSCMulti)
        {
            DataTable dt = new DataTable();
            float MACActyDuration = 0.00F;
            dt = _slotPickerRepo.getAllMDSMACDuration();
            if (bool.Parse(htSlotData["MdsOld"].ToString()))
            {
                if ((((DataTable)htSlotData["MDSMACAct"]) != null) && (((DataTable)htSlotData["MDSMACAct"]).Rows.Count > 0))
                {
                    if (((DataTable)htSlotData["MngDevTbl"] != null) && (((DataTable)htSlotData["MngDevTbl"]).Rows.Count > 0))
                        MACActyDuration = _slotPickerRepo.getMDSMACDuration(dt, ((DataTable)htSlotData["MDSMACAct"])) * ((DataTable)htSlotData["MngDevTbl"]).Rows.Count;
                    else
                        MACActyDuration = _slotPickerRepo.getMDSMACDuration(dt, ((DataTable)htSlotData["MDSMACAct"]));
                }
                else
                    return 0.00F;
            }
            else
            {
                if ((((List<MdsEventMacActy>)htSlotData["MDSMACAct"]) != null) && (((List<MdsEventMacActy>)htSlotData["MDSMACAct"]).Count > 0))
                {
                    if (((List<MdsEventOdieDev>)htSlotData["MngDevTbl"] != null) && (((List<MdsEventOdieDev>)htSlotData["MngDevTbl"]).Count > 0))
                        MACActyDuration = _slotPickerRepo.getMDSMACDuration(dt, ((List<MdsEventMacActy>)htSlotData["MDSMACAct"])) * ((List<MdsEventOdieDev>)htSlotData["MngDevTbl"]).Count;
                    else
                        MACActyDuration = _slotPickerRepo.getMDSMACDuration(dt, ((List<MdsEventMacActy>)htSlotData["MDSMACAct"]));
                }
                else
                    return 0.00F;
            }

            return (MACActyDuration * fSCMulti);
        }

        protected Single CalculateMPLSEventDuration()
        {
            Single Floor = 60.00F;

            short NoOfAccess = short.Parse(htSlotData["NoOfAccess"].ToString());

            List<string> sMPLSActvType = (List<string>)htSlotData["MPLSActivityType"];
            Single BaseLine = 60.00F;

            Single COSBaseLine = 75.00F;

            Single Duration = (NoOfAccess == 1) ? GetDurationForMPLSEventType(NoOfAccess) : (GetDurationforWholeSale(NoOfAccess) + GetDurationForMPLSActivityType(NoOfAccess) +
                              GetDurationForMPLSEventType(NoOfAccess));

            if (sMPLSActvType.Contains(((int)MplsActivityType.Cos).ToString()))
                Duration += COSBaseLine;
            else
                Duration += BaseLine;

            if (Duration < Floor)
                Duration += Floor;

            return Duration;
        }

        protected Single CalculateSLNKEventDuration()
        {
            Single BaseLine = 60.00F;

            string sActyType = htSlotData["SLNKActyType"].ToString();
            short NoOfAccess = short.Parse(htSlotData["NoOfAccess"].ToString());

            Single Duration = 0.00F;
            Duration = BaseLine + ((NoOfAccess - 1)
                * (sActyType.Contains(((int)SplkActivityType.DLCIChange).ToString()) ? 30.00F
                : (sActyType.Contains(((int)SplkActivityType.Conversion).ToString()) ? 15.00F : Duration)));

            return Duration;
        }

        protected Single GetDurationForMPLSEventType(short NoOfAccess)
        {
            Single ActDuration = 0.00F;
            Single JBaseLine = 120.00F;

            string sMPLSEventType = htSlotData["MPLSEventType"].ToString();
            string sVPNPltType = htSlotData["VPNPltType"].ToString();
            List<string> lVasType = ((List<string>)htSlotData["lVasType"]);

            if (sMPLSEventType.Equals("IPv6/Dual Stack Implementation"))
                ActDuration = ActDuration + 0.00F;
            else if (sMPLSEventType.Equals("IPv6/Dual Stack Pre-Configuration"))
                ActDuration = ActDuration + 0.00F;
            else if (sMPLSEventType.Equals(((int)MplsEventType.VASScheduledPreConfiguration).ToString())
                || sMPLSEventType.Equals(((int)MplsEventType.VASScheduledImplementation).ToString()))
            {
                if (sVPNPltType.Equals(((int)VpnPlatformType.MplsJuniper).ToString()))
                {
                    if (lVasType.Contains(((int)VasType.FirewallInternet).ToString()))
                        ActDuration = ((NoOfAccess == 1) ? (ActDuration + JBaseLine) : (ActDuration + JBaseLine + ((NoOfAccess - 1) * 60.00F)));
                    if (lVasType.Contains(((int)VasType.RemoteAccess).ToString()))
                        ActDuration = ((NoOfAccess == 1) ? (ActDuration + JBaseLine) : (ActDuration + JBaseLine + ((NoOfAccess - 1) * 60.00F)));
                }
                else if (sVPNPltType.Equals(((int)VpnPlatformType.MplsCiscoIpsec).ToString()))
                {
                    if (lVasType.Contains(((int)VasType.Tunnels).ToString()))
                        ActDuration = ActDuration + (NoOfAccess * 120.00F);
                }
            }
            return ActDuration;
        }

        protected Single GetDurationforWholeSale(short NoOfAccess)
        {
            Single FormulaDuration = 0.00F;

            string sIsWhlSalePrtnr = htSlotData["IsWhlSalePrtnr"].ToString().ToUpper();

            if (sIsWhlSalePrtnr.Equals("TRUE"))
                FormulaDuration = (NoOfAccess - 1) * 15.00F;
            else
                FormulaDuration = 0.00F;

            return FormulaDuration;
        }

        protected Single GetDurationForMPLSActivityType(short NoOfAccess)
        {
            Single ActDuration = 0.00F;

            List<string> sMPLSActvType = (List<string>)htSlotData["MPLSActivityType"];

            if (sMPLSActvType != null && sMPLSActvType.Count() > 0)
            {
                foreach (string s in sMPLSActvType)
                {
                    ActDuration = s.Contains(((int)MplsActivityType.Downgrade).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 30.00F))
                                : (s.Contains(((int)MplsActivityType.Upgrade).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 30.00F))
                                : (s.Contains(((int)MplsActivityType.ReHome).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 30.00F))
                                : (s.Contains(((int)MplsActivityType.Multipath).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 30.00F))
                                : (s.Contains(((int)MplsActivityType.TurnUp).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 30.00F))
                                : (s.Contains(((int)MplsActivityType.Cos).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 15.00F))
                                : (s.Contains(((int)MplsActivityType.Dsl).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 30.00F))
                                : (s.Contains(((int)MplsActivityType.Migration).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 15.00F))
                                : (s.Contains(((int)MplsActivityType.SpecialProject).ToString()) ? (ActDuration + ((NoOfAccess - 1) * 15.00F)) : ActDuration))))))));
                }
            }

            return ActDuration;
        }

        protected Single GetDurationForManagedDevices(float fSCMulti)
        {
            float ActDuration = 0.00F;

            DataTable dt = new DataTable();
            List<short> mod = _sAction.Equals("UCaaS") ? (from od in ((List<UcaaSEventOdieDev>)(htSlotData["MngDevTbl"])) select od.DevModelId).ToList() : ((!bool.Parse(htSlotData["MdsOld"].ToString())) ? (from od in ((List<MdsEventOdieDev>)(htSlotData["MngDevTbl"])) select od.DevModelId).ToList() : new List<short>());

            dt = _sAction.Equals("UCaaS") ? ((((List<UcaaSEventOdieDev>)(htSlotData["MngDevTbl"])) != null)
                                                            ? LinqHelper.CopyToDataTable(mod)
                                                                : new DataTable())
                                                                : (!bool.Parse(htSlotData["MdsOld"].ToString()))
                                                                    ? LinqHelper.CopyToDataTable(mod)
                                                                    : (DataTable)htSlotData["MngDevTbl"];

            if (dt.Rows.Count > 0)
                ActDuration = _slotPickerRepo.getManDevDuration(bool.Parse(htSlotData["MdsOld"].ToString()), dt);

            return (ActDuration * fSCMulti);
        }

        protected Single GetDurationForVirtualConnections()
        {
            Single ActDuration = 0.00F;
            if (bool.Parse(htSlotData["MdsOld"].ToString()))
            {
                if (htSlotData["AnyVirtualConn"].ToString().ToUpper().Equals("TRUE"))
                {
                    int rows = ((DataTable)htSlotData["VirtualTable"]).Rows.Count;
                    if (rows == 0)
                    {
                        return ActDuration;
                    }
                    else if (rows < 4)
                    {
                        ActDuration = 30.00F;
                        return ActDuration;
                    }
                    else if (rows < 7)
                    {
                        ActDuration = 60.00F;
                        return ActDuration;
                    }
                    else if (rows < 10)
                    {
                        ActDuration = 90.00F;
                        return ActDuration;
                    }
                    else if (rows < 13)
                    {
                        ActDuration = 120.00F;
                        return ActDuration;
                    }
                    else if (rows < 16)
                    {
                        ActDuration = 150.00F;
                        return ActDuration;
                    }
                    else if (rows < 19)
                    {
                        ActDuration = 180.00F;
                        return ActDuration;
                    }
                    else if (rows < 22)
                    {
                        ActDuration = 210.00F;
                        return ActDuration;
                    }
                    else if (rows < 25)
                    {
                        ActDuration = 240.00F;
                        return ActDuration;
                    }
                    else if (rows < 28)
                    {
                        ActDuration = 270.00F;
                        return ActDuration;
                    }
                    else if (rows < 31)
                    {
                        ActDuration = 300.00F;
                        return ActDuration;
                    }
                    else
                    {
                        ActDuration = 330.00F;
                        return ActDuration;
                    }
                }
                else
                    return ActDuration;
            }
            else
                return ActDuration;
        }

        protected Single GetDurationForInternational()
        {
            Single ActDuration = 0.00F;
            Single Multiplier = 30.00F;

            if (htSlotData["IsUSInternational"].ToString().Equals("I"))
                ActDuration = Single.Parse(htSlotData["IsSprintCPETabCnt"].ToString()) * Multiplier;

            return ActDuration;
        }

        protected Single GetDurationForBackup()
        {
            Single ActDuration = 0.00F;
            Single Multiplier = 30.00F;
            int brows = 0;

            if (bool.Parse(htSlotData["MdsOld"].ToString()))
            {
                if (htSlotData["IsSPAETransportRequired"].ToString().ToUpper().Equals("TRUE"))
                {
                    int rows = ((DataTable)htSlotData["MDSSPAETable"]).Rows.Count;
                    brows = ((DataTable)htSlotData["MDSSPAETable"]).Select("PRIM_BKUP_CD = 'B'").Length;
                    if (rows == 0)
                        return ActDuration;
                    else if (brows > 0)
                    {
                        ActDuration = brows * Multiplier;
                    }
                }

                if (htSlotData["IsDSLTransportRequired"].ToString().ToUpper().Equals("TRUE"))
                {
                    int rows = ((DataTable)htSlotData["MDSDSLTable"]).Rows.Count;
                    brows = ((DataTable)htSlotData["MDSDSLTable"]).Select("PRIM_BKUP_CD = 'B'").Length;
                    if (rows == 0)
                        return ActDuration;
                    else if (brows > 0)
                    {
                        ActDuration += brows * Multiplier;
                    }
                }

                if (htSlotData["IsSpRFTransportRequired"].ToString().ToUpper().Equals("TRUE"))
                {
                    int rows = ((DataTable)htSlotData["MDSSPRFTable"]).Rows.Count;
                    brows = ((DataTable)htSlotData["MDSSPRFTable"]).Select("PRIM_BKUP_CD = 'B'").Length;
                    if (rows == 0)
                        return ActDuration;
                    else if (brows > 0)
                    {
                        ActDuration += brows * Multiplier;
                    }
                }

                if (htSlotData["IsWiredDeviceTransportRequired"].ToString().ToUpper().Equals("TRUE"))
                {
                    int rows = ((DataTable)htSlotData["MDSWiredTable"]).Rows.Count;
                    brows = ((DataTable)htSlotData["MDSWiredTable"]).Select("PRIM_BKUP_CD = 'B'").Length;
                    if (rows == 0)
                        return ActDuration;
                    else if (brows > 0)
                    {
                        ActDuration += brows * Multiplier;
                    }
                }
            }
            else
            {
                if (htSlotData["IsSPRFWiredDevTrptReq"].ToString().ToUpper().Equals("TRUE"))
                {
                    int rows = ((List<MdsEventSlnkWiredTrpt>)htSlotData["MDSSPRFWiredTable"]).Count;
                    brows = ((List<MdsEventSlnkWiredTrpt>)htSlotData["MDSSPRFWiredTable"]).Where(a => a.PrimBkupCd == "B").Count();
                    if (rows == 0)
                        return ActDuration;
                    else if (brows > 0)
                    {
                        ActDuration += brows * Multiplier;
                    }
                }

                if (htSlotData["IsDSLSBICCustTrptReq"].ToString().ToUpper().Equals("TRUE"))
                {
                    int rows = ((List<MdsEventDslSbicCustTrpt>)(htSlotData["MDSDSLSBICCustTable"])).Count;
                    brows = ((List<MdsEventDslSbicCustTrpt>)(htSlotData["MDSDSLSBICCustTable"])).Where(a => a.PrimBkupCd == "B").Count();
                    if (rows == 0)
                        return ActDuration;
                    else if (brows > 0)
                    {
                        ActDuration += (brows * Multiplier);
                    }
                }
            }

            if (htSlotData["IsWirelessDeviceTransportRequired"].ToString().ToUpper().Equals("TRUE"))
            {
                int rows = (bool.Parse(htSlotData["MdsOld"].ToString()))
                            ? ((DataTable)htSlotData["MDSWirelessTable"]).Rows.Count
                            : ((List<MdsEventWrlsTrpt>)(htSlotData["MDSWirelessTable"])).Count;
                brows = (bool.Parse(htSlotData["MdsOld"].ToString()))
                            ? ((DataTable)htSlotData["MDSWirelessTable"]).Select("PRIM_BKUP_CD = 'B'").Length
                            : ((List<MdsEventWrlsTrpt>)(htSlotData["MDSWirelessTable"])).Where(a => a.PrimBkupCd == "B").Count();
                if (rows == 0)
                    return ActDuration;
                else if (brows > 0)
                {
                    ActDuration += (brows * Multiplier);
                }
            }

            return ActDuration;
        }

        protected Single CalculateMDSNtwkEventDuration()
        {
            Single Duration = 60.00F;
            Single Multiplier = 30.00F;
            if (htSlotData["MDSNtwkTrpt"] != null)
            {
                // This is where the error goes - km967761
                int rows = ((DataTable)htSlotData["MDSNtwkTrpt"]).Rows.Count;
                if (rows <= 1)
                    return Duration;
                else
                    Duration += (rows - 1) * Multiplier;
            }

            return Duration;
        }

        protected bool DoesMDSMACHasAddSite()
        {
            if (bool.Parse(htSlotData["MdsOld"].ToString()))
            {
                DataTable dtMDSMACAct = ((DataTable)htSlotData["MDSMACAct"]);
                if ((dtMDSMACAct != null) && (dtMDSMACAct.Rows.Count > 0))
                {
                    if (dtMDSMACAct.Select("MDS_MAC_ACTY_NME = 'Install Managed Device (Simple or Complex)'").Length > 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
            {
                List<MdsEventMacActy> lmm = (List<MdsEventMacActy>)htSlotData["MDSMACAct"];
                if ((lmm != null) && (lmm.Count > 0))
                {
                    return lmm.Any(a => a.MdsMacActyId == 11);
                }
                else
                    return false;
            }
        }

        protected void SetSkilledMemberObjectForSubmitType()
        {
            string sIPVersion = string.Empty;
            string sSLNKEventType = string.Empty;
            if (_sAction.Equals("STANDARD") || _sAction.Equals("VAS") || _sAction.Equals("SLNK STANDARD"))
                sIPVersion = htSlotData["IPVersion"].ToString();
            if (_sAction.Equals("SLNK STANDARD"))
                sSLNKEventType = htSlotData["SLNKEventType"].ToString();

            DataTable dtAssignUsers = _slotPickerRepo.GetAssignSkilledMembersforSubmitType(_sAction, sIPVersion, sSLNKEventType);

            var x = (from au in dtAssignUsers.AsEnumerable()
                     join pm in _dtProductUser.AsEnumerable() on Convert.ToString(au.Field<Object>("USER_ID").ToString()) equals Convert.ToString(pm.Field<Object>("USER_ID").ToString())
                     select new
                     {
                         USER_ID = au.Field<Object>("USER_ID").ToString(),
                         USER_ADID = au.Field<Object>("USER_ADID").ToString(),
                         DSPL_NME = au.Field<Object>("DSPL_NME").ToString(),
                         FULL_NME = au.Field<Object>("FULL_NME").ToString()
                     });
            if (x != null)
            {
                _dtSkilledMember = LinqHelper.CopyToDataTable(x, null, null);
            }
        }

        protected void SetSkilledMemberObjectForMDSUCaaS()
        {
            DataTable dtMngDev = _sAction.Equals("UCaaS") ? ((((List<UcaaSEventOdieDev>)(htSlotData["MngDevTbl"])) != null)
                                                            ? LinqHelper.CopyToDataTable((from od in ((List<UcaaSEventOdieDev>)(htSlotData["MngDevTbl"])) select new { DEV_MODEL_ID = od.DevModelId }))
                                                                : new DataTable())
                                                                : (!bool.Parse(htSlotData["MdsOld"].ToString()))
                                                                    ? LinqHelper.CopyToDataTable((from od in ((List<MdsEventOdieDev>)(htSlotData["MngDevTbl"])) select new { DEV_MODEL_ID = od.DevModelId }))
                                                                    : (DataTable)htSlotData["MngDevTbl"];
            string sH1 = htSlotData["H1"].ToString(); // 926508167
            string sCustNme = htSlotData["CustomerName"].ToString(); // CINTAS CORPORATION
            string sMDSNtwkActy = BuildMDSNtwkActy(); // Migration
            string sIsFirewallSecurity = _sAction.Equals("UCaaS") ? string.Empty : htSlotData["IsFirewallSecurity"].ToString();
            DataTable dtAssignUsers = _slotPickerRepo.GetAssignSkilledMembersforMDS(ref dtMngDev, sH1, sIsFirewallSecurity, sCustNme, lMDSNtwkActy, (List<string>)(htSlotData["MPLSActivityType"]));

            var x = (from au in dtAssignUsers.AsEnumerable()
                     join pm in _dtProductUser.AsEnumerable() on Convert.ToString(au.Field<Object>("USER_ID").ToString()) equals Convert.ToString(pm.Field<Object>("USER_ID").ToString())
                     select new
                     {
                         USER_ID = au.Field<Object>("USER_ID").ToString(),
                         USER_ADID = au.Field<Object>("USER_ADID").ToString(),
                         DSPL_NME = au.Field<Object>("DSPL_NME").ToString(),
                         FULL_NME = au.Field<Object>("FULL_NME").ToString()
                     });
            if (x != null)
            {
                _dtSkilledMember = LinqHelper.CopyToDataTable(x, null, null);
            }
        }

        protected void CalculateEndTime(string sStTime, string sStHr, string sStMin, Single sDuration)
        {
            // sStTime = 04/18/2022 09:00 AM
            // sStHr = 9
            // sStMin = 0
            // sDuration = 60
            _dtOrigSt = DateTime.Parse(sStTime).Date.AddHours(Int32.Parse(sStHr)).AddMinutes(Int32.Parse(sStMin)); // 04/18/2022 09:00
            _dtSt = DateTime.Parse(sStTime); // 04/18/2022
            TimeSpan tsST = new TimeSpan(Int32.Parse(sStHr), Int32.Parse(sStMin), 0);
            _dtSt = _dtSt.Date + tsST;

            _dtEnd = _dtSt.AddMinutes(sDuration); // 04/18/2022 10:00
        }

        protected void SetFreeCollectionObject(Single siDuration, Single siIncrement)
        {
            DateTime DCStart = _dtOrigSt;  //_dtSt.Date.AddDays(_sOffSet)+ new TimeSpan(0, 0, 0); //may be need to add back  // 04/18/2022 09:00
            DateTime DCEnd = _dtSt.Date.AddDays(_eOffSet) + new TimeSpan(23, 55, 0); // 04/21/2022 23:55
            DateTime BusyStart = _dtSt.Date.Subtract(new TimeSpan(_eOffSet, 0, 0, 0)) + new TimeSpan(0, 0, 0); // 04/15/2022 00:00

            SetWFCollection(DCStart, DCEnd);

            SetFreeBusyCollection(BusyStart, DCEnd, (_dtOrigSt.Date + new TimeSpan(0, 0, 0)));

            /*GetFreeCollectionObject(siDuration, siIncrement, false);

            var f = (from fc in _dtFreeCollection.AsEnumerable()
                     group fc by new { USER_ID = Convert.ToString(fc.Field<Object>("USER_ID").ToString()) }
                         into myGroup
                         where myGroup.Count() > 0
                         select new { USER_ID = myGroup.Key.USER_ID.ToString(), fcount = myGroup.Count() }).Where(a => (a.fcount < 5)).ToList();

            if ((f.Count > 0) || (_dtFreeCollection == null) || (_dtFreeCollection.Rows.Count == 0))*/
            GetFreeCollectionObject(siDuration, siIncrement);

            var x = (from fc in _dtFreeCollection.AsEnumerable()
                     where (DateTime.Parse(fc.Field<Object>("StartDt").ToString()) >= _dtSt)
                     select new
                     {
                         USER_ID = Convert.ToString(fc.Field<Object>("USER_ID").ToString()),
                         DSPL_NME = Convert.ToString(fc.Field<Object>("DSPL_NME").ToString()),
                         StartDt = Convert.ToDateTime(fc.Field<Object>("StartDt").ToString()),
                         EndDt = Convert.ToDateTime(fc.Field<Object>("EndDt").ToString())
                     }).Distinct();

            _dtFreeCollection = LinqHelper.CopyToDataTable(x, null, null);

            var y = (from fc in _dtFreeUserCollection.AsEnumerable()
                     select new
                     {
                         USER_ID = Convert.ToString(fc.Field<Object>("USER_ID").ToString()),
                         USER_ADID = Convert.ToString(fc.Field<Object>("USER_ADID").ToString()),
                         DSPL_NME = Convert.ToString(fc.Field<Object>("DSPL_NME").ToString()),
                         FULL_NME = Convert.ToString(fc.Field<Object>("FULL_NME").ToString())
                     }).Distinct();

            _dtFreeUserCollection = LinqHelper.CopyToDataTable(y, null, null);

            foreach (DataRow dr in _dtFreeUserCollection.Rows)
            {
                var lpc = (from fc in _dtFreeCollection.AsEnumerable()
                           where (DateTime.Parse(fc.Field<Object>("StartDt").ToString()).Equals(_dtSt))
                              && (Convert.ToString(fc.Field<Object>("USER_ID").ToString()).Equals(dr["USER_ID"].ToString()))
                           select fc).ToList();

                if ((lpc != null) && (lpc.Count > 0))
                    _dtLaunchPickerCollection.Rows.Add(new Object[] { dr["USER_ID"], dr["USER_ADID"], dr["DSPL_NME"], dr["FULL_NME"], _dtSt, lpc[0]["EndDt"] });
            }
        }

        protected void SetWFCollection(DateTime DCStart, DateTime DCEnd)
        {
            DataSet dsWFData = _slotPickerRepo.GetWFCollection(ref _dtSkilledMember, DCStart, DCEnd, _sAction, _sIsMDSFT, _ProductID);

            _dtWFCollection = dsWFData.Tables[0];
            _dtScheduleMember = dsWFData.Tables[1];
        }

        protected void SetFreeBusyCollection(DateTime BusyStart, DateTime DCEnd, DateTime OrigStDt)
        {
            int EventID = htSlotData["EventID"].ToString().Length > 0 ? Int32.Parse(htSlotData["EventID"].ToString()) : -1;
            _dtBusyCollection = _slotPickerRepo.GetFreeBusyCollection(ref _dtScheduleMember, BusyStart, DCEnd, OrigStDt, EventID);
        }

        private void GetFreeCollectionObject(Single siDuration, Single siIncrement)
        {
            DateTime ActualStdt = _dtSt;
            DateTime FStDt = _dtSt;
            DateTime FEndDt = _dtSt.AddMinutes(siDuration);
            Single Increment = 0.00F;
            DateTime ShiftStart;
            DateTime ShiftEnd;

            bool bUserLoop = false;

            //bool bShiftLoop = false;
            string sLoopUser = string.Empty;
            DataTable dt = new DataTable();

            var ra = (from fc in _dtWFCollection.AsEnumerable()

                          //where DateTime.Parse(fc.Field<Object>("StartDt").ToString()) > FStDt
                      select new
                      {
                          StartDt = DateTime.Parse(fc.Field<Object>("StartDt").ToString()),
                          EndDt = DateTime.Parse(fc.Field<Object>("EndDt").ToString()),
                          USER_ID = Int32.Parse(fc.Field<Object>("USER_ID").ToString()),
                          USER_ADID = Convert.ToString(fc.Field<Object>("USER_ADID").ToString()),
                          DSPL_NME = Convert.ToString(fc.Field<Object>("DSPL_NME").ToString()),
                          FULL_NME = Convert.ToString(fc.Field<Object>("FULL_NME").ToString())
                      }).Distinct().OrderBy(v => v.StartDt).ThenBy(w => w.USER_ID);

            dt = LinqHelper.CopyToDataTable(ra, null, null);

            DataTable dtTempWFCol = dt.Copy();

            int iSlotLimit = 0;
            DateTime StartDt = new DateTime();
            foreach (DataRow dr in dtTempWFCol.Rows)
            {
                //  if (dr["USER_ID"].ToString().Equals("6446"))
                //{
                Increment = 0.00F;
                iSlotLimit = (int)((DateTime.Parse(dr["EndDt"].ToString()) - DateTime.Parse(dr["StartDt"].ToString())).TotalMinutes / 5);
                for (int i = 0; i < iSlotLimit; i++)
                {
                    bUserLoop = false;
                    ShiftStart = DateTime.Parse(dr["StartDt"].ToString());
                    ShiftEnd = DateTime.Parse(dr["EndDt"].ToString());
                    if (i == 0)
                    {
                        StartDt = ShiftStart;
                        FStDt = ShiftStart;
                        FEndDt = FStDt.AddMinutes(siDuration);
                    }

                    if ((_dtWFCollection.Select("USER_ID = '" + dr["USER_ID"].ToString() + "' AND StartDt <= #" + FStDt.ToString() + "# AND EndDt >= #" + FEndDt.ToString() + "#").Length > 0)
                        &&
                            (((ShiftStart.Date == ActualStdt.Date) && (FStDt >= ActualStdt)
                               && (((float)((DateTime.Parse(dr["EndDt"].ToString()) - DateTime.Parse(dr["StartDt"].ToString())).TotalMinutes)) >= siDuration))// && (FStDt >= ShiftStart) && (FStDt < ShiftEnd) && (FEndDt > ShiftStart) && (FEndDt <= ShiftEnd))
                        ||
                        ((ShiftStart.Date != ActualStdt.Date) && (FStDt >= ShiftStart) && (FStDt < ShiftEnd) && (FEndDt > ShiftStart) && (FEndDt <= ShiftEnd))
                        )
                    )
                    {
                        if (_dtBusyCollection.Select("USER_ID = '" + dr["USER_ID"].ToString() + "'").Length > 0)
                        {
                            foreach (DataRow drb in _dtBusyCollection.Select("USER_ID = '" + dr["USER_ID"].ToString() + "'"))
                            {
                                ShiftStart = DateTime.Parse(drb["StrtTmst"].ToString());
                                ShiftEnd = DateTime.Parse(drb["EndTmst"].ToString());

                                if ((FStDt < ShiftEnd) && (FEndDt > ShiftStart))
                                {
                                    bUserLoop = true;
                                    sLoopUser = dr["USER_ID"].ToString();
                                }
                            }
                        }
                        if (bUserLoop == false)
                        {
                            if (_dtFreeCollection.Select("USER_ID = '" + dr["USER_ID"].ToString() + "' AND StartDt = #" + FStDt.ToString() + "# AND EndDt = #" + FEndDt.ToString() + "#").Length <= 0)
                            {
                                _dtFreeCollection.Rows.Add(new Object[] { dr["USER_ID"], dr["DSPL_NME"], FStDt, FEndDt });
                                _dtFreeUserCollection.Rows.Add(new Object[] { dr["USER_ID"], dr["USER_ADID"], dr["DSPL_NME"], dr["FULL_NME"] });
                            }
                            bUserLoop = true;
                            sLoopUser = dr["USER_ID"].ToString();
                        }
                    }

                    sLoopUser = string.Empty;
                    bUserLoop = false;
                    Increment = Increment + siIncrement;
                    FStDt = StartDt;
                    FEndDt = FStDt;
                    FStDt = FStDt.AddMinutes(Increment);
                    FEndDt = FEndDt.AddMinutes(siDuration + Increment);
                }
            }

            dtTempWFCol.Clear();
            dt.Clear();

            dtTempWFCol = null;
            dt = null;
        }

        [HttpPost("GetOpenSlots")]
        public ActionResult<DataTable> GetOpenSlots([FromBody] DynamicParametersViewModel model)
        {
            LoadHashTable(model);

            _ProductID = Int32.Parse(htSlotData["Product"].ToString());

            if (_dtProductUser.Rows.Count <= 0)
                LoadInitialData();
            SetSkilledMemberObjectForEventType(_ProductID);
            CalculateEndTime(htSlotData["StTime"].ToString(), htSlotData["StHr"].ToString(), htSlotData["StMin"].ToString(), _duration);
            SetOpenSlotsCollection();

            return _dtOpenSlotsCollection;
        }

        protected void SetSkilledMemberObjectForEventType(int iProductID)
        {
            DataTable dt = null;
            DataTable dtAssignUsers = (iProductID == 3)
                ? _slotPickerRepo.GetAssignSkilledMembersforMPLS(htSlotData["MPLSEventType"].ToString(), htSlotData["IPVersion"].ToString())
                : ((iProductID == 5)
                    ? _slotPickerRepo.GetAssignSkilledMembersforMDS(ref dt, string.Empty, string.Empty, string.Empty, (List<LkMdsNtwkActyType>)htSlotData["MDSNtwkActy"])
                    : _slotPickerRepo.GetAssignSkilledMembersforEventType(iProductID.ToString()));

            // Updated by Sarah Sandoval [20220506] - Returned data columns names were updated from GetAssignSkilledMembersforMDS
            var x = (from au in dtAssignUsers.AsEnumerable()
                         //join pm in _dtProductUser.AsEnumerable() on Convert.ToString(au.Field<Object>("UserId").ToString()) equals Convert.ToString(pm.Field<Object>("USER_ID").ToString())
                     join pm in _dtProductUser.AsEnumerable() on Convert.ToString(au.Field<Object>("USER_ID").ToString()) equals Convert.ToString(pm.Field<Object>("USER_ID").ToString())
                     select new
                     {
                         USER_ID = au.Field<Object>("USER_ID").ToString(),
                         USER_ADID = au.Field<Object>("USER_ADID").ToString(),
                         DSPL_NME = au.Field<Object>("DSPL_NME").ToString(),
                         FULL_NME = au.Field<Object>("FULL_NME").ToString()
                         //USER_ID = au.Field<Object>("UserId").ToString(),
                         //USER_ADID = au.Field<Object>("UserAdid").ToString(),
                         //DSPL_NME = au.Field<Object>("DsplNme").ToString(),
                         //FULL_NME = au.Field<Object>("FullNme").ToString()
                     });
            if (x != null)
            {
                _dtSkilledMember = LinqHelper.CopyToDataTable(x, null, null);
            }
        }

        protected void SetOpenSlotsCollection()
        {
            try
            {
                DataTable dtWFBusyCol = new DataTable("WFbusyCollection");
                DataColumn[] wfbkeys = new DataColumn[3];

                wfbkeys[0] = new DataColumn("StartDt", typeof(DateTime));
                wfbkeys[1] = new DataColumn("EndDt", typeof(DateTime));
                wfbkeys[2] = new DataColumn("USER_ID");

                //wfbkeys[3] = new DataColumn("USER_ADID");
                dtWFBusyCol.Columns.Add(wfbkeys[0]);
                dtWFBusyCol.Columns.Add(wfbkeys[1]);
                dtWFBusyCol.Columns.Add(wfbkeys[2]);
                dtWFBusyCol.Columns.Add("USER_ADID");
                dtWFBusyCol.Columns.Add("DSPL_NME");
                dtWFBusyCol.Columns.Add("FULL_NME");
                dtWFBusyCol.Columns.Add("Flag");
                dtWFBusyCol.PrimaryKey = wfbkeys;

                DataTable dtTempWFBusyCol = new DataTable("TempWFbusyCollection");
                DataColumn[] twfbkeys = new DataColumn[4];

                twfbkeys[0] = new DataColumn("StartDt", typeof(DateTime));
                twfbkeys[1] = new DataColumn("EndDt", typeof(DateTime));
                twfbkeys[2] = new DataColumn("USER_ID");
                twfbkeys[3] = new DataColumn("USER_ADID");
                dtTempWFBusyCol.Columns.Add(twfbkeys[0]);
                dtTempWFBusyCol.Columns.Add(twfbkeys[1]);
                dtTempWFBusyCol.Columns.Add(twfbkeys[2]);
                dtTempWFBusyCol.Columns.Add(twfbkeys[3]);
                dtTempWFBusyCol.Columns.Add("DSPL_NME");
                dtTempWFBusyCol.Columns.Add("FULL_NME");
                dtTempWFBusyCol.PrimaryKey = twfbkeys;

                DataTable dtBusyCollection2 = new DataTable("BusyCollection2");
                DataColumn[] tbc2keys = new DataColumn[4];

                tbc2keys[0] = new DataColumn("StrtTmst", typeof(DateTime));
                tbc2keys[1] = new DataColumn("EndTmst", typeof(DateTime));
                tbc2keys[2] = new DataColumn("USER_ID");
                tbc2keys[3] = new DataColumn("USER_ADID");
                dtBusyCollection2.Columns.Add(tbc2keys[0]);
                dtBusyCollection2.Columns.Add(tbc2keys[1]);
                dtBusyCollection2.Columns.Add(tbc2keys[2]);
                dtBusyCollection2.Columns.Add(tbc2keys[3]);
                dtBusyCollection2.Columns.Add("DSPL_NME");
                dtBusyCollection2.Columns.Add("Flag");
                dtBusyCollection2.PrimaryKey = tbc2keys;

                DataTable dtTempBusyCollection = new DataTable("TempBusyCollection");
                DataColumn[] tbckeys = new DataColumn[4];

                tbckeys[0] = new DataColumn("StrtTmst", typeof(DateTime));
                tbckeys[1] = new DataColumn("EndTmst", typeof(DateTime));
                tbckeys[2] = new DataColumn("USER_ID");
                tbckeys[3] = new DataColumn("USER_ADID");
                dtTempBusyCollection.Columns.Add(tbckeys[0]);
                dtTempBusyCollection.Columns.Add(tbckeys[1]);
                dtTempBusyCollection.Columns.Add(tbckeys[2]);
                dtTempBusyCollection.Columns.Add(tbckeys[3]);
                dtTempBusyCollection.Columns.Add("DSPL_NME");
                dtTempBusyCollection.PrimaryKey = tbckeys;

                DataTable dtTempWFUsersNotBusy = new DataTable("TempWFNonbusyCollection");
                DataColumn[] twfnbkeys = new DataColumn[4];

                twfnbkeys[0] = new DataColumn("StartDt", typeof(DateTime));
                twfnbkeys[1] = new DataColumn("EndDt", typeof(DateTime));
                twfnbkeys[2] = new DataColumn("USER_ID");
                twfnbkeys[3] = new DataColumn("USER_ADID");
                dtTempWFUsersNotBusy.Columns.Add(twfnbkeys[0]);
                dtTempWFUsersNotBusy.Columns.Add(twfnbkeys[1]);
                dtTempWFUsersNotBusy.Columns.Add(twfnbkeys[2]);
                dtTempWFUsersNotBusy.Columns.Add(twfnbkeys[3]);
                dtTempWFUsersNotBusy.Columns.Add("DSPL_NME");
                dtTempWFUsersNotBusy.Columns.Add("FULL_NME");
                dtTempWFUsersNotBusy.PrimaryKey = twfnbkeys;

                DataColumn[] tmpkeys = new DataColumn[4];

                tmpkeys[0] = new DataColumn("StartDt", typeof(DateTime));
                tmpkeys[1] = new DataColumn("EndDt", typeof(DateTime));
                tmpkeys[2] = new DataColumn("USER_ID");
                tmpkeys[3] = new DataColumn("USER_ADID");

                DataTable dtTempOSWFCollection = new DataTable("TempOpenSlotsWFCollection");

                //dtTempOSWFCollection.Columns.Add("StartDt");
                //dtTempOSWFCollection.Columns.Add("EndDt");
                //dtTempOSWFCollection.Columns.Add("USER_ID");
                dtTempOSWFCollection.Columns.Add(tmpkeys[0]);
                dtTempOSWFCollection.Columns.Add(tmpkeys[1]);
                dtTempOSWFCollection.Columns.Add(tmpkeys[2]);
                dtTempOSWFCollection.Columns.Add(tmpkeys[3]);
                dtTempOSWFCollection.Columns.Add("DSPL_NME");
                dtTempOSWFCollection.Columns.Add("FULL_NME");
                dtTempOSWFCollection.PrimaryKey = tmpkeys;

                DateTime DCStart = _dtOrigSt; //_dtSt.Date.AddDays(_sOffSet) + new TimeSpan(0, 0, 0);
                DateTime DCEnd = _dtSt.Date.AddDays(2);//_dtSt.Date.AddDays(_eOffSet) + new TimeSpan(23, 55, 0);
                DateTime BusyStart = _dtSt.Date.Subtract(new TimeSpan(2, 0, 0, 0));//_dtSt.Date.Subtract(new TimeSpan(_eOffSet, 0, 0, 0)) + new TimeSpan(0, 0, 0);

                SetWFCollection(DCStart, DCEnd);

                SetFreeBusyCollection(BusyStart, DCEnd, (_dtOrigSt.Date + new TimeSpan(0, 0, 0)));

                //Bring all users from WF who have atleast one conflict BF Collection
                List<string> CommonWFBFUsers = (from wfc in _dtWFCollection.AsEnumerable()
                                                join bfc in _dtBusyCollection.AsEnumerable()
                                                on Convert.ToString(wfc.Field<Object>("USER_ID").ToString()) equals Convert.ToString(bfc.Field<Object>("USER_ID").ToString())
                                                where ((((DateTime.Parse(wfc.Field<Object>("StartDt").ToString()).Date == _dtSt.Date)
                                                        || ((DateTime.Parse(wfc.Field<Object>("EndDt").ToString()) > _dtSt)
                                                            && (DateTime.Parse(wfc.Field<Object>("StartDt").ToString()).Date == _dtSt.AddDays(-1).Date)))
                                                    && ((DateTime.Parse(bfc.Field<Object>("StrtTmst").ToString()).Date == _dtSt.Date)
                                                        || ((DateTime.Parse(bfc.Field<Object>("EndTmst").ToString()) > _dtSt)
                                                            && (DateTime.Parse(bfc.Field<Object>("StrtTmst").ToString()).Date == _dtSt.AddDays(-1).Date))))
                                                ||
                                                    (((DateTime.Parse(wfc.Field<Object>("StartDt").ToString()) >= DateTime.Parse(bfc.Field<Object>("StrtTmst").ToString()))
                                                    && (DateTime.Parse(wfc.Field<Object>("EndDt").ToString()) <= DateTime.Parse(bfc.Field<Object>("EndTmst").ToString())))))

                                                //&& (!((DateTime.Parse(wfc.Field<Object>("StartDt").ToString()) < DateTime.Parse(bfc.Field<Object>("StrtTmst").ToString()))
                                                // && (DateTime.Parse(wfc.Field<Object>("EndDt").ToString()) < DateTime.Parse(bfc.Field<Object>("StrtTmst").ToString()))))
                                                select Convert.ToString(wfc.Field<Object>("USER_ID").ToString())
                                                    ).Distinct().ToList<string>();

                foreach (DataRow dr in _dtBusyCollection.Rows)
                {
                    if (dtBusyCollection2.Select("StrtTmst = #" + dr[0].ToString() + "# AND EndTmst = #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "'").Length <= 0)
                        dtBusyCollection2.Rows.Add(new Object[] { Convert.ToDateTime(dr[0]), Convert.ToDateTime(dr[1]), dr[2], dr[3], dr[4], "0" });
                }

                //merge all the busy collection before applying split logic..
                var sortBCByUserEndTm = (from dwfbc in dtBusyCollection2.AsEnumerable()
                                             //where Convert.ToString(dwfbc.Field<Object>("USER_ID").ToString()).Equals("277")
                                         select dwfbc)
                                         .OrderBy(u => Convert.ToString(u.Field<Object>("USER_ID").ToString()))
                                         .ThenByDescending(et => DateTime.Parse(et.Field<Object>("EndTmst").ToString()))
                                         .ThenByDescending(st => DateTime.Parse(st.Field<Object>("StrtTmst").ToString()));

                string EndDt = string.Empty;
                string UserID = string.Empty;
                bool bInsert = false;
                bool bFinalTm = false;

                foreach (DataRow drs in sortBCByUserEndTm)
                {
                    if (!(drs["USER_ID"].ToString().Equals(UserID)))
                    {
                        bFinalTm = true;
                        EndDt = drs["EndTmst"].ToString();
                        UserID = drs["USER_ID"].ToString();
                    }
                    //else if (drs["EndTmst"].ToString().Equals(EndDt))
                    //{
                    //    if (bInsert)
                    //        drs["Flag"] = "1";
                    //}
                    else if (!(drs["EndTmst"].ToString().Equals(EndDt)))
                    {
                        bFinalTm = false;
                        EndDt = drs["EndTmst"].ToString();
                    }
                    //if (drs["Flag"].ToString().Equals("0"))
                    //{
                    bInsert = false;
                    if ((dtBusyCollection2.Select("EndTmst = #" + drs["EndTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length > 1) && (bFinalTm))
                    {
                        if ((dtTempBusyCollection.Select("StrtTmst <= #" + drs["EndTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length >= 1)
                            && (dtTempBusyCollection.Select("StrtTmst >= #" + drs["StrtTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length >= 1))
                        {
                            var del = (from delold in dtTempBusyCollection.AsEnumerable()
                                       where (Convert.ToString(delold.Field<Object>("USER_ID").ToString()).Equals(drs["USER_ID"].ToString()))
                                          && (((DateTime.Parse(delold.Field<Object>("StrtTmst").ToString())) <= DateTime.Parse(drs["EndTmst"].ToString()))
                                             && ((DateTime.Parse(delold.Field<Object>("StrtTmst").ToString())) >= DateTime.Parse(drs["StrtTmst"].ToString())))
                                       select delold).OrderBy(et => DateTime.Parse(et.Field<Object>("EndTmst").ToString())).ToList();

                            DateTime finalEDT = new DateTime();
                            foreach (DataRow item in del)
                            {
                                finalEDT = Convert.ToDateTime(item["EndTmst"]);
                                dtTempBusyCollection.Rows.Remove(dtTempBusyCollection.Rows.Find(new Object[] { item["StrtTmst"], item["EndTmst"], item["USER_ID"], item["USER_ADID"] }));
                            }

                            dtTempBusyCollection.Rows.Add(new Object[] { Convert.ToDateTime(drs[0]), finalEDT, drs[2], drs[3], drs[4] });
                            bInsert = true;
                        }
                        else if (dtTempBusyCollection.Select("StrtTmst = #" + drs[0].ToString() + "# AND EndTmst = #" + drs[1].ToString() + "# AND USER_ID = '" + drs[2].ToString() + "'").Length <= 0)
                        {
                            dtTempBusyCollection.Rows.Add(new Object[] { Convert.ToDateTime(drs[0]), Convert.ToDateTime(drs[1]), drs[2], drs[3], drs[4] });
                            bInsert = true;
                        }
                    }
                    //else if ((dtBusyCollection2.Select("EndTmst = #" + drs["EndTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length > 1) && (!bFinalTm))
                    //{
                    //    var stTime = (from picksttop in dtBusyCollection2.AsEnumerable()
                    //                  where (Convert.ToString(picksttop.Field<Object>("USER_ID").ToString()).Equals(drs["USER_ID"].ToString()))
                    //                     && ((DateTime.Parse(picksttop.Field<Object>("EndTmst").ToString())) == DateTime.Parse(drs["EndTmst"].ToString()))
                    //                     && ((DateTime.Parse(picksttop.Field<Object>("StrtTmst").ToString())) < DateTime.Parse(drs["EndTmst"].ToString()))
                    //                  select picksttop).OrderByDescending(st => DateTime.Parse(st.Field<Object>("StrtTmst").ToString())).ToList();

                    //    if ((stTime.Count > 0) && (dtTempBusyCollection.Select("StrtTmst = #" + stTime[0]["StrtTmst"].ToString() + "# AND EndTmst = #" + drs[1].ToString() + "# AND USER_ID = '" + drs[2].ToString() + "'").Length <= 0))
                    //    {
                    //        dtTempBusyCollection.Rows.Add(new Object[] { Convert.ToDateTime(stTime[0]["StrtTmst"]), Convert.ToDateTime(drs[1]), drs[2], drs[3], drs[4] });
                    //        bInsert = true;
                    //    }
                    //}
                    else if ((dtBusyCollection2.Select("EndTmst = #" + drs["EndTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length >= 1) && (!bFinalTm))
                    {
                        //Overlapping Schedules, For Ex :- already in tempbusy = 1p-4p; incoming busycoll = 12p-2p, merge them to 12p-4p
                        if ((dtTempBusyCollection.Select("StrtTmst <= #" + drs["EndTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length >= 1)
                            && (dtTempBusyCollection.Select("EndTmst >= #" + drs["StrtTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length >= 1))
                        //if ((dtTempBusyCollection.Select("StrtTmst <= #" + drs["EndTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length >= 1)
                        //    && (dtTempBusyCollection.Select("StrtTmst >= #" + drs["StrtTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length >= 1))
                        {
                            var del = (from delold in dtTempBusyCollection.AsEnumerable()
                                       where (Convert.ToString(delold.Field<Object>("USER_ID").ToString()).Equals(drs["USER_ID"].ToString()))
                                          && (((DateTime.Parse(delold.Field<Object>("StrtTmst").ToString())) <= DateTime.Parse(drs["EndTmst"].ToString()))
                                             && ((DateTime.Parse(delold.Field<Object>("StrtTmst").ToString())) >= DateTime.Parse(drs["StrtTmst"].ToString())))
                                       select delold).OrderBy(et => DateTime.Parse(et.Field<Object>("EndTmst").ToString())).ToList();

                            DateTime finalEDT = new DateTime();
                            foreach (DataRow item in del)
                            {
                                finalEDT = Convert.ToDateTime(item["EndTmst"]);
                                dtTempBusyCollection.Rows.Remove(dtTempBusyCollection.Rows.Find(new Object[] { item["StrtTmst"], item["EndTmst"], item["USER_ID"], item["USER_ADID"] }));
                            }

                            dtTempBusyCollection.Rows.Add(new Object[] { Convert.ToDateTime(drs[0]), finalEDT, drs[2], drs[3], drs[4] });
                            bInsert = true;
                        }
                        else //if not overlapping
                        {
                            var stTime = (from picksttop in dtBusyCollection2.AsEnumerable()
                                          where (Convert.ToString(picksttop.Field<Object>("USER_ID").ToString()).Equals(drs["USER_ID"].ToString()))
                                             && ((DateTime.Parse(picksttop.Field<Object>("EndTmst").ToString())) == DateTime.Parse(drs["EndTmst"].ToString()))
                                             && ((DateTime.Parse(picksttop.Field<Object>("StrtTmst").ToString())) < DateTime.Parse(drs["EndTmst"].ToString()))
                                          select picksttop).OrderByDescending(st => DateTime.Parse(st.Field<Object>("StrtTmst").ToString())).ToList();

                            if ((stTime.Count > 0) && (dtTempBusyCollection.Select("StrtTmst = #" + stTime[0]["StrtTmst"].ToString() + "# AND EndTmst = #" + drs[1].ToString() + "# AND USER_ID = '" + drs[2].ToString() + "'").Length <= 0))
                            {
                                dtTempBusyCollection.Rows.Add(new Object[] { Convert.ToDateTime(stTime[0]["StrtTmst"]), Convert.ToDateTime(drs[1]), drs[2], drs[3], drs[4] });
                                bInsert = true;
                            }
                            //if (dtTempBusyCollection.Select("StrtTmst = #" + drs[0].ToString() + "# AND EndTmst = #" + drs[1].ToString() + "# AND USER_ID = '" + drs[2].ToString() + "'").Length <= 0)
                            //{
                            //    dtTempBusyCollection.Rows.Add(new Object[] { Convert.ToDateTime(drs[0]), Convert.ToDateTime(drs[1]), drs[2], drs[3], drs[4] });
                            //    bInsert = true;
                            //}
                        }
                    }
                    else if ((dtBusyCollection2.Select("EndTmst = #" + drs["EndTmst"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length == 1) && (bFinalTm))
                    {
                        if (dtTempBusyCollection.Select("StrtTmst = #" + drs[0].ToString() + "# AND EndTmst = #" + drs[1].ToString() + "# AND USER_ID = '" + drs[2].ToString() + "'").Length <= 0)
                        {
                            dtTempBusyCollection.Rows.Add(new Object[] { Convert.ToDateTime(drs[0]), Convert.ToDateTime(drs[1]), drs[2], drs[3], drs[4] });
                            bInsert = true;
                        }
                    }
                    //}
                }

                //Cleanup Same Start/End and EOD slots, Also remove the slots whose starttime is same and endtime > other slots
                foreach (DataRow dr in dtTempBusyCollection.Rows)
                {
                    var stTime = (from picksttop in dtTempBusyCollection.AsEnumerable()
                                  where (Convert.ToString(picksttop.Field<Object>("USER_ID").ToString()).Equals(dr["USER_ID"].ToString()))
                                     && (((DateTime.Parse(picksttop.Field<Object>("StrtTmst").ToString())) == DateTime.Parse(dr["StrtTmst"].ToString()))
                                     && (((DateTime.Parse(picksttop.Field<Object>("EndTmst").ToString())) < DateTime.Parse(dr["EndTmst"].ToString()))))
                                  select picksttop).ToList();

                    if (stTime.Count > 0)
                    {
                        if (dtTempOSWFCollection.Select("StartDt = #" + dr[0].ToString() + "# AND EndDt = #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "'").Length <= 0)
                            dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(dr[0]), Convert.ToDateTime(dr[1]), dr[2], dr[3], dr[4], string.Empty });
                    }

                    if ((DateTime.Parse(dr[0].ToString()) == DateTime.Parse(dr[1].ToString()))
                        || ((DateTime.Parse(dr[0].ToString()).Date == _dtSt.Date)
                            && (DateTime.Parse(dr[0].ToString()).ToString("t").Equals("11:59 PM"))))
                    {
                        if (dtTempOSWFCollection.Select("StartDt = #" + dr[0].ToString() + "# AND EndDt = #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "'").Length <= 0)
                            dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(dr[0]), Convert.ToDateTime(dr[1]), dr[2], dr[3], dr[4], string.Empty });
                    }
                }

                if ((dtTempOSWFCollection != null) && (dtTempOSWFCollection.Rows.Count > 0))
                {
                    foreach (DataRow dr in dtTempOSWFCollection.Rows)
                    {
                        dtTempBusyCollection.Rows.Remove(dtTempBusyCollection.Rows.Find(new Object[] { dr["StartDt"], dr["EndDt"], dr["USER_ID"], dr["USER_ADID"] }));
                    }
                }

                EndDt = string.Empty;
                UserID = string.Empty;
                bInsert = false;
                bFinalTm = false;
                dtTempOSWFCollection.Clear();

                //Split WF collection using BF Slots
                var WFBusyCol = (from wfc in _dtWFCollection.AsEnumerable()
                                 join bfc in dtTempBusyCollection.AsEnumerable() on Convert.ToString(wfc.Field<Object>("USER_ID").ToString()) equals Convert.ToString(bfc.Field<Object>("USER_ID").ToString())
                                 where (((DateTime.Parse(wfc.Field<Object>("EndDt").ToString()).Date == DateTime.Parse(bfc.Field<Object>("StrtTmst").ToString()).Date)
                                 || (DateTime.Parse(wfc.Field<Object>("EndDt").ToString()).AddDays(-1).Date == DateTime.Parse(bfc.Field<Object>("StrtTmst").ToString()).Date))
                                 && (CommonWFBFUsers.Contains(Convert.ToString(wfc.Field<Object>("USER_ID").ToString()))))

                                 //&& (Convert.ToString(wfc.Field<Object>("USER_ID").ToString()).Equals("212"))
                                 select new
                                 {
                                     Slot = SplitSlotsOverLap(wfc, bfc)
                                 }).Distinct().ToList();

                //DataTable dt11 = LinqHelper.CopyToDataTable(WFBusyCol, null, null);

                //Merge all adjacent timeslots
                foreach (var wfbc in WFBusyCol)
                {
                    foreach (Object[] obj in wfbc.Slot)
                    {
                        DataRow[] drPrefix = dtWFBusyCol.Select("EndDt = #" + obj[0].ToString() + "# AND USER_ID = '" + obj[2].ToString() + "'");
                        DataRow[] drSuffix = dtWFBusyCol.Select("StartDt = #" + obj[1].ToString() + "# AND USER_ID = '" + obj[2].ToString() + "'");

                        if (drPrefix.Length == 1)
                        {
                            if (dtTempOSWFCollection.Select("StartDt = #" + drPrefix[0][0].ToString() + "# AND EndDt = #" + drPrefix[0][1].ToString() + "# AND USER_ID = '" + drPrefix[0][2].ToString() + "'").Length <= 0)
                                dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(drPrefix[0][0]), Convert.ToDateTime(drPrefix[0][1]), drPrefix[0][2], drPrefix[0][3], drPrefix[0][4], drPrefix[0][5] });
                            if (dtWFBusyCol.Select("StartDt = #" + drPrefix[0]["StartDt"].ToString() + "# AND EndDt = #" + obj[1].ToString() + "# AND USER_ID = '" + obj[2].ToString() + "'").Length <= 0)
                                dtWFBusyCol.Rows.Add(new Object[] { Convert.ToDateTime(drPrefix[0]["StartDt"].ToString()), Convert.ToDateTime(obj[1]), obj[2], obj[3], obj[4], obj[5], "0" });
                        }
                        else if (drSuffix.Length == 1)
                        {
                            if (dtTempOSWFCollection.Select("StartDt = #" + drSuffix[0][0].ToString() + "# AND EndDt = #" + drSuffix[0][1].ToString() + "# AND USER_ID = '" + drSuffix[0][2].ToString() + "'").Length <= 0)
                                dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(drSuffix[0][0]), Convert.ToDateTime(drSuffix[0][1]), drSuffix[0][2], drSuffix[0][3], drSuffix[0][4], drSuffix[0][5] });
                            if (dtWFBusyCol.Select("StartDt = #" + obj[0].ToString() + "# AND EndDt = #" + drSuffix[0]["EndDt"].ToString() + "# AND USER_ID = '" + obj[2].ToString() + "'").Length <= 0)
                                dtWFBusyCol.Rows.Add(new Object[] { Convert.ToDateTime(obj[0]), Convert.ToDateTime(drSuffix[0]["EndDt"]), obj[2], obj[3], obj[4], obj[5], "0" });
                        }
                        else if (dtWFBusyCol.Select("StartDt = #" + obj[0].ToString() + "# AND EndDt = #" + obj[1].ToString() + "# AND USER_ID = '" + obj[2].ToString() + "'").Length <= 0)
                            dtWFBusyCol.Rows.Add(new Object[] { Convert.ToDateTime(obj[0]), Convert.ToDateTime(obj[1]), obj[2], obj[3], obj[4], obj[5], "0" });

                    }
                }

                if ((dtTempOSWFCollection != null) && (dtTempOSWFCollection.Rows.Count > 0))
                {
                    foreach (DataRow dr in dtTempOSWFCollection.Rows)
                    {
                        dtWFBusyCol.Rows.Remove(dtWFBusyCol.Rows.Find(new Object[] { dr["StartDt"], dr["EndDt"], dr["USER_ID"] }));
                    }
                }

                dtTempOSWFCollection.Clear();

                //delete overlapping slots, ex: wf 10-6p, bf 10-12, 12-4, 4-6. remv entire rec
                List<ConflictSlotData> licd = new List<ConflictSlotData>();

                foreach (DataRow dr in dtWFBusyCol.Rows)
                {
                    if (dtWFBusyCol.Select("StartDt > #" + dr[0].ToString() + "# AND EndDt > #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "' AND StartDt < #" + dr[1].ToString() + "#").Length > 0)
                    {
                        licd.Add(new ConflictSlotData(dr[2].ToString(), DateTime.Parse((dtWFBusyCol.Select("StartDt > #" + dr[0].ToString() + "# AND EndDt > #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "' AND StartDt < #" + dr[1].ToString() + "#")[0][0]).ToString()),
                                                DateTime.Parse(dr[1].ToString()), Convert.ToDateTime(dr[0]), Convert.ToDateTime(dtWFBusyCol.Select("StartDt > #" + dr[0].ToString() + "# AND EndDt > #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "' AND StartDt < #" + dr[1].ToString() + "#")[0][1]), DateTime.Parse(dr[0].ToString()), DateTime.Parse(dr[1].ToString())));
                    }
                    else if (dtWFBusyCol.Select("StartDt < #" + dr[0].ToString() + "# AND EndDt < #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "' AND EndDt > #" + dr[0].ToString() + "#").Length > 0)
                    {
                        licd.Add(new ConflictSlotData(dr[2].ToString(), DateTime.Parse(dr[0].ToString()), DateTime.Parse((dtWFBusyCol.Select("StartDt < #" + dr[0].ToString() + "# AND EndDt < #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "' AND EndDt > #" + dr[0].ToString() + "#")[0][1]).ToString()),
                                                        Convert.ToDateTime(dtWFBusyCol.Select("StartDt < #" + dr[0].ToString() + "# AND EndDt < #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "' AND EndDt > #" + dr[0].ToString() + "#")[0][0]), Convert.ToDateTime(dr[1]), DateTime.Parse(dr[0].ToString()), DateTime.Parse(dr[1].ToString())));
                    }
                }

                foreach (ConflictSlotData csi in licd)
                {
                    if (CheckBusyConflict(csi.BusyStartDt, csi.BusyEndDt, csi.UserID))
                    {
                        if (_dtWFCollection.Select("USER_ID = '" + csi.UserID + "' AND StartDt = #" + csi.WFStartDt.ToString() + "# AND EndDt = #" + csi.WFEndDt.ToString() + "#").Length > 0)
                        {
                            dtWFBusyCol.Rows.Remove(dtWFBusyCol.Rows.Find(new Object[] { csi.ActStartDt, csi.ActEndDt, csi.UserID }));
                        }
                    }
                }

                //After applying WF and Busy Collection splitting, the generated slots needs to be consolidated
                var sortWFByUserEndTm = (from dwfbc in dtWFBusyCol.AsEnumerable()

                                             //where Convert.ToString(dwfbc.Field<Object>("USER_ID").ToString()).Equals("276")
                                         select dwfbc)
                                         .OrderBy(u => Convert.ToString(u.Field<Object>("USER_ID").ToString()))
                                         .ThenByDescending(et => DateTime.Parse(et.Field<Object>("EndDt").ToString()))
                                         .ThenByDescending(st => DateTime.Parse(st.Field<Object>("StartDt").ToString()));

                foreach (DataRow drs in sortWFByUserEndTm)
                {
                    if (!(drs["USER_ID"].ToString().Equals(UserID)))
                    {
                        bFinalTm = true;
                        EndDt = drs["EndDt"].ToString();
                        UserID = drs["USER_ID"].ToString();
                    }
                    else if (drs["EndDt"].ToString().Equals(EndDt))
                    {
                        if (bInsert)
                            drs["Flag"] = "1";
                    }
                    else if (!(drs["EndDt"].ToString().Equals(EndDt)))
                    {
                        bFinalTm = false;
                        EndDt = drs["EndDt"].ToString();
                    }
                    if (drs["Flag"].ToString().Equals("0"))
                    {
                        bInsert = false;
                        if ((dtWFBusyCol.Select("EndDt = #" + drs["EndDt"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length > 1) && (bFinalTm))
                        {
                            if (dtTempWFBusyCol.Select("StartDt = #" + drs[0].ToString() + "# AND EndDt = #" + drs[1].ToString() + "# AND USER_ID = '" + drs[2].ToString() + "'").Length <= 0)
                            {
                                dtTempWFBusyCol.Rows.Add(new Object[] { Convert.ToDateTime(drs[0]), Convert.ToDateTime(drs[1]), drs[2], drs[3], drs[4], drs[5] });
                                bInsert = true;
                            }
                        }
                        else if ((dtWFBusyCol.Select("EndDt = #" + drs["EndDt"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length == 1) && (!bFinalTm))
                        {
                            var stTime = (from picksttop in dtWFBusyCol.AsEnumerable()
                                          where (Convert.ToString(picksttop.Field<Object>("USER_ID").ToString()).Equals(drs["USER_ID"].ToString()))
                                             && ((DateTime.Parse(picksttop.Field<Object>("StartDt").ToString())) < DateTime.Parse(drs["EndDt"].ToString()))
                                          select picksttop).OrderByDescending(st => DateTime.Parse(st.Field<Object>("StartDt").ToString())).ToList();

                            if ((stTime.Count > 0) && (dtTempWFBusyCol.Select("StartDt = #" + stTime[0]["StartDt"].ToString() + "# AND EndDt = #" + drs[1].ToString() + "# AND USER_ID = '" + drs[2].ToString() + "'").Length <= 0))
                            {
                                dtTempWFBusyCol.Rows.Add(new Object[] { Convert.ToDateTime(stTime[0]["StartDt"]), Convert.ToDateTime(drs[1]), drs[2], drs[3], drs[4], drs[5] });
                                bInsert = true;
                            }
                        }
                        else if ((dtWFBusyCol.Select("EndDt = #" + drs["EndDt"].ToString() + "# AND USER_ID = '" + drs["USER_ID"].ToString() + "'").Length == 1) && (bFinalTm))
                        {
                            if (dtTempWFBusyCol.Select("StartDt = #" + drs[0].ToString() + "# AND EndDt = #" + drs[1].ToString() + "# AND USER_ID = '" + drs[2].ToString() + "'").Length <= 0)
                            {
                                dtTempWFBusyCol.Rows.Add(new Object[] { Convert.ToDateTime(drs[0]), Convert.ToDateTime(drs[1]), drs[2], drs[3], drs[4], drs[5] });
                                bInsert = true;
                            }
                        }
                    }
                }

                //Cleanup Same Start/End and EOD slots, Also remove the slots whose starttime is same and endtime > other slots
                foreach (DataRow dr in dtTempWFBusyCol.Rows)
                {
                    var stTime = (from picksttop in dtTempWFBusyCol.AsEnumerable()
                                  where (Convert.ToString(picksttop.Field<Object>("USER_ID").ToString()).Equals(dr["USER_ID"].ToString()))
                                     && (((DateTime.Parse(picksttop.Field<Object>("StartDt").ToString())) == DateTime.Parse(dr["StartDt"].ToString()))
                                     && (((DateTime.Parse(picksttop.Field<Object>("EndDt").ToString())) < DateTime.Parse(dr["EndDt"].ToString()))))
                                  select picksttop).ToList();

                    if (stTime.Count > 0)
                    {
                        if (dtTempOSWFCollection.Select("StartDt = #" + dr[0].ToString() + "# AND EndDt = #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "'").Length <= 0)
                            dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(dr[0]), Convert.ToDateTime(dr[1]), dr[2], dr[3], dr[4], dr[5] });
                    }

                    if ((DateTime.Parse(dr[0].ToString()) == DateTime.Parse(dr[1].ToString()))
                        || ((DateTime.Parse(dr[0].ToString()).Date == _dtSt.Date)
                            && (DateTime.Parse(dr[0].ToString()).ToString("t").Equals("11:59 PM"))))
                    {
                        if (dtTempOSWFCollection.Select("StartDt = #" + dr[0].ToString() + "# AND EndDt = #" + dr[1].ToString() + "# AND USER_ID = '" + dr[2].ToString() + "'").Length <= 0)
                            dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(dr[0]), Convert.ToDateTime(dr[1]), dr[2], dr[3], dr[4], dr[5] });
                    }
                }

                if ((dtTempOSWFCollection != null) && (dtTempOSWFCollection.Rows.Count > 0))
                {
                    foreach (DataRow dr in dtTempOSWFCollection.Rows)
                    {
                        dtTempWFBusyCol.Rows.Remove(dtTempWFBusyCol.Rows.Find(new Object[] { dr["StartDt"], dr["EndDt"], dr["USER_ID"], dr["USER_ADID"] }));
                    }
                }

                dtTempOSWFCollection.Clear();

                //Only WF Data for Users with no Busy collection
                var WFUsersNotBusy = (from wfc in _dtWFCollection.AsEnumerable()
                                      where ((DateTime.Parse(wfc.Field<Object>("StartDt").ToString()).Date == _dtSt.Date)
                        || ((DateTime.Parse(wfc.Field<Object>("EndDt").ToString()).Date == _dtSt.Date)
                           && (DateTime.Parse(wfc.Field<Object>("StartDt").ToString()).Date == _dtSt.AddDays(-1).Date)))
                           && (!(CommonWFBFUsers.Contains(Convert.ToString(wfc.Field<Object>("USER_ID").ToString()))))
                                      select wfc).Distinct().OrderBy(ob => Convert.ToString(ob.Field<Object>("FULL_NME").ToString())).ToList();

                //Merge all adjacent timeslots
                foreach (var drWFU in WFUsersNotBusy)
                {
                    DataRow[] drPrefix = dtTempWFUsersNotBusy.Select("EndDt = #" + drWFU[0].ToString() + "# AND USER_ID = '" + drWFU[2].ToString() + "'");
                    DataRow[] drSuffix = dtTempWFUsersNotBusy.Select("StartDt = #" + drWFU[1].ToString() + "# AND USER_ID = '" + drWFU[2].ToString() + "'");

                    if (drPrefix.Length == 1)
                    {
                        if (dtTempOSWFCollection.Select("StartDt = #" + drPrefix[0][0].ToString() + "# AND EndDt = #" + drPrefix[0][1].ToString() + "# AND USER_ID = '" + drPrefix[0][2].ToString() + "'").Length <= 0)
                            dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(drPrefix[0][0]), Convert.ToDateTime(drPrefix[0][1]), drPrefix[0][2], drPrefix[0][3], drPrefix[0][4], drPrefix[0][5] });
                        if (dtTempWFUsersNotBusy.Select("StartDt = #" + drPrefix[0]["StartDt"].ToString() + "# AND EndDt = #" + drWFU[1].ToString() + "# AND USER_ID = '" + drWFU[2].ToString() + "'").Length <= 0)
                            dtTempWFUsersNotBusy.Rows.Add(new Object[] { Convert.ToDateTime(drPrefix[0]["StartDt"].ToString()), Convert.ToDateTime(drWFU[1]), drWFU[2], drWFU[3], drWFU[4], drWFU[5] });
                    }
                    else if (drSuffix.Length == 1)
                    {
                        if (dtTempOSWFCollection.Select("StartDt = #" + drSuffix[0][0].ToString() + "# AND EndDt = #" + drSuffix[0][1].ToString() + "# AND USER_ID = '" + drSuffix[0][2].ToString() + "'").Length <= 0)
                            dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(drSuffix[0][0]), Convert.ToDateTime(drSuffix[0][1]), drSuffix[0][2], drSuffix[0][3], drSuffix[0][4], drSuffix[0][5] });
                        if (dtTempWFUsersNotBusy.Select("StartDt = #" + drWFU[0].ToString() + "# AND EndDt = #" + drSuffix[0]["EndDt"].ToString() + "# AND USER_ID = '" + drWFU[2].ToString() + "'").Length <= 0)
                            dtTempWFUsersNotBusy.Rows.Add(new Object[] { Convert.ToDateTime(drWFU[0]), Convert.ToDateTime(drSuffix[0]["EndDt"]), drWFU[2], drWFU[3], drWFU[4], drWFU[5] });
                    }
                    else if (dtTempWFUsersNotBusy.Select("StartDt = #" + drWFU[0].ToString() + "# AND EndDt = #" + drWFU[1].ToString() + "# AND USER_ID = '" + drWFU[2].ToString() + "'").Length <= 0)
                        dtTempWFUsersNotBusy.Rows.Add(new Object[] { Convert.ToDateTime(drWFU[0]), Convert.ToDateTime(drWFU[1]), drWFU[2], drWFU[3], drWFU[4], drWFU[5] });

                    //Cleanup Same Start/End and EOD slots
                    if ((DateTime.Parse(drWFU[0].ToString()) == DateTime.Parse(drWFU[1].ToString()))
                        || ((DateTime.Parse(drWFU[0].ToString()).Date == _dtSt.Date)
                            && (DateTime.Parse(drWFU[0].ToString()).ToString("t").Equals("11:59 PM"))))
                    {
                        if (dtTempOSWFCollection.Select("StartDt = #" + drWFU[0].ToString() + "# AND EndDt = #" + drWFU[1].ToString() + "# AND USER_ID = '" + drWFU[2].ToString() + "'").Length <= 0)
                            dtTempOSWFCollection.Rows.Add(new Object[] { Convert.ToDateTime(drWFU[0]), Convert.ToDateTime(drWFU[1]), drWFU[2], drWFU[3], drWFU[4], drWFU[5] });
                    }
                }

                if ((dtTempOSWFCollection != null) && (dtTempOSWFCollection.Rows.Count > 0))
                {
                    foreach (DataRow dr in dtTempOSWFCollection.Rows)
                    {
                        dtTempWFUsersNotBusy.Rows.Remove(dtTempWFUsersNotBusy.Rows.Find(new Object[] { dr["StartDt"], dr["EndDt"], dr["USER_ID"], dr["USER_ADID"] }));
                    }
                }

                dtTempWFUsersNotBusy.AsEnumerable().CopyToDataTable(dtTempWFBusyCol, LoadOption.PreserveChanges);

                var z = (from sortTime in dtTempWFBusyCol.AsEnumerable()
                         select new
                         {
                             USER_ID = sortTime.Field<Object>("USER_ID").ToString(),
                             FULL_NME = sortTime.Field<Object>("FULL_NME").ToString(),
                             StartDt = sortTime.Field<Object>("StartDt").ToString(),
                             EndDt = sortTime.Field<Object>("EndDt").ToString()
                         }).Distinct().OrderBy(ob => ob.FULL_NME).ThenBy(ts => DateTime.Parse(ts.StartDt));

                DataTable dtFinal = LinqHelper.CopyToDataTable(z, null, null);

                var y = (from fc in dtFinal.AsEnumerable()
                         where ((DateTime.Parse(fc.Field<Object>("StartDt").ToString()).Date == _dtSt.Date)
                            || ((DateTime.Parse(fc.Field<Object>("EndDt").ToString()).Date == _dtSt.Date)
                               && (DateTime.Parse(fc.Field<Object>("StartDt").ToString()).Date == _dtSt.AddDays(-1).Date)))
                         select new
                         {
                             USER_ID = fc.Field<Object>("USER_ID").ToString(),
                             FULL_NME = fc.Field<Object>("FULL_NME").ToString(),
                             TimeSlot = BuildTimeSlot(fc)
                         }).Distinct().OrderBy(ob => ob.FULL_NME);//.ThenBy(ts => ts.TimeSlot);

                _dtOpenSlotsCollection = LinqHelper.CopyToDataTable(y, _dtOpenSlotsCollection, null);

                dtTempOSWFCollection.Clear();
                dtTempWFBusyCol.Clear();
                dtTempWFUsersNotBusy.Clear();
                dtFinal.Clear();

                dtTempOSWFCollection = null;
                dtTempWFBusyCol = null;
                dtTempWFUsersNotBusy = null;
                dtFinal = null;
            }
            catch (Exception ex)
            {
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace + " ; " + ex.Source, string.Empty);
            }
        }

        private List<Object[]> SplitSlotsOverLap(DataRow drWF, DataRow drBF)
        {
            List<Object[]> liOut = new List<object[]>();

            if ((DateTime.Parse(drWF["StartDt"].ToString()) < DateTime.Parse(drBF["StrtTmst"].ToString()))
                                    && (DateTime.Parse(drWF["EndDt"].ToString()) > DateTime.Parse(drBF["EndTmst"].ToString()))
                                    && (DateTime.Parse(drBF["StrtTmst"].ToString()) != DateTime.Parse(drBF["EndTmst"].ToString()))
                                    && (DateTime.Parse(drBF["StrtTmst"].ToString()) < DateTime.Parse(drBF["EndTmst"].ToString()))
                                    && ((DateTime.Parse(drBF["StrtTmst"].ToString()).Date == _dtSt.Date)
                                            || (DateTime.Parse(drBF["EndTmst"].ToString()).Date == _dtSt.Date))
                                    && ((DateTime.Parse(drWF["StartDt"].ToString()).Date == (DateTime.Parse(drBF["StrtTmst"].ToString()).Date))
                                       || (DateTime.Parse(drWF["StartDt"].ToString()).Date == (DateTime.Parse(drWF["EndDt"].ToString()).AddDays(-1).Date))))//x->-<-y
            {
                if (!(CheckBusyConflict(drWF["StartDt"], drBF["StrtTmst"], drWF["USER_ID"])))
                    liOut.Add(new Object[] { drWF["StartDt"], drBF["StrtTmst"], drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
                if (!(CheckBusyConflict(drBF["EndTmst"], drWF["EndDt"], drWF["USER_ID"])))
                    liOut.Add(new Object[] { drBF["EndTmst"], drWF["EndDt"], drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
            }
            else if ((DateTime.Parse(drWF["StartDt"].ToString()) == DateTime.Parse(drBF["StrtTmst"].ToString()))
                && (DateTime.Parse(drWF["EndDt"].ToString()) > DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drBF["StrtTmst"].ToString()) != DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drBF["StrtTmst"].ToString()) < DateTime.Parse(drBF["EndTmst"].ToString()))
                && ((DateTime.Parse(drBF["StrtTmst"].ToString()).Date == _dtSt.Date)
                        || (DateTime.Parse(drBF["EndTmst"].ToString()).Date == _dtSt.Date))
                && (DateTime.Parse(drWF["StartDt"].ToString()).Date == (DateTime.Parse(drWF["EndDt"].ToString()).AddDays(-1).Date)))//x=->-<-y
            {
                if (!(CheckBusyConflict(drBF["EndTmst"], drWF["EndDt"], drWF["USER_ID"])))
                    liOut.Add(new Object[] { drBF["EndTmst"], drWF["EndDt"], drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
            }
            else if ((DateTime.Parse(drWF["StartDt"].ToString()) < DateTime.Parse(drBF["StrtTmst"].ToString()))
                && (DateTime.Parse(drWF["EndDt"].ToString()) == DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drBF["StrtTmst"].ToString()) != DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drBF["StrtTmst"].ToString()) < DateTime.Parse(drBF["EndTmst"].ToString()))
                && ((DateTime.Parse(drBF["StrtTmst"].ToString()).Date == _dtSt.Date)
                        || (DateTime.Parse(drBF["EndTmst"].ToString()).Date == _dtSt.Date))
                && (DateTime.Parse(drWF["StartDt"].ToString()).Date == (DateTime.Parse(drWF["EndDt"].ToString()).AddDays(-1).Date)))//x->-<-=y
            {
                if (!(CheckBusyConflict(drWF["StartDt"], drBF["StrtTmst"], drWF["USER_ID"])))
                    liOut.Add(new Object[] { drWF["StartDt"], drBF["StrtTmst"], drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
            }
            else if ((DateTime.Parse(drWF["StartDt"].ToString()) >= DateTime.Parse(drBF["StrtTmst"].ToString()))
                && (DateTime.Parse(drWF["EndDt"].ToString()) <= DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drBF["StrtTmst"].ToString()) < DateTime.Parse(drBF["EndTmst"].ToString()))
                && ((DateTime.Parse(drBF["StrtTmst"].ToString()).Date == _dtSt.Date)
                        || (DateTime.Parse(drBF["EndTmst"].ToString()).Date == _dtSt.Date))
                && (DateTime.Parse(drWF["StartDt"].ToString()).Date == (DateTime.Parse(drBF["StrtTmst"].ToString()).Date)))//<-x-y->
            {
            }
            else if ((DateTime.Parse(drWF["StartDt"].ToString()) >= DateTime.Parse(drBF["StrtTmst"].ToString()))
                    && (DateTime.Parse(drWF["EndDt"].ToString()) > DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drBF["StrtTmst"].ToString()) < DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drWF["StartDt"].ToString()) < DateTime.Parse(drBF["EndTmst"].ToString()))
                && ((DateTime.Parse(drBF["StrtTmst"].ToString()).Date == _dtSt.Date)
                        || (DateTime.Parse(drBF["EndTmst"].ToString()).Date == _dtSt.Date)
                        || ((DateTime.Parse(drBF["StrtTmst"].ToString()).Date == _dtSt.AddDays(-1).Date)
                    && (DateTime.Parse(drBF["EndTmst"].ToString()).Date == _dtSt.Date)))
                && ((DateTime.Parse(drWF["StartDt"].ToString()).Date == (DateTime.Parse(drBF["StrtTmst"].ToString()).Date))
                   || (DateTime.Parse(drWF["StartDt"].ToString()).AddDays(-1).Date == (DateTime.Parse(drBF["StrtTmst"].ToString()).Date))))//<-x-<-y, incl.busy spread two days
            {
                if (!(CheckBusyConflict(drBF["EndTmst"], drWF["EndDt"], drWF["USER_ID"])))
                    liOut.Add(new Object[] { drBF["EndTmst"], drWF["EndDt"], drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
            }
            else if ((DateTime.Parse(drWF["StartDt"].ToString()) < DateTime.Parse(drBF["StrtTmst"].ToString()))
                    && (DateTime.Parse(drWF["EndDt"].ToString()) <= DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drBF["StrtTmst"].ToString()) < DateTime.Parse(drBF["EndTmst"].ToString()))
                && (DateTime.Parse(drWF["EndDt"].ToString()) > DateTime.Parse(drBF["StrtTmst"].ToString()))
                && ((DateTime.Parse(drBF["StrtTmst"].ToString()).Date == _dtSt.Date)
                        || (DateTime.Parse(drBF["EndTmst"].ToString()).Date == _dtSt.Date))
                && (DateTime.Parse(drWF["StartDt"].ToString()).Date == (DateTime.Parse(drBF["StrtTmst"].ToString()).Date)))//x->-y->
            {
                if (!(CheckBusyConflict(drWF["StartDt"], drBF["StrtTmst"], drWF["USER_ID"])))
                    liOut.Add(new Object[] { drWF["StartDt"], drBF["StrtTmst"], drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
            }
            else if ((DateTime.Parse(drWF["StartDt"].ToString()).Date == _dtSt.Date)
                    || ((DateTime.Parse(drWF["StartDt"].ToString()).Date == _dtSt.AddDays(-1).Date)
                       && (DateTime.Parse(drWF["EndDt"].ToString()).Date == _dtSt.Date)))
            {
                if ((DateTime.Parse(drWF["StartDt"].ToString()).Date < DateTime.Parse(drWF["EndDt"].ToString()).Date)
                && (DateTime.Parse(drWF["StartDt"].ToString()).Date == _dtSt.Date))
                {
                    if (!(CheckBusyConflict(drWF["StartDt"], DateTime.Parse(drWF["EndDt"].ToString()).Date, drWF["USER_ID"])))
                        liOut.Add(new Object[] { drWF["StartDt"], DateTime.Parse(drWF["EndDt"].ToString()).Date, drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
                }
                else if ((DateTime.Parse(drWF["StartDt"].ToString()).Date < DateTime.Parse(drWF["EndDt"].ToString()).Date)
                                    && (DateTime.Parse(drWF["StartDt"].ToString()).Date == _dtSt.AddDays(-1).Date))
                {
                    if (!(CheckBusyConflict(DateTime.Parse(drWF["StartDt"].ToString()).Date.AddDays(1), drWF["EndDt"], drWF["USER_ID"])))
                        liOut.Add(new Object[] { DateTime.Parse(drWF["StartDt"].ToString()).Date.AddDays(1), drWF["EndDt"], drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
                }
                else
                {
                    if (!(CheckBusyConflict(drWF["StartDt"], drWF["EndDt"], drWF["USER_ID"])))
                    {
                        if (!((
                                (_dtBusyCollection.Select("EndTmst >= #" + drWF["EndDt"].ToString() + "# AND StrtTmst > #" + drWF["StartDt"].ToString() + "# AND StrtTmst < #" + drWF["EndDt"].ToString() + "# AND USER_ID = '" + drWF["USER_ID"].ToString() + "'").Length > 0)
                            )
                            ||
                            (
                                (_dtBusyCollection.Select("EndTmst < #" + drWF["EndDt"].ToString() + "# AND EndTmst > #" + drWF["StartDt"].ToString() + "# AND StrtTmst <= #" + drWF["StartDt"].ToString() + "# AND USER_ID = '" + drWF["USER_ID"].ToString() + "'").Length > 0)
                            )
                            ||
                            (
                                (_dtBusyCollection.Select("EndTmst <= #" + drWF["EndDt"].ToString() + "# AND StrtTmst >= #" + drWF["StartDt"].ToString() + "# AND USER_ID = '" + drWF["USER_ID"].ToString() + "'").Length > 0)
                            )
                            ||
                            (
                                (_dtBusyCollection.Select("EndTmst >= #" + drWF["EndDt"].ToString() + "# AND StrtTmst <= #" + drWF["StartDt"].ToString() + "# AND USER_ID = '" + drWF["USER_ID"].ToString() + "'").Length > 0)
                            )
                           ))
                        {
                            liOut.Add(new Object[] { drWF["StartDt"], drWF["EndDt"], drWF["USER_ID"], drWF["USER_ADID"], drWF["DSPL_NME"], drWF["FULL_NME"] });
                        }
                    }
                }
            }

            return liOut;
        }

        protected bool CheckBusyConflict(object dcStDt, object dcEndDt, object userID)
        {
            //DataRow[] drt = _dtBusyCollection.Select("StrtTmst <= '" + (DateTime.Parse(dcStDt.ToString())) + "' AND EndTmst >= '" + (DateTime.Parse(dcEndDt.ToString())) + "' AND USER_ID = '" + userID.ToString() + "'");

            var x = (from bc in _dtBusyCollection.AsEnumerable()
                     where ((DateTime.Parse(bc.Field<Object>("StrtTmst").ToString()) <= (DateTime.Parse(dcStDt.ToString())))
                            && (DateTime.Parse(bc.Field<Object>("EndTmst").ToString()) >= (DateTime.Parse(dcEndDt.ToString())))
                            && (bc.Field<Object>("USER_ID").ToString()).Equals(userID.ToString()))
                     select bc);

            if (x.Count() > 0)
                return true;
            else
                return false;
        }

        private string BuildTimeSlot(DataRow drIn)
        {
            return ((DateTime.Parse(drIn.Field<Object>("StartDt").ToString()).Date < DateTime.Parse(drIn.Field<Object>("EndDt").ToString()).Date)
                                                     && (DateTime.Parse(drIn.Field<Object>("StartDt").ToString()).Date == _dtSt.Date))
                                                     ? DateTime.Parse(drIn.Field<Object>("StartDt").ToString()).ToString("t") + " - 11:59 PM"
                                                     : (((DateTime.Parse(drIn.Field<Object>("StartDt").ToString()).Date < DateTime.Parse(drIn.Field<Object>("EndDt").ToString()).Date)
                                                         && (DateTime.Parse(drIn.Field<Object>("StartDt").ToString()).Date == _dtSt.AddDays(-1).Date))
                                                         ? "12:00 AM - " + DateTime.Parse(drIn.Field<Object>("EndDt").ToString()).ToString("t")
                                                         : DateTime.Parse(drIn.Field<Object>("StartDt").ToString()).ToString("t") + " - " + DateTime.Parse(drIn.Field<Object>("EndDt").ToString()).ToString("t"));
        }
        private bool CheckMDSNtwkActy(string sMDSNtwkActy)
        {
            bool hasMDS = false, hasNtwk = false, hasVAS = false;
            foreach (var li in lMDSNtwkActy)
            {
                if (!hasMDS)
                    hasMDS = (li.NtwkActyTypeId == 1);
                if (!hasNtwk)
                    hasNtwk = (li.NtwkActyTypeId == 2 || li.NtwkActyTypeId == 4);
                if (!hasVAS)
                    hasVAS = (li.NtwkActyTypeId == 3);
            }
            if (sMDSNtwkActy == "hasMDS")
                return hasMDS;
            else if (sMDSNtwkActy == "hasNtwk")
                return hasNtwk;
            else if (sMDSNtwkActy == "hasVAS")
                return hasVAS;
            else if (sMDSNtwkActy == "MDSOnly")
                return hasMDS && !hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "NtwkOnly")
                return !hasMDS && hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "VASOnly")
                return !hasMDS && !hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "MDSNtwk")
                return hasMDS && hasNtwk && !hasVAS;
            else if (sMDSNtwkActy == "MDSVAS")
                return hasMDS && !hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "NtwkVAS")
                return !hasMDS && hasNtwk && hasVAS;
            else if (sMDSNtwkActy == "MDSNtwkVAS")
                return hasMDS && hasNtwk && hasVAS;
            else
                return false;
        }

        protected Single CalculateNtwkVASEventDuration()
        {
            string sListNtwkActy = BuildMDSNtwkActy() + "^MDSSlotDuration";
            var config = _systemConfigRepo.Find(a => a.PrmtrNme == sListNtwkActy).SingleOrDefault();
            if (config == null)
                config = _systemConfigRepo.GetByName(sListNtwkActy);
            string sDurCfg = config.PrmtrValuTxt;
            Single Duration = float.Parse(sDurCfg.Split(new char[] { ',' })[0]);  //60.00F;
            Single Multiplier = float.Parse(sDurCfg.Split(new char[] { ',' })[1]); //30.00F;

            if (byte.Parse(htSlotData["VASCECd"].ToString()) == 3)//for Ntwk + CE
            {
                var sCEBaseDurationCfg = _systemConfigRepo.Find(a => a.PrmtrNme == "CEBaseDuration").SingleOrDefault();
                string sCEBaseDuration = sCEBaseDurationCfg.PrmtrValuTxt;
                Duration = float.Parse(sCEBaseDuration);
                Multiplier = 30.00F;
            }
            else if (bool.Parse(htSlotData["IsSpclPrj"].ToString())) //Always 60mins for Special Project
                return 60.00F;

            if (htSlotData["MDSNtwkTrpt"] != null)
            {
                int rows = ((DataTable)htSlotData["MDSNtwkTrpt"]).Rows.Count;
                if (rows <= 1)
                    return Duration;
                else
                    Duration += (rows - 1) * Multiplier;
            }

            return Duration;
        }
        
        private string BuildMDSNtwkActy()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var li in lMDSNtwkActy)
            {
                if (sb.Length > 0)
                    sb.Append("+");
                sb.Append(li.NtwkActyTypeDes);
            }
            return sb.ToString();
        }

        [HttpPost("GetNtwkIntlDuration")]
        public ActionResult GetNtwkIntlDuration([FromBody] DynamicParametersViewModel model)
        {
            GetSlotsForCurrentSlot(model);

            return Ok(_duration);
        }
    }
}