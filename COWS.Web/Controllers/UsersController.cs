﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepo;
        private readonly IUserProfileRepository _userProfileRepo;
        private readonly ICommonRepository _commonRepository;
        private readonly ILogger<UsersController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;
        private readonly ILDAPUserManagementService _ldapUserManagement;
        private readonly ILoggedInUserService _loggedInUser;
        private IHttpContextAccessor _accessor;

        public UsersController(IMapper mapper,
                               IUserRepository userRepo,
                               IUserProfileRepository userProfileRepo,
                               ICommonRepository commonRepository,
                               ILogger<UsersController> logger,
                               IMemoryCache memoryCache,
                               ILDAPUserManagementService ldapUserManagement,
                               ILoggedInUserService loggedInUser,
                               IHttpContextAccessor accessor)
        {
            _mapper = mapper;
            _userRepo = userRepo;
            _userProfileRepo = userProfileRepo;
            _commonRepository = commonRepository;
            _logger = logger;
            _cache = memoryCache;
            _ldapUserManagement = ldapUserManagement;
            _loggedInUser = loggedInUser;
            _accessor = accessor;
        }

        // GET: api/<controller>
        [HttpGet]
        public ActionResult<IEnumerable<UserViewModel>> Get()
        {
            var users = _mapper.Map<IEnumerable<UserViewModel>>(_userRepo.GetAll());
            return Ok(users);
        }

        [HttpGet("GetAllUsers")]
        public ActionResult<IEnumerable<UserViewModel>> GetAllUsers()
        {
            var users = _mapper.Map<IEnumerable<UserViewModel>>(_userRepo.GetAllUsers());
            return Ok(users);
        }

        // GET api/<controller>/5
        [HttpGet("getByUserID/{id}")]
        public ActionResult<UserViewModel> getByUserID(int id)
        {
            UserViewModel user = _mapper.Map<UserViewModel>(_userRepo.Find(a => a.UserId == id).SingleOrDefault());
            return Ok(user);
        }

        [HttpGet("getUserWithCsgLvlByUserID/{id}")]
        public ActionResult<UserViewModel> getUserWithCsgLvlByUserID(int id)
        {
            UserViewModel user = _mapper.Map<UserViewModel>(_userRepo.GetById(id));
            if(user != null)
            {
                user.csgLvlCd = _userRepo.GetCsgLevelCD(id);
            }
            return Ok(user);
        }

        // GET api/<controller>/5
        [HttpGet("{adid}")]
        public ActionResult<UserViewModel> Get(string adid)
        {
            if (!_cache.TryGetValue(CacheKeys.UserByADID + adid, out UserViewModel user))
            {
                // Key not in cache, so get data.
                if (adid.ToLower() == "ntwkintl")
                    user = _mapper.Map<UserViewModel>(_userRepo.Find(a => a.UserAdid == adid).SingleOrDefault());
                else
                    user = _mapper.Map<UserViewModel>(_userRepo.Find(a => a.UserAdid == adid && a.RecStusId == 1).SingleOrDefault());

                CacheManager.Set(_cache, CacheKeys.UserByADID + adid, user);
            }

            return Ok(user);
        }

        [Authorize]
        [HttpGet("[action]")]
        [AllowAnonymous]
        public IActionResult GetLoggedInUserDetails()
        {
            var sessionId = HttpContext.Session.GetString("sessionId");
            var addLog = (sessionId == null) ? true: false;

            var loggedInUser = _loggedInUser.GetLoggedInUser(addLog);

      

            // GET FROM LDAP AND INSERT NEW RECORD TO DB
            if (loggedInUser == null)
            {

                _logger.LogInformation($"loggedInUser Message: User do not exist on LK_USER");

                var id = _accessor.HttpContext.User.Identity.Name;
               
                if (id == null)
                {
                    id = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                }
                id = id.ToLower().Replace("ad\\", "").Replace("gsm1900\\", "").Trim();
                _logger.LogInformation($"WINDOWS ID Returned: {id}");
                Hashtable ht = new Hashtable();
                try
                {
                    ht = _ldapUserManagement.GetUserFromLDAP(id, "ad");
                }
                catch (Exception)
                {
                    return BadRequest(new { Message = "Encountered an error on the dip into LDAP for user information" });
                }

                if ((ht.Count > 0) && (ht["ADID"].ToString().Trim().Length > 0))
                {
                    LkUser user = new LkUser
                    {
                        UserAdid = id
                    };

                    var email = ((string)ht["EMAIL"]).Replace("&nbsp;", string.Empty).Trim().ToLower();
                    var ntid_email = "";

                    _logger.LogInformation($"USER LDAP Returned EMAIL : {email}");

                    if (email.Contains("t-mobile.com"))
                    {
                        ntid_email = email;
                        email = email.Replace("t-mobile.com", "sprint.com");
                    }
                    var existingUser = _userRepo.Find(s => s.EmailAdr.ToLower() == email).SingleOrDefault();
                    if (existingUser != null)
                    {
                        _logger.LogInformation($"LEGACY USER Found ID: {existingUser.UserId}");
                        _logger.LogInformation($"LEGACY USER Found OLD_ADID: {existingUser.UserAdid}");
                        // Will only trigger the update when there is a NEW ADID
                        if (String.IsNullOrEmpty(existingUser.OldUserAdid))
                        {
                            // [ID] comes from WINDOWS AUTH [New User ADID]
                            // [existingUser.UserAdid] - Current User ADID
                            _userRepo.UpdateLegacyUserADID(id, existingUser.UserAdid, ntid_email);

                            // VIEW PURPOSES
                            existingUser.OldUserAdid = existingUser.UserAdid;
                            existingUser.UserAdid = id;
                            _logger.LogInformation($"LEGACY USER Update executed from {existingUser.OldUserAdid} to {existingUser.UserAdid}");
                        }
                        loggedInUser = _mapper.Map<LogInUserViewModel>(existingUser);
                    }
                    else
                    {
                        if (ht["MIDDLE_NAME"].ToString().Trim().Length > 0)
                        {
                            user.FullNme = (string)ht["FIRST_NAME"] + " " + (string)ht["MIDDLE_NAME"] + " " + (string)ht["LAST_NAME"];
                        }
                        else
                        {
                            user.FullNme = (string)ht["FIRST_NAME"] + " " + (string)ht["LAST_NAME"];
                        }
                        user.UserAcf2Id = null;
                        user.PhnNbr = ((string)ht["PHONE"]).Replace("&nbsp;", string.Empty).Trim();
                        user.EmailAdr = ((string)ht["EMAIL"]).Replace("&nbsp;", string.Empty).Trim();
                        user.RecStusId = 0;
                        user.CreatByUserId = 1;
                        user.CreatDt = DateTime.Now;
                        user.DsplNme = ht["DISPLAYNAME"].ToString();
                        user.SttCd = ht["STT_CD"].ToString();
                        user.CtyNme = ht["CTY_NME"].ToString();

                        var test = _userRepo.Create(user);
                        test.RecStusId = 0;
                        _userRepo.Update(test.UserId, test);

                        loggedInUser = _mapper.Map<LogInUserViewModel>(test);
                    }
                }
                else
                {
                    _logger.LogInformation($"No user information returned from LDAP: {loggedInUser.UserAdid}");
                    return BadRequest(new { Message = "No user information returned from LDAP" });
                }
            }

            _logger.LogInformation($"Current User Login ADID: {loggedInUser.UserAdid}");
            _logger.LogInformation($"Current User Login OLD_ADID: {loggedInUser.OldUserAdid}");

            var adid = loggedInUser.UserAdid;
           
            if (sessionId == null)
            {
                sessionId = HttpContext.Session.Id;
                HttpContext.Session.SetString("sessionId", sessionId);

                var ipAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                _commonRepository.UpdateWebSession(adid, sessionId, ipAddress);

                _logger.LogInformation($"Windows User for request : {loggedInUser.UserAdid}-{loggedInUser.DsplNme} IPAdress : {ipAddress} Browser : {HttpContext.Request.Headers["User-Agent"].ToString()} ");
            }
 
            return Ok(loggedInUser);
        }

        [HttpGet("GetUsersByNameOrADID/{filter}")]
        public ActionResult<IEnumerable<UserViewModel>> GetUsersByNameOrADID(string filter, [FromQuery] bool? isLIKE)
        {
            IEnumerable<UserViewModel> users;
            if (isLIKE == true)
            {
                users = _mapper.Map<IEnumerable<UserViewModel>>(_userRepo.Find(a =>
                    (EF.Functions.Like(a.UserAdid, String.Format("%{0}%", filter)) ||
                    EF.Functions.Like(a.EmailAdr, String.Format("%{0}%", filter)) ||
                    EF.Functions.Like(a.DsplNme, String.Format("%{0}%", filter)) ||
                    EF.Functions.Like(a.FullNme, String.Format("%{0}%", filter)))).AsNoTracking());
            }
            else
            {
                users = _mapper.Map<IEnumerable<UserViewModel>>(_userRepo.Find(a =>
                    (EF.Functions.Like(a.UserAdid, String.Format("%{0}%", filter)) ||
                    EF.Functions.Like(a.EmailAdr, String.Format("%{0}%", filter)) ||
                    EF.Functions.Like(a.DsplNme, String.Format("%{0}%", filter)) ||
                    EF.Functions.Like(a.FullNme, String.Format("%{0}%", filter)))).AsNoTracking());
                //users = _mapper.Map<IEnumerable<UserViewModel>>(_userRepo.Find(a => (a.UserAdid == filter || a.EmailAdr == filter || a.FullNme == filter) && a.RecStusId == 1));
            }

            return Ok(users);
        }

        [HttpGet("GetUsersByProfileName/{profileName}")]
        public ActionResult<IEnumerable<UserViewModel>> GetUsersByProfileName(string profileName)
        {
            IEnumerable<UserViewModel> users;
            users = _mapper.Map<IEnumerable<UserViewModel>>(_userRepo.GetUsersByProfileName(profileName).AsNoTracking());

            return Ok(users);
        }

        [HttpPost("EventActivator/{type}")]
        public ActionResult<IEnumerable<UserViewModel>> GetEventActivator([FromRoute] string type, [FromBody] DynamicParametersViewModel model)
        {
            var param = _mapper.Map<DynamicParameters>(model);

            IEnumerable<UserViewModel> users;
            users = _mapper.Map<IEnumerable<UserViewModel>>(_userRepo.GetEventActivator(type, param));

            return Ok(users);
        }

        // GET api/<controller>/5
        [HttpGet("{adid}/Profiles")]
        public ActionResult<IEnumerable<UserViewModel>> GetProfiles(string adid)
        {
            IEnumerable<UserProfileViewModel> userProfile;

            //if (!_cache.TryGetValue(CacheKeys.UserByADID + adid, out user))
            //{
            // Key not in cache, so get data.
            var user = _userRepo.Find(a => a.UserAdid == adid && a.RecStusId == 1).SingleOrDefault();

            if (user != null)
            {
                int id = user.UserId;
                userProfile = _mapper.Map<IEnumerable<UserProfileViewModel>>(
                    _userProfileRepo
                        .Find(a => a.MapUsrPrf.Where(b => b.UserId == id && b.RecStusId == 1)
                            .Count() > 0)
                        .ToList()
                );

                return Ok(userProfile);
            }

            //if (user != null)
            //{
            //    //int id = user.UserId;
            //    //IEnumerable<LkUsrPrf> lkusrprf = _userProfileRepo
            //    //        .Find(a => a.MapUsrPrf.Where(b => b.UserId == id && b.RecStusId == 1)
            //    //            .Count() > 0)
            //    //        .AsNoTracking()
            //    //        .ToList();
            //    //IEnumerable<UserProfileViewModel> dest;
            //    //userProfile = _mapper.Map<IEnumerable<LkUsrPrf>, IEnumerable<UserProfileViewModel>>(lkusrprf, opt => { opt.AfterMap((src, dest) => dest.Where(c => c.UserId == id)); });

            //    userProfile = _mapper.Map<IEnumerable<UserProfileViewModel>>(_userProfileRepo.GetUserProfilesByUserId(user.UserId, user.UserId));

            //    //_mapper.Map<IEnumerable<UserProfileViewModel>>(
            //    //    _userProfileRepo
            //    //        .Find(a => a.MapUsrPrf.Where(b => b.UserId == id && b.RecStusId == 1)
            //    //            .Count() > 0)
            //    //        .AsNoTracking()
            //    //        .ToList()
            //    //).Where(c=>c.UserId == id);

            //    return Ok(userProfile);
            //}
            else
            {
                return BadRequest(new { Message = "No user data found" });
            }
        }

        // GET api/<controller>/5
        [HttpGet("{adid}/Profile")]
        public ActionResult<UserProfileViewModel> GetFinalProfile([FromRoute] string adid, [FromQuery] int eventTypeId, [FromQuery] bool isEvent)
        {
            UserProfileViewModel profile = _mapper.Map<UserProfileViewModel>(_userRepo.GetFinalUserProfile(_loggedInUser.GetLoggedInUserId(), eventTypeId, isEvent));

            return Ok(profile);
        }

        // GET api/<controller>/5
        [HttpGet("{adid}/GetActiveUserProfiles")]
        public IActionResult GetActiveUserProfiles([FromQuery] int eventTypeId)
        {
            var profiles = _userRepo.GetActiveProfileIdByEventTypeId(_loggedInUser.GetLoggedInUserId(), eventTypeId).ToArray();

            return Ok(profiles);
        }



        // GET api/<controller>/5
        [HttpGet("Bridge/{id}")]
        public ActionResult<IEnumerable<LkCnfrcBrdg>> GetConfBridge([FromRoute] int id)
        {
            var confBridge = _userRepo.GetCnfrcBrdgCreatByUser(id);
            if (confBridge != null)
            {
                return Ok(confBridge);
            }
            else
            {
                return Ok(null);
            }
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] UserViewModel model)
        {
            var user = _mapper.Map<LkUser>(model);

            //var loggedInUser = await _loggedInUser.GetLoggedInUser();
            //if (loggedInUser != null)
            //{
            //    channel.ModifiedById = loggedInUser.Id;
            //}

            _userRepo.Update(id, user);
            CacheManager.Set(_cache, CacheKeys.UserByADID + user.UserAdid, _mapper.Map<UserViewModel>(user));

            _logger.LogInformation($"User Updated. { JsonConvert.SerializeObject(user).ToString() } ");
            return Created($"api/Users/{ user.UserId }", model);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpGet("InsertUser/{adid}")]
        public IActionResult InsertUser(string adid)
        {
            Hashtable ht = new Hashtable();
            try
            {
                try
                {
                    ht = _ldapUserManagement.GetUserFromLDAP(adid, "ad");
                }
                catch (Exception)
                {
                    return BadRequest(new { Message = "Encountered an error on the dip into LDAP for user information" });
                }

                if ((ht.Count > 0) && (ht["ADID"].ToString().Trim().Length > 0))
                {
                    LkUser user = new LkUser
                    {
                        UserAdid = adid
                    };
                    if (ht["MIDDLE_NAME"].ToString().Trim().Length > 0)
                    {
                        user.FullNme = (string)ht["FIRST_NAME"] + " " + (string)ht["MIDDLE_NAME"] + " " + (string)ht["LAST_NAME"];
                    }
                    else
                    {
                        user.FullNme = (string)ht["FIRST_NAME"] + " " + (string)ht["LAST_NAME"];
                    }
                    user.UserAcf2Id = null;
                    user.PhnNbr = ((string)ht["PHONE"]).Replace("&nbsp;", string.Empty).Trim();
                    user.EmailAdr = ((string)ht["EMAIL"]).Replace("&nbsp;", string.Empty).Trim();
                    user.RecStusId = 1;
                    user.CreatByUserId = 1;
                    user.CreatDt = DateTime.Now;
                    user.DsplNme = ht["DISPLAYNAME"].ToString();

                    var newUser = _userRepo.Create(user);

                    if (newUser == null)
                    {
                        return BadRequest(new { Message = "User ID already present in database, please perform a search for this NTID" });
                    }
                    else
                    {
                        return Created($"api/Users/{ adid }", user);
                    }
                }
                else
                {
                    return BadRequest(new { Message = "No data found in LDAP for this NTID" });
                }
            }
            catch (Exception)
            {
                return BadRequest(new { Message = "An error occurred while attempting to create new user ID" });
            }
        }

        [HttpGet("IsCSGLevelUser")]
        public IActionResult IsCSGLevelUser([FromQuery] string adid, [FromQuery] int csgLevel)
        {
            return Ok(_userRepo.IsCSGLevelUser(adid, csgLevel));
        }

        [HttpGet("UserActiveWithProfileName")]
        public IActionResult UserActiveWithProfileName([FromQuery] int userId, [FromQuery] string profileName)
        {
            return Ok(_userRepo.UserActiveWithProfileName(userId, profileName));
        }

        [HttpGet("GetConferenceBridgeInfo/{userId}")]
        public ActionResult<DataTable> GetConferenceBridgeInfo([FromRoute] int userId)
        {
            //userId = 46;
            DataTable confBridge = _userRepo.GetConferenceBridgeInfo();
            if (userId > 0)
            {
                var cb = (from item in confBridge.AsEnumerable()
                          where item.Field<int>("CreatByUserId") == userId
                          select new
                          {
                              CnfrcBrdgNbr = item.Field<string>("CnfrcBrdgNbr").ToString(),
                              CnfrcPinNbr = item.Field<string>("CnfrcPinNbr").ToString(),
                              OnlineMeetingAdr = item.Field<string>("OnlineMeetingAdr").ToString(),
                              CreatByUserId = item.Field<int>("CreatByUserId")
                          }).Distinct().OrderBy(ob => ob.CreatByUserId).ToList();

                return Ok(LinqHelper.CopyToDataTable(cb, null, null));
            }

            //return Ok(confBridge);
            return Ok(null);
        }

        [HttpGet("GetSDEViewUsers")]
        public IActionResult GetSDEViewUsers()
        {
            return Ok(_userRepo.GetSDEViewUsers());
        }

        [HttpGet("GetAdminExcPerm")]
        public IActionResult GetAdminExcPerm([FromQuery] string name)
        {
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            return Ok(_commonRepository.GetAdminExcPerm(name, loggedInUser.UserAdid));
        }
        
    }
}