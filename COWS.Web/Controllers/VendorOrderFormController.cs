﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VendorOrderForms")]
    [ApiController]
    public class VendorOrderFormController : ControllerBase
    {
        private readonly IVendorOrderFormRepository _repo;
        private readonly ILogger<VendorOrderFormController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public VendorOrderFormController(IMapper mapper,
                               IVendorOrderFormRepository repo,
                               ILogger<VendorOrderFormController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VendorOrderFormViewModel>> Get()
        {
            IEnumerable<VendorOrderFormViewModel> list;
            //if (!_cache.TryGetValue(CacheKeys.VendorOrderFormList, out list))
            //{
                list = _mapper.Map<IEnumerable<VendorOrderFormViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.CreatDt));

            //    CacheManager.Set(_cache, CacheKeys.VendorOrderFormList, list);
            //}

            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] VendorOrderFormViewModel model)
        {
            model.FileCntnt = Convert.FromBase64String(model.Base64string);

            _logger.LogInformation($"Uploading Vendor Form: { model.FileNme }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrOrdrForm>(model);

                // Remove this comment once logged in user service is available
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                var rep = _repo.Create(obj);
                if (rep != null)
                {
                    var list = _mapper.Map<IEnumerable<VendorOrderFormViewModel>>(_repo
                        .Find(a => a.VndrOrdrId == rep.VndrOrdrId)
                        .ToList());

                    _logger.LogInformation($"Vendor Form Uploaded. { JsonConvert.SerializeObject(list, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    return Created($"api/VendorOrderForms/{ rep.FileNme }", list);
                }
            }

            return BadRequest(new { Message = "Currency File Could Not Be Uploaded." });
        }

        [HttpDelete("{id}")]
        public void Delete([FromRoute]int id)
        {
            _logger.LogInformation($"Delete Vendor Form By Id: {id}");

            _repo.Delete(id);
            var count = _repo.SaveAll();

            if (count > 0)
            {
                _logger.LogInformation($"Deleted Vendor Form By Id: {id}");
            }
        }

        [HttpGet("{id}/download")]
        public IActionResult Download([FromRoute] int id)
        {
            _logger.LogInformation($"Search Vendor Form by Id: { id }.");

            var obj = _repo.Find(s => s.VndrOrdrFormId == id).SingleOrDefault();
            if (obj != null)
            {
                try
                {
                    return new FileContentResult(obj.FileCntnt, "application/octet")
                    {
                        FileDownloadName = obj.FileNme
                    };
                }
                catch (Exception)
                {
                    return NotFound(new { Message = $"Vendor Form Id: { id } not found." });
                }
            }
            else
            {
                _logger.LogInformation($"Vendor Form by Id: { id } not found.");
                return NotFound(new { Message = $"Vendor Form Id: { id } not found." });
            }
        }
    }
}
