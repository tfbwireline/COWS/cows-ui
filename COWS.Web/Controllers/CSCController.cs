﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CSC")]
    [ApiController]
    public class CSCController : ControllerBase
    {
        private readonly IDCPERepository _repo;
        private readonly ILogger<CSCController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IOrderNoteRepository _orderNoteRepo;
        private readonly IWorkGroupRepository _workGroupRepo;

        public CSCController(IMapper mapper,
                               IDCPERepository repo,
                               ILogger<CSCController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache,
                               IOrderNoteRepository orderNoteRepo,
                               IWorkGroupRepository workGroupRepo)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _orderNoteRepo = orderNoteRepo;
            _workGroupRepo = workGroupRepo;
        }


        [HttpPost("SaveCSCSpecificData")]
        public IActionResult SaveCSCSpecificData([FromBody] OrderViewModel model)
        {
            var insertCSCInfo = _workGroupRepo.InsertUpdateCSCInfo(model.OrderId, model.SerialNo, model.MR, model.PO, model.IMS, model.UserId);

            if (insertCSCInfo == 0)
            {
                return BadRequest(false);
            }

            if (model.NoteText != "") {
                var insertNote = InsertNote(model.OrderId, 8, model.NoteText);

                if (!insertNote)
                {
                    return BadRequest(false);
                }
            }

            if (model.IsComplete) {
                int i = _workGroupRepo.CompleteActiveTask(model.OrderId, 400, 2, "The M5 # is marked as CSC Smart Maintenance Complete. ", model.UserId);
                if (i != 0)
                {
                    return Ok(true);
                }
                else { 
                    return BadRequest(false);
                }
            }
            return Ok(true);
        }

        private bool InsertNote(int orderId, byte nteType, string nte)
        {
            var note = new OrderNoteViewModel
            {
                OrdrId = orderId,
                NteTypeId = nteType,
                NteTxt = nte
            };

            var obj = _mapper.Map<OrdrNte>(note);
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.CreatByUserId = loggedInUser.UserId;
                obj.CreatDt = DateTime.Now;
            }

            var insertNote = _orderNoteRepo.Create(obj);

            if (insertNote == null)
                return false;
            else
                return true;
        }        
    }
}