﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/MvsProdTypes")]
    [ApiController]
    public class CptMvsProdTypeController : ControllerBase
    {
        private readonly ICptMvsProdTypeRepository _repo;
        private readonly ILogger<CptMvsProdTypeController> _logger;
        private readonly IMapper _mapper;

        public CptMvsProdTypeController(IMapper mapper,
                               ICptMvsProdTypeRepository repo,
                               ILogger<CptMvsProdTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptMvsProdTypeViewModel>> Get()
        {
           var list = _mapper.Map<IEnumerable<CptMvsProdTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptMvsProdTypeId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT MVS Prod Type by Id: { id }.");

            var obj = _repo.Find(s => s.CptMvsProdTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptMvsProdTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT MVS Prod Type by Id: { id } not found.");
                return NotFound(new { Message = $"CPT MVS Prod Type Id: { id } not found." });
            }
        }
    }
}