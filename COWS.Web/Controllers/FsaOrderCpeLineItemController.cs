﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/FsaOrderCpeLineItem")]
    [ApiController]
    public class FsaOrderCpeLineItemController : ControllerBase
    {
        private readonly IFsaOrderCpeLineItemRepository _repo;
        private readonly ILogger<FsaOrderCpeLineItemController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public FsaOrderCpeLineItemController(IMapper mapper,
                               IFsaOrderCpeLineItemRepository repo,
                               ILogger<FsaOrderCpeLineItemController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<FsaOrderCpeLineItemViewModel>> Get()
        {
            IEnumerable<FsaOrderCpeLineItemViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.FsaOrderCpeLineItemList, out list))
            {
                list = _mapper.Map<IEnumerable<FsaOrderCpeLineItemViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.OrdrId));

                CacheManager.Set(_cache, CacheKeys.FsaOrderCpeLineItemList, list);
            }
           
            return Ok(list);
        }

        [HttpGet("order/{id}")]
        public IActionResult GetCpeLineItemsByOrderId([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPE Line Items by Order Id: { id }.");

            var obj = _repo.Find(a => a.OrdrId == id);

            if (obj != null)
            {
                return Ok(_mapper.Map<IEnumerable<FsaOrderCpeLineItemViewModel>>(obj.ToList()));
            }
            else
            {
                _logger.LogInformation($"CPE Line Items by Order Id: { id } not found.");
                return Ok();
            }
        }
    }
}
