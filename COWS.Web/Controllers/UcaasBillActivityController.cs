﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/UcaasBillActivities")]
    [ApiController]
    public class UcaasBillActivityController : ControllerBase
    {
        private readonly IUcaasBillActivityRepository _repo;
        private readonly ILogger<UcaasBillActivityController> _logger;
        private readonly IMapper _mapper;

        public UcaasBillActivityController(IMapper mapper,
                               IUcaasBillActivityRepository repo,
                               ILogger<UcaasBillActivityController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UcaasBillActivityViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<UcaasBillActivityViewModel>>(_repo
                                                                   .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                   .OrderBy(s => s.UcaaSBillActyDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search UCaaS Bill Activity by Id: { id }.");

            var obj = _repo.Find(s => s.UcaaSBillActyId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<UcaasBillActivityViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"UCaaS Bill Activity by Id: { id } not found.");
                return NotFound(new { Message = $"UCaaS Bill Activity Id: { id } not found." });
            }
        }
    }
}