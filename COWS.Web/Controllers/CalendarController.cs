﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Compatibility;
using DevExpress.XtraScheduler.iCalendar;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Globalization;
using System.Net.Mail;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Calendar")]
    [ApiController]
    public class CalendarController : ControllerBase
    {
        private readonly ICalendarRepository _repo;
        private readonly ILogger<CalendarController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private readonly IConfiguration _configuration;

        public CalendarController(IMapper mapper,
                               ICalendarRepository repo,
                               ILogger<CalendarController> logger,
                               ICommonRepository commonRepo,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache memoryCache,
                               IConfiguration configuration)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _configuration = configuration;
        }

        [HttpGet("GetCalendarData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ActionResult<DataSet> GetCalendarData([FromQuery] int isWFCalendar, [FromQuery] int userId, [FromQuery] int apptTypeId = 0)
        {
            //IEnumerable<CalendarDataViewModel> list;
            // var loggedInUser = _loggedInUser.GetLoggedInUser();
            //var userId = loggedInUser.UserId;
            var csgLvlId = _loggedInUser.GetLoggedInUserCsgLvlId();
            DataSet ds = _repo.GetAppointmentData(isWFCalendar, userId, apptTypeId, csgLvlId);
            //list = _mapper.Map<IEnumerable<CalendarDataViewModel>>(_repo.GetCalendarData(isWFCalendar, userId, apptTypeId, csgLvlId));

            if ((ds != null) && (ds.Tables.Count > 0) && (ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (!dr["RCURNC_DES_TXT"].ToString().Equals(string.Empty))
                    {
                        var appointment = StaticAppointmentFactory.CreateAppointment(AppointmentType.Pattern);
                        var pattern = dr["RCURNC_DES_TXT"].ToString();
                        appointment.RecurrenceInfo.FromXml(pattern, DateTimeSavingMode.Utc);
                        appointment.Start = DateTime.Parse(dr["STRT_TMST"].ToString(), CultureInfo.CurrentCulture, DateTimeStyles.RoundtripKind).ToLocalTime();
                        appointment.End = DateTime.Parse(dr["END_TMST"].ToString(), CultureInfo.CurrentCulture, DateTimeStyles.RoundtripKind).ToLocalTime();
                        appointment.AllDay = true;

                        string s = DevExpress.XtraScheduler.iCalendar.iCalendarHelper.ExtractRecurrenceRule(appointment.RecurrenceInfo);
                        dr["RCURNC_DES_TXT"] = s;
                    }
                }
            }
            //return ds;

            return Ok(ds);
        }

        [HttpGet("GetApptTypes")]
        public ActionResult<DataSet> GetApptTypes([FromQuery] int isMain, [FromQuery] int isWF = 0)
        {
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            var userId = loggedInUser.UserId;

            var dataSet = _repo.GetApptTypes(userId, Convert.ToByte(isMain), Convert.ToByte(isWF));
            if (dataSet != null)
            {
                return Ok(dataSet);
            }
            return BadRequest(new { Message = "No Appoinment Types found." });
        }

        [HttpPost]
        public IActionResult CreateUpdateCalendarData([FromBody] CalendarDataViewModel data)
        {
            var model = _mapper.Map<CalendarData>(data);
            //var addView = _repo.UpdateCalendarData(model.UserId, model.DsplVwNme, model.SiteCntntId, model.DfltVwCd, model.PblcVwCd, model.FiltrCol1OprId, model.FiltrCol2OprId, model.FiltrCol1Id, model.FiltrCol2Id, model.FiltrCol1ValuTxt, model.FiltrCol2ValuTxt, model.FiltrOnCd, model.SortByCol1Id, model.SortByCol1AscOrdrCd, model.SortByCol2Id, model.SortByCol2AscOrdrCd, model.FiltrLogicOprId, model.ColIDs, model.ColPos);
            if (!data.RcurncDesTxt.Equals(string.Empty))
            {
                RecurrenceInfo ri = new RecurrenceInfo();
                ri.Start = data.StrtTmst;

                iCalendarHelper.ApplyRecurrenceRule(data.RcurncDesTxt, ri);
                model.RcurncDesTxt = ri.ToXml();
                
            }
            var addView = _repo.CreateUpdateCalendarData(model);
            if (addView)
            {
                if (model.ApptTypeId > 16)
                {
                    SendEmailOnSave(model);
                }
                _logger.LogInformation($"Event View Created.");
                return Ok(true);
            }

            return BadRequest(new { Message = "An error occurred while attempting to create New Event View" });
        }

        private void SendEmailOnSave(CalendarData model)
        {
            DataSet ds = _repo.GetAppointmentDetailsByApptID(model.ApptId);

            var loggedInUser = _loggedInUser.GetLoggedInUser();

            if ((ds != null) && (ds.Tables.Count > 0) && (ds.Tables[0].Rows.Count > 0))
            {
                string _sEmailTo = ds.Tables[0].Rows[0]["EmailTo"].ToString();
                string _CCTo = ds.Tables[0].Rows[0]["CCTo"].ToString();
                string _Subject = ds.Tables[0].Rows[0]["Subject"].ToString();
                string _Config = _configuration.GetSection("AppSettings:CalenderEntryURL").Value + "?eid=0&mode=1&apptId=" + model.ApptId.ToString();
                string _Body = "ES Calendar Appointment <br/> <br/> Appointment Type : " + ds.Tables[0].Rows[0]["APPTTYPE"].ToString() + "<br/>Updated By : " + loggedInUser.DsplNme + "<br/><a href ='" + _Config + "'>Click here to open and save the entry into your outlook calendar.</a>";

                if (_sEmailTo.Length > 5)
                {
                    string sEmailFrom = _configuration.GetSection("AppSettings:DefaultFromEmail").Value;
                    EmailHelper emailHelper = new EmailHelper(_configuration);
                    emailHelper.SendSingleEmail(true, MailPriority.Normal, sEmailFrom, _sEmailTo, _CCTo, "", _Subject, _Body);
                }
            }

        }
    }
}