﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/IPVersions")]
    [ApiController]
    public class IPVersionController : ControllerBase
    {
        private readonly IIPVersionRepository _repo;
        private readonly ILogger<IPVersionController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;

        public IPVersionController(IMapper mapper,
                               IIPVersionRepository repo,
                               ILogger<IPVersionController> logger,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<IPVersionViewModel>> Get()
        {
            IEnumerable<IPVersionViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.IPVersionList, out list))
            {
                list = _mapper.Map<IEnumerable<IPVersionViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.IpVerNme));

                CacheManager.Set(_cache, CacheKeys.IPVersionList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search IPVer by Id: { id }.");

            var obj = _repo.Find(s => s.IpVerId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<IPVersionViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"IPVer by Id: { id } not found.");
                return NotFound(new { Message = $"IPVer Id: { id } not found." });
            }
        }
    }
}