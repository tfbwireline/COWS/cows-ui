﻿using System;
using System.Collections.Generic;
using System.Linq;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace COWS.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private const string Canned_ReportType = "C";
        private readonly IDashboardRepository _dashboardRepo;
        private readonly ICannedReportsRepository _cannedRepo;
        private readonly ILoggedInUserService _userService;

        public ReportsController(IDashboardRepository dashboardRepo,
                                ICannedReportsRepository cannedRepo,
                                ILoggedInUserService loggedInUserService)
        {
            _dashboardRepo = dashboardRepo;
            _cannedRepo = cannedRepo;
            _userService = loggedInUserService;
        }

        [HttpGet("Dashboard")]
        public ActionResult GetDashboardReport()
        {
            return Ok(_dashboardRepo.Get());
        }

        [HttpPost("CannedReport")]
        public ActionResult<IEnumerable<CannedReport>> GetCannedReports(CannedReportViewModel report)
        {
            return Ok(CannedReports(report.reportType, report.startDate, report.endDate));
        }


        [HttpPost("DownloadCannedReportFile")]
        public IActionResult DownloadCannedReportFile(CannedReportViewModel report)
        {

            byte[] array = _cannedRepo.GetCannedReportFile(report.filePath);

            try
            {
                return new FileContentResult(array, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = report.fileName
                };
            }
            catch (Exception)
            {
                return NotFound(new { Message = $"Canned Report File not found." });
            }
        }

        [HttpPost("ReloadReports")]
        public ActionResult<IEnumerable<CannedFile>> ReloadReports(CannedReportViewModel report)
        {
            var file = _cannedRepo.GetCannedFilesByReportID(report.reportId, _userService.GetLoggedInUserCsgLvlId(),
                                                                        report.reportType, report.startDate, report.endDate);
            return Ok(file);
        }

        private IEnumerable<CannedReport> CannedReports(string reportType, DateTime startDate, DateTime endDate)
        {

            List<CannedReport> reports = new List<CannedReport>();

            var reportList = _cannedRepo.GetCannedReportByUserId(Canned_ReportType,
                                                                _userService.GetLoggedInUserId(),
                                                                _userService.GetLoggedInUserCsgLvlId())
                                                                .Where(x => x.IntervalCode == reportType);

            if (reportList != null)
            {
                foreach (var report in reportList)
                {
                    var reportFile = _cannedRepo.GetCannedFilesByReportID(report.Id,
                                                                        _userService.GetLoggedInUserCsgLvlId(),
                                                                        report.IntervalCode);

                    var r = new CannedReport
                    {
                        File = reportFile,
                        Report = report
                    };

                    reports.Add(r);
                }
            }

            return reports;

        }

    }
}