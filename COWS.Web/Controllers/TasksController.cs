﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Tasks")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskRepository _repo;
        private readonly ILogger<TasksController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public TasksController(IMapper mapper,
                               ITaskRepository repo,
                               ILogger<TasksController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskViewModel>> Get()
        {
            IEnumerable<TaskViewModel> list;
            if (!_cache.TryGetValue(CacheKeys.TaskList, out list))
            {
                list = _mapper.Map<IEnumerable<TaskViewModel>>(_repo.GetAll().OrderBy(s => s.TaskNme));

                CacheManager.Set(_cache, CacheKeys.TaskList, list);
            }

            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Task by Id: { id }.");

            var obj = _repo.Find(s => s.TaskId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<TaskViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Task by Id: { id } not found.");
                return NotFound(new { Message = $"Task Id: { id } not found." });
            }
        }

        [HttpGet("{id}/CurrentStatus")]
        public IActionResult GetCurrentStatus([FromRoute] int id, [FromQuery] int wgId, [FromQuery] int orderId)
        {
            _logger.LogInformation($"Get Current Status of Order by Id: {orderId}");

            string taskName = GetTaskName(id, wgId, orderId);

            return Ok(taskName);

            //if (data != null)
            //{
            //    OrderDetailViewModel orderDetails = _mapper.Map<OrderDetailViewModel>(data.SingleOrDefault());

            //    if (orderDetails.OrderTypeDesc == "CANCEL" && orderDetails.OrderCategoryId == 2)
            //    {
            //        return Ok(orderDetails.FsaOrderTypeDesc);
            //    }

            //    return Ok();
            //}
            //else
            //{
            //    _logger.LogInformation($"Order not found.");
            //    return NotFound(new { Message = $"Order not found." });
            //}
        }

        private string GetTaskName(int id, int wgId, int orderId)
        {

            if (id == 0)
            {
                var task = _repo.GetCurrentStatus(a =>
                    a.ActTask.Any(b => b.OrdrId == orderId && b.StusId == 0) &&
                    a.MapPrfTask.Any(b => b.PrntPrfId == wgId) &&
                    a.TaskId != 211 &&
                    a.TaskId != 217 &&
                    a.TaskId != 218
                ).ToList();

                if (task != null)
                {
                    if (task.SingleOrDefault().TaskNme == string.Empty)
                    {
                        return GetTaskName(id, wgId, 0);
                    }
                    else
                    {
                        return $"{task.SingleOrDefault().TaskNme} - Pending";
                    }
                }
            }
            else
            {
                if (id != 1001 && id != 1000)
                {
                    var task = _repo.GetCurrentStatus(a =>
                        a.ActTask.Any(b => b.TaskId == id && b.OrdrId == orderId && b.StusId == 0)
                    ).ToList();

                    if (task != null)
                    {
                        return $"{task.SingleOrDefault().TaskNme} - Pending";
                    }
                }
            }

            var systemTask = _repo.GetCurrentStatus(a =>
                a.ActTask.Any(b => (
                    b.TaskId == 1000 || b.TaskId == 1001) &&
                    b.OrdrId == orderId &&
                    b.StusId == 0
                )
            ).ToList();

            if (systemTask != null)
            {
                var currentTask = systemTask.SingleOrDefault();
                if (currentTask.ActTask.Any(a => a.Ordr.OrdrStusId == 0 || a.Ordr.OrdrStusId == 1))
                {
                    return $"{currentTask.TaskNme} - Pending";
                }
                else
                {
                    return currentTask.ActTask.SingleOrDefault().Ordr.OrdrStus.OrdrStusDes;
                }
            }
            else
            {
                return null;
            }
        }
    }
}