﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CustContractLength")]
    [ApiController]
    public class CustContractLength : ControllerBase
    {
        private readonly ICustContractLengthRepository _repo;
        private readonly IMapper _mapper;

        public CustContractLength(IMapper mapper,
                               ICustContractLengthRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<IEnumerable<LkCustCntrcLgth>> Get()
        {
            var list = _mapper.Map<IEnumerable<CustContractLengthViewModel>>(_repo
                                                                .GetAll());
            return Ok(list);
        }
    }
}