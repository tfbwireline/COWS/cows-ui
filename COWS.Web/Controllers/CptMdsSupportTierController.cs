﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CPT/MdsSupportTiers")]
    [ApiController]
    public class CptMdsSupportTierController : ControllerBase
    {
        private readonly ICptMdsSupportTierRepository _repo;
        private readonly ILogger<CptMdsSupportTierController> _logger;
        private readonly IMapper _mapper;

        public CptMdsSupportTierController(IMapper mapper,
                               ICptMdsSupportTierRepository repo,
                               ILogger<CptMdsSupportTierController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CptMdsSupportTierViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CptMdsSupportTierViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.CptMdsSuprtTierId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search CPT MDS Support Tier by Id: { id }.");

            var obj = _repo.Find(s => s.CptMdsSuprtTierId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CptMdsSupportTierViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"CPT MDS Support Tier by Id: { id } not found.");
                return NotFound(new { Message = $"CPT MDS Support Tier Id: { id } not found." });
            }
        }
    }
}