﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using COWS.Entities.QueryModels;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VendorOrderEmails")]
    [ApiController]
    public class VendorOrderEmailController : ControllerBase
    {
        private readonly IVendorOrderEmailRepository _repo;
        private readonly ISystemConfigRepository _systemConfigRepo;
        private readonly IVendorOrderRepository _vndrOrdrRepo;
        private readonly IVendorFolderRepository _repoVendorFolder;
        private readonly IVendorEmailLanguageRepository _repoVendorEmailLanguage;
        private readonly IActiveTaskRepository _activeTaskRepo;
        private readonly INRMRepository _nrmRepo;
        private readonly IOrderNoteRepository _orderNoteRepo;
        private readonly ILogger<VendorOrderEmailController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IConfiguration _configuration;
        private const string VendorOrderConfirmation = "Firm Order Confirmation";
        private const string VendorOrderDisconnectConfirmation = "Vendor Order Disconnect Confirmation";
        private const string ASRDetail = "ASR Detail Document";

        public VendorOrderEmailController(IMapper mapper,
                               IVendorOrderEmailRepository repo,
                               ISystemConfigRepository systemConfigRepo,
                               IVendorOrderRepository vndrOrdrRepo,
                               IVendorFolderRepository repoVendorFolder,
                               IVendorEmailLanguageRepository repoVendorEmailLanguage,
                               IActiveTaskRepository activeTaskRepo,
                               INRMRepository nrmRepo,
                               IOrderNoteRepository orderNoteRepo,
                               ILogger<VendorOrderEmailController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache,
                               IConfiguration configuration)
        {
            _mapper = mapper;
            _repo = repo;
            _systemConfigRepo = systemConfigRepo;
            _vndrOrdrRepo = vndrOrdrRepo;
            _repoVendorFolder = repoVendorFolder;
            _repoVendorEmailLanguage = repoVendorEmailLanguage;
            _activeTaskRepo = activeTaskRepo;
            _nrmRepo = nrmRepo;
            _orderNoteRepo = orderNoteRepo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _configuration = configuration;
            //_emailHelper = emailHelper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VendorOrderEmailViewModel>> Get()
        {
            IEnumerable<VendorOrderEmailViewModel> list;
            //if (!_cache.TryGetValue(CacheKeys.VendorOrderEmailList, out list))
            //{
                list = _mapper.Map<IEnumerable<VendorOrderEmailViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.VndrOrdrId));

                //CacheManager.Set(_cache, CacheKeys.VendorOrderEmailList, list);
            //}

            return Ok(list);
        }

        [HttpPost("")]
        public IActionResult Post([FromBody] VendorOrderEmailViewModel model)
        {
            _logger.LogInformation($"Create Vendor Order Email: { model }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrOrdrEmail>(model);
                obj.FromEmailAdr = _loggedInUser.GetLoggedInUser().EmailAdr;
                obj.CcEmailAdr = _loggedInUser.GetLoggedInUser().EmailAdr;
                obj.CreatByUserId = _loggedInUser.GetLoggedInUserId();
                obj.CreatDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;

                var rep = _repo.Create(obj);

                if (rep != null)
                {
                    var list = _mapper.Map<IEnumerable<VendorOrderEmailViewModel>>(_repo
                                                                .Find(a => a.VndrOrdrId == rep.VndrOrdrId)
                                                                .OrderBy(s => s.VndrOrdrId));

                    _logger.LogInformation($" Vendor Order Email Created. { JsonConvert.SerializeObject(rep, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    return Created($"api/VendorOrderEmails/{ rep.VndrOrdrEmailId }", list);
                }
            }

            return BadRequest(new { Message = "Vendor Order Email Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] VendorOrderEmailViewModel model)
        {
            _logger.LogInformation($"Update Vendor Order Email: { model }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrOrdrEmail>(model);
                obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                obj.ModfdDt = DateTime.Now;

                if (model.EmailStusId == (short)EmailStatus.SentEnc || model.EmailStusId == (short)EmailStatus.SentUnenc)
                {
                    obj.EmailStusId = (short)EmailStatus.Draft;
                    obj.VndrOrdrEmailId = CloneEmail(id);
                    _repo.Update(obj.VndrOrdrEmailId, obj);
                }
                else
                {
                    _repo.Update(id, obj);
                }

                int count = _repo.SaveAll();

                if (count > 0)
                {
                    return Created($"api/VendorOrderEmails/{ obj.VndrOrdrEmailId }", _mapper.Map<VendorOrderEmailViewModel>(obj));
                }
            }

            return BadRequest(new { Message = "Vendor Order Email Could Not Be Updated." });
        }

        [HttpPut("{id}/Send")]
        public IActionResult SendEmail([FromRoute] int id, [FromBody] VendorOrderEmailViewModel model)
        {
            _logger.LogInformation($"Sending Vendor Order Email: { model }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<VndrOrdrEmail>(model);
                //obj.EmailStusId = (short)EmailStatus.Draft;
                ////obj.SentDt = DateTime.Now;
                //obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                //obj.ModfdDt = DateTime.Now;

                if (model.EmailStusId == (short)EmailStatus.SentEnc || model.EmailStusId == (short)EmailStatus.SentUnenc)
                {
                    obj.VndrOrdrEmailId = CloneEmail(id);
                }

                obj.ModfdByUserId = _loggedInUser.GetLoggedInUserId();
                obj.ModfdDt = DateTime.Now;

                if (obj.VndrOrdrEmailId == 0)
                {
                    var email = _repo.Create(obj);
                    if (email != null)
                    {
                        obj = email;
                        //_logger.LogInformation($" Vendor Order Email Updated. { JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                }
                else
                {
                    _repo.Update(obj.VndrOrdrEmailId, obj);
                    int count = _repo.SaveAll();

                    if (count > 0)
                    {
                        //_logger.LogInformation($" Vendor Order Email Updated. { JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString() } ");
                    }
                }

                _logger.LogInformation($" DEBUG LOGGER: Preparing for Email Sending");
                // Send Email
                VndrOrdr vendorOrder = _vndrOrdrRepo.GetById(obj.VndrOrdrId);
                if (SendEmail(vendorOrder.VndrFoldr.VndrCd, obj.FromEmailAdr, obj.ToEmailAdr, obj.CcEmailAdr, "", obj.EmailSubjTxt, obj.EmailBody, obj.VndrOrdrEmailAtchmt.ToList(), obj.VndrOrdrEmailId, _loggedInUser.GetLoggedInUserId()))
                {
                    var vendorOrderEmail = vendorOrder.VndrOrdrEmail
                        .Where(a => a.VndrOrdrEmailId == obj.VndrOrdrEmailId)
                        .FirstOrDefault();

                    return Created($"api/VendorOrderEmails/{ obj.VndrOrdrEmailId }", _mapper.Map<VendorOrderEmailViewModel>(vendorOrderEmail));
                }
            }

            return BadRequest(new { Message = "Vendor Order Email Could Not Be Updated." });
        }

        [HttpDelete("{id}")]
        public void Delete([FromRoute]int id)
        {
            _logger.LogInformation($"Delete Vendor Order Email By Id: {id}");

            _repo.Delete(id);
            var count = _repo.SaveAll();

            if (count > 0)
            {
                _logger.LogInformation($"Deleted Vendor Order Email By Id: {id}");
            }
        }

        [HttpPut("{id}/UpdateEmailAckDate")]
        public IActionResult UpdateEmailAckDate([FromRoute] int id, [FromBody] VndrOrdrMs obj)
        {
            var user = _loggedInUser.GetLoggedInUser();
            var status = _repo.UpdateEmailAckDate(id, obj.AckByVndrDt, user.UserId);
            if(status)
            {
                var list = _mapper.Map<IEnumerable<VendorOrderEmailViewModel>>(_repo
                                            .Find(a => a.VndrOrdrId == obj.VndrOrdrId)
                                            .OrderBy(s => s.VndrOrdrId));

                _logger.LogInformation($" Vendor Order Email Updated. { obj.VndrOrdrEmailId } ");

                var orderId = _vndrOrdrRepo.GetById(obj.VndrOrdrId).OrdrId;
                if (orderId != null)
                {
                    List<StateMachine> sm = new List<StateMachine>();
                    sm.Add(CreateStateMachine((int)orderId, 202, "Milestone: Order Acknowledged by Vendor Date completed."));
                    CompleteActiveTask(sm);
                }

                return Created($"api/VendorOrderEmails/{ obj.VndrOrdrEmailId }", list);
            }
            return BadRequest(new { Message = "Vendor Order Email Could Not Be Updated." });
        }

        private int CloneEmail(int VndrOrdrEmailId)
        {
            var user = _loggedInUser.GetLoggedInUser();
            int createdBy = user.UserId;
            int vndrEmailId = _repo.CloneVendorOrderEmail(VndrOrdrEmailId, "", createdBy);

            return vndrEmailId;
        }


        #region "Vendor EmailEncryption"
        public bool SendEmail(string VendorCd, string sEmailFrom, string sEmailTo,
                                            string sEmailCC, string sEmailBCC, string sSubject, string sBody,
                                            List<VndrOrdrEmailAtchmt> attachments, int iVendorOrderEmailID, int iModifiedBy)
        {
            Int16 emailStatus = Convert.ToInt16(EmailStatus.SentUnenc);
            StringBuilder sbAttachFilePath = new StringBuilder();
            bool response = false;
            List<string> lAtt = new List<string>();
            String sVendorName = string.Empty;

            var lRogers = GetVendorCodeList("lRogers").Union(GetVendorCodeList("lRogersVPN")).ToList();
            var lOrange = GetVendorCodeList("lOrange").Union(GetVendorCodeList("lOrangeVPN")).ToList();
            var lNavega = GetVendorCodeList("lNavega").Union(GetVendorCodeList("lNavegaVPN")).ToList();
            var lGCI = GetVendorCodeList("lGCI").Union(GetVendorCodeList("lGCIVPN")).ToList();
            var lBellCanada = GetVendorCodeList("lBellCanada").Union(GetVendorCodeList("lBellCanadaVPN")).ToList();
            var lChinaTelecom = GetVendorCodeList("lChinaTelecom").Union(GetVendorCodeList("lChinaTelecomVPN")).ToList();

            if (lRogers.Where(p => VendorCd.Contains(p, StringComparison.CurrentCultureIgnoreCase)).Count() > 0)
            {
                sVendorName = "Rogers";
                emailStatus = Convert.ToInt16(EmailStatus.SentEnc);
            }
            else if (lGCI.Where(p => VendorCd.Contains(p, StringComparison.CurrentCultureIgnoreCase)).Count() > 0)
            {
                sVendorName = "GCI";
                emailStatus = Convert.ToInt16(EmailStatus.SentEnc);
            }
            else if (lOrange.Where(p => VendorCd.Contains(p, StringComparison.CurrentCultureIgnoreCase)).Count() > 0)
                sVendorName = "Orange";
            else if (lNavega.Where(p => VendorCd.Contains(p, StringComparison.CurrentCultureIgnoreCase)).Count() > 0)
                sVendorName = "Navega";

            var attachPath = _configuration.GetSection("AppSettings:ExportFolder").Value;

            try
            {
                if (!Directory.Exists(attachPath))
                    Directory.CreateDirectory(attachPath);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error Occured while trying to create a directory for saving attachment files: {ex.Message.ToString()}");
            }

            if ((attachments != null) && (attachments.Count > 0))
            {
                List<KeyValuePair<string, Stream>> files = null;
                files = new List<KeyValuePair<string, Stream>>();

                foreach (VndrOrdrEmailAtchmt a in attachments)
                {
                    Stream fileContent = new MemoryStream(a.FileCntnt);
                    files.Add(new KeyValuePair<string, Stream>(a.FileNme, fileContent));
                }

                foreach (KeyValuePair<string, Stream> ve in files)
                {
                    sbAttachFilePath.Append(attachPath);

                    sbAttachFilePath.Append($"\\{ve.Key}");
                    if (WriteAttachToFile(sbAttachFilePath.ToString(), ve.Value))
                    {
                        lAtt.Add(sbAttachFilePath.ToString());
                    }
                    else
                    {
                        _logger.LogInformation($"Writing Attachment for Encryption Failed, Path: {sbAttachFilePath.ToString()}");
                    }

                    sbAttachFilePath.Clear();

                    //lAtt.Add(new KeyValuePair<string, byte[]>(ve.Key, ((MemoryStream)ve.Value).ToArray()));
                }
            }

            try
            {
                string key = sVendorName + "_EOC";
                string sOrigBody = sBody;

                if (!(sVendorName.Equals(string.Empty)))
                {
                    if (_configuration.GetSection($"AppSettings:{key}").Value != null)
                    {
                        string[] day1 = _configuration.GetSection($"AppSettings:{key}").Value.Split(',');

                        DateTime dtCert = new DateTime(Int32.Parse(day1[0]), Int32.Parse(day1[1]), Int32.Parse(day1[2]));
                        DateTime today = DateTime.Now;

                        System.TimeSpan diffResult = dtCert.Subtract(today);

                        if (diffResult.TotalDays <= 30)
                        {
                            string notice = "<font size=2 color=red> Your certificate for COWS Email Encryption will expire on[" + dtCert.ToString() + "].\n. Please provide COWS with new certifate at the earliest.\n</font>";
                            sBody = notice + sBody;
                        }
                    }
                }

                bool CanBeEncrypted = false;

                if (sEmailTo.Length > 0)
                {
                    string[] sToArr = sEmailTo.Replace(";", ",").Split(new char[] { ',' });
                    foreach (string s in sToArr)
                    {
                        if (s.ToUpper().IndexOf("SPRINT.COM") <= 0 && (s.ToUpper().IndexOf("T-MOBILE.COM") <= 0))
                            CanBeEncrypted = true;
                    }
                }
                if (sEmailCC.Length > 0)
                {
                    string[] sCCArr = sEmailCC.Replace(";", ",").Split(new char[] { ',' });
                    foreach (string s in sCCArr)
                    {
                        if (s.ToUpper().IndexOf("SPRINT.COM") <= 0 && (s.ToUpper().IndexOf("T-MOBILE.COM") <= 0))
                            CanBeEncrypted = true;
                    }
                }
                if (sEmailBCC.Length > 0)
                {
                    string[] sBCCArr = sEmailBCC.Replace(";", ",").Split(new char[] { ',' });
                    foreach (string s in sBCCArr)
                    {
                        if (s.ToUpper().IndexOf("SPRINT.COM") <= 0 && (s.ToUpper().IndexOf("T-MOBILE.COM") <= 0))
                            CanBeEncrypted = true;
                    }
                }

                if (sEmailTo.Length > 0 || sEmailCC.Length > 0 || sEmailBCC.Length > 0)
                {
                    EmailHelper emailHelper = new EmailHelper(_configuration);
                    if ((sVendorName.Length > 0) && ((CanBeEncrypted) || (_configuration.GetSection($"AppSettings:VndrEncryptFlg").Value.Equals("1"))) && (_configuration.GetSection($"AppSettings:VendorCert{sVendorName}").Value != null))
                    {
                        X509Certificate2 xc = new X509Certificate2(_configuration.GetSection($"AppSettings:VendorCert{sVendorName}").Value);
                        emailHelper.SendSingleEncryptEmail(xc, sEmailFrom, sEmailTo, sEmailCC, sEmailBCC, sSubject, sBody, lAtt);
                        UpdVendOrdEmailStatus(iVendorOrderEmailID, Convert.ToInt16(EmailStatus.SentEnc), iModifiedBy);
                    }
                    else
                    {
                        emailHelper.SendSingleEmail(sEmailFrom, sEmailTo, sEmailCC, sEmailBCC, sSubject, sOrigBody, lAtt);
                        UpdVendOrdEmailStatus(iVendorOrderEmailID, emailStatus, iModifiedBy);
                    }
                }

                response = true;
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"SendEncryptEmails: {ex.Message};");
                response = false;
            }

            return response;
        }

        # endregion "Vendor EmailEncryption"

        private string GetSystemConfig(string param)
        {
            var systemConfig = _systemConfigRepo.Find(a => a.PrmtrNme == param).SingleOrDefault();

            if (systemConfig != null)
            {
                return systemConfig.PrmtrValuTxt;
            }

            return String.Empty;
        }
        private List<string> GetVendorCodeList(string param)
        {
            var result = GetSystemConfig(param).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            .Select(s => s.Trim())
            .ToList();

            return result;
        }

        private bool WriteAttachToFile(string path, Stream contents)
        {
            try
            {
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }

                FileStream fs = new FileStream(path, FileMode.CreateNew, FileAccess.Write);
                int Length = 256;
                Byte[] buffer = new Byte[Length];
                int bytesRead = contents.Read(buffer, 0, Length);

                // write the required bytes
                while (bytesRead > 0)
                {
                    fs.Write(buffer, 0, bytesRead);
                    bytesRead = contents.Read(buffer, 0, Length);
                }
                contents.Close();
                fs.Close();

                /*StreamWriter sw = new StreamWriter(fs);
                sw.Write((contents);
                sw.Close();
                fs.Close();*/
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Vendor Order Attachment File Creation Failed. {ex.Message.ToString()}");
                //fl.WriteLogFile(fl.Event.GenericMessage, fl.MsgType.error, 0, string.Empty, ex.Message + " ; " + ex.StackTrace, string.Empty);
                return false;
            }
        }

        private void UpdVendOrdEmailStatus(int vendorOrderEmailId, short emailStatus, int modifiedBy)
        {
            VndrOrdrEmail email = _repo.GetById(vendorOrderEmailId);

            email.EmailStusId = emailStatus;
            email.SentDt = DateTime.Now;
            email.ModfdByUserId = modifiedBy;
            email.ModfdDt = DateTime.Now;

            _repo.Update(email.VndrOrdrEmailId, email);
            if (_repo.SaveAll() > 0)
            {
                _repo.CreateVendorOrderMs(email.VndrOrdrEmailId, modifiedBy);
            }
        }

        [HttpGet("AddVendorOrderEmail")]
        public IActionResult AddVendorOrderEmail([FromQuery] int vendorOrderID, [FromQuery] byte emailTypeID, [FromQuery] int vendorEmailLangID)
        {
            var user = _loggedInUser.GetLoggedInUser();
            int createdBy = user.UserId;
            string createdByFullName = user.FullNme;
            string createdByEmail = user.EmailAdr;

            string[] emailData = new string[2] { "", "" };
            try
            {
                emailData = GetVendorOrderEmailData(emailTypeID, vendorOrderID, vendorEmailLangID, createdBy, createdByFullName, createdByEmail);
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Add Vendor Order Email by VendorOrderID: { vendorOrderID } failed. { e.Message }");
            }
            int vndrEmail = _repo.AddVendorOrderEmail(vendorOrderID, emailTypeID, emailData[0], emailData[1], createdBy);

            if (vndrEmail > 0) {
                return Ok(_mapper.Map<VendorOrderEmailViewModel>(_repoVendorFolder.GetVendorOrderEmail(vndrEmail)));
            }
            return BadRequest(new { Message = "Error creating new Vendor Email." });
        }

        [HttpGet("GetVendorOrderEmails")]
        public IActionResult GetVendorOrderEmails([FromQuery] int vendorOrderID)
        {
            var list = _mapper.Map<IEnumerable<VendorOrderEmailViewModel>>(_repo
                                                                    .Find(a => a.VndrOrdrId == vendorOrderID)         
                                                                    .OrderBy(s => s.VndrOrdrId));

            return Ok(list);
        }

        private string[] GetVendorOrderEmailData(byte inVendorEmailTypeID, int inVendorOrderID, int inVendorEmailLangID,
                            int inCreatedBy, string inCreatedByFullName, string inCreatedByEmail)
        {
            //Get data needed to build email Subject and email Body
            DataSet ds = _repoVendorFolder.GetVendorOrderEmailData(inVendorOrderID, _loggedInUser.GetLoggedInUserCsgLvlId());
            string pln = string.Empty;
            string plnSeq = string.Empty;
            if (ds.Tables[2] != null)
                if (ds.Tables[2].Rows != null)
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[2].Rows)
                        {
                            pln = (dr["PLN_NME"] == null ? string.Empty : Convert.ToString(dr["PLN_NME"]));
                            plnSeq += (dr["PLN_SEQ_NBR"] == null ? string.Empty : Convert.ToString(dr["PLN_SEQ_NBR"])) + ", ";
                        }

                        plnSeq = plnSeq.Remove(plnSeq.LastIndexOf(","), 1);
                    }
            string emailSubject = GetVendorOrderEmailSubject(inVendorEmailTypeID, pln, ds);
            StringBuilder emailBody = new StringBuilder();
            emailBody.Append(GetVendorOrderEmailBody(inVendorEmailTypeID, inVendorOrderID, pln, plnSeq, ds, inCreatedByFullName, inCreatedByEmail));
            if (inVendorEmailLangID > 0)
            {
                LkVndrEmailLang language = GetVendorEmailLanguage(inVendorEmailLangID);
                emailBody.Append(Environment.NewLine);
                emailBody.Append(Environment.NewLine);
                emailBody.Append(Environment.NewLine);
                emailBody.Append(Environment.NewLine);
                emailBody.Append(Environment.NewLine);
                emailBody.Append(language.LangNme);
            }
            string[] emailData = new string[] { emailSubject, emailBody.ToString() };
            return emailData;
        }

        private string GetVendorOrderEmailSubject(byte inVendorEmailTypeID, string inPLNumber, DataSet ds)
        {
            StringBuilder subject = new StringBuilder();
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    //FTN details
                    if (ds.Tables[0] != null)
                    {
                        subject.Append(GetSubjectPartOrder(inVendorEmailTypeID, inPLNumber, ds.Tables[0]));
                    }
                }
            }
            return subject.ToString();
        }

        private string GetSubjectPartOrder(byte inVendorEmailTypeID, string inPLNumber, DataTable dt)
        {
            StringBuilder subject = new StringBuilder();
            if (dt != null)
            {
                if (dt.Rows != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        string country = (dr["CTRY_NME"] == null ? string.Empty : Convert.ToString(dr["CTRY_NME"]));
                        string ftn = "FTN ";
                        bool isIPL = false;
                        if (dr["ISIPL"].ToString() == "1")
                            isIPL = true;
                        if (isIPL)
                            ftn = ftn.Replace("FTN ", "CTN ");
                        ftn += (dr["FTN"] == null ? string.Empty : Convert.ToString(dr["FTN"]));
                        if (inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.ASR))
                        {
                            subject.Append(dr["FTN"] == null ? string.Empty : Convert.ToString(dr["FTN"]));
                            subject.Append("-");
                            subject.Append(ASRDetail);
                        }
                        else if ((inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.VendOrdConfirmation))
                            || (inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.VendOrdDiscConfirmation)))
                        {
                            if (isIPL)
                                subject.Append("IPL");

                            //subject.Append(dr["FTN"] == null ? string.Empty : Convert.ToString(dr["FTN"]));
                            else
                                subject.Append(country);
                            subject.Append(inPLNumber.Length > 0 ? "-" : "");
                            subject.Append(inPLNumber);
                            if (inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.VendOrdConfirmation))
                            {
                                string orderDesc = dr["ORDR_TYPE_DES"] == null ? string.Empty : Convert.ToString(dr["ORDR_TYPE_DES"]);
                                subject.Append(orderDesc.Length > 0 ? "-" : "");
                                subject.Append(orderDesc);
                                subject.Append("-");
                                subject.Append(VendorOrderConfirmation);
                            }
                            else if (inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.VendOrdDiscConfirmation))
                            {
                                subject.Append("-");
                                subject.Append("Disco");
                                subject.Append("-");
                                subject.Append(VendorOrderDisconnectConfirmation);
                            }
                        }
                        else
                        {
                            string cust = (dr["CUST_NME"] == null ? string.Empty : Convert.ToString(dr["CUST_NME"])); ;
                            string city = (dr["CTY_NME"] == null ? string.Empty : Convert.ToString(dr["CTY_NME"]));
                            subject.Append(ftn);
                            subject.Append(cust.Length > 0 ? "-" : "");
                            subject.Append(cust);
                            subject.Append(city.Length > 0 ? "-" : "");
                            subject.Append(city);
                            subject.Append(country.Length > 0 ? "-" : "");
                            subject.Append(country);
                        }
                    }
                }
            }
            return subject.ToString();
        }

        private string GetVendorOrderEmailBody(byte inVendorEmailTypeID, int inVendorOrderID, string inPLNumber,
                                string inPLNumberSeq, DataSet ds, string userName, string userEmail)
        {
            StringBuilder emailBody = new StringBuilder();
            if (ds.Tables != null)
            {
                if (ds.Tables.Count > 1)
                {
                    bool isIPL = (ds.Tables[0].Rows[0]["ISIPL"].ToString() == "1" ? true : false);
                    string FTN = ds.Tables[0].Rows[0]["FTN"].ToString();
                    string cirOrig = "Vendor Circuit ID: ";
                    string cirTerm = "Vendor Circuit ID: ";
                    string quote = "Vendor Quote Number: ";
                    string targetDelDateOrig = "Target Delivery Date: ";
                    string targetDelDateTerm = "Target Delivery Date: ";
                    string discConfDateOrig = "Vendor Disconnect Confirmation Date: ";
                    string discConfDateTerm = "Vendor Disconnect Confirmation Date: ";

                    //Get Circuit details
                    if (ds.Tables[1] != null)
                    {
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[1].Rows)
                            {
                                quote += (DBNull.Value.Equals(dr["QOT_NBR"]) ? "" : dr["QOT_NBR"] + ", ");
                                if (dr["TRMTG_CD"].ToString().ToLower() == "0")
                                {
                                    cirOrig += (DBNull.Value.Equals(dr["VNDR_CKT_ID"]) ? "" : dr["VNDR_CKT_ID"] + ", ");
                                    targetDelDateOrig += (DBNull.Value.Equals(dr["TRGT_DLVRY_DT"]) ? "" : Convert.ToDateTime(dr["TRGT_DLVRY_DT"]).ToShortDateString() + ", ");
                                    discConfDateOrig += (DBNull.Value.Equals(dr["VNDR_CNFRM_DSCNCT_DT"]) ? "" : dr["VNDR_CNFRM_DSCNCT_DT"] + ", ");
                                }
                                else
                                {
                                    cirTerm += (DBNull.Value.Equals(dr["VNDR_CKT_ID"]) ? "" : dr["VNDR_CKT_ID"] + ", ");
                                    targetDelDateTerm += (DBNull.Value.Equals(dr["TRGT_DLVRY_DT"]) ? "" : Convert.ToDateTime(dr["TRGT_DLVRY_DT"]).ToShortDateString() + ", ");
                                    discConfDateTerm += (DBNull.Value.Equals(dr["VNDR_CNFRM_DSCNCT_DT"]) ? "" : dr["VNDR_CNFRM_DSCNCT_DT"] + ", ");
                                }
                            }
                            if (quote.IndexOf(",") > 0)
                                quote = quote.Remove(quote.LastIndexOf(","), 1);
                            if (cirOrig.IndexOf(",") > 0)
                                cirOrig = cirOrig.Remove(cirOrig.LastIndexOf(","), 1);
                            if (cirTerm.IndexOf(",") > 0)
                                cirTerm = cirTerm.Remove(cirTerm.LastIndexOf(","), 1);
                            if (targetDelDateOrig.IndexOf(",") > 0)
                                targetDelDateOrig = targetDelDateOrig.Remove(targetDelDateOrig.LastIndexOf(","), 1);
                            if (targetDelDateTerm.IndexOf(",") > 0)
                                targetDelDateTerm = targetDelDateTerm.Remove(targetDelDateTerm.LastIndexOf(","), 1);
                            if (discConfDateOrig.IndexOf(",") > 0)
                                discConfDateOrig = discConfDateOrig.Remove(discConfDateOrig.LastIndexOf(","), 1);
                            if (discConfDateTerm.IndexOf(",") > 0)
                                discConfDateTerm = discConfDateTerm.Remove(discConfDateTerm.LastIndexOf(","), 1);
                        }
                    }

                    if ((inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.VendOrdConfirmation))
                        || (inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.VendOrdDiscConfirmation)))
                    {
                        string vendOrig = "";
                        string vendTerm = "";
                        string vendOrdNoOrig = "";
                        string vendOrdNoTerm = "";
                        string rfqOrig = "";
                        string rfqTerm = "";

                        #region FTN
                        if (isIPL)
                            emailBody.Append("CTN: " + FTN + Environment.NewLine);
                        else
                            emailBody.Append("FTN: " + FTN + Environment.NewLine); //1
                        #endregion

                        if (ds.Tables[0] != null)
                            if (ds.Tables[0].Rows != null)
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    if (isIPL)
                                    {
                                        DataRow[] drOrig = ds.Tables[0].Select("TRMTG_CD = 0");
                                        if (drOrig != null)
                                            if (drOrig.Length > 0)
                                            {
                                                DataRow dr = drOrig[0];
                                                if ((Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.MPLSOffnet))
                                                || (Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.SLFROffnet))
                                                || (Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.DIAOffnet)))
                                                    vendOrig = dr["OFFNET_VNDR_NME"] == null ? string.Empty : Convert.ToString(dr["OFFNET_VNDR_NME"]);
                                                else
                                                    vendOrig = dr["ONNET_VNDR_NME"] == null ? string.Empty : Convert.ToString(dr["ONNET_VNDR_NME"]);
                                                vendOrdNoOrig = GetVON(DBNull.Value.Equals(dr["VNDR_ORDR_ID"]) ? 0 : Convert.ToInt32(dr["VNDR_ORDR_ID"]));
                                                rfqOrig = dr["TSUP_RFQ_NBR"] == null ? string.Empty : Convert.ToString(dr["TSUP_RFQ_NBR"]);
                                            }
                                        DataRow[] drTerm = ds.Tables[0].Select("TRMTG_CD = 1");
                                        if (drTerm != null)
                                            if (drTerm.Length > 0)
                                            {
                                                DataRow dr = drTerm[0];
                                                if ((Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.MPLSOffnet))
                                                || (Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.SLFROffnet))
                                                || (Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.DIAOffnet)))
                                                    vendTerm = dr["OFFNET_VNDR_NME"] == null ? string.Empty : Convert.ToString(dr["OFFNET_VNDR_NME"]);
                                                else
                                                    vendTerm = dr["ONNET_VNDR_NME"] == null ? string.Empty : Convert.ToString(dr["ONNET_VNDR_NME"]);
                                                vendOrdNoTerm = GetVON(DBNull.Value.Equals(dr["VNDR_ORDR_ID"]) ? 0 : Convert.ToInt32(dr["VNDR_ORDR_ID"]));
                                                rfqTerm = dr["TSUP_RFQ_NBR"] == null ? string.Empty : Convert.ToString(dr["TSUP_RFQ_NBR"]);
                                            }
                                        emailBody.Append("Originating Vendor Name: ");
                                        emailBody.Append(vendOrig);
                                        emailBody.Append(Environment.NewLine);
                                        emailBody.Append("Terminating Vendor Name: ");
                                        emailBody.Append(vendTerm);
                                        emailBody.Append(Environment.NewLine);
                                        if (inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.VendOrdConfirmation))
                                        {
                                            emailBody.Append("Originating ");
                                            emailBody.Append(cirOrig);
                                            emailBody.Append(Environment.NewLine);
                                            emailBody.Append("Terminating ");
                                            emailBody.Append(cirTerm);
                                            emailBody.Append(Environment.NewLine);
                                        }
                                        emailBody.Append("Originating Vendor Order Number: ");
                                        emailBody.Append(vendOrdNoOrig);
                                        emailBody.Append(Environment.NewLine);
                                        emailBody.Append("Terminating Vendor Order Number: ");
                                        emailBody.Append(vendOrdNoTerm);
                                        emailBody.Append(Environment.NewLine);
                                        if (inVendorEmailTypeID == Convert.ToByte(VendorEmailTypes.VendOrdConfirmation))
                                        {
                                            emailBody.Append("Originating RFQ#: ");
                                            emailBody.Append(rfqOrig);
                                            emailBody.Append(Environment.NewLine);
                                            emailBody.Append("Terminating RFQ#: ");
                                            emailBody.Append(rfqTerm);
                                            emailBody.Append(Environment.NewLine);
                                            emailBody.Append("Originating ");
                                            emailBody.Append(targetDelDateOrig);
                                            emailBody.Append(Environment.NewLine);
                                            emailBody.Append("Terminating ");
                                            emailBody.Append(targetDelDateTerm);
                                            emailBody.Append(Environment.NewLine);
                                        }
                                        else
                                        {
                                            emailBody.Append("Originating ");
                                            emailBody.Append(discConfDateOrig);
                                            emailBody.Append(Environment.NewLine);
                                            emailBody.Append("Terminating ");
                                            emailBody.Append(discConfDateTerm);
                                            emailBody.Append(Environment.NewLine);
                                        }
                                    }
                                    else
                                    {
                                        #region VENDOR NAME
                                        emailBody.Append("Vendor Name: "); //4
                                        DataRow dr = ds.Tables[0].Rows[0];
                                        if ((Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.MPLSOffnet))
                                            || (Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.SLFROffnet))
                                            || (Convert.ToByte(dr["PROD_TYPE_ID"]) == Convert.ToByte(OrderProduct.DIAOffnet)))
                                            emailBody.Append(dr["OFFNET_VNDR_NME"] == null ? string.Empty : Convert.ToString(dr["OFFNET_VNDR_NME"]));
                                        else
                                            emailBody.Append(dr["ONNET_VNDR_NME"] == null ? string.Empty : Convert.ToString(dr["ONNET_VNDR_NME"]));
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region VENDOR ORDER NUMBER
                                        emailBody.Append("Vendor Order Number: "); //6
                                        emailBody.Append(GetVON(inVendorOrderID));
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region VENDOR ORDER ID (RLTD_VNDR_ORDR_ID)
                                        emailBody.Append("Vendor Order ID: ");
                                        emailBody.Append(ds.Tables[1].Rows[0]["RLTD_VNDR_ORDR_ID"].ToString());
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region VENDOR CIRCUIT ID
                                        emailBody.Append(cirOrig); //5
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region VENDOR LEC ID
                                        emailBody.Append("Vendor LEC ID: ");
                                        emailBody.Append(ds.Tables[1].Rows[0]["LEC_ID"].ToString());
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region VENDOR VLAN ID
                                        emailBody.Append("Vendor VLAN ID: ");
                                        emailBody.Append(ds.Tables[3].Rows[0]["VLAN_ID"].ToString());
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region TARGET DELIVERY DATE
                                        emailBody.Append(targetDelDateOrig);
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region TARGET DELIVERY DATE RECEIVED
                                        emailBody.Append("Target Delivery Date Received: ");
                                        emailBody.Append(DBNull.Value.Equals(ds.Tables[1].Rows[0]["TRGT_DLVRY_DT_RECV_DT"]) ? "" : Convert.ToDateTime(ds.Tables[1].Rows[0]["TRGT_DLVRY_DT_RECV_DT"]).ToShortDateString());
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region ADDITIONAL FOC NOTES
                                        emailBody.Append("Additional FOC Notes: ");
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region FOR DEDICATED ONLY
                                        emailBody.Append("For Dedicated Only: ");
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region CROSS CONNECT PROVIDER
                                        emailBody.Append("Cross-connect Provider: ");
                                        emailBody.Append(ds.Tables[1].Rows[0]["X_CNNCT_PRVDR"].ToString());
                                        emailBody.Append(Environment.NewLine);
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region TIE DOWN ASSIGNMENTS
                                        emailBody.Append("Tie Down Assignments: ");
                                        emailBody.Append(ds.Tables[1].Rows[0]["T_DWN_ASSN"].ToString());
                                        emailBody.Append(Environment.NewLine);
                                        emailBody.Append(Environment.NewLine);
                                        #endregion
                                        #region TIE DOWNS REQUIRED
                                        emailBody.Append("Tie Downs Required: ");
                                        emailBody.Append(ds.Tables[1].Rows[0]["T_DWN_REQ"].ToString());
                                        emailBody.Append(Environment.NewLine);
                                        #endregion

                                    }
                                }
                    }
                    else if (inVendorEmailTypeID != Convert.ToByte(VendorEmailTypes.ASR))
                    {
                        string cust = "Customer Name: ";
                        string orderno = "Order Number: ";
                        string pl = "T-Mobile FMS PL Number: " + inPLNumber;
                        string sprintFTN = "T-Mobile FTN: ";

                        //Get FTN details
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow dr = ds.Tables[0].Rows[0];
                                if (dr["ISIPL"].ToString() == "1")
                                    sprintFTN = "T-Mobile CTN: ";
                                cust += (dr["CUST_NME"] == null ? string.Empty : Convert.ToString(dr["CUST_NME"]));
                                sprintFTN += dr["FTN"].ToString();
                            }
                        }
                        emailBody.Append(cust);
                        emailBody.Append(Environment.NewLine);
                        emailBody.Append(quote);
                        emailBody.Append(Environment.NewLine);
                        emailBody.Append(orderno);
                        emailBody.Append(GetVON(inVendorOrderID));
                        emailBody.Append(Environment.NewLine);
                        emailBody.Append(cirOrig);
                        emailBody.Append(Environment.NewLine);
                        emailBody.Append(pl);
                        emailBody.Append(Environment.NewLine);
                        emailBody.Append(sprintFTN);
                        emailBody.Append(Environment.NewLine);

                        //User details
                        emailBody.Append("T-Mobile Contact: ");
                        emailBody.Append(userName);
                        emailBody.Append(", Email - ");
                        emailBody.Append(userEmail);
                        emailBody.Append(Environment.NewLine);
                    }
                }
            }

            return emailBody.ToString();
        }

        public static string GetVON(int inVendorOrder)
        {
            string von = "VON";
            return von.Insert(3, inVendorOrder.ToString().PadLeft(10, '0'));
        }
        private LkVndrEmailLang GetVendorEmailLanguage(int inVendorEmailLangID)
        {
            var response = _repoVendorEmailLanguage.GetById(inVendorEmailLangID);
            if (response != null)
                _repoVendorEmailLanguage.ApplyLanguageRules(response);

            return response;
        }

        private StateMachine CreateStateMachine(int orderId, int taskId, string comments = null)
        {
            StateMachine sm = new StateMachine();
            sm.OrderId = orderId;
            sm.TaskId = taskId;
            sm.UserId = _loggedInUser.GetLoggedInUserId();
            sm.Comments = (comments != null ? comments : $"Milestone: { GetMilestoneInfo(taskId) } completed.");
            //if (sComments != null)
            //    sm.sComments = sComments;//"Bypassing Vendor Order Milestones.";
            //else
            //    sm.sComments = bOrdrCncld == false ? "Milestone: " + GetxNCIMilestoneInfo(iTaskID) + " completed." : "Cancel Milestone: " + GetxNCIMilestoneInfo(iTaskID) + " completed.";
            return sm;
        }

        private string GetMilestoneInfo(int taskId)
        {
            try
            {
                switch (taskId)
                {
                    case (int)Tasks.xNCIOrderValidatedDate:
                        return "Order Validated Date";
                    case (int)Tasks.xNCIOrderSenttoVendorDate:
                        return "Order Sent to Vendor Date";
                    case (int)Tasks.xNCIOrderAcknowledgedbyVendorDate:
                        return "Order Acknowledged by Vendor Date";
                    case (int)Tasks.xNCIDisconnectCompletedDate:
                        return "Disconnect Completed Date";
                    case (int)Tasks.xNCITargetDeliveryDate:
                        return "Target Delivery Date";
                    case (int)Tasks.xNCITargetDeliveryDateReceivedDate:
                        return "Target Delivery Date Received Date";
                    case (int)Tasks.xNCIAccessDeliveryDate:
                        return "Access Delivery Date";
                    case (int)Tasks.xNCIAccessAcceptedDate:
                        return "Access Accepted Date";
                    case (int)Tasks.xNCIVendorConfirmDisconnectDate:
                        return "Vendor Confirmed Disconnect Date";
                    case (int)Tasks.xNCIOrderInstallDate:
                        return "Order Install Date";
                    case (int)Tasks.xNCICnclReady:
                        return "Vendor Cancellation Date";
                    case (int)Tasks.xNCICloseDate:
                        return "Close Date";
                    case (int)Tasks.xNCIBillClearInstallDate:
                        return "Bill Clear Date";
                    default:
                        return "No Milestone Info";
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"GetMilestoneInfo Failed. { ex.Message };");
                throw ex;
            }
        }

        private void CompleteActiveTask(IEnumerable<StateMachine> list)
        {
            foreach (StateMachine sm in list)
            {
                bool isActive = _activeTaskRepo
                    .Find(a => a.OrdrId == sm.OrderId && a.TaskId == sm.TaskId)
                    .Any(a => a.StusId == 0);

                if (isActive)
                {
                    _activeTaskRepo.CompleteActiveTask(sm);

                    if (sm.TaskId == 203 || sm.TaskId == 207)
                    {
                        _nrmRepo.SendASRMessageToNRMBPM(sm.OrderId, sm.TaskId == 203 ? "FOC" : "BILL_CLEAR_DT", 1);

                        var orderNote = new OrdrNte
                        {
                            OrdrId = sm.OrderId,
                            NteTypeId = 11,
                            CreatByUserId = _loggedInUser.GetLoggedInUserId(),
                            CreatDt = DateTime.Now
                        };

                        if (sm.TaskId == 203)
                        {
                            // Insert Notes for FOC
                            orderNote.NteTxt = "FOC Messages sent back to NRM BPM";
                        }
                        else
                        {
                            // Insert Notes for Bill Clear
                            orderNote.NteTxt = "Bill clear message sent back to NRM BPM";
                        }

                        _orderNoteRepo.CreateNote(orderNote);
                    }
                }
            }
        }
    }
}
