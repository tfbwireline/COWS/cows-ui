﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/OrderLock")]
    [ApiController]
    public class OrderLockContoller : ControllerBase
    {
        private readonly IOrderLockRepository _repo;
        private readonly ILogger<OrderLockContoller> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;

        public OrderLockContoller(IMapper mapper,
                               IOrderLockRepository repo,
                               ILogger<OrderLockContoller> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
        }

        [HttpGet("CheckLock/{id}")]
        public IActionResult CheckLock([FromRoute] int id)
        {
            _logger.LogInformation($"Check Order Lock by Id: { id }.");

            var obj = _repo.CheckLock(id);
            if (obj != null && _loggedInUser.GetLoggedInUserId() == obj.LockByUserId)
                return Ok();

            return Ok(_mapper.Map<OrderLockViewModel>(obj));
        }

        [HttpGet("Lock/{id}")]
        public IActionResult Lock([FromRoute] int id)
        {
            _logger.LogInformation($"Lock Order by Id: { id }.");

            _repo.Lock(new OrdrRecLock
            {
                OrdrId = id,
                LockByUserId = _loggedInUser.GetLoggedInUserId(),
                StrtRecLockTmst = DateTime.Now
            });

            return Ok();
        }

        [HttpGet("Unlock/{id}")]
        public IActionResult Unlock([FromRoute] int id)
        {
            _logger.LogInformation($"Unlock Order by Id: { id }.");

            _repo.Unlock(id, _loggedInUser.GetLoggedInUserId());
            return Ok();
        }

        [HttpGet("LockUnlockOrdersEvents")]
        public IActionResult LockUnlockOrdersEvents([FromQuery] int orderId, [FromQuery] int eventId, [FromQuery] int userId, [FromQuery] bool isOrder, [FromQuery] bool unlock, [FromQuery] int isLocked)
        {
            return Ok(_repo.LockUnlockOrdersEvents(orderId, eventId, userId, isOrder, unlock, isLocked).Result);
        }
    }
}