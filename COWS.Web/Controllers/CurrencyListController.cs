﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CurrencyList")]
    [ApiController]
    public class CurrencyListController : ControllerBase
    {
        private readonly ICurrencyListRepository _repo;
        private readonly ILogger<CurrencyListController> _logger;
        private readonly IMapper _mapper;

        public CurrencyListController(IMapper mapper,
                               ICurrencyListRepository repo,
                               ILogger<CurrencyListController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CurrencyListViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<CurrencyListViewModel>>(_repo
                                                                .GetAll()
                                                                .OrderBy(s => s.CurId));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] string id)
        {
            _logger.LogInformation($"Search Currency by Id: { id }.");

            var obj = _repo.Find(s => s.CurId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<CurrencyListViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Currency by Id: { id } not found.");
                return NotFound(new { Message = $"Currency by Id: { id } not found." });
            }
        }

        [HttpGet("GetCurrencyList")]
        public IActionResult GetCurrencyList()
        {
            List<LkCur> cacHist = _repo.GetCurrencyList("c.CurNme", null);
            return Ok(cacHist);
        }
    }
}