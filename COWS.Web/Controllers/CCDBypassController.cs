﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Models;
using COWS.Web.Library.Extensions;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using COWS.Entities.Enums;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/CCDBypass")]
    [ApiController]
    public class CCDBypassController : ControllerBase
    {
        private readonly ICCDBypassRepository _CCDBypassRepo;
        private readonly ILogger<CCDBypassController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;
        private readonly ILoggedInUserService _loggedInUser;

        public CCDBypassController(IMapper mapper,
                               ICCDBypassRepository CCDBypassRepo,
                               ILogger<CCDBypassController> logger,
                               IMemoryCache memoryCache,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _CCDBypassRepo = CCDBypassRepo;
            _logger = logger;
            _cache = memoryCache;
            _loggedInUser = loggedInUser;
        }

        [HttpGet("GetBypassUserList")]
        public ActionResult<IEnumerable<CCDBypassUserViewModel>> GetBypassUserList()
        {
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            IEnumerable<CCDBypassUserViewModel> events;
            events = _mapper.Map<IEnumerable<CCDBypassUserViewModel>>(_CCDBypassRepo.GetBypassUserList(loggedInUser.UserId));
            return Ok(events);
        }

        [HttpGet("findUser/{searchString}")]
        public ActionResult<IEnumerable<UserViewModel>> findUser(string searchString)
        {
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            IEnumerable<UserViewModel> events;
            events = _mapper.Map<IEnumerable<UserViewModel>>(_CCDBypassRepo.FindUser(searchString, loggedInUser.UserId));
            return Ok(events);
        }

        [HttpPost]
        public IActionResult Post([FromBody] CCDBypassUserViewModel model)
        {
            _logger.LogInformation($"Insert CCD Bypass User: { model.UserName }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<CCDBypassUserView>(model);

                // Remove this comment once logged in user service is available
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                //if (loggedInUser != null)
                //{
                //    obj.UserADID = model.UserADID;
                //    obj.ConfigID = 0;
                //    obj.UserName = "";
                //}
                obj.UserADID = model.UserADID;
                IEnumerable<CCDBypassUserViewModel> events;
                var rep = _CCDBypassRepo.CreateCCD(obj);
                if (rep != 0)
                {
                    // Update Cache
                    events = _mapper.Map<IEnumerable<CCDBypassUserViewModel>>(_CCDBypassRepo.GetBypassUserList(loggedInUser.UserId));

                    CacheManager.Set(_cache, CacheKeys.CCDBypassModelList, events);

                    _logger.LogInformation($"Inserted CCD Bypass User. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/CCDBypass/{ rep }", model);
                }
            }

            return BadRequest(new { Message = "Insert CCD Bypass User Could Not Be completed." });
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Delete CCD Bypass User  CfgID: { id }.");

            //var ccd = _mapper.Map<IEnumerable<LkSysCfg>>(_CCDBypassRepo.GetBySYSCFGId(id));
            //var obj = _mapper.Map<LkTelco>(Telco.SingleOrDefault());

            // Remove this comment once logged in user service is available
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            //if (loggedInUser != null)
            //{
            //    obj.ModfdByUserId = loggedInUser.UserId;
            //    obj.ModfdDt = DateTime.Now;
            //}
            //obj.RecStusId = 0;
            _CCDBypassRepo.Delete(id);
            // Update Cache
            var list = _mapper.Map<IEnumerable<CCDBypassUserViewModel>>(_CCDBypassRepo.GetBypassUserList(loggedInUser.UserId));

            CacheManager.Set(_cache, CacheKeys.CCDBypassModelList, list);
            _logger.LogInformation($"CCD Bypass User  CfgID { id } Deleted.");
        }

        //[HttpGet("")]
        ////public ActionResult<IEnumerable<TimeSlotOccurrenceViewModel>> GetTimeSlotOccurrences(short eventID)
        //public IActionResult Get([FromQuery] short eventID, [FromQuery] bool bShowAfterHrsSlots)
        //{
        //    IEnumerable<TimeSlotOccurrenceViewModel> timeSlots;

        //    DateTime startDate = new DateTime();
        //    DateTime endDate = new DateTime();

        //    switch (eventID)
        //    {
        //        case 5:
        //            startDate = DateTime.Now.AddDays(-1);
        //            endDate = DateTime.Now.AddMonths(1);
        //            break;

        //        case 9:
        //            startDate = DateTime.Now.AddDays(-1);
        //            endDate = DateTime.Now.AddDays(7 * 6);
        //            break;

        //        default:
        //            return BadRequest(new { Message = "Event Type does not have a date range set up." });
        //    }

        //    timeSlots = _mapper.Map<IEnumerable<TimeSlotOccurrenceViewModel>>(_CCDBypassRepo.GetAll(eventID, startDate, endDate, bShowAfterHrsSlots)).ToList();
        //    return Ok(timeSlots);
        //}

        //[HttpGet("getFTEventsForTS/{resourceID}")]
        //public ActionResult<IEnumerable<FTAvailEventViewModel>> GetFTEventsForTS(int resourceID)
        //{
        //    IEnumerable<FTAvailEventViewModel> events;
        //    events = _mapper.Map<IEnumerable<FTAvailEventViewModel>>(_CCDBypassRepo.GetFTEventsForTS(resourceID));

        //    return Ok(events);
        //}

        //[HttpGet("getFedEventsForTS/{resourceID}")]
        //public ActionResult<IEnumerable<FedlineManageUserEventViewModel>> getFedEventsForTS(int resourceID)
        //{
        //    IEnumerable<FedlineManageUserEventViewModel> events;
        //    events = _mapper.Map<IEnumerable<FedlineManageUserEventViewModel>>(_CCDBypassRepo.GetFedEventsForTS(resourceID));

        //    return Ok(events);
        //}

        //[HttpGet("getInvalidTSEvents")]
        //public ActionResult<IEnumerable<FedlineManageUserEventViewModel>> GetInvalidTSEvents()
        //{
        //    IEnumerable<FedlineManageUserEventViewModel> events;
        //    events = _mapper.Map<IEnumerable<FedlineManageUserEventViewModel>>(_CCDBypassRepo.GetInvalidTSEvents());

        //    return Ok(events);
        //}

        //[HttpGet("getDisconnectEvents")]
        //public ActionResult<IEnumerable<FedlineManageUserEventViewModel>> GetDisconnectEvents()
        //{
        //    IEnumerable<FedlineManageUserEventViewModel> events;
        //    events = _mapper.Map<IEnumerable<FedlineManageUserEventViewModel>>(_CCDBypassRepo.GetDisconnectEvents());

        //    return Ok(events);
        //}

        //[HttpGet("updateResourceAvailability/{updResultset}")]
        //public void UpdateResourceAvailability(string updResultset)
        //{
        //    var loggedInUser = _loggedInUser.GetLoggedInUser();

        //    _CCDBypassRepo.UpdateResourceAvailability(updResultset, loggedInUser.UserId);
        //}

        //[HttpGet("checkUserAccessToAfterHrsSlots/{adid}")]
        //public bool CheckUserAccessToAfterHrsSlots(string adid)
        //{
        //   return _CCDBypassRepo.GetBypassUserList(adid);
        //}

        [HttpGet("GetCCDHistory")]
        public IActionResult GetCCDHistory([FromQuery] int orderID)
        {
            IEnumerable<CCDHistoryViewModel> ccd = _mapper.Map<IEnumerable<CCDHistoryViewModel>>(_CCDBypassRepo.GetCCDHistory(orderID));
            return Ok(ccd);
        }
    }
}