﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/DCPE")]
    [ApiController]
    public class DCPEController : ControllerBase
    {
        private readonly IDCPERepository _repo;
        private readonly ILogger<DCPEController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;
        private IMemoryCache _cache;
        private readonly IOrderNoteRepository _orderNoteRepo;
        private readonly IWorkGroupRepository _workGroupRepo;
        private readonly IWFMRepository _wfmRepo;

        public DCPEController(IMapper mapper,
                               IDCPERepository repo,
                               ILogger<DCPEController> logger,
                               ILoggedInUserService loggedInUser,
                               IMemoryCache cache,
                               IOrderNoteRepository orderNoteRepo,
                               IWorkGroupRepository workGroupRepo,
                               IWFMRepository wfmRepo)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
            _cache = cache;
            _orderNoteRepo = orderNoteRepo;
            _workGroupRepo = workGroupRepo;
            _wfmRepo = wfmRepo;
        }


        [HttpGet("GetFTNData")]
        public IActionResult GetFTNData([FromQuery] int orderId, [FromQuery] int usrPrfId)
        {
            var adid = _loggedInUser.GetLoggedInUserAdid();
            return Ok(_repo.GetFTNData(orderId, usrPrfId, adid));
        }

        [HttpGet("GetCPEOrderInfo_V5U")]
        public IActionResult GetCPEOrderInfo_V5U([FromQuery] int orderId)
        {
            var userAdid = _loggedInUser.GetLoggedInUser().UserAdid;
            var userCsgLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            return Ok(_repo.GetCPEOrderInfo_V5U(orderId, userAdid, userCsgLevel).Result);
        }

        [HttpGet("GetTasksList")]
        public IActionResult GetTasksList()
        {
            return Ok(_repo.GetTasksList());
        }

        [HttpGet("GetJeopardyCodeList")]
        public IActionResult GetJeopardyCodeList()
        {
            return Ok(_repo.GetJeopardyCodeList());
        }

        [HttpGet("GetCpeAcctCntct_V5U")]
        public IActionResult GetCpeAcctCntct_V5U([FromQuery] int orderId)
        {
            var userCsgLevel = _loggedInUser.GetLoggedInUserCsgLvlId();
            return Ok(_repo.GetCpeAcctCntct_V5U(orderId, userCsgLevel).Result);
        }

        [HttpGet("GetAssignedTechList")]
        public IActionResult GetAssignedTechList()
        {
            return Ok(_repo.GetAssignedTechList());
        }

        [HttpGet("GetTechAssignment_V5U")]
        public IActionResult GetTechAssignment_V5U([FromQuery] int orderId)
        {
            return Ok(_repo.GetTechAssignment_V5U(orderId));
        }

        [HttpGet("GetCPELineItems_V5U")]
        public IActionResult GetCPELineItems_V5U([FromQuery] int orderId)
        {
            return Ok(_repo.GetCPELineItems_V5U(orderId).Result);
        }

        [HttpGet("GetEquipOnlyInfo")]
        public IActionResult GetEquipOnlyInfo([FromQuery] int orderId)
        {
            return Ok(_repo.GetEquipOnlyInfo(orderId));
        }

        [HttpGet("GetRequisitionNumber_V5U")]
        public IActionResult GetRequisitionNumber_V5U([FromQuery] int orderId)
        {
            return Ok(_repo.GetRequisitionNumber_V5U(orderId).Result);
        }

        [HttpGet("GetReqHeaderQueueByOrderID")]
        public IActionResult GetReqHeaderQueueByOrderID([FromQuery] int orderId)
        {
            return Ok(_repo.GetReqHeaderQueueByOrderID(orderId));
        }

        [HttpGet("GetCpeReqHeaderAcctCodes_V5U")]
        public IActionResult GetCpeReqHeaderAcctCodes_V5U([FromQuery] int orderId)
        {
            return Ok(_repo.GetCpeReqHeaderAcctCodes_V5U(orderId).Result);
        }

        [HttpGet("GetCPEPidByOrderID")]
        public IActionResult GetCPEPidByOrderID([FromQuery] int orderId)
        {
            return Ok(_repo.GetCPEPidByOrderID(orderId));
        }

        [HttpGet("GetCPEReqLineItems_V5U")]
        public IActionResult GetCPEReqLineItems_V5U([FromQuery] int orderId)
        {
            return Ok(_repo.GetCPEReqLineItems_V5U(orderId).Result);
        }

        [HttpGet("GetVendorsList")]
        public IActionResult GetVendorsList()
        {
            return Ok(_repo.GetVendorsList());
        }

        [HttpGet("GetDomesticClliCode")]
        public IActionResult GetDomesticClliCode()
        {
            return Ok(_repo.GetDomesticClliCode());
        }

        [HttpGet("GetInternationalClliCode")]
        public IActionResult GetInternationalClliCode()
        {
            return Ok(_repo.GetInternationalClliCode());
        }

        [HttpGet("GetCpeCmplLineItems_V5U")]
        public IActionResult GetCpeCmplLineItems_V5U([FromQuery] int orderId)
        {
            return Ok(_repo.GetCpeCmplLineItems_V5U(orderId).Result);
        }

        [HttpGet("InsertODIEReq_V5U")]
        public IActionResult InsertODIEReq_V5U([FromQuery] int orderId)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.InsertODIEReq_V5U(orderId);
                if (rep != 0)
                {
                    _logger.LogInformation("InsertODIEReq_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "InsertODIEReq_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "InsertODIEReq_V5U Failed." });
        }

        [HttpGet("InsertDmstcStndMatReq_V5U")]
        public IActionResult InsertDmstcStndMatReq_V5U([FromQuery] int orderId)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.InsertDmstcStndMatReq_V5U(orderId);
                if (rep != 0)
                {
                    _logger.LogInformation("InsertDmstcStndMatReq_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "InsertDmstcStndMatReq_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "InsertDmstcStndMatReq_V5U Failed." });
        }

        [HttpGet("InsertSSTATReq")]
        public IActionResult InsertSSTATReq([FromQuery] int orderId, [FromQuery] int msgId, [FromQuery] string ftn)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.InsertSSTATReq(orderId, msgId, ftn);
                if (rep != 0)
                {
                    _logger.LogInformation("InsertSSTATReq Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "InsertSSTATReq Failed." });
                }
            }
            return BadRequest(new { Message = "InsertSSTATReq Failed." });
        }

        [HttpGet("InsertEmailRequest_V5U")]
        public IActionResult InsertEmailRequest_V5U([FromQuery] int orderId, [FromQuery] string deviceID, [FromQuery] int emailReqTypeID, [FromQuery] int statusID, [FromQuery] string fTN, [FromQuery] string primaryEmail, [FromQuery] string secondaryEmail, [FromQuery] string emailNote, [FromQuery] string emailJeopardyNote)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.InsertEmailRequest_V5U(orderId, deviceID, emailReqTypeID, statusID, fTN, primaryEmail, secondaryEmail, emailNote, emailJeopardyNote);
                if (rep != 0)
                {
                    _logger.LogInformation("InsertEmailRequest_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "InsertEmailRequest_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "InsertEmailRequest_V5U Failed." });
        }

        [HttpGet("InsertRTS_V5U")]
        public IActionResult InsertRTS_V5U([FromQuery] int orderId, [FromQuery] string deviceID, [FromQuery] string jeopardyCode, [FromQuery] string note)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.InsertRTS_V5U(orderId, deviceID, jeopardyCode, note);
                if (rep != 0)
                {
                    _logger.LogInformation("InsertRTS_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "InsertRTS_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "InsertRTS_V5U Failed." });
        }

        [HttpGet("UpdateCPEOrdr_V5U")]
        public IActionResult UpdateCPEOrdr_V5U([FromQuery] int orderId, [FromQuery] int modType, [FromQuery] string adid, [FromQuery] string jCode, [FromQuery] string comments, [FromQuery] string clli)
        {
            if (ModelState.IsValid)
            {
                if (comments == null)
                    comments = "";

                var rep = _repo.UpdateCPEOrdr_V5U(orderId, modType, adid, jCode, comments, clli);
                if (rep != 0)
                {
                    _logger.LogInformation("UpdateCPEOrdr_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "UpdateCPEOrdr_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "UpdateCPEOrdr_V5U Failed." });
        }

        [HttpGet("UpdateDmstcWFM_V5U")]
        public IActionResult UpdateDmstcWFM_V5U([FromQuery] int orderId, [FromQuery] string adid, [FromQuery] string assignerADID, [FromQuery] string comments)
        {
            if (ModelState.IsValid)
            {
                if (comments == null)
                    comments = "";

                var rep = _repo.UpdateDmstcWFM_V5U(orderId, adid, assignerADID, comments);
                if (rep != 0)
                {
                    _logger.LogInformation("UpdateDmstcWFM_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "UpdateDmstcWFM_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "UpdateDmstcWFM_V5U Failed." });
        }

        [HttpGet("ProcessCPEMve_V5U")]
        public IActionResult ProcessCPEMve_V5U([FromQuery] int orderId, [FromQuery] string adid, [FromQuery] string taskName, [FromQuery] string comments)
        {
            if (ModelState.IsValid)
            {
                if (comments == null)
                    comments = "";

                var rep = _repo.ProcessCPEMve_V5U(orderId, adid, taskName, comments);
                if (rep != 0)
                {
                    _logger.LogInformation("ProcessCPEMve_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "ProcessCPEMve_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "ProcessCPEMve_V5U Failed." });
        }

        [HttpPost("InsertReqLineItem_V5U")]
        public IActionResult InsertReqLineItem_V5U([FromBody] PsReqLineItmQueue obj)
        {
            if (ModelState.IsValid)
            {
                obj.CreatDt = DateTime.Now;

                var rep = _repo.InsertReqLineItem_V5U(obj);
                if (rep != 0)
                {
                    _logger.LogInformation($"Req Line Item Queue Created. {  JsonConvert.SerializeObject(obj).ToString() } ");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "Could Not Insert Req Line Item Queue." });
                }
            }

            return BadRequest(new { Message = "Req Line Item Queue Could Not Be Created." });
        }

        [HttpPost("InsertRequisitionHeader_V5U")]
        public IActionResult InsertRequisitionHeader_V5U([FromBody] PsReqHdrQueue obj)
        {
            if (ModelState.IsValid)
            {
                obj.CreatDt = DateTime.Now;

                var rep = _repo.InsertRequisitionHeader_V5U(obj);
                if (rep != 0)
                {
                    _logger.LogInformation($"Req Header Queue Created. {  JsonConvert.SerializeObject(obj).ToString() } ");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "Could Not Insert Req Header Queue." });
                }
            }

            return BadRequest(new { Message = "Req Header Queue Could Not Be Created." });
        }

        [HttpGet("InsertEqpRecCmpl_V5U")]
        public IActionResult InsertEqpRecCmpl_V5U([FromQuery] string adid, [FromQuery] int orderId)
        {
            var response = _repo.InsertEqpRecCmpl_V5U(adid, orderId);
            if (response != 0)
            {
                _logger.LogInformation($"InsertEqpRecCmpl_V5U Success");
                return Ok(true);
            }
            else
            {
                return BadRequest(new { Message = "Could Not InsertEqpRecCmpl_V5U." });
            }
        }

        [HttpGet("InsertEqpReceipt_V5U")]
        public IActionResult InsertEqpReceipt_V5U([FromQuery] int fsaCpeLineItemId, [FromQuery] int partialQty, [FromQuery] string action, [FromQuery] string adid, [FromQuery] int orderId)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.InsertEqpReceipt_V5U(fsaCpeLineItemId, partialQty, action, adid, orderId);
                if (rep != 0)
                {
                    _logger.LogInformation("InsertEqpReceipt_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "InsertEqpReceipt_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "InsertEqpReceipt_V5U Failed." });
        }

        [HttpPost("InsertTechAssignment_V5U")]
        public IActionResult InsertTechAssignment_V5U([FromBody] OrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.InsertTechAssignment_V5U(model.OrderId, model.Tech, model.DispatchTime, model.EventID, model.AssignedADID);
                if (rep != 0)
                {
                    _logger.LogInformation("InsertTechAssignment_V5U Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "InsertTechAssignment_V5U Failed." });
                }
            }
            return BadRequest(new { Message = "InsertTechAssignment_V5U Failed." });
        }

        [HttpPost("InsertOrdrCmplPartial_V5U")]
        public IActionResult InsertOrdrCmplPartial_V5U([FromBody] OrderViewModel model)
        {
            var response = _repo.InsertOrdrCmplPartial_V5U(model.FsaCpeLineItemId, Convert.ToDateTime(model.CompleteDate)); // Added date convertion due to nullable type
            if (response != 0)
            {
                _logger.LogInformation($"InsertOrdrCmplPartial_V5U Success");
                return Ok(true);
            }
            else
            {
                return BadRequest(new { Message = "InsertOrdrCmplPartial_V5U Failed." });
            }
        }

        [HttpPost("InsertOrdrCmplAll_V5U")]
        public IActionResult InsertOrdrCmplAll_V5U([FromBody] OrderViewModel model)
        {
            var response = _repo.InsertOrdrCmplAll_V5U(model.OrderId, model.DeviceID, model.NoteText, model.Tech, model.InRouteDate, model.OnSiteDate, model.CompleteDate);
            if (response != 0)
            {
                _logger.LogInformation($"InsertOrdrCmplAll_V5U Success");
                return Ok(true);
            }
            else
            {
                return BadRequest(new { Message = "InsertOrdrCmplAll_V5U Failed." });
            }
        }

        #region Equipment Review
        [HttpPost("CompleteODIETask")]
        public IActionResult CompleteODIETask([FromBody] OrderViewModel model)
        {
            if (model.TaskId != 0)
            {
                if (model.NoteText != "")
                {
                    var insertNote = InsertNote(model.OrderId, 6, model.NoteText);

                    if (!insertNote)
                    {
                        return BadRequest(false);
                    }
                }
                var insertODIEReq = _repo.InsertODIEReq_V5U(model.OrderId);

                if (insertODIEReq == 0)
                {
                    return BadRequest(false);
                }
                else
                {
                    if (model.OrderType == "INST")
                    {
                        if (model.ChkSMR)
                        {
                            var insertDmstcStndMateq = _repo.InsertDmstcStndMatReq_V5U(model.OrderId);
                            if (insertDmstcStndMateq == 0)
                            {
                                return BadRequest(false);
                            }
                            else
                            {
                                var insertNote = InsertNote(model.OrderId, 6, "Standard Material Req requested");

                                if (!insertNote)
                                {
                                    return BadRequest(false);
                                }
                            }
                        }
                    }

                    var sNote = String.Empty;
                    if (model.TaskStatus == 2)
                        sNote = "Move the order from Equipment Review pending to Equipment Review Complete.";
                    else if (model.TaskStatus == 1)
                        sNote = "Move the order from Equipment Review to RTS(Sales Support) workgroup.";

                    var completeActiveTask = _workGroupRepo.CompleteActiveTask(model.OrderId, model.TaskId, Convert.ToInt16(model.TaskStatus), sNote, model.UserId);

                    if (completeActiveTask != 0)
                    {
                        if (model.OrderType == "DISC")
                        {
                            var insertSSTATReq = _repo.InsertSSTATReq(model.OrderId, 1, model.FTN);

                            if (insertSSTATReq == 0)
                            {
                                return BadRequest(false);
                            }
                        }
                        else
                        {
                            if (model.ChkSMR || model.ChkNMR)
                            {
                                var completeTask = _workGroupRepo.CompleteActiveTask(model.OrderId, model.TaskId, Convert.ToInt16(model.TaskStatus), "No Matl Req selected.  Auto Move thru Matl Req", model.UserId);

                                if (completeTask != 0)
                                    return Ok(true);
                                else
                                    return BadRequest(false);

                            }
                        }
                        return Ok(true);
                    }
                    else
                    {
                        return BadRequest(false);
                    }
                }
            }
            else
            {
                return BadRequest(new { Message = "An error occurred converting the selected task into its corresponding id" });
            }
        }

        private bool InsertNote(int orderId, byte nteType, string nte)
        {
            var note = new OrderNoteViewModel
            {
                OrdrId = orderId,
                NteTypeId = nteType,
                NteTxt = nte
            };

            var obj = _mapper.Map<OrdrNte>(note);
            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.CreatByUserId = loggedInUser.UserId;
                obj.CreatDt = DateTime.Now;
            }

            var insertNote = _orderNoteRepo.Create(obj);

            if (insertNote == null)
                return false;
            else
                return true;
        }
        #endregion

        #region Material Requisition
        [HttpPost("CompleteMatReq")]
        public IActionResult CompleteMatReq([FromBody] OrderViewModel model)
        {
            if (SendCompleteMatReq(model))
                return Ok(true);
            else
                return BadRequest(new { Message = "An error occurred while Sending Material Requisition." });
        }

        private bool SaveNotesAndComplete(OrderViewModel model)
        {
            if (model.ShippingInstructions != "")
            {
                var insertNotes = InsertNote(model.OrderId, 6, "Shipping PO Instructions: " + model.ShippingInstructions);
                if (!insertNotes)
                    return false;
            }

            if (model.NoteText != "")
            {
                var insertNotes = InsertNote(model.OrderId, 6, model.NoteText);
                if (!insertNotes)
                    return false;
            }

            if (model.SendAndComplete)
            {
                var completeActiveTask = _workGroupRepo.CompleteActiveTask(model.OrderId, model.TaskId, Convert.ToInt16(model.TaskStatus), "Material Requisitioning Completed", model.UserId);
                if (completeActiveTask == 0)
                {
                    return false;
                }
            }
            return true;
        }

        private bool SendCompleteMatReq(OrderViewModel model)
        {
            var selectedMatReqLineItemKeys = model.PSReqLineItemQueue;

            if (selectedMatReqLineItemKeys != null && selectedMatReqLineItemKeys.Count > 0)
            {
                int i = 0;
                foreach (var item in selectedMatReqLineItemKeys)
                {
                    var insertReqLineItem = _repo.InsertReqLineItem_V5U(item);
                    if (insertReqLineItem != 0)
                    {
                        if (i == 0)
                        {
                            string clli = "";
                            if (model.DomesticCD)
                                clli = model.InternationalCLLICd;
                            else
                                clli = model.DomesticCLLICd;

                            if (model.ChkShipAddr)
                            {
                                var insertRequisitionHeader = _repo.InsertRequisitionHeader_V5U(model.DeliveryAddress);
                                if (insertRequisitionHeader != 0)
                                {
                                    if (!SaveNotesAndComplete(model))
                                        return false;
                                }
                            }
                            else
                            {
                                var insertRequisitionHeader = _repo.InsertRequisitionHeader_V5U(model.InstallAddress);
                                if (insertRequisitionHeader != 0)
                                {
                                    if (!SaveNotesAndComplete(model))
                                        return false;
                                }
                            }
                        }
                    }
                    else
                        return false;

                    i += 1;
                }
                return true;
            }
            else
            {
                var insertNotes = InsertNote(model.OrderId, 6, "No items were selected to send to PS.");
                if (insertNotes)
                {
                    if (!String.IsNullOrEmpty(model.NoteText))
                    {
                        var insertNotes1 = InsertNote(model.OrderId, 6, model.NoteText);
                        if (!insertNotes1)
                            return false;
                    }

                    if (model.SendAndComplete)
                    {
                        var completeTask = _workGroupRepo.CompleteActiveTask(model.OrderId, model.TaskId, 2, "Material Requisitioning Completed", model.UserId);
                        if (completeTask == 0)
                            return false;
                    }
                }
            }
            return true;
        }
        #endregion

        #region Equipment Receipts
        [HttpPost("CompleteEquipReceipts")]
        public IActionResult CompleteEquipReceipts([FromBody] OrderViewModel model)
        {
            if (CompleteERTask(model))
                return Ok(true);
            else
                return BadRequest(new { Message = "An error occurred while Complete Equipment Receipts." });
        }

        private bool UpdateERSelectedLineItems(OrderViewModel model)
        {
            var updateSelectedItems = model.EquipmentReceiptsLineItems;

            foreach (var item in updateSelectedItems)
            {
                var insertEqpReceipt = _repo.InsertEqpReceipt_V5U(item.FSACPELineItemID, item.PartialQuantity, model.Action, model.UserADID, model.OrderId);

                if (insertEqpReceipt == 0)
                {

                    return false;
                }
            }
            return true;
        }

        private bool CompleteERTask(OrderViewModel model)
        {
            if (UpdateERSelectedLineItems(model))
            {
                if (!String.IsNullOrEmpty(model.NoteText))
                {
                    var insertNotes1 = InsertNote(model.OrderId, 6, model.NoteText);
                    if (!insertNotes1)
                        return false;
                }
            }

            if (model.TaskId != 0)
            {
                if (model.Action == "C")
                {
                    string sNote = "Equip Receipts Completed";
                    var insertEqpRecCmpl = _repo.InsertEqpRecCmpl_V5U(model.UserADID, model.OrderId);

                    if (insertEqpRecCmpl == 0)
                        return false;

                    var completeTask = _workGroupRepo.CompleteActiveTask(model.OrderId, model.TaskId, 2, sNote, model.UserId);
                    if (completeTask != 0)
                        if (model.VoiceOrder == "N")
                        {
                            var insertSSTATReq = _repo.InsertSSTATReq(model.OrderId, 1, model.FTN);
                            if (insertSSTATReq == 0)
                                return false;
                        }
                }
                return true;
            }
            else
                return false;
        }
        #endregion

        #region Order Complete
        [HttpPost("OrderComplete")]
        public IActionResult OrderComplete([FromBody] OrderViewModel model)
        {
            if (OrderCompleteTask(model))
                return Ok(true);
            else
                return BadRequest(new { Message = "An error occurred while Order Completion." });
        }

        private bool OrderCompleteTask(OrderViewModel model)
        {
            if (UpdateOCSelectedLineItems(model))
            {
                if (!String.IsNullOrEmpty(model.NoteText))
                {
                    var insertNotes1 = InsertNote(model.OrderId, 6, model.NoteText);
                    if (!insertNotes1)
                        return false;
                }

                if (model.Action == "C")
                {
                    if (model.ThirdPartySite == "Y")
                    {
                        var insertOrdrCmplAll = _repo.InsertOrdrCmplAll_V5U(model.OrderId, model.DeviceID, "V5U Note: Order Complete in BPM.", model.Tech, model.InRouteDate, model.OnSiteDate, model.CompleteDate);
                        if (insertOrdrCmplAll == 0)
                            return false;
                    }
                    else
                    {
                        var insertOrdrCmplAll = _repo.InsertOrdrCmplAll_V5U(model.OrderId, model.DeviceID, "V5U Note: Order Complete in BPM.", "", null, null, null);
                        if (insertOrdrCmplAll == 0)
                            return false;
                    }
                }
            }
            return true;
        }
        private bool UpdateOCSelectedLineItems(OrderViewModel model)
        {
            var selectedItems = model.OrderCompletionLineItems;

            foreach (var item in selectedItems)
            {
                var insertOrdrCmplPartial = _repo.InsertOrdrCmplPartial_V5U(item.FSACPELineItemID, item.CompleteDate);

                if (insertOrdrCmplPartial == 0)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        [HttpPost("RTSClick")]
        public IActionResult RTSClick([FromBody] OrderViewModel model)
        {
            if (RTSTask(model))
                return Ok(true);
            else
                return BadRequest(new { Message = "An error occurred while RTSClick." });
        }

        private bool RTSTask(OrderViewModel model)
        {
            bool insertNotesSuccess;
            if (!string.IsNullOrEmpty(model.NoteText))
                insertNotesSuccess = InsertNote(model.OrderId, 6, model.NoteText);
            else
                insertNotesSuccess = InsertNote(model.OrderId, 26, model.JeopardyCode);

            if (insertNotesSuccess)
            {
                if (model.ChkEmailPrimaryIPM)
                {
                    if (model.ChkEmailPrimaryIS)
                        _repo.InsertEmailRequest_V5U(model.OrderId, model.DeviceID, 31, 10, model.FTN, model.PrimaryEmail, model.SecondaryEmail, model.NoteText, model.JeopardyCode);
                    else
                        _repo.InsertEmailRequest_V5U(model.OrderId, model.DeviceID, 31, 10, model.FTN, model.PrimaryEmail, null, model.NoteText, model.JeopardyCode);
                }

                var insertRTSSuccess = _repo.InsertRTS_V5U(model.OrderId, model.DeviceID, model.JeopardyCode, model.NoteText);

                if (insertRTSSuccess != 0)
                {
                    var insertSSTATReqSuccess = _repo.InsertSSTATReq(model.OrderId, 4, model.FTN);
                    if (insertSSTATReqSuccess != 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }

        #region GOM Order Complete
        [HttpPost("CompleteGOMTask")]
        public IActionResult CompleteGOMTask([FromBody] OrderViewModel model)
        {
            string sTaskName = string.Empty;
            switch (model.TaskId)
            {
                case (Int16)Tasks.GOMPreSubmitReady:
                    sTaskName = "GOM Pre Submit Ready";
                    break;

                case (Int16)Tasks.GOMError:
                    sTaskName = "GOM Error";
                    break;

                case (Int16)Tasks.GOMReview:
                    sTaskName = "GOM CPE Milestones";
                    break;

                case (Int16)Tasks.GOMPL:
                    sTaskName = "GOM Private Line";
                    break;

                case (Int16)Tasks.GOMSubmitReady:
                    sTaskName = "GOM Submit Ready";
                    break;

                case (Int16)Tasks.GOMCCDReady:
                    sTaskName = "GOM CCD Ready";
                    break;

                case (Int16)Tasks.GOMCCDPL:
                    sTaskName = "GOM CCD Private Line";
                    break;

                case (Int16)Tasks.GOMCancelReady:
                    sTaskName = "GOM Cancel Ready";
                    break;

                case (Int16)Tasks.GOMMissingBillingOrders:
                    sTaskName = "GOM Missing Billing Orders";
                    break;

                case (Int16)Tasks.GOMInitiateBillingOrder:
                    sTaskName = "GOM Initiate Billing Order";
                    break;

                case (Int16)Tasks.GOMIPLIPASR:
                    sTaskName = "GOM IPL+IP ASR";
                    break;

                default:
                    break;
            }

            string _Note = "Move the order out from " + sTaskName + " - Pending to Complete state.";
            if (model.TaskId != 0 && model.TaskId != 110)
                return Ok(_workGroupRepo.CompleteActiveTask(model.OrderId, model.TaskId, Convert.ToInt16(model.TaskStatus), _Note, model.UserId));
            else if (model.TaskId == 110)
            {
                InsertNote(model.OrderId, 10, _Note);
                return Ok(_workGroupRepo.CompleteGOMIBillTask(model.OrderId));
            }
            else
                return Ok(1);   //Task doesn't exist, close order like it was before
        }

        [HttpPost("SaveGOMSpecificData")]
        public IActionResult SaveGOMSpecificData([FromBody] OrderViewModel model)
        {
            bool noteInserted = false;
            if (_workGroupRepo.IsOrderCompleted(model.OrderId))
            {
                if (model.NoteText != string.Empty)
                    noteInserted = InsertNote(model.OrderId, 10, model.NoteText);
            }
            string _Notes = model.NoteText;
            var items = _mapper.Map<List<CPEOrdrUpdInfo>>(model.GOMItems);

            _workGroupRepo.SaveCPEData(model.OrderId, items, model.UserId, (int)WorkGroup.GOM);
            _workGroupRepo.InsertUpdateGOMInfo(model.OrderId, model.UserId, model.CPEStatusId, model.CpeEquipmentType, model.CompleteGOMDate, model.ReqAmount);

            if (_Notes != string.Empty && !noteInserted)
            {
                if (model.TaskId == 112)
                {
                    InsertNote(model.OrderId, 22, _Notes.Replace(Environment.NewLine, "</br>"));
                }
                else
                {
                    InsertNote(model.OrderId, 10, _Notes);
                }
            }
            return Ok(true);
            //if (hfSelectedRow.Value.ToString() != "")
            //{
            //    int iRowIndex = Convert.ToInt32(hfSelectedRow.Value.ToString());
            //    GridView grdTNs = (GridView)ucFTNs.FindControl("grdTNs");
            //    PlaceHolder pOrdTNs = (PlaceHolder)grdTNs.Rows[iRowIndex].FindControl("phFSA");
            //    _FTNs.OpenFSAData(iRowIndex, OrderID, hfSelectedFTN.Value.ToString(), true);
            //}

            //lblMessage.Text = "GOM info has been updated!";
        }
        [HttpPost("SaveAMNCISpecificData")]
        public IActionResult SaveAMNCISpecificData([FromBody] OrderViewModel model)
        {
            UpdateUserAssignmentColorCode(model.OrderId, model.WorkGroup, model.UserId);
            //if (_workGroupRepo.IsOrderCompleted(model.OrderId))
            //{
            //    if (model.NoteText != string.Empty)
            //        InsertNote(model.OrderId, 11, model.NoteText);
            //}
            string _Notes = model.NoteText;
            var items = _mapper.Map<List<CPEOrdrUpdInfo>>(model.GOMItems);

            _workGroupRepo.SaveCPEData(model.OrderId, items, model.UserId, (int)WorkGroup.xNCIAmerica);
            _workGroupRepo.InsertUpdateAMNCIInfo(model.OrderId, model.UserId, model.CPEStatusId, model.CpeEquipmentType, model.CompleteDate, model.ReqAmount);

            if (_Notes != string.Empty)
            {
                if (model.TaskId == 112)
                {
                    InsertNote(model.OrderId, 22, _Notes.Replace(Environment.NewLine, "</br>"));
                }
                else
                {
                    InsertNote(model.OrderId, 11, _Notes);
                }
            }
            return Ok(true);
            //if (hfSelectedRow.Value.ToString() != "")
            //{
            //    int iRowIndex = Convert.ToInt32(hfSelectedRow.Value.ToString());
            //    GridView grdTNs = (GridView)ucFTNs.FindControl("grdTNs");
            //    PlaceHolder pOrdTNs = (PlaceHolder)grdTNs.Rows[iRowIndex].FindControl("phFSA");
            //    _FTNs.OpenFSAData(iRowIndex, OrderID, hfSelectedFTN.Value.ToString(), true);
            //}

            //lblMessage.Text = "GOM info has been updated!";
        }
        [HttpPost("GOMRTSClick")]
        public IActionResult GOMRTSClick([FromBody] OrderViewModel model)
        {
            var success = InsertNote(model.OrderId, 10, "GOM user initiated RTS on this order.");

            if (model.NoteText != string.Empty)
            {
                return Ok(InsertNote(model.OrderId, 21, model.NoteText));
            }
            return Ok(success);
        }
        #endregion

        [HttpGet("IsNIDDevice")]
        public IActionResult IsNIDDevice([FromQuery] int orderId, [FromQuery] string deviceID)
        {
            if (ModelState.IsValid)
            {
                var rep = _repo.IsNIDDevice(orderId, deviceID);
                if (rep)
                {
                    _logger.LogInformation("IsNIDDevice Success");
                    return Ok(true);
                }
                else
                {
                    return Ok(false);
                }
            }
            return BadRequest(new { Message = "IsNIDDevice Failed." });
        }

        [HttpGet("IsNIDAlreadyUsed")]
        public IActionResult IsNIDAlreadyUsed([FromQuery] int orderId, [FromQuery] string nidSerialNbr)
        {
            if (ModelState.IsValid)
            {
                var isAlreadyUsed = _repo.IsNIDAlreadyUsed(orderId, nidSerialNbr);
                _logger.LogInformation("IsNIDAlreadyUsed Success");

                return Ok(isAlreadyUsed);
            }

            return BadRequest(new { Message = "IsNIDAlreadyUsed Failed." });
        }

        [HttpGet("UpdateNIDSerialNbr")]
        public IActionResult UpdateNIDSerialNbr([FromQuery] int orderId, [FromQuery] string ftn, [FromQuery] string deviceId, [FromQuery] string nidSerialNbr)
        {
            if (ModelState.IsValid)
            {
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                var rep = _repo.UpdateNIDSerialNbr(orderId, ftn, deviceId, nidSerialNbr, loggedInUser.UserId);
                if (rep != 0)
                {
                    _logger.LogInformation("UpdateNIDSerialNbr Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "UpdateNIDSerialNbr Failed." });
                }
            }
            return BadRequest(new { Message = "UpdateNIDSerialNbr Failed." });
        }

        [HttpGet("UpdateNuaCkt")]
        public IActionResult UpdateNuaCkt([FromQuery] int orderId, [FromQuery] string nuaCkt)
        {
            if (ModelState.IsValid)
            {
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                var rep = _repo.UpdateNuaCkt(orderId, nuaCkt, loggedInUser.UserAdid);
                if (rep != 0)
                {
                    _logger.LogInformation("UpdateNuaCkt Success");
                    return Ok(true);
                }
                else
                {
                    return BadRequest(new { Message = "UpdateNuaCkt Failed." });
                }
            }
            return BadRequest(new { Message = "UpdateNuaCkt Failed." });
        }

        private void UpdateUserAssignmentColorCode(int iOrderID, int iWGID, int iUserID)
        {
            if (iWGID == 2) { iWGID = 14; }
            if (iWGID == 100) { iWGID = 112; }
            if (iWGID == 16) { iWGID = 28; }

            if (_wfmRepo.GetUserAssignmentColorCD(iOrderID, iWGID, iUserID))
            {
                _wfmRepo.UpdateUserAssignmentColorCD(iOrderID, iWGID, iUserID);
            }
        }

        [HttpGet("GetShippingInstr")]
        public IActionResult GetShippingInstr([FromQuery] int orderId)
        {
            return Ok(_repo.GetShippingInstr(orderId).Result);
        }
    }
}