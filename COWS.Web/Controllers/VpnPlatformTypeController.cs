﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/VpnPlatformTypes")]
    [ApiController]
    public class VpnPlatformTypeController : ControllerBase
    {
        private readonly IVpnPlatformTypeRepository _repo;
        private readonly ILogger<VpnPlatformTypeController> _logger;
        private readonly IMapper _mapper;

        public VpnPlatformTypeController(IMapper mapper,
                               IVpnPlatformTypeRepository repo,
                               ILogger<VpnPlatformTypeController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VpnPlatformTypeViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<VpnPlatformTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.VpnPltfrmTypeDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search VPN Platform Type by Id: { id }.");

            var obj = _repo.Find(s => s.VpnPltfrmTypeId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<VpnPlatformTypeViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"VPN Platform Type by Id: { id } not found.");
                return NotFound(new { Message = $"VPN Platform Type by Id: { id } not found." });
            }
        }

        [HttpGet("getByEventId/{id}")]
        public ActionResult<IEnumerable<VpnPlatformTypeViewModel>> getByEventId([FromRoute] int id)
        {
            var list = _mapper.Map<IEnumerable<VpnPlatformTypeViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active
                                                                    && s.EventTypeId == id)
                                                                .OrderBy(s => s.VpnPltfrmTypeDes));
            return Ok(list);
        }
    }
}