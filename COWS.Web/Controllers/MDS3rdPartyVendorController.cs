﻿using AutoMapper;
using COWS.Data.Extensions;
using COWS.Data.Interface;
using COWS.Entities.Enums;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MDS3rdPartyVendors")]
    [ApiController]
    public class MDS3rdPartyVendorController : ControllerBase
    {
        private readonly IMDS3rdPartyVendorRepository _repo;
        private readonly ILogger<MDS3rdPartyVendorController> _logger;
        private readonly IMapper _mapper;
        private readonly ILoggedInUserService _loggedInUser;

        public MDS3rdPartyVendorController(IMapper mapper,
                               IMDS3rdPartyVendorRepository repo,
                               ILogger<MDS3rdPartyVendorController> logger,
                               ILoggedInUserService loggedInUser)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
            _loggedInUser = loggedInUser;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MDS3rdPartyVendorViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<MDS3rdPartyVendorViewModel>>(_repo
                                                                .Find(s => s.RecStusId == (byte)ERecStatus.Active)
                                                                .OrderBy(s => s.ThrdPartyVndrDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search MDS 3rd Party Vendor by Id: { id }.");

            var obj = _repo.Find(s => s.ThrdPartyVndrId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<MDS3rdPartyVendorViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"MDS 3rd Party Vendor by Id: { id } not found.");
                return NotFound(new { Message = $"MDS 3rd Party Vendor Id: { id } not found." });
            }
        }

        [HttpGet("GetForLookup")]
        public ActionResult<IEnumerable<MDS3rdPartyVendorViewModel>> GetForLookup()
        {
            var list = _mapper.Map<IEnumerable<MDS3rdPartyVendorViewModel>>(_repo.GetAll()
                                                                .OrderBy(s => s.ThrdPartyVndrDes));
            return Ok(list);
        }

        [HttpPost]
        public IActionResult Post([FromBody] MDS3rdPartyVendorViewModel model)
        {
            _logger.LogInformation($"Create MDS 3rd Party Vendor: { model.ThrdPartyVndrDes }.");

            if (ModelState.IsValid)
            {
                var obj = _mapper.Map<LkMds3rdpartyVndr>(model);

                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.CreatByUserId = loggedInUser.UserId;
                    obj.CreatDt = DateTime.Now;
                }

                // Added by Sarah Sandoval [20190909]
                // Added condition to check if name is duplicate since most Admin pages
                // don't delete actual db record but update RecStusId to Active/Inactive
                var newData = new LkMds3rdpartyVndr();
                var duplicate = _repo.Find(i =>
                    i.ThrdPartyVndrDes.Trim().ToLower() == obj.ThrdPartyVndrDes.Trim().ToLower()
                        && i.SrvcTypeId == obj.SrvcTypeId).SingleOrDefault();
                if (duplicate != null)
                {
                    // Throw duplicate error if name already exists and RecStusId is Active
                    if (duplicate.RecStusId == (byte)ERecStatus.Active)
                    {
                        return BadRequest(new { Message = obj.ThrdPartyVndrDes + " already exists for Service Type " + duplicate.SrvcType.SrvcTypeDes + "." });
                    }
                    else
                    {
                        // Update RecStusId to Active
                        newData = duplicate;
                        if (loggedInUser != null)
                        {
                            newData.SrvcTypeId = obj.SrvcTypeId;
                            newData.ModfdByUserId = loggedInUser.UserId;
                            newData.ModfdDt = DateTime.Now;
                            newData.RecStusId = (byte)ERecStatus.Active;
                        }

                        _repo.Update(newData.ThrdPartyVndrId, newData);
                    }
                }
                else
                {
                    newData = _repo.Create(obj);
                }

                if (newData != null)
                {
                    _logger.LogInformation($"MDS 3rd Party Vendor Created. { JsonConvert.SerializeObject(model).ToString() } ");
                    return Created($"api/MDS3rdPartyVendors/{ newData.ThrdPartyVndrId }", model);
                }
            }

            return BadRequest(new { Message = "MDS 3rd Party Vendor Could Not Be Created." });
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, [FromBody] MDS3rdPartyVendorViewModel model)
        {
            _logger.LogInformation($"Update MDS 3rd Party Vendor Id: { id }.");

            var obj = _mapper.Map<LkMds3rdpartyVndr>(model);

            var loggedInUser = _loggedInUser.GetLoggedInUser();
            if (loggedInUser != null)
            {
                obj.ModfdByUserId = loggedInUser.UserId;
                obj.ModfdDt = DateTime.Now;
                obj.RecStusId = (byte)ERecStatus.Active;
            }

            // Added by Sarah Sandoval [20190909]
            // Added condition to check if name is duplicate since most Admin pages
            // don't delete actual db record but update RecStusId to Active/Inactive
            var duplicate = _repo.Find(i =>
                i.ThrdPartyVndrDes.Trim().ToLower() == obj.ThrdPartyVndrDes.Trim().ToLower()
                    && i.SrvcTypeId == obj.SrvcTypeId).SingleOrDefault();
            if (duplicate != null)
            {
                // Throw duplicate error if name already exists and RecStusId is Active
                if (duplicate.RecStusId == (byte)ERecStatus.Active)
                {
                    return BadRequest(new { Message = obj.ThrdPartyVndrDes + " already exists for Service Type " + duplicate.SrvcType.SrvcTypeDes + "." });
                }
                else
                {
                    // Delete duplicate inactive record
                    _repo.Delete(duplicate.ThrdPartyVndrId);
                }
            }

            _repo.Update(id, obj);
            
            _logger.LogInformation($"MDS 3rd Party Vendor Updated. { JsonConvert.SerializeObject(model) } ");
            return Created($"api/MDS3rdPartyVendors/{ id }", model);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.LogInformation($"Deactivating MDS 3rd Party Vendor by Id: { id }.");

            var srvc = _repo.Find(s => s.ThrdPartyVndrId == id);
            if (srvc != null)
            {
                var obj = _mapper.Map<LkMds3rdpartyVndr>(srvc.SingleOrDefault());
                var loggedInUser = _loggedInUser.GetLoggedInUser();
                if (loggedInUser != null)
                {
                    obj.ModfdByUserId = loggedInUser.UserId;
                    obj.ModfdDt = DateTime.Now;
                    obj.RecStusId = (byte)ERecStatus.InActive;
                }

                _repo.Update(id, obj);

                _logger.LogInformation($"MDS 3rd Party Vendor by Id: { id } Deactivated.");
            }
            else
            {
                _logger.LogInformation($"Deactivating record failed due to MDS 3rd Party Vendor by Id: { id } not found.");
            }
        }
    }
}
