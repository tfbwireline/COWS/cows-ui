﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Regions")]
    [ApiController]
    public class RegionController : ControllerBase
    {
        private readonly IRegionRepository _repo;
        private readonly ILogger<RegionController> _logger;
        private readonly IMapper _mapper;

        public RegionController(IMapper mapper,
                               IRegionRepository repo,
                               ILogger<RegionController> logger)
        {
            _mapper = mapper;
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<RegionViewModel>> Get()
        {
            var list = _mapper.Map<IEnumerable<RegionViewModel>>(_repo
                                                                .Find(s => s.RecStusId == 1)
                                                                .OrderBy(s => s.RgnDes));
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            _logger.LogInformation($"Search Region by Id: { id }.");

            var obj = _repo.Find(s => s.RgnId == id);
            if (obj != null)
            {
                return Ok(_mapper.Map<RegionViewModel>(obj.SingleOrDefault()));
            }
            else
            {
                _logger.LogInformation($"Region by Id: { id } not found.");
                return NotFound(new { Message = $"Region Id: { id } not found." });
            }
        }
    }
}