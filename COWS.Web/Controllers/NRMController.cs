﻿using AutoMapper;
using COWS.Data.Interface;
using COWS.Web.Library.Extensions;
using COWS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace COWS.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/NRM")]
    [ApiController]
    public class NRMController : ControllerBase
    {
        private readonly INRMRepository _nrmRepo;
        private readonly ILogger<NRMController> _logger;
        private readonly IMapper _mapper;
        private IMemoryCache _cache;
        private readonly IOrderRepository _repoOrder;

        public NRMController(IMapper mapper,
                               INRMRepository nrmRepo,
                               ILogger<NRMController> logger,
                               IMemoryCache memoryCache,
                               IOrderRepository repoOrder)
        {
            _mapper = mapper;
            _nrmRepo = nrmRepo;
            _logger = logger;
            _cache = memoryCache;
            _repoOrder = repoOrder;
        }

        [HttpGet("GetNRMServiceInstanceList")]
        public IActionResult GetNRMServiceInstanceList(int orderId)
        {
            var orderDetails = _repoOrder.GetById(orderId);
            string ftn = orderDetails.FsaOrdr.Ftn;

            var data = _nrmRepo.GetNRMServiceInstanceList(ftn);
            return Ok(data);
        }

        [HttpGet("GetNRMCircuitList")]
        public IActionResult GetNRMCircuitList(int orderId)
        {
            var orderDetails = _repoOrder.GetById(orderId);
            string ftn = orderDetails.FsaOrdr.Ftn;

            var data = _nrmRepo.GetNRMCircuitList(ftn);
            return Ok(data);
        }

        [HttpGet("GetNRMVendorList")]
        public IActionResult GetNRMVendorList(int orderId)
        {
            var orderDetails = _repoOrder.GetById(orderId);
            string ftn = orderDetails.FsaOrdr.Ftn;

            var data = _nrmRepo.GetNRMVendorList(ftn);
            return Ok(data);
        }

        [HttpGet("GetNRMNotes")]
        public IActionResult GetNRMNotes(int orderId)
        {
            var orderDetails = _repoOrder.GetById(orderId);
            string ftn = orderDetails.FsaOrdr.Ftn;

            var data = _nrmRepo.GetNRMNotes(ftn);
            return Ok(data);
        }
    }
}