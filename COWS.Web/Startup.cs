using AutoMapper;
using COWS.Data;
using COWS.Data.Interface;
using COWS.Data.Repository;
using COWS.Entities.Models;
using COWS.Web.Library.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.OpenApi.Models;
using System;

namespace COWS.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string DefaultPolicy = "DefaultPolicy";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor |
                    ForwardedHeaders.XForwardedProto;

                options.KnownNetworks.Clear();
                options.KnownProxies.Clear();
            });

            services.AddHttpsRedirection(options =>
                {
                    options.RedirectStatusCode = StatusCodes.Status308PermanentRedirect;
                    options.HttpsPort = 443;
                });

            services.AddMvc()
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            //services.AddAuthentication(HttpSysDefaults.AuthenticationScheme);
            services.AddAuthentication(IISDefaults.AuthenticationScheme);
            services.AddMemoryCache();

            services.AddAutoMapper();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "COWS 2.0 API", Version = "v1" });
            });

            services.AddDbContext<COWSAdminDBContext>(config =>
            {
                config.EnableSensitiveDataLogging();
                config.UseSqlServer(Configuration.GetConnectionString("SqlDbConn")
                                       , b => b.MigrationsAssembly("COWS.Web")
                                       //, sqlServerOptions => sqlServerOptions.CommandTimeout(60)
                                       );
            });

            services.AddDbContext<ReportsDbContext>(config =>
            {
                config.UseSqlServer(Configuration.GetConnectionString("SqlDbConn")
                                       , b => b.MigrationsAssembly("COWS.Web")
                                       //, sqlServerOptions => sqlServerOptions.CommandTimeout(60)
                                       );
            });

            services.AddDbContext<COWSReportingDbContext>(config =>
            {
                config.UseSqlServer(Configuration.GetConnectionString("ReportingDbConn")
                                       , b => b.MigrationsAssembly("COWS.Web")
                                       //, sqlServerOptions => sqlServerOptions.CommandTimeout(60)
                                       );
            });

            //services.AddMemoryCache(); - removed duplicate NS

            services.AddCors(options =>
            {
                options.AddPolicy(name: DefaultPolicy,
                                  builder =>
                                  {
                                      builder.WithOrigins("http://cows2",
                                                          "https://cows2",
                                                          "http://cows",
                                                          "https://cows",
                                                          "http://cowsdev2.dev.sprint.com",
                                                          "http://dvmxd823.dev.sprint.com:82",
                                                          "https://dvmxd823.dev.sprint.com:82",
                                                          "http://cowsrtb1.test.sprint.com",
                                                          "https://cowsrtb1.test.sprint.com",
                                                          "http://cowsrtb2.test.sprint.com",
                                                          "https://cowsrtb2.test.sprint.com")
                                                    .AllowAnyHeader()
                                                    .AllowAnyMethod();
                                  });
            });


            //services.AddResponseCaching(); - removed response caching

            services.AddHttpContextAccessor();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(60);//You can set Time   
            });

            services.AddMvc(opt =>
            {
                opt.CacheProfiles.Add("Default",
                    new CacheProfile()
                    {
                        Location = ResponseCacheLocation.None,
                        NoStore = true
                    });
            })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v2", new OpenApiInfo { Title = "COWS API", Version = "v2" });
            });

            //This for support swagger documentation
            services.AddMvcCore()
                .AddApiExplorer();

            ////support for load-balancer/ proxy
            //services.Configure<ForwardedHeadersOptions>(options =>
            //{
            //    options.ForwardedHeaders =
            //        ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            //});

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            #region Dependency Injection

            services.AddScoped<IUserRepository, UserRepository>();

            //services.AddScoped<IAdminRepository, AdminRepository>();

            services.AddScoped<INRMBPMInterfaceRepository, NRMBPMInterfaceRepository>();

            services.AddScoped<IPeopleSoftInterfaceRepository, PeopleSoftInterfaceRepository>();

            services.AddScoped<IMenuRepository, MenuRepository>();

            services.AddScoped<IUserProfileRepository, UserProfileRepository>();

            services.AddScoped<IUserProfileMenuRepository, UserProfileMenuRepository>();

            services.AddScoped<IDedicatedCustomerRepository, DedicatedCustomerRepository>();

            services.AddScoped<IServiceAssuranceRepository, ServiceAssuranceRepository>();

            services.AddScoped<IEventStatusRepository, EventStatusRepository>();

            services.AddScoped<IEventTypeRepository, EventTypeRepository>();

            services.AddScoped<IMDSMACActivityRepository, MDSMACActivityRepository>();

            services.AddScoped<IMDSActivityTypeRepository, MDSActivityTypeRepository>();

            services.AddScoped<IMDSEventNtwkActyRepository, MDSEventNtwkActyRepository>(); 

            services.AddScoped<IMDS3rdPartyServiceRepository, MDS3rdPartyServiceRepository>();

            services.AddScoped<IMDS3rdPartyVendorRepository, MDS3rdPartyVendorRepository>();

            services.AddScoped<IMDSServiceTypeRepository, MDSServiceTypeRepository>();

            services.AddScoped<IServiceDeliveryRepository, ServiceDeliveryRepository>();

            services.AddScoped<ISpecialProjectRepository, SpecialProjectRepository>();

            services.AddScoped<ISprintHolidayRepository, SprintHolidayRepository>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<ILoggedInUserService, LoggedInUserService>();

            services.AddScoped<IL2PInterfaceService, L2PInterfaceService>();

            services.AddScoped<ITelcoRepository, TelcoRepository>();

            services.AddScoped<IServiceTierRepository, ServiceTierRepository>();

            services.AddScoped<IEnhncSrvcRepository, EnhncSrvcRepository>();

            services.AddScoped<IMDSFastTrackTypeRepository, MDSFastTrackTypeRepository>();

            services.AddScoped<IProductTypeRepository, ProductTypeRepository>();

            services.AddScoped<IOrderTypeRepository, OrderTypeRepository>();

            services.AddScoped<IOrderSubTypeRepository, OrderSubTypeRepository>();

            services.AddScoped<IGroupRepository, GroupRepository>();

            services.AddScoped<IPlatformRepository, PlatformRepository>();

            services.AddScoped<IRegionRepository, RegionRepository>();

            services.AddScoped<IRedesignCategoryRepository, RedesignCategoryRepository>();

            services.AddScoped<IRedesignTypeRepository, RedesignTypeRepository>();

            services.AddScoped<IStatusRepository, StatusRepository>();

            services.AddScoped<IFedlineOrderTypeRepository, FedlineOrderTypeRepository>();

            services.AddScoped<IXnciMsSlaRepository, XnciMsSlaRepository>();

            services.AddScoped<ISearchRepository, SearchRepository>();

            services.AddScoped<IXnciMsRepository, XnciMsRepository>();

            services.AddScoped<IOrdrUnlockRepository, OrdrUnlockRepository>();

            services.AddScoped<ICurrencyListRepository, CurrencyListRepository>();

            services.AddScoped<IMapUserProfileRepository, MapUserProfileRepository>();

            services.AddScoped<ILDAPUserManagementService, LDAPUserManagementService>();

            services.AddScoped<IWorldHolidayRepository, WorldHolidayRepository>();

            services.AddScoped<ICountryRepository, CountryRepository>();

            services.AddScoped<ICityRepository, CityRepository>();

            services.AddScoped<IOrdrCancelRepository, OrdrCancelRepository>();

            services.AddScoped<ISetPreferenceRepository, SetPreferenceRepository>();

            services.AddScoped<ITimeSlotOccurrenceRepository, TimeSlotOccurrenceRepository>();

            services.AddScoped<IAccessCitySiteRepository, AccessCitySiteRepository>();

            services.AddScoped<ICountryCxrRepository, CountryCxrRepository>();

            services.AddScoped<IForeignCxrRepository, ForeignCxrRepository>();

            services.AddScoped<IVendorRepository, VendorRepository>();

            services.AddScoped<IMDSImplementationIntervalRepository, MDSImplementationIntervalRepository>();

            services.AddScoped<IDeviceManufacturerRepository, DeviceManufacturerRepository>();

            services.AddScoped<IDeviceModelRepository, DeviceModelRepository>();

            services.AddScoped<ICurrencyFileRepository, CurrencyFileRepository>();

            services.AddScoped<ICCDBypassRepository, CCDBypassRepository>();

            services.AddScoped<IOrderActionRepository, OrderActionRepository>();

            services.AddScoped<ICustomerUserProfileRepository, CustomerUserProfileRepository>();

            services.AddScoped<IWFMUserAssignmentRepository, WFMUserAssignmentRepository>();

            services.AddScoped<IIPVersionRepository, IPVersionRepository>();

            services.AddScoped<IMDSNetworkActivityTypeRepository, MDSNetworkActivityTypeRepository>();

            services.AddScoped<IQualificationRepository, QualificationRepository>();

            services.AddScoped<IAdEventRepository, AdEventRepository>();

            services.AddScoped<INgvnEventRepository, NgvnEventRepository>();

            services.AddScoped<IAdEventAccessTagRepository, AdEventAccessTagRepository>();

            services.AddScoped<IEventViewRepository, EventViewRepository>();

            services.AddScoped<IEscalationReasonRepository, EscalationReasonRepository>();

            services.AddScoped<IMplsActivityTypeRepository, MplsActivityTypeRepository>();

            services.AddScoped<IMplsEventTypeRepository, MplsEventTypeRepository>();

            services.AddScoped<IActivityLocaleRepository, ActivityLocaleRepository>();

            services.AddScoped<IVasTypeRepository, VasTypeRepository>();

            services.AddScoped<IVpnPlatformTypeRepository, VpnPlatformTypeRepository>();

            services.AddScoped<IMultiVrfReqRepository, MultiVrfReqRepository>();

            services.AddScoped<IMplsAccessBandwidthRepository, MplsAccessBandwidthRepository>();

            services.AddScoped<IMplsMigrationTypeRepository, MplsMigrationTypeRepository>();

            services.AddScoped<ISIPTEventRepository, SIPTEventRepository>();

            services.AddScoped<IWorkflowRepository, WorkflowRepository>();

            services.AddScoped<IMplsEventRepository, MplsEventRepository>();

            services.AddScoped<IWholesalePartnerRepository, WholesalePartnerRepository>();

            services.AddScoped<IEventAsnToUserRepository, EventAsnToUserRepository>();

            services.AddScoped<IMplsEventAccessTagRepository, MplsEventAccessTagRepository>();

            services.AddScoped<IMnsOfferingRouterRepository, MnsOfferingRouterRepository>();

            services.AddScoped<IMnsPerformanceReportingRepository, MnsPerformanceReportingRepository>();

            services.AddScoped<IMnsRoutingTypeRepository, MnsRoutingTypeRepository>();

            services.AddScoped<IUcaasActivityTypeRepository, UcaasActivityTypeRepository>();

            services.AddScoped<IUcaasBillActivityRepository, UcaasBillActivityRepository>();

            services.AddScoped<IUcaasPlanTypeRepository, UcaasPlanTypeRepository>();

            services.AddScoped<IUcaasProdTypeRepository, UcaasProdTypeRepository>();

            services.AddScoped<IEventRuleRepository, EventRuleRepository>();

            services.AddScoped<IUcaasEventRepository, UcaasEventRepository>();

            services.AddScoped<IUcaasEventBillingRepository, UcaasEventBillingRepository>();

            services.AddScoped<IUcaasEventOdieDeviceRepository, UcaasEventOdieDeviceRepository>();

            services.AddScoped<IUserCsgLevelRepository, UserCsgLevelRepository>();

            services.AddScoped<IL2PInterfaceRepository, L2PInterfaceRepository>();

            services.AddScoped<IEventLockRepository, EventLockRepository>();

            services.AddScoped<ISPLKEventRepository, SPLKEventRepository>();

            services.AddScoped<IFsaOrderRepository, FsaOrderRepository>();

            services.AddScoped<IEventHistoryRepository, EventHistoryRepository>();

            services.AddScoped<IActivatorsRepository, ActivatorsRepository>();

            services.AddScoped<IMDSEventPortBndwdRepository, MDSEventPortBndwdRepository>();

            services.AddScoped<IMDSEventWrlsTrptRepository, MDSEventWrlsTrptRepository>();

            services.AddScoped<IMDSEventSlnkWiredTrptRepository, MDSEventSlnkWiredTrptRepository>();

            services.AddScoped<IMDSEventDslSbicCustTrptRepository, MDSEventDslSbicCustTrptRepository>();

            services.AddScoped<IMDSEventSiteSrvcRepository, MDSEventSiteSrvcRepository>();

            services.AddScoped<IEventCpeDevRepository, EventCpeDevRepository>();

            services.AddScoped<IEventDeviceCompletionRepository, EventDeviceCompletionRepository>();

            services.AddScoped<IEventDiscoDevRepository, EventDiscoDevRepository>();

            services.AddScoped<IEventFailActyRepository, EventFailActyRepository>();

            services.AddScoped<IEventSucssActyRepository, EventSucssActyRepository>();

            services.AddScoped<IEventDevSrvcMgmtRepository, EventDevSrvcMgmtRepository>();

            services.AddScoped<IMDSEventNtwkCustRepository, MDSEventNtwkCustRepository>();

            services.AddScoped<IMDSEventNtwkTrptRepository, MDSEventNtwkTrptRepository>();

            services.AddScoped<IOdieReqRepository, OdieReqRepository>();

            services.AddScoped<IOdieRspnRepository, OdieRspnRepository>();

            services.AddScoped<IApptRepository, ApptRepository>();

            services.AddScoped<ICommonRepository, CommonRepository>();

            services.AddScoped<IEventRuleDataRepository, EventRuleDataRepository>();

            services.AddScoped<ISlotPickerRepository, SlotPickerRepository>();

            services.AddScoped<IEmailReqRepository, EmailReqRepository>();

            services.AddScoped<IFedlineEventRepository, FedlineEventRepository>();

            services.AddScoped<IMSSRepository, MSSRepository>();

            services.AddScoped<IOrderNoteRepository, OrderNoteRepository>();

            services.AddScoped<INccoOrderRepository, NccoOrderRepository>();

            services.AddScoped<IMdsEventRepository, MdsEventRepository>();

            services.AddScoped<IFedlineEventRepository, FedlineEventRepository>();

            services.AddScoped<IOrderLockRepository, OrderLockRepository>();

            services.AddScoped<ICalendarRepository, CalendarRepository>();

            services.AddScoped<IRedesignRepository, RedesignRepository>();

            services.AddScoped<ICptCustomerTypeRepository, CptCustomerTypeRepository>();

            services.AddScoped<ICptHostManagedServiceRepository, CptHostManagedServiceRepository>();

            services.AddScoped<ICptManagedAuthProdRepository, CptManagedAuthProdRepository>();

            services.AddScoped<ICptMdsSupportTierRepository, CptMdsSupportTierRepository>();

            services.AddScoped<ICptMvsProdTypeRepository, CptMvsProdTypeRepository>();

            services.AddScoped<ICptPlanServiceTierRepository, CptPlanServiceTierRepository>();

            services.AddScoped<ICptPlanServiceTypeRepository, CptPlanServiceTypeRepository>();

            services.AddScoped<ICptPrimSecondaryTransportRepository, CptPrimSecondaryTransportRepository>();

            services.AddScoped<ICptPrimSiteRepository, CptPrimSiteRepository>();

            services.AddScoped<ICptProvisionTypeRepository, CptProvisionTypeRepository>();

            services.AddScoped<ICptRecLockRepository, CptRecLockRepository>();

            services.AddScoped<ICptRepository, CptRepository>();

            services.AddScoped<IMSSDocRepository, MSSDocRepository>();

            services.AddScoped<IMSSProdRepository, MSSProdRepository>();

            services.AddScoped<IVendorFolderRepository, VendorFolderRepository>();

            services.AddScoped<IVendorFolderContactRepository, VendorFolderContactRepository>();

            services.AddScoped<IStateRepository, StateRepository>();

            services.AddScoped<IRedesignDocRepository, RedesignDocRepository>();

            services.AddScoped<ISystemConfigRepository, SystemConfigRepository>();

            services.AddScoped<IRedesignCustBypassRepository, RedesignCustBypassRepository>();

            services.AddScoped<IRedesignDevInfoRepository, RedesignDevInfoRepository>();

            services.AddScoped<IRedesignWorkflowRepository, RedesignWorkflowRepository>();

            services.AddScoped<IRedesignNoteRepository, RedesignNoteRepository>();

            services.AddScoped<IOdieCustomerH1Repository, OdieCustomerH1Repository>();

            services.AddScoped<IFsaOrderCustRepository, FsaOrderCustRepository>();

            services.AddScoped<IOrderAddressRepository, OrderAddressRepository>();

            services.AddScoped<IOrderContactRepository, OrderContactRepository>();

            services.AddScoped<IOrderVlanRepository, OrderVlanRepository>();

            services.AddScoped<IH5FolderRepository, H5FolderRepository>();

            services.AddScoped<IH5DocRepository, H5DocRepository>();

            services.AddScoped<IOdieRspnInfoRepository, OdieRspnInfoRepository>();

            services.AddScoped<IRedesignRecLockRepository, RedesignRecLockRepository>();

            services.AddScoped<IDisplayViewRepository, DisplayViewRepository>();

            services.AddScoped<IBillingDispatchRepository, BillingDispatchRepository>();

            services.AddScoped<IXnciRegionRepository, XnciRegionRepository>();

            services.AddScoped<IOrderStdiHistoryRepository, OrderStdiHistoryRepository>();

            services.AddScoped<IWorkGroupRepository, WorkGroupRepository>();

            services.AddScoped<IVendorOrderRepository, VendorOrderRepository>();

            services.AddScoped<IVendorTemplateRepository, VendorTemplateRepository>();

            services.AddScoped<IDCPERepository, DCPERepository>();

            services.AddScoped<IFsaOrderCpeLineItemRepository, FsaOrderCpeLineItemRepository>();

            services.AddScoped<IFsaProdTypeRepository, FsaProdTypeRepository>();

            services.AddScoped<IDashboardRepository, DashboardRepository>();

            services.AddScoped<INRMRepository, NRMRepository>();

            services.AddScoped<ICircuitChargeRepository, CircuitChargeRepository>();

            services.AddScoped<ICannedReportsRepository, CannedReportsRepository>();

            services.AddScoped<IVendorEmailTypeRepository, VendorEmailTypeRepository>();

            services.AddScoped<IVendorOrderEmailRepository, VendorOrderEmailRepository>();

            services.AddScoped<IWFMRepository, WFMRepository>();

            services.AddScoped<ICpeClliRepository, CpeClliRepository>();

            services.AddScoped<ICcdRepository, CcdRepository>();

            services.AddScoped<IVendorOrderFormRepository, VendorOrderFormRepository>();

            services.AddScoped<IOrderRepository, OrderRepository>();

            services.AddScoped<IAdditionalCustomerChargeRepository, AdditionalCustomerChargeRespository>();

            services.AddScoped<ICircuitRepository, CircuitRepository>();

            services.AddScoped<ICircuitMsRepository, CircuitMsRepository>();

            services.AddScoped<IOrderMsRepository, OrderMsRepository>();

            services.AddScoped<IActiveTaskRepository, ActiveTaskRepository>();

            services.AddScoped<IMdsEventOdieDevRepository, MdsEventOdieDevRepository>();

            services.AddScoped<ITaskRepository, TaskRepository>();

            services.AddScoped<IStdiReasonRepository, StdiReasonRepository>();

            services.AddScoped<IAccessMbRepository, AccessMbRepository>();

            services.AddScoped<IVendorOrderEmailAttachmentRepository, VendorOrderEmailAttachmentRepository>();

            services.AddScoped<IProfileHierarchyRepository, ProfileHierarchyRepository>();

            services.AddScoped<IAsrTypeRepository, AsrTypeRepository>();

            services.AddScoped<IAsrRepository, AsrRepository>();

            services.AddScoped<IVendorEmailLanguageRepository, VendorEmailLanguageRepository>();

            services.AddScoped<IIpManagementRepository, IpManagementRepository>();

            services.AddScoped<ICustContractLengthRepository, CustContractLengthRepository>();

            services.AddScoped<INidActyRepository, NidActyRepository>();

            services.AddScoped<ICompleteOrderRepository, CompleteOrderRepository>();

            services.AddScoped<ICustScrdDataRepository, CustScrdDataRepository>();

            services.AddScoped<IAdminSupportRepository, AdminSupportRepository>();

            services.AddScoped<ISstatReqRepository, SstatReqRepository>();

            services.AddScoped<IContactDetailRepository, ContactDetailRepository>();

            services.AddScoped<IVendorOrderMsRepository, VendorOrderMsRepository>();

            #endregion Dependency Injection
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.UseForwardedHeaders();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();


            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "COWS 2.0 V1");
            });

            //app.UseSwaggerDocs();
            if (!env.IsProduction())
            {
                //disable swagger in production
            }

            app.UseSession();

            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //    name: "default",
                //    template: "{controller}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "api-route",
                    template: "api/{controller}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.Options.StartupTimeout = new TimeSpan(0, 3, 59);
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

            app.UseCors();

        }
    }
}