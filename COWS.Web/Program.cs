using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.IO;

namespace COWS.Web
{
    public class Program
    {
        private static IConfiguration Configuration;

        public static void Main(string[] args)
        {
            var currentEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .AddJsonFile($"appsettings.{currentEnv}.json", optional: true)
               .AddEnvironmentVariables()
               .Build();

            //CreateWebHostBuilder(args).Build().Run();
            BuildWebHost(args).Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseSerilog(new LoggerConfiguration()
                    .ReadFrom.Configuration(Configuration)
                    .Enrich.WithEnvironmentUserName()
                    .CreateLogger())
                .UseStartup<Startup>();

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseSerilog(new LoggerConfiguration()
                    .ReadFrom.Configuration(Configuration)
                    .Enrich.WithEnvironmentUserName()
                    .CreateLogger())
                .UseStartup<Startup>()
                .UseIISIntegration()
                //.UseHttpSys(options =>
                //{
                //    options.Authentication.Schemes =
                //        AuthenticationSchemes.NTLM | AuthenticationSchemes.Negotiate;
                //    options.Authentication.AllowAnonymous = false;
                //})
                .Build();
    }
}