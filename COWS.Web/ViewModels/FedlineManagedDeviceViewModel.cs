﻿namespace COWS.Web.ViewModels
{
    public class FedlineManagedDeviceViewModel
    {
        public string DeviceName { get; set; }
        public string SerialNum { get; set; }
        public string Vendor { get; set; }
        public string Model { get; set; }
        public string DeviceType { get; set; }
    }
}