﻿using System;

namespace COWS.Web.ViewModels
{
    public class SiptProdTypeViewModel
    {
        public short SiptProdTypeId { get; set; }
        public string SiptProdTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}