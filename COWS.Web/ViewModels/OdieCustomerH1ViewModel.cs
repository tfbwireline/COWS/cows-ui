﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class OdieCustomerH1ViewModel
    {
        public string Customer { get; set; }
        public string H1 { get; set; }
        public string CustomerH1 => Customer + " - " + H1;
    }
}
