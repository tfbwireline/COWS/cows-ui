﻿using System;

namespace COWS.Web.ViewModels
{
    public class MapUserProfileViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public short UsrPrfId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte? PoolCd { get; set; }
    }
}