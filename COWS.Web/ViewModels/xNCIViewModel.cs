﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class xNCIViewModel
    {
        public int OrdrId { get; set; }
        public short? OrdrCatId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int? H5FoldrId { get; set; }
        //public int? PprtId { get; set; }
        public bool DmstcCd { get; set; }
        //public int? H5H6CustId { get; set; }
        //public int? PrntOrdrId { get; set; }
        public short? RgnId { get; set; }
        //public string PltfrmCd { get; set; }
        //public bool CcdUpdtCd { get; set; }
        public byte OrdrStusId { get; set; }
        //public string OrdrCmpltnDrtnTmeTxt { get; set; }
        //public string CharsId { get; set; }
        //public bool SlaVltdCd { get; set; }
        public DateTime? CustCmmtDt { get; set; }
        //public string InstlEsclCd { get; set; }
        //public string FsaExpTypeCd { get; set; }
        //public string SmrNmr { get; set; }
        //public DateTime? RasDt { get; set; }
        //public string DlvyClli { get; set; }
        //public string ProdId { get; set; }
        //public string CpeClli { get; set; }
        public byte CsgLvlId { get; set; }

        //public virtual LkUser CreatByUser { get; set; }
        //public virtual LkCsgLvl CsgLvl { get; set; }
        public virtual H5FolderViewModel H5Foldr { get; set; }
        //public virtual LkUser ModfdByUser { get; set; }
        //public virtual LkOrdrCat OrdrCat { get; set; }
        //public virtual LkOrdrStus OrdrStus { get; set; }
        //public virtual LkPltfrm PltfrmCdNavigation { get; set; }
        //public virtual LkPprt Pprt { get; set; }
        //public virtual Ordr PrntOrdr { get; set; }
        //public virtual LkRecStus RecStus { get; set; }
        //public virtual LkXnciRgn Rgn { get; set; }
        public virtual FsaOrderViewModel FsaOrdr { get; set; }
        //public virtual IplOrdr IplOrdr { get; set; }
        public virtual NccoOrderViewModel NccoOrdr { get; set; }
        //public virtual OrdrRecLock OrdrRecLock { get; set; }
        public virtual TrptOrdr TrptOrdr { get; set; }
        //public virtual ICollection<ActTask> ActTask { get; set; }
        //public virtual ICollection<CcdHist> CcdHist { get; set; }
        public virtual ICollection<Ckt> Ckt { get; set; }
        //public virtual ICollection<EmailReq> EmailReq { get; set; }
        //public virtual ICollection<Ordr> InversePrntOrdr { get; set; }
        //public virtual ICollection<OrdrAdr> OrdrAdr { get; set; }
        //public virtual ICollection<OrdrCktChg> OrdrCktChg { get; set; }
        //public virtual ICollection<OrdrCntct> OrdrCntct { get; set; }
        //public virtual ICollection<OrdrJprdy> OrdrJprdy { get; set; }
        public virtual ICollection<OrdrMs> OrdrMs { get; set; }
        public virtual ICollection<OrderNoteViewModel> OrdrNte { get; set; }
        public virtual ICollection<OrderStdiHistoryViewModel> OrdrStdiHist { get; set; }
        //public virtual ICollection<OrdrVlan> OrdrVlan { get; set; }
        //public virtual ICollection<UserWfmAsmt> UserWfmAsmt { get; set; }
        public virtual ICollection<VendorOrderViewModel> VndrOrdrOrdr { get; set; }

    }
}

