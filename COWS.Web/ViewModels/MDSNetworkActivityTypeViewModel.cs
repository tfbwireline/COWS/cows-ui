﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class MDSNetworkActivityTypeViewModel
    {
        public byte NtwkActyTypeId { get; set; }
        public string NtwkActyTypeCd { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(25)]
        public string NtwkActyTypeDes { get; set; }

        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}