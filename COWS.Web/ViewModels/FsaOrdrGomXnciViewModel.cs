﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class FsaOrdrGomXnciViewModel
    {
        public int OrdrId { get; set; }
        public short GrpId { get; set; }
        public DateTime? VndrShipDt { get; set; }
        public DateTime? DrtbnShipDt { get; set; }
        public DateTime? CustDlvryDt { get; set; }
        public DateTime? CstmDt { get; set; }
        public string ShpmtTrkNbr { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? HdwrDlvrdDt { get; set; }
        public DateTime? EqptInstlDt { get; set; }
        public short? CpeStusId { get; set; }
        public DateTime? CpeCmpltDt { get; set; }
        public string AtlasWrkOrdrNbr { get; set; }
        public string PlsftRqstnNbr { get; set; }
        public decimal? RqstnAmt { get; set; }
        public DateTime? RqstnDt { get; set; }
        public string PrchOrdrNbr { get; set; }
        public DateTime? PrchOrdrBackOrdrShipDt { get; set; }
        public DateTime? VndrRecvFromDt { get; set; }
        public string VndrCourierTrkNbr { get; set; }
        public DateTime? PlsftRelsDt { get; set; }
        public string PlsftRelsId { get; set; }
        public string PlsftRcptId { get; set; }
        public string CourierTrkNbr { get; set; }
        public string CpeEqptTypeTxt { get; set; }
        public string CpeVndrNme { get; set; }
        public string ShpmtSignByNme { get; set; }
        public string PlnNme { get; set; }
        public string BillFtn { get; set; }
        public bool? CustPrchCd { get; set; }
        public bool? CustRntlCd { get; set; }
        public short? CustRntlTermNbr { get; set; }
        public decimal? LogicalisMnthLeaseRtAmt { get; set; }
        public DateTime? TermStrtDt { get; set; }
        public DateTime? TermEndDt { get; set; }
        public DateTime? Term60DayNtfctnDt { get; set; }
        public bool? CustRnlCd { get; set; }
        public short? CustRnlTermNbr { get; set; }
        public decimal? LogicalisRnlMnthLeaseRtAmt { get; set; }
        public bool? CustDscnctCd { get; set; }
        public DateTime? CustDscnctDt { get; set; }
        public int? FsaCpeLineItemId { get; set; }
        public int Id { get; set; }
        public short? UsrPrfId { get; set; }
    }
}
