﻿namespace COWS.Web.ViewModels
{
    public class FedlineEventViewModel
    {
        public int EventId { get; set; }
        public byte EventStatusId { get; set; }
        public string EventStatus { get; set; }
        public byte WorkflowStatusId { get; set; }
        public string WorkflowStatus { get; set; }
        public short FailCodeId { get; set; }
        public string FailCode { get; set; }
        public string StatusComments { get; set; }
        public FedlineTadpoleDataViewModel TadpoleData { get; set; }
        public FedlineUserDataViewModel UserData { get; set; }
        public string[] changeList { get; set; }
    }
}