﻿using System;

namespace COWS.Web.ViewModels
{
    public class RedesignNotesViewModel
    {
        public int RedsgnNotesId { get; set; }
        public int RedsgnId { get; set; }
        public byte RedsgnNteTypeId { get; set; }
        public string RedsgnNteType { get; set; }
        public string Notes { get; set; }
        public int CretdByCd { get; set; }
        public string CretdBy { get; set; }
        public DateTime CretdDt { get; set; }
    }
}
