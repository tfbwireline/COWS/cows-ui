﻿using System;

namespace COWS.Web.ViewModels
{
    public class RedesignDevicesInfoViewModel
    {
        public int RedsgnDevId { get; set; }
        public int RedsgnId { get; set; }
        public string RedesignNbr { get; set; }
        public DateTime ExpirationDate { get; set; }
        public short SeqNbr { get; set; }
        public string DevNme { get; set; }
        public bool? FastTrkCd { get; set; }
        public bool? DspchRdyCd { get; set; }
        public int? DispatchReadyCd => DspchRdyCd.HasValue ? (DspchRdyCd.Value ? 1 : 0) : (int?)null;
        public bool RecStusId { get; set; }
        public int CretdByCd { get; set; }
        public DateTime CretdDt { get; set; }
        public int? ModfdByCd { get; set; }
        public DateTime? ModfdDt { get; set; }
        public bool NteChrgCd { get; set; }
        public DateTime? DevBillDt { get; set; }
        public bool? DevCmpltnCd { get; set; }
        public string H6CustId { get; set; }
        public DateTime? EventCmpltnDt { get; set; }
        public string ScCd { get; set; }
        public bool IsSelected { get; set; }
        public int EventId { get; set; }
    }
}