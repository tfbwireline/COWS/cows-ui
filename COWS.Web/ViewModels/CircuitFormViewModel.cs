﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class CircuitFormViewModel
    {
        public int VendorOrderId { get; set; }
        public string VendorCircuitId { get; set; }
        public string LecId { get; set; }
        public string VlanId { get; set; }
        public string NoOfIpAddresses { get; set; }
        public string RoutingProtocol { get; set; }
        public string CustomerIpAddresses { get; set; }
        public string SpecialCustomerAccessDetails { get; set; }
        public string rltdVendorOrderId { get; set; }
        public string XCnnctPrvdr { get; set; }
        public string TDwnAssn { get; set; }
        public string TDwnReq { get; set; }
    }
}