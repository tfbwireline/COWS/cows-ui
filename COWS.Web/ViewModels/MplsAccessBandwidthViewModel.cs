﻿using System;

namespace COWS.Web.ViewModels
{
    public class MplsAccessBandwidthViewModel
    {
        public byte MplsAccsBdwdId { get; set; }
        public string MplsAccsBdwdDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}