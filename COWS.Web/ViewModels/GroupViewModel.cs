﻿using System;

namespace COWS.Web.ViewModels
{
    public class GroupViewModel
    {
        public short GrpId { get; set; }
        public string GrpNme { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public string ModifiedByAdid { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}