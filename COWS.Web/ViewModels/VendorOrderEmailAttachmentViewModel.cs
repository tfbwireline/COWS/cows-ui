﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class VendorOrderEmailAttachmentViewModel
    {
        public int VndrOrdrEmailAtchmtId { get; set; }
        public int VndrOrdrEmailId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }

        //Additional field
        public string CreatByUserFullName { get; set; }
        public string Base64string { get; set; }
    }
}
