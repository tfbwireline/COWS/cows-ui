﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CptRelatedInfoViewModel
    {
        public int CptRltdInfoId { get; set; }
        public int CptId { get; set; }
        public short? CptPlnSrvcTierId { get; set; }
        public short CptCustTypeId { get; set; }
        public bool? HstdUcCd { get; set; }
        public bool CubeSiptSmiCd { get; set; }
        public bool? E2eNddCd { get; set; }
        public string NtwkDsgnDocTxt { get; set; }
        public string CmntTxt { get; set; }
        public bool? CustUcdCd { get; set; }
        public bool? SpsSowCd { get; set; }
        public DateTime CreatDt { get; set; }
        public bool? SdwanCustCd { get; set; }
        public bool? SuplmntlEthrntCd { get; set; }
        public string CntctEmailList { get; set; }
        public List<int> SecTrnsprtTypes { get; set; }
        public ICollection<CptPrimWanViewModel> CptPrimWan { get; set; }
    }
}
