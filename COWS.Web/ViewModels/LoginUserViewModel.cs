﻿using System;

namespace COWS.Web.ViewModels
{
    public class LogInUserViewModel
    {
        public int UserId { get; set; }
        public string UserAdid { get; set; }
        public string OldUserAdid { get; set; }
        public string FullNme { get; set; }
        public string EmailAdr { get; set; }
        public byte RecStusId { get; set; }
        public string DsplNme { get; set; }
        public string CtyNme { get; set; }
        public string SttCd { get; set; }
        public string PhnNbr { get; set; }
        public DateTime? LstLoggdinDt { get; set; }
        public char? MenuPref { get; set; }
        public string CellPhnNbr { get; set; }
    }
}