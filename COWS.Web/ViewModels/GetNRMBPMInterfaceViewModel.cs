﻿using System;

namespace COWS.Web.ViewModels
{
    public class GetNRMBPMInterfaceViewModel
    {
        public string CpeDeviceId { get; set; }
        public string M5OrdrNbr { get; set; }
        public string M5RltdOrdrNbr { get; set; }
        public string SubTypeNme { get; set; }
        public string TypeNme { get; set; }
        public string ProdCd { get; set; }
        public string XtrctCd { get; set; }
        public DateTime? XtrctDt { get; set; }
    }
}