﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class RedesignCustBypassViewModel
    {
        public int Id { get; set; }
        public string H1Cd { get; set; }
        public bool RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte CsgLvlId { get; set; }
        public string CustNme { get; set; }
    }
}
