﻿using System;

namespace COWS.Web.ViewModels
{
    public class OrderContactViewModel
    {
        public int OrdrCntctId { get; set; }
        public int OrdrId { get; set; }
        public byte CntctTypeId { get; set; }
        public string TmeZoneId { get; set; }
        public bool IntprtrCd { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public byte? RoleId { get; set; }
        public string CisLvlType { get; set; }
        public string FsaMdulId { get; set; }
        public string CntctHrTxt { get; set; }
        public string Npa { get; set; }
        public string Nxx { get; set; }
        public string StnNbr { get; set; }
        public string CtyCd { get; set; }
        public string IsdCd { get; set; }
        public string PhnExtNbr { get; set; }
        public string SuppdLangNme { get; set; }
        public string FsaTmeZoneCd { get; set; }
        public string FrstNme { get; set; }
        public string LstNme { get; set; }
        public string EmailAdr { get; set; }
        public string CntctNme { get; set; }
        public string PhnNbr { get; set; }
        public string FaxNbr { get; set; }

        // Related fields
        public string CntctTypeDes { get; set; }
        public string RoleNme { get; set; }
    }
}