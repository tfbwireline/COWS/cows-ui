﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class ContactDetailViewModel
    {
        public int Id { get; set; }
        public int ObjId { get; set; }
        // ObjTypCd Values: E (Event), R (Redesign), C (CPT)
        public string ObjTypCd { get; set; }
        // HierLvlCd Values: H1, H2, H4, H6
        public string HierLvlCd { get; set; }
        public string HierId { get; set; }
        public byte RoleId { get; set; }
        public string RoleNme { get; set; }
        public string EmailAdr { get; set; }
        public string PhnNbr { get; set; }
        // AutoRfrshCd Values: 1 - By default refresh this contact details from Mach5 based on Role_CD, Hier_lvl_cd and HIER_ID;
        // For MNS contacts, get the details from ODIE
        public bool? AutoRfrshCd { get; set; }
        // EmailCd Values: NULL : Copy on all Emails(based on workflow);
        // |3|4|6| : Copy on Rework, Published (this is just example);
        // |0| : Dont copy this contact
        public string EmailCd { get; set; }
        public short RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public bool SuprsEmail { get; set; }
    }
}
