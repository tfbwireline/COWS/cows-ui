﻿using System;

namespace COWS.Web.ViewModels
{
    public class OrderActionViewModel
    {
        public byte OrdrActnId { get; set; }
        public string OrdrActnDes { get; set; }
        public DateTime CreatDt { get; set; }
    }
}