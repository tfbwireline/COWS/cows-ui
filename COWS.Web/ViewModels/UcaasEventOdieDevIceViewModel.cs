﻿using System;

namespace COWS.Web.ViewModels
{
    public class UcaasEventOdieDevIceViewModel
    {
        public int UcaaSEventOdieDevId { get; set; }
        public int EventId { get; set; }
        public string RdsnNbr { get; set; }
        public DateTime RdsnExpDt { get; set; }
        public string OdieDevNme { get; set; }
        public short DevModelId { get; set; }
        public short ManfId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}