﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CpeClliRequestViewModel
    {
        public int ID { get; set; }

        public string CLLI_ID { get; set; }

        public string SiteName { get; set; }

        public string Address { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Status { get; set; }

        public string IsActive { get; set; }

    }
}
