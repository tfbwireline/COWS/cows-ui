﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class VendorFolderContactViewModel
    {
        public int CntctId { get; set; }
        public int VndrFoldrId { get; set; }
        public string CntctFrstNme { get; set; }
        public string CntctLstNme { get; set; }
        public string CntctEmailAdr { get; set; }
        public string CntctProdTypeTxt { get; set; }
        public string CntctProdRoleTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CntctPhnNbr { get; set; }
    }
}