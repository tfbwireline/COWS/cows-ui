﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CptCustomerH1ViewModel
    {
        public string CustomerName { get; set; }
        public string H1 { get; set; }
        public string CustH1Display => CustomerName + " - " + H1;
        public string CustomerId { get; set; }
        public string AcctRoleName { get; set; }
        public string ContactName { get; set; }
        public string EmailAddress { get; set; }
        public string CcEmailAddress { get; set; }
        public string DesignLinkName { get; set; }
        public string SEEmail { get; set; }
        public string IPMEmail { get; set; }
        public string AMEmail { get; set; }
    }
}
