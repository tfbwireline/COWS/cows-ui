﻿using System;

namespace COWS.Web.ViewModels
{
    public class CalendarDataViewModel
    {
        public string SubjTxt { get; set; }
        public string Des { get; set; }
        public DateTime StrtTmst { get; set; }
        public DateTime EndTmst { get; set; }
        public string ApptLocTxt { get; set; }
        public string RcurncDesTxt { get; set; }
        public int ApptId { get; set; }
        public int ApptTypeId { get; set; }
        public string AsnToUserIdListTxt { get; set; }
        public int RcurncCd { get; set; }
        public int CreatByUserId { get; set; }
        public int ModfdByUserId { get; set; }
        public int DelFlg { get; set; }
    }
}