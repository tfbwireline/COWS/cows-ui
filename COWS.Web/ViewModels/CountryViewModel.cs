﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class CountryViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string CtryNme { get; set; }

        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public string ModifiedByAdId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public string CtryCd { get; set; }
        public short? RgnId { get; set; }
        public string RgnDes { get; set; }
        public string L2pCtryCd { get; set; }
        public int? IsdCd { get; set; }
        public string CountryRegion => CtryNme + " - " + RgnDes;
    }
}