﻿using COWS.Entities.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class ServiceTierViewModel
    {
        public byte MdsSrvcTierId { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string MdsSrvcTierDes { get; set; }
        public short EventTypeId { get; set; }
        public string MdsSrvcTierCd { get; set; }
        public byte? RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public string ModifiedByAdId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public LkEventType EventType { get; set; }
    }
}
