﻿using COWS.Entities.Enums;
using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class FedlineTadpoleDataViewModel
    {
        public int CowsFedlineId { get; set; }
        public int FrbRequestId { get; set; }
        public int Version { get; set; }
        public string CustName { get; set; }
        public string CustOrgId { get; set; }
        public string DesignType { get; set; }
        public FedlineActivityType? MacType { get; set; }
        public string OrderTypeCode { get; set; }
        public string MoveType { get; set; }
        public string RelatedEvent { get; set; }
        public string InstallType { get; set; }
        public string OrderSubType { get; set; }
        public string ReorderReason { get; set; }
        public FedlineManagedDeviceViewModel ManagedDeviceData { get; set; }
        public FedlineOrigDeviceViewModel OrigDeviceData { get; set; }
        public FedlineSiteLocationViewModel SiteInstallInfo { get; set; }
        public FedlineSiteLocationViewModel SiteShippingInfo { get; set; }
        public FedlineSiteLocationViewModel SiteDeactivationInfo { get; set; }
        public List<FedlineConfigViewModel> Configs { get; set; }
        public FedlineConfigViewModel Wan { get; set; }
        public FedlineConfigViewModel Lan { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? FrbSentDate { get; set; }
        public DateTime? FrbModifyDate { get; set; }
        public bool isMove { get; set; }
        public bool isExpedite { get; set; }
        public string existingDevice { get; set; }
        public string TimeBlock { get; set; }
        public DateTime? ActivityDate { get; set; }
        public string DesignDisplay { get; set; }
        public bool isInstallorHeadend { get; set; }
        public bool isDisconnect { get; set; }
        public bool isRefresh { get; set; }
        public bool isHeadend { get; set; }
    }
}