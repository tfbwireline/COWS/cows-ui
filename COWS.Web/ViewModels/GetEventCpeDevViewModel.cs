﻿namespace COWS.Web.ViewModels
{
    public class GetEventCpeDevViewModel
    {
        public int CpeDevId { get; set; }
        public string CpeOrdrId { get; set; }
        public string CpeM5OrdrNbr { get; set; }
        public int OrdrId { get; set; }
        public int EventId { get; set; }
        public string DeviceId { get; set; }
        public string AssocH6 { get; set; }
        public string Ccd { get; set; }
        public string CmpntStus { get; set; }
        public string RcptStus { get; set; }
        public string OdieDevNme { get; set; }
        public string CreatDt { get; set; }
    }
}