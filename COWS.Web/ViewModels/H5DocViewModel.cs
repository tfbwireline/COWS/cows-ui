﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class H5DocViewModel
    {
        public int H5DocId { get; set; }
        public int H5FoldrId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string CmntTxt { get; set; }
        public string CreatByUserAdid { get; set; }
        public string Base64string { get; set; }
    }
}