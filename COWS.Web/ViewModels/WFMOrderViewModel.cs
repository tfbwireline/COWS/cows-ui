﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class WFMOrderViewModel
    {
        public int UserId { get; set; }
        public string UserToAssign { get; set; }
        public ICollection<WFMOrdersModel> WFMOrders { get; set; }
        public bool NCIAssigner { get; set; }
        public string Profile { get; set; }
        public string SearchCriteria { get; set; }

    }
}

