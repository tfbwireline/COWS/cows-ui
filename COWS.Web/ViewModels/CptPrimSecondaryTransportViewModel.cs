﻿using System;

namespace COWS.Web.ViewModels
{
    public class CptPrimSecondaryTransportViewModel
    {
        public short CptPrimScndyTprtId { get; set; }
        public string CptPrimScndyTprt { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}