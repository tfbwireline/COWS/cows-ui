﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CptDocViewModel
    {
        public int CptDocId { get; set; }
        public int CptId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public string CmntTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatByAdId { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte RecStusId { get; set; }
        public string Base64string { get; set; }
    }
}
