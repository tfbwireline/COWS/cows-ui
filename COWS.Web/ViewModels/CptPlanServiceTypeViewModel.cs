﻿using System;

namespace COWS.Web.ViewModels
{
    public class CptPlanServiceTypeViewModel
    {
        public short CptPlnSrvcTypeId { get; set; }
        public string CptPlnSrvcType { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}