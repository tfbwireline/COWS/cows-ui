﻿using System;

namespace COWS.Web.ViewModels
{
    public class UserProfileViewModel
    {
        public short UsrPrfId { get; set; }
        public string UsrPrfNme { get; set; }
        public string UsrPrfDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public int UserId { get; set; }
        public byte? SrchCd { get; set; }


        public string NewStatus { get; set; }
        public string NewManagerAdid { get; set; }
        public string NewAutoAssignment { get; set; }
    }
}