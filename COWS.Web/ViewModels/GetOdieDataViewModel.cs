﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class GetOdieDataViewModel
    {
        public string H1 { get; set; }
        public IEnumerable<EventDiscoDevViewModel> EventDiscoDev { get; set; }
    }
}