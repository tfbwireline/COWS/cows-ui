﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class H5Doc2ViewModel
    {
        public int H5DocId { get; set; }
        public int H5FoldrId { get; set; }
        public string FileNme { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime CreatDt { get; set; }
        public string CreatByUserAdid { get; set; }
        public string Base64string { get; set; }
    }
}