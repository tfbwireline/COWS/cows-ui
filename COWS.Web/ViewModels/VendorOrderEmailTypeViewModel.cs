﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class VendorOrderEmailTypeViewModel
    {
        public byte VndrEmailTypeId { get; set; }
        public string VndrEmailTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
