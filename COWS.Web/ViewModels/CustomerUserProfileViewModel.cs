﻿using System;

namespace COWS.Web.ViewModels
{
    public class CustomerUserProfileViewModel
    {
        //[Required]
        //[DataType(DataType.Text)]
        //[MaxLength(30)]
        //public string VndrCd { get; set; }
        //[Required]
        //[DataType(DataType.Text)]
        //[MaxLength(50)]
        //public string VndrNme { get; set; }
        //public string CtryCd { get; set; }
        //public DateTime CreatDt { get; set; }
        //public DateTime? ModfdDt { get; set; }
        //public int? ModfdByUserId { get; set; }
        //public int CreatByUserId { get; set; }
        //public string CreatedByAdId { get; set; }
        //public string ModifiedByAdId { get; set; }
        //public byte RecStusId { get; set; }
        //public bool RecStatus => RecStusId == 1 ? true : false;
        public int usrH5Id { get; set; }

        public int custID { get; set; }
        public string custNme { get; set; }
        public string crtyNme { get; set; }
        public string usrNme { get; set; }
        public int userId { get; set; }
        public string mns { get; set; }
        public string isip { get; set; }
        public string status { get; set; }
        public int mnsCd { get; set; }
        public int isipCd { get; set; }
        public int statusId { get; set; }
        public DateTime? assignDT { get; set; }
    }
}