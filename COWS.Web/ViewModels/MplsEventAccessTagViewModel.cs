﻿using System;

namespace COWS.Web.ViewModels
{
    public class MplsEventAccessTagViewModel
    {
        public int MplsEventAccsTagId { get; set; }
        public int EventId { get; set; }
        public string LocCtyNme { get; set; }
        public string LocSttNme { get; set; }
        public string VasSolNbr { get; set; }
        public string TrsNet449Adr { get; set; }
        public byte? MplsAccsBdwdId { get; set; }
        public string MnsSipAdr { get; set; }
        public byte? MnsOffrgRoutrId { get; set; }
        public string MnsRoutgTypeId { get; set; }
        public byte? MnsPrfmcRptId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public string CtryCd { get; set; }
        public string PlDalCktNbr { get; set; }
        public string TrnsprtOeFtnNbr { get; set; }
    }
}