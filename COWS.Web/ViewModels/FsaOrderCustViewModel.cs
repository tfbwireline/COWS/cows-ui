﻿using System;

namespace COWS.Web.ViewModels
{
    public class FsaOrderCustViewModel
    {
        public int OrdrId { get; set; }
        public string CisLvlType { get; set; }
        public int CustId { get; set; }
        public string BrCd { get; set; }
        public string CurrBillCycCd { get; set; }
        public string FutBillCycCd { get; set; }
        public string SrvcSubTypeId { get; set; }
        public string SoiCd { get; set; }
        public string SalsPersnPrimCid { get; set; }
        public string SalsPersnScndyCid { get; set; }
        public string ClliCd { get; set; }
        public DateTime CreatDt { get; set; }
        public string TaxXmptCd { get; set; }
        public string CharsCustId { get; set; }
        public string SiteId { get; set; }
        public int FsaOrdrCustId { get; set; }
        public string CustNme { get; set; }

        //public virtual FsaOrdr Ordr { get; set; }
    }
}