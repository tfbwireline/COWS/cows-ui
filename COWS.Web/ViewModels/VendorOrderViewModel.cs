﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class VendorOrderViewModel
    {
        public int VndrOrdrId { get; set; }
        public int VndrFoldrId { get; set; }
        public int? OrdrId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public byte RecStusId { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte VndrOrdrTypeId { get; set; }
        public bool TrmtgCd { get; set; }
        public int? PrevOrdrId { get; set; }
        public bool? BypasVndrOrdrMsCd { get; set; }

        public virtual VendorFolderViewModel VndrFoldr { get; set; }
        //public virtual OrderVendorViewModel Ordr { get; set; }
        public virtual ICollection<VendorOrderFormViewModel> VndrOrdrForm { get; set; }
        public virtual ICollection<VendorOrderEmailViewModel> VndrOrdrEmail { get; set; }
        public virtual ICollection<VendorOrderMsViewModel> VndrOrdrMs { get; set; }

        //Additional field
        public string VndrNme { get; set; }
        public string CtryNme { get; set; }
        public string VndrCd { get; set; }
        public string VndrOrdrTypeDes { get; set; }
        public byte CsgLvlId { get; set; }
        public int CustId { get; set; }
        public H5FolderViewModel H5Foldr { get; set; }
        public string Ftn { get; set; }
        public string ProductType { get; set; }
        public string OrderType { get; set; }

        public ICollection<object> VendorOrderEmail { get; set; }
    }
}
