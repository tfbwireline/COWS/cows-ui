﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class AccessMbViewModel
    {
        public int AccsMbId { get; set; }
        public string AccsMbDes { get; set; }
        public DateTime CreatDt { get; set; }
        public int? SortId { get; set; }
        public byte? AsrTypeId { get; set; }
    }
}