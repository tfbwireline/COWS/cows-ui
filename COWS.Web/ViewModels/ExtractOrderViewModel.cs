﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class ExtractOrderViewModel
    {
        public int M5OrderId { get; set; }
        public int RelatedM5OrderId { get; set; }
        public string OrderType { get; set; }
        public string DeviceId { get; set; }
        public bool IsTransaction { get; set; }
    }
}