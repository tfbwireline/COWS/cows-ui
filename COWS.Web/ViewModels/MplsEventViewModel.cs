﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class MplsEventViewModel
    {
        public int AuthStatus { get; set; }
        public string EventTitleTxt { get; set; }
        public string EventDes { get; set; }
        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public string EventStusDes { get; set; }
        public string Ftn { get; set; }
        public string CharsId { get; set; }
        public string H1 { get; set; }
        public string H6 { get; set; }
        public byte EventCsgLvlId { get; set; }
        public string CustNme { get; set; }
        public string CustCntctNme { get; set; }
        public string CustCntctPhnNbr { get; set; }
        public string CustEmailAdr { get; set; }
        public string CustCntctCellPhnNbr { get; set; }
        public string CustCntctPgrNbr { get; set; }
        public string CustCntctPgrPinNbr { get; set; }
        public int ReqorUserId { get; set; }
        public UserViewModel ReqorUser { get; set; }
        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public int SalsUserId { get; set; }
        public UserViewModel SalsUser { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public string DocLinkTxt { get; set; }
        public string DdAprvlNbr { get; set; }
        public bool NddUpdtdCd { get; set; }
        public string DesCmntTxt { get; set; }
        public byte MplsEventTypeId { get; set; }
        public byte VpnPltfrmTypeId { get; set; }
        public byte? IpVerId { get; set; }
        public string ActyLocaleId { get; set; }
        public string MultiVrfReqId { get; set; }
        public string VrfNme { get; set; }
        public bool MdsMngdCd { get; set; }
        public bool AddE2eMontrgCd { get; set; }
        public string MdsVrfNme { get; set; }
        public string MdsIpAdr { get; set; }
        public string MdsDlci { get; set; }
        public string MdsStcRteDes { get; set; }
        public bool OnNetMontrgCd { get; set; }
        public bool WhlslPtnrCd { get; set; }
        public byte? WhlslPtnrId { get; set; }
        public string NniDsgnDocNme { get; set; }
        public bool CxrPtnrCd { get; set; }
        public string MplsCxrPtnrId { get; set; } //VndrCd
        public bool OffNetMontrgCd { get; set; }
        public bool MgrtnCd { get; set; }
        public byte? MplsMgrtnTypeId { get; set; }
        public bool ReltdCmpsNcrCd { get; set; }
        public string ReltdCmpsNcrNme { get; set; }

        public bool EsclCd { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime StrtTmst { get; set; }
        public short EventDrtnInMinQty { get; set; }
        public short ExtraDrtnTmeAmt { get; set; }
        public DateTime EndTmst { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public byte? WrkflwStusId { get; set; }
        public string WrkflwStusDes { get; set; }

        //public byte RecStusId { get; set; }
        //public bool RecStatus => RecStusId == 1 ? true : false;
        //public int CreatByUserId { get; set; }
        //public int? ModfdByUserId { get; set; }
        //public string CreatedByAdId { get; set; }
        //public string ModifiedByAdId { get; set; }
        //public string CreatedByDisplayName { get; set; }
        //public string ModifiedByDisplayName { get; set; }
        //public DateTime? ModfdDt { get; set; }
        //public DateTime CreatDt { get; set; }
        public int? SowsEventId { get; set; }

        public List<int> MplsEventActyTypeIds { get; set; }
        public List<int> MplsEventVasTypeIds { get; set; }
        public EntityBaseViewModel EntityBase { get; set; }
        public ICollection<MplsEventAccessTagViewModel> MplsEventAccsTag { get; set; }
        public List<int> Activators { get; set; }

        public int ReviewerUserId { get; set; }
        public string ReviewerComments { get; set; }
        public int ActivatorUserId { get; set; }
        public string ActivatorComments { get; set; }

        public List<int> EventSucssActyIds { get; set; }
        public List<int> EventFailActyIds { get; set; }
        public string PreCfgConfgCode { get; set; }
        public string Profile { get; set; }
        public int FailCode { get; set; }
    }
}