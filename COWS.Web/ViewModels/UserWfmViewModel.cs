﻿using System;

namespace COWS.Web.ViewModels
{
    public class UserWfmViewModel
    {
        public int WfmId { get; set; }
        public int UserId { get; set; }
        public short GrpId { get; set; }
        public byte RoleId { get; set; }
        public byte? OrdrTypeId { get; set; }
        public byte? ProdTypeId { get; set; }
        public string PltfrmCd { get; set; }
        public string VndrCd { get; set; }
        public string OrgtngCtryCd { get; set; }
        public string IplTrmtgCtryCd { get; set; }
        public byte? OrdrActnId { get; set; }
        public byte? WfmAsmtLvlId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short? UsrPrfId { get; set; }
    }
}