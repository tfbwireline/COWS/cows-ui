﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class OdieRspnInfoViewModel
    {
        public int RspnInfoId { get; set; }
        public int ReqId { get; set; }
        public string ModelId { get; set; }
        public string DevId { get; set; }
        public string SerialNo { get; set; }
        public string RdsnNbr { get; set; }
        public bool FastTrkCd { get; set; }
        public bool DspchRdyCd { get; set; }
        public DateTime RspnInfoDt { get; set; }
        public bool SlctdCd { get; set; }
        public DateTime CreatDt { get; set; }
        public string ManfId { get; set; }
        public string OptOutCd { get; set; }
        public string OptOutReasTxt { get; set; }
        public string BusJustnTxt { get; set; }
        public string MgtTxt { get; set; }
        public string OneMbTxt { get; set; }
        public string FrwlProdCd { get; set; }
        public int? FsaMdsEventId { get; set; }
        public int? EventId { get; set; }
        public string H6CustId { get; set; }
    }
}
