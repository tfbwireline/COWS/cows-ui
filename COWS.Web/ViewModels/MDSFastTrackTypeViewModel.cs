﻿using System;

namespace COWS.Web.ViewModels
{
    public class MDSFastTrackTypeViewModel
    {
        public string MdsFastTrkTypeId { get; set; }
        public string MdsFastTrkTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public string ModifiedByAdid { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}