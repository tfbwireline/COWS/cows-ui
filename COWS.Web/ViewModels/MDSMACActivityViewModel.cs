﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class MDSMACActivityViewModel
    {
        public int MdsMacActyId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string MdsMacActyNme { get; set; }

        public short MinDrtnTmeReqrAmt { get; set; }
        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public string ModifiedByAdId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}