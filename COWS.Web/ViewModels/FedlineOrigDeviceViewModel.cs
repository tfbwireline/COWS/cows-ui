﻿namespace COWS.Web.ViewModels
{
    public class FedlineOrigDeviceViewModel
    {
        public string DeviceName { get; set; }
        public string SerialNum { get; set; }
        public int? OrigReqId { get; set; }
        public string Model { get; set; }
    }
}