﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class VendorFolderViewModel
    {
        public int VndrFoldrId { get; set; }
        public string VndrCd { get; set; }
        public string VndrNme { get; set; }
        public string StreetAdr1 { get; set; }
        public string StreetAdr2 { get; set; }
        public string CtyNme { get; set; }
        public string PrvnNme { get; set; }
        public string ZipPstlCd { get; set; }
        public string SttCd { get; set; }
        public string SttNme { get; set; }
        public string CmntTxt { get; set; }
        public string VndrEmailListTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatByUserName { get; set; }
        public string ModfdByUserName { get; set; }
        public byte RecStusId { get; set; }
        public string CtryCd { get; set; }
        public string CtryNme { get; set; }
        public string BldgNme { get; set; }
        public string FlrId { get; set; }
        public string RmNbr { get; set; }
        public int TemplateCnt { get; set; }

        public ICollection<VendorFolderContactViewModel> VndrFoldrCntct { get; set; }
        public ICollection<VendorOrderViewModel> VndrOrdr { get; set; }
        public ICollection<VendorTemplateViewModel> VndrTmplt { get; set; }
        public ICollection<DocEntityViewModel> DocList { get; set; }
    }
}