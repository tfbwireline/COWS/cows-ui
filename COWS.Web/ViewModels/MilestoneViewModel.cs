﻿using System;

namespace COWS.Web.ViewModels
{
    public class MilestoneViewModel
    {
        public int OrdrId { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? CustomerCommitDate { get; set; }
        public DateTime? CustomerWantDate { get; set; }
        public DateTime? ValidatedDate { get; set; }
        public DateTime? OrderSentToVendorDate { get; set; }
        public DateTime? OrderAcknowledgedDate { get; set; }
        public DateTime? TargetDeliveryDate { get; set; }
        public DateTime? TargetDeliveryDateReceived { get; set; }
        public DateTime? AccessDeliveryDate { get; set; }
        public string VendorContractTerm { get; set; }
        public DateTime? VendorContractStartDate { get; set; }
        public DateTime? AccessAcceptDate { get; set; }
        public DateTime? BillClearInstallDate { get; set; }
        public string CustomerContractTerm { get; set; }
        public DateTime? CustomerContractStartDate { get; set; }
        public DateTime? VendorCancellationDate { get; set; }
        public DateTime? VendorConfirmedDisconnectDate { get; set; }
        public DateTime? DisconnectCompletedDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public bool? BypassVendorOrderCode { get; set; }
        public bool? NrmUpdate { get; set; }
        public bool IsBillClearInstallDateChanged { get; set; }
    }
}