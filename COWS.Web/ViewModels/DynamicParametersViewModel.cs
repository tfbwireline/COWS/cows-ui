﻿using COWS.Entities.Models;
using COWS.Entities.QueryModels;
using System;
using System.Collections.Generic;
using System.Data;

namespace COWS.Web.ViewModels
{
    public class DynamicParametersViewModel
    {
        // Custom ViewModel for dynamic parameters
        public int EventTypeId { get; set; }

        public int EnhanceServiceId { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public int IpVersionId { get; set; }

        //Check Current Slot properties
        public int EventId { get; set; }

        public string UID { get; set; }

        public string Product { get; set; }
        public string SubmitType { get; set; }
        public string IsMDSFT { get; set; }
        public string ExtraDuration { get; set; }
        public List<int> mdsNtwkActy { get; set; }
        public int vASCECd { get; set; }
        public string macActivityType { get; set; }
        public string StTime { get; set; }
        public string StHr { get; set; }
        public string StMin { get; set; }
        public string MplsEventType { get; set; }
        public List<string> MplsActivityTypes { get; set; }
        public string VpnPlatformType { get; set; }
        public List<string> MplsVasTypes { get; set; }
        public string MplsActyLocale { get; set; }
        public string MultiVRF { get; set; }
        public string IsWhlSalePrtnr { get; set; }
        public string MdsMnged { get; set; }
        public string NoOfAccess { get; set; }
        public string SlnkEventType { get; set; }
        public string SlnkActyType { get; set; }

        #region MDS
        public int[] MdsMacActivityTypeIds { get; set; }
        public List<MdsEventOdieDevView> ManagedDevice { get; set; }
        public string h1 { get; set; }
        public string customerName { get; set; }
        public string isFirewallSecurity { get; set; } // True or False
        public int[] ActivityType { get; set; } // MDS Network Activity Type
        public string mdsOld { get; set; } // True or False
        public List<MdsEventMacActy> mdsMacAct { get; set; }
        public List<MdsEventOdieDev> mngDevTbl { get; set; }
        public string anyVirtualConn { get; set; }
        public DataTable virtualTable { get; set; }
        public string isUSInternational { get; set; }

        public string isSprintCPETabCnt { get; set; }
        public string isSPRFWiredDevTrptReq { get; set; }
        public List<MdsEventSlnkWiredTrpt> mdsSPRFWiredTable { get; set; }
        public string isDSLSBICCustTrptReq { get; set; }
        public List<MdsEventDslSbicCustTrpt> mdsDSLSBICCustTable { get; set; }
        public string isWirelessDeviceTransportRequired { get; set; }
        public List<MdsEventWrlsTrpt> mdsWirelessTable { get; set; }
        public DataTable mdsNtwkTrpt { get; set; }
        public string scFlag { get; set; }
        public string mngDevTblCnt { get; set; }
        public string isSpclPrj { get; set; }
        #endregion
    }
}