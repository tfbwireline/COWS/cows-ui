﻿using COWS.Entities.Models;
using System;

namespace COWS.Web.ViewModels
{
    public class OrderNoteViewModel
    {
        public int NteId { get; set; }
        public byte NteTypeId { get; set; }
        public int OrdrId { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public string NteTxt { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }

        // Additional Fields
        public string creatByUserFullName { get; set; }
        public string NteTypeDes { get; set; }
    }
}