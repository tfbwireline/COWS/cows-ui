﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class TrptOrdrViewModel
    {
        public string AccsMbDes { get; set; }
        public int? AccsCtySiteId { get; set; }
        //public string AccessMegabyteValue { get; set; }
        //public int? AccessCityName { get; set; }
        public short? AEndRgnId { get; set; }
    }
}

