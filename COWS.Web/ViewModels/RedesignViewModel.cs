﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class RedesignViewModel
    {
        public int RedsgnId { get; set; }
        public string RedsgnNbr { get; set; }
        public string H1Cd { get; set; }
        public string NteAssigned { get; set; }
        public string NteAssignedFullName { get; set; }
        public string PmAssigned { get; set; }
        public string PmAssignedFullName { get; set; }
        public byte RedsgnTypeId { get; set; }
        public short StusId { get; set; }
        public bool EntireNwCkdId { get; set; }
        public short? RedsgnDevStusId { get; set; }
        public int CretdByCd { get; set; }
        public DateTime CretdDt { get; set; }
        public int? ModfdByCd { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime? SubmitDt { get; set; }
        public DateTime? ModSubmitDt { get; set; }
        public DateTime? SlaDt { get; set; }
        public string OdieCustId { get; set; }
        public string ApprovNbr { get; set; }
        public decimal? CostAmt { get; set; }
        public DateTime? ExprtnDt { get; set; }
        public byte CsgLvlId { get; set; }
        public bool? ExtXpirnFlgCd { get; set; }
        public byte? RedsgnCatId { get; set; }
        public string DsgnDocLocTxt { get; set; }
        public string SdeAsnNme { get; set; }
        public string SdeAsnFullNme { get; set; }
        public decimal? NteLvlEffortAmt { get; set; }
        public decimal? PmLvlEffortAmt { get; set; }
        public decimal? NeLvlEffortAmt { get; set; }
        public decimal? NteOtLvlEffortAmt { get; set; }
        public decimal? PmOtLvlEffortAmt { get; set; }
        public decimal? NeOtLvlEffortAmt { get; set; }
        public decimal? SpsLvlEffortAmt { get; set; }
        public string NeAsnNme { get; set; }
        public string NeAsnFullNme { get; set; }
        public string SowsFoldrPathNme { get; set; }
        public decimal? SpsOtLvlEffortAmt { get; set; }
        public bool BillOvrrdnCd { get; set; }
        public decimal? BillOvrrdnAmt { get; set; }
        public bool IsSdwan { get; set; }
        public string CustNme { get; set; }
        public string CustEmailAdr { get; set; }
        public string DsgnDocNbrBpm { get; set; }
        public bool? CiscSmrtLicCd { get; set; }
        public string SmrtAccntDmn { get; set; }
        public string VrtlAccnt { get; set; }
        public decimal? MssImplEstAmt { get; set; }
        public string BpmRedsgnNbr { get; set; }
        public string AddlDocTxt { get; set; }

        // Additional properties
        public string Description { get; set; }
        public string Caveats { get; set; }
        public string EmailNotifications { get; set; }
        public short OldStusId { get; set; }
        public string StatusDesc { get; set; }
        public int OdieRspnInfoReqId { get; set; }
        public string NewDevice { get; set; }
        public bool SuppressEmail { get; set; }
        public bool IsBillable { get; set; }
        public RedesignRecLockViewModel RedesignRecLock { get; set; }

        public bool SystemCreatedRedesign { get; set; }

        public ICollection<RedesignDocViewModel> Docs { get; set; }
        public ICollection<RedesignDevicesInfoViewModel> Devices { get; set; }
        public ICollection<RedesignNotesViewModel> Notes { get; set; }
        public ICollection<RedesignEmailNotificationViewModel> Emails { get; set; }
        public ICollection<string> SelectedOdieDevices { get; set; }
        public ICollection<ContactDetailViewModel> ContactDetails { get; set; }
    }
}
