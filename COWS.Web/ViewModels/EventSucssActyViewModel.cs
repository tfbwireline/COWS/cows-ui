﻿using System;

namespace COWS.Web.ViewModels
{
    public partial class EventSucssActyViewModel
    {
        public int EventHistId { get; set; }
        public short SucssActyId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }

        //public virtual EventHist EventHist { get; set; }
        //public virtual LkRecStus RecStus { get; set; }
        //public virtual LkSucssActy SucssActy { get; set; }
    }
}