﻿namespace COWS.Web.ViewModels
{
    public class SDEOpportunityNoteViewModel
    {
        public int SdeNoteID { get; set; }
        public string NoteText { get; set; }
    }
}