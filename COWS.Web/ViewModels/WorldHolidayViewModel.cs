﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class WorldHolidayViewModel
    {
        public int WrldHldyId { get; set; }
        public DateTime WrldHldyDt { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(75)]
        public string WrldHldyDes { get; set; }

        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public string ModifiedByAdId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CtyNme { get; set; }
        public string CtryCd { get; set; }
    }
}