﻿using System;

namespace COWS.Web.ViewModels
{
    public class WholesalePartnerViewModel
    {
        public byte WhlslPtnrId { get; set; }
        public string WhlslPtnrNme { get; set; }
        public byte RecStusId { get; set; }
        public int? CreatByUserId { get; set; }
        public int ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}