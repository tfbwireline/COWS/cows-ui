﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class VendorViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string VndrCd { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string VndrNme { get; set; }

        public string CtryCd { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public string ModifiedByAdId { get; set; }
        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public string VndrDsplNme => string.Format("{0} - {1}", VndrCd, VndrNme);
    }
}