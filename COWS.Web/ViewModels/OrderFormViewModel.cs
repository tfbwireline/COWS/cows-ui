﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class OrderFormViewModel
    {
        public FsaOrderViewModel FsaInfo { get; set; }
        public MilestoneViewModel MilestoneInfo { get; set; }
        public CircuitFormViewModel CircuitInfo { get; set; }
        public int TaskId { get; set; }
        public string nteTxt { get; set; }
        public int? UserProfileId { get; set; }
    }
}

