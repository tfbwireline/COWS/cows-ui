﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class TaskViewModel
    {
        public short TaskId { get; set; }
        public string TaskNme { get; set; }
        public short OrdrStusId { get; set; }
        public short WgStusId { get; set; }
        public int ModfdByUserId { get; set; }
        public DateTime ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string TaskDes { get; set; }
    }
}