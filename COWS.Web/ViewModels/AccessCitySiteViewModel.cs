﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class AccessCitySiteViewModel
    {
        public int AccsCtySiteId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(20)]
        public string AccsCtyNmeSiteCd { get; set; }

        public DateTime CreatDt { get; set; }
    }
}