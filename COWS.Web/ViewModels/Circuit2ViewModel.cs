﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class Circuit2ViewModel
    {
        public int CktId { get; set; }
        public int? OrdrId { get; set; }
        public string VndrCktId { get; set; }
        public string LecId { get; set; }
        public bool TrmtgCd { get; set; }
        public string RtngPrcol { get; set; }
        public string IpAdrQty { get; set; }
        public string CustIpAdr { get; set; }
        public string ScaDet { get; set; }
        public string PlSeqNbr { get; set; }
        public virtual ICollection<CircuitMs2ViewModel> CktMs { get; set; }
    }
}