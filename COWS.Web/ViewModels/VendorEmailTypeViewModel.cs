﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class VendorEmailTypeViewModel
    {
        public byte VndrEmailTypeId { get; set; }
        public string VndrEmailTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
    }
}