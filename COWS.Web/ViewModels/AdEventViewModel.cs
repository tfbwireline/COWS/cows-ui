﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class AdEventViewModel
    {
        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public string Ftn { get; set; }
        public string CharsId { get; set; }
        public string H1 { get; set; }
        public string H6 { get; set; }
        public short EnhncSrvcId { get; set; }
        public int ReqorUserId { get; set; }
        public int SalsUserId { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public string DesCmntTxt { get; set; }
        public bool CpeAtndCd { get; set; }
        public string DocLinkTxt { get; set; }
        public bool EsclCd { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime StrtTmst { get; set; }
        public short ExtraDrtnTmeAmt { get; set; }
        public DateTime EndTmst { get; set; }
        public byte WrkflwStusId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte? IpVerId { get; set; }
        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public short EventDrtnInMinQty { get; set; }
        public int? SowsEventId { get; set; }
        public string CustNme { get; set; }
        public string CustCntctNme { get; set; }
        public string CustCntctPhnNbr { get; set; }
        public string CustEmailAdr { get; set; }
        public string CustCntctCellPhnNbr { get; set; }
        public string CustCntctPgrNbr { get; set; }
        public string CustCntctPgrPinNbr { get; set; }
        public string EventTitleTxt { get; set; }
        public string EventDes { get; set; }
        public string CreatByUserDsplNme { get; set; }
        public string CreatByUserAdid { get; set; }
        public string EnhncSrvcNme { get; set; }
        public string EventStusDes { get; set; }
        public string WrkflwStusDes { get; set; }
        public ICollection<AdEventAccessTagViewModel> AdEventAccsTag { get; set; }

        // Added
        public List<int> Activators { get; set; }

        public int AuthStatus { get; set; }
        public byte EventCsgLvlId { get; set; }

        public int ReviewerUserId { get; set; }
        public string ReviewerComments { get; set; }
        public int ActivatorUserId { get; set; }
        public string ActivatorComments { get; set; }

        public List<int> EventSucssActyIds { get; set; }
        public List<int> EventFailActyIds { get; set; }
        public string PreCfgConfgCode { get; set; }
        public string Profile { get; set; }
        public int FailCode { get; set; }
    }
}