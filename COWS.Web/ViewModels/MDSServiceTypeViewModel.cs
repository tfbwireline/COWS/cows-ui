﻿using System;

namespace COWS.Web.ViewModels
{
    public class MDSServiceTypeViewModel
    {
        public short SrvcTypeId { get; set; }
        public string SrvcTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}