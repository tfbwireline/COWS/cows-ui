﻿namespace COWS.Web.ViewModels
{
    public class OdieCustRspnViewModel
    {
        public int RspnId { get; set; }
        public string CustTeamPdl { get; set; }
        public string MnspmId { get; set; }
        public string SowsFoldrPathNme { get; set; }
    }
}