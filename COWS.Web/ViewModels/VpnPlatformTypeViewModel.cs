﻿using System;

namespace COWS.Web.ViewModels
{
    public class VpnPlatformTypeViewModel
    {
        public byte VpnPltfrmTypeId { get; set; }
        public string VpnPltfrmTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short EventTypeId { get; set; }
    }
}