﻿using System;

namespace COWS.Web.ViewModels
{
    public class TimeSlotOccurrenceViewModel
    {
        private string _displayTimeSlot;
        public byte TimeSlotID { get; set; }
        public short CurFilledTimeSlots { get; set; }
        public short MaxTimeSlots { get; set; }
        public short TotalTimeSlots { get; set; }
        public DateTime Day { get; set; }
        public int RsrcAvlbltyID { get; set; }
        public short CurAvailTimeSlots { get; set; }
        public string IsOverbooked { get; set; }

        public string DisplayTimeSlot
        {
            get
            {
                FormattedTimeSlot();
                return _displayTimeSlot;
            }
        }

        public string EventTypeDes { get; set; }
        public byte RAP { get; set; }
        public TimeSpan TimeSlotEnd { get; set; }
        public TimeSpan TimeSlotStart { get; set; }
        public int EventTypeID { get; set; }

        public void FormattedTimeSlot()
        {
            string toReturn = null;
            string tTime = null;

            if (TimeSlotStart.Hours < 12)
                tTime = TimeSlotStart.Hours.ToString() + ":" + TimeSlotStart.Minutes.ToString("00") + " AM";
            else if (TimeSlotStart.Hours == 12)
                tTime = TimeSlotStart.Hours.ToString() + ":" + TimeSlotStart.Minutes.ToString("00") + " PM";
            else
                tTime = (TimeSlotStart.Hours - 12).ToString() + ":" + TimeSlotStart.Minutes.ToString("00") + " PM";

            toReturn = tTime + "-";

            if (TimeSlotEnd.Hours < 12)
                tTime = TimeSlotEnd.Hours.ToString() + ":" + TimeSlotEnd.Minutes.ToString("00") + " AM";
            else if (TimeSlotEnd.Hours == 12)
                tTime = TimeSlotEnd.Hours.ToString() + ":" + TimeSlotEnd.Minutes.ToString("00") + " PM";
            else
                tTime = (TimeSlotEnd.Hours - 12).ToString() + ":" + TimeSlotEnd.Minutes.ToString("00") + " PM";

            _displayTimeSlot = toReturn + tTime;
        }
    }
}