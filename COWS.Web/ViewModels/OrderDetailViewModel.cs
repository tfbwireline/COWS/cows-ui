﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class OrderDetailViewModel
    {
        public int OrderId { get; set; }
        public string Ftn { get; set; }
        public string RelatedFtn { get; set; }
        public string ParentFtn { get; set; }
        public string OrderTypeDesc { get; set; }
        public string OrderSubTypeCd { get; set; }
        public string ProductTypeDesc { get; set; }
        public DateTime? Ccd { get; set; }
        public DateTime? FsaCcd { get; set; }
        public int? H5FolderId { get; set; }
        public int? OrderCategoryId { get; set; }
        public int? RegionId { get; set; }
        public string PlatformCd { get; set; }
        public string FsaOrderTypeDesc { get; set; }
        public int OrderStatusId { get; set; }
        public int CsgLvlId { get; set; }

        // Additional Order Details
        public H5FolderViewModel H5Foldr { get; set; }
        public FsaOrderViewModel FsaOrdr { get; set; }
        public NccoOrderViewModel NccoOrdr { get; set; }
        public VendorOrderViewModel VndrOrdr { get; set; }

        public ICollection<OrderNoteViewModel> OrdrNte { get; set; }
        public ICollection<OrderStdiHistoryViewModel> OrdrStdiHist { get; set; }
        public ICollection<VendorOrderViewModel> VndrOrdrOrdr { get; set; }
        public ICollection<CCDHistoryViewModel> CcdHist { get; set; }
        public ICollection<OrderAddressViewModel> OrdrAdr { get; set; }
        public ICollection<OrderContactViewModel> OrdrCntct { get; set; }
        public ICollection<OrderVlanViewModel> OrdrVlan { get; set; }
        public OrderContactViewModel H6Contact { get; set; }
        //public ICollection<object> OrderAddress { get; set; }
        //public ICollection<object> OrderStdiHist { get; set; }
    }
}