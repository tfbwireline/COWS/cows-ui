﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace COWS.Web.ViewModels
{
    public class CurrencyFileViewModel
    {
        private IFormFile _UploadFile;

        public int SrcCurFileId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntntBtsm { get; set; }
        public string PrsdCurListTxt { get; set; }
        public DateTime? PrcsdDt { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatDt { get; set; }

        public string Base64string { get; set; }

        //[Display(Name = "file")]
        //[DataType(DataType.Upload)]
        //public IFormFile UploadFile
        //{
        //    get => _UploadFile;
        //    set
        //    {
        //        _UploadFile = value;
        //        if (_UploadFile != null)
        //        {
        //            FileNme = _UploadFile.FileName; // Set FileName
        //            using (var ms = new MemoryStream())
        //            {
        //                _UploadFile.CopyTo(ms);
        //                var fileBytes = ms.ToArray();
        //                FileCntntBtsm = fileBytes; // Set byte array for file
        //            }
        //        }
        //    }
        //}
    }
}