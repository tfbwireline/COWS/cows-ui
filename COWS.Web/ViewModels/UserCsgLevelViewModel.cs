﻿using System;

namespace COWS.Web.ViewModels
{
    public class UserCsgLevelViewModel
    {
        public int UserId { get; set; }
        public byte CsgLvlId { get; set; }
        public string EmplActCd { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}