﻿using System;

namespace COWS.Web.ViewModels
{
    public class FilterOperatorViewModel
    {
        public string FiltrOprId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public string FiltrOprDes { get; set; }
        public string LogicOprCd { get; set; }
    }
}