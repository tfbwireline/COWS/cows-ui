﻿using System;

namespace COWS.Web.ViewModels
{
    public class RedesignCategoryViewModel
    {
        public byte RedsgnCatId { get; set; }
        public string RedsgnCatDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public DateTime CreatDt { get; set; }
    }
}