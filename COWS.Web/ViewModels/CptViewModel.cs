﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class CptViewModel
    {
        public int CptId { get; set; }
        public string CptCustNbr { get; set; }
        public string CustShrtNme { get; set; }
        public string SubnetIpAdr { get; set; }
        public string StartIpAdr { get; set; }
        public string EndIpAdr { get; set; }
        public string QipSubnetNme { get; set; }
        public string MplsIpAdr { get; set; }
        public string WpaaSIpAdr { get; set; }
        public int SubmtrUserId { get; set; }
        public string SeEmailAdr { get; set; }
        public string AmEmailAdr { get; set; }
        public string IpmEmailAdr { get; set; }
        public string H1 { get; set; }
        public short DevCnt { get; set; }
        public bool? GovtCustCd { get; set; }
        public bool? ScrtyClrnceCd { get; set; }
        public bool SprntWhlslReslrCd { get; set; }
        public string SprntWhlslReslrNme { get; set; }
        public bool TacacsCd { get; set; }
        public bool EncMdmCd { get; set; }
        public bool HstMgndSrvcCd { get; set; }
        public short CptStusId { get; set; }
        public string CptStatus { get; set; }
        public int? CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public string PreShrdKey { get; set; }
        public bool RevwdPmCd { get; set; }
        public bool RetrnSdeCd { get; set; }
        public bool SdwanCd { get; set; }
        public byte CsgLvlId { get; set; }
        public string CompnyNme { get; set; }
        public string CustAdr { get; set; }
        public string CustFlrBldgNme { get; set; }
        public string CustCtyNme { get; set; }
        public string CustSttPrvnNme { get; set; }
        public string CustCtryRgnNme { get; set; }
        public string CustZipCd { get; set; }
        public string PreStgdKey { get; set; }

        public string SubmitterName { get; set; }
        public string SubmitterEmail { get; set; }
        public string SubmitterPhone { get; set; }
        public string Notes { get; set; }
        public bool CancelCpt { get; set; }

        public int? Gatekeeper { get; set; }
        public int? Manager { get; set; }
        public int? Mss { get; set; }
        public int? Nte { get; set; }
        public int? Pm { get; set; }
        public int? Crm { get; set; }

        public string ReviewedByPMDate { get; set; }
        public string ReturnedToSDEDate { get; set; }

        public List<int> CustomerTypes { get; set; }
        public List<int> HostedManagedServices { get; set; }
        public List<int> MdsPlnSrvcTiers { get; set; }
        public List<int> MdsSupportTiers { get; set; }
        public List<int> MssPlnSrvcTypes { get; set; }
        public List<int> MssMngdAuthenticationProds { get; set; }
        public List<int> MvsProdTypes { get; set; }
        public List<int> PrimSites { get; set; }
        public ICollection<CptDocViewModel> CptDocs { get; set; }
        public ICollection<CptRelatedInfoViewModel> RelatedInfos { get; set; }
        public ICollection<CptProvisionViewModel> CptPrvsn { get; set; }
        public ICollection<CptUserAsmtViewModel> CptUserAsmt { get; set; }
        public ICollection<CptHistViewModel> CptHist { get; set; }
        public ICollection<ContactDetailViewModel> ContactDetails { get; set; }
    }
}