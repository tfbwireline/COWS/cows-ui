﻿namespace COWS.Web.ViewModels
{
    public class SDEOpportunityProductViewModel
    {
        public int ProductTypeID { get; set; }
        public string ProductType { get; set; }
        public string ProductTypeDesc { get; set; }
        public int Qty { get; set; }
    }
}