﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class SdeOpportunityViewModel
    {
        public int SdeOpportunityId { get; set; }

        public int StatusId { get; set; }
        public string Status { get; set; }
        public string StatusDesc { get; set; }
        public string CompanyName { get; set; }

        public string Address { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        public string ContactName { get; set; }

        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string SaleType { get; set; }
        public string SaleTypeDesc { get; set; }
        public bool IsRFP { get; set; }
        public bool IsWirelineCustomer { get; set; }

        public bool IsManagedServicesCustomer { get; set; }
        public string Comments { get; set; }
        public string RequestorLocation { get; set; }
        public string AssignedTo { get; set; }
        public int AssignedToId { get; set; }
        public string CcEmail { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string CreatedByFullName { get; set; }
        public int CreatedByUserId { get; set; }

        public List<SDEOpportunityDocViewModel> SdeDocs { get; set; }

        public List<SDEOpportunityProductViewModel> SdeProducts { get; set; }
        public List<SDEOpportunityNoteViewModel> SdeNotes { get; set; }
    }
}