﻿using COWS.Entities.Models;
using System;

namespace COWS.Web.ViewModels
{
    public class OrderNote2ViewModel
    {
        public int NteId { get; set; }
        public int OrdrId { get; set; }
        public byte NteTypeId { get; set; }
        public string NteTypeDes { get; set; }
        public string NteTxt { get; set; }
        public string creatByUserFullName { get; set; }
        public DateTime CreatDt { get; set; }
    }
}