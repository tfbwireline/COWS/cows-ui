﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class SplkEventViewModel
    {
        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public string Ftn { get; set; }
        public string CharsId { get; set; }
        public string H1 { get; set; }
        public string H6 { get; set; }
        public int? ReqorUserId { get; set; }
        public int? SalsUserId { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }
        public string DsgnCmntTxt { get; set; }
        public string ReviewCmntTxt { get; set; }
        public byte? SplkEventTypeId { get; set; }
        public byte SplkActyTypeId { get; set; }
        public byte? IpVerId { get; set; }
        public bool MdsMngdCd { get; set; }
        public bool EsclCd { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime StrtTmst { get; set; }
        public short ExtraDrtnTmeAmt { get; set; }
        public DateTime EndTmst { get; set; }
        public byte? WrkflwStusId { get; set; }
        public byte EventCsgLvlId { get; set; }
        public string EventStusDes { get; set; }

        //part of EntityBase
        public byte RecStusId { get; set; }

        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public short EventDrtnInMinQty { get; set; }
        public int? SowsEventId { get; set; }
        public bool ReltdCmpsNcrCd { get; set; }
        public string ReltdCmpsNcrNme { get; set; }
        public string CustNme { get; set; }
        public string CustCntctNme { get; set; }
        public string CustCntctPhnNbr { get; set; }
        public string CustEmailAdr { get; set; }
        public string CustCntctCellPhnNbr { get; set; }
        public string CustCntctPgrNbr { get; set; }
        public string CustCntctPgrPinNbr { get; set; }
        public string EventTitleTxt { get; set; }
        public string EventDes { get; set; }
        public string WrkflwStusDes { get; set; }
        public UserViewModel SalsUser { get; set; }
        public UserViewModel ReqorUser { get; set; }
        public EntityBaseViewModel EntityBase { get; set; }
        public ICollection<SplkEventAccessTagViewModel> SplkEventAccsTag { get; set; }
        public List<int> Activators { get; set; }
        public List<int> EventSucssActyIds { get; set; }
        public List<int> EventFailActyIds { get; set; }
        public string PreCfgConfgCode { get; set; }
        public string Profile { get; set; }
        public int FailCode { get; set; }
    }
}