﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class CircuitMs2ViewModel
    {
        public int CktId { get; set; }
        public short VerId { get; set; }
        public DateTime? TrgtDlvryDt { get; set; }
        public DateTime? TrgtDlvryDtRecvDt { get; set; }
        public DateTime? AccsDlvryDt { get; set; }
        public DateTime? CntrcStrtDt { get; set; }
        public string CntrcTermId { get; set; }
    }
}