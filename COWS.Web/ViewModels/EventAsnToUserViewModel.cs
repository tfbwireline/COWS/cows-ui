﻿using System;

namespace COWS.Web.ViewModels
{
    public class EventAsnToUserViewModel
    {
        public int EventId { get; set; }
        public int AsnToUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public byte RoleId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string UserDsplNme { get; set; }
        public string UserEmail { get; set; }
    }
}