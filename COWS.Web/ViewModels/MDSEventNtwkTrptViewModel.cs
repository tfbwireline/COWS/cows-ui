﻿using System;

namespace COWS.Web.ViewModels
{
    public class MDSEventNtwkTrptViewModel
    {
        public int MdsEventNtwkTrptId { get; set; }
        public int EventId { get; set; }
        public string MdsTrnsprtType { get; set; }
        public string LecPrvdrNme { get; set; }
        public string LecCntctInfo { get; set; }
        public string Mach5OrdrNbr { get; set; }
        public string IpNuaAdr { get; set; }
        public string MplsAccsBdwd { get; set; }
        public string ScaNbr { get; set; }
        public bool? TaggdCd { get; set; }
        public string VlanNbr { get; set; }
        public string MultiVrfReq { get; set; }
        public string IpVer { get; set; }
        public string LocCity { get; set; }
        public string LocSttPrvn { get; set; }
        public string LocCtry { get; set; }
        public string AssocH6 { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }

        // Additional
        public string CustNme { get; set; }
        public string DdAprvlNbr { get; set; }
        //public string SalsEngrPhn { get; set; }
        //public string SalsEngrEmail { get; set; }
        public string VASType { get; set; }
        public string VASTypeDes { get; set; }
        public string VASCd { get; set; }
        public string CeSrvcId { get; set; }
        public int NidActyId { get; set; }
        public string NidHostName { get; set; }
        public string NIDSerialNbr { get; set; }
        public string NidIpAdr { get; set; }
        public string Guid { get; set; }
        public string BdwdNme { get; set; }
        public bool IsNidHostNameNative { get { return !string.IsNullOrEmpty(NidHostName); } }
        public bool IsNidSerialNbrNative { get { return !string.IsNullOrEmpty(NIDSerialNbr); } }
    }
}