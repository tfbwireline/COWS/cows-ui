﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class GetVendorOrderDetailsViewModel
    {
        public VendorOrderViewModel VendorOrder { get; set; }
        public IEnumerable<WorkGroupData> Workgroup { get; set; }
        public IEnumerable<ProfileHierarchyViewModel> ProfileHierarchy { get; set; }
        public bool IsBasic { get; set; }
    }
}
