﻿using COWS.Entities.Models;
using System;

namespace COWS.Web.ViewModels
{
    public class SetSlasViewModel
    {
        public int MsSlaId { get; set; }

        //[Required]
        //[DataType(DataType.Text)]
        //[MaxLength(2)]
        public short MsId { get; set; }

        public string PltfrmCd { get; set; }
        public byte OrdrTypeId { get; set; }
        public byte? SlaInDayQty { get; set; }
        public short ToMsId { get; set; }

        public int CreatByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public DateTime CreatDt { get; set; }

        public LkUser CreatByUser { get; set; }
        public LkXnciMs Ms { get; set; }
        public LkOrdrType OrdrType { get; set; }
        public LkPltfrm PltfrmCdNavigation { get; set; }
        public LkXnciMs ToMs { get; set; }
    }
}