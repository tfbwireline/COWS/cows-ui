﻿using System;

namespace COWS.Web.ViewModels
{
    public class OrderVlanViewModel
    {
        public int OrdrVlanId { get; set; }
        public int OrdrId { get; set; }
        public string VlanId { get; set; }
        public string VlanSrcNme { get; set; }
        public double? VlanPctQty { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public bool TrmtgCd { get; set; }
    }
}