﻿using System;

namespace COWS.Web.ViewModels
{
    public class CCDHistoryViewModel
    {
        // From CCDHistView on Query Models
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int NoteID { get; set; }
        public string Note { get; set; }
        public DateTime CCDOld { get; set; }
        public DateTime CCDNew { get; set; }
        public DateTime VndrSent { get; set; }
        public DateTime VndrAck { get; set; }
        public int FTN { get; set; }
        public string ReasonCSV { get; set; }
        public string ReasonPipeSV { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string CreatedByDspNme { get; set; }
        public string CreatedByUserName { get; set; }

        // Used in CCD Change in CCD Tools
        public string OrderIds { get; set; }
        public string CcdReasons { get; set; }
        public string Notes { get; set; }
        public DateTime CcdDate { get; set; }

        // From CCDHist Model
        public int CcdHistId { get; set; }
        public int OrdrId { get; set; }
        public DateTime OldCcdDt { get; set; }
        public DateTime NewCcdDt { get; set; }
        public int NteId { get; set; }
        public DateTime? SentToVndrDt { get; set; }
        public DateTime? AckByVndrDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}