﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class H5FolderViewModel
    {
        public int H5FoldrId { get; set; }
        public int CustId { get; set; }
        public string CustCtyNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int CreatByUserId { get; set; }
        public string ModfdByUserAdId { get; set; }
        public string CreatByUserAdId { get; set; }
        public byte RecStusId { get; set; }
        public string CtryCd { get; set; }
        public byte CsgLvlId { get; set; }
        public string CustNme { get; set; }
        public int H5DocCnt { get; set; }

        public ICollection<H5DocViewModel> H5Docs { get; set; }
        //public ICollection<OrderViewModel> Orders { get; set; }
    }
}