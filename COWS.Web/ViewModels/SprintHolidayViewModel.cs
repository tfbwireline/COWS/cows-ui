﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class SprintHolidayViewModel
    {
        public int SprintHldyId { get; set; }
        public DateTime SprintHldyDt { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(75)]
        public string SprintHldyDes { get; set; }

        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public string ModifiedByAdId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}