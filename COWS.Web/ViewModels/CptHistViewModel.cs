﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CptHistViewModel
    {
        public int CptHistId { get; set; }
        public int CptId { get; set; }
        public byte ActnId { get; set; }
        public string ActionName { get; set; }
        public string CmntTxt { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
