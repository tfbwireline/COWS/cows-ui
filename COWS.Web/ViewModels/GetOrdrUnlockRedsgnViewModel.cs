﻿namespace COWS.Web.ViewModels
{
    public class GetOrdrUnlockRedsgnViewModel
    {
        public string RedsgnNumber { get; set; }   /* for Ordr/Event its integer  for Redsgn/CPT  its string  so convert to int that time*/
        public string RedsgnId { get; set; }  /*Varchar 50*/
        public string LockByUserId { get; set; }
        public string LockedBy { get; set; }   /*Varchar 100*/

        //public string FTN { get; set; }  /*Varchar 50*/
        public string RedsgnType { get; set; } /*Varchar 100*/

        //public string OrdrType { get; set; } /*Varchar 100*/
    }
}