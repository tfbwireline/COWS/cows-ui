﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class CircuitMsViewModel
    {
        public int CktId { get; set; }
        public short VerId { get; set; }
        public DateTime? TrgtDlvryDtRecvDt { get; set; }
        public DateTime? TrgtDlvryDt { get; set; }
        public DateTime? AccsDlvryDt { get; set; }
        public DateTime? AccsAcptcDt { get; set; }
        public DateTime? CntrcStrtDt { get; set; }
        public DateTime? CntrcEndDt { get; set; }
        public DateTime? RnlDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public string CntrcTermId { get; set; }
        public DateTime? VndrCnfrmDscnctDt { get; set; }
    }
}