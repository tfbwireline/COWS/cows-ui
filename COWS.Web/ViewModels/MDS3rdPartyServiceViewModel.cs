﻿using COWS.Entities.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class MDS3rdPartyServiceViewModel
    {
        public short ThrdPartySrvcLvlId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string ThrdPartySrvcLvlDes { get; set; }

        public short SrvcTypeId { get; set; }
        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public string ModifiedByAdId { get; set; }

        public LkMdsSrvcType SrvcType { get; set; }
    }
}