﻿using System;

namespace COWS.Web.ViewModels
{
    public class OrderTypeViewModel
    {
        public byte OrdrTypeId { get; set; }
        public string OrdrTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public string FsaOrdrTypeCd { get; set; }
        public decimal? XnciWfmOrdrWeightageNbr { get; set; }
    }
}