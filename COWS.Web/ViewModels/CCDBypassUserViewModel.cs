﻿namespace COWS.Web.ViewModels
{
    public class CCDBypassUserViewModel
    {
        public int ConfigID { get; set; }
        public string UserADID { get; set; }   /*Varchar 10*/
        public string UserName { get; set; }   /*Varchar 100*/
    }
}