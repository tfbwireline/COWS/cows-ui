﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class RedesignEmailNotificationViewModel
    {
        public int RedsgnEmailNtfctnId { get; set; }
        public int RedsgnId { get; set; }
        public string UserIdntfctnEmail { get; set; }
        public bool RecStusId { get; set; }
        public int CretdUserId { get; set; }
        public DateTime CretdDt { get; set; }
    }
}
