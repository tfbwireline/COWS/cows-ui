﻿using System;

namespace COWS.Web.ViewModels
{
    public class GetQualificationViewModel
    {
        public int QualificationID { get; set; }
        public byte RecStatusID { get; set; }
        public int AssignToUserID { get; set; }
        public string AssignToUser { get; set; }
        public string AssignToUserAdId { get; set; }
        public int CreatedByUserID { get; set; }
        public string CreatedByUser { get; set; }
        public string CreatedByAdId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedByUserID { get; set; }
        public string ModifiedByUser { get; set; }
        public string ModifiedByUserAdId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CustomerIDs { get; set; }
        public string CustomerNames { get; set; }
        public string DeviceIDs { get; set; }
        public string DeviceNames { get; set; }
        public string EnhanceServiceIDs { get; set; }
        public string EnhanceServiceNames { get; set; }
        public string EventTypeIDs { get; set; }
        public string EventTypeNames { get; set; }
        public string IPVersionIDs { get; set; }
        public string IPVersionNames { get; set; }
        public string MplsActyTypeIDs { get; set; }
        public string MplsActyTypeNames { get; set; }
        public string NetworkActivityIDs { get; set; }
        public string NetworkActivityNames { get; set; }
        public string SpecialProjectIDs { get; set; }
        public string SpecialProjectNames { get; set; }
        public string VendorModelIDs { get; set; }
        public string VendorModelNames { get; set; }
    }
}