﻿using System;

namespace COWS.Web.ViewModels
{
    public class OrderSubTypeViewModel
    {
        public byte OrdrSubTypeId { get; set; }
        public string OrdrSubTypeCd { get; set; }
        public string OrdrSubTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
    }
}