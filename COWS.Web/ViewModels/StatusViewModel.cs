﻿using System;

namespace COWS.Web.ViewModels
{
    public class StatusViewModel
    {
        public short StusId { get; set; }
        public string StusDes { get; set; }
        public byte RecStusId { get; set; }
        public string StusTypeId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public string ModifiedByAdid { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}