﻿using System;

namespace COWS.Web.ViewModels
{
    public class FedlineManageUserEventViewModel
    {
        public int EventID { get; set; }
        public int FRBID { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Activator { get; set; }
        public string PreConfigEng { get; set; }
        public string Activity { get; set; }
        public string Status { get; set; }

        public string TimeBlock
        {
            get
            {
                if (StartTime.HasValue && EndTime.HasValue && StartTime.Value.Date != EndTime.Value.Date)
                    return string.Format("{0:ddd hhtt} - {1:ddd hhtt}", StartTime.Value, EndTime.Value);
                return ((StartTime == null) ? "?? - " : string.Format("{0:hh:mm tt} - ", StartTime)) +
                    ((EndTime == null) ? "??" : string.Format("{0:hh:mm tt}", EndTime));
            }
        }
    }
}