﻿using System;

namespace COWS.Web.ViewModels
{
    public class AdvancedSearchRedesignViewModel
    {
        public int Type { get; set; }
        public int? Status { get; set; }
        public string RedesignNo { get; set; }
        public string Customer { get; set; }
        public string H1 { get; set; }
        public int? RedesignCategory { get; set; }
        public string RedesignType { get; set; }
        public int? RedesignStatus { get; set; }
        public string OdieDeviceName { get; set; }
        public string NteAssigned { get; set; }
        public string PmAssigned { get; set; }
        public int? EventId { get; set; }
        public string MssSeAssigned { get; set; }
        public string NeAssigned { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? SlaDueDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int CsgLvlId { get; set; }

        public int RedesignId { get; set; }
        public string StatusDesc { get; set; }
        public string BpmRedesignNbr { get; set; }
    }
}