﻿using System;

namespace COWS.Web.ViewModels
{
    public class MplsEventTypeViewModel
    {
        public byte MplsEventTypeId { get; set; }
        public string MplsEventTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short EventTypeId { get; set; }
    }
}