﻿namespace COWS.Web.ViewModels
{
    public class EventViewModel
    {
        public int DsplVwId { get; set; }
        public int UserId { get; set; }
        public string DsplVwNme { get; set; }
        public int SiteCntntId { get; set; }
        public string DfltVwCd { get; set; }
        public string PblcVwCd { get; set; }
        public string FiltrCol1OprId { get; set; }
        public string FiltrCol2OprId { get; set; }
        public int FiltrCol1Id { get; set; }
        public int FiltrCol2Id { get; set; }
        public string FiltrCol1ValuTxt { get; set; }
        public string FiltrCol2ValuTxt { get; set; }
        public string FiltrOnCd { get; set; }
        public int SortByCol1Id { get; set; }
        public int SortByCol2Id { get; set; }
        public string FiltrLogicOprId { get; set; }
        public string SortByCol1AscOrdrCd { get; set; }
        public string SortByCol2AscOrdrCd { get; set; }
        public string ColIDs { get; set; }
        public string ColPos { get; set; }
    }
}