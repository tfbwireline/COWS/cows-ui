﻿using System;

namespace COWS.Web.ViewModels
{
    public class XnciMsViewModel
    {
        public int MsId { get; set; }

        //[Required]
        //[DataType(DataType.Text)]
        //[MaxLength(2)]
        public string MsDes { get; set; }

        public short MsTaskId { get; set; }

        public DateTime CreatDt { get; set; }
    }
}