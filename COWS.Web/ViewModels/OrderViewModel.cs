﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class OrderViewModel
    {
        public int OrderId { get; set; }
        public int TaskId { get; set; }
        public int TaskStatus { get; set; }
        public int UserId { get; set; }
        public string NoteText { get; set; }
        public bool ChkSMR { get; set; }
        public bool ChkNMR { get; set; }
        public string OrderType { get; set; }
        public string FTN { get; set; }
        public ICollection<PsReqLineItmQueue> PSReqLineItemQueue { get; set; }
        public bool DomesticCD { get; set; }
        public string InternationalCLLICd { get; set; }
        public string DomesticCLLICd { get; set; }
        public bool ChkShipAddr { get; set; }
        public PsReqHdrQueue DeliveryAddress { get; set; }
        public PsReqHdrQueue InstallAddress { get; set; }
        public string ShippingInstructions { get; set; }
        public bool SendAndComplete { get; set; }
        public ICollection<SelectedLineItems> EquipmentReceiptsLineItems { get; set; }
        public string Action { get; set; }
        public string UserADID { get; set; }
        public string VoiceOrder { get; set; }
        public int FsaCpeLineItemId { get; set; }
        public int EventID { get; set; }
        public string Tech { get; set; }
        public string AssignedADID { get; set; }
        public string DeviceID { get; set; }
        public DateTime DispatchTime { get; set; }
        public DateTime? CompleteDate { get; set; } // changed to nullable due to error on api call
        //public DateTime CompleteDate { get; set; }
        public DateTime? InRouteDate { get; set; }
        public DateTime? OnSiteDate { get; set; }
        public ICollection<SelectedLineItems> OrderCompletionLineItems { get; set; }
        public string ThirdPartySite { get; set; }
        public bool ChkEmailPrimaryIPM { get; set; }
        public bool ChkEmailPrimaryIS { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string JeopardyCode { get; set; }
        public ICollection<CPEOrdrUpdInfoView> GOMItems { get; set; }
        public int CPEStatusId { get; set; }
        public string CpeEquipmentType { get; set; }
        public decimal? ReqAmount { get; set; }
        public DateTime? CompleteGOMDate { get; set; }

        // Added by Sarah Sandoval - To be used in CCD Order Search
        public string ProductType { get; set; }
        public int H1 { get; set; }
        public int H5 { get; set; }
        public string DomesticFlag { get; set; }
        public string OrderSubType { get; set; }
        public DateTime Ccd { get; set; }

        public int NoteTypeId { get; set; }
        public string SerialNo { get; set; }
        public string MR { get; set; }
        public string PO { get; set; }
        public string IMS { get; set; }
        public bool IsComplete { get; set; }
        public int WorkGroup { get; set; }
    }

    public class SelectedLineItems
    {
        public int FSACPELineItemID { get; set; }
        public int PartialQuantity { get; set; }
        public DateTime CompleteDate { get; set; }
    }

    public class CPEOrdrUpdInfoView
    {
        public int CPEOrdrID { get; set; }
        public int CPELineItemID { get; set; }
        public string FTN { get; set; }
        public string ItemReceiveDate { get; set; }
        public string OrderShipDate { get; set; }
        public string HrdwareShipDate { get; set; }
        public string USDistReceiveDate { get; set; }
        public string CustDelvryDate { get; set; }
        public string EquipTypeCd { get; set; }
        public string EquipID { get; set; }
        public string EquipDesc { get; set; }
        public string EquipManf { get; set; }
        public string ManfPartCd { get; set; }
        public string PONbr { get; set; }
        public string ShipTrkNbr { get; set; }
        public string CarrierNme { get; set; }
        public int? FsaCpeLineItemId { get; set; }
    }
}

