﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class RedesignWorkflowViewModel
    {
        public int RedsgnWrkflwId { get; set; }
        public short? PreWrkflwStusId { get; set; }
        public string PreWrkflwStus { get; set; }
        public short? DesrdWrkflwStusId { get; set; }
        public string DesrdWrkflwStus { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CretdDt { get; set; }
    }
}
