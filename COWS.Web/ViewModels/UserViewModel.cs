﻿using System;

namespace COWS.Web.ViewModels
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string UserAdid { get; set; }
        public string UserAcf2Id { get; set; }
        public string FullNme { get; set; }
        public string EmailAdr { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CellPhnNbr { get; set; }
        public string PgrNbr { get; set; }
        public string PgrPinNbr { get; set; }
        public string DsplNme { get; set; }
        public decimal? UserOrdrCnt { get; set; }
        public string MgrAdid { get; set; }
        public decimal? CptQty { get; set; }
        public string CtyNme { get; set; }
        public string SttCd { get; set; }
        public DateTime? LstLoggdinDt { get; set; }
        public string PhnNbr { get; set; }
        public char? MenuPref { get; set; }

        public string csgLvlCd { get; set; }
    }
}