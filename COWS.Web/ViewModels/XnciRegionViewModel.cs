﻿using System;

namespace COWS.Web.ViewModels
{
    public class XnciRegionViewModel
    {
        public short RgnId { get; set; }
        public string RgnDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreateByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public short GrpId { get; set; }
        public short? UsrPrfId { get; set; }
    }
}