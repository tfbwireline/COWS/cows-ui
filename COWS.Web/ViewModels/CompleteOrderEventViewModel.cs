﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CompleteOrderEventViewModel
    {
        public string FTN { get; set; }
        public int MsgType { get; set; }
    }
}
