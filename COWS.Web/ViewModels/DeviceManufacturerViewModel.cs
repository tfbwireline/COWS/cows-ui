﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class DeviceManufacturerViewModel
    {
        public short ManfId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string ManfNme { get; set; }

        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}