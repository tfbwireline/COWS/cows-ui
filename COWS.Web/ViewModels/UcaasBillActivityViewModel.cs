﻿using System;

namespace COWS.Web.ViewModels
{
    public class UcaasBillActivityViewModel
    {
        public short UcaaSBillActyId { get; set; }
        public string UcaaSBillActyDes { get; set; }
        public string UcaaSBicType { get; set; }
        public string MrcNrcCd { get; set; }
        public short UcaaSPlanTypeId { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}