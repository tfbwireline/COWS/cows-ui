﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CptUserAsmtViewModel
    {
        public int CptUserAsmtId { get; set; }
        public int CptId { get; set; }
        public int CptUserId { get; set; }
        public string CptUser { get; set; }
        public string CptUserEmail { get; set; }
        public byte RoleId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime ModfdDt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public short? UsrPrfId { get; set; }
    }
}
