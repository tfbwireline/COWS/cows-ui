﻿using System;

namespace COWS.Web.ViewModels
{
    public class EventHistViewModel
    {
        public int EventHistId { get; set; }
        public int EventId { get; set; }
        public byte ActnId { get; set; }
        public string ActnDes { get; set; }
        public string CmntTxt { get; set; }
        public int CreatByUserId { get; set; }
        public string PerformedBy { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public string PreCfgCmpltCd { get; set; }
        public short? FailReasId { get; set; }
        public int? FsaMdsEventId { get; set; }
        public DateTime? EventStrtTmst { get; set; }
        public DateTime? EventEndTmst { get; set; }
    }
}