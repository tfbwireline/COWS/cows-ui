﻿using System;

namespace COWS.Web.ViewModels
{
    public class UcaasEventBillingViewModel
    {
        public int UcaaSEventBillingId { get; set; }
        public int EventId { get; set; }
        public short UcaaSBillActyId { get; set; }
        public string UcaaSBillActyDes { get; set; }
        public string UcaaSBicType { get; set; }
        public string MrcNrcCd { get; set; }
        public short UcaaSPlanTypeId { get; set; }
        public DateTime CreatDt { get; set; }
        public short IncQty { get; set; }
        public short? DcrQty { get; set; }
    }
}