﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CptUserViewModel
    {
        public int UserId { get; set; }
        public string UserAdId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public short UsrPrfId { get; set; }
        public string UsrPrfName { get; set; }
        public decimal Weighted => Convert.ToDecimal(Tasks * Devices * 1.5);
        public string DisplayField => PoolCd ? FullName + " - " + Weighted : FullName;
        public bool PoolCd { get; set; }
        public int Devices { get; set; }
        public int Tasks { get; set; }
    }
}
