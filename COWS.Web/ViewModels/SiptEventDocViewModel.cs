﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class SiptEventDocViewModel
    {
        public int SiptEventDocId { get; set; }
        public int EventId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatByUserAdId { get; set; }
        public DateTime CreatDt { get; set; }
        public string Base64string { get; set; }
    }
}
