﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class NccoOrder2ViewModel
    {
        public int OrdrId { get; set; }
        public byte OrdrTypeId { get; set; }
        public byte ProdTypeId { get; set; }
        public string VndrCd { get; set; }
        public string SiteCityNme { get; set; }
        public DateTime? CwdDt { get; set; }
        public DateTime? CcsDt { get; set; }
        public string CustNme { get; set; }
        public int? H5AcctNbr { get; set; }
        public string SolNme { get; set; }
        public string OeNme { get; set; }
        public string PlNbr { get; set; }
        public string SotsNbr { get; set; }
        public string OrdrByLassieCd { get; set; }
        public string BillCycNbr { get; set; }
        public string EmailAdr { get; set; }
        public string CmntTxt { get; set; }
    }
}