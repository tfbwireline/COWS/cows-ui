﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CptProvisionViewModel
    {
        public int CptPrvsnId { get; set; }
        public int CptId { get; set; }
        public short CptPrvsnTypeId { get; set; }
        public string CptPrvsnType { get; set; }
        public bool? PrvsnStusCd { get; set; }
        public string PrvsnStus => PrvsnStusCd.HasValue && PrvsnStusCd.Value ? "Yes" : "No";
        public string PrvsnNteTxt { get; set; }
        public string SrvrNme { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public short RecStusId { get; set; }
        public string RecStus { get; set; }
    }
}
