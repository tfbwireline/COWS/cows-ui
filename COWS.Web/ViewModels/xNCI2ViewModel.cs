﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class xNCI2ViewModel
    {
        public int OrdrId { get; set; }
        public short? OrdrCatId { get; set; }
        public DateTime? CustCmmtDt { get; set; }
        public short? RgnId { get; set; }
        public NccoOrder2ViewModel NccoOrdr { get; set; }
        public FsaOrder2ViewModel FsaOrdr { get; set; }
        public TrptOrdrViewModel TrptOrdr { get; set; }
        public H5Folder2ViewModel H5Foldr { get; set; }
        //public LkOrdrStus OrdrStus { get; set; }
        public ICollection<OrderStdiHistory2ViewModel> OrdrStdiHist { get; set; }
        public virtual ICollection<OrderNoteViewModel> OrdrNte { get; set; }
        public virtual ICollection<OrderMsViewModel> OrdrMs { get; set; }
        public virtual ICollection<Circuit2ViewModel> Ckt { get; set; }
        public virtual ICollection<OrdrVlanViewModel> OrdrVlan { get; set; }

    }
}

