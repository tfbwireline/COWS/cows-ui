﻿using System;

namespace COWS.Web.ViewModels
{
    public class UcaasPlanTypeViewModel
    {
        public short UcaaSPlanTypeId { get; set; }
        public string UcaaSPlanType { get; set; }
        public string UcaaSPlanTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}