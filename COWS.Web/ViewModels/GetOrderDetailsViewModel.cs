﻿using COWS.Entities.QueryModels;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class GetOrderDetailsViewModel
    {
        public OrderDetailViewModel OrderInfo { get; set; }
        public FsaOrderCustViewModel H1Info { get; set; }
        public FsaOrderCustViewModel H4Info { get; set; }
        public FsaOrderCustViewModel H6Info { get; set; }
        public object FSADisconnectDetails { get; set; }
        public TransportOrderDetails TransportOrderDetails { get; set; }
        public object FTNListDetails { get; set; }
        public object PreQualLineInfo { get; set; }
        public string CurrentTaskName { get; set; }
        public string LatestNonSystemOrderNote { get; set; }
        public bool IsGomCancelTaskCompleted { get; set; }
        public ICollection<CircuitCostViewModel> CircuitCost { get; set; }
        public ICollection<InstallPort> InstallPort { get; set; }
    }
}
