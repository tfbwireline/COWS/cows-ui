﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class VendorOrderMsViewModel
    {
        public int VndrOrdrMsId { get; set; }
        public int VndrOrdrId { get; set; }
        public short VerId { get; set; }
        public int? VndrOrdrEmailId { get; set; }
        public DateTime? SentToVndrDt { get; set; }
        public DateTime? AckByVndrDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
