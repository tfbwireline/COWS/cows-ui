﻿using System;

namespace COWS.Web.ViewModels
{
    public class FedlineUserDataViewModel
    {
        public string EventTitle { get; set; }
        public string EmailCcpdl { get; set; }
        public string ShippingCarrier { get; set; }
        public string TrackingNumber { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public bool PreStageComplete { get; set; }
        public int? PreStagingEngineerUserId { get; set; }
        public string PreStagingEngineerUserDisplay { get; set; }
        public int? MnsActivatorUserId { get; set; }
        public string MnsActivatorUserDisplay { get; set; }
        public string DeviceType { get; set; }
        public string SowLocation { get; set; }
        public string Redesign { get; set; }
        public string TunnelStatus { get; set; }
        public int? FailCodeId { get; set; }
        public string SlaCd { get; set; }
    }
}