﻿using System;

namespace COWS.Web.ViewModels
{
    public class MDSEventSlnkWiredTrptViewModel
    {
        public int SlnkWiredTrptId { get; set; }
        public int EventId { get; set; }
        public string MdsTrnsprtType { get; set; }
        public string PrimBkupCd { get; set; }
        public string VndrPrvdrTrptCktId { get; set; }
        public string VndrPrvdrNme { get; set; }
        public string BdwdChnlNme { get; set; }
        public string PlNbr { get; set; }
        public string IpNuaAdr { get; set; }
        public string OldCktId { get; set; }
        public string MultiLinkCktCd { get; set; }
        public string DedAccsCd { get; set; }
        public string SprintMngdCd { get; set; }
        public string OdieDevNme { get; set; }
        public DateTime CreatDt { get; set; }
        public string FmsNbr { get; set; }
        public string DlciVpiVci { get; set; }
        public string VlanNbr { get; set; }
        public string SpaNua { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string ReadyBeginCd { get; set; }
        public string OptInCktCd { get; set; }
        public string OptInHCd { get; set; }
        public string H6 { get; set; }
    }
}