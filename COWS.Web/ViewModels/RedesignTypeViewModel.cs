﻿using System;

namespace COWS.Web.ViewModels
{
    public class RedesignTypeViewModel
    {
        public byte RedsgnTypeId { get; set; }
        public string RedsgnTypeNme { get; set; }
        public string RedsgnTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
        public byte? RedsgnCatId { get; set; }
        public bool IsBlblCd { get; set; }
        public bool? OdieReqCd { get; set; }
    }
}