﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CptPrimWanViewModel
    {
        public int CptPrimWanId { get; set; }
        public int CptId { get; set; }
        public short CptPlnSrvcTierId { get; set; }
        public short CptPrimScndyTprtId { get; set; }
        public int? Qty { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
