﻿using System;

namespace COWS.Web.ViewModels
{
    public class VendorTemplateViewModel
    {
        public int VndrTmpltId { get; set; }
        public int VndrFoldrId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatByUserAdId { get; set; }
        public byte RecStusId { get; set; }
        public string Base64string { get; set; }
    }
}
