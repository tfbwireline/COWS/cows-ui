﻿namespace COWS.Web.ViewModels
{
    public class GetCancelOrdrViewModel
    {
        public string FTN { get; set; }  /*Varchar 50*/
        public string h5_h6 { get; set; }  /* original int*/
        public string OrdrType { get; set; } /*Varchar 100*/
        public string ProductType { get; set; } /*Varchar 100*/
        public string custName { get; set; }   /* varchar 1000*/
        public string ccd { get; set; }
        public string notes { get; set; }   /*Varchar 500*/
    }
}