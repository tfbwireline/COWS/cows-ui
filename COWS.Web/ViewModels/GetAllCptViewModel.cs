﻿using System;

namespace COWS.Web.ViewModels
{
    public class GetAllCptViewModel
    {
        //public int CptID { get; set; }
        //public string CptCustomerNumber { get; set; }
        //public string CustomerShortName { get; set; }
        //public bool ReturnedToSDE { get; set; }
        //public bool ReviewedByPM { get; set; }
        //public string CompanyName { get; set; }
        //public string H1 { get; set; }
        //public Int16 CntDevices { get; set; }
        //public DateTime CreatedDateTime { get; set; }
        //public string CreatedByUserName { get; set; }

        public int CptId { get; set; }
        public string CptCustNbr { get; set; }
        public string CustShortName { get; set; }
        public bool ReturnedToSDE { get; set; }
        public bool ReviewedByPM { get; set; }
        public string CompanyName { get; set; }
        public string H1 { get; set; }
        public Int16 CntDevices { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public string Nte { get; set; }
        public string Mss { get; set; }
        public string Pm { get; set; }
        public DateTime? PmReviewedDate { get; set; }
        public string LockedByUser { get; set; }
    }
}