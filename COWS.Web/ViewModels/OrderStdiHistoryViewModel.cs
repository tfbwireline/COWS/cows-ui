﻿using System;

namespace COWS.Web.ViewModels
{
    public class OrderStdiHistoryViewModel
    {
        public int OrdrStdiId { get; set; }
        public int OrdrId { get; set; }
        public byte StdiReasId { get; set; }
        public string CmntTxt { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime StdiDt { get; set; }

        // Related fields
        public string StdiReasDes { get; set; }
        public string CreatByUserFullNme { get; set; }
        public DateTime? Ccd { get; set; }
    }
}