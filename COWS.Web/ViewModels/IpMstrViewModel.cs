﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class IpMstrViewModel
    {
        public short Id { get; set; }
        public string StrtIpAdr { get; set; }
        public string EndIpAdr { get; set; }
        public string SystmCd { get; set; }
        public short RecStusId { get; set; }
        public string RecStusDes { get; set; }
    }
}
