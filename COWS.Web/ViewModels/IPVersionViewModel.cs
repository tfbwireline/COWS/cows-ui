﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class IPVersionViewModel
    {
        public byte IpVerId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string IpVerNme { get; set; }

        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}