﻿namespace COWS.Web.ViewModels
{
    public class SDEOpportunityDocViewModel
    {
        public int SdeDocID { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public int FileSizeQuantity { get; set; }
        public string Base64string { get; set; }
    }
}