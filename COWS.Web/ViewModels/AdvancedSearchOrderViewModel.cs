﻿using System;

namespace COWS.Web.ViewModels
{
    public class AdvancedSearchOrderViewModel
    {
        public int Type { get; set; }
        public int? Status { get; set; }
        public string M5_no { get; set; }
        public bool? IsRelatedFtn { get; set; }
        public string Name { get; set; }
        public DateTime? Ccd { get; set; }
        public string PrivateLine { get; set; }
        public string H1 { get; set; }
        public string VendorName { get; set; }
        public string PrsQuote { get; set; }
        public string VendorCircuit { get; set; }
        public string ProductType { get; set; }
        public string Soi { get; set; }
        public string H5_h6 { get; set; }
        public string OrderSubType { get; set; }
        public string OrderType { get; set; }
        public string WorkGroup { get; set; }
        public string Nua { get; set; }
        public string PlatformType { get; set; }
        public string Region { get; set; }
        public string ParentM5_no { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string AtlasWorkOrderNumber { get; set; }
        public string PeopleSoftRequisitionNumber { get; set; }
        public string PreQualId { get; set; }
        public string DeviceId { get; set; }
        public string H5Names { get; set; }
        public string CcdOps { get; set; }
        public string CcdOption { get; set; }
        public int ReqByUsrId { get; set; }
        public string RptSchedule { get; set; }
        public string AdhocEmailChk { get; set; }
        public string PreQualdId { get; set; }
        public int CsgLvlId { get; set; }
        public string SiteId { get; set; }
    }
}