﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class RoleViewModel
    {
        public byte RoleId { get; set; }
        public string RoleNme { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public string RoleTypeId { get; set; }
        public string RoleCd { get; set; }
        public string DisplayText { get; set; }
    }
}
