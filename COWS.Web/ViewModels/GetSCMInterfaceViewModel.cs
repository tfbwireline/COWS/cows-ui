﻿using System;

namespace COWS.Web.ViewModels
{
    public class GetSCMInterfaceViewModel
    {
        public string FTN { get; set; }
        public string DeviceId { get; set; }
        public string PlsftRqstnNbr { get; set; }
        public DateTime? RqstnDt { get; set; }
        public string PrchOrdrNbr { get; set; }
        public string OrderStatus { get; set; }
    }
}