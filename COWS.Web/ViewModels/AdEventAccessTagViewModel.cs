﻿using System;

namespace COWS.Web.ViewModels
{
    public class AdEventAccessTagViewModel
    {
        public int EventId { get; set; }
        public string CktId { get; set; }
        public DateTime CreatDt { get; set; }
        public string OldCktId { get; set; }

        // SHOULD BE AdEvent VIEW MODEL
        //public AdEvent Event { get; set; }
    }
}