﻿using System;

namespace COWS.Web.ViewModels
{
    public class QualificationViewModel
    {
        public int QlfctnId { get; set; }
        public int AsnToUserId { get; set; }
        public string AsnToUser { get; set; }
        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}