﻿using System;

namespace COWS.Web.ViewModels
{
    public class MssProductTypeViewModel
    {
        public short SdePrdctTypeId { get; set; }
        public string SdePrdctTypeNme { get; set; }
        public string SdePrdctTypeDesc { get; set; }
        public short OrdrBySeqNbr { get; set; }
        public bool OutsrcdCd { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}