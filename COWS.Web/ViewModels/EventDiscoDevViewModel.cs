﻿using System;

namespace COWS.Web.ViewModels
{
    public class EventDiscoDevViewModel
    {
        public int EventDiscoDevId { get; set; }
        public int EventId { get; set; }
        public string OdieDevNme { get; set; }
        public string H5H6 { get; set; }
        public string SiteId { get; set; }
        public string DeviceId { get; set; }
        public short? DevModelId { get; set; }
        public short? ManfId { get; set; }
        public string SerialNbr { get; set; }
        public string ThrdPartyCtrct { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string ReadyBeginCd { get; set; }
        public string OptInCktCd { get; set; }
        public string OptInHCd { get; set; }
    }
}