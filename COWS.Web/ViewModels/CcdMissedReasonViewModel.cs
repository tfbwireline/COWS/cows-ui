﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CcdMissedReasonViewModel
    {
        public short CcdMissdReasId { get; set; }
        public string CcdMissdReasDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
