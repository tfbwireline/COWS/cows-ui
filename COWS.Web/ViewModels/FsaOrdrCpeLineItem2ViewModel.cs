﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class FsaOrdrCpeLineItem2ViewModel
    {
        public int FsaCpeLineItemId { get; set; }
        public int OrdrId { get; set; }
        public string DeviceId { get; set; }
    }
}
