﻿using System;

namespace COWS.Web.ViewModels
{
    public class SplkEventAccessTagViewModel
    {
        //public int SplkEventAccsId { get; set; }
        public int EventId { get; set; }

        public string OldPlNbr { get; set; }
        public string OldPortSpeedDes { get; set; }
        public string NewPlNbr { get; set; }
        public string NewPortSpeedDes { get; set; }
        public DateTime CreatDt { get; set; }
        public string OldVpiVciDlciNme { get; set; }
        public string NewVpiVciDlciNme { get; set; }
    }
}