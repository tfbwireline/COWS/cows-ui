﻿using System;

namespace COWS.Web.ViewModels
{
    public class StateViewModel
    {
        public string UsStateName { get; set; }
        public string UsStateId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }
        public string TmeZoneId { get; set; }
    }
}