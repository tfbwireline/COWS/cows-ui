﻿using System;

namespace COWS.Web.ViewModels
{
    public class OdieRspnViewModel
    {
        public int RspnId { get; set; }
        public int ReqId { get; set; }
        public DateTime RspnDt { get; set; }
        public string CustTeamPdl { get; set; }
        public string MnspmId { get; set; }
        public string RspnErrorTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public bool? AckCd { get; set; }
        public string SowsFoldrPathNme { get; set; }
        public string DocUrlAdr { get; set; }
        public bool? ActCd { get; set; }
        public string OdieCustId { get; set; }
        public string NteId { get; set; }
        public string SdeAssigned { get; set; }
        public string CustNme { get; set; }
    }
}