﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class CircuitViewModel
    {
        public int CktId { get; set; }
        public int? OrdrId { get; set; }
        public string VndrCktId { get; set; }
        public string LecId { get; set; }
        public DateTime? VndrCntrcTermEndDt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public int? VndrOrdrId { get; set; }
        public string PlSeqNbr { get; set; }
        public bool TrmtgCd { get; set; }
        public virtual ICollection<CircuitMsViewModel> CktMs { get; set; }
        public virtual ICollection<CircuitCostViewModel> CktCost { get; set; }
    }
}