﻿namespace COWS.Web.ViewModels
{
    public class FedlineConfigViewModel
    {
        public int FedlineConfigId { get; set; }
        public int FedlineEventId { get; set; }
        public string PortName { get; set; }
        public string IpAddress { get; set; }
        public string SubnetMask { get; set; }
        public string DefaultGateway { get; set; }
        public string Speed { get; set; }
        public string Duplex { get; set; }
        public string MacAddress { get; set; }
    }
}