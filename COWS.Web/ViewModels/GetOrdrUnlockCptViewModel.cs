﻿namespace COWS.Web.ViewModels
{
    public class GetOrdrUnlockCptViewModel
    {
        public string cptCustNumber { get; set; }   /* for Ordr/Event its integer  for Redsgn/CPT  its string  so convert to int that time*/
        public string LockByUserId { get; set; }
        public string LockedBy { get; set; }   /*Varchar 100*/
        public string cptId { get; set; }  /*Varchar 50*/
        public string CustName { get; set; } /*Varchar 100*/
        //public string OrdrType { get; set; } /*Varchar 100*/
    }
}