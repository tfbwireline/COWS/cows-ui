﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class VendorOrderFormViewModel
    {

        public int VndrOrdrFormId { get; set; }
        public int VndrOrdrId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public bool TmpltCd { get; set; }
        public int? TmpltId { get; set; }

        //Additional field
        public string CreatByUserFullName { get; set; }
        public string Base64string { get; set; }
    }
}