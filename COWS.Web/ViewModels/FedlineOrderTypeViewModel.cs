﻿using System;

namespace COWS.Web.ViewModels
{
    public class FedlineOrderTypeViewModel
    {
        public string OrdrTypeCd { get; set; }
        public string OrdrTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public DateTime CreatDt { get; set; }
        public string PrntOrdrTypeDes { get; set; }
    }
}