﻿using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class UcaasEventViewModel
    {
        public int EventId { get; set; }
        public byte EventStusId { get; set; }
        public string EventTitleTxt { get; set; }
        public string H1 { get; set; }
        public string CustNme { get; set; }
        public string CharsId { get; set; }
        public string CustAcctTeamPdlNme { get; set; }
        public string CustSowLocTxt { get; set; }
        public short UcaaSProdTypeId { get; set; }
        public short UcaaSPlanTypeId { get; set; }
        public short UcaaSActyTypeId { get; set; }
        public string UcaaSDesgnDoc { get; set; }
        public string ShrtDes { get; set; }

        public string H6 { get; set; }
        public DateTime? Ccd { get; set; }
        public string SiteId { get; set; }
        public string SiteAdr { get; set; }

        public string UsIntlCd { get; set; }
        public string InstlSitePocNme { get; set; }
        public string InstlSitePocIntlPhnCd { get; set; }
        public string InstlSitePocPhnNbr { get; set; }
        public string InstlSitePocIntlCellPhnCd { get; set; }
        public string InstlSitePocCellPhnNbr { get; set; }
        public string SrvcAssrnPocNme { get; set; }
        public string SrvcAssrnPocIntlPhnCd { get; set; }
        public string SrvcAssrnPocPhnNbr { get; set; }
        public string SrvcAssrnPocIntlCellPhnCd { get; set; }
        public string SrvcAssrnPocCellPhnNbr { get; set; }

        public string StreetAdr { get; set; }
        public string SttPrvnNme { get; set; }
        public string FlrBldgNme { get; set; }
        public string CtryRgnNme { get; set; }
        public string CtyNme { get; set; }
        public string ZipCd { get; set; }

        // When Sprint CPE and Velociti is chosen on CPE Delivery Option
        public byte? SprintCpeNcrId { get; set; }

        public string CpeDspchEmailAdr { get; set; }
        public string CpeDspchCmntTxt { get; set; }

        // When Disconnect is chosen on UCaaS Activity
        public string DiscMgmtCd { get; set; }

        public bool? FullCustDiscCd { get; set; }
        public string FullCustDiscReasTxt { get; set; }

        public bool EsclCd { get; set; }
        public byte? EsclReasId { get; set; }
        public DateTime? PrimReqDt { get; set; }
        public DateTime? ScndyReqDt { get; set; }
        public string BusJustnTxt { get; set; }
        public byte? CnfrcBrdgId { get; set; }
        public string CnfrcBrdgNbr { get; set; }
        public string CnfrcPinNbr { get; set; }
        public string OnlineMeetingAdr { get; set; }
        public string PubEmailCcTxt { get; set; }
        public string CmpltdEmailCcTxt { get; set; }

        public int ReqorUserId { get; set; }
        public UserViewModel ReqorUser { get; set; }
        public string ReqorUserCellPhnNbr { get; set; }
        public string CmntTxt { get; set; }

        public DateTime StrtTmst { get; set; }
        public DateTime EndTmst { get; set; }
        public short ExtraDrtnTmeAmt { get; set; }
        public short EventDrtnInMinQty { get; set; }
        public string MnsPmId { get; set; }
        public short? FailReasId { get; set; }
        public byte WrkflwStusId { get; set; }
        public List<int> Activators { get; set; }
        public byte? OldEventStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public byte RecStusId { get; set; }

        public int ReviewerUserId { get; set; }
        public string ReviewerComments { get; set; }
        public int ActivatorUserId { get; set; }
        public string ActivatorComments { get; set; }
        public bool SuppressEmail { get; set; }

        public IEnumerable<UcaasEventBillingViewModel> UcaasEventBilling { get; set; }
        public IEnumerable<UcaasEventOdieDevIceViewModel> UcaasEventOdieDevice { get; set; }
        public IEnumerable<EventCpeDevViewModel> EventCpeDev { get; set; }
        public IEnumerable<EventDeviceCompletionViewModel> EventDeviceCompletion { get; set; }
        public IEnumerable<EventDevSrvcMgmtViewModel> EventDevServiceMgmt { get; set; }
        public IEnumerable<EventDiscoDevViewModel> EventDiscoDev { get; set; }
    }
}