﻿using System;

namespace COWS.Web.ViewModels
{
    public class MDSActivityTypeViewModel
    {
        public byte MdsActyTypeId { get; set; }
        public string MdsActyTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}