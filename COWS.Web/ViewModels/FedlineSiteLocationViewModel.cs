﻿namespace COWS.Web.ViewModels
{
    public class FedlineSiteLocationViewModel
    {
        public string SiteAddress1 { get; set; }
        public string SiteAddress2 { get; set; }
        public string City { get; set; }
        public string StateProv { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string PrimaryPocName { get; set; }
        public string PrimaryPocPhone { get; set; }
        public string PrimaryPocPhoneExtension { get; set; }
        public string PrimaryPocEmail { get; set; }
        public string SecondaryPocName { get; set; }
        public string SecondaryPocPhone { get; set; }
        public string SecondaryPocPhoneExtension { get; set; }
        public string SecondaryPocEmail { get; set; }
    }
}