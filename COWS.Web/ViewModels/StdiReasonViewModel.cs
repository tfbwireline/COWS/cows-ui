﻿using System;

namespace COWS.Web.ViewModels
{
    public class StdiReasonViewModel
    {
        public byte StdiReasId { get; set; }
        public string StdiReasDes { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
        public int? ModfdByUserId { get; set; }
        public int? CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
    }
}
