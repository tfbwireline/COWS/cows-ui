﻿using COWS.Entities.Models;
using System;
using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class FsaOrder2ViewModel
    {
        public int OrdrId { get; set; }
        public string Ftn { get; set; }
        public DateTime? CustCmmtDt { get; set; }
        public DateTime? CustWantDt { get; set; }
        public string TportPrequalSiteId { get; set; }
        public string TportPrequalLineItemId { get; set; }
        public DateTime? TportPreqLineItemIdXpirnDt { get; set; }
        public string TportVndrRawMrcQotCurAmt { get; set; }
        public string TportVndrRawNrcQotCurAmt { get; set; }
        public string TportCnvrsnRtQty { get; set; }
        public string TportMarkupPctQty { get; set; }
        public string TportTaxPctQty { get; set; }
        public string TportBearerChgAmt { get; set; }
        public string TportAddlMrcAmt { get; set; }
        public string TportCalcRtMrcUsdAmt { get; set; }
        public string TportCalcRtNrcUsdAmt { get; set; }
        public string TportCalcRtMrcInCurAmt { get; set; }
        public string TportCalcRtNrcInCurAmt { get; set; }
        public string TportCurNme { get; set; }
        public string OrdrTypeCd { get; set; }

        // Additional fields
        public string ParentOrderTypeCd { get; set; }

        public virtual ICollection<FsaOrdrCpeLineItem2ViewModel> FsaOrdrCpeLineItem { get; set; }
    }
}