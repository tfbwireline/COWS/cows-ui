﻿using System;

namespace COWS.Web.ViewModels
{
    public class CptProvisionTypeViewModel
    {
        public short CptPrvsnTypeId { get; set; }
        public string CptPrvsnType { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}