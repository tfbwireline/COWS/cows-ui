﻿using System;

namespace COWS.Web.ViewModels
{
    public class SiptRltdOrdrViewModel
    {
        public int SiptReltdOrdrId { get; set; }
        public int EventId { get; set; }
        public string M5OrdrNbr { get; set; }
        public DateTime CreatDt { get; set; }
    }
}