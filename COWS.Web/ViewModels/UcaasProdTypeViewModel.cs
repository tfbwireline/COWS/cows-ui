﻿using System;

namespace COWS.Web.ViewModels
{
    public class UcaasProdTypeViewModel
    {
        public short UcaaSProdTypeId { get; set; }
        public string UcaaSProdType { get; set; }
        public string UcaaSProdTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}