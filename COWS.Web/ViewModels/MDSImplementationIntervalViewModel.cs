﻿using System;

namespace COWS.Web.ViewModels
{
    public class MDSImplementationIntervalViewModel
    {
        public short EventTypeId { get; set; }
        public short NonCpeNtvlInDayQty { get; set; }
        public short CpeNtvlInDayQty { get; set; }
        public string EventTypeName { get; set; }
        public byte? RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public DateTime CreatDt { get; set; }

        //public DateTime CreatDt { get; set; } = DateTime.Now;
        public int? ModfdByUserId { get; set; }

        public string ModifiedByAdid { get; set; }
        public DateTime? ModfdDt { get; set; }
    }
}