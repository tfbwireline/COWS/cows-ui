﻿using System;

namespace COWS.Web.ViewModels
{
    public class SiptTollTypeViewModel
    {
        public byte SiptTollTypeId { get; set; }
        public string SiptTollTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}