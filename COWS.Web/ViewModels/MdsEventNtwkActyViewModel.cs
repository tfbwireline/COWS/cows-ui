﻿using System;

namespace COWS.Web.ViewModels
{
    public class MdsEventNtwkActyViewModel
    {
        public long Id { get; set; }
        public int EventId { get; set; }
        public byte NtwkActyTypeId { get; set; }
        public DateTime CreatDt { get; set; }
        //public string NtwkActyType { get; set; }
    }
}