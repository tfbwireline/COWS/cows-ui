﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class CustContractLengthViewModel
    {
        public string CustCntrcLgthId { get; set; }
        public string CustCntrcLgthDes { get; set; }
        //public DateTime CreatDt { get; set; }
        //public DateTime? ModfdDt { get; set; }
        //public int? ModfdByUserId { get; set; }
        //public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
    }
}