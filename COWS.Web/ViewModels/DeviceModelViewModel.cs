﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class DeviceModelViewModel
    {
        public short DevModelId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string DevModelNme { get; set; }

        public short MinDrtnTmeReqrAmt { get; set; }
        public byte RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdId { get; set; }
        public string ModifiedByAdId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
        public short ManfId { get; set; }
        public string ManfModelName { get; set; }
        public bool IsAlreadyInUse { get; set; }
    }
}