﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class AdvancedSearchEventViewModel
    {
        public int Type { get; set; }
        public int? Status { get; set; }
        public string M5_no { get; set; }
        public string AssignedName { get; set; }
        public string RequestorName { get; set; }
        public string EventId { get; set; }
        public string EventType { get; set; }
        public string EventStatus { get; set; }
        public string FmsCircuit { get; set; }
        public string NUA { get; set; }
        public string H5_h6 { get; set; }
        public string FtActivityType { get; set; }
        public string Customer { get; set; }
        public string DeviceName { get; set; }
        public string DeviceSerialNo { get; set; }
        public string FrbRequestId { get; set; }
        public string FrbOrgId { get; set; }
        public string FrbConfigurationType { get; set; }
        public DateTime? ApptStartDate { get; set; }
        public DateTime? ApptEndDate { get; set; }
        public string FrbActivityType { get; set; }
        public string RedesignNo { get; set; }
        public string MdsActivityType { get; set; }
        public int[] MdsMacType { get; set; }
        public string H1 { get; set; }
        public string SowsId { get; set; }

        public string AdType { get; set; }
        public string ApptStartOp { get; set; }
        public string ApptStartOption { get; set; }
        public string ApptEndOp { get; set; }
        public string ApptEndOption { get; set; }
        public int ReqByUsrId { get; set; }
        public string RptSchedule { get; set; }
        public string AdhocEmailChk { get; set; }

        public int CsgLvlId { get; set; }
        public string MdsNtwkActyType { get; set; }



        //public string ActionType { get; set; }
        //public string FrbId { get; set; }
        //public string Serial { get; set; }
        //public string DesignType { get; set; }
        //public string OrgId { get; set; }
        //public string ActType { get; set; }
        // public string Status { get; set; }

        //public int Req_By_Usr_Id { get; set; }

    }
}
