﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CpeClliRequestDetailsViewModel
    {
        public int CpeClliId { get; set; }

        [Required]
        public string ClliId { get; set; }

        public string SiteNme { get; set; }


        [Required]
        public string AddrTxt { get; set; }

        [Required]
        public string SttId { get; set; }


        [Required]
        public string CityNme { get; set; }

        [Required]
        public string ZipCd { get; set; }

        [Required]
        public string CountyNme { get; set; }
        public string CmntyNme { get; set; }
        public string Flr { get; set; }
        public string Room { get; set; }
        public string LttdeCoord { get; set; }
        public string LngdeCoord { get; set; }
        public string SiteOwnr { get; set; }
        public bool SprntOwndCd { get; set; }
        public string SitePurpose { get; set; }
        public string SitePhn { get; set; }
        public string CmntTxt { get; set; }
        public string EmailCc { get; set; }
        public DateTime? PsoftUpdDt { get; set; }
        public DateTime? SstatUpdDt { get; set; }
        public bool PsoftSendCd { get; set; }
        public byte RecStusId { get; set; }
        public short CpeClliStusId { get; set; }
    }
}
