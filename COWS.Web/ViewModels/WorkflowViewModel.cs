﻿using System;

namespace COWS.Web.ViewModels
{
    public class WorkflowViewModel
    {
        public byte WrkflwStusId { get; set; }
        public string WrkflwStusDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        //public LkUser CreatByUser { get; set; }
        //public LkUser ModfdByUser { get; set; }
        //public LkRecStus RecStus { get; set; }
        //public ICollection<AdEvent> AdEvent { get; set; }
        //public ICollection<MplsEvent> MplsEvent { get; set; }
        //public ICollection<SiptEvent> SiptEvent { get; set; }
    }
}