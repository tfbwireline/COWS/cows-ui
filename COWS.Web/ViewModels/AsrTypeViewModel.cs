﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class AsrTypeViewModel
    {
        public int AsrTypeId { get; set; }
        public string AsrTypeNme { get; set; }
        public string AsrTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public int? CreatByUserId { get; set; }
    }
}