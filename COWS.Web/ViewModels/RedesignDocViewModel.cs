﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace COWS.Web.ViewModels
{
    public class RedesignDocViewModel
    {
        public int RedsgnDocId { get; set; }
        public int RedsgnId { get; set; }
        public string FileNme { get; set; }
        public byte[] FileCntnt { get; set; }
        public int FileSizeQty { get; set; }
        public DateTime? CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public string CreatByUserName { get; set; }
        public int? ModfdByUserId { get; set; }
        public byte RecStusId { get; set; }
        public short? RedsgnDevStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string Base64string { get; set; }

        private IFormFile _UploadFile;
        [Display(Name = "file")]
        [DataType(DataType.Upload)]
        public IFormFile UploadFile
        {
            get => _UploadFile;
            set
            {
                _UploadFile = value;
                if (_UploadFile != null)
                {
                    FileNme = _UploadFile.FileName; // Set FileName
                    using (var ms = new MemoryStream())
                    {
                        _UploadFile.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        FileCntnt = fileBytes; // Set byte array for file
                    }
                }
            }
        }
    }
}
