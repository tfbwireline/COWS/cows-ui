﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class H5Folder2ViewModel
    {
        public int H5FoldrId { get; set; }
        public int CustId { get; set; }
        public string CustNme { get; set; }
        public string CustCtyNme { get; set; }
        public string CtryCd { get; set; }

        public ICollection<H5Doc2ViewModel> H5Docs { get; set; }
    }
}