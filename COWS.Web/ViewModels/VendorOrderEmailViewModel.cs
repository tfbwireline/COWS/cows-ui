﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class VendorOrderEmailViewModel
    {
        public int VndrOrdrEmailId { get; set; }
        public int VndrOrdrId { get; set; }
        public byte VerNbr { get; set; }
        public string FromEmailAdr { get; set; }
        public string ToEmailAdr { get; set; }
        public string CcEmailAdr { get; set; }
        public string EmailBody { get; set; }
        public short EmailStusId { get; set; }
        public DateTime? SentDt { get; set; }
        public DateTime CreatDt { get; set; }
        public int CreatByUserId { get; set; }
        public byte RecStusId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public string EmailSubjTxt { get; set; }
        public byte? VndrEmailTypeId { get; set; }

        public virtual VendorOrderEmailTypeViewModel VndrEmailType { get; set; }
        public virtual ICollection<VendorOrderEmailAttachmentViewModel> VndrOrdrEmailAtchmt { get; set; }
        public virtual ICollection<VendorOrderMsViewModel> VndrOrdrMs { get; set; }

        //Additional field
        public string EmailTypeDesc { get; set; }
        public List<string> EmailAttachmentName { get; set; }
        public List<int> EmailAttachmentSize { get; set; }
        public string CreatByUserFullName { get; set; }
        public string ModfdByUserFullName { get; set; }
        public string EmailStatusDesc { get; set; }
        public DateTime? SentToVendorDate { get; set; }
        public DateTime? AckByVendorDate { get; set; }
    }
}
