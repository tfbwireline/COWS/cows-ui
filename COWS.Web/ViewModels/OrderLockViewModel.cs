﻿using System;

namespace COWS.Web.ViewModels
{
    public class OrderLockViewModel
    {
        public int OrdrId { get; set; }
        public DateTime StrtRecLockTmst { get; set; }
        public int LockByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public string LockByFullName { get; set; }
    }
}