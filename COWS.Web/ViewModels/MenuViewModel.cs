﻿using System.Collections.Generic;

namespace COWS.Web.ViewModels
{
    public class MenuViewModel
    {
        public short MenuId { get; set; }
        public string MenuNme { get; set; }
        public string MenuDes { get; set; }
        public byte DpthLvl { get; set; }
        public short PrntMenuId { get; set; }
        public byte? DsplOrdr { get; set; }
        public string SrcPath { get; set; }
        public byte RecStusId { get; set; }

        //public DateTime? ModfdDt { get; set; }
        //public DateTime CreatDt { get; set; }
        //public ICollection<LkUsrPrfMenu> LkUsrPrfMenu { get; set; }
        public IEnumerable<MenuViewModel> children { get; set; }
    }
}