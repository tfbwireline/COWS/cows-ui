﻿namespace COWS.Web.ViewModels
{
    public class RetrieveCustomerByH1ViewModel
    {
        public string CUST_CHARS_ID { get; set; }
        public string SCTY_GRP_CD { get; set; }
        public string RLAT_KEY_ID { get; set; }
    }
}