﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class CircuitCostViewModel
    {
        public int CktId { get; set; }
        public short VerId { get; set; }
        public string QotNbr { get; set; }
        public decimal? TaxRtPctQty { get; set; }
        public string VndrCurId { get; set; }
        public decimal? VndrMrcAmt { get; set; }
        public decimal? VndrMrcInUsdAmt { get; set; }
        public decimal? VndrNrcAmt { get; set; }
        public decimal? VndrNrcInUsdAmt { get; set; }
        public string AsrCurId { get; set; }
        public decimal? AsrMrcAty { get; set; }
        public decimal? AsrMrcInUsdAmt { get; set; }
        public decimal? AsrNrcAmt { get; set; }
        public decimal? AsrNrcInUsdAmt { get; set; }
        public string AccsCustCurId { get; set; }
        public decimal? AccsCustMrcAmt { get; set; }
        public decimal? AccsCustMrcInUsdAmt { get; set; }
        public decimal? AccsCustNrcAmt { get; set; }
        public decimal? AccsCustNrcInUsdAmt { get; set; }
        public int CreatByUserId { get; set; }
        public DateTime CreatDt { get; set; }
        public bool BndlCd { get; set; }
        public string VndrOrdrNbr { get; set; }
        public string VndrCktId { get; set; }
        public string VndrCurNme { get; set; }
        public string AsrCurNme { get; set; }
        public string AccsCustCurNme { get; set; }

        //public virtual LkCur AccsCustCur { get; set; }
        //public virtual LkCur AsrCur { get; set; }
        //public virtual Ckt Ckt { get; set; }
        //public virtual LkUser CreatByUser { get; set; }
        //public virtual LkCur VndrCur { get; set; }
    }
}