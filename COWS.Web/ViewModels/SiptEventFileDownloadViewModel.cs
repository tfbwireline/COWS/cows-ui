﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class SiptEventFileDownloadViewModel
    {
        public string FileName { get; set; }

        public string Base64string { get; set; }
    }
}
