﻿using COWS.Entities.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class EnhncSrvcViewModel
    {
        public byte EnhncSrvcId { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string EnhncSrvcNme { get; set; }
        public short EventTypeId { get; set; }
        public byte? RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public string ModifiedByAdid { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }

        public LkEventType EventType { get; set; }
    }
}
