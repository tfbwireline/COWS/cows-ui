﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace COWS.Web.ViewModels
{
    public class ProfileHierarchyViewModel
    {

        public int Id { get; set; }
        public short ChldPrfId { get; set; }
        public short PrntPrfId { get; set; }
        public byte RecStusId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}