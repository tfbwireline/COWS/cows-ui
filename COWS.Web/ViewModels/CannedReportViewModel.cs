﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class CannedReportViewModel
    {
        public int reportId { get; set; }
        public string reportType { get; set; }

        public DateTime startDate { get; set; }

        public DateTime endDate { get; set; }


        public string fileName { get; set; }
        public string filePath { get; set; }
    }
}
