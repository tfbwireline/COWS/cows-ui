﻿using System;

namespace COWS.Web.ViewModels
{
    public class OrderStdiHistory2ViewModel
    {
        public int OrdrStdiId { get; set; }
        public int OrdrId { get; set; }
        public byte StdiReasId { get; set; }
        public DateTime StdiDt { get; set; }
        public string StdiReasDes { get; set; }
        public DateTime? Ccd { get; set; }
        public string CmntTxt { get; set; }
        public DateTime CreatDt { get; set; }
        public string CreatByUserFullNme { get; set; }
    }
}