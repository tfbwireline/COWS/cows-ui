﻿namespace COWS.Web.ViewModels
{
    public class FTAvailEventViewModel
    {
        public string EventID { get; set; }

        public string EscYN { get; set; }

        public string CustName { get; set; }

        public string AssignedUsers { get; set; }

        public string EventStatus { get; set; }

        public string IsMDSNew { get; set; }
    }
}