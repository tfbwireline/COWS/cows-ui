﻿using System;

namespace COWS.Web.ViewModels
{
    public class PlatformViewModel
    {
        public string PltfrmCd { get; set; }
        public string PltfrmNme { get; set; }
        public DateTime CreatDt { get; set; }
    }
}