﻿using System;

namespace COWS.Web.ViewModels
{
    public class ProductTypeViewModel
    {
        public byte ProdTypeId { get; set; }
        public string ProdTypeDes { get; set; }
        public DateTime CreatDt { get; set; }
        public short OrdrCatId { get; set; }
        public bool DmstcCd { get; set; }
        public string FsaProdTypeCd { get; set; }
        public string ProdNme { get; set; }
        public bool TrptCd { get; set; }
    }
}