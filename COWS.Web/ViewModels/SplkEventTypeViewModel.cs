﻿using System;

namespace COWS.Web.ViewModels
{
    public class SplkEventTypeViewModel
    {
        public byte SplkEventTypeId { get; set; }
        public string SplkEventTypeDes { get; set; }
        public byte RecStusId { get; set; }
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}