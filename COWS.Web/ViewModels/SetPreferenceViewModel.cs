﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COWS.Web.ViewModels
{
    public class SetPreferenceViewModel
    {
        public short CnfrcBrdgId { get; set; }
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string CnfrcBrdgNbr { get; set; }
        [DataType(DataType.Text)]
        [MaxLength(20)]
        public string CnfrcPinNbr { get; set; }
        [DataType(DataType.Text)]
        [MaxLength(20)]
        public string Soi { get; set; }
        [DataType(DataType.Text)]
        [MaxLength(1000)]
        public string OnlineMeetingAdr { get; set; }
        public byte? RecStusId { get; set; }
        public bool RecStatus => RecStusId == 1 ? true : false;
        public int CreatByUserId { get; set; }
        public int? ModfdByUserId { get; set; }
        public string CreatedByAdid { get; set; }
        public string ModifiedByAdid { get; set; }
        public DateTime? ModfdDt { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
