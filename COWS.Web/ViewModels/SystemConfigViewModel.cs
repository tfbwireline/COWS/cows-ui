﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace COWS.Web.ViewModels
{
    public class SystemConfigViewModel
    {
        public int CfgId { get; set; }
        public string PrmtrNme { get; set; }
        public string PrmtrValuTxt { get; set; }
        public byte RecStusId { get; set; }
        public DateTime CreatDt { get; set; }
    }
}
